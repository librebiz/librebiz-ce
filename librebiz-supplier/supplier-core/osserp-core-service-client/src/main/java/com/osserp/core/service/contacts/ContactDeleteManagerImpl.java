/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 12, 2008 6:37:31 PM 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.mail.EmailAddress;

import com.osserp.core.BusinessCaseSearch;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactDeleteManager;
import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerSummary;
import com.osserp.core.customers.SalesSummary;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Customers;
import com.osserp.core.dao.MailingLists;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.records.SalesRecordQueries;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.sales.SalesRecordSummary;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactDeleteManagerImpl extends AbstractService implements ContactDeleteManager {
    private static Logger log = LoggerFactory.getLogger(ContactDeleteManagerImpl.class.getName());
    private Contacts contacts = null;
    private Customers customers = null;
    private BusinessCaseSearch businessCaseSearch = null;
    private RecordSearch recordSearch = null;
    private SalesRecordQueries salesQueries = null;
    private Suppliers suppliers = null;
    private MailingLists mailingLists = null;

    protected ContactDeleteManagerImpl(
            Contacts contacts,
            Customers customers,
            BusinessCaseSearch businessCaseSearch,
            RecordSearch recordSearch,
            SalesRecordQueries salesQueries,
            Suppliers suppliers,
            MailingLists mailingLists) {

        this.contacts = contacts;
        this.customers = customers;
        this.businessCaseSearch = businessCaseSearch;
        this.recordSearch = recordSearch;
        this.salesQueries = salesQueries;
        this.suppliers = suppliers;
        this.mailingLists = mailingLists;
    }

    public void deleteContact(Employee user, Contact contact) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("deleteContact() invoked [user=" + user.getId()
                    + ", contact=" + contact.getContactId() + "]");
        }
        if (isQualified(contact)) {
            if (contact.isEmployee()) {
                throw new ClientException(ErrorCode.EMPLOYEE_NOT_DELETABLE);
            }
            if (contact.isClient()) {
                throw new ClientException(ErrorCode.CLIENT_NOT_DELETABLE);
            }
            if (contact.isCustomer()) {
                checkCustomer(contact);
            }
            if (contact.isSupplier()) {
                ClassifiedContact rc = suppliers.findByContact(contact);
                if (rc != null) {
                    if (recordSearch.isInvolvedInPurchase(rc)) {
                        throw new ClientException(ErrorCode.SUPPLIER_ASSIGNED_TO_PURCHASE);
                    }
                }
            }
        }
        this.delete(user, contact);
    }

    private boolean isQualified(Contact contact) {
        return (contact.isCustomer()
                || contact.isEmployee()
                || contact.isClient()
                || contact.isSupplier()
                || contact.isUser());
    }

    private void checkCustomer(Contact contact) throws ClientException {
        Customer customer = (Customer) customers.findByContact(contact);
        if (customer != null) {
            List<SalesRecordSummary> salesRecords = salesQueries.getSummary(customer);
            if (!salesRecords.isEmpty()) {
                for (int i = 0, j = salesRecords.size(); i < j; i++) {
                    SalesRecordSummary next = salesRecords.get(i);
                    if (!next.isDownpayment()) {
                        throw new ClientException(ErrorCode.NOT_DELETABLE_FINANCE_RECORD_FOUND);
                    }
                }
            }
            CustomerSummary summary = businessCaseSearch.getCustomerSummary(customer.getId());
            if (summary.getSalesCount() > 0) {
                for (int i = 0, j = summary.getSales().size(); i < j; i++) {
                    SalesSummary next = summary.getSales().get(i);
                    if (next.getStatus() != null && next.getStatus() == 100) {
                        throw new ClientException(ErrorCode.CLOSED_PROJECT_FOUND);
                    }
                    if (!next.isCancelled()) {
                        throw new ClientException(ErrorCode.OPEN_PROJECT_FOUND);
                    }
                }
            }
            if (summary.getRequestCount() > 0) {
                for (int i = 0, j = summary.getRequests().size(); i < j; i++) {
                    SalesSummary next = summary.getRequests().get(i);
                    if (!next.isCancelled()) {
                        throw new ClientException(ErrorCode.OPEN_REQUEST_FOUND);
                    }
                }
            }
        }
    }

    private void delete(Employee user, Contact contact) {
        contact.setStatus(Contact.STATUS_DELETED);
        contact.setPersonChanged(new Date(System.currentTimeMillis()));
        contact.setPersonChangedBy(user.getId());
        List<EmailAddress> mails = contact.getEmails();
        contacts.save(contact);
        for (int i = 0; i < mails.size(); i++) {
            EmailAddress next = mails.get(i);
            if (mailingLists.exists(next.getId())) {
                mailingLists.removeAddress(next.getId());
            }
            contact.removeEmail(next.getId());
            contacts.save(contact);
        }
    }
}
