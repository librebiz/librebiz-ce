<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="appointmentCreatorView"/>
<style type="text/css">
div.standardForm .label {
    min-width: 130px;
    font-weight: normal;
}
</style>
<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="${view.headerName}"/>
	</div>
	<div class="modalBoxHeaderRight">
		<div class="boxnav">
		</div>
	</div>
</div>
<div class="modalBoxData">

	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<v:ajaxForm name="dynamicForm" targetElement="appointmentCreator_popup" url="/events/appointmentCreator/save">
				<div class="standardForm">
					<div class="formContent">
						<c:import url="/errors/_errors_standardform.jsp"/>
						<div class="data start">
							<div class="label"><p><fmt:message key="appointmentType"/></p></div>
							<div class="value">
								<p><oc:select name="action" options="${view.actions}" value="${view.selectedAction.id}" styleClass="inputSelect"/></p>
							</div>
						</div>
						<div class="data">
							<div class="label"><p><fmt:message key="description"/></p></div>
							<div class="value"><p><v:textarea styleClass="form-control" name="note" rows="5"/></p></div>
						</div>
						<div class="data">
							<div class="label"><p><fmt:message key="receiver"/></p></div>
							<div class="value">
								<p><oc:select name="recipient" options="${view.recipients}" value="${view.recipient}" nameProperty="displayName" promptKey="ownAppointment" styleClass="inputSelect"/></p>
							</div>
						</div>
						<div class="data">
							<div id="datePicker" style="position: absolute; visibility: hidden; background-color: white; z-index:10;"> </div>
							<div class="label"><p><fmt:message key="appointmentAt"/></p></div>
							<div class="value">
								<p>
									<v:date style="width: 100px; text-align: right;" name="appointmentDate" id="dateInput" />
									<a href="javascript:;" onclick="var cal1x = new CalendarPopup('datePicker'); cal1x.select($('dateInput'),'anchor1x','dd.MM.yyyy');" title="cal1x.select(document.forms[0].date1x,'anchor1x','MM/dd/yyyy'); return false;" name="anchor1x" id="anchor1x">
										<img src="<c:url value='${applicationScope.calendarIcon}'/>" />
									</a>
									<fmt:message key="atTime"/>
									<v:text styleClass="inputDays" name="appointmentHours"/> :
									<v:text styleClass="inputDays" name="appointmentMinutes"/>&nbsp;&nbsp;<fmt:message key="clock"/>
								</p>
							</div>
						</div>
						<div class="data">
							<div class="label"><p><fmt:message key="memoryFrom"/></p></div>
							<div class="value">
								<p>
									<v:date style="width: 100px; text-align: right;" name="reminderDate" id="reminderInput" />
									<a href="javascript:;" onclick="var cal1x = new CalendarPopup('datePicker'); cal1x.select($('reminderInput'),'anchor1x','dd.MM.yyyy');" title="cal1x.select(document.forms[0].date1x,'anchor1x','MM/dd/yyyy'); return false;" name="anchor1x" id="anchor1x">
										<img src="<c:url value='${applicationScope.calendarIcon}'/>" />
									</a>
									<fmt:message key="atTime"/>
									<v:text styleClass="inputDays" name="reminderHours"/> :
									<v:text styleClass="inputDays" name="reminderMinutes"/>&nbsp;&nbsp;<fmt:message key="clock"/>
								</p>
							</div>
						</div>
						<div class="data inputSubmit" style="margin-bottom: 15px;">
							<div class="label"><p>&nbsp;</p></div>
							<div class="value">
								<v:ajaxLink url="/events/appointmentCreator/exit" linkId="exitLink" targetElement="appointmentCreator_popup">
									<input type="button" class="submit submitExit" value="<fmt:message key="exit"/>" />
								</v:ajaxLink>
								<input type="submit" class="submit" style="margin-left: 90px;" value="<fmt:message key="save"/>" />
							</div>
						</div>
					</div>	
				</div>	
			</v:ajaxForm>
			<div class="spacer"></div>
		</div>
	</div>
</div>
