/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 8, 2011 12:31:08 PM 
 * 
 */
package com.osserp.core.sales;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.products.ProductSelectionConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRevenueConfig extends AbstractOption {

    private ProductSelectionConfig hardwareCostsAFilter;
    private ProductSelectionConfig hardwareCostsBFilter;
    private ProductSelectionConfig hardwareCostsCFilter;
    private ProductSelectionConfig otherCostsFilter;
    private ProductSelectionConfig foreignCostsFilter;
    private ProductSelectionConfig developmentCostsFilter;
    private ProductSelectionConfig unknownCostsFilter;
    private boolean initialized;

    protected SalesRevenueConfig() {
        super();
    }

    public SalesRevenueConfig(String name) {
        super(name);
    }

    public void setFilters(
            ProductSelectionConfig hardwareCostsAFilter,
            ProductSelectionConfig hardwareCostsBFilter,
            ProductSelectionConfig hardwareCostsCFilter,
            ProductSelectionConfig otherCostsFilter,
            ProductSelectionConfig foreignCostsFilter,
            ProductSelectionConfig developmentCostsFilter,
            ProductSelectionConfig unknownCostsFilter) {
        this.hardwareCostsAFilter = hardwareCostsAFilter;
        this.hardwareCostsBFilter = hardwareCostsBFilter;
        this.hardwareCostsCFilter = hardwareCostsCFilter;
        this.otherCostsFilter = otherCostsFilter;
        this.foreignCostsFilter = foreignCostsFilter;
        this.developmentCostsFilter = developmentCostsFilter;
        this.unknownCostsFilter = unknownCostsFilter;
        this.initialized = true;
    }

    public boolean isInitialized() {
        return initialized;
    }

    protected void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    public ProductSelectionConfig getHardwareCostsAFilter() {
        return hardwareCostsAFilter;
    }
    public ProductSelectionConfig getHardwareCostsBFilter() {
        return hardwareCostsBFilter;
    }
    public ProductSelectionConfig getHardwareCostsCFilter() {
        return hardwareCostsCFilter;
    }
    public ProductSelectionConfig getOtherCostsFilter() {
        return otherCostsFilter;
    }
    public ProductSelectionConfig getForeignCostsFilter() {
        return foreignCostsFilter;
    }
    public ProductSelectionConfig getDevelopmentCostsFilter() {
        return developmentCostsFilter;
    }
    public ProductSelectionConfig getUnknownCostsFilter() {
        return unknownCostsFilter;
    }

    protected void setHardwareCostsAFilter(ProductSelectionConfig hardwareCostsAFilter) {
        this.hardwareCostsAFilter = hardwareCostsAFilter;
    }
    protected void setHardwareCostsBFilter(ProductSelectionConfig hardwareCostsBFilter) {
        this.hardwareCostsBFilter = hardwareCostsBFilter;
    }
    protected void setHardwareCostsCFilter(ProductSelectionConfig hardwareCostsCFilter) {
        this.hardwareCostsCFilter = hardwareCostsCFilter;
    }
    protected void setOtherCostsFilter(ProductSelectionConfig otherCostsFilter) {
        this.otherCostsFilter = otherCostsFilter;
    }
    protected void setForeignCostsFilter(ProductSelectionConfig foreignCostsFilter) {
        this.foreignCostsFilter = foreignCostsFilter;
    }
    protected void setDevelopmentCostsFilter(ProductSelectionConfig developmentCostsFilter) {
        this.developmentCostsFilter = developmentCostsFilter;
    }
    protected void setUnknownCostsFilter(ProductSelectionConfig unknownCostsFilter) {
        this.unknownCostsFilter = unknownCostsFilter;
    }

    public String getHardwareCostsAFilterName() {
        return hardwareCostsAFilter != null ? hardwareCostsAFilter.getName() : null;
    }
    public String getHardwareCostsBFilterName() {
        return hardwareCostsBFilter != null ? hardwareCostsBFilter.getName() : null;
    }
    public String getHardwareCostsCFilterName() {
        return hardwareCostsCFilter != null ? hardwareCostsCFilter.getName() : null;
    }
    public String getOtherCostsFilterName() {
        return otherCostsFilter != null ? otherCostsFilter.getName() : null;
    }
    public String getForeignCostsFilterName() {
        return foreignCostsFilter != null ? foreignCostsFilter.getName() : null;
    }
    public String getDevelopmentCostsFilterName() {
        return developmentCostsFilter != null ? developmentCostsFilter.getName() : null;
    }
    public String getUnknownCostsFilterName() {
        return unknownCostsFilter != null ? unknownCostsFilter.getName() : null;
    }

    public String getHardwareCostsAFilterDescription() {
        return hardwareCostsAFilter != null ? hardwareCostsAFilter.getDescription() : null;
    }
    public String getHardwareCostsBFilterDescription() {
        return hardwareCostsBFilter != null ? hardwareCostsBFilter.getDescription() : null;
    }
    public String getHardwareCostsCFilterDescription() {
        return hardwareCostsCFilter != null ? hardwareCostsCFilter.getDescription() : null;
    }
    public String getOtherCostsFilterDescription() {
        return otherCostsFilter != null ? otherCostsFilter.getDescription() : null;
    }
    public String getForeignCostsFilterDescription() {
        return foreignCostsFilter != null ? foreignCostsFilter.getDescription() : null;
    }
    public String getDevelopmentCostsFilterDescription() {
        return developmentCostsFilter != null ? developmentCostsFilter.getDescription() : null;
    }
    public String getUnknownCostsFilterDescription() {
        return unknownCostsFilter != null ? unknownCostsFilter.getDescription() : null;
    }
}
