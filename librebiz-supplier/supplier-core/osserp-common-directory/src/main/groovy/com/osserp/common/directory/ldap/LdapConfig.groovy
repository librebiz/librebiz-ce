/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.apache.directory.groovyldap.LDAP

import com.osserp.common.PropertyProvider
import com.osserp.common.beans.Configurable


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class LdapConfig extends Configurable {
    private static Logger logger = LoggerFactory.getLogger(LdapConfig.class.getName())

    private static final String APPLICATION_NAME = 'osserp'
    private static final String CUSTOM_CONFIG_FILE_NAME = 'custom.properties'
    private static final String DEFAULT_CONFIG_FILE_NAME = 'ldap.properties'

    private static final String LOGIN_ENABLED = '/bin/bash'
    private static final String LOGIN_DISABLED = '/bin/false'

    static final int INTERACTIVE_MODE = 1
    static final int CONFIG_FILE_MODE = 2


    String serverUrl = 'localhost:389'
    String base

    String admin
    String adminPassword

    List adminObjectClasses = []
    String adminIdStart
    String adminQuery
    String adminAccountsClientOu

    List clientObjectClasses = []
    String clientIdStart
    String clientQuery

    List domainObjectClasses = []
    String domainQuery

    List groupObjectClasses = []
    String groupIdStart
    String groupQuery

    List userObjectClasses = []
    String userIdStart
    String userGroupDefault
    String userMailaliasAttribute
    String userQuery

    String userLang
    String userTimezone
    String userHomeDirectory
    String userShellEnabled
    String userShellDisabled
    String userMailStoreDir
    String userUidnumberMax
    String userUidnumberMin
    String userMailboxMax
    String userMailboxMin
    String userSubscriberMax
    String userSubscriberMin

    List permissionObjectClasses = []

    // dbmail support
    private String dbmailSupportFlag
    List dbmailObjectClasses = []
    String dbmailDefaultDomain

    // OpenXchange support
    private String oxSupportFlag
    String oxAppointmentdays
    String oxTaskdays
    String oxEnabled
    String oxDisabled

    // Samba support
    private String sambaSupportFlag
    List sambaObjectClasses = []
    String sambaObjectClass
    String sambaObjectClassGroup
    String sambaSid
    String sambaGid
    String sambaKickofftime
    String sambaLogontime
    String sambaLogofftime
    String sambaPwdcanchange
    String sambaPwdmustchange
    String sambaHomeDrive

    String lmPasswordFlag
    String ntPasswordFlag

    private String zarafaSupportFlag
    private String fetchmailSupportFlag
    private String internetdomainSupportFlag

    FetchmailConfig fetchmailConfig

    boolean noc

    private String configuredBy
    private Object configuration

    LdapConfig() {
        configureProperties(findProperties())
        if (initialized) {
            configuredBy = 'fileLookup'
        }
    }

    LdapConfig(String configFile) {
        configureProperties(findPropertiesByFilename(configFile))
        if (!initialized) {
            configureProperties(findPropertiesByFilename(CUSTOM_CONFIG_FILE_NAME))
        }
        if (initialized) {
            configuredBy = 'fileName'
            configuration = configFile
        }
    }

    LdapConfig(Properties res) {
        configureProperties(res)
        if (initialized) {
            configuredBy = 'properties'
            configuration = res
        }
    }

    LdapConfig(String basedn, String server, String adminName, String password, PropertyProvider propertyProvider) {
        base = basedn
        serverUrl = server
        admin = adminName
        adminPassword = password
        configureProperties(propertyProvider?.properties)
        if (initialized) {
            configuredBy = 'propertyProvider'
            configuration = propertyProvider
        }
    }

    void setInitializeBy(String configFile) {
        configureProperties(findPropertiesByFilename(configFile))
        if (initialized) {
            configuredBy = 'fileName'
            configuration = configFile
        }
    }

    void reloadConfiguration() {
        if (configuredBy == 'fileName' && configuration instanceof String) {
            configureProperties(findPropertiesByFilename(configuration))
        } else if (configuredBy == 'properties' && configuration instanceof Properties) {
            configureProperties(configuration)
        } else if (configuredBy == 'propertyProvider' && configuration instanceof PropertyProvider) {
            configureProperties(configuration?.properties)
        }
    }

    String getLoginName() {
        admin
    }

    String getApplicationName() {
        APPLICATION_NAME
    }

    String getConfigFileName() {
        DEFAULT_CONFIG_FILE_NAME
    }

    boolean getInitialized() {
        (serverUrl && admin && adminPassword && base)
    }

    boolean getDomainAvailable() {
        if (!initialized) {
            logger.warn("domainAvailable: missing property [base='$base', host:$serverUrl, passwordExists=${(adminPassword != null)}]")
            return false
        }
        try {
            LDAP ldap = createConnection()
            return ldap.exists(getBase())
        } catch (Throwable t) {
            logger.error("domainAvailable: not found [name='$base', host:$serverUrl, exception=${t.getClass().getName()}, error=${t.message}]")
            return false
        }
    }

    boolean getDbmailSupportEnabled() {
        ('true' == dbmailSupportFlag)
    }

    boolean getFetchmailSupportEnabled() {
        ('true' == fetchmailSupportFlag)
    }

    boolean getOxSupportEnabled() {
        ('true' == oxSupportFlag)
    }

    boolean getSambaSupportEnabled() {
        ('true' == sambaSupportFlag)
    }

    boolean getZarafaSupportEnabled() {
        ('true' == zarafaSupportFlag)
    }

    boolean getNtPasswordEnabled() {
        ('true' == ntPasswordFlag)
    }

    boolean getLmPasswordEnabled() {
        ('true' == lmPasswordFlag)
    }

    @Override
    String toString() {
        "[serverUrl:$serverUrl,base:$base,admin:$admin,adminPassword:$adminPassword]"
    }

    LDAP createConnection() {
        LDAP.newInstance(serverUrl, loginName, adminPassword)
    }

    private def configureProperties(Properties props) {
        if (!props.entrySet().isEmpty()) {

            def ldapbase = props.getProperty('ldapBase')
            if (ldapbase) {
                base = ldapbase
            }
            def server = props.getProperty('ldapServer')
            if (server) {
                serverUrl = server
            }
            def name = props.getProperty('ldapAdministrator')
            if (name) {
                admin = name
            }
            def password = props.getProperty('ldapPassword')
            if (password) {
                adminPassword = password
            }
            configureOptions(props)
        } else {
            logger.warn('configureProperties: Empty properties provided')
        }
    }

    void configureOptions(Properties props) {
        if (!props.entrySet().isEmpty()) {

            // NOC specific properties
            adminIdStart = fetchProperty(props, 'ldapAdminStartId', adminIdStart)
            adminQuery = fetchProperty(props, 'ldapAdminQuery', adminQuery)
            String adminClasses = props.getProperty('ldapAdminObjectclasses')
            if (adminClasses) {
                adminObjectClasses = adminClasses.tokenize(',')
            }
            if (props.getProperty('ldapAdminClientOu')) {
                adminAccountsClientOu = props.getProperty('ldapAdminClientOu')
            }

            clientIdStart = fetchProperty(props, 'ldapClientStartId', clientIdStart)
            clientQuery = fetchProperty(props, 'ldapClientQuery', clientQuery)
            String clientClasses = props.getProperty('ldapClientObjectclasses')
            if (clientClasses) {
                clientObjectClasses = clientClasses.tokenize(',')
            }

            // common properties
            domainQuery = fetchProperty(props, 'ldapInternetdomainQuery', domainQuery)
            String domainClasses = props.getProperty('ldapInternetdomainObjectclasses')
            if (domainClasses) {
                domainObjectClasses = domainClasses.tokenize(',')
            }

            groupIdStart = fetchProperty(props, 'ldapGroupStartId', groupIdStart)
            groupQuery = fetchProperty(props, 'ldapGroupQuery', groupQuery)
            String groupClasses = props.getProperty('ldapGroupObjectclasses')
            if (groupClasses) {
                groupObjectClasses = groupClasses.tokenize(',')
            }

            userIdStart = fetchProperty(props, 'ldapUserStartIdSystem', userIdStart)
            userUidnumberMax = fetchProperty(props, 'ldapUserUidnumberMax', userUidnumberMax)
            userUidnumberMin = fetchProperty(props, 'ldapUserUidnumberMin', userUidnumberMin)
            userMailboxMax = fetchProperty(props, 'ldapUserMailboxMax', userMailboxMax)
            userMailboxMin = fetchProperty(props, 'ldapUserMailboxMin', userMailboxMin)
            userSubscriberMax = fetchProperty(props, 'ldapUserSubscriberMax', userSubscriberMax)
            userSubscriberMin = fetchProperty(props, 'ldapUserSubscriberMin', userSubscriberMin)

            userQuery = fetchProperty(props, 'ldapUserQuery', userQuery)
            String userClasses = props.getProperty('ldapUserObjectclasses')
            if (userClasses) {
                userObjectClasses = userClasses.tokenize(',')
            }
            if (props.getProperty('ldapUserMailStorDir')) {
                userMailStoreDir = props.getProperty('ldapUserMailStorDir')
            }

            userLang = fetchProperty(props, 'ldapUserLang', userLang)
            userHomeDirectory = fetchProperty(props, 'ldapUserHome', userHomeDirectory)
            userTimezone = fetchProperty(props, 'ldapUserTimezone', userTimezone)
            userGroupDefault = fetchProperty(props, 'ldapUserGroupId', userGroupDefault)

            userShellEnabled = props.getProperty('ldapUserShellEnabled') ?: LOGIN_ENABLED
            userShellDisabled = props.getProperty('ldapUserShellDisabled') ?: LOGIN_DISABLED

            def aliasattr = props.getProperty('ldapUserAlias')
            if (aliasattr) {
                userMailaliasAttribute = aliasattr
            }

            // dbmail support
            dbmailSupportFlag = props.getProperty('ldapEnableDbmail')
            dbmailDefaultDomain = props.getProperty('ldapUserDbmailDefaultDomain')

            // fetchmail support
            fetchmailSupportFlag = props.getProperty('ldapEnableFetchmail')
            if (fetchmailSupportEnabled) {
                FetchmailConfig fmc = new FetchmailConfig(
                        fetchMode: props.getProperty('ldapFetchmailMode'),
                        serverName: props.getProperty('ldapFetchmailServerName'),
                        serverType: props.getProperty('ldapFetchmailServerType'))
                if (fmc.serverName && fmc.serverType && fmc.fetchMode) {
                    fetchmailConfig = fmc
                }
            }

            // samba support
            sambaSupportFlag = props.getProperty('ldapEnableSamba')
            if (sambaSupportEnabled) {
                userObjectClasses << 'sambaSamAccount'
                sambaSid = props.getProperty('ldapSambaSidUser')
                sambaGid = props.getProperty('ldapSambaSidGroup')
                sambaHomeDrive = props.getProperty('ldapSambaHomedrive')
                sambaKickofftime = props.getProperty('ldapSambaKickofftime')
                sambaLogontime = props.getProperty('ldapSambaLogontime')
                sambaLogofftime = props.getProperty('ldapSambaLogofftime')
                sambaPwdcanchange = props.getProperty('ldapSambaPwdCanChange')
                sambaPwdmustchange = props.getProperty('ldapSambaPwdMustChange')
                ntPasswordFlag = props.getProperty('ldapSambaNTPassword')
                lmPasswordFlag = props.getProperty('ldapSambaLMPassword')
            }

            // ox support
            oxSupportFlag = props.getProperty('ldapEnableOx')
            if (oxSupportEnabled) {
                oxAppointmentdays = props.getProperty('ldapOxAppointmentdays')
                oxTaskdays = props.getProperty('ldapOxTaskdays')
                oxEnabled = props.getProperty('ldapOxEnabled')
                oxDisabled = props.getProperty('ldapOxDisabled')
            }
        }
    }

    protected String fetchProperty(Properties props, String name, String defaultValue) {
        def propVal = props.getProperty(name)
        (propVal != null && propVal != 'default' && propVal != 'null') ? propVal : defaultValue
    }
}
