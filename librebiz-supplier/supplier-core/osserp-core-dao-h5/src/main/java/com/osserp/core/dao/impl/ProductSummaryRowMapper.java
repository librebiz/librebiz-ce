/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 7, 2007 12:55:05 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.dao.AbstractRowMapper;

import com.osserp.core.products.ProductSummaryImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSummaryRowMapper extends AbstractRowMapper {

    private Long stockId = null;

    public ProductSummaryRowMapper(Long stockId) {
        this.stockId = stockId;
    }

    public static RowMapperResultSetExtractor getReader(Long stockId) {
        return new RowMapperResultSetExtractor(new ProductSummaryRowMapper(stockId));
    }

    public static final String LIST =
            "product_id," + // 1
                    "name," + // 2
                    "description," + // 3
                    "group_id," + // 4
                    "end_of_life," + // 5 
                    "is_virtual," + // 6
                    "has_stock," + // 7
                    "stock," + // 8
                    "receipt," + // 9
                    "expected," + // 10
                    "ordered," + // 11
                    "vacant," + // 12
                    "unreleased," + // 13
                    "purchase_avg," + // 14 
                    "purchase_last," + // 15
                    "receipt_sales "; // 16

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new ProductSummaryImpl(
                this.stockId,
                Double.valueOf(rs.getDouble(8)),
                Double.valueOf(rs.getDouble(9)),
                Double.valueOf(rs.getDouble(10)),
                Double.valueOf(rs.getDouble(11)),
                Double.valueOf(rs.getDouble(12)),
                Double.valueOf(rs.getDouble(13)),
                Double.valueOf(rs.getDouble(14)),
                Double.valueOf(rs.getDouble(15)),
                Double.valueOf(rs.getDouble(16)));
    }
}
