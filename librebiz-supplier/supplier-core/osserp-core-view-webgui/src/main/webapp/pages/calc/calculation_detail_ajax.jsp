<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<c:set var="view" value="${sessionScope.calculationDetailView}" />

<c:if test="${!empty view}">

    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <fmt:message key="changeCalculationInfo" />
        </div>
    </div>
    <div class="modalBoxData">
        <div class="row">
            <div class="col-md-12 panel-area panel-area-default">
                <o:ajaxForm name="dynamicForm" onSuccess="" preRequest="" postRequest=""
                    targetElement="calculation_detail_popup" url="/calculationDetail.do">
                    <input type="hidden" name="method" value="save" />
                    <div class="panel-body">
                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="name"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" value="<o:out value="${view.calculation.name}" />" />
                                    </div>
                                </div>
                            </div>

                            <div class="row next">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <o:ajaxLink url="/calculationDetail.do?method=exit" linkId="exitLink" targetElement="calculation_detail_popup">
                                                    <input type="button" class="form-control cancel" value="<fmt:message key="exit"/>" />
                                                </o:ajaxLink>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <o:submit styleClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </o:ajaxForm>
            </div>
        </div>
    </div>
</c:if>