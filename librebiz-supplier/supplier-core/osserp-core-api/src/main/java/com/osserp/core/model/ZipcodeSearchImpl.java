/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 21, 2011 12:55:30 PM 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.contacts.ZipcodeSearch;

/**
 * 
 * @author jg <jg@osserp.com>
 */
public class ZipcodeSearchImpl extends AbstractEntity implements ZipcodeSearch {

    String callFrom;
    Long contactId;
    String searchFor;
    String zipcode;
    String city;
    String street;
    String district;

    public ZipcodeSearchImpl() {
        super();
    }

    public ZipcodeSearchImpl(Long employee, String callFrom, Long contactId, String searchFor, String zipcode, String city, String street, String district) {
        super(null, employee);
        this.callFrom = callFrom;
        this.contactId = contactId;
        this.searchFor = searchFor;
        this.zipcode = zipcode;
        this.city = city;
        this.street = street;
        this.district = district;
    }

    public String getCallFrom() {
        return callFrom;
    }

    public void setCallFrom(String callFrom) {
        this.callFrom = callFrom;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public String getSearchFor() {
        return searchFor;
    }

    public void setSearchFor(String searchFor) {
        this.searchFor = searchFor;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
