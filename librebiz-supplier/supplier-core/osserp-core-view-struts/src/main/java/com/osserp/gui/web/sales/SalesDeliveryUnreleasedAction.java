/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Dec-2006 10:14:35 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.core.products.Product;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.products.ProductView;
import com.osserp.gui.client.sales.UnreleasedDeliveriesView;
import com.osserp.gui.web.struts.AbstractViewAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesDeliveryUnreleasedAction extends AbstractViewAction {
    private static Logger log = LoggerFactory.getLogger(SalesDeliveryUnreleasedAction.class.getName());

    /**
     * Forwards to offer listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forward() request from " + getUser(session).getId());
        }
        UnreleasedDeliveriesView view = createUnreleasedDeliveriesView(request);
        String exitTarget = RequestUtil.getExit(request);
        if (exitTarget != null) {
            view.setExitTarget(exitTarget);
        }
        Long productId = RequestUtil.getLong(request, "id", false);
        if (productId != null) {
            ProductView productView = (ProductView) fetchView(session, ProductView.class);
            if (productView != null
                    && productView.getBean() != null) {
                view.refresh(productView.getProduct());
            } else {
                productView = (ProductView) createView(request, ProductView.class.getName());
                Product product = productView.getProduct();
                view.refresh(product);
            }
        } else {
            view.refresh();
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to offer listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        UnreleasedDeliveriesView view = fetchUnreleasedDeliveriesView(session);
        if (view != null) {
            String exitTarget = view.getExitTarget();
            if (exitTarget != null) {
                removeView(session, UnreleasedDeliveriesView.class);
                return mapping.findForward(exitTarget);
            }
        }
        removeView(session, UnreleasedDeliveriesView.class);
        return mapping.findForward(Actions.EXIT);
    }

    /**
     * Changes summary mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeSummaryMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeSummaryMode() request from " + getUser(session).getId());
        }
        UnreleasedDeliveriesView view = fetchUnreleasedDeliveriesView(session);
        if (view == null) {
            log.warn("changeSummaryMode() failed: no view bound!");
            throw new com.osserp.common.ActionException();
        }
        view.changeSummaryMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Refreshs view data
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward refresh(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("refresh() request from " + getUser(session).getId());
        }
        UnreleasedDeliveriesView view = fetchUnreleasedDeliveriesView(session);
        if (view == null) {
            if (log.isDebugEnabled()) {
                log.debug("refresh() no view bound, forwarding...");
            }
            return forward(mapping, form, request, response);
        }
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    private UnreleasedDeliveriesView createUnreleasedDeliveriesView(HttpServletRequest request) throws ClientException, PermissionException {
        return (UnreleasedDeliveriesView) createView(request, UnreleasedDeliveriesView.class.getName());
    }

    private UnreleasedDeliveriesView fetchUnreleasedDeliveriesView(HttpSession session) throws PermissionException {
        return (UnreleasedDeliveriesView) fetchView(session, UnreleasedDeliveriesView.class);
    }
}
