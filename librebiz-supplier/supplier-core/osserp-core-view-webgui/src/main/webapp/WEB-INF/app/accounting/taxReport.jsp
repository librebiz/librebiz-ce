<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="taxReportView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="styles" type="string">
        <style type="text/css">

        .recordId {
            width: 84px;
        }

        .recordAmount {
            text-align: right;
            width: 80px;
        }

        .recordTaxAmount {
            text-align: right;
            width: 74px;
        }

        .recordContact {
            width: 220px;
        }

        .recordDate {
            width: 70px;
        }

        .table .action {
            text-align: center;
            width: 25px;
        }
        
        .reportName {
            width: 450px;
        }
        
        .reportDate {
            width: 70px;
        }
        
        .reportStatus {
            width: 188px;
        }
        
        </style>
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="taxReportLabel" />
        <c:choose>
            <c:when test="${view.inflowSelectionMode}">
                - <fmt:message key="debitors" />
            </c:when>
            <c:when test="${view.outflowSelectionMode}">
                - <fmt:message key="creditors" />
            </c:when>
        </c:choose>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    
    <tiles:put name="content" type="string" >
        <div class="subcolumns">
            <c:choose>
                <c:when test="${view.createMode}">
                    <c:import url="_taxReportCreate.jsp"/>
                </c:when>
                <c:when test="${view.inflowSelectionMode or view.outflowSelectionMode}">
                    <div class="subcolumn">
                        <div class="spacer"></div>
                        <c:import url="_taxReportRecordList.jsp"/>
                    </div>
                    <div class="spacer"></div>
                </c:when>
                <c:when test="${empty view.bean}">
                
                    <c:set var="taxReports" scope="request" value="${view.list}"/>
                    <c:set var="taxReportCreateUrl" scope="request" value="/accounting/taxReports/enableCreateMode"/>
                    <c:set var="taxReportSelectUrl" scope="request" value="/accounting/taxReports/select"/>
                    
                    <div class="subcolumn">
                        <div class="spacer"></div>
                        <div class="table-responsive table-responsive-default">
                        <c:import url="_taxReportSelectionList.jsp"/>
                        </div>
                    </div>
                    <div class="spacer"></div>
                </c:when>
                <c:otherwise>
                    <div class="subcolumn">
                        <div class="spacer"></div>
                        <c:import url="_taxReportDisplay.jsp"/>
                    </div>
                    <div class="spacer"></div>
                </c:otherwise>
            </c:choose>
        </div>
    </tiles:put>
</tiles:insert>
