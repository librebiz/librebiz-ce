/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.dms;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Entity;
import com.osserp.common.PersistentEntity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DmsReference implements Serializable {
    private static Logger log = LoggerFactory.getLogger(DmsReference.class.getName());

    private Long typeId;
    private Long referenceId;
    private Object reference;
    private Long referenceType;

    /**
     * Default constructor.
     */
    protected DmsReference() {
        super();
    }

    /**
     * Default constructor for external documents. Binaries of external documents will be stored on the filesystem. 
     * TODO Store binaries in a JCR compliant repository
     * @param typeId
     * @param referenceId
     */
    public DmsReference(Long typeId, Long referenceId) {
        super();
        this.typeId = typeId;
        this.referenceId = referenceId;
    }

    /**
     * Constructor for internal documents with a reference implementing Entity or PersistentEntity and a dedicated referenceType.
     * @param reference
     * @param referenceType
     */
    public DmsReference(Object reference, Long referenceType) {
        super();
        this.referenceType = referenceType;
        initReference(reference);
    }

    private void initReference(Object ref) {
        reference = ref;
        if (reference instanceof PersistentEntity) {
            referenceId = ((PersistentEntity) reference).getPrimaryKey();
        } else if (reference instanceof Entity) {
            referenceId = ((Entity) reference).getId();
        } else {
            log.warn("initReference() invoked with unknown reference [class="
                    + (reference == null ? "null" : reference.getClass().getName())
                    + "]");
        }
    }

    public Long getTypeId() {
        return typeId;
    }

    protected void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getReferenceId() {
        return referenceId;
    }

    protected void setReferenceId(Long referenceId) {
        this.referenceId = referenceId;
    }

    public Long getReferenceType() {
        return referenceType;
    }

    protected void setReferenceType(Long referenceType) {
        this.referenceType = referenceType;
    }

    public Object getReference() {
        return reference;
    }

    protected void setReference(Object reference) {
        this.reference = reference;
    }
}
