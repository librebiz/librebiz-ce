/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api

import javax.persistence.Column
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.MapKeyColumn
import javax.persistence.Table


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Entity
@Table(name='ws_product')
class WsProduct extends WsObject implements WSProduct {
    private static final long serialVersionUID = 4L

    WsProduct() {
        super()
    }

    WsProduct(String name, String description, Map<String, String> propertyMap) {
        super()
        this.name = name
        this.description = description
        this.propertyMap = propertyMap
    }

    @ElementCollection(fetch=FetchType.EAGER) 
    @JoinTable(name='ws_product_property', joinColumns=@JoinColumn(name="reference_id"))
    @MapKeyColumn(name='name')
    @Column(name='val')
    Map<String, String> propertyMap = [:]

    @Override
    Map<String, Object> getProperties() {
        propertyMap
    }

    @Column(name='type_id')
    Long typeId
    @Column(name='type_name')
    String type

    @Column(name='group_id')
    Long groupId
    @Column(name='group_name')
    String group

    @Column(name='category_id')
    Long categoryId
    @Column(name='category_name')
    String category

    String name
    String info
    String description
    @Column(name='description_short')
    String shortDescription
    @Column(name='marketing_text')
    String marketingText
    @Column(name='tech_note')
    String techNote
    @Column(name='payment_note')
    String paymentNote
    String gtin

    String manufacturer

    String color

    Double width
    Double height
    Double lenght
    @Column(name='munit')
    String measurementUnit

    @Column(name='qunit')
    String quantityUnit
    Double weight
    @Column(name='wunit')
    String weightUnit

    Double power
    @Column(name='punit')
    String powerUnit

    @Column(name='onstock')
    Double availableStock

    @Column(name='cprice')
    Double consumerPrice
    @Column(name='rprice')
    Double resellerPrice
    @Column(name='pprice')
    Double partnerPrice
    @Column(name='ppprice')
    Double purchasePrice

    @Column(name='price')
    Double price
    @Column(name='nprice')
    Double priceNet

    @Column(name='vat_reduced')
    boolean vatReduced
    @Column(name='eol')
    boolean endOfLife
    @Column(name='subscription')
    boolean subscription
}
