/**
 *
 * Copyright (C) 2018 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 * 
 * Created on Sep 28, 2018 
 * 
 */
package com.osserp.gui.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.DateUtil

import com.osserp.core.finance.RecordMailManager
import com.osserp.core.mail.MailMessageContext

import com.osserp.gui.mail.MailEditorView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RecordMailView extends RecordPrintView {
    private static Logger logger = LoggerFactory.getLogger(RecordMailView.class.getName())

    RecordMailView() {
        super()
        dependencies = ['mailEditorView']
    }

    @Override
    boolean isForwardRequest() {
        requestContext.method == 'forward'
    }

    MailMessageContext getMailMessageContext() {
        Map<String, String> opts = [ successTarget: '/records/recordMail/mailSent' ]
        if (exitTarget) {
            opts.exitTarget = exitTarget
        }
        return recordMailManager.createContext(domainEmployee, record, opts)
    }

    String logMail() {
        MailEditorView editorView = env['mailEditorView']
        if (editorView?.mailMessageContext?.sendStatus == 'OK') {
            logger.debug("logMail: mail was sent")
            recordMailManager.writeLog(
                    domainEmployee,
                    record,
                    editorView.mailMessageContext.mail?.originator,
                    editorView.recipients,
                    editorView.recipientsCC,
                    editorView.recipientsBCC,
                    null) // TODO messageID
            record.setDateSent(DateUtil.getCurrentDate())
            recordManager.persist(record)
            reloadViewReference()
        }
        return exitTarget
    }

    protected RecordMailManager getRecordMailManager() {
        getService(RecordMailManager.class.getName())
    }
}
