<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.contactSearchView}" />

<c:if test="${!empty sessionScope.statusMessage}">
	<div class="recordheader">
		<p><o:out value="${sessionScope.statusMessage}" /></p>
	</div>
	<c:remove var="statusMessage" scope="session" />
</c:if>
<div class="table-responsive table-responsive-default">
	<table class="table table-striped">
		<thead>
			<tr>
				<th><fmt:message key="companySlashName" /></th>
				<th><fmt:message key="street" /></th>
				<th><fmt:message key="city" /></th>
				<th><fmt:message key="email"/></th>
				<th class="contact-key"><fmt:message key="typeLabel"/></th>
				<th class="contact-key"><fmt:message key="grp"/></th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty view.list}">
					<tr>
						<td colspan="6"><fmt:message key="searchUnsuccessfulPleaseChangeYourInput" /></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="contact" items="${view.list}" varStatus="s">
						<tr id="contact${contact.primaryKey}">
							<td>
								<a href="<v:url value="/contacts/contactSearch/select?id=${contact.primaryKey}&exitId=contact${contact.primaryKey}"/>"><o:out value="${contact.displayName}" /></a>
								<c:if test="${!empty contact.parent}"> <a href="<c:url value="/contacts.do?method=load&exit=contactSearchResult&exitId=contact${contact.primaryKey}&id=${contact.parentContact}"/>">(<o:out value="${contact.parent}"/>)</a></c:if>
							</td>
							<td><o:out value="${contact.streetDisplay}" /></td>
							<td><o:out value="${contact.addressDisplay}" /></td>
							<td>
								<o:email value="${contact.email}" limit="50" />
							</td>
							<td class="contact-key"><span title="${contact.type.description}"><o:out value="${contact.type.shortname}"/></span></td>
							<td class="contact-key">
								<c:choose>
									<c:when test="${contact.client}">
										<span title="<fmt:message key="client"/>">Co</span>
									</c:when>
									<c:otherwise>
										<c:if test="${contact.employee}"><span title="<fmt:message key="employee"/>"><fmt:message key="contact.type.employee"/> </span></c:if>
										<c:if test="${contact.customer}"><span title="<fmt:message key="customer"/>"><fmt:message key="contact.type.customer"/> </span></c:if>
										<c:if test="${contact.supplier}"><span title="<fmt:message key="supplier"/>"><fmt:message key="contact.type.supplier"/> </span></c:if>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>