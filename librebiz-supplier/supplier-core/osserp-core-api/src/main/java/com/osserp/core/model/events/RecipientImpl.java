/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Oct-2005 21:14:55 
 * 
 */
package com.osserp.core.model.events;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.contacts.Contact;
import com.osserp.core.events.Recipient;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecipientImpl extends AbstractEntity implements Recipient {
    private static final long serialVersionUID = 42L;

    private Long poolId = null;
    private Long type = null;
    private Long recipientId = null;
    private String recipientName = null;

    protected RecipientImpl() {
        super();
    }

    public RecipientImpl(Long poolId, Long employeeId) {
        this.type = Contact.EMPLOYEE;
        this.poolId = poolId;
        this.recipientId = employeeId;
    }

    public RecipientImpl(Long id, Long type, Long recipientId) {
        super(id);
        this.type = type;
        this.recipientId = recipientId;
    }

    public RecipientImpl(Long id, Long type, String recipientName) {
        super(id);
        this.type = type;
        this.recipientName = recipientName;
    }

    protected Long getPoolId() {
        return poolId;
    }

    protected void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Long recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }
}
