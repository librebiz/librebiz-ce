/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 26, 2008 9:04:48 AM 
 * 
 */
package com.osserp.core.model.contacts.groups;

import com.osserp.common.EntityRelation;
import com.osserp.common.beans.AbstractOption;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeDescription;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeDescriptionImpl extends AbstractOption
        implements EmployeeDescription, EntityRelation {

    private Long i18nConfigId = null;
    private boolean defaultDescription = false;

    protected EmployeeDescriptionImpl() {
        super();
    }

    protected EmployeeDescriptionImpl(EmployeeDescription o) {
        super(o);
        this.i18nConfigId = o.getI18nConfigId();
        this.defaultDescription = o.isDefaultDescription();
    }

    public EmployeeDescriptionImpl(
            Employee createdBy,
            Long i18nConfigId,
            Employee employee,
            String name,
            String description,
            boolean defaultDescription) {
        super((Long) null, employee.getId(), name, description, null, createdBy.getId());
        this.defaultDescription = defaultDescription;
        this.i18nConfigId = i18nConfigId;
    }

    public Long getI18nConfigId() {
        return i18nConfigId;
    }

    public void setI18nConfigId(Long configId) {
        i18nConfigId = configId;
    }

    public boolean isDefaultDescription() {
        return defaultDescription;
    }

    public void setDefaultDescription(boolean defaultDescription) {
        this.defaultDescription = defaultDescription;
    }

    @Override
    public Object clone() {
        return new EmployeeDescriptionImpl(this);
    }
}
