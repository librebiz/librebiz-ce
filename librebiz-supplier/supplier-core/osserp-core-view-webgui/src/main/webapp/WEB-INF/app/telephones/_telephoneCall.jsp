<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="telephoneCallView"/>
<c:set var="sortUrl" scope="page" value="/telephones/telephoneCall/sort"/>

<c:choose>
	<c:when test="${!empty sessionScope.error}">
		&nbsp;
		<div class="errormessage">
			<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
		</div>
        <o:removeErrors/>
	</c:when>
	<c:otherwise>
		<c:if test="${!empty view}">
			<div class="table-responsive table-responsive-default">
				<table class="table table-striped">
					<thead>
						<tr>
							<th style="width: 65px;"><v:ajaxLink url="${sortUrl}?key=incoming" targetElement="ajaxContent"><fmt:message key="phone.incoming"/></v:ajaxLink></th>
							<th style="width: 130px;"><v:ajaxLink url="${sortUrl}?key=destinationPhoneNumber" targetElement="ajaxContent"><fmt:message key="target"/></v:ajaxLink></th>
							<th style="width: 200px;"><v:ajaxLink url="${sortUrl}?key=destinationDisplayName" targetElement="ajaxContent"><fmt:message key="name"/></v:ajaxLink></th>
							<th style="width: 133px;"><v:ajaxLink url="${sortUrl}?key=startedAt" targetElement="ajaxContent"><fmt:message key="fromDate"/></v:ajaxLink></th>
							<th style="width: 75px;"><v:ajaxLink url="${sortUrl}?key=duration" targetElement="ajaxContent"><fmt:message key="duration"/> (<fmt:message key="common.seconds.short"/>)</v:ajaxLink></th>
							<th style="width: 133px;"><v:ajaxLink url="${sortUrl}?key=endedAt" targetElement="ajaxContent"><fmt:message key="til"/></v:ajaxLink></th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty view.list}">
								<tr>
									<td colspan="7"><fmt:message key="error.no.telephoneCalls.found"/></td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="obj" items="${view.list}">
									<tr id="telephone_call_${obj.id}" <c:if test="${obj.duration == 0 && obj.incoming}">class="red"</c:if>>
										<td style="width: 65px;">
											<c:choose>
												<c:when test="${obj.incoming && !obj.clickToCall}"><fmt:message key="yes"/></c:when>
												<c:otherwise><fmt:message key="no"/></c:otherwise>
											</c:choose>
										</td>
										<td style="width: 130px;"><oc:phone value="${obj.destinationPhoneNumber}"/></td>
										<td style="width: 200px;">
											<c:choose>
												<c:when test="${empty obj.callContacts}">
													<v:ajaxLink url="/contacts/phoneNumberSearchPopup/search?value=${obj.destinationPhoneNumber}" linkId="phoneNumberSearchPopup" title="phoneNumberSearch">
														<fmt:message key="unknown"/>
													</v:ajaxLink>
												</c:when>
												<c:otherwise>
													<c:forEach var="callContact" items="${obj.callContacts}">
														<v:link url="/contacts.do" parameters="method=load&id=${callContact.contactId}&exit=systemTelephoneCalls"><oc:contact value="${callContact.contactId}"/><br/></v:link>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</td>
										<td style="width: 133px;"><o:date value="${obj.startedAt}" addtime="true"/></td>
										<td style="width: 75px;">
											<c:choose>
												<c:when test="${obj.duration != 0}"><o:out value="${obj.duration}"/></c:when>
												<c:otherwise>-</c:otherwise>
											</c:choose>
										</td>
										<td style="width: 133px;"><o:date value="${obj.endedAt}" addtime="true"/></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
		</c:if>
	</c:otherwise>
</c:choose>