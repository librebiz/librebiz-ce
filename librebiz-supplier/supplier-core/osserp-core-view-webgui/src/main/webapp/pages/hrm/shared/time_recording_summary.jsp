<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="recordingView" value="${requestScope.timeRecordingView}"/>
<c:set var="recording" value="${recordingView.timeRecording}"/>
<c:set var="period" value="${recording.selectedPeriod}"/>
<c:set var="config" value="${period.config}"/>
<c:set var="selectedYear" value="${period.selectedYear}"/> 
<c:set var="selectedMonth" value="${selectedYear.selectedMonth}"/>

<c:set var="adminPermissionGrant" value="false"/>
<o:permission role="time_recording_admin" info="permissionTimeRecordingConfigChange">
    <c:set var="adminPermissionGrant" value="true"/>
    <o:logger write="time recording admin permission grant" level="debug"/>
</o:permission>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><o:out value="${recording.reference}"/> - <oc:employee value="${recording.reference}"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="profile"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${recordingView.configChangeMode}">
                                <c:set var="configs" value="${recordingView.availableConfigs}"/>
                                <select name="selectAction" class="form-control" size="1" onChange="gotoUrl(this.value);">
                                    <option value="<c:url value="/timeRecordings.do"><c:param name="method" value="changeConfig"/></c:url>"><fmt:message key="promptSelect"/></option>
                                    <option value="<c:url value="/timeRecordings.do"><c:param name="method" value="disableConfigChangeMode"/></c:url>"><fmt:message key="exit"/></option>
                                    <c:forEach var="cfg" items="${configs}">
                                        <option value="<c:url value="/timeRecordings.do"><c:param name="method" value="changeConfig"/><c:param name="id" value="${cfg.id}"/><c:param name="exit" value="index"/></c:url>"><o:out value="${cfg.name}"/></option>
                                    </c:forEach>
                                </select>
                            </c:when>
                            <c:when test="${!requestScope.popup and period.configChangeable and adminPermissionGrant}">
                                <a href="<c:url value="/timeRecordings.do?method=enableConfigChangeMode"/>">
                                    <o:out value="${config.name}"/>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <o:out value="${config.name}"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="description"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${config.description}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="validFrom"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:date value="${period.validFrom}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="validUntil"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${empty period.validTil}">
                                <fmt:message key="unlimited"/>
                            </c:when>
                            <c:otherwise>
                                <o:date value="${period.validTil}"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="coreTime"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${config.coreTimeStartDisplay}"/> <fmt:message key="until"/>
                        <o:out value="${config.coreTimeEndDisplay}"/> <fmt:message key="clock"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="hoursPerDayLabel"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${config.hoursDaily}" format="hours"/> <fmt:message key="hourShort"/>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <c:if test="${selectedYear.annualLeave > 0}">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="leave"/> <o:out value="${selectedYear.year}"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="annualLeave"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${selectedYear.annualLeave}" format="currency"/> <fmt:message key="daysLabel"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="carryoverFrom"/> <o:out value="${selectedYear.year - 1}"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${selectedYear.carryoverLeave}" format="currency"/> <fmt:message key="daysLabel"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="expiredLeave"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${selectedMonth.expiredLeave}" format="currency"/> <fmt:message key="daysLabel"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="consumedLeave"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${selectedYear.consumedLeave}" format="currency"/> <fmt:message key="daysLabel"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="remainingLeave"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${selectedMonth.remainingLeave}" format="currency"/> <fmt:message key="daysLabel"/>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </c:if>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="currentMonthLabel"/> <o:out value="${selectedMonth.monthAndYearDisplay}"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="workingDays"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${selectedMonth.workingDaysCount}" format="currency"/> <fmt:message key="daysLabel"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="according"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${selectedMonth.workingDaysCountDisplay}"/> <fmt:message key="hourShort"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="alreadyAccomplished"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${selectedMonth.totalDisplay}"/> <fmt:message key="hourShort"/>
                    </div>
                </div>
            </div>

            <c:if test="${selectedMonth.minutesLeave > 0}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="leave"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${selectedMonth.leaveDisplay}"/> <fmt:message key="hourShort"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${selectedMonth.minutesIllness > 0}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="illnessLabel"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${selectedMonth.illnessDisplay}"/> <fmt:message key="hourShort"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="timeBank"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${selectedMonth.carryoverDisplay}"/> <fmt:message key="hourShort"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="maximumCarryoverHoursLabel"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${selectedMonth.monthMarkerValue}" format="hours"/> <fmt:message key="hourShort"/>
                    </div>
                </div>
            </div>

		</div>
	</div>










