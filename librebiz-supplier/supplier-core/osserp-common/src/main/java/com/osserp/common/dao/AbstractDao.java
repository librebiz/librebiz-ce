/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 8:13:10 PM 
 * 
 */
package com.osserp.common.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.transaction.annotation.Transactional;

import com.osserp.common.BackendException;
import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractClass;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Transactional
public abstract class AbstractDao extends AbstractClass {
    private static Logger log = LoggerFactory.getLogger(AbstractDao.class.getName());

    /**
     * Binds a regex pattern to a column name
     * @param name of the column
     * @param pattern to bind to column
     * @param startsWith if regular expression should be match from beginning
     * @return sql regex statement with pattern bound to column
     */
    protected String bindColumnToRegExPattern(String name, String pattern, boolean startsWith) {
        StringBuilder buffer = new StringBuilder(20);
        buffer.append(name);
        if (name.indexOf("id") > -1) {
            buffer.append("::varchar");
        }
        buffer.append(" ~* '");
        if (startsWith) {
            buffer.append("^");
        }
        buffer.append(fetchInsecureInput(pattern)).append("'");
        return buffer.toString();
    }

    protected String fetchInsecureInput(String param) {
        try {
            return StringUtil.getSQLInput(param);
        } catch (Exception e) {
            return "";
        }
    }
    
    protected boolean isEmpty(String[] array) {
        return (array == null || array.length < 1);
    }

    protected boolean isEmpty(String s) {
        return (s == null || s.length() < 1);
    }

    protected boolean isEmpty(Long l) {
        return (l == null || l == 0);
    }

    protected boolean getBoolean(int value) {
        return (value != 0);
    }

    protected int setBoolean(boolean value) {
        return (value) ? 1 : 0;
    }

    /**
     * Tries safely to create a long by a string
     * @param s to convert
     * @return value or null if s is null or not convertable.
     */
    @Override
    protected Long createLong(String s) {
        if (s == null) {
            return null;
        }
        try {
            return Long.valueOf(s);
        } catch (Throwable t) {
            return null;
        }
    }

    protected Long getLong(long value) {
        return (value == 0) ? 0L : value;
    }

    protected Long getLongOrNull(long value) {
        return (value == 0) ? null : value;
    }

    protected Long getLong(Long value) {
        return (value == null) ? 0L : value;
    }

    protected Long getLongOrNull(Long value) {
        return (value == null) ? null : value;
    }

    protected Double getDouble(Double value) {
        return (value == null) ? 0d : value;
    }

    protected BigDecimal getDecimal(double val) {
        return new BigDecimal(Double.toString(val));
    }

    protected boolean empty(List list) {
        return (list == null || list.isEmpty());
    }

    protected String getColumn(String key) {
        if (key == null) {
            log.error("getColumn() key is null!");
            throw new BackendException(BackendException.MISSING_PARAM);
        }
        if (key.equals(Constants.SEARCH_BY_ID)) {
            return "id";
        }
        if (key.equals(Constants.SEARCH_BY_PROJECT_NAME)) {
            return "name";
        }
        if (key.equals(Constants.SEARCH_BY_FIRSTNAME)) {
            return "firstname";
        }
        if (key.equals(Constants.SEARCH_BY_LASTNAME)) {
            return "lastname";
        }
        if (key.equals(Constants.SEARCH_BY_CUSTOMER_ID)) {
            return "customer_id";
        } else if (key.equals(Constants.SEARCH_BY_EMAIL)) {
            return "email";
        }
        log.error("getColumn() key " + key + " is invalid!");
        throw new BackendException(BackendException.INVALID_PARAM);
    }

    /**
     * Closes the specified resource objects
     * @param rs to close
     * @param stmt to close
     * @param con to close
     */
    protected void close(ResultSet rs, Statement stmt, Connection con) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ignorable) {
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ignorable) {
            }
        }
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ignorable) {
            }
        }
    }

    /**
     * Closes the specified resource objects
     * @param rs to close
     * @param stmt to close
     * @param con to close
     */
    protected void close(ResultSet rs, PreparedStatement stmt, Connection con) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ignorable) {
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ignorable) {
            }
        }
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ignorable) {
            }
        }
    }

}
