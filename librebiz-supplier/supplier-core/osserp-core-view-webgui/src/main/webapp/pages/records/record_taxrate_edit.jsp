<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<c:set var="view" value="${sessionScope.recordView}" />
<c:set var="record" value="${view.record}" />
<tr>
    <td><fmt:message key="turnoverTax" /></td>
    <td align="right">
        <select name="taxRate" style="width: 100px; text-align: right;">
            <c:forEach var="rate" items="${view.taxRates}">
                <c:choose>
                    <c:when test="${record.amounts.taxRate == rate}">
                        <option value="<o:out value="${rate}"/>" selected="selected"><o:number value="${rate}" format="tax" /> %
                        </option>
                    </c:when>
                    <c:otherwise>
                        <option value="<o:out value="${rate}"/>"><o:number value="${rate}" format="tax" /> %
                        </option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
    </td>
</tr>
<tr>
    <td><fmt:message key="reducedTurnoverTax" /></td>
    <td align="right">
        <select name="reducedTaxRate" style="width: 100px; text-align: right;">
            <c:forEach var="rate" items="${view.reducedTaxRates}">
                <c:choose>
                    <c:when test="${record.amounts.reducedTaxRate == rate}">
                        <option value="<o:out value="${rate}"/>" selected="selected"><o:number value="${rate}" format="tax" /> %
                        </option>
                    </c:when>
                    <c:otherwise>
                        <option value="<o:out value="${rate}"/>"><o:number value="${rate}" format="tax" /> %
                        </option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
    </td>
</tr>
