/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 14, 2015 11:05:24 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.finance.RecordType;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractRecordViews extends AbstractDao {

    private RecordTypes recordTypesDao = null;

    public AbstractRecordViews(JdbcTemplate jdbcTemplate, Tables tables, 
            SessionFactory sessionFactory, RecordTypes recordTypesDao) {
        super(jdbcTemplate, tables, sessionFactory);
        this.recordTypesDao = recordTypesDao;
    }

    public Map<Long, RecordType> getRecordTypes() {
        return recordTypesDao.getRecordTypeMap();
    }

    protected StringBuilder createQuerySelectFrom(Long type) {
        StringBuilder query = new StringBuilder("SELECT * FROM v_");
        if (RecordType.SALES_DOWNPAYMENT.equals(type)) {
            query.append("sales_order_downpayments_query");
        } else if (RecordType.SALES_CANCELLATION.equals(type)) {
            query.append("sales_cancellations_query");
        } else if (RecordType.SALES_INVOICE.equals(type)) {
            query.append("sales_invoices_query");
        } else if (RecordType.SALES_CREDIT_NOTE.equals(type)) {
            query.append("sales_credit_notes_query");
        } else if (RecordType.SALES_PAYMENT.equals(type)) {
            query.append("sales_payments_query");
        } else if (RecordType.PURCHASE_INVOICE.equals(type)) {
            query.append("purchase_invoices_query");
        } else if (RecordType.PURCHASE_PAYMENT.equals(type)) {
            query.append("purchase_payments_query");
        }
        return query;
    }
}
