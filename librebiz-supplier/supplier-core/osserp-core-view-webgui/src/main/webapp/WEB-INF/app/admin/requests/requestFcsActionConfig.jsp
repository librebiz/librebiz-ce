<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestFcsActionConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string" >
        <div class="content-area" id="fcsConfigContent">
            <div class="row">
                <c:choose>
				    <c:when test="${view.createMode}">
                        <v:form url="${view.baseLink}/save" name="flowControlConfigEditForm">
                            <c:import url="_requestFcsActionCreate.jsp"/>
                        </v:form>
				    </c:when>
				    <c:when test="${view.editMode}">
                        <v:form url="${view.baseLink}/save" name="flowControlConfigEditForm">
                            <c:import url="_requestFcsActionEdit.jsp"/>
                        </v:form>
				    </c:when>
				    <c:when test="${!empty view.bean}">
                        <div class="subcolumns">
                            <c:import url="_requestFcsActionDisplay.jsp"/>
                        </div>
				    </c:when>
				    <c:otherwise>
                        <c:import url="_requestFcsActionList.jsp"/>
                    </c:otherwise>
                 </c:choose>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
