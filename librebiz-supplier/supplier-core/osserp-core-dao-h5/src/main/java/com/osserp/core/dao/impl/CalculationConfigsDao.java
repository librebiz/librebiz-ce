/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 10, 2006 1:27:13 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.Options;
import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.CalculationType;
import com.osserp.core.dao.CalculationConfigs;
import com.osserp.core.dao.ProductSelectionConfigs;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.calc.CalculationConfigGroupImpl;
import com.osserp.core.model.calc.CalculationConfigImpl;
import com.osserp.core.model.calc.CalculationTypeImpl;
import com.osserp.core.products.ProductSelectionConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationConfigsDao extends AbstractProductAwareDao implements CalculationConfigs {
    private static Logger log = LoggerFactory.getLogger(CalculationConfigsDao.class.getName());
    private ProductSelectionConfigs productSelectionConfigs = null;

    public CalculationConfigsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            ProductSelectionConfigs productSelectionConfigs) {

        super(jdbcTemplate, tables, sessionFactory, cache);
        this.productSelectionConfigs = productSelectionConfigs;
    }

    public CalculationConfig load(Long id) {
        return (CalculationConfig) getCurrentSession().load(CalculationConfigImpl.class, id);
    }

    public List<CalculationConfig> findAll() {
        return getCurrentSession().createQuery(
                "from " + CalculationConfigImpl.class.getName() 
                + " cfg order by cfg.name").list();
    }

    public List<CalculationConfig> findByType(Long type) {
        return getCurrentSession().createQuery(
                "from " + CalculationConfigImpl.class.getName() 
                + " o where o.type.id = :typeId")
                .setParameter("typeId", type).list();
    }

    public CalculationConfig createConfig(CalculationType type, String name) throws ClientException {
        CalculationConfigImpl obj = new CalculationConfigImpl(name, type);
        save(obj);
        return obj;
    }

    public CalculationConfigGroup findGroup(Long id) {
        return (CalculationConfigGroup) getCurrentSession().get(CalculationConfigGroupImpl.class.getName(), id);
    }

    public CalculationConfigGroup addGroup(CalculationConfig config, String name, boolean discounts, boolean option) throws ClientException {
        CalculationConfigImpl obj = (CalculationConfigImpl) config;
        Long nextId = getNextLong("calculation_config_groups_id_seq");
        CalculationConfigGroup group = obj.addGroup(nextId, name, discounts, option);
        save(config);
        return group;
    }

    public CalculationConfig addProductSelection(CalculationConfig config, Long groupId, Long configId) {
        if (log.isDebugEnabled()) {
            log.debug("addProductSelection() invoked [config=" + config.getId() + ", group=" + groupId + ", selection=" + configId + "]");
        }
        ProductSelectionConfig selectionConfig = productSelectionConfigs.load(configId);
        config.addProductSelection(groupId, selectionConfig);
        save(config);
        return load(config.getId());
    }

    public CalculationConfig removeProductSelection(CalculationConfig config, Long groupId, Long configId) {
        config.removeProductSelection(groupId, configId);
        save(config);
        return load(config.getId());
    }

    /*
     * Update calculation group order new > old update groups set order_id = order_id - 1 where order_id > oldorder and config_id = cfgid update groups set
     * order_id = order_id + 1 where order_id >= neworder and config_id = 2; update calculation_groups set order_id = neworder where id = cfgid; new < old
     * update groups set order_id = order_id + 1 where order_id >= neworder and order_id < oldorder and config_id = 2; update groups set order_id = neworder
     * where id = cfgid;
     */
    public void updateGroupOrder(
            long groupid,
            long configid,
            int oldorder,
            int neworder) {

        if (oldorder != neworder) {
            StringBuilder updateGroup = new StringBuilder(96);
            updateGroup
                    .append("UPDATE ")
                    .append(getTable(TableKeys.CALCULATION_GROUPS))
                    .append(" SET order_id = ")
                    .append(neworder)
                    .append(" WHERE id = ")
                    .append(groupid);

            if (neworder > oldorder) {
                if (log.isDebugEnabled()) {
                    log.debug("updateGroupOrder() new order id > oldorder");
                }

                StringBuilder moveDown = new StringBuilder(96);
                moveDown
                        .append("UPDATE ")
                        .append(getTable(TableKeys.CALCULATION_GROUPS))
                        .append(" SET order_id = order_id - 1 ")
                        .append("WHERE order_id > ").append(oldorder)
                        .append(" AND config_id = ").append(configid);
                StringBuilder moveUp = new StringBuilder(96);
                moveUp
                        .append("UPDATE ")
                        .append(getTable(TableKeys.CALCULATION_GROUPS))
                        .append(" SET order_id = order_id + 1 ")
                        .append("WHERE order_id >= ").append(neworder)
                        .append(" AND config_id = ").append(configid);

                jdbcTemplate.update(moveDown.toString());
                jdbcTemplate.update(moveUp.toString());
                jdbcTemplate.update(updateGroup.toString());

            } else {
                if (log.isDebugEnabled()) {
                    log.debug("updateGroupOrder() new order id < old order id");
                }
                StringBuilder moveUp = new StringBuilder(96);
                moveUp
                        .append("UPDATE ")
                        .append(getTable(TableKeys.CALCULATION_GROUPS))
                        .append(" SET order_id = order_id + 1 ")
                        .append("WHERE order_id >= ").append(neworder)
                        .append(" AND order_id < ").append(oldorder)
                        .append(" AND config_id = ").append(configid);
                jdbcTemplate.update(moveUp.toString());
                jdbcTemplate.update(updateGroup.toString());
            }
        } else if (log.isDebugEnabled()) {
            log.debug("updateGroupOrder() nothing to do");
        }
    }

    public CalculationConfig configurePartList(CalculationConfig config, Long groupId, CalculationConfig partListConfig) {
        if (config instanceof CalculationConfigImpl) {
            CalculationConfigImpl obj = (CalculationConfigImpl) config;
            obj.configurePartlist(groupId, partListConfig);
            save(obj);
            return load(config.getId());

        }
        return config;
    }

    public void save(CalculationConfig config) {
        getCurrentSession().saveOrUpdate(CalculationConfigImpl.class.getName(), config);
    }

    public CalculationType createType(String name, String description) throws ClientException {
        if (isEmpty(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (isEmpty(description)) {
            throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
        }
        checkExistingType(name, null);
        CalculationTypeImpl obj = new CalculationTypeImpl(name, description);
        getCurrentSession().saveOrUpdate(CalculationTypeImpl.class.getName(), obj);
        getOptionsCache().refresh(Options.CALCULATION_TYPES);
        return obj;
    }

    public CalculationType updateType(CalculationType type, String name, String description) throws ClientException {
        if (isEmpty(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (isEmpty(description)) {
            throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
        }
        checkExistingType(name, type.getId());
        type.setName(name);
        type.setDescription(description);
        getCurrentSession().saveOrUpdate(CalculationTypeImpl.class.getName(), type);
        getOptionsCache().refresh(Options.CALCULATION_TYPES);
        return loadType(type.getId());
    }

    private void checkExistingType(String name, Long existingId) throws ClientException {
        List<CalculationType> existing = getTypes();
        for (Iterator<CalculationType> i = existing.iterator(); i.hasNext();) {
            CalculationType type = i.next();
            if (type.getName().equalsIgnoreCase(name)) {
                if (existingId == null) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                } else if (!existingId.equals(type.getId())) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }
    }

    public CalculationType loadType(Long id) {
        return (CalculationType) getCurrentSession().load(CalculationTypeImpl.class, id);
    }

    public List<CalculationType> getTypes() {
        StringBuilder hql = new StringBuilder(64);
        hql
                .append("from ")
                .append(CalculationTypeImpl.class.getName())
                .append(" tp order by tp.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }
}
