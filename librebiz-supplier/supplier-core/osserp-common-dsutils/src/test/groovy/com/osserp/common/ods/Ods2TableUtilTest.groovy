/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 */
package com.osserp.common.ods

import static org.junit.Assert.*

import org.junit.Test

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class Ods2TableUtilTest {
	
	@Test
	void testCreateColumnName() {
		String columnContent = "COLUMNNAME,1,3"
		String result = OdsUtil.createColumnName(columnContent)
		assertTrue("COLUMNNAME".equals(result))
	}

	@Test
	void testCreateMaskedValueWithApostrophe() {
		String value = "test with 2 ' occurences of '"
		String result = Ods2TableUtil.createMaskedValue(value)
		assertTrue("test with 2 \\' occurences of \\'".equals(result))
	}

	@Test
	void testCreateMaskedValueWithBackslash() {
		String value = "\\AM2\\"
		String result = Ods2TableUtil.createMaskedValue(value)
		assertTrue("AM2".equals(result))
	}

	@Test
	void testCreateTableName() {
		String fileName = "ZAHLUNG.ods"
		String result = Ods2TableUtil.createTableName(fileName)
		assertTrue("ZAHLUNG".equals(result))
	}

	@Test
	void testCreateTableCommand() {
		String[] columnNames = [ 'NFIRMA', 'CCODE', 'CNAME' ]
		String result = Ods2TableUtil.createTableCommand("ZAHLUNG", columnNames)
		String expectedResult = "CREATE TABLE ZAHLUNG (NFIRMA VARCHAR(255),CCODE VARCHAR(255),CNAME VARCHAR(255));"
		assertTrue(expectedResult.equals(result))
	}

	@Test
	void testCreateTableInsert() {
		String[] columnNames = [ 'NFIRMA', 'CCODE', 'CNAME' ]
		String result = Ods2TableUtil.createTableInsert("ZAHLUNG", columnNames)
		String expectedResult = "INSERT INTO ZAHLUNG (NFIRMA,CCODE,CNAME) VALUES (?,?,?);"
		assertTrue(expectedResult.equals(result))
	}
}
