<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.id}" />
                -
                <o:out value="${view.bean.name}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:import url="${viewdir}/admin/fcs/_fcsConfigDisplayCommon.jsp" />

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="deliveryAuthorizationProperty" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.enablingDelivery}">
                                <fmt:message key="yes" />
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="no" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="deliveryCompletedProperty" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.closingDelivery}">
                                <fmt:message key="yes" />
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="no" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
