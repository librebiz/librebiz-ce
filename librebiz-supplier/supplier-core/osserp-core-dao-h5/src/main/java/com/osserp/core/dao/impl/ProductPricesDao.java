/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 28, 2011 10:55:28 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.products.ProductImpl;
import com.osserp.core.model.products.ProductSalesPriceChangedImpl;
import com.osserp.core.model.products.ProductSalesPriceImpl;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSalesPrice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductPricesDao extends AbstractProductEntitiesDao implements ProductPrices {

    /**
     * Creates a new ProductPrices dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param optionsCache
     * @param summaryCache
     */
    public ProductPricesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache,
            ProductSummaryCache summaryCache) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache, summaryCache);
    }

    public List<ProductSalesPrice> loadPriceHistory(Long productId) {
        return getCurrentSession().createQuery(
                "from " + ProductSalesPriceImpl.class.getName()
                + " a where a.productId = :product order by a.changed desc")
                .setParameter("product", productId).list();
    }

    public void createSalesPriceHistory(Employee employee, Product product, Date validFrom) {
        loadSummary(product);
        ProductSalesPriceImpl sp = new ProductSalesPriceImpl(
                validFrom,
                product.getProductId(),
                employee.getId(),
                product.getConsumerPrice(),
                product.getResellerPrice(),
                product.getPartnerPrice(),
                product.getConsumerPriceMinimum(),
                product.getResellerPriceMinimum(),
                product.getPartnerPriceMinimum(),
                product.getConsumerMinimumMargin(),
                product.getResellerMinimumMargin(),
                product.getPartnerMinimumMargin(),
                product.getSummary().getAveragePurchasePrice(),
                product.getEstimatedPurchasePrice(),
                product.getSummary().getLastPurchasePrice());
        save(sp);
    }

    public List<ProductSalesPrice> updatePriceHistory(Long createdBy, Long priceHistoryId, Date validFrom) {
        ProductSalesPrice productSalesPrice = (ProductSalesPrice) getCurrentSession().load(ProductSalesPriceImpl.class.getName(), priceHistoryId);
        ProductSalesPriceChangedImpl dateHist = new ProductSalesPriceChangedImpl(createdBy, productSalesPrice, validFrom, productSalesPrice.getChanged());
        productSalesPrice.setChanged(validFrom);
        getCurrentSession().saveOrUpdate(ProductSalesPriceImpl.class.getName(), productSalesPrice);
        getCurrentSession().saveOrUpdate(ProductSalesPriceChangedImpl.class.getName(), dateHist);
        List<ProductSalesPrice> result = loadPriceHistory(productSalesPrice.getProductId());
        if (result.get(0).getId().equals(productSalesPrice.getId())) {
            // changed sales price is currentPrice now
            ProductImpl product = (ProductImpl) getCurrentSession().load(ProductImpl.class.getName(), productSalesPrice.getProductId());
            product.setConsumerPrice(productSalesPrice.getConsumerPrice());
            product.setConsumerPriceMinimum(productSalesPrice.getConsumerPriceMinimum());
            product.setConsumerMinimumMargin(productSalesPrice.getConsumerMinimumMargin());
            product.setPartnerPrice(productSalesPrice.getPartnerPrice());
            product.setPartnerPriceMinimum(productSalesPrice.getPartnerPriceMinimum());
            product.setPartnerMinimumMargin(productSalesPrice.getPartnerMinimumMargin());
            product.setResellerPrice(productSalesPrice.getResellerPrice());
            product.setResellerPriceMinimum(productSalesPrice.getResellerPriceMinimum());
            product.setResellerMinimumMargin(productSalesPrice.getResellerMinimumMargin());
            getCurrentSession().saveOrUpdate(ProductImpl.class.getName(), product);
        }
        return result;
    }

    public Double getPartnerPrice(Product product, Date date, Double quantity) {
        Double result = null;
        if (product.isPriceByQuantity()) {
            result = product.getPartnerPriceByQuantity(quantity);
        } else {
            Object[] params = { product.getProductId(), date };
            int[] types = { Types.BIGINT, Types.TIMESTAMP };
            List<Double> list = (List<Double>) jdbcTemplate.query(
                    Procs.GET_PRODUCT_PARTNER_PRICE_AT_DATE,
                    params,
                    types,
                    getDoubleRowMapper());

            result = empty(list) ? null : list.get(0);
            if (result == null || result == 0) {
                result = product.getPartnerPrice();
            }
        }
        return (result == null ? 0d : NumberUtil.round(result, 5));
    }

    public Double getPurchasePrice(Product product, Long stockId, Date date) {
        Double result = null;
        Object[] params = { (stockId == null ? DEFAULT_STOCK : stockId), product.getProductId(), date };
        int[] types = { Types.BIGINT, Types.BIGINT, Types.TIMESTAMP };
        List<Double> list = (List<Double>) jdbcTemplate.query(
                Procs.GET_PRODUCT_AVERAGE_PURCHASE_PRICE_AT_DATE,
                params,
                types,
                getDoubleRowMapper());

        result = empty(list) ? null : list.get(0);
        if (result == null || result == 0) {
            loadSummary(product);
            product.enableStock((stockId == null ? DEFAULT_STOCK : stockId));
            result = product.getSummary().getLastPurchasePrice();
        }
        return (result == null ? 0d : NumberUtil.round(result, 5));
    }

    private void save(ProductSalesPrice salesPriceHistory) {
        getCurrentSession().saveOrUpdate(ProductSalesPriceImpl.class.getName(), salesPriceHistory);
    }

}
