<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${requestScope.branchSelectionView}"/>
<c:set var="url" value="${requestScope.branchSelectionUrl}"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="selectBranchLabel"/>
	</div>
</div>

<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>

			<select name="selectAction" size="1" onChange="gotoUrl(this.value);">
				<option value="<c:url value="${url}?method=select"/>"><fmt:message key="promptSelect"/> / <fmt:message key="reset"/></option>
				<c:forEach var="obj" items="${view.list}" varStatus="s">
					<c:choose>
						<c:when test="${!empty view.bean and view.bean.id == obj.id}">
							<option value="<c:url value="${url}?method=select&id=${obj.id}"/>" selected="selected"><o:out value="${obj.shortkey}"/> - <o:out value="${obj.name}"/></option>
						</c:when>
						<c:otherwise>
							<option value="<c:url value="${url}?method=select&id=${obj.id}"/>"><o:out value="${obj.shortkey}"/> - <o:out value="${obj.name}"/></option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>

			<div class="spacer"></div>
		</div>
	</div>
</div>