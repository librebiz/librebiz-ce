<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="recordingView" value="${sessionScope.timeRecordingView}"/>
<c:set var="period" value="${recordingView.timeRecording.selectedPeriod}"/>
<c:set var="config" value="${period.config}"/>
<c:set var="selectedMonth" value="${period.selectedYear.selectedMonth}"/>
<c:set var="hrmPermissionGrant" value="false"/>

<o:permission role="hrm,disciplinarian_edit,client_edit" info="permissionDeleteMarker">
    <c:set var="hrmPermissionGrant" value="true"/>
</o:permission>

<c:if test="${!empty selectedMonth}">
    <o:logger write="${'[selectedMonth='}${selectedMonth.month}${', config='}${config.id}${']'}" level="debug"/>
</c:if>

<c:set var="markers" value="${selectedMonth.markers}"/>

<c:if test="${!empty config and config.recordingHours}">
    <div class="col-md-12 panel-area">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><fmt:message key="date"/></th>
                        <th><fmt:message key="type"/></th>
                        <th><fmt:message key="info"/></th>
                        <th><fmt:message key="value"/></th>
                        <c:if test="${hrmPermissionGrant}">
                            <th class="center"><fmt:message key="action"/></th>
                        </c:if>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="marker" items="${markers}">
                        <tr>
                            <td><o:date value="${marker.markerDate}"/></td>
							<td><o:out value="${marker.type.name}"/></td>
							<td><o:out value="${marker.note}"/></td>
							<td class="right"><o:number value="${marker.markerValue}" format="currency"/></td>
                            <c:if test="${hrmPermissionGrant}">
                                <td class="center">
                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="deleteMarker"/>','<c:url value="/timeRecordings.do"><c:param name="method" value="deleteMarker"/><c:param name="id" value="${marker.id}"/></c:url>');" title="<fmt:message key="deleteMarker"/>"><o:img name="deleteIcon"/></a>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</c:if>
