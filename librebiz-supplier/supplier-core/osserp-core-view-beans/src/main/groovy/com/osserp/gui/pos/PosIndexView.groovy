/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 12, 2017 
 * 
 */
package com.osserp.gui.pos


import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.BankAccount
import com.osserp.core.finance.BillingType
import com.osserp.core.finance.Payment
import com.osserp.core.customers.Customer
import com.osserp.core.customers.CustomerSearch

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.accounting.AbstractAccountingView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class PosIndexView extends AbstractAccountingView {
    private static Logger logger = LoggerFactory.getLogger(PosIndexView.class.getName())
    
    Customer customerAccount
    
    PosIndexView() {
        super()
        enableBankAccountPreselection()
        enableCompanyPreselection()
        //providesCustomNavigation()
    }
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        
        if (forwardRequest) {
            
            if (!bankAccounts) {
                throw new ClientException(ErrorCode.BANKACCOUNTS_UNDEFINED)
            }
            bankAccounts.each { BankAccount account ->
                if (account.cashAccount) {
                    selectedBankAccount = account
                }
            }
            if (!selectedBankAccount) {
                throw new ClientException(ErrorCode.BANKACCOUNT_MISMATCH)
            }
            
            if (selectedBranch && !selectedBranch?.posAccountId && !multipleBranchsAvailable) {
                throw new ClientException(ErrorCode.POSACCOUNT_MISSING)
            }
            if (selectedBranch?.posAccountId) {
                customerAccount = customerSearch.findById(selectedBranch.posAccountId)
                if (!customerAccount) {
                    throw new ClientException(ErrorCode.POSACCOUNT_INVALID)
                }
                logger.debug("initRequest: done [posCustomer=${customerAccount?.id}, bankAccount=${selectedBankAccount.id}]")
            } else {
                // multiple branchs - user has to select customerAccount by branch list first
                // TODO implement selection
            }
        }
    }

    protected CustomerSearch getCustomerSearch() {
        getService(CustomerSearch.class.getName())
    }
}
