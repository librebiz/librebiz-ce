/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Constants
import com.osserp.common.Entity
import com.osserp.common.PermissionException
import com.osserp.common.PersistentEntity
import com.osserp.common.TimeoutException
import com.osserp.common.User
import com.osserp.common.service.Locator
import com.osserp.common.service.ResourceLocator
import com.osserp.common.util.StringUtil
import com.osserp.common.web.Form
import com.osserp.common.web.PortalView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractView implements View {
    private static Logger logger = LoggerFactory.getLogger(AbstractView.class.getName())

    private Map<Locale, ResourceBundle> resourceBundles = new HashMap<Locale, ResourceBundle>()

    /**  Provides view context. View context will be kept alive on reset. */
    Map context =
    [destroyViewOnExit: true,
        destroyViewOnHome: true,
        providesCreateMode: false,
        providesEditMode: false,
        providesSetupMode: false,
        autoreloadEnabled: false,
        popupView: false,
        popupSaveExit: false,
        popupSaveReloadTarget: false,
        userRequired: true
    ]

    /**  Provides current request attributes */
    Map requestAttributes = [:]
    /**  Provides current request context */
    Map requestContext = [:]
    /**  Provides current request headers */
    Map requestHeaders = [:]
    /** Provides current request parameters */
    Form form
    Boolean formLocked = false

    /** Provides information about dependencies */
    List<String> dependencies = []

    Map<String, Object> env = [:]

    Map nav = [:]

    Integer listStart = 0
    Integer listStop = 0
    Integer listStep = 50
    Long lastId
    List list = []
    List collected = []
    Object bean
    User user
    private String _name
    String headerName
    String permissions

    Boolean updateOnly = false
    String updateOnlyTarget

    /** Parameters for layout **/
    String actualSortKey
    boolean actualSortReverse
    Long scrollPosition
    String uploadDir

    Boolean exitTargetInitialized = false
    Boolean overrideExitTarget = false

    List<String> scriptsList = []
    List<String> scriptsListBottom = []
    List<String> stylesList = []

    void initRequest(Map atts, Map headers, Map params) {
        if (permissions) {
            checkPermission(permissions)
        }
        requestAttributes = atts
        requestHeaders = headers
        if (!formLocked) {
            form = new WebForm(params)
        }
        nav.currentNavigation = []
        addExitId(form.getString('exitId'))
        addExitTarget(form.getString('exit'))
        updateOnly = form.getBoolean('updateOnly')
        updateOnlyTarget = form.getString('updateOnlyTarget')
    }

    protected final void reloadResourceBundle() {
        Locale currentLocale = getLocale()
        try {
            ResourceLocator resLocator = resourceLocator
            if (resLocator) {
                resLocator.reload(currentLocale)
                ResourceBundle bundle = resLocator.getResourceBundle(currentLocale)
                resourceBundles.put(currentLocale, bundle)
                logger.debug('reloadResourceBundle() done')
            }
        } catch (Throwable t) {
            logger.error('reloadResourceBundle: ignoring exception [message='
                + t.getMessage() + ', exception=' + t.getClass().getName() + ']')
        }
    }

    protected final ResourceBundle getResourceBundle() {
        Locale currentLocale = getLocale()
        if (!resourceBundles.containsKey(currentLocale)) {
            try {
                ResourceLocator resLocator = resourceLocator
                if (resLocator) {
                    ResourceBundle bundle = resLocator.getResourceBundle(currentLocale)
                    resourceBundles.put(currentLocale, bundle)
                    logger.debug('getResourceBundle() loading bundle from backend')
                }
            } catch (Throwable t) {
                logger.error('getResourceBundle: ignoring exception [message='
                    + t.getMessage() + ', exception=' + t.getClass().getName() + ']')
            }
        }
        return resourceBundles.get(currentLocale)
    }

    protected final Locale getLocale() {
        if (user && user.getLocale()) {
            return user.getLocale()
        }
        Locale locale
        try {
            locale = resourceLocator?.defaultLocale
        } catch (Throwable t) {
            logger.info('getLocale: ignoring exception [message='
                + t.getMessage() + ', exception=' + t.getClass().getName() + ']')
        }
        locale ?: Constants.DEFAULT_LOCALE_OBJECT
    }

    protected final String getLocalizedMessage(String key) {
        ResourceBundle resources = getResourceBundle()
        if (resources && resources.containsKey(key)) {
            return resources.getString(key)
        }
        return key
    }

    List<MenuItem> getNavigation() {
        if (nav.currentNavigation) {
            logger.debug("getNavigation: using currentNavigation")
            return nav.currentNavigation
        }
        if (setupMode) {
            logger.debug("getNavigation: using setupNavigation")
            return nav.setupNavigation
        }
        if (createMode) {
            logger.debug("getNavigation: using createNavigation")
            return nav.createNavigation
        }
        if (editMode) {
            logger.debug("getNavigation: using editNavigation")
            return nav.editNavigation
        }
        if (bean && list) {
            logger.debug("getNavigation: using beanListNavigation")
            return nav.beanListNavigation
        }
        if (bean) {
            logger.debug("getNavigation: using beanNavigation")
            return nav.beanNavigation
        }
        if (list) {
            logger.debug("getNavigation: using listNavigation")
            return nav.listNavigation
        }
        logger.debug("getNavigation: using defaultNavigation")
        return nav.defaultNavigation
    }

    final void generateNavigation() {
        logger.debug("generateNavigation: invoked")
        createDefaultNavigation()
        createCustomNavigation()
    }

    /**
     * Override to set custom navigation
     */
    protected void createCustomNavigation() {
    }


    private final void createDefaultNavigation() {
        if (!nav.defaultNavigation) {
            if (context.providesCreateMode) {
                nav.defaultNavigation = context.popupView ? [createLink ]: [exitLink, createLink, homeLink]
                logger.debug("createDefaultNavigation: nav.defaultNavigation set [links=${context.popupView ? 'createLink' : 'exitLink, createLink, homeLink'}]")
            } else {
                nav.defaultNavigation = context.popupView ? [ ]: [exitLink, homeLink]
                logger.debug("createDefaultNavigation: nav.defaultNavigation set [links=${context.popupView ? '' : 'exitLink, homeLink'}]")
            }
        }
        if (!nav.listNavigation) {
            if (context.providesSetupMode && context.providesCreateMode) {
                nav.listNavigation = context.popupView ? [createLink, setupLink ]: [exitLink, createLink, setupLink, homeLink]
                logger.debug("createDefaultNavigation: nav.listNavigation set [links=${context.popupView ? 'createLink, setupLink' : 'exitLink, createLink, setupLink, homeLink'}]")
            } else if  (context.providesSetupMode) {
                nav.listNavigation = context.popupView ? [setupLink ]: [exitLink, setupLink, homeLink]
                logger.debug("createDefaultNavigation: nav.listNavigation set [links=${context.popupView ? 'setupLink' : 'exitLink, setupLink, homeLink'}]")
            } else if  (context.providesCreateMode) {
                nav.listNavigation = context.popupView ? [createLink ]: [exitLink, createLink, homeLink]
                logger.debug("createDefaultNavigation: nav.listNavigation set [links=${context.popupView ? 'createLink' : 'exitLink, createLink, homeLink'}]")
            } else {
                nav.listNavigation = context.popupView ? [ ]: [exitLink, homeLink]
                logger.debug("createDefaultNavigation: nav.listNavigation set [links=${context.popupView ? '' : 'exitLink, homeLink'}]")
            }
        }
        if (!nav.beanNavigation) {
            if (context.providesSetupMode && context.providesEditMode) {
                nav.beanNavigation = context.popupView ? [editLink, setupLink ]: [exitLink, editLink, setupLink, homeLink]
                logger.debug("createDefaultNavigation: nav.beanNavigation set [links=${context.popupView ? 'editLink, setupLink' : 'exitLink, editLink, setupLink, homeLink'}]")
            } else if  (context.providesSetupMode) {
                nav.beanNavigation = context.popupView ? [setupLink ]: [exitLink, setupLink, homeLink]
                logger.debug("createDefaultNavigation: nav.beanNavigation set [links=${context.popupView ? 'setupLink' : 'exitLink, setupLink, homeLink'}]")
            } else if  (context.providesEditMode) {
                nav.beanNavigation = context.popupView ? [editLink ]: [exitLink, editLink, homeLink]
                logger.debug("createDefaultNavigation: nav.beanNavigation set [links=${context.popupView ? 'editLink' : 'exitLink, editLink, homeLink'}]")
            } else {
                nav.beanNavigation = context.popupView ? [ ]: [exitLink, homeLink]
                logger.debug("createDefaultNavigation: nav.beanNavigation set [links=${context.popupView ? '' : 'exitLink, homeLink'}]")
            }
        }
        if (!nav.beanListNavigation) {
            if (context.providesSetupMode && context.providesEditMode) {
                nav.beanListNavigation = context.popupView ? [selectExitLink, editLink, setupLink ]: [selectExitLink, editLink, setupLink, homeLink]
                logger.debug("createDefaultNavigation: nav.beanListNavigation set [links=${context.popupView ? 'selectExitLink, editLink, setupLink' : 'selectExitLink, editLink, setupLink, homeLink'}]")
            } else if  (context.providesSetupMode) {
                nav.beanListNavigation = context.popupView ? [selectExitLink, setupLink ]: [selectExitLink, setupLink, homeLink]
                logger.debug("createDefaultNavigation: nav.beanListNavigation set [links=${context.popupView ? 'selectExitLink, setupLink' : 'selectExitLink, setupLink, homeLink'}]")
            } else if  (context.providesEditMode) {
                nav.beanListNavigation = context.popupView ? [selectExitLink, editLink ]: [selectExitLink, editLink, homeLink]
                logger.debug("createDefaultNavigation: nav.beanListNavigation set [links=${context.popupView ? 'selectExitLink, editLink' : 'selectExitLink, editLink, homeLink'}]")
            } else {
                nav.beanListNavigation = context.popupView ? [selectExitLink ]: [selectExitLink, homeLink]
                logger.debug("createDefaultNavigation: nav.beanListNavigation set [links=${context.popupView ? 'selectExitLink' : 'selectExitLink, homeLink'}]")
            }
        }
        if (!nav.createNavigation) {
            nav.createNavigation = context.popupView ? [disableCreateLink ]: [disableCreateLink, homeLink]
            logger.debug("createDefaultNavigation: nav.createNavigation set [links=${context.popupView ? 'disableCreateLink' : 'disableCreateLink, homeLink'}]")
        }
        if (!nav.editNavigation) {
            nav.editNavigation = context.popupView ? [disableEditLink ]: [disableEditLink, homeLink]
            logger.debug("createDefaultNavigation: nav.editNavigation set [links=${context.popupView ? 'disableEditLink' : 'disableEditLink, homeLink'}]")
        }
        if (!nav.setupNavigation) {
            nav.setupNavigation = context.popupView ? [disableSetupLink ]: [disableSetupLink, homeLink]
            logger.debug("createDefaultNavigation: nav.setupNavigation set [links=${context.popupView ? 'disableSetupLink' : 'disableSetupLink, homeLink'}]")
        }
    }

    void collect() {
        def selected
        Long id = form.getLong('id')
        if (id) {
            for (obj in list) {
                if (isEntityMatching(obj, id)) {
                    selected = obj
                    break
                }
            }
        }
        if (selected) {
            collected << selected
            if (!env.collectMultipleEnabled) {
                for (Iterator i = list.iterator(); i.hasNext();) {
                    def obj = i.next()
                    if (isEntityMatching(obj, id)) {
                        i.remove()
                        break
                    }
                }
            }
        }
    }

    void removeCollected() {
        def selected
        Long id = form.getLong('id')
        if (id) {
            for (Iterator i = collected.iterator(); i.hasNext();) {
                def obj = i.next()
                if (isEntityMatching(obj, id)) {
                    selected = obj
                    i.remove()
                    break
                }
            }
        }
        if (selected && !env.collectMultipleEnabled) {
            list << selected
        }
    }

    void save() {
        // implement persistence tasks
    }

    void select() {
        Long id = form.getLong('id')
        if (!id) {
            bean = null
            form.params.id = null
        } else {
            for (obj in list) {
                if (isEntityMatching(obj, id)) {
                    bean = obj
                    break
                }
            }
        }
    }

    protected boolean isEntityMatching(Object obj, Long id) {
        return ((obj instanceof Entity && obj.id == id)
                || (obj instanceof PersistentEntity && obj.getPrimaryKey() == id))
    }

    void disableAllModes() {
        env.createMode = false
        env.editMode = false
        env.setupMode = false
    }

    boolean isAutoreloadEnabled() {
        context.autoreloadEnabled
    }

    void enableAutoreload() {
        context.autoreloadEnabled = true
    }

    void disableAutoreload() {
        context.autoreloadEnabled = false
    }

    boolean isSetupMode() {
        env.setupMode
    }

    void disableSetupMode() {
        env.setupMode = false
    }

    void enableSetupMode() {
        env.setupMode = true
    }

    boolean isCreateMode() {
        env.createMode
    }

    void disableCreateMode() {
        env.createMode = false
    }

    void enableCreateMode() {
        env.createMode = true
    }

    boolean isEditMode() {
        env.editMode
    }

    void disableEditMode() {
        env.editMode = false
    }

    void enableEditMode() {
        env.editMode = true
    }

    String getExitId() {
        env.exitId
    }

    String getExitTarget() {
        env.exitTarget ?: context.exitTarget
    }

    String getExitTargetSource() {
        env.exitTargetSource ?: '/index'
    }

    String getHomeTarget() {
        env.homeTarget ?: context.homeTarget
    }

    boolean isExternalInvocationMode() {
        env.externalInvocationMode
    }

    void disableExternalInvocationMode() {
        env.externalInvocationMode = false
    }

    void enableExternalInvocationMode() {
        env.externalInvocationMode = true
    }

    boolean isDestroyViewOnExit() {
        context.destroyViewOnExit
    }

    boolean isDestroyViewOnHome() {
        context.destroyViewOnHome
    }

    boolean isPopupView() {
        context.popupView
    }

    void enablePopupView() {
        context.popupView = true
    }

    void disablePopupView() {
        context.popupView = false
    }

    boolean isPopupSaveExit() {
        context.popupSaveExit
    }

    void enablePopupSaveExit() {
        context.popupSaveExit = true
    }

    boolean isPopupSaveReloadParent() {
        context.popupSaveReloadParent
    }

    void enablePopupSaveReloadParent() {
        context.popupSaveReloadParent = true
    }

    /**
     * Enables createMode availability
     */
    protected void providesCreateMode() {
        context.providesCreateMode = true
    }

    /**
     * Disables createMode availability
     */
    protected void notProvidesCreateMode() {
        context.providesCreateMode = false
    }

    /**
     * Enables editMode availability
     */
    protected void providesEditMode() {
        context.providesEditMode = true
    }

    /**
     * Disables editMode availability
     */
    protected void notProvidesEditMode() {
        context.providesEditMode = false
    }

    /**
     * Enables setupMode availability
     */
    protected void providesSetupMode() {
        context.providesSetupMode = true
    }

    /**
     * Disables setupMode availability
     */
    protected void notProvidesSetupMode() {
        context.providesSetupMode = false
    }

    boolean isForwardRequest() {
        requestContext.method == 'forward'
    }

    boolean isPrintRequest() {
        requestContext.method == 'print'
    }

    boolean isReloadRequest() {
        requestContext.method == 'reload'
    }

    boolean isSaveRequest() {
        requestContext.method == 'save'
    }

    boolean isSelectRequest() {
        requestContext.method == 'select'
    }

    boolean requestIs(String methodName) {
        requestContext.method == methodName
    }

    boolean isUpdateOnlyEnabled() {
        form.getBoolean('updateOnly')
    }

    boolean isUserRequired() {
        context.userRequired
    }

    boolean isCustomNavigationAvailable() {
        env.customNavigation
    }

    protected void providesCustomNavigation() {
        env.customNavigation = true
    }

    String getName() {
        if (!_name) {
            _name = getClass().getSimpleName()
            _name = "${_name.substring(0,1).toLowerCase()}${_name.substring(1, _name.length())}"
        }
        return _name
    }

    Object getViewReference() {
        if (env['viewReferenceName']) {
            return env[env['viewReferenceName']]
        }
        null
    }

    protected String getViewReferenceName() {
        return env['viewReferenceName']
    }

    void reloadViewReference() {
        if (viewReference) {
            try {
                viewReference.reload()
                logger.debug("reloadViewReference: done [name=${env['viewReferenceName']}]")
            } catch (Throwable t) {
                logger.debug("reloadViewReference: reference did not reload [name=${env['viewReferenceName']}, message=${t.message}]")
            }
        }
    }

    PortalView getPortalView() {
        env.portalView instanceof PortalView ? env.portalView : null
    }

    void reload() {
        // implement bean and/or list reloading if implementing class requires this
    }

    void reset() {
        requestAttributes = [:]
        requestContext = [:]
        requestHeaders = [:]
        env = [:]
        nav = [:]
        list = []
        bean = null
    }

    String getAppLink() {
        "${context.appPath}/${context.name}"
    }

    String getBaseLink() {
        "/${context.name}"
    }

    MenuItem getExitLink() {
        navigationLink("/${context.name}/exit", 'backIcon', 'backToLast')
    }

    MenuItem getHomeLink() {
        navigationLink("/${context.name}/home", 'homeIcon', 'backToMenu')
    }

    MenuItem getReloadLink() {
        return navigationLink("/${context.name}/reload", 'replaceIcon', 'reload')
    }

    MenuItem getSetupLink() {
        return navigationLink("/${context.name}/enableSetupMode", 'configureIcon', 'configuration')
    }

    MenuItem getDisableSetupLink() {
        navigationLink("/${context.name}/disableSetupMode", 'backIcon', 'disableSetupMode')
    }

    MenuItem getEditLink() {
        return context.popupView ? navigationLink("/${context.name}/enableEditMode", 'editIcon', 'edit') : navigationLink("/${context.name}/enableEditMode", 'writeIcon', 'edit')
    }

    MenuItem getDisableEditLink() {
        navigationLink("/${context.name}/disableEditMode", 'backIcon', 'disableEditMode')
    }

    MenuItem getCreateLink() {
        return context.popupView ? navigationLink("/${context.name}/enableCreateMode", 'newIcon', 'create') : navigationLink("/${context.name}/enableCreateMode", 'smallNewIcon', 'create')
    }

    MenuItem getDisableCreateLink() {
        navigationLink("/${context.name}/disableCreateMode", 'backIcon', 'disableCreateMode')
    }

    MenuItem getPrintLink() {
        navigationLink("/${context.name}/print", 'printIcon', 'documentPrint')
    }

    MenuItem getSaveLink() {
        navigationLink("/${context.name}/save", 'saveIcon', 'save')
    }

    MenuItem getSelectExitLink() {
        navigationLink("/${context.name}/select", 'backIcon', 'backToLast')
    }

    MenuItem getSelectLink() {
        navigationLink("/${context.name}/select", 'enabledIcon', 'select')
    }

    protected final MenuItem contextLink(String url, String icon, String title) {
        new MenuItem(link: "${context.contextPath}${url}", icon: icon, title: title)
    }

    protected final MenuItem navigationLink(String url, String icon, String title) {
        new MenuItem(link: "${context.fullPath}${url}", icon: icon, title: title)
    }

    protected final MenuItem navigationLink(String url, String icon, String title, String permissions, String permissionInfo) {
        new MenuItem(link: "${context.fullPath}${url}", icon: icon, title: title, permissions: permissions, permissionInfo: permissionInfo)
    }

    protected final MenuItem mailtoLink(String recipients, String title) {
        new MenuItem(link: "mailto:${recipients}", icon: 'mailIcon', title: title)
    }

    protected final String url(String link) {
        "${(context.appPath ?: '')}${link}"
    }

    protected final String createTarget(String url) {
        def target = url
        if (WebUtil.isStrutsRequest(target) && target.indexOf('redirect') < 0) {
            target = "redirect:${target}"
        } else if (target.startsWith('/') && target.indexOf('redirect') < 0) {
            target = "redirect:${context.appPath}${target}"
        }
        return target
    }

    protected void addExitId(String exitId) {
        if (exitId) {
            env.exitId = exitId
            logger.debug("addExitId: done [id=${env.exitId}]")
        }
    }

    protected void addExitTarget(String exit) {
        if (exit && (!exitTargetInitialized || overrideExitTarget)) {
            env.exitTargetSource = exit
            env.exitTarget = createTarget(exit)
            exitTargetInitialized = true
            logger.debug("addExitTarget: done [target=${env.exitTarget}]")
        }
    }

    final String getSelectionExit() {
        env.selectionExit
    }

    void setSelectionExit(String exit) {
        env.selectionExit = exit
    }

    final String getSelectionTarget() {
        env.selectionTarget
    }

    void setSelectionTarget(String target) {
        env.selectionTarget = target
    }

    final String getSelectionTargetIdParam() {
        env.selectionTargetIdParam ?: 'id'
    }

    void setSelectionTargetIdParam(String idParamName) {
        env.selectionTargetIdParam = idParamName
    }

    void checkPermission(String[] permissions) throws PermissionException {
        if (!isPermissionGrant(permissions)) {
            logger.debug("checkPermission: not found [permissions=${StringUtil.createCommaSeparated(permissions)}]")
            throw new PermissionException()
        }
    }

    void checkPermission(String permissions) throws PermissionException {
        String[] array = StringUtil.getTokenArray(permissions)
        checkPermission(array)
    }

    void checkPermission(String permissions, String errorKey) throws PermissionException {
        String[] array = StringUtil.getTokenArray(permissions)
        try {
            checkPermission(array)
        } catch (PermissionException e) {
            throw new PermissionException(errorKey)
        }
    }

    final Boolean isPermissionGrant(String[] permissions) {
        if (!user && userRequired) {
            if (popupView) {
                throw new PopupTimeoutException()
            }
            throw new TimeoutException()
        }
        if (!permissions) {
            return true
        }
        user.isPermissionGrant(permissions)
    }

    final Boolean isPermissionGrant(String permissions) {
        String[] array = StringUtil.getTokenArray(permissions)
        isPermissionGrant(array)
    }

    protected final boolean isUserPropertyEnabled(String name) {
        user?.properties[name]?.enabled
    }

    protected final String getUserProperty(String name) {
        user?.properties[name]?.value
    }

    Boolean isRoot() {
        false
    }

    Long getScrollPosition() {
        return scrollPosition?:0
    }

    void setScrollPosition(Long scrollPosition) {
        this.scrollPosition = scrollPosition
    }

    void sort() {
        sortByProperty(getString('key'))
    }

    protected void sortByProperty(String propertyName) {
        if (actualSortKey && propertyName && actualSortKey != propertyName) {
            // reset reverse search
            actualSortReverse = false
            logger.debug("sortByProperty: disabling reverse sort [name=${actualSortKey}]")
        }
        if (actualSortKey && actualSortKey == propertyName) {
            list = list.reverse()
            actualSortReverse = !actualSortReverse
            logger.debug("sortByProperty: same property, listing reverse [name=${actualSortKey}]")
        } else {
            if (propertyName) {
                actualSortKey = propertyName
            }
            if (actualSortKey) {
                logger.debug("sortByProperty: property found [name=${actualSortKey}]")
                try {
                    List keys = actualSortKey.tokenize('.')
                    logger.debug("sortByProperty: sort key tokenized [hierarchie=${keys?.size()}]")
                    switch (keys.size) {
                        case 1:
                            list.sort{ a, b -> a?."${keys.get(0)}" <=> b?."${keys.get(0)}" }
                            break
                        case 2:
                            list.sort{ a, b -> a?."${keys.get(0)}"."${keys.get(1)}" <=> b?."${keys.get(0)}"."${keys.get(1)}" }
                            break
                        case 3:
                            list.sort{ a, b -> a?."${keys.get(0)}"."${keys.get(1)}"."${keys.get(2)}" <=> b?."${keys.get(0)}"."${keys.get(1)}"."${keys.get(2)}" }
                            break
                        case 4:
                            list.sort{ a, b -> a?."${keys.get(0)}"."${keys.get(1)}"."${keys.get(2)}"."${keys.get(3)}" <=> b?."${keys.get(0)}"."${keys.get(1)}"."${keys.get(2)}"."${keys.get(3)}" }
                            break
                        case 5:
                            list.sort{ a, b -> a?."${keys.get(0)}"."${keys.get(1)}"."${keys.get(2)}"."${keys.get(3)}"."${keys.get(4)}" <=> b?."${keys.get(0)}"."${keys.get(1)}"."${keys.get(2)}"."${keys.get(3)}"."${keys.get(4)}" }
                            break
                        default:
                            return
                    }
                    if (!propertyName && actualSortReverse) {
                        // reload context
                        logger.debug('sortByProperty: reverting search result')
                        list = list.reverse()
                    }

                } catch(Exception e) {
                    // ignore exceptions
                    logger.debug("sortByProperty: failed [message=${e.message}]", e)
                }
            } else {
                logger.debug('sortByProperty: no property defined')
            }
        }
    }

    void initListStep(int initialListStep) {
        listStep = initialListStep
        listStart = 0
        listStop = listStep - 1
    }

    void listNext() {
        logger.debug("listNext: invoked [start=${listStart}, end=${listStop}]")
        if (listStart + listStep >= listCount) {
            listStart = 0
            listStop = listStep - 1
        } else {
            listStart = listStart + listStep
            listStop = listStop + listStep
        }
        logger.debug("listNext: done [start=${listStart}, end=${listStop}]")
    }

    void listPrevious() {
        logger.debug("listPrevious: invoked [start=${listStart}, end=${listStop}]")
        if (listStart - listStep < 0) {
            int cnt = listCount
            int it = cnt / listStep;
            if (cnt % listStep == 0) {
                it = it - 1
            }
            listStart = it * listStep;
            listStop = listStart + listStep - 1
        } else {
            listStart = listStart - listStep
            listStop = listStart + listStep
        }
        logger.debug("listPrevious: done [start=${listStart}, end=${listStop}]")
    }

    boolean isFirstList() {
        listStart == 0
    }

    boolean isLastList() {
        listStop == 0 || listStop >= listCount - 1
    }

    Integer getListEnd() {
        listStop > 0 ? listStop : listCount - 1
    }

    Integer getListCount() {
        list == null ? 0 : list.size()
    }

    void resetListCounters() {
        listStart = 0
        listStop = listStart + listStep
    }

    protected final boolean getBoolean(String name) {
        form.getBoolean(name)
    }

    protected final Date getDate(String name) {
        form.getDate(name)
    }

    protected final BigDecimal getDecimal(String name) {
        form.getDecimal(name)
    }

    protected final Double getDouble(String name) {
        form.getDouble(name)
    }

    protected final Integer getInteger(String name) {
        form.getInteger(name)
    }

    protected final Long getLong(String name) {
        form.getLong(name)
    }

    protected final Double getPercentage(String name) {
        form.getPercentage(name)
    }

    protected final String getString(String name) {
        form.getString(name)
    }

    protected final void lockForm() {
        formLocked = true
    }

    protected final void unlockForm() {
        formLocked = false
    }

    protected void updateBean(String propertyName, Object value) {
        if (bean) {
            bean[propertyName] = value
        }
    }

    protected void dumpRequest() {
        String headers = StringUtil.dump(requestHeaders, '\n')
        logger.debug("\n=== REQUEST-HEADERS    ===\n${headers}\n")
        String attrs = StringUtil.dump(requestAttributes, '\n')
        logger.debug("\n=== REQUEST-ATTRIBUTES ===\n${attrs}\n")
    }

    protected final ResourceLocator getResourceLocator() {
        getService(ResourceLocator.class.getName())
    }

    protected final Object getService(String name) {
        if (serviceLocator) {
            try {
                serviceLocator.lookupService(name)
            } catch (Exception e) {
                logger.debug("getService: locator available but service not bound [name=${name}]")
            }
        }
    }

    private transient Locator serviceLocator
    public void setLocator(Locator locator) {
        serviceLocator = locator
    }
}
