<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<o:page />
<c:set var="portalView" value="${sessionScope.portalView}" />
<!DOCTYPE html>
<html lang="de">
<head>
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_default_css.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_default_custom_css.jsp" />
<tiles:getAsString name="styles" ignore="true" />
<tiles:insert attribute="javascript" ignore="true" />
<title><tiles:getAsString name="title" ignore="true" /></title>
</head>
<body>
    <jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/header_public.jsp" />
    <div class="container-fluid">
        <div class="row">
            <tiles:insert attribute="content" ignore="true" />
        </div>
    </div>
</body>
</html>
