/**
 *
 * Copyright (C) 2018 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 4, 2018 
 * 
 */
package com.osserp.gui.admin.mail

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.StylesheetDmsManager
import com.osserp.common.mail.Mail
import com.osserp.common.mail.EmailAddress

import com.osserp.core.mail.MailMessageContext
import com.osserp.core.mail.MailTemplate
import com.osserp.core.mail.MailTemplateManager

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class MailTemplateConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(MailTemplateConfigView.class.getName())

    DmsDocument templateText
    String previewText
    String previewSentTo
    String selectedRecipient
    List<EmailAddress> availableRecipients = []

    MailTemplateConfigView() {
        super()
        headerName = 'mailTemplateConfigHeader'
        providesEditMode()
    }

    private void setTemplate(MailTemplate mailTemplate) {
        if (mailTemplate && !mailTemplate.customText && mailTemplate.template) {
            templateText = templateDmsManager.findEmailText(mailTemplate.template)
            try {
                previewText = mailTemplateManager.renderExample(domainEmployee, mailTemplate)
            } catch (ClientException e) {
                previewText = getLocalizedMessage(e.message)
            } catch (Exception e) {
                previewText = e.message
            }
        }
        bean = mailTemplate
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            availableRecipients = domainUser.employee.emails
            Long templateId = form.getLong('id')
            if (templateId) {
                setTemplate(mailTemplateManager.getTemplate(templateId))
            } else {
                String templateName = form.getString('template')
                if (templateName) {
                    setTemplate(mailTemplateManager.findTemplate(templateName))
                } else {
                    list = mailTemplateManager.templates
                }
            }
        } else if (reloadRequest && bean) {
            setTemplate(mailTemplateManager.getTemplate(bean.id))
        }
        previewSentTo = null
    }

    @Override
    void save() {
        mailTemplate.update(
                form.getString('name'),
                form.getString('subject'),
                form.getBoolean('sendAsUser'),
                form.getString('originator'),
                form.getBoolean('fixedOriginator'),
                form.getString('recipients'),
                mailTemplate.fixedRecipients,
                form.getString('recipientsCC'),
                form.getBoolean('fixedRecipientsCC'),
                form.getString('recipientsBCC'),
                form.getBoolean('fixedRecipientsBCC'),
                form.getBoolean('hideRecipientsBCC'),
                mailTemplate.addAttachments,
                mailTemplate.fixedAttachment,
                form.getBoolean('fixedText'),
                form.getBoolean('personalizedHeader'),
                form.getBoolean('personalizedFooter'),
                form.getBoolean('addGroup'),
                form.getString('groupname'),
                form.getBoolean('addUser'),
                form.getString('username'),
                form.getBoolean('internal'),
                form.getBoolean('internalOnly'))
        setTemplate(mailTemplateManager.update(mailTemplate))
        disableEditMode()
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            setTemplate(mailTemplateManager.getTemplate(bean.id))
        } else {
            templateText = null
            previewText = null
            if (list) {
                list = mailTemplateManager.templates
            }
        }
    }

    MailMessageContext getMailMessageContext() throws ClientException {
        Long id = form.getLong('recipient')
        EmailAddress emailAddress = domainEmployee.emails.find { it.id == id }
        selectedRecipient = emailAddress?.email
        if (!selectedRecipient) {
            throw new ClientException(ErrorCode.RECIPIENT_MISSING)
        }
        if (!mailTemplate?.originator) {
            throw new ClientException(ErrorCode.ORIGINATOR_MISSING)
        }
        Mail mail = new Mail(
            mailTemplate.originator,
            selectedRecipient,
            mailTemplate.subject,
            previewText)
        Map<String, Object> opts = [
            successTarget: "${baseLink}/mailSent".toString(),
            exitTarget: "${baseLink}/forward?id=${mailTemplate.id}".toString()
            ]
        return new MailMessageContext(mailTemplate, mail, opts);
    }

    void logMail() {
        previewSentTo = selectedRecipient
        selectedRecipient = null
    }

    protected MailTemplate getMailTemplate() {
        return bean
    }

    protected MailTemplateManager getMailTemplateManager() {
        getService(MailTemplateManager.class.getName())
    }

    protected StylesheetDmsManager getTemplateDmsManager() {
        getService(StylesheetDmsManager.class.getName())
    }
}
