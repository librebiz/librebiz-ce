<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.purchaseOrderView or empty sessionScope.purchaseOrderView.bean}">
<o:logger write="no view found" level="warn"/>
	<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.purchaseOrderView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="baseUrl" scope="request" value="/purchaseOrder.do"/>
<c:set var="deliveryNoteUrl" scope="request" value="/purchaseDeliveryNote.do"/>
<c:set var="infoUrl" scope="request" value="/purchaseOrderInfos.do"/>
<c:set var="productExitTarget" scope="request" value="purchaseOrderReload"/>
<c:set var="productUrl" scope="request" value="/purchaseOrderProducts.do"/>
<c:set var="displayPrice" scope="request" value="false"/>
<c:set var="salesExitTarget" scope="request" value="purchaseOrder"/>
<c:set var="userIsPurchaser" value="false"/>
<o:permission role="purchasing,accounting,executive" info="permissionPurchaseDisplayPrice">
<c:set var="displayPrice" scope="request" value="true"/>
</o:permission>
<c:set var="createDeliveryNotePermissions" value="false"/>
<o:permission role="purchase_delivery_create" info="permissionCreateDeliveryNote">
<c:set var="createDeliveryNotePermissions" value="true"/>
</o:permission>
<o:permission role="purchasing,executive" info="permissionUserIsPurchaser">
<c:set var="userIsPurchaser" value="true"/>
<o:logger write="user is purchaser..." level="debug"/>
</o:permission>
<o:forbidden role="purchasing,executive">
<c:set var="paymentConditionEditEnabled" scope="request" value="false"/>
</o:forbidden>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
    <style type="text/css">
        <c:import url="/css/records.css"/>
    </style>
</tiles:put>

<tiles:put name="headline"><o:out value="${record.contact.id}"/> - <o:out value="${record.contact.displayName}"/></tiles:put>
<tiles:put name="headline_right">
<c:choose>
<c:when test="${recordView.setupMode}">
<a href="<c:url value="/purchaseOrder.do?method=disableSetup"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
</c:when>
<c:otherwise>
<a href="<c:url value="/purchaseOrder.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<c:if test="${!record.unchangeable and !empty record.items}">
<v:link url="/records/recordPrint/release?view=purchaseOrderView" confirm="true" message="confirmRecordRelease" title="recordRelease"><o:img name="enabledIcon" /></v:link>
</c:if>
<v:link url="/records/recordPrint/print?view=purchaseOrderView" title="documentPrint"><o:img name="printIcon" /></v:link>
<c:if test="${userIsPurchaser}">
<c:if test="${record.unchangeable}">
<v:link url="/records/recordMail/forward?view=purchaseOrderView&exit=/purchaseOrder.do?method=reload" title="sendViaEmail"><o:img name="mailIcon" /></v:link>
<a href="javascript:onclick=confirmLink('<fmt:message key="confirmPlease"/>\n<fmt:message key="confirmCreateCopyOfPurchaseOrder"/>','<c:url value="/purchaseOrder.do?method=createCopy"/>');" title="<fmt:message key="createByCopy"/>"><o:img name="copyIcon"/></a>
</c:if>
<c:if test="${!record.unchangeable}">
<a href="<c:url value="/purchaseOrder.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon"/></a>
</c:if>
<c:if test="${!record.closed and (empty record.deliveryNotes)}">
<a href="javascript:onclick=confirmLink('<fmt:message key="deleteConfirmOrdering"/>:\n<fmt:message key="ordering"/> <o:out value="${record.number}"/> <fmt:message key="from"/> <o:date value="${record.created}"/>', 
'<c:url value="/purchaseOrder.do?method=delete"/>');" title="<fmt:message key="recordDelete"/>"><o:img name="deleteIcon"/></a>
<a href="<c:url value="/purchaseOrder.do?method=enableSetup"/>" title="<fmt:message key="extended"/>"><o:img name="configureIcon"/></a>
</c:if>
</c:if>
</c:otherwise>
</c:choose>
<v:link url="/notes/note/forward?id=${record.id}&name=purchaseOrderNote&exit=/purchaseOrder.do?method=redisplay" title="displayAndWriteNotes"><o:img name="texteditIcon"/></v:link>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<c:choose>
<c:when test="${recordView.setupMode}">
<c:import url="/pages/purchasing/order_settings.jsp"/>
</c:when>
<c:otherwise>
<div style="width:100%; text-align:center; color:red;" id="errorMsg"></div>

<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
                
                    <div class="column66l">
                        <c:import url="/pages/records/record_branch_display.jsp" />
                        <c:import url="/pages/records/record_sigs_display.jsp"/>
                        <c:import url="/pages/records/record_payment_display.jsp"/>
                        <c:import url="/pages/records/record_delivery_conditions.jsp"/>
                        <c:import url="/pages/records/record_delivery_address_display.jsp"/>
                        <c:import url="/pages/records/record_shipping_display.jsp"/>
                        <c:import url="/pages/purchasing/supplier_reference.jsp"/>
                        <c:import url="/pages/purchasing/sales_reference.jsp"/>
                        <c:if test="${!record.type.displayNotesBelowItems}">
                            <c:import url="/pages/records/record_note_display.jsp"/>
                        </c:if>
                        <br/>
                    </div>
                    <div class="column33r">
                        <c:import url="/pages/records/record_status.jsp"/>
                        <c:if test="${recordView.importSupported}">
                        <div class="recordLabelValue">
                            <v:link url="/records/recordImport/forward?view=purchaseOrderView&exit=/purchaseOrder.do?method=reload"><fmt:message key="itemImportLabel"/></v:link>
                        </div>
                        </c:if>
                        <c:import url="/pages/records/record_delivery_date.jsp"/>
                        <c:if test="${userIsPurchaser or createDeliveryNotePermissions}">
                        <c:if test="${!record.notDeliveryNoteAffectingItemsOnly}">
                        <c:import url="/pages/purchasing/order_delivery_notes.jsp" />
                        </c:if>
                        <c:if test="${userIsPurchaser}">
                        <c:if test="${record.unchangeable and !record.closed}">
                        <c:choose>
                        <c:when test="${empty record.invoices}">
                            <c:choose>
                                <c:when test="${!empty record.deliveryNotes}">
                                    <div class="recordLabelValue">
                                        <a href="javascript:onclick=confirmLink('<fmt:message key="confirmPlease"/>:\n<fmt:message key="bookDeliveryInvoiceByStockReceipts"/>','<c:url value="/purchaseOrder.do?method=createInvoice"/>');" title="<fmt:message key="bookInDeliveries"/>">
                                            <fmt:message key="closeBill"/>
                                        </a>
                                    </div>
                                </c:when>
                                <c:when test="${record.notDeliveryNoteAffectingItemsOnly}">
                                    <div class="recordLabelValue">
                                        <a href="javascript:onclick=confirmLink('<fmt:message key="confirmPlease"/>:\n<fmt:message key="bookDeliveryInvoiceWithOrderItems"/>','<c:url value="/purchaseOrder.do?method=createInvoice"/>');" title="<fmt:message key="purchaseOrderComplete"/>">
                                            <fmt:message key="closeBill"/>
                                        </a>
                                    </div>
                                </c:when>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <div class="recordLabelValue">
                                <a href="<c:url value="/purchaseOrder.do?method=forwardInvoice"/>" title="<fmt:message key="displayDeliveryInvoice"/>">
                                    <fmt:message key="deliveryInvoice"/>
                                </a>
                            </div>
                        </c:otherwise>
                        </c:choose>
                        </c:if>
                        </c:if>
                        </c:if>
                        <c:if test="${!empty record.carryoverFrom}">
                            <div class="recordLabelValue">
                                <fmt:message key="carryoverFrom"/> <o:out value="${record.carryoverDisplay}"/>
                            </div>
                        </c:if>
                    </div>
                
                </div>
            </div>
        </div>
        
        <c:if test="${recordView.openDeliveryDisplay}">
        <c:import url="/pages/records/record_order_deliveries.jsp"/>
        </c:if>
        <c:import url="/pages/records/record_items.jsp"/>
        <c:if test="${record.type.displayNotesBelowItems}">
            <c:import url="/pages/records/record_note_display.jsp"/>
        </c:if>
        <c:import url="/pages/records/record_conditions.jsp"/>
    </div>
</div>
</c:otherwise>
</c:choose>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
