<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="recordView" scope="session" value="${sessionScope.salesInvoiceView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="hadSomethingToDisplay" value="false"/>

<div class="content-area" id="salesInvoiceSetupContent">
    <div class="row">

        <div class="col-md-6 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><fmt:message key="actions"/></h4>
                </div>
            </div>
            <div class="panel-body">
                <c:if test="${recordView.recordNumberChangeable}">
                    <o:form name="recordForm" url="/salesInvoice.do">
                        <input type="hidden" name="method" value="changeRecordId"/>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="invoiceNumber" />
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" name="recordId" class="form-control" value="<o:out value="${record.id}"/>"/>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <input type="image" name="method" src="<o:icon name="saveIcon"/>" value="changeRecordId" title="<fmt:message key="change"/>" class="bigicon"/>
                                </div>
                            </div>
                        </div>
                    </o:form>
                </c:if>

                <c:if test="${record.contact.discountEnabled or record.rebateAvailable}">
                    <div class="row">
                        <c:choose>
                            <c:when test="${record.rebateAvailable}">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="rebateEnabled" />
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <c:if test="${!record.closed}">
                                            <a href="<c:url value="/salesInvoice.do?method=removeRebate"/>">
                                                <fmt:message key="deactivate"/>
                                            </a>
                                        </c:if>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="rebateDisabled" />
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <c:if test="${!record.closed}">
                                            <a href="<c:url value="/salesInvoice.do?method=addRebate"/>">
                                                <fmt:message key="activate"/>
                                            </a>
                                        </c:if>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <c:set var="hadSomethingToDisplay" value="true"/>
                </c:if>

                <c:if test="${recordView.documentImportable}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="externalDocument" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <a href="<c:url value="/salesInvoice.do?method=switchHistorical"/>">
                                    <c:choose>
                                        <c:when test="${record.historical}">
                                            <fmt:message key="bigYes"/>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="bigNo"/>
                                        </c:otherwise>
                                    </c:choose>
                                </a>
                            </div>
                        </div>
                    </div>
                    <c:set var="hadSomethingToDisplay" value="true"/>
                </c:if>

                <c:if test="${record.downpayment and record.canceled}">
                    <o:permission role="${recordView.accountingPermissions}" info="permissionResetCanceled">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="cancellation" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmResetCancellation"/>','<c:url value="/salesInvoice.do?method=restoreDownpayment"/>');" title="<fmt:message key="resetCancellation"/>">
                                        <fmt:message key="delete"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <c:set var="hadSomethingToDisplay" value="true"/>
                    </o:permission>
                </c:if>
                <c:if test="${!hadSomethingToDisplay}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <fmt:message key="noActionsAvailableOnCurrentRecordState" />
                            </div>
                        </div>
                    </div>
                </c:if>

            </div>
        </div>

        <div class="col-md-6 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><fmt:message key="corrections"/></h4>
                </div>
            </div>
            <div class="panel-body">

                <c:choose>
                    <c:when test="${recordView.correctionEditMode or recordView.correctionCreateMode}">
                        <c:set var="obj" value="${record.correction}"/>
                        <c:if test="${empty obj}">
                            <c:set var="obj" value="${record.contact.address}"/>
                        </c:if>
                        <o:form name="recordForm" url="/salesInvoice.do">
                            <input type="hidden" name="method" value="saveCorrection"/>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="name"/> / <fmt:message key="company"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" value="<o:out value="${obj.name}"/>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="street"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="street" class="form-control" value="<o:out value="${obj.street}"/>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="zipcode"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="zipcode" class="form-control" value="<o:out value="${obj.zipcode}"/>"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="city"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="city" class="form-control" value="<o:out value="${obj.city}"/>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="country"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <oc:select name="countryId" value="${obj.country}" options="countryNames" styleClass="form-control" prompt="false"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="reason"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <textarea name="note" class="form-control" rows="6" ><o:out value="${obj.note}"/></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row next">
                                <div class="col-md-4"> </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <a href="<c:url value="/salesInvoice.do?method=disableCorrectionEditMode"/>">
                                                    <input type="button" value="<fmt:message key="break"/>" class="form-control cancel"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <o:submit styleClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </o:form>
                    </c:when>
                    <c:when test="${empty record.corrections}">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <fmt:message key="noCorrectionsAvailable"/>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <a href="<c:url value="/salesInvoice.do?method=enableCorrectionCreateMode"/>">
                                        [<fmt:message key="create"/>]
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:set var="obj" value="${record.correction}"/>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="name"/> / <fmt:message key="company"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${obj.name}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="street"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${obj.street}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="zipcode"/> / <fmt:message key="city"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${obj.zipcode}"/> <o:out value="${obj.city}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="country"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <oc:options name="countryNames" value="${obj.country}"/>
                                </div>
                            </div>
                        </div>
                        <c:if test="${!empty obj.note}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="reason"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <o:textOut value="${obj.note}"/>
                                    </div>
                                </div>
                            </div>
                        </c:if>

                        <c:choose>
                            <c:when test="${record.correction.unchangeable}">
                                <div class="row next">
                                    <div class="col-md-10"> </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="<c:url value="/salesInvoice.do?method=printCorrection"/>" title="<fmt:message key="print"/>">
                                                <o:img name="printIcon" styleClass="bigicon" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="<c:url value="/salesInvoice.do?method=enableCorrectionCreateMode"/>" title="<fmt:message key="createFurtherCorrection"/>">
                                                <o:img name="newdataIcon" styleClass="bigicon" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="row next">
                                    <div class="col-md-4"> </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="<c:url value="/salesInvoice.do?method=releaseCorrection"/>" title="<fmt:message key="release"/>">
                                                <o:img name="enabledIcon" styleClass="bigicon" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-5"> </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="<c:url value="/salesInvoice.do?method=enableCorrectionEditMode"/>" title="<fmt:message key="change"/>">
                                                <o:img name="writeIcon" styleClass="bigicon" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <a href="<c:url value="/salesInvoice.do?method=printCorrection"/>" title="<fmt:message key="preview"/>">
                                                <o:img name="printIcon" styleClass="bigicon" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>

                    </c:otherwise>
                </c:choose>

            </div>
        </div>

    </div>
</div>
