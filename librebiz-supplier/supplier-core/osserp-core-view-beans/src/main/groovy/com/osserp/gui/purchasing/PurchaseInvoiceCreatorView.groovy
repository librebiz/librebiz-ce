/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 3, 2011 9:09:37 AM 
 * 
 */
package com.osserp.gui.purchasing

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.util.NumberUtil

import com.osserp.core.products.Product
import com.osserp.core.products.ProductSearch
import com.osserp.core.purchasing.PurchaseInvoice

/**
 *
 * @author tn <tn@osserp.com>
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class PurchaseInvoiceCreatorView extends PurchaseRecordCreatorView {
    private static Logger logger = LoggerFactory.getLogger(PurchaseInvoiceCreatorView.class.getName())

    PurchaseInvoice alreadyAvailable
    
    PurchaseInvoiceCreatorView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
    }
    

    @Override
    void save() {
        enableCreateMode()
        alreadyAvailable = null
        
        if (!selectedBranch) {
            throw new ClientException(ErrorCode.CLIENT_NOT_SELECTED)
        }
        if (!selectedType) {
            throw new ClientException(ErrorCode.TYPE_MISSING)
        }
        
        Date invoiceDate = getDate('recordDateSupplier')
        String invoiceNumber = getString('recordNumberSupplier')
        
        if (invoiceNumber) {
            alreadyAvailable = purchaseInvoiceManager.getBySupplierReference(supplier, invoiceNumber)
            if (alreadyAvailable) {
                throw new ClientException(ErrorCode.RECORD_EXISTING)
            }
        }
        
        boolean recordDateInvoiceDate = getBoolean('recordDateSupplierDate')
        Date recordDate = recordDateInvoiceDate ? invoiceDate : getDate('recordDate')
        
        String recordNumberString = getString('recordNumber')
        Long recordNumber = NumberUtil.createLong(recordNumberString)
        if (recordNumberString && !recordNumber) {
            throw new ClientException(ErrorCode.RECORD_ID_NUMBER)
        }
        
        Product product
        Long productId = getLong('productId')
        if (productId) {
            product = productSearch.find(productId)
        }
        
        PurchaseInvoice invoice
        
        if (existing) {
            invoice = purchaseInvoiceManager.create(
                domainEmployee,
                existing,
                recordDate,
                recordNumber,
                invoiceDate,
                invoiceNumber)

        } else if (supplier) {
            invoice = purchaseInvoiceManager.create(
                    domainEmployee,
                    selectedBranch.company.id,
                    selectedBranch.id,
                    selectedType,
                    supplier,
                    recordDate,
                    recordNumber,
                    invoiceDate,
                    invoiceNumber,
                    product,
                    getString('customName'),
                    getString('productNote'),
                    getDouble('productQuantity'),
                    getDecimal('productPrice'))

        } else {
            invoice = purchaseInvoiceManager.create(
                    domainEmployee,
                    selectedBranch.company.id,
                    selectedBranch.id,
                    selectedType.id)
        }
        bean = invoice
        disableCreateMode()
        logger.debug("save: done [id=${invoice?.id}]")
    }
    
    boolean isPurchaseInvoiceRecordDateCurrent() {
        isSystemPropertyEnabled('purchaseInvoiceRecordDateCurrent')
    }

    protected ProductSearch getProductSearch() {
        getService(ProductSearch.class.getName())
    } 
}
