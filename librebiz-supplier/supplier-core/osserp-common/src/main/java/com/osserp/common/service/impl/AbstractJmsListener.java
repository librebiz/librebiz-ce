/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 18, 2007 6:00:01 PM 
 * 
 */
package com.osserp.common.service.impl;

import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;

import com.osserp.common.service.JmsListener;
import com.osserp.common.xml.JDOMParser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractJmsListener extends AbstractService implements JmsListener {
    private static Logger log = LoggerFactory.getLogger(AbstractJmsListener.class.getName());

    public void onMessage(Message message) {
        try {
            execute(message);
            if (log.isDebugEnabled()) {
                log.debug("onMessage() done [listener=" + getClass().getName()
                        + ", messageId=" + message.getJMSMessageID() + "]");
            }
        } catch (Exception e) {
            log.error("onMessage() failed [message=" + e.getMessage() + "]", e);
            throw new RuntimeException(e);
        }
    }

    protected abstract void execute(Message msg) throws Exception;

    protected Document fetchDocument(Message msg) throws Exception {
        String xml = fetchString(msg);
        if (xml != null) {
            try {
                return JDOMParser.getDocument(xml);
            } catch (Throwable t) {
                // we're fetching so we do nothing here and return null
            }
        }
        return null;
    }

    private String fetchString(Message msg) throws Exception {
        if (msg instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) msg;
            return textMessage.getText();
        }
        if (msg instanceof ObjectMessage) {
            Object obj = ((ObjectMessage) msg).getObject();
            if (obj instanceof String) {
                return (String) obj;
            }
        }
        log.warn("fetchString() message type is currently not supported [class" + msg.getClass().getName() + "]");
        return null;
    }
}
