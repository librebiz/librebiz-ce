/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 28, 2013 3:43:44 PM 
 * 
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.BusinessCaseSearch
import com.osserp.core.BusinessNote
import com.osserp.core.NoteManager

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class BusinessCaseDisplayView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(BusinessCaseDisplayView .class.getName())

    boolean notesDisplay = false
    List<BusinessNote> notes = []
    BusinessNote lastNote

    BusinessCaseDisplayView() {
        super()
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!bean) {
            Long id = form.getLong('id')
            notesDisplay = form.getBoolean('notes', notesDisplay)
            if (id) {
                bean = businessCaseSearch.findBusinessCase(id)
            }
            if (bean) {
                notes = noteManager.load(bean.primaryKey)?.notes
                if (notes) {
                    lastNote = notes[0]
                }
            }
        }
    }

    protected BusinessCaseSearch getBusinessCaseSearch() {
        getService(BusinessCaseSearch.class.getName())
    }

    protected NoteManager getNoteManager() {
        getService(getSystemProperty('salesNoteManager'))
    }

}
