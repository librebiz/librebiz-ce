/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 27, 2008 12:09:19 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import com.osserp.common.dao.Tables;
import com.osserp.common.service.impl.AbstractDatabaseStateSavingCache;
import com.osserp.common.util.DateUtil;

import com.osserp.core.dao.ProjectQueries;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.records.SalesRecordQueries;
import com.osserp.core.sales.SalesUtil;
import com.osserp.core.sales.SalesVolumeReport;
import com.osserp.core.sales.SalesVolumeReportService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesVolumeReportServiceImpl extends AbstractDatabaseStateSavingCache implements SalesVolumeReportService {
    private static Logger log = LoggerFactory.getLogger(SalesVolumeReportServiceImpl.class.getName());
    private Set<SalesVolumeReport> reports = new HashSet<SalesVolumeReport>();

    private ProjectQueries queries = null;
    private SalesRecordQueries salesQueries = null;

    protected SalesVolumeReportServiceImpl(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            ProjectQueries queries,
            SalesRecordQueries salesQueries) {
        super(jdbcTemplate, tables, TableKeys.SALES_VOLUME_REPORT_TASKS);
        this.queries = queries;
        this.salesQueries = salesQueries;
    }

    public SalesVolumeReport getReport(DomainUser user, int month, int year) {
        SalesVolumeReport data = getReport(month, year);
        SalesVolumeReport report = new SalesVolumeReport(month, year);
        report.addYearValues(new ArrayList(SalesUtil.filter(user, data.getYearList())));
        report.addYearUnreferencedValues(SalesUtil.filterSummary(user, data.getYearUnreferencedList()));
        report.addMonthValues(new ArrayList(SalesUtil.filter(user, data.getMonthList())));
        report.addMonthUnreferencedValues(SalesUtil.filterSummary(user, data.getMonthUnreferencedList()));
        report.addCancelledValues(new ArrayList(SalesUtil.filter(user, data.getCancelledList())));
        report.addStoppedValues(new ArrayList(SalesUtil.filter(user, data.getStoppedList())));
        report.addMonthCreatedValues(new ArrayList(SalesUtil.filter(user, data.getMonthCreatedList())));
        report.addYearCreatedValues(new ArrayList(SalesUtil.filter(user, data.getYearCreatedList())));
        return report;
    }

    public void refresh(String event) {
        setRefreshing(true);
        long started = System.currentTimeMillis();
        Set<SalesVolumeReport> refreshed = new HashSet<SalesVolumeReport>();
        for (Iterator<SalesVolumeReport> i = reports.iterator(); i.hasNext();) {
            SalesVolumeReport next = i.next();
            SalesVolumeReport latest = createReport(next.getMonth(), next.getYear());
            refreshed.add(latest);
        }
        reports = refreshed;
        setRefreshing(false);
        if (log.isDebugEnabled()) {
            long duration = (System.currentTimeMillis() - started);
            StringBuffer buffer = new StringBuffer("refresh() done [event=").append(event)
                    .append(", duration=").append(duration).append("ms]");
            log.debug(buffer.toString());
        }
    }

    private SalesVolumeReport getReport(int month, int year) {
        SalesVolumeReport result = fetchReport(month, year);
        if (result == null) {
            result = createReport(month, year);
            reports.add(result);
        }
        return result;
    }

    private SalesVolumeReport createReport(int month, int year) {
        if (log.isDebugEnabled()) {
            log.debug("createReport() invoked [month=" + month + ", year=" + year + "]");
        }
        long started = System.currentTimeMillis();
        SalesVolumeReport report = new SalesVolumeReport(month, year);
        try {
            // init years

            Date first = DateUtil.createDate("01.01." + year, false, 0, 0);
            Date last = null;
            if (month == 0) {
                last = DateUtil.createDate("01.02." + year, false, 0, 0);
            } else if (month == 11) {
                last = DateUtil.createDate("01.01." + (year + 1), false, 0, 0);
            } else {
                last = DateUtil.createDate("01." + DateUtil.createMonth(month + 1) + "." + year, false, 0, 0);
            }

            report.addYearValues(queries.getByExistingInvoice(first, last));
            report.addYearUnreferencedValues(salesQueries.getUnreferencedSummary(first, last));

            // init months

            StringBuffer buf = new StringBuffer("01.");
            buf.append(DateUtil.createMonth(month)).append(".").append(year);
            first = DateUtil.createDate(buf.toString(), false, 0, 0);

            report.addMonthValues(queries.getByExistingInvoice(first, last));
            report.addMonthUnreferencedValues(salesQueries.getUnreferencedSummary(first, last));

            // init cancelled

            report.addCancelledValues(queries.getByCancelledStatus(year));

            // init stopped

            report.addStoppedValues(queries.getByStoppedStatus(year));

            // init new created

            report.addMonthCreatedValues(queries.getCreatedInMonth(month, year));
            report.addYearCreatedValues(queries.getCreatedInYear(year));

            // log result

            if (log.isDebugEnabled()) {
                long duration = (System.currentTimeMillis() - started);
                StringBuffer buffer = new StringBuffer("createReport() done [duration=");
                buffer
                        .append(duration)
                        .append("ms, monthCount=")
                        .append(report.getMonthList().size())
                        .append(", monthUnreferencedCount=")
                        .append(report.getMonthUnreferencedList().size())
                        .append(", yearCount=")
                        .append(report.getYearList().size())
                        .append(", yearUnreferencedCount=")
                        .append(report.getYearUnreferencedList().size())
                        .append(", yearCreatedCount=")
                        .append(report.getYearCreatedList().size())
                        .append("]");
                log.debug(buffer.toString());
            }
        } catch (Throwable ignorable) {
            log.error("createReport() failed: " + ignorable.toString(), ignorable);
        }
        return report;
    }

    private SalesVolumeReport fetchReport(int month, int year) {
        if (!reports.isEmpty()) {
            for (Iterator<SalesVolumeReport> i = reports.iterator(); i.hasNext();) {
                SalesVolumeReport next = i.next();
                if (next.getMonth() == month && next.getYear() == year) {
                    return next;
                }
            }
        }
        return null;
    }

}
