SET search_path = osserp, pg_catalog;

UPDATE system_properties SET displayable = false WHERE id = 214;

UPDATE system_properties SET pvalue = '0.15.2', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
