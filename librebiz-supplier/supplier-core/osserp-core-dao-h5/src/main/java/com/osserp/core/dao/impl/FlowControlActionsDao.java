/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2006 10:19:57 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.Constants;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.FcsItem;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.dao.FlowControlWastebaskets;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.FlowControlActionImpl;
import com.osserp.core.model.FlowControlItemImpl;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.sales.Sales;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class FlowControlActionsDao extends AbstractFcsActionsDao implements FlowControlActions {
    private static Logger log = LoggerFactory.getLogger(FlowControlActionsDao.class.getName());

    private String flowControlsTable = null;

    public FlowControlActionsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            FlowControlWastebaskets baskets,
            String actionsTableKey,
            String dependencyTableKey,
            String wastebasketDefaultsTableKey,
            String itemTableKey) {
        super(jdbcTemplate, tables, sessionFactory, baskets, actionsTableKey, dependencyTableKey,
                wastebasketDefaultsTableKey);
        this.flowControlsTable = getTable(itemTableKey);
    }

    @Override
    protected List<FcsAction> findActions(BusinessCase object) {
        return findByType(((Sales) object).getType().getId());
    }

    @Override
    protected boolean isMatchingExternalStatus(FcsAction action, BusinessType businessType, String externalStatus) {
        FlowControlAction fcs = action instanceof FlowControlAction ? (FlowControlAction) action : null;
        if (fcs != null && businessType != null && externalStatus != null) {
            if (businessType.isExternalStatusSalesClosed(externalStatus) && fcs.isClosing()) {
                return true;
            }
            if (businessType.isExternalStatusSales(externalStatus)
                    && !businessType.isExternalStatusSalesClosed(externalStatus) && fcs.getOrderId() == 1) {
                // starting fcs action
                return true;
            }
        }
        return false;
    }

    public FcsAction createAction(Long typeId, String name, String description, Long groupId, boolean displayEverytime,
            int orderId) {
        FlowControlAction action = new FlowControlActionImpl(typeId, name, description, groupId, displayEverytime,
                orderId);
        getCurrentSession().save(FlowControlActionImpl.class.getName(), action);
        return action;
    }

    public List<FcsAction> getList() {
        return getCurrentSession()
                .createQuery("from " + FlowControlActionImpl.class.getName() + " a order by a.typeId, a.orderId")
                .list();
    }

    public FcsAction load(Long id) {
        return (FcsAction) getCurrentSession().load(FlowControlActionImpl.class, id);
    }

    public List<FcsAction> findByType(Long type) {
        if (type == null) {
            return getList();
        }
        return getCurrentSession().createQuery(
                "from " + FlowControlActionImpl.class.getName() + " a where a.typeId = :typeId order by a.orderId")
                .setParameter("typeId", type).list();
    }

    @Override
    public void save(FcsAction action) {
        getCurrentSession().merge(FlowControlActionImpl.class.getName(), action);
    }

    public Integer getIncrement(Long id) {
        StringBuffer query = new StringBuffer(64);
        query.append("SELECT fcs_increment FROM ").append(getFlowControlActionsTable()).append(" WHERE id = ?");
        final Object[] params = { id };
        final int[] types = { Types.BIGINT };
        return jdbcTemplate.queryForObject(query.toString(), params, types, Integer.class);
    }

    public String getLastAction(Long projectId) {
        return getActionName(getLastFcs(projectId));
    }

    private String getActionName(Long actionId) {
        if (actionId == null || Constants.LONG_NULL.equals(actionId)) {
            return "";
        }
        StringBuilder sql = new StringBuilder(96);
        sql.append("SELECT name FROM ").append(getFlowControlActionsTable()).append(" WHERE id = ?");
        final Object[] params = { actionId };
        final int[] types = { Types.BIGINT };
        return jdbcTemplate.queryForObject(sql.toString(), params, types, String.class);
    }

    private Long getLastFcs(Long projectId) {
        StringBuilder sql = new StringBuilder(96);
        sql.append("select fcs_id from ").append(flowControlsTable)
                .append(" WHERE project_id = ? order by created desc");
        final Object[] params = { projectId };
        final int[] types = { Types.BIGINT };
        List actions = (List) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
        if (empty(actions)) {
            return Constants.LONG_NULL;
        }
        return (Long) actions.get(0);
    }

    public Date getCreated(Long projectId, Long fcsId) {
        StringBuilder sql = new StringBuilder(96);
        sql.append("SELECT created FROM ").append(flowControlsTable)
                .append(" WHERE project_id = ? AND fcs_id = ? ORDER BY id DESC");
        final Object[] params = { projectId, fcsId };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        List list = (List) jdbcTemplate.query(sql.toString(), params, types, getTimestampRowMapper());
        return (list == null || list.isEmpty()) ? null : (Timestamp) list.get(0);
    }

    public List<FcsItem> findItemsByReference(Long reference) {
        return getCurrentSession()
                .createQuery("from " + FlowControlItemImpl.class.getName()
                        + " f where f.reference = :referenceId order by f.created desc")
                .setParameter("referenceId", reference).list();
    }

    public void createActions(Employee user, BusinessType source, BusinessType target) {
        List<FcsAction> src = findByType(source.getId());
        Map<Long, Long> sourceTarget = new HashMap<Long, Long>();
        Map<Long, Long> targetSource = new HashMap<Long, Long>();
        for (int i = 0, j = src.size(); i < j; i++) {
            FlowControlAction next = (FlowControlAction) src.get(i);
            FlowControlActionImpl copy = new FlowControlActionImpl(target, next);
            getCurrentSession().save(FlowControlActionImpl.class.toString(), copy);
            sourceTarget.put(next.getId(), copy.getId());
            targetSource.put(copy.getId(), next.getId());
            if (log.isDebugEnabled()) {
                log.debug("createActions() created next [id=" + copy.getId() + ", name=" + copy.getName() + "]");
            }
        }
        StringBuilder depSql = new StringBuilder();
        depSql.append("INSERT INTO ").append(getFlowControlDependenciesTable())
                .append(" (action_id, depends_on) VALUES (?,?)");
        String execute = depSql.toString();
        for (int i = 0, j = src.size(); i < j; i++) {
            FlowControlAction next = (FlowControlAction) src.get(i);
            Long newAction = sourceTarget.get(next.getId());
            Set<Long> deps = getDependencies(next.getId());
            for (Iterator<Long> it = deps.iterator(); it.hasNext();) {
                Long nextDep = it.next();
                Long newDep = sourceTarget.get(nextDep);
                final Object[] params = { newAction, newDep };
                final int[] types = { Types.BIGINT, Types.BIGINT };
                try {
                    jdbcTemplate.update(execute, params, types);
                } catch (Throwable e) {
                    log.warn("createActions() ignoring failure while creating deps [message=" + e.getMessage());
                }
            }
        }
        List<Long> wastebasket = getWastebasket();
        for (int i = 0, j = wastebasket.size(); i < j; i++) {
            Long next = wastebasket.get(i);
            if (sourceTarget.containsKey(next)) {
                StringBuilder wasteSql = new StringBuilder();
                wasteSql.append("INSERT INTO ").append(getFlowControlWastebasketDefaultsTable())
                        .append(" (fcs_id) VALUES (?)");
                final Object[] params = { sourceTarget.get(sourceTarget.get(next)) };
                final int[] types = { Types.BIGINT };
                try {
                    jdbcTemplate.update(wasteSql.toString(), params, types);
                } catch (Throwable e) {
                    log.warn("createActions() ignoring failure while creating waste [message=" + e.getMessage());
                }
            }
        }
    }

    public List<FcsClosing> findClosings(Long type) {
        return new ArrayList<FcsClosing>();
    }

    public FcsClosing createClosing(Long createdBy, String name, String description) {
        throw new java.lang.UnsupportedOperationException(
                "flowControl does not provide closing status in sales context");
    }

}
