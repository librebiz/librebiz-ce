/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2011 12:22:08 PM 
 * 
 */
package com.osserp.core.crm;

import java.util.Date;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Campaign extends Option {

    static final String REACH_COMPANY_UP = "reachCompanyUp";
    static final String REACH_BRANCH_UP = "reachBranchUp";
    static final String REACH_BRANCH_DOWN = "reachBranchDown";

    /**
     * Provides the campaign group
     * @return group
     */
    CampaignGroup getGroup();

    /**
     * Sets the group
     * @param group
     */
    void setGroup(CampaignGroup group);

    /**
     * Provides the campaign type
     * @return type
     */
    CampaignType getType();

    /**
     * Sets the type
     * @param type
     */
    void setType(CampaignType type);

    /**
     * Indicates that branch is parent campaign of other -active- campaigns. Note: A campaign may have children even isParent returns false (e.g. all children
     * eol)
     * @return parent
     */
    boolean isParent();

    /**
     * Sets that campaign is parent
     * @param parent
     */
    void setParent(boolean parent);

    /**
     * Updates parent reference
     * @param parent
     */
    void updateParent(Campaign parent);

    /**
     * Provides the reach of the campaign as key
     * @return reach
     */
    String getReach();

    /**
     * Sets reach of campaign
     * @param reach
     */
    void setReach(String reach);

    /**
     * Provides the related company (client)
     * @return company id
     */
    Long getCompany();

    /**
     * Provides the related branch (optional)
     * @return branch id
     */
    Long getBranch();

    /**
     * Sets the related branch id if required
     * @param branch
     */
    void setBranch(Long branch);

    /**
     * Indicates when campaign starts
     * @return campaignStarts
     */
    Date getCampaignStarts();

    /**
     * Sets the date when campaign starts
     * @param campaignStarts
     */
    void setCampaignStarts(Date campaignStarts);

    /**
     * Indicates when campaign ends
     * @return campaignEnds
     */
    Date getCampaignEnds();

    /**
     * Sets the date when campaign terminates
     * @param campaignEnds
     */
    void setCampaignEnds(Date campaignEnds);

    /**
     * Indicates that campaign is available to all companies (reach == companyUp)
     * @return ignoreCompany
     */
    boolean isIgnoreCompany();
}
