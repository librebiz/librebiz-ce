/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 27, 2014 12:23:51 PM 
 * 
 */
package com.osserp.gui.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.common.util.StringUtil

import com.osserp.core.products.Product
import com.osserp.core.products.ProductManager

import com.osserp.gui.CoreView


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ProductDescriptionView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ProductDescriptionView.class.getName())

    Product product
    String errorMessage

    ProductDescriptionView() {
        dependencies = ['productView']
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            product = env.productView?.product
        }
        errorMessage = null
        refresh()
    }

    @Override
    void save() {
        String language = form.getString('language')
        String[] localeArray = StringUtil.getLocaleParts(language)
        String lang = localeArray[0]
        String name = form.getString('name')
        String description = form.getString('description')
        String datasheetText = form.getString('datasheetText')
        productManager.updateDescription(domainUser.id, product, lang, name, description, datasheetText)
        env.productView.load(product, env.productView.exitTarget, "product${product.productId}")
        refresh()
        disableEditMode()
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            enableEditMode()
        } else {
            disableEditMode()
        }
    }

    protected void refresh() {
        if (product) {
            list = productManager.findDescriptions(product)
            if (!list) {
                try {
                    productManager.updateDescription(
                            product.getChangedBy() == null ? product.getCreatedBy() : product.getChangedBy(),
                            product,
                            product.getDefaultLanguage(),
                            product.getName(),
                            product.getDescription(),
                            null)
                    list = productManager.findDescriptions(product)
                } catch (Throwable t) {
                    logger.warn("refresh: caught exception [message=${t.message}]")
                    // error is handled below
                }
            }
        }
        if (!list) {
            errorMessage = ErrorCode.PRODUCT_CONFIG_INVALID
        }
    }

    protected ProductManager getProductManager() {
        getService(ProductManager.class.getName())
    }
}
