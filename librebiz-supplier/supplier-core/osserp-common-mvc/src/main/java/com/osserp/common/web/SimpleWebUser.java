/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 13, 2007 11:18:49 AM 
 * 
 */
package com.osserp.common.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.osserp.common.Constants;
import com.osserp.common.Property;
import com.osserp.common.User;
import com.osserp.common.UserPermission;
import com.osserp.common.beans.AbstractUser;
import com.osserp.common.beans.PropertyImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SimpleWebUser extends AbstractUser implements User {

    /**
     * Default constructor. Needed to implement the serializable interface
     */
    protected SimpleWebUser() {
        super();
    }

    /**
     * Default constructor. Needed to implement the serializable interface
     */
    protected SimpleWebUser(User o) {
        super((AbstractUser) o);
    }
    
    public static SimpleWebUser createExecutive(Long id, String name, String remoteIp) {
        return new SimpleWebUser(id, name, remoteIp, "executive");
    }
    
    public static SimpleWebUser createGuest(Long id, String name, String remoteIp) {
        return new SimpleWebUser(id, name, remoteIp, "guest");
    }

    public SimpleWebUser(
            Long id,
            String name,
            String remoteIp,
            String initialPermission) {
        super(id, Long.valueOf(6000), null, name, null);
        setUserEmployee(false);
        setPassword("secret");
        addPermission(id, initialPermission);
        setActive(true);
        setLoginTime(new java.util.Date(System.currentTimeMillis()));
    }

    public SimpleWebUser(Map<String, Object> user, List<Map<String, Object>> groups, String remoteIp) {
        super((Long) user.get("id"), Long.valueOf(6000), null, (String) user.get("uidnumber"), null);
        setUserEmployee(false);
        setLoginName((String) user.get("displayname"));
        setPassword((String) user.get("userPasswordDisplay"));
        setRemoteIp(remoteIp);
        addPermissions(groups);
        setActive(true);
        setLoginTime(new java.util.Date(System.currentTimeMillis()));
    }

    private void addPermissions(List<Map<String, Object>> groups) {
        for (int i = 0, j = groups.size(); i < j; i++) {
            Map<String, Object> next = groups.get(i);
            addPermission(Constants.SYSTEM_USER, (String) next.get("name"));
        }
    }

    @Override
    protected UserPermission createPermission(Long grantBy, String permission) {
        return new SimpleWebUserPermission(getId(), permission);
    }

    @Override
    protected Property createProperty(String name, String value) {
        return new PropertyImpl(getId(), name, value, getId());
    }

    @Override
    public Map<String, Property> getProperties() {
        return new HashMap<>();
    }

    @Override
    public void setProperty(User user, String name, String value) {
        // simple web user does not provide properties
    }

    @Override
    public Object clone() {
        return new SimpleWebUser(this);
    }
}
