/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06.10.2004 
 * 
 */
package com.osserp.core.products;

import org.jdom2.Element;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductDetails {
    
    /**
     * Provides the reference id (productId)
     * @return reference
     */
    Long getReference();

    /**
     * Indicates if at least one of width, height or depth is > 0
     * @return true if at least one set
     */
    boolean isMeasurementsAvailable();

    /**
     * The height
     * @return height
     */
    Integer getHeight();

    /**
     * Sets the height
     * @param height
     */
    void setHeight(Integer height);

    /**
     * The width
     * @return width
     */
    Integer getWidth();

    /**
     * Sets the width
     * @param width
     */
    void setWidth(Integer width);

    /**
     * The depth
     * @return depth
     */
    Integer getDepth();

    /**
     * Sets the depth
     * @param depth
     */
    void setDepth(Integer depth);

    /**
     * The measurement unit
     * @return measurementUnit
     */
    String getMeasurementUnit();

    /**
     * Sets the measurement unit
     * @param measurementUnit the measurementUnit to set
     */
    void setMeasurementUnit(String measurementUnit);

    /**
     * The weight
     * @return weight
     */
    Double getWeight();

    /**
     * Sets the weight
     * @param weight
     */
    void setWeight(Double weight);

    /**
     * Provides the unit of weight
     * @return the weightUnit
     */
    String getWeightUnit();

    /**
     * Sets the unit of weight
     * @param weightUnit the weightUnit to set
     */
    void setWeightUnit(String weightUnit);

    /**
     * A technical note
     * @return techNote
     */
    String getTechNote();

    /**
     * Sets the technical note
     * @param techNote
     */
    void setTechNote(String techNote);

    /**
     * The color id
     * @return color
     */
    Long getColor();

    /**
     * Sets the id of the color
     * @param color
     */
    void setColor(Long color);

    /**
     * Indicates that the width should be interpreted as diameter
     * @return widthIsDiameter
     */
    boolean isWidthIsDiameter();

    /**
     * Provides an XML representation of the details values
     * @return element
     */
    Element getXML();

    /**
     * Provides an XML representation of the details values
     * @param rootElementName
     * @return element
     */
    Element getXML(String rootElementName);

}
