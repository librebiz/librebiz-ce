<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.purchaseOrderView}"/>
<c:set var="records" value="${view.list}"/>
<c:set var="displayPrice" value="false"/>
<o:permission role="purchasing,accounting,executive" info="permissionOrderDisplayPrice">
    <c:set var="displayPrice" value="true"/>
</o:permission>
<c:set var="productMode" value="${!empty view.product}"/>
<o:logger write="Product mode: " value="${productMode}" level="debug"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="styles">
        <style type="text/css">
            .table .currency, .table .initials { text-align: center; }
        </style>
    </tiles:put>
    <tiles:put name="headline"><o:listSize value="${view.list}"/> <fmt:message key="openOrderings"/></tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/purchaseOrder.do?method=exitList"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
            <li><a href="<c:url value="/purchaseOrder.do?method=forwardCreate"/>" title="<fmt:message key="createPurchaseOrder"/>"><o:img name="newdataIcon"/></a></li>
            <li><a href="<c:url value="/purchaseOrder.do?method=changeListingMode"/>" title="<fmt:message key="productDisplay"/>"><o:img name="reloadIcon"/></a></li>
            <li><a href="<v:url value="/index"/>" title="<fmt:message key="backToMenu"/>"><o:img name="homeIcon"/></a></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <c:if test="${!empty sessionScope.message}">
            <div id="recordheader"><p>&nbsp;<span class="boldtext">&nbsp;&nbsp;<o:out value="${sessionScope.message}"/></span></p></div>
            <c:remove var="message" scope="session"/>
        </c:if>
        <div class="content-area" id="purchaseOrderListContent">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <c:choose>
                        <c:when test="${view.itemListing}">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <tr>
                                        <th><fmt:message key="numberOfOrdering"/></th>
                                        <th><fmt:message key="product"/></th>
                                        <th><fmt:message key="name"/></th>
                                        <th class="right"><fmt:message key="quantity"/></th>
                                        <c:if test="${displayPrice}">
                                            <th class="right"><fmt:message key="price"/></th>
                                            <th> </th>
                                        </c:if>
                                        <th><fmt:message key="toThe"/></th>
                                    </tr>
                                    <c:choose>
                                        <c:when test="${empty records}">
                                            <tr>
                                                <td><fmt:message key="noOpenOrdersAvailable"/> <a href="<c:url value="/purchaseOrder.do?method=forwardCreate"/>">[<fmt:message key="createNew"/>]</a></td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach var="record" items="${records}" varStatus="s">
                                                <c:forEach var="item" items="${record.items}">
                                                    <c:if test="${empty view.selectedStock or view.selectedStock == item.stockId}">
                                                        <c:if test="${!productMode or (productMode and item.product.productId == view.product.productId)}">
                                                            <tr<c:if test="${!item.deliveryConfirmed}"> class="red"</c:if>>
                                                                <c:choose>
                                                                    <c:when test="${record.unchangeable}">
                                                                        <td>
                                                                            <a href="<c:url value="/purchaseOrder.do?method=select&id=${record.id}"/>">
                                                                                <c:choose><c:when test="${empty record.carryoverFrom}"><o:out value="${record.number}"/></c:when><c:otherwise><o:out value="${record.carryoverDisplay}"/></c:otherwise></c:choose>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                                                                                <o:out value="${item.product.productId}"/>
                                                                            </o:popupSelect>
                                                                        </td>
                                                                        <td><o:out value="${item.productName}"/></td>
                                                                        <td class="right"><o:number value="${item.quantity}" format="integer"/></td>
                                                                        <c:if test="${displayPrice}">
                                                                            <td class="right"><o:number value="${item.price}" format="currency"/></td>
                                                                            <td><o:out value="${record.currencySymbol}"/></td>
                                                                        </c:if>
                                                                        <td><o:date value="${item.delivery}"/></td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <td>
                                                                            <a href="<c:url value="/purchaseOrder.do?method=select&id=${record.id}"/>">
                                                                                <c:choose><c:when test="${empty record.carryoverFrom}"><o:out value="${record.number}"/></c:when><c:otherwise><o:out value="${record.carryoverDisplay}"/></c:otherwise></c:choose>
                                                                            </a>
                                                                        </td>
                                                                        <td style="color: red;">
                                                                            <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                                                                                <o:out value="${item.product.productId}"/>
                                                                            </o:popupSelect>
                                                                        </td>
                                                                        <td style="color: red;"><o:out value="${item.productName}"/></td>
                                                                        <td style="color: red;" class="right"><o:number value="${item.quantity}" format="integer"/></td>
                                                                        <c:if test="${displayPrice}">
                                                                            <td style="color: red;" class="right"><o:number value="${item.price}" format="currency"/></td>
                                                                            <td style="color: red;"><o:out value="${record.currencySymbol}"/></td>
                                                                        </c:if>
                                                                        <td style="color: red;"><o:date value="${item.delivery}"/></td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </tr>
                                                        </c:if>
                                                    </c:if>
                                                </c:forEach>
                                                <c:forEach var="delivery" items="${record.deliveryNotes}">
                                                    <c:forEach var="deliveryItem" items="${delivery.items}">
                                                        <c:if test="${empty view.selectedStock or view.selectedStock == deliveryItem.stockId}">
                                                            <c:if test="${!productMode or (productMode and deliveryItem.product.productId == view.product.productId)}">
                                                                <tr>
                                                                    <td style="color: green;"><o:out value="${delivery.number}"/></td>
                                                                    <td style="color: green;">
                                                                        <o:popupSelect action="/productPopup.do" name="deliveryItem" property="product.productId" scope="page"
                                                                                                    header="product" dimension="${popupPageSize}" title="displayProductData">
                                                                            <o:out value="${deliveryItem.product.productId}"/>
                                                                        </o:popupSelect>
                                                                    </td>
                                                                    <td style="color: green;"><o:out value="${deliveryItem.productName}"/></td>
                                                                    <td style="color: green;" class="amount"><o:number value="${deliveryItem.quantity}" format="integer"/></td>
                                                                    <c:if test="${displayPrice}">
                                                                        <td style="color: green;" colspan="2">&nbsp;</td>
                                                                    </c:if>
                                                                    <td style="color: green;"><o:date value="${delivery.created}"/></td>
                                                                </tr>
                                                            </c:if>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:forEach>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </table>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <tr>
                                        <th><fmt:message key="numberOfOrdering"/></th>
                                        <th><fmt:message key="company"/></th>
                                        <th><fmt:message key="appointed"/></th>
                                        <th><fmt:message key="delivery"/></th>
                                        <th><fmt:message key="employee"/></th>
                                    </tr>
                                    <c:choose>
                                        <c:when test="${empty records}">
                                            <tr>
                                                <td><fmt:message key="noOpenOrdersAvailable"/> <a href="<c:url value="/purchaseOrder.do?method=forwardCreate"/>">[<fmt:message key="createNew"/>]</a></td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach var="record" items="${records}" varStatus="s">
                                                <tr>
                                                    <c:choose>
                                                        <c:when test="${record.unchangeable}">
                                                            <td>
                                                                <a href="<c:url value="/purchaseOrder.do?method=select&id=${record.id}"/>">
                                                                    <c:choose><c:when test="${empty record.carryoverFrom}"><o:out value="${record.number}"/></c:when><c:otherwise><o:out value="${record.carryoverDisplay}"/></c:otherwise></c:choose>
                                                                </a>
                                                            </td>
                                                            <td><o:out value="${record.contact.displayName}"/></td>
                                                            <td><o:date value="${record.created}"/></td>
                                                            <td><o:date value="${record.delivery}"/></td>
                                                            <td>
                                                                <v:ajaxLink linkId="createdByPopupView" url="/employees/employeePopup/forward?id=${record.createdBy}">
                                                                    <oc:employee value="${record.createdBy}" />
                                                                </v:ajaxLink>
                                                            </td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td>
                                                                <a href="<c:url value="/purchaseOrder.do?method=select&id=${record.id}"/>">
                                                                    <c:choose><c:when test="${empty record.carryoverFrom}"><o:out value="${record.number}"/></c:when><c:otherwise><o:out value="${record.carryoverDisplay}"/></c:otherwise></c:choose>
                                                                </a>
                                                            </td>
                                                            <td style="color: red;"><o:out value="${record.contact.displayName}"/></td>
                                                            <td style="color: red;"><o:date value="${record.created}"/></td>
                                                            <td style="color: red;"><o:date value="${record.delivery}"/></td>
                                                            <td style="color: red;" class="initials">
                                                                <v:ajaxLink linkId="createdByPopupView" url="/employees/employeePopup/forward?id=${record.createdBy}">
                                                                    <oc:employee value="${record.createdBy}"/>
                                                                </v:ajaxLink>
                                                            </td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </table>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
