/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Apr 16, 2009 2:06:03 PM 
 * 
 */
package com.osserp.core.dao;

import com.osserp.core.finance.DeliveryCondition;

/**
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public interface DeliveryConditions {

    /**
     * Loads a plant logger type from persistent storage
     * @param id
     * @return plant
     */
    DeliveryCondition load(Long id);

}
