/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.junit.Test

import static org.junit.Assert.*

import com.osserp.TestConfig
import com.osserp.common.directory.ldap.impl.LdapManagerImpl

/**
 * 
 * LdapManager integration tests (requires running ldap server & valid config)
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ILdapManagerImpl {
    private static Logger logger = LoggerFactory.getLogger(ILdapManagerImpl.class.getName())

    @Test
    void defaultConstructor() {
        LdapManagerImpl ldapManager = new LdapManagerImpl()
        assertNotNull(ldapManager)
    }

    @Test
    void constructorByConfigFile() {
        LdapManagerImpl ldapManager = new LdapManagerImpl(TestConfig.PROPERTY_FILE)
        assertTrue(ldapManager.config != null)
    }

    @Test
    void testConnection() {
        LdapManagerImpl ldapManager = new LdapManagerImpl(TestConfig.PROPERTY_FILE)
        assertTrue(ldapManager.config?.domainAvailable)
    }

    @Test
    void testFindClient() {
        LdapManagerImpl ldapManager = new LdapManagerImpl(TestConfig.PROPERTY_FILE)
        Map client = ldapManager.lookupClient(TestConfig.TESTCLIENT)
        assertTrue(client != null)
        assertTrue(client?.cn == TestConfig.TESTCLIENT)
        assertTrue(client?.mail == 'hostmaster@osserp.org')
        for (key in client.keySet()) {
            if ('userpassword' != key) {
                logger.debug("key=$key, value=${client[key]}")
            }
        }
    }

    @Test
    void testLookupUser() {
        LdapManagerImpl ldapManager = new LdapManagerImpl(TestConfig.PROPERTY_FILE)
        Map user = ldapManager.lookupUser(TestConfig.TESTUID, TestConfig.TESTCLIENT)
        assertTrue(user != null)
        assertTrue(user?.uid == TestConfig.TESTUID)
        assertTrue(user?.uidnumber == '10001')
        // camelcase notation is ignored when reading attributes
        // 13/7/17: this is no longer true so test is disabled
        // assertTrue(user.displayname == user.displayName)
        for (key in user.keySet()) {
            if ('userpassword' != key) {
                logger.debug("key=$key, value=${user[key]}")
            }
        }
    }

    @Test
    void testUserUpdate() {
        LdapManagerImpl ldapManager = new LdapManagerImpl(TestConfig.PROPERTY_FILE)
        Map user = ldapManager.lookupUser(TestConfig.TESTUID, TestConfig.TESTCLIENT)
        if (user) {
            def employeeTypeOrigin = user.employeetype
            ldapManager.updateUser(user, [employeetype:'Employee'])
            user = ldapManager.lookupUser(TestConfig.TESTUID, TestConfig.TESTCLIENT)
            assertTrue(user?.employeetype == 'Employee')
            // restore original attribute value
            ldapManager.updateUser(user, [employeetype:employeeTypeOrigin])
            user = ldapManager.lookupUser(TestConfig.TESTUID, TestConfig.TESTCLIENT)
            assertTrue(user?.employeetype == employeeTypeOrigin)

            // camelcase notation is also ignored when writing attributes:
            def displayNameOrigin = user.displayname
            // update attribute with camel case
            ldapManager.updateUser(user, [displayName:'testDisplayName'])
            user = ldapManager.lookupUser(TestConfig.TESTUID, TestConfig.TESTCLIENT)
            // reading attribute with lowercase is same as reading camel case
            assertTrue(user?.displayname == 'testDisplayName')
            // restore original attribute value
            ldapManager.updateUser(user, [displayName:displayNameOrigin])
            user = ldapManager.lookupUser(TestConfig.TESTUID, TestConfig.TESTCLIENT)
            assertTrue(user?.displayname == displayNameOrigin)
        }
    }

    @Test
    void testLookupUsers() {
        LdapManagerImpl ldapManager = new LdapManagerImpl(TestConfig.PROPERTY_FILE)
        List<Map> users = ldapManager.lookupUsers(TestConfig.TESTCLIENT)
        assertTrue(!users.empty)
    }
}

