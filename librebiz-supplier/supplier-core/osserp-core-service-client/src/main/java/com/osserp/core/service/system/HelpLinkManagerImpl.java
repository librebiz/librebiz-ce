/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 12, 2007 8:00:48 PM 
 * 
 */
package com.osserp.core.service.system;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.gui.HelpLink;
import com.osserp.common.gui.HelpLinkManager;
import com.osserp.core.dao.HelpLinks;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class HelpLinkManagerImpl extends AbstractService
        implements HelpLinkManager {
    private HelpLinks helpLinks = null;

    public HelpLinkManagerImpl(HelpLinks helpLinks) {
        super();
        this.helpLinks = helpLinks;
    }

    public HelpLink getLink(String name) {
        HelpLink link = helpLinks.getLinkByName(name);
        if (link == null) {
            link = helpLinks.addLink(name);
        }
        return link;
    }

    public void update(HelpLink link) throws ClientException {
        if (helpLinks.existsUnderAnotherId(link.getId(), link.getDescription())) {
            throw new ClientException(ErrorCode.NAME_EXISTS);
        }
        this.helpLinks.save(link);
    }
}
