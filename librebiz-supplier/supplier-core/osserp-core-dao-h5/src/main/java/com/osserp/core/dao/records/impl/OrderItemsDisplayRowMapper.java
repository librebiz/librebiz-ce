/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 20-Sep-2006 06:42:52 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.dao.AbstractRowMapper;

import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.products.ProductSummary;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OrderItemsDisplayRowMapper extends AbstractRowMapper {

    private Long stockId = null;
    private boolean salesContext = true;
    private boolean vacant = false;
    ProductSummaryCache summaryCache = null;

    /*
     * database definition record.sales_id as sales, get_sales_name(record.sales_id) as salesname, record.id as id, record.created, record.delivered item.id as
     * item, item.product_id as product, a.name as name, a.group_id, a.manufacturer_id, item.quantity, item.delivered, item.delivery_ack
     */

    public OrderItemsDisplayRowMapper(
            Long stockId,
            ProductSummaryCache summaryCache,
            boolean salesContext,
            boolean vacant) {
        super();
        this.stockId = stockId;
        this.salesContext = salesContext;
        this.vacant = vacant;
        this.summaryCache = summaryCache;
    }

    public Object mapRow(ResultSet rs, int arg1) throws SQLException {
        Long productId = Long.valueOf(rs.getLong(10));
        ProductSummary summary = summaryCache.getSummary(stockId, productId);
        OrderItemsDisplay vo = new OrderItemsDisplay(
                Long.valueOf(rs.getLong(1)),
                rs.getString(2),
                Long.valueOf(rs.getLong(3)),
                rs.getDate(4),
                Long.valueOf(rs.getLong(5)),
                Long.valueOf(rs.getLong(6)),
                Long.valueOf(rs.getLong(7)),
                rs.getDate(8),
                Long.valueOf(rs.getLong(9)),
                productId,
                rs.getString(11),
                Long.valueOf(rs.getLong(12)),
                Long.valueOf(rs.getLong(13)),
                Long.valueOf(rs.getLong(14)),
                Double.valueOf(rs.getDouble(15)),
                Double.valueOf(rs.getDouble(16)),
                rs.getBoolean(17),
                rs.getString(19), // deliveryNote
                summary.getStock(),
                summary.getOrdered(),
                summary.getVacant(),
                summary.getReceipt(),
                summary.getExpected(),
                summary.getSalesReceipt(),
                this.salesContext,
                this.vacant,
                this.stockId);
        return vo;
    }

    public static RowMapperResultSetExtractor getReader(
            Long stockId,
            ProductSummaryCache summaryCache,
            boolean salesContext,
            boolean vacant) {
        return new RowMapperResultSetExtractor(new OrderItemsDisplayRowMapper(stockId, summaryCache, salesContext, vacant));
    }

}
