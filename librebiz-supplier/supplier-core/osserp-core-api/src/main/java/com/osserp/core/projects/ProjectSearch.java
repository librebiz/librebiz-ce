/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 1, 2004 
 * 
 */
package com.osserp.core.projects;

import java.util.Date;
import java.util.List;

import com.osserp.common.Calendar;

import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.projects.results.ProjectByActionDate;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.sales.SalesListProductItem;
import com.osserp.core.sales.SalesMonitoringItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectSearch {

    /**
     * Finds all projects related to a project manager
     * @param managerId
     * @param openOnly indicates that we should lookup for open projects only
     * @return related projects
     */
    List<SalesListItem> findByManager(Long managerId, boolean openOnly);

    /**
     * Finds all projects related to a project manager
     * @param salesId
     * @param openOnly indicates that we should lookup for open projects only
     * @return related projects
     */
    List<SalesListItem> findBySalesperson(Long salesId, boolean openOnly);

    /**
     * Provides all projects with existing -final- invoice
     * @param from
     * @param til
     * @return by existing invoice
     */
    List<SalesListItem> findByExistingInvoice(Date from, Date til);

    /**
     * Provides all sales where all items are delivered and no final invoice exists
     * @return byMissingInvoices
     */
    List<SalesListItem> findByMissingInvoices();

    /**
     * Provides all sales with activated cancelled flag by year
     * @param year, all if null
     * @return cancelled
     */
    List<SalesListItem> findCancelled(Integer year);

    /**
     * Provides all sales with activated stop flag by year
     * @param year, all if null
     * @return stopped
     */
    List<SalesListItem> findStopped(Integer year);

    /**
     * Provides all outstanding deliveries
     * @return deliveries
     */
    List<SalesListProductItem> findDeliveries();

    /**
     * Provides outstanding deliveries by day
     * @param day
     * @param items return delivery items instead of sales if true
     * @return deliveries
     */
    List<SalesListProductItem> findDeliveriesByDay(Calendar day, boolean items);

    /**
     * Provides all outstanding installations
     * @return installations
     */
    List<SalesListItem> findMountings();

    /**
     * Finds all projects sleeping longer than given days
     * @param maxDaysAgo. Defaults to 7 if null or intValue = 0
     * @return projects sleeping longer
     */
    List<SalesListItem> findSleepingProjects(Integer maxDaysAgo);

    /**
     * Provides a list of all projects of given type where the given action was performed in the specified time period.
     * @param projectType
     * @param actionId
     * @param from
     * @param til
     * @param withCanceled includes canceled projects
     * @param withClosed includes closed projects
     * @param zipcode
     * @return projectsByActionAndDate
     */
    List<ProjectByActionDate> findByActionAndDate(
            Long projectType,
            Long actionId,
            Date from,
            Date til,
            boolean withCanceled,
            boolean withClosed,
            String zipcode);

    /**
     * Provides a list of all projects of given type where the given action was not performed
     * @param projectType
     * @param missingAction
     * @param withAction
     * @param withCanceled includes canceled projects
     * @param withClosed includes closed projects
     * @return projects
     */
    List<ProjectByActionDate> findByMissingAction(
            Long projectType,
            Long missingAction,
            Long withAction,
            boolean withCanceled,
            boolean withClosed);

    /**
     * Provides delivery monitoring values
     * @param stockId
     * @param descendant
     * @return deliveryMonitoring
     */
    List<SalesListProductItem> getDeliveryMonitoring(Long stockId, boolean descendant);
    
    /**
     * Provides a list of available subcontractors with stats
     * @return subcontractors with stats
     */
    List<BusinessCaseRelationStat> getSubcontractorStats();
    
    /**
     * Provides a list of available subcontractors with stats
     * @param contractorId optional, null returns all
     * @param ignoreClosed
     * @return subcontractor stats as spreadsheet
     */
    String getSubcontractorSpreadsheet(Long id, boolean ignoreClosed);

    /**
     * Provides subcontractor monitoring items
     * @param contractorId
     * @return contractor monitoring
     */
    List<SalesMonitoringItem> getSubcontractorMonitoring(Long contractorId);
}
