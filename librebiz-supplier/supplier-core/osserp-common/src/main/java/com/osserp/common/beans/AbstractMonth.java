/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 30, 2008 10:25:37 AM 
 * 
 */
package com.osserp.common.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.osserp.common.Calendar;
import com.osserp.common.Month;
import com.osserp.common.util.DateUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractMonth extends AbstractYear implements Month {

    /**
     * Creates a new calendar month initialized with current month values
     */
    protected AbstractMonth() {
        super();
    }

    /**
     * Creates a new month by month and year
     * @param month
     * @param year
     */
    protected AbstractMonth(int month, int year) {
        setDate(1, month, year);
    }

    /**
     * Creates a new month by month and year
     * @param month
     */
    protected AbstractMonth(Month month) {
        this(month.getMonth(), month.getYear());
    }

    /**
     * Creates a new month by date
     * @param date
     */
    protected AbstractMonth(Date date) {
        super();
        Integer[] time = DateUtil.getTime(date);
        setDate(1, time[1], time[2]);
    }

    @Override
    public Integer getMonth() {
        return super.getMonth();
    }

    @Override
    public Long getMonthAsLong() {
        return super.getMonthAsLong();
    }

    public final String getMonthAndYearDisplay() {
        StringBuilder mb = new StringBuilder();
        Integer m = getMonth();
        String md = getMonthPart(m);
        Integer year = getYear();
        if (year != null) {
            mb.append(md).append("-").append(year);
        }
        return mb.toString();
    }

    public final String getYearAndMonthDisplay() {
        StringBuilder mb = new StringBuilder();
        String m = getMonthPart(getMonth());
        Integer year = getYear();
        if (year != null) {
            mb.append(year).append(" - ").append(m);
        }
        return mb.toString();
    }

    public final String getShortYearAndMonthDisplay() {
        StringBuilder mb = new StringBuilder();
        String m = getMonthPart(getMonth());
        String y = getShortYearDisplay();
        if (y != null) {
            mb.append(y).append("-").append(m);
        }
        return mb.toString();
    }
    
    private String getMonthPart(Integer m) {
        StringBuilder result = new StringBuilder();
        if (m != null) {
            m = m + 1;
            result.append((m < 10 ? ("0" + m) : m.toString()));
        }
        return result.toString();
    }

    public int getDaysOfMonthCount() {
        GregorianCalendar cal = new GregorianCalendar(getYear(), getMonth(), 1);
        return DateUtil.getDaysOfMonthCount(cal.getTime());
    }

    public int getDaysOfLastMonthCount() {
        GregorianCalendar cal = new GregorianCalendar(getYear(), getMonth() - 1, 1);
        return DateUtil.getDaysOfMonthCount(cal.getTime());
    }

    public boolean isAfter(Month month) {
        if (getYear() > month.getYear()
                || (getYear().equals(month.getYear()) && getMonth() > month.getMonth())) {
            return true;
        }
        return false;
    }

    public boolean isBefore(Month month) {
        if (getYear() < month.getYear()
                || (getYear().equals(month.getYear()) && getMonth() < month.getMonth())) {
            return true;
        }
        return false;
    }

    public boolean isSame(Month month) {
        return (super.isSame(month) && getMonth().equals(month.getMonth()));
    }

    protected List<Calendar> getCalendarDays() {
        List<Calendar> list = new ArrayList<Calendar>();
        int size = getDaysOfMonthCount();
        for (int i = 0; i < size; i++) {
            Calendar day = new CalendarImpl(i + 1, getMonth(), getYear());
            list.add(day);
        }
        return list;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof Month)) {
            return super.equals(other);
        }
        Month om = (Month) other;
        return (om.getMonth().equals(getMonth()) && om.getYear().equals(getYear()));
    }
}
