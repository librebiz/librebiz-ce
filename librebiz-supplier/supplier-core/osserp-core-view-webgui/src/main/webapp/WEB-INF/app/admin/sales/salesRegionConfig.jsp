<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesRegionConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="headline">
		<fmt:message key="salesRegionConfigView"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
		<div class="subcolumns">
			<c:choose>
				<c:when test="${view.createMode}">
					<c:import url="_salesRegionConfigCreate.jsp"/>
				</c:when>
				<c:when test="${view.editMode}">
					<c:import url="_salesRegionConfigEdit.jsp"/>
				</c:when>
				<c:when test="${view.bean != null}">
					<c:import url="_salesRegionConfigDisplay.jsp"/>
				</c:when>
				<c:otherwise>
					<c:import url="_salesRegionConfigList.jsp"/>
				</c:otherwise>
			</c:choose>

			<div class="spacer"></div>
		</div>
	</tiles:put>
</tiles:insert>