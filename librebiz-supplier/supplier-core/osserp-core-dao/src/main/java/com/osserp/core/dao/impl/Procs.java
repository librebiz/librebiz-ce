/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06-Aug-2005 18:17:44 
 * 
 */
package com.osserp.core.dao.impl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class Procs extends Schema {

    /**
     * Creates a new plant output year <br/>
     * Params: <br/>
     * projectid bigint <br/>
     * yearid bigint
     */
    public static final String GET_EVENT_ESCALATIONS_WITH_INITIALS =
            "SELECT * FROM " + SCHEMA + "get_event_escalations_with_initials()" +
                    " ORDER BY src_rcp_name";

    /**
     * Creates a new plant output year <br/>
     * Params: <br/>
     * projectid bigint <br/>
     * yearid bigint
     */
    public static final String GET_REQUEST_ID_BY_OFFER =
            "SELECT " + SCHEMA + "get_request_id_by_offer(?)";

    /**
     * Provides customer values. <br/>
     * Params: <br/>
     * customerid bigint
     */
    public static final String GET_CUSTOMER =
            "SELECT * FROM " + SCHEMA + "get_customer_list_values(?)";

    /**
     * Cleans all terminated request reset warnings
     */
    public static final String EXECUTE_REQUEST_RESET_WARNING_TASKS =
            "SELECT * FROM " + SCHEMA + "execute_request_reset_warning_tasks()";

    /**
     * Restores canceled downpayment document, resets flags indicating canceled status
     */
    public static final String RESTORE_CANCELED_DOWNPAYMENT =
            "SELECT " + SCHEMA + "restore_sales_order_downpayment(?)";

    /**
     * Function gets user id related to given employee id Function param is employee id as bigint, returns bigint or sql null if not found
     */
    public static final String USER_BY_EMPLOYEE =
            "SELECT " + SCHEMA + "get_user_by_employee(?)";

    /**
     * Function gets employee id related to given user id Function param is employee id as bigint, returns bigint or sql null if not found
     */
    public static final String EMPLOYEE_BY_USER =
            "SELECT " + SCHEMA + "get_employee_by_user(?)";

    /**
     * Function gets employees email address Function param is employee id as bigint, returns email or empty string if none found
     */
    public static final String EMPLOYEE_EMAIL =
            "SELECT " + SCHEMA + "get_employee_email(?)";

    /**
     * Function provides the average capacity over all request related sales offers. Params: <br>
     * 'plantid BIGINT'
     */
    public static final String GET_REQUEST_CAPACITY =
            "SELECT " + SCHEMA + "get_request_capacity(?)";

    /**
     * Function moves closed events to event history and returns moved count
     */
    public static final String MOVE_CLOSED_EVENTS =
            "SELECT " + SCHEMA + "move_closed_events()";

    /**
     * Function restores moved closed events to events
     */
    public static final String RESTORE_CLOSED_EVENTS =
            "SELECT " + SCHEMA + "restore_closed_events()";

    /**
     * Function providing current purchase receipt count <br>
     * 'productid BIGINT'
     */
    public static final String GET_PRODUCT_PURCHASE_RECEIPT_COUNT =
            "SELECT " + SCHEMA + "get_purchase_receipt_product_count(?)";

    /**
     * Function providing current sales order count <br>
     * 'productid BIGINT'
     */
    public static final String GET_PRODUCT_SALES_ORDER_COUNT =
            "SELECT " + SCHEMA + "get_sales_ordered_product_count(?)";

    /**
     * Function providing current sales order unreleased count <br>
     * 'productid BIGINT'
     */
    public static final String GET_PRODUCT_SALES_ORDER_UNRELEASED_COUNT =
            "SELECT " + SCHEMA + "get_sales_ordered_unreleased_product_count(?)";

    /**
     * Provides the partner price by date, current if none found for this time <br>
     * 'productid BIGINT' <br>
     * 'date TIMESTAMP'
     */
    public static final String GET_PRODUCT_PARTNER_PRICE_AT_DATE =
            "SELECT " + SCHEMA + "get_partner_price_by_date(?,?)";

    /**
     * Provides the average purchase price <br>
     * 'stockid BIGINT' <br>
     * 'productid BIGINT' <br>
     * 'rcreated TIMESTAMP'
     */
    public static final String GET_PRODUCT_AVERAGE_PURCHASE_PRICE_AT_DATE =
            "SELECT " + SCHEMA + "get_average_purchase_price(?,?,?)";

    /**
     * Provides the current summary <br>
     * 'stockid BIGINT' <br>
     * 'productid BIGINT' <br>
     * 'recordCreated TIMESTAMP'
     */
    public static final String GET_PRODUCT_SUMMARY_BY_STOCK =
            "SELECT * FROM " + SCHEMA + "get_product_summary(?,?)";
}
