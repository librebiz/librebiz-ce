/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 2, 2007 9:43:53 AM 
 * 
 */
package com.osserp.common.service;

import java.util.Map;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TemplateManager {

    static final String TEMPLATE_NAME = "velocityTemplateName";
    static final String MAIL_ORIGINATOR = "velocityMailOriginator";
    static final String MAIL_SUBJECT = "velocityMailSubject";
    static final String MAIL_RECIPIENTS = "velocityMailRecipients";
    static final String TEMPLATE_SUFFIX = ".vm";

    static String createFilename(String templateName) {
        if (templateName != null &&
                templateName.indexOf(TemplateManager.TEMPLATE_SUFFIX) < 0) {
            return templateName + TEMPLATE_SUFFIX;
        }
        return templateName;
    }
    
    /**
     * Provides the configured template path. All stylesheet lookups
     * will be performed relative to this location. 
     * @return template path
     */
    String getTemplatePath();
    
    /**
     * Indicates if template with provided name exists.
     * @param templateName
     * @return true if template exists
     */
    boolean templateExists(String templateName);

    /**
     * Creates text by template and provided values
     * @param templateName name of the template to use.
     * @param values map with variable values of the sheet 
     * @return created text
     */
    String createText(String templateName, Map values);
}
