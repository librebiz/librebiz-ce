/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 21, 2005 10:09:36 AM 
 * 
 */
package com.osserp.gui.client.calc.impl;

import com.osserp.common.service.Locator;

import com.osserp.core.BusinessCase;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationManager;
import com.osserp.core.calc.Calculator;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DefaultCalculatorImpl extends AbstractCalculator implements Calculator {
    private static final long serialVersionUID = 42L;

    protected DefaultCalculatorImpl() {
        super();
    }

    /**
     * Provides a calculator for request based calculations
     * @param locator
     * @param user
     * @param calculation
     * @param businessCase
     * @param discountPermissions
     * @param overridePartnerPricePermissions
     */
    public DefaultCalculatorImpl(
            Locator locator,
            DomainUser user,
            Calculation calculation,
            BusinessCase businessCase,
            String[] discountPermissions,
            String[] overridePartnerPricePermissions) {
        super(locator, user, calculation, businessCase, discountPermissions, overridePartnerPricePermissions);
        if (businessCase.isSalesContext()) {
            getCalculationManager().lock(calculation, user);
        }
    }

    @Override
    protected final CalculationManager getCalculationManager() {
        return (CalculationManager) getService(CalculationManager.class.getName());
    }
}
