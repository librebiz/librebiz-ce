/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 7:31:05 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;
import java.util.Set;

import com.osserp.common.dao.Selects;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FcsActions extends Selects {

    /**
     * Loads an action from persistent storage
     * @param id
     * @return action
     */
    FcsAction load(Long id);

    /**
     * Provides an ordered action list of specified type or all actions if type is null
     * @param type
     * @return actions
     */
    List<FcsAction> findByType(Long type);

    /**
     * Tries to find an action either by id or name
     * @param type required type 
     * @param id action.id or null if name provided
     * @param name the name of the action if id not provided, an external status 
     * matching the type or null if not provided
     * @return action or null if not exists
     */
    FcsAction fetchAction(BusinessType type, Long id, String name);

    /**
     * Provides the closing status if defined for type 
     * @param type
     * @return closings
     */
    List<FcsClosing> findClosings(Long type);
    
    /**
     * Creates a new closing status if name not already exists
     * @param createdBy
     * @param name
     * @param description
     * @return new created closing or existing
     */
    FcsClosing createClosing(Long createdBy, String name, String description);

    /**
     * Persists a flow control action
     * @param action
     */
    void save(FcsAction action);

    /**
     * Provides fcs dependencies
     * @param action
     * @return deps
     */
    Set<Long> getDependencies(Long action);

    /**
     * Provides the wastebasket startups
     * @param type
     * @return wastebasketStartup
     */
    List<FcsAction> getWastebasketStartUp(BusinessType type);

    /**
     * Creates a flow control action list for add operations. The list contains all available flow control actions whose are not already added. If an actions
     * displayEverytime flag is set, the action is always added.
     * @param object
     * @return available flow control actions
     */
    List<FcsAction> createActionList(BusinessCase object);

    /**
     * Creates flow control actions for a new business type
     * @param user
     * @param source
     * @param target
     */
    void createActions(Employee user, BusinessType source, BusinessType target);

}
