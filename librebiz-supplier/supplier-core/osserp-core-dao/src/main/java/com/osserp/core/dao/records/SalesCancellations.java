/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13-Jun-2006 11:49:28 
 * 
 */
package com.osserp.core.dao.records;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.sales.Sales;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesCancellations extends PaymentAwareRecords, Records {

    /**
     * Creates a new cancellation
     * @param user
     * @param record to cancel
     * @param customId
     * @return cancellation created
     * @throws ClientException
     */
    Cancellation create(Employee user, CancellableRecord record, Date date, Long customId) throws ClientException;

    /**
     * Updates the branch office by sales
     * @param sales
     */
    void changeBranch(Sales sales);

    /**
     * Loads cancellation by cancelled record
     * @param record
     * @return record or null if not exists
     */
    Cancellation loadCancellation(CancellableRecord record);

}
