<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
	<table class="table table-striped">
		<thead>
			<tr>
				<th style="width: 15%;"><fmt:message key="productType"/></th>
				<th style="width: 15%;"><fmt:message key="productGroup"/></th>
				<th style="width: 15%;"><fmt:message key="category"/></th>
				<th style="width: 15%;"><fmt:message key="productName"/></th>
				<th style="width: 30%;" class="action center"><fmt:message key="inverted"/></th>
                <th style="width: 5%;" class="action center">
                    <v:link url="/productSelectionConfigProductSearch.do?method=forward" title="addProducts">
                        <o:img name="openFolderIcon" styleClass="smallIcon"/>
                    </v:link>
                </th>
                <th style="width: 5%;" class="action center">
                    <v:ajaxLink url="/selections/productClassificationSelection/forward?showStock=false&selectionTarget=/selections/productSelectionConfigsSelection/addSelection" linkId="productClassificationSelection" title="addConfiguration">
                        <o:img name="newIcon" styleClass="smallIcon"/>
                    </v:ajaxLink>
                </th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty view.bean.items}">
					<tr>
						<td colspan="7"><fmt:message key="noConfigurationAvailable"/></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="obj" items="${view.bean.items}">
						<tr>
							<td style="width: 15%;"><c:choose><c:when test="${empty obj.type}">-</c:when><c:otherwise><o:out value="${obj.type.name}"/></c:otherwise></c:choose></td>
							<td style="width: 15%;"><c:choose><c:when test="${empty obj.group}">-</c:when><c:otherwise><o:out value="${obj.group.name}"/></c:otherwise></c:choose></td>
							<td style="width: 15%;"><c:choose><c:when test="${empty obj.category}">-</c:when><c:otherwise><o:out value="${obj.category.name}"/></c:otherwise></c:choose></td>
							<td style="width: 15%;">
								<c:choose>
									<c:when test="${empty obj.product}">-</c:when>
									<c:otherwise>
										<v:link url="/products.do" parameters="method=load&id=${obj.product.productId}&exit=productSelectionConfigsSelection" title="${obj.product.productId} - ${obj.product.name}">
											<o:out value="${obj.product.name}" limit="28"/>
										</v:link>
									</c:otherwise>
								</c:choose>
							</td>
							<td style="width: 30%;" class="center">
								<c:choose>
									<c:when test="${obj.exclusion}">
										<v:link url="/selections/productSelectionConfigsSelection/toggleExclusion" parameters="id=${obj.id}" title="productSelectionConfigItemInvertRevoke"><fmt:message key="yes"/></v:link>
									</c:when>
									<c:otherwise>
										<v:link url="/selections/productSelectionConfigsSelection/toggleExclusion" parameters="id=${obj.id}" title="productSelectionConfigItemInvert"><fmt:message key="no"/></v:link>
									</c:otherwise>
								</c:choose>
							</td>
							<td colspan="2" style="width: 10%;" class="center">
								<v:link url="/selections/productSelectionConfigsSelection/removeSelection" parameters="id=${obj.id}" title="removeFromConfiguration"><o:img name="deleteIcon" styleClass="bigIcon"/></v:link>
							</td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>
