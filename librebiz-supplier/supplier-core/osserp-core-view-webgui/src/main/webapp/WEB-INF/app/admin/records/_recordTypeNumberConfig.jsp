<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-app" prefix="oa" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${empty view.selectedNumberConfig}">
                        <fmt:message key="numberRangeConfig" /> 
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="numberRangeAssign" /> 
                    </c:otherwise>
                </c:choose>
                - <o:out value="${view.bean.name}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <c:choose>
                <c:when test="${!empty view.selectedNumberConfig}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="shortkey"><fmt:message key="${view.selectedNumberConfig.name}" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <fmt:message key="${view.selectedNumberConfig.value}" />
                            </div>
                        </div>
                    </div>
                    <c:choose>
                        <c:when test="${empty view.numberConfig.lowestNumber}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="current"><fmt:message key="currentLabel" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <fmt:message key="noRecordsAvailable" />
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="current"><fmt:message key="currentLabel" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <o:out value="${view.numberConfig.lowestNumber}"/>
                                        - <o:out value="${view.numberConfig.highestNumber}"/>
                                    </div>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${view.selectedNumberConfig.name == 'dateSequence'}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="numberRange"><fmt:message key="numberRange" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <fmt:message key="date" />
                                        +  <v:text name="value" value="${view.bean.uniqueNumberDigitCount}" style="width:35px; text-align:right;"/>
                                        <fmt:message key="digits" />
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${view.selectedNumberConfig.name == 'yearlySequence'}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="numberRange"><fmt:message key="numberRange" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <fmt:message key="year" />
                                        +  <v:text name="value" value="${view.numberConfig.digits}" style="width:35px; text-align:right;"/>
                                        <fmt:message key="digits" />
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${!empty view.numberConfig.defaultNumber}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="current"><fmt:message key="defaultSetting" /></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <o:out value="${view.numberConfig.defaultNumber}"/> 
                                            <fmt:message key="onMissingInput"/> <fmt:message key="numberRange" />
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="numberRange"><fmt:message key="numberRange" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <fmt:message key="initOrRestartWith" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <v:text name="value" value="${view.numberSuggestion}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                    <div class="row next">
                        <div class="col-md-4"> </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v:submitExit url="${view.baseLink}/selectNumberConfig"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <o:submit value="save" styleClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <c:forEach var="method" items="${view.numberConfigs}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${view.bean.numberCreatorName == method.name}">
                                            <label for="name"><strong><fmt:message key="${method.name}" /></strong></label>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="/admin/records/recordTypeConfig/selectNumberConfig?name=${method.name}">
                                                <label for="name"><fmt:message key="${method.name}" /></label>
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <fmt:message key="${method.value}" />
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </c:otherwise>
            </c:choose>

        </div>
    </div>
</div>

<c:if test="${!empty view.latestRecords}">
<c:set var="latestRecords" scope="request" value="${view.latestRecords}"/>
<c:set var="latestRecordsExit" scope="request" value="recordTypeConfig"/>
<c:set var="latestRecordsViewExit" scope="request" value="/admin/records/recordTypeConfig/reload"/>
<c:import url="${viewdir}/admin/records/_latestRecordList.jsp" />
</c:if>
