/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 31, 2011 3:06:54 PM 
 * 
 */
package com.osserp.gui.client.calc.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.SearchRequest;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.CalculationConfigManager;
import com.osserp.core.calc.CalculationTemplate;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSearch;
import com.osserp.core.products.ProductSearchRequest;

import com.osserp.gui.client.calc.CalculationTemplateProductSearchView;
import com.osserp.gui.client.products.impl.AbstractProductSearchView;

/**
 * 
 * @author jg <jg@osserp.com>
 */
@ViewName("calculationTemplateProductSearchView")
public class CalculationTemplateProductSearchViewImpl extends AbstractProductSearchView implements CalculationTemplateProductSearchView {
    private static Logger log = LoggerFactory.getLogger(CalculationTemplateProductSearchViewImpl.class.getName());

    private CalculationConfigGroup selectedConfigGroup = null;
    private Long selectedItemId = null;
    private CalculationTemplate calculationTemplate;
    private Set<Long> redunancyIgnorables = new HashSet();

    protected CalculationTemplateProductSearchViewImpl() {
        super();
    }

    protected CalculationTemplateProductSearchViewImpl(Set<String> methods, String defaultMethod) {
        super(methods, defaultMethod, false);
        setKeepViewWhenExistsOnCreate(false);
        setIncludeEolAvailable(false);
    }

    public CalculationConfigGroup getSelectedConfigGroup() {
        return selectedConfigGroup;
    }

    public Long getSelectedItemId() {
        return selectedItemId;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        setProductSelectionConfigsAvailable(false);

        calculationTemplate = null;
        Object obj = request.getSession().getAttribute("calculationTemplate");
        
        if (obj instanceof CalculationTemplate) {
            calculationTemplate = (CalculationTemplate) obj;
        }

        if (calculationTemplate != null) {
            selectedItemId = RequestUtil.fetchLong(request, "selectedItemId");
            Long selectedGroup = RequestUtil.getLong(request, "selectedGroup");
            boolean ignorePreselection = RequestUtil.getBoolean(request, "ignorePreselection");

            redunancyIgnorables = fetchIgnorables();
            CalculationConfigManager manager = getCalculationConfigManager();
            selectedConfigGroup = manager.findGroup(selectedGroup);

            if (selectedConfigGroup != null
                    && !ignorePreselection
                    && !selectedConfigGroup.getProductSelections().isEmpty()) {

                setProductSelectionConfigs(selectedConfigGroup.getProductSelections());
                setProductSelectionConfigsAvailable(true);

                ProductSearch search = (ProductSearch) getService(ProductSearch.class.getName());
                List<Product> list = search.find(
                        new ProductSearchRequest(
                                getContactReference(),
                                isContactReferenceCustomer(),
                                isIncludeEol(),
                                SearchRequest.IGNORE_FETCHSIZE),
                        getProductSelectionConfigs());
                setList(list);
                if (!list.isEmpty()) {
                    setProductSelectionConfigsMode(true);
                }
            }
        } else {
            log.warn("init() calculationTemplate not bound!");
        }
    }

    @Override
    protected void setList(List list) {
        if (calculationTemplate != null) {
            for (Iterator<Product> i = list.iterator(); i.hasNext();) {
                Product next = i.next();
                if (calculationTemplate.isAlreadyAdded(next)
                        && !redunancyIgnorables.contains(next.getProductId())) {

                    if (log.isDebugEnabled()) {
                        log.debug("setList() removed already added product [id="
                                + next.getProductId() + "]");
                    }
                    i.remove();
                }
            }
        }
        super.setList(list);
    }

    private Set<Long> fetchIgnorables() {
        Set<Long> igns = new java.util.HashSet();
        try {
            CalculationConfigManager manager = getCalculationConfigManager();
            CalculationConfig cfg = manager.getConfig(calculationTemplate.getBusinessType().getCalculationConfig());
            for (int i = 0, j = cfg.getAllowedRedundancies().size(); i < j; i++) {
                Product next = cfg.getAllowedRedundancies().get(i);
                igns.add(next.getProductId());
            }
        } catch (Exception e) {
            log.warn("fetchIgnorables() ignoring exception on attempt to get products [message="
                    + e.getMessage() + "]");
        }
        if (log.isDebugEnabled() && !igns.isEmpty()) {
            log.debug("fetchIgnorables() added redundancy check ignorables [count=" + igns.size() + "]");
        }
        return igns;
    }

    private CalculationConfigManager getCalculationConfigManager() {
        return (CalculationConfigManager) getService(CalculationConfigManager.class.getName());
    }

}
