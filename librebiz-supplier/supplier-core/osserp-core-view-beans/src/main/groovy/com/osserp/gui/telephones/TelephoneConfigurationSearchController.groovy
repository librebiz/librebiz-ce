/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import com.osserp.common.ClientException
import com.osserp.common.PermissionException
import com.osserp.groovy.web.ViewController
import javax.servlet.http.HttpServletRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

/**
 * Search for telephone configurations
 * 
 * @author so <so@osserp.com>
 * 
 */
@RequestMapping("/telephones/telephoneConfigurationSearch/*")
@Controller class TelephoneConfigurationSearchController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(TelephoneConfigurationSearchController.class.getName())

    @RequestMapping
    def search(HttpServletRequest request) {
        logger.debug('search invoked...')
        TelephoneConfigurationSearchView view = getView(request)
        try {
            view?.search()
        } catch (PermissionException t) {
            if (logger.debugEnabled) {
                logger.debug("search: caught exception [message=${t.message}]")
            }
            request.getSession().setAttribute('error', t.message)
        }
        return pageArea
    }

    @RequestMapping
    def findTelephoneExchangesByTelephoneSystem(HttpServletRequest request) {
        logger.debug('findTelephoneExchangesByTelephoneSystem invoked...')
        TelephoneConfigurationSearchView view = getView(request)
        try {
            view?.findTelephoneExchangesByTelephoneSystem()
        } catch (PermissionException t) {
            if (logger.debugEnabled) {
                logger.debug("findTelephoneExchangesByTelephoneSystem: caught exception [message=${t.message}]")
            }
            request.getSession().setAttribute('error', t.message)
        }
        return defaultPage
    }

    @RequestMapping
    @Override
    def sort(HttpServletRequest request) {
        logger.debug("sort: invoked [user=${getUserId(request)}]")
        TelephoneConfigurationSearchView view = getView(request)
        view?.sort()
        return pageArea
    }
}
