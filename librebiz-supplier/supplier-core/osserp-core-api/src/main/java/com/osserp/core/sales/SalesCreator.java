/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 27 Oct 2011 17:38:41 
 * 
 */
package com.osserp.core.sales;

import java.util.Date;

import com.osserp.common.ClientException;

import com.osserp.core.BusinessType;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesCreator {

    /**
     * Creates a new sales
     * @param user
     * @param request
     * @param selectedOffer
     * @param dateConfirmed
     * @param note
     * @return new created sales
     * @throws ClientException if request validation failed
     */
    Sales create(DomainUser user, Request request, Long selectedOffer, Date dateConfirmed, String note) throws ClientException;

    /**
     * Creates a new sales related to other sales
     * @param user
     * @param salesPerson
     * @param related
     * @param type
     * @param name
     * @return
     * @throws ClientException
     */
    Sales create(DomainUser user, Employee salesPerson, Sales related, BusinessType type, String name) throws ClientException;

}
