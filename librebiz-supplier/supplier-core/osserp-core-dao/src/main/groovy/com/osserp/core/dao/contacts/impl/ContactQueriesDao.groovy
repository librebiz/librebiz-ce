/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao.contacts.impl

import groovy.sql.Sql

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.PhoneUtil
import com.osserp.common.util.StringUtil

import com.osserp.core.contacts.ContactSearchResult
import com.osserp.core.contacts.ContactType
import com.osserp.core.contacts.Salutation
import com.osserp.core.dao.ContactQueries

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ContactQueriesDao extends AbstractContactQueriesDao implements ContactQueries {
    private static Logger logger = LoggerFactory.getLogger(ContactQueriesDao.class.getName())
    
    static final Integer MAX_FETCH_SIZE = 250


    private static String VALUES = '''
		id,
		contact_id as contactId,
		contact_ref as parentContact,
		type_id as type,
		salutation,
		lastname,
		firstname,
		street,
		zipcode,
		city,
		parent,
		parentstreet as parentStreet,
		parentzipcode as parentZipcode,
		parentcity as parentCity,
		email,
		pe as primaryEmail,
		e as employee,
		c as customer,
		s as supplier,
		i as installer,
		cc as client,
		b as branch,
		o as other,
		idx'''

    private static String PHONE_VALUES = '''
		device_id as phoneDeviceId,
		phone_type_id as phoneTypeId,
		country as phoneCountry,
		prefix as phonePrefix,
		number as phoneNumber,
		isprimary as phonePrimary,
		phone_key as phoneKey,
		phone_display as phoneDisplay,
		0 as customertype,
		0 as customerstatus'''

    private static String SELECT = "SELECT $VALUES"
    private static String SELECT_PHONE = "SELECT phone_id as phoneId, $VALUES, $PHONE_VALUES"
    private static String SEARCH_PREFIX = 'v_'
    private static String SEARCH_POSTFIX = '_search'

    List<ContactSearchResult> find(String context, String column, Long type, String value, boolean startsWith) {
        logger.debug("find: invoked [context=$context, column=$column, type=$type, value=$value]")
        String ctx = context ?: 'contact'
        def filterResult = true
        boolean whereAdded = false
        def query = ('customer' == ctx
                ? "$SELECT, customertype, customerstatus FROM $SEARCH_PREFIX$ctx$SEARCH_POSTFIX"
                : "$SELECT, 0 as customertype, 0 as customerstatus FROM $SEARCH_PREFIX$ctx$SEARCH_POSTFIX")
        
        if (type) {
            query = "$query WHERE type_id = $type"
            whereAdded = true
        }
        String input = value?.trim() ? value : ''
        if (input) {
            if (column && column != 'address') {
                if (whereAdded) {
                    query = "$query AND $column ~* ${getInput(input, startsWith)}"
                } else {
                    query = "$query WHERE $column ~* ${getInput(input, startsWith)}"
                    whereAdded = true
                }
                if (column == 'email') {
                    filterResult = false
                }
            } else {
                String[] vals = StringUtil.getTokenArray(value, " ");
                if (whereAdded) {
                    query = "$query AND idx ~* "
                } else {
                    query = "$query WHERE idx ~* "
                    whereAdded = true
                }
                if (vals.length == 1) {
                    query = "$query ${getInput(input, startsWith)}"
                } else {
                    StringBuilder buffer = new StringBuilder("$query ${getInput(vals[0], false)}".toString())
                    for (int i = 2; i <= vals.length; i++) {
                        buffer.append(" AND idx ~* ").append(getInput(vals[i-1], false))
                    }
                    query = buffer.toString()
                }
            }
            if (input.length() < 3) {
                query = "$query LIMIT ${MAX_FETCH_SIZE}"
            }
        } else {
            // limit number of search results of unspecified queries
            query = "$query LIMIT ${MAX_FETCH_SIZE}"
        }
    
        logger.debug("find: query created [sql=$query]")
        List list = []
        Map map = [:]
        Sql sql = getSql()
        sql.eachRow(query.toString()) { obj ->
            def contact = createResult(obj)
            if (filterResult) {
                if (map[contact.id] && contact.email) {
                    map[contact.id].emails << contact.email
                    if (contact.primaryEmail) {
                        map[contact.id].emails << map[contact.id].email
                        map[contact.id].email = contact.email
                    }
                } else {
                    list << contact
                    map[contact.id] = contact
                }
            } else {
                list << contact
            }
        }
        return list
    }
    
    protected getInput(String input, boolean startsWith) {
        String escaped = fetchInsecureInput(input)
        return startsWith ? "'^$escaped'" : "'$escaped'"
    }

    List<ContactSearchResult> findFirstRows(String context) {
        logger.debug("findFirstRows: invoked [context=${context}]")
        String ctx = context ?: 'contact'
        def filterResult = true
        def query = ('customer' == ctx
                ? "$SELECT, customertype, customerstatus FROM $SEARCH_PREFIX$ctx$SEARCH_POSTFIX"
                : "$SELECT, 0 as customertype, 0 as customerstatus FROM $SEARCH_PREFIX$ctx$SEARCH_POSTFIX")
        query = "$query ORDER BY lastname LIMIT ${MAX_FETCH_SIZE}"
        logger.debug("find: query created [sql=$query]")
        List list = []
        Map map = [:]
        Sql sql = getSql()
        sql.eachRow(query.toString()) { obj ->
            def contact = createResult(obj)
            if (filterResult) {
                if (map[contact.id] && contact.email) {
                    map[contact.id].emails << contact.email
                    if (contact.primaryEmail) {
                        map[contact.id].emails << map[contact.id].email
                        map[contact.id].email = contact.email
                    }
                } else {
                    list << contact
                    map[contact.id] = contact
                }
            } else {
                list << contact
            }
        }
        return list
    }

    List<ContactSearchResult> findPhoneNumbers(String number) {
        List list = []
        String value = fetchInsecureInput(PhoneUtil.clearPhoneNumber(number, true))
        if (value) {
            def query = "$SELECT_PHONE FROM v_contact_phones WHERE phone_display ~ '$value' OR phone_key ~ '$value' ORDER BY phone_display"
            logger.debug("findPhoneNumbers: invoked [number=$number, query=$query]")
            Sql sql = getSql()
            Set<Long> added = new HashSet<Long>()
            sql.eachRow(query.toString()) { obj ->
                def contact = createResult(obj)
                addPhone(contact, obj)
                if (!added.contains(contact.phoneId)) {
                    list << contact
                    added << contact.phoneId
                }
            }
        }
        return list
    }

    ContactSearchResult findNameByContactId(Long contactId) {
        def contact
        if (contactId) {
            def query = "SELECT lastname, firstname FROM contacts WHERE contact_id = '$contactId'"
            Sql sql = getSql()
            def row = sql.firstRow(query.toString())
            if (row) {
                contact = new ContactSearchResult(
                        lastname: row.lastname,
                        firstname: row.firstname)
            }
        }
        return contact
    }

    private ContactSearchResult createResult(def obj) {
        ContactSearchResult result = new ContactSearchResult(
                id: obj.id,
                contactId: obj.contactId,
                parentContact: obj.parentContact,
                lastname: obj.lastname,
                firstname: obj.firstname,
                street: obj.street,
                zipcode: obj.zipcode,
                city: obj.city,
                parent: obj.parent,
                parentStreet: obj.parentStreet,
                parentZipcode: obj.parentZipcode,
                parentCity: obj.parentCity,
                email: obj.email,
                primaryEmail: obj.primaryEmail,
                employee: obj.employee,
                customer: obj.customer,
                supplier: obj.supplier,
                installer: obj.installer,
                client: obj.client,
                branch: obj.branch,
                other: obj.other,
                idx: obj.idx,
                customerType: obj.customertype,
                customerStatus: obj.customerstatus
                )
          ContactType type = fetchContactType(obj.type)
          if (type) {
              result.type = type
          }
          Salutation salutation = fetchSalutation(obj.salutation)
          if (salutation) {
              result.salutation = salutation
          }
          return result
    }

    private void addPhone(ContactSearchResult result, def obj) {
        result.phoneId = obj.phoneId
        result.phoneDeviceId = obj.phoneDeviceId
        result.phoneTypeId = obj.phoneTypeId
        result.phoneCountry = obj.phoneCountry
        result.phonePrefix = obj.phonePrefix
        result.phoneNumber = obj.phoneNumber
        result.phonePrimary = obj.phonePrimary
        result.phoneKey = obj.phoneKey
        result.phoneDisplay = obj.phoneDisplay
    }
}
