/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 5, 2015 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationTemplate;
import com.osserp.core.calc.CalculationTemplateItem;
import com.osserp.core.calc.CalculationTemplateService;
import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.ProductSelections;
import com.osserp.core.dao.CalculationTemplates;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationTemplateServiceImpl implements CalculationTemplateService {
    private ProductPrices productPrices;
    private ProductSelections productSelections = null;
    private CalculationTemplates calculationTemplates;

    public CalculationTemplateServiceImpl(
            CalculationTemplates calculationTemplates, 
            ProductPrices productPrices, 
            ProductSelections productSelections) {
        this.calculationTemplates = calculationTemplates;
        this.productPrices = productPrices;
        this.productSelections = productSelections;
    }

    public boolean isTemplateAvailable(Long productId, Long businessTypeId) {
        return this.calculationTemplates.isTemplateAvailable(productId, businessTypeId);
    }

    public CalculationTemplate findByProductAndType(Calculation calculation, Long productId, Long businessTypeId) {
        CalculationTemplate template = calculationTemplates.findByProductAndType(productId, businessTypeId);
        if (template != null) {
            List<CalculationTemplateItem> items = template.getItems();
            List<ProductSelectionConfigItem> partnerPriceEditables =
                    productSelections.findByEditablePartnerPrice();
            for (int i = 0; i < items.size(); i++) {
                CalculationTemplateItem calculationTemplateItem = items.get(i);
                calculationTemplateItem.update(
                        calculation.getInitialCreated(),
                        productPrices.getPartnerPrice(
                                calculationTemplateItem.getProduct(),
                                calculation.getInitialCreated(),
                                Double.valueOf(1.0D)),
                        ProductUtil.isPartnerPriceEditable(
                                partnerPriceEditables,
                                calculationTemplateItem.getProduct()),
                        productPrices.getPurchasePrice(
                                calculationTemplateItem.getProduct(),
                                calculationTemplateItem.getProduct().getDefaultStock(),
                                calculation.getInitialCreated()));
            }
        }
        return template;
    }

}
