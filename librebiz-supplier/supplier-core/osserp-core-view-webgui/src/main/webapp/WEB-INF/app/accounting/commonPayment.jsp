<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="commonPaymentView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="styles" type="string">
  		<style type="text/css">

        #amount {
            width: 100px;
        }
        
        #note {
            width: 200px;
        }

        .table .recordAmount {
            width: 80px;
            text-align: right;
        }

        .table .recordDate {
            width: 65px;
        }

        .table .recordName {
            width: 220px;
        }

        .table .recordNote {
            width: 200px;
        }

        .table .recordType {
            width: 130px;
        }

        .table .recordAction {
            width: 45px;
            text-align: center;
        }

        .table .icon {
            margin-left: 5px;
            text-align: center;
        }
        
        td p {
            padding-bottom: 8px;
        }       
        </style>
	</tiles:put>
	<tiles:put name="headline">
        <fmt:message key="booking"/> <o:out value="${view.bean.type.billingClass.name}"/> 
        - <o:out value="${view.bean.contact.displayName}" />
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="commonPaymentContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.editMode}">
                        <v:form name="accountTransactionForm" url="/accounting/commonPayment/save">
                            <c:import url="_commonPaymentEdit.jsp"/>
                        </v:form>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_commonPaymentDisplay.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
