<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="businessTypePaymentDefaultsView" />
<c:set var="payment" scope="page" value="${view.bean}" />

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="${view.headerName}" />
    </div>
    <div class="modalBoxHeaderRight">
        <div class="boxnav">
            <ul>
                <c:forEach var="nav" items="${view.navigation}">
                    <li><v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}">
                            <o:img name="${nav.icon}" />
                        </v:ajaxLink></li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>
<div class="modalBoxData">

    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <c:choose>
                <c:when test="${view.editMode}">
                    <c:set var="url">
                        <v:url value="/admin/projects/businessTypePaymentDefaults/selectPercentage" />
                    </c:set>
                    <v:ajaxForm name="businessTypePaymentDefaultsForm" targetElement="${view.name}_popup" url="/admin/projects/businessTypePaymentDefaults/save">
                        <table class="valueTable">
                            <tr>
                                <td><fmt:message key="advancePaymentOne" /></td>
                                <td style="text-align: right; padding-right: 20px;"><oc:selectPercentage name="downpaymentPercent" value="${payment.downpaymentPercent}" options="billingPercentages" style="width: 80px; text-align:right;" onchange="ojsAjax.ajaxForm('${url}', '${view.name}_popup', true, $('businessTypePaymentDefaultsForm'), null, null, null, null)" prompt="false" /></td>
                                <td><oc:select name="downpaymentTargetId" value="${payment.downpaymentTargetId}" options="recordPaymentTargets" /></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="advancePaymentTwo" /></td>
                                <td style="text-align: right; padding-right: 20px;"><oc:selectPercentage name="deliveryInvoicePercent" value="${payment.deliveryInvoicePercent}" options="billingPercentages" style="width: 80px; text-align:right;" onchange="ojsAjax.ajaxForm('${url}', '${view.name}_popup', true, $('businessTypePaymentDefaultsForm'), null, null, null, null)" prompt="false" /></td>
                                <td><oc:select name="deliveryInvoiceTargetId" value="${payment.deliveryInvoiceTargetId}" options="recordPaymentTargets" prompt="false" /></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="finalInvoice" /></td>
                                <td style="text-align: right; padding-right: 20px;"><o:number value="${payment.finalInvoicePercent}" format="percent" />%</td>
                                <td><oc:select name="finalInvoiceTargetId" value="${payment.finalInvoiceTargetId}" options="recordPaymentTargets" prompt="false" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: right; padding-right: 20px;">
                                    <input type="checkbox" name="hidePayments" <c:if test="${payment.hidePayments}">checked="checked"</c:if>/>
                                </td>
                                <td><span><fmt:message key="hidePaymentStepsDisplayHint" /></span></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="payment" /></td>
                                <td></td>
                                <td><oc:select name="paymentConditionId" value="${payment.paymentCondition}" options="recordPaymentConditions" prompt="false" /></td>
                            </tr>
                            <tr>
                                <td class="row-submit"><v:ajaxLink url="${view.disableEditLink.link}" linkId="disableEditLink" targetElement="${view.name}_popup">
                                        <input class="cancel" type="button" value="<fmt:message key="exit"/>" />
                                    </v:ajaxLink></td>
                                <td class="row-submit"><o:submit /></td>
                            </tr>
                        </table>
                    </v:ajaxForm>
                </c:when>
                <c:otherwise>
                    <table class="valueTable">
                        <tr>
                            <td><fmt:message key="advancePaymentOne" /></td>
                            <td><c:if test="${!empty payment.downpaymentPercent}">
                                    <fmt:formatNumber value="${payment.downpaymentPercent}" type="percent" />
                                    <oc:options name="recordPaymentTargets" value="${payment.downpaymentTargetId}" />
                                </c:if></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="advancePaymentTwo" /></td>
                            <td><c:if test="${!empty payment.deliveryInvoicePercent}">
                                    <fmt:formatNumber value="${payment.deliveryInvoicePercent}" type="percent" />
                                    <oc:options name="recordPaymentTargets" value="${payment.deliveryInvoiceTargetId}" />
                                </c:if></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="finalInvoice" /></td>
                            <td><c:if test="${!empty payment.finalInvoicePercent}">
                                    <fmt:formatNumber value="${payment.finalInvoicePercent}" type="percent" />
                                    <oc:options name="recordPaymentTargets" value="${payment.finalInvoiceTargetId}" />
                                </c:if></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2"><fmt:message key="hidePaymentStepsDisplayHint" />:
                                <span style="padding-left:5px;">
                                    <c:choose>
                                        <c:when test="${payment.hidePayments}"><fmt:message key="bigYes" /></c:when>
                                        <c:otherwise><fmt:message key="bigNo" /></c:otherwise>
                                    </c:choose>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><fmt:message key="payment" /></td>
                            <td><o:out value="${payment.paymentCondition.name}" /></td>
                        </tr>
                    </table>
                </c:otherwise>
            </c:choose>

            <div class="spacer"></div>
        </div>
    </div>
</div>
