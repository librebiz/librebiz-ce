/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 30, 2006 2:28:52 PM 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordPrintManager {

    /**
     * Creates pdf representation of a record and updates status if required.
     * @param employee
     * @param record
     * @param status
     * @return pdf document data
     * @throws ClientException
     */
    DocumentData createPdf(
            Employee employee,
            Record record,
            Long status) throws ClientException;

    /**
     * Creates a correction pdf of a record
     * @param employee
     * @param record
     * @param status
     * @return correction pdf document data 
     * @throws ClientException if validation of values failed
     */
    DocumentData createCorrectionPdf(
            Employee employee,
            PaymentAwareRecord record,
            Long status) throws ClientException;

    /**
     * Creates a pdf with custom header and date
     * @param employee
     * @param record
     * @param preview
     * @param headerName
     * @param date
     * @return pdf document data
     * @throws ClientException if validation failed
     */
    DocumentData createCustomPdf(
            Employee employee,
            Record record,
            boolean preview,
            String headerName,
            Date date) throws ClientException;

    /**
     * Fetches an existing pdf of a record correction
     * @param correction
     * @return correction pdf document data
     * @throws ClientException if no pdf available
     */
    DocumentData getCorrectionPdf(RecordCorrection correction) throws ClientException;

    /**
     * Provides a record pdf
     * @param Record record
     * @return document
     */
    DocumentData getPdf(Record record);

    /**
     * Removes all referenced documents. Use with care!
     * @param record
     */
    void archiveCleanup(Record record);

}
