/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 20, 2008 8:14:44 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.RecordComparators;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.RecordDisplayVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordDisplayRowMapper extends BaseRecordDisplayRowMapper {

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new RecordDisplayRowMapper());
    }

    protected static final String RECORD_VALUES =
            "id" +
                    ",type_id" +
                    ",company_id" +
                    ",reference_id" +
                    ",contact_id" +
                    ",contact_type" +
                    ",status" +
                    ",created" +
                    ",created_by" +
                    ",signature_left" +
                    ",signature_right" +
                    ",base_tax" +
                    ",reduced_tax" +
                    ",tax_free" +
                    ",tax_free_id" +
                    ",net_amount" +
                    ",tax_amount" +
                    ",reduced_tax_amount" +
                    ",gross_amount" +
                    ",currency_id" +
                    ",internal" +
                    ",maturity" +
                    ",prorate_tax" +
                    ",net_prorated_amount" +
                    ",tax_prorated_amount" +
                    ",reduced_prorated_tax_amount" +
                    ",gross_prorated_amount" +
                    ",lastname" +
                    ",firstname" +
                    ",street" +
                    ",zipcode" +
                    ",city" +
                    ",product_id" +
                    ",product_name" +
                    ",custom_name" +
                    ",product_type" +
                    ",product_group" +
                    ",product_category" +
                    ",has_stock" +
                    ",is_virtual" +
                    ",is_plant" +
                    ",is_bundle" +
                    ",is_service" +
                    ",item_id" +
                    ",quantity" +
                    ",price" +
                    ",include_price" +
                    ",stock_id" +
                    ",delivered" +
                    ",delivery" +
                    ",delivery_ack" +
                    ",sales_id" +
                    ",sub_type" +
                    ",sent" +
                    ",external_item_id" +
                    ",paid_amount";

    @Override
    protected void populate(ResultSet rs, Map<String, Object> map) {
        addLong(map, "signatureLeft", rs, "signature_left");
        addLong(map, "signatureRight", rs, "signature_right");
        addDouble(map, "baseTax", rs, "base_tax");
        addDouble(map, "reducedTax", rs, "reduced_tax");
        addLong(map, "productId", rs, "product_id");
        addString(map, "productName", rs, "product_name");
        addLong(map, "productType", rs, "product_type");
        addLong(map, "productTypeCount", rs, "type_count");
        addLong(map, "productGroup", rs, "product_group");
        addLong(map, "productCategory", rs, "product_category");
        addBoolean(map, "stockAware", rs, "has_stock");
        addBoolean(map, "virtual", rs, "is_virtual");
        addBoolean(map, "plant", rs, "is_plant");
        addBoolean(map, "bundle", rs, "is_bundle");
        addBoolean(map, "service", rs, "is_service");
        addLong(map, "itemId", rs, "item_id");
        addDouble(map, "quantity", rs, "quantity");
        addDouble(map, "price", rs, "price");
        addBoolean(map, "includePrice", rs, "include_price");
        addLong(map, "stockId", rs, "stock_id");
        addDate(map, "delivery", rs, "delivery");
        addDouble(map, "delivered", rs, "delivered");
        addBoolean(map, "deliveryConfirmed", rs, "delivery_ack");
        addLong(map, "subType", rs, "sub_type");
        addDate(map, "dateSent", rs, "sent");
        addDate(map, "maturity", rs, "maturity");
        addLong(map, "externalItemId", rs, "external_item_id");
        addDouble(map, "prorateTax", rs, "prorate_tax");
        addDouble(map, "prorateNetAmount", rs, "net_prorated_amount");
        addDouble(map, "prorateTaxAmount", rs, "tax_prorated_amount");
        addDouble(map, "prorateReducedTaxAmount", rs, "reduced_prorated_tax_amount");
        addDouble(map, "prorateGrossAmount", rs, "gross_prorated_amount");
        addDouble(map, "paidAmount", rs, "paid_amount");
    }

    public final static List<RecordDisplay> createDisplayList(
            OptionsCache optionsCache, 
            List<Map<String, Object>> values) {
        Map<Long, RecordDisplay> result = new HashMap<>();
        int size = values.size();
        for (int i = 0; i < size; i++) {
            Map<String, Object> nextMap = values.get(i);
            Long id = (Long) nextMap.get("id");
            if (id != null) {
                if (result.containsKey(id)) {
                    RecordDisplay recordObj = result.get(id);
                    recordObj.addItem(nextMap);
                } else {
                    Long type = (Long) nextMap.get("type");
                    RecordType recordType = null;
                    if (type != null) {
                        recordType = RecordProcs.getRecordType(
                                optionsCache, type);
                        if (recordType != null) {
                            nextMap.put("recordType", recordType);
                        }
                    }
                    Long bookingType = (Long) nextMap.get("subType");
                    if (recordType != null && bookingType != null) {
                        BookingType bt = RecordProcs.getBookingType(
                                optionsCache, recordType, bookingType);
                        if (bt != null) {
                            nextMap.put("bookingType", bt);
                        }
                    }
                    result.put(id, new RecordDisplayVO(nextMap));
                }
            }
        }
        List<RecordDisplay> finalResult = new ArrayList(result.values());
        return CollectionUtil.sort(finalResult, RecordComparators.createRecordDisplayByCreatedComparator(true));
    }
}
