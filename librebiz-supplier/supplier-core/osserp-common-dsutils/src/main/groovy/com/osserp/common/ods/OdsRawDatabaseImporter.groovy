/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.ods

import com.osserp.common.ds.ImportException
import com.osserp.common.ds.RawDatabaseImporter
import com.osserp.common.ds.Sheet2Table


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class OdsRawDatabaseImporter extends RawDatabaseImporter {

    OdsRawDatabaseImporter(String selectedMode, String directory) throws ImportException {
        super(selectedMode, directory)
    }

    @Override
    protected Boolean isImportFile(File file) {
        (file && !file.directory && !file.hidden
                && (file.name.indexOf('ODS') > -1
                || file.name.indexOf('ods') > -1))
    }

    @Override
    protected Sheet2Table createImportFile(File file) throws ImportException {
        try {
            return new Ods2TableImportFile(file)
        } catch (Exception e) {
            throw new ImportException(e.message, e)
        }
    }
}
