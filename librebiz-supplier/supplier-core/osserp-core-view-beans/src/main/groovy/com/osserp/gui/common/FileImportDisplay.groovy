/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 24, 2014 
 * 
 */
package com.osserp.gui.common

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.JsonObject
import com.osserp.common.util.JsonUtil

import com.osserp.core.system.SyncTask

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class FileImportDisplay {
    private static Logger logger = LoggerFactory.getLogger(FileImportDisplay.class.getName())
    
    Long taskId
    Long userId
    String filename
    String importfile
    Date changed
    String status
    int result
    String errorReport
    boolean performActions

    /**
     * Serializable support
     */
    protected FileImportDisplay() {
        super()
    }

    /**
     * Creates a new FileImportDisplay 
     * @param syncTask
     */
    public FileImportDisplay(SyncTask syncTask) {
        JsonObject json = JsonUtil.parse(syncTask.getObjectData())
        taskId = syncTask.id
        performActions = json.getBoolean('performActions')
        filename = json.getString('sourcefile')
        importfile = json.getString('filename')
        userId = json.getLong('user')
        if (json.containsKey('resultCount')) {
            result = json.getInt('resultCount')
        }
        if (json.containsKey('errorReport')) {
            errorReport = json.getString('errorReport')
            logger.debug("<init> found task with errorReport [taskId=${taskId}]")
        }
        changed = syncTask.changed != null ? syncTask.changed : syncTask.created
        status = syncTask.statusDisplay
    }
}
