/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01-Feb-2007 20:34:47 
 * 
 */
package com.osserp.core.service.records;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.Records;
import com.osserp.core.finance.CalculationAwareRecord;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.OfferManager;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOfferManager extends AbstractPaymentAgreementAwareRecordManager
        implements OfferManager {

    protected AbstractOfferManager(
            Records records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask) {
        super(records, systemConfigManager, products, productPlanningTask);
    }

    public void update(
            Offer record,
            Date estimatedDelivery,
            Date validUntil,
            Long signatureLeft,
            Long signatureRight,
            Long personId,
            String note,
            boolean taxFree,
            Long taxFreeId,
            Double taxRate,
            Double reducedTaxRate,
            Long currency,
            String language,
            Long branchId,
            Long shippingId,
            String customHeader,
            boolean printComplimentaryClose,
            boolean printProjectmanager,
            boolean printSalesperson,
            boolean printBusinessId,
            boolean printBusinessInfo,
            boolean printRecordDate,
            boolean printRecordDateByStatus,
            boolean printPaymentTarget,
            boolean printConfirmationPlaceholder,
            boolean printCoverLetter,
            String offerInfo)
            throws ClientException {

        Date now = new Date(System.currentTimeMillis());
        if (estimatedDelivery != null) {
            if (now.after(estimatedDelivery)) {
                throw new ClientException(ErrorCode.DATE_PAST);
            }
        }
        if (validUntil != null) {
            if (now.after(validUntil)) {
                throw new ClientException(ErrorCode.DATE_PAST);
            }
        }
        if (taxFree && isNotSet(taxFreeId)) {
            throw new ClientException(ErrorCode.TAX_FREE_REASON_MISSING);
        }
        Offer offer = (Offer) getDao().load(record.getId());
        offer.setValidUntil(validUntil);

        if (isSet(offerInfo) && offer instanceof CalculationAwareRecord) {
            ((CalculationAwareRecord) offer).setCalculationInfo(offerInfo);
        }
        offer.update(
                signatureLeft,
                signatureRight,
                personId,
                note,
                taxFree,
                taxFreeId,
                taxRate,
                reducedTaxRate,
                currency,
                language,
                branchId,
                shippingId,
                customHeader,
                printComplimentaryClose,
                printProjectmanager,
                printSalesperson,
                printBusinessId,
                printBusinessInfo,
                printRecordDate,
                printRecordDateByStatus,
                printPaymentTarget,
                printConfirmationPlaceholder,
                printCoverLetter);
        getDao().save(offer);
    }

}
