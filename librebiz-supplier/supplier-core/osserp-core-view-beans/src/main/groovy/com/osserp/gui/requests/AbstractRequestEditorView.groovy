/**
 *
 * Copyright (C) 2009, 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.requests

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Option

import com.osserp.core.BusinessCase
import com.osserp.core.BusinessType
import com.osserp.core.BusinessTypeManager
import com.osserp.core.Options
import com.osserp.core.requests.Request
import com.osserp.core.requests.RequestManager
import com.osserp.core.sales.Sales
import com.osserp.core.sales.SalesManager
import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractRequestEditorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractRequestEditorView.class.getName())

    List<BusinessType> requestTypes = []
    List<Option> requestOriginTypes = []
    Request selectedRequest
    Sales selectedSales
    boolean requestLookup
    
    AbstractRequestEditorView() {
        super()
        dependencies = ['businessCaseView']
    }

    /**
     * Invokes super.initRequest, loads request and originTypes and performs a lookup 
     * for the request to edit if required by implementing class. The request loopkup
     * is actually performed by an existing session object businessCaseView or by request
     * param 'businessId'.
     * @param atts
     * @param headers
     * @param params  
     */
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            
            requestTypes = businessTypeManager.createTypeSelection(domainUser, false)
            logger.debug("initRequest: requestTypes lookup done [size=${requestTypes.size()}]")
            
            requestOriginTypes = getOptions(Options.REQUEST_ORIGIN_TYPES)
            logger.debug("initRequest: requestOriginTypes lookup done [size=${requestOriginTypes.size()}]")
            
            if (requestLookup) {
                
                if (env.businessCaseView) {
                    selectedRequest = env.businessCaseView.request
                } else {
                    Long id = form.getLong('businessId')
                    
                    if (id && salesManager.exists(id)) {
                        selectedSales = salesManager.load(id)
                        selectedRequest = selectedSales.request
                        
                    } else if (id && requestManager.exists(id)) {
                        selectedRequest = requestManager.load(id)
                    }
                    if (!selectedRequest) {
                        logger.warn('initRequest: did not find request by businessId nor businessCaseView')
                        throw new ViewContextException(this)
                    }
                }
                // TODO add request lookup by 'view' param if parent view is not main businessCase view 
            }
        }
    }
    
    protected BusinessType getSelectedRequestType(Long id) {
        id == null ? null : businessTypeManager.load(id)
    }
    
    void reloadDependencies() {
        if (env.businessCaseView) {
            env.businessCaseView.reload()
            logger.warn('reloadDependencies: done [object=businessCaseView]')
        }
    }

    void disableRequestLookup() {
        requestLookup = false
    }
    
    void enableRequestLookup() {
        requestLookup = true
    }
    
    BusinessCase getBusinessCase() {
        selectedSales ?: selectedRequest
    }

    protected BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }
    
    protected RequestManager getRequestManager() {
        getService(RequestManager.class.getName())
    }

    protected SalesManager getSalesManager() {
        getService(SalesManager.class.getName())
    }
}
