/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 11-Sep-2006 11:54:53 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.records.RecordActions;
import com.osserp.core.finance.SerialNumberDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordActionsDao extends AbstractTablesAwareSpringDao
        implements RecordActions {

    private OptionsCache optionsCache = null;
    private String serialNumberView = null;
    private String purchaseSerialTable = null;
    private String salesSerialTable = null;

    public RecordActionsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache) {

        super(jdbcTemplate, tables);
        this.optionsCache = optionsCache;
        this.purchaseSerialTable = getTable(TableKeys.PURCHASE_DELIVERY_NOTE_SERIALS);
        this.salesSerialTable = getTable(TableKeys.SALES_DELIVERY_NOTE_SERIALS);
        this.serialNumberView = getTable(TableKeys.SERIAL_NUMBER_VIEW);
    }

    public List<SerialNumberDisplay> findSerialNumbers(String pattern, boolean startsWith) {
        String lookup = (pattern == null) ? "" : pattern.trim();
        StringBuilder sql = new StringBuilder(SerialNumberDisplayRowMapper.SELECT_FROM);
        sql
                .append(serialNumberView)
                .append(" WHERE serial_number ~* '");
        if (startsWith) {
            sql.append("^");
        }
        sql.append(lookup).append("'");
        return (List<SerialNumberDisplay>) jdbcTemplate.query(
                sql.toString(),
                SerialNumberDisplayRowMapper.getReader(optionsCache));
    }

    public void changeSerialNumber(SerialNumberDisplay serialNumber, String value) {
        StringBuilder sql = new StringBuilder("UPDATE ");
        if (serialNumber.isSales()) {
            sql.append(salesSerialTable);
        } else {
            sql.append(purchaseSerialTable);
        }
        sql
                .append(" SET serial_number = '")
                .append(value)
                .append("' WHERE id = ")
                .append(serialNumber.getId());
        jdbcTemplate.update(sql.toString());
    }

    public boolean isProductReferenced(Long productId) {
        List<Boolean> result = (List<Boolean>) jdbcTemplate.query(
                RecordProcs.IS_PRODUCT_DELETEABLE,
                new Object[] { productId },
                new int[] { Types.BIGINT },
                getBooleanRowMapper());
        if (!result.isEmpty()) {
            return !result.get(0);
        }
        return false;
    }
}
