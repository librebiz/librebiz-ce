/**
 *
 * Copyright (C) 2012 The original author or authors.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.core.api

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WsProduct
import com.osserp.api.WSProduct
import com.osserp.common.OptionsCache
import com.osserp.core.Options
import com.osserp.core.products.Product

/**
 * 
 * WsProductMapper maps core products to ws products
 * 
 * 
 * @author rk <rk@osserp.com>
 * 
 */
class WsProductMapper {
    private static Logger logger = LoggerFactory.getLogger(WsProductMapper.class.getName())

    private OptionsCache options

    protected WsProductMapper(OptionsCache optionsCache) {
        options = optionsCache
    }

    WSProduct map(Product product) {
        //logger.debug("map: invoked [productId=${product?.productId}, options=${(options != null)}]:\n${product.toString()}")
        new WsProduct(
            gtin: product.gtin13 ?: product.gtin14 ?: product.gtin8 ?: null,
            primaryKey: product.productId,
            name: product.name,
            description: product.description,
            shortDescription: product.descriptionShort,
            marketingText: product.marketingText,
            manufacturer: getManufacturer(product.manufacturer),
            power: product.power,
            powerUnit: getPowerUnitName(product.powerUnit),
            color: getColorName(product.details?.color),
            price: product.consumerPrice,
            priceNet: product.consumerPrice,
            typeId: product.typeDefault?.id,
            type: product.typeDefault?.name,
            groupId: product.group?.id,
            group: product.group?.name,
            categoryId: product.category?.id,
            category: product.category?.name
        )
    }

    protected String getColorName(Long colorId) {
        !colorId ? null : options.getMappedValue(Options.PRODUCT_COLORS, colorId)
    }

    protected String getManufacturer(Long manufacturerId) {
        !manufacturerId ? null : options.getMappedValue(Options.PRODUCT_MANUFACTURERS, manufacturerId)
    }

    protected String getPowerUnitName(Long powerUnitId) {
        !powerUnitId ? null : options.getMappedValue(Options.QUANTITY_UNITS, powerUnitId)
    }
}
