/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Apr-2005 14:26:53 
 * 
 */
package com.osserp.common.dms;

import java.util.Date;

import com.osserp.common.EntityRelation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DmsDocument extends EntityRelation {

    /**
     * Provides the sourceDate or date created if source date not exists
     * @return note or filename
     */
    Date getDisplayDate();

    /**
     * Provides a note if exists or filename
     * @return note or filename
     */
    String getDisplayName();

    /**
     * An optional header name
     * @return
     */
    String getHeaderName();

    /**
     * Returns the document type
     * @return type
     */
    DocumentType getType();

    /**
     * Provides the type of the reference if documentType requires this. 
     * This is the case where different references with overlapping 
     * identifiers share the same documentType.
     * @return referenceType or null if not supported by documentType
     */
    Long getReferenceType();

    /**
     * The file name of the document
     * @return fileName
     */
    String getFileName();

    /**
     * Sets the file name of the document
     * @param fileName
     */
    void setFileName(String fileName);

    /**
     * The file size of the document
     * @return fileSize
     */
    long getFileSize();

    /**
     * Sets the file size of the document
     * @param fileSize
     */
    void setFileSize(long fileSize);

    /**
     * Provides the persistence format
     * @return persistence format
     */
    int getPersistenceFormat();

    /**
     * Sets the persistence format 
     * @param persistenceFormat
     */
    void setPersistenceFormat(int persistenceFormat);
    
    /**
     * Indicates if the document is stored in plain format,
     * e.g. default persistenceFormat == 0. 
     * @return true for plain persistence format
     */
    boolean isFormatPlain();

    /**
     * Provides the contextName of the document. This property is used to
     * group static documents such as templates.
     * @return contextName
     */
    String getContextName();

    /**
     * Provides a short description of the document content
     * @return description or null if non provided
     */
    String getDescription();

    /**
     * Sets an optional description of the document content
     * @param description
     */
    void setDescription(String description);
    
    /**
     * @return note
     */
    String getNote();

    /**
     * Sets the note
     * @param note
     */
    void setNote(String note);

    /**
     * Provides the language key of the document
     * @return language key
     */
    String getLanguage();

    /**
     * Sets the language
     * @param language
     */
    void setLanguage(String language);

    /**
     * Provides the suffix if not provided by type
     * @return suffix
     */
    String getSuffix();

    /**
     * Provides the real filename
     * @return realFileName
     */
    String getRealFileName();

    /**
     * Provides the url
     * @return url
     */
    String getUrl();

    /**
     * Indicates that this document is declared as reference, e.g. primary, default
     * @return referenceDocument
     */
    boolean isReferenceDocument();

    /**
     * Enables disables reference document flag
     * @param referenceDocument
     */
    void setReferenceDocument(boolean referenceDocument);

    /**
     * Provides the original source date of the document
     * @return sourceDate
     */
    Date getSourceDate();

    /**
     * Sets the original source date of the document
     * @param sourceDate
     */
    void setSourceDate(Date sourceDate);
    
    /**
     * Provides an optional document category
     * @return category id or null if undefined
     */
    Long getCategoryId();
    
    /**
     * Sets an optional document category
     * @param categoryId
     */
    void setCategoryId(Long categoryId);

    /**
     * Provides an optional orderId for static documents
     * @return orderId or 0 if not provided 
     */
    int getOrderId();
    
    /**
     * Provides the start of validity period if supported by documentType.
     * @return valid from date or null if not set and/or supported
     */
    Date getValidFrom();

    /**
     * Sets start of validity period
     * @param validFrom
     */
    void setValidFrom(Date validFrom);

    /**
     * Provides the end of validity period if supported by documentType.
     * @return valid til date or null if not set and/or supported
     */
    Date getValidTil();

    /**
     * Sets end of validity period
     * @param validTil
     */
    void setValidTil(Date validTil);

    /**
     * Provides the messageId if document is email attachment
     * @return messageId
     */
    String getMessageId();

    /**
     * Sets the messageId for email attachments
     * @return messageId
     */
    void setMessageId(String messageId);

    /**
     * Optional property, usage depends on referenced type. 
     * @return true if enabled
     */
    boolean isUnused();

    /**
     * Indicates thumbnail availabilty
     * @return true if thumbnail exists
     */
    boolean isThumbnailAvailable();

    void setThumbnailAvailable(boolean thumbnailAvailable);

}
