<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactView}"/>
<c:set var="contact" value="${contactView.bean}"/>

<div class="contentBox">
	<div class="contentBoxHeader">
		<div class="contentBoxHeaderLeft">
			<table class="valueTable first33">
				<tbody>
					<c:choose>
    					<c:when test="${contact.type.business}">
	       					<tr>
								<td style="padding: 0 5px;">
                                    <a href="javascript:ojsCommon.toggleDisplay('created');" title="<fmt:message key="editStatus"/>">
										<c:choose>
											<c:when test="${contact.firstNamePrefix}">
												<o:out value="${contact.firstName}"/>
											</c:when>
			     							<c:otherwise>
												<o:out value="${contact.lastName}"/>
											</c:otherwise>
										</c:choose>
									</a>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
				    		<tr>
								<td style="padding: 0 5px;">
                                    <c:if test="${!empty contact.salutation.name}"><span style="margin-right: 8px;"><o:out value="${contact.salutation.name}"/></span></c:if>
									<c:if test="${!empty contact.title.name}"><span style="margin-right: 8px;"><o:out value="${contact.title.name}"/></span></c:if>
								    <a href="javascript:ojsCommon.toggleDisplay('created');" title="<fmt:message key="editStatus"/>"><o:out value="${contact.displayName}"/></a>
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
	</div>
	<div class="contentBoxData">
		<div id="created" style="display: none;">
			<div class="smallSpacer"></div>
			<table class="valueTable first33" style="margin-top: 5px;">
				<tbody>
					<tr>
						<td><fmt:message key="databaseId"/></td>
						<td><o:out value="${contact.contactId}"/></td>
					</tr>
					<tr>
						<td><fmt:message key="createdBy"/></td>
						<td>
							<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${contact.personCreatedBy}">
								<oc:employee value="${contact.personCreatedBy}"/>
							</v:ajaxLink>
							<c:if test="${!empty contact.personCreated}"> / <fmt:formatDate value="${contact.personCreated}"/></c:if>
						</td>
					</tr>
					<c:if test="${!empty contact.personChangedBy}">
						<tr>
							<td><fmt:message key="changedBy"/></td>
							<td>
								<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${contact.personChangedBy}">
									<oc:employee value="${contact.personChangedBy}"/>
								</v:ajaxLink>
								<c:if test="${!empty contact.personChanged}"> / <fmt:formatDate value="${contact.personChanged}"/></c:if>
							</td>
						</tr>
					</c:if>
				</tbody>
			</table>
    		<div class="smallSpacer"></div>
		</div>
	</div>
</div>
