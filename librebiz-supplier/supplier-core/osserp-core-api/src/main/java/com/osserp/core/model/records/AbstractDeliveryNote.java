/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 19:59:28 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import com.osserp.common.Constants;

import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.SerialNumber;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDeliveryNote extends AbstractRecord implements DeliveryNote {

    private Date delivery = null;

    /**
     * Default constructor required by Serializable
     */
    protected AbstractDeliveryNote() {
        super();
    }

    /**
     * Creates a new delivery note
     * @param id
     * @param company
     * @param branchId
     * @param shippingId
     * @param type
     * @param bookingType
     * @param reference
     * @param contact
     * @param createdBy
     * @param sale
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected AbstractDeliveryNote(
            Long id,
            Long company,
            Long branchId,
            Long shippingId,
            RecordType type,
            DeliveryNoteType bookingType,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long sale,
            boolean itemsChangeable,
            boolean itemsEditable) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                sale,
                Constants.DOUBLE_NULL,
                Constants.DOUBLE_NULL,
                itemsChangeable,
                itemsEditable);
        setShippingId(shippingId);
        setBookingType(bookingType);
    }

    public Date getDelivery() {
        return delivery;
    }

    protected void setDelivery(Date delivery) {
        this.delivery = delivery;
    }

    public void update(
            String note,
            String language,
            Date delivery,
            boolean printBusinessId,
            boolean printBusinessInfo,
            boolean printRecordDate,
            boolean printRecordDateByStatus) {
        setNote(note);
        setLanguage(language);
        setDelivery(delivery);
        setPrintBusinessCaseId(printBusinessId);
        setPrintBusinessCaseInfo(printBusinessInfo);
        setPrintRecordDate(printRecordDate);
        setPrintRecordDateByStatus(printRecordDateByStatus);
    }

    public boolean isBooked() {
        return (getStatus() == null) ? false :
                getStatus().longValue() >= Record.STAT_BOOKED.longValue();
    }

    public boolean isCorrection() {
        return (getBookingType() instanceof DeliveryNoteType
                && ((DeliveryNoteType) getBookingType()).isCorrection());
    }

    public boolean isSerialsCompleted() {
        boolean result = true;
        for (int i = 0, j = getItems().size(); i < j; i++) {
            Item item = getItems().get(i);
            if (item.getProduct().isSerialAvailable()
                    && item.getProduct().isSerialRequired()
                    && !item.isSerialComplete()) {
                result = false;
                break;
            }
        }
        return result;
    }

    public void addSerial(Item item, String number, Employee user) {
        Item obj = fetchItem(item.getId());
        if (obj != null) {
            boolean available = false;
            for (int i = 0, j = obj.getSerials().size(); i < j; i++) {
                SerialNumber next = obj.getSerials().get(i);
                if (next.getSerial().equals(number)) {
                    available = true;
                    break;
                }
            }
            if (!available) {
                obj.getSerials().add(createSerial(item, number, user));
            }
        }
    }

    protected abstract SerialNumber createSerial(Item item, String number, Employee employee);

    @Override
    protected void calculateByBaseTaxOnly() {
        // nothing to do here for delivery notes
    }

    @Override
    protected void calculateByMixedTax() {
        // nothing to do here for delivery notes
    }
}
