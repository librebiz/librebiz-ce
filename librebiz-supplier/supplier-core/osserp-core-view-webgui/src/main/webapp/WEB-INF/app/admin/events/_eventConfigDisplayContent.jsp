<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="actionView" value="${requestScope.eventConfigView}"/>
<c:set var="action" value="${requestScope.eventConfig}"/>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${action.id}"/> - <o:out value="${action.name}"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <c:if test="${actionView.fcsMode}">

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="fcsAction"><fmt:message key="fcsAction"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:options name="${action.type.flowControlActionsName}" value="${action.callerId}"/>
                    </div>
                </div>
            </div>

            <c:if test="${!empty action.closings}" >
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="scheduledThrough"><fmt:message key="scheduledThrough"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:forEach var="closed" items="${action.closings}" varStatus="status">
                                <c:if test="${status.index > 0}"><br></c:if>
                                <oc:options name="${action.type.flowControlActionsName}" value="${closed.callerId}"/>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </c:if>
        </c:if>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="manuallySchedulable"><fmt:message key="manuallySchedulable" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${action.closeableByTarget}">
                            <fmt:message key="yes"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="no"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="jobIsInformation"><fmt:message key="jobIsInformation" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${action.info}">
                            <fmt:message key="yes"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="no"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="priority"><fmt:message key="priority" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${action.priority == 1}">
                            <fmt:message key="high"/>
                        </c:when>
                        <c:when test="${action.priority == 2}">
                            <fmt:message key="normal"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="low"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="recipient"><fmt:message key="recipient" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${action.sendSales}">
                            <fmt:message key="responsibleSalesman"/>
                        </c:when>
                        <c:when test="${action.sendManager}">
                            <fmt:message key="responsibleTechnician"/>
                        </c:when>
                        <c:when test="${action.sendPool}">
                            <v:ajaxLink linkId="showPoolMembers" url="/admin/events/poolMembers/forward?id=${action.pool.id}">
                                <fmt:message key="pool"/> <o:out value="${action.pool.name}"/>
                            </v:ajaxLink>
                        </c:when>
                        <c:otherwise>
                            <span class="errortext"><fmt:message key="undefined"/>!</span>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>

        <c:if test="${action.sendPool}">
            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="ignoreBranchoffice"><fmt:message key="ignoreBranchoffice" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${action.ignoringBranch}">
                                <fmt:message key="yesSendToEveryPoolMember"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="noOnlySendToBranchOffice"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </c:if>

        <oc:pluginHookAvailable hook="crmEventConfig">
            <oc:pluginHook hook="crmEventConfig" view="eventConfigView" exit="/admin/events/eventConfig/reload" />
       </oc:pluginHookAvailable>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="completionAfter"><fmt:message key="completionAfter" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${empty action.terminationTime}">
                            <span class="errortext"><fmt:message key="undefined"/>!</span>
                        </c:when>
                        <c:otherwise>
                            <span title="<o:out value="${action.terminationTime}"/> ms"><o:days value="${action.terminationTime}"/> <fmt:message key="days"/></span>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="escalationSupport"><fmt:message key="escalationSupport" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${action.sendAlert}">
                            <fmt:message key="yes"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="no"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>

        <c:if test="${action.sendAlert}">

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="escalationAfter"><fmt:message key="escalationAfter" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${empty action.terminationAlert}">
                                <p class="errortext"><fmt:message key="undefined"/>!
                            </c:when>
                            <c:otherwise>
                                <o:days value="${action.terminationAlert}"/> <fmt:message key="days"/> / <o:out value="${action.terminationAlert}"/> ms
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${!empty action.terminationAlert}">
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="escalationStrategy"><fmt:message key="escalationStrategy" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${empty action.alertEvent}">
                                    <p class="errortext"><fmt:message key="undefined"/>!
                                </c:when>
                                <c:otherwise>
                                    <o:out value="${action.alertEvent.pool.name}"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </c:if>
        </c:if>

    </div>
</div>
