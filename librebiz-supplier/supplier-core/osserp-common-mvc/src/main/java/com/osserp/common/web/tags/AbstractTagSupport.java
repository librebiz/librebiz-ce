/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 12, 2008 2:25:49 PM 
 * 
 */
package com.osserp.common.web.tags;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.TagSupport;

import com.osserp.common.User;
import com.osserp.common.util.StringUtil;
import com.osserp.common.web.Globals;
import com.osserp.common.web.PortalView;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.SessionUtil;
import com.osserp.common.web.View;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class AbstractTagSupport extends TagSupport {
    private String style = null;
    private String styleClass = null;
    private String target = null;
    private String title = null;
    private String rawtitle = null;
    private String role = null;
    private boolean roleCheckByPortalView = true;

    @Override
    public void release() {
        super.release();
        style = null;
        styleClass = null;
        target = null;
        title = null;
        rawtitle = null;
    }

    /**
     * Creates a link by url (adds context path if required)
     * @param url
     * @return url with added context path
     */
    protected String createUrl(String url) {
        if (url != null) {
            String ctxPath = getContextPath();
            if (url.startsWith("http") || url.startsWith(ctxPath)) {
                return url;
            }
            return ctxPath + url;
        }
        return null;
    }

    /**
     * Provides the context path of the web application
     * @return context path
     */
    protected final String getContextPath() {
        return ((HttpServletRequest) pageContext.getRequest()).getContextPath();
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    protected final String[] getRoles() {
        if (role == null || role.length() < 1) {
            return null;
        }
        return StringUtil.getTokenArray(role, ",");
    }

    /**
     * Provides the style attribute value
     * @return style
     */
    protected final String getStyle() {
        return style;
    }

    /**
     * Sets the style attribute value
     * @param style
     */
    public final void setStyle(String style) {
        this.style = style;
    }

    /**
     * Provides the class attribute value
     * @return styleClass
     */
    protected final String getStyleClass() {
        return styleClass;
    }

    /**
     * Sets the class attribute value
     * @param styleClass
     */
    public final void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    /**
     * Provides the target attribute for links
     * @return target
     */
    protected String getTarget() {
        return target;
    }

    /**
     * Sets the target attribute
     * @param target
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * Provides the title attribute value. Note: The value is considered as i18nkey.
     * Use the rawtitle to set the value as is.  
     * @return title
     */
    protected final String getTitle() {
        return title;
    }

    /**
     * Sets the title attribute value. Note: The value is considered as i18nkey.
     * Use the rawtitle attribute to set the value as is.  
     * @param title
     */
    public final void setTitle(String title) {
        this.title = title;
    }

    /**
     * Provides the title attribute value. Note: The value is considered as is.
     * Use the title attribute to set the value as i18nkey.  
     * @return rawtitle
     */
    protected String getRawtitle() {
        return rawtitle;
    }

    /**
     * Sets the title attribute value. Note: The value is considered as is.
     * Use the title attribute to set the value as i18nkey.  
     * @param rawtitle
     */
    public void setRawtitle(String rawtitle) {
        this.rawtitle = rawtitle;
    }

    public StringBuilder addId(StringBuilder buffer) {
        if (id != null) {
            buffer.append(" id=\"").append(id).append("\" ");
        }
        return buffer;
    }

    public StringBuilder addStyle(StringBuilder buffer) {
        if (style != null) {
            buffer.append(" style=\"").append(style).append("\" ");
        }
        return buffer;
    }

    public StringBuilder addStyleClass(StringBuilder buffer) {
        if (styleClass != null) {
            buffer.append(" class=\"").append(styleClass).append("\" ");
        }
        return buffer;
    }

    public StringBuilder addTitle(StringBuilder buffer) {
        if (title != null) {
            buffer.append(" title=\"").append(getResourceString(title)).append("\" ");
        } else if (rawtitle != null) {
            buffer.append(" title=\"").append(rawtitle).append("\" ");
        }
        return buffer;
    }

    public final String renderStyles() {
        StringBuilder buffer = new StringBuilder();
        if (style != null && style.length() > 1) {
            buffer.append(" style=\"").append(style).append("\"");
        }
        if (styleClass != null && styleClass.length() > 1) {
            buffer.append(" class=\"").append(styleClass).append("\"");
        }
        return buffer.toString();
    }

    public String renderTarget() {
        return createTarget(target);
    }

    public String renderBlank() {
        return createTarget("_blank");
    }

    private String createTarget(String providedTarget) {
        StringBuilder buffer = new StringBuilder();
        if (providedTarget != null) {
            buffer.append(" target=\"").append(providedTarget).append("\"");
        }
        return buffer.toString();
    }

    /**
     * Renders a hidden input text tag providing the CSRF token.
     * @param formUrl
     * @return csrf input tag
     */
    protected String renderCSRFInput(String formUrl) {
        String[] csrfToken = RequestUtil.getCSRFToken(getRequest(), formUrl);
        return TagUtil.createCsrfInput(csrfToken[0], csrfToken[1]);
    }

    /**
     * Appends current csrf token to provided uri
     * @param uri
     * @return uri link with csrf token param 
     */
    protected String appendCSRFToken(String uri) {
        return RequestUtil.appendCSRFToken(getRequest(), uri);
    }

    /**
     * Provides current request
     * @return request
     */
    protected final HttpServletRequest getRequest() {
        return (HttpServletRequest) pageContext.getRequest();
    }

    /**
     * Provides the servlet context
     * @return servletContext
     */
    protected final ServletContext getServletContext() {
        return pageContext.getServletContext();
    }

    /**
     * Provides current session
     * @return session
     */
    protected final HttpSession getSession() {
        return getRequest().getSession();
    }

    /**
     * Fetches a session object
     * @param key
     * @return object or null if not exists
     */
    protected Object getSessionObject(String key) {
        return ((HttpServletRequest) pageContext.getRequest()).getSession(true).getAttribute(key);
    }

    /**
     * Provides the resource string as provided by portal views resource bundle. Locale depends on current user or application default if no user logged in
     * during view activity.
     * @param key
     * @return resource or key depending on whether a string could be found or not.
     */
    protected final String getResourceString(String key) {
        return RequestUtil.getResourceString(getRequest(), key);
    }

    /**
     * Tries to fetch portal view by name as defined under Globals.PORTAL_VIEW. This method makes no attempt to create a portal view if none exists. Subclass
     * this class and override this method if you need a page tag creating a new portal view when no view exists.
     * @return portalView or null if not exists
     */
    protected PortalView getPortalView() {
        View view = (View) getSession().getAttribute(Globals.PORTAL_VIEW);
        if (view != null && view instanceof PortalView) {
            return (PortalView) view;
        }
        return null;
    }

    /**
     * Provides current user if found in session context.
     * @return user or null if not logged in
     */
    protected final User getUser() {
        try {
            return SessionUtil.getUser(getSession());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Indicates logged in user
     * @return true if user logged in
     */
    protected final boolean isUserLoggedIn() {
        return SessionUtil.isUserLoggedIn(getSession());
    }

    /**
     * Checks if user matches provided role. 
     * @return true if role assigned or role not set.
     */
    protected final boolean isRequiredRoleAssigned() {
        if (role == null || role.length() == 0) {
            return true;
        }
        return isPermissionGrant(role);
    }

    /**
     * Indocates if provided permission is grant to current user
     * @param permissions
     * @return true if permission grant
     */
    protected final boolean isPermissionGrant(String permissions) {
        return isPermissionGrant(StringUtil.getTokenArray(permissions));
    }

    /**
     * Indocates if provided permission is grant to current user
     * @param permissions
     * @return true if permission grant
     */
    protected final boolean isPermissionGrant(String[] permissions) {
        if (roleCheckByPortalView) {
            PortalView portalView = getPortalView();
            if (portalView == null) {
                return false;
            }
            return !portalView.isPermissionDenied(permissions);
        }
        User user = getUser();
        if (user == null) {
            return false;
        }
        return user.isPermissionGrant(permissions);
    }

    protected String encodeForAttribute(String untrusted) {
        return TagUtil.encodeForAttribute(untrusted);
    }

    protected String encodeForElement(String untrusted) {
        return TagUtil.encodeForAttribute(untrusted);
    }
}
