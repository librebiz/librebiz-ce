<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

	<tr>
		<td><fmt:message key="name"/></td>
		<td><v:text name="name"/></td>
	</tr>
	<tr>
		<td><fmt:message key="salesRegionManager"/></td>
		<td>
			<select name="salesExecutiveId" class="select" size="1">
				<option value=""><fmt:message key="promptSelect"/>:</option>
				<c:forEach var="option" items="${view.salesStaff}">
					<c:choose>
						<c:when test="${option.id == view.bean.salesExecutive.id}">
							<option value="<o:out value="${option.id}"/>" selected="selected"><o:out value="${option.displayName}"/></option>
						</c:when>
						<c:otherwise>
							<option value="<o:out value="${option.id}"/>"><o:out value="${option.displayName}"/></option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td><fmt:message key="salesBy"/></td>
		<td>
			<select name="salesId" class="select" size="1">
				<option value=""><fmt:message key="promptSelect"/>:</option>
				<c:forEach var="option" items="${view.salesStaff}">
					<c:choose>
						<c:when test="${option.id == view.bean.sales.id}">
							<option value="<o:out value="${option.id}"/>" selected="selected"><o:out value="${option.displayName}"/></option>
						</c:when>
						<c:otherwise>
							<option value="<o:out value="${option.id}"/>"><o:out value="${option.displayName}"/></option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</td>
	</tr>
