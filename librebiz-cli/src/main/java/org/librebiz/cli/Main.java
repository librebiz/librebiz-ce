/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 5, 2017 
 * 
 */
package org.librebiz.cli;

import com.osserp.common.service.ConsoleTask;

import com.osserp.core.sql.DatabaseSetupTask;
import com.osserp.core.sql.DatabaseUpdateTask;
import com.osserp.core.sql.DatabaseTaskRunner;

import org.librebiz.bs.LibrebizSetupTask;
import org.librebiz.bs.LinuxSetupTask;

/**
 * 
 * Setup task runner
 * 
 * @author Rainer Kirchner <rk@librebiz.org>
 * 
 */
public class Main extends ConsoleTask {

    public static void main(String[] args) {
        boolean silent = false;
        boolean verbose = false;
        int result = 0;
        String arg0 = ((args != null && args.length > 0) ? args[0] : null);

        if ("init".equals(arg0)) {
            log("Starting admin tasks...", false, silent);
            result = DatabaseTaskRunner.runDatabaseTasks(
                    DatabaseSetupTask.class.getName(),
                    null,
                    silent,
                    verbose);
            if (result == 0) {
                result = DatabaseTaskRunner.runDatabaseTasks(
                        LibrebizSetupTask.class.getName(),
                        null,
                        silent, verbose);
            }
            log("Running linux tasks...", false, silent);
            result = LinuxSetupTask.runTasks(silent);
            if (result != 0) {
                System.exit(result);
            }

        } else if ("update".equals(arg0)) {
            String version = ((args != null && args.length > 1) ? args[1] : null);
            if (version != null) {
                log("Starting update task for version " + version, false, silent);
            }
            result = DatabaseTaskRunner.runDatabaseTasks(
                    DatabaseUpdateTask.class.getName(),
                    version, silent, verbose);
        } else {
            log("Unknown command. Neither 'init' nor 'update' requested.", false, silent);
            result = 1;
        }
        if (result == 0) {
            System.exit(0);
        }
        System.exit(result);
    }
}
