/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 12-Aug-2006 12:54:09 
 * 
 */
package com.osserp.core.events;

import java.util.Date;
import java.util.List;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EventAction extends EventConfig, Option {

    /**
     * Sets the name of the event
     * @param name
     */
    void setName(String name);

    /**
     * Provides the default priority of the event
     * @return priority
     */
    int getPriority();

    /**
     * Sets the priority
     * @param priority
     */
    void setPriority(int priority);

    /**
     * Provides the closing event configs if this event is not a closing event itself, e.g. if this is not a closing event and event is terminated by another
     * event.
     * @return closings
     */
    List<EventTermination> getClosings();

    /**
     * Adds a closing
     * @param callerId
     */
    void addClosing(Long callerId);

    /**
     * The name of the action link to be performed from user to initiate terminating link
     * @return actionLink
     */
    String getActionLink();

    /**
     * Sets the name of the action link to be performed from user to initiate terminating link
     * @param actionLink
     */
    void setActionLink(String actionLink);

    /**
     * Provides the time in ms when event should be terminated
     * @return terminationTime
     */
    Long getTerminationTime();

    /**
     * Sets the termination time in ms
     * @param terminationTime
     */
    void setTerminationTime(Long terminationTime);

    /**
     * Provides the time in ms when an alert event should be raised if the event is not terminated
     * @return terminationAlert
     */
    Long getTerminationAlert();

    /**
     * Sets the termination alert in ms
     * @param terminationAlert
     */
    void setTerminationAlert(Long terminationAlert);

    /**
     * Provides the termination alert event action
     * @return alertEvent
     */
    public EventAction getAlertEvent();

    /**
     * Sets the termination alert event action
     * @param alertEvent
     */
    void setAlertEvent(EventAction alertEvent);

    /**
     * Updates an existing alert event
     * @param pool
     */
    void updateAlertEvent(RecipientPool pool);

    /**
     * The target queue if this event initiates anotehr event
     * @return targetQueue
     */
    String getTargetQueue();

    /**
     * Sets the target queue
     * @param targetQueue
     */
    void setTargetQueue(String targetQueue);

    /**
     * Indicates whether this event is closeable by targeted person
     * @return closeableByTarget
     */
    boolean isCloseableByTarget();

    /**
     * Sets that additional action(s) should be performed before and/or after executing this fcs action
     * @param closeableByTarget
     */
    void setCloseableByTarget(boolean closeableByTarget);

    /**
     * Indicates that events of this action support sending of alerts events
     * @return sendAlert
     */
    boolean isSendAlert();

    /**
     * Enables/disables support for sending of alerts for events of this action
     * @param sendAlert
     */
    void setSendAlert(boolean sendAlert);

    /**
     * Indicates that this event should be directed to a pool
     * @return sendPool
     */
    boolean isSendPool();

    /**
     * Sets that this event should be directed to a pool
     * @param sendPool
     */
    void setSendPool(boolean sendPool);

    /**
     * Returns the related pool
     * @return pool
     */
    public RecipientPool getPool();

    /**
     * Sets the related pool
     * @param pool
     */
    void setPool(RecipientPool pool);

    /**
     * Indicates that this event should be sent to the sales person
     * @return sendSales
     */
    boolean isSendSales();

    /**
     * Sets that this event should be sent to the sales person
     * @param sendSales
     */
    void setSendSales(boolean sendSales);

    /**
     * Indicates that this event should be sent to the project manager
     * @return sendManager
     */
    boolean isSendManager();

    /**
     * Sets that this project note should be sent to the project manager
     * @param sendManager
     */
    void setSendManager(boolean sendManager);

    /**
     * Indicates that events of this type should be addressed to user itself
     * @return the sendSelf
     */
    boolean isSendSelf();

    /**
     * Sets that events of this type should be addressed to user itself
     * @param sendSelf the sendSelf to set
     */
    void setSendSelf(boolean sendSelf);

    /**
     * Indicates that events of this type should be addressed to several selected users
     * @return the sendSelected
     */
    boolean isSendSelected();

    /**
     * Sets that events of this type should be addressed to several selected users
     * @param sendSelected the sendSelected to set
     */
    void setSendSelected(boolean sendSelected);

    /**
     * Indicates whether this action is an info or job
     * @return true if info, false if job
     */
    boolean isInfo();

    /**
     * Sets that action is info
     * @param info
     */
    void setInfo(boolean info);
    
    /**
     * Checks if an info action is configured for type or action.
     * @return true if infoActionId or type.infoActionId exists
     */
    boolean isInfoActionAvailable();
    
    /**
     * Provides the configured info action id by action or type.
     * A configured info action on eventAction should override
     * an info action on type.
     * @return info action id or null if non set
     */
    Long getInfoAction();

    /**
     * Indicates that the action ignores recipients branch flag.
     * @return ignoringBranch
     */
    boolean isIgnoringBranch();

    /**
     * Sets that event is ignoring the branch flag
     * @param ignoringBranch
     */
    void setIgnoringBranch(boolean ignoringBranch);

    /**
     * Indicates whether this action is paused.
     * @return true if paused
     */
    boolean isPaused();

    /**
     * Sets that action is paused
     * @param paused
     */
    void setPaused(boolean paused);

    /**
     * Provided the id of the user switching paused status
     * @return user id
     */
    Long getPausedBy();

    /**
     * Sets the user switching paused status
     * @param pausedBy
     */
    void setPausedBy(Long pausedBy);

    /**
     * Sets the date when paused status changed
     * @return date last change
     */
    Date getPausedDate();

    /**
     * Updates last paused status change
     * @param pausedDate
     */
    void setPausedDate(Date pausedDate);
}
