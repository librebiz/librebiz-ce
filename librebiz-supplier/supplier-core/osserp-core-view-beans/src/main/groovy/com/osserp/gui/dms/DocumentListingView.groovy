/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Mar 12, 2016
 *
 */
package com.osserp.gui.dms

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.dms.DmsSearch
import com.osserp.common.dms.DmsUtil
import com.osserp.common.dms.DocumentType

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class DocumentListingView extends DocumentAwareView {
    private static Logger logger = LoggerFactory.getLogger(DocumentListingView.class.getName())

    DocumentType documentType
    Integer documentCount = 0

    DocumentListingView() {
        headerName  = 'documents'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            // check for listing by type
            initDocumentType(getTypeId())
        }
    }
    
    /**
     * Override this method to set a static documentType in derived views.
     * The default is to fetch the id by form.type parameter if exists.
     */
    protected Long getTypeId() {
        getLong('type')
    }
    
    protected final void initDocumentType(Long id) {
        if (id) {
            documentType = dmsSearch.getDocumentType(id)
            reload()
        } else {
            logger.debug("initRequest: did not find expected param(s) [type=null]")
            documentType = null
        }
    }
    
    @Override
    void reload() {
        if (documentType) {
            list = DmsUtil.sortByDate(dmsSearch.findByType(documentType), true)
            documentCount = list.size()
            logger.debug("reload: done [type=${documentType?.id}, count=${list.size()}]")
        } else {
            logger.debug('reload: no documentType associated with current view')
        }
    }
}
