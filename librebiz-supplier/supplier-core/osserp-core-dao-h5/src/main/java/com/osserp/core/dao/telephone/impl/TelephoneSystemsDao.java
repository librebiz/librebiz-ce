/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 2:42:27 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.telephone.TelephoneSystems;
import com.osserp.core.model.telephone.TelephoneSystemImpl;
import com.osserp.core.telephone.TelephoneSystem;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneSystemsDao extends AbstractDao implements TelephoneSystems {

    protected TelephoneSystemsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public TelephoneSystem find(Long id) {
        List<TelephoneSystem> list = getCurrentSession().createQuery(
                "from " + TelephoneSystemImpl.class.getName() + " ts where ts.id = :pk")
                .setParameter("pk", id).list();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public List<TelephoneSystem> findByType(Long type) {
        List<TelephoneSystem> list = new ArrayList<TelephoneSystem>();
        list = getCurrentSession().createQuery(
                "from " + TelephoneSystemImpl.class.getName() 
                + " ts where ts.systemType.id = :typeId")
                .setParameter("typeId", type).list();
        return list;
    }

    public TelephoneSystem findByBranch(Long branch) {
        List<TelephoneSystem> list = getCurrentSession().createQuery(
                "from " + TelephoneSystemImpl.class.getName() 
                + " ts where ts.branchId = :branch")
                .setParameter("branch", branch).list();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public List<TelephoneSystem> getAll() {
        return getCurrentSession().createQuery("from " + TelephoneSystemImpl.class.getName()
                + " ts order by ts.name").list();
    }

    public void save(TelephoneSystem telephoneSystem) {
        getCurrentSession().saveOrUpdate(TelephoneSystemImpl.class.getName(), telephoneSystem);
    }

    public TelephoneSystem createTelephoneSystem(TelephoneSystem telephoneSystem) {
        // TODO Auto-generated method stub
        return null;
    }
}
