/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2007 10:45:32 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class AjaxFormTag extends AbstractAjaxFormTag {
    private static Logger log = LoggerFactory.getLogger(AjaxFormTag.class.getName());
    private String name = null;

    @Override
    public int doStartTag() throws JspException {
        try {
            StringBuilder buffer = new StringBuilder();
            buffer
                    .append("<form")
                    .append(" name=\"").append(fetchName()).append("\"")
                    .append(" id=\"").append(fetchName()).append("\"")
                    .append(" method=\"post\"")
                    .append(" action=\"javascript:;\" onsubmit=\" ");
            if (getPreRequest() != null) {
                buffer.append(getPreRequest());
            }

            buffer.append(createAjaxFormFunctionCall());

            if (getPostRequest() != null) {
                buffer.append(" ").append(getPostRequest());
            }
            buffer.append(renderStyles()).append("\">");
            buffer.append(renderCSRFInput(createUrl()));
            if (!isDisableAutoupdate()) {
                buffer = TagUtil.createUpdateOnlyFormFields(buffer, isUpdateonly());
            }
            if (log.isDebugEnabled()) {
                log.debug("doStartTag() done [markup=" + buffer.toString() + "]");
            }
            JspWriter out = pageContext.getOut();
            out.print(buffer.toString());
            resetParams();
        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("</form>");
        } catch (IOException ioe) {
            log.error("doEndTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    /**
     * @return returns the Name of the form.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The Name of the form to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Tries to fetch form name by 'name' attribute and returns default form name if name not exists.
     * @return value of attribute name of 'dynamicForm' if name not set
     */
    protected String fetchName() {
        if (name != null && name.length() > 0) {
            return name;
        }
        return "dynamicForm";
    }

    @Override
    public String getTargetElement() {
        String te = super.getTargetElement();
        return (te != null && te != "") ? te : fetchName() + "_popup";
    }
}
