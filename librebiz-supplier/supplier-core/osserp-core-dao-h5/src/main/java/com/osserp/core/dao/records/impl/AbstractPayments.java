/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 02-Feb-2007 02:04:33 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateUtil;

import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.Payments;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPayments extends AbstractFinanceRecords implements Payments {
    private static Logger log = LoggerFactory.getLogger(AbstractPayments.class.getName());
    private RecordTypes billingTypes = null;

    public AbstractPayments(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            RecordTypes recordTypes) {
        super(jdbcTemplate, tables, sessionFactory, cache, taxRates);
        billingTypes = recordTypes;
    }

    public Payment create(
            Long paymentType,
            PaymentAwareRecord record,
            BigDecimal amount,
            Date paid,
            Long bankAccountId,
            Employee user)
            throws ClientException {

        BillingType type = fetchBillingType(paymentType);
        Payment obj = record.addPayment(
                user,
                type,
                amount,
                paid,
                bankAccountId);
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), obj);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + obj.getId() + "]");
        }
        return obj;
    }

    public Payment createCustom(
            PaymentAwareRecord record,
            BigDecimal amount,
            Date paid,
            String customHeader,
            String note,
            Long bankAccountId,
            Employee user)
            throws ClientException {

        BillingType type = fetchBillingType(BillingType.TYPE_CUSTOM_PAYMENT);
        Payment obj = record.addCustomPayment(
                user,
                type,
                amount,
                paid,
                customHeader,
                note,
                bankAccountId);
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), obj);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + obj.getId() + "]");
        }
        return obj;
    }

    @Override
    public final List<Payment> getByContact(Long contactId) {
        return (List<Payment>) super.getByContact(contactId);
    }

    public final List<Payment> getByOrder(Order order) {
        return getCurrentSession().createQuery(
                "from " + getPersistentClass().getName() 
                + " o where o.reference = :orderId")
                .setParameter("orderId", order.getId()).list();
    }

    public final List<Payment> getByBankAccount(Long bankAccountId) {
        List<Payment> list = getCurrentSession().createQuery(
                "from " + getPersistentClass().getName() 
                + " o where o.bankAccountId = :accountId order by o.paid")
                .setParameter("accountId", bankAccountId).list();
        return CollectionUtil.reverse(list);
    }

    @Override
    public final List<Payment> getByReference(Long id) {
        return (List<Payment>) super.getByReference(id);
    }

    public List<Payment> getByRecord(PaymentAwareRecord record) {
        return getCurrentSession().createQuery(
                "from " + getPersistentClass().getName() 
                + " o where o.recordId = :recordid and o.recordTypeId = :typeid")
                .setParameter("recordid", record.getId())
                .setParameter("typeid", record.getType().getId())
                .list();
    }

    @Override
    protected String getByDateProperty() {
        return "paid";
    }

    public Payment getPayment(Long id) {
        List<Payment> result = getCurrentSession().createQuery(
                "from " + getPersistentClass().getName() + " o where o.id = :pmId")
                .setParameter("pmId", id).list();
        return (result.isEmpty() ? null : result.get(0));
    }

    public void delete(Payment payment) {
        Payment deletable = getPayment(payment.getId());
        getCurrentSession().delete(getPersistentClass().getName(), deletable);
    }

    public void increaseTime(Payment payment) {
        payment.setPaid(DateUtil.addMillis(payment.getPaid(), 1000));
        save(payment);
    }

    public void subtract(Payment payment, BigDecimal cashDiscount) {
        payment.subtract(cashDiscount);
        save(payment);
    }

    public void save(Payment payment) {
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), payment);
    }

    protected BillingType fetchBillingType(Long id) {
        if (id == null) {
            log.warn("fetchBillingType() invoked with NULL as id");
        }
        return billingTypes.loadBillingType(id);
    }
}
