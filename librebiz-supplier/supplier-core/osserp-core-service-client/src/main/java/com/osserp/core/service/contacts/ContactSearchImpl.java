/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16-Jan-2007 09:15:09 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactSearch;
import com.osserp.core.contacts.ContactSearchResult;
import com.osserp.core.contacts.Interest;
import com.osserp.core.dao.ContactQueries;
import com.osserp.core.dao.Contacts;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactSearchImpl extends AbstractService implements ContactSearch {
    private static Logger log = LoggerFactory.getLogger(ContactSearchImpl.class.getName());

    private Contacts contacts = null;
    private ContactQueries contactQueries = null;

    public ContactSearchImpl(Contacts contacts, ContactQueries contactQueries) {
        this.contacts = contacts;
        this.contactQueries = contactQueries;
    }

    public Contact find(Long contactId) throws ClientException {
        return contacts.loadContact(contactId);
    }

    public List<Contact> findAllByGroup(Long contactGroup) {
        return contacts.findAllByGroup(contactGroup);
    }

    public List<ContactSearchResult> find(String context, String column, Long type, String value, boolean startsWith) {
        return contactQueries.find(context, column, type, value, startsWith);
    }

    public List<ContactSearchResult> findFirstRows(String context) {
        return contactQueries.findFirstRows(context);
    }

    public List<ContactSearchResult> findPhoneNumbers(String number) {
        return contactQueries.findPhoneNumbers(number);
    }

    public ContactSearchResult findNameByContactId(Long contactId) {
        return contactQueries.findNameByContactId(contactId);
    }

    public List<Contact> findContactPersons(Long contactId) {
        return contacts.findContactPersons(contactId);
    }

    public boolean contactPersonExisting(Contact parent, String lastName, String firstName) {
        List<Contact> persons = findContactPersons(parent.getContactId());
        if (persons != null && !persons.isEmpty()) {
            for (int i = 0, j = persons.size(); i < j; i++) {
                Contact c = persons.get(i);
                String fn = c.getFirstName();
                String ln = c.getLastName();
                if (fn != null && ln != null) {
                    if (fn.equals(firstName) && ln.equals(lastName)) {
                        return true;
                    }
                } else if (ln != null) {
                    if (ln.equals(lastName)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public List<Contact> findExisting(String zipcode, String lastName) {
        return contacts.findExisting(zipcode, lastName);
    }

    public List<Contact> findExisting(Interest interest) {
        List<Contact> existingContacts = new ArrayList<Contact>();
        if (interest.getAddress() != null && isSet(interest.getAddress().getZipcode())) {
            if (log.isDebugEnabled()) {
                log.debug("findExisting() interest has required values set [zipcode="
                        + interest.getAddress().getZipcode()
                        + ", name=" + interest.getLastName()
                        + (interest.isBusiness() ? (", company=" + interest.getCompany() + "]") : "]"));
            }
            existingContacts.addAll(findExisting(interest.getAddress().getZipcode(), interest.getLastName()));
            if (interest.isBusiness()) {
                existingContacts.addAll(findExisting(interest.getAddress().getZipcode(), interest.getCompany()));
            }
            if (isSet(interest.getEmail())) {
                if (log.isDebugEnabled()) {
                    log.debug("findExisting() checking email address [value=" + interest.getEmail() + "]");
                }
                existingContacts.addAll(contacts.findByEmail(interest.getEmail()));
            }
        } else if (interest.getAddress() != null && isNotSet(interest.getAddress().getZipcode())) {
            if (log.isDebugEnabled()) {
                log.debug("findExisting() missing zipcode, trying for name [value=" + interest.getLastName() + "]");
            }
            existingContacts.addAll(findSimilarByLastname(interest));
        }
        // remove duplicates
        Set<Long> added = new java.util.HashSet<Long>();
        for (Iterator<Contact> i = existingContacts.iterator(); i.hasNext();) {
            Contact next = i.next();
            if (added.contains(next.getContactId())) {
                i.remove();
            } else {
                added.add(next.getContactId());
            }
        }
        return existingContacts;
    }

    private List<Contact> findSimilarByLastname(Interest interest) {
        List<Contact> result = new ArrayList<Contact>();
        List<ContactSearchResult> searchResult = contactQueries.find("contact", null, null, interest.getLastName(), false);
        if (!searchResult.isEmpty()) {
            for (int i = 0, j = searchResult.size(); i < j; i++) {
                ContactSearchResult csr = searchResult.get(i);
                Contact found = contacts.loadContact(csr.getContactId());
                if (found != null) {
                    result.add(found);
                }
            }
        }
        return result;

    }

    public List<Contact> findByEmail(String email) {
        return contacts.findByEmail(email);
    }
}
