/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 24-Jan-2008 
 * 
 */
package com.osserp.core.model.system;

import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.employees.Employee;
import com.osserp.core.system.ManagingDirector;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class ManagingDirectorImpl extends AbstractOption implements ManagingDirector {

    public ManagingDirectorImpl() {
        super();
    }

    protected ManagingDirectorImpl(ManagingDirector o) {
        super(o);
    }

    public ManagingDirectorImpl(Employee user, SystemCompany company, String name) {
        super((Long) null, company.getId(), name, null, user.getId());
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "referenceId", getReference());
        return map;
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        return root;
    }

    @Override
    public Object clone() {
        return new ManagingDirectorImpl(this);
    }
}
