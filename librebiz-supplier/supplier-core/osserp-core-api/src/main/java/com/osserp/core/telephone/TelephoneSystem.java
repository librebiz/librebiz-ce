/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 1:14:27 PM 
 * 
 */
package com.osserp.core.telephone;

import com.osserp.common.Entity;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneSystem extends Entity {

    /**
     * Returns the systemType
     * @return systemType
     */
    TelephoneSystemType getSystemType();

    /**
     * Sets the systemType
     * @param systemType
     */
    void setSystemType(TelephoneSystemType systemType);

    /**
     * Returns the branchId
     * @return branchId
     */
    Long getBranchId();

    /**
     * Sets the branchId
     * @param branchId
     */
    void setBranchId(Long branchId);

    /**
     * Provides the name
     * @return name
     */
    String getName();

    /**
     * Sets the name
     * @param name
     */
    void setName(String name);

    /**
     * Provides the ipRange
     * @return ipRange
     */
    String getIpRange();

    /**
     * Sets the ipRange
     * @param ipRange
     */
    void setIpRange(String ipRange);

    /**
     * Provides the hostname
     * @return hostname
     */
    String getHostname();

    /**
     * Sets the hostname
     * @param hostname
     */
    void setHostname(String hostname);

    /**
     * Indicates that the telephone system is active
     * @return active
     */
    boolean isActive();

    /**
     * Sets telephone system active
     * @param active
     */
    void setActive(boolean active);
}
