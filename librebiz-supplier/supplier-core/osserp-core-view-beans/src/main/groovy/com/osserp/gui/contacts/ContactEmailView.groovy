/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 9, 2009 12:47:27 PM 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.Option
import com.osserp.common.PermissionException
import com.osserp.common.mail.EmailAddress
import com.osserp.common.util.EmailValidator

import com.osserp.core.Options
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactManager

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ContactEmailView extends AbstractContactViewAware {
    private static Logger logger = LoggerFactory.getLogger(ContactEmailView.class.getName())

    Set<String> managedDomains = new HashSet<String>()
    List<EmailAddress> aliases = []
    List<Option> emailTypes = []
    String[] systemDomainMailPermissions
    Long selectedType
    Contact contact

    ContactEmailView() {
        super()
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest && contactView?.bean) {
            systemDomainMailPermissions = systemConfigManager.getPermissionConfig('mailDomainAdministration')
            managedDomains = systemConfigManager.getManagedMailDomainNames()
            List<Option> tps = getOptions(Options.EMAIL_TYPES)
            tps.each {
                if (managedDomains || (it.id != EmailAddress.INTERNAL && it.id != EmailAddress.ALIAS)) {
                    emailTypes << it
                }
            }
            contact = contactManager.find(contactView.bean.contactId)
            list = contact?.emails
            logger.debug("initRequest: initialized email addresses [contact=${contact?.contactId}, count=${list?.size()}]")
            select()
            if (!list) {
                enableCreateMode()
            }
        }
    }

    void selectType() {
        String email = form.getString('email')
        if (email) {
            form.setValue('email', email.toLowerCase().trim())
        }
        Long type = form.getLong('type')
        if (type) {
            selectedType = type
            logger.debug("selectType: type selected [id=${selectedType}]")
            if (EmailAddress.ALIAS == selectedType) {
                boolean internalExists = false
                list.each {
                    if (it.internal) {
                        internalExists = true
                    }
                }
                if (!internalExists) {
                    logger.debug("selectType: error: alias selected while internal not exists!")
                    selectedType = null
                    throw new ClientException(ErrorCode.PARENT_ACCOUNT_MISSING)
                }
            }
        } else {
            selectedType = null
            logger.debug("selectType: no type selected")
        }
    }

    boolean isSelectedTypeAlias() {
        EmailAddress.ALIAS == selectedType
    }

    Long getAliasOf() {
        form.getLong('aliasOf')
    }

    void deleteAddress() throws ClientException {
        select()
        if (bean) {
            try {
                checkManagedAccess(bean)
            } catch (PermissionException e) {
                bean = null
                throw new ClientException(e.getMessage())
            }
            if (bean.isInternal() && aliases) {
                throw new ClientException(ErrorCode.DEPENDING_ADDRESS_FOUND)
            }
            contactManager.deleteEmail(contact, bean.id)
            bean = null
            reload()
        }
    }

    @Override
    void save() {

        String emailAddressValue = form.getString('email')
        Long emailTypeValue = form.getLong('type')
        checkManagedMail(emailAddressValue, emailTypeValue)

        if (createMode) {
            contactManager.addEmail(
                    domainEmployee,
                    contact,
                    emailTypeValue,
                    emailAddressValue,
                    form.getBoolean('primary'),
                    form.getLong('aliasOf'))
        } else if (bean) {
            contactManager.updateEmail(
                    domainEmployee,
                    contact,
                    bean,
                    emailTypeValue,
                    emailAddressValue,
                    form.getBoolean('primary'),
                    form.getLong('aliasOf'))
        }
        disableCreateMode()
        disableEditMode()
        reload()
    }

    @Override
    void reload() {
        contactView.refresh()
        form.resetValue('email')
        contact = contactManager.find(contact.contactId)
        list = contact.getEmails()
        if (bean) {
            bean = list.find { it.id == bean.id }
        }
    }

    private void checkManagedAccess(EmailAddress address) throws ClientException, PermissionException {
        if (address) {
            String selectedDomain = EmailValidator.fetchDomainPart(address.email)
            if (selectedDomain && managedDomains.contains(selectedDomain.toLowerCase())) {
                checkPermission(systemDomainMailPermissions)
            }
        }
    }

    private void checkManagedMail(String address, Long type) throws ClientException, PermissionException {
        if (address && systemDomainMailPermissions) {
            String selectedDomain = EmailValidator.fetchDomainPart(address)
            if (selectedDomain) {
                boolean internalAddress = managedDomains.contains(selectedDomain.toLowerCase())
                if (internalAddress) {
                    checkPermission(systemDomainMailPermissions)
                    if (!EmailAddress.INTERNAL.equals(type) && !EmailAddress.ALIAS.equals(type)) {
                        logger.debug("checkManagedMail: domain is managed but type does not match [domain=${selectedDomain}]")
                        throw new ClientException(ErrorCode.TYPE_INVALID)
                    }
                } else if (EmailAddress.INTERNAL.equals(type) || EmailAddress.ALIAS.equals(type)) {
                    logger.debug("checkManagedMail: domain is not managed but type is [domain=${selectedDomain}, type=${type}]")
                    throw new ClientException(ErrorCode.TYPE_INVALID)
                }
            }
        }
    }

    @Override
    void select() {
        super.select()
        if (contact && bean?.internal) {
            aliases = contact.getEmailAliases(bean)
        }
    }

    protected ContactManager getContactManager() {
        getService(ContactManager.class.getName())
    }
}
