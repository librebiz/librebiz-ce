/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.Status;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessCase;
import com.osserp.core.dao.ProjectTrackings;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.projects.ProjectTrackingImpl;
import com.osserp.core.model.projects.ProjectTrackingRecordTypeImpl;
import com.osserp.core.model.projects.ProjectTrackingTypeImpl;
import com.osserp.core.projects.ProjectTracking;
import com.osserp.core.projects.ProjectTrackingRecordType;
import com.osserp.core.projects.ProjectTrackingType;
import com.osserp.core.projects.ProjectUtil;
import com.osserp.core.requests.Request;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectTrackingsDao extends AbstractDao implements ProjectTrackings {
    private static Logger log = LoggerFactory.getLogger(ProjectTrackingsDao.class.getName());
    private String trackingTable;
    
    public ProjectTrackingsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        trackingTable = getTable("projectTrackings");
    }
    
    public int getTrackingCount(Long reference) {
        if (!isEmpty(reference)) {
            StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
            sql.append(trackingTable).append(" WHERE reference_id = ?");
            Object[] params = { reference };
            int[] types = { Types.BIGINT };
            return jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
        }
        return 0;
    }
    
    public List<ProjectTrackingRecordType> getRecordTypes() {
        return getCurrentSession().createQuery(
                "from " + ProjectTrackingRecordTypeImpl.class.getName() + " o order by o.name").list();
    }

    public List<ProjectTrackingType> getTrackingTypes() {
        return getCurrentSession().createQuery(
                "from " + ProjectTrackingTypeImpl.class.getName() + " o order by o.name").list();
    }

    public ProjectTracking create(
            Status initialStatus,
            Employee user, 
            BusinessCase businessCase, 
            ProjectTrackingType type,
            String name,
            Date startDate,
            Date endDate) {
        Request request = ProjectUtil.fetchRequest(businessCase);
        ProjectTracking result = new ProjectTrackingImpl(initialStatus, user.getId(), request, type, name, startDate, endDate);
        getCurrentSession().saveOrUpdate(ProjectTrackingImpl.class.getName(), result);
        return result;
    }
    
    public void save(ProjectTracking tracking) {
        getCurrentSession().saveOrUpdate(ProjectTrackingImpl.class.getName(), tracking);
    }

    public ProjectTracking find(BusinessCase businessCase) {
        Long requestId = ProjectUtil.getRequestId(businessCase);
        if (log.isDebugEnabled()) {
            log.debug("find() invoked [request=" + requestId + "]");
        }
        StringBuilder hql = new StringBuilder("from ");
        hql.append(ProjectTrackingImpl.class.getName()).append(
                " o where o.reference = :referenceId order by o.startDate desc");
        List result = getCurrentSession().createQuery(hql.toString())
                .setParameter("referenceId", requestId).list();
        return result.isEmpty() ? null : (ProjectTracking) result.get(0);
    }

    public List<ProjectTracking> findAll(BusinessCase businessCase) {
        Long requestId = ProjectUtil.getRequestId(businessCase);
        if (log.isDebugEnabled()) {
            log.debug("find() invoked [request=" + requestId + "]");
        }
        StringBuilder hql = new StringBuilder("from ").append(ProjectTrackingImpl.class.getName())
                .append(" o where o.reference = :referenceId order by o.startDate desc");
        List result = getCurrentSession().createQuery(hql.toString())
                .setParameter("referenceId", requestId).list();
        return result;
    }

    public ProjectTracking getTracking(Long id) {
        return (ProjectTracking) getCurrentSession().load(ProjectTrackingImpl.class.getName(), id);
    }
}
