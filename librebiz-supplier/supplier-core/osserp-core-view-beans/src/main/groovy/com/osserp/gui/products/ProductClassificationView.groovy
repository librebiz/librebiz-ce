/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 22, 2014 
 * 
 */
package com.osserp.gui.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.groovy.web.MenuItem

import com.osserp.core.products.Product
import com.osserp.core.products.ProductManager

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ProductClassificationView extends AbstractProductClassificationView {
    private static Logger logger = LoggerFactory.getLogger(ProductClassificationView.class.getName())

    private static final String CONFIG_PERMISSIONS =
        'product_create,product_edit,product_selections_admin,executive,organisation_admin'
    
    ProductClassificationView() {
        super()
        enableAutoreload()
        dependencies = ['productView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            bean = env.productView?.product
            if (bean) {
                if (bean.types.size() == 1) {
                    Long id = bean.types.get(0).id
                    setType(id)
                    setGroup(bean.group?.id)
                    if (bean.category) {
                        setCategory(bean.category.id)
                    }
                }
                logger.debug("initRequest: done [product=${bean.productId}]")
            }
        }
    }

    @Override
    void save() {
        if (bean && !selectionTarget) {
            bean = productManager.updateClassification(bean, selectedType, selectedGroup, selectedCategory)
            env.productView?.reload()
        }
    }

    void removeType() {
        if (bean) {
            Long id = form.getLong('id')
            def type = typeSelections.find { it.id == id }
            bean = productManager.removeType(bean, type)
            env.productView?.reload()
        }
    }
    
    @Override
    protected void createCustomNavigation() {
        if (isPermissionGrant(CONFIG_PERMISSIONS)) {
            nav.defaultNavigation = [ exitLink, setupLink, homeLink ]
        }
    }

    protected ProductManager getProductManager() {
        getService(ProductManager.class.getName())
    }
}
