/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.mail

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.dms.DocumentData
import com.osserp.common.mail.Attachment
import com.osserp.common.mail.ReceivedMail
import com.osserp.common.mail.ReceivedMailInfo
import com.osserp.common.util.FileUtil
import com.osserp.common.web.tags.TagUtil

import com.osserp.core.BusinessCase
import com.osserp.core.contacts.Contact
import com.osserp.core.mail.FetchmailInboxManager
import com.osserp.core.mail.MailMessage

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class FetchmailInboxView extends AbstractFetchmailInboxView {
    private static Logger logger = LoggerFactory.getLogger(FetchmailInboxView.class.getName())

    public FetchmailInboxView() {
        super()
        headerName = 'inboxLabel'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest || reloadRequest) {
            adminMode = isPermissionGrant(FetchmailInboxManager.INBOX_ADMIN_PERMISSIONS)
            loadList()
        }
    }

    @Override
    protected void loadList() {
        list = fetchmailInboxManager.findByStatus(domainUser, MailMessage.STATUS_NEW)
    }

    @Override
    void select() {
        super.select()
        if (receivedMail && !receivedMail.businessId) {
            enableCreateMode()
        } else {
            disableCreateMode()
        }
    }

    @Override
    Integer getListCount() {
        if (selectedUser) {
            return (list.findAll { it.userId == selectedUser }).size()
        }
        return super.getListCount()
    }

    void selectUser() {
        selectedUser = form.getLong('userId')
    }

    @Override
    void save() {
        if (receivedMail) {
            Long businessId = form.getLong('businessId')
            BusinessCase businessCase = businessId ? businessCaseSearch.findBusinessCase(businessId) : null
            if (!businessCase) {
                throw new ClientException(ErrorCode.ID_INVALID)
            }
            assignBusinessCase(
                businessCase.customer.primaryKey,
                Contact.CUSTOMER,
                businessCase.primaryKey)
            disableCreateMode()
            loadList()
        }
    }
}
