/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Jan-2007 10:58:20 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.common.Option;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.DeliveryCondition;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Records;
import com.osserp.core.products.Product;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseOrderImpl extends AbstractOrder implements PurchaseOrder {

    private String supplierReferenceNumber = null;
    private Date supplierReferenceDate = null;
    
    private Long carryoverFrom = null;

    private List<Invoice> invoices = new ArrayList<Invoice>();
    private DeliveryCondition deliveryCondition = null;

    /**
     * Default constructor required by Serializable
     */
    protected PurchaseOrderImpl() {
        super();
    }

    /**
     * Creates a new purchase order
     * @param id
     * @param type
     * @param bookingType
     * @param company
     * @param supplier
     * @param createdBy
     * @param businessCaseId
     * @param taxRate
     * @param reducedTaxRate
     */
    public PurchaseOrderImpl(
            Long id,
            RecordType type,
            BookingType bookingType,
            Long company,
            Long branchId,
            Supplier supplier,
            Employee createdBy,
            Long businessCaseId,
            Double taxRate,
            Double reducedTaxRate) {
        super(
                id,
                company,
                branchId,
                type,
                bookingType,
                null,
                supplier,
                createdBy,
                businessCaseId,
                taxRate,
                reducedTaxRate,
                true,
                true);
        if (getPaymentAgreement() == null) {
            getPaymentAgreements().add(createPaymentAgreement());
        }
    }

    /**
     * Creates a new purchase order as copy from another order
     * @param id
     * @param createdBy
     * @param other
     */
    public PurchaseOrderImpl(Long id, Long createdBy, PurchaseOrder other) {
        super(
                id,
                other.getCompany(),
                other.getBranchId(),
                other.getType(),
                other.getBookingType(),
                null,
                other.getContact(),
                null,
                other.getBusinessCaseId(),
                other.getAmounts().getTaxRate(),
                other.getAmounts().getReducedTaxRate(),
                true,
                true,
                other.getItems());
        setCreatedBy(createdBy);
        if (getPaymentAgreement() == null) {
            getPaymentAgreements().add(createPaymentAgreement());
        }
    }

    /**
     * Creates a new purchase order as carry over from another order
     * @param id
     * @param createdBy
     * @param other
     * @param items
     */
    public PurchaseOrderImpl(
            Long id,
            Long createdBy,
            PurchaseOrder other,
            List<Item> items) {
        super(
                id,
                other.getCompany(),
                other.getBranchId(),
                other.getType(),
                other.getBookingType(),
                null,
                other.getContact(),
                null,
                other.getBusinessCaseId(),
                other.getAmounts().getTaxRate(),
                other.getAmounts().getReducedTaxRate(),
                true,
                true,
                items);
        setCreatedBy(createdBy);
        setSupplierReferenceNumber(other.getSupplierReferenceNumber());
        this.carryoverFrom =
                other.getCarryoverFrom() != null
                        && other.getCarryoverFrom() != 0 ?
                        other.getCarryoverFrom() : other.getId();
        if (getPaymentAgreement() == null) {
            getPaymentAgreements().add(createPaymentAgreement());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.BusinessNoteAware#getBusinessId()
     */
    public Long getBusinessId() {
        return getId();
    }

    public Long getCarryoverFrom() {
        return carryoverFrom;
    }

    protected void setCarryoverFrom(Long carryoverFrom) {
        this.carryoverFrom = carryoverFrom;
    }

    public String getCarryoverDisplay() {
        return (carryoverFrom == null ? "" : Records.createNumber(
                getType(), carryoverFrom, isInternal()));
    }

    public Date getSupplierReferenceDate() {
        return supplierReferenceDate;
    }

    public void setSupplierReferenceDate(Date supplierReferenceDate) {
        this.supplierReferenceDate = supplierReferenceDate;
    }

    public String getSupplierReferenceNumber() {
        return supplierReferenceNumber;
    }

    public void setSupplierReferenceNumber(String supplierReferenceNumber) {
        this.supplierReferenceNumber = supplierReferenceNumber;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        if (invoices != null) {
            this.invoices = invoices;
        }
    }

    @Override
    protected RecordPaymentAgreement createPaymentAgreement() {
        return new PurchaseOrderPaymentAgreementImpl(this);
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new PurchaseOrderItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        return new PurchaseOrderInfoImpl(this, info, user);
    }

    @Override
    protected Item createOptionItem(
            Product product,
            String customName,
            Double quantity, 
            BigDecimal price, 
            Date priceDate, 
            BigDecimal partnerPrice, 
            BigDecimal purchasePrice,
            String note, 
            boolean includePrice) {
        throw new UnsupportedOperationException("purchase orders does not support options");
    }

    @Override
    public List<Item> getOptions() {
        return new ArrayList<Item>();
    }

    @Override
    public boolean isSales() {
        return false;
    }

    public boolean isDeliveryConfirmed() {
        for (int i = 0, j = getItems().size(); i < j; i++) {
            OrderItem nextItem = (OrderItem) getItems().get(i);
            if (!nextItem.isDeliveryConfirmed()) {
                return false;
            }
        }
        return true;
    }

    public DeliveryCondition getDeliveryCondition() {
        return deliveryCondition;
    }

    public void setDeliveryCondition(DeliveryCondition deliveryCondition) {
        this.deliveryCondition = deliveryCondition;
    }
}
