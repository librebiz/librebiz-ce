<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-app" prefix="oa" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="list" value="${requestScope.latestRecords}"/>
<c:set var="exit" value="${requestScope.latestRecordsExit}"/>
<c:set var="viewExit" value="${requestScope.latestRecordsViewExit}"/>

<c:if test="${!empty list}">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="recentRecordsLabel" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <tbody>
                        <c:forEach var="record" items="${list}">
                            <tr>
                                <td class="recordId">
                                    <oa:recordLink record="${record}" exit="${exit}" viewExit="${viewExit}">
                                        <o:out value="${record.number}"/>
                                    </oa:recordLink>
                                </td>
                                <td class="recordDate"><o:date value="${record.created}" /></td>
                                <td><o:out value="${record.contact.displayName}" limit="30"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</c:if>