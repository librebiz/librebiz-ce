/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 19, 2006 2:41:59 PM 
 * 
 */
package com.osserp.gui.client.products;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DmsDocument;

import com.osserp.core.finance.ListItem;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductType;
import com.osserp.core.products.ProductTypeConfig;
import com.osserp.core.views.StockReportDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductView extends StockSelectingView {

    /**
     * (Re-)Initializes view with an product
     * @param product
     * @param exitTarget
     * @param exitId
     */
    public void load(Product product, String exitTarget, String exitId);

    /**
     * Reloads current selected product from backend and re-initializes documents
     */
    public void reload();

    /**
     * Removes type from current selected product
     * @param type
     * @throws ClientException if type alternative not assigned
     */
    public void removeType(ProductType type) throws ClientException;

    /**
     * Updates current selcted product classification
     * @param type
     * @param group
     * @param category
     * @throws ClientException if type-group-category relation is amiguous
     */
    public void updateClassification(ProductTypeConfig type, ProductGroupConfig group, ProductCategoryConfig category) throws ClientException;

    /**
     * Changes serial number flags of current product
     * @throws ClientException if product validation failed
     */
    public void changeSerialnumberFlag() throws ClientException;

    /**
     * Indicates that product is referenced in any record.
     * @return productReferenced
     */
    public boolean isProductReferenced();

    /**
     * Updates current products document flag
     * @param docType (e.g. datasheet or other)
     * @param available flag to set
     * @throws ClientException if validation of form values failed
     * @throws PermissionException if current user has no permissions to update
     */
    public void updateDocumentFlag(Long docType, boolean available) throws ClientException, PermissionException;

    /**
     * Provides current selected bean as product
     * @return product
     */
    public Product getProduct();

    /**
     * Provides all product goups for create actions
     * @return productGroups
     */
    public List<ProductGroup> getProductGroups();

    /**
     * Indicates that view is in sales volume display mode
     * @return true if so
     */
    public boolean isSalesVolumeMode();

    /**
     * Enables/disables sales volume display mode
     * @param true if volume xml is loaded from the backend
     */
    public void setSalesVolumeMode(boolean enable);

    /**
     * Provides the sales volume
     * @return sales volume
     */
    public List<ListItem> getSalesVolume();

    /**
     * Provides the sales volume average price
     * @return average price
     */
    public Double getSalesVolumePriceAverage();

    /**
     * Provides the total quantity of the sales volume
     * @return quantity
     */
    public Double getSalesVolumeQuantity();

    /**
     * Indicates that stock report view is enabled
     * @return stockReportMode
     */
    public boolean isStockReportMode();

    /**
     * Enables stock report display
     * @param exitTarget
     * @throws ClientException if no stock selected
     */
    public void enableStockReport(String exitTarget) throws ClientException;

    /**
     * Disables stock report display
     * @return exitTarget
     */
    public String disableStockReport();

    /**
     * Provides the stock report
     * @return stockReport
     */
    public List<StockReportDisplay> getStockReport();

    /**
     * Provides list of documents for this product
     * @return docs
     */
    public List<DmsDocument> getDocs();
}
