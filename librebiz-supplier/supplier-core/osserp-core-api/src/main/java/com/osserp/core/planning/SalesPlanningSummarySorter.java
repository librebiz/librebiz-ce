/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Nov 12, 2010 11:39:59 AM 
 * 
 */
package com.osserp.core.planning;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.Comparators;

/**
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public class SalesPlanningSummarySorter {
    private static Logger log = LoggerFactory.getLogger(SalesPlanningSummarySorter.class.getName());

    public static final String[] COMPARATORS = {
            "byProductId",
            "byProductIdReverse",
            "byProductName",
            "byProductNameReverse",
            "byQuantity",
            "byQuantityReverse",
            "byStock",
            "byStockReverse",
            "byReceipt",
            "byReceiptReverse",
            "byExpected",
            "byExpectedReverse"
    };

    private Map<String, Comparator> comparators = null;

    private static SalesPlanningSummarySorter cinstance = null;

    @SuppressWarnings("unchecked")
    private SalesPlanningSummarySorter() {
        super();
        comparators = Collections.synchronizedMap(new HashMap<>());
        comparators.put("byProductId", createByProductIdComparator(false));
        comparators.put("byProductIdReverse", createByProductIdComparator(true));
        comparators.put("byProductName", createByProductNameComparator(false));
        comparators.put("byProductNameReverse", createByProductNameComparator(true));
        comparators.put("byQuantity", createByQuantityComparator(false));
        comparators.put("byQuantityReverse", createByQuantityComparator(true));
        comparators.put("byStock", createByStockComparator(false));
        comparators.put("byStockReverse", createByStockComparator(true));
        comparators.put("byReceipt", createByReceiptComparator(false));
        comparators.put("byReceiptReverse", createByReceiptComparator(true));
        comparators.put("byExpected", createByExpectedComparator(false));
        comparators.put("byExpectedReverse", createByExpectedComparator(true));
    }

    public static SalesPlanningSummarySorter instance() {
        if (cinstance == null) {
            cinstance = new SalesPlanningSummarySorter();
        }
        return cinstance;
    }

    @SuppressWarnings("unchecked")
    public void sort(String name, List<SalesPlanningSummary> items) {
        if (log.isDebugEnabled()) {
            log.debug("sort() invoked [name=" + name + "]");
        }
        Comparator c = comparators.get(name);
        if (c == null) {
            c = comparators.get("byName");
            if (log.isDebugEnabled()) {
                log.debug("sort() comparator '" + name + "' not found");
            }
        }
        Collections.sort(items, c);
    }

    private Comparator createByProductIdComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesPlanningSummary) a).getProductId(), ((SalesPlanningSummary) b).getProductId(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesPlanningSummary) a).getProductId(), ((SalesPlanningSummary) b).getProductId(), false);
            }
        };
    }

    private Comparator createByProductNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesPlanningSummary) a).getProductName(), ((SalesPlanningSummary) b).getProductName(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesPlanningSummary) a).getProductName(), ((SalesPlanningSummary) b).getProductName(), false);
            }
        };
    }

    private Comparator createByQuantityComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesPlanningSummary) a).getQuantity(), ((SalesPlanningSummary) b).getQuantity(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesPlanningSummary) a).getQuantity(), ((SalesPlanningSummary) b).getQuantity(), false);
            }
        };
    }

    private Comparator createByStockComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesPlanningSummary) a).getStock(), ((SalesPlanningSummary) b).getStock(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesPlanningSummary) a).getStock(), ((SalesPlanningSummary) b).getStock(), false);
            }
        };
    }

    private Comparator createByReceiptComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesPlanningSummary) a).getReceipt(), ((SalesPlanningSummary) b).getReceipt(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesPlanningSummary) a).getReceipt(), ((SalesPlanningSummary) b).getReceipt(), false);
            }
        };
    }

    private Comparator createByExpectedComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesPlanningSummary) a).getExpected(), ((SalesPlanningSummary) b).getExpected(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesPlanningSummary) a).getExpected(), ((SalesPlanningSummary) b).getExpected(), false);
            }
        };
    }

}
