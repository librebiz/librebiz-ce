/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 21, 2011 2:35:29 PM 
 * 
 */
package com.osserp.core.dao;

import com.osserp.core.contacts.ZipcodeSearch;

/**
 * 
 * @author jg <jg@osserp.com>
 */
public interface ZipcodeSearches {

    /**
     * Load the zipcodeSearch with give id
     * @param id
     * @return zipcodeSearch
     */
    ZipcodeSearch load(Long id);

    /**
     * Saves the given zipcodeSearch
     * @param zipcodeSearch
     */
    void save(ZipcodeSearch zipcodeSearch);

    /**
     * Count the zipcode searches for the current day
     * @return count of todays searches
     */
    Integer countTodaysSearches();
}
