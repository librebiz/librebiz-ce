/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 26, 2016 
 * 
 */
package com.osserp.gui.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.finance.BillingType
import com.osserp.core.finance.Downpayment
import com.osserp.core.finance.PaymentAwareRecord

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.accounting.AbstractBankAccountReferenceView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractRecordPaymentView extends AbstractBankAccountReferenceView {

    private static Logger logger = LoggerFactory.getLogger(AbstractRecordPaymentView.class.getName())

    List<BillingType> paymentTypes = []
    BillingType selectedPaymentType

    PaymentAwareRecord record
    PaymentAwareRecord resultingRecord

    boolean cancellationMode = false
    boolean customPaymentMode = false
    boolean creditNoteMode = false
    boolean paymentDeleteMode = false
    boolean recordCloseMode = false
    boolean partialPayment = false
    boolean clearDecimalAmountMode = false
    Double clearDecimalAmountLimit = 0.0d
    protected Long lastRecordId

    protected AbstractRecordPaymentView() {
        super()
        overrideExitTarget = true
    }

    void enablePaymentDeleteMode() {
        paymentDeleteMode = true
    }   

    abstract void selectPaymentType();

    protected abstract String getDefaultExitLink();

    protected abstract String getPaymentAwareRecordViewName();

    protected abstract String createCancellation() throws ClientException;

    protected abstract String createCreditNote() throws ClientException;

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {

            if (env[paymentAwareRecordViewName]?.bean) {
                record = env[paymentAwareRecordViewName].bean
                if (!record) {
                    logger.warn("initRequest: not bound [user=${user?.id}, name=${paymentAwareRecordViewName}]")
                    throw new ViewContextException(this)
                }
                if (!record.isUnchangeable()) {
                    logger.warn("initRequest: record changeable [user=${user?.id}, name=${record.id}]")
                    throw new ViewContextException(ErrorCode.RECORD_CHANGEABLE)
                }
            }
            lastRecordId = record?.id
            if (record?.company) {
                selectCompany(record.company, record.branchId)
                if (selectedCompany) {
                    bankAccounts = selectedCompany.bankAccounts
                }
            }
            if (billingViewExists()) {
                addExitTarget('/projectBillingForward.do')
            } else {
                addExitTarget(defaultExitLink)
            }
            try {
                clearDecimalAmountLimit = Double.valueOf(getSystemProperty(BillingType.CLEARING_LIMIT_PROPERTY))
            } catch (Exception e) {
                logger.debug("initRequest: Undefined system property: ${BillingType.CLEARING_LIMIT_PROPERTY}")
            }

            logger.debug("initRequest: done [record=${record.id}, bankAccounts=${bankAccounts?.size()}]")
        }
    }

    final String getDefaultExitTarget() {
        createTarget(defaultExitLink)
    }

    Double getRecordAmount() {
        Downpayment dp = getDownpayment()
        if (dp) {
            if (record.taxFree) {
                return dp.amount
            }
            return dp.grossAmount
        }
        return record.amounts.grossAmount
    }

    protected Downpayment getDownpayment() {
        (record instanceof Downpayment) ? record : null
    }

    protected boolean billingViewExists() {
        (record?.contact?.id == env.salesBillingView?.order?.contact?.id)
    }

}
