<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${contactView.bean}" />
<o:form name="employeeForm" url="/employees.do">
	<input type="hidden" name="method" value="updateDetails" />
	<table class="valueTable first33">
		<tbody>
            <c:if test="${empty contact.roleConfigs}">
                <tr>
                    <td style="vertical-align: middle;">
                        <fmt:message key="branch" />
                    </td>
                    <td>
                        <select name="branchId" size="1">
                            <c:forEach var="branch" items="${contactView.availableBranchs}">
                                <option value="<o:out value="${branch.id}"/>">
                                    <o:out value="${branch.name}"/> / <o:out value="${branch.company.name}"/>
                                </option>
                            </c:forEach>
                        </select> 
                    </td>
                </tr>
            </c:if>
			<tr>
				<td style="vertical-align: middle;">
					<fmt:message key="typeLabel" />
				</td>
				<td>
					<oc:select name="typeId" value="${contact.employeeType.id}"	options="employeeTypes" />
				</td>
			</tr>
			<tr>
				<td style="vertical-align: middle;">
					<fmt:message key="entrance" />
				</td>
				<td>
					<input type="text" name="beginDate"	value="<o:date value="${contact.beginDate}"/>" />
				</td>
			</tr>
			<tr>
				<td style="vertical-align: middle;">
					<fmt:message key="until" />
				</td>
				<td>
					<input type="text" name="endDate" value="<o:date value="${contact.endDate}"/>" />
				</td>
			</tr>
			<tr>
				<td style="vertical-align: middle;">
					<fmt:message key="initials" />
				</td>
				<td>
					<input type="text" name="initials" value="<o:out value="${contact.initials}"/>" />
				</td>
			</tr>
			<tr>
				<td style="vertical-align: middle;">
					<fmt:message key="role" />
				</td>
				<td>
					<input type="text" name="role" value="<o:out value="${contact.role}"/>" />
				</td>
			</tr>
			<tr>
				<td style="vertical-align: middle;">
					<fmt:message key="signatureBy" />
				</td>
				<td>
					<%-- TODO replace signatureSource selection with dedicated tag --%>
					<select name="signatureSource" size="1">
						<c:choose>
							<c:when test="${empty contact.signatureSource}">
								<option value="none">
									<fmt:message key="none" />
								</option>
								<option value="" selected="selected">
									<fmt:message key="automatically" />
								</option>
								<option value="role">
									<fmt:message key="role" />
								</option>
								<option value="group">
									<fmt:message key="group" />
								</option>
							</c:when>
							<c:otherwise>
								<option value="">
									<fmt:message key="automatically" />
								</option>
								<c:choose>
									<c:when test="${contact.signatureSource == 'none'}">
										<option value="none" selected="selected">
											<fmt:message key="none" />
										</option>
										<option value="role">
											<fmt:message key="role" />
										</option>
										<option value="group">
											<fmt:message key="group" />
										</option>
									</c:when>
									<c:when test="${contact.signatureSource == 'role'}">
										<option value="none">
											<fmt:message key="none" />
										</option>
										<option value="role" selected="selected">
											<fmt:message key="role" />
										</option>
										<option value="group">
											<fmt:message key="group" />
										</option>
									</c:when>
									<c:otherwise>
										<option value="none">
											<fmt:message key="none" />
										</option>
										<option value="role">
											<fmt:message key="role" />
										</option>
										<option value="group" selected="selected">
											<fmt:message key="group" />
										</option>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
					</select>
				</td>
			</tr>
			<tr>
				<td style="vertical-align: middle;">
					<fmt:message key="costCenter" />
				</td>
				<td>
					<input type="text" name="costCenter" value="<o:out value="${contact.costCenter}"/>" />
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<c:choose>
						<c:when test="${contact.emailAccount}">
							<input type="checkbox" name="emailAccount" checked="checked" />
						</c:when>
						<c:otherwise>
							<input type="checkbox" name="emailAccount" />
						</c:otherwise>
					</c:choose> 
					<span style="margin-left: 10px;"><fmt:message key="emailAccount" /></span> 
					<c:choose>
						<c:when test="${contact.cellularPhone}">
							<span style="margin-left: 10px;">
								<input type="checkbox" name="cellularPhone" checked="checked" />
							</span>
						</c:when>
						<c:otherwise>
							<span style="margin-left: 10px;">
								<input type="checkbox" name="cellularPhone" />
							</span>
						</c:otherwise>
					</c:choose> 
					<span style="margin-left: 10px;"><fmt:message key="cellularPhone" /></span>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<c:choose>
						<c:when test="${contact.businessCard}">
							<input type="checkbox" name="businessCard" checked="checked" />
						</c:when>
						<c:otherwise>
							<input type="checkbox" name="businessCard" />
						</c:otherwise>
					</c:choose> 
					<span style="margin-left: 10px;"><fmt:message key="businessCard" /></span> 
					<c:choose>
						<c:when test="${contact.internationalBusinessCard}">
							<input type="checkbox" name="internationalBusinessCard"	
								style="margin-left: 25px;" checked="checked" />
						</c:when>
						<c:otherwise>
							<input type="checkbox" name="internationalBusinessCard"
								style="margin-left: 25px;" />
						</c:otherwise>
					</c:choose> 
					<span style="margin-left: 10px;"><fmt:message key="international" /></span>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<c:choose>
						<c:when test="${contact.timeRecordingEnabled}">
							<input type="checkbox" name="timeRecordingEnabled" checked="checked" />
						</c:when>
						<c:otherwise>
							<input type="checkbox" name="timeRecordingEnabled" />
						</c:otherwise>
					</c:choose> 
					<span style="margin-left: 10px;"><fmt:message key="timeRecording" /></span>
				</td>
			</tr>
            <tr>
                <td class="row-submit">
                    <a href="<c:url value="/employees.do?method=disableDetailsEdit"/>">
                        <input type="button" value="<fmt:message key="break"/>" class="cancel" />
                    </a>
                </td>
                <td class="row-submit">
                    <o:submit/>
                </td>
            </tr>
		</tbody>
	</table>
</o:form>
