/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Dec 28, 2015 10:15:11 AM
 *
 */
package com.osserp.core.dao.records.impl

import groovy.sql.Sql

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.OptionsCache
import com.osserp.common.util.DateUtil
import com.osserp.common.util.NumberUtil
import com.osserp.common.util.StringUtil

import com.osserp.core.Options
import com.osserp.core.dao.SystemConfigs
import com.osserp.core.employees.Employee
import com.osserp.core.finance.RecordNumberCreator
import com.osserp.core.finance.RecordType

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class NumberCreatorByYearlySequence extends AbstractSequenceNumberCreator implements RecordNumberCreator {
    private static Logger logger = LoggerFactory.getLogger(NumberCreatorByYearlySequence.class.getName())

    OptionsCache options
    SystemConfigs systemConfigs

    synchronized Long create(Employee user, Long company, RecordType type, Date created) {
        Long result
        Map sequence = getSequence(company, type)
        if (sequence?.id && sequence?.name) {
            Long currentYear = Long.valueOf(DateUtil.getCurrentYear())
            Long year = created != null ? DateUtil.getYear(created) : currentYear;
            List<RecordType> other = []
            if (!sequence.dedicated) {
                other = getTypesBySequence(sequence.name)
            }
            List<Long> existing = getExistingIds(type, other, year)
            Long latest = null
            if (existing) {
                latest = existing.get(0)
            }
            if (!existing && !latest) {
                restartSequence(sequence.name, createSequenceStart(sequence, year))
            }
            if (latest && existing) {
                if (year < currentYear) {
                    result = latest + 1L;
                } else {
                    if (type.numberCreatorName) {
                        List<Long> ignorables = getIgnorableIds(type.numberCreatorName)
                        if (ignorables) {
                            ignorables.each {
                                existing.add(it)
                            }
                        }
                    }
                    Long seqStart = createSequenceStart(sequence, year)
                    result = getFreeId(seqStart, latest, existing as Set)
                    if (!result) {
                        result = latest + 1L;
                    }
                }
            }
            if (!result) {
                result = getNextSequenceValue(sequence.name)
            }
        }
        return result
    }

    protected Long getFreeId(Long start, Long latest, Set<Long> existing) {
        for (Long i = start; i <= latest; i++) {
            if (!existing.contains(i)) {
                return i
            }
        }
        return null
    }

    protected Long createSequenceStart(Map sequence, Long year) {
        StringBuilder seqval = new StringBuilder(year.toString())
        int digits = sequence.digits ?: 4
        for (int i = 0; i < digits; i++) {
            seqval.append('0')
        }
        Long seqstart = NumberUtil.createLong(seqval.toString())
        return (seqstart + 1L)
    }

    protected List<Long> getExistingIds(RecordType type, List<RecordType> types, Long year) {
        List<Long> result = []
        String query = createExistingIdQuery(type, types)
        String y = year.toString()
        if (query) {
            try {
                Sql sql = getSql()
                sql.eachRow(query) { obj ->
                    Long id = obj.id
                    if (id.toString().startsWith(y)) {
                        result << id
                    }
                }
            } catch (Exception e) {
                logger.error("getExistingIds: failed [query=${query}, message=${e.message}]", e)
            }
        }
        return result
    }

    protected List<Long> getIgnorableIds(String sequenceName) {
        List<Long> result = []
        String ignorableString = systemConfigs?.getSystemProperty("${sequenceName}Ignorables")
        if (ignorableString) {
            List<String> ids = StringUtil.getTokenList(ignorableString, ',')
            ids.each {
                Long i = NumberUtil.createLong(it)
                if (i) {
                    result << i
                }
            }
        }
        return result
    }

    protected String createExistingIdQuery(RecordType type, List<RecordType> types) {
        if (!type.tableName) {
            return ''
        }
        StringBuilder query = new StringBuilder(createIdQuery(type))
        if (types) {
            types.each { RecordType rt ->
                if (rt.id != type.id) {
                    query.append(' UNION ').append(createIdQuery(rt))
                }
            }
        }
        query.append(' ORDER BY id DESC')
        //logger.debug("createExistingIdQuery: done [sql=${query.toString()}]")
        query.toString()
    }

    protected String createIdQuery(RecordType type) {
        if (!type.tableName) {
            return ''
        }
        return "SELECT id FROM ${type.tableName}"
    }

    protected List<RecordType> getTypesBySequence(String sequenceName) {
        List<Map> sequenceList = getSequenceListBySameName(sequenceName)
        List<RecordType> result = []
        sequenceList.each {
            Long typeId = it.type_id
            if (typeId) {
                RecordType type = options.getMapped(Options.RECORD_TYPES, typeId)
                if (type) {
                    result << type
                }
            }
        }
        return result
    }
}
