<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="financialData"/> <fmt:message key="and"/> <fmt:message key="bankAccounts"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                    <o:permission role="accounting_reports,accounting_bank_display,accounting_bank_documents,executive_accounting,executive" info="permissionViewFinanceReports">
                        <c:if test="${!empty view.selectedCompany}">
                            <c:choose>
                                <c:when test="${view.currentYearOnly}">
                                    <tr>
                                        <td><fmt:message key="financialData"/></td>
                                        <td colspan="2">
                                            <v:link url="/accounting/annualReport/forward?company=${view.selectedCompany.id}&year=${view.year}&exit=/accounting/accountingIndex/forward" title="currentYear">
                                                <o:out value="${view.year}"/>
                                            </v:link>
                                        </td>
                                    </tr>
                                </c:when>
                                <c:otherwise>
                                    <tr>
                                        <td>
                                            <v:link url="/accounting/annualReport/forward?company=${view.selectedCompany.id}&year=${view.year}&exit=/accounting/accountingIndex/forward" title="currentYear">
                                                <fmt:message key="currentYearLabel"/>
                                            </v:link>
                                        </td>
                                        <td colspan="2">
                                            <c:forEach var="obj" items="${view.selectedCompany.accountingYears}" varStatus="s">
                                                <v:link url="/accounting/annualReport/forward?company=${view.selectedCompany.id}&year=${obj.year}&exit=/accounting/accountingIndex/forward">
                                                    <c:if test="${view.year != obj.year}">
                                                        <c:choose>
                                                            <c:when test="${s.last}">
                                                                <o:out value="${obj.year}"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <o:out value="${obj.year}"/>,
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:if>
                                                </v:link>
                                            </c:forEach>                                        
                                        </td>
                                    </tr>
                                </c:otherwise>
                            </c:choose>
                            <tr><td colspan="3"> </td></tr>
                        </c:if>
                    </o:permission>
                    <c:choose>
                        <c:when test="${empty view.bankAccounts}">
                            <tr>
                                <td colspan="3"><span class="error"><fmt:message key="bankAccountRequiredButMissing" /></span> <span style="margin-left: 20px;"> <fmt:message key="bankAccountRequiredButMissingHint" /></span></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="obj" items="${view.bankAccounts}">
                                <tr>
                                    <o:permission role="accounting,accounting_bank_display,accounting_bank_documents,organisation_admin" info="permissionViewBankAccounts">
                                        <td>
                                            <v:link url="/accounting/paymentRecords/forward?account=${obj.id}&exit=/accounting/accountingIndex/forward">
                                                <o:out value="${obj.name}" />
                                            </v:link>
                                        </td>
                                        <o:permission role="accounting_bank_documents,organisation_admin" info="permissionViewBankAccounts">
                                            <td><o:out value="${obj.bankAccountNumberIntl}" /></td>
                                            <td class="right">
                                                <o:permission role="accounting_bank_documents,organisation_admin" info="permissionViewBankAccounts">
                                                    <v:link url="/accounting/bankAccountDocument/forward?id=${obj.id}&exit=/accounting/accountingIndex/forward">
                                                        <o:img name="pdfIcon"/>
                                                    </v:link>
                                                </o:permission>
                                                <o:forbidden role="accounting_bank_documents,organisation_admin" info="permissionViewBankAccounts">
                                                    <o:img name="toggleStoppedFalseIcon"/>
                                                </o:forbidden>
                                            </td>
                                        </o:permission>
                                        <o:forbidden role="accounting_bank_documents,organisation_admin" info="permissionViewBankAccounts">
                                            <td colspan="2"><o:out value="${obj.bankAccountNumberIntl}" /></td>
                                        </o:forbidden>
                                    </o:permission>
                                    <o:forbidden role="accounting,accounting_bank_display,accounting_bank_documents,organisation_admin">
                                        <td><o:out value="${obj.name}" /></td>
                                        <td colspan="2"><o:out value="${obj.bankAccountNumberIntl}" /></td>
                                    </o:forbidden>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </tbody>
            </table>
        </div>
    </div>
</div>
