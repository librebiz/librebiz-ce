/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 2:28:48 PM 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.Dependency;
import com.osserp.core.FcsAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFcsAction extends OptionImpl implements FcsAction {
    private static final long serialVersionUID = 42L;

    private Long groupId = null;
    private Integer status = Constants.INT_NULL;
    private boolean displayEverytime = false;
    private boolean noteRequired = false;
    private boolean performAction = false;
    private boolean performOnStart = false;
    private String startActionName;
    private boolean performAtEnd = false;
    private String endActionName;
    private boolean performEvents = false;
    private Long eventConfigId = null;
    private boolean throwable = false;

    private boolean cancelling = false;
    private boolean closingUnconditionally = false;
    private boolean stopping = false;
    private boolean displayReverse = false;
    private boolean triggered = false;
    private boolean manualSelectionDisabled = false;
    private boolean importAction = false;
    private String permissions = null;

    private Set<Long> dependencies = new HashSet<Long>();
    private List<Dependency> dependencyList = new ArrayList<Dependency>();

    /**
     * Default constructor to implement serializable interface
     */
    protected AbstractFcsAction() {
        super();
    }

    /**
     * Creates a new action
     * @param name
     * @param groupId
     * @param description
     * @param eventConfigId
     * @param displayEverytime
     */
    protected AbstractFcsAction(
            String name,
            Long groupId,
            String description,
            Long eventConfigId,
            boolean displayEverytime) {
        super((Long) null, name, description);
        this.groupId = groupId;
        this.displayEverytime = displayEverytime;
        this.eventConfigId = eventConfigId;
    }

    /**
     * Creates a new action
     * @param name
     * @param groupId
     * @param description
     * @param status
     * @param eventConfigId
     * @param displayEverytime
     * @param noteRequired
     * @param performAction
     * @param performOnStart
     * @param startActionName
     * @param performAtEnd
     * @param endActionName
     * @param throwable
     * @param cancelling
     * @param stopping
     * @param displayReverse
     * @param endOfLife
     * @param triggered
     */
    protected AbstractFcsAction(
            String name,
            Long groupId,
            String description,
            Integer status,
            Long eventConfigId,
            boolean displayEverytime,
            boolean noteRequired,
            boolean performAction,
            boolean performOnStart,
            String startActionName,
            boolean performAtEnd,
            String endActionName,
            boolean throwable,
            boolean cancelling,
            boolean stopping,
            boolean displayReverse,
            boolean endOfLife,
            boolean triggered) {
        super((Long) null, name, description);
        setEndOfLife(endOfLife);
        this.groupId = groupId;
        this.status = status;
        this.eventConfigId = eventConfigId;
        this.displayEverytime = displayEverytime;
        this.noteRequired = noteRequired;
        this.performAction = performAction;
        this.performOnStart = performOnStart;
        this.startActionName = startActionName;
        this.performAtEnd = performAtEnd;
        this.endActionName = endActionName;
        this.throwable = throwable;
        this.cancelling = cancelling;
        this.stopping = stopping;
        this.displayReverse = displayReverse;
        this.triggered = triggered;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public boolean isDisplayEverytime() {
        return displayEverytime;
    }

    public void setDisplayEverytime(boolean displayEverytime) {
        this.displayEverytime = displayEverytime;
    }

    public boolean isNoteRequired() {
        return noteRequired;
    }

    public void setNoteRequired(boolean noteRequired) {
        this.noteRequired = noteRequired;
    }

    public boolean isPerformAction() {
        return performAction;
    }

    public void setPerformAction(boolean performAction) {
        this.performAction = performAction;
    }

    public boolean isPerformOnStart() {
        return performOnStart;
    }

    public void setPerformOnStart(boolean performOnStart) {
        this.performOnStart = performOnStart;
    }

    public String getStartActionName() {
        return startActionName;
    }

    public void setStartActionName(String startActionName) {
        this.startActionName = startActionName;
    }

    public boolean isPerformAtEnd() {
        return performAtEnd;
    }

    public void setPerformAtEnd(boolean performAtEnd) {
        this.performAtEnd = performAtEnd;
    }

    public String getEndActionName() {
        return endActionName;
    }

    public void setEndActionName(String endActionName) {
        this.endActionName = endActionName;
    }

    public boolean isPerformEvents() {
        return performEvents;
    }

    public void setPerformEvents(boolean performEvents) {
        this.performEvents = performEvents;
    }

    public Long getEventConfigId() {
        return eventConfigId;
    }

    public void setEventConfigId(Long eventConfigId) {
        this.eventConfigId = eventConfigId;
    }

    public boolean isThrowable() {
        return throwable;
    }

    public void setThrowable(boolean throwable) {
        this.throwable = throwable;
    }

    public final boolean isClosing() {
        return (status == null ? false :
                status == 100);
    }

    public boolean isClosingUnconditionally() {
        return closingUnconditionally;
    }

    public void setClosingUnconditionally(boolean closingUnconditionally) {
        this.closingUnconditionally = closingUnconditionally;
    }

    public boolean isCancelling() {
        return cancelling;
    }

    public void setCancelling(boolean cancelling) {
        this.cancelling = cancelling;
    }

    public boolean isStopping() {
        return stopping;
    }

    public void setStopping(boolean stopping) {
        this.stopping = stopping;
    }

    public boolean isDisplayReverse() {
        return displayReverse;
    }

    public void setDisplayReverse(boolean displayReverse) {
        this.displayReverse = displayReverse;
    }

    public boolean isTriggered() {
        return triggered;
    }

    public void setTriggered(boolean triggered) {
        this.triggered = triggered;
    }

    public boolean isManualSelectionDisabled() {
        return manualSelectionDisabled;
    }

    public void setManualSelectionDisabled(boolean manualSelectionDisabled) {
        this.manualSelectionDisabled = manualSelectionDisabled;
    }

    public boolean isImportAction() {
        return importAction;
    }

    public void setImportAction(boolean importAction) {
        this.importAction = importAction;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    protected abstract Dependency createDependency(Long dependency);

    public Set<Long> getDependencies() {
        if (dependencies.isEmpty() && !dependencyList.isEmpty()) {
            for (int i = 0, j = dependencyList.size(); i < j; i++) {
                Dependency next = dependencyList.get(i);
                dependencies.add(next.getDependsOn());
            }
        }
        return dependencies;
    }

    public void addDependency(Long dependsOn) {
        Set<Long> currentDepends = getDependencies();
        if (!currentDepends.contains(dependsOn)) {
            dependencyList.add(createDependency(dependsOn));
            dependencies.add(dependsOn);
        }
    }

    public void removeDependency(Long dependency) {
        for (int i = 0, j = dependencyList.size(); i < j; i++) {
            Dependency next = dependencyList.get(i);
            if (next.getDependsOn().equals(dependency)) {
                dependencyList.remove(i);
                break;
            }
        }
    }

    protected List<Dependency> getDependencyList() {
        return dependencyList;
    }

    protected void setDependencyList(List<Dependency> dependencyList) {
        this.dependencyList = dependencyList;
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("groupId", groupId));
        root.addContent(JDOMUtil.createElement("status", status));
        root.addContent(JDOMUtil.createElement("eventConfigId", eventConfigId));
        root.addContent(JDOMUtil.createElement("displayEverytime", displayEverytime));
        root.addContent(JDOMUtil.createElement("noteRequired", noteRequired));
        root.addContent(JDOMUtil.createElement("performAction", performAction));
        root.addContent(JDOMUtil.createElement("performOnStart", performOnStart));
        root.addContent(JDOMUtil.createElement("startActionName", startActionName));
        root.addContent(JDOMUtil.createElement("performAtEnd", performAtEnd));
        root.addContent(JDOMUtil.createElement("endActionName", endActionName));
        root.addContent(JDOMUtil.createElement("performEvents", performEvents));
        root.addContent(JDOMUtil.createElement("throwable", throwable));
        root.addContent(JDOMUtil.createElement("cancelling", cancelling));
        root.addContent(JDOMUtil.createElement("stopping", stopping));
        root.addContent(JDOMUtil.createElement("closing", isClosing()));
        root.addContent(JDOMUtil.createElement("closingUnconditionally", closingUnconditionally));
        root.addContent(JDOMUtil.createElement("displayReverse", displayReverse));
        root.addContent(JDOMUtil.createElement("triggered", triggered));
        root.addContent(JDOMUtil.createElement("dependencies", dependencies));
        root.addContent(JDOMUtil.createElement("dependencyList", dependencyList));
        root.addContent(JDOMUtil.createElement("manualSelectionDisabled", manualSelectionDisabled));
        return root;
    }
}
