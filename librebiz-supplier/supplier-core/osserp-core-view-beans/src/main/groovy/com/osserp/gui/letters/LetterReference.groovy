/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 17, 2008 7:09:17 PM 
 * 
 */
package com.osserp.gui.letters

import com.osserp.core.contacts.ClassifiedContact
import com.osserp.core.dms.Letter

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class LetterReference implements Serializable {
    static final String NAME = 'letterReference'

    Long letterTypeId
    ClassifiedContact recipient
    Object businessObject
    Long branchId
    Long businessId
    String exitTarget

    /**
     * Default constructor required for serialization
     */
    protected LetterReference() {
        super()
    }

    /**
     * Provides a new instance 
     * @param letterTypeId to use
     * @param recipient of the letter
     * @param businessObject if associated (optional)
     * @param businessId
     * @param branchId
     * @param exitTarget
     */
    protected LetterReference(
        Long letterTypeId,
        ClassifiedContact recipient,
        Object businessObject,
        Long businessId,
        Long branchId,
        String exitTarget) {
        super()
        if (recipient == null) {
            throw new IllegalStateException("recipient not specified!")
        }
        if (letterTypeId == null) {
            throw new IllegalStateException("letterTypeId not specified!")
        }
        this.letterTypeId = letterTypeId
        this.recipient = recipient
        this.businessObject = businessObject
        this.branchId = branchId
        this.businessId = businessId
        this.exitTarget = exitTarget
    }

    /**
     * Indicates if provided infos contain info
     * @param infos
     * @param info
     * @return true if info found in infos
     */
    protected boolean isActivated(String[] infos, String info) {
        if (infos != null && info != null) {
            for (int i = 0; i < infos.length; i++) {
                String next = infos[i]
                if (next.equals(info)) {
                    return true
                }
            }
        }
        return false
    }

    abstract void updateInfos(Letter letter, String[] infos);

    abstract Long getObjectId();
}
