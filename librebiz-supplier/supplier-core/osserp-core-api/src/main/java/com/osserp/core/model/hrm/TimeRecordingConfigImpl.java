/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 27, 2007 9:26:02 PM 
 * 
 */
package com.osserp.core.model.hrm;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.util.DateFormatter;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecordingConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingConfigImpl extends AbstractOption implements TimeRecordingConfig {

    private boolean eol = false;
    private boolean defaultConfig = false;
    private double annualLeave = 0;
    private double daysPerWeek = 5;
    private double hoursPerWeek = 40;
    private boolean recordingHours = false;
    private double hoursDaily = 8;
    private int coreTimeStartHours = 9;
    private int coreTimeStartMinutes = 0;
    private int coreTimeEndHours = 16;
    private int coreTimeEndMinutes = 0;
    private double carryoverHoursMax = 0;

    protected TimeRecordingConfigImpl() {
        super();
    }

    public TimeRecordingConfigImpl(Employee user, String name, String description) {
        super(name, description, user.getId());
    }

    protected TimeRecordingConfigImpl(TimeRecordingConfig other) {
        super(other);
        this.annualLeave = other.getAnnualLeave();
        this.coreTimeEndHours = other.getCoreTimeEndHours();
        this.coreTimeEndMinutes = other.getCoreTimeEndMinutes();
        this.coreTimeStartHours = other.getCoreTimeStartHours();
        this.coreTimeStartMinutes = other.getCoreTimeStartMinutes();
        this.daysPerWeek = other.getDaysPerWeek();
        this.hoursPerWeek = other.getHoursPerWeek();
        this.hoursDaily = other.getHoursDaily();
        this.defaultConfig = other.isDefaultConfig();
        this.recordingHours = other.isRecordingHours();
        this.carryoverHoursMax = other.getCarryoverHoursMax();
        this.eol = other.isEol();
    }

    public boolean isEol() {
        return eol;
    }

    public void setEol(boolean eol) {
        this.eol = eol;
    }

    public boolean isDefaultConfig() {
        return defaultConfig;
    }

    protected void setDefaultConfig(boolean defaultConfig) {
        this.defaultConfig = defaultConfig;
    }

    public double getAnnualLeave() {
        return annualLeave;
    }

    public void setAnnualLeave(double annualLeave) {
        this.annualLeave = annualLeave;
    }

    public double getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(double daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public boolean isRecordingHours() {
        return recordingHours;
    }

    public void setRecordingHours(boolean recordingHours) {
        this.recordingHours = recordingHours;
    }

    public double getHoursDaily() {
        return hoursDaily;
    }

    public void setHoursDaily(double hoursDaily) {
        this.hoursDaily = hoursDaily;
    }

    public double getHoursPerWeek() {
        return hoursPerWeek;
    }

    public void setHoursPerWeek(double hoursPerWeek) {
        this.hoursPerWeek = hoursPerWeek;
    }

    public int getCoreTimeStartHours() {
        return coreTimeStartHours;
    }

    public void setCoreTimeStartHours(int coreTimeStartHours) {
        this.coreTimeStartHours = coreTimeStartHours;
    }

    public int getCoreTimeStartMinutes() {
        return coreTimeStartMinutes;
    }

    public void setCoreTimeStartMinutes(int coreTimeStartMinutes) {
        this.coreTimeStartMinutes = coreTimeStartMinutes;
    }

    public int getCoreTimeEndHours() {
        return coreTimeEndHours;
    }

    public void setCoreTimeEndHours(int coreTimeEndHours) {
        this.coreTimeEndHours = coreTimeEndHours;
    }

    public int getCoreTimeEndMinutes() {
        return coreTimeEndMinutes;
    }

    public void setCoreTimeEndMinutes(int coreTimeEndMinutes) {
        this.coreTimeEndMinutes = coreTimeEndMinutes;
    }

    public double getCarryoverHoursMax() {
        return carryoverHoursMax;
    }

    public void setCarryoverHoursMax(double carryoverHoursMax) {
        this.carryoverHoursMax = carryoverHoursMax;
    }

    public int getCarryoverHoursMaxMinutes() {
        return Double.valueOf(carryoverHoursMax * 60).intValue();
    }

    public int getDailyMinutes() {
        return Double.valueOf(hoursDaily * 60).intValue();
    }

    public String getCoreTimeStartDisplay() {
        StringBuilder buffer = new StringBuilder(DateFormatter.getFormatted(coreTimeStartHours));
        buffer.append(":").append(DateFormatter.getFormatted(coreTimeStartMinutes));
        return buffer.toString();
    }

    public String getCoreTimeEndDisplay() {
        StringBuilder buffer = new StringBuilder(DateFormatter.getFormatted(coreTimeEndHours));
        buffer.append(":").append(DateFormatter.getFormatted(coreTimeEndMinutes));
        return buffer.toString();
    }
}
