/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 28-Dec-2006 20:31:22 
 * 
 */
package com.osserp.core.model.products;

import java.util.Date;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.products.ProductSalesPrice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSalesPriceImpl extends AbstractEntity implements ProductSalesPrice {

    private Long productId;
    private Double consumerPrice = Constants.DOUBLE_NULL;
    private Double resellerPrice = Constants.DOUBLE_NULL;
    private Double partnerPrice = Constants.DOUBLE_NULL;
    private Double consumerPriceMinimum = Constants.DOUBLE_NULL;
    private Double resellerPriceMinimum = Constants.DOUBLE_NULL;
    private Double partnerPriceMinimum = Constants.DOUBLE_NULL;
    private Double consumerMinimumMargin = Constants.DOUBLE_NULL;
    private Double resellerMinimumMargin = Constants.DOUBLE_NULL;
    private Double partnerMinimumMargin = Constants.DOUBLE_NULL;
    private Double averagePurchasePrice = Constants.DOUBLE_NULL;
    private Double estimatedPurchasePrice = Constants.DOUBLE_NULL;
    private Double lastPurchasePrice = Constants.DOUBLE_NULL;

    protected ProductSalesPriceImpl() {
        super();
        setChanged(new Date(System.currentTimeMillis()));
    }

    /**
     * Default constructor for creating new serializable items
     * @param validFrom
     * @param productId
     * @param changedBy
     * @param consumerPrice
     * @param resellerPrice
     * @param partnerPrice
     * @param consumerPriceMinimum
     * @param resellerPriceMinimum
     * @param partnerPriceMinimum
     * @param consumerMinimumMargin
     * @param resellerMinimumMargin
     * @param partnerMinimumMargin
     * @param manualPurchasePrice
     * @param averagePurchasePrice
     * @param estimatedPurchasePrice
     * @param lastPurchasePrice
     */
    public ProductSalesPriceImpl(
            Date validFrom,
            Long productId,
            Long changedBy,
            Double consumerPrice,
            Double resellerPrice,
            Double partnerPrice,
            Double consumerPriceMinimum,
            Double resellerPriceMinimum,
            Double partnerPriceMinimum,
            Double consumerMinimumMargin,
            Double resellerMinimumMargin,
            Double partnerMinimumMargin,
            Double averagePurchasePrice,
            Double estimatedPurchasePrice,
            Double lastPurchasePrice) {

        this.productId = productId;
        setChanged((validFrom != null) ? validFrom : new Date(System.currentTimeMillis()));
        setChangedBy(changedBy);
        if (consumerPrice == null) {
            this.consumerPrice = Constants.DOUBLE_NULL;
        } else {
            this.consumerPrice = consumerPrice;
        }
        if (resellerPrice == null) {
            this.resellerPrice = Constants.DOUBLE_NULL;
        } else {
            this.resellerPrice = resellerPrice;
        }
        if (partnerPrice == null) {
            this.partnerPrice = Constants.DOUBLE_NULL;
        } else {
            this.partnerPrice = partnerPrice;
        }
        this.consumerPriceMinimum = (consumerPriceMinimum == null ? Constants.DOUBLE_NULL : consumerPriceMinimum);
        this.resellerPriceMinimum = (resellerPriceMinimum == null ? Constants.DOUBLE_NULL : resellerPriceMinimum);
        this.partnerPriceMinimum = (partnerPriceMinimum == null ? Constants.DOUBLE_NULL : partnerPriceMinimum);
        this.consumerMinimumMargin = (consumerMinimumMargin == null ? Constants.DOUBLE_NULL : consumerMinimumMargin);
        this.resellerMinimumMargin = (resellerMinimumMargin == null ? Constants.DOUBLE_NULL : resellerMinimumMargin);
        this.partnerMinimumMargin = (partnerMinimumMargin == null ? Constants.DOUBLE_NULL : partnerMinimumMargin);
        this.averagePurchasePrice = (averagePurchasePrice == null ? Constants.DOUBLE_NULL : averagePurchasePrice);
        this.estimatedPurchasePrice = (estimatedPurchasePrice == null ? Constants.DOUBLE_NULL : estimatedPurchasePrice);
        this.lastPurchasePrice = (lastPurchasePrice == null ? Constants.DOUBLE_NULL : lastPurchasePrice);
    }

    public Long getProductId() {
        return productId;
    }

    protected void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getConsumerPrice() {
        return consumerPrice;
    }

    public void setConsumerPrice(Double consumerPrice) {
        this.consumerPrice = consumerPrice;
    }

    public Double getPartnerPrice() {
        return partnerPrice;
    }

    public void setPartnerPrice(Double partnerPrice) {
        this.partnerPrice = partnerPrice;
    }

    public Double getResellerPrice() {
        return resellerPrice;
    }

    public void setResellerPrice(Double resellerPrice) {
        this.resellerPrice = resellerPrice;
    }

    public Double getAveragePurchasePrice() {
        return averagePurchasePrice;
    }

    public void setAveragePurchasePrice(Double averagePurchasePrice) {
        this.averagePurchasePrice = averagePurchasePrice;
    }

    public Double getEstimatedPurchasePrice() {
        return estimatedPurchasePrice;
    }

    public void setEstimatedPurchasePrice(Double estimatedPurchasePrice) {
        this.estimatedPurchasePrice = estimatedPurchasePrice;
    }

    public Double getLastPurchasePrice() {
        return lastPurchasePrice;
    }

    public void setLastPurchasePrice(Double lastPurchasePrice) {
        this.lastPurchasePrice = lastPurchasePrice;
    }

    public Double getConsumerPriceMinimum() {
        return consumerPriceMinimum;
    }

    public void setConsumerPriceMinimum(Double consumerPriceMinimum) {
        this.consumerPriceMinimum = consumerPriceMinimum;
    }

    public Double getResellerPriceMinimum() {
        return resellerPriceMinimum;
    }

    public void setResellerPriceMinimum(Double resellerPriceMinimum) {
        this.resellerPriceMinimum = resellerPriceMinimum;
    }

    public Double getPartnerPriceMinimum() {
        return partnerPriceMinimum;
    }

    public void setPartnerPriceMinimum(Double partnerPriceMinimum) {
        this.partnerPriceMinimum = partnerPriceMinimum;
    }

    public Double getConsumerMinimumMargin() {
        return consumerMinimumMargin;
    }

    public void setConsumerMinimumMargin(Double consumerMinimumMargin) {
        this.consumerMinimumMargin = consumerMinimumMargin;
    }

    public Double getResellerMinimumMargin() {
        return resellerMinimumMargin;
    }

    public void setResellerMinimumMargin(Double resellerMinimumMargin) {
        this.resellerMinimumMargin = resellerMinimumMargin;
    }

    public Double getPartnerMinimumMargin() {
        return partnerMinimumMargin;
    }

    public void setPartnerMinimumMargin(Double partnerMinimumMargin) {
        this.partnerMinimumMargin = partnerMinimumMargin;
    }
}
