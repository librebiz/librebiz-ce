<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesFcsView"/>

<c:set var="sales" value="${view.businessCaseView.sales}"/>
<c:set var="salesContext" scope="request" value="true"/>
<c:set var="popup" scope="request" value="false"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="${viewdir}/fcs/flowControl.css"/>
</style>
</tiles:put>
<tiles:put name="headline">
    <c:if test="${view.wastebasketMode}">
        FCS-<fmt:message key="trash"/> - 
    </c:if>
    <span>
        <c:if test="${sales.cancelled}">
            <span class="bolderror"><fmt:message key="cancelled"/>: </span>
        </c:if>
        <o:out value="${sales.status}"/> % 
        - <o:out value="${sales.id}"/> - <o:out value="${sales.request.name}" limit="45"/>
    </span>
</tiles:put>
<tiles:put name="headline_right">
    <v:navigation/>
</tiles:put>

<tiles:put name="content" type="string">
    <div class="content-area" id="salesFcsContent">
        <div class="row">
            <c:choose>
                <c:when test="${view.editMode}">
                    <c:import url="${viewdir}/fcs/_flowControlEditor.jsp"/>
                </c:when>
                <c:when test="${view.wastebasketMode}">
                    <div class="col-lg-12 panel-area">
                        <c:import url="${viewdir}/fcs/_flowControlWastebasket.jsp"/>
                    </div>
                </c:when>
                <c:when test="${!sales.closed and !sales.cancelled}">
                    <c:import url="${viewdir}/fcs/_flowControlDisplay.jsp"/>
                </c:when>
                <c:otherwise>
                    <div class="col-lg-12 panel-area">
                        <c:import url="${viewdir}/fcs/_flowControlTableClosed.jsp"/>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</tiles:put>
</tiles:insert>
