/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2009 12:39:40 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessType;
import com.osserp.core.BusinessTypeContext;
import com.osserp.core.BusinessTypeSelection;
import com.osserp.core.dao.BusinessTypes;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.RequestTypeContextImpl;
import com.osserp.core.model.RequestTypeImpl;
import com.osserp.core.model.RequestTypeSelectionImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessTypesDao extends AbstractRepository implements BusinessTypes {
    private String typesTable = null;

    protected BusinessTypesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        typesTable = getTable(TableKeys.REQUEST_TYPES);
    }

    public BusinessType load(Long id) {
        return (BusinessType) getCurrentSession().load(RequestTypeImpl.class, id);
    }

    public List<BusinessType> findAll() {
        StringBuilder hql = new StringBuilder(128);
        hql.append("from ").append(RequestTypeImpl.class.getName()).append(" o where o.dummy = false order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public BusinessType findBySystemProperty(String propertyName) {
        Long id = propertyName != null ? getPropertyId(propertyName) : null;
        if (id != null) {
            try {
                return load(id);
            } catch (Exception e) {
            }
        }
        return null;
    }

    public List<BusinessTypeContext> findContexts() {
        StringBuilder hql = new StringBuilder(128);
        hql.append("from ").append(RequestTypeContextImpl.class.getName()).append(" o order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public BusinessTypeSelection findSelection(String name) {
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(RequestTypeSelectionImpl.class.getName())
                .append(" obj where obj.name = :name order by obj.name");
        List<BusinessTypeSelection> list =
                getCurrentSession().createQuery(hql.toString())
                        .setParameter("name", name)
                        .list();
        return list.isEmpty() ? null : list.get(0);
    }

    public boolean exists(Long id) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(typesTable).append(" WHERE id = ?");
        int cnt = jdbcTemplate.queryForObject(
                sql.toString(),
                new Object[] { id },
                new int[] { Types.BIGINT }, 
                Integer.class);
        return (cnt > 0);
    }

    public Long findNextId() {
        StringBuilder sql = new StringBuilder("SELECT max(id) FROM ");
        sql.append(typesTable);
        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                getLongRowMapper());
        return (list.isEmpty() ? null : (list.get(0) + 1));
    }

    public void save(BusinessType type) {
        getCurrentSession().merge(RequestTypeImpl.class.getName(), type);
    }
}
