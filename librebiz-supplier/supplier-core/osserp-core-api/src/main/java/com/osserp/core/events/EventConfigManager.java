/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 4, 2007 10:41:55 AM 
 * 
 */
package com.osserp.core.events;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.views.EventDisplayView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EventConfigManager {

    /**
     * Provides a list of all event actions of a given caller
     * @param eventConfigType
     * @param callerId
     * @return event actions
     */
    List<EventAction> findActionsByCaller(EventConfigType eventConfigType, Long callerId);

    /**
     * Provides all event actions of given event type
     * @param eventConfigType
     */
    List<EventAction> findActionsByEventType(EventConfigType eventConfigType);

    /**
     * Provides all event actions related to fcs actions of given request type
     * @param configType
     * @param requestType optional request type used by project fcs actions
     * @return actions
     */
    List<EventAction> findFlowControlActions(EventConfigType configType, Long requestType);

    /**
     * Finds the event action of with given id
     * @param actionId
     * @return action
     */
    EventAction findAction(Long actionId);

    /**
     * Finds the event termination of with given id
     * @param terminationId
     * @return termination
     * @throws ClientException if no such termination exists
     */
    EventTermination findTermination(Long terminationId) throws ClientException;

    /**
     * Finds the recipient pool with given id
     * @param poolId
     * @return pool
     * @throws ClientException if no such pool exists
     */
    RecipientPool findPool(Long poolId) throws ClientException;

    /**
     * Provides all available recipient pools
     * @return pools
     */
    List<RecipientPool> findPools();

    /**
     * Provides all event config types
     * @return types
     */
    List<EventConfigType> findTypes();

    /**
     * Provides an event config type
     * @return type
     */
    EventConfigType findType(Long id);

    /**
     * Synchronizes fcs actions event flag with current event configs. E.g. enables all fcs actions event flags whose are referenced by events and disables all
     * whose are not referenced.
     */
    void synchronizeFcs();

    /**
     * Creates a new action
     * @param requestType
     * @param type
     * @param name
     * @param caller
     * @return new created action
     */
    EventAction createAction(
            Long requestType,
            EventConfigType type,
            String name,
            Long caller);

    /**
     * Sets an alert on given action
     * @param action
     * @param pool
     */
    void setAlert(EventAction action, RecipientPool pool);

    /**
     * Creates a new recipient pool
     * @param name
     * @return pool
     * @throws ClientException if name already exists
     */
    RecipientPool createPool(String name) throws ClientException;

    /**
     * Saves an action
     * @param action
     */
    void save(EventAction action);

    /**
     * Deletes an action
     * @param action
     * @throws ClientException if action could not be deleted
     */
    void delete(EventAction action) throws ClientException;

    /**
     * Saves a termination
     * @param termination
     */
    void save(EventTermination termination);

    /**
     * Deletes a termination
     * @param termination
     */
    void delete(EventTermination termination);

    /**
     * Saves a pool
     * @param pool
     */
    void save(RecipientPool pool);

    /**
     * Provides an ordered view on fcs actions starting events
     * @param configType
     * @param requestType
     * @return startActionView
     */
    List<EventDisplayView> findStartActionView(EventConfigType configType, Long requestType);

    /**
     * Closes all events of an action so action could be safely removed.
     * @param eventAction
     */
    void close(EventAction action);

}
