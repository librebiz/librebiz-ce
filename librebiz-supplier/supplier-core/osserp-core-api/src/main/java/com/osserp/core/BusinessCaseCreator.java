/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2012 
 * 
 */
package com.osserp.core;

import java.util.Date;
import java.util.List;

import com.osserp.common.Option;

import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessCaseCreator {
    
    /**
     * Looks up for an existing businessCase
     * @param id
     * @return businessCase by id or null if not exists
     */
    BusinessCase findExisting(Long id);

    /**
     * Creates a business case by all required and some optional properties.
     * This method is used by services implementing the public API. 
     * @param businessId
     * @param externalReference
     * @param externalUrl
     * @param createdDate
     * @param contextName
     * @param businessType
     * @param branchOffice
     * @param customer
     * @param salesPersonId
     * @param projectManagerId
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param countryId
     * @param campaign
     * @param origin
     * @param note
     * @param action
     * @param items
     * @return new created businessCase (request or sales)
     */
    BusinessCase createOrUpdate(
            Long businessId,
            String externalReference,
            String externalUrl,
            Date createdDate,
            String contextName,
            BusinessType businessType,
            BranchOffice branchOffice,
            Customer customer,
            Long salesPersonId,
            Long projectManagerId, 
            String name, 
            String street, 
            String streetAddon, 
            String zipcode,
            String city,
            Long countryId,
            Campaign campaign,
            Option origin,
            String note,
            FcsAction action,
            List<Item> items);
    
    /**
     * Overrides the existing businessCase identified by targetId with values
     * and businessId provided by this method.
     * @param targetId
     * @param businessId
     * @param externalReference
     * @param externalUrl
     * @param createdDate
     * @param contextName
     * @param businessType
     * @param branchOffice
     * @param customer
     * @param salesPersonId
     * @param projectManagerId
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param countryId
     * @param campaign
     * @param origin
     * @param note
     * @param action
     * @param items
     * @return overridden businessCase or null if nothing to do
     */
    BusinessCase override(
            Long targetId,
            Long businessId,
            String externalReference,
            String externalUrl,
            Date createdDate,
            String contextName,
            BusinessType businessType,
            BranchOffice branchOffice,
            Long salesPersonId,
            Long projectManagerId, 
            String name, 
            String street, 
            String streetAddon, 
            String zipcode,
            String city,
            Long countryId,
            Campaign campaign,
            Option origin,
            String note,
            FcsAction action,
            List<Item> items);

}
