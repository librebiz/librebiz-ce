/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 17, 2010 1:17:59 PM 
 * 
 */
package com.osserp.core.service.telephone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.model.telephone.TelephoneConfigurationImpl;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.telephone.Telephone;
import com.osserp.core.telephone.TelephoneActionManager;
import com.osserp.core.tasks.SfaTelephoneActionSender;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneActionManagerImpl extends AbstractService implements TelephoneActionManager {
    private static Logger log = LoggerFactory.getLogger(TelephoneActionManagerImpl.class.getName());

    private SfaTelephoneActionSender sfaTelephoneActionSender = null;

    public TelephoneActionManagerImpl(SfaTelephoneActionSender sfaTelephoneActionSender) {
        super();
        this.sfaTelephoneActionSender = sfaTelephoneActionSender;
    }

    public void rebootPhone(Telephone phone) {
        if (log.isDebugEnabled()) {
            log.debug("rebootPhone() invoked ...");
        }
        if (phone != null && phone.getTelephoneSystem() != null) {
            if (log.isDebugEnabled()) {
                log.debug("rebootPhone() phone is not null and got telephoneSystem");
            }
            this.sfaTelephoneActionSender.send(TelephoneConfigurationImpl.REBOOT_PHONE, phone.getMac(), phone.getTelephoneSystem().getId());
        }
    }

    public void sendCheckConfigToPhone(Telephone phone) {
        if (log.isDebugEnabled()) {
            log.debug("sendCheckConfigToPhone() invoked ...");
        }
        if (phone != null && phone.getTelephoneSystem() != null) {
            log.debug("sendCheckConfigToPhone() phone is not null and got telephoneSystem");
            this.sfaTelephoneActionSender.send(TelephoneConfigurationImpl.SEND_CHECK_CONFIG_TO_PHONE, phone.getMac(), phone.getTelephoneSystem().getId());
        }
    }
}
