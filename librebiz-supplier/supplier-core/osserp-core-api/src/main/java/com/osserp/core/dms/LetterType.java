/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 9, 2008 8:18:03 AM 
 * 
 */
package com.osserp.core.dms;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface LetterType extends LetterComponent {

    /**
     * Provides a comma separated list of all info keys
     * @return infoKeys
     */
    String getInfoKeys();

    /**
     * Provides a comma separated list of all parameter keys
     * @return parameterKeys
     */
    String getParameterKeys();

    /**
     * Indicates how the reference to contact should be interpreted
     * @return contactType
     */
    Long getContactType();

    /**
     * Provides the name of the search service responsible to find contacts of implementing type
     * @return contactSearch
     */
    String getContactSearch();

    /**
     * Provides the client context
     * @return clientContext
     */
    String getClientContext();

    /**
     * Provides the name of the stylesheet required to render an output of the letter
     * @return stylesheetName
     */
    String getStylesheetName();

    /**
     * Provides the name of the stylesheet required to render an output of the letter
     * @param stylesheetName
     */
    void setStylesheetName(String stylesheetName);

    /**
     * Indicates template availability
     * @return true if template should be embedded
     */
    boolean isDisabled();

    void setDisabled(boolean disabled);

    /**
     * Indicates autoselection of related templates
     * @return true if template should be selected automatically
     */
    boolean isAutoselect();

    void setAutoselect(boolean autoselect);
}
