/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Jul-2006 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;

import com.osserp.common.Entity;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.contacts.Contact;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ReceivableDisplay extends AbstractEntity implements Entity {

    private Long salutation = null;
    private String firstName = null;
    private String lastName = null;
    private Long contactType = null;
    private Long requestId = null;
    private Long type = null;
    private Long customerId = null;
    private Long status = null;
    private Long managerId = null;
    private Long salesId = null;
    private Long branchId = null;
    private String name = null;
    private String city = null;
    private Long last = null;
    private Double amount = null;

    protected ReceivableDisplay() {
        super();
    }

    public ReceivableDisplay(
            Long businesssCaseId,
            Date created,
            Long salutation,
            String firstName,
            String lastName,
            Long contactType,
            Long requestId,
            Long type,
            Long customerId,
            Long status,
            Long managerId,
            Long salesId,
            Long branchId,
            String name,
            String city,
            Long last,
            Double amount) {
        super(businesssCaseId);
        setCreated(created);
        this.salutation = salutation;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactType = contactType;
        this.requestId = requestId;
        this.type = type;
        this.customerId = customerId;
        this.status = status;
        this.managerId = managerId;
        this.salesId = salesId;
        this.branchId = branchId;
        this.name = name;
        this.city = city;
        this.last = last;
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }

    protected void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCity() {
        return city;
    }

    protected void setCity(String city) {
        this.city = city;
    }

    public Long getContactType() {
        return contactType;
    }

    protected void setContactType(Long contactType) {
        this.contactType = contactType;
    }

    public Long getCustomerId() {
        return customerId;
    }

    protected void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    protected void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Long getLast() {
        return last;
    }

    protected void setLast(Long last) {
        this.last = last;
    }

    public String getLastName() {
        return lastName;
    }

    protected void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getManagerId() {
        return managerId;
    }

    protected void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Long getBranchId() {
        return branchId;
    }

    protected void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    public Long getRequestId() {
        return requestId;
    }

    protected void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Long getSalesId() {
        return salesId;
    }

    protected void setSalesId(Long salesId) {
        this.salesId = salesId;
    }

    public Long getSalutation() {
        return salutation;
    }

    protected void setSalutation(Long salutation) {
        this.salutation = salutation;
    }

    public Long getStatus() {
        return status;
    }

    protected void setStatus(Long status) {
        this.status = status;
    }

    public Long getType() {
        return type;
    }

    protected void setType(Long type) {
        this.type = type;
    }

    public String getCustomerName() {
        String res = "";
        if (Contact.TYPE_BUSINESS.equals(contactType)
                || Contact.TYPE_PUBLIC.equals(contactType)) {
            res = (lastName == null) ? "" : lastName;
        } else {
            if (lastName != null) {
                StringBuilder b = new StringBuilder(lastName);
                if (firstName != null) {
                    b.append(", ").append(firstName);
                }
                res = b.toString();
            }
        }
        return res;
    }

    public String[] getSheetHeader() {
        return new String[] { 
            "recordId", "branchId", "contactId", "contactName", 
            "reference", "sales", "created", "maturity",
            "grossAmount" };
    }

    public String[] getSheetValues() {
        String[] r = new String[9];
        r[0] = createString(getId());
        r[1] = createString(branchId);
        r[2] = createString(customerId);
        r[3] = getCustomerName();
        r[4] = createString(getReference());
        r[5] = createString(salesId);
        r[6] = createDateForSheet(getCreated());
        r[7] = createDateForSheet(new Date());
        r[8] = createCurrencyForSheet(amount);
        return r;
    }
}
