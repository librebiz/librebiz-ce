/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 22, 2020 
 * 
 */
package com.osserp.gui.records
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.dms.Letter
import com.osserp.core.finance.Record

import com.osserp.gui.letters.LetterReference

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RecordTextReference extends LetterReference {
    private static Logger logger = LoggerFactory.getLogger(RecordTextReference.class.getName())


    protected RecordTextReference() {
        super()
    }

    RecordTextReference(Long letterTypeId, Record rec, String exit) {
        super(letterTypeId, rec.getContact(), rec, rec?.getId(), rec?.getBranchId(), exit)
    }

    void updateInfos(Letter letter, String[] infos) {
        Record rec = getRecord()
        letter.infos.each {

            if (isActivated(infos, it.name)) {
                logger.debug("updateInfos: found activated [name=${it.name}]")

                if ('customerNo' == it.name) {
                    it.setValue(rec?.contact?.id?.toString())
                    it.setIgnore(false)
                    logger.debug("updateInfos: update info [customerId=${it.value}, ignore=false]")
                } else if ('salesOfferNo' == it.name || 'salesOrderNo' == it.name) {
                    it.setValue(rec?.number)
                    it.setIgnore(false)
                    logger.debug("updateInfos: update info [recordNo=${it.value}, ignore=false]")
                } else {
                    it.setIgnore(true)
                    logger.debug("updateInfos: update info [name=${it.name}, ignore=true]")
                }
            } else {
                it.setIgnore(true)
                logger.debug("updateInfos: update info [name=${it.name}, ignore=true]")
            }
        }
        logger.debug('updateInfos: done')
    }

    Long getObjectId() {
        return record?.id
    }

    private Record getRecord() {
        getBusinessObject()
    }

}
