<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:if test="${!empty sessionScope.errors}">
<div class="data start">
    <div class="label error"><p><fmt:message key="error"/></p></div>
    <div class="value error">
        <p><o:out value="${sessionScope.errors}"/></p>
    </div>
</div>
<o:removeErrors/>
</c:if>
