/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2020 
 * 
 */
package com.osserp.core.model;


import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.PluginMapping;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PluginMappingImpl extends AbstractEntity implements PluginMapping {

    private static final long serialVersionUID = 1L;
    private String name;
    private Long mappedId;
    private String objectId;
    private String objectName;
    private String objectStatus = "unmapped";
    private String objectType;

    public PluginMappingImpl() {}

    public PluginMappingImpl(PluginMapping other) {
        super(other);
        this.name = other.getName();
        this.mappedId = other.getMappedId();
        this.objectId = other.getObjectId();
        this.objectName = other.getObjectName();
        this.objectStatus = other.getObjectStatus();
        this.objectType = other.getObjectType();
    }

    public PluginMappingImpl(
        Long referenceId,
        String name,
        Long mappedId,
        String objectId,
        String objectName,
        String objectType,
        String objectStatus) {
        super((Long) null, referenceId);
        this.name = name;
        this.mappedId = mappedId;
        this.objectId = objectId;
        this.objectName = objectName;
        this.objectType = objectType;
        if (objectStatus != null) {
            this.objectStatus =  objectStatus;
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getMappedId() {
        return this.mappedId;
    }

    @Override
    public void setMappedId(Long mappedId) {
        this.mappedId = mappedId;
    }

    @Override
    public String getObjectId() {
        return this.objectId;
    }

    @Override
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    @Override
    public String getObjectName() {
        return objectName;
    }

    @Override
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    @Override
    public String getObjectStatus() {
        return objectStatus;
    }

    @Override
    public void setObjectStatus(String objectStatus) {
        this.objectStatus = objectStatus;
    }

    @Override
    public String getObjectType() {
        return this.objectType;
    }

    @Override
    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }
}
