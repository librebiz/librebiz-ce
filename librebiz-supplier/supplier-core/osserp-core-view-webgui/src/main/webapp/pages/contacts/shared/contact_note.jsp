<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="contactView" value="${sessionScope.contactView}" />
<div class="contentBox">
    <div class="contentBoxHeader">
        <div class="contentBoxHeaderLeft">
            <fmt:message key="info" />
        </div>
        <div class="contentBoxHeaderRight">
            <div class="boxnav">
            </div>
        </div>
    </div>
    <div class="contentBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="smallSpacer"></div>
                <table class="valueTable first33">
                    <tbody>
                        <tr>
                            <td style="width: 85%"><o:out value="${contactView.stickyNote.note}" /></td>
                            <td> </td>
                        </tr>
                    </tbody>
                </table>
                <div class="smallSpacer"></div>
            </div>
        </div>
    </div>
</div>