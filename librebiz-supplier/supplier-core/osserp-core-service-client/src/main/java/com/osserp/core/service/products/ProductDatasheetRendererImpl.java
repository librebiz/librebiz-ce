/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 3, 2015 
 * 
 */
package com.osserp.core.service.products;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.OptionsCache;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.service.ResourceLocator;

import com.osserp.core.dao.Products;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductDatasheetRenderer;
import com.osserp.core.products.ProductDescription;
import com.osserp.core.products.Manufacturer;
import com.osserp.core.service.impl.AbstractOutputService;
import com.osserp.core.service.doc.XmlService;
import com.osserp.core.users.DomainUser;

/**
 * This class exists to save previous work done for a dedicated company.
 * See interface description for details.  
 *  
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductDatasheetRendererImpl extends AbstractOutputService implements ProductDatasheetRenderer {
    
    private DmsManager dmsManager;
    private Products products;

    protected ProductDatasheetRendererImpl(
            ResourceLocator resourceLocator, 
            OptionsCache optionsCache, 
            CoreStylesheetService stylesheetService,
            XmlService xmlService, 
            DmsManager dmsManager,
            Products products) {
        super(resourceLocator, optionsCache, stylesheetService, xmlService);
        this.dmsManager = dmsManager;
        this.products = products;
    }

    public Document createDatasheet(DomainUser user, Product product, Locale locale) {
        // we reload product data to catch eventually changes 
        Product updated = products.load(product.getProductId());
        List<ProductDescription> productDescriptions = products.findDescriptions(updated.getProductId()); 
        Manufacturer manufacturer = products.findManufacturer(updated);
        DmsDocument picture = loadPicture(updated);
        Document doc = createSheet(user, locale, updated, productDescriptions, manufacturer, picture);
        return doc;
    }

    private Document createSheet(
            DomainUser user, 
            Locale locale,
            Product product,
            List<ProductDescription> productDescriptions,
            Manufacturer manufacturer,
            DmsDocument picture) {
    
        Document doc = xmlService.createDocument(user.getEmployee());
        Element root = doc.getRootElement();
        root.addContent(new Element("datasheetLanguage").setText(locale.getLanguage()));
        
        Element productElement = product.getXML("product");

        Element descriptions = new Element("descriptions");
        for (Iterator<ProductDescription> i = productDescriptions.iterator(); i.hasNext();) {
            ProductDescription productDescription = i.next();
            Element description = productDescription.getXML("description");
            descriptions.addContent(description);
        }
        productElement.addContent(descriptions);

        productElement.removeChild("details");
        if (product.getDetails() == null) {
            productElement.addContent(new Element("details"));
        } else {
            Element details = xmlService.createProductDetails("details", product.getDetails());
            details.setAttribute("type", product.getDetails().getClass().getName());
            productElement.addContent(details);
        }

        productElement.removeChild("manufacturer");
        if (manufacturer == null) {
            productElement.addContent(new Element("manufacturer"));
        } else {
            productElement.addContent(manufacturer.getXML("manufacturer"));
        }
        if (picture != null) {
            productElement.addContent(new Element("pictureFileName").setText(
                    picture.getRealFileName()));
        }
        root.addContent(productElement);
        return doc;
    }

    private DmsDocument loadPicture(Product product) {
        if (product.isPictureAvailable()) {
            List<DmsDocument> list = dmsManager.findByReference(
                    new DmsReference(Product.PICTURE, product.getProductId()));
            if (!list.isEmpty()) {
                return list.get(0);
            }
        }
        return null;
    }
}
