/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.ods

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ds.ImportException
import com.osserp.common.ds.ImportFile
import com.osserp.common.ds.ImporterImpl
import com.osserp.common.util.NumberUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class OdsImporter extends ImporterImpl {
    private static Logger logger = LoggerFactory.getLogger(OdsImporter.class.getName())
    Boolean sheetProvidesHeader = false
    
    OdsImporter(Boolean sheetProvidesHeader) throws ImportException {
        super(null, null, false, false)
        this.sheetProvidesHeader = sheetProvidesHeader
    }
    
    OdsImporter(Boolean sheetProvidesHeader, String selectedMode, String directory, Boolean modeRequired, Boolean importDirRequired) throws ImportException {
        super(selectedMode, directory, modeRequired, importDirRequired)
        this.sheetProvidesHeader = sheetProvidesHeader
    }

    @Override
    protected Boolean isImportFile(File file) {
        OdsUtil.isImportFile(file)
    }

    @Override
    protected ImportFile createImportFile(File file) throws ImportException {
        return new OdsImportFile(file, [ providesHeader: sheetProvidesHeader ])
    }

    /**
     * Creates a long by a string from cell
     * @param cellValue
     * @return long value or null if none provided
     */
    protected Long createLong(String cellValue) {
        if (!cellValue) {
            return null
        }
        try {
            return Double.valueOf(cellValue).longValue()
        } catch (Exception ignorable) {
            logger.warn("createLong: failed [cellValue=${cellValue}, message=${ignorable.message}]")
        }
        NumberUtil.createLong(cellValue)
    }
    
    protected Long createLong(Long value) {
        // This method exists to provide a matching method instead of performing explicit type analysis.
        // (Values provided by map may be a string assigned by sheet or a long assigned by importer itself)
        value
    }

    /**
     * Creates an integer by a string from cell
     * @param cellValue
     * @return integer value or null if none provided
     */
    protected Integer createInteger(String cellValue) {
        if (!cellValue) {
            return null
        }
        try {
            return Double.valueOf(cellValue).intValue()
        } catch (Exception ignorable) {
            logger.warn("createInteger: failed [cellValue=${cellValue}, message=${ignorable.message}]")
        }
        NumberUtil.createInteger(cellValue)
    }

    /**
     * Creates a double by a string from cell
     * @param cellValue
     * @return double value or null if none provided
     */
    protected Double createDouble(String cellValue) {
        if (!cellValue) {
            return null
        }
        try {
            return Double.valueOf(cellValue)
        } catch (Exception ignorable) {
            logger.warn("createDouble: failed [cellValue=${cellValue}, message=${ignorable.message}]")
        }
        NumberUtil.createDouble(cellValue)
    }

    /**
     * Creates a double value by a string from cell
     * @param cellValue
     * @return double value or 0.0 if none provided
     */
    protected Double createDoubleValue(String cellValue) {
        Double result = createDouble(cellValue)
        if (result == null) {
            return 0.0
        }
        return result
    }
    
    /**
     * Provides a trimmed string by a string ignoring empty
     * @param value the string to trim
     * @return trimmed string or null if value is an empty string
     */
    protected String fetchString(String value) {
        value ? value.trim() : null
    }
}

