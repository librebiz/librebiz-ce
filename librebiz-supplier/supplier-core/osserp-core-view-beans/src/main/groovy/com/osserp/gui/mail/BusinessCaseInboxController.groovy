/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.mail

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/mail/businessCaseInbox/*")
@Controller class BusinessCaseInboxController extends AbstractFetchmailInboxController {
    private static Logger logger = LoggerFactory.getLogger(BusinessCaseInboxController.class.getName())

    @RequestMapping
    def moveAttachmentToDocuments(HttpServletRequest request) {
        logger.debug("moveAttachmentToDocuments: invoked [user=${getUserId(request)}]")
        BusinessCaseInboxView view = getView(request)
        try {
            view.moveAttachmentToDocuments()
        } catch (ClientException e) {
            saveError(request, e.message)
        }
        defaultPage
    }

    @RequestMapping
    def moveAttachmentToPictures(HttpServletRequest request) {
        logger.debug("moveAttachmentToPictures: invoked [user=${getUserId(request)}]")
        BusinessCaseInboxView view = getView(request)
        try {
            view.moveAttachmentToPictures()
        } catch (ClientException e) {
            saveError(request, e.message)
        }
        defaultPage
    }

    @RequestMapping
    def setBusinessCase(HttpServletRequest request) {
        logger.debug("setBusinessCase: invoked [user=${getUserId(request)}]")
        BusinessCaseInboxView view = getView(request)
        try {
            view.setBusinessCase()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        return defaultPage
    }

    @Override
    @RequestMapping
    def disableEditMode(HttpServletRequest request) {
        return select(request)
    }

    @Override
    @RequestMapping
    def save(HttpServletRequest request) {
        def target = super.save(request)
        BusinessCaseInboxView view = getView(request)
        if (view.list.empty && !view.receivedMail) {
            return exit(request)
        }
        return target
    }

}
