/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 22, 2013 
 * 
 */
package com.osserp.gui.users

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ActionTarget
import com.osserp.common.User
import com.osserp.common.UserManager

import com.osserp.core.Options
import com.osserp.core.employees.EmployeeGroup

import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class UserSettingsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(UserSettingsView.class.getName())

    List<ActionTarget> startupTargets = []
    List<EmployeeGroup> flowControlGroups = []

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            List<EmployeeGroup> employeeGroups = getOptions(Options.EMPLOYEE_GROUPS)
            employeeGroups.each {
                if (it.flowControlAware) {
                    flowControlGroups.add(it)
                }
            }
            def trgts = userManager.startupTargets
            // target setup is not selectable by user itself
            startupTargets = trgts.findAll { it.key != 'setup' }
        }
    }

    User updateFlowControlGroup() {
        Long id = form.getLong('id')
        logger.debug("updateFlowControlGroup: invoked [id=$id]")
        domainUser.setDefaultFcsGroup(id)
        userManager.save(domainUser)
    }

    User updateLocale() {
        userManager.changeLocale(user, form.getString('value'))
    }

    User switchProperty() {
        userManager.switchProperty(user, user, form.getString('name'))
    }

    User updateProperty() {
        userManager.setProperty(
            user, user, form.getString('name'), form.getString('value'))
    }

    User updateStartup() {
        Long id = form.getLong('id')
        ActionTarget target = startupTargets.find { it.id == id }
        if (target) {
            domainUser.setStartup(target)
        }
        userManager.save(domainUser)
    }

    protected UserManager getUserManager() {
        getService(UserManager.class.getName())
    }
}
