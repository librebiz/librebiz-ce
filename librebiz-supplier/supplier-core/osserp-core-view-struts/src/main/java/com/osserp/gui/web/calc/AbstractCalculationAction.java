/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Nov-2006 08:43:34 
 * 
 */
package com.osserp.gui.web.calc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.ViewManager;
import com.osserp.gui.client.calc.CalculationView;
import com.osserp.gui.web.struts.AbstractBusinessCaseAwareAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractCalculationAction extends AbstractBusinessCaseAwareAction {
    private static Logger log = LoggerFactory.getLogger(AbstractCalculationAction.class.getName());

    protected CalculationView createCalculationView(HttpServletRequest request) throws ClientException, PermissionException {
        return (CalculationView) ViewManager.newInstance(servlet.getServletContext()).createView(request, CalculationView.class.getName());
    }

    protected CalculationView fetchCalculationView(HttpSession session) throws PermissionException {
        return (CalculationView) fetchView(session, CalculationView.class);
    }

    protected CalculationView getCalculationView(HttpSession session) throws PermissionException {
        CalculationView view = (CalculationView) fetchView(session, CalculationView.class);
        if (view == null) {
            log.warn("getCalculationView() failed, view not bound!");
            throw new ActionException();
        }
        return view;
    }

    protected void removeCalculationView(HttpSession session) {
        ViewManager.removeView(session, CalculationView.class);
    }
}
