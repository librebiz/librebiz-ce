<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="campaignSelectionView"/>
<c:choose>
<c:when test="${!view.branchSelectionMode}">
<style type="text/css">

.name {
	width: 330px;
}

.group, .type {
	width: 184px;
}

.company {
	width: 55px;
	text-align: center;
}

#campaignSelectionSearch {
	height:28px;
	border:0px;
	margin-left:260px;
}

#campaigns .activated {
	background-color: #FFFFFF;
}

#campaigns .eol {
	background-color: #FBC1C5;
}

#campaigns .deselected {
	background-color: lightgrey;
}

</style>
</c:when>
<c:otherwise>
<style type="text/css">

.name {
	width: 55px;
}

.group, .type {
	width: 55px;
}

.company {
	width: 425px;
	text-align: left;
}

#campaignSelectionSearch {
	height:28px;
	border:0px;
	margin-left:260px;
}

#campaigns .activated {
	background-color: #FFFFFF;
}

#campaigns .deselected {
	background-color: lightgrey;
}

</style>
</c:otherwise>
</c:choose>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="campaignSelection"/>
	</div>
</div>

<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
				<div id="campaignSelectionSearch">
					<o:ajaxLookupForm name="campaignSelectionSearchForm" url="${view.appLink}/search" targetElement="campaignSelectionResult" minChars="0" events="onkeyup" activityElement="value">
						<input style="background: whitesmoke;" type="text" name="value" id="value" value="<o:out value="${view.searchPattern}"/>" />
					</o:ajaxLookupForm>
				</div>
				<div id="campaignSelectionResult">
					<c:import url="_campaignSelectionResult.jsp"/>
				</div>
			<div class="spacer"></div>
		</div>
	</div>
</div>
