/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2008 3:38:50 PM 
 * 
 */
package com.osserp.core.service.sales;

import java.util.ArrayList;
import java.util.List;

import com.osserp.core.dao.SalesMonitoringCache;
import com.osserp.core.sales.SalesMonitoringItem;
import com.osserp.core.sales.SalesMonitoringManager;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.tasks.SalesMonitoringCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesMonitoringManagerImpl extends AbstractService implements SalesMonitoringManager {
    private SalesMonitoringCache cache = null;
    private SalesMonitoringCacheSender salesMonitoringCacheSender = null;

    public SalesMonitoringManagerImpl(
            SalesMonitoringCache cache, 
            SalesMonitoringCacheSender salesMonitoringCacheSender) {
        super();
        this.cache = cache;
        this.salesMonitoringCacheSender = salesMonitoringCacheSender;
    }

    public List<SalesMonitoringItem> getSalesMonitoring(String requestTypeSelection) {
        return clone(cache.getSalesMonitoring(requestTypeSelection));
    }

    public List<SalesMonitoringItem> getSalesMonitoring(Long company) {
        return clone(cache.getSalesMonitoring(company));
    }

    public void reload() {
        salesMonitoringCacheSender.send();
    }

    private List<SalesMonitoringItem> clone(List<SalesMonitoringItem> list) {
        List<SalesMonitoringItem> result = new ArrayList<SalesMonitoringItem>();
        for (int i = 0, j = list.size(); i < j; i++) {
            result.add(list.get(i));
        }
        return result;
    }
}
