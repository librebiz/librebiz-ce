/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.projects;

import java.util.Date;

import com.osserp.common.EntityRelation;

import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordType;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectSupplier extends EntityRelation {

    /**
     * Provides the supplier id
     * @return supplierId
     */
    Supplier getSupplier();

    /**
     * Provides the related voucher if assigned and load.
     * @return voucher or null if not assigned
     */
    Record getVoucher();

    void setVoucher(Record voucher);

    /**
     * Provides the type of the related voucher if assigned and load.
     * @return voucher type or null if not assigned
     */
    RecordType getVoucherType();

    void setVoucherType(RecordType voucherType);

    /**
     * Provides the label to display the object in lists.
     * @return label
     */
    String getLabel();

    /**
     * Provides the installer type. The default implementation points to
     * group property of supplier.
     * @return type
     */
    String getGroupName();

    /**
     * Provides the associated purchase record id
     * @return recordId
     */
    Long getRecordId();

    /**
     * Provides the type of the associated purchase record
     * @return recordTypeId
     */
    Long getRecordTypeId();

    /**
     * Service delivery date 
     * @return date or null if not defined
     */
    Date getDeliveryDate();

    /**
     * Beginning of mounting if required by product
     * @return date or null if not defined
     */
    Date getMountingDate();

    /**
     * Duration in days for mounting. Default is 0.
     * @return days
     */
    Double getMountingDays();

    /**
     * Provides workflow status
     * @return status
     */
    Long getStatus();

    /**
     * Updates supplier reference
     * @param changedBy
     * @param supplier
     */
    void update(Long changedBy, Supplier supplier);

    /**
     * Updates supplier reference
     * @param changedBy
     * @param groupName
     * @param deliveryDate
     * @param mountingDate
     * @param mountingDays
     * @param status
     */
    void update(
            Long changedBy,
            String groupName,
            Date deliveryDate,
            Date mountingDate,
            Double mountingDays,
            Long status);

    /**
     * Updates voucher reference
     * @param id
     * @param type
     */
    void assignVoucher(Long id, Long type);

    /**
     * Removes voucher reference and resets delivery date
     */
    void resetVoucher();

    boolean isPurchaseOrderAssigned();

    boolean isPurchaseInvoiceAssigned();
}
