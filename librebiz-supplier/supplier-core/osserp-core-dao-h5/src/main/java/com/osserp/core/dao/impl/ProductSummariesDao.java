/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import com.osserp.common.dao.AbstractSpringDao;
import com.osserp.core.dao.ProductSummaries;
import com.osserp.core.products.ProductSummary;
import com.osserp.core.products.ProductSummaryImpl;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSummariesDao extends AbstractSpringDao implements ProductSummaries {

    public ProductSummariesDao(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    public ProductSummary load(Long stockId, Long productId) {
        List<ProductSummary> result = (List<ProductSummary>) jdbcTemplate.query(
                Procs.GET_PRODUCT_SUMMARY_BY_STOCK,
                new Object[] { stockId, productId },
                new int[] { Types.BIGINT, Types.BIGINT },
                ProductSummaryRowMapper.getReader(stockId));
        return result.isEmpty() ? new ProductSummaryImpl(stockId) : result.get(0);
    }

}
