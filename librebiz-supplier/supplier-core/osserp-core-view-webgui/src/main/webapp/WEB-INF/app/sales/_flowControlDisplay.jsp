<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" scope="page" value="${sessionScope.flowControlDisplayView}"/>

<c:if test="${!empty view}">
	<c:set var="bean" scope="page" value="${view.bean}"/>

	<div class="modalBoxHeader">
		<div class="modalBoxHeaderLeft">
			<c:choose>
				<c:when test="${empty bean}">
					<fmt:message key="common.error"/>
				</c:when>
				<c:when test="${view.businessCaseAvailable}">
					<o:out value="${bean.primaryKey}"/> - <o:out value="${bean.name}"/>
				</c:when>
				<c:otherwise>
					<o:out value="${bean.id}"/> - <o:out value="${bean.name}"/>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="modalBoxHeaderRight">
			<div class="boxnav">
				<ul>
					<c:forEach var="nav" items="${view.navigation}">
						<li>
							<v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxLink>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
	<div class="modalBoxData">
		<div class="subcolumns">
			<div class="subcolumn">
				<c:if test="${!empty sessionScope.error}">
					<div class="spacer"></div>
					<div class="errormessage">
						<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
					</div>
                    <o:removeErrors/>
				</c:if>
				<div class="spacer"></div>
				<c:choose>
					<c:when test="${empty bean}">
						<fmt:message key="error.search"/>
					</c:when>
					<c:when test="${view.businessCaseAvailable}">
						<table style="width:450px;" class="table">
							<c:forEach var="item" items="${bean.flowControlSheet}" varStatus="s">
								<tr class="altrow">
									<td><o:date value="${item.created}" casenull="-" addcentury="false"/></td>
									<td><o:out value="${item.action.name}"/></td>
									<td><oc:employee value="${item.employeeId}"/></td>
								</tr>
								<c:if test="${!empty item.note}">
									<tr>
										<td></td>
										<td style="padding-left:10px;" colspan="2"><o:out value="${item.note}"/></td>
									</tr>
								</c:if>
							</c:forEach>
						</table>
					</c:when>
					<c:otherwise>
						<table style="width:450px;" class="table">
							<c:forEach var="item" items="${bean.flowControlSheet}" varStatus="s">
								<tr class="altrow">
									<td><o:date value="${item.created}" casenull="-" addcentury="false"/></td>
									<td><o:out value="${item.name}"/></td>
									<td><oc:employee value="${item.createdBy}"/></td>
								</tr>
								<c:if test="${!empty item.note}">
									<tr>
										<td></td>
										<td style="padding-left:10px;" colspan="2"><o:out value="${item.note}"/></td>
									</tr>
								</c:if>
							</c:forEach>
						</table>
					</c:otherwise>
				</c:choose>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</c:if>