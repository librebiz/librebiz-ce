<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="listView" value="${requestScope.documentListView}" />

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="docListDate"><fmt:message key="date" /></th>
                <th class="docListName"><fmt:message key="label" /></th>
                <th class="docListSize"><fmt:message key="size" /></th>
                <th class="action"> </th>
            </tr>
        </thead>
        <tbody>
            <c:if test="${empty listView.list}">
                <td colspan="4">
                    <fmt:message key="noDocumentAvailable"/>
                </td>
            </c:if>
            <c:forEach var="obj" items="${listView.list}">
                <tr>
                    <td class="docListDate" title="<oc:employee value="${obj.createdBy}" />">
                        <o:date value="${obj.created}" />
                    </td>
                    <td class="docListName">
                        <o:doc obj="${obj}">
                            <o:out value="${obj.displayName}" />
                        </o:doc>
                    </td>
                    <td class="docListSize"><o:number value="${obj.fileSize}" format="bytes" /></td>
                    <td class="action">
                        <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="${listView.baseLink}/delete?id=${obj.id}" />');" title="<fmt:message key="delete"/>">
                            <o:img name="deleteIcon" />
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
