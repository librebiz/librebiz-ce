/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2016 
 * 
 */
package com.osserp.common.service;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.EntityRelation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SecurityService {
    
    /**
     * Indicates if security service is configured. Don't use any other method
     * of the service if this method reports false.
     * @return true if configured
     */
    boolean isConfigured();
    
    /**
     * Retieves a password from password storage
     * @param contextName
     * @param name
     * @return password or null if not available
     */
    String getPassword(String contextName, String name);
    
    /**
     * Encrypts a plain object and writes data to disk.
     * @param filename name including path
     * @param plainObject plain object to persist encrypted.
     */
    void writeFile(String filename, byte[] plainObject) throws ClientException;
    
    /**
     * Reads file from disk and decrypts the stream
     * @param filename name including path of encrypted file
     * @return decrypted data
     */
    byte[] readFile(String filename) throws ClientException;
    
    /**
     * Encrypts and stores a textual information
     * @param reference required to identity 
     * @param contextName required to identity
     * @param text information to store encrypted
     */
    void writeText(EntityRelation reference, String contextName, String text);
    
    /**
     * Encrypts and stores a textual information
     * @param reference required to identity 
     * @param contextName required to identity
     */
    String readText(EntityRelation reference, String contextName);
    
    /**
     * Copy an existing text  
     * @param contextName required to identity
     * @param source reference with text to copy from 
     * @param target reference to copy text to 
     */
    void copyText(String contextName, EntityRelation source, EntityRelation target);
    
    /**
     * Looks up for stored text on each provided reference and returns a list 
     * of references with text found.
     * @param contextName 
     * @param references
     * @return references with stored text
     */
    List<EntityRelation> textAvailable(String contextName, List<EntityRelation> references);
}
