/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 22, 2016 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.service.impl.AbstractService;
import com.osserp.common.util.DateFormatter;

import com.osserp.core.FcsAction;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.Records;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.projects.ProjectFcsManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesCancellationManager;
import com.osserp.core.sales.SalesCreditNoteManager;
import com.osserp.core.sales.SalesDownpaymentManager;
import com.osserp.core.sales.SalesInvoiceManager;
import com.osserp.core.sales.SalesInvoicePaymentManager;
import com.osserp.core.sales.SalesPaymentManager;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesInvoicePaymentManagerImpl extends AbstractService implements SalesInvoicePaymentManager {
    private static Logger log = LoggerFactory.getLogger(SalesInvoicePaymentManagerImpl.class.getName());
    
    private SalesCancellationManager salesCancellationManager;
    private SalesCreditNoteManager salesCreditNoteManager;
    private SalesDownpaymentManager salesDownpaymentManager;
    private SalesInvoiceManager salesInvoiceManager;
    private SalesPaymentManager salesPaymentManager;
    
    private SystemConfigs systemConfigs;
    private Projects projects;
    private ProjectFcsManager projectFcsManager;

    /**
     * Creates a new invoice payment manager
     * @param salesCancellationManager
     * @param salesCreditNoteManager
     * @param salesDownpaymentManager
     * @param salesInvoiceManager
     * @param salesPaymentManager
     * @param systemConfigs
     * @param projects
     * @param projectFcsManager
     */
    public SalesInvoicePaymentManagerImpl(
            SalesCancellationManager salesCancellationManager,
            SalesCreditNoteManager salesCreditNoteManager,
            SalesDownpaymentManager salesDownpaymentManager, 
            SalesInvoiceManager salesInvoiceManager,
            SalesPaymentManager salesPaymentManager,
            SystemConfigs systemConfigs,
            Projects projects,
            ProjectFcsManager projectFcsManager) {
        super();
        this.salesCancellationManager = salesCancellationManager;
        this.salesCreditNoteManager = salesCreditNoteManager;
        this.salesDownpaymentManager = salesDownpaymentManager;
        this.salesInvoiceManager = salesInvoiceManager;
        this.salesPaymentManager = salesPaymentManager;
        this.systemConfigs = systemConfigs;
        this.projects = projects;
        this.projectFcsManager = projectFcsManager;
    }


    public Invoice addDiscount(DomainUser user, Invoice invoice, Long paymentType, BigDecimal amount, boolean cashDiscount) throws ClientException {
        if (isNotSet(amount)) {
            throw new ClientException(ErrorCode.AMOUNT_MISSING);
        }
        if (invoice.isDownpayment() || !BillingType.TYPE_CASH_DISCOUNT.equals(paymentType)) {
            log.warn("addDiscount() invalid state [invoice=" + invoice.getId()
                    + ", downpayment=" + invoice.isDownpayment()
                    + ", typeCashDisount=" + BillingType.TYPE_CASH_DISCOUNT.equals(paymentType) 
                    + "]");
            throw new ClientException(ErrorCode.OPERATION_NOT_SUPPORTED);
        }
        Invoice result = loadInvoice(invoice.getId());
        if (result.getDueAmount().doubleValue() <= 0.01 && !result.getPayments().isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("addDiscount() invocation while invoice is paid [record=" 
                        + result.getId() + "]");
            }
            double paymentsSummary = 0;
            for (int i = 0, j = result.getPayments().size(); i < j; i++) {
                Payment p = result.getPayments().get(i);
                if (p.getAmount().doubleValue() > amount.doubleValue()) {
                    if (cashDiscount) {
                        if (log.isDebugEnabled()) {
                            log.debug("addDiscount() cash discount to subtract found [record="
                                + result.getId() + ", payment=" + p.getId()
                                + ", discount=" + amount + "]");
                        }
                        salesPaymentManager.subtract(p, amount);
                        result = loadInvoice(result.getId());
                        salesInvoiceManager.addPayment(
                                user.getEmployee(),
                                result,
                                paymentType,
                                amount,
                                p.getPaid(),
                                p.getBankAccountId());
                        return loadInvoice(result.getId());
                    }
                    paymentsSummary = paymentsSummary + p.getAmount().doubleValue();

                } else {
                    paymentsSummary = paymentsSummary + p.getAmount().doubleValue();
                }
            }
            if (!cashDiscount && paymentsSummary > amount.doubleValue()) {
                BigDecimal discount = new BigDecimal(paymentsSummary - amount.doubleValue());
                for (int i = 0, j = result.getPayments().size(); i < j; i++) {
                    Payment p = result.getPayments().get(i);
                    if (p.getAmount().doubleValue() > discount.doubleValue()) {
                        if (log.isDebugEnabled()) {
                            log.debug("addDiscount() found payment to subtract cash discount from [record="
                                + result.getId() + ", payment=" + p.getId()
                                + ", discount=" + discount + "]");
                        }
                        salesPaymentManager.subtract(p, discount);
                        result = loadInvoice(result.getId());
                        salesInvoiceManager.addPayment(
                                user.getEmployee(),
                                result,
                                paymentType,
                                discount,
                                p.getPaid(),
                                p.getBankAccountId());
                        return loadInvoice(result.getId());
                    }
                }
                
            }
            if (log.isDebugEnabled()) {
                log.debug("addPayment() did not find enough payments to subtract cash discount from [record=" 
                        + result.getId() + "]");
            }
        } else if (log.isDebugEnabled()) {
                log.debug("addPayment() cash discount selected while record not paid [record=" + result.getId()
                        + ", dueAmount=" + result.getDueAmount()
                        + ", payments=" + result.getPayments().size() + "]");
        }
        Date paid = null;
        Long bankAccountId = null;
        for (int i = 0, j = result.getPayments().size(); i < j; i++) {
            Payment next = result.getPayments().get(i);
            if (paid == null || paid.before(next.getPaid())) {
                paid = next.getPaid();
                bankAccountId = next.getBankAccountId();
            }
        }
        if (paid == null) {
            throw new ClientException(ErrorCode.PAYMENT_MISSING);
        }
        salesInvoiceManager.addPayment(
                user.getEmployee(), 
                result, 
                paymentType, 
                amount, 
                paid, 
                bankAccountId);
        return loadInvoice(result.getId());
    }


    public Invoice addPayment(
            DomainUser user, 
            Invoice invoice, 
            Long bankAccountId, 
            Long paymentType, 
            BigDecimal amount, 
            Date datePaid, 
            boolean paid,
            boolean cashDiscount)
            throws ClientException {
        
        if (isNotSet(amount)) {
            throw new ClientException(ErrorCode.AMOUNT_MISSING);
        }
        if (BillingType.TYPE_CASH_DISCOUNT.equals(paymentType)) {
            log.warn("addPayment() invalid attempt to add cash discount. Use dedicated method instead"
                    + " [record=" + invoice.getId() + "]");
            throw new ClientException(ErrorCode.OPERATION_NOT_SUPPORTED);
        }
        if (invoice.isDownpayment()) {
            salesDownpaymentManager.addPayment(
                    user.getEmployee(), invoice, paymentType, amount, datePaid, bankAccountId);
            if (paid) {
                salesDownpaymentManager.updateStatus(invoice, user.getEmployee(), Record.STAT_CLOSED);
            }
            return (Invoice) salesDownpaymentManager.load(invoice.getId());
        }
        Invoice result = loadInvoice(invoice.getId());
        salesInvoiceManager.addPayment(
                user.getEmployee(), 
                invoice, 
                paymentType, 
                amount, 
                datePaid, 
                bankAccountId);
        
        result = loadInvoice(invoice.getId());
        if (cashDiscount && result.getDueAmount().doubleValue() > 0) {
            salesInvoiceManager.addPayment(
                    user.getEmployee(), 
                    result, 
                    BillingType.TYPE_CASH_DISCOUNT, 
                    result.getDueAmount(), 
                    datePaid, 
                    bankAccountId);
            result = loadInvoice(invoice.getId());
        }
        if (paid || (result.getDueAmount() != null && result.getDueAmount().doubleValue() < 0.01
                        && !result.getPayments().isEmpty())) {
            salesInvoiceManager.updateStatus(result, user.getEmployee(), Record.STAT_CLOSED);
            result = loadInvoice(result.getId());
            addFlowControlIfRequired(user, result, datePaid);
        }
        return result;
    }

    public Invoice addCustomPayment(
            DomainUser user,
            Invoice invoice,
            BigDecimal amount,
            Date datePaid,
            boolean paid,
            String customHeader,
            String note,
            Long bankAccountId)
            throws ClientException {

        if (invoice.isDownpayment()) {
            throw new ClientException(ErrorCode.INVALID_CONTEXT);
        }
        if (isNotSet(amount)) {
            throw new ClientException(ErrorCode.AMOUNT_MISSING);
        }
        if (isNotSet(customHeader)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (isNotSet(note)) {
            throw new ClientException(ErrorCode.ACTION_NOTE_REQUIRED);
        }
        Invoice result = loadInvoice(invoice.getId());
        salesInvoiceManager.addCustomPayment(
                user.getEmployee(),
                invoice,
                amount,
                datePaid,
                customHeader,
                note,
                bankAccountId);
        result = loadInvoice(invoice.getId());
        if (paid || (result.getDueAmount() != null && result.getDueAmount().doubleValue() < 0.01
                        && !result.getPayments().isEmpty())) {
            salesInvoiceManager.updateStatus(result, user.getEmployee(), Record.STAT_CLOSED);
            result = loadInvoice(result.getId());
            addFlowControlIfRequired(user, result, datePaid);
        }
        return result;
    }

    private Invoice loadInvoice(Long id) {
        return (Invoice) salesInvoiceManager.load(id);
    }

    public Cancellation createCancellation(DomainUser user, Invoice invoice, Date date, Long customId) throws ClientException {
        if (Records.isPaymentAssigned(invoice)) {
            // don't cancel invoice with payments, user should create creditNote instead
            throw new ClientException(ErrorCode.CANCEL_IMPOSSIBLE_PAYMENT_BOOKED);
        }
        return salesCancellationManager.create(user.getEmployee(), invoice, date, customId);
    }

    public CreditNote createCreditNote(DomainUser user, Invoice invoice, Long bookingType, boolean copyItems) {
        BookingType selectedBookingType = null;
        if (bookingType != null) {
            selectedBookingType = salesCreditNoteManager.getBookingType(bookingType);
        }
        if (bookingType == null) {
            selectedBookingType = salesCreditNoteManager.getDefaultBookingType();
        }
        return salesCreditNoteManager.create(user.getEmployee(), invoice, selectedBookingType, copyItems);
    }

    public Invoice clearDecimalAmount(DomainUser user, Invoice invoice) throws ClientException {
        Double limit = parseDouble(systemConfigs.getSystemProperty(BillingType.CLEARING_LIMIT_PROPERTY));
        if (!invoice.getPayments().isEmpty() && limit != null && (limit < 0 || limit > 0)) {
            Double due = invoice.getDueAmount().doubleValue();
            Long bankAccountId = null;
            Date datePaid = null;
            BigDecimal clearAmount = new BigDecimal(0);
            if (due > 0 && due < limit) {
                clearAmount = new BigDecimal(due * -1);
            } else if (due < 0 && due > (limit * -1)) {
                clearAmount = new BigDecimal(due);
                Payment last = null;
                for (int i = 0, j = invoice.getPayments().size(); i < j; i++) {
                    Payment next = invoice.getPayments().get(i);
                    if (last == null || last.getPaid().before(next.getPaid())) {
                        last = next;
                    }
                }
                if (last != null) {
                    bankAccountId = last.getBankAccountId();
                    datePaid = last.getPaid();
                }
            }
            salesInvoiceManager.addPayment(
                    user.getEmployee(),
                    invoice,
                    BillingType.TYPE_CLEARING,
                    clearAmount,
                    datePaid,
                    bankAccountId);
        }
        return loadInvoice(invoice.getId());
    }

    public Invoice deletePayment(DomainUser user, Invoice invoice, Payment payment) throws ClientException {
        assert (user != null && invoice != null && payment != null);
        if (!user.isAccounting() && !user.isExecutiveAccounting() && !user.isExecutive()) {
            throw new ClientException(ErrorCode.PERMISSION_DENIED);
        }
        if (!invoice.isUnchangeable()) {
            throw new ClientException(ErrorCode.UNRELEASED_RECORD);
        }
        salesPaymentManager.delete(payment);
        if (Record.STAT_CLOSED.equals(invoice.getStatus())) {
            try {
                if (invoice.isDownpayment()) {
                    salesDownpaymentManager.updateStatus(invoice, user.getEmployee(), Record.STAT_SENT);
                } else {
                    salesInvoiceManager.updateStatus(invoice, user.getEmployee(), Record.STAT_SENT);
                }
            } catch (ClientException e) {
                log.error("delete() ignoring exception on attempt to reset invoice status [invoice=" 
                        + invoice.getId() + ", payment=" + payment.getId()
                        + ", message=" + e.getMessage() + "]", e);
            }
        }
        if (invoice.isDownpayment()) {
            return (Invoice) salesDownpaymentManager.load(invoice.getId());
        } 
        return (Invoice) salesInvoiceManager.load(invoice.getId());
    }
    
    public Invoice setAsPaid(DomainUser user, Invoice invoice) throws ClientException {
        if (!user.isAccounting() && !user.isExecutiveAccounting() && !user.isExecutive()) {
            throw new ClientException(ErrorCode.PERMISSION_DENIED);
        }
        if (!invoice.isUnchangeable()) {
            throw new ClientException(ErrorCode.UNRELEASED_RECORD);
        }
        salesInvoiceManager.updateStatus(invoice, user.getEmployee(), Record.STAT_CLOSED);
        return (Invoice) salesInvoiceManager.load(invoice.getId());
    }

    private void addFlowControlIfRequired(DomainUser user, Invoice invoice, Date datePaid) {
        try {
            if (projects != null && projectFcsManager != null && systemConfigs != null
                    && !invoice.isPartial() && isSet(invoice.getBusinessCaseId())
                    && systemConfigs.isSystemPropertyEnabled("salesInvoicePaymentFcsSupport")) {

                Sales sales = projects.load(invoice.getBusinessCaseId());
                if (!sales.isClosed()) {
                    List<FcsAction> todo = projectFcsManager.createActionList(sales);
                    FlowControlAction invResponse = fetchInvoiceResponse(sales, todo);
                    if (invResponse != null) {
                        StringBuilder note = new StringBuilder(invoice.getNumber());
                        note.append(" / ").append(DateFormatter.getDate(invoice.getCreated()));
                        projectFcsManager.addFlowControl(
                                user.getEmployee(), 
                                sales, 
                                invResponse, 
                                datePaid == null ? new Date(System.currentTimeMillis()) : datePaid, 
                                note.toString(),  // note 
                                null,  // headline
                                null,  // closing, 
                                true); // ignoreDependencies
                        if (log.isDebugEnabled()) {
                            log.warn("addFlowControlIfRequired() done [sales=" + sales.getId() 
                                    + ", invoice=" + invoice.getId()
                                    + ", fcs=" + invResponse.getId() + "]");
                        }
                    }
                }
            }
        } catch (Exception e) {
            // optional operation should not cause adding payment to fail
            log.warn("addFlowControlIfRequired() failed [message=" + e.getMessage() + "]", e);
        }
    }
    
    private FlowControlAction fetchInvoiceResponse(Sales sales, List<FcsAction> todo) {
        for (Iterator<FcsAction> i = todo.iterator(); i.hasNext();) {
            FlowControlAction action = (FlowControlAction) i.next();
            if (action.isInvoiceResponse()) {
                return action;
            }
        }
        return null;
    }
}
