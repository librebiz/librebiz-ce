/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.common.util.DateUtil

import com.osserp.core.BusinessType
import com.osserp.core.BusinessTypeManager
import com.osserp.core.BusinessTypeSelection
import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeSearch
import com.osserp.core.sales.Sales
import com.osserp.core.sales.SalesCreator

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class SalesRelationCreatorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(SalesRelationCreatorView.class.getName())
    private static final String IGNORE_BRANCH_FILTER = 'create_suborder_ignore_branch'
    BusinessTypeSelection businessTypeSelection
    List<BusinessType> businessTypes = []
    BusinessType businessType
    List<Employee> salesPersons = []
    Employee salesPerson
    String salesName

    Sales selectedSales

    SalesRelationCreatorView() {
        dependencies = ['businessCaseView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!env.businessCaseView?.bean) {
            logger.warn("initRequest: not bound [user=${user?.id}, name=businessCaseView]")
            throw new ViewContextException('missing.businessCaseView', this)
        }
        if (forwardRequest) {
            env.saveTarget = createTarget('/loadSales.do')
            businessTypeSelection = businessTypeManager.findTypeSelection('salesRelationCreator')
            businessTypes = businessTypeSelection.getTypes()
            if (!isPermissionGrant(IGNORE_BRANCH_FILTER)) {
                Employee employee = getDomainEmployee()
                for (Iterator i = businessTypes.iterator(); i.hasNext();) {
                    BusinessType businessType = i.next()
                    if (!employee.isSupportingCompany(businessType.company)) {
                        logger.debug("initRequest: filtered business type [id=${businessType.id}]")
                        i.remove()
                    }
                }
            }
            selectedSales = env.businessCaseView.bean
            salesName = selectedSales.name
            salesPersons = salesPersonList
            if (selectedSales.getSalesId()) {
                salesPerson = employeeSearch.find(selectedSales.getSalesId())
            }
        }
    }

    @Override
    void save() {
        Long typeId = form.getLong('typeId')
        businessTypes.each {
            if (typeId && typeId == it.id) {
                businessType = it
            }
        }
        Long sales = form.getLong('salesId')
        if (sales && sales != selectedSales?.id) {
            salesPersons.each {
                if (sales == it.id) {
                    salesPerson = it
                }
            }
        }
        salesName = form.getString('salesName')
        logger.debug("save: fetched values [typeId=$typeId, salesId=$sales, salesName=$salesName]")
        bean = salesCreator.create(domainUser, salesPerson, selectedSales, businessType, salesName)
    }

    String getSaveTarget() {
        env.saveTarget
    }

    protected SalesCreator getSalesCreator() {
        getService(SalesCreator.class.getName())
    }
    
    protected BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }
}

