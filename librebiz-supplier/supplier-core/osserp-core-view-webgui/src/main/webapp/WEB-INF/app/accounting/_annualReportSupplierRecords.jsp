<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
    <table class="table">
        <thead>
            <tr>
                <th class="recordDate"><v:sortLink key="created">
                        <fmt:message key="date" />
                    </v:sortLink></th>
                <th class="recordNumber"><fmt:message key="number" /></th>
                <th class="recordName"><fmt:message key="name" /></th>
                <th class="recordAmount"><fmt:message key="amount" /></th>
                <th class="recordVat"><fmt:message key="turnoverTax1" /></th>
                <th class="recordVat"><fmt:message key="turnoverTax2" /></th>
                <th class="recordAmount"><fmt:message key="total" /></th>
                <th class="recordAction"></th>
            </tr>
        </thead>
        <tbody>
            <c:choose>
                <c:when test="${empty view.list}">
                    <tr>
                        <td colspan="8"><fmt:message key="noRecordsAvailable" /></td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach var="record" items="${view.list}" varStatus="s">
                        <c:if test="${!record.payment or (record.payment and record.commonPayment)}">
                        <tr id="record${record.id}"<c:if test="${record.commonPayment && !record.outpayment}"> class="altrow"</c:if>>
                            <c:choose>
                                <c:when test="${record.canceled}">
                                    <td class="recordDate canceled"><o:date value="${record.created}"/></td>
                                    <td class="recordName canceled"><o:out value="${record.name}" /></td>
                                    <td class="recordNumber canceled">
                                        <c:choose>
                                            <c:when test="${record.commonPayment}">
                                                <v:link url="/accounting/commonPayment/forward?id=${record.id}&exit=/accounting/annualReport/reload&exitId=record${record.id}">
                                                    <o:out value="${record.note}" limit="35" />
                                                </v:link>
                                            </c:when>
                                            <c:when test="${record.purchaseRecord}">
                                                <a  href="<c:url value="/purchaseInvoice.do?method=display&exit=annualReport&readonly=true&id=${record.id}&exitId=record${record.id}"/>"><o:out value="${record.number}" /></a>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${record.number}" />
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="recordAmount canceled"><o:number value="${record.amount}" format="currency" /></td>
                                    <c:choose>
                                        <c:when test="${record.taxFree}">
                                            <td class="recordVat canceled"><o:number value="0.00" format="currency" /></td>
                                            <td class="recordVat canceled"><o:number value="0.00" format="currency" /></td>
                                            <td class="recordAmount canceled"><o:number value="${record.gross}" format="currency" /></td>
                                        </c:when>
                                        <c:otherwise>
                                            <td class="recordVat canceled"><o:number value="${record.tax}" format="currency" /></td>
                                            <td class="recordVat canceled"><o:number value="${record.reducedTax}" format="currency" /></td>
                                            <td class="recordAmount canceled"><o:number value="${record.gross}" format="currency" /></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td class="recordAction">
                                        <c:choose>
                                            <c:when test="${record.commonPayment}">
                                                <c:if test="${!empty record.documentId}">
                                                    <v:link url="/dms/document/print?id=${record.documentId}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                </c:if>
                                            </c:when>
                                            <c:when test="${record.purchaseRecord and record.unchangeable}">
                                                <v:link url="/records/recordPrint/print?name=purchaseInvoice&id=${record.id}" title="documentPrint">
                                                    <o:img name="printIcon" />
                                                </v:link>
                                            </c:when>
                                        </c:choose>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td class="recordDate"><o:date value="${record.created}" /></td>
                                    <td class="recordName"><o:out value="${record.name}" /></td>
                                    <td class="recordNumber">
                                        <c:choose>
                                            <c:when test="${record.commonPayment}">
                                                <v:link url="/accounting/commonPayment/forward?id=${record.id}&exit=/accounting/annualReport/reload&exitId=record${record.id}">
                                                    <o:out value="${record.note}" limit="35" />
                                                </v:link>
                                            </c:when>
                                            <c:when test="${record.purchaseRecord}">
                                                <a  href="<c:url value="/purchaseInvoice.do?method=display&exit=annualReport&readonly=true&id=${record.id}&exitId=record${record.id}"/>"><o:out value="${record.number}" /></a>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${record.number}" />
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="recordAmount"><o:number value="${record.amount}" format="currency" /></td>
                                    <c:choose>
                                        <c:when test="${record.taxFree}">
                                            <td class="recordVat"><o:number value="0.00" format="currency" /></td>
                                            <td class="recordVat"><o:number value="0.00" format="currency" /></td>
                                            <c:choose>
                                                <c:when test="${record.amountOpen}">
                                                    <td class="recordAmount error"><o:number value="${record.gross}" format="currency" /></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="recordAmount"><o:number value="${record.gross}" format="currency" /></td>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            <td class="recordVat"><o:number value="${record.tax}" format="currency" /></td>
                                            <td class="recordVat"><o:number value="${record.reducedTax}" format="currency" /></td>
                                            <c:choose>
                                                <c:when test="${record.amountOpen}">
                                                    <td class="recordAmount error"><o:number value="${record.gross}" format="currency" /></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="recordAmount"><o:number value="${record.gross}" format="currency" /></td>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                    <td class="recordAction">
                                        <c:choose>
                                            <c:when test="${record.commonPayment}">
                                                <c:if test="${!empty record.documentId}">
                                                    <v:link url="/dms/document/print?id=${record.documentId}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                </c:if>
                                            </c:when>
                                            <c:when test="${record.purchaseRecord and record.unchangeable}">
                                                <v:link url="/records/recordPrint/print?name=purchaseInvoice&id=${record.id}" title="documentPrint">
                                                    <o:img name="printIcon" />
                                                </v:link>
                                            </c:when>
                                        </c:choose>
                                    </td>
                                </c:otherwise>
                            </c:choose>
                        </tr>
                        </c:if>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </tbody>
    </table>
</div>
