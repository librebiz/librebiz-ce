<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productPriceByQuantityView"/>
<c:set var="productPriceAwareView" scope="request" value="${view}"/>
<c:set var="productPriceEditable" scope="request" value="true"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="headline">
		<o:out  value="${view.productName}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
		<div class="subcolumns">

			<div class="subcolumn">
				<div class="spacer"></div>

				<div class="table-responsive table-responsive-default">
					<c:choose>
						<c:when test="${view.editMode}">
							<c:import url="_salesPriceByQuantityEditor.jsp"/>
						</c:when>
						<c:otherwise>
							<c:import url="_purchasePriceDisplay.jsp"/>
							<c:import url="_salesPriceByQuantityDisplay.jsp"/>
						</c:otherwise>
					</c:choose>
				</div>

				<div class="spacer"></div>
			</div>
		</div>

	</tiles:put>
</tiles:insert>