/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 10:27:21 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;
import java.util.List;

import com.osserp.common.Option;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactListEntry;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.contacts.ClassifiedContact;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ClassifiedContacts extends Persons {

    /**
     * Creates a new related contact based on given contact
     * @param createdBy
     * @param contact related
     * @param customId
     * @return new created contact
     */
    ClassifiedContact create(Long createdBy, Contact contact, Long customId);

    /**
     * Indicates existing contact relation
     * @param id
     * @return true if classified contact with provided id exists
     */
    boolean exists(Long id);

    /**
     * Tries to find a related contact by id
     * @param id
     * @return contact or null if not exists
     */
    ClassifiedContact find(Long id);

    /**
     * Tries to find a related contact by related contact
     * @param contactId
     * @return relatedContact contact or null if none exists
     */
    ClassifiedContact findByContact(Long contactId);

    /**
     * Tries to find a related contact by related contact
     * @param contact
     * @return contact
     */
    ClassifiedContact findByContact(Contact contact);

    /**
     * Finds classified contact by email address
     * @param emailAddress
     * @return contact or null if not exist
     */
    ClassifiedContact findByEmail(String emailAddress);

    /**
     * Persists contact and contact relation values
     * @param obj
     */
    void save(ClassifiedContact obj);

    /**
     * Updates common data
     * @param contact
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param birthDate
     * @param grantEmail
     * @param grantEmailNote
     * @param grantPhone
     * @param grantPhoneNote
     * @param grantNone
     * @param grantNoneNote
     * @return
     */
    ClassifiedContact update(
            ClassifiedContact contact,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Date birthDate,
            boolean grantEmail,
            String grantEmailNote,
            boolean grantPhone,
            String grantPhoneNote,
            boolean grantNone,
            String grantNoneNote);

    /**
     * Persists related contact values
     * @param contact
     * @param status
     */
    void updateContactStatus(ClassifiedContact contact, Long status);

    /**
     * Provides a list of all contacts related to implementing class The query to execute is given as param
     * @return list
     */
    List<ContactListEntry> findAll();

    /**
     * Provides a list of all names as option list. The name attribute provides a lastname, firstname string or the company string.
     * @return names
     */
    List<Option> findNames();

}
