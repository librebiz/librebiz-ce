/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 8, 2012 at 14:43:04 
 * 
 */
package com.osserp.core.tasks.impl;

import java.util.Map;

import com.osserp.common.service.MessageSender;
import com.osserp.common.service.impl.AbstractMessageInitiator;

import com.osserp.core.products.Product;
import com.osserp.core.tasks.ProductChangedSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductChangedSenderImpl extends AbstractMessageInitiator implements ProductChangedSender {

    /**
     * Creates a new product changed event sender
     * @param sender
     * @param destinationQueue
     */
    protected ProductChangedSenderImpl(MessageSender sender, String destinationQueue) {
        super(sender, destinationQueue, "productChangedTask");
    }

    /* (non-Javadoc)
     * @see com.osserp.core.tasks.ProductChangedSender#send(java.util.Map)
     */
    public void send(Map<String, Object> eventChangedValues) {
        super.send(eventChangedValues);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.tasks.ProductChangedSender#send(java.lang.Long, java.lang.String)
     */
    public void send(Long productId, String eventName) {
        super.send(createPayload(productId, eventName));
    }

    /* (non-Javadoc)
     * @see com.osserp.core.tasks.ProductChangedSender#sendChanged(java.lang.Long)
     */
    public void sendChanged(Long productId) {
        if (productId != null && productId > 0) {
            Map<String, Object> eventChangedValues = createPayload(productId, ProductChangedSender.EVENT_PRODUCT_CHANGED);
            super.send(eventChangedValues);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.tasks.ProductChangedSender#syncPublic()
     */
    public void syncPublic() {
        Map<String, Object> eventChangedValues = createPayload(null, ProductChangedSender.EVENT_PRODUCT_CHANGED);
        super.send(eventChangedValues);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.tasks.ProductChangedSender#syncPublicMedia()
     */
    public void syncPublicMedia() {
        Map<String, Object> eventChangedValues = createPayload(null, ProductChangedSender.EVENT_PRODUCT_MEDIA_CHANGED);
        eventChangedValues.put(ProductChangedSender.PRODUCT_DOCUMENT_TYPE, Product.PICTURE);
        super.send(eventChangedValues);
        eventChangedValues.put(ProductChangedSender.PRODUCT_DOCUMENT_TYPE, Product.DATASHEET);
        super.send(eventChangedValues);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.tasks.ProductChangedSender#sendMediaChanged(java.lang.Long, java.lang.Long, java.lang.Long)
     */
    public void sendMediaChanged(Long productId, Long documentType, Long documentId) {
        Map<String, Object> eventChangedValues = createPayload(productId, ProductChangedSender.EVENT_PRODUCT_MEDIA_CHANGED);
        eventChangedValues.put(ProductChangedSender.PRODUCT_DOCUMENT_TYPE, documentType);
        eventChangedValues.put(ProductChangedSender.PRODUCT_DOCUMENT_ID, documentId);
        super.send(eventChangedValues);
    }

    private Map<String, Object> createPayload(Long productId, String eventName) {
        Map<String, Object> eventChangedValues = new java.util.HashMap<String, Object>();
        if (productId != null && productId > 0) {
            eventChangedValues.put(ProductChangedSender.PRODUCT_ID, productId);
        }
        eventChangedValues.put(ProductChangedSender.EVENT, eventName);
        return eventChangedValues;
    }
}
