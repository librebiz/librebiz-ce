/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 17:10:00 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.NumberUtil;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public abstract class AbstractDownpayment extends AbstractSalesInvoice implements Downpayment {
    private static Logger log = LoggerFactory.getLogger(AbstractDownpayment.class.getName());
    private boolean fixedAmount = false;
    private BigDecimal prorateAmount = null;
    private BigDecimal proratePercent = null;
    private Double prorateTax = null;
    private BigDecimal amount = null;
    private BigDecimal taxAmount = null;
    private BigDecimal reducedTaxAmount = null;
    private BigDecimal grossAmount = null;
    private Date dateCanceled = null;
    private Long idCanceled = null;
    private Cancellation cancellation = null;

    /**
     * Default constructor required by Serializable
     */
    protected AbstractDownpayment() {
        super();
    }

    /**
     * Creates a new downpayment by order
     * @param id
     * @param type
     * @param order
     * @param createdBy
     */
    protected AbstractDownpayment(
            Long id,
            RecordType type,
            Order order,
            Employee createdBy) {
        super(id, type, order, createdBy);
    }

    public void updateProrate(BigDecimal pAmount) {
        if (pAmount != null && pAmount.doubleValue() != 0) {
            amount = pAmount;
            prorateAmount = pAmount;
            proratePercent = null;
            if (isTaxFree()) {
                grossAmount = pAmount;
            } else {
                prorateTax = getAmounts().getTaxRate();
                if (isReducedTaxAvailable()) {
                    BigDecimal percent = NumberUtil.getPercent(
                            amounts.getAmount(), pAmount, NumberUtil.DIGITS_PERCENT);
                    if (isSet(amounts.getReducedTaxAmount())) {
                        reducedTaxAmount = amounts.getReducedTaxAmount().multiply(percent);
                        taxAmount = amounts.getTaxAmount().multiply(percent);
                        grossAmount = amount.add(reducedTaxAmount).add(taxAmount);
                        if (isSet(amounts.getAmountWithZeroTax())) {
                            BigDecimal zeroAmountPercent = amounts.getAmountWithZeroTax().multiply(percent);
                            grossAmount = grossAmount.add(zeroAmountPercent);
                        }
                    } else if (isSet(amounts.getAmountWithZeroTax())) {
                        taxAmount = amounts.getTaxAmount().multiply(percent);
                        grossAmount = amount.add(taxAmount);
                    }
                } else {
                    grossAmount = NumberUtil.round(pAmount.multiply(new BigDecimal(prorateTax)), 2);
                    taxAmount = grossAmount.subtract(pAmount);
                }
            }
        }
    }

    public void cancel(Employee user, Cancellation cancellation) {
        setStatus(Record.STAT_CANCELED);
        setCanceled(true);
        if (cancellation != null) {
            dateCanceled = cancellation.getCreated();
            idCanceled = cancellation.getId();
            this.cancellation = cancellation;
        } else {
            dateCanceled = new Date();
        }
    }

    public boolean isInvoiceBehaviour() {
        return (amount == null);
    }

    public boolean isFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(boolean fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    protected BigDecimal getProrateAmount() {
        return prorateAmount;
    }

    protected void setProrateAmount(BigDecimal prorateAmount) {
        this.prorateAmount = prorateAmount;
    }

    public BigDecimal getProratePercent() {
        return proratePercent;
    }

    protected void setProratePercent(BigDecimal proratePercent) {
        this.proratePercent = proratePercent;
    }

    protected Double getProrateTax() {
        return prorateTax;
    }

    protected void setProrateTax(Double prorateTax) {
        this.prorateTax = prorateTax;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    protected void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    protected void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getReducedTaxAmount() {
        return reducedTaxAmount;
    }

    protected void setReducedTaxAmount(BigDecimal reducedTaxAmount) {
        this.reducedTaxAmount = reducedTaxAmount;
    }

    public BigDecimal getGrossAmount() {
        return grossAmount;
    }

    protected void setGrossAmount(BigDecimal grossAmount) {
        this.grossAmount = grossAmount;
    }

    public Double getTax() {
        return (getAmounts() != null
                && getAmounts().getTaxRate() != null
                ? getAmounts().getTaxRate()
                : Constants.DOUBLE_NULL);
    }

    public Double getReducedTax() {
        return (getAmounts() == null
                && amounts.getReducedTaxRate() != null
                ? getAmounts().getReducedTaxRate()
                : Constants.DOUBLE_NULL);
    }

    public Date getDateCanceled() {
        return dateCanceled;
    }

    protected void setDateCanceled(Date dateCanceled) {
        this.dateCanceled = dateCanceled;
    }

    public Long getIdCanceled() {
        return idCanceled;
    }

    protected void setIdCanceled(Long idCanceled) {
        this.idCanceled = idCanceled;
    }

    public Cancellation getCancellation() {
        return cancellation;
    }

    public void setCancellation(Cancellation cancellation) {
        this.cancellation = cancellation;
    }

    @Override
    public boolean isDownpayment() {
        return true;
    }

    @Override
    public BigDecimal getDueAmount() {
        if (dueAmount != null) {
            return dueAmount;
        }
        int paymentSize = getPayments().size();
        if (paymentSize == 0) {
            return getBaseAmountForDueAmount();
        }
        BigDecimal total = new BigDecimal(0);
        for (int i = 0; i < paymentSize; i++) {
            Payment p = getPayments().get(i);
            total = total.add(p.getAmount());
        }
        if (log.isDebugEnabled()) {
            log.debug("getDueAmount() total paid: " + total);
        }
        dueAmount = NumberUtil.round(getBaseAmountForDueAmount().subtract(total), 2);
        if (log.isDebugEnabled()) {
            log.debug("getDueAmount() due amount: " + dueAmount);
        }
        return dueAmount;
    }

    protected void createItems(Order order, Long type) throws ClientException {
        updateItems(order);
        prorateTax = amounts.getTaxRate();
        if (Downpayment.DELIVERY.equals(type) || Downpayment.DOWNPAYMENT.equals(type)) {
            // check that downpayment has an amount
            if (amounts == null || isNotSet(amounts.getAmount())) {
                throw new ClientException(ErrorCode.AMOUNT_MISSING);
            }
            RecordPaymentAgreement pa = order.getPaymentAgreement();
            BigDecimal percent = null;
            taxAmount = new BigDecimal(0);
            if (!pa.isAmountsFixed()) {
                // amount is prorated
                percent = getPercent(pa, type);
                if (isNotSet(percent)) {
                    throw new ClientException(ErrorCode.PAYMENT_STEPS_INVALID);
                }
                proratePercent = percent;
                amount = amounts.getAmount().multiply(proratePercent);
                if (isTaxFree()) {
                    grossAmount = amount;
                } else if (isSet(amounts.getReducedTaxAmount())) {
                    reducedTaxAmount = amounts.getReducedTaxAmount().multiply(percent);
                    taxAmount = amounts.getTaxAmount().multiply(percent);
                    grossAmount = amount.add(reducedTaxAmount).add(taxAmount);
                } else if (isSet(amounts.getAmountWithZeroTax())) {
                    taxAmount = amounts.getTaxAmount().multiply(percent);
                    grossAmount = amount.add(taxAmount);
                } else {
                    grossAmount = NumberUtil.round(amount.multiply(new BigDecimal(prorateTax)), 2);
                    taxAmount = NumberUtil.round(grossAmount.subtract(amount), 2);
                }
            } else {
                // amount is fixed (by payment agreement)
                BigDecimal netAmount = getAmount(pa, type);
                if (isNotSet(netAmount)) {
                    throw new ClientException(ErrorCode.AMOUNT_MISSING);
                }
                updateProrate(netAmount);
            }
        }
    }

    private BigDecimal getPercent(RecordPaymentAgreement pa, Long invoiceType) {
        if (invoiceType.equals(Invoice.DELIVERY)) {
            return new BigDecimal(pa.getDeliveryInvoicePercent());
        }
        return new BigDecimal(pa.getDownpaymentPercent());
    }

    private BigDecimal getAmount(RecordPaymentAgreement pa, Long invoiceType) {
        if (invoiceType.equals(Invoice.DELIVERY)) {
            return new BigDecimal(pa.getDeliveryInvoiceAmount());
        }
        return new BigDecimal(pa.getDownpaymentAmount());
    }

    private BigDecimal getBaseAmountForDueAmount() {
        if (!isTaxFree()) {
            if (grossAmount != null && grossAmount.doubleValue() > 0) {
                return grossAmount;
            }
            return (getAmounts() == null
                    || getAmounts().getGrossAmount() == null
                    || getAmounts().getGrossAmount().doubleValue() == 0)
                    ? new BigDecimal(0) : getAmounts().getGrossAmount();
        }
        if (amount != null && amount.doubleValue() > 0) {
            return amount;
        }
        return (getAmounts() == null
                || getAmounts().getAmount() == null
                || getAmounts().getAmount().doubleValue() == 0)
                ? new BigDecimal(0) : getAmounts().getAmount();
    }
}
