SET search_path = osserp, pg_catalog;

INSERT INTO system_properties VALUES (214, 'addressAutocompletionSupport', 'false', 'java.lang.Boolean', true, false, null, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 214, true);

UPDATE system_properties SET pvalue = '0.14.6', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
