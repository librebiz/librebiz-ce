/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2007 3:39:03 PM 
 * 
 */
package com.osserp.common.web;

import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;

/**
 * 
 * ViewManager is responsible for creating, fetching and removing the backing beans of the view component, e.g. jsp's
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ViewManager {
    private static Logger log = LoggerFactory.getLogger(ViewManager.class.getName());

    private static ViewManager _managerSelf = null;
    private ApplicationContext ctx = null;
    private static final String BACKUP_EXTENSION = "Backup";
    private static final String DATA_EXTENSION = "Data";

    private ViewManager(ApplicationContext ctx) {
        super();
        this.ctx = ctx;
    }

    public static ViewManager newInstance(ServletContext ctx) {
        if (ViewManager._managerSelf == null) {
            ApplicationContext appctx = (ApplicationContext) ctx.getAttribute(Context.APPLICATION_CONTEXT);
            if (appctx == null) {
                appctx = (ApplicationContext) ctx.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
            }
            ViewManager._managerSelf = new ViewManager(appctx);
        }
        return ViewManager._managerSelf;
    }

    public static ViewManager newInstance(ApplicationContext ctx) {
        if (ViewManager._managerSelf == null) {
            ViewManager._managerSelf = new ViewManager(ctx);
        }
        return ViewManager._managerSelf;
    }

    /**
     * Creates new view using application contexts getBean() method, sets current user and invokes views init method.<br>
     * If {@code view.createBackupWhenExisting} enabled, an existing view with same name will be backuped.
     * @param request
     * @param viewClassName
     * @return new created view
     * @throws ClientException if initialization failed
     * @throws PermissionException if user is no domain user
     */
    public View createView(HttpServletRequest request, String viewClassName)
            throws ClientException, PermissionException {
        if (log.isDebugEnabled()) {
            log.debug("createView() invoked [class=" + viewClassName + "]");
        }
        HttpSession session = request.getSession(true);
        View view = create(session, viewClassName);
        injectLocator(request, view);
        if (!(view instanceof PortalView) && session.getAttribute(Globals.PORTAL_VIEW) != null) {
            Object obj = session.getAttribute(Globals.PORTAL_VIEW);
            if (obj instanceof PortalView) {
                PortalView portalView = (PortalView) obj;
                Locale locale = portalView.getUser() == null ? null : portalView.getUser().getLocale();
                if (locale != null) {
                    view.addResourceBundle(locale, portalView.getResourceBundle(locale));
                }
            }
        }
        view.init(request);

        if (view.isCreateBackupWhenExisting()
                && isOtherViewAvailable(session, view)) {

            createViewBackup(session, view);
            session.removeAttribute(view.getName());
        }
        if (log.isDebugEnabled()) {
            log.debug("storing new created view [name=" + view.getName() + ", scope=session]");
        }
        session.setAttribute(view.getName(), view);
        return view;
    }

    /**
     * Creates new view with application context getBean() method and sets current user. View is than returned without storing in any scope
     * @param request
     * @param viewClassName
     * @return view
     * @throws PermissionException if user is no domain user
     */
    public View createViewOnly(HttpServletRequest request, String viewClassName)
            throws PermissionException {
        if (log.isDebugEnabled()) {
            log.debug("createViewOnly() invoked [class=" + viewClassName + "]");
        }
        View view = create(request.getSession(true), viewClassName);
        return injectLocator(request, view);
    }

    /**
     * Tries to fetch view from session scope if exists.
     * @param session
     * @param clazz
     * @return view or null if no view exists
     */
    public static View fetchView(HttpSession session, Class<? extends View> clazz) {
        if (clazz.isAnnotationPresent(ViewName.class)) {
            ViewName viewName = clazz.getAnnotation(ViewName.class);
            Object obj = session.getAttribute(viewName.value());
            if (obj instanceof View && clazz.isAssignableFrom(obj.getClass())) {
                /*
                 * if (log.isDebugEnabled()) { log.debug("fetchView() found by annotation [name=" + viewName.value() + ", class=" + obj.getClass().getName() +
                 * "]"); }
                 */
                return injectLocator(session, (View) obj);
            }
        }
        String lookUpName = clazz.getSimpleName();
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String attributeName = (String) attributeNames.nextElement();
            if (attributeName.indexOf(BACKUP_EXTENSION) < 0) {
                Object next = session.getAttribute(attributeName);
                if (next instanceof View && clazz.isAssignableFrom(next.getClass())) {
                    String nextName = next.getClass().getName();
                    if (nextName.indexOf(lookUpName) > -1) {
                        /*
                         * if (log.isDebugEnabled()) { log.debug("fetchView() found name [name=" + lookUpName + ", class=" + next.getClass().getName() + "]"); }
                         */
                        return injectLocator(session, (View) next);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Removes a view from session scope if exists. Restores view backup if such exists
     * @param session
     * @param clazz of the view
     */
    public static void removeView(HttpSession session, Class<? extends View> clazz) {
        if (clazz.isAnnotationPresent(ViewName.class)) {
            ViewName viewName = clazz.getAnnotation(ViewName.class);
            session.removeAttribute(viewName.value());
        } else {
            String lookUpName = clazz.getSimpleName();
            java.util.Enumeration attributeNames = session.getAttributeNames();
            while (attributeNames.hasMoreElements()) {
                String attributeName = (String) attributeNames.nextElement();
                if (attributeName.indexOf(BACKUP_EXTENSION) < 0) {
                    Object next = session.getAttribute(attributeName);
                    if (next instanceof View) {
                        String nextName = next.getClass().getName();
                        if (nextName.indexOf(lookUpName) > -1) {
                            if (log.isDebugEnabled()) {
                                log.debug("removeView() found required [name="
                                        + next.getClass().getName() + "]");
                            }
                            if (!restoreViewBackupWhenAvailable(
                                    session,
                                    attributeName,
                                    next)) {

                                session.removeAttribute(attributeName);
                            }
                            if (log.isDebugEnabled()) {
                                log.debug("removeView() attribute removed");
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    private static boolean restoreViewBackupWhenAvailable(
            HttpSession session,
            String attributeName,
            Object obj) {
        if (isViewBackupAvailable(session, attributeName)) {
            if (log.isDebugEnabled()) {
                log.debug("removeView() found view backup");
            }
            if (obj instanceof View) {
                View view = (View) obj;
                if (view.isCreateBackupWhenExisting()) {
                    if (log.isDebugEnabled()) {
                        log.debug("removeView() view to remove supports backups, restoring...");
                    }
                    restoreViewBackup(session, attributeName);
                    return true;
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("removeView() found none configurable view, restoring...");
                }
                restoreViewBackup(session, attributeName);
                return true;
            }
        }
        return false;
    }

    public static void createViewBackup(HttpSession session, View view) {
        View other = (View) session.getAttribute(view.getName());
        if (other != null) {
            session.setAttribute(view.getName() + BACKUP_EXTENSION, other);
            if (log.isDebugEnabled()) {
                log.debug("createViewBackup() done [class=" + other.getClass().getName() + "]");
            }
        }
    }

    public static boolean isViewBackupAvailable(HttpSession session, String name) {
        Object obj = session.getAttribute(name + BACKUP_EXTENSION);
        return (obj == null ? false : (obj instanceof View));
    }

    public static void restoreViewBackup(HttpSession session, String name) {
        Object obj = session.getAttribute(name + BACKUP_EXTENSION);
        if (obj != null && obj instanceof View) {
            session.setAttribute(name, injectLocator(session, (View) obj));
            if (log.isDebugEnabled()) {
                log.debug("restoreViewBackup() done [class=" + obj.getClass().getName() + "]");
            }
        }
    }

    /**
     * Stores any object under key 'viewName' + 'Data' in request scope
     * @param request
     * @param viewName
     * @param data
     */
    public static void storeViewData(HttpServletRequest request, String viewName, Map<String, Object> data) {
        request.setAttribute(viewName + DATA_EXTENSION, data);
    }

    /**
     * Stores any object under key 'viewName' + 'Data' in session scope
     * @param session
     * @param viewName
     * @param data
     */
    public static void storeViewData(HttpSession session, String viewName, Map<String, Object> data) {
        session.setAttribute(viewName + DATA_EXTENSION, data);
    }

    /**
     * Tries to fetch view data as set by storeViewData. Method first looks up for data in request scope and then in session scope.<br>
     * Note: This is a one time action. The attribute is removed from retrieved scope immediately after fetching and before delivery
     * @param request
     * @param view
     * @return data or null if none exists
     */
    public static Map<String, Object> fetchViewData(HttpServletRequest request, View view) {
        String key = view.getName() + DATA_EXTENSION;
        Map<String, Object> obj = (Map<String, Object>) request.getAttribute(key);
        if (obj == null) {
            HttpSession session = request.getSession();
            obj = (Map<String, Object>) session.getAttribute(key);
            if (obj != null) {
                session.removeAttribute(key);
            }
        } else {
            request.removeAttribute(key);
        }
        return obj;
    }

    /**
     * Tries to fetch view data from session scope as set by storeViewData. Note: This is a one time action. The attribute is removed from session scope
     * immediately after fetching and before delivery
     * @param session
     * @param view
     * @return data or null if none exists
     */
    public static Map<String, Object> fetchViewData(HttpSession session, View view) {
        String key = view.getName() + DATA_EXTENSION;
        Map<String, Object> obj = (Map<String, Object>) session.getAttribute(key);
        if (obj != null) {
            session.removeAttribute(key);
        }
        return obj;
    }

    private View create(HttpSession session, String viewClassName) throws PermissionException {
        View view = (View) ctx.getBean(viewClassName);
        view.setCurrentUser(SessionUtil.lookupUser(session));
        return view;
    }

    private boolean isOtherViewAvailable(HttpSession session, View view) {
        Object obj = session.getAttribute(view.getName());
        return (obj == null ? false : (obj instanceof View));
    }

    private static View injectLocator(HttpServletRequest request, View view) {
        if (view instanceof AbstractView) {
            ((AbstractView) view).setLocator(ContextUtil.getServiceLocator(request));
        }
        return view;
    }

    private static View injectLocator(HttpSession session, View view) {
        if (view instanceof AbstractView) {
            ((AbstractView) view).setLocator(ContextUtil.getServiceLocator(session));
        }
        return view;
    }
}
