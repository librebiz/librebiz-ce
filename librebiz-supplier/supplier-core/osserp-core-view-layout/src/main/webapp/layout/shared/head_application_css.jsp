<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_default_css.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_default_static_css.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_default_custom_css.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_font_opensans_css.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_font_awesome_css.jsp" />
