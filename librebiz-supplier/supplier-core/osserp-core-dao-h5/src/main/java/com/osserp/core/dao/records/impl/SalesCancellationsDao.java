/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13-Jun-2006 11:28:27 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesCancellations;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.SalesCancellationImpl;
import com.osserp.core.sales.Sales;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesCancellationsDao extends AbstractSalesRecords implements SalesCancellations {
    private String salesCancellationsTable = null;

    public SalesCancellationsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            SalesPayments payments) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                payments);
        salesCancellationsTable = getTable(TableKeys.SALES_CANCELLATIONS);
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.SALES_CANCELLATION;
    }

    public Cancellation create(Employee user, CancellableRecord record, Date date, Long customId) throws ClientException {
        if (customId != null && exists(customId)) {
            throw new ClientException(ErrorCode.ID_EXISTS);
        }
        RecordType recordType = getRecordType();
        SalesCancellationImpl obj =
                new SalesCancellationImpl(
                customId != null ? customId : getNextRecordId(user, record.getCompany(), recordType, null),
                recordType,
                user,
                record,
                date);
        saveInitial(obj);
        return obj;
    }

    public void changeBranch(Sales sales) {
        if (sales != null && sales.getBranch() != null) {
            changeBranchBySales(
                salesCancellationsTable, 
                sales.getId(), 
                sales.getRequest().getBranch().getId(), 
                true);
        }
    }

    public Cancellation loadCancellation(CancellableRecord cancellable) {
        if (cancellable.isCanceled()) {
            List list = getCurrentSession().createQuery(
                    "from " + getPersistentClass().getName() +
                    " o where o.reference = :recordid and o.cancelledType = :typeid")
                    .setParameter("recordid", cancellable.getId())
                    .setParameter("typeid", cancellable.getType().getId())
                    .list();
            if (!list.isEmpty()) {
                return (Cancellation) list.get(0);
            }
        }
        return null;
    }

    public boolean exists(Long recordId) {
        return exists(salesCancellationsTable, recordId);
    }

    public int count(Long referenceId) {
        return count(salesCancellationsTable, referenceId);
    }

    public int countBySales(Long salesId) {
        return countBySales(salesCancellationsTable, salesId);
    }

    public List getPrinted() {
        return getPrinted(salesCancellationsTable);
    }

    public Date getCreated(Long id) {
        return getCreated(salesCancellationsTable, id);
    }

    @Override
    protected Class<SalesCancellationImpl> getPersistentClass() {
        return SalesCancellationImpl.class;
    }

    @Override
    protected void loadPayments(PaymentAwareRecord record) {
        if (isSupportingPayments()) {
            SalesCancellationImpl note = (SalesCancellationImpl) record;
            List<Payment> list = getPayments().getByRecord(note);
            for (int i = 0, j = list.size(); i < j; i++) {
                Payment next = list.get(i);
                note.getPayments().add(next);
            }
        }
    }
}
