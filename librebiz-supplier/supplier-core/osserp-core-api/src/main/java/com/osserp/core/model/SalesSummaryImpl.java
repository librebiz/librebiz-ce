/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29.04.2005 14:34:54 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.common.beans.OptionImpl;

import com.osserp.core.BusinessType;
import com.osserp.core.customers.SalesSummary;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesSummaryImpl extends OptionImpl implements SalesSummary {
    private static final long serialVersionUID = 42L;

    private BusinessType type = null;
    private Long salesId = null;
    private Long managerId = null;
    private Integer status = null;
    private String lastAction = null;
    private boolean cancelled = false;
    private boolean stopped = false;

    protected SalesSummaryImpl() {
        super();
    }

    public SalesSummaryImpl(
            Long id,
            BusinessType type,
            Long salesId,
            Long managerId,
            String name,
            Integer status,
            String lastAction,
            Date created,
            boolean cancelled,
            boolean stopped) {

        super(id, name);
        setCreated(created);
        this.type = type;
        this.salesId = salesId;
        this.managerId = managerId;
        this.status = status;
        this.lastAction = lastAction;
        this.cancelled = cancelled;
        this.stopped = stopped;
    }

    public BusinessType getType() {
        return type;
    }

    protected void setType(BusinessType type) {
        this.type = type;
    }

    public Long getTypeId() {
        return (type == null) ? null : type.getId();
    }

    public Long getSalesId() {
        return salesId;
    }

    public void setSalesId(Long salesId) {
        this.salesId = salesId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLastAction() {
        return lastAction;
    }

    public void setLastAction(String lastAction) {
        this.lastAction = lastAction;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isStopped() {
        return stopped;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    public boolean isClosed() {
        return Integer.valueOf(100).equals(status);
    }
}
