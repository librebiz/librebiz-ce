/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 14, 2006 10:57:59 PM 
 * 
 */
package com.osserp.common.util;

import java.util.Date;
import java.util.GregorianCalendar;

import com.osserp.common.Calendar;
import com.osserp.common.beans.CalendarImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalendarUtil {

    /**
     * Creates a new calendar object
     * @return new calendar object
     */
    public static Calendar createCalendar() {
        CalendarImpl obj = new CalendarImpl();
        return obj;
    }

    /**
     * @param day
     * @param month
     * @param year
     * @return number of day in week (1=mon ... 7=sun)
     */
    public static int getDayOfWeek(Integer day, Integer month, Integer year) {
        int[] date = fetchValues(day, month, year);
        GregorianCalendar cal = new GregorianCalendar(date[2], date[1], date[0]);
        int tmp = DateUtil.getDayOfWeek(cal.getTime());
        return (tmp == 1) ? 7 : tmp - 1;
    }

    /**
     * @param day
     * @param month
     * @param year
     * @return number of given week in year
     */
    public static int getWeekOfYear(Integer day, Integer month, Integer year) {
        int[] date = fetchValues(day, month, year);
        GregorianCalendar cal = new GregorianCalendar(date[2], date[1], date[0]);
        return DateUtil.getWeekOfYear(cal.getTime());
    }

    private static int[] fetchValues(Integer day, Integer month, Integer year) {
        int[] result = new int[3];
        result[0] = (day != null && day > 0) ? day : DateUtil.getCurrentDay();
        result[1] = (month != null) ? month : DateUtil.getCurrentMonth();
        result[2] = (year != null && year > 0) ? year : DateUtil.getCurrentYear();
        return result;
    }

    public static String getReverseDate() {
        return getReverseDate(null);
    }

    public static String getReverseDate(Date date) {
        Calendar cal = createCalendar();
        if (date != null) {
            Integer[] dateVals = DateUtil.getTime(date);
            cal.setDate(dateVals[0], dateVals[1], dateVals[2]);
        }
        StringBuilder buffer = new StringBuilder(cal.getYear().toString());
        String month = Integer.valueOf(cal.getMonth().intValue() + 1).toString();
        if (month.length() < 2) {
            buffer.append("0");
        }
        buffer.append(month);
        String day = cal.getDay().toString();
        if (day.length() < 2) {
            buffer.append("0");
        }
        buffer.append(day);
        return buffer.toString();
    }
}
