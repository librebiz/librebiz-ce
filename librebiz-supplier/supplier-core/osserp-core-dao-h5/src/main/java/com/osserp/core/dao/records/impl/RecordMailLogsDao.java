/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 26, 2008 12:36:48 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateDao;

import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordMailLog;
import com.osserp.core.model.records.RecordMailLogImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordMailLogsDao extends AbstractHibernateDao implements RecordMailLogs {
    private static Logger log = LoggerFactory.getLogger(RecordMailLogsDao.class.getName());

    protected RecordMailLogsDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public void create(
            Employee user,
            Record record,
            String originator,
            String[] recipients,
            String[] recipientsCC,
            String[] recipientsBCC,
            String messageID) {

        StringBuilder recipient = new StringBuilder(128);
        if (recipients != null) {
            for (int i = 0, j = recipients.length; i < j; i++) {
                recipient.append(recipients[i]);
                if (i < j - 1) {
                    recipient.append(", ");
                }
            }
        }
        StringBuilder recipientCC = new StringBuilder(128);
        if (recipientsCC != null) {
            for (int i = 0, j = recipientsCC.length; i < j; i++) {
                recipientCC.append(recipientsCC[i]);
                if (i < j - 1) {
                    recipientCC.append(", ");
                }
            }
        }
        StringBuilder recipientBCC = new StringBuilder(128);
        if (recipientsBCC != null) {
            for (int i = 0, j = recipientsBCC.length; i < j; i++) {
                recipientBCC.append(recipientsBCC[i]);
                if (i < j - 1) {
                    recipientBCC.append(", ");
                }
            }
        }
        create(user,
                record,
                originator,
                recipient.toString(),
                recipientCC.toString(),
                recipientBCC.toString(),
                messageID);
    }

    public void create(
            Employee user,
            Record record,
            String originator,
            String recipients,
            String recipientsCC,
            String recipientsBCC,
            String messageID) {
        RecordMailLogImpl obj = new RecordMailLogImpl(
                user.getId(),
                record,
                originator,
                recipients,
                recipientsCC,
                recipientsBCC,
                messageID);
        getCurrentSession().save(RecordMailLogImpl.class.getName(), obj);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + obj.getId()
                    + ", user=" + user.getId()
                    + ", record=" + (record == null ? "null" : record.getId())
                    + "]");
        }
    }

    public List<RecordMailLog> find(Record record) {
        StringBuilder hql = new StringBuilder(128);
        hql
                .append("from ")
                .append(RecordMailLogImpl.class.getName())
                .append(" o where o.reference = :referenceId and o.type = :typeId order by o.id desc");
        List<RecordMailLog> result = getCurrentSession().createQuery(hql.toString())
                .setParameter("referenceId", record.getId())
                .setParameter("typeId", record.getType().getId())
                .list();
        return result;
    }
}
