/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jul-2005 12:02:19 
 * 
 */
package com.osserp.core.suppliers;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.contacts.RelatedContactSearch;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SupplierSearch extends RelatedContactSearch {

    /**
     * Provides the supplier values of supplier with given id
     * @param id
     * @return supplier values
     * @throws ClientException if no supplier with id exists
     */
    Supplier findById(Long id) throws ClientException;

    /**
     * Finds all suppliers dealing with given product group
     * @param groupId
     * @return suppliers
     */
    List<Supplier> findByProductGroup(Long groupId);

    /**
     * Finds all suppliers delivering specified product
     * @param productId
     * @return suppliers
     */
    List<Supplier> findByDeliveries(Long productId);

    /**
     * Finds all suppliers whose direct invoice booking flag is enabled
     * @return suppliers or empty list
     */
    List<Supplier> findByDirectInvoiceBooking();

    /**
     * Finds all suppliers whose simple billing flag is enabled. This method
     * provides all suppliers including employee accounts. 
     * @return simple billing suppliers including employee salary accounts.
     */
    List<Supplier> findBySimpleBilling();
    
    /**
     * Provides all employee salary accounts (e.g. suppliers with employee 
     * reference and enabled simpleBilling flag. These virtual suppliers are 
     * used for salary operations. 
     * @return employee salary accounts
     */
    List<Supplier> getSalaryAccounts();
    
    /**
     * Provides all supplier accounts with enabled simple billing excluding 
     * employee salary accounts.
     * @return simple billing accounts
     */
    List<Supplier> getSimpleBillingAccounts();
    
    /**
     * Provides all activated supplier types (excluding eol).
     * @return available supplier types
     */
    List<SupplierType> getSupplierTypeSelection();

}
