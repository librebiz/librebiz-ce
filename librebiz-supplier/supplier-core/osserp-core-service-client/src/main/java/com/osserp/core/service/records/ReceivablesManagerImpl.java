/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 11, 2007 9:15:52 PM 
 * 
 */
package com.osserp.core.service.records;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.StringUtil;

import com.osserp.core.dao.ProjectQueries;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.finance.ReceivableDisplay;
import com.osserp.core.finance.ReceivablesManager;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.service.doc.XmlService;
import com.osserp.core.service.impl.AbstractOutputService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ReceivablesManagerImpl extends AbstractOutputService implements ReceivablesManager {
    private static Logger log = LoggerFactory.getLogger(ReceivablesManagerImpl.class.getName());
    private static final String SALES_RECEIVABLES_STYLESHEET = "xsl/documents/account_ballance";
    private SalesOrderDownpayments downpayments = null;
    private SalesInvoices invoices = null;
    private ProjectQueries projectQueries = null;

    protected ReceivablesManagerImpl(
            ResourceLocator resourceLocator,
            OptionsCache optionsCache, 
            CoreStylesheetService stylesheetService, 
            XmlService xmlService,
            SalesOrderDownpayments downpayments,
            SalesInvoices invoices,
            ProjectQueries projectQueries) {
        super(resourceLocator, optionsCache, stylesheetService, xmlService);
        this.downpayments = downpayments;
        this.invoices = invoices;
        this.projectQueries = projectQueries;
    }

    public List<RecordDisplay> getInvoices(boolean descending) {
        List<RecordDisplay> result = invoices.findWithPayments(descending);
        Map<Long, RecordDisplay> invoiceByOrderMap = createByOrderMap(result);
        List<RecordDisplay> downpaymentList = downpayments.findWithPayments(descending);
        for (Iterator<RecordDisplay> i = downpaymentList.iterator(); i.hasNext();) {
            RecordDisplay next = i.next();
            if (invoiceByOrderMap.containsKey(next.getReference())) {
                if (log.isDebugEnabled()) {
                    log.debug("getInvoices() should remove downpayment "
                            + next.getId() + " with amount "
                            + next.getDueAmount());
                }
            }
            result.add(next);
        }
        if (descending) {
            Collections.sort(result, new RecordDisplayByCreatedDescComparator());
        } else {
            Collections.sort(result, new RecordDisplayByCreatedComparator());
        }
        return result;
    }

    public List<RecordDisplay> getInvoices(Long customerId, boolean descending) {
        List<RecordDisplay> result = new ArrayList<>();
        List<RecordDisplay> outstandingPayments = invoices.findWithPayments(descending);
        for (int i = 0, j = outstandingPayments.size(); i < j; i++) {
            RecordDisplay next = outstandingPayments.get(i);
            if (next.getContactId().equals(customerId)) {
                result.add(next);
            }
        }
        return result;
    }

    private Map<Long, RecordDisplay> createByOrderMap(List<RecordDisplay> invoiceList) {
        Map<Long, RecordDisplay> result = new HashMap<Long, RecordDisplay>();
        for (int i = 0, j = invoiceList.size(); i < j; i++) {
            RecordDisplay next = invoiceList.get(i);
            if (next.getReference() != null) {
                result.put(next.getReference(), next);
            }
        }
        return result;
    }

    public List<ReceivableDisplay> getSalesReceivables() {
        return projectQueries.getByReceivables();
    }
    
    public byte[] createSalesReceivablesPdf(List<ReceivableDisplay> salesReceivableDisplay) throws ClientException{
        Document xml = createSalesReceivableXml(salesReceivableDisplay);
        return stylesheetService.createPdf(xml, SALES_RECEIVABLES_STYLESHEET);
    }
    
    public String createSalesReceivablesSheet(List<ReceivableDisplay> receivables) {
        List<String[]> valueList = new ArrayList<String[]>();
        boolean headerAdded = false;
        for (int i = 0, j = receivables.size(); i < j; i++) {
            ReceivableDisplay next = receivables.get(i);
            if (!headerAdded) {
                valueList.add(next.getSheetHeader());
                headerAdded = true;
            }
            valueList.add(next.getSheetValues());
        }
        return StringUtil.createSheet(valueList);
        
    }

    private Document createSalesReceivableXml(List<ReceivableDisplay> salesReceivableList) {
        Double totalAmount = 0d;
        Element root = new Element("outstandingPayments");
        root.addContent(new Element("currentDate").setText(DateFormatter.getCurrentDate()));
        for (int i = 0, j = salesReceivableList.size(); i < j; i++) {
            ReceivableDisplay oad = salesReceivableList.get(i);
            totalAmount = totalAmount + oad.getAmount();
            root.addContent(createSalesReceivableDisplayXml(oad));
        }
        if (log.isDebugEnabled()) {
            log.debug("refresh() total amount is " + totalAmount);
        }
        root.setAttribute("summary", NumberFormatter.getValue(
                totalAmount,
                NumberFormatter.CURRENCY));
        Document doc = new Document();
        doc.setRootElement(root);
        return doc;
    }

    private Element createSalesReceivableDisplayXml(ReceivableDisplay oad) {
        Element root = new Element("openAmount");
        if (oad.getCustomerId() != null) {
            root.addContent(new Element("customer").setText(
                    oad.getCustomerId().toString()));
        } else {
            root.addContent(new Element("customer"));
        }
        root.addContent(new Element("name").setText(oad.getCustomerName()));
        root.addContent(new Element("sale").setText(oad.getId().toString()));
        root.addContent(new Element("status").setText(oad.getStatus().toString()));
        root.addContent(new Element("created").setText(
                DateFormatter.getDate(oad.getCreated())));
        root.addContent(xmlService.createEmployee("createdBy", oad.getCreatedBy()));
        root.addContent(new Element("amount").setText(
                NumberFormatter.getValue(oad.getAmount(),
                        NumberFormatter.CURRENCY)));
        if (oad.getManagerId() != null) {
            root.addContent(xmlService.createEmployee("manager", oad.getManagerId()));
        } else {
            root.addContent(new Element("manager"));
        }
        if (oad.getSalesId() != null) {
            root.addContent(xmlService.createEmployee("sales", oad.getSalesId()));
        } else {
            root.addContent(new Element("sales"));
        }
        return root;
    }
}
