/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.common.mail;

import java.util.Map;

import com.osserp.common.ClientException;

import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ReceivedMail extends Mail implements ReceivedMailInfo {

    private String originatorName;
    private String recipient;
    private String recipientName;
    private Date receivedDate;
    private Date sentDate;
    private boolean read = false;
    private String cachedFolder;
    private Map<String, String> headers = new HashMap<>();

    public ReceivedMail() {
        super();
    }

    public ReceivedMail(ReceivedMailInfo mailInfo) {
        super(mailInfo.getOriginator());
        setMessageId(mailInfo.getMessageId());
        setSubject(mailInfo.getSubject());
        originatorName = mailInfo.getOriginatorName();
        recipient = mailInfo.getRecipient();
        recipientName = mailInfo.getRecipientName();
        receivedDate = mailInfo.getReceivedDate();
        sentDate = mailInfo.getSentDate();
        read = mailInfo.isRead();
        headers = mailInfo.getHeaders();
    }

    public ReceivedMail(
            Long id,
            Long reference,
            Long referenceType,
            Long businessId,
            Integer status,
            Long userId,
            String originator,
            String recipient,
            String messageId,
            Date sent,
            Date received,
            String subject,
            String text,
            String html) {
        setId(id);
        setReference(
                reference != null && reference < 1 ? null : reference);
        setReferenceType( referenceType != null &&
                referenceType < 1 ? null : referenceType);
        setBusinessId(businessId != null &&
                businessId < 1 ? null : businessId);
        setStatus(status);
        setUserId(userId);
        setOriginator(originator);
        setRecipient(recipient);
        setMessageId(messageId);
        setSubject(subject);
        setText(text);
        setHtml(html);
        receivedDate = received;
        sentDate = sent;
    }

    public String getOriginatorName() {
        return originatorName;
    }

    public void setOriginatorName(String originatorName) {
        this.originatorName = originatorName;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getCachedFolder() {
        return cachedFolder;
    }

    public void setCachedFolder(String cachedFolder) {
        this.cachedFolder = cachedFolder;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    /**
     * Override method to prevent validation of received email address
     * @param recipientCC
     */
    @Override
    public boolean addRecipientCC(String recipientCC) throws ClientException {
        if (!recipientAlreadyAdded(getRecipientsCC(), recipientCC)) {
            getRecipientsCC().add(recipientCC);
            return true;
        }
        return false;
    }

    /**
     * Override method to prevent validation of received email address
     * @param recipient
     */
    @Override
    public boolean addRecipient(String recipient) throws ClientException {
        if (!recipientAlreadyAdded(getRecipients(), recipient)) {
            getRecipients().add(recipient);
            return true;
        }
        return false;
    }
}
