/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 26, 2016 
 * 
 */
package com.osserp.core.service.projects;


import org.jdom2.Document;
import org.jdom2.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.OptionsCache;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseSearch;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.projects.ProjectTracking;
import com.osserp.core.projects.ProjectTrackingRecord;
import com.osserp.core.service.doc.XmlService;
import com.osserp.core.service.impl.AbstractOutputService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectTrackingOutputServiceImpl extends AbstractOutputService implements ProjectTrackingOutputService {
    private static Logger log = LoggerFactory.getLogger(ProjectTrackingOutputServiceImpl.class.getName());
    
    private BusinessCaseSearch businessCaseSearch;
    private SystemConfigs systemConfigs;

    protected ProjectTrackingOutputServiceImpl(
            ResourceLocator resourceLocator,
            OptionsCache optionsCache, 
            CoreStylesheetService stylesheetService, 
            XmlService xmlService,
            BusinessCaseSearch businessCaseSearch,
            SystemConfigs systemConfigs) {
        super(resourceLocator, optionsCache, stylesheetService, xmlService);
        this.businessCaseSearch = businessCaseSearch;
        this.systemConfigs = systemConfigs;
    }

    public DocumentData createPdf(DomainUser user, ProjectTracking tracking) {
        try {
            BusinessCase businessCase = businessCaseSearch.findBusinessCase(tracking.getReference());
            Document document = createXml(user, tracking, businessCase);
            String stylesheetName = tracking.getType().getStylesheetName();
            byte[] result = stylesheetService.createPdf(document,
                    stylesheetService.getStylesheetSource(
                            stylesheetName,
                            businessCase.getBranch().getId(),
                            (user.getLocale() == null ? Constants.DEFAULT_LOCALE_OBJECT : user.getLocale())));
            return new DocumentData(result, 
                    "project_tracking-" + tracking.getId() +  ".pdf", 
                    Constants.MIME_TYPE_PDF);

        } catch (Throwable t) {
            log.error("createPdf() failed [message=" + t.getMessage() + "]");
        }
        return null;
    }

    protected Document createXml(DomainUser user, ProjectTracking tracking, BusinessCase businessCase) {
        Document doc = xmlService.createDocument(user.getEmployee());
        Element root = doc.getRootElement();
        
        Element pt = new Element("projectTracking");
        
        if (isSet(tracking.getName())) {
            pt.addContent(new Element("name").setText(tracking.getName()));
        } else {
            pt.addContent(new Element("name"));
        }
        
        if (isSet(tracking.getDescription())) {
            pt.addContent(new Element("description").setText(
                    tracking.getDescription()));
        } else {
            pt.addContent(new Element("description"));
        }
        
        if (tracking.isPrintWorker() && isSet(tracking.getWorker())) {
            pt.addContent(new Element("printWorker").setText("true"));
            // TODO fetch and add employee
        } else {
            pt.addContent(new Element("printWorker").setText("false"));
        }
        
        if (tracking.isClosed()) {
            pt.addContent(new Element("closed").setText("true"));
        } else {
            pt.addContent(new Element("closed").setText("false"));
        }
        
        if (tracking.getStartDate() != null) {
            pt.addContent(new Element("startDate").setText(DateFormatter.getDate(
                    tracking.getStartDate())));
        } else {
            pt.addContent(new Element("startDate"));
        }
        
        if (tracking.getEndDate() != null) {
            pt.addContent(new Element("endDate").setText(DateFormatter.getDate(
                    tracking.getEndDate())));
        } else {
            pt.addContent(new Element("endDate"));
        }
        
        pt.addContent(new Element("totalTime").setText(NumberFormatter.getValue(
                tracking.getTotalTime(), NumberFormatter.CURRENCY)));
        
        Element items = new Element("items");
        for (int i = 0, j = tracking.getRecords().size(); i < j; i++) {
            ProjectTrackingRecord record = tracking.getRecords().get(i);
            if (!record.isIgnore()) {
                Element item = new Element("item");
                if (record.isSingleDay()) {
                    item.addContent(new Element("singleDay").setText("true"));
                } else {
                    item.addContent(new Element("singleDay").setText("false"));
                }
                if (record.getStartTime() != null) {
                    item.addContent(new Element("startDate").setText(DateFormatter.getDate(
                            record.getStartTime())));
                } else {
                    item.addContent(new Element("startDate"));
                }
                if (record.getEndTime() != null) {
                    item.addContent(new Element("endDate").setText(DateFormatter.getDate(
                            record.getEndTime())));
                } else {
                    item.addContent(new Element("endDate"));
                }
                item.addContent(new Element("duration").setText(NumberFormatter.getValue(
                        record.getDuration(), NumberFormatter.CURRENCY)));
                if (isSet(record.getName())) {
                    item.addContent(new Element("name").setText(record.getName()));
                } else {
                    item.addContent(new Element("name"));
                }
                if (isSet(record.getDescription())) {
                    item.addContent(new Element("description").setText(
                            record.getDescription()));
                } else {
                    item.addContent(new Element("description"));
                }
                if (tracking.getType().isTimeExactly()) {

                    if (isSet(record.getStartHours())) {
                        item.addContent(new Element("startHours").setText(
                                record.getStartHours().toString()));
                    } else {
                        item.addContent(new Element("startHours"));
                    }
                    if (isSet(record.getStartMinutes())) {
                        item.addContent(new Element("startMinutes").setText(
                                record.getStartMinutesDisplay()));
                    } else {
                        item.addContent(new Element("startMinutes"));
                    }
                    if (isSet(record.getEndHours())) {
                        item.addContent(new Element("endHours").setText(
                                record.getEndHours().toString()));
                    } else {
                        item.addContent(new Element("endHours"));
                    }
                    if (isSet(record.getEndMinutes())) {
                        item.addContent(new Element("endMinutes").setText(
                                record.getEndMinutesDisplay()));
                    } else {
                        item.addContent(new Element("endMinutes"));
                    }
                }
                items.addContent(item);
            }
        }
        pt.addContent(items);
        root.addContent(pt);
        
        Element bc = xmlService.createBusinessCase("businessCase", businessCase);
        root.addContent(bc);
        if (systemConfigs.isSystemPropertyEnabled("documentOutputDebugXML")) { 
            if (log.isDebugEnabled()) {
                log.debug("createXml() done:\n" + JDOMUtil.getPrettyString(doc));
            } else if (log.isInfoEnabled()) {
                log.info("createXml() done:\n" + JDOMUtil.getPrettyString(doc));
            }
        }
        return doc;
    }
}
