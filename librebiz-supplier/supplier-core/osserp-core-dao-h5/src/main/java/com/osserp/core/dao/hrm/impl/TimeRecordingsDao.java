/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 12:38:13 PM 
 * 
 */
package com.osserp.core.dao.hrm.impl;

import java.sql.Types;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.PermissionException;
import com.osserp.common.PublicHoliday;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;

import com.osserp.core.dao.PublicHolidays;
import com.osserp.core.dao.hrm.TimeRecordings;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordStatus;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.model.hrm.TimeRecordCorrectionImpl;
import com.osserp.core.model.hrm.TimeRecordStatusImpl;
import com.osserp.core.model.hrm.TimeRecordingImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingsDao extends AbstractTimeRecordings implements TimeRecordings {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingsDao.class.getName());

    private PublicHolidays publicHolidays = null;

    protected TimeRecordingsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache options,
            String timeRecordingTableKey,
            String timeRecordingPeriodTableKey,
            String timeRecordTableKey,
            String timeRecordCorrectionsTableKey,
            String timeRecordingApprovalsTableKey,
            PublicHolidays publicHolidays) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                options,
                timeRecordingTableKey,
                timeRecordingPeriodTableKey,
                timeRecordTableKey,
                timeRecordCorrectionsTableKey,
                timeRecordingApprovalsTableKey);
        this.publicHolidays = publicHolidays;
    }

    public TimeRecording create(Employee user, Employee timeRecording) {
        TimeRecordingImpl obj = new TimeRecordingImpl(user, timeRecording);
        save(obj);
        TimeRecordingConfig defaultConfig = getDefaultConfig();
        obj.addPeriod(user, defaultConfig, DateUtil.getCurrentDate());
        save(obj);
        return sychronizePublicHoliday(obj);
    }

    public void createPeriod(Employee user, TimeRecording timeRecording) {
        TimeRecordingConfig defaultConfig = getDefaultConfig();
        timeRecording.addPeriod(user, defaultConfig, DateUtil.getCurrentDate());
        save(timeRecording);
    }

    public TimeRecording sychronizePublicHoliday(TimeRecording recording) {
        recording.synchronizePublicHolidays(getPublicHolidays(recording));
        save(recording);
        return load(recording.getId());
    }

    public boolean exists(Employee timeRecording) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT count(*) FROM ")
                .append(getRecordingTableName())
                .append(" WHERE reference_id = ?");
        Object[] params = { timeRecording.getId() };
        int[] types = { Types.BIGINT };
        return (jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0);
    }

    public boolean exists(Long terminalChipnumber) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT count(*) FROM ")
                .append(getRecordingTableName())
                .append(" WHERE terminal_chip_number = ?");
        Object[] params = { terminalChipnumber };
        int[] types = { Types.BIGINT };
        return (jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0);
    }

    public TimeRecording find(Employee timeRecording) {
        long start = System.currentTimeMillis();
        List<TimeRecording> list = getCurrentSession().createQuery(
                "from " + TimeRecordingImpl.class.getName() + " where reference = :referenceId")
                .setParameter("referenceId", timeRecording.getId()).list();
        TimeRecording tr = (list.isEmpty() ? null : list.get(0));
        if (log.isDebugEnabled()) {
            log.debug("find() done [employee=" + timeRecording.getId()
                    + ", id=" + (tr == null ? "null" : tr.getId())
                    + ", duration=" + (System.currentTimeMillis() - start)
                    + "]");
        }
        return tr;
    }

    public void addRecord(Employee user, TimeRecording recording, TimeRecordType type, Date date, String note) throws ClientException {
        if (user == null || recording == null || type == null) {
            log.warn("addRecord() invoked with invalid params ["
                    + "user=" + (user == null ? "null" : user.getId())
                    + ", recording=" + (recording == null ? "null" : recording.getId())
                    + ", type=" + (type == null ? "null" : type.getId())
                    + ", date=" + date
                    + ", note=" + note
                    + "]");
            throw new BackendException("user, recording or type was null");
        }

        if (log.isDebugEnabled()) {
            log.debug("addRecord() invoked with user [user=" + user.getId()
                    + ", recording=" + recording.getId()
                    + ", type=" + type.getId()
                    + ", date=" + date
                    + ", note=" + note
                    + "]");
        }
        if (type.isInterval()) {
            throw new ClientException(ErrorCode.TYPE_INVALID);
        }

        Boolean sysTime = false;
        Date value = DateUtil.getCurrentDate();
        if (date == null) {
            if (type.isManualInputRequired()) {
                throw new ClientException(ErrorCode.DATE_MISSING);
            }
            sysTime = true;
        } else {
            if (DateUtil.isLaterDay(date, value) && !type.isBookingFuture()) {
                throw new ClientException(ErrorCode.DATE_FUTURE);
            }
            if (DateUtil.isLaterDay(value, date) && !type.isBookingPast()) {
                throw new ClientException(ErrorCode.DATE_PAST);
            }
            value = date;
        }

        if (recording.getSelectedPeriod() == null
                || !DateUtil.isDayInInterval(value, recording.getSelectedPeriod().getValidFrom(), recording.getSelectedPeriod().getValidTil())) {
            throw new ClientException(ErrorCode.BOOKING_NOT_IN_PERIOD);
        }

        recording.checkRecordDate(value, type.isStarting());
        recording.addRecord(user, type, getDefaultStatus(), value, note, sysTime);
        save(recording);

        recording.refreshCurrentSelection();

        log.debug("addRecord() done [recording=" + recording.getId()
                + ", date=" + value
                + ", type=" + type.getId()
                + ", systemTime=" + sysTime
                + "]");
    }

    public void addInterval(Employee user, TimeRecording recording, TimeRecordType type, Date startValue, Date stopValue, String note) throws ClientException {
        Date current = DateUtil.getCurrentDate();
        if (!type.isInterval()) {
            throw new ClientException(ErrorCode.TYPE_INVALID);
        }
        if (startValue == null) {
            throw new ClientException(ErrorCode.DATE_FROM_MISSING);
        }
        if (stopValue == null) {
            throw new ClientException(ErrorCode.DATE_UNTIL_MISSING);
        }
        if (stopValue.before(startValue) || startValue.equals(stopValue)) {
            throw new ClientException(ErrorCode.DATE_INVALID);
        }
        if ((DateUtil.isLaterDay(startValue, current) || DateUtil.isLaterDay(stopValue, current)) && !type.isBookingFuture()) {
            throw new ClientException(ErrorCode.DATE_FUTURE);
        }
        if ((DateUtil.isLaterDay(current, startValue) || DateUtil.isLaterDay(current, stopValue)) && !type.isBookingPast()) {
            throw new ClientException(ErrorCode.DATE_PAST);
        }

        if (!DateUtil.isDayInInterval(startValue, recording.getSelectedPeriod().getValidFrom(), recording.getSelectedPeriod().getValidTil())
                || !DateUtil.isDayInInterval(stopValue, recording.getSelectedPeriod().getValidFrom(), recording.getSelectedPeriod().getValidTil())) {
            throw new ClientException(ErrorCode.BOOKING_NOT_IN_PERIOD);
        }

        if (DateUtil.isSameDay(startValue, stopValue)) {
            if (log.isDebugEnabled()) {
                log.debug("addInterval() booking single day while same day");
            }
            recording.checkRecordDate(startValue, stopValue);
            addIntervalDay(user, recording, type, startValue, stopValue, note);
        } else {
            if (!type.isIntervalPeriod()) {
                throw new ClientException(ErrorCode.INTERVAL_PERIOD);
            }
            if (log.isDebugEnabled()) {
                log.debug("addInterval() booking interval days");
            }
            Integer[] startTime = DateUtil.getTime(startValue);
            Integer[] stopTime = DateUtil.getTime(stopValue);
            Date next = DateUtil.resetTime(startValue);
            while (!DateUtil.isLaterDay(next, stopValue)) {
                Date start = DateUtil.createDate(next, startTime[3], startTime[4]);
                Date stop = DateUtil.createDate(next, stopTime[3], stopTime[4]);
                recording.checkRecordDate(start, stop);
                next = DateUtil.addDays(next, 1);
            }
            next = DateUtil.resetTime(startValue);
            List<PublicHoliday> publicHolidayList = getPublicHolidays(recording);
            while (!DateUtil.isLaterDay(next, stopValue)) {
                Date start = DateUtil.createDate(next, startTime[3], startTime[4]);
                Date stop = DateUtil.createDate(next, stopTime[3], stopTime[4]);
                if (!DateUtil.isPublicHoliday(publicHolidayList, start) && !DateUtil.isWeekend(start)) {
                    addIntervalDay(user, recording, type, start, stop, note);
                }
                next = DateUtil.addDays(next, 1);
            }
        }
    }

    private List<PublicHoliday> getPublicHolidays(TimeRecording recording) {
        TimeRecordingPeriod p = recording.getCurrentPeriod();
        if (p == null) {
            return publicHolidays.findAll();
        }
        return publicHolidays.find(p.getCountry(), p.getState());
    }

    private void addIntervalDay(
            Employee user,
            TimeRecording recording,
            TimeRecordType type,
            Date startDate,
            Date stopDate,
            String note)
            throws ClientException {

        TimeRecordStatus status = getDefaultStatus();
        TimeRecordType stopType = getStopType();
        if (type.isNeedApproval()) {
            status = (TimeRecordStatus) getCurrentSession().load(
                            TimeRecordStatusImpl.class.getName(),
                            TimeRecordStatus.WAITFORAPPROVAL);
        }

        if (log.isDebugEnabled()) {
            log.debug("addIntervalDay() add start booking [date=" + DateFormatter.getDateAndTime(startDate)
                    + ", type=" + type.getId() + "]");
        }
        recording.addRecord(user, type, status, startDate, note, false);
        getCurrentSession().saveOrUpdate(TimeRecordingImpl.class.getName(), recording);

        if (log.isDebugEnabled()) {
            log.debug("addIntervalDay() add stop booking [date=" + DateFormatter.getDateAndTime(stopDate)
                    + ", type=" + stopType.getId() + "]");
        }
        recording.addRecord(user, stopType, status, stopDate, null, false);
        save(recording);
        recording.refreshCurrentSelection();

        if (log.isDebugEnabled()) {
            log.debug("addIntervalDay() done");
        }
    }

    public void addMarker(
            Employee user,
            TimeRecording recording,
            TimeRecordMarkerType type,
            Date date,
            Double value,
            String note)
            throws ClientException {

        if (!DateUtil.isDayInInterval(date, recording.getSelectedPeriod().getValidFrom(), recording.getSelectedPeriod().getValidTil())) {
            throw new ClientException(ErrorCode.MARKER_NOT_IN_PERIOD);
        }

        recording.addMarker(user, type, date, value, note);
        save(recording);
        if (log.isDebugEnabled()) {
            log.debug("addMarker() added marker [date="
                    + DateFormatter.getDate(date)
                    + ", type=" + type.getId()
                    + ", value=" + value + "]");
        }
        recording.refreshCurrentSelection();
        if (log.isDebugEnabled()) {
            log.debug("addMarker() done");
        }
    }

    public void removeMarker(Employee user, TimeRecording recording, Long id) throws PermissionException {
        TimeRecording obj = load(recording.getId());
        obj.removeMarker(id);
        getCurrentSession().merge(TimeRecordingImpl.class.getName(), obj);
    }

    public void removeRecord(Employee user, TimeRecording recording, Long id) throws PermissionException {
        StringBuilder update = new StringBuilder(64);
        update
                .append("DELETE FROM ")
                .append(getRecordingApprovalsTableName())
                .append(" WHERE record_id = ")
                .append(id);
        jdbcTemplate.update(update.toString());
        update = new StringBuilder(64);
        update
                .append("DELETE FROM ")
                .append(getRecordCorrectionsTableName())
                .append(" WHERE reference_id = ")
                .append(id);
        jdbcTemplate.update(update.toString());
        update = new StringBuilder(64);
        update
                .append("DELETE FROM ")
                .append(getRecordTableName())
                .append(" WHERE closing_id = ")
                .append(id);
        jdbcTemplate.update(update.toString());
        update = new StringBuilder(64);
        update
                .append("DELETE FROM ")
                .append(getRecordTableName())
                .append(" WHERE id = ")
                .append(id);
        jdbcTemplate.update(update.toString());
    }

    public void updateRecord(Employee user, TimeRecording recording, TimeRecord record, Date startDate, Date stopDate, String note)
            throws ClientException {
        TimeRecordStatus status = (TimeRecordStatus) getCurrentSession().load(
                TimeRecordStatusImpl.class.getName(),
                TimeRecordStatus.CORRECTIONWAITFORAPPROVAL);
        //replace this.defaultStatus with status when finished approvals
        TimeRecord correctionObj = recording.createCorrection(user, record, startDate, stopDate, note, status);
        getCurrentSession().saveOrUpdate(TimeRecordCorrectionImpl.class.getName(), correctionObj);
        save(user, recording);
    }

    public void save(Employee user, TimeRecording recording) {
        recording.setChanged(new Date(System.currentTimeMillis()));
        recording.setChangedBy((user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
        save(recording);
    }

    @Override
    public List<TimeRecordCorrection> fetchCorrections(TimeRecord record) {
        return super.fetchCorrections(record);
    }

}
