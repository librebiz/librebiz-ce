/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.crm

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.StringUtil
import com.osserp.core.crm.CampaignUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class CampaignSelectionView extends AbstractCampaignView {
    private static Logger logger = LoggerFactory.getLogger(CampaignSelectionView.class.getName())

    private String targetViewName
    String searchPattern

    CampaignSelectionView() {
        super()
        enablePopupView()
        headerName = 'campaignSelection'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            targetViewName = form.getString('view')
            Long company = form.getLong('company')
            if (company) {
                selectedCompany = systemConfigManager.getCompany(company)
            }
            list = CampaignUtil.sortByCampaignStart(campaignManager.findSelectable())
            env.includeEol = false
        }
    }

    void search() {
        def pattern = form.getString('value')
        if (pattern) {
            searchPattern = pattern
        } else {
            searchPattern = null
        }
    }

    @Override
    void select() {
        def campaignAware = env[targetViewName]
        Long id = form.getLong('id')
        logger.debug("select: invoked [id=${id}, campaignAware=${targetViewName}, found=${campaignAware?.name}]")
        if (campaignAware && id) {
            def campaign = list.find { it.id == id }
            campaignAware.setCampaign(campaign)
            logger.debug("select: campaign assigned [id=${id}]")
        }
    }

    List getCampaigns() {
        if (!selectedType && !selectedGroup && (selectedBranch == null)) {
            return filterByCompany(list)
        }
        List result = []
        List filtered = []
        if (selectedType) {
            list.each {
                if (it.type?.id == selectedType.id) {
                    if (selectedGroup) {
                        if (it.group?.id == selectedGroup.id) {
                            filtered << it
                        }
                    } else {
                        filtered << it
                    }
                }
            }
        } else if (selectedGroup) {
            list.each {
                if (it.group?.id == selectedGroup.id) {
                    filtered << it
                }
            }
        } else {
            list.each { filtered << it }
        }
        filtered.each {
            if ((selectedBranch == null) || selectedBranch.id == it.branch || it.ignoreCompany) {
                result << it
            }
        }
        filterByCompany(result)
    }

    private List filterByCompany(List campaigns) {
        List filtered = []
        campaigns.each {
            if (!selectedCompany || selectedCompany.id == it.company || it.ignoreCompany) {
                if (!searchPattern || StringUtil.contains(it.name, searchPattern)) {
                    filtered << it
                }
            }
        }
        return filtered
    }
}

