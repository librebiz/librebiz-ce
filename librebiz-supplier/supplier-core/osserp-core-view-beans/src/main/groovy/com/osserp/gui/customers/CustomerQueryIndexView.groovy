/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 22, 2017 
 * 
 */
package com.osserp.gui.customers

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.DateUtil
import com.osserp.common.util.NumberUtil

import com.osserp.core.customers.CustomerSearch
import com.osserp.core.customers.CustomerSearchResult

import com.osserp.gui.QueryIndexView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class CustomerQueryIndexView extends QueryIndexView {
    private static Logger logger = LoggerFactory.getLogger(CustomerQueryIndexView.class.getName())
    
    List<CustomerSearchResult> customers = []
    List<CustomerSearchResult> latestCustomers = []
    String selectedContext
    Date dateBefore
    boolean preselection
    boolean exportAvailable
    
    CustomerQueryIndexView() {
        super()
        headerName = 'customerQueriesHeader'
        enableAutoreload()
        providesCustomNavigation()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            reload()
            if (selectedQueryType) {
                preselection = true
            }
            exportAvailable = isSystemPropertyEnabled('customerExportAvailable')
            if (exportAvailable) {
                exportAvailable = isPermissionGrantByConfig('customerExport')
            }
        }
    }
    
    List<String> getAvailableQueries() {
        [ 'listWithoutAction', 'listWithoutOrder', 'listWithoutSales', 'listLastSalesBefore' ]
    }
        
    @Override
    void reload() {
        customers = customerSearch.customerStats
        String type = form.getString('type')
        if (type || selectedQueryType) {
            executeQuery(type ?: selectedQueryType)
        }
        if (!dateBefore) {
            initDateBefore()
        }
    }
    
    String getSpreadsheet() {
        customerSearch.getCustomerStatsSheet(list)
    }

    void selectQuery() {
        executeQuery(form.getString('type'))
    }
    
    void executeQuery(String typeValue) {
        selectedQueryType = typeValue
        if (selectedQueryType) {
            switch ( selectedQueryType ) {
                
                case 'listWithoutAction':
                    listWithoutAction()
                    break
                    
                case 'listWithoutOrder':
                    listWithoutOrder()
                    break
                        
                case 'listWithoutSales':
                    listWithoutSales()
                    break

                case 'listLastSalesBefore':
                    listLastSalesBefore()
                    break
                    
                default:
                    logger.warn("selectQuery: invalid query type [name=${selectedQueryType}]")
            }
        } else {
            defaultListMode = false
            headerName = 'customerQueriesHeader'
            list = []
        }
    }
    
    private void listWithoutAction() {
        list = []
        customers.each { CustomerSearchResult customer ->
            if (isWithoutAction(customer)) {
                list << customer
            }
        }
        headerName = 'customerlistWithoutAction'
        logger.debug("listWithoutAction: done [count=${list.size()}]")
    }
    
    private void listWithoutOrder() {
        list = []
        customers.each { CustomerSearchResult customer ->
            if (!isWithoutAction(customer) && isRequestsOnly(customer)) {
                list << customer
            }
        }
        headerName = 'customerlistWithoutOrder'
        logger.debug("listWithoutOrder: done [count=${list.size()}]")
    }

    private void listWithoutSales() {
        list = []
        customers.each { CustomerSearchResult customer ->
            if (!isWithoutAction(customer) && isOrdersOnly(customer)) {
                list << customer
            }
        }
        headerName = 'customerlistWithoutSales'
        logger.debug("listWithoutSales: done [count=${list.size()}]")
    }
    
    private void listLastSalesBefore() {
        Date tempDate = form.getDate('date')
        if (tempDate) {
            dateBefore = tempDate
        } else {
            Integer monthsBack = form.getInteger('months')
            if (monthsBack) {
                dateBefore = DateUtil.subtractMonths(new Date(), monthsBack)
            }
        }
        if (!dateBefore) {
            initDateBefore()
        }
        list = []
        customers.each { CustomerSearchResult customer ->
            if (!isWithoutAction(customer) && !isRequestsOnly(customer)
                    && !isOrdersOnly(customer) 
                    && customer.lastSalesDate?.before(dateBefore)) {
                list << customer
            }
        }
        headerName = 'customerlistLastSalesBefore'
        logger.debug("listLastSalesBefore: done [count=${list.size()}]")
    }
    
    private void initDateBefore() {
        String[] cfg = DateUtil.getTimeIntervalConfig(getSystemProperty('customerlistLastSalesBefore'))
        if (cfg[0] == 'Y') {
            dateBefore = DateUtil.subtractYears(new Date(), NumberUtil.createInteger(cfg[1]))
        } else if (cfg[0] == 'M') {
            dateBefore = DateUtil.subtractMonths(new Date(), NumberUtil.createInteger(cfg[1]))
        } else if (cfg[0] == 'D') {
            dateBefore = DateUtil.subtractDays(new Date(), NumberUtil.createInteger(cfg[1]))
        }
    }
    
    /**
     * Customer has requests and/or orders but no invoices
     */
    protected boolean isOrdersOnly(CustomerSearchResult customer) {
        return customer.orderCount > 0 && !customer.lastSalesDate
    }

    /**
     * Customer has requests but no orders and no invoices
     */
    protected boolean isRequestsOnly(CustomerSearchResult customer) {
        return customer.requestCount > 0 && customer.orderCount == 0 && !customer.lastSales
    }
    
    /**
     * Customer has no request, no orders and no invoice
     */
    protected boolean isWithoutAction(CustomerSearchResult customer) {
        return customer.requestCount == 0 && customer.orderCount == 0 && !customer.lastSales
    }
    
    @Override
    void createCustomNavigation() {
        if (selectedQueryType && !preselection) {
            nav.defaultNavigation = [ querySelectExitLink, homeLink ]
            if (exportAvailable) {
                nav.listNavigation = [ querySelectExitLink, xlsLink, homeLink ]
            } else {
                nav.listNavigation = [ querySelectExitLink, homeLink ]
            }
        } else {
            nav.defaultNavigation = [ exitLink, homeLink ]
            if (exportAvailable) {
                nav.listNavigation = [ exitLink, xlsLink, homeLink ]
            } else {
                nav.listNavigation = [ exitLink, homeLink ]
            }
        }
    }

    protected CustomerSearch getCustomerSearch() {
        getService(CustomerSearch.class.getName())
    }
}
