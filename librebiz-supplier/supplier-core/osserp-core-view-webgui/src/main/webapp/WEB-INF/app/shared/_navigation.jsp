<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="navigation" scope="page" value="${requestScope.navigation}"/>
<c:if test="${!empty navigation}">
	<ul>
		<c:forEach var="nav" items="${navigation}">
			<c:if test="${nav != null}">
				<c:choose>
					<c:when test="${!empty nav.permissions}">
						<o:permission role="${nav.permissions}" info="${nav.permissionInfo}">
							<li>
								<c:choose>
									<c:when test="${nav.ajaxPopup}">
										<v:ajaxLink url="${nav.link}" linkId="${nav.ajaxPopupName}" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxLink>
									</c:when>
									<c:when test="${nav.newWindow}">
										<a href="javascript:;" onclick="${nav.link}" title="<fmt:message key="${nav.title}"/>"><o:img name="${nav.icon}"/></a>
									</c:when>
									<c:when test="${nav.execAction}">
										<v:ajaxExecute url="${nav.link}" linkId="${nav.ajaxPopupName}" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxExecute>
									</c:when>
									<c:when test="${nav.confirmLink}">
										<a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="${nav.title}"/>','${nav.link}');" title="<fmt:message key="${nav.title}"/>"><o:img name="${nav.icon}"/></a>
									</c:when>
									<c:otherwise>
										<a href="${nav.link}" title="<fmt:message key="${nav.title}"/>"><o:img name="${nav.icon}"/></a>
									</c:otherwise>
								</c:choose>
							</li>
						</o:permission>
					</c:when>
					<c:otherwise>
						<li>
							<c:choose>
								<c:when test="${nav.ajaxPopup}">
									<v:ajaxLink url="${nav.link}" linkId="${nav.ajaxPopupName}" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxLink>
								</c:when>
								<c:when test="${nav.newWindow}">
									<a href="javascript:;" onclick="${nav.link}" title="<fmt:message key="${nav.title}"/>"><o:img name="${nav.icon}"/></a>
								</c:when>
								<c:when test="${nav.execAction}">
									<v:ajaxExecute url="${nav.link}" linkId="${nav.ajaxPopupName}" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxExecute>
								</c:when>
								<c:when test="${nav.confirmLink}">
									<a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="${nav.title}"/>','${nav.link}');" title="<fmt:message key="${nav.title}"/>"><o:img name="${nav.icon}"/></a>
								</c:when>
								<c:otherwise>
									<a href="${nav.link}" title="<fmt:message key="${nav.title}"/>"><o:img name="${nav.icon}"/></a>
								</c:otherwise>
							</c:choose>
						</li>
					</c:otherwise>
				</c:choose>
			</c:if>
		</c:forEach>
	</ul>
</c:if>
