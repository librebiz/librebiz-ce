<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<oc:systemPropertyEnabled name="businessCaseAccountingRefSupport">
<c:set var="view" value="${sessionScope.businessCaseView}"/>
<c:if test="${view.salesContext}">
<c:set var="project" value="${sessionScope.businessCaseView.sales}"/>
<c:set var="accountingReference" value="${project.accountingReference}"/>
</c:if>
<c:set var="plan" value="${sessionScope.businessCaseView.request}"/>
<c:if test="${!view.salesContext}">
<c:set var="accountingReference" value="${plan.accountingReference}"/>
</c:if>
<tr>
    <td style="vertical-align: top;">
        <v:ajaxLink url="/requests/requestEditor/forward" title="change" linkId="requestEditor">
            <oc:systemPropertyOut name="businessCaseAccountingRefName"/>
        </v:ajaxLink>
    </td>
    <td>
        <c:choose>
            <c:when test="${empty accountingReference}">
                <fmt:message key="undefined"/>
            </c:when>
            <c:otherwise>
                <o:out value="${accountingReference}"/>
            </c:otherwise>
        </c:choose>
    </td>
</tr>
</oc:systemPropertyEnabled>