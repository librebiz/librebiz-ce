/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 26, 2016 
 * 
 */
package com.osserp.core.service.records;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DmsUtil;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.FileUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BankAccount;
import com.osserp.core.dms.CoreDocumentType;
import com.osserp.core.finance.AnnualReport;
import com.osserp.core.finance.CommonPaymentSummary;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.finance.RecordSummary;
import com.osserp.core.purchasing.PurchaseRecordSummary;
import com.osserp.core.service.impl.ExportArchiveService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AnnualReportArchiveImpl extends ExportArchiveService implements AnnualReportArchive {
    private static Logger log = LoggerFactory.getLogger(AnnualReportArchiveImpl.class.getName());

    private static final String BANKS_DIR = "bank";
    private static final String SALES_DIR = "debitors";
    private static final String SUPPLIERS_DIR = "creditors";

    private ResourceLocator resourceLocator;
    private RecordSearch recordSearch;
    private DmsManager dmsManager;

    public AnnualReportArchiveImpl(
            ResourceLocator resourceLocator,
            OptionsCache optionsCache,
            RecordSearch recordSearch,
            RecordPrintService recordPrintService,
            DmsManager dmsManager) {
        super(optionsCache, dmsManager, recordSearch, recordPrintService);
        this.resourceLocator = resourceLocator;
        this.recordSearch = recordSearch;
        this.dmsManager = dmsManager;
    }

    public List<DmsDocument> getBankAccountDocuments(Long bankAccountId, int year) {
        DmsReference dmsRef =  new DmsReference(CoreDocumentType.BANK_ACCOUNT_DOCUMENT, bankAccountId);
        List<DmsDocument> result = new ArrayList<>();
        List<DmsDocument> allDocs = dmsManager.findByReference(dmsRef);
        for (int i = 0, j = allDocs.size(); i < j; i++) {
            DmsDocument doc = allDocs.get(i);
            if (doc.getValidFrom() != null && DateUtil.getYear(doc.getValidFrom()) == year
                    || doc.getValidTil() != null && DateUtil.getYear(doc.getValidTil()) == year) {
                result.add(doc);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getBankAccountDocuments() done [id=" + bankAccountId 
                    + ", year=" + year + ", count=" + result.size()
                    + "]");
            
        }
        return CollectionUtil.sort(result, DmsUtil.comparatorByValidFrom(false));
    }

    public boolean archiveExists(AnnualReport report) {
        File file = new File(createArchiveName(report));
        return file.exists();
    }

    public String createArchive(AnnualReport report) throws ClientException {
        long start = System.currentTimeMillis();
        
        String reportRoot = createReportDirectory(report);
        exportSalesRecords(report, reportRoot + "/" + SALES_DIR);
        exportSupplierRecords(report, reportRoot + "/" + SUPPLIERS_DIR);
        exportBankAccountDocuments(report, reportRoot + "/" + BANKS_DIR);
        String result = createZip(report);
        if (log.isDebugEnabled()) {
            File zip = new File(result);
            log.debug("createArchive() done [zip=" + result + ", size=" + zip.length() 
                    + ", duration=" + (System.currentTimeMillis() - start) + "ms]");
        }
        return result;
    }

    public void deleteArchive(AnnualReport report) throws ClientException {
        String reportsDir = createReportDirectoryName(report);
        File dir = new File(reportsDir);
        FileUtil.delete(dir);
        String archiveName = createArchiveName(report);
        FileUtil.delete(archiveName);
    }

    public final DocumentData getArchive(AnnualReport report) throws ClientException {
        File file = new File(createArchiveName(report));
        if (!file.exists()) {
            createArchive(report);
        }
        if (!file.exists()) {
            log.warn("getArchive() zip does not exist post invocation of createArchive [name="
                    + file.getAbsolutePath() + "]");
            throw new BackendException();
        }
        byte[] zip = FileUtil.getBytes(file);
        DocumentData result = new DocumentData(zip, createBareArchiveName(report));
        if (log.isDebugEnabled()) {
            log.debug("getArchive() done [name=" + result.getRealFilename() 
                    + ", size=" + result.getContentLength() + "]");
        }
        try {
            deleteArchive(report);
        } catch (Exception e) {
            log.warn("getArchive() failed to cleanup archive [message=" + e.getMessage() + "]", e);
        }
        return result;
    }

    private String createArchiveName(AnnualReport report) {
        StringBuilder buffer = new StringBuilder(getDmsExportRoot());
        buffer.append(createBareArchiveName(report));
        return buffer.toString();
    }

    private String createBareArchiveName(AnnualReport report) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("/annual-report-").append(report.getYear()).append(".zip");
        return buffer.toString();
    }

    private String createZip(AnnualReport report) throws ClientException {
        String reportsDir = createReportDirectoryName(report);
        String archiveName = createArchiveName(report);
        return FileUtil.zip(reportsDir, archiveName);
    }

    /**
     * Creates the archive directory and subdirectories. 
     * @return path to the new created archive
     */
    private String createReportDirectory(AnnualReport report) throws ClientException {
        String directory = createReportDirectoryName(report);
        createDirectory(directory);
        File subdir = new File(directory + "/" + SALES_DIR);
        subdir.mkdir();
        subdir = new File(directory + "/" + SUPPLIERS_DIR);
        subdir.mkdir();
        String banksDir = directory + "/" + BANKS_DIR;
        subdir = new File(banksDir);
        subdir.mkdir();
        if (report.getCompany() != null && !report.getCompany().getBankAccounts().isEmpty()) {
            for (int i = 0, j = report.getCompany().getBankAccounts().size(); i < j; i++) {
                BankAccount bankAccount = report.getCompany().getBankAccounts().get(i);
                String name = createBankAccountDirectoryName(banksDir, bankAccount);
                String nextBankArchive = banksDir + "/" + name; 
                subdir = new File(nextBankArchive);
                if (!subdir.mkdirs()) {
                    log.debug("createReportDirectory() mkdirs returns false [name=" 
                            + nextBankArchive + "]");
                }
                if (subdir.exists()) {
                    log.debug("createReportDirectory() bank directory created [name=" 
                            + nextBankArchive + "]");
                } else {
                    log.warn("createReportDirectory() bank directory not found [name=" 
                            + nextBankArchive + "]");
                }
            }
        } else if (log.isDebugEnabled()) {
            log.debug("createReportDirectory() report does not provide company or bank accounts [company=" 
                    + (report.getCompany() != null ? report.getCompany().getId() : "null") + "]");
        }
        return directory;
    }

    private String createBankAccountDirectoryName(String banksDir, BankAccount bankAccount) throws ClientException {
        String name = isSet(bankAccount.getShortkey()) ? bankAccount.getShortkey() : null;
        if (isNotSet(name) && isSet(bankAccount.getName())) {
            name = StringUtil.replaceUmlauts(StringUtil.deleteBlanks(bankAccount.getName().toLowerCase()));
        }
        if (isSet(name)) {
            return name;
        }
        log.warn("createExportDirectory() bank does not provide shortKey nor name [id=" 
                + bankAccount.getId() + "]");
        throw new ClientException(ErrorCode.BANKACCOUNT_NAMES_MISSING);
    }

    private String createReportDirectoryName(AnnualReport report) {
        return getDmsExportRoot() + "/reports/annual-report-" + report.getYear();
    }

    private void exportSalesRecords(AnnualReport report, String directory) {
        if (!report.getSalesRecords().isEmpty()) {
            for (int i = 0, j = report.getSalesRecords().size(); i < j; i++) {
                RecordSummary summary = report.getSalesRecords().get(i);
                if (!summary.isPayment()) {
                    exportVoucher(directory, summary.getId(), summary.getType().getId());
                }
            }
        } else {
            log.debug("exportSalesRecords() report does not provide sales records");
        }
    }

    protected void exportSupplierRecords(AnnualReport report, String directory) {
        int[] result = new int[] { 0, 0 };
        if (!report.getSupplierRecords().isEmpty()) {
            for (int i = 0, j = report.getSupplierRecords().size(); i < j; i++) {
                RecordSummary nextSummary = report.getSupplierRecords().get(i);
                if (nextSummary instanceof CommonPaymentSummary) {
                    CommonPaymentSummary summary = (CommonPaymentSummary) nextSummary;
                    if (isSet(summary.getDocumentId())) {
                        DmsDocument document = dmsManager.findDocument(summary.getDocumentId());
                        if (document != null && write(dmsManager.getDocumentData(document), directory)) {
                            result[0]++;
                        } else {
                            result[1]++;
                            if (log.isDebugEnabled()) {
                                log.debug("exportSupplierRecords() did not find corresponding document [record="
                                        + summary.getId() + ", type=" + summary.getType().getId() + "]");
                            }
                        }
                    }
                    
                } else if (nextSummary instanceof PurchaseRecordSummary) {
                    PurchaseRecordSummary summary = (PurchaseRecordSummary) nextSummary;
                    if (!summary.isPayment()) {
                        Record record = recordSearch.findRecord(summary.getId(), summary.getType().getId());
                        if (record != null) {
                            DmsDocument document = dmsManager.findReferenced(
                                    new DmsReference(record.getType().getDocumentTypeId(), record.getId()));
                            if (document != null && write(dmsManager.getDocumentData(document), directory)) {
                                result[0]++;
                            } else {
                                result[1]++;
                                if (log.isDebugEnabled()) {
                                    log.debug("exportSupplierRecords() did not find corresponding document [record="
                                            + record.getId() + ", type=" + record.getType().getId() + "]");
                                }
                            }
                        } else {
                            log.warn("exportSupplierRecords() did not find corresponding record [summary="
                                    + summary.getId() + ", type=" + summary.getType().getId() + "]");
                        }
                    }
                }
            }
        } else {
            log.debug("exportSupplierRecords() report does not provide supplier records");
        }
    }

    protected void exportBankAccountDocuments(AnnualReport report, String directory) throws ClientException {
        int[] result = new int[] { 0, 0 };
        for (int i = 0, j = report.getCompany().getBankAccounts().size(); i < j; i++) {
            BankAccount bankAccount = report.getCompany().getBankAccounts().get(i);
            List<DmsDocument> documents = getBankAccountDocuments(bankAccount.getId(), report.getYear());
            if (!documents.isEmpty()) {
                String bankDir = directory + "/" + createBankAccountDirectoryName(directory, bankAccount);
                if (log.isDebugEnabled()) {
                    log.debug("exportBankAccountDocuments() fetched account directory [name=" + bankDir + "]");
                }
                for (int k = 0, l = documents.size(); k < l; k++) {
                    DmsDocument document = documents.get(k);
                    if (document != null && write(dmsManager.getDocumentData(document), bankDir)) {
                        result[0]++;
                    } else {
                        result[1]++;
                    }
                }
            }
        }
    }
}
