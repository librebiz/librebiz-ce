/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 13, 2006 4:15:56 PM 
 * 
 */
package com.osserp.common.dms.dao;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ReferencedDocuments extends Documents {

    /**
     * Provides the document type if implementing dao supports a dedicated type only
     * @return documentType
     */
    DocumentType getDocumentType();

    /**
     * Provides all document related to given reference
     * @param reference
     * @return documents
     */
    List<DmsDocument> findByReference(DmsReference reference);

    /**
     * Provides count by reference
     * @param reference
     * @return count
     */
    int countByReference(DmsReference reference);

    /**
     * Creates a document for the specified reference
     * @param user creating the document
     * @param reference
     * @param file
     * @param note to the document
     * @param sourceDate the optional source file created date
     * @param categoryId an optional category reference
     * @param validFrom
     * @param validTil
     * @param messageId
     * @throws ClientException if validation failed
     */
    DmsDocument create(
            User user,
            DmsReference reference,
            FileObject file,
            String note,
            Date sourceDate,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId) throws ClientException;

    /**
     * Creates a document for the specified reference from existing file
     * @param user creating the document
     * @param reference
     * @param fileName
     * @param absoluteFile
     * @param note to the document
     * @param sourceDate the optional source file created date
     * @param categoryId an optional category reference
     * @param validFrom
     * @param validTil
     * @param messageId
     * @throws ClientException if validation failed
     */
    DmsDocument create(
            User user,
            DmsReference reference,
            String fileName,
            String absoluteFile,
            String note,
            Date sourceDate,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId) throws ClientException;

    /**
     * Creates a new document by existing provided by previous import
     * @param user
     * @param reference
     * @param imported
     * @return new created document
     * @throws ClientException if validation failed
     */
    DmsDocument create(User user, DmsReference reference, DmsDocument imported)
            throws ClientException;

    /**
     * Deletes the given document from the backend
     * @param doc
     */
    void deleteDocument(DmsDocument doc);

    /**
     * Gets the reference picture of the given reference
     * @param reference
     * @throws ClientException if no reference picture exists
     */
    DmsDocument getReferenceDocument(DmsReference reference) throws ClientException;

    /**
     * Creates thumbnails for all images with provided document type
     * @param documentType
     * @param forceOverride override existing thumbnails
     */
    void createThumbnails(DocumentType documentType, boolean forceOverride);

}
