/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 6, 2005 
 * 
 */
package com.osserp.core.contacts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.AbstractValidator;
import com.osserp.common.util.AddressValidator;
import com.osserp.core.Address;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactValidator extends AbstractValidator {
    private static Logger log = LoggerFactory.getLogger(ContactValidator.class.getName());

    /**
     * Validates a contact address
     * @param contactType
     * @param addressType
     * @param street
     * @param zipcode
     * @param city
     * @param required if a complete address is required
     * @throws ClientException if validation failed
     */
    public static void validateAddress(
            Long contactType,
            Long addressType,
            String street,
            String zipcode,
            String city,
            Long country,
            boolean required)
            throws ClientException {

        AddressValidator.validateAddress(
                street, zipcode, city, country, required);
        if (addressType == null || addressType.equals(Address.UNDEFINED)) {
            throw new ClientException(ErrorCode.ADDRESS_TYPE_MISSING);
        }
        if (contactType != null) {
            if ((contactType.equals(Contact.TYPE_BUSINESS)
                    && addressType.equals(Address.HOME))
                    || ((contactType.equals(Contact.TYPE_PRIVATE)
                    && addressType.equals(Address.BUSINESS)))) {
                throw new ClientException(ErrorCode.ADDRESS_TYPE_INVALID);
            }
        }
    }

    /**
     * Validates the given given contact values. Validation fails if <br>
     * the type is null <br>
     * some other criterias
     * @param type
     * @param salutation
     * @param firstName
     * @param lastName
     * @throws ClientException if validation failed
     */
    public static void validateContact(
            Long type,
            Long salutation,
            String firstName,
            String lastName)
            throws ClientException {

        if (log.isDebugEnabled()) {
            log.debug("validateContact() invoked [type=" + type
                    + ", salutation=" + salutation
                    + ", firstName=" + firstName
                    + ", lastName=" + lastName
                    + "]");
        }

        // contact type spec required
        if (isNotSet(type)) {
            if (log.isDebugEnabled()) {
                log.debug("validateContact() failed [message=" + ErrorCode.CONTACT_TYPE_MISSING + "]");
            }
            throw new ClientException(ErrorCode.CONTACT_TYPE_MISSING);
        }
        // check that no german salutation is set as first name
        validateFirstName(salutation, firstName);

        // company or lastname required
        if (type.equals(Contact.TYPE_BUSINESS) || type.equals(Contact.TYPE_PUBLIC)) {
            validateCompany(lastName);
        }
        if (type.equals(Contact.TYPE_PRIVATE) || type.equals(Contact.TYPE_PERSON)) {
            if (isNotSet(lastName)) {
                throw new ClientException(ErrorCode.LASTNAME_MISSING);
            }
            validateLastName(type, salutation, lastName);
        }
    }

    /**
     * Validates given values on a contact person
     * @param salutation
     * @param firstName
     * @param lastName
     * @throws ClientException if validation failed
     */
    public static void validatePerson(Long salutation, String firstName, String lastName)
            throws ClientException {
        if (isNotSet(lastName)) {
            throw new ClientException(ErrorCode.LASTNAME_MISSING);
        }
        if (isNotSet(salutation)) {
            throw new ClientException(ErrorCode.SALUTATION_MISSING);
        }
        validateFirstName(salutation, firstName);
        validateLastName(Contact.TYPE_PERSON, salutation, lastName);
    }

    /**
     * Validates the company name
     * @param company
     * @throws ClientException if company not set or starting with whitespace
     */
    public static void validateCompany(String company) throws ClientException {
        if (isNotSet(company)) {
            throw new ClientException(ErrorCode.COMPANY_MISSING);
        }
        if (Character.isWhitespace(company.charAt(0))) {
            throw new ClientException(ErrorCode.COMPANY_SPACE);
        }
    }

    private static void validateLastName(Long type, Long salutation, String lastName)
            throws ClientException {
        // salutation set but lastname missing
        if (isSet(salutation) && isNotSet(lastName)) {
            if (log.isDebugEnabled()) {
                log.debug("validateLastName() failed [salutation=" + salutation + ", lastname=notSet]");
            }
            throw new ClientException(ErrorCode.SALUTATION_INVALID);
        }
        // salutation required if lastname set, lastname does not start
        // with space or lowercase letter
        if (isSet(lastName)) {

            if (isNotSet(salutation)) {
                throw new ClientException(ErrorCode.SALUTATION_MISSING);
            } else if (Character.isWhitespace(lastName.charAt(0))) {
                throw new ClientException(ErrorCode.LASTNAME_SPACE);
            } else if (Character.isLowerCase(lastName.charAt(0))) {
                throw new ClientException(ErrorCode.LASTNAME_LOWER);
            }
        }
        if (Contact.TYPE_PERSON.equals(type) || Contact.TYPE_PRIVATE.equals(type)) {
            if (Salutation.COMPANY.equals(salutation)) {
                if (log.isDebugEnabled()) {
                    log.debug("validateLastName() failed [salutation=company, type="
                            + (Contact.TYPE_PERSON.equals(type) ? "person]" : "private]"));
                }
                throw new ClientException(ErrorCode.SALUTATION_INVALID);
            }
        }
    }

    private static void validateFirstName(Long salutation, String firstName)
            throws ClientException {
        // check that no german salutation is set as first name
        if ((isSet(firstName)) && (firstName.equalsIgnoreCase("Herr")
                || firstName.equalsIgnoreCase("Herrn")
                || firstName.equalsIgnoreCase("Hr.")
                || firstName.equalsIgnoreCase("Fr.")
                || firstName.equalsIgnoreCase("Frau")
                || firstName.equalsIgnoreCase("Familie")
                || firstName.equalsIgnoreCase("Fam.")
                || firstName.equalsIgnoreCase("Fa.")
                || firstName.equalsIgnoreCase("Firma"))) {
            throw new ClientException(ErrorCode.FIRSTNAME_INVALID);

        }
        // check that firstname begins with upper case letter and not with whitespace
        if (isSet(firstName)) {
            if (Character.isWhitespace(firstName.charAt(0))) {
                throw new ClientException(ErrorCode.FIRSTNAME_SPACE);
            } else if (Character.isLowerCase(firstName.charAt(0))) {
                throw new ClientException(ErrorCode.FIRSTNAME_LOWER);
            }
        }
        // only one firstname allowed if selected type not customizable
        if (!Salutation.isCustom(salutation)) {
            if (contains(firstName, ",")
                    || contains(firstName, "&")
                    || contains(firstName, " und ")
                    || contains(firstName, "+")) {
                throw new ClientException(ErrorCode.SALUTATION_FIRSTNAME);
            }
        }
    }

    private static boolean contains(String inspect, String searchFor) {
        return inspect == null ? false : (inspect.indexOf(searchFor) > 0);
    }

}
