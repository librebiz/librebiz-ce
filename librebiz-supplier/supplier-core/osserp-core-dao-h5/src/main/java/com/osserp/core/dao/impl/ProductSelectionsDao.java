/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 27, 2009 5:26:00 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateAndSpringEntityDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.ProductSelectionProfiles;
import com.osserp.core.dao.ProductSelections;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.products.ProductSelectionConfigItemImpl;
import com.osserp.core.model.products.ProductSelectionImpl;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductSelectionProfile;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSelectionsDao extends AbstractHibernateAndSpringEntityDao
        implements ProductSelections {

    private ProductSelectionProfiles productSelectionProfiles = null;
    private String selectionTable = null;

    protected ProductSelectionsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            ProductSelectionProfiles productSelectionProfiles) {

        super(jdbcTemplate, tables, sessionFactory);
        this.selectionTable = getTable("productSelections");
        this.productSelectionProfiles = productSelectionProfiles;
    }

    @Override
    protected Class<ProductSelectionImpl> getEntityClass() {
        return ProductSelectionImpl.class;
    }

    public ProductSelection find(Employee employee, boolean returnDefault) {
        ProductSelection config = null;
        StringBuilder hql = new StringBuilder(128);
        hql.append("from ").append(ProductSelectionImpl.class.getName())
                .append(" selection where selection.employee.id = :employeeId");
        List<ProductSelection> configs = getCurrentSession().createQuery(
                hql.toString()).setParameter("employeeId", employee.getId()).list();
        if (configs.isEmpty() && returnDefault) {
            ProductSelectionProfile profile = productSelectionProfiles.getDefaultProfile();
            if (profile != null) {
                configs.add(new ProductSelectionImpl(profile));
            }
        }
        if (!configs.isEmpty()) {
            config = configs.get(0);
        }
        return config;
    }

    public List<ProductSelectionConfigItem> findByEditablePartnerPrice() {
        StringBuilder hql = new StringBuilder(128);
        hql.append("from ").append(ProductSelectionConfigItemImpl.class.getName())
                .append(" item where item.partnerPriceEditable = true");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public void delete(Employee employee) {
        if (employee != null) {
            StringBuilder sql = new StringBuilder("DELETE FROM ");
            sql.append(selectionTable).append(" WHERE reference_id = ").append(employee.getId());
            jdbcTemplate.execute(sql.toString());
        }
    }
}
