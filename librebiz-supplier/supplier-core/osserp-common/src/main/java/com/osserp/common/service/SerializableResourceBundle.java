/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.service;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SerializableResourceBundle extends ResourceBundle implements Serializable {

    protected Hashtable<String, String> data = new Hashtable<String, String>();

    /**
     * Required for object serialization
     */
    protected SerializableResourceBundle() {
        super();
    }

    /**
     * Creates a new serializable resource bundle
     * @param valueMap
     */
    public SerializableResourceBundle(Map<String, Object> valueMap) {
        super();
        if (valueMap != null) {
            Iterator<Map.Entry<String, Object>> it = valueMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Object> entry = it.next();
                Object value = entry.getValue();
                if (value != null) {
                    data.put(entry.getKey(), value.toString());
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.ResourceBundle#getKeys()
     */
    @Override
    public Enumeration<String> getKeys() {
        return data.keys();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.ResourceBundle#handleGetObject(java.lang.String)
     */
    @Override
    protected Object handleGetObject(String key) {
        return data.get(key);
    }

}
