/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Dec 28, 2015 10:32:25 AM
 *
 */
package com.osserp.core.dao.records.impl

import groovy.sql.Sql

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dao.DataSourceService
import com.osserp.common.util.CalendarUtil
import com.osserp.core.employees.Employee
import com.osserp.core.finance.RecordNumberCreator
import com.osserp.core.finance.RecordType

/**
 * 
 * Provides basic record sequence operations 
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractSequenceNumberCreator extends DataSourceService {
    private static Logger logger = LoggerFactory.getLogger(AbstractSequenceNumberCreator.class.getName())

    String sequenceTableName
    
    protected final String getSequenceName(Long company, RecordType type) {
        Map sequence = getSequence(company, type)
        return sequence?.name
    }
    
    protected final Map getSequence(Long company, RecordType type) {
        String sequenceNameTableQuery =
            "SELECT * FROM ${sequenceTableName} WHERE company_id = ${company} AND type_id = ${type?.id}"
        Map sequence
        try {
            Sql sql = getSql()
            List seqs = sql.rows(sequenceNameTableQuery.toString())
            sequence = !seqs.isEmpty() ? seqs.get(0) : null
            
        } catch (Exception e) {
            logger.error("getSequence: failed [query=${sequenceNameTableQuery}, message=${e.message}]", e)
        }
        return sequence
    }

    protected final List<Map> getSequenceListBySameName(String name) {
        String sequenceNameTableQuery =
            "SELECT * FROM ${sequenceTableName} WHERE name = '${name}'"
        List<Map> sequenceList = []
        try {
            Sql sql = getSql()
            List seqs = sql.rows(sequenceNameTableQuery.toString())
            seqs.each {
                sequenceList << it
            }
        } catch (Exception e) {
            logger.error("getSequenceListBySameName: failed [query=${sequenceNameTableQuery}, message=${e.message}]", e)
        }
        return sequenceList
    }
}
