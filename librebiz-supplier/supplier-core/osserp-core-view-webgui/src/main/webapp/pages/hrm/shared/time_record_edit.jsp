<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:set var="view" value="${sessionScope.timeRecordingView}"/>
<c:set var="recording" value="${view.timeRecording}"/>
<c:set var="period" value="${recording.selectedPeriod}"/>
<c:set var="selected" value="${view.selectedType}"/>
<c:set var="lastBooking" value="${period.lastRecord}"/>
<c:set var="stopBooking" value="${lastBooking.currentDay and lastBooking.type.starting}"/>

<o:form id="timeRecordForm" name="timeRecordForm" url="/timeRecordings.do">
	<input type="hidden" name="method" value="save"/>
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="createBooking" /></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <c:if test="${!empty lastBooking}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="lastBooking"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:date value="${lastBooking.value}" addtime="true"/>
                                <span> - </span><o:out value="${lastBooking.type.name}"/>
                            </div>
                        </div>
                    </div>
                </c:if>

                <c:choose>
                    <c:when test="${!selected.starting}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="nextBooking"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="type" class="form-control">
                                        <option value="<o:out value="${view.stopType.id}"/>"><o:out value="${view.stopType.name}"/></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:set var="types" value="${view.startTypes}"/>
                        <c:if test="${selected.interval}">
                            <c:set var="types" value="${view.intervalTypes}"/>
                        </c:if>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="nextBooking"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="type" class="form-control" onChange="gotoUrl(this.value);">
                                        <c:forEach var="obj" items="${types}">
                                            <c:choose>
                                                <c:when test="${view.selectedType.id == obj.id}">
                                                    <option value="<c:url value="/timeRecordings.do"><c:param name="method" value="selectType"/><c:param name="id" value="${obj.id}"/></c:url>" selected="selected"><o:out value="${obj.name}"/></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="<c:url value="/timeRecordings.do"><c:param name="method" value="selectType"/><c:param name="id" value="${obj.id}"/></c:url>"><o:out value="${obj.name}"/></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${!view.manualRecordingMode}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="clockLabel"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${view}" param="startValue"/>
                                    <span> - </span>
                                    <o:out value="${view}" param="startValueHours"/>:<o:out value="${view}" param="startValueMinutes"/>
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div id="datePicker" style="position: absolute; visibility: hidden; background-color: white; z-index:10;"> </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${stopBooking}"><fmt:message key="time"/></c:when>
                                        <c:otherwise><fmt:message key="startBooking"/></c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="startValue" value="<o:out value="${view}" param="startValue"/>" id="startValueInput" />
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <o:datepicker input="startValueInput"/>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <fmt:message key="atTime"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input  class="form-control right" type="text" name="startValueHours" value="<o:out value="${view}" param="startValueHours"/>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input class="form-control right" type="text" name="startValueMinutes" value="<o:out value="${view}" param="startValueMinutes"/>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="comment"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <textarea name="note" class="form-control" rows="4"></textarea>
                                </div>
                            </div>
                        </div>

                        <c:if test="${view.selectedType.interval}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="stopBooking"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="stopValue" value="<o:out value="${view}" param="stopValue"/>" id="stopValueInput" />
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <o:datepicker input="stopValueInput"/>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <fmt:message key="atTime"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input class="form-control right" type="text" name="stopValueHours" value="<o:out value="${view}" param="stopValueHours"/>"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input class="form-control right" type="text" name="stopValueMinutes" value="<o:out value="${view}" param="stopValueMinutes"/>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${view.manualRecordingMode}">
                        <div class="row next">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/timeRecordings.do?method=disableEdit"/>');" value="<fmt:message key="exit"/>" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <o:submit styleClass="form-control" value="save" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="row next">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/timeRecordings.do?method=enableManualRecordingMode"/>');" value="<fmt:message key="timeInputPrompt"/>" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <o:submit styleClass="form-control" value="directBookingPrompt" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</o:form>
