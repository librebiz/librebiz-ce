/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Oct-2006 03:40:59 
 * 
 */
package com.osserp.gui.client.requests.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.requests.Request;

import com.osserp.gui.client.impl.AbstractBusinessCaseView;
import com.osserp.gui.client.requests.RequestView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("businessCaseView")
public class RequestViewImpl extends AbstractBusinessCaseView implements RequestView {
    private static Logger log = LoggerFactory.getLogger(RequestViewImpl.class.getName());

    protected RequestViewImpl() {
        super();
    }

    protected RequestViewImpl(String forwardTarget, Long requestLetterTypeId) {
        super(forwardTarget, requestLetterTypeId);
    }

    public void init(Request request, String exitTarget) throws ClientException, PermissionException {
        super.load(request, exitTarget);
    }

    public int getDocumentCount() {
        return (getRequestDocumentCount() + getRequestPictureCount() + getLetterCount());
    }

    public void updateAgentCommission(Form form) throws ClientException {
        Request request = getRequest();
        Double ac = form.getDouble(Form.AGENT_COMMISSION);
        boolean cp = form.getBoolean(Form.AGENT_COMMISSION_PERCENT);
        request.configureAgent(request.getAgent(), ac, cp);
        getRequestManager().update(request);
    }

    public void updateTipCommission(Form form) throws ClientException {
        Request request = getRequest();
        Double ac = form.getDouble(Form.AGENT_COMMISSION);
        request.configureTip(request.getTip(), ac);
        getRequestManager().update(request);
    }

    @Override
    public void reload() {
        try {
            Request request = getRequestSearch().findById(getRequest().getRequestId());
            setBean(request);
            loadDocumentCounters();
            loadFetchmailInboxCounters();
            loadRecordCounters();
            loadTemplateCounters();
            reloadEvents();
            loadStickyNote();
        } catch (Throwable t) {
            log.warn("reload() ignoring failure: " + t.toString(), t);
        }
    }

    @Override
    protected void loadRecordCounters() {
        setRecordCount(getSalesOfferManager().getReferencedCount(getRequest()));
    }
}
