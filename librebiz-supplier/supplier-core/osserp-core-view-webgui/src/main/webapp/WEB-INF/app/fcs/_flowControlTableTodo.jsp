<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="popup" value="${requestScope.popup}" />
<c:set var="view" value="${requestScope.view}" />
<c:set var="businessCase" value="${view.businessCase}" />

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="actionName"><fmt:message key="todo" /></th>
                <th class="actionUser">
                    <c:choose>
                        <c:when test="${empty view.selectedFlowControlGroup}">
                            <fmt:message key="responsibility" />
                        </c:when>
                        <c:otherwise>
                            <v:link url="${view.baseLink}/selectGroup" title="displayAll">
                                <fmt:message key="responsibility" />
                            </v:link>
                        </c:otherwise>
                    </c:choose>
                </th>
                <th class="action">
                    <c:if test="${!empty view.wastebasket}">
                        <v:link url="${view.baseLink}/enableWastebasketMode" title="displayTrash">
                            <o:img name="trashIcon" styleClass="smallicon" />
                        </v:link>
                    </c:if>
                </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="todo" items="${view.actions}" varStatus="s">
                <c:if test="${!empty todo.permissions}">
                    <o:forbidden role="${todo.permissions}">
                        <c:set var="hideAction" value="true"/>
                    </o:forbidden>
                </c:if>
                <c:if test="${empty hideAction and (empty view.selectedFlowControlGroup or view.selectedFlowControlGroup == todo.groupId)}">
                    <tr>
                        <c:choose>
                            <c:when test="${popup}">
                                <td class="actionName"><o:out value="${todo.name}" /></td>
                                <td class="actionUser">
                                    <c:choose>
                                        <c:when test="${todo.triggered}">
                                            <fmt:message key="system" /> (<fmt:message key="automatically" />)
                                        </c:when>
                                        <c:otherwise>
                                            <oc:options name="employeeGroups" value="${todo.groupId}" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="action"></td>
                            </c:when>
                            <c:otherwise>
                                <td class="actionName">
                                    <c:choose>
                                        <c:when test="${todo.manualSelectionDisabled}">
                                            <o:out value="${todo.name}" />
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="${view.baseLink}/selectAction?id=${todo.id}" title="selectAction">
                                                <o:out value="${todo.name}" />
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="actionUser">
                                    <c:choose>
                                        <c:when test="${todo.triggered}">
                                            <fmt:message key="system" /> (<fmt:message key="automatically" />)
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${empty view.selectedFlowControlGroup}">
                                                    <v:link url="${view.baseLink}/selectGroup?id=${todo.groupId}" title="displayThisGroupOnly">
                                                        <oc:options name="employeeGroups" value="${todo.groupId}" />
                                                    </v:link>
                                                </c:when>
                                                <c:otherwise>
                                                    <oc:options name="employeeGroups" value="${todo.groupId}" />
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="action">
                                    <c:if test="${todo.throwable}">
                                        <v:link url="${view.baseLink}/throwAction?id=${todo.id}" title="title.trash.throw">
                                            <o:img name="deleteIcon" styleClass="bigicon" />
                                        </v:link>
                                    </c:if>
                                </td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:if>
            </c:forEach>
        </tbody>
    </table>
</div>
