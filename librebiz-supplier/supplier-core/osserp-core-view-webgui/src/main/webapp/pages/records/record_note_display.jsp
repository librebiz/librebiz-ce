<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${!empty record.note}">
<div class="recordLabelValue">
<fmt:message key="recordNotes"/>:
<br/><br/><o:textOut value="${record.note}"/>
</div>
</c:if>