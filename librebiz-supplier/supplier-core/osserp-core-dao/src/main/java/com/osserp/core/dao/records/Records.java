/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 3:37:49 PM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.Date;
import java.util.List;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordStatusHistory;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Stock;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Records extends FinanceRecords {
    
    /**
     * Provides the record type the implementation is responsible for. 
     * @return recordType
     */
    RecordType getRecordType();

    /**
     * Provides stock by record
     * @param record
     * @return stock by branch or default stock
     */
    Stock getStockByRecord(Record record);

    /**
     * Provides a booking type by id
     * @param id
     * @return booking type or null if not supported
     */
    BookingType getBookingType(Long id);

    /**
     * Provides the default booking type
     * @return booking type or null if not supported
     */
    BookingType getDefaultBookingType();

    /**
     * Provides record specific booking types if available
     * @return bookingTypes or empty list if not supported or available
     */
    List<BookingType> getBookingTypes();
    
    /**
     * Provides all sales related to records of implementing type
     * @param salesId
     * @return records or empty list
     */
    List<RecordDisplay> getBySalesId(Long salesId);

    /**
     * Provides all open records of implementing type
     * @param descending
     * @return open
     */
    List<Record> getOpen(boolean descending);

    /**
     * Provides all unreleased records
     * @return unreleased
     */
    List<Record> getUnreleased();

    /**
     * Provides all unreleased records of implementing type and company
     * @param company or null if all records should be selected
     * @return unreleased
     */
    List<RecordDisplay> findUnreleased(Long company);

    /**
     * Loads a well known record from persistent storage. This method throws a runtime exception if no such record exists.
     * @param id of the record
     * @return record
     */
    Record load(Long id);

    /**
     * Loads all records by primary key. Id must not exist.
     * @param ids
     * @return records
     */
    List<Record> load(List<Long> ids);

    /**
     * Persist a record. This method performs no validation, ist just stores.
     * @param record to persist
     */
    void save(Record record);

    /**
     * Updates global printOptionDefaults for a recordType by 
     * the actual settings of a record.
     * @param record
     */
    void updatePrintOptionDefaults(Record record);

    /**
     * Changes related contact
     * @param user
     * @param record
     * @param contact
     * @return record
     */
    Record changeContact(Employee user, Record record, ClassifiedContact contact);

    /**
     * Delete permission is always grant if record is not unchangeable and 
     * method is invoked by creating or changing user. 
     * Delete permission is also grant if not unchangeable and implementing 
     * class does not provide a value for deletePermissions property.
     * 
     * If this requirements are not met, the result depends on the result of 
     * invocation of the protected deletePermissionGrantByRecordType method.
     *   
     * @param user
     * @param record
     * @return true if user has record delete permission grant
     */
    boolean deletePermissionGrant(DomainUser user, Record record);

    /**
     * Deletes a record
     * @param record
     * @param user deleting the record
     */
    void delete(Record record, Employee user);

    /**
     * Provides the status history
     * @param recordId
     * @param recordType
     * @return statusHistory
     */
    List<RecordStatusHistory> getStatusHistory(Long recordId, Long recordType);

    /**
     * Provides the id of the employee whose created last status entry
     * @param record
     * @return employee id
     */
    Long getLastStatusBy(Record record);

    /**
     * Creates a new status history entry
     * @param record
     * @param status
     * @param createdBy
     */
    void createStatusHistory(Record record, Long status, Long createdBy);

    /**
     * Sets the default infos for a record
     * @param user
     * @param record
     */
    void setDefaultInfos(Employee user, Record record);

    /**
     * Provides item changed history
     * @param id
     * @return item changed infos or empty list
     */
    List<ItemChangedInfo> getItemChangedInfos(Long id);

    /**
     * Indicates whether given record exists
     * @param recordId
     * @return true if so
     */
    boolean exists(Long recordId);

    /**
     * Counts all records related to given reference
     * @param referenceId
     * @return count
     */
    int count(Long referenceId);

    /**
     * Provides the created date of a record
     * @param id of the record
     * @return created or null if not exists
     */
    Date getCreated(Long id);

    /**
     * Loads the payment agreement from persistent storage
     * @param id of the payment agreement
     * @return condition
     */
    PaymentCondition loadPaymentCondition(Long id);

    /**
     * Provides the id of the record with the highest id. This method may be
     * useful to provide some hints for sequence reconfiguration tasks.
     * @param company
     * @return highest record id
     */
    Long getHighestId(Long company);

    /**
     * Provides the id of the record with the lowest id. This method may be
     * useful to provide some hints for sequence reconfiguration tasks.
     * @param company
     * @return lowest record id
     */
    Long getLowestId(Long company);
}
