<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.selectedType.name}" />
                -
                <o:out value="${view.selectedType.description}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:choose>
                <c:when test="${view.contactPersonSelected}">
                    <c:import url="_contactContactInput.jsp" />
                    <c:import url="_contactContactInputPerson.jsp" />
                </c:when>
                <c:when test="${view.privateContactSelected}">
                    <c:import url="_contactContactInput.jsp" />
                    <c:import url="_contactContactInputPrivate.jsp" />
                </c:when>
                <c:otherwise>
                    <c:import url="_contactCompanyInput.jsp" />
                    <c:choose>
                        <c:when test="${view.freelanceSelected}">
                            <c:import url="_contactContactInputFreelance.jsp" />
                        </c:when>
                    </c:choose>
                </c:otherwise>
            </c:choose>
            <c:if test="${view.tenantSelected or view.officeSelected}">
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="/contacts/contactEditor/exit" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit value="save" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

        </div>
    </div>
</div>

<c:if test="${!view.tenantSelected and !view.officeSelected}">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="contactingLabel" />
                    <c:if test="${!view.contactPersonSelected}"><span> / </span><fmt:message key="gdprStatus" /></c:if>
                </h4>
            </div>
        </div>
        <div class="panel-body">

            <div class="form-body">

                <c:if test="${!view.contactPersonSelected}">
                    <c:import url="_contactContactInputGrants.jsp" />
                </c:if>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="newsletter" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="newsletterConfirmation"> <v:checkbox id="newsletterConfirmation" name="newsletterConfirmation" /> <fmt:message key="newsletterConfirmationInfo" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>&nbsp;</span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text name="newsletterConfirmationNote" styleClass="form-control" title="confirmationNoteInfo" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="vip" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="vip"> <v:checkbox id="vip" name="vip" /> <fmt:message key="contactVIPStatusLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>&nbsp;</span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text name="vipNote" styleClass="form-control" title="contactVIPNoteInfo" />
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="/contacts/contactEditor/exit" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit value="save" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</c:if>