<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.businessCaseView}" />
<c:set var="customer" value="${requestScope.customer}" />

<div class="contentBox">
    <div class="contentBoxHeader">
        <div class="contentBoxHeaderLeft">
            <div class="subHeaderLeft"></div>
            <div class="subHeaderRight">
                <c:choose>
                    <c:when test="${!empty view.businessCase and view.customerContext}">
                        <%-- exitTarget already refers to customer --%>
                        <a href="<c:url value="${requestScope.customerExitUrl}"/>"><fmt:message key="customer" /></a>
                    </c:when>
                    <c:when test="${!empty view.businessCase and view.salesContext}">
                        <a href="<c:url value="${requestScope.contactDisplayUrl}"/>"><fmt:message key="customer" /></a>
                    </c:when>
                    <c:otherwise>
                        <a href="<c:url value="${requestScope.contactDisplayUrl}"/>"><fmt:message key="potencialBuyer" /></a>
                    </c:otherwise>
                </c:choose>
                <o:out value="${customer.id}" />
                <c:if test="${!empty customer.created}">
                    <fmt:message key="from" />
                    <o:date value="${customer.created}" />
                </c:if>
            </div>
        </div>
        <div class="contentBoxHeaderRight">
            <div class="boxnav">
                <o:permission role="request_customer_change" info="requestCustomerChange">
                    <c:if test="${!customer.grantContactNone && !view.businessCase.cancelled and !view.businessCase.closed}">
                        <ul>
                            <li>
                                <c:choose>
                                    <c:when test="${!empty view and view.salesContext}">
                                        <c:if test="${!empty view.sales and view.sales.changeable}">
                                            <a href="<v:url value="/contacts/contactSearch/forward?exit=/projectDisplay.do&selectionTarget=/salesCustomerChange.do&context=customer"/>" title="<fmt:message key="customerChange"/>"><o:img name="editIcon" /></a>
                                        </c:if>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="<v:url value="/contacts/contactSearch/forward?exit=/requestDisplay.do&selectionTarget=/requestCustomerChange.do&context=customer"/>" title="<fmt:message key="potencialBuyerChange"/>"><o:img name="editIcon" /></a>
                                    </c:otherwise>
                                </c:choose>
                            </li>
                        </ul>
                    </c:if>
                </o:permission>
            </div>
        </div>
    </div>
    <div class="contentBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>
                <table class="valueTable first33">
                    <c:choose>
                        <c:when test="${customer.grantContactNone}">
                            <tr>
                                <td><fmt:message key="contactForbidden" /></td>
                                <td><o:out value="${customer.displayName}" /></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td><fmt:message key="name" /></td>
                                <td><o:out value="${customer.displayName}" /></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="address" /></td>
                                <td><o:out value="${customer.address.street}" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><o:out value="${customer.address.zipcode}" /> <o:out value="${customer.address.city}" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><oc:options name="countryNames" value="${customer.address.country}" /></td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                </table>
                <div class="spacer"></div>
            </div>
        </div>
    </div>
</div>
