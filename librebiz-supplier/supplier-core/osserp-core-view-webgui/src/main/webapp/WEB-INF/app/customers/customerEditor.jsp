<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="customerEditorView" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="javascript" type="string">
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="customerEditorContent">
            <v:form url="${view.baseLink}/save" id="customerForm" name="customerForm">
                <div class="row">
                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4><o:out value="${view.bean.displayName}" /></h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-body">

                                <c:if test="${!view.bean.client}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label><fmt:message key="taxTreatment" /></label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <oc:select name="typeId" styleClass="form-control" options="customerTypes" value="${view.bean.typeId}" /></td>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><fmt:message key="status" /></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <oc:select name="statusId" styleClass="form-control" options="customerStatus" value="${view.bean.statusId}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><fmt:message key="vatId" /></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <v:text name="vatId" styleClass="form-control" value="${view.bean.vatId}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><fmt:message key="invoiceBy" /></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <select name="invoiceBy" class="form-control">
                                                <c:forEach var="opt" items="${view.invoiceDeliveryList}">
                                                    <option value="${opt}"<c:if test="${opt == view.bean.invoiceBy}"> selected="selected"</c:if>><fmt:message key="${opt}" /></option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><fmt:message key="invoiceEmail" /></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <v:text name="invoiceEmail" styleClass="form-control" value="${view.bean.invoiceEmail}"
                                                placeholder="invoiceEmailInputPlaceholder" title="invoiceEmailInputHint" />
                                        </div>
                                    </div>
                                </div>

                                <c:if test="${!view.bean.client}">
                                    <o:permission role="executive,executive_sales,sales_agent_config" info="permissionEditAgentProvision">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label><fmt:message key="agent" /></label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label for="agent">
                                                                    <v:checkbox id="agent" name="agent" value="${view.bean.agent}" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <div style="padding-top: 6px;"><fmt:message key="provision" />:</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <v:text name="agentCommission" styleClass="form-control" value="${view.bean.agentCommission}" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label for="agent">
                                                                    <v:checkbox id="agentCommissionPercent" name="agentCommissionPercent" value="${view.bean.agentCommissionPercent}" />
                                                                    <fmt:message key="percent" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label><fmt:message key="tipProvider" /></label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label for="agent">
                                                                    <v:checkbox id="tipProvider" name="tipProvider" value="${view.bean.tipProvider}" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <div style="padding-top: 6px;"><fmt:message key="provision" />:</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <v:text name="tipCommission" styleClass="form-control" value="${view.bean.tipCommission}" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div style="padding-top: 6px;">EUR</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </o:permission>
                                </c:if>

                                <div class="row next">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <v:submitExit url="${view.baseLink}/exit" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <o:submit value="save" styleClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div> <!-- row -->
            </v:form>
        </div> <!-- content-area -->
    </tiles:put>
</tiles:insert>
