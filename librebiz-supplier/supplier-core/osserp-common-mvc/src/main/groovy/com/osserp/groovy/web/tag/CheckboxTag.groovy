/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 13.01.2011 10:19:15 
 * 
 */
package com.osserp.groovy.web.tag

import com.osserp.groovy.web.View
import com.osserp.groovy.web.WebUtil

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
class CheckboxTag extends AbstractFormTag  {

    String printvalue
    Boolean reverse
	
	protected final String execute() {
		StringBuilder buffer = new StringBuilder()
		buffer.append('<input type="checkbox" name="')
        if (id && !name) {
            name = id
        }
        buffer.append(name).append('"').append(renderCommonAttributes())
        if (printvalue) {
			buffer.append(' value="').append(printvalue).append('" ')
        } else {
			buffer.append(' value="true" ')
        }
		buffer.append(getChecked())
        buffer.append(renderDisabled())
        buffer.append(renderReadonly())
		buffer.append('/>')
		buffer.toString()
	}
	
	private String getChecked() {
        View view = getCurrentView()
		if (name) {
            if (view && (view.editMode || view.createMode)) { 
                Object obj = view.form?.params[name]
                if (obj instanceof Boolean) {
                    if (obj == true) {
                        return checkedval()
                    }
                } else if (obj && obj == 'true') {
                    return checkedval()
                }
			}
		}
		if (value) {
            return checkedval()
		}
        String val = WebUtil.getFormValue(view, name, objectpath, false)
        if (val && val == 'true') {
            return checkedval()
        }
        if (reverse) {
            return 'checked="checked"'
		}
		return ''
	}
    
    private String checkedval() {
        if (reverse) {
            return ''
        } else {
            return 'checked="checked"'
        }
    }
}

