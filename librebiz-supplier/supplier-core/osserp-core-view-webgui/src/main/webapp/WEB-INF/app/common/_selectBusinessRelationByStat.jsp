<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><v:sortLink key="id">ID</v:sortLink></th>
                <th><v:sortLink key="name"><fmt:message key="name" /></v:sortLink></th>
                <th class="right"><v:sortLink key="countOpen"><fmt:message key="open" /></v:sortLink></th>
                <th class="right"><v:sortLink key="capOpen"><fmt:message key="value" /></v:sortLink></th>
                <th class="right"><v:sortLink key="countStopped"><fmt:message key="stop" /></v:sortLink></th>
                <th class="right"><v:sortLink key="capStopped"><fmt:message key="value" /></v:sortLink></th>
                <th class="right"><v:sortLink key="countClosed"><fmt:message key="completion" /></v:sortLink></th>
                <th class="right"><v:sortLink key="capClosed"><fmt:message key="value" /></v:sortLink></th>
                <th class="right"><v:sortLink key="countLost"><fmt:message key="cancellation" /></v:sortLink></th>
                <th class="right"><v:sortLink key="capLost"><fmt:message key="value" /></v:sortLink></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="obj" items="${view.list}" varStatus="s">
                <c:if test="${obj.countTotal > 0}">
                    <tr>
                        <td><o:out value="${obj.id}" /></td>
                        <td><a href="<c:url value="${view.url}${obj.id}"><c:param name="exit" value="${view.baseLink}/reload"/></c:url>"><o:out value="${obj.name}" /></a></td>
                        <td class="right"><o:out value="${obj.countOpen}" /></td>
                        <td class="right"><o:number value="${obj.capOpen}" format="decimal" /></td>
                        <td class="right"><o:out value="${obj.countStopped}" /></td>
                        <td class="right"><o:number value="${obj.capStopped}" format="decimal" /></td>
                        <td class="right"><o:out value="${obj.countClosed}" /></td>
                        <td class="right"><o:number value="${obj.capClosed}" format="decimal" /></td>
                        <td class="right"><o:out value="${obj.countLost}" /></td>
                        <td class="right"><o:number value="${obj.capLost}" format="decimal" /></td>
                    </tr>
                </c:if>
            </c:forEach>
        </tbody>
    </table>
</div>
