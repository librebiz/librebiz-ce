/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 12, 2013 
 * 
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode

import com.osserp.core.finance.Stock
import com.osserp.core.system.BranchOffice
import com.osserp.core.system.SystemCompany

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BranchView extends AbstractCompanyAdminView {
    private static Logger logger = LoggerFactory.getLogger(BranchView.class.getName())

    List<SystemCompany> companies = []
    Stock stock

    BranchView() {
        super()
        overrideExitTarget = true
        dependencies = ['branchCreateView', 'contactView']
        providesEditMode()
    }
    
    BranchOffice getBranchOffice() {
        bean
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {

            companies = systemCompanyManager.getAll()

            Long id = getLong('id')
            if (id) {
                bean = branchOfficeManager.find(id)
                logger.debug("initRequest: done by primary key [id=" + id + "]")
            }
            id = getLong('contact')
            if (id) {
                bean = branchOfficeManager.findByContact(id)
                logger.debug("initRequest: done by contact ref [contact=" + id + "]")
            }
            if (!bean && env.branchCreateView?.bean != null) {
                bean = branchOfficeManager.find(env.branchCreateView.bean.id)
                addExitTarget(env.branchCreateView.exitTarget)
                logger.debug("initRequest: done by branchCreateView")
            }
            if (!bean && env.contactView?.bean != null) {
                logger.debug("initRequest: done by contactView")
                bean = branchOfficeManager.findByContact(env.contactView.bean.contactId)
            }
            if (bean) {
                loadCompanyLogo(branchOffice.company.id)
                loadStock()
            }
            if (!bean && !list) {
                list = branchOfficeManager.findAll()
            }
            if (getBoolean('enableEdit')) {
                enableEditMode()
                enableExternalInvocationMode()
            }
        }
    }

    void loadStock() {
        if (!bean) {
            stock = null
        } else {
            List<Stock> stockList = systemConfigManager.availableStocks
            stockList.each { Stock st ->
                if ((branchOffice.headquarter && st.defaultStock)
                        || (!branchOffice.headquarter &&
                            st.branchId && st.branchId == branchOffice.id)) {
                    stock = st
                }
            }
        }
    }

    @Override
    protected void createStock() {
        if (branchOffice) {
            stock = systemConfigManager.createStock(domainUser, branchOffice.company, branchOffice)
        }
    }

    @Override
    void save() {
        if (!bean) {
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
        }
        String name = form.getString('name')
        Long companyId = form.getLong('reference')
        def company = (companyId == null) ? null : systemCompanyManager.find(companyId)
        
        branchOfficeManager.update(
                bean,
                name,
                company,
                form.getString('shortkey'),
                form.getString('shortname'),
                form.getString('email'),
                form.getString('phoneCountry'),
                form.getString('phonePrefix'),
                form.getString('phoneNumber'),
                form.getLong('costCenter'),
                form.getBoolean('headquarter'),
                form.getBoolean('thirdparty'),
                form.getBoolean('customTemplates'),
                form.getBoolean('recordTemplatesDefault'))
        
        if (!externalInvocationMode) {
            disableEditMode()
        }
    }

    @Override
    void select() {
        super.select()
        loadStock()
    }

    @Override
    MenuItem getEditLink() {
        return navigationLink("/${context.name}/enableEditMode", 'writeIcon', 'edit', 'branch_edit', 'permissionEditBranch')
    }
}
