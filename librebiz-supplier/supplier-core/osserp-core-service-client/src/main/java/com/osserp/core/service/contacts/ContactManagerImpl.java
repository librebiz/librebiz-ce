/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2008 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.EmailValidator;
import com.osserp.common.util.PhoneUtil;

import com.osserp.core.NoteAware;
import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactManager;
import com.osserp.core.contacts.ContactNoteManager;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.dao.ContactCreator;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.MailingLists;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.employees.Employee;
import com.osserp.core.mail.NewsletterRecipient;
import com.osserp.core.tasks.ContactGrantsChangedSender;
import com.osserp.core.tasks.ContactUpdateSender;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactManagerImpl extends AbstractContactManager implements ContactManager {
    private static Logger log = LoggerFactory.getLogger(ContactManagerImpl.class.getName());
    private ContactGrantsChangedSender contactGrantsChangedSender;
    private ContactNoteManager contactNoteManager;
    private ContactCreator contactCreator;
    private MailingLists mailingLists;
    private SystemConfigs systemConfigs;

    public ContactManagerImpl(
            Contacts contacts,
            ContactCreator contactCreator,
            OptionsCache options,
            ResourceLocator resourceLocator,
            ContactNoteManager contactNoteManager,
            MailingLists mailingLists,
            ContactGrantsChangedSender contactGrantsChangedSender,
            ContactUpdateSender contactUpdateSender,
            SystemConfigs systemConfigs) {
        super(contacts, contactUpdateSender, options, resourceLocator);
        this.contactCreator = contactCreator;
        this.contactGrantsChangedSender = contactGrantsChangedSender;
        this.contactNoteManager = contactNoteManager;
        this.mailingLists = mailingLists;
        this.systemConfigs = systemConfigs;
    }

    public ContactManagerImpl(Contacts contacts) {
        super(contacts, null);
    }

    public Contact create(
            DomainUser user,
            Long type,
            String companyAffix,
            String companyName,
            String note,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String website,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException {
        Contact contact = contactCreator.create(
                fetchEmployeeId(user),
                type,
                companyAffix, companyName, street, streetAddon, zipcode, city,
                federalState, federalStateName, country, website,
                email, phoneNumber, faxNumber, mobileNumber, initialStatus);
        createInitialNote(fetchEmployee(user), contact, note);
        return contact;
    }

    public Contact create(
            DomainUser user,
            Long type,
            String companyAffix,
            String companyName,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            String note,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String website,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException {
        Contact contact = contactCreator.create(
                fetchEmployeeId(user),
                type,
                companyAffix, companyName, fetchSalutation(salutationId),
                fetchTitle(titleId), firstName, lastName,
                street, streetAddon, zipcode, city,
                federalState, federalStateName, country, website,
                email, phoneNumber, faxNumber, mobileNumber, initialStatus);
        createInitialNote(fetchEmployee(user), contact, note);
        return contact;
    }

    public Contact create(
            DomainUser user,
            Long type,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            Long spouseSalutationId,
            Long spouseTitleId,
            String spouseFirstName,
            String spouseLastName,
            Date birthDate,
            String note,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException {

        Employee createdBy = fetchEmployee(user);
        Locale userLocale = (user == null ? null : user.getLocale());
        if (isNotSet(salutationId) && !isRequiredValidationOnly()) {
            throw new ClientException(ErrorCode.SALUTATION_MISSING);
        }
        Contact contact = contactCreator.create(
                (createdBy == null ? null : createdBy.getId()),
                type,
                fetchSalutation(salutationId),
                fetchTitle(titleId),
                firstName,
                lastName,
                fetchSalutation(spouseSalutationId),
                fetchTitle(spouseTitleId),
                spouseFirstName,
                spouseLastName,
                createCustomSalutation(userLocale, salutationId, titleId, firstName, lastName, spouseSalutationId, spouseTitleId, spouseFirstName,
                        spouseLastName),
                createCustomAddressName(userLocale, salutationId, titleId, firstName, lastName, spouseSalutationId, spouseTitleId, spouseFirstName,
                        spouseLastName),
                birthDate, street, streetAddon, zipcode,
                city, federalState, federalStateName, country,
                email, phoneNumber, faxNumber, mobileNumber, initialStatus);
        createInitialNote(createdBy, contact, note);
        return contact;
    }

    public Contact create(
            Contact contactRef,
            DomainUser user,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office) throws ClientException {
        return contactCreator.create(
                contactRef,
                fetchEmployeeId(user),
                fetchSalutation(salutationId),
                fetchTitle(titleId),
                firstName, lastName, street,
                streetAddon, zipcode, city, federalState, federalStateName, country,
                position, section, office);
    }

    public Contact create(
            Contact contactRef,
            DomainUser user,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            Date birthDate,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office,
            String note,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber) throws ClientException {
        Contact contact = contactCreator.create(
                contactRef,
                fetchEmployeeId(user),
                fetchSalutation(salutationId),
                fetchTitle(titleId),
                firstName, lastName, birthDate,
                street, streetAddon, zipcode, city, federalState, federalStateName, country,
                position, section, office,
                email, phoneNumber, faxNumber, mobileNumber);
        createInitialNote(fetchEmployee(user), contact, note);
        return contact;
    }

    private void createInitialNote(Employee user, Contact contact, String note) {
        if (contactNoteManager != null && isSet(note)) {
            try {
                NoteAware noteAware = contactNoteManager.create(contact);
                contactNoteManager.addNote(user, noteAware, null, note);
            } catch (Exception e) {
                log.warn("createInitialNote() ignoring exception [message=" + e.getMessage() + "]", e);
            }
        }
    }

    public List<NewsletterRecipient> findNewsletterRecipients(boolean active) {
        return getContactsDao().findNewsletterRecipients(active);
    }

    public void updateNewsletterStatus(Long[] contacts, boolean status) {
        getContactsDao().updateNewsletterStatus(contacts, status);
    }

    public boolean isDeletable(Contact contact) {
        return false;
    }

    public boolean isRequiredValidationOnly() {
        return systemConfigs.isSystemPropertyEnabled("contactValidationMinimal");
    }

    public void updateEmail(
            Employee user,
            Contact contact,
            EmailAddress selected,
            Long type,
            String email,
            boolean primary,
            Long aliasOf)
            throws ClientException {

        if (isNotSet(type)) {
            throw new ClientException(ErrorCode.TYPE_REQUIRED);
        }
        EmailValidator.validate(email, true);
        List<Option> existing = getContactsDao().findExistingEmailContacts(email);
        if (!existing.isEmpty()) {
            for (int i = 0, j = existing.size(); i < j; i++) {
                Option next = existing.get(i);
                if (!next.getId().equals(contact.getContactId())) {
                    throw new ClientException(ErrorCode.EMAIL_EXISTS);
                }
            }
        }
        // TODO check against real managed domains to get this method save (independent from frontend checks) 
        EmailAddress existingInternal = fetchInternal(contact.getEmails());
        for (Iterator<EmailAddress> i = contact.getEmails().iterator(); i.hasNext();) {
            EmailAddress next = i.next();
            if (next.getId().equals(selected.getId())) {
                if (EmailAddress.INTERNAL.equals(type) && !selected.isInternal()) {
                    if (existingInternal == null) {
                        // setting existing to requested internal type 'cause no other exists
                        next.setType(type);
                    } else if (!next.isAlias()) {
                        // selected address should be set as internal but type is not managed
                        throw new ClientException(ErrorCode.TYPE_INVALID);
                    }
                    Long previousInternal = null;
                    for (Iterator<EmailAddress> i2 = contact.getEmails().iterator(); i2.hasNext();) {
                        EmailAddress next2 = i2.next();
                        if (!next2.getId().equals(next.getId())) {
                            if (next2.isInternal() && next.getAliasOf().equals(next2.getId())) {
                                next2.setType(EmailAddress.ALIAS);
                                next2.setAliasOf(next.getId());
                                next.setAliasOf(null);
                                next.setType(type);
                                previousInternal = next2.getId();
                            }
                        }
                    }
                    if (previousInternal != null) {
                        for (Iterator<EmailAddress> i2 = contact.getEmails().iterator(); i2.hasNext();) {
                            EmailAddress next2 = i2.next();
                            if (!next2.getId().equals(next.getId()) && next2.isAlias() && next2.getAliasOf().equals(previousInternal)) {
                                next2.setAliasOf(next.getId());
                            }
                        }
                    }
                } else {
                    next.setType(type);
                }
                next.setEmail(email);
                next.setPrimary(primary);
            } else {
                if (primary) {
                    next.setPrimary(false);
                }
            }
        }
        save(contact);
    }

    private EmailAddress fetchInternal(List<EmailAddress> list) {
        for (Iterator<EmailAddress> i = list.iterator(); i.hasNext();) {
            EmailAddress nextAddress = i.next();
            if (nextAddress.isInternal()) {
                return nextAddress;
            }
        }
        return null;
    }

    public void deleteEmail(Contact contact, Long id) {
        if (mailingLists.exists(id)) {
            mailingLists.removeAddress(id);
        }
        contact.removeEmail(id);
        save(contact);
    }

    public void addPhone(
            Employee user,
            Contact contact,
            Long device,
            Long type,
            String number,
            String note,
            boolean primary) throws ClientException {

        if (isNotSet(number)) {
            throw new ClientException(ErrorCode.PHONE_NUMBER_MISSING);
        }
        String[] pn = PhoneUtil.createPhoneNumber(number);
        //  pn: { country, prefix, number, "device" , "validationStatus" }
        if (pn != null) {
            contact.addPhone(
                    user == null ? Constants.SYSTEM_EMPLOYEE : user.getId(),
                    device,
                    type,
                    pn[0],
                    pn[1],
                    pn[2],
                    note,
                    primary);
            save(contact);
        }
    }

    public void updatePhone(
            Employee user,
            Contact contact,
            Phone phone,
            Long type,
            String country,
            String prefix,
            String number,
            String note,
            boolean primary) throws ClientException {

        contact.updatePhone(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId(), phone, type, country, prefix, number, note, primary);
        save(contact);
    }

    public void deletePhone(Employee user, Contact contact, Long id) {
        for (Iterator<Phone> i = contact.getPhoneNumbers().iterator(); i.hasNext();) {
            Phone phone = i.next();
            if (phone.getId().equals(id)) {
                i.remove();
                break;
            }
        }
        contact.setPersonChanged(new Date(System.currentTimeMillis()));
        if (user != null) {
            contact.setPersonChangedBy(user.getId());
        } else {
            contact.setPersonChangedBy(Constants.SYSTEM_EMPLOYEE);
        }
        save(contact);
    }

    public void save(Person person) {
        if (person instanceof Contact) {
            Contact c = (Contact) person;
            persist(c);
            sendChangedEvent(c);
        } else {
            throw new IllegalArgumentException("unexpected instance [expected=" + Contact.class.getName()
                    + ", found=" + (person == null ? "null" : person.getClass().getName()) + "]");
        }
    }

    public void updateBranchFlag(Long id, boolean branch) {
        getContactsDao().updateBranchFlag(id, branch);
    }

    @Override
    protected void sendGrantsChangedEvent(Contact contact) {
        if (contactGrantsChangedSender != null) {
            contactGrantsChangedSender.send(contact);
        }
    }

    private String createCustomAddressName(
            Locale userLocale,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            Long spouseSalutationId,
            Long spouseTitleId,
            String spouseFirstName,
            String spouseLastName) {

        Salutation salutation = fetchSalutation(salutationId);
        Option title = fetchTitle(titleId);
        Salutation spouseSalutation = fetchSalutation(spouseSalutationId);
        Option spouseTitle = fetchTitle(spouseTitleId);
        return createCustomAddressName(
                userLocale,
                salutation,
                title,
                firstName,
                lastName,
                spouseSalutation,
                spouseTitle,
                spouseFirstName,
                spouseLastName);
    }

    private String createCustomSalutation(
            Locale userLocale,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            Long spouseSalutationId,
            Long spouseTitleId,
            String spouseFirstName,
            String spouseLastName) {

        //info of spouse
        Salutation spouseSalutation = fetchSalutation(spouseSalutationId);
        Option spouseTitle = fetchTitle(spouseTitleId);
        Option title = fetchTitle(titleId);
        Salutation salutation = fetchSalutation(salutationId);
        if (salutation == null) {
            throw new IllegalArgumentException(Options.SALUTATIONS + ".id not existing: " + salutationId);
        }
        return createCustomSalutation(
                userLocale,
                salutation,
                title,
                firstName,
                lastName,
                spouseSalutation,
                spouseTitle,
                spouseFirstName,
                spouseLastName);
    }

    private Employee fetchEmployee(DomainUser user) {
        return (user == null || user.getEmployee() == null ? null : user.getEmployee());
    }

    private Long fetchEmployeeId(DomainUser user) {
        Employee employee = fetchEmployee(user);
        return (employee == null ? null : employee.getId());
    }
}
