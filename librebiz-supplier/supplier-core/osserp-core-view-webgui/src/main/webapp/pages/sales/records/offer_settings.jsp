<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:set var="recordView" scope="session" value="${sessionScope.salesOfferView}" />
<c:set var="record" value="${recordView.record}" />
<c:set var="hadSomethingToDisplay" value="false" />

<div class="content-area" id="salesOfferSetupContent">
    <div class="row">
        <div class="col-md-6 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><fmt:message key="actions"/></h4>
                </div>
            </div>
            <div class="panel-body">

                <c:if test="${record.contact.discountEnabled or record.rebateAvailable}">
                    <div class="row">
                        <c:choose>
                            <c:when test="${record.rebateAvailable}">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="rebateEnabled" />
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <c:if test="${!record.closed}">
                                            <a href="<c:url value="/salesOffer.do?method=removeRebate"/>">
                                                <fmt:message key="deactivate" />
                                            </a>
                                        </c:if>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="rebateDisabled" />
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <c:if test="${!record.closed}">
                                            <a href="<c:url value="/salesOffer.do?method=addRebate"/>">
                                                <fmt:message key="activate" />
                                            </a>
                                        </c:if>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <c:set var="hadSomethingToDisplay" value="true" />
                </c:if>

                <c:if test="${recordView.statusResetAvailable}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="status" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <oc:options name="recordStatus" value="${record.status}"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <a href="<c:url value="/salesOffer.do?method=resetStatus"/>">
                                    [<fmt:message key="reset" />]
                                </a>
                            </div>
                        </div>
                    </div>
                    <c:set var="hadSomethingToDisplay" value="true" />
                </c:if>
                <c:if test="${!hadSomethingToDisplay}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <fmt:message key="noActionsAvailableOnCurrentRecordState" />
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>
