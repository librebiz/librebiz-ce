/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 15.05.2015 
 * 
 */
package com.osserp.api;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface WSMedia extends WSObject {

    /**
     * Provides the id of a referenced entity (e.g. an article.id if media 
     * represents an article, contact.id if employee pic, etc). 
     * @return referenceId
     */
    Long getReferenceId();

    /**
     * The logical name of the reference (e.g. 'articles', 'employees', etc.)
     * @return referenceContext
     */
    String getReferenceContext();

    /**
     * Provides the public media name. This name may contain any string
     * and does not belong to source media, file or url.
     * @return name
     */
    String getName();

    /**
     * Provides the type of the media. The value corresponds to the document
     * shortname assigned by user. The default client document import expects 
     * this value as the column name of the client media import table.
     * @return mediaType
     */
    String getMediaType();

    /**
     * Provides the media url as relative path. 
     * @return url
     */
    String getUrl();
}
