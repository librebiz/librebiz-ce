<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${!empty view.bean.description}">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="description"><fmt:message key="description" /></label>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <o:out value="${view.bean.description}" />
            </div>
        </div>
    </div>
</c:if>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="responsible"><fmt:message key="responsible" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <oc:options name="employeeGroups" value="${view.bean.groupId}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <fmt:message key="displayAlways" />
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <c:choose>
                <c:when test="${view.bean.displayEverytime}">
                    <fmt:message key="yes" />
                </c:when>
                <c:otherwise>
                    <fmt:message key="no" />
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="status"><fmt:message key="status" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <o:out value="${view.bean.status}" />
        </div>
    </div>
</div>
