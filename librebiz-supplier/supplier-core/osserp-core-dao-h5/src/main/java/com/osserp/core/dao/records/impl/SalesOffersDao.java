/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 3:14:28 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.StringUtil;

import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.Calculations;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.impl.Procs;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesOffers;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.SalesOfferImpl;
import com.osserp.core.model.records.SalesOrderImpl;
import com.osserp.core.products.Product;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.SalesOffer;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOffersDao extends AbstractSalesRecords implements SalesOffers {
    private static Logger log = LoggerFactory.getLogger(SalesOffersDao.class.getName());

    private Calculations calculations = null;
    private String salesOfferItemsTable = null;
    private String salesOfferTable = null;
    private String salesOfferCalculationsTable = null;

    public SalesOffersDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Calculations calculations) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs);
        this.calculations = calculations;
        this.salesOfferItemsTable = getTable(TableKeys.SALES_OFFER_ITEMS);
        this.salesOfferTable = getTable(TableKeys.SALES_OFFERS);
        this.salesOfferCalculationsTable = getTable(TableKeys.SALES_OFFER_CALCULATIONS);
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.SALES_OFFER;
    }

    public List<Offer> getByRequest(Request request) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.reference = :requestId order by o.id desc");
        return getCurrentSession().createQuery(query.toString())
                .setParameter("requestId", request.getRequestId()).list();
    }

    public Offer findActivated(Request request) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.reference = :requestId AND status = ")
                .append(Offer.STAT_CLOSED);
        List<Offer> result = getCurrentSession().createQuery(sql.toString())
                .setParameter("requestId", request.getRequestId()).list();
        return (result.isEmpty() ? null : result.get(0));
    }

    public Long findActivated(Long requestId) {
        if (log.isDebugEnabled()) {
            log.debug("findActivated() invoked [id=" + requestId + "]");
        }
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT id FROM ")
                .append(salesOfferTable)
                .append(" WHERE reference_id = ? AND status = ")
                .append(Offer.STAT_CLOSED);
        final Object[] params = { requestId };
        final int[] types = { Types.BIGINT };
        @SuppressWarnings("rawtypes")
        List list = (List) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
        return empty(list) ? null : (Long) list.get(0);
    }

    public int getReferencedCount(Long requestId) {
        if (log.isDebugEnabled()) {
            log.debug("getReferencedCount() invoked [id=" + requestId + "]");
        }
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT count(*} FROM ")
                .append(salesOfferTable)
                .append(" WHERE reference_id = ?");
        final Object[] params = { requestId };
        final int[] types = { Types.BIGINT };
        return jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
    }

    public Offer create(Request request, Employee employee) throws ClientException {
        if (request.getType().isRecordByCalculation()) {
            Calculation calculation = fetchCalculation(request);
            return create(request, calculation, employee);
        }
        return createEmpty(request, employee);
    }

    public Offer createOrUpdate(Request request, List<Item> items) {
        List<Offer> existingOffers = getByRequest(request);
        SalesOffer result = null;
        if (!existingOffers.isEmpty()) {
            for (int i = 0, j = existingOffers.size(); i < j; i++) {
                Offer offer = existingOffers.get(i);
                if (!offer.isUnchangeable()) {
                    result = (SalesOffer) offer;
                    break;
                }
            }
        }
        if (result == null) {
            PaymentAgreement paymentAgreement = fetchPaymentAgreement(request, null);
            RecordType recordType = getRecordType();
            Employee user = fetchEmployee(request.getCreatedBy());
            Long id = getNextRecordId(user, request.getType().getCompany(), recordType, null);
            result = new SalesOfferImpl(
                    id,
                    recordType,
                    request,
                    paymentAgreement,
                    user,
                    taxRate,
                    reducedTaxRate);
            saveInitial(result);
            for (int i = 0, j = items.size(); i < j; i++) {
                Item next = items.get(i);
                Product product = next.getProduct();
                BigDecimal purchasePrice = product.getEstimatedPurchasePrice() == null
                        ? new BigDecimal(0)
                        : new BigDecimal(product.getEstimatedPurchasePrice().longValue());
                BigDecimal partnerPrice = product.getPartnerPrice() == null
                        ? new BigDecimal(0)
                        : new BigDecimal(product.getPartnerPrice().longValue());
                result.addItem(
                        product.getDefaultStock(),
                        product,
                        next.getCustomName(),
                        next.getQuantity(),
                        next.getTaxRate(),
                        next.getPrice(),
                        new Date(),
                        partnerPrice,
                        false, // override pp 
                        false, // pp overidden
                        purchasePrice,
                        next.getNote(),
                        true,
                        next.getExternalId());
            }
            save(result);
            setDefaultInfos(user, result, request.getType());
        }
        return result;
    }

    private Offer createEmpty(Request request, Employee employee) {
        // fetch previous offer if available to use previous payment agreement
        List<Offer> existingOffers = getByRequest(request);
        SalesOffer existing = null;
        if (!existingOffers.isEmpty()) {
            existing = (SalesOffer) existingOffers.get(0);
        }
        PaymentAgreement paymentAgreement = fetchPaymentAgreement(request, existing);
        RecordType recordType = getRecordType();
        Long id = getNextRecordId(employee, request.getType().getCompany(), recordType, null);
        SalesOfferImpl obj = new SalesOfferImpl(
                id,
                recordType,
                request,
                paymentAgreement,
                employee,
                taxRate,
                reducedTaxRate);
        saveInitial(obj);
        if (existing == null) {
            setDefaultInfos(employee, obj, request.getType());
        } else {
            setInfos(employee, obj, existing);
        }
        if (log.isDebugEnabled()) {
            log.debug("createEmpty() done [id=" + obj.getId() + ", request=" + request.getPrimaryKey() + "]");
        }
        return obj;
    }

    public Offer create(Request request, Calculation calculation, Employee employee)
            throws ClientException {
        // fetch previous offer if available to use previous payment agreement
        List<Offer> existingOffers = getByRequest(request);
        SalesOffer existing = null;
        if (!existingOffers.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("create() found existing offer, using existing payment agreement");
            }
            existing = (SalesOffer) existingOffers.get(0);
        }
        RecordType recordType = getRecordType();
        PaymentAgreement paymentAgreement = fetchPaymentAgreement(request, existing);
        Long id = getNextRecordId(employee, request.getType().getCompany(), recordType, null);
        SalesOfferImpl obj = new SalesOfferImpl(
                id,
                recordType,
                request,
                calculation,
                paymentAgreement,
                employee,
                taxRate,
                reducedTaxRate);
        saveInitial(obj);
        if (existing == null) {
            setDefaultInfos(employee, obj, request.getType());
        } else {
            setInfos(employee, obj, existing);
        }
        createCalculationLink(obj, calculation);
        calculation.setInitialCreated(obj.getCreated());
        calculations.save(calculation);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + obj.getId() + ", request=" + request.getPrimaryKey() + "]");
        }
        return obj;
    }

    public Offer create(Employee employee, Offer offer) throws ClientException {
        if (employee == null || offer == null) {
            log.warn("create() invoked with unexpected null arg [employee="
                    + (employee == null ? "null" : employee.getId())
                    + ", offer=" + (offer == null ? "null" : offer.getId()) + "]");
            throw new BackendException("employee or offer null");
        }
        RecordType recordType = getRecordType();
        Long id = getNextRecordId(employee, offer.getCompany(), recordType, null);
        SalesOfferImpl obj = new SalesOfferImpl(
                id,
                recordType,
                offer,
                employee,
                taxRate,
                reducedTaxRate);
        saveInitial(obj);
        obj.setPrintBusinessCaseId(offer.isPrintBusinessCaseId());
        obj.setPrintBusinessCaseInfo(offer.isPrintBusinessCaseInfo());
        obj.setPrintComplimentaryClose(offer.isPrintComplimentaryClose());
        obj.setPrintConfirmationPlaceholder(offer.isPrintConfirmationPlaceholder());
        obj.setPrintPaymentTarget(offer.isPrintPaymentTarget());
        obj.setPrintProjectmanager(offer.isPrintProjectmanager());
        obj.setPrintRecordDate(offer.isPrintRecordDate());
        obj.setPrintRecordDateByStatus(offer.isPrintRecordDateByStatus());
        obj.setPrintSalesperson(offer.isPrintSalesperson());
        save(obj);
        setInfos(employee, obj, offer);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + obj.getId() + ", source=" + offer.getId()
                    + ", request=" + obj.getReference() + "]");
        }
        return obj;
    }

    private PaymentAgreement fetchPaymentAgreement(Request request, SalesOffer existing) {
        if (existing != null && existing.getPaymentAgreement() != null) {
            return existing.getPaymentAgreement();
        }
        boolean externalRequest = (request == null ? false : isSet(request.getExternalReference()));
        Customer customer = (request == null ? null : request.getCustomer());
        if (!externalRequest && customer != null) {
            if (customer.isReseller()) {
                PaymentAgreement customerAgreement = ((customer.getPaymentAgreement() != null) ? customer.getPaymentAgreement() : null);
                if (customerAgreement != null && customerAgreement.getId() != null
                        && customerAgreement.getPaymentCondition() != null) {
                    return customerAgreement;
                }
            } else {
                StringBuilder hql = new StringBuilder(64);
                hql
                        .append("from ")
                        .append(SalesOrderImpl.class.getName())
                        .append(" o where o.contact.id = :customerId order by o.created desc");
                List<SalesOrderImpl> result = getCurrentSession().createQuery(hql.toString())
                        .setParameter("customerId", customer.getId())
                        .list();
                if (!result.isEmpty()) {
                    SalesOrderImpl obj = result.get(0);
                    return obj.getPaymentAgreement();
                }
            }
        }
        return getPaymentAgreement((request != null ? request.getType() : null), existing);
    }

    public void changeBranch(Request request) {
        StringBuilder sql = new StringBuilder(128);
        sql
                .append("UPDATE ")
                .append(salesOfferTable)
                .append(" SET branch_id = ? WHERE reference_id = ? AND status < ?");
        final Object[] params = { request.getBranch().getId(), request.getRequestId(), Record.STAT_SENT };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }

    public boolean exists(Long recordId) {
        return exists(salesOfferTable, recordId);
    }

    public int count(Long referenceId) {
        return count(salesOfferTable, referenceId);
    }

    public int countBySales(Long salesId) {
        return countBySales(salesOfferTable, salesId);
    }

    public Date getCreated(Long id) {
        return getCreated(salesOfferTable, id);
    }

    public Offer findByCalculation(Calculation calculation) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT reference_id FROM ")
                .append(salesOfferCalculationsTable)
                .append(" WHERE calculation_id = ?");
        final Object[] params = { calculation.getId() };
        final int[] types = { Types.BIGINT };
        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper());
        return empty(list) ? null : (Offer) load(list.get(0));
    }

    public Offer findWithoutCalculation(Long requestId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT id FROM ")
                .append(salesOfferTable)
                .append(" WHERE reference_id = ? AND id NOT IN ")
                .append("(SELECT reference_id FROM ")
                .append(salesOfferCalculationsTable)
                .append(")");

        Object[] params = { requestId };
        int[] types = { Types.BIGINT };

        List<Long> list = (List) jdbcTemplate.query(
                sql.toString(), params, types, getLongRowMapper());

        Long result = empty(list) ? null : (Long) list.get(0);
        if (log.isDebugEnabled()) {
            log.debug("getOfferWithoutCalculation() done [result=" + result + "]");
        }
        return isEmpty(result) ? null : (Offer) load(result);
    }

    public Calculation getCalculation(Record record) {
        Long calculationId = getCalculationByOffer(record.getId());
        if (calculationId == null) {
            return null;
        }
        return (Calculation) calculations.load(calculationId);
    }

    public void deselectOffer(Offer offer) {
        Calculation calc = getCalculation(offer);
        if (calc != null) {
            calc.setHistorical(true);
            calculations.save(calc);
        }
        if (offer instanceof SalesOffer) {
            ((SalesOffer) offer).resetOrderStatus();
            save(offer);
        }
    }

    public Double getAveragePlantPower(Long reference) {
        List<Double> list = (List<Double>) jdbcTemplate.query(
                Procs.GET_REQUEST_CAPACITY,
                new Object[] { reference },
                new int[] { Types.BIGINT },
                getDoubleRowMapper());
        return empty(list) ? 0d : list.get(0);
    }

    public void updateSalesRevenue(Product product) {
        StringBuilder sql = new StringBuilder(64);
        sql.append("SELECT id FROM ")
                .append(salesOfferItemsTable)
                .append(" WHERE product_id = ? AND reference_id IN (SELECT id FROM ")
                .append(salesOfferTable)
                .append(" WHERE status BETWEEN ")
                .append(Record.STAT_NEW)
                .append(" AND ")
                .append(Record.STAT_SENT)
                .append(" AND id NOT IN (SELECT reference_id FROM ")
                .append(salesOfferCalculationsTable)
                .append("))");

        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                new Object[] { product.getProductId() },
                new int[] { Types.BIGINT },
                getLongRowMapper());
        if (!list.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("updateSalesRevenue() found items [count=" + list.size() + "]");
            }
            String updateIds = StringUtil.createCommaSeparated(list);
            sql = new StringBuilder(64);
            sql
                    .append("UPDATE ")
                    .append(salesOfferItemsTable)
                    .append(" SET partner_price = ?, purchase_price = ? WHERE id IN (")
                    .append(updateIds)
                    .append(")");
            int updated = jdbcTemplate.update(
                    sql.toString(),
                    new Object[] { product.getPartnerPrice(), product.getCalculationPurchasePrice() },
                    new int[] { Types.DECIMAL, Types.DECIMAL });
            if (log.isDebugEnabled()) {
                log.debug("updateSalesRevenue() done [updatePerformed=" + (list.size() == updated) + "]");
            }
        }
    }

    @Override
    protected Class<SalesOfferImpl> getPersistentClass() {
        return SalesOfferImpl.class;
    }

    // local helpers

    private Calculation fetchCalculation(Request request) throws ClientException {
        Calculation calculation = (Calculation) calculations.getActive(request.getRequestId());
        if (calculation == null) {
            throw new ClientException(ErrorCode.CALCULATION_REQUIRED);

        } else if (calculation.getPrice() == null
                || calculation.getPrice().doubleValue() == 0) {
            throw new ClientException(ErrorCode.CALCULATION_INCOMPLETE);
        }
        return calculation;
    }

    private void createCalculationLink(Record record, Calculation calculation) {
        if (!isCalculationLinkExisting(record, calculation)) {
            StringBuilder sql = new StringBuilder(64);
            sql
                    .append("INSERT INTO ")
                    .append(salesOfferCalculationsTable)
                    .append(" (reference_id, calculation_id) VALUES (?,?)");
            final Object[] params = { record.getId(), calculation.getId() };
            final int[] types = { Types.BIGINT, Types.BIGINT };
            jdbcTemplate.update(
                    sql.toString(),
                    params,
                    types);
        }
    }

    private boolean isCalculationLinkExisting(Record record, Calculation calculation) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT id FROM ")
                .append(salesOfferCalculationsTable)
                .append(" WHERE reference_id = ? AND calculation_id = ?");
        final Object[] params = { record.getId(), calculation.getId() };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        @SuppressWarnings("rawtypes")
        List list = (List) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper());
        return empty(list) ? false : true;
    }

    private Long getCalculationByOffer(Long recordId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT calculation_id FROM ")
                .append(salesOfferCalculationsTable)
                .append(" WHERE reference_id = ?");
        final Object[] params = { recordId };
        final int[] types = { Types.BIGINT };
        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper());
        return empty(list) ? null : list.get(0);
    }
}
