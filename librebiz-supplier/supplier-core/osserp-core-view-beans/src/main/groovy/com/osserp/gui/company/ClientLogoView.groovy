/**
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsReference

import com.osserp.core.dms.CoreDocumentType
import com.osserp.core.system.SystemCompany
import com.osserp.core.system.SystemCompanyManager

import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.dms.AbstractDocumentView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ClientLogoView extends AbstractDocumentView {
    private static Logger logger = LoggerFactory.getLogger(ClientLogoView.class.getName())

    SystemCompany client

    ClientLogoView() {
        super()
        documentTypePicture = true
        dependencies = ['clientCompanyView', 'companyPrintConfigView']
    }

    @Override
    protected DmsReference getDmsReference() {
        new DmsReference(CoreDocumentType.CLIENT_LOGO, client.id)
    }


    @Override
    void initRequest(Map atts, Map headers, Map params) {
        logger.debug("initRequest: invoked")
        super.initRequest(atts, headers, params)

        if (!list) {
            if (env.companyPrintConfigView?.selectedCompany) {
                client = env.companyPrintConfigView.selectedCompany
            } else if (env.clientCompanyView?.bean) {
                client = env.clientCompanyView?.bean
            } else {
                logger.warn("initRequest: not bound [user=${user?.id}, name=clientCompanyView]")
                throw new ViewContextException(this)
            }
            reload()
        }
    }
    
    @Override
    void setReference() {
        super.setReference()
        if (lastChangedId && client) {
            DmsDocument document = dmsManager.getDocument(lastChangedId)
            systemCompanyManager.deployLogo(client, document)
        }
    }
    
    @Override
    void delete() {
        DmsDocument doc = fetchDocument(getLong('id'))
        if (doc && client) {
            super.delete()
            if (doc.referenceDocument && lastChangedId == doc.id) {
                systemCompanyManager.removeLogo(client, doc.fileName)
                SystemCompany updated = systemCompanyManager.find(client.id)
                if (updated) {
                    client = updated
                    if (env.companyPrintConfigView?.selectedCompany) {
                        env.companyPrintConfigView?.selectedCompany = updated
                    } 
                    if (env.clientCompanyView?.bean) {
                        env.clientCompanyView?.bean = updated
                    }
                }
            }
        }
    }

    protected SystemCompanyManager getSystemCompanyManager() {
        getService(SystemCompanyManager.class.getName())
    }
}
