<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:choose>
<c:when test="${empty sessionScope.user or empty sessionScope.portalView}">
<o:logger write="no user or portal available, sending info about probably timeout" level="info"/>
<c:redirect url="/errors/error_timeout.jsp"/>
</c:when>
<c:otherwise>
<o:logger write="user available, setting context error and forwarding to startup" level="info"/>
<c:set var="errors" scope="session"><fmt:message key="error.invalid.context"/></c:set>
<v:redirect url="/index"/>
</c:otherwise>
</c:choose>
