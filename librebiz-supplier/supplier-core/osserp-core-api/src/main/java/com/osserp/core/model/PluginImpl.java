/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2020 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.osserp.common.Property;
import com.osserp.common.beans.AbstractPropertyAware;

import com.osserp.core.Plugin;
import com.osserp.core.PluginHook;
import com.osserp.core.PluginType;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PluginImpl extends AbstractPropertyAware implements Plugin {

    private static final long serialVersionUID = 1L;

    private PluginType type;
    private String vendor;
    private String name;
    private String description;
    private String resourceKey;
    private boolean disabled;
    private List<PluginHook> hookList = new ArrayList<>();
    private Map<String, PluginHook> _hooks = new HashMap<>();

    protected PluginImpl() {}

    protected PluginImpl(PluginImpl other) {
        super(other);
        this.name = other.getName();
        this.description = other.getDescription();
        this.resourceKey = other.getResourceKey();
        this.disabled = other.isDisabled();
    }

    public Object clone() {
        return new PluginImpl(this);
    }

    @Override
    protected Property createProperty(String name, String value) {
        return new PluginPropertyImpl(name, value);
    }

    public PluginType getType() {
        return type;
    }

    protected void setType(PluginType type) {
        this.type = type;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResourceKey() {
        return resourceKey;
    }

    public void setResourceKey(String resourceKey) {
        this.resourceKey = resourceKey;
    }

    public boolean isDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public boolean isSelectionLabel() {
        return false;
    }

    public Map<String, PluginHook> getHooks() {
        if (_hooks.isEmpty()) {
            for (int i = 0, j = hookList.size(); i < j; i++) {
                PluginHook next = hookList.get(i);
                _hooks.put(next.getResourceKey(), next);
            }
        }
        return _hooks;
    }

    public List<PluginHook> getHookList() {
        return hookList;
    }

    protected void setHookList(List<PluginHook> hookList) {
        this.hookList = hookList;
    }
}
