/**
 * osserp-core - osserp-i18n-0.1.7.js - Common functions.
 * Copyright 2001-2021 The original author or authors.
 * Licensed under the Apache License, Version 2.0.
 * 
 * ojsI18n - A list of internationalized strings.
 * 
 * ae=%E4  oe=%F6  ue=%FC
 */

var ojsI18n_DE = {
	atPosition: unescape("an Position "),
	enterAValidDate: unescape("Bitte geben Sie ein richtiges Datum nach folgendem\nMuster ein (tt.mm.jjjj) oder (tt.mm.jj)"),
	error_email_invalid: unescape("Email ist ung%FCltig"),
	error_prefix_missing: unescape("Vorwahl fehlt"),
	error_number_missing: unescape("Nummer fehlt"),
	fax: unescape("Fax"),
	fieldContentCannotBeChanged: unescape("Feldinhalt kann nicht ge%E4ndert werden!"),
	function_not_available: unescape("Diese Funktion steht momentan Leider nicht zur Verf%FCgung. Bitte kontaktieren Sie den/die Administrator/en."),
	illegalDayValue: unescape("Eingabe Tag -> illegaler Wert "),
	invalidToken: unescape("ung%FCltiges Zeichen"),
	loading: unescape("l%E4dt..."),
	phone: unescape("Telefon"),
	please_wait: unescape("bitte warten..."),
	promptChangePrivateToCompany: unescape("Privatkontakt unwiderruflich in Firma umwandeln"),
	promptEmail: unescape("Bitte Email-Adresse angeben"),
	promptLastName: unescape("Bitte Nachname angeben"),
	promptPhonePrefix: unescape("Bitte Vorwahl angeben"),
	promptPhoneNumber: unescape("Bitte Telefonnummer angeben"),
	promptSalutation: unescape("Bitte Anrede angeben"),
	promptType: unescape("Bitte Typ angeben"),
	mandatoryValuesMissing: unescape("nicht alle Pflichtfelder ausgef%FCllt"),
	max255chars: unescape("Pro Feld bitte max. 255 Zeichen eingeben."),
	min_character_count: unescape("Minimale Zeichenanzahl: "),
	missingFirstName: unescape("Vorname fehlt"),
	missingLastName: unescape("Nachname fehlt"),
	missingStreet: unescape("Strasse fehlt"),
	missingZipcode: unescape("PLZ fehlt"),
	missingCity: unescape("Ort fehlt"),
	missingBirthDate: unescape("Geburtsdatum fehlt"),
	missingEmail: unescape("E-Mail fehlt"),
	missingPhonePrefix: unescape("Tel.-Vorwahl fehlt"),
	missingNumberPrefix: unescape("Tel.-Nr. fehlt"),
	missingValues: unescape("Folgende Werte fehlen:"),
	mobile: unescape("Mobil"),
	noCodeInjection: unescape("Bitte keinen HTML-Code eingeben!"),
	invalidFirstName: unescape("Vorname ung%FCltig"),
	invalidLastName: unescape("Nachname ung%FCltig"),
	invalidStreet: unescape("Strasse ung%FCltig"),
	invalidZipcode: unescape("PLZ ung%FCltig"),
	invalidCity: unescape("Ort ung%FCltig"),
	invalidBirthDate: unescape("Geburtsdatum ung%FCltig"),
	invalidEmail: unescape("E-Mail ung%FCltig"),
	invalidPhonePrefix: unescape("Tel.-Vorwahl ung%FCltig"),
	invalidNumberPrefix: unescape("Tel.-Nr. ung%FCltig"),
	isNotAnInteger: unescape("ist keine GANZE ZAHL"),
	isNotADecimalValue: unescape("ist kein DEZIMALWERT"),
	start_synchronisation: unescape("starte Synchronisierung..."),
	sumOfPercentages: unescape("Summe Prozente")
};

var ojsI18n_EN = {
	function_not_available: unescape("This function is not available at the moment. Please contact the administrator(s)."),
	loading: unescape("loading..."),
	please_wait: unescape("please wait..."),
	start_synchronisation: unescape("start synchronisation..."),
	min_character_count: unescape("minimum character count: ")
};

var ojsI18n = ojsI18n_DE;