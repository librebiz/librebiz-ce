/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 19-Dec-2006 15:21:56 
 * 
 */
package com.osserp.core.sales;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.User;

import com.osserp.core.BusinessCaseManager;
import com.osserp.core.requests.Request;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesManager extends BusinessCaseManager {

    /**
     * Writes sale values to persistent storage
     * @param sales
     */
    void save(Sales sales);

    /**
     * Updates projects name
     * @param user who updates the name
     * @param sales
     * @param name
     * @throws ClientException if name is not set or related project not exists
     */
    void setName(User user, Sales sales, String name) throws ClientException;

    /**
     * Provides the account balance of given sale
     * @param sales
     * @return account balance
     */
    Double getAccountBalance(Sales sales);

    /**
     * Provides a list with every project status one time that exists
     * @return statusList
     */
    List<Long> getStatusList();

    /**
     * Sets the manager substitute for this project
     * @param user
     * @param sales
     * @param managerId of the selected manager
     */
    void setManagerSubstitute(User user, Sales sales, Long managerId);

    /**
     * Sets the observer for this project
     * @param user
     * @param sales
     * @param observerId
     */
    void setObserver(User user, Sales sales, Long observerId);

    /**
     * Sets the salesman for this project
     * @param user
     * @param sales
     * @param salesId of the selected salesman
     */
    void setCoSales(User user, Sales sales, Long salesId);

    /**
     * Changes website reference status
     * @param sales
     */
    void changeWebsiteReferenceStatus(Sales sales);

    /**
     * Resets request status by removing all objects created in sales context.
     * Use with care.
     * @param user
     * @param sales
     * @return the request with previous status
     * @throws ClientException if user has no permission or other error
     */
    Request resetRequestStatus(User user, Sales sales) throws ClientException;
}
