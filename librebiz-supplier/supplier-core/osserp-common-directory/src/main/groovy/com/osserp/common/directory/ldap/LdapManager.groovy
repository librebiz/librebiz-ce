/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
interface LdapManager {

    /**
     * Provides the configuration metadata
     * @return config
     */
    LdapConfig getConfig()

    /**
     * Provides the displayable attribute names of users
     * @return displayAttributes
     */
    List<String> getUserDisplayAttributes()

    /**
     * Indicates that attribute name is stored as array
     * @return true if attributes of given name are stored as array
     */
    boolean isUserArrayAttribute(String attributeName)

    /**
     * Attempts to retrieve client object by name and password
     * @param client name	
     * @param password in clear text	
     * @return client
     * @throws LdapException if client not exists or password does not match	
     */
    Map clientLogin(String client, String password) throws LdapException

    /**
     * Creates an id suggestion for a new system user
     * @param min uidnumber 
     * @param max uidnumber
     * @param client if client dn required
     * @return next available id
     */
    String createUserIdSuggestion(String min, String max, String client)

    /**
     * Creates a new user
     * @param atts required attributes: 'uid', 'uidnumber', 'employeetype', 
     * 'cn', 'sn', 'gecos', 'osserpcategory', 'displayname' and 'description'  
     * @param client if client dn required
     * @return user or atts with 'errors' property set
     */
    Map createUser(Map atts, String client)

    /**
     * Deletes a user
     * @param user
     */
    void deleteUser(Map user)

    /**
     * Adds or changes an email address
     * @param user
     * @param address
     * @return updated user
     */
    Map addMailAddress(Map user, String address) throws LdapException

    /**
     * Adds an email alias
     * @param user
     * @param alias
     * @return updated user
     */
    Map addMailAlias(Map user, String alias)

    /**
     * Adds an email alias
     * @param user
     * @param alias
     * @return updated user
     */
    Map removeMailAlias(Map user, String alias)

    /**
     * Checks if a user with uid number already exists
     * @param uidNumber of the user
     * @param client if client based dn required
     * @return true if uid number already assigned
     */
    boolean userIdAssigned(String uidNumber, String client)

    /**
     * Finds a user by uid
     * @param uid
     * @param client if client based dn required
     * @return user or empty map
     */
    Map lookupUser(String uid, String client)

    /**
     * Loads a user by dn
     * @param dn
     * @return user or empty map
     */
    Map lookupUserByDn(String dn)

    /**
     * Finds groups by user
     * @param uid
     * @param client if client based dn required
     * @return groups with member uid
     */
    List<Map> lookupUserGroups(String uid, String client)

    /**
     * Finds permissions by user
     * @param uid
     * @param client if client based dn required
     * @return permissions with member uid
     */
    List<Map> lookupUserPermissions(String uid, String client)

    /**
     * Finds all users
     * @param client if client based dn required
     * @return users
     */
    List<Map> lookupUsers(String client)

    /**
     * Looks up for a user by uid and password
     * @param uid
     * @param password
     * @param client if client based dn required
     * @return user or empty map
     */
    Map login(String uid, String password, String client) throws LdapException

    /**
     * Updates user values
     * @param user
     * @param values
     * @return user with updated values
     * @throws LdapException if value or key invalid
     */
    Map updateUser(Map user, Map values) throws LdapException

    /**
     * Changes the client password tokens
     * @param client
     * @param password
     * @return updated client
     */
    Map changeClientPassword(Map client, String password)

    /**
     * Changes all password tokens of a user
     * @param user
     * @param password
     * @return updated user
     */
    Map changePassword(Map user, String password)

    /**
     * Creates a new internet domain
     * @param name
     * @param mailhost
     * @param client
     * @return new created or unchanged existing domain
     */
    Map createDomain(String name, String mailhost, String client)

    /**
     * Deletes a group (removes ldap entry)
     * @param domain
     */
    void deleteDomain(Map domain)

    /**
     * Updates the mailhost entries of a domain
     * @param domain
     * @param mailhost
     * @param mailrelay (optional), set to 'null' to delete existing
     * @return updated domain
     */
    Map changeDomainMailhosts(Map domain, String mailhost, String mailrelay)

    /**
     * Finds all -client- domains
     * @param client if client based dn required
     * @return domains
     */
    List<Map> lookupDomains(String client)

    /**
     * Finds a domain by name
     * @param name
     * @param client if client based dn required
     * @return domain or empty map
     */
    Map lookupDomain(String name, String client)

    /**
     * Enables/disables maildomain. maildomain is read by mail- & dnsservers 
     * @param domain	
     */
    Map switchMaildomain(Map domain)

    /**
     * Updates a domain value
     * @param domain to update
     * @param name of the ldap property
     * @param value to set
     * @return updated domain	
     */
    Map updateDomain(Map domain, String name, String value)

    /**
     * Creates a new group if not exists
     * @param name
     * @param description
     * @param client if client based dn required
     * @param id if provided by caller
     * @return group new created or -unchanged- already existing
     */
    Map createGroup(String name, String description, String client, Long id)

    /**
     * Deletes a group (removes ldap entry)
     * @param group
     */
    void deleteGroup(Map group)

    /**
     * Provides the name of the default group
     * @param client if client based dn required
     * @return name
     */
    String getDefaultGroupName(String client)

    /**
     * Finds all groups
     * @param client if client based dn required
     * @return groups
     */
    List<Map> lookupGroups(String client)

    /**
     * Finds a group by name
     * @param name
     * @param client if client based dn required
     * @return group or empty map if not exists
     */
    Map lookupGroup(String name, String client)

    /**
     * Loads a group by dn
     * @param dn
     * @return group or empty map
     */
    Map lookupGroupByDn(String dn)

    /**
     * Adds a new member by uid
     * @param group
     * @param uid
     * @return group with updated values
     */
    Map addGroupMember(Map group, String uid)

    /**
     * Removes a member by uid
     * @param group
     * @param uid
     * @return group with updated values
     */
    Map removeGroupMember(Map group, String uid)

    /**
     * Updates group values
     * @param group
     * @param values
     * @return group with updated values
     */
    Map updateGroup(Map group, Map values) throws LdapException

    /**
     * Creates a new permission if not exists
     * @param name
     * @param description
     * @param client if client based dn required
     * @param id if provided by caller
     * @return permission new created or -unchanged- already existing
     */
    Map createPermission(String name, String description, String client, Long id)

    /**
     * Deletes a permission (removes ldap entry)
     * @param permission
     */
    void deletePermission(Map permission)

    /**
     * Finds all permissions
     * @param client if client based dn required
     * @return permissions
     */
    List<Map> lookupPermissions(String client)

    /**
     * Finds a permission by name
     * @param name
     * @param client if client based dn required
     * @return permission or empty map if not exists
     */
    Map lookupPermission(String name, String client)

    /**
     * Adds a new member by uid
     * @param permission
     * @param uid
     * @return group with updated values
     */
    Map addPermissionMember(Map permission, String uid)

    /**
     * Removes a member by uid
     * @param permission
     * @param uid
     * @return permission with updated values
     */
    Map removePermissionMember(Map permission, String uid)

    /**
     * Updates permission values
     * @param permission
     * @param values
     * @return permission with updated values
     */
    Map updatePermission(Map permission, Map values) throws LdapException
}

