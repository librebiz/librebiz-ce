<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="selectionUrl" value="${requestScope.bankAccountSelectionUrl}"/>

<div class="row">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="bankAccountSelection"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="accountKey"><fmt:message key="shortkey" /></th>
                            <th class="accountName"><fmt:message key="name" /></th>
                            <th class="accountIBAN">IBAN</th>
                            <th class="accountBIC">BIC</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.bankAccounts}">
                                <tr>
                                    <td colspan="4">
                                        <span class="error"><fmt:message key="bankAccountRequiredButMissing" /></span> 
                                        <span style="margin-left: 20px;"> <fmt:message key="bankAccountRequiredButMissingHint" /></span>
                                    </td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="account" items="${view.bankAccounts}">
                                    <tr>
                                        <td class="accountKey"><o:out value="${account.shortkey}" /></td>
                                        <td class="accountName"><v:link url="${selectionUrl}/selectBankAccount?id=${account.id}">
                                                <o:out value="${account.name}" />
                                            </v:link></td>
                                        <td class="accountIBAN"><o:out value="${account.bankAccountNumberIntl}" /></td>
                                        <td class="accountBIC"><o:out value="${account.bankIdentificationCodeIntl}" /></td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        
</div>
