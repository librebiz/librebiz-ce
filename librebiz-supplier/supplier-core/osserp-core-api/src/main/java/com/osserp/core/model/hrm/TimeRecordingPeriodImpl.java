/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 12:57:08 PM 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.Month;
import com.osserp.common.PublicHoliday;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateUtil;
import com.osserp.core.Comparators;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordMarker;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordStatus;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingDay;
import com.osserp.core.hrm.TimeRecordingMonth;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.hrm.TimeRecordingYear;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingPeriodImpl extends AbstractEntity
        implements TimeRecordingPeriod {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingPeriodImpl.class.getName());

    private TimeRecordingConfig config = null;
    private Date validFrom = null;
    private Date validTil = null;
    private double carryoverLeave = 0;
    private double extraLeave = 0;
    private double firstYearLeave = 0;
    private double carryoverHours = 0;
    private double carryoverHoursMax = 0;
    private double carryoverHoursIgnorable = 0;
    private Long state = null;

    private List<TimeRecord> recordList = new ArrayList<TimeRecord>();
    private List<TimeRecordMarker> recordMarkerList = new ArrayList<TimeRecordMarker>();
    private List<PublicHoliday> publicHolidays = new ArrayList<PublicHoliday>();
    private List<TimeRecordingYear> supportedYears = new ArrayList<TimeRecordingYear>();
    private TimeRecordingYear selectedYear = null;

    protected TimeRecordingPeriodImpl() {
        super();
    }

    protected TimeRecordingPeriodImpl(
            TimeRecording recording,
            TimeRecordingConfig config,
            Employee user,
            Date whenValid) {
        super(null, recording.getId(), user.getId());
        this.config = config;
        this.validFrom = whenValid;
        if (user.getAddress() != null && user.getAddress().getFederalStateId() != null) {
            state = user.getAddress().getFederalStateId();
        }
    }

    public boolean isCurrent() {
        Date now = DateUtil.getCurrentDate();
        return DateUtil.isDayInInterval(now, validFrom, validTil);
    }

    public TimeRecordingConfig getConfig() {
        return config;
    }

    protected void setConfig(TimeRecordingConfig config) {
        this.config = config;
    }

    public void changeConfig(TimeRecordingConfig config) {
        if (config != null) {
            this.config = config;
            setChangedBy(Constants.SYSTEM_EMPLOYEE);
            setChanged(DateUtil.getCurrentDate());
        }
    }

    public boolean isConfigChangeable() {
        if (log.isDebugEnabled()) {
            log.debug("isConfigChangeable() invoked [currentPeriodSelected="
                    + isCurrent() + ", recordSize="
                    + recordList.size() + "]");
        }
        return isCurrent();
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTil() {
        return validTil;
    }

    public void setValidTil(Date validTil) {
        this.validTil = validTil;
    }

    public double getExtraLeave() {
        return extraLeave;
    }

    public void setExtraLeave(double extraLeave) {
        this.extraLeave = extraLeave;
    }

    public double getFirstYearLeave() {
        return firstYearLeave;
    }

    public void setFirstYearLeave(double firstYearLeave) {
        this.firstYearLeave = firstYearLeave;
    }

    public double getCarryoverLeave() {
        return carryoverLeave;
    }

    public void setCarryoverLeave(double carryoverLeave) {
        this.carryoverLeave = carryoverLeave;
    }

    public double getCarryoverHours() {
        return carryoverHours;
    }

    public void setCarryoverHours(double carryoverHours) {
        this.carryoverHours = carryoverHours;
    }

    public double getCarryoverHoursMax() {
        return carryoverHoursMax;
    }

    public void setCarryoverHoursMax(double carryoverHoursMax) {
        this.carryoverHoursMax = carryoverHoursMax;
    }

    public double getCarryoverHoursIgnorable() {
        return carryoverHoursIgnorable;
    }

    public void setCarryoverHoursIgnorable(double carryoverHoursIgnorable) {
        this.carryoverHoursIgnorable = carryoverHoursIgnorable;
    }

    public List<PublicHoliday> getPublicHolidays() {
        return publicHolidays;
    }

    protected void setPublicHolidays(List<PublicHoliday> publicHolidays) {
        this.publicHolidays = publicHolidays;
    }

    public void synchronizePublicHolidays(List<PublicHoliday> allPublicHoliday) {
        for (Iterator<PublicHoliday> i = publicHolidays.iterator(); i.hasNext();) {
            PublicHoliday next = i.next();
            if (validFrom != null && next.getDate().before(validFrom)) {
                i.remove();
            } else if (validTil != null && validTil.before(next.getDate())) {
                i.remove();
            } else if (state != null && !next.getState().equals(state)) {
                i.remove();
            }
        }
        if (validFrom != null) {
            for (int i = 0, j = allPublicHoliday.size(); i < j; i++) {
                PublicHoliday next = allPublicHoliday.get(i);
                if (validFrom.before(next.getDate())
                        && (validTil == null || validTil.after(next.getDate()))
                        && state != null && next.getState().equals(state)) {
                    addHoliday(next);
                }
            }
        }
    }

    private void addHoliday(PublicHoliday holiday) {
        boolean added = false;
        for (int i = 0, j = publicHolidays.size(); i < j; i++) {
            PublicHoliday next = publicHolidays.get(i);
            if (next.getId().equals(holiday.getId())) {
                added = true;
                break;
            }
        }
        if (!added) {
            this.publicHolidays.add(holiday);
        }
    }

    public List<TimeRecord> getRecords() {
        List<TimeRecord> result = new ArrayList<TimeRecord>();
        Map<Long, TimeRecord> closings = getClosingMap();
        for (int i = 0, j = recordList.size(); i < j; i++) {
            TimeRecord next = recordList.get(i);
            if (next.getType().isStarting()) {
                TimeRecord stopping = closings.get(next.getId());
                if (stopping != null) {
                    next.setClosedBy(stopping);
                }
                next.setPublicHoliday(getPublicHoliday(next.getValue()));
                result.add(next);
            }
        }
        return result;
    }

    public List<TimeRecordMarker> getMarkers() {
        return recordMarkerList;
    }

    private Map<Long, TimeRecord> getClosingMap() {
        Map<Long, TimeRecord> map = new HashMap<Long, TimeRecord>();
        for (int i = 0, j = recordList.size(); i < j; i++) {
            TimeRecord next = recordList.get(i);
            if (next.getClosing() != null) {
                map.put(next.getClosing(), next);
            }
        }
        return map;
    }

    public TimeRecord getLastRecord() {
        if (config != null
                && config.isRecordingHours()
                && !this.recordList.isEmpty()) {
            Date now = DateUtil.getCurrentDate();
            for (int i = 0, j = recordList.size(); i < j; i++) {
                TimeRecord next = recordList.get(i);
                if (next.getValue().before(now)) {
                    return next;
                }
            }
        }
        return null;
    }

    public TimeRecord getLastCreatedRecord() {
        TimeRecord result = null;
        for (int i = 0, j = recordList.size(); i < j; i++) {
            TimeRecord next = recordList.get(i);
            if (result == null || (result.getId() < next.getId())) {
                result = next;
            }
        }
        return result;
    }

    public void addRecord(
            Employee user,
            TimeRecordType type,
            TimeRecordStatus status,
            Date value,
            String note,
            boolean systemTime,
            Long terminalId)
            throws ClientException {

        TimeRecordImpl obj = new TimeRecordImpl(
                this,
                type,
                status,
                user,
                value,
                note,
                systemTime,
                terminalId);
        this.recordList.add(obj);
        CollectionUtil.sort(recordList, Comparators.createTimeRecordComparator(true));
    }

    public void addRecord(
            Long user,
            TimeRecordType type,
            TimeRecordStatus status,
            Date value,
            String note,
            boolean systemTime,
            Long terminalId)
            throws ClientException {

        TimeRecordImpl obj = new TimeRecordImpl(
                this,
                type,
                status,
                user,
                value,
                note,
                systemTime,
                terminalId);
        this.recordList.add(obj);
        CollectionUtil.sort(recordList, Comparators.createTimeRecordComparator(true));
    }

    public void addMarker(
            Employee user,
            TimeRecordMarkerType type,
            Date date,
            Double value,
            String note) {

        TimeRecordMarkerImpl obj = new TimeRecordMarkerImpl(
                type,
                getId(),
                user,
                date,
                value,
                note);
        this.recordMarkerList.add(obj);
        CollectionUtil.sort(recordMarkerList, Comparators.createTimeRecordMarkerComparator(true));
    }

    public TimeRecord createCorrection(Employee user, TimeRecord record, Date startDate, Date stopDate, String note, TimeRecordStatus status)
            throws ClientException {
        if (record == null) {
            throw new IllegalArgumentException("time record must not be null");
        }
        for (Iterator<TimeRecord> i = recordList.iterator(); i.hasNext();) {
            TimeRecord next = i.next();
            if (next.getId() != null && next.getId().equals(record.getId())) {
                TimeRecordCorrection correctionRecord = next.createCorrection(user, startDate, note, status);
                if (stopDate != null) {
                    TimeRecord stopRecord = next.getClosedBy();
                    if (stopRecord != null) {
                        correctionRecord.setStopValue(stopRecord.getValue());
                        stopRecord.createCorrection(user, stopDate, note, status);
                    } else {
                        log.warn("createCorrection() stop date available but no stop record found");
                    }
                }
                return correctionRecord;
            }
        }
        return null;
    }

    public boolean createClosingsIfRequired(TimeRecordType stopType, TimeRecordStatus status) {
        boolean closingCreated = false;
        if (stopType != null && status != null) {
            List<TimeRecord> records = getRecords();
            for (int i = 0, j = records.size(); i < j; i++) {
                TimeRecord next = records.get(i);
                if (next.getType().isStarting()
                        && next.getClosedBy() == null
                        && !DateUtil.isToday(next.getValue())
                        && DateUtil.isLaterDay(DateUtil.getCurrentDate(), next.getValue())) {

                    if (log.isDebugEnabled()) {
                        log.debug("createClosingsIfRequired() found open record [id=" + next.getId() + "]");
                    }
                    TimeRecordImpl obj = new TimeRecordImpl(
                            this,
                            stopType,
                            status,
                            next);
                    this.recordList.add(obj);
                    closingCreated = true;
                }
            }
        }
        if (closingCreated) {
            CollectionUtil.sort(recordList, Comparators.createTimeRecordComparator(true));
        } else {
            if (log.isDebugEnabled()) {
                log.debug("createClosingsIfRequired() done, nothing to do");
            }
        }
        return closingCreated;
    }

    public boolean removeRecord(Long id) {
        TimeRecord removed = deleteRecord(id);
        if (removed != null) {
            List<TimeRecord> closings = getClosings(removed);
            for (Iterator<TimeRecord> i = closings.iterator(); i.hasNext();) {
                TimeRecord closing = i.next();
                this.deleteRecord(closing.getId());
            }
            return true;
        }
        return false;
    }

    private TimeRecord deleteRecord(Long id) {
        for (Iterator<TimeRecord> i = recordList.iterator(); i.hasNext();) {
            TimeRecord next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                if (log.isDebugEnabled()) {
                    log.debug("deleteRecord() found and deleted [id=" + id + "]");
                }
                return next;
            }
        }
        return null;
    }

    public boolean removeMarker(Long id) {
        for (Iterator<TimeRecordMarker> i = recordMarkerList.iterator(); i.hasNext();) {
            TimeRecordMarker next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                if (log.isDebugEnabled()) {
                    log.debug("deleteMarker() found and deleted [id=" + id + "]");
                }
                return true;
            }
        }
        return false;
    }

    public List<TimeRecord> getClosings(TimeRecord starting) {
        List<TimeRecord> stops = new ArrayList<TimeRecord>();
        for (int i = 0, j = recordList.size(); i < j; i++) {
            TimeRecord next = recordList.get(i);
            if (!next.getType().isStarting()
                    && next.getClosing() != null
                    && next.getClosing().equals(starting.getId())) {
                stops.add(next);
            }
        }
        return stops;
    }

    public List<TimeRecordingYear> getSupportedYears() {
        if (supportedYears.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("getSupportedYears() initially invoked [recording=" + getReference()
                        + ", period=" + getId() + "]");
            }
            Date vt = validTil;
            if (vt == null) {
                vt = DateUtil.addYears(DateUtil.getCurrentDate(), 1);
            }
            int startYear = DateUtil.getYear(validFrom);
            int nextYear = startYear;
            int lastYear = DateUtil.getYear(vt);
            int minutesLost = 0;
            TimeRecordingYear prev = null;
            while (nextYear <= lastYear) {
                int carryoverLeaveMinutes = 0;
                int annualLeaveMinutes = 0;
                int carryoverMinutes = 0;
                if (nextYear == startYear) {
                    carryoverLeaveMinutes = getCarryoverMinutesLeave();
                    annualLeaveMinutes = getFirstYearMinutesLeave();
                    carryoverMinutes = getCarryoverMinutes();
                } else if (prev != null) {
                    carryoverLeaveMinutes = prev.getRemainingMinutesLeave();
                    annualLeaveMinutes = getAnnualMinutesLeave() + getExtraMinutesLeave();
                    carryoverMinutes = prev.getCarryoverMinutes();
                }
                TimeRecordingYearVO obj = new TimeRecordingYearVO(this, nextYear, carryoverMinutes, carryoverLeaveMinutes, annualLeaveMinutes, minutesLost);
                carryoverMinutes = obj.getCarryoverMinutes();
                minutesLost = obj.getMinutesLost();
                this.supportedYears.add(obj);
                prev = obj;
                nextYear++;
            }
        } else if (log.isDebugEnabled()) {
            log.debug("getSupportedYears() already initialized [recording=" + getReference()
                    + ", period=" + getId() + "]");
        }
        return supportedYears;
    }

    public TimeRecordingYear getSelectedYear() {
        return selectedYear;
    }

    public final void selectCurrentMonth() {
        if (log.isDebugEnabled()) {
            log.debug("selectCurrentMonth() invoked [recording=" + getReference()
                    + ", period=" + getId() + "]");
        }
        long start = System.currentTimeMillis();
        if (!isCurrent()) {
            this.selectMonth(null);
        } else {
            this.selectMonth(DateUtil.createMonth());
        }
        if (log.isDebugEnabled()) {
            log.debug("selectCurrentMonth() done [recording=" + getReference()
                    + ", period=" + getId()
                    + ", duration=" + (System.currentTimeMillis() - start)
                    + "ms]");
        }
    }

    public final void selectMonth(Month month) {
        List<TimeRecordingYear> list = getSupportedYears();
        if (log.isDebugEnabled()) {
            log.debug("selectMonth() invoked [recording=" + getReference()
                    + ", period=" + getId()
                    + ", supportedYears=" + list.size()
                    + ", month=" + (month == null ? "null" : (month.getMonth() + ", year=" + month.getYear()))
                    + "]");
        }
        if (month != null) {
            for (int i = 0, j = list.size(); i < j; i++) {
                TimeRecordingYear next = list.get(i);
                if (next.getYear().equals(month.getYear())) {
                    this.selectedYear = next;
                    this.selectedYear.selectMonth(month);
                    if (log.isDebugEnabled()) {
                        log.debug("selectMonth() done [year=" + selectedYear.getYear()
                                + ", month="
                                + (selectedYear.getSelectedMonth() == null ? "null" : selectedYear.getSelectedMonth().getMonth())
                                + "]");
                    }
                    break;
                } else if (log.isDebugEnabled()) {
                    log.debug("selectMonth() ignoring year [required=" + month.getYear() + ", found=" + next.getYear() + "]");
                }
            }
        } else {
            if (!list.isEmpty()) {
                TimeRecordingYear next = list.get(list.size() - 1);
                this.selectedYear = next;
                this.selectedYear.selectMonth(null);
                if (log.isDebugEnabled()) {
                    log.debug("selectMonth() done [year=" + selectedYear.getYear()
                            + ", month="
                            + (selectedYear.getSelectedMonth() == null ? "null" : selectedYear.getSelectedMonth().getMonth())
                            + "]");
                }
            } else if (log.isDebugEnabled()) {
                log.debug("selectMonth() found period without years [recording=" + getReference() + ", period=" + getId() + "]");
            }
        }
    }

    public TimeRecordingDay fetchDate(Date date) {
        Month month = DateUtil.createMonth(date);
        TimeRecordingMonth trm = fetchMonth(month);
        if (trm != null) {
            return trm.fetchDate(date);
        }
        return null;
    }

    public TimeRecordingMonth fetchMonth(Month month) {
        List<TimeRecordingYear> list = getSupportedYears();
        for (int i = 0, j = list.size(); i < j; i++) {
            TimeRecordingYear next = list.get(i);
            TimeRecordingMonth trm = next.fetchMonth(month);
            if (trm != null) {
                return trm;
            }
        }
        return null;
    }

    protected List<TimeRecord> getRecordList() {
        return recordList;
    }

    protected void setRecordList(List<TimeRecord> recordList) {
        this.recordList = recordList;
    }

    protected List<TimeRecordMarker> getRecordMarkerList() {
        return recordMarkerList;
    }

    protected void setRecordMarkerList(List<TimeRecordMarker> recordMarkerList) {
        this.recordMarkerList = recordMarkerList;
    }

    protected PublicHoliday getPublicHoliday(Date date) {
        for (int i = 0, j = publicHolidays.size(); i < j; i++) {
            PublicHoliday next = publicHolidays.get(i);
            try {
                if (DateUtil.isSameDay(date, next.getDate())) {
                    return next;
                }
            } catch (Exception e) {
                log.error("Exception while checking if a date is a public holiday! [Date: " + date + ", PublicHoliday Date: " + next.getDate()
                        + "], Exception: " + e.toString());
            }
        }
        return null;
    }

    protected int getAnnualMinutesLeave() {
        double annualMinutesLeave = config.getAnnualLeave() * this.config.getHoursDaily() * 60;
        return (int) annualMinutesLeave;
    }

    protected int getCarryoverMinutesLeave() {
        double carryoverMinutesLeave = carryoverLeave * this.config.getHoursDaily() * 60;
        return (int) carryoverMinutesLeave;
    }

    protected int getCarryoverMinutes() {
        double carryoverMinutes = carryoverHours * 60;
        return (int) carryoverMinutes;
    }

    protected int getExtraMinutesLeave() {
        double extraMinutesLeave = extraLeave * this.config.getHoursDaily() * 60;
        return (int) extraMinutesLeave;
    }

    protected int getFirstYearMinutesLeave() {
        double firstYearMinutesLeave = firstYearLeave * this.config.getHoursDaily() * 60;
        return (int) firstYearMinutesLeave;
    }

    public Long getCountry() {
        return Constants.GERMANY;
    }

    public Long getState() {
        return state;
    }

    public void setState(Long state) {
        this.state = state;
    }
}
