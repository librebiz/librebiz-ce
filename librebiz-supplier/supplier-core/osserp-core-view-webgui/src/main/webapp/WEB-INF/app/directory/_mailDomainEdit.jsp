<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="domain" value="${view.bean}" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><o:out value="${domain.name}" /></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <v:form name="mailDomainForm" url="${view.baseLink}/save">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                <fmt:message key="domainnameLabel" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="name" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                <fmt:message key="provider" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="provider" />
                        </div>
                    </div>
                </div>

                <v:date name="domainCreated" styleClass="form-control" label="registered" picker="true" />

                <v:date name="domainExpires" styleClass="form-control" label="til" picker="true" />

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                SMTP Hostname
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="smtpServerName" value="${domain.smtpServerName}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                MX Hostname
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="hostMx" value="${domain.hostMx}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                MX IP Address
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="hostMxIp" value="${domain.hostMxIp}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                Secondary MX
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="hostSecondaryMx" value="${domain.hostSecondaryMx}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                Secondary MX IP Address
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="hostSecondaryMxIp" value="${domain.hostSecondaryMxIp}" />
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4"> </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/disableEditMode"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </v:form>
        </div>
    </div>
</div>
