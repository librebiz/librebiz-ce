/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Aug 2, 2016
 *
 */
package com.osserp.gui.admin.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.products.ManufacturerManager
import com.osserp.core.products.ProductGroup
import com.osserp.core.products.ProductGroupManager

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ManufacturerConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ManufacturerConfigView.class.getName())
    
    List<ProductGroup> productGroups = []
    ProductGroup selectedProductGroup = null

    ManufacturerConfigView() {
        super()
        providesCustomNavigation()
        headerName = 'manufacturer'
        permissions = 'manufacturer_edit,executive'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            productGroups = productGroupManager.getList()
        }
    }
    
    @Override
    protected void createCustomNavigation() {
        if (selectedProductGroup) {
            if (editMode) {
                nav.defaultNavigation = [ selectExitLink, homeLink ]
            } else {
                nav.defaultNavigation = [ productGroupResetLink, createLink, homeLink ]
            }
            nav.listNavigation = nav.defaultNavigation
            nav.currentNavigation = nav.defaultNavigation
        } else {
            nav.defaultNavigation = [exitLink, homeLink]
        }
    }
    
    MenuItem getProductGroupResetLink() {
        navigationLink("/${context.name}/selectProductGroup", 'backIcon', 'backToLast')
    }

    @Override
    void reload() {
        if (selectedProductGroup) {
            list = manufacturerManager.findByGroup(selectedProductGroup.id)
            bean = null
            logger.debug("reload: done [productGroup=${selectedProductGroup.id}, manufacturers=${list.size()}]")
        } else {
            list = []
            bean = null
            logger.debug('reload: reset done while productGroup not selected')
        }
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            enableEditMode()
        } else {
            disableEditMode()
            disableCreateMode()
        }
    }

    void selectProductGroup() {
        Long id = getLong('id')
        selectedProductGroup = id ? productGroups.find { it.id == id } : null
        reload()
        if (selectedProductGroup && !list) {
            // no manufacturer created before
            enableCreateMode()
        } else {
            disableCreateMode()
        }
    }

    @Override
    void save() {
        String name = getString('name')
        if (!name) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }
        if (createMode) {
            manufacturerManager.add(name, selectedProductGroup.id)
            disableCreateMode()
        } else {
            manufacturerManager.rename(bean.id, name)
            disableEditMode()
        }
        reload()
    }

    protected ManufacturerManager getManufacturerManager() {
        getService(ManufacturerManager.class.getName())
    }

    protected ProductGroupManager getProductGroupManager() {
        getService(ProductGroupManager.class.getName())
    }
}
