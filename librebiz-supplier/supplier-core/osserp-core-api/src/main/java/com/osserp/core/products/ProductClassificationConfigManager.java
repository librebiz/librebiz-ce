/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 28, 2009 1:10:08 PM 
 * 
 */
package com.osserp.core.products;

import java.util.List;

import com.osserp.common.ClientException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductClassificationConfigManager {

    /**
     * Provides available configurations
     * @return list
     */
    List<ProductClassificationConfig> getClassificationConfigsDisplay();

    /**
     * Provides a categories
     * @return category
     */
    ProductCategoryConfig getCategory(Long id);

    /**
     * Provides all available categories
     * @return categories
     */
    List<ProductCategoryConfig> getCategories();

    /**
     * Provides all product categories as immutable
     * @return categories
     */
    List<ProductClassificationEntity> getCategoriesDisplay();

    /**
     * Provides a groups
     * @return group
     */
    ProductGroupConfig getGroup(Long id);

    /**
     * Provides all available groups
     * @return groups
     */
    List<ProductGroupConfig> getGroups();

    /**
     * Provides all product groups as immutable
     * @return groups
     */
    List<ProductClassificationEntity> getGroupsDisplay();

    /**
     * Provides a type
     * @return type
     */
    ProductTypeConfig getType(Long id);

    /**
     * Provides all available types
     * @return types
     */
    List<ProductTypeConfig> getTypes();

    /**
     * Removes a type
     * @param type
     * @throws ClientException if type is referenced
     */
    void removeType(ProductTypeConfig type) throws ClientException;

    /**
     * Provides all product types as immutable (e.g. used for selection lists)
     * @return types
     */
    List<ProductClassificationEntity> getTypesDisplay();

    /**
     * Adds group to type
     * @param type
     * @param group to add
     */
    void addGroup(ProductTypeConfig type, ProductClassificationEntity group);

    /**
     * Adds group to type
     * @param type
     * @param group to remove
     * @throws ClientException if group is referenced
     */
    void removeGroup(ProductTypeConfig type, ProductGroupConfig group) throws ClientException;

    /**
     * Moves a group to next or previous list position
     * @param type
     * @param group to move
     * @param down if group should be moved next
     */
    void moveGroup(ProductTypeConfig type, ProductGroupConfig group, boolean down);

    /**
     * Adds a category to a group
     * @param group
     * @param category to add
     * @throws ClientException if group/category relation is ambiguous
     */
    void addCategory(ProductGroupConfig group, ProductClassificationEntity category) throws ClientException;

    /**
     * Adds a category to a group
     * @param group
     * @param category to remove
     * @throws ClientException if category is referenced
     */
    void removeCategory(ProductGroupConfig group, ProductCategoryConfig category) throws ClientException;

    /**
     * Moves a category to next or previous list position
     * @param type
     * @param category to move
     * @param down if category should be moved next
     */
    void moveCategory(ProductGroupConfig group, ProductCategoryConfig category, boolean down);

    /**
     * Assigns a numberRange to a classification
     * @param obj classification entity (group or category supported)
     * @param numberRangeId
     */
    void assignNumberRange(ProductClassificationEntity obj, Long numberRangeId);
}
