/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.service.projects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.util.DateUtil;
import com.osserp.core.BusinessCase;
import com.osserp.core.Options;
import com.osserp.core.dao.ProjectSuppliers;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordDisplayItem;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.finance.RecordType;
import com.osserp.core.projects.ProjectSupplier;
import com.osserp.core.projects.ProjectSupplierManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.service.impl.AbstractCoreService;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectSupplierManagerImpl extends AbstractCoreService implements ProjectSupplierManager {

    private ProjectSuppliers projectSuppliers = null;
    private RecordSearch recordSearch = null;
    private Suppliers suppliers = null;
    private Set<String> uniqueGroupNames = new HashSet<>();

    /**
     * @param options
     * @param projectSuppliers
     * @param recordSearch
     * @param suppliers
     */
    protected ProjectSupplierManagerImpl(
            OptionsCache options,
            ProjectSuppliers projectSuppliers,
            RecordSearch recordSearch,
            Suppliers suppliers) {
        super(options);
        this.projectSuppliers = projectSuppliers;
        this.recordSearch = recordSearch;
        this.suppliers = suppliers;
    }

    public List<String> getGroupNames() {
        List<String> result = new ArrayList<>(projectSuppliers.findGroupNames());
        Collections.sort(result);
        return result;
    }

    public ProjectSupplier getById(Long id) {
        return load(projectSuppliers.getProjectSupplier(id));
    }

    public ProjectSupplier create(
            Employee user,
            BusinessCase businessCase,
            Long supplierId,
            String groupName,
            Date deliveryDate) throws ClientException {

        if (supplierId == null || supplierId == 0L || !suppliers.exists(supplierId)) {
            throw new IllegalStateException("Supplier must exist: " + supplierId);
        }
        if (groupName == null) {
            throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
        }
        Supplier supplier = (Supplier) suppliers.find(supplierId);
        List<ProjectSupplier> list = getSuppliers(businessCase);
        if (uniqueGroupNames.contains(groupName) || uniqueGroupNames.contains(groupName.toLowerCase())) {
            for (int i = 0, j = list.size(); i < j; i++) {
                ProjectSupplier ps = list.get(i);
                if (ps.getGroupName().equalsIgnoreCase(groupName)) {
                    if (ps.getSupplier().getId().equals(supplier.getId())) {
                        return ps;
                    }
                    ps.update(user.getId(), supplier);
                    projectSuppliers.save(ps);
                    return ps;
                }
            }
        }
        return projectSuppliers.create(user, businessCase, supplier, groupName, deliveryDate);
    }

    public void remove(Employee user, ProjectSupplier supplier, boolean includePurchaseOrder) throws ClientException {
        projectSuppliers.remove(supplier, includePurchaseOrder);
    }

    public List<ProjectSupplier> getSuppliers(BusinessCase businessCase) {
        Long projectId = getProjectId(businessCase);
        removeObsolete(projectId, assignUnassigned(projectId));
        List<ProjectSupplier> list = projectSuppliers.getSuppliers(projectId);
        for (int i = 0, j = list.size(); i < j; i++) {
            ProjectSupplier ps = list.get(i);
            if (ps.getRecordTypeId() != null) {
                ps.setVoucherType((RecordType) getOption(Options.RECORD_TYPES, ps.getRecordTypeId()));
            }
        }
        return list;
    }

    private List<RecordDisplay> assignUnassigned(Long projectId) {
        List<RecordDisplay> purchaseRecords = recordSearch.findPurchases(projectId);
        for (int i = 0, j = purchaseRecords.size(); i < j; i++) {
            RecordDisplay pr = purchaseRecords.get(i);
            assignVoucher(pr);
        }
        return purchaseRecords;
    }

    private ProjectSupplier assignVoucher(RecordDisplay voucher) {
        List<ProjectSupplier> supplierList = projectSuppliers.getSuppliers(voucher.getSales());
        for (int i = 0, j = supplierList.size(); i < j; i++) {
            ProjectSupplier ps = supplierList.get(i);
            if (ps.getSupplier().getId().equals(voucher.getContactId())) {
                if (ps.getRecordId() == null || ps.getRecordId() == 0L) {
                    ps.assignVoucher(voucher.getId(), voucher.getType());
                    projectSuppliers.save(ps);
                    return update(ps, voucher);
                }
                if (ps.getRecordId() != null
                        && ps.getRecordId().equals(voucher.getId())
                        && ps.getRecordTypeId().equals(voucher.getType())) {
                    return update(ps, voucher);
                }
            }
        }
        ProjectSupplier result = null;
        Supplier supplier = (Supplier) suppliers.find(voucher.getContactId());
        if (supplier != null) {
            String groupName = null;
            Date deliveryDate = null;
            if (!voucher.getItems().isEmpty()) {
                RecordDisplayItem item = voucher.getItems().get(0);
                groupName = getOptionName(Options.PRODUCT_GROUPS, item.getGroupId());
                deliveryDate = item.getDelivery();
            }
            result = projectSuppliers.createDefault(
                    supplier,
                    voucher.getCreated(),
                    voucher.getCreatedBy(),
                    voucher.getSales(),
                    voucher.getId(),
                    voucher.getType(),
                    groupName,
                    deliveryDate);
        }
        return result;
    }

    private ProjectSupplier update(ProjectSupplier ps, RecordDisplay voucher) {
        if (!voucher.getItems().isEmpty()) {
            RecordDisplayItem item = voucher.getItems().get(0);
            if (ps.getGroupName() == null || ps.getDeliveryDate() == null ||
                    (ps.getDeliveryDate() != null
                    && !DateUtil.isSameDay(ps.getDeliveryDate(), item.getDelivery()))) {
                String groupName = getOptionName(Options.PRODUCT_GROUPS, item.getGroupId());
                ps.update(
                        voucher.getCreatedBy(),
                        ps.getGroupName() == null ? groupName : ps.getGroupName(),
                        item.getDelivery(),
                        ps.getMountingDate(), // keep manual input
                        ps.getMountingDays(),
                        ps.getStatus());
                projectSuppliers.save(ps);
            }
        }
        return ps;
    }

    private void removeObsolete(Long projectId, List<RecordDisplay> purchaseRecords) {
        List<ProjectSupplier> supplierList = projectSuppliers.getSuppliers(projectId);
        for (Iterator<ProjectSupplier> iterator = supplierList.iterator(); iterator.hasNext();) {
            ProjectSupplier ps = iterator.next();
            if (RecordType.PURCHASE_ORDER.equals(ps.getRecordTypeId())) {
                for (int i = 0, j = purchaseRecords.size(); i < j; i++) {
                    RecordDisplay rd = purchaseRecords.get(i);
                    if (RecordType.PURCHASE_INVOICE.equals(rd.getType())
                            && isSet(rd.getReference()) && rd.getReference().equals(ps.getRecordId())) {
                        projectSuppliers.remove(ps, false);
                        iterator.remove();
                    }
                }
            }
        }
    }

    public ProjectSupplier update(
            Employee user,
            ProjectSupplier supplier,
            String groupName,
            Date deliveryDate,
            Date mountingDate,
            Double mountingDays,
            Long status) throws ClientException {
        if (groupName == null) {
            throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
        }
        ProjectSupplier obj = projectSuppliers.getProjectSupplier(supplier.getId());
        obj.update(user.getId(), groupName, deliveryDate, mountingDate, mountingDays, status);
        projectSuppliers.save(obj);
        return projectSuppliers.getProjectSupplier(supplier.getId());
    }

    private ProjectSupplier load(ProjectSupplier ps) {
        Record voucher = recordSearch.findRecord(ps.getRecordId(), ps.getRecordTypeId());
        if (voucher != null) {
            ps.setVoucher(voucher);
        }
        return ps;
    }

    private Long getProjectId(BusinessCase businessCase) {
        return (businessCase instanceof Sales)
                ? ((Sales) businessCase).getRequest().getRequestId()
                        : businessCase.getPrimaryKey();
    }
}
