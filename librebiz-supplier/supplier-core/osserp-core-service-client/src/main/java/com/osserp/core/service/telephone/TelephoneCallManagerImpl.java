/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 13, 2010 1:17:59 PM 
 * 
 */
package com.osserp.core.service.telephone;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.DateUtil;
import com.osserp.core.contacts.ContactSearch;
import com.osserp.core.dao.telephone.TelephoneCalls;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.telephone.TelephoneCall;
import com.osserp.core.telephone.TelephoneCallManager;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneCallManagerImpl extends AbstractService implements TelephoneCallManager {
    private static Logger log = LoggerFactory.getLogger(TelephoneCallManagerImpl.class.getName());

    private TelephoneCalls telephoneCallsDao = null;
    private ContactSearch contactSearch = null;

    public TelephoneCallManagerImpl(TelephoneCalls telephoneCallsDao, ContactSearch contactSearch) {
        super();
        this.telephoneCallsDao = telephoneCallsDao;
        this.contactSearch = contactSearch;
    }

    public List<TelephoneCall> find(String uid) {
        if (log.isDebugEnabled()) {
            log.debug("find(" + uid + ") invoked ...");
        }
        if (uid != null) {
            return telephoneCallsDao.find(uid);
        }
        return new ArrayList<TelephoneCall>();
    }

    public List<TelephoneCall> find(String uid, Date start, Date end) {
        if (log.isDebugEnabled()) {
            log.debug("find(" + uid + ", " + start + ", " + end + ") invoked ...");
        }
        if (end != null) {
            end = DateUtil.addDays(end, 1);
        }
        List<TelephoneCall> list = find(uid);
        if (start != null && end != null) {
            List<TelephoneCall> tmpList = new ArrayList<TelephoneCall>();
            for (Iterator<TelephoneCall> iterator = list.iterator(); iterator.hasNext();) {
                TelephoneCall next = iterator.next();
                if (next.getStartedAt().after(start) && next.getStartedAt().before(end)) {
                    tmpList.add(next);
                }
            }
            list = tmpList;
        } else if (start != null) {
            List<TelephoneCall> tmpList = new ArrayList<TelephoneCall>();
            for (Iterator<TelephoneCall> iterator = list.iterator(); iterator.hasNext();) {
                TelephoneCall next = iterator.next();
                if (next.getStartedAt().after(start)) {
                    tmpList.add(next);
                }
            }
            list = tmpList;
        } else if (end != null) {
            List<TelephoneCall> tmpList = new ArrayList<TelephoneCall>();
            for (Iterator<TelephoneCall> iterator = list.iterator(); iterator.hasNext();) {
                TelephoneCall next = iterator.next();
                if (next.getStartedAt().before(end)) {
                    tmpList.add(next);
                }
            }
            list = tmpList;
        }
        return list;
    }
}
