/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 9, 2007 1:59:44 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;

import com.osserp.core.Options;
import com.osserp.core.model.SalesListProductItemVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesListProductItemRowMapper extends SalesListItemRowMapper {

    public SalesListProductItemRowMapper(OptionsCache cache, boolean supportsLastAction) {
        super(cache, supportsLastAction);
    }

    public static RowMapperResultSetExtractor getReader(OptionsCache cache) {
        return new RowMapperResultSetExtractor(new SalesListProductItemRowMapper(cache, false));
    }

    public Object mapRow(ResultSet rs, int row) throws SQLException {
        SalesListProductItemVO vo = new SalesListProductItemVO(
                createItem(rs, row),
                fetchDate(rs, "installation_date"),
                null, // Long packageListId,
                fetchLong(rs, "product_id"),
                fetchString(rs, "product_name"),
                fetchDouble(rs, "quantity"),
                fetchDouble(rs, "delivered"),
                fetchLong(rs, "stock_id"));
        if (vo.getShippingId() != null) {
            vo.setShippingName(fetchOptionValue(Options.SHIPPING_COMPANY_KEYS, vo.getShippingId()));
        }
        return vo;
    }
}
