<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.businessCaseView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="view" value="${sessionScope.businessCaseView}" />
<c:set var="selectList" scope="request" value="${view.employees}" />
<c:set var="selectUrl" scope="request" value="/requests.do" />
<c:set var="selectMethod" scope="request" value="selectEmployee" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="employeeSelection" />
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/requests.do?method=disableEmployeeSelection"/>"><o:img name="backIcon" /></a></li>
            <li><v:link url="/index" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="contractContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <c:choose>
                        <c:when test="${view.observerSelectMode}">
                            <c:set var="selectUrl" scope="request" value="/requests.do" />
                            <c:set var="selectMethod" scope="request" value="selectEmployee" />
                            <c:import url="/pages/employees/shared/employee_selection.jsp" />
                        </c:when>
                        <c:otherwise>
                            <c:import url="/pages/employees/shared/employee_capacity_selection.jsp" />
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
