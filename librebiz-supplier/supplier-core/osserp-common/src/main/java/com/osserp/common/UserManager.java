/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 19, 2004 
 * 
 */
package com.osserp.common;

import java.util.List;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface UserManager {

    /**
     * Provides a list of all available active users
     * @return active
     */
    List<User> findActive();

    /**
     * Returns the related user if user id matches a db entry
     * @see com.osserp.common.User
     * @param userId
     * @return user
     */
    User findById(Long userId);

    /**
     * Checks whether a permission is grant to a user
     * @param userId where we lookup for permission
     * @param permission to check
     * @return permission is grant
     */
    boolean isPermissionGrant(Long userId, String permission);

    /**
     * Changes user values
     * @param user to change
     * @return updated user
     * @throws ClientException if any values are null or username already exists on another id
     */
    User save(User user) throws ClientException;

    /**
     * Sets or removes an user property.
     * @param currentUser user logged in
     * @param user to change
     * @param name
     * @param value
     * @return user with updated values
     */
    User setProperty(User currentUser, User user, String name, String value) throws ClientException;

    /**
     * Switches current state of a boolean property
     * @param currentUser changing property
     * @param user to change
     * @param name of the property
     * @return updated user
     */
    User switchProperty(User currentUser, User user, String name);

    /**
     * Changes users password
     * @param user whose password should be changed
     * @param newPassword of the user
     * @param confirmPassword
     * @return user with changed values
     * @throws ClientException if old password not matching or new password not confirmed
     */
    User changePassword(
            User user,
            String newPassword,
            String confirmPassword)
            throws ClientException;

    /**
     * Changes users locale
     * @param user whose locale should be changed
     * @param locale to set (xx_YY)
     * @return user with changed values
     */
    User changeLocale(User user, String locale);

    /**
     * Provides a list of all public displayable permissions
     * @return permissions
     */
    List<Permission> getPermissions();

    /**
     * Provides all users with given permission
     * @param permission
     * @return members
     */
    List<Option> getPermissionMembers(String permission);

    /**
     * Provides all available startup targets
     * @return startupTargets
     */
    List<ActionTarget> getStartupTargets();
}
