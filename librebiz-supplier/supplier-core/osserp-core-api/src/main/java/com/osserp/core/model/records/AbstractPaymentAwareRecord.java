/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 19:12:48 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.ArithUtil;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordCorrection;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPaymentAwareRecord extends AbstractRecord implements PaymentAwareRecord {
    private static Logger log = LoggerFactory.getLogger(AbstractPaymentAwareRecord.class.getName());
    protected List<Payment> payments = new ArrayList<>();
    protected BigDecimal dueAmount = null;
    private List<RecordCorrection> corrections = new ArrayList<>();

    /**
     * Default constructor required by Serializable
     */
    protected AbstractPaymentAwareRecord() {
        super();
    }

    /**
     * Creates a new payment aware record
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected AbstractPaymentAwareRecord(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                itemsChangeable,
                itemsEditable);
    }

    /**
     * Creates a payment aware record as copy of another record.
     * @param id
     * @param type
     * @param reference
     * @param createdBy
     * @param copyReferenceId indicates if id of reference should be set as reference of new record
     * @param changeable indictes if items should be changeable after create by reference.items 
     */
    protected AbstractPaymentAwareRecord(
            Long id,
            RecordType type,
            Record reference,
            Employee createdBy,
            boolean copyReferenceId,
            boolean changeable) {
        super(id, type, reference, createdBy, copyReferenceId, changeable);
    }

    /**
     * Creates a payment by payment implementing class.
     * @param user
     * @param type
     * @param amount
     * @param paid
     * @param bankAccountId
     * @return new created payment
     */
    protected abstract Payment createPayment(
            Employee user,
            BillingType type,
            BigDecimal amount,
            Date paid,
            Long bankAccountId);

    /**
     * Creates a custom payment
     * @param user
     * @param type
     * @param amount
     * @param paid
     * @param customHeader
     * @param note
     * @param bankAccountId
     * @return new created payment
     */
    protected abstract Payment createCustomPayment(
            Employee user,
            BillingType type,
            BigDecimal amount,
            Date paid,
            String customHeader,
            String note,
            Long bankAccountId);

    public Payment addCustomPayment(
            Employee user,
            BillingType type,
            BigDecimal amount,
            Date paid,
            String customHeader,
            String note,
            Long bankAccountId)
            throws ClientException {

        Payment payment = createCustomPayment(
                user,
                type,
                amount,
                (paid == null ? DateUtil.getCurrentDate() : paid),
                customHeader,
                note,
                bankAccountId);
        payments.add(payment);
        return payment;
    }

    public Payment addPayment(Employee user, BillingType type, BigDecimal amount, Date paid, Long bankAccountId)
            throws ClientException {
        if (isNotSet(amount)) {
            throw new ClientException(ErrorCode.AMOUNT_MISSING);
        }
        Payment payment = createPayment(
                user,
                type,
                amount,
                (paid == null ? DateUtil.getCurrentDate() : paid),
                bankAccountId);
        payments.add(payment);
        return payment;
    }

    public BigDecimal getPaidAmount() {
        BigDecimal p_paid = new BigDecimal(0);
        for (int i = 0, j = payments.size(); i < j; i++) {
            Payment payment = payments.get(i);
            p_paid = p_paid.add(payment.getAmount());
        }
        return p_paid;
    }

    public BigDecimal getPaidTax() {
        BigDecimal p_gross = getPaidAmount();
        BigDecimal p_net = getPaidNetAmount();
        BigDecimal result = p_gross.subtract(p_net);
        return result;
    }

    public BigDecimal getPaidNetAmount() {
        BigDecimal p_sum = new BigDecimal(0);
        for (int i = 0, j = payments.size(); i < j; i++) {
            Payment payment = payments.get(i);
            Double dnet = ArithUtil.div(payment.getAmount().doubleValue(), payment.getTaxRate());
            p_sum = p_sum.add(new BigDecimal(dnet));
        }
        return p_sum;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    protected void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public BigDecimal getDueAmount() {
        if (dueAmount != null) {
            return dueAmount;
        }
        BigDecimal paid = getPaidAmount();
        if (isTaxFree()) {
            BigDecimal da = fetchBigDecimal(amounts.getAmount()).subtract(fetchBigDecimal(paid));
            dueAmount = NumberUtil.round(da, 2);
            if (log.isDebugEnabled()) {
                log.debug("getDueAmount() record is tax free [dueAmount=" + dueAmount + "]");
                
            }
            return dueAmount;
        } else if (log.isDebugEnabled()) {
            log.debug("getDueAmount() record is not tax free");
        }
        if (log.isDebugEnabled() && amounts == null) {
            log.debug("getDueAmount() amounts is NULL!");
        }

        BigDecimal da = fetchBigDecimal(amounts.getGrossAmount()).subtract(fetchBigDecimal(paid));
        if (log.isDebugEnabled()) {
            log.debug("getDueAmount() before invoking round [dueAmount=" + da + "]");
        }
        dueAmount = NumberUtil.round(da, 2);
        if (log.isDebugEnabled()) {
            log.debug("getDueAmount() calculation done [dueAmount=" + dueAmount + "]");
        }
        return dueAmount;
    }

    public RecordCorrection getCorrection() {
        return corrections.isEmpty() ? null : corrections.get(0);
    }

    public List<RecordCorrection> getCorrections() {
        return corrections;
    }

    protected void setCorrections(List<RecordCorrection> corrections) {
        this.corrections = corrections;
    }

    public int getCorrectionCount() {
        return corrections.size();
    }

    public void addCorrection(
            Employee user,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String note) throws ClientException {

        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (isNotSet(zipcode)) {
            throw new ClientException(ErrorCode.ZIPCODE_MISSING);
        }
        if (isNotSet(city)) {
            throw new ClientException(ErrorCode.CITY_MISSING);
        }
        RecordCorrection c = createCorrection(user, name, street, streetAddon, zipcode, city, country, note);
        if (c != null) {
            corrections.add(c);
        }
    }

    public void updateCorrection(
            Employee user,
            Long id,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String note) throws ClientException {
        for (Iterator<RecordCorrection> i = corrections.iterator(); i.hasNext();) {
            RecordCorrection next = i.next();
            if (next.getId().equals(id)) {
                next.setName(name);
                next.setChanged(new Date(System.currentTimeMillis()));
                next.setChangedBy(user.getId());
                next.setCity(city);
                next.setZipcode(zipcode);
                next.setCountry(country);
                next.setStreet(street);
                next.setStreetAddon(streetAddon);
                next.setNote(note);
                next.setStatus(Record.STAT_CHANGED);
                break;
            }
        }
    }

    public void releaseCorrection() {
        for (Iterator<RecordCorrection> i = corrections.iterator(); i.hasNext();) {
            RecordCorrection next = i.next();
            next.setStatus(Record.STAT_SENT);
        }
    }

    /**
     * Creates a new record address correction. Returns null by default. Override this to return your record specific implementation
     * @param user creating correction
     * @param name on address output
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param note
     * @return correction new created or null if not supported by implementing record
     */
    protected RecordCorrection createCorrection(
            Employee user,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String note) {

        return null;
    }
}
