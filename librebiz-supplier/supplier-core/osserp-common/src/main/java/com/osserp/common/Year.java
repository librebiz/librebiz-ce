/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 7, 2008 10:12:43 PM 
 * 
 */
package com.osserp.common;

import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Year {

    /**
     * Provides the year
     * @return year
     */
    Integer getYear();

    /**
     * Provides the year as long value
     * @return yearAsLong
     */
    Long getYearAsLong();

    /**
     * Provides the year as string
     * @return year
     */
    String getYearDisplay();
    
    /**
     * Provides the short year as string (e.g. 01 for 2001)
     * @return shortyear
     */
    String getShortYearDisplay();

    /**
     * Indicates that this year is after another year
     * @param year to compare with
     * @return true if this year is after year
     */
    boolean isAfter(Year year);

    /**
     * Indicates that this year is before another year
     * @param year to compare with
     * @return true if this year is after year
     */
    boolean isBefore(Year year);

    /**
     * Indicates that year is same as given
     * @param year
     * @return true if both years match
     */
    boolean isSame(Year year);

    /**
     * Provides the beginning of the year as date value
     * @return date
     */
    Date getDate();
}
