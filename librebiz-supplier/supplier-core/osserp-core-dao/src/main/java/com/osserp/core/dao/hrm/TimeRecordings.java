/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 12:37:40 PM 
 * 
 */
package com.osserp.core.dao.hrm;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordings {

    /**
     * Looks up for type with id
     * @param id of the type
     * @return type or null if no such type exists
     */
    TimeRecordType findType(Long id);

    /**
     * Creates a new time recording
     * @param user creating the new entry
     * @param timeRecording the employee the new created recording belongs to
     * @return timeRecording new created
     */
    TimeRecording create(Employee user, Employee timeRecording);

    /**
     * Creates a new time recording period
     * @param user creating the new entry
     * @param timeRecording
     */
    void createPeriod(Employee user, TimeRecording timeRecording);

    /**
     * Closes all open records in the past if found
     * @param recording
     * @return recording
     */
    TimeRecording createClosingsIfRequired(TimeRecording recording);

    /**
     * Checks whether a time recording entry for an employee already exists
     * @param timeRecording
     * @return true if time recording was already created
     */
    boolean exists(Employee timeRecording);

    /**
     * Checks if terminal chip number exists
     * @param terminalChipnumber
     * @return true if recording with assigned chipt number exists
     */
    boolean exists(Long terminalChipnumber);

    /**
     * Tries to find the time recording of an employee
     * @param timeRecording
     * @return timeRecording
     */
    TimeRecording find(Employee timeRecording);

    /**
     * Loads time recording by id
     * @param id
     * @return time recording
     */
    TimeRecording load(Long id);

    /**
     * Change employees time recording
     * @param user
     * @param recording
     */
    void save(Employee user, TimeRecording recording);

    /**
     * Synchonizes public holiday data with current available list
     * @param recording
     * @return updated recording
     */
    TimeRecording sychronizePublicHoliday(TimeRecording recording);

    /**
     * Adds a new time record
     * @param user
     * @param recording
     * @param type
     * @param date
     * @param note
     * @throws ClientException on unexpected type
     */
    void addRecord(Employee user,
            TimeRecording recording,
            TimeRecordType type,
            Date date,
            String note) throws ClientException;

    /**
     * Adds a new interval record to current selected period
     * @param user
     * @param recording
     * @param type
     * @param startDate
     * @param stopDate
     * @param note
     * @throws ClientException on unexpected type
     */
    void addInterval(
            Employee user,
            TimeRecording recording,
            TimeRecordType type,
            Date startDate,
            Date stopDate,
            String note)
            throws ClientException;

    /**
     * Adds a new marker to current selected period
     * @param user
     * @param recording
     * @param type
     * @param date
     * @param value
     * @param note
     * @throws ClientException
     */
    void addMarker(
            Employee user,
            TimeRecording recording,
            TimeRecordMarkerType type,
            Date date,
            Double value,
            String note)
            throws ClientException;

    /**
     * Updates time recording with a correction value
     * @param user
     * @param recording
     * @param record
     * @param startDate
     * @param stopDate
     * @param note
     * @throws ClientException if new value or note missing
     */
    void updateRecord(Employee user, TimeRecording recording, TimeRecord record, Date startDate, Date stopDate, String note)
            throws ClientException;

    /**
     * Removes a marker
     * @param user
     * @param recording
     * @param id
     * @throws PermissionException
     */
    void removeMarker(Employee user, TimeRecording recording, Long id) throws PermissionException;

    /**
     * Removes a record
     * @param user
     * @param recording
     * @param id
     * @throws PermissionException
     */
    void removeRecord(Employee user, TimeRecording recording, Long id) throws PermissionException;

    /**
     * Provides the existing corrections of a time record
     * @param record
     * @return corrections or empty list if none exists
     */
    List<TimeRecordCorrection> fetchCorrections(TimeRecord record);
}
