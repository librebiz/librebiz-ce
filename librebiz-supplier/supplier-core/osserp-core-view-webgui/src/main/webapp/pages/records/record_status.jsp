<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:choose>
<c:when test="${view.providingStatusHistory}">
<c:set var="recordStatusHistory" scope="session" value="${view.statusHistory}"/>
<div class="recordLabelValue">
<o:ajaxLink linkId="statusFrameLink" url="/recordStatusHistory.do" title="displayStatusHistory"><fmt:message key="status"/></o:ajaxLink>: <c:choose><c:when test="${empty record.status or record.status == 0}"><fmt:message key="created"/></c:when><c:otherwise><oc:options name="recordStatus" value="${record.status}"/></c:otherwise></c:choose>
</div>
</c:when>
<c:otherwise>
<div class="recordLabelValue">
<fmt:message key="status"/>: <c:choose><c:when test="${empty record.status or record.status == 0}"><fmt:message key="created"/></c:when><c:otherwise><oc:options name="recordStatus" value="${record.status}"/></c:otherwise></c:choose>
</div>
</c:otherwise>
</c:choose>
<c:if test="${!empty record.mailLogs}">
<div class="recordLabelValue">
<o:ajaxLink linkId="mailLogLink" url="/recordMailLog.do?target=${view.name}" title="displaySendLog"><fmt:message key="sentLabel"/></o:ajaxLink>: <o:date value="${record.mailLog.created}" addtime="true"/>
</div>
</c:if>
<div class="recordLabelValue">
<span><fmt:message key="currency"/>: <o:out value="${record.currencyKey}" /></span>
<c:if test="${record.type.supportingPrint}">
<o:permission role="xsl_download,template_admin,organisation_admin,runtime_config">
<oc:systemPropertyEnabled name="documentOutputDownloadXML,documentOutputDownloadXSL">
<span style="padding-left: 35px;"><fmt:message key="documentDataExportLabel"/>:</span>
<oc:systemPropertyEnabled name="documentOutputDownloadXML">
<span style="padding-left: 10px;" title="<fmt:message key="documentOutputDownloadXML"/>"><a href="<c:url value="${requestScope.baseUrl}?method=getRecordDocument&doc=record.xml" />" target="_blank"><fmt:message key="data"/></a></span>
</oc:systemPropertyEnabled>
<oc:systemPropertyEnabled name="documentOutputDownloadXSL">
<span style="padding-left: 10px;" title="<fmt:message key="documentOutputDownloadXSL"/>"><a href="<c:url value="${requestScope.baseUrl}?method=getRecordStylesheet&doc=record.xml"/>" target="_blank"><fmt:message key="templateLabel"/></a></span>
</oc:systemPropertyEnabled>
</oc:systemPropertyEnabled>
</o:permission>
</c:if>
</div>
<c:if test="${view.itemChangedHistoryAvailable}">
<div class="recordLabelValue">
<v:ajaxLink linkId="itemChangedHistory" url="/records/itemHistory/forward?name=${sessionScope.recordView.name}&id=${record.id}"><fmt:message key="itemChangedHistory"/></v:ajaxLink>
</div>
</c:if>
