/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 11, 2007 9:16:14 PM 
 * 
 */
package com.osserp.core.finance;

import java.util.List;

import com.osserp.common.ClientException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ReceivablesManager {

    /**
     * Provides all invoices with outstanding payments
     * @param descending
     * @return invoice list
     */
    List<RecordDisplay> getInvoices(boolean descending);

    /**
     * Provides list of invoices by customer
     * @param customerId
     * @param descending
     * @return invoices with outstanding payments or empty list
     */
    List<RecordDisplay> getInvoices(Long customerId, boolean descending);

    /**
     * Provides all sales with sales receivables
     * @return salesReceivables
     */
    List<ReceivableDisplay> getSalesReceivables();

    /**
     * Provides given sales receivables as xml output
     * @param salesReceivables
     * @return document
     * @throws ClientException if input validation failed
     */
    byte[] createSalesReceivablesPdf(List<ReceivableDisplay> salesReceivables) throws ClientException;

    /**
     * Provides the open amount list as sheet.
     * @param salesReceivables
     * @return string in csv format using tab as separator
     */
    String createSalesReceivablesSheet(List<ReceivableDisplay> salesReceivables);
}
