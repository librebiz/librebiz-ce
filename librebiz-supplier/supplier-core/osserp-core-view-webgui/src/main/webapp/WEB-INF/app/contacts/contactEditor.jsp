<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="contactEditorView" />
<c:set var="contactInputViewName" scope="session" value="contactEditorView"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="javascript" type="string">
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" /> <c:if test="${!empty view.bean}"></c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${view.typeChangeMode}">
                    <li><v:link url="/contacts/contactEditor/disableTypeChangeMode" title="disableEditMode"><o:img name="backIcon"/></v:link></li>
                </c:when>
                <c:otherwise>
                    <li><v:link url="/contacts/contactEditor/exit" title="disableEditMode"><o:img name="backIcon"/></v:link></li>
                    <c:if test="${view.typeChangeModeAvailable}">
                        <li><v:link url="/contacts/contactEditor/enableTypeChangeMode" title="transferPrivateToBusinessContact"><o:img name="reloadIcon"/></v:link></li>
                    </c:if>
                </c:otherwise>
            </c:choose>
            <li><v:link url="/contacts/contactEditor/home" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>
    <%--  onsubmit="return ojsForms.validateContactForm(this);" --%>
    <tiles:put name="content" type="string">
        <div class="content-area" id="contactEditorContent">
            <v:form url="/contacts/contactEditor/save" id="contactForm" name="contactForm">
               <div class="row">
                    <c:choose>
                        <c:when test="${view.typeChangeMode}">
                            <c:import url="_contactEditorTypeChange.jsp" />
                        </c:when>
                        <c:otherwise>
                            <c:import url="_contactEditorForm.jsp" />
                        </c:otherwise>
                    </c:choose>
                </div> <!-- row -->
            </v:form>
        </div> <!-- content-area -->
    </tiles:put>
</tiles:insert>
