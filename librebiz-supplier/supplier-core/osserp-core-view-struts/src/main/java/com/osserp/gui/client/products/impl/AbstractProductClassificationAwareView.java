/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 28, 2009 1:08:04 PM 
 * 
 */
package com.osserp.gui.client.products.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.model.products.ProductClassificationConfigVO;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductClassificationConfigManager;
import com.osserp.core.products.ProductClassificationEntity;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductTypeConfig;

import com.osserp.gui.client.impl.AbstractSearchByMethodView;
import com.osserp.gui.client.products.ProductClassificationAwareView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractProductClassificationAwareView extends AbstractSearchByMethodView
        implements ProductClassificationAwareView {
    private static Logger log = LoggerFactory.getLogger(AbstractProductClassificationAwareView.class.getName());

    private List<ProductClassificationEntity> typeSelections = new ArrayList<ProductClassificationEntity>();

    private boolean includeEolAvailable = true;
    private boolean includeEol = false;
    private ProductTypeConfig selectedType = null;
    private ProductGroupConfig selectedGroup = null;
    private ProductCategoryConfig selectedCategory = null;

    protected AbstractProductClassificationAwareView() {
        super();
    }

    protected AbstractProductClassificationAwareView(boolean includeEol) {
        super();
        this.includeEol = includeEol;
    }

    /**
     * Sets search methods and default search method, view name if annotated and action target as 'success'
     * @param methods
     * @param defaultMethod
     */
    protected AbstractProductClassificationAwareView(Set<String> methods, String defaultMethod) {
        super(methods, defaultMethod);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        this.typeSelections = getClassificationConfigManager().getTypesDisplay();
    }

    public boolean isIncludeEol() {
        return includeEol;
    }

    public void setIncludeEol(boolean includeEol) {
        this.includeEol = includeEol;
    }

    public boolean isIncludeEolAvailable() {
        return includeEolAvailable;
    }

    protected void setIncludeEolAvailable(boolean includeEolAvailable) {
        this.includeEolAvailable = includeEolAvailable;
    }

    public List<ProductClassificationEntity> getTypeSelections() {
        return typeSelections;
    }

    protected void setTypeSelections(List<ProductClassificationEntity> typeSelections) {
        this.typeSelections = typeSelections;
    }

    public ProductTypeConfig getSelectedType() {
        return selectedType;
    }

    public void selectType(Long id) {
        if (id == null) {
            this.selectedType = null;
        } else {
            this.selectedType = getClassificationConfigManager().getType(id);
        }
        this.selectedGroup = null;
        if (log.isDebugEnabled()) {
            log.debug("selectType() done [type=" + (selectedType == null ? "null" : selectedType.getId()) + "]");
        }
    }

    public ProductGroupConfig getSelectedGroup() {
        return selectedGroup;
    }

    public void selectGroup(Long id) {
        if (id == null || selectedType == null) {
            this.selectedGroup = null;
        } else {
            this.selectedGroup = (ProductGroupConfig) CollectionUtil.getById(selectedType.getGroups(), id);
        }
        this.selectedCategory = null;
        if (log.isDebugEnabled()) {
            log.debug("selectGroup() done [group=" + (selectedGroup == null ? "null" : selectedGroup.getId()) + "]");
        }
    }

    public ProductCategoryConfig getSelectedCategory() {
        return selectedCategory;
    }

    public void selectCategory(Long id) {
        if (id == null || selectedGroup == null) {
            this.selectedCategory = null;
        } else {
            this.selectedCategory = (ProductCategoryConfig) CollectionUtil.getById(selectedGroup.getCategories(), id);
        }
        if (log.isDebugEnabled()) {
            log.debug("selectCategory() done [category=" + (selectedCategory == null ? "null" : selectedCategory.getId()) + "]");
        }
    }

    public ProductClassificationConfig getSelectedConfig() {
        return new ProductClassificationConfigVO(selectedType, selectedGroup, selectedCategory);
    }

    protected List<ProductClassificationEntity> fetchCategories(ProductGroupConfig group) {
        List<ProductClassificationEntity> result = getClassificationConfigManager().getCategoriesDisplay();
        if (group != null) {
            for (Iterator<ProductClassificationEntity> i = result.iterator(); i.hasNext();) {
                ProductClassificationEntity next = i.next();
                if (group.supportsCategory(next)) {
                    i.remove();
                }
            }
        }
        return result;
    }

    protected List<ProductClassificationEntity> fetchGroups(ProductTypeConfig type) {
        List<ProductClassificationEntity> result = getClassificationConfigManager().getGroupsDisplay();
        if (type != null) {
            for (Iterator<ProductClassificationEntity> i = result.iterator(); i.hasNext();) {
                ProductClassificationEntity next = i.next();
                if (type.supportsGroup(next)) {
                    i.remove();
                }
            }
        }
        return result;
    }

    protected void refresh() {
        this.typeSelections = getClassificationConfigManager().getTypesDisplay();
        if (selectedType != null) {
            Long group = (selectedGroup == null ? null : selectedGroup.getId());
            this.selectType(selectedType.getId());
            if (group != null) {
                Long category = (selectedCategory == null ? null : selectedCategory.getId());
                this.selectGroup(group);
                if (category != null) {
                    this.selectCategory(category);
                }
            }
        }
    }

    protected ProductClassificationConfigManager getClassificationConfigManager() {
        return (ProductClassificationConfigManager) getService(ProductClassificationConfigManager.class.getName());
    }
}
