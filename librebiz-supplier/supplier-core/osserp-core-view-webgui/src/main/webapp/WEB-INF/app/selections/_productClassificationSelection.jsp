<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>


<v:view viewName="productClassificationSelectionView" />
<c:set var="bean" value="${view.bean}" />

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="${view.headerName}" />
    </div>
</div>
<div class="modalBoxData">
    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <table>
                <tr>
                    <td style="width: 250px;"><v:ajaxForm name="productTypeSelectionForm" url="/selections/productClassificationSelection/selectType" targetElement="productClassificationSelection_popup">
                            <oc:select disabled="${view.typeBlocked}" name="typeId" options="${view.list}" onchange="document.productTypeSelectionForm.onsubmit();" value="${view.selectedType}" prompt="false" style="width: 200px;">
                                <oc:option name="typeWithDots" value="-1" />
                            </oc:select>
                        </v:ajaxForm></td>
                    <td style="width: 250px;"><v:ajaxForm name="productGroupSelectionForm" url="/selections/productClassificationSelection/selectGroup" targetElement="productClassificationSelection_popup">
                            <oc:select disabled="${empty view.selectedType || view.groupBlocked}" name="groupId" options="${view.selectedType.groups}" onchange="document.productGroupSelectionForm.onsubmit();" value="${view.selectedGroup}" prompt="false" style="width: 200px;">
                                <oc:option name="groupWithDots" value="-1" />
                            </oc:select>
                        </v:ajaxForm></td>
                    <td style="width: 200px;"><v:ajaxForm name="productCategorySelectionForm" url="/selections/productClassificationSelection/selectCategory" targetElement="productClassificationSelection_popup">
                            <oc:select disabled="${empty view.selectedGroup}" name="categoryId" options="${view.selectedGroup.categories}" onchange="document.productCategorySelectionForm.onsubmit();" value="${view.selectedCategory}" prompt="false" style="width: 200px;">
                                <oc:option name="categoryWithDots" value="-1" />
                            </oc:select>
                        </v:ajaxForm></td>
                </tr>
                <c:if test="${view.showStock}">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 200px;"><v:ajaxForm name="productStockSelectionForm" url="/selections/productClassificationSelection/selectStock" targetElement="productClassificationSelection_popup">
                                <oc:select name="stockId" options="${view.availableStocks}" onchange="document.productStockSelectionForm.onsubmit();" value="${view.selectedStock}" prompt="false" style="width: 200px;">
                                    <oc:option name="stockWithDots" value="-1" />
                                </oc:select>
                            </v:ajaxForm></td>
                    </tr>
                </c:if>
                <c:if test="${!empty view.selectedType || !empty view.selectedStock}">
                    <tr>
                        <td class="row-submit"></td>
                        <td class="row-submit"><input type="button" value="<fmt:message key="save"/>" onclick="gotoUrl('<c:url value="${view.url}"/>');" /></td>
                    </tr>
                </c:if>
            </table>
            <div class="spacer"></div>
        </div>
    </div>
</div>