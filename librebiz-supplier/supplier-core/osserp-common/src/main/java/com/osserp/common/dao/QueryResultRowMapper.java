/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 27-Jan-2007 17:58:24 
 * 
 */
package com.osserp.common.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class QueryResultRowMapper extends AbstractRowMapper {
    private static Logger log = LoggerFactory.getLogger(QueryResultRowMapper.class.getName());
    private JdbcQuery query = null;

    public QueryResultRowMapper(JdbcQuery query) {
        super();
        this.query = query;
        if (log.isDebugEnabled()) {
            log.debug("<init> invoked, output size " + query.getOutputs().size());
        }
    }

    public static RowMapperResultSetExtractor getReader(JdbcQuery query) {
        return new RowMapperResultSetExtractor(new QueryResultRowMapper(query));
    }

    public Object mapRow(ResultSet rs, int pos) throws SQLException {
        List<JdbcOutput> cfgs = query.getOutputs();
        int size = cfgs.size();
        String[] result = new String[size];
        for (int i = 0, j = size; i < j; i++) {
            result[i] = getValue(rs, i + 1, cfgs.get(i));
        }
        return result;
    }

    private String getValue(ResultSet rs, int i, JdbcOutput cfg) throws SQLException {
        if (JdbcQuery.BOOLEAN.equals(cfg.getTypeName())) {
            if (rs.getBoolean(i)) {
                return "true";
            }
            return "false";
        } else if (JdbcQuery.DOUBLE.equals(cfg.getTypeName())) {
            return NumberFormatter.getValue(rs.getDouble(i), NumberFormatter.SHEET);
        } else if (JdbcQuery.INTEGER.equals(cfg.getTypeName())) {
            return Integer.valueOf(rs.getInt(i)).toString();
        } else if (JdbcQuery.LONG.equals(cfg.getTypeName())) {
            return Long.valueOf(rs.getLong(i)).toString();
        } else if (JdbcQuery.TIMESTAMP.equals(cfg.getTypeName())) {
            return DateFormatter.getISO(rs.getDate(i));
        } else {
            return rs.getString(i);
        }
    }
}
