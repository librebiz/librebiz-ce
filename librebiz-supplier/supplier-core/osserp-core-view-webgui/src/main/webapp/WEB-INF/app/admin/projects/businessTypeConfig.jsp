<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="businessTypeConfigView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="businessTypeConfigContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.editMode}">
                        <v:form name="businessTypeForm" url="/admin/projects/businessTypeConfig/save">
                            <c:import url="_businessTypeConfigEdit.jsp" />
                        </v:form>
                    </c:when>
                    <c:when test="${view.createMode}">
                        <v:form name="businessTypeForm" url="/admin/projects/businessTypeConfig/save">
                            <c:import url="_businessTypeConfigCreate.jsp" />
                        </v:form>
                    </c:when>
                    <c:when test="${view.bean != null}">
                        <c:import url="_businessTypeConfigDisplay.jsp" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="businessTypeSelection" scope="request" value="${view.list}" />
                        <c:set var="businessTypeSelectionUrl" scope="request" value="/admin/projects/businessTypeConfig/select" />
                        <c:import url="${viewdir}/common/_businessTypeSelectingList.jsp" />
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>