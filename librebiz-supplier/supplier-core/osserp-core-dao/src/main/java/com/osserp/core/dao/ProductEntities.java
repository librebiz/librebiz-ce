/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 2, 2009 11:26:57 AM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.Item;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductDescription;
import com.osserp.core.products.ProductSummary;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductEntities {

    /**
     * Tries to find the product with given id
     * @return product or null if not exists
     */
    Product find(Long id);

    /**
     * Finds all descriptions of an product
     * @param productId
     * @return list of available descriptions
     */
    List<ProductDescription> findDescriptions(Long productId);

    /**
     * Finds the product's description in the given language. If more than one is found (though this should never happen), the first one is returned.
     * @param productId
     * @param language
     * @return products description
     */
    ProductDescription findDescription(Long productId, String language);

    /**
     * @return true if all the given items provide a description in the given language
     */
    boolean descriptionsAvailable(List<Item> items, String lang);

    /**
     * Loads an product from persistent storage. Throws runtime exception if no such product exists
     * @param id
     * @return product
     */
    Product load(Long id);

    /**
     * Loads the summary of an product
     * @param product
     */
    void loadSummary(Product product);

    /**
     * Loads the summary of an product
     * @param stockId
     * @param productId
     * @return summary
     */
    ProductSummary loadSummary(Long stockId, Long productId);

    /**
     * Loads products by id
     * @param idList
     * @return products by id
     */
    List<Product> load(List<Long> idList);

    /**
     * Persists an product
     * @param product
     */
    void save(Product product);
}
