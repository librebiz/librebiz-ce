/**
 *
 * Copyright (C) 2019 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 26, 2019 
 * 
 */
package com.osserp.core.dao.records;

import java.util.List;

import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordBackupItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordBackups {

    /**
     * Creates a backup of all items of a record
     * @param record
     * @param context
     */
    void createItemBackup(Record record, String context);

    /**
     * Deletes current backup
     * @param record
     * @param context
     */
    void deleteItemBackup(Record record, String context);

    /**
     * Provides backed up items
     * @param record
     * @param context
     * @return items
     */
    List<RecordBackupItem> getItemBackup(Record record, String context);

    /**
     * Provides the items of previous backup
     * @param record
     * @param context
     * @return items or empty list if no backup available
     */
    List<RecordBackupItem> getBackupItems(Record record, String context);

    /**
     * Provides the version number of last backup
     * @param record
     * @param context
     * @return version number
     */
    int getCurrentVersion(Record record, String context);

    /**
     * Indicates availability of item backups
     * @param record
     * @param context
     * @return true if item backup exists
     */
    boolean isItemBackupAvailable(Record record, String context);

}
