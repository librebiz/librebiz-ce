/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 1, 2010 1:12:00 PM 
 * 
 */
package com.osserp.core.dao.telephone;

import java.util.List;

import com.osserp.core.telephone.TelephoneConfiguration;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneConfigurations {

    /**
     * Tries to find a telephoneConfiguration by id
     * @param id
     * @return telephoneConfiguration or null if not found
     */
    TelephoneConfiguration find(Long id);

    /**
     * Tries to find a telephoneConfiguration by employeeId and branchId
     * @param employeeId
     * @param branchId
     * @return telephoneConfiguration or null if not found
     */
    TelephoneConfiguration find(Long employeeId, Long branchId);

    /**
     * Tries to find a telephoneConfiguration by telephoneGroupId
     * @param telephoneGroupId
     * @return telephoneConfiguration or null if not found
     */
    List<TelephoneConfiguration> findByTelephoneGroup(Long telephoneGroupId);

    /**
     * Tries to find telephoneConfiguration by reference
     * @param reference
     * @return configurations or null if not found
     */
    List<TelephoneConfiguration> findByReference(Long reference);

    /**
     * Tries to find telephoneConfiguration by employee id
     * @param reference
     * @return telephoneConfiguration or null if not found
     */
    TelephoneConfiguration findByEmployeeId(Long id);

    /**
     * Tries to find telephoneConfiguration by BranchId
     * @param branchId
     * @return telephoneConfiguration or null if not found
     */
    List<TelephoneConfiguration> findByBranchId(Long branchId);

    /**
     * Tries to find telephoneConfiguration by telephone branch id and internal
     * @param internal
     * @param branchId
     * @return telephoneConfiguration or null if not found
     */
    TelephoneConfiguration findByReference(String internal, Long branchId);

    /**
     * Provides all telephoneConfigurations without templates
     * @return telephoneConfigurations
     */
    List<TelephoneConfiguration> getAllTelephoneConfigurations();

    /**
     * Provides all telephoneConfiguration templates
     * @return telephoneConfigurations
     */
    List<TelephoneConfiguration> getAllTelephoneConfigurationsTemplates();

    /**
     * Persists current telephoneConfiguration values
     * @param telephoneConfiguration
     */
    void save(TelephoneConfiguration telephoneConfiguration);

    /**
     * Provides default TelephoneConfiguration
     * @return telephoneConfiguration
     */
    TelephoneConfiguration getDefaultConfig();
}
