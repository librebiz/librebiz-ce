/**
 *
 * Copyright (C) 2002 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 08.08.2002 
 * 
 */
package com.osserp.common.util;

import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.Map;

import org.junit.Test;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UStringUtil {

    @Test
    public void testReplaceBlanks() {
        assertTrue("Hello_World".equals(StringUtil.replaceBlanks("Hello World", "_")));
    }

    @Test
    public void testGetTokenList() {
        LinkedList<String> result = StringUtil.getTokenList("address.header", ".");
        String first = (result == null || result.size() < 1) ? null : result.get(0);
        String second = (result == null || result.size() < 2) ? null : result.get(1);
        assertTrue("address".equals(first));
        assertTrue("header".equals(second));
    }

    @Test
    public void testGetTokenArray() {
        String[] result = StringUtil.getTokenArray("product    100    abc", " ");
        assertTrue(result.length == 3);
        String first = result.length < 1 ? null : result[0];
        String second = result.length < 1 ? null : result[1];
        String third = result.length < 1 ? null : result[2];
        assertTrue("product".equals(first));
        assertTrue("100".equals(second));
        assertTrue("abc".equals(third));
        result = StringUtil.getTokenArray("   100  ", " ");
        assertTrue(result.length == 1);
        assertTrue("100".equals(result.length < 1 ? null : result[0]));
    }

    @Test
    public void testGetPropertyMap() {
        String testProperties = "key1=val1,key2=val2,key3=val3";
        Map<String, String> result = StringUtil.getPropertyMap(testProperties);
        assertTrue(!result.isEmpty());
        assertTrue("val3".equals(result.get("key3")));
    }

    @Test
    public void testCreateLoginUid() {
        String fname = "Mark";
        String lname = "Marker";
        String expectedResult = "mmarker";
        String result = StringUtil.createLoginUid(fname, 1, lname, lname.length());
        assertTrue(result.equals(expectedResult));
        result = StringUtil.createLoginUid(fname, 1, lname, lname.length() + 10);
        assertTrue(result.equals(expectedResult));
    }

    @Test
    public void testCreateLoginUidWithBlanks() {
        String fname = "Mark";
        String lname = "Marks Marker";
        String expectedResult = "mmarksmarker";
        String result = StringUtil.createLoginUid(fname, 1, lname, lname.length());
        assertTrue(result.equals(expectedResult));
        result = StringUtil.createLoginUid(fname, 1, lname, lname.length() + 10);
        assertTrue(result.equals(expectedResult));
    }

    @Test
    public void testCreateLoginUidWithMinus() {
        String fname = "Mark";
        String lname = "Marks-Marker";
        String expectedResult = "mmarksmarker";
        String result = StringUtil.createLoginUid(fname, 1, lname, lname.length());
        assertTrue(result.equals(expectedResult));
        result = StringUtil.createLoginUid(fname, 1, lname, lname.length() + 10);
        assertTrue(result.equals(expectedResult));
    }

    @Test
    public void testArrayCopy() {
        String[] target = StringUtil.concat(
                new String[] { "Mark", "Marker", "Marks" }, 
                new String[] { "Is", "Murks" });
        assertTrue(target[2] == "Marks");
        assertTrue(target[4] == "Murks");
    }
}
