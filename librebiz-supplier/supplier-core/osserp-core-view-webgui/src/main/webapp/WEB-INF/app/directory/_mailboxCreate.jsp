<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="directoryMailboxCreate" />
                <span> - </span>
                <o:out value="${view.bean.values['uid']}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <v:form name="dynamicForm" url="${view.baseLink}/save">

                <c:set var="values" value="${view.form.params}"/>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="uid">
                                <fmt:message key="uid" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="uid" value="${values['uid']}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="uidNumber">
                                <fmt:message key="uidNumber" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="uidnumber" value="${values['uidnumber']}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                <fmt:message key="emailAddress" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="mail" value="${values['mail']}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">
                                <fmt:message key="name" /><span> (cn)</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="cn" value="${values['cn']}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="description">
                                <fmt:message key="description" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="description" value="${values['description']}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="createMailboxLabel">
                                <fmt:message key="createMailbox" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="createMailboxCheckbox">
                                    <v:checkbox id="createMailbox" value="${values['createMailbox']}" /> <fmt:message key="mailboxCreateHint" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="mailboxUser">
                                <fmt:message key="account" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="mailboxUser" value="${values['mailboxUser']}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="password">
                                <fmt:message key="password" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="mailboxPassword" value="${values['mailboxPassword']}" />
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4"> </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/disableCreateMode"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </v:form>
    </div>
</div>
