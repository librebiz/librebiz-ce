/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2020
 * 
 */
package com.osserp.core;

import com.osserp.common.ClientException;
import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PluginManager {

    /**
     * Provides all active plugins
     * @return plugins
     */
    Plugins getPlugins();

    /**
     * Provides plugin by name if exists
     * @param name
     * @return plugin or null if not exists
     */
    Plugin fetch(String name);

    /**
     * Provides plugin by name
     * @param name
     * @return plugin
     * @throws ClientException if plugin not exists or disabled
     */
    Plugin get(String name) throws ClientException;

    /**
     * Enables/disables plugin and/or updates common properties
     * @param user the user performing the action
     * @param plugin the plugin to update
     * @param description new description text
     * @param disabled disables plugin if true  
     */
    Plugin update(User user, Plugin plugin, String description, boolean disabled);

    /**
     * Updates a property of the plugin
     * @param user the user performing the action
     * @param plugin the plugin to update
     * @param name the name of the property
     * @param value property value  
     */
    Plugin updateProperty(User user, Plugin plugin, String name, String value);

    /**
     * Provides an existing mapping
     * @param plugin
     * @param name logical name of the mapped property
     * @param entityId primary key of the mapped core entity
     * @return mapping or null if no such mapping exists
     */
    PluginMapping getMapping(Plugin plugin, String name, Long entityId);

    /**
     * Creates an entity mapping
     * @param plugin
     * @param name
     * @param mappedId
     * @param objectId
     * @param objectName
     * @param objectType
     * @param objectStatus
     * @return mapping object
     */
    PluginMapping createMapping(
            Plugin plugin,
            String name,
            Long mappedId,
            String objectId,
            String objectName,
            String objectType,
            String objectStatus);

    /**
     * Removes an entity mapping
     * @param plugin
     * @param name
     * @param mappedId
     */
    void removeMapping(Plugin plugin, String name, Long mappedId);

    /**
     * Overrides existing mapping object
     * @param mapping
     * @param objectId
     * @param objectName
     * @param objectType
     * @param objectStatus
     * @return mapping object with updated status
     */
    PluginMapping updateMapping(
            PluginMapping mapping,
            String objectId,
            String objectName,
            String objectType,
            String objectStatus);

    /**
     * Updates mapping status
     * @param plugin
     * @param name
     * @param mappedId
     * @param status
     * @return mapping object with updated status
     */
    PluginMapping updateMapping(Plugin plugin, String name, Long mappedId, String status);

}
