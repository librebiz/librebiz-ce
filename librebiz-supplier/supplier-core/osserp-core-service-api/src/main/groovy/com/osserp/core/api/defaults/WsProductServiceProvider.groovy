/**
 *
 * Copyright (C) 2012 The original author or authors.
 * 
 * Created on 22.12.2012 
 * 
 */
package com.osserp.core.api.defaults

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WSObjectException
import com.osserp.api.WSProduct
import com.osserp.api.WSProductService

import com.osserp.common.ClientException
import com.osserp.common.service.Locator

import com.osserp.core.service.impl.AbstractWebService

/**
 *
 * @author rk <rk@osserp.com>
 *
 */
class WsProductServiceProvider extends AbstractWebService implements WSProductService {
    private static Logger logger = LoggerFactory.getLogger(WsProductServiceProvider.class.getName())

    public WsProductServiceProvider(Locator locator) {
        super(locator);
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSProductService#fetchProductById(java.lang.Long)
     */
    public WSProduct fetchProductById(Long productId) {
        WSProduct result
        try {
            result = wsProductService.fetchProductById(productId)
        } catch (Exception e) {
            logger.error("fetchProductById: caught exception [productId=${productId}, message=${e.message}]", e)        
        }
        result
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSProductService#getProductById(java.lang.Long)
     */
    public WSProduct getProductById(Long productId) throws WSObjectException {
        WSProduct result
        try {
            result = wsProductService.getProductById(productId)
        } catch (Exception e) {
            logger.error("getProductById: caught exception [productId=${productId}, message=${e.message}]", e)
            throw e        
        }
        result
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSProductService#createOrUpdate(com.osserp.api.WSProduct)
     */
    public WSProduct createOrUpdate(WSProduct wsProduct) {
        WSProduct result
        try {
            result = wsProductService.createOrUpdate(wsProduct)
        } catch (ClientException c) {
            logger.error("createOrUpdate: caught well known exception [id=${wsProduct?.primaryKey}, message=${c.message}]")
        } catch (Exception e) {
            logger.error("createOrUpdate: caught unexpected exception [id=${wsProduct?.primaryKey}, message=${e.message}]", e)
        }
        result
    }

    protected WSProductService getWsProductService() {
        getService(WSProductService.class.getName())
    }

}
