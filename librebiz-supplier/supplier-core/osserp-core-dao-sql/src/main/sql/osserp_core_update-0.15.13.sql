SET search_path = osserp, pg_catalog;

UPDATE system_menu_links SET order_id = order_id + 1 WHERE reference_id = 2 AND order_id > 0;
INSERT INTO system_menu_links VALUES (81, 2, 1, 'create', 'accounting,customer_service,sales,sales_change,requests_cancel,purchasing,organisation_admin,executive,executive_branch,executive_customer_service,executive_sales', false, '/contacts/contactSearch/forward?selectionExit=ignore&selectionTarget=/requests/requestCreator/forward&context=customer', false, false, NULL, false, true);
SELECT pg_catalog.setval('system_menu_links_id_seq', 81, true);

UPDATE system_properties SET pvalue = '0.15.13', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
