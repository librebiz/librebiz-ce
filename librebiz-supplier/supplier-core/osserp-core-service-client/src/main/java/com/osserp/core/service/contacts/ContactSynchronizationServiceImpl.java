/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 9, 2012 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.OptionsCache;

import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.Employees;
import com.osserp.core.directory.DirectoryUserSynchronizer;
import com.osserp.core.employees.Employee;
import com.osserp.core.system.BranchOffice;

/**
 * The default implementation of the ContactSynchronizationService. This implementation syncs the contact with the default OpenLDAP directory service.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactSynchronizationServiceImpl implements ContactSynchronisationService {
    private static Logger log = LoggerFactory.getLogger(ContactSynchronizationServiceImpl.class.getName());

    private OptionsCache optionsCache;
    private DirectoryUserSynchronizer directoryUserSynchronizer = null;
    private BranchOffices branchOffices = null;
    private Employees employees = null;

    public ContactSynchronizationServiceImpl(
            OptionsCache optionsCache,
            DirectoryUserSynchronizer directoryUserSynchronizer,
            BranchOffices branchOffices,
            Employees employees) {
        super();
        this.optionsCache = optionsCache;
        this.directoryUserSynchronizer = directoryUserSynchronizer;
        this.branchOffices = branchOffices;
        this.employees = employees;
    }

    public void sync(Contact contact) {
        //if contact is branchOffice, we need to synchronize all employees of it
        BranchOffice office = branchOffices.findByContact(contact.getContactId());
        if (office != null) {
            if (log.isDebugEnabled()) {
                log.debug("sync() contact is branch [contact=" + contact.getContactId() + ", branch=" + office.getId() + "]");
            }
            List<Employee> employeeList = employees.findActiveEmployees();
            for (Iterator<Employee> i = employeeList.iterator(); i.hasNext();) {
                try {
                    Employee next = i.next();
                    if (next.isSupportingBranch(office)) {
                        if (log.isDebugEnabled()) {
                            log.debug("sync() found related [employee=" + next.getId() + ", contact=" + next.getContactId() + "]");
                        }
                        directoryUserSynchronizer.synchronizeEmployee(next);
                    }
                } catch (Exception e) {
                    log.error("sync() caught exception [message=" + e.getMessage() + "]", e);
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("sync() employees of branch synchronized [branch=" + office.getId() + "]");
            }
        } else if (contact.isEmployee()) {

            Employee employee = (Employee) employees.findByContact(contact);
            if (employee == null) {
                log.warn("sync() related contact not found [contactId="
                        + contact.getContactId() + "]");
            } else {
                employees.reload(employee.getId());
                optionsCache.refresh(Options.ACTIVATED_EMPLOYEES);
                optionsCache.refresh(Options.EMPLOYEES);
                optionsCache.refresh(Options.EMPLOYEE_KEYS);
                directoryUserSynchronizer.synchronizeEmployee(employee);
            }
        } else {
            directoryUserSynchronizer.synchronizeContact(contact);
        }
    }

    public void sync(PrivateContact privateContact) {
        directoryUserSynchronizer.synchronizePrivateContact(privateContact);
    }
}
