/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 26, 2010 3:43:43 PM 
 * 
 */
package com.osserp.common.web.tags;

import javax.servlet.jsp.JspException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SubmitTag extends AbstractSubmitTag {

    /* (non-Javadoc)
     * @see com.osserp.common.web.tags.AbstractOutTag#getOutput()
     */
    @Override
    public String getOutput() throws JspException {
        StringBuilder buffer = new StringBuilder("<input type=\"submit\" name=\"");
        if (getName() != null && getName().length() > 0) {
            buffer.append(getName()).append("\"");
        } else {
            buffer.append("sendSubmit").append("\"");
        }
        if (getTabindex() != null && getTabindex().length() > 0) {
            buffer.append(" tabindex=\"").append(getTabindex()).append("\"");
        }
        if (getStyleClass() == null || getStyleClass().length() < 1
                && getSubmitDefaultStyle() != null && getSubmitDefaultStyle().length() < 1) {
            setStyleClass(getSubmitDefaultStyle());
        }
        return addCommonSubmitOutput(addOnclick(buffer)).toString();
    }
    
    @Override
    public StringBuilder addStyleClass(StringBuilder buffer) {
        String styleClass = getStyleClass();
        // old form-control relating submit without primary declaration
        if (styleClass != null && styleClass.indexOf("form-control") > -1
                && styleClass.indexOf("btn-primary") < 0) {
            if (styleClass.indexOf("btn") < 0) {
                // neither btn nor primary defined
                styleClass = "btn btn-primary " + styleClass;
            } else {
                // btn class was already defined but not primary
                styleClass =  styleClass + " btn-primary";
            }
        }
        return appendAttribute(buffer, "class", styleClass);
    }

}
