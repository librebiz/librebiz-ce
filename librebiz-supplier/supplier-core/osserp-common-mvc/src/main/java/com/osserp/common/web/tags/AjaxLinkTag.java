/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2007 10:45:32 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Tag renders an HTML-Link executing an ajax request on click. The function invoked depends on given params.
 * 
 * function ajaxLink(requestUrl, targetElement, targetIsPopup, activityElement, activityMessage, successFunction, link)
 * 
 * @author cf <cf@osserp.com>
 */
public class AjaxLinkTag extends AbstractAjaxLinkTag {
    private static Logger log = LoggerFactory.getLogger(AjaxLinkTag.class.getName());

    @Override
    public int doStartTag() throws JspException {
        try {
            if (getActivityElement() == null) {
                setActivityElement("");
            }
            if (getPostRequest() == null) {
                setPostRequest("");
            }

            StringBuilder buffer = new StringBuilder();
            buffer.append("<a");
            if (getLinkId() != null) {
                buffer.append(" id=\"").append(getLinkId()).append("\"");
            }
            buffer
                    .append(" href=\"javascript:;\"")
                    .append(" onclick=\"");
            if (getPreRequest() != null) {
                buffer.append(getPreRequest());
            }
            buffer
                    .append(" ojsAjax.ajaxLink('")
                    .append(createUrl())
                    .append("', '")
                    .append(getTargetElement())
                    .append("', ");
            if (isPopup()) {
                buffer.append("true");
            } else {
                buffer.append("false");
            }
            buffer
                    .append(", '")
                    .append(getActivityElement())
                    .append("', '")
                    .append(getResourceString("waitPleaseWithDots"))
                    .append("', function() { ")
                    .append(getOnSuccess())
                    .append(" }, this); ")
                    .append(getPostRequest())
                    .append("\"");

            appendEnd(buffer);
            JspWriter out = pageContext.getOut();
            out.print(buffer.toString());

            setTargetElement(null);
            setPreRequest(null);
            setPostRequest(null);
            setOnSuccess(null);
        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }
}
