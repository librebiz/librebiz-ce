/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on May 25, 2010 1:28:54 PM 
 * 
 */
package com.osserp.core.model.telephone;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.SfaPhoneBookItem;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class SfaPhoneBookItemImpl extends AbstractEntity implements SfaPhoneBookItem {
    private boolean synced = false;
    private boolean unused = false;
    private String uid = null;
    private Long contactId = null;

    private String name = null;
    private String number = null;
    private String firstName = null;
    private String lastName = null;
    private Long numberType = null;
    private String title = null;
    private String organization = null;
    private String email = null;
    private String note = null;
    private String groupTypeName = null;
    private Date birthdayItem = null;
    private boolean favourite = false;

    public SfaPhoneBookItemImpl() {
        super();
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isUnused() {
        return unused;
    }

    public void setUnused(boolean unused) {
        this.unused = unused;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getNumberType() {
        return numberType;
    }

    public void setNumberType(Long numberType) {
        this.numberType = numberType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getGroupTypeName() {
        return groupTypeName;
    }

    public void setGroupTypeName(String groupTypeName) {
        this.groupTypeName = groupTypeName;
    }

    public Date getBirthdayItem() {
        return birthdayItem;
    }

    public void setBirthdayItem(Date birthdayItem) {
        this.birthdayItem = birthdayItem;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

}
