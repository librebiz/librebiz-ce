/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 5, 2011 10:34:01 AM 
 * 
 */
package com.osserp.common.directory.impl

import com.osserp.common.ErrorCode
import com.osserp.common.directory.DirectoryException
import com.osserp.common.directory.DirectoryMailboxManager
import com.osserp.common.directory.DirectoryUser
import com.osserp.common.directory.FetchmailAccount
import com.osserp.common.directory.ldap.FetchmailLdapManager
import com.osserp.common.directory.ldap.LdapException
import com.osserp.common.directory.ldap.LdapManager
import com.osserp.common.util.EmailValidator


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class DirectoryMailboxManagerImpl extends AbstractDirectoryManager implements DirectoryMailboxManager {

    protected FetchmailLdapManager fetchmailLdapManager

    protected DirectoryMailboxManagerImpl() {
        super()
    }

    protected DirectoryMailboxManagerImpl(LdapManager initialized, FetchmailLdapManager fetchmailLdapManager) {
        super(initialized)
        this.fetchmailLdapManager = fetchmailLdapManager
    }

    Map createMailboxUserAttributes(String client) {
        createDefaultUserAttributes(DirectoryUser.BUSINESS_CATEGORY_MAILBOX, client)
    }

    DirectoryUser createMailbox(Map userValues, Map mailboxValues, String client) throws DirectoryException {
        DirectoryUser result
        try {
            if (mailboxValues && ldapManager.config.fetchmailSupportEnabled) {
                if (!mailboxValues.name) {
                    throw new DirectoryException(ErrorCode.ENTRY_MISSING)
                }
                if (!mailboxValues.mail) {
                    throw new DirectoryException(ErrorCode.EMAIL_MISSING)
                }
                if (!mailboxValues.password) {
                    throw new DirectoryException(ErrorCode.PASSWORD_MISSING)
                }
            }
            if (!userValues.uid) {
                throw new DirectoryException(ErrorCode.USERID_INVALID)
            }
            if (!userValues.mail) {
                throw new DirectoryException(ErrorCode.EMAIL_MISSING)
            }
            if (!EmailValidator.validate(userValues.mail)) {
                throw new DirectoryException(ErrorCode.EMAIL_INVALID)
            }
            Map user = ldapManager.lookupUser(userValues.uid, client)
            if (!user) {
                user = ldapManager.createUser(userValues, client)
            } else {
                user = ldapManager.updateUser(user, [mail: userValues.mail])
            }
            if (mailboxValues && user && ldapManager.config.fetchmailSupportEnabled) {
                Map fetchmail = fetchmailLdapManager.createMbox(user, mailboxValues.mail, mailboxValues.name, mailboxValues.password)
            }
            result = new DirectoryUser(values:user)
        } catch (LdapException e) {
            throw new DirectoryException(e.message, e)
        }
        result
    }

    FetchmailAccount findMailbox(DirectoryUser user) {
        if (ldapManager.config.fetchmailSupportEnabled) {
            Map account = fetchmailLdapManager.lookupFetchmailAccount(user.values)
            if (account) {
                return new FetchmailAccount([values:account])
            }
        }
        return null
    }

    FetchmailAccount update(FetchmailAccount account, Map values) {
        Map updatedAccount = fetchmailLdapManager.updateMbox(account.values, values)
        new FetchmailAccount(values:updatedAccount)
    }

    FetchmailAccount updateMailboxActivation(FetchmailAccount account, Boolean enable) {
        Map result
        if (enable) {
            result = fetchmailLdapManager.enableMbox(account.values)
        } else {
            result = fetchmailLdapManager.disableMbox(account.values)
        }
        new FetchmailAccount([values: result])
    }
}
