/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jul 27, 2011 4:43:00 PM 
 * 
 */
package com.osserp.gui.selections

import com.osserp.core.employees.EmployeeSearch

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
public class EmployeeCapacitySelectionView extends AbstractSelectionView {

    public static final String MANAGER_MODE = 'manager'
    public static final String SALES_MODE = 'sales'

    boolean inactive = false

    EmployeeCapacitySelectionView() {
        super()
        disablePopupView()
        env.managerMode = false
        env.salesMode = false
        headerName = 'employeeSelection'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            String mode = form.getString('mode')
            if (mode == MANAGER_MODE) {
                env.managerMode = true
            } else {
                env.salesMode = true
            }
            inactive = form.getBoolean('inactive', false)
            reload()
            targetParams.remove('exit')
        }
    }

    @Override
    void reload() {
        if (salesMode) {
            list = employeeSearch.findSalesCapacity(domainUser, null)
        } else if (managerMode) {
            list = employeeSearch.findTecCapacity(domainUser, null)
        }
        if (!inactive) {
            list = list.findAll { it.active }
        }
        actualSortKey = 'name'
    }

    boolean isManagerMode() {
        env.managerMode
    }

    boolean isSalesMode() {
        env.salesMode
    }
}
