/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2009 12:15:44 PM 
 * 
 */
package com.osserp.gui.client.records.impl;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.Item;
import com.osserp.core.customers.Customer;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.TaxRateManager;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductUtil;

import com.osserp.gui.client.products.impl.AbstractProductSearchView;
import com.osserp.gui.client.records.RecordProductSearchView;
import com.osserp.gui.client.records.RecordView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("recordProductSearchView")
public class RecordProductSearchViewImpl extends AbstractProductSearchView implements RecordProductSearchView {

    private RecordView recordView = null;
    private List<Double> taxRates = null;
    private List<Double> reducedTaxRates = null;

    /**
     * Constructor required for serialization. Initializes view name, actionUrl and selectionUrl as annotated and initializes action target as 'success'.
     */
    protected RecordProductSearchViewImpl() {
        super();
    }

    /**
     * Default constructor. Initializes view name, actionUrl and selectionUrl as annotated and initializes action target as 'success'.
     * @param methods
     * @param defaultMethod
     */
    protected RecordProductSearchViewImpl(Set<String> methods, String defaultMethod) {
        super(methods, defaultMethod, true);
    }

    /**
     * Initializes view by fetching current record view and exit target. Params: <br/>
     * 'view' session scope name of current record view
     * @param request
     */
    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        String recordViewName = RequestUtil.getString(request, "view");
        HttpSession session = request.getSession();
        if (recordViewName != null && session.getAttribute(recordViewName) != null) {
            setRecordView((RecordView) session.getAttribute(recordViewName));
        }
        TaxRateManager taxRateManager = (TaxRateManager) getService(TaxRateManager.class.getName());
        taxRates = taxRateManager.getRates();
        reducedTaxRates = taxRateManager.getReducedRates();
        setExistingTaxRate(taxRateManager.getCurrentRate());
        setExistingReducedTaxRate(taxRateManager.getCurrentReducedRate());
    }

    public void init(RecordView otherRecordView) {
        setRecordView(otherRecordView);
        setExitTarget(recordView.getName());
    }

    public RecordView getRecordView() {
        return recordView;
    }

    protected void setRecordView(RecordView recordView) {
        this.recordView = recordView;
        if (recordView != null  && recordView.getRecord() != null
                && recordView.getRecord().getContact() != null) {
            setContactReference(recordView.getRecord().getContact().getId());
            setContactReferenceCustomer(recordView.getRecord().getContact() instanceof Customer);
            if (isSet(getContactReference()) && isContactReferenceCustomer()) {
                setProductPriceMethod(getSystemProperty(ProductUtil.PRODUCT_PRICE_SALES_PROPERTY_METHOD));
                setProductPriceDefault(getSystemProperty(ProductUtil.PRODUCT_PRICE_SALES_PROPERTY_DEFAULT));
            } else if (isSet(getContactReference())) {
                setProductPriceMethod(getSystemProperty(ProductUtil.PRODUCT_PRICE_PURCHASING_PROPERTY_METHOD));
                setProductPriceDefault(getSystemProperty(ProductUtil.PRODUCT_PRICE_PURCHASING_PROPERTY_DEFAULT));
            }
            searchBlank();
        }
    }

    public void selectItem(Long id) {
        if (recordView.isItemReplaceMode()) {
            Product p = getProductSearch().find(id);
            recordView.replaceItem(p);
        }
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        Long[] productIds = form.getIndexedLong(Form.PRODUCT_ID);
        String[] customNames = form.getIndexedStrings(Form.CUSTOM_NAME);
        Double[] quantities = getDouble(form, Form.QUANTITY);
        BigDecimal[] prices = getDecimal(form, Form.PRICE);
        String[] notes = form.getIndexedStrings(Form.NOTE);
        Double[] taxRates = getDouble(form, "taxRate");
        if (productIds == null || quantities == null || taxRates == null
                || prices == null || notes == null) {
            throw new ClientException(ErrorCode.SELECTION_MISSING);
        }
        Product[] selected = getSelection(productIds);
        Record record = recordView != null && recordView.getRecord() != null ? recordView.getRecord() : null;
        if (selected.length == 1 && record != null && record.containsProduct(selected[0].getProductId())
                && isSystemPropertyEnabled("recordItemsUnique")) {

            for (Iterator<Item> i = record.getItems().iterator(); i.hasNext();) {
                Item existing = i.next();
                if (existing.getProduct().getProductId().equals(selected[0].getProductId())) {
                    recordView.updateItem(
                            existing,
                            customNames[0],
                            quantities[0],
                            prices[0],
                            taxRates[0],
                            notes[0]);
                    setExistingProduct(null);
                    setExistingQuantity(null);
                    setExistingPrice(null);
                    setExistingTaxRate(null);
                }
            }
        } else {
            recordView.addItems(selected, customNames, quantities, taxRates, prices, notes);
        }
        recordView.refresh();
    }

    @Override
    protected void setList(List list) {
        setExistingProduct(null);
        setExistingQuantity(null);
        setExistingPrice(null);
        if (isSystemPropertyEnabled("recordItemsUnique") && recordView != null) {
            Record record = recordView.getRecord();
            if (record != null && record.getItems().size() > 0) {
                for (Iterator<Product> i = list.iterator(); i.hasNext();) {
                    Product next = i.next();
                    if (next.isAffectsStock() 
                            && record.containsProduct(next.getProductId())) {
                        if (list.size() == 1) {
                            for (int k = 0, l = record.getItems().size(); k < l; k++) {
                                Item nextItem = record.getItems().get(k);
                                if (nextItem.getProduct().getProductId().equals(next.getProductId())) {
                                    setExistingProduct(nextItem.getProduct().getProductId());
                                    setExistingQuantity(nextItem.getQuantity());
                                    setExistingPrice(nextItem.getPrice());
                                    setExistingTaxRate(nextItem.getTaxRate());
                                    break;
                                }
                            }
                        } else {
                            i.remove();
                        }
                    }
                }
            }
        }
        super.setList(list);
    }

    public List<Double> getTaxRates() {
        return taxRates;
    }

    public void setTaxRates(List<Double> taxRates) {
        this.taxRates = taxRates;
    }

	public List<Double> getReducedTaxRates() {
		return reducedTaxRates;
	}

	public void setReducedTaxRates(List<Double> reducedTaxRates) {
		this.reducedTaxRates = reducedTaxRates;
	}
}
