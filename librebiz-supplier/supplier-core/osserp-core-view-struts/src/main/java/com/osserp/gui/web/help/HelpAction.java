/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 12, 2007 12:59:21 PM 
 * 
 */
package com.osserp.gui.web.help;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.web.Form;
import com.osserp.common.web.PortalView;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.gui.client.Actions;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class HelpAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(HelpAction.class.getName());

    /**
     * Enables help config mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableHelpConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableHelpConfig() request from " + getUser(session).getId());
        }
        View view = fetchView(session);
        PortalView pv = null;
        if (view != null && view instanceof PortalView) {
            pv = (PortalView) view;
            pv.enableHelpConfigMode();
            if (log.isDebugEnabled()) {
                log.debug("enableHelpConfig() done");
            }
        }
        return findForward(mapping, pv);
    }

    /**
     * Disables help config mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableHelpConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableHelpConfig() request from " + getUser(session).getId());
        }
        View view = fetchView(session);
        PortalView pv = null;
        if (view != null && view instanceof PortalView) {
            pv = (PortalView) view;
            pv.disableHelpConfigMode();
            if (log.isDebugEnabled()) {
                log.debug("disableHelpConfig() done");
            }
        }
        return findForward(mapping, pv);
    }

    /**
     * Enables help link for current page
     * @param mapping
     * @param actionForm
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enablePage(
            ActionMapping mapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enablePage() request from " + getUser(session).getId());
        }
        View view = fetchView(session);
        PortalView pv = null;
        if (view != null && view instanceof PortalView) {
            pv = (PortalView) view;
            pv.enableHelp();
            if (pv.getHelpLink() != null) {
                return new ActionForward(pv.getHelpLink().getName());
            }
        }
        return findForward(mapping, pv);
    }

    /**
     * Enables help link for current page
     * @param mapping
     * @param actionForm
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disablePage(
            ActionMapping mapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disablePage() request from " + getUser(session).getId());
        }
        View view = fetchView(session);
        PortalView pv = null;
        if (view != null && view instanceof PortalView) {
            pv = (PortalView) view;
            pv.disableHelp();
        }
        return findForward(mapping, pv);
    }

    @Override
    public ActionForward save(
            ActionMapping mapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("save() request from " + getUser(session).getId());
        }
        View view = fetchView(session);
        PortalView pv = null;
        if (view != null && view instanceof PortalView) {
            pv = (PortalView) view;
            Form form = new StrutsForm(actionForm);
            try {
                pv.updateHelp(
                        form.getString(Form.NAME),
                        form.getString(Form.VALUE));
                if (log.isDebugEnabled()) {
                    log.debug("save() done");
                }
            } catch (ClientException e) {
                RequestUtil.saveError(request, e.getMessage());
            }
        }
        return findForward(mapping, pv);
    }

    private ActionForward findForward(ActionMapping mapping, PortalView view) {
        if (view != null && view.getPage() != null && view.getPage().getName() != null) {
            if (log.isDebugEnabled()) {
                log.debug("findForward() found page " + view.getPage().getName());
            }
            return new ActionForward(view.getPage().getName());
        }
        return mapping.findForward(Actions.INDEX);
    }

    @Override
    protected Class<PortalView> getViewClass() {
        return PortalView.class;
    }

}
