/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 17, 2007 10:12:24 AM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Orders extends Records {

    /**
     * Updates the delivery date of the order
     * @param user
     * @param order
     * @param deliveryDate
     * @param deliveryNote
     * @param defines what to to with order item delivery dates, <br>
     * 'noItems' indicates that nothing should be done, <br>
     * 'allItems' updates the delivery date on all items <br>
     * 'emptyItems' updates all items with current empty date
     * @param deliveryConfirmed indicates that delivery is confirmed
     * @return delivery changed info list of all changed items
     * @throws ClientException if date is in the past
     */
    List<DeliveryDateChangedInfo> update(
            Employee user,
            Order order,
            Date deliveryDate,
            String deliveryNote,
            String itemBehaviour,
            boolean deliveryConfirmed)
            throws ClientException;

    /**
     * Updates the delivery date of a specified item
     * @param user
     * @param order
     * @param estimatedDelivery
     * @param deliveryNote
     * @param item
     * @return delivery changed info of changed item or null if nothing changed
     * @throws ClientException if date is in the past
     */
    DeliveryDateChangedInfo update(
            Employee user,
            Order order,
            Date estimatedDelivery,
            String deliveryNote,
            Item item) throws ClientException;

    /**
     * Fetches delivery date changes of an order
     * @param order
     * @return all delivery date changed infos of an order
     */
    List<DeliveryDateChangedInfo> getDeliveryDateChangedInfos(Order order);

    /**
     * Fetches item changes of a record
     * @param id
     * @return all item changed infos of a record
     */
    List<ItemChangedInfo> getItemChangedInfos(Long id);

}
