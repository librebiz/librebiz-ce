<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="documentImportView"/>

<v:form url="${view.baseLink}/save" multipart="true">
<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 style="text-transform: none;"><fmt:message key="documentImportFilesSelection" /></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="documentImportHint1"> </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <fmt:message key="documentImportHint1Text"/>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="fileUpload"><fmt:message key="selection"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input class="fileInput" type="file" name="files" multiple="multiple" />
                    </div>
                </div>
            </div>
            
            <div class="row next">
                <div class="col-md-4"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submitExit url="${view.appLink}/exit"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="uploadButton" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</v:form>
