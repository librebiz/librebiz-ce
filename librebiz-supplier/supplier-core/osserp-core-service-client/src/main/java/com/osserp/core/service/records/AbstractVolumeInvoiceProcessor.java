/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 30, 2009 1:22:32 AM 
 * 
 */
package com.osserp.core.service.records;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.util.DateFormatter;

import com.osserp.core.Item;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderVolumeExports;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Records;
import com.osserp.core.finance.VolumeInvoiceProcessor;
import com.osserp.core.products.Product;
import com.osserp.core.sales.SalesConstants;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesInvoiceRebateManager;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.sales.SalesOrderVolumeExport;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractVolumeInvoiceProcessor extends AbstractService implements VolumeInvoiceProcessor {
    private static Logger log = LoggerFactory.getLogger(AbstractVolumeInvoiceProcessor.class.getName());

    private Projects projects = null;
    private SalesInvoices salesInvoices = null;
    private SalesInvoiceRebateManager invoiceRebateManager = null;
    private SalesOrderVolumeExports exportsArchive = null;
    private SystemConfigManager systemConfigManager = null;
    private Product vProduct = null;

    protected AbstractVolumeInvoiceProcessor(
            Products products,
            Projects projects,
            SalesOrderManager orderManager,
            SalesOrderVolumeExports exportsArchive,
            SalesInvoices invoices,
            SalesInvoiceRebateManager invoiceRebateManager,
            SystemConfigManager systemConfigManager,
            String volumeSeparatorProduct) {

        this.projects = projects;
        this.exportsArchive = exportsArchive;
        this.salesInvoices = invoices;
        this.invoiceRebateManager = invoiceRebateManager;
        this.systemConfigManager = systemConfigManager;
        try {
            Long productId = Long.valueOf(systemConfigManager.getSystemProperty(volumeSeparatorProduct));
            this.vProduct = products.load(productId);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("<init> missing product defintion for system property '" 
                    + volumeSeparatorProduct + "', Service not available.");
            }
        }
    }

    /**
     * Method is invoked by {@link #createInvoice(SalesOrderVolumeExportConfig, DomainUser, List)} to check and add/ignore order positions to an invoice.
     * @param export
     * @param invoice
     * @param orders to check and add/ignore
     */
    protected abstract List<Item> addOrders(SalesOrderVolumeExport export, SalesInvoice invoice, List<Order> orders);

    public void execute(DomainUser user, SalesOrderVolumeExportConfig config, List<Order> orders) {

        if (log.isDebugEnabled()) {
            log.debug("execute() invoked [config=" + config.getId()
                    + ", company=" + ((config.getCompany() == null) ? "null" : config.getCompany().getId())
                    + ", customer: " + ((config.getCustomer() == null) ? "null" : config.getCustomer().getId())
                    + ",orderCount: " + ((orders == null) ? "null" : orders.size())
                    + "\nuser: " + ((user == null) ? "null" : user.getId()));
        }
        try {
            if (config.getCompany() != null && config.getCustomer() != null && orders != null && !orders.isEmpty()) {
                this.createInvoice(config, user, orders);
            }
        } catch (ClientException e) {
            log.error("execute() failed, no headquarter defined!", e);
        }
    }

    protected void createInvoice(SalesOrderVolumeExportConfig config, DomainUser user, List<Order> orders) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("createInvoice() invoked [user="
                    + user.getId()
                    + ", config=" + config.getId()
                    + ", ordersCount="
                    + orders.size()
                    + "]");
        }
        long duration = System.currentTimeMillis();
        BranchOffice headquarter = getHeadquarter(config);
        SalesInvoice invoice = createInvoice(user, headquarter, config.getCustomer());
        SalesOrderVolumeExports archive = getExportsArchive();
        SalesOrderVolumeExport export = archive.create(config, invoice);
        addOrders(export, invoice, orders);
        getInvoiceRebateManager().rebate(invoice);
        updateInvoiceStatus(user, invoice);
        duration = System.currentTimeMillis() - duration;
        if (log.isDebugEnabled()) {
            log.debug("createInvoice() done, duration (ms): "
                    + duration);
        }
    }

    /**
     * Creates a new invoice
     * @param user
     * @param headquarter
     * @param customer
     * @return new created invoice
     * @throws ClientException if validation of input failed
     */
    protected SalesInvoice createInvoice(DomainUser user, BranchOffice headquarter, Customer customer) throws ClientException {
        return salesInvoices.create(user.getEmployee(), headquarter, customer, null, null, false); 
    }

    /**
     * Adds an order separating product
     * @param invoice
     * @param order
     */
    protected void addOrderSeparator(SalesInvoice invoice, Order order, Long reference) {
        invoice.addItem(
                invoice.getCompany(),
                vProduct,
                null, // customName
                SalesConstants.VOLUME_PRODUCT_QUANTITY,
                SalesConstants.VOLUME_PRODUCT_PRICE,
                this.createVolumeText(order, reference),
                false,
                true,
                order.getId(),
                order.getCreated());
    }

    /**
     * Updates invoice status to changed after creating and adding all orders
     * @param user
     * @param invoice
     */
    protected void updateInvoiceStatus(DomainUser user, SalesInvoice invoice) {
        invoice.updateStatus(Order.STAT_CHANGED);
        salesInvoices.save(invoice);
        salesInvoices.createStatusHistory(invoice, invoice.getStatus(), user.getEmployee().getId());
    }

    /**
     * Fetches a project name
     * @param id
     * @return project name
     */
    protected String getProjectName(Long id) {
        return projects.getName(id);
    }

    /**
     * Provides the exports archive
     * @return the exportsArchive
     */
    protected SalesOrderVolumeExports getExportsArchive() {
        return exportsArchive;
    }

    /**
     * Provides the invoice rebate manager
     * @return the invoiceRebateManager
     */
    protected SalesInvoiceRebateManager getInvoiceRebateManager() {
        return invoiceRebateManager;
    }

    /**
     * Persists an invoice
     * @param invoice
     */
    protected void persist(SalesInvoice invoice) {
        salesInvoices.save(invoice);
    }

    /**
     * Creates the volume text
     * @param order
     * @return volume text with record number and date
     */
    private String createVolumeText(Order order, Long reference) {
        StringBuilder volumeText = new StringBuilder(Records.createNumber(
                order.getType(), order.getId(), order.isInternal()));
        volumeText.append(" / ").append(DateFormatter.getDate(order.getCreated()));
        if (reference != null) {
            volumeText.append(" / ").append(reference).append(" ").append(projects.getName(reference));
        }
        return volumeText.toString();
    }

    /**
     * Fetches the invoicing headquarter
     * @param config
     * @return headquarter
     * @throws ClientException
     */
    private BranchOffice getHeadquarter(SalesOrderVolumeExportConfig config) throws ClientException {
        return systemConfigManager.getHeadquarter(config.getCompany().getId());
    }
}
