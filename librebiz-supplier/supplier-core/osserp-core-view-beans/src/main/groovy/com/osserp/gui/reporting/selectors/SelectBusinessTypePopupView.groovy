/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 27, 2010 11:18:29 AM 
 * 
 */
package com.osserp.gui.reporting.selectors

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.servlet.http.HttpSession

import com.osserp.common.Entity
import com.osserp.common.PersistentEntity

import com.osserp.core.BusinessTypeManager
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author jg <jg@osserp.com>
 * 
 */
public class SelectBusinessTypePopupView extends CoreView  {
    private static Logger logger = LoggerFactory.getLogger(SelectBusinessTypePopupView.class.getName())

    def queryView

    SelectBusinessTypePopupView() {
        super()
        enablePopupView()
        headerName = 'selector.businessTypeTitle'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!list) {
            String viewName = form.getString('view')
            if (viewName && !env[viewName]) {
                logger.debug('initRequest: invalid context [message=query view not bound]')
                throw new ViewContextException(this)
            } else {
                queryView = env[viewName]
            }
            list = businessTypeManager.findTypeSelection(queryView.selectors['businessType'])?.types?.findAll { it.active }
            if (queryView.values.businessType) {
                for (obj in list) {
                    if ((obj instanceof Entity && obj.id == queryView.values.businessType)
                    || (obj instanceof PersistentEntity && obj.getPrimaryKey() == queryView.values.businessType)) {
                        bean = obj
                        break
                    }
                }
            }
        }
    }

    void createCustomNavigation() {
        nav.beanListNavigation = []
    }

    void save() {
        Long id = form.getLong('businessType')
        queryView.values.businessType = id
        queryView.reload()
    }

    protected BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }
}
