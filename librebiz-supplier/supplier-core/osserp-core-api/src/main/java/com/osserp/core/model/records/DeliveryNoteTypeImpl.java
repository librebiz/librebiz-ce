/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 20:10:27 
 * 
 */
package com.osserp.core.model.records;

import com.osserp.core.finance.DeliveryNoteType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DeliveryNoteTypeImpl extends AbstractBookingType implements DeliveryNoteType {

    private boolean sales = true;
    private boolean defaultType = false;
    private boolean correction = false;
    private boolean stockAffecting = false;
    private boolean customContent = false;
    private boolean parentContent = false;
    private boolean contentStrict = false;
    private boolean manualSelection = false;
    private boolean disabled = false;
    private Long productSelectionId;

    protected DeliveryNoteTypeImpl() {
        super();
    }

    public boolean isSales() {
        return sales;
    }

    protected void setSales(boolean sales) {
        this.sales = sales;
    }

    public boolean isDefaultType() {
        return defaultType;
    }

    protected void setDefaultType(boolean defaultType) {
        this.defaultType = defaultType;
    }

    public boolean isCorrection() {
        return correction;
    }

    protected void setCorrection(boolean correction) {
        this.correction = correction;
    }

    public boolean isCustomContent() {
        return customContent;
    }

    public void setCustomContent(boolean customContent) {
        this.customContent = customContent;
    }

    public boolean isStockAffecting() {
        return stockAffecting;
    }

    public void setStockAffecting(boolean stockAffecting) {
        this.stockAffecting = stockAffecting;
    }

    public boolean isParentContent() {
        return parentContent;
    }

    public void setParentContent(boolean parentContent) {
        this.parentContent = parentContent;
    }

    public boolean isContentStrict() {
        return contentStrict;
    }

    public void setContentStrict(boolean contentStrict) {
        this.contentStrict = contentStrict;
    }

    public boolean isManualSelection() {
        return manualSelection;
    }

    public void setManualSelection(boolean manualSelection) {
        this.manualSelection = manualSelection;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public Long getProductSelectionId() {
        return productSelectionId;
    }

    public void setProductSelectionId(Long productSelectionId) {
        this.productSelectionId = productSelectionId;
    }

}
