/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 10, 2008 
 * 
 */
package com.osserp.core.service.letters;

import java.util.Date;

import org.jdom2.Document;
import org.jdom2.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.OptionsCache;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterInfo;
import com.osserp.core.dms.LetterParameter;
import com.osserp.core.dms.LetterTemplate;
import com.osserp.core.employees.Employee;
import com.osserp.core.service.doc.XmlService;
import com.osserp.core.service.impl.AbstractOutputService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LetterOutputServiceImpl extends AbstractOutputService implements LetterOutputService {
    private static Logger log = LoggerFactory.getLogger(LetterOutputServiceImpl.class.getName());

    protected LetterOutputServiceImpl(
            ResourceLocator resourceLocator,
            OptionsCache optionsCache, 
            CoreStylesheetService stylesheetService, 
            XmlService xmlService) {
        super(resourceLocator, optionsCache, stylesheetService, xmlService);
    }

    public DocumentData createPdf(Employee user, LetterContent letter) {
        try {
            if (letter.getSalutation() == null) {
                letter.setSalutation(getDefaultSalutation(letter));
            }
            Document document = (letter instanceof LetterTemplate) ? 
                    createXml(user, (LetterTemplate) letter) :
                        createXml(user, (Letter) letter);
            String stylesheetName = letter.getType().getStylesheetName();
            byte[] result = stylesheetService.createPdf(document,
                    stylesheetService.getStylesheetSource(
                            stylesheetName,
                            letter.getBranchId(),
                            letter.getLocale()));
            return new DocumentData(result, 
                    "letter-" + letter.getId() +  ".pdf", 
                    Constants.MIME_TYPE_PDF);

        } catch (Throwable t) {
            log.error("createPdf() failed [message=" + t.getMessage() + "]", t);
        }
        return null;
    }
    

    public String getDefaultSalutation(LetterContent letter) {
        return getResourceString("defaultSalutation", letter.getLanguage());
    }

    protected Document createXml(Employee user, Letter letter) {
        Element root = xmlService.createLetter(user, letter, "letter");
        Date printDate = letter.getPrintDate() == null ? new Date(System.currentTimeMillis()) : letter.getPrintDate();
        root.addContent(new Element("printDate").setText(DateFormatter.getDate(printDate)));
        root.addContent(new Element("businessId").setText(createString(letter.getBusinessId())));
        Element infos = new Element("infos");
        for (int i = 0, j = letter.getInfos().size(); i < j; i++) {
            LetterInfo next = letter.getInfos().get(i);
            if (!next.isIgnore()) {
                String label = getResourceString(next.getName());
                if (isSet(label) && isSet(next.getValue())) {
                    Element info = new Element("info");
                    info.addContent(new Element("label").setText(label));
                    info.addContent(new Element("value").setText(next.getValue()));
                    infos.addContent(info);
                }
            }
        }
        root.addContent(infos);
        Element parameters = new Element("parameters");
        for (int i = 0, j = letter.getParameters().size(); i < j; i++) {
            LetterParameter next = letter.getParameters().get(i);
            if (isSet(next.getName()) && isSet(next.getValue())) {
                Element param = new Element(next.getName()).setText(next.getValue());
                parameters.addContent(param);
            }
        }
        root.addContent(parameters);
        Document doc = xmlService.createDocument(user);
        doc.getRootElement().addContent(root);
        if (log.isDebugEnabled()) {
            log.debug("createLetterXml() done, value:\n" + JDOMUtil.getPrettyString(doc));
        }
        return doc;
    }

    protected Document createXml(Employee user, LetterTemplate letter) {
        Element root = xmlService.createLetter(user, letter, "letter");
        root.addContent(new Element("printDate").setText(DateFormatter.getDate(new Date(System.currentTimeMillis()))));

        Document doc = xmlService.createDocument(user);
        doc.getRootElement().addContent(root);
        if (log.isDebugEnabled()) {
            log.debug("createXml() done, value:\n" + JDOMUtil.getPrettyString(doc));
        }
        return doc;
    }
}
