/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 25, 2005 
 * 
 */
package com.osserp.core.model.contacts.groups;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.customers.CustomerSummary;
import com.osserp.core.customers.SalesSummary;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CustomerSummaryVO extends AbstractEntity implements CustomerSummary {
    private static final long serialVersionUID = 42L;

    private List<SalesSummary> requests = new ArrayList<SalesSummary>();
    private Integer requestCount = 0;
    private Integer canceledRequestCount = 0;

    private List<SalesSummary> sales = new ArrayList<SalesSummary>();
    private Integer salesCount = 0;
    private Integer openSalesCount = 0;
    private Integer canceledSalesCount = 0;

    protected CustomerSummaryVO() {
    }

    public CustomerSummaryVO(Long id) {
        super(id);
    }

    public List<SalesSummary> getRequests() {
        return requests;
    }

    public Integer getRequestCount() {
        return requestCount;
    }

    public Integer getCancelledRequestCount() {
        return canceledRequestCount;
    }

    protected void setRequets(List<SalesSummary> plans) {
        this.requests = plans;
    }

    public Long getLatestRequestId() {
        return getLatestId(requests);
    }

    public void addRequest(SalesSummary summary) {
        requests.add(summary);
        if (summary.isCancelled()) {
            canceledRequestCount = canceledRequestCount + 1;
        } else {
            requestCount = requestCount + 1;
        }
    }

    public List<SalesSummary> getSales() {
        return sales;
    }

    public Integer getSalesCount() {
        return salesCount;
    }

    public Integer getCancelledSalesCount() {
        return canceledSalesCount;
    }

    public Integer getOpenSalesCount() {
        return openSalesCount;
    }

    protected void setSales(List<SalesSummary> sales) {
        this.sales = sales;
    }

    public void addSales(SalesSummary summary) {
        sales.add(summary);
        if (summary.isCancelled()) {
            canceledSalesCount += 1;
        } else {
            salesCount += 1;
            if (summary.getStatus() < 100) {
                openSalesCount += 1;
            }
        }
    }

    public Long getLatestSalesId() {
        return getLatestId(sales);
    }

    protected final Long getLatestId(List<SalesSummary> list) {
        if (list.isEmpty()) {
            return null;
        }
        SalesSummary latest = null;
        SalesSummary latestOpen = null;
        SalesSummary latestNotCanceled = null;
        for (int i = 0, j = list.size(); i < j; i++) {
            SalesSummary next = list.get(i);
            if (latest == null || latest.getCreated().before(next.getCreated())) {
                latest = next;
            }
            if (!next.isCancelled()) {
                if (latestNotCanceled == null 
                        || latestNotCanceled.getCreated().before(next.getCreated())) {
                    latestNotCanceled = next;
                }
                if (!next.isClosed()) {
                    if (latestOpen == null 
                            || latestOpen.getCreated().before(next.getCreated())) {
                        latestOpen = next;
                    }
                }
            }
        }
        if (latestOpen != null) {
            return latestOpen.getId();
        }
        if (latestNotCanceled != null) {
            return latestNotCanceled.getId();
        }
        return latest == null || latest.isCancelled() ? null : latest.getId();
    }

    public final Long getLatestCancelledRequestId() {
        if (requests.isEmpty()) {
            return null;
        }
        SalesSummary sum = null;
        for (int i = 0, j = requests.size(); i < j; i++) {
            SalesSummary next = requests.get(i);
            if (next.isCancelled()) {
                if (sum == null || sum.getCreated().before(next.getCreated())) {
                    sum = next;
                }
            }
        }
        return (sum == null ? null : sum.getId());
    }

    public final Long getLatestCancelledSalesId() {
        if (sales.isEmpty()) {
            return null;
        }
        SalesSummary sum = null;
        for (int i = 0, j = sales.size(); i < j; i++) {
            SalesSummary next = sales.get(i);
            if (next.isCancelled()) {
                if (sum == null || sum.getCreated().before(next.getCreated())) {
                    sum = next;
                }
            }
        }
        return (sum == null ? null : sum.getId());
    }

}
