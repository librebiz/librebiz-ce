/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 15-Apr-2009 07:53:49 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.finance.DeliveryCondition;

/**
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public class DeliveryConditionImpl extends AbstractOption implements DeliveryCondition {

    protected DeliveryConditionImpl() {
        super();
    }

    protected DeliveryConditionImpl(Long id, String name) {
        super(id, name);
    }

    protected DeliveryConditionImpl(Long id, String name, String description) {
        super(id, name, description);
    }

    protected DeliveryConditionImpl(Long id, String name, String description, String resourceKey) {
        super(id, name, description, resourceKey);
    }

}
