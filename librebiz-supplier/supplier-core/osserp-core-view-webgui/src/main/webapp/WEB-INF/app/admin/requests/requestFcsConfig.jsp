<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestFcsConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
        <c:choose>
            <c:when test="${empty view.bean && view.comparisonMode}">
                - <fmt:message key="fcsActionConfigComparison"/>
            </c:when>
            <c:when test="${!empty view.selectedType.name}">
                - <o:out value="${view.selectedType.name}"/>
            </c:when>
            <c:otherwise>
                - <fmt:message key="selectionOrderType"/>
            </c:otherwise>
        </c:choose>
        
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string" >
        <div class="content-area" id="fcsConfigContent">
            <div class="row">
                <c:choose>
                    <c:when test="${empty view.selectedType}">
                        <c:set var="businessTypeSelection" scope="request" value="${view.businessTypes}"/>
                        <c:set var="businessTypeSelectionUrl" scope="request" value="${view.baseLink}/selectBusinessType"/>
                        <c:import url="${viewdir}/common/_businessTypeSelectingList.jsp"/>
                    </c:when>
                    <c:when test="${view.comparisonMode}">
                        <c:import url="_requestFcsConfigComparison.jsp"/>
                    </c:when>
                    <c:when test="${view.editMode}">
                        <c:import url="_requestFcsConfigEdit.jsp"/>
                    </c:when>
				    <c:otherwise>
                        <c:import url="_requestFcsConfigList.jsp"/>
                    </c:otherwise>
                 </c:choose>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
