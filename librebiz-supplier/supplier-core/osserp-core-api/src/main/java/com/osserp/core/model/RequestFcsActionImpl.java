/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 22-Feb-2007 13:16:46 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.jdom2.Element;

import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.Dependency;
import com.osserp.core.events.EventConfigType;
import com.osserp.core.requests.RequestFcsAction;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class RequestFcsActionImpl extends AbstractFcsAction implements RequestFcsAction {

    private boolean transfering = false;
    private boolean managerSelecting = false;
    private boolean salesSelecting = false;
    private Set<Long> businessTypeExcludes = new HashSet<Long>();
    private List<RequestFcsActionExclude> excludes = new ArrayList<RequestFcsActionExclude>();

    protected RequestFcsActionImpl() {
        super();
        setEventConfigId(EventConfigType.REQUEST_FCS_EVENT);
    }

    public RequestFcsActionImpl(String name, String description, Long groupId, boolean displayEverytime, Long status) {
        super(name, groupId, description, EventConfigType.REQUEST_FCS_EVENT, displayEverytime);
        if (isSet(status)) {
            setStatus(status.intValue());
        }
    }

    public RequestFcsActionImpl(BusinessType type, RequestFcsAction source) {
        super(
                source.getName(),
                source.getGroupId(),
                source.getDescription(),
                source.getStatus(),
                source.getEventConfigId(),
                source.isDisplayEverytime(),
                source.isNoteRequired(),
                source.isPerformAction(),
                source.isPerformOnStart(),
                source.getStartActionName(),
                source.isPerformAtEnd(),
                source.getEndActionName(),
                source.isThrowable(),
                source.isCancelling(),
                source.isStopping(),
                source.isDisplayReverse(),
                source.isEndOfLife(),
                source.isTriggered());
        transfering = source.isTransfering();
        managerSelecting = source.isManagerSelecting();
        salesSelecting = source.isSalesSelecting();
    }

    public boolean isTransfering() {
        return transfering;
    }

    public void setTransfering(boolean transfering) {
        this.transfering = transfering;
    }

    public boolean isManagerSelecting() {
        return managerSelecting;
    }

    public void setManagerSelecting(boolean managerSelecting) {
        this.managerSelecting = managerSelecting;
    }

    public boolean isSalesSelecting() {
        return salesSelecting;
    }

    public void setSalesSelecting(boolean salesSelecting) {
        this.salesSelecting = salesSelecting;
    }

    public Set<Long> getBusinessTypeExcludes() {
        if (businessTypeExcludes.isEmpty() && !excludes.isEmpty()) {
            for (int i = 0, j = excludes.size(); i < j; i++) {
                RequestFcsActionExclude next = excludes.get(i);
                businessTypeExcludes.add(next.getBusinessTypeId());
            }
        }
        return businessTypeExcludes;
    }

    public void addBusinessTypeExclude(Long businessTypeId) {
        Set<Long> currentBusinessTypes = getBusinessTypeExcludes();
        if (!currentBusinessTypes.contains(businessTypeId)) {
            excludes.add(new RequestFcsActionExclude(this, businessTypeId));
            businessTypeExcludes.clear();
        }
    }

    public void removeBusinessTypeExclude(Long businessTypeId) {
        for (Iterator<RequestFcsActionExclude> i = excludes.iterator(); i.hasNext();) {
            RequestFcsActionExclude next = i.next();
            if (next.getBusinessTypeId().equals(businessTypeId)) {
                i.remove();
                break;
            }
        }
    }

    protected List<RequestFcsActionExclude> getExcludes() {
        return excludes;
    }

    protected void setExcludes(List<RequestFcsActionExclude> excludes) {
        this.excludes = excludes;
    }

    @Override
    protected Dependency createDependency(Long dependency) {
        return new RequestFcsDependencyImpl(getId(), dependency);
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("transfering", transfering));
        root.addContent(JDOMUtil.createElement("managerSelecting", managerSelecting));
        root.addContent(JDOMUtil.createElement("salesSelecting", salesSelecting));
        return root;
    }
}
