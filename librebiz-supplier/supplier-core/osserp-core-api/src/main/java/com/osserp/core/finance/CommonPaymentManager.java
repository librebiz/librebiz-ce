/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Feb-2013 07:15:22 
 * 
 */
package com.osserp.core.finance;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CommonPaymentManager extends PaymentManager {

    /**
     * Provides all billingTypes supported by paymentManager implementation.
     * @return supported billing types
     */
    List<BillingType> getSupportedTypes();

    /**
     * Finds all billingTypes supported by provided supplier
     * @return supported billing types of supplier
     */
    List<BillingType> findSupportedTypes(Supplier supplier);

    /**
     * Creates a common payment
     * @param user
     * @param contact
     * @param branch
     * @param bankAccount
     * @param paymentType
     * @param currency
     * @param amount
     * @param recordDate optional, replaces created if provided
     * @param paid
     * @param note
     * @param reducedTax
     * @param taxFree
     * @param taxFreeId
     * @param template marks record as template 
     * @param templateName
     * @return new created payment
     * @throws ClientException if input validation failed
     */
    CommonPayment create(
        Employee user,
        ClassifiedContact contact,
        BranchOffice branch,
        BankAccount bankAccount,
        Long paymentType,
        Long currency,
        BigDecimal amount,
        Date recordDate,
        Date paid,
        String note,
        boolean reducedTax,
        boolean taxFree, 
        Long taxFreeId,
        boolean template,
        String templateName) throws ClientException;

    /**
     * Updates the data of an existing payment
     * @param user
     * @param payment the payment to update
     * @param paymentType
     * @param currency
     * @param amount
     * @param recordDate
     * @param paid
     * @param note
     * @param reducedTax
     * @param taxFree
     * @param taxFreeId
     * @param template
     * @param templateName
     * @return updated payment
     */
    public CommonPayment update(
            Employee user,
            CommonPayment payment,
            Long paymentType,
            Long currency,
            BigDecimal amount,
            Date recordDate,
            Date paid,
            String note,
            boolean reducedTax,
            boolean taxFree, 
            Long taxFreeId,
            boolean template,
            String templateName);
    

    /**
     * Updates the document id of an existing payment
     * @param user
     * @param payment the payment to update
     * @param id the primary key of the document
     * @return updated payment
     */
    CommonPayment updateDocumentReference(Employee user, CommonPayment payment, Long id);
    
    /**
     * Provides all payments set as template 
     * @return templates
     */
    List<CommonPayment> getTemplates();
    
    /**
     * Disables a payment template assignment
     * @param template
     */
    void disableTemplate(CommonPayment template);
}
