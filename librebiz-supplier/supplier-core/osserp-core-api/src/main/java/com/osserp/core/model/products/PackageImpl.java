/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Sep-2006 08:37:40 
 * 
 */
package com.osserp.core.model.products;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.products.Product;
import com.osserp.core.products.ProductDetails;
import com.osserp.core.products.Package;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PackageImpl extends ProductDetailsImpl implements Package {

    private Double extraChargePercent = Constants.DOUBLE_NULL;
    private Double extraChargePerUnit = Constants.DOUBLE_NULL;
    private boolean selfMounting = false;
    private boolean extension = false;
    private boolean exchange = false;
    private boolean warrantyService = false;
    private boolean freeService = false;

    protected PackageImpl() {
        super();
    }

    protected PackageImpl(ProductDetails other) {
        super((ProductDetailsImpl) other);
        if (other != null) {
            deployValues();
            /*
             * TODO remove when finished this.extraChargePercent = other.getExtraChargePercent(); this.extraChargePerUnit = other.getExtraChargePerUnit();
             * this.selfMounting = other.isSelfMounting(); this.extension = other.isExtension(); this.exchange = other.isExchange(); this.warrantyService =
             * other.isWarrantyService(); this.freeService = other.isFreeService();
             */
        }
    }

    protected PackageImpl(Product product, Package other) {
        super((ProductDetailsImpl) other);
        if (other != null) {
            setReference(product.getProductId());
            deployValues();
            /*
             * TODO remove when finished this.extraChargePercent = other.getExtraChargePercent(); this.extraChargePerUnit = other.getExtraChargePerUnit();
             * this.selfMounting = other.isSelfMounting(); this.extension = other.isExtension(); this.exchange = other.isExchange(); this.warrantyService =
             * other.isWarrantyService(); this.freeService = other.isFreeService();
             */
        }
    }

    public boolean isExtraChargeAvailable() {
        return (isExtraChargePercentAvailable() || isExtraChargePerUnitAvailable());
    }

    public boolean isExtraChargePercentAvailable() {
        return (extraChargePercent != null && extraChargePercent != 0);
    }

    public Double getExtraChargePercent() {
        return extraChargePercent;
    }

    public void setExtraChargePercent(Double extraChargePercent) {
        addProperty(Constants.SYSTEM_USER, "extraChargePercent", extraChargePercent);
        deployValues();
    }

    public boolean isExtraChargePerUnitAvailable() {
        return (extraChargePerUnit != null && extraChargePerUnit != 0);
    }

    public Double getExtraChargePerUnit() {
        return extraChargePerUnit;
    }

    public void setExtraChargePerUnit(Double extraChargePerUnit) {
        addProperty(Constants.SYSTEM_USER, "extraChargePerUnit", extraChargePerUnit);
        deployValues();
    }

    public boolean isSelfMounting() {
        return selfMounting;
    }

    public void setSelfMounting(boolean selfMounting) {
        addProperty(Constants.SYSTEM_USER, "selfMounting", selfMounting);
        deployValues();
    }

    public boolean isExtension() {
        return extension;
    }

    public void setExtension(boolean extension) {
        addProperty(Constants.SYSTEM_USER, "extension", extension);
        deployValues();
    }

    public boolean isExchange() {
        return exchange;
    }

    public void setExchange(boolean exchange) {
        addProperty(Constants.SYSTEM_USER, "exchange", exchange);
        deployValues();
    }

    public boolean isWarrantyService() {
        return warrantyService;
    }

    public void setWarrantyService(boolean warrantyService) {
        addProperty(Constants.SYSTEM_USER, "warrantyService", warrantyService);
        deployValues();
    }

    public boolean isFreeService() {
        return freeService;
    }

    public void setFreeService(boolean freeService) {
        addProperty(Constants.SYSTEM_USER, "freeService", freeService);
        deployValues();
    }

    protected void deployValues() {
        if (getPropertyMap().containsKey("extraChargePercent")) {
            this.extraChargePercent = getPropertyMap().get("extraChargePercent").getAsDouble();
        }
        if (getPropertyMap().containsKey("extraChargePerUnit")) {
            this.extraChargePerUnit = getPropertyMap().get("extraChargePerUnit").getAsDouble();
        }
        if (getPropertyMap().containsKey("selfMounting")) {
            this.selfMounting = getPropertyMap().get("selfMounting").isEnabled();
        }
        if (getPropertyMap().containsKey("extension")) {
            this.extension = getPropertyMap().get("extension").isEnabled();
        }
        if (getPropertyMap().containsKey("exchange")) {
            this.exchange = getPropertyMap().get("exchange").isEnabled();
        }
        if (getPropertyMap().containsKey("warrantyService")) {
            this.warrantyService = getPropertyMap().get("warrantyService").isEnabled();
        }
        if (getPropertyMap().containsKey("freeService")) {
            this.freeService = getPropertyMap().get("freeService").isEnabled();
        }
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("extraChargePercent", extraChargePercent));
        root.addContent(JDOMUtil.createElement("extraChargePerUnit", extraChargePerUnit));
        root.addContent(JDOMUtil.createElement("selfMounting", selfMounting));
        root.addContent(JDOMUtil.createElement("extension", extension));
        root.addContent(JDOMUtil.createElement("exchange", exchange));
        root.addContent(JDOMUtil.createElement("warrantyService", warrantyService));
        root.addContent(JDOMUtil.createElement("freeService", freeService));
        return root;
    }
}
