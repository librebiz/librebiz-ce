/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2012 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.Status;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.Options;
import com.osserp.core.crm.Campaign;
import com.osserp.core.dao.BusinessCaseUtilities;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.dao.RequestFcsActions;
import com.osserp.core.model.BusinessStatusImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessCaseUtilitiesDao extends AbstractBusinessCaseContextDao implements BusinessCaseUtilities {
    private static Logger log = LoggerFactory.getLogger(BusinessCaseUtilitiesDao.class.getName());

    private RequestFcsActions requestFcsActions;
    private FlowControlActions flowControlActions;
    
    protected BusinessCaseUtilitiesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache,
            RequestFcsActions requestFcsActions,
            FlowControlActions flowControlActions) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache);
        this.flowControlActions = flowControlActions;
        this.requestFcsActions = requestFcsActions;
    }
    
    public Status getBusinessStatusDefault() {
        try {
            return (Status) getCurrentSession().load(BusinessStatusImpl.class.getName(), 0L);
        } catch (Exception e) {
            log.warn("getBusinessStatusDefault() failed on attempt to load default business status with id 0 [message="
                    + e.getMessage() + "]");
            throw new BackendException("business.status.missing.0", e);
        }
    }

    public List<Status> getBusinessStatusList() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(BusinessStatusImpl.class.getName()).append(" o order by o.id");
        try {
            return getCurrentSession().createQuery(hql.toString()).list();
        } catch (Throwable t) {
            log.error("getBusinessStatusList() failed [message=" + t.getMessage() + "]", t);
            throw new BackendException(t);
        }
    }

    public Campaign fetchCampaign(Long id, String name, String createNonExistingPropertyName, String defaultCampaignPropertyName) {
        Campaign result = (Campaign) fetchOption(Options.CAMPAIGNS, id, name, createNonExistingPropertyName);
        if (result == null && defaultCampaignPropertyName != null) {
            id = getPropertyId(defaultCampaignPropertyName);
            if (id != null) {
                result = (Campaign) fetchOption(Options.CAMPAIGNS, id, null, createNonExistingPropertyName);
                if (result == null) {
                    log.warn("fetchCampaign() found invalid default [property=" + defaultCampaignPropertyName
                            + ", value=" + id + "]");
                } else if (log.isDebugEnabled()) {
                    log.debug("fetchCampaign() using default while non provided [id=" + result.getId() + "]");
                }
            }
        }
        return result;
    }
    
    public Option fetchOrigin(Long id, String name, String createNonExistingPropertyName) {
        return fetchOption(Options.REQUEST_ORIGIN_TYPES, id, name, createNonExistingPropertyName);
    }
    
    public FcsAction fetchFcsAction(String context, BusinessType businessType, Long id, String name) {
        FcsAction action = null;
        if ("sales".equals(context)) {
            action = fetchSalesFcsAction(businessType, id, name);
        } else {
            action = fetchRequestFcsAction(businessType, id, name);
        }
        if (log.isDebugEnabled()) {
            if (action != null) {
                log.debug("fetchFcsAction() done [context=" + context + 
                        ", action=" + action.getId() + 
                        ", import=" + action.isImportAction() + "]");
            } else {
                log.debug("fetchFcsAction() not found [context=" + context + 
                        ", type=" + businessType.getId() + 
                        ", id=" + id + ", name=" + name + "]");
            }
        }
        return action;
    }
    
    public FcsAction fetchRequestFcsAction(BusinessType businessType, Long id, String name) {
        return requestFcsActions.fetchAction(businessType, id, name);
    }
    
    public FcsAction fetchSalesFcsAction(BusinessType businessType, Long id, String name) {
        return flowControlActions.fetchAction(businessType, id, name);
    }
    
    public boolean isPropertyEnabled(String property) {
        return systemPropertyEnabled(property);
    }

    public BusinessType fetchRequestType(String externalStatus) {
        List<BusinessType> types = new ArrayList(getOptions(Options.REQUEST_TYPES));
        for (int i = 0, j = types.size(); i < j; i++) {
            BusinessType next = types.get(i);
            if (next.isMatchingExternalStatus(externalStatus)) {
                return next;
            }
        }
        return null;
    }
}
