<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
	<table class="table table-striped">
		<thead>
			<tr>
				<th class="recordType"><fmt:message key="type" /></th>
				<th class="recordNumber"><fmt:message key="number" /></th>
				<th class="recordDate"><v:sortLink key="created"><fmt:message key="ofDate"/></v:sortLink></th>
				<th class="recordNumber"><fmt:message key="referenceNumberShort" /></th>
				<th class="recordAmount"><fmt:message key="amount" /></th>
				<th class="recordVat"><fmt:message key="turnoverTax1" /></th>
				<th class="recordVat"><fmt:message key="turnoverTax2" /></th>
				<th class="recordAmount"><fmt:message key="total" /></th>
				<th class="recordAction"> </th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty view.list}">
           			<tr>
               			<td colspan="9">
                   			<span><fmt:message key="noRecordsAvailable" /></span> 
                   			<span style="margin-left: 20px;"> 
                   				<v:link url="/suppliers/supplierPurchaseRecords/enableCreateMode">[<fmt:message key="create" />]</v:link>
                   			</span>
               			</td>
           			</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="record" items="${view.list}" varStatus="s">
						<tr<c:if test="${record.payment}"> class="altrow"</c:if>>
							<c:choose>
								<c:when test="${record.canceled}">
									<td class="recordType canceled"><o:out value="${record.type.numberPrefix}"/></td>
									<td class="recordNumber canceled">
										<a	href="<c:url value="/purchaseInvoice.do?method=display&exit=supplier&readonly=true&id=${record.id}"/>"><o:out value="${record.number}" /></a>
									</td>
									<td class="recordDate canceled"><o:date value="${record.created}" /></td>
									<td class="recordNumber canceled"><o:out value="${record.reference}" /></td>
									<td class="recordAmount canceled"><o:number value="${record.amount}" format="currency" /></td>
									<c:choose>
										<c:when test="${record.taxFree}">
											<td class="recordVat canceled"><o:number value="0.00" format="currency" /></td>
											<td class="recordVat canceled"><o:number value="0.00" format="currency" /></td>
											<td class="recordAmount canceled"><o:number value="${record.amount}" format="currency" /></td>
										</c:when>
										<c:otherwise>
											<td class="recordVat canceled"><o:number value="${record.tax}" format="currency" /></td>
											<td class="recordVat canceled"><o:number value="${record.reducedTax}" format="currency" /></td>
											<td class="recordAmount canceled"><o:number value="${record.gross}" format="currency" /></td>
										</c:otherwise>
									</c:choose>
									<td class="recordAction"><c:if test="${record.unchangeable}"><v:link url="/records/recordPrint/print?name=purchaseInvoice&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link></c:if></td>
								</c:when>
								<c:when test="${record.payment}">
									<td class="recordType"><o:out value="${record.type.numberPrefix}"/></td>
									<td class="recordNumber"><a  href="<c:url value="/purchaseInvoice.do?method=display&exit=supplier&readonly=true&id=${record.reference}"/>"><o:out value="${record.referenceNumber}" /></a></td>
									<td class="recordDate"><o:date value="${record.created}" /></td>
									<td colspan="4">&nbsp;</td>
									<td class="recordAmount"><o:number value="${record.gross}" format="currency" /></td>
                                    <td class="recordAction"> </td>
								</c:when>
								<c:otherwise>
									<td class="recordType"><o:out value="${record.type.numberPrefix}"/></td>
									<c:choose>
										<c:when test="${record.unchangeable}">
											<td class="recordNumber">
												<a href="<c:url value="/purchaseInvoice.do?method=display&exit=supplier&readonly=true&id=${record.id}"/>"><o:out value="${record.number}" /></a>
											</td>
										</c:when>
										<c:otherwise>
											<td class="recordNumber">
                                                <a href="<c:url value="/purchaseInvoice.do?method=display&exit=supplier&readonly=false&id=${record.id}"/>"><o:out value="${record.number}" /></a>
											</td>
										</c:otherwise>
									</c:choose>
									<td class="recordDate"><o:date value="${record.created}" /></td>
									<td class="recordNumber"><o:out value="${record.reference}" /></td>
									<td class="recordAmount"><o:number value="${record.amount}" format="currency" /></td>
									<c:choose>
										<c:when test="${record.taxFree}">
											<td class="recordVat"><o:number value="0.00" format="currency" /></td>
											<td class="recordVat"><o:number value="0.00" format="currency" /></td>
											<td class="recordAmount">
                                                <span<c:if test="${view.purchasePaymentWarnings and record.amountOpen}"> style="font-style: italic;"</c:if>>
                                                    <o:number value="${record.amount}" format="currency" />
                                                </span>
                                            </td>
										</c:when>
										<c:otherwise>
											<td class="recordVat"><o:number value="${record.tax}" format="currency" /></td>
											<td class="recordVat"><o:number value="${record.reducedTax}" format="currency" /></td>
                                            <td class="recordAmount">
                                                <span<c:if test="${view.purchasePaymentWarnings and record.amountOpen}"> class="error"</c:if>>
                                                    <o:number value="${record.gross}" format="currency" />
                                                </span>
                                            </td>
										</c:otherwise>
									</c:choose>
									<td class="recordAction"><c:if test="${record.unchangeable}"><v:link url="/records/recordPrint/print?name=purchaseInvoice&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link></c:if></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
					<tr>
						<th colspan="7"><fmt:message key="balance" /></th>
						<th class="recordAmount"><o:number value="${view.accountBalance}" format="currency"/></th>
						<th class="recordAction"> </th>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>

