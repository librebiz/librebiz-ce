/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 29, 2011 1:57:30 PM 
 * 
 */
package com.osserp.core.model.calc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.BusinessType;
import com.osserp.core.calc.CalculationTemplate;
import com.osserp.core.calc.CalculationTemplateItem;
import com.osserp.core.products.Product;

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 */
public class CalculationTemplateImpl extends AbstractOption implements CalculationTemplate {

    Product product = null;
    BusinessType businessType = null;
    List<CalculationTemplateItem> items = new ArrayList<CalculationTemplateItem>();

    protected CalculationTemplateImpl() {
        super();
    }

    public CalculationTemplateImpl(String name, Product product, BusinessType businessType) {
        super(name);
        this.product = product;
        this.businessType = businessType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public List<CalculationTemplateItem> getItems() {
        return items;
    }

    public void setItems(List<CalculationTemplateItem> items) {
        this.items = items;
    }

    public void addItem(CalculationTemplateItem item) {
        this.items.add(item);
    }

    public void removeItem(Long itemId) {
        for (Iterator<CalculationTemplateItem> cti = items.iterator(); cti.hasNext();) {
            CalculationTemplateItem next = cti.next();
            if (next.getId().equals(itemId)) {
                cti.remove();
                break;
            }
        }
    }

    public void toggleOptionalItem(Long itemId) {
        for (Iterator<CalculationTemplateItem> cti = items.iterator(); cti.hasNext();) {
            CalculationTemplateItem next = cti.next();
            if (next.getId().equals(itemId)) {
                next.setOptional(!next.isOptional());
                break;
            }
        }
    }

    public boolean isAlreadyAdded(Product product) {
        if (product == null) {
            return false;
        }
        for (int i = 0; i < items.size(); i++) {
            CalculationTemplateItem item = items.get(i);
            if (item.getProduct() != null && product.getProductId().equals(item.getProduct().getProductId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object clone() {
        throw new IllegalStateException("clone currently not supported");
    }
}
