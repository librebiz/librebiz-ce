/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.projects;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordType;
import com.osserp.core.projects.ProjectSupplier;
import com.osserp.core.sales.Sales;
import com.osserp.core.suppliers.Supplier;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectSupplierImpl extends AbstractEntity implements ProjectSupplier {

    private Date deliveryDate;
    private Date mountingDate;
    private Double mountingDays = 0d;
    private String groupName = null;
    private Long productGroupId = null;
    private Long productId = null;
    private Long recordId = null;
    private Long recordTypeId = null;
    private Supplier supplier = null;
    private Long status = 0L;

    private transient Record voucher;
    private transient RecordType voucherType;

    /**
     * Creates an empty installer instance required to implement Serializable
     */
    protected ProjectSupplierImpl() {
        super();
    }

    /**
     * Creates a new project supplier relation
     * @param user
     * @param businessCase
     * @param supplier
     * @param groupName
     * @param deliveryDate
     */
    public ProjectSupplierImpl(
            Employee user,
            BusinessCase businessCase,
            Supplier supplier,
            String groupName,
            Date deliveryDate) {
        super((Long) null, ((businessCase instanceof Sales) ? ((Sales) businessCase).getRequest().getRequestId() :
            businessCase.getPrimaryKey()), (user == null ? null : user.getId()));
        this.deliveryDate = deliveryDate;
        this.groupName = groupName;
        this.supplier = supplier;
    }

    /**
     * Creates a new project supplier relation
     * @param supplier
     * @param created
     * @param createdBy
     * @param businessCase
     * @param recordId
     * @param recordTypeId
     * @param groupName
     * @param deliveryDate
     */
    public ProjectSupplierImpl(
            Supplier supplier,
            Date created,
            Long createdBy,
            Long businessCase,
            Long recordId,
            Long recordTypeId,
            String groupName,
            Date deliveryDate) {
        super((Long) null, businessCase, created, createdBy);
        this.supplier = supplier;
        this.recordId = recordId;
        this.recordTypeId = recordTypeId;
        this.groupName = groupName;
        this.deliveryDate = deliveryDate;
    }

    public String getLabel() {
        if (isSet(groupName)) {
            return groupName;
        }
        if (voucher != null && !voucher.getItems().isEmpty()) {
            Item item = voucher.getItems().get(0);
            if (item != null && item.getProduct() != null) {
                return item.getProduct().getGroup().getName();
            }
        }
        if (isSet(recordId)) {
            return recordId.toString();
        }
        return "n/a";
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getMountingDate() {
        return mountingDate;
    }

    public void setMountingDate(Date mountingDate) {
        this.mountingDate = mountingDate;
    }

    public Double getMountingDays() {
        return mountingDays;
    }

    public void setMountingDays(Double mountingDays) {
        this.mountingDays = mountingDays;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getProductGroupId() {
        return productGroupId;
    }

    public void setProductGroupId(Long productGroupId) {
        this.productGroupId = productGroupId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(Long recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public boolean isPurchaseOrderAssigned() {
        return recordId != null && RecordType.PURCHASE_ORDER.equals(recordTypeId);
    }

    public boolean isPurchaseInvoiceAssigned() {
        return recordId != null && RecordType.PURCHASE_INVOICE.equals(recordTypeId);
    }

    public void update(
            Long changedBy,
            String groupName,
            Date deliveryDate,
            Date mountingDate,
            Double mountingDays,
            Long status) {
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(changedBy);
        this.groupName = groupName;
        this.deliveryDate = deliveryDate;
        if (mountingDays == null || mountingDays == 0d) {
            this.mountingDate = null;
            this.mountingDays = 0d;
        } else {
            this.mountingDate = mountingDate;
            this.mountingDays = mountingDays;
        }
        if (status != null) {
            this.status = status;
        }
    }

    public void update(Long changedBy, Supplier supplier) {
        setCreated(new Date(System.currentTimeMillis()));
        setCreatedBy(changedBy);
        setSupplier(supplier);
    }

    public void assignVoucher(Long id, Long typeId) {
        this.recordId = id;
        this.recordTypeId = typeId;
    }

    public void resetVoucher() {
        assignVoucher(null, null);
        deliveryDate = null;
    }

    public RecordType getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(RecordType voucherType) {
        this.voucherType = voucherType;
    }

    public Record getVoucher() {
        return voucher;
    }

    public void setVoucher(Record voucher) {
        this.voucher = voucher;
        if (this.voucher != null) {
            this.voucherType = this.voucher.getType();
        }
    }
}
