/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 24, 2008 7:42:13 PM 
 * 
 */
package com.osserp.gui.web.products;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;

import com.osserp.gui.client.products.StockReportView;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 */
public class StockReportAction extends AbstractLazyIgnoringAction {

    private static Logger log = LoggerFactory.getLogger(StockReportAction.class.getName());

    @Override
    protected Class<StockReportView> getViewClass() {
        return StockReportView.class;
    }

    @Override
    public ActionForward redisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("redisplay() request from " + getUser(session).getId());
        }
        StockReportView view = getStockReportView(session);
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward forwardGroup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardGroup() request from " + getUser(session).getId());
        }
        StockReportView view = (StockReportView) fetchView(session);
        if (view == null) {
            view = (StockReportView) createView(request);
        }
        Long id = RequestUtil.getId(request);
        view.loadProductGroup(id);
        String exit = RequestUtil.getExit(request);
        if (exit != null) {
            view.setExitTarget(exit);
        }
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward filter(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("filter() request from " + getUser(session).getId());
        }
        StockReportView view = getStockReportView(session);

        Long categoryId = RequestUtil.fetchLong(request, Form.CATEGORY_ID);
        Long groupId = RequestUtil.fetchLong(request, Form.GROUP_ID);
        Long typeId = RequestUtil.fetchLong(request, Form.TYPE_ID);
        if (categoryId != null && categoryId != 0) {
            view.loadProductCategory(categoryId);
        } else if (groupId != null && groupId != 0) {
            view.loadProductGroup(groupId);
        } else if (typeId != null && typeId != 0) {
            view.loadProductType(typeId);
        }

        Long stockId = RequestUtil.fetchLong(request, Form.STOCK_ID);
        if (stockId != null) {
            view.selectStock(stockId);
        }
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward refresh(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("refresh() request from " + getUser(session).getId());
        }
        StockReportView view = getStockReportView(session);
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward toggleListVacantMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleListVacantMode() request from " + getUser(session).getId());
        }
        StockReportView view = getStockReportView(session);
        view.toggleListVacantMode();
        return mapping.findForward(view.getActionTarget());
    }

    private StockReportView getStockReportView(HttpSession session) {
        StockReportView view = fetchStockReportView(session);
        if (view == null) {
            log.warn("getStockReportView() view not bound!");
            throw new ActionException();
        }
        return view;
    }

    private StockReportView fetchStockReportView(HttpSession session) {
        return (StockReportView) fetchView(session);
    }
}
