<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="classificationConfigsView" />

<div class="col-md-4 panel-area panel-area-default">

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="table-responsive">
                <table class="table table-head">
                    <tbody>
                        <tr>
                            <td><h4><fmt:message key="productType" /></h4></td>
                            <td class="action">
                                <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductTypeAdd">
                                    <v:ajaxLink url="/admin/products/classificationConfigCreator/forward?mode=type" title="createProductType" linkId="createType">
                                        <o:img name="newIcon" styleClass="bigicon" />
                                    </v:ajaxLink>
                                </o:permission>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-body">

        <div class="table-responsive table-responsive-default">
            <table class="table">
                <tbody>
                    <c:if test="${!empty view.bean}">

                        <c:choose>
                            <c:when test="${empty view.bean.groups}">
                                <tr class="altrow">
                                    <td class="boldtext"><o:out value="${view.bean.name}" /></td>
                                    <td class="action right">
                                        <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductGroupMoveDelete">
                                            <v:ajaxLink url="/admin/products/productTypeEditor/forward" linkId="productTypeEditor">
                                                <o:img name="writeIcon" styleClass="bigicon" />
                                            </v:ajaxLink>
                                            <v:link url="/admin/products/classificationConfigs/removeType" confirm="true" message="confirmProductClassificationRemoveType" title="productClassificationRemoveType">
                                                <o:img name="deleteIcon" styleClass="bigicon" />
                                            </v:link>
                                        </o:permission>
                                    </td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr class="altrow">
                                    <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductGroupMoveDelete">
                                        <td class="boldtext"><o:out value="${view.bean.name}" /></td>
                                        <td class="action right">
                                            <v:ajaxLink url="/admin/products/productTypeEditor/forward" linkId="productTypeEditor">
                                                <o:img name="writeIcon" styleClass="bigicon" />
                                            </v:ajaxLink>
                                        </td>
                                    </o:permission>
                                    <o:forbidden role="product_group_edit,purchase_invoice_create,organisation_admin">
                                        <td class="boldtext" colspan="2"><o:out value="${view.bean.name}" /></td>
                                    </o:forbidden>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                    <c:forEach var="type" items="${view.list}">
                        <c:if test="${type.id != view.bean.id}">
                            <tr>
                                <td colspan="2">
                                    <v:link url="/admin/products/classificationConfigs/select" parameters="id=${type.id}">
                                        <o:out value="${type.name}" />
                                    </v:link>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>

        </div>
        <!--  table div -->

    </div>
    <!--  panel body -->
</div>
<!--  panel area -->

<div class="col-md-4 panel-area panel-area-default">

    <div class="panel panel-default">
        <div class="panel-heading">

            <div class="table-responsive">
                <table class="table table-head">
                    <tbody>
                        <tr>
                            <td><h4><fmt:message key="productGroup" /></h4></td>
                            <td class="action">
                                <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductGroupAdd">
                                    <v:ajaxLink url="/selections/productClassificationCriteriaSelection/forward?mode=group&exclude=containingGroups&view=classificationConfigsView&selectionTarget=/admin/products/classificationConfigs/addGroup" linkId="selectGroup"
                                        title="assignExistingGroup">
                                        <o:img name="openFolderIcon" styleClass="bigicon" />
                                    </v:ajaxLink>
                                </o:permission> <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductGroupAdd">
                                    <v:ajaxLink url="/admin/products/classificationConfigCreator/forward?mode=group" title="createProductGroup" linkId="createGroup">
                                        <o:img name="newIcon" styleClass="bigicon" />
                                    </v:ajaxLink>
                                </o:permission>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="panel-body">

        <c:if test="${!empty view.bean}">
            <div class="table-responsive table-responsive-default">
                <table class="table">
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.bean.groups}">
                                <tr>
                                    <td colspan="2"><fmt:message key="noGroupsDefined" /></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${!empty view.selectedGroup}">
                                    <tr class="altrow">
                                        <td class="boldtext"><o:out value="${view.selectedGroup.name}" /></td>
                                        <td class="action right">
                                            <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductGroupMoveDelete">
                                                <v:ajaxLink url="/admin/products/productGroupEditor/forward" linkId="productGroupEditor">
                                                    <o:img name="writeIcon" styleClass="bigicon" />
                                                </v:ajaxLink>
                                            </o:permission>
                                        </td>
                                    </tr>
                                </c:if>
                                <c:forEach var="group" items="${view.bean.groups}">
                                    <c:if test="${group.id != view.selectedGroup.id}">
                                        <tr>
                                            <td>
                                                <v:link url="/admin/products/classificationConfigs/selectGroup" parameters="id=${group.id}">
                                                    <o:out value="${group.name}" />
                                                </v:link>
                                            </td>
                                            <td class="action right">
                                                <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductGroupMoveDelete">
                                                    <v:link url="/admin/products/classificationConfigs/moveGroupUp" parameters="id=${group.id}">
                                                        <o:img name="upIcon" styleClass="bigicon" />
                                                    </v:link>
                                                    <v:link url="/admin/products/classificationConfigs/moveGroupDown" parameters="id=${group.id}">
                                                        <o:img name="downIcon" styleClass="bigicon" />
                                                    </v:link>
                                                    <c:choose>
                                                        <c:when test="${!empty group.numberRangeId}">
                                                            <c:set var="assignNumberRangeGroupInvocationUrl"
                                                                value="/admin/products/productNumberRangeEditor/forward?target=/admin/products/classificationConfigs/assignGroupNumberRange?group=${group.id}&range=${group.numberRangeId}" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:set var="assignNumberRangeGroupInvocationUrl" value="/admin/products/productNumberRangeEditor/forward?target=/admin/products/classificationConfigs/assignGroupNumberRange?group=${group.id}" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <v:ajaxLink url="${assignNumberRangeGroupInvocationUrl}" linkId="product_number_range_editor">
                                                        <o:img name="numbersIcon" styleClass="bigicon" />
                                                    </v:ajaxLink>
                                                    <v:link url="/admin/products/classificationConfigs/removeGroup" parameters="id=${group.id}">
                                                        <o:img name="deleteIcon" styleClass="bigicon" />
                                                    </v:link>
                                                </o:permission>
                                            </td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </c:if>

    </div>
    <!--  panel body -->
</div>
<!--  panel area -->

<div class="col-md-4 panel-area panel-area-default">

    <div class="panel panel-default">
        <div class="panel-heading">

            <div class="table-responsive">
                <table class="table table-head">
                    <tbody>
                        <tr>
                            <td><h4><fmt:message key="category" /></h4></td>
                            <td class="action">
                                <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductCategoryAdd">
                                    <v:ajaxLink url="/selections/productClassificationCriteriaSelection/forward?mode=category&exclude=containingCategories&view=classificationConfigsView&selectionTarget=/admin/products/classificationConfigs/addCategory"
                                        linkId="selectCategory" title="assignExistingCategory">
                                        <o:img name="openFolderIcon" styleClass="bigicon" />
                                    </v:ajaxLink>
                                </o:permission> <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductCategoryAdd">
                                    <v:ajaxLink url="/admin/products/classificationConfigCreator/forward?mode=category" title="createProductCategory" linkId="createCategory">
                                        <o:img name="newIcon" styleClass="bigicon" />
                                    </v:ajaxLink>
                                </o:permission>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-body">

        <c:if test="${!empty view.selectedGroup}">
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.selectedGroup.categories}">
                                <tr>
                                    <td colspan="2"><fmt:message key="noCategoriesDefined" /></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${!empty view.selectedCategory}">
                                    <tr class="altrow">
                                        <td class="boldtext"><o:out value="${view.selectedCategory.name}" /></td>
                                        <td class="action right">
                                            <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductCategoryEdit">
                                                <v:ajaxLink url="/admin/products/productCategoryEditor/forward?id=${category.id}" linkId="productCategoryEditor">
                                                    <o:img name="writeIcon" styleClass="bigicon" />
                                                </v:ajaxLink>
                                            </o:permission>
                                        </td>
                                    </tr>
                                </c:if>
                                <c:forEach var="category" items="${view.selectedGroup.categories}">
                                    <c:if test="${category.id != view.selectedCategory.id}">
                                        <tr>
                                            <td>
                                                <v:link url="/admin/products/classificationConfigs/selectCategory?id=${category.id}">
                                                    <o:out value="${category.name}" />
                                                </v:link>
                                            </td>
                                            <td class="action right">
                                                <o:permission role="product_group_edit,purchase_invoice_create,organisation_admin" info="permissionProductCategoryMoveDelete">
                                                    <v:link url="/admin/products/classificationConfigs/moveCategoryUp" parameters="id=${category.id}">
                                                        <o:img name="upIcon" styleClass="bigicon" />
                                                    </v:link>
                                                    <v:link url="/admin/products/classificationConfigs/moveCategoryDown" parameters="id=${category.id}">
                                                        <o:img name="downIcon" styleClass="bigicon" />
                                                    </v:link>
                                                    <c:choose>
                                                        <c:when test="${!empty category.numberRangeId}">
                                                            <c:set var="assignNumberRangeCategoryInvocationUrl"
                                                                value="/admin/products/productNumberRangeEditor/forward?target=/admin/products/classificationConfigs/assignCategoryNumberRange?category=${category.id}&range=${category.numberRangeId}" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:set var="assignNumberRangeCategoryInvocationUrl" value="/admin/products/productNumberRangeEditor/forward?target=/admin/products/classificationConfigs/assignCategoryNumberRange?category=${category.id}" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <v:ajaxLink url="${assignNumberRangeCategoryInvocationUrl}" linkId="product_number_range_editor">
                                                        <o:img name="numbersIcon" styleClass="bigicon" />
                                                    </v:ajaxLink>
                                                    <v:link url="/admin/products/classificationConfigs/removeCategory" parameters="id=${category.id}">
                                                        <o:img name="deleteIcon" styleClass="bigicon" />
                                                    </v:link>
                                                </o:permission>
                                            </td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </c:if>
    </div>
    <!--  panel body -->
</div>
<!--  panel area -->
