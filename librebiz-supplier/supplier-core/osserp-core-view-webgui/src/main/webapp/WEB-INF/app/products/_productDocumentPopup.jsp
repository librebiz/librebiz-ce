<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productDocumentPopupView"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<c:choose>
			<c:when test="${view.groupId == 1}"><fmt:message key="datasheets"/></c:when>
			<c:when test="${view.groupId == 2}"><fmt:message key="salesDocuments"/></c:when>
			<c:when test="${view.groupId == 3}"><fmt:message key="certificates"/></c:when>
			<c:when test="${view.groupId == 4}"><fmt:message key="mountingInstructions"/></c:when>
			<c:when test="${view.groupId == 5}"><fmt:message key="exemptionCertificates"/></c:when>
			<c:otherwise><fmt:message key="documents"/></c:otherwise>
		</c:choose>
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
				<table class="table table-striped" style="min-width: 400px;">
					<thead>
						<tr>
							<th>
								<fmt:message key="name"/>
							</th>
							<c:if test="${empty view.groupId}">
								<th><fmt:message key="type"/></th>
							</c:if>
							<th><fmt:message key="uploadBy"/></th>
							<th><fmt:message key="at"/></th>
							<th style="width: 55px;"><fmt:message key="actions"/></th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${empty view.list}">
							<tr><td colspan="4"><fmt:message key="noneAvailable"/></td></tr>
						</c:if>
						<c:forEach items="${view.list}" var="doc">
							<tr>
								<td>
                                    <o:doc obj="${doc}"><o:out value="${doc.displayName}" limit="70"/></o:doc>
								</td>
								<td><o:out value="${doc.type.name}" /></td>
								<td>
									<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${doc.createdBy}">
										<oc:employee value="${doc.createdBy}"/>
									</v:ajaxLink>
								</td>
								<td><o:date value="${doc.created}"/></td>
								<td>
									<v:ajaxLink targetElement="productDocumentPopupView_popup" url="/products/productDocumentPopup/delete?id=${doc.id}"><o:img name="deleteIcon" /></v:ajaxLink>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			<div class="spacer"></div>
		</div>
	</div>
</div>
