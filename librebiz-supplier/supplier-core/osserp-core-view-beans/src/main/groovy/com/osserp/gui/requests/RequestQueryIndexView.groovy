/**
 *
 * Copyright (C) 2008, 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 28, 2008 11:39:14 PM 
 * 
 */
package com.osserp.gui.requests

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Option

import com.osserp.core.BusinessType
import com.osserp.core.crm.Campaign
import com.osserp.core.crm.CampaignManager
import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeGroup
import com.osserp.core.requests.RequestListItem

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 *
 * @author <a href="mailto:rk@osserp.com">rk</a>
 *
 */
class RequestQueryIndexView extends AbstractRequestQueryView {
    private static Logger logger = LoggerFactory.getLogger(RequestQueryIndexView.class.getName())

    private static final String BY_BRANCH = 'requestsByBranch'
    private static final String BY_MANAGER = 'requestsByManager'
    private static final String BY_ORIGIN = 'requestsByOrigin'
    private static final String BY_SALES = 'requestsBySales'
    private static final String BY_TYPE = 'requestsByType'
    
    List<String> AVAILABLE_QUERIES = [
        BY_BRANCH,
        BY_SALES,
        BY_MANAGER,
        BY_ORIGIN,
        BY_TYPE
    ]

    List<BusinessType> businessTypes = []
    List<Campaign> origins = []
    List<Employee> salesPersons = []
    List<Employee> tecPersons = []
    
    List<RequestListItem> latestRequests = []
    
    List<Option> queryParams = []


    RequestQueryIndexView() {
        super()
        providesCustomNavigation()
        enableCompanyPreselection()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            salesPersons = employeeSearch.findByGroup(domainUser, EmployeeGroup.SALES)
            tecPersons = employeeSearch.findByGroup(domainUser, EmployeeGroup.TECHNICIAN)
            origins = campaignManager.findSelected(domainUser)
            businessTypes = requestTypeSelection
            latestRequests = findByDate()
            logger.debug("initRequest: done [salesPersons=${salesPersons.size()}, tecPersons=${tecPersons.size()}, origins=${origins.size()}, businessTypes=${businessTypes.size()}]")
        }
    }
    
    List<String> getAvailableQueries() {
        List queryList = []
        if (!isBranchSelectionAvailable()) {
            return AVAILABLE_QUERIES.findAll { String query -> 
                (query != BY_BRANCH)
            }
        }
        AVAILABLE_QUERIES
    }

    void selectQuery() {
        selectedQueryType = form.getString('type')
        if (selectedQueryType) {
            switch ( selectedQueryType ) {
                
                case "requestsByBranch":
                    queryParams = systemConfigManager.getBranchs(domainEmployee)
                    break

                case "requestsByManager":
                    queryParams = tecPersons
                    break
                    
                case "requestsByOrigin":
                    queryParams = origins
                    break
                    
                case "requestsBySales":
                    queryParams = salesPersons
                    break
    
                case "requestsByType":
                    queryParams = businessTypes
                    break
                    
                case "currentRequests":
                    list = findByDate()
                    defaultListMode = true
                    selectedQueryType = null
                    break
            }
        } else {
            defaultListMode = false
            queryParams = []
        }
    }
    
    void executeQuery() {
        Long id = getLong('id')
        if (id) {
            switch ( selectedQueryType ) {
                case "requestsByBranch":
                    findByBranch(id)
                    break

                case "requestsByManager":
                    findByManager(id)
                    break
                
                case "requestsByOrigin":
                    findByOrigin(id)
                    break
                    
                case "requestsBySales":
                    findBySales(id)
                    break
                        
                case "requestsByType":
                    findByRequestType(id)
                    break
            }
        } else {
            list = []
            defaultListMode = false
            logger.debug('executeQuery: id is null, done reset.')
        }
    }
    
    @Override
    void createCustomNavigation() {
        nav.listNavigation = [ queryResultExitLink, homeLink ]
        if (selectedQueryType) {
            nav.defaultNavigation = [ querySelectExitLink, homeLink ]
        } else {
            nav.defaultNavigation = [ exitLink, homeLink ]
        }
        logger.debug('createCustomNavigation: done')
    }
    
    MenuItem getQueryResultExitLink() {
        navigationLink("/${context.name}/executeQuery", 'backIcon', 'backToLast')
    }
}
