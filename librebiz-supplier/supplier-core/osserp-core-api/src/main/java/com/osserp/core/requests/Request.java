/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Sep-2005 15:09:29 
 * 
 */
package com.osserp.core.requests;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.common.Details;
import com.osserp.common.Option;

import com.osserp.core.BusinessAddress;
import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Request extends BusinessCase, Details {

    static final Long PICTURES = 11L;
    static final Long DOCUMENTS = 12L;

    /**
     * Returns the request id
     * @return requestId
     */
    Long getRequestId();

    /**
     * Returns the related type id
     * @return typeId
     */
    Long getTypeId();

    /**
     * Provides the type
     * @return type
     */
    BusinessType getType();

    /**
     * Returns the status
     * @return status
     */
    Long getStatus();

    /**
     * Sets the status
     * @param status
     */
    void setStatus(Long status);

    /**
     * A name for the request
     * @return name
     */
    String getName();

    /**
     * Sets the name
     * @param name
     */
    void setName(String name);

    /**
     * Returns the related customer
     * @return customer
     */
    Customer getCustomer();

    /**
     * Sets the related customer
     * @param customer
     */
    void setCustomer(Customer customer);

    /**
     * The delivery address
     * @return address
     */
    BusinessAddress getAddress();

    /**
     * Updates address values
     * @param updateBy
     * @param name
     * @param person
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param federalState
     * @param federalStateName
     * @param district
     * @throws ClientException if validation failed
     */
    void updateAddress(
            Long updateBy,
            String name,
            String person,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String federalStateName,
            Long district) throws ClientException;

    /**
     * Updates the request address
     * @param values
     * @throws ClientException if validation failed
     */
    void updateAddress(BusinessAddress values) throws ClientException;

    /**
     * Updates the branch association
     * @param branch
     */
    void updateBranch(BranchOffice branch);

    /**
     * Updates the sales person
     * @param sales
     * @param branch
     */
    void updateSales(Employee sales, BranchOffice branch);

    /**
     * Provides the sales persons percentage when co sales exists
     * @return salesPercent
     */
    Double getSalesPercent();

    /**
     * Returns the related sales co id
     * @return salesCoId
     */
    Long getSalesCoId();

    /**
     * Sets the related sales co id
     * @param salesCoId
     */
    void setSalesCoId(Long salesCoId);

    /**
     * Provides the percentage of co sales engagement
     * @return salesCoPercent
     */
    Double getSalesCoPercent();

    /**
     * Sets the percentage of co sales engagement
     * @param salesCoPercent
     */
    void setSalesCoPercent(Double salesCoPercent);

    /**
     * Updates sales assignment strategy
     * @param salesAssignment
     * @param salesAssignmentStrict
     */
    void updateSalesAssignment(String salesAssignment, boolean salesAssignmentStrict);

    /**
     * Provides the sales agent if exists
     * @return agent
     */
    Customer getAgent();

    /**
     * Sets the sales agent and default commission
     * @param agent or null to reset agent config
     * @param commission
     * @param commissionPercent
     */
    void configureAgent(
            Customer agent,
            Double commission,
            boolean commissionPercent);

    /**
     * Provides the sales commission of the agent if set. Value is a fixed or percentage value depending on agentCommissionPercent flag
     * @return agentCommission
     */
    Double getAgentCommission();

    /**
     * Indicates that agents sales commission is interpreted as percentage value
     * @return agentCommissionPercent
     */
    boolean isAgentCommissionPercent();

    /**
     * Sets agent commission is interpreted as percentage value
     * @param agentCommissionPercent
     */
    void setAgentCommissionPercent(boolean agentCommissionPercent);

    /**
     * Provides the tip provider if exists
     * @return tip
     */
    Customer getTip();

    /**
     * Sets the tip provider and default tip commission
     * @param tip or null to reset tip config
     * @param commission
     */
    void configureTip(Customer agent, Double commission);

    /**
     * Provides the tip commission of the tip provider if set. Value is fixed.
     * @return tipCommission
     */
    Double getTipCommission();

    /**
     * Provides the sales commission goodwill (added by management)
     * @return salesCommissionGoodwill
     */
    Double getSalesCommissionGoodwill();

    /**
     * Sets the sales commission goodwill added by management
     * @param salesCommissionGoodwill
     */
    void setSalesCommissionGoodwill(Double salesCommissionGoodwill);

    /**
     * Indicates if salesCommission was paid out
     * @return salesCommissionPaid
     */
    boolean isSalesCommissionPaid();

    /**
     * Sets that salesCommission is paid
     * @param salesCommissionPaid
     */
    void setSalesCommissionPaid(boolean salesCommissionPaid);

    /**
     * Sets the related manager id
     * @param managerId
     */
    void setManagerId(Long managerId);

    /**
     * Returns the origin campaign
     * @return origin campaign id
     */
    Long getOrigin();

    /**
     * Sets the origin campaign
     * @param origin campaign
     */
    void setOrigin(Long origin);

    /**
     * Returns the origin type option
     * @return originType option id
     */
    Long getOriginType();

    /**
     * Sets the origin type option
     * @param originType option id
     */
    void updateOriginType(Long employee, Long originType);

    /**
     * Provides the order probability in percent
     * @return orderProbability
     */
    Integer getOrderProbability();

    /**
     * Provides the order probability date
     * @return orderProbabilityDate
     */
    Date getOrderProbabilityDate();

    /**
     * Updates order probability status
     * @param orderProbability
     * @param orderProbabilityDate
     */
    void updateOrderProbability(Integer orderProbability, Date orderProbabilityDate);

    /**
     * The sales talk date
     * @return salesTalkDate
     */
    Date getSalesTalkDate();

    /**
     * Sets the sales talk date
     * @param salesTalkDate
     */
    void setSalesTalkDate(Date salesTalkDate);

    /**
     * The presentation date
     * @return presentationDate
     */
    Date getPresentationDate();

    /**
     * Sets the presentation date
     * @param presentationDate
     */
    void setPresentationDate(Date presentationDate);

    /**
     * The delivery date
     * @return deliveryDate
     */
    Date getDeliveryDate();

    /**
     * Sets the delivery date
     * @param deliveryDate
     */
    void setDeliveryDate(Date deliveryDate);

    /**
     * Provides the average estimated capacity of the request
     * @return capacity
     */
    Double getCapacity();

    /**
     * Sets the average estimated capacity by all created offers
     * @param capacity
     */
    void setCapacity(Double capacity);

    /**
     * Sets the payment id
     * @param paymentId
     */
    void setPaymentId(Long paymentId);

    /**
     * Sets the shipping company id
     * @param shippingId
     */
    void setShippingId(Long shippingId);

    /**
     * Indicates that request is cancelled
     * @return true if request is cancelled
     */
    boolean isCancelled();

    /**
     * Indicates that request is closed (now order)
     * @return closed
     */
    boolean isClosed();

    /**
     * Updates common values
     * @param office
     * @param salesPersonId
     * @param projectManagerId
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param campaign
     * @param origin
     */
    void update(
            BranchOffice office,
            Long salesPersonId,
            Long projectManagerId,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Campaign campaign,
            Option origin);

    /**
     * Allows to set external businessCaseId if supported by type
     * @param externalBusinessCaseId
     */
    void setExternalId(Long externalBusinessCaseId);

}
