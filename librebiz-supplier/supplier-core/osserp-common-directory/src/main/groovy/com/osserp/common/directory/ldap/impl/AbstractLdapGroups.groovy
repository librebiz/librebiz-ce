/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 19, 2013 11:58:48 AM 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.apache.directory.groovyldap.LDAP
import org.apache.directory.groovyldap.SearchScope

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.LdapUtils
import com.osserp.common.directory.ldap.StructuralSchema
import com.osserp.common.directory.ldap.dao.LdapGroups
import com.osserp.common.directory.ldap.dao.Users


abstract class AbstractLdapGroups extends AbstractLdapDao implements LdapGroups {
    private static Logger logger = LoggerFactory.getLogger(AbstractLdapGroups.class.getName())

    Users users
    StructuralSchema schema

    protected AbstractLdapGroups() {
        super()
    }

    protected AbstractLdapGroups(LdapConfig configInstance) {
        super(configInstance)
        users = new UsersImpl(configInstance)
    }

    List<Map> findAll(String client) {
        LDAP ldap = createConnection()
        ldap.search(schema.query, createBaseDn(client), SearchScope.SUB)
    }

    List<Map> findByUser(String uid, String client) {
        LDAP ldap = createConnection()
        List<Map> list = ldap.search(schema.query, createBaseDn(client), SearchScope.SUB)
        List<Map> result = []
        for (group in list) {
            def members = LdapUtils.fetchMembers(group, 'memberuid')
            def member = members.find() { it == uid }
            if (member) {
                result << group
            }
        }
        result.sort { Map a, Map b ->
            def res = 0
            try {
                res = a.cn.compareToIgnoreCase(b.cn)
            } catch (Exception e) {
                logger.warn("findByUser: unsupported group object found")
                logger.warn("values a:")
                logger.warn(a)
                logger.warn("values b:")
                logger.warn(b)
            }
            return res
        }
        return result
    }

    Map find(String name, String client) {
        def dn = createDn(name, client)
        lookupByDn(dn)
    }

    Map lookupByDn(String dn) {
        LDAP ldap = createConnection()
        if (ldap.exists(dn)) {
            return ldap.read(dn)
        }
        return [:]
    }

    Map create(String gidnumber, String name, String description, String client) {
        Map group = find(name, client)
        if (!group) {
            def dn = createDn(name, client)
            def atts = [
                cn: name,
                gidnumber: gidnumber,
                description: description,
            ]
            LDAP ldap = createConnection()
            ldap.add(dn, schema.create(atts))
            group = ldap.read(dn)
        }
        return group
    }

    Long createIdSuggestion(String client) {
        Long currentMax = 0
        List<Map> existing = findAll(client)
        for (group in existing) {
            Long next = fetchLong(group.gidnumber)
            if (next && next > currentMax) {
                currentMax = next
            }
        }
        return (currentMax + 1)
    }

    void delete(Map group) {
        LDAP ldap = createConnection()
        if (group?.dn && ldap.exists(group.dn)) {
            try {
                ldap.delete(group.dn)
                logger.debug("delete: done [dn=${group.dn}]")
            } catch (Throwable t) {
                logger.error("delete: failed [message=${t.message}]")
            }
        }
    }

    Map addMember(Map group, String uid) {
        String client = fetchClientName(group.dn)
        LDAP ldap = createConnection()
        Map user = users.lookup(uid, client)
        if (user && group && group.dn) {
            List memberuid = LdapUtils.fetchMembers(group, 'memberuid')
            List member = LdapUtils.fetchMembers(group, 'member')
            memberuid << user.uid
            member << user.dn
            Map values = [memberuid : memberuid, member : member ]
            ldap.modify(group.dn, 'REPLACE', values)
        }
        return (group && group.dn ? ldap.read(group.dn) : [:])
    }

    Map removeMember(Map group, String uid) {
        String client = fetchClientName(group.dn)
        LDAP ldap = createConnection()
        Map user = users.lookup(uid, client)
        if (user && group && group.dn) {
            List memberuid = LdapUtils.fetchMembers(group, 'memberuid')
            List member = LdapUtils.fetchMembers(group, 'member')
            List changeduids = memberuid.findAll() {
                (it != user.uid)
            }
            List changedmembers = member.findAll() {
                (it != user.dn)
            }
            Map values = [memberuid : changeduids, member : changedmembers ]
            ldap.modify(group.dn, 'REPLACE', values)
        }
        return (group && group.dn ? ldap.read(group.dn) : [:])
    }
}
