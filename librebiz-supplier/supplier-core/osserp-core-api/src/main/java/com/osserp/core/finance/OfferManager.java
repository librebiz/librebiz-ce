/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06-Feb-2007 08:53:16 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;

import com.osserp.common.ClientException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface OfferManager extends PaymentAgreementAwareRecordManager {

    /**
     * Updates the specified values
     * @param offer
     * @param estimatedDelivery
     * @param validUntil
     * @param signaturLeft
     * @param signaturRight
     * @param personId
     * @param note
     * @param taxFree
     * @param taxFreeId
     * @param taxRate
     * @param reducedTaxRate
     * @param currency
     * @param language
     * @param branchId
     * @param shippingId
     * @param customHeader
     * @param printComplimentaryClose
     * @param printProjectmanager
     * @param printSalesperson
     * @param printBusinessId
     * @param printBusinessInfo
     * @param printRecordDate
     * @param printRecordDateByStatus
     * @param printPaymentTarget
     * @param printConfirmationPlaceholder
     * @param printCoverLetter
     * @param offerInfo an optional internal name for the offer
     * @throws ClientException any given date is null or in the past or any other validation error
     */
    void update(
            Offer record,
            Date estimatedDelivery,
            Date validUntil,
            Long signatureLeft,
            Long signatureRight,
            Long personId,
            String note,
            boolean taxFree,
            Long taxFreeId,
            Double taxRate,
            Double reducedTaxRate,
            Long currency,
            String language,
            Long branchId,
            Long shippingId,
            String customHeader,
            boolean printComplimentaryClose,
            boolean printProjectmanager,
            boolean printSalesperson,
            boolean printBusinessId,
            boolean printBusinessInfo,
            boolean printRecordDate,
            boolean printRecordDateByStatus,
            boolean printPaymentTarget,
            boolean printConfirmationPlaceholder,
            boolean printCoverLetter,
            String offerInfo)
            throws ClientException;

}
