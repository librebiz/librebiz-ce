/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 21, 2007 11:06:45 AM 
 * 
 */
package com.osserp.core.users;

import com.osserp.common.User;
import com.osserp.common.UserManager;

import com.osserp.core.contacts.Contact;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DomainUserManager extends UserManager {

    /**
     * Enables a user
     * @param user
     * @return updated user
     */
    User enable(User user);

    /**
     * Disables a user
     * @param user
     * @return updated user
     */
    User disable(User user);

    /**
     * Finds an user account by contact if exists
     * @param contact
     * @return user account or null if none exist
     */
    User find(Contact contact);

    /**
     * Finds an user account by employee if exists
     * @param employee
     * @return user account or null if none exist
     */
    User find(Employee employee);

    /**
     * Grants a dedicated permission
     * @param grantBy
     * @param employee
     * @param permission
     * @return domainUser with grant permission
     */
    User grantPermission(User grantBy, Employee employee, String permission);

    /**
     * Revokes a dedicated permission
     * @param grantBy
     * @param employee
     * @param permission
     * @return domainUser with removed permission
     */
    User revokePermission(User grantBy, Employee employee, String permission);

    /**
     * Grants all group permissions to user associated with given employee
     * @param grantBy user granting permissions
     * @param employee to add permissions
     * @param group to lookup for permissions to grant
     */
    void grantPermissions(User grantBy, Employee employee, EmployeeGroup group);

    /**
     * Revokes all permissions grant by group if not grant by other group from user associated with employee
     * @param revokedBy user revoking permissions
     * @param employee to revoke permissions
     * @param group to lookup for permissions to revoke
     */
    void revokePermissions(User grantBy, Employee employee, EmployeeGroup group);
}
