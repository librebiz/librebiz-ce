<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="content-area" id="productSelectionConfigsListContent">
    <div class="row">
        <div class="col-lg-12 panel-area">

            <v:form name="productSelectionConfigForm" url="/selections/productSelectionConfigsSelection/save">
                <div class="form-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><fmt:message key="context" /></th>
                                    <th style="width: 15%;"><fmt:message key="name" /></th>
                                    <th style="width: 30%;"><fmt:message key="description" /></th>
                                    <th class="center"><fmt:message key="deactivated" /></th>
                                    <th colspan="2"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <c:choose>
                                        <c:when test="${view.editMode && view.bean.movable}">
                                            <td><oc:select id="contextId" name="contextId" options="${view.contexts}" value="${view.selectedContext.id}" prompt="false" styleClass="form-control" /></td>
                                        </c:when>
                                        <c:otherwise>
                                            <td><o:out value="${view.selectedContext.name}" /><input type="hidden" name="contextId" value="${view.selectedContext.id}" /></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td style="width: 15%;"><v:text name="name" value="${view.bean.name}" styleClass="form-control" /></td>
                                    <td style="width: 30%;"><v:text name="description" value="${view.bean.description}" styleClass="form-control" /></td>
                                    <td class="center"><v:checkbox name="disabled" value="${view.bean.disabled}" /></td>
                                    <c:choose>
                                        <c:when test="${view.createMode}">
                                            <td><v:submitExit url="/selections/productSelectionConfigsSelection/disableCreateMode" /></td>
                                            <td><o:submit value="create" styleClass="form-control" /></td>
                                        </c:when>
                                        <c:otherwise>
                                            <td><v:submitExit url="/selections/productSelectionConfigsSelection/disableEditMode" /></td>
                                            <td><o:submit value="save" styleClass="form-control" onclick="checkContext();" /></td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </v:form>

            <c:if test="${view.editMode}">
                <c:import url="_productSelectionConfigsClassifications.jsp" />
            </c:if>

        </div>
    </div>
</div>
