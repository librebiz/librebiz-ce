<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.calculationView}">
    <o:logger write="No calculation view found in session context!" level="info"/>
</c:if>
<c:if test="${empty sessionScope.calculationView.bean}">
    <o:logger write="View found but no calculation loaded!" level="info"/>
</c:if>
<c:set var="view" value="${sessionScope.calculationView}"/>
<c:set var="calculator" value="${view.calculator}"/>
<c:set var="calculation" value="${calculator.calculation}"/>
<c:set var="targetPricePerUnit" value="0"/>
<c:if test="${!empty calculator.revenue and !empty calculation.plantCapacity and calculation.plantCapacity > 0}">
<c:set var="targetPricePerUnit" value="${calculator.revenue.targetVolume / calculation.plantCapacity}"/>
</c:if>

<c:choose>
    <c:when test="${calculator.optionMode}">
        <c:set var="positions" value="${calculation.options}"/>
    </c:when>
    <c:otherwise>
        <c:set var="positions" value="${calculation.positions}"/>
    </c:otherwise>
</c:choose>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="headline">
        <c:choose>
            <c:when test="${calculator.optionMode}">
                <span class="bolderror" title="<fmt:message key='optionModeActivated'/>"><fmt:message key="options"/>:</span>
            </c:when>
            <c:otherwise>
                <span class="bolderror" title="<fmt:message key='changeModeActivated'/>"><fmt:message key="changeLabel"/>:</span>
            </c:otherwise>
        </c:choose>
        <fmt:message key="calculation"/>
        <c:if test="${!empty calculation}">
            <o:out value="${calculation.number}"/>
        </c:if>
        <c:if test="${!empty view.businessCase}">
            <o:out value="${view.businessCase.primaryKey}"/> - <o:out value="${view.businessCase.name}" limit="45"/>
        </c:if>
    </tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${!calculator.optionMode}">
                    <li><a href="<c:url value="/calculationEditAction.do?method=exit"/>"><o:img name="backIcon"/></a></li>
                    <c:if test="${!empty calculation.allItems}">
                        <c:if test="${!empty calculation.calculatorClass and 'calculatorByItems' == calculation.calculatorClass}">
                            <li>
                                <a href="<c:url value="/calculationEditAction.do?method=enablePriceDisplay"/>" title="<fmt:message key="enablePriceDisplayHint"/>">
                                    <o:img name="plusIcon"/>
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/calculationEditAction.do?method=disablePriceDisplay"/>" title="<fmt:message key="disablePriceDisplayHint"/>">
                                    <o:img name="minusIcon"/>
                                </a>
                            </li>
                            <o:permission role="executive,executive_sales,sales_revenue" info="permissionPostCalculation">
                                <li><v:ajaxLink linkId="salesRevenueView" url="/sales/salesRevenue/forward?id=${calculation.id}"><o:img name="moneyIcon"/></v:ajaxLink></li>
                                <li>
                                    <c:choose>
                                        <c:when test="${calculation.salesPriceLocked}">
                                            <a href="<c:url value="/calculationEditAction.do?method=toggleSalesPriceLock"/>" title="<fmt:message key="toggleSalesPriceLockDisable"/>">
                                                <o:img name="lockIcon"/>
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="<c:url value="/calculationEditAction.do?method=toggleSalesPriceLock"/>" title="<fmt:message key="toggleSalesPriceLockEnable"/>">
                                                <o:img name="unlockIcon"/>
                                            </a>
                                        </c:otherwise>
                                    </c:choose>
                                </li>
                            </o:permission>
                        </c:if>
                        <li><o:ajaxExecute url="/calculationEditAction.do?method=close" linkId="foobar"><o:img name="calcIcon"/></o:ajaxExecute></li>
                    </c:if>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/calculationEditAction.do?method=exitOptions"/>"><o:img name="backIcon"/></a></li>
                </c:otherwise>
            </c:choose>
            <li><a href="<c:url value="/calculationEditAction.do?method=exitHome"/>"><o:img name="homeIcon"/></a></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">

        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>
                <div class="contentBox">
                    <div class="contentBoxHeader">
                        <div class="contentBoxHeaderLeft">
                            <fmt:message key="providedFor"/>
                            <o:out value="${calculation.name}"/> <fmt:message key="by"/>
                            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${calculation.createdBy}">
                                <oc:employee value="${calculation.createdBy}"/>
                            </v:ajaxLink>
                            / <o:date value="${calculation.created}"/>
                            <fmt:message key="basedOn"/> <fmt:message key="${calculation.calculatorClass}"/>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="right"><fmt:message key="product"/></th>
                            <th><fmt:message key="label"/></th>
                            <th class="right"><span title="<fmt:message key="partnerPrice"/>"><fmt:message key="partnerPriceShort"/></span></th>
                            <th class="right"><fmt:message key="quantity"/></th>
                            <th class="center"><span title="<fmt:message key="quantityUnit"/>"><fmt:message key="quantityUnitShort"/></span></th>
                            <th class="right"><fmt:message key="price"/></th>
                            <th class="right"><fmt:message key="total"/></th>
                            <th class="center" colspan="4"><fmt:message key="action"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:if test="${(calculator.optionMode) and (empty calculation.options)}">
                            <tr>
                                <td> </td>
                                <td colspan="9">
                                    <fmt:message key="stillNoStructurePresent"/>
                                </td>
                                <td class="center">
                                    <a href="<c:url value="/calculationEditAction.do?method=createOptions"/>"><fmt:message key="create"/></a>
                                </td>
                            </tr>
                        </c:if>
                        <c:forEach var="position" items="${positions}">
                            <tr>
                                <td></td>
                                <td colspan="6" class="boldtext">
                                    <a href="<c:url value="/calculationProductSearch.do?method=forward&positionId=${position.id}"/>" title="<fmt:message key="productsByPreselection"/>">
                                        <o:out value="${position.name}"/>
                                    </a>
                                </td>
                                <td class="right" colspan="4">
                                    <a href="<c:url value="/calculationProductSearch.do?method=forward&positionId=${position.id}&ignorePreselection=true"/>" title="<fmt:message key="productSearch"/>">
                                        <o:img name="forwardIcon"/>
                                    </a>
                                </td>
                            </tr>
                            <c:forEach var="item" items="${position.items}">
                                <tr id="product${item.product.productId}" class="lightgrey">
                                    <td class="right">
                                        <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=calculationEdit&exitId=product${item.product.productId}"/>">
                                            <o:out value="${item.product.productId}"/>
                                        </a>
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${empty sessionScope.openSalesOrdersView}">
                                                <a href="<c:url value="/calculations.do?method=forwardProductPlanning&id=${item.product.productId}&exit=calculationEdit"/>" title="<fmt:message key="deliveryStatus"/>">
                                                    <o:out value="${item.product.name}" limit="45"/>
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${item.product.name}" limit="45"/>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:if test="${!empty item.note}">
                                            <div class="smalltext tab"><o:textOut value="${item.note}"/></div>
                                        </c:if>
                                    </td>
                                    <td class="right"><fmt:formatNumber value="${item.partnerPrice}" maxFractionDigits="2" minFractionDigits="2"/></td>
                                    <td class="right">
                                        <a href="<c:url value="/calculationItemEdit.do?method=select&id=${item.id}"/>">
                                            <o:number value="${item.quantity}" format="decimal"/>
                                        </a>
                                    </td>
                                    <td class="center"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
                                    <td class="right">
                                        <c:choose>
                                            <c:when test="${item.price != 0}">
                                                <a href="javascript:onclick=confirmLink('<fmt:message key="confirmationDeductionOnProducts"/><o:out value="${item.product.name}" limit="45"/> <fmt:message key="withAmount"/> <o:number value="${item.price}" format="currency"/> <fmt:message key="give"/>','<c:url value="/calculationEditAction.do"><c:param name="method" value="addDiscount"/><c:param name="id" value="${item.id}"/></c:url>');" title="<fmt:message key="setDiscount"/>"><o:number value="${item.price}" format="currency"/></a>
                                            </c:when>
                                            <c:otherwise>
                                                <o:number value="${item.price}" format="currency"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="right">
                                        <c:choose>
                                            <c:when test="${item.includePrice}">
                                                <a href="<c:url value="/calculationEditAction.do"><c:param name="method" value="changePriceDisplay"/><c:param name="id" value="${item.id}"/><c:param name="positionId" value="${position.id}"/></c:url>" title="<fmt:message key="turnOffPriceDisplay"/>">
                                                    <o:number value="${item.amount}" format="currency"/>
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="<c:url value="/calculationEditAction.do"><c:param name="method" value="changePriceDisplay"/><c:param name="id" value="${item.id}"/><c:param name="positionId" value="${position.id}"/></c:url>" title="<fmt:message key="turnOnPriceDisplay"/>">
                                                    <span class="error canceled"><o:number value="${item.amount}" format="currency"/></span>
                                                </a>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="icon center">
                                        <a href="<c:url value="/calculationEditAction.do"><c:param name="method" value="moveUp"/><c:param name="itemId" value="${item.id}"/><c:param name="positionId" value="${position.id}"/></c:url>" title="<fmt:message key="moveUp"/>">
                                            <o:img name="upIcon"/>
                                        </a>
                                    </td>
                                    <td class="icon center">
                                        <a href="<c:url value="/calculationEditAction.do"><c:param name="method" value="moveDown"/><c:param name="itemId" value="${item.id}"/><c:param name="positionId" value="${position.id}"/></c:url>" title="<fmt:message key="moveDown"/>">
                                            <o:img name="downIcon"/>
                                        </a>
                                    </td>
                                    <c:choose>
                                        <c:when test="${position.partlist}">
                                            <td class="icon center">
                                                <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmPartlistDelete"/>','<c:url value="/calculationProductSearch.do"><c:param name="method" value="forward"/><c:param name="positionId" value="${position.id}"/><c:param name="itemId" value="${item.id}"/></c:url>');" title="<fmt:message key="replaceProduct"/>">
                                                    <o:img name="replaceIcon"/>
                                                </a>
                                            </td>
                                            <td class="icon center">
                                                <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmPartlistDelete"/>','<c:url value="/calculationEditAction.do"><c:param name="method" value="remove"/><c:param name="itemId" value="${item.id}"/><c:param name="positionId" value="${position.id}"/></c:url>');" title="<fmt:message key="deleteProduct"/>">
                                                    <o:img name="deleteIcon"/>
                                                </a>
                                            </td>
                                        </c:when>
                                        <c:otherwise>
                                            <td class="icon center">
                                                <a href="<c:url value="/calculationProductSearch.do"><c:param name="method" value="forward"/><c:param name="positionId" value="${position.id}"/><c:param name="itemId" value="${item.id}"/></c:url>" title="<fmt:message key="replaceProduct"/>">
                                                    <o:img name="replaceIcon"/>
                                                </a>
                                            </td>
                                            <td class="icon center">
                                                <a href="<c:url value="/calculationEditAction.do"><c:param name="method" value="remove"/><c:param name="itemId" value="${item.id}"/><c:param name="positionId" value="${position.id}"/></c:url>" title="<fmt:message key="deleteProduct"/>">
                                                    <o:img name="deleteIcon"/>
                                                </a>
                                            </td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </c:forEach>
                        </c:forEach>
                        <c:if test="${!calculator.optionMode}">
                            <%-- replcace, see below--%>
                            <tr>
                                <td colspan="11"></td>
                            </tr>
                            <tr class="lightgrey">
                                <td></td>
                                <td colspan="5" class="boldtext"><fmt:message key="summary"/></td>
                                <td class="right boldtext"><o:number value="${calculation.price}" format="currency"/></td>
                                <td colspan="4"></td>
                            </tr>
                            <tr>
                                <td colspan="11"></td>
                            </tr>
                            <%-- re-activate when revenue calculation fixed
                            <c:choose>
                                <c:when test="${empty calculator.revenue}">
                                    <tr>
                                        <td colspan="11"></td>
                                    </tr>
                                    <tr class="lightgrey">
                                        <td></td>
                                        <td colspan="5" class="boldtext"><fmt:message key="summary"/></td>
                                        <td class="right boldtext"><o:number value="${calculation.price}" format="currency"/></td>
                                        <td colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="11"></td>
                                    </tr>
                                </c:when>
                                <c:otherwise>
                                    <tr class="lightgrey">
                                        <td></td>
                                        <td class="boldtext"><fmt:message key="summary"/></td>
                                        <td class="right"><o:number value="${calculator.revenue.netCosts}" format="currency"/></td>
                                        <td class="right"><o:number value="${calculation.plantCapacity}" format="decimal"/></td>
                                        <td class="right">Eh</td>
                                        <td class="right">
                                            <c:choose>
                                                <c:when test="${targetPricePerUnit > 0}">
                                                    <o:number value="${targetPricePerUnit}" format="currency"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <o:number value="${calculator.revenue.targetVolume}" format="currency"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td class="right boldtext"><o:number value="${calculation.price}" format="currency"/></td>
                                        <td colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="11"></td>
                                    </tr>
                                </c:otherwise>
                            </c:choose>
                            --%>
                            <tr>
                                <td></td>
                                <td colspan="10"><a href="<c:url value="/calculationEditAction.do?method=forwardOptions"/>"><fmt:message key="title.edit.options"/></a></td>
                            </tr>
                            <tr>
                                <td colspan="11"></td>
                            </tr>
                        </c:if>
                    </tbody>
                </table>
                <div class="spacer"></div>
            </div>
        </div>

    </tiles:put>
</tiles:insert>
