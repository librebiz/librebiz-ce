/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 30, 2013
 *  
 */
package com.osserp.common.directory.ldap.impl

import org.junit.Test

import static org.junit.Assert.*

import com.osserp.TestConfig
import com.osserp.common.directory.ldap.impl.AdminAccountsImpl
import com.osserp.common.directory.ldap.LdapConfig

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class UAdminAccountsImpl {

    @Test
    void defaultConstructorThrowsException() {
        try {
            AdminAccountsImpl impl = new AdminAccountsImpl()
            fail('Initializing AdminAccountsImpl with default constructor is expected as not working')
        } catch (Exception e) {
        }
    }

    @Test
    void initAdminAccounts() {
        LdapConfig config = new LdapConfig(TestConfig.PROPERTY_FILE)
        AdminAccountsImpl impl = new AdminAccountsImpl(config)
        assertTrue(impl.config != null)
    }

    @Test
    void testCreateBaseDn() {
        LdapConfig config = new LdapConfig(TestConfig.PROPERTY_FILE)
        AdminAccountsImpl impl = new AdminAccountsImpl(config)
        String dn = impl.createBaseDn(null)
        assertTrue(dn != null)
        String testDN = config.noc ? "dc=noc,${TestConfig.TESTDC}" : "${TestConfig.TESTDC}"
        assertTrue(dn == testDN)
    }

    @Test
    void testCreateDnWithoutClientName() {
        LdapConfig config = new LdapConfig(TestConfig.PROPERTY_FILE)
        AdminAccountsImpl impl = new AdminAccountsImpl(config)
        String dn = impl.createDn('mailservice', null)
        assertTrue(dn != null)
        String testDN = config.noc ? "uid=mailservice,dc=noc,${TestConfig.TESTDC}" : "uid=mailservice,ou=clients,${TestConfig.TESTDC}"
        assertTrue(dn == testDN)
        config.noc = false
        dn = impl.createDn('mailservice', null)
        assertTrue(dn != null)
        testDN = config.noc ? "uid=mailservice,dc=noc,${TestConfig.TESTDC}" : "uid=mailservice,${TestConfig.TESTDC}"
        assertTrue(dn == testDN)
    }

    @Test
    void testCreateDnWithClientName() {
        LdapConfig config = new LdapConfig(TestConfig.PROPERTY_FILE)
        AdminAccountsImpl impl = new AdminAccountsImpl(config)
        String dn = impl.createDn('mailservice', 'client1')
        assertTrue(dn != null)
        String testDN = config.noc ? "uid=mailservice,cn=client1,ou=clients,dc=noc,${TestConfig.TESTDC}" : "uid=mailservice,cn=client1,ou=clients,${TestConfig.TESTDC}"
        assertTrue(dn == testDN)
        config.noc = false
        dn = impl.createDn('mailservice', 'client1')
        assertTrue(dn != null)
        testDN = config.noc ? "uid=mailservice,cn=client1,ou=clients,dc=noc,${TestConfig.TESTDC}" : "uid=mailservice,cn=client1,ou=clients,${TestConfig.TESTDC}"
        assertTrue(dn == testDN)
    }
}
