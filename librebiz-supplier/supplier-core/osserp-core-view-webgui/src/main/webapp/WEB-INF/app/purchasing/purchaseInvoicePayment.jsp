<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="purchaseInvoicePaymentView" />
<c:set var="record" value="${view.record}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="paymentRecordHeader" />
        <o:out value="${record.contact.displayName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="purchaseInvoicePaymentContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.recordCloseMode or view.paymentDeleteMode}">
                        <c:import url="${viewdir}/records/_recordPaymentActionConfirmation.jsp"/>
                    </c:when>
                    <c:when test="${view.creditNoteMode}">
                        <v:form url="/purchasing/purchaseInvoicePayment/createCreditNote" name="purchaseInvoicePaymentForm">

                            <div class="col-md-6 panel-area panel-area-default">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4>
                                            <fmt:message key="invoiceNumberShort" />
                                            <o:out value="${view.record.number}" />
                                            <span> / </span>
                                            <o:date value="${view.record.created}" />
                                        </h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="selectedActionName"> <v:link url="${view.baseLink}/selectPaymentType" title="selectType">
                                                        <fmt:message key="action" />
                                                    </v:link>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <fmt:message key="creditNoteRelatingToInvoice" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="recordHeaderCustomLabel"><fmt:message key="recordHeaderCustomLabel" /></label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <v:text name="customHeader" styleClass="form-control" value="${view.customHeaderSuggestion}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row next">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <o:submit value="create" styleClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </v:form>
                    </c:when>
                    <c:when test="${empty view.selectedPaymentType}">
                        <c:import url="${viewdir}/records/_recordPaymentTypeSelection.jsp" />
                    </c:when>
                    <c:otherwise>
                        <v:form url="/purchasing/purchaseInvoicePayment/save" name="purchaseInvoicePaymentForm">
                            <c:import url="${viewdir}/records/_recordPaymentInput.jsp" />
                        </v:form>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>