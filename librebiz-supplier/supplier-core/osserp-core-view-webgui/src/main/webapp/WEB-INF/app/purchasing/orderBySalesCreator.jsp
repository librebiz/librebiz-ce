<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="orderBySalesCreatorView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <fmt:message key="purchaseOrderBySales" /> / <o:out value="${view.salesOrder.businessCaseId}" /> - <o:out value="${view.salesOrder.contact.displayName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="orderBySalesCreatorContent">
            <div class="row">
                <v:form name="orderBySalesCreatorForm" url="/purchasing/orderBySalesCreator/save">
                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <fmt:message key="purchaseOrderSettings" />
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="client"><fmt:message key="client" /></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <select name="selectCompany" size="1" onChange="gotoUrl(this.value);" class="form-control">
                                                <option value="<v:url value="/purchasing/orderBySalesCreator/selectCompany"/>"><fmt:message key="selection" /></option>
                                                <c:forEach var="obj" items="${view.companies}">
                                                    <c:choose>
                                                        <c:when test="${obj.id == view.selectedCompany.id}">
                                                            <option value="<v:url value="/purchasing/orderBySalesCreator/selectCompany?id=${obj.id}"/>" selected="selected"><o:out value="${obj.name}" /></option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="<v:url value="/purchasing/orderBySalesCreator/selectCompany?id=${obj.id}"/>"><o:out value="${obj.name}" /></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="branch"><fmt:message key="branch" /></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <select name="selectBranch" size="1" onChange="gotoUrl(this.value);" class="form-control">
                                                <option value="<v:url value="/purchasing/orderBySalesCreator/selectBranch"/>"><fmt:message key="selection" /></option>
                                                <c:forEach var="obj" items="${view.branchOfficeList}">
                                                    <c:choose>
                                                        <c:when test="${obj.id == view.selectedBranch.id}">
                                                            <option value="<v:url value="/purchasing/orderBySalesCreator/selectBranch?id=${obj.id}"/>" selected="selected"><o:out value="${obj.name}" /></option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="<v:url value="/purchasing/orderBySalesCreator/selectBranch?id=${obj.id}"/>"><o:out value="${obj.name}" /></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <c:if test="${!empty view.selectedCompany and !empty view.selectedBranch}">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="supplier">
                                                    <c:choose>
                                                        <c:when test="${!empty view.suppliers && empty view.selectedSupplier}">
                                                            <v:link url="/contacts/contactSearch/forward?exit=/purchasing/orderBySalesCreator/reload&selectionExit=/salesOrderReload.do&selectionTarget=/purchasing/orderBySalesCreator/selectSupplier&context=supplier">
                                                                <fmt:message key="supplier" />
                                                            </v:link>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <fmt:message key="supplier" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <c:choose>
                                                    <c:when test="${empty view.suppliers && empty view.selectedSupplier}">
                                                        <v:link url="/contacts/contactSearch/forward?exit=/purchasing/orderBySalesCreator/reload&selectionExit=/salesOrderReload.do&selectionTarget=/purchasing/orderBySalesCreator/selectSupplier&context=supplier">
                                                            <fmt:message key="noSelectionAvailable" />
                                                        </v:link>
                                                    </c:when>
                                                    <c:when test="${empty view.suppliers && !empty view.selectedSupplier}">
                                                        <%-- selection by search result --%>
                                                        <v:link url="/purchasing/orderBySalesCreator/selectSupplier">
                                                            <o:out value="${view.selectedSupplier.displayName}" />
                                                        </v:link>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <select name="selectSupplier" size="1" onChange="gotoUrl(this.value);" class="form-control">
                                                            <option value="<v:url value="/purchasing/orderBySalesCreator/selectSupplier"/>"><fmt:message key="selection" /></option>
                                                            <c:forEach var="obj" items="${view.suppliers}">
                                                                <c:choose>
                                                                    <c:when test="${obj.id == view.selectedSupplier.id}">
                                                                        <option value="<v:url value="/purchasing/orderBySalesCreator/selectSupplier?id=${obj.id}"/>" selected="selected"><o:out value="${obj.name}" /></option>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <option value="<v:url value="/purchasing/orderBySalesCreator/selectSupplier?id=${obj.id}"/>"><o:out value="${obj.name}" /></option>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </select>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>

                                <c:if test="${!empty view.selectedCompany and !empty view.selectedBranch and !empty view.selectedSupplier}">
                                    <div class="row next">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <o:submit styleClass="form-control" value="createPurchaseOrder" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>

                            </div>
                        </div>
                    </div>
                </v:form>

            </div>
        </div>
    </tiles:put>
</tiles:insert>