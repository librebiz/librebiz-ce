/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Aug-2006 08:22:15 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.common.Constants;
import com.osserp.common.beans.OptionImpl;
import com.osserp.core.finance.ListItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ListItemVO extends OptionImpl implements ListItem {
    private Long productId = null;
    private Long productGroupId = null;
    private Double quantity = null;
    private String quantityUnit = null;
    private Double price = null;
    private boolean includePrice = false;
    private String contactName = null;
    private Long contactId = null;
    private Long currency = null;
    private Double summary = null;
    private Long status = null;

    protected ListItemVO() {
        super();
    }

    public ListItemVO(
            Long id,
            Long productId,
            Long productGroupId,
            String name,
            Double quantity,
            String quantityUnit,
            Double price,
            boolean includePrice) {
        super(id, name);
        this.productId = productId;
        this.productGroupId = productGroupId;
        this.quantity = quantity;
        this.quantityUnit = quantityUnit;
        this.price = price;
        this.includePrice = includePrice;
    }

    public ListItemVO(
            Long id,
            Long status,
            Date created,
            Double price,
            Double quantity,
            Double summary,
            Long currency,
            Long contactId,
            String contactName) {
        super(id, null);
        setCreated(created);
        this.status = status;
        this.price = price;
        this.quantity = quantity;
        this.summary = summary;
        this.currency = currency;
        this.contactId = contactId;
        this.contactName = contactName;
    }

    public Long getProductGroupId() {
        return productGroupId;
    }

    public Long getProductId() {
        return productId;
    }

    public Double getPrice() {
        return price;
    }

    public Double getQuantity() {
        return quantity;
    }

    public String getQuantityUnit() {
        return quantityUnit;
    }

    public boolean isIncludePrice() {
        return includePrice;
    }

    public Double getSummary() {
        if (summary == null) {
            return (price == null || quantity == null) ? 0d : quantity * price;
        }
        return summary;
    }

    protected void setProductGroupId(Long productGroupId) {
        this.productGroupId = productGroupId;
    }

    protected void setProductId(Long productId) {
        this.productId = productId;
    }

    protected void setPrice(Double price) {
        this.price = price;
    }

    protected void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    protected void setQuantityUnit(String quantityUnit) {
        this.quantityUnit = quantityUnit;
    }

    protected void setIncludePrice(boolean includePrice) {
        this.includePrice = includePrice;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public Long getCurrency() {
        return currency;
    }

    public void setCurrency(Long currency) {
        this.currency = currency;
    }

    public String getCurrencyKey() {
        return Constants.getDefaultCurrency(currency);
    }

    public String getCurrencySymbol() {
        return Constants.getCurrencySymbol(currency);
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
