/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 22, 2009 3:31:08 PM 
 * 
 */
package com.osserp.gui.client.calc.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.CalculationConfigManager;
import com.osserp.core.calc.Calculator;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSearch;
import com.osserp.core.products.ProductSearchRequest;
import com.osserp.core.products.ProductUtil;

import com.osserp.gui.client.calc.CalculationProductSearchView;
import com.osserp.gui.client.calc.CalculationView;
import com.osserp.gui.client.products.impl.AbstractProductSearchView;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
@ViewName("calculationProductSearchView")
public class CalculationProductSearchViewImpl extends AbstractProductSearchView
        implements CalculationProductSearchView {
    private static Logger log = LoggerFactory.getLogger(CalculationProductSearchViewImpl.class.getName());

    private CalculationView calculationView = null;
    private CalculationConfigGroup selectedCalculationGroup = null;
    private ItemPosition selectedCalculationPosition = null;
    private Item selectedCalculationItem = null;
    private Set<Long> redundancyIgnorables = new HashSet();

    protected CalculationProductSearchViewImpl() {
        super();
    }

    protected CalculationProductSearchViewImpl(Set<String> methods, String defaultMethod) {
        super(methods, defaultMethod, true);
        setKeepViewWhenExistsOnCreate(false);
        setIncludeEolAvailable(false);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        setProductSelectionConfigsAvailable(false);
        calculationView = (CalculationView) fetchView(request, CalculationView.class);
        Long positionId = RequestUtil.getLong(request, "positionId");
        if (calculationView != null && calculationView.isCalculatorAvailable()) {
            if (calculationView.getBusinessCase() != null
                    && calculationView.getBusinessCase().getCustomer() != null) {
                setContactReference(calculationView.getBusinessCase().getCustomer().getId());
                setContactReferenceCustomer(true);
                setProductPriceMethod(getSystemProperty(ProductUtil.PRODUCT_PRICE_SALES_PROPERTY_METHOD));
                setProductPriceDefault(getSystemProperty(ProductUtil.PRODUCT_PRICE_SALES_PROPERTY_DEFAULT));
            }
            Calculator calculator = calculationView.getCalculator();
            if (calculator.isOptionMode()) {
                this.selectedCalculationPosition = calculator.getCalculation().getOptionPosition(positionId);
            } else {
                this.selectedCalculationPosition = calculator.getCalculation().getPosition(positionId);
            }
            this.calculationView.setJumpTo(positionId);
            Long itemId = RequestUtil.fetchLong(request, "itemId");
            if (itemId != null) {
                this.selectedCalculationItem = selectedCalculationPosition.getItem(itemId);
                if (selectedCalculationItem == null) {
                    log.warn("init() itemId provided but no item found!");
                }
            }
            this.redundancyIgnorables = fetchIgnorables(calculator.getCalculation());
            CalculationConfigManager manager = getCalculationConfigManager();
            this.selectedCalculationGroup = manager.findGroup(selectedCalculationPosition);
            boolean ignorePreselection = RequestUtil.getBoolean(request, "ignorePreselection");
            if (log.isDebugEnabled()) {
                log.debug("init() initialized search [calculation="
                        + calculator.getCalculation().getId()
                        + ", position=" + selectedCalculationPosition.getId()
                        + ", ignorePreselection=" + ignorePreselection
                        + ", calculationGroup="
                        + (selectedCalculationGroup == null ? "null" : selectedCalculationGroup.getId())
                        + "]");
            }

            // enable preselection if possible:
            if (selectedCalculationGroup != null
                    && !ignorePreselection
                    && !this.selectedCalculationGroup.getProductSelections().isEmpty()) {

                setProductSelectionConfigs(selectedCalculationGroup.getProductSelections());
                setProductSelectionConfigsAvailable(true);
                ProductSearch search = (ProductSearch) getService(ProductSearch.class.getName());
                List<Product> list = search.find(
                        new ProductSearchRequest(
                                getContactReference(),
                                isContactReferenceCustomer(),
                                isIncludeEol(),
                                getBlankSearchProductFetchSize()),
                        getProductSelectionConfigs());
                setList(list);
                if (!list.isEmpty()) {
                    setProductSelectionConfigsMode(true);
                }
                if (log.isDebugEnabled()) {
                    log.debug("init() done by product selection "
                            + "[calculation=" + selectedCalculationPosition.getReference()
                            + ", position=" + positionId
                            + ", calculationGroup=" + selectedCalculationGroup.getId()
                            + ", productSelectionConfigsMode=" + isProductSelectionConfigsMode()
                            + ", productCount=" + list.size()
                            + "]");
                }
            } else if (log.isDebugEnabled()) {
                StringBuilder buffer = new StringBuilder("init() preselection disabled: ");
                if (selectedCalculationGroup == null) {
                    buffer.append("selected product group not available");
                } else if (selectedCalculationGroup.getProductSelections().isEmpty()) {
                    buffer.append("product selections empty");
                }
                log.debug(buffer.toString());
            }
        } else {
            if (calculationView == null) {
                log.warn("init() calculation view not bound!");
            } else {
                log.warn("init() calculator not active!");
            }
        }
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        if (form != null && calculationView != null && calculationView.isCalculatorAvailable()) {
            Long[] productId = form.getIndexedLong(Form.PRODUCT_ID);
            String[] customNames = form.getIndexedStrings(Form.CUSTOM_NAME);
            String[] quantity = form.getIndexedStrings(Form.QUANTITY);
            String[] price = form.getIndexedStrings(Form.PRICE);
            String[] note = form.getIndexedStrings(Form.NOTE);
            this.calculationView.getCalculator().addProducts(
                    selectedCalculationPosition,
                    selectedCalculationItem,
                    productId,
                    customNames,
                    quantity,
                    price,
                    note);
        } else {
            String error = "no calculation view bound or caluclator not available";
            log.warn("save() invoked while " + error);
            throw new ActionException(error);
        }
    }

    public ItemPosition getSelectedCalculationPosition() {
        return selectedCalculationPosition;
    }

    public CalculationConfigGroup getSelectedCalculationGroup() {
        return selectedCalculationGroup;
    }

    @Override
    protected void setList(List list) {
        if (calculationView != null && calculationView.isCalculatorAvailable()) {
            Calculation calc = calculationView.getCalculator().getCalculation();
            for (Iterator<Product> i = list.iterator(); i.hasNext();) {
                Product next = i.next();
                if (calc.isAlreadyAdded(next)
                        && !this.redundancyIgnorables.contains(next.getProductId())) {

                    if (log.isDebugEnabled()) {
                        log.debug("setList() removed already added product [id="
                                + next.getProductId() + "]");
                    }
                    i.remove();

                }
            }
        }
        super.setList(list);
    }

    private Set<Long> fetchIgnorables(Calculation calc) {
        Set<Long> igns = new HashSet();
        try {
            CalculationConfigManager manager = getCalculationConfigManager();
            CalculationConfig cfg = manager.findConfig(calculationView.getBusinessCase().getType());
            for (int i = 0, j = cfg.getAllowedRedundancies().size(); i < j; i++) {
                Product next = cfg.getAllowedRedundancies().get(i);
                igns.add(next.getProductId());
            }
        } catch (Exception e) {
            // blocking users work by this cause should be avoided
            log.warn("fetchIgnorables() ignoring exception on attempt to get products [message="
                    + e.getMessage() + "]");
        }
        if (log.isDebugEnabled() && !igns.isEmpty()) {
            log.debug("fetchIgnorables() added redundancy check ignorables [count=" + igns.size() + "]");
        }
        return igns;
    }

    private CalculationConfigManager getCalculationConfigManager() {
        return (CalculationConfigManager) getService(CalculationConfigManager.class.getName());
    }
}
