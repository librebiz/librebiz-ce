/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2008 2:06:42 AM 
 * 
 */
package com.osserp.core.service.letters;

import java.util.Date;
import java.util.Iterator;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.dao.LetterContents;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterContentManager;
import com.osserp.core.dms.LetterParagraph;
import com.osserp.core.dms.LetterType;
import com.osserp.core.service.impl.AbstractCoreService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractLetterContentManager extends AbstractCoreService implements LetterContentManager {
    private LetterContents letterContentsDao = null;
    private LetterOutputService letterOutputService = null;

    protected AbstractLetterContentManager(OptionsCache optionsCache, LetterContents letterContentsDao, LetterOutputService letterOutputService) {
        super(optionsCache);
        this.letterContentsDao = letterContentsDao;
        this.letterOutputService = letterOutputService;   
    }

    public LetterType findType(String contextName) {
        return letterContentsDao.findType(contextName);
    }

    public LetterType getType(Long id) {
        return letterContentsDao.loadType(id);
    }

    public LetterContent find(Long id) {
        return letterContentsDao.find(id);
    }

    public String getDefaultSalutation(LetterContent letter) {
        return letterOutputService.getDefaultSalutation(letter);
    }

    public LetterParagraph createParagraph(DomainUser user, LetterContent letterContent) {
        return letterContentsDao.createParagraph(letterContent, user.getEmployee());
    }

    public void deleteParagraph(DomainUser user, LetterContent letterContent,
            LetterParagraph paragraph) {
        for (Iterator<LetterParagraph> i = letterContent.getParagraphs().iterator(); i.hasNext();) {
            LetterParagraph next = i.next();
            if (next.getId().equals(paragraph.getId())) {
                i.remove();
                letterContent.setChanged(new Date(System.currentTimeMillis()));
                letterContent.setChangedBy(user.getEmployee().getId());
                letterContentsDao.save(letterContent);
                break;
            }
        }
    }

    public void moveParagraphDown(LetterContent letterContent, Long id) {
        letterContent.moveParagraphDown(id);
        letterContentsDao.save(letterContent);
    }

    public void moveParagraphUp(LetterContent letterContent, Long id) {
        letterContent.moveParagraphUp(id);
        letterContentsDao.save(letterContent);
    }

    public void updateParagraph(
            DomainUser user,
            LetterContent letterContent,
            LetterParagraph paragraph,
            String content) throws ClientException {

        letterContent.update(user.getEmployee(), paragraph.getId(), content);
        letterContentsDao.save(letterContent);
    }

    public void delete(DomainUser user, LetterContent letter) throws PermissionException {
        letterContentsDao.delete(letter);
    }
    
    protected final DocumentData createPdf(DomainUser user, LetterContent letter) {
        return letterOutputService.createPdf(user.getEmployee(), letter);
    }

    protected LetterContents getLetterContentsDao() {
        return letterContentsDao;
    }
}
