/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2012 
 * 
 */
package com.osserp.core.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.Constants;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseCreator;
import com.osserp.core.BusinessNote;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsItem;
import com.osserp.core.Item;
import com.osserp.core.NoteType;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.impl.AbstractBusinessCaseContextDao;
import com.osserp.core.dao.records.SalesOffers;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.Order;
import com.osserp.core.model.FlowControlItemImpl;
import com.osserp.core.model.RequestImpl;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.projects.ProjectFcsManager;
import com.osserp.core.requests.Request;
import com.osserp.core.requests.RequestFcsAction;
import com.osserp.core.requests.RequestFcsManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessCaseCreatorImpl extends AbstractBusinessCaseContextDao implements BusinessCaseCreator {
    private static Logger log = LoggerFactory.getLogger(BusinessCaseCreatorImpl.class.getName());

    private Requests requests;
    private RequestFcsManager requestFcsManager;
    private Projects projects;
    private ProjectFcsManager projectFcsManager;
    private SalesOffers salesOffers;
    private SalesOrders salesOrders;
    private BusinessNotes businessNotes;
    private NoteType noteType;
    private Employees employees;

    
    public BusinessCaseCreatorImpl(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache,
            Requests requests,
            RequestFcsManager requestFcsManager,
            Projects projects,
            ProjectFcsManager projectFcsManager,
            SalesOffers salesOffers,
            SalesOrders salesOrders,
            BusinessNotes businessNotes,
            Employees employees) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache);
        this.projects = projects;
        this.projectFcsManager = projectFcsManager;
        this.requests = requests;
        this.requestFcsManager = requestFcsManager;
        this.salesOffers = salesOffers;
        this.salesOrders = salesOrders;
        this.employees = employees;
        this.businessNotes = businessNotes;
        try {
            if (businessNotes != null) {
                noteType = businessNotes.getType(BusinessCase.NOTE_TYPE);
            }
        } catch (Exception e) {
            log.warn("<init> failed to initialize businessNote type [message="
                + e.getMessage() + ", class=" + e.getClass().getName() + "]", e);
        }
    }

    public BusinessCase findExisting(Long id) {
        if (projects.exists(id)) {
            log.debug("findExisting() done [businessId=" + id + ", type=sales]");
            return projects.load(id);
        }
        if (requests.exists(id)) {
            log.debug("findExisting() done [businessId=" + id + ", type=request]");
            return requests.load(id);
        }
        return null;
    }

    public final BusinessCase createOrUpdate(
            Long businessId,
            String externalReference,
            String externalUrl,
            Date createdDate,
            String contextName,
            BusinessType businessType,
            BranchOffice branchOffice,
            Customer customer,
            Long salesPersonId,
            Long projectManagerId, 
            String name, 
            String street, 
            String streetAddon, 
            String zipcode,
            String city,
            Long countryId,
            Campaign campaign,
            Option origin,
            String note,
            FcsAction action,
            List<Item> items) {
        
        BusinessCase result = fetchExisting(businessId, externalReference);
        boolean salesContext = "sales".equals(contextName);
        if (result == null) {
            if (salesContext) {
                Sales sales = (Sales) projects.createBusinessCase(
                    businessId, externalReference, externalUrl, createdDate, 
                    businessType, branchOffice, customer, salesPersonId, 
                    projectManagerId, name, street, streetAddon, zipcode, city, 
                    countryId, campaign, origin);//, action);
                if (action != null && action.isImportAction()) {
                    if (sales.getFlowControlSheet().size() == 1
                        && sales.getFlowControlSheet().get(0) instanceof FlowControlItemImpl) {
                        FlowControlItemImpl fcs = (FlowControlItemImpl) sales.getFlowControlSheet().get(0);
                        fcs.setAction(action);
                        fcs.setCreated(createdDate);
                        if (isSet(action.getStatus())) {
                            sales.setStatus(Long.valueOf(action.getStatus()));
                        }
                        sales.getRequest().setSalesTalkDate(createdDate);
                        sales.getRequest().setPresentationDate(createdDate);
                        sales.getRequest().setChanged(null);
                        sales.getRequest().setChangedBy(null);
                        projects.save(sales);
                    }
                } else {
                    try {
                        projectFcsManager.createInitialEvents(sales);
                    } catch (Exception e) {
                        log.error("createOrUpdate() failed on attempt to "
                                + "initialize FCS [message=" + e.getMessage()+ "]");
                    }
                    sales = (Sales) addFlowControlIfRequired(sales, action, salesPersonId, note);
                }
                result = finalizeSales(sales, items, note);
            } else {
                Request request = (Request) requests.createBusinessCase(
                    businessId, externalReference, externalUrl, createdDate, 
                    businessType, branchOffice, customer, salesPersonId, 
                    projectManagerId, name, street, streetAddon, zipcode, city, 
                    countryId, campaign, origin);
                
                if (action == null || !action.isImportAction()) {
                    try {
                        requestFcsManager.initializeFlowControl(request);
                    } catch (Exception e) {
                        log.error("createOrUpdate() ignoring failed attempt to "
                                + "initialize FCS [message=" + e.getMessage()+ "]");
                    }
                }
                request = (Request) addFlowControlIfRequired(request, action, salesPersonId, note);
                if (action != null && action.isImportAction()) {
                    request.setChanged(null);
                    request.setChangedBy(null);
                    request.setSalesTalkDate(createdDate);
                    request.setPresentationDate(createdDate);
                    requests.save(request);
                }
                result = finalizeRequest(request, items, note);
            }
            
        } else if ((!result.isCancelled()) && (!result.isClosed())) {
              Request request = null;
              Sales sales = null;
              if (result instanceof Sales) {
                  sales = (Sales)result;
                  request = sales.getRequest();
              } else {
                  request = (Request)result;
              }
              request.update(
                      branchOffice, 
                      salesPersonId, 
                      projectManagerId, 
                      name, 
                      street, 
                      streetAddon, 
                      zipcode, 
                      city, 
                      countryId, 
                      campaign, 
                      origin);
              requests.save(request);
              
              result = addFlowControlIfRequired(result, action, salesPersonId, note);
              
              if (sales != null) {
                  projects.save(sales);
                  result = finalizeSales(sales, items, note);
              } else {
                  requests.save(request);
                  result = finalizeRequest(request, items, note);
              }
              log.debug("createOrUpdate() done [businessId=" 
                      + businessId + ", method=update]");
              
        } else {
              log.info("createOrUpdate() nothing to do [businessId=" 
                      + businessId + ", message=closedOrCancelled]");
        }
        return result;
    }

    public BusinessCase override(
            Long targetId, 
            Long businessId, 
            String externalReference, 
            String externalUrl, 
            Date createdDate, 
            String contextName,
            BusinessType businessType, 
            BranchOffice branchOffice, 
            Long salesPersonId, 
            Long projectManagerId, 
            String name, 
            String street,
            String streetAddon, 
            String zipcode, 
            String city, 
            Long countryId, 
            Campaign campaign, 
            Option origin, 
            String note, 
            FcsAction action, 
            List<Item> items) {
        
        Request existing = (Request) requests.findById(targetId);
        if (existing == null) {
            log.warn("override() nothing to do: non existing target [id=" + targetId + "]");
            return null;
        }
        if (existing.isClosed()) {
            log.warn("override() nothing to do: target is closed [id=" + targetId + "]");
            return null;
        }
        if (existing.isCancelled()) {
            log.warn("override() nothing to do: target is cancelled [id=" + targetId + "]");
            return null;
        }
        
        String existingIdString = existing.getPrimaryKey().toString();
        
        Request result = (Request) requests.findById(businessId);
        if (result == null) {
            
            log.debug("override() businessId does not exist, trying to override target [businessId=" 
                    + businessId + ", target=" + targetId + "]");
            
            if (businessType != null && !businessType.getId().equals(existing.getType().getId())) {
                
                existing = requests.changeType(salesPersonId, existing, businessType);
                
            } else if (log.isDebugEnabled()) {
                log.debug("override() businessType not provided or not changed [request="
                        + existing.getPrimaryKey() + ", type=" 
                        + existing.getType().getId() + ", provided=" 
                        + (businessType == null ? "null" : businessType.getId()) + "]");
            }
            result = requests.changeRequestId(salesPersonId, existing, businessId);
            result = update(
                    result, 
                    externalReference, 
                    externalUrl, 
                    createdDate, 
                    contextName, 
                    branchOffice, 
                    salesPersonId, 
                    projectManagerId, 
                    name, 
                    street, 
                    streetAddon, 
                    zipcode, 
                    city, 
                    countryId, 
                    campaign, 
                    origin);
            StringBuilder info = new StringBuilder();
            if (isSet(note)) {
                info.append(note).append("; ");
            }
            info.append(existingIdString).append(" => ").append(businessId);
            result = finalizeRequest(result, items, info.toString());
            return result;
        }
        log.info("override() nothing to do [businessId=" 
                + businessId + ", target=" + targetId 
                + ", message=alreadyExisting]");
        
        return null;
    }
        
        

    public Request update(
            Request request,
            String externalReference, 
            String externalUrl, 
            Date createdDate, 
            String contextName,
            BranchOffice branchOffice, 
            Long salesPersonId, 
            Long projectManagerId, 
            String name, 
            String street,
            String streetAddon, 
            String zipcode, 
            String city, 
            Long countryId, 
            Campaign campaign, 
            Option origin) {
        
        if (branchOffice != null && (request.getBranch() == null || 
                !branchOffice.getId().equals(request.getBranch().getId()))) {
            request = requests.changeBranch(request, branchOffice.getId());
        }
        // merge values
        if (isSet(externalReference)) {
            request.setExternalReference(externalReference);
        }
        if (isSet(externalUrl)) {
            request.setExternalUrl(externalUrl);
        }
        if (request instanceof RequestImpl) {
            RequestImpl obj = (RequestImpl) request;
            if (createdDate != null && createdDate.before(obj.getCreated())) {
                obj.setCreated(createdDate);
            }
            if (isSet(salesPersonId)) {
                obj.setSalesId(salesPersonId);
            }
            if (isSet(projectManagerId)) {
                obj.setManagerId(projectManagerId);
            }
            if (isSet(name) && !name.equals(obj.getName())) {
                obj.setName(name);
            }
        }
        if (request.getAddress() != null) {
            if (isSet(street)) {
                request.getAddress().setStreet(street);
            }
            if (isSet(streetAddon)) {
                request.getAddress().setStreetAddon(streetAddon);
            }
            if (isSet(zipcode)) {
                request.getAddress().setZipcode(zipcode);
            }
            if (isSet(city)) {
                request.getAddress().setCity(city);
            }
            if (isSet(countryId)) {
                request.getAddress().setCountry(countryId);
            }
        }
        requests.save(request);
        return request;
    }
    
    protected Request finalizeRequest(Request request, List<Item> items, String note) {
        addNoteIfAvailable(Constants.SYSTEM_EMPLOYEE, request.getPrimaryKey(), note);
        if (!items.isEmpty()) {
            Offer offer = salesOffers.createOrUpdate(request, items);
            if (log.isDebugEnabled()) {
                log.debug("finalizeRequest() salesOffer created [businessCase="
                        + request.getPrimaryKey() + ", offer=" + offer.getId() + "]");
            }
        }
        return request;
    }

    protected Sales finalizeSales(Sales sales, List<Item> items, String note) {
        addNoteIfAvailable(Constants.SYSTEM_EMPLOYEE, sales.getRequest().getPrimaryKey(), note);
        if (!items.isEmpty()) {
            Order order = salesOrders.createOrUpdate(sales, items);
            if (log.isDebugEnabled()) {
                log.debug("finalizeSales() salesOrder created [businessCase="
                        + sales.getPrimaryKey() + ", order=" + order.getId() + "]");
            }
        }
        return sales;
    }
    
    protected BusinessCase fetchExisting(Long businessId, String externalReference) {
        BusinessCase existing = fetchSales(businessId, externalReference);
        if (existing == null) {
            existing = fetchRequest(businessId, externalReference);
        }
        return existing;
    }
    
    protected Request fetchRequest(Long businessId, String externalReference) {
        Request result = null;
        if (isSet(businessId) && requests.exists(businessId)) {
            
            result = requests.load(businessId);
            if (log.isDebugEnabled()) {
                log.debug("fetchRequest() found existing by provided id [businessCase="
                        + result.getPrimaryKey() + "]");
            }
            
        } else if (isSet(externalReference)) {
            
            result = (Request) requests.findByExternalReference(externalReference);
            
            if (result != null && isSet(businessId)) {
                log.warn("fetchRequest() found by external key but different id "
                    + "[provided=" + businessId 
                    + ", foundId=" + result.getPrimaryKey()
                    + ", externalReference=" + externalReference 
                    + "]\nShould we create a new one by provided id?");
                throw new IllegalStateException("externalReference mismatch");
            }
        }
        return result;
    }
    
    protected Sales fetchSales(Long businessId, String externalReference) {
        Sales result = null;
        if (isSet(businessId) && projects.exists(businessId)) {
            
            result = projects.load(businessId);
            if (log.isDebugEnabled()) {
                log.debug("fetchSales() found existing by provided id [businessCase="
                        + result.getId() + "]");
            }
            
        } else if (isSet(externalReference)) {
            
            result = (Sales) projects.findByExternalReference(externalReference);
            
            if (result != null && isSet(businessId)) {
                log.warn("fetchSales() found by external key but different id "
                    + "[provided=" + businessId 
                    + ", foundId=" + result.getId()
                    + ", externalReference=" + externalReference 
                    + "]\nShould we create a new one by provided id?");
                throw new IllegalStateException("externalReference mismatch");
            }
        }
        return result;
    }
    
    protected void addNoteIfAvailable(Long createdBy, Long businessId, String note) {
        if (noteType != null && !isEmpty(note)) {
            try {
                BusinessNote bnote = businessNotes.create(noteType, businessId, Constants.SYSTEM_EMPLOYEE, note, null);
                if (log.isDebugEnabled()) {
                    log.debug("addNoteIfAvailable() note found and added [businessId="
                            + businessId + ", note=" 
                            + (bnote == null ? null : bnote.getId()) + "]");
                }
            } catch (Exception e) {
                log.warn("addNoteIfAvailable() failed on attempt to add note [request="
                        + businessId + ", message=" + e.getMessage()
                        + ", class=" + e.getClass().getName() + "]", e);
            }
        } else if (noteType == null) {
            log.warn("addNoteIfAvailable() did not find required noteType!");
        } else if (log.isDebugEnabled()) {
            log.debug("addNoteIfAvailable() no note provided...");
        }
    }

    private BusinessCase addFlowControlIfRequired(
            BusinessCase businessCase, 
            FcsAction action, 
            Long personId, 
            String note) {
        FcsAction flowControl = flowControlToAdd(businessCase, action);
        if (flowControl != null) {
            Employee user = null;
            if (isSet(personId)) {
                try {
                    user = employees.get(personId);
                } catch (Exception e) {
                    log.warn("addFlowControlIfRequired() ignoring failed attempt "
                            + "to fetch employee [id=" + personId
                            + ", message=" + e.getMessage()+ "]");
                }
            }
            try {
                if (businessCase instanceof Sales 
                        && action instanceof FlowControlAction) {
                    
                    projectFcsManager.addFlowControl(
                            user, 
                            businessCase, 
                            flowControl, 
                            new Date(System.currentTimeMillis()), 
                            note, 
                            null,  // headline 
                            null,  // closing 
                            true); // ignoreDependencies
                } else if (businessCase instanceof Request 
                        && action instanceof RequestFcsAction) {
                    requestFcsManager.addFlowControl(
                            user, 
                            businessCase, 
                            flowControl, 
                            new Date(System.currentTimeMillis()), 
                            note, 
                            null,  // headline 
                            null,  // closing 
                            true); // ignoreDependencies
                }
            } catch (Exception e) {
                log.error("addFlowControlIfRequired() ignoring failed attempt "
                        + "to add FCS [id=" + businessCase.getPrimaryKey()
                        + ", message=" + e.getMessage()+ "]");
            }
        }
        return businessCase;
    }
    
    private FcsAction flowControlToAdd(BusinessCase businessCase, FcsAction action) {
        if (action != null) {
            if (businessCase.getFlowControlSheet().isEmpty()) {
                // action is first action
                return action;
            }
            for (int i = 0, j = businessCase.getFlowControlSheet().size(); i < j; i++) {
                FcsItem next = businessCase.getFlowControlSheet().get(i);
                if (next.getAction().getId().equals(action.getId())) {
                    // already added
                    return null;
                }
            }
            // action is new
            return action;
        }
        return null;
    }
}
