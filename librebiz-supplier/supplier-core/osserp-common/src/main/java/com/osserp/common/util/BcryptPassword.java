/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.common.util;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.BCrypt.Version;

/**
 * 
 * Simple wrapper class to create and verify bcrypt hashes.
 *
 */
public class BcryptPassword {

    private static final int PASSWORD_ITERATIONS = 12;

    /**
     * Creates a hash using bcrypt version 2y with 12 iterations.
     * @param password
     * @return hash
     */
    public static String hash(String password) {
        assert password != null;
        return BCrypt.with(BCrypt.Version.VERSION_2Y)
                .hashToString(PASSWORD_ITERATIONS, password.toCharArray());
    }

    /**
     * Creates a custom bcrypt hash
     * @param password required
     * @param version optional (2a|2b|2y), default/null = 2y
     * @param iterations (< || ==) 0 -> 12
     * @return hash
     */
    public static String hash(String password, String version, int iterations) {
        assert password != null;
        char[] pwd = password.toCharArray();
        int iter = iterations <= 0 ? PASSWORD_ITERATIONS : iterations;
        Version ver = Version.VERSION_2Y;
        if ("2a".equalsIgnoreCase(version)) {
            ver = Version.VERSION_2A;
        } else if ("2b".equalsIgnoreCase(version)) {
            ver = Version.VERSION_2B;
        }
        return BCrypt.with(ver).hashToString(iter, pwd);
    }

    /**
     * Verifies bcrypt hash
     * @param password
     * @param hash
     * @return true if hash matches provided clear text password
     */
    public static boolean verify(String password, String hash) {
        assert password != null && hash != null;
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), hash);
        return result.verified;
    }

}
