<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="purchaseInvoiceArchiveView" />

<o:logger write="rendering items" level="debug" />
<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="recordNumber"><fmt:message key="recordNumberShort" /></th>
                <th class="recordDate"><fmt:message key="from" /></th>
                <th class="recordNumber"><fmt:message key="product" /></th>
                <th class="recordName"><fmt:message key="name" /></th>
                <th class="quantity right"><fmt:message key="quantity" /></th>
                <th class="amount right"><fmt:message key="price" /></th>
                <th class="action"></th>
            </tr>
        </thead>
        <tbody>
            <c:choose>
                <c:when test="${empty view.list}">
                    <tr>
                        <td colspan="7"><fmt:message key="noRecordsAvailable" /> <v:link url="/purchasing/purchaseInvoiceCreator/forward?exit=/purchaseInvoiceReceiptForward.do">
                                [<fmt:message key="createNew" />]
                            </v:link></td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach var="record" items="${view.list}" varStatus="s" begin="${view.listStart}" end="${view.listEnd}">
                        <c:forEach var="item" items="${record.items}">
                            <tr id="record${record.id}product${item.productId}">
                                <td class="recordNumber"><a href="<c:url value="/purchaseInvoice.do?method=display&id=${record.id}&exit=${view.name}&exitId=record${record.id}product${item.productId}"/>"><o:out value="${record.number}" /></a></td>
                                <td class="recordDate"><o:date value="${record.created}" /></td>
                                <td class="recordNumber"><a href="<c:url value="/products.do?method=load&id=${item.productId}&exit=${view.name}&exitId=record${record.id}product${item.productId}"/>"> <o:out value="${item.productId}" />
                                </a></td>
                                <td class="recordName"><o:out value="${item.name}" /></td>
                                <td class="quantity right"><o:number value="${item.quantity}" format="decimal" /></td>
                                <td class="amount right"><o:number value="${item.price}" format="currency" /> <span><o:out value="${record.currencySymbol}" /></span></td>
                                <td class="action"><c:if test="${record.unchangeable and !empty record.typeName}">
                                        <v:link url="/records/recordPrint/print?name=${record.typeName}&id=${record.id}" title="documentPrint">
                                            <o:img name="printIcon" />
                                        </v:link>
                                    </c:if></td>
                            </tr>
                        </c:forEach>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </tbody>
    </table>
</div>
