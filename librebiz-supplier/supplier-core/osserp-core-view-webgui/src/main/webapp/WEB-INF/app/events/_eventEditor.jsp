<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="eventEditorView" />
<c:set var="event" value="${view.bean}" />

<style type="text/css">
div.standardForm .label {
    min-width: 210px;
    text-align: right;
    margin-right: 10px;
    font-size: 100%;
    font-weight: normal;
}
</style>
<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <o:out value="${event.description}" />
    </div>
    <div class="modalBoxHeaderRight">
        <div class="boxnav"></div>
    </div>
</div>
<div class="modalBoxData">
    <div class="spacer"></div>
    <v:ajaxForm name="dynamicForm" targetElement="eventEditor_popup" url="/events/eventEditor/save">
        <div class="standardForm">
            <div class="formContent">
                <c:import url="/errors/_errors_standardform.jsp" />
                <c:choose>
                    <c:when test="${event.action.type.appointment}">
                        <div class="data start">
                            <div id="datePicker" style="position: absolute; visibility: hidden; background-color: white; z-index: 10;"></div>
                            <div class="label">
                                <p>
                                    <fmt:message key="appointmentAt" />
                                </p>
                            </div>
                            <div class="value">
                                <p>
                                    <v:date style="width: 100px; text-align: right;" name="appointmentDate" value="${event.appointmentDate}" id="dateInput" />
                                    <a href="javascript:;" onclick="var cal1x = new CalendarPopup('datePicker'); cal1x.select($('dateInput'),'anchor1x','dd.MM.yyyy');" title="cal1x.select(document.forms[0].date1x,'anchor1x','MM/dd/yyyy'); return false;" name="anchor1x" id="anchor1x"> <img src="<c:url value='${applicationScope.calendarIcon}'/>" />
                                    </a>
                                    <fmt:message key="atTime" />
                                    <v:hours styleClass="inputDays" name="appointmentHours" value="${event.appointmentDate}" />
                                    :
                                    <v:minutes styleClass="inputDays" name="appointmentMinutes" value="${event.appointmentDate}" />
                                    &nbsp;&nbsp;
                                    <fmt:message key="clock" />
                                </p>
                            </div>
                        </div>
                        <div class="data">
                            <div class="label">
                                <p>
                                    <fmt:message key="memoryFrom" />
                                </p>
                            </div>
                            <div class="value">
                                <p>
                                    <v:date style="width: 100px; text-align: right;" name="reminderDate" value="${event.activation}" id="reminderInput" />
                                    <a href="javascript:;" onclick="var cal1x = new CalendarPopup('datePicker'); cal1x.select($('reminderInput'),'anchor1x','dd.MM.yyyy');" title="cal1x.select(document.forms[0].date1x,'anchor1x','MM/dd/yyyy'); return false;" name="anchor1x" id="anchor1x"> <img src="<c:url value='${applicationScope.calendarIcon}'/>" />
                                    </a>
                                    <fmt:message key="atTime" />
                                    <v:hours styleClass="inputDays" name="reminderHours" value="${event.activation}" />
                                    :
                                    <v:minutes styleClass="inputDays" name="reminderMinutes" value="${event.activation}" />
                                    &nbsp;&nbsp;
                                    <fmt:message key="clock" />
                                </p>
                            </div>
                        </div>
                        <div class="data">
                            <div class="label">
                                <p>
                                    <fmt:message key="newNote" />
                                </p>
                            </div>
                            <div class="value">
                                <p>
                                    <v:textarea styleClass="form-control" name="note" rows="5" />
                                </p>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="data start">
                            <div class="label">
                                <p>
                                    <fmt:message key="eventDateLabel" />
                                </p>
                            </div>
                            <div class="value">
                                <p>
                                    <o:date value="${event.created}" addtime="true" />
                                </p>
                            </div>
                        </div>
                        <div class="data">
                            <div id="datePicker" style="position: absolute; visibility: hidden; background-color: white; z-index: 10;"></div>
                            <div class="label">
                                <p>
                                    <fmt:message key="expiryDateLabel" />
                                </p>
                            </div>
                            <div class="value">
                                <p>
                                    <v:date style="width: 100px; text-align: right;" name="appointmentDate" value="${event.expires}" id="dateInput" />
                                    <a href="javascript:;" onclick="var cal1x = new CalendarPopup('datePicker'); cal1x.select($('dateInput'),'anchor1x','dd.MM.yyyy');" title="cal1x.select(document.forms[0].date1x,'anchor1x','MM/dd/yyyy'); return false;" name="anchor1x" id="anchor1x"> <img src="<c:url value='${applicationScope.calendarIcon}'/>" />
                                    </a>
                                    <fmt:message key="atTime" />
                                    <v:hours styleClass="inputDays" name="appointmentHours" value="${event.expires}" />
                                    :
                                    <v:minutes styleClass="inputDays" name="appointmentMinutes" value="${event.expires}" />
                                    &nbsp;&nbsp;
                                    <fmt:message key="clock" />
                                </p>
                            </div>
                        </div>
                        <div class="data">
                            <div class="label">
                                <p>
                                    <fmt:message key="displayAgainFrom" />
                                </p>
                            </div>
                            <div class="value">
                                <p>
                                    <v:date style="width: 100px; text-align: right;" name="reminderDate" value="${event.activation}" id="reminderInput" />
                                    <a href="javascript:;" onclick="var cal1x = new CalendarPopup('datePicker'); cal1x.select($('reminderInput'),'anchor1x','dd.MM.yyyy');" title="cal1x.select(document.forms[0].date1x,'anchor1x','MM/dd/yyyy'); return false;" name="anchor1x" id="anchor1x"> <img src="<c:url value='${applicationScope.calendarIcon}'/>" />
                                    </a>
                                    <fmt:message key="atTime" />
                                    <v:hours styleClass="inputDays" name="reminderHours" value="${event.activation}" />
                                    :
                                    <v:minutes styleClass="inputDays" name="reminderMinutes" value="${event.activation}" />
                                    &nbsp;&nbsp;
                                    <fmt:message key="clock" />
                                </p>
                            </div>
                        </div>
                        <div class="data">
                            <div class="label">
                                <p>
                                    <fmt:message key="newNote" />
                                </p>
                            </div>
                            <div class="value">
                                <p>
                                    <v:textarea styleClass="form-control" name="note" rows="5" />
                                </p>
                            </div>
                        </div>
                        <div class="data">
                            <div class="label">
                                <p>
                                    <fmt:message key="sameTodoRecipientsLabel" />
                                </p>
                            </div>
                            <div class="value">
                                <p>
                                    <input type="checkbox" name="distribute" checked="checked">
                                    <fmt:message key="sendAllRecipients" />
                                </p>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
                <oc:systemPropertyEnabled name="eventDistributionEnabled">
                    <c:if test="${!empty view.recipients}">
                        <div class="data">
                            <div class="label">
                                <p>
                                    <fmt:message key="informationalTo" />
                                </p>
                            </div>
                            <div class="value">
                                <p>
                                    <c:set var="ajax">
                                        <v:ajaxFormAction url="/events/eventEditor/addRecipient" formObject="$('dynamicForm')" targetElement="eventEditor_popup" />
                                    </c:set>
                                    <oc:select name="recipient" options="${view.recipients}" style="width: 98%;" onchange="${ajax}" />
                                </p>
                                <p>
                                    <c:forEach var="rcpt" items="${view.recipientIds}">
                                        <br />
                                        <c:set var="ajax">
                                            <v:ajaxFormAction url="/events/eventEditor/removeRecipient?id=${rcpt}" formObject="$('dynamicForm')" targetElement="eventEditor_popup" />
                                        </c:set>
                                        <a onclick="${ajax}"><oc:employee value="${rcpt}" /></a>
                                    </c:forEach>
                                </p>
                            </div>
                        </div>
                    </c:if>
                </oc:systemPropertyEnabled>
                <div class="data inputSubmit" style="margin-bottom: 15px;">
                    <div class="label">
                        <p>&nbsp;</p>
                    </div>
                    <div class="value">
                        <v:ajaxLink url="/events/eventEditor/exit" linkId="exitLink" targetElement="eventEditor_popup">
                            <input type="button" class="submit submitExit" value="<fmt:message key="exit"/>" />
                        </v:ajaxLink>
                        <input type="submit" class="submit" style="margin-left: 90px;" value="<fmt:message key="save"/>" />
                    </div>
                </div>
            </div>
        </div>
    </v:ajaxForm>
    <div class="spacer"></div>
</div>
