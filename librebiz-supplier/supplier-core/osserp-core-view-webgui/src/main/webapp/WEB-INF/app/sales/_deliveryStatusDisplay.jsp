<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<v:view viewName="deliveryStatusDisplayView" />
<c:if test="${!empty view}">
    <c:set var="bean" scope="page" value="${view.bean}" />
    <div class="row row-modal">
        <div class="col-md-6 panel-area panel-area-modal">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="table-responsive">
                        <table class="table table-head">
                            <tbody>
                                <tr>
                                    <td class="left"><c:choose>
                                            <c:when test="${!empty bean}">
                                                <h4><o:out value="${bean.primaryKey}" /> - <o:out value="${bean.name}" /></h4>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:message key="common.error" />
                                            </c:otherwise>
                                        </c:choose></td>
                                    <td class="boxnav">
                                        <ul>
                                            <c:forEach var="nav" items="${view.navigation}">
                                                <li><v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}">
                                                        <o:img name="${nav.icon}" />
                                                    </v:ajaxLink></li>
                                            </c:forEach>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive table-responsive-default">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><fmt:message key="product" /></th>
                                <th><fmt:message key="name" /></th>
                                <th><span title="<fmt:message key='stillToDeliverQuantity'/>"><fmt:message key="order" /></span></th>
                                <th><span title="<fmt:message key='currentlyInInventory'/>"><fmt:message key="inventory" /></span></th>
                                <th><span title="<fmt:message key='locatedInStockReceipt'/>"><fmt:message key="stockReceiptShort" /></span></th>
                                <th><span title="<fmt:message key='orderedBySupplier'/>"><fmt:message key="appointedShortcut" /></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:choose>
                                <c:when test="${bean.deliveryClosed}">
                                    <td colspan="6"><fmt:message key="materialPlanningFinished" /></td>
                                </c:when>
                                <c:when test="${empty view.list}">
                                    <td colspan="6"><fmt:message key="noEntryAvailable" /></td>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach var="record" items="${view.list}" varStatus="s">
                                        <tr class="row-alt">
                                            <td><o:out value="${record.productId}" /></td>
                                            <td><o:out value="${record.productName}" /></td>
                                            <td><o:number value="${record.quantity}" format="integer" /> (<o:number value="${record.vacant}" format="integer" />)</td>
                                            <td><o:number value="${record.stock}" format="integer" /></td>
                                            <td><o:number value="${record.receipt}" format="integer" /></td>
                                            <td><o:number value="${record.expected}" format="integer" /></td>
                                        </tr>
                                        <c:forEach var="item" items="${record.items}" varStatus="s">
                                            <c:choose>
                                                <c:when test="${item.salesItem}">
                                                    <tr <c:if test="${bean.id == item.salesId}"> class="yellow"</c:if> <c:if test="${item.vacant}"> class="red"</c:if>>
                                                        <td><o:out value="${item.salesId}" /></td>
                                                        <td><o:out value="${item.salesName}" /></td>
                                                        <td><o:number value="${item.outstanding * (-1)}" format="integer" /></td>
                                                        <td><o:number value="${item.expectedStock}" format="integer" /></td>
                                                        <td colspan="2" <c:if test="${item.lazy}">class="canceled"</c:if>><o:date value="${item.delivery}" /></td>
                                                    </tr>
                                                </c:when>
                                                <c:otherwise>
                                                    <tr <c:if test="${!item.deliveryConfirmed}"> class="red"</c:if>>
                                                        <td><o:out value="${item.id}" /></td>
                                                        <td><o:out value="${item.salesName}" /></td>
                                                        <td><o:number value="${item.outstanding}" format="integer" /></td>
                                                        <td><o:number value="${item.expectedStock}" format="integer" /></td>
                                                        <td colspan="2"><o:date value="${item.delivery}" /></td>
                                                    </tr>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</c:if>
