<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeEventsView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <fmt:message key="availableAccounts" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="templateContent">
            <div class="row">
                <div class="col-lg-12 panel-area">

                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 15%;"><fmt:message key="id" /></th>
                                    <th style="width: 70%;"><fmt:message key="account" /></th>
                                    <th style="width: 15%;" class="right"><fmt:message key="todos" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="employee" items="${view.list}">
                                    <c:if test="${employee.events > 0}">
                                        <tr>
                                            <td style="width: 15%;"><o:out value="${employee.id}" /></td>
                                            <td style="width: 70%;"><v:link url="/events/userEvents/selectUser" parameters="id=${employee.id}&exit=/events/employeeEvents/reload">
                                                    <o:out value="${employee.name}" />
                                                </v:link></td>
                                            <td style="width: 15%;" class="right"><o:out value="${employee.events}" /></td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>