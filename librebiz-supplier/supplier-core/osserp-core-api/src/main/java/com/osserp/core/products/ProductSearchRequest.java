/**
 *
 * Copyright (C) 2019 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 30, 2019 
 * 
 */
package com.osserp.core.products;

import com.osserp.core.CoreSearchRequest;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSearchRequest extends CoreSearchRequest {

    private Long contactReference;
    private boolean contactReferenceCustomer;
    private boolean includeEol;

    public ProductSearchRequest() {
        super();
    }

    public ProductSearchRequest(
            Long contactReference,
            boolean contactReferenceCustomer,
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean includeEol,
            int fetchSize) {
        super(columnKey, pattern, startsWith);
        setFetchSize(fetchSize);
        this.contactReference = contactReference;
        this.contactReferenceCustomer = contactReferenceCustomer;
        this.includeEol = includeEol;
    }

    public ProductSearchRequest(Long contactReference, boolean contactReferenceCustomer, int fetchSize) {
        super();
        setFetchSize(fetchSize);
        this.contactReference = contactReference;
        this.contactReferenceCustomer = contactReferenceCustomer;
    }

    public ProductSearchRequest(Long contactReference, boolean contactReferenceCustomer, boolean includeEol, int fetchSize) {
        super();
        setFetchSize(fetchSize);
        this.contactReference = contactReference;
        this.contactReferenceCustomer = contactReferenceCustomer;
        this.includeEol = includeEol;
    }

    public Long getContactReference() {
        return contactReference;
    }

    public void setContactReference(Long contactReference) {
        this.contactReference = contactReference;
    }

    public boolean isContactReferenceCustomer() {
        return contactReferenceCustomer;
    }

    public void setContactReferenceCustomer(boolean contactReferenceCustomer) {
        this.contactReferenceCustomer = contactReferenceCustomer;
    }

    public boolean isIncludeEol() {
        return includeEol;
    }

    public void setIncludeEol(boolean includeEol) {
        this.includeEol = includeEol;
    }
}
