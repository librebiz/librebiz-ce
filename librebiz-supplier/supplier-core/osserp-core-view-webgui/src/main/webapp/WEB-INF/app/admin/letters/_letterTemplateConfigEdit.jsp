<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="letterTemplateConfigView" />
<v:form name="letterTemplateConfigForm" url="/admin/letters/letterTemplateConfig/save">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="settings" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="templateName"><fmt:message key="templateName" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="templateName" value="${view.bean.name}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="language"><fmt:message key="language" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select class="form-control" name="language" id="languageSelection">
                                <c:choose>
                                    <c:when test="${empty view.bean.language}">
                                        <option value="de" selected="selected">DE</option>
                                        <option value="en">EN</option>
                                    </c:when>
                                    <c:when test="${view.bean.language == 'de'}">
                                        <option value="de" selected="selected">DE</option>
                                        <option value="en">EN</option>
                                    </c:when>
                                    <c:when test="${view.bean.language == 'en'}">
                                        <option value="en" selected="selected">EN</option>
                                        <option value="de">DE</option>
                                    </c:when>
                                </c:choose>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="signatureLeft"><fmt:message key="signatureLeft" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:select styleClass="form-control" name="signatureLeft" options="employees" value="${view.bean.signatureLeft}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="signatureRight"><fmt:message key="signatureRight" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:select styleClass="form-control" name="signatureRight" options="employees" value="${view.bean.signatureRight}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"><fmt:message key="options" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreSubject"> <v:checkbox name="ignoreSubject" value="${view.bean.ignoreSubject}" /> <fmt:message key="ignoreSubjectLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreHeader"> <v:checkbox name="ignoreHeader" value="${view.bean.ignoreHeader}" /> <fmt:message key="ignoreHeaderLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreSalutation"> <v:checkbox name="ignoreSalutation" value="${view.bean.ignoreSalutation}" /> <fmt:message key="ignoreSalutationLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreGreetings"> <v:checkbox name="ignoreGreetings" value="${view.bean.ignoreGreetings}" /> <fmt:message key="ignoreGreetingsLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="contentKeepTogether">
                                    <c:choose>
                                        <c:when test="${empty view.bean.contentKeepTogether or view.bean.contentKeepTogether == 'auto'}">
                                            <v:checkbox name="contentKeepTogetherAlways" />
                                            <span><fmt:message key="contentKeepTogetherAlways" /></span>
                                        </c:when>
                                        <c:otherwise>
                                            <v:checkbox name="contentKeepTogetherAlways" value="true" />
                                            <span><fmt:message key="contentKeepTogetherAlways" /></span>
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="defaultTemplate"> <v:checkbox name="defaultTemplate" value="${view.bean.defaultTemplate}" /> <fmt:message key="templateDefaultInfo" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="embedded">
                                    <c:choose>
                                        <c:when test="${view.selectedType.embedded}">
                                            <v:checkbox name="embedded" value="${view.bean.embedded}" disabled="true" title="templateEmbeddedActivatedCheckboxTitle" />
                                            <span title="<fmt:message key="templateEmbeddedActivatedCheckboxTitle" />"><fmt:message key="templateEmbeddedLabel" /></span>
                                        </c:when>
                                        <c:otherwise>
                                            <v:checkbox name="embedded" value="${view.bean.embedded}" disabled="true" title="templateEmbeddedDeactivatedCheckboxTitle" />
                                            <span title="<fmt:message key="templateEmbeddedDeactivatedCheckboxTitle" />"><fmt:message key="templateEmbeddedLabel" /></span>
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <c:if test="${view.selectedType.embedded}">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="options"> </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="embeddedAbove"> <v:checkbox name="embeddedAbove" value="${view.bean.embeddedAbove}" /> <fmt:message key="templateEmbeddedAboveLabel" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row next">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="options"> </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <v:text styleClass="form-control" name="embeddedSpaceAfter" value="${view.bean.embeddedSpaceAfter}" />
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="embeddedSpaceAfterLabel">
                                                <fmt:message key="letterEmbeddedSpaceAfterLabel" />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </c:if>

            </div>
        </div>
    </div>

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="printGreetingsLabel" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="subject"><fmt:message key="subject" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="subject" value="${view.bean.subject}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="salutation"><fmt:message key="salutation" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="salutation" value="${view.bean.salutation}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="greetings"><fmt:message key="greetingsLabel" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="greetings" value="${view.bean.greetings}" />
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="/admin/letters/letterTemplateConfig/disableEditMode" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</v:form>
