/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory



import org.junit.Test

import static org.junit.Assert.*
import static org.mockito.Mockito.*

import com.osserp.TestConfig
import com.osserp.common.directory.impl.DirectoryManagerImpl
import com.osserp.common.OptionsCache
import com.osserp.common.directory.DirectoryUser
import com.osserp.common.directory.ldap.LdapManager
import com.osserp.common.directory.ldap.impl.LdapManagerImpl

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class IDirectoryManagerImpl extends AbstractDirectoryManagerTest {
    private static Logger logger = LoggerFactory.getLogger(IDirectoryManagerImpl.class.getName())

    @Test
    void constructorByConfigFile() {
        DirectoryManagerImpl directoryManager = new DirectoryManagerImpl(ldapManager)
        assertTrue(directoryManager.connected)
    }

    @Test
    void findUser() {
        DirectoryManagerImpl directoryManager = new DirectoryManagerImpl(ldapManager)
        if (directoryManager.connected) {
            DirectoryUser user = directoryManager.findUser(TestConfig.TESTUID, TestConfig.TESTCLIENT)
            assertNotNull(user)
        }
    }

    @Test
    void findUsers() {
        DirectoryManagerImpl directoryManager = new DirectoryManagerImpl(ldapManager)
        if (directoryManager.connected) {
            String origin = changeosserpcategory(TestConfig.TESTUID, TestConfig.TESTCLIENT, DirectoryUser.BUSINESS_CATEGORY_EMPLOYEE)
            List<DirectoryUser> users = directoryManager.findUsers()
            assertTrue(!users.empty)
            changeosserpcategory(TestConfig.TESTUID, TestConfig.TESTCLIENT, origin)
        }
    }

    @Test
    void findMailboxUsers() {
        DirectoryManagerImpl directoryManager = new DirectoryManagerImpl(ldapManager)
        if (directoryManager.connected) {
            String origin = changeosserpcategory(TestConfig.TESTUID, TestConfig.TESTCLIENT, DirectoryUser.BUSINESS_CATEGORY_MAILBOX)
            List<DirectoryUser> users = directoryManager.findMailboxUsers()
            assertTrue(!users.empty)
            changeosserpcategory(TestConfig.TESTUID, TestConfig.TESTCLIENT, origin)
        }
    }

    @Test
    void findSystemUsers() {
        DirectoryManagerImpl directoryManager = new DirectoryManagerImpl(ldapManager)
        if (directoryManager.connected) {
            String origin = changeosserpcategory(TestConfig.TESTUID, TestConfig.TESTCLIENT, DirectoryUser.BUSINESS_CATEGORY_SYSTEM)
            List<DirectoryUser> users = directoryManager.findSystemUsers()
            assertTrue(!users.empty)
            changeosserpcategory(TestConfig.TESTUID, TestConfig.TESTCLIENT, origin)
        }
    }

    @Override
    protected LdapManager getLdapManager() {
        new LdapManagerImpl(TestConfig.PROPERTY_FILE)
    }

    private String changeosserpcategory(String uid, String client, String category) {
        try {
            def ldap = getLdapManager()
            Map user = ldap.lookupUser(uid, client)
            String result = user.osserpcategory
            ldap.updateUser(user, [osserpcategory:category])
            return result
        } catch (Exception e) {
            logger.error("changeosserpcategory: failed [message=${e.getMessage()}]")
            return null
        }
    }
}
