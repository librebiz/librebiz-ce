<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestTypeChangeView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <o:out value="${view.businessCase.primaryKey}" /> - <o:out value="${view.businessCase.name}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="requestTypeChangeContent">
            <div class="row">
                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel-body">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="name"><fmt:message key="name" /></th>
                                        <th class="type"><fmt:message key="description" /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:choose>
                                        <c:when test="${empty view.transferTypes}">
                                            <tr>
                                                <td colspan="2" class="error empty"><fmt:message key="noSelectionAvailable" /></td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach var="item" items="${view.transferTypes}" varStatus="s">
                                                <tr>
                                                    <td class="name"><v:link url="${view.baseLink}/save?id=${item.id}">
                                                            <o:out value="${item.name}" />
                                                        </v:link></td>
                                                    <td class="type"><o:out value="${item.workflow.description}" /></td>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
