/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22.12.2013 
 * 
 */
package com.osserp.core.system;

import com.osserp.common.Entity;

/**
 * 
 * Provides synchronization tasks
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SyncTask extends Entity {

    static final Long STATUS_CLOSED = 2L;
    static final Long STATUS_ERROR = -1L;
    static final Long STATUS_OPEN = 0L;
    static final Long STATUS_RUNNING = 1L;

    /**
     * Provides the task configuration
     * @return config
     */
    SyncTaskConfig getConfig();

    /**
     * Provides the task status
     * @return status
     */
    Long getStatus();
    
    /**
     * Provides the corresponding i18n key of the status 
     * @return statusDisplay
     */
    String getStatusDisplay();

    /**
     * Provides the task type name
     * @return typeName
     */
    String getTypeName();

    /**
     * Provides the action name
     * @return actionName
     */
    String getActionName();

    /**
     * Provides the id of the object to sync
     * @return objectId
     */
    String getObjectId();

    /**
     * Provides the serialized data of the object (optional if synchronizer supports this)
     * @return objectData
     */
    String getObjectData();
    
    /**
     * Updates object data
     * @param objectData
     */
    void setObjectData(String objectData);

    /**
     * Provides the error message if task was stopped due to execution exceptions
     * @return errorMessage
     */
    String getErrorMessage();

    /**
     * Stops a task setting a failure status and errorMessage
     * @param message
     */
    void stop(String message);

    /**
     * Update task status to running 
     */
    void setRunning();

    /**
     * Update task status to closed 
     */
    void setClosed();
    
    /**
     * Indicates if task is closed.
     * @return true if task has status 'closed'
     */
    boolean isClosed();
    
    /**
     * Indicates if task failed
     * @return true if task has status 'error'
     */
    boolean isFailed();
    
    /**
     * Indicates if task is waiting
     * @return true if task has status 'open'
     */
    boolean isOpen();
    
    /**
     * Indicates if task is currently running
     * @return true if task has status 'running'
     */
    boolean isRunning();
}
