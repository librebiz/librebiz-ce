/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Sep-2006 16:17:24 
 * 
 */
package com.osserp.core.views;

import java.io.Serializable;
import java.util.Date;

import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StockReportDisplay implements Serializable {

    private Long id = null;
    private Date created = null;
    private Long type = null;
    private Long contactId = null;
    private String contactName = null;
    private Long productId = null;
    private String productName = null;
    private Double quantity = null;
    private Long bookType = null;
    private Long stockId = null;

    private Double resultingStock = 0d;

    protected StockReportDisplay() {
        super();
    }

    public StockReportDisplay(
            Long id,
            Date created,
            Long type,
            Long contactId,
            String contactName,
            Long productId,
            String productName,
            Double quantity,
            Long bookType,
            Long stockId) {

        this.id = id;
        this.created = created;
        this.type = type;
        this.contactId = contactId;
        this.contactName = contactName;
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.bookType = bookType;
        this.stockId = stockId;
    }

    public String getNumber() {
        return id == null ? "" : id.toString();
    }

    public Long getId() {
        return id;
    }

    public Date getCreated() {
        return created;
    }

    public Long getType() {
        return type;
    }

    public Long getBookType() {
        return bookType;
    }

    public Long getContactId() {
        return contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public Long getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public Long getStockId() {
        return stockId;
    }

    public Double getResultingStock() {
        return resultingStock;
    }

    public void calculateResultingStock(Double previous) {
        this.resultingStock = previous + quantity;
    }

    public boolean isInput() {
        return RecordType.PURCHASE_INVOICE.equals(type);
    }

    public boolean isOutputCancellation() {
        if (isInput()) {
            return false;
        }
        return DeliveryNoteType.SALES_CANCELLATION_BOOKING.equals(bookType);
    }

    public boolean isOutputInvoice() {
        if (isInput()) {
            return false;
        }
        return DeliveryNoteType.SALES_INVOICE_BOOKING.equals(bookType);
    }

    public boolean isOutputCreditNote() {
        if (isInput()) {
            return false;
        }
        return DeliveryNoteType.SALES_CREDIT_NOTE_BOOKING.equals(bookType);
    }

    // setters are protected 'cause views are not updateable

    protected void setId(Long id) {
        this.id = id;
    }

    protected void setCreated(Date created) {
        this.created = created;
    }

    protected void setType(Long type) {
        this.type = type;
    }

    protected void setBookType(Long bookType) {
        this.bookType = bookType;
    }

    protected void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    protected void setContactName(String contactName) {
        this.contactName = contactName;
    }

    protected void setProductId(Long productId) {
        this.productId = productId;
    }

    protected void setProductName(String productName) {
        this.productName = productName;
    }

    protected void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    protected void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    protected void setResultingStock(Double resultingStock) {
        this.resultingStock = resultingStock;
    }
}
