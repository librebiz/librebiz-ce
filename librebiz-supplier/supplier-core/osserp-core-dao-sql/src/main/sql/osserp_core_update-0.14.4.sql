SET search_path = osserp, pg_catalog;


CREATE TABLE record_status_infos (
    id bigserial,
    reference_id bigint,
    type_id bigint,
    status_id bigint,
    created timestamp without time zone DEFAULT now(),
    created_by bigint
);

CREATE VIEW v_record_status_infos AS
 SELECT s.id, s.reference_id, s.type_id, rs.id as status_id, rs.name, e.lastname, e.firstname, s.created
  FROM record_status_infos s
   LEFT JOIN record_status rs ON (s.status_id = rs.id)
   LEFT JOIN v_employees e ON (s.created_by = e.id)
  ORDER BY s.created DESC;


CREATE VIEW v_record_status AS select id, reference_id, 101 as type_id, status_id, created, created_by FROM purchase_offer_status_histories
union select id, reference_id, 102 as type_id, status_id, created, created_by FROM purchase_order_status_histories
union select id, reference_id, 10 as type_id, status_id, created, created_by FROM sales_cancellation_status_histories
union select id, reference_id, 9 as type_id, status_id, created, created_by FROM sales_credit_note_status_histories
union select id, reference_id, 3 as type_id, status_id, created, created_by FROM sales_delivery_note_status_histories
union select id, reference_id, 5 as type_id, status_id, created, created_by FROM sales_invoice_status_histories
union select id, reference_id, 1 as type_id, status_id, created, created_by FROM sales_offer_status_histories
union select id, reference_id, 4 as type_id, status_id, created, created_by FROM sales_order_downpayment_status_histories
union select id, reference_id, 2 as type_id, status_id, created, created_by FROM sales_order_status_histories;

CREATE OR REPLACE FUNCTION merge_record_status() RETURNS int
    LANGUAGE plpgsql
    AS $$
DECLARE
    obj         RECORD;
    n_result    INT := 0;
BEGIN
    for obj in select * from v_record_status order by created loop
        INSERT INTO record_status_infos (reference_id, type_id, status_id, created, created_by) VALUES (obj.reference_id, obj.type_id, obj.status_id, obj.created, obj.created_by);
        n_result := n_result + 1;
    end loop;
    return n_result;
END;
$$;
SELECT merge_record_status();
DROP FUNCTION merge_record_status();
DROP VIEW v_record_status;


CREATE OR REPLACE FUNCTION osserp.remove_sales_offer(offerid bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    op_result   INT := 0;
    salesid     BIGINT;
    calcid      BIGINT;
BEGIN
    SELECT sales_id INTO salesid FROM sales_offers WHERE id = offerid;
    IF NOT FOUND THEN
        RAISE NOTICE 'Did not find corresponding sales id for offer: %',offerid;
        RETURN -1;
    END IF;

    SELECT calculation_id INTO calcid FROM sales_offer_calculations WHERE reference_id = offerid;
    IF NOT FOUND THEN
        RAISE NOTICE 'Did not find corresponding calculation id for offer: %',offerid;
        RETURN -1;
    END IF;
    RAISE NOTICE 'Found sales_id %, calc id % for offer: %',salesid,calcid,offerid;

    DELETE FROM calculation_option_items WHERE reference_id IN (SELECT id FROM calculation_option_positions WHERE reference_id IN (SELECT calculation_id FROM sales_offer_calculations WHERE reference_id = offerid));
    DELETE FROM calculation_option_positions WHERE reference_id IN (SELECT calculation_id FROM sales_offer_calculations WHERE reference_id = offerid);
    DELETE FROM calculation_items WHERE reference_id IN (SELECT id FROM calculation_positions WHERE reference_id IN (SELECT calculation_id FROM sales_offer_calculations WHERE reference_id = offerid));
    DELETE FROM calculation_positions WHERE reference_id IN (SELECT calculation_id FROM sales_offer_calculations WHERE reference_id = offerid);
    DELETE FROM sales_offer_calculations WHERE reference_id = offerid;
    DELETE FROM calculations WHERE id = calcid;
    DELETE FROM sales_offer_discounts WHERE reference_id = offerid;
    DELETE FROM sales_offer_items WHERE reference_id = offerid;
    DELETE FROM sales_offer_option_items WHERE reference_id = offerid;
    DELETE FROM sales_offer_payment_agreements WHERE reference_id = offerid;
    DELETE FROM record_status_infos WHERE type_id = 1 and reference_id = offerid;
    DELETE FROM sales_offers WHERE id = offerid;
    RETURN 0;
END;
$$;

DROP VIEW purchase_offer_status_history_view;
DROP VIEW purchase_order_status_history_view;
DROP VIEW sales_cancellation_status_history_view;
DROP VIEW sales_credit_note_status_history_view;
DROP VIEW sales_delivery_note_status_history_view;
DROP VIEW sales_invoice_status_history_view;
DROP VIEW sales_offer_status_history_view;
DROP VIEW sales_order_downpayment_status_history_view;
DROP VIEW sales_order_status_history_view;

DROP TABLE purchase_offer_status_histories;
DROP TABLE purchase_order_status_histories;
DROP TABLE sales_cancellation_status_histories;
DROP TABLE sales_credit_note_status_histories;
DROP TABLE sales_delivery_note_status_histories;
DROP TABLE sales_invoice_status_histories;
DROP TABLE sales_offer_status_histories;
DROP TABLE sales_order_downpayment_status_histories;
DROP TABLE sales_order_status_histories;

UPDATE system_properties SET pvalue = '0.14.4', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
