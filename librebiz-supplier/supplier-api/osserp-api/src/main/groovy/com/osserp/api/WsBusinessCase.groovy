/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToOne
import javax.persistence.MapKeyColumn
import javax.persistence.OneToMany
import javax.persistence.Table

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Entity
@Table(name='ws_business_case')
class WsBusinessCase extends FinanceRecord implements WSBusinessCase {
    private static final long serialVersionUID = 4L

    WsBusinessCase() {
        super()
    }

    WsBusinessCase(
        WsContact contact, 
        Long businessType, 
        String context, 
        String name,
        String origin,
        String campaign,
        String note,
        String currency,
        String remoteIp) {
        super()
        this.contact = contact
        this.businessType = businessType 
        this.context = context
        this.name = name
        this.originName = origin
        this.campaignName = campaign
        this.note = note
        this.currency = currency
        this.remoteIp = remoteIp
        requestDate = new Date()
    }

    @ManyToOne
    @JoinColumn(name = "contact_id")
    WsContact contact

    @Column(name='customer_id')
    Long customerId

    @Column(name='sales_person')
    Long salesPersonId
    @Column(name='project_manager')
    Long projectManagerId

    @Column(name='branch_id')
    Long branchId
    @Column(name='branch_key')
    String branchKey

    @Column(name='business_type')
    Long businessType
    @Column(name='business_type_name')
    String businessTypeName

    String context
    String name

    String street
    @Column(name='street_addon')
    String streetAddon
    String zipcode
    String city
    @Column(name='federal_state')
    String federalState
    String country
    @Column(name='country_id')
    Long countryId

    String status
    @Column(name='status_id')
    Long statusId

    String note

    @Column(name='origin_name')
    String originName
    @Column(name='origin_id')
    Long originId

    @Column(name='campaign_name')
    String campaignName
    @Column(name='campaign_id')
    Long campaignId

    @Column(name='request_created')
    String requestCreated
    @Column(name='request_date')
    Date requestDate

    @Column(name='remoteip')
    String remoteIp

    @Column(name='remoteip_last')
    String remoteIpLast

    @Column(name='remoteip_last_date')
    Date remoteIpLastDate

    @Column(name='site_status')
    String siteStatus

    @Column(name='start_date')
    Date startDate

    @Column(name='stop_date')
    Date stopDate

    /**
     * Calculates net amount by items
     * @return calculated net amount
     */
    Double getNetAmount() {
        Double result = 0
        items.each {
            result = result + (it.getPrice() ?: 0d) 
        }
        return result
    }

    @ElementCollection(fetch=FetchType.EAGER)
    @JoinTable(name='ws_business_case_property', joinColumns=@JoinColumn(name="reference_id"))
    @MapKeyColumn(name='name')
    @Column(name='val')
    Map<String, String> propertyMap = [:]

    @Override
    Map<String, Object> getProperties() {
        propertyMap
    }

    @OneToMany(mappedBy = 'wsBusinessCase', cascade = CascadeType.ALL, orphanRemoval = true)
    List<WsBusinessCaseItem> items = []

    void addItem(WsProduct product, Double quantity, BigDecimal price, String note) {
        WsBusinessCaseItem item = new WsBusinessCaseItem(this, product, quantity, price, note)
        items << item
    }

    void deleteItem(WsBusinessCaseItem item) {
        for (Iterator i = items.iterator(); i.hasNext();) {
            WsBusinessCaseItem nextItem = i.next()
            if (nextItem.primaryKey == item.primaryKey) {
                i.remove()
            }
        }
    }

    void replaceItem(WsProduct product, Double quantity, BigDecimal price, String note) {
        if (items.size() == 1) {
            WsBusinessCaseItem item = items.get(0) 
            item.setProduct(product)
            item.setQuantity(quantity)
            item.setPrice(price)
            item.setNote(note)
        }
    }
}
