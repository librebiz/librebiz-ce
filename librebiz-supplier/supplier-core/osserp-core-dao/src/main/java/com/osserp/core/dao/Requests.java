/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2006 12:45:57 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.Option;
import com.osserp.common.User;

import com.osserp.core.BusinessCaseSearchRequest;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsItem;
import com.osserp.core.crm.CampaignAssignmentHistory;
import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerSummary;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;
import com.osserp.core.requests.RequestListItem;
import com.osserp.core.tickets.RequestResetWarningTicket;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Requests extends BusinessCaseDao {

    /**
     * Initializes a new, not persistent request object
     * @param user
     * @param customer
     * @param type
     * @return request
     */
    Request createTransient(User user, Customer customer, BusinessType type);

    /**
     * Loads a request from persistent storage
     * @param id of the request
     * @return request values
     */
    Request load(Long requestId);

    /**
     * Changes request type
     * @param user
     * @param request
     * @param type
     * @return request with changed type
     */
    Request changeType(Long user, Request request, BusinessType type);

    /**
     * Changes the primary key of the request and all dependent references.
     * @param user
     * @param request
     * @param requestId
     * @return request with changed number
     */
    Request changeRequestId(Long user, Request request, Long requestId);

    /**
     * Changes branch and dependent objects
     * @param request
     * @param branchId
     * @return request with changed branch
     */
    Request changeBranch(Request request, Long branchId);

    /**
     * Persists a request
     * @param request
     */
    void save(Request request);

    /**
     * Tries to find a request id by an offer id
     * @param offerId
     * @return request id or null
     */
    Long findIdByOffer(Long offerId);

    /**
     * Adds customers requests to given summary
     * @param summary
     */
    void addSummary(CustomerSummary summary);

    /**
     * Finds all sales requests matching provided search request
     * @param request
     * @return requests or empty list if non matches
     */
    List<RequestListItem> find(BusinessCaseSearchRequest request);

    /**
     * Updates request origin
     * @param request
     * @param origin
     * @return campaignAssignmentHistory entry
     */
    CampaignAssignmentHistory updateOrigin(Request request, Employee user, Long origin);

    /**
     * Indicates that campaignAssignment history is available
     * @param request
     * @return campaignAssignmentHistory entries orderd by id descending
     */
    boolean isCampaignAssignmentHistoryAvailable(Request request);

    /**
     * Provides the camaignAssignment history
     * @param request
     * @return campaignAssignmentHistory entries orderd by id descending
     */
    List<CampaignAssignmentHistory> getCampaignAssignmentHistory(Request request);

    /**
     * Provides all request types
     * @return requestTypes
     */
    List<Option> getRequestTypeKeys();

    /**
     * Provides all request types
     * @return requestTypes
     */
    List<BusinessType> getRequestTypes();

    /**
     * Provides all request types referenced by at least one request.
     * @param canceled Finds types with canceled requests if true
     * @return request type list
     */
    List<BusinessType> getRequestTypesByUsage(boolean canceled);

    /**
     * Provides the type of a given request
     * @param id
     * @return type
     */
    BusinessType getRequestType(Long id);

    /**
     * Provides the request type by request id
     * @param requestId
     * @return request type
     */
    BusinessType getRequestTypeByRequest(Long requestId);

    /**
     * Persists a flow control item
     * @param item
     */
    void save(FcsItem item);

    /**
     * Performs all request reset warning tasks
     */
    void executeResetWarningTasks();

    /**
     * Provides all open request reset warnings where an reset warning must be send
     * @return openRequestResetWarnings
     */
    List<RequestResetWarningTicket> getOpenRequestResetWarnings();

    /**
     * Closes a request reset warning ticket after warning was sent
     * @param ticket
     */
    void closeOpenRequestResetWarning(RequestResetWarningTicket ticket);
}
