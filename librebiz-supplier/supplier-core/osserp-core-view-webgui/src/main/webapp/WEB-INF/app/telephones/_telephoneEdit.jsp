<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.telephoneEditView}"/>
<c:set var="phone" value="${sessionScope.telephoneEditView.bean}"/>
<c:set var="url" value="/telephones/telephoneEdit"/>

<c:if test="${!empty sessionScope.error}">
	&nbsp;
	<div class="errormessage">
		<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
	</div>
    <o:removeErrors/>
</c:if>
<c:if test="${!empty view}">
	<o:logger write="view not empty" level="debug"/>
	<c:choose>
		<c:when test="${view.editMode}">
			<o:logger write="view in editMode" level="debug"/>
			<c:choose>
				<c:when test="${!empty phone}">
					<o:logger write="phone not empty" level="debug"/>
					<c:choose>
						<c:when test="${view.edit == 'telephone_system'}">
							<v:ajaxForm name="dynamicForm" targetElement="${view.edit}_${phone.id}" url="${url}/save">
								<select id="value" name="value">
									<option value="0" <c:if test="${phone.telephoneSystem.id == null || phone.telephoneSystem.id == 0}">selected="selected"</c:if>><fmt:message key="selection"/></option>
									<c:forEach var="obj" items="${view.systems}">
										<option value="${obj.id}" <c:if test="${obj.id == phone.telephoneSystem.id}">selected="selected"</c:if>><o:out value="${obj.name}"/></option>
									</c:forEach>
								</select>
								<v:ajaxLink url="${url}/disableEditMode?name=${view.edit}&id=${phone.id}" targetElement="${view.edit}_${phone.id}">
									<o:img name="backIcon" styleClass="bigicon"/>
								</v:ajaxLink>
								<input type="hidden" name="id" id="id" value="${phone.id}"/>
								<input type="hidden" name="name" id="name" value="${view.edit}"/>
								<input type="image" name="submit" src="<c:url value="${applicationScope.saveIcon}"/>" class="bigicon" title="<fmt:message key="save"/>" />
							</v:ajaxForm>
						</c:when>
						<c:when test="${view.edit == 'telephone_type'}">
							<v:ajaxForm name="dynamicForm" targetElement="${view.edit}_${phone.id}" url="${url}/save">
								<oc:select name="value" options="telephoneTypes" value="${phone.telephoneType}"/>
								<v:ajaxLink url="${url}/disableEditMode?name=${view.edit}&id=${phone.id}" targetElement="${view.edit}_${phone.id}">
									<o:img name="backIcon" styleClass="bigicon"/>
								</v:ajaxLink>
								<input type="hidden" name="id" id="id" value="${phone.id}"/>
								<input type="hidden" name="name" id="name" value="${view.edit}"/>
								<input type="image" name="submit" src="<c:url value="${applicationScope.saveIcon}"/>" class="bigicon" title="<fmt:message key="save"/>" />
							</v:ajaxForm>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					&nbsp;
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${!empty phone}">
					<o:logger write="phone not empty" level="debug"/>
					<c:choose>
						<c:when test="${view.edit == 'telephone_system'}">
							<v:ajaxLink url="${url}/enableEditMode?name=${view.edit}&id=${phone.id}" targetElement="${view.edit}_${phone.id}" title="edit">
								<c:choose>
									<c:when test="${!empty phone.telephoneSystem.name}"><o:out value="${phone.telephoneSystem.name}"/></c:when>
									<c:otherwise>(<fmt:message key="notSet"/>)</c:otherwise>
								</c:choose>
							</v:ajaxLink>
						</c:when>
						<c:when test="${view.edit == 'telephone_type'}">
							<v:ajaxLink url="${url}/enableEditMode?name=${view.edit}&id=${phone.id}" targetElement="${view.edit}_${phone.id}" title="edit">
								<c:choose>
									<c:when test="${!empty phone.telephoneType.name}"><o:out value="${phone.telephoneType.name}"/></c:when>
									<c:otherwise>(<fmt:message key="notSet"/>)</c:otherwise>
								</c:choose>
							</v:ajaxLink>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					&nbsp;
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
</c:if>
