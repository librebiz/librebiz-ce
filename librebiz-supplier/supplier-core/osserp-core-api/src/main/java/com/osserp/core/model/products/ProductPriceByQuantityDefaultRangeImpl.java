/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 13, 2011 8:59:19 PM 
 * 
 */
package com.osserp.core.model.products;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductPriceByQuantityDefault;
import com.osserp.core.products.ProductPriceByQuantityDefaultRange;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductPriceByQuantityDefaultRangeImpl extends AbstractEntity implements ProductPriceByQuantityDefaultRange {

    private Double rangeStart = Constants.DOUBLE_NULL;
    private Double rangeEnd = Constants.DOUBLE_NULL;

    protected ProductPriceByQuantityDefaultRangeImpl() {
        super();
    }

    public ProductPriceByQuantityDefaultRangeImpl(
            Employee user,
            ProductPriceByQuantityDefault reference,
            Double rangeStart,
            Double rangeEnd) {
        super((Long) null, reference.getId(), user.getId());
        this.rangeEnd = rangeEnd;
        this.rangeStart = rangeStart;
    }

    protected ProductPriceByQuantityDefaultRangeImpl(ProductPriceByQuantityDefaultRange other) {
        super(other);
        this.rangeEnd = other.getRangeEnd();
        this.rangeStart = other.getRangeStart();
    }

    public Double getRangeStart() {
        return rangeStart;
    }

    public void setRangeStart(Double rangeStart) {
        this.rangeStart = rangeStart;
    }

    public Double getRangeEnd() {
        return rangeEnd;
    }

    public void setRangeEnd(Double rangeEnd) {
        this.rangeEnd = rangeEnd;
    }
}
