/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2012 
 * 
 */
package com.osserp.core;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * Provides filter settings for business case queries.
 *  
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessCaseFilter implements Serializable {

    private Date dateFrom = null;
    private Date dateTil = null;
    
    private boolean dateByAction = true;
    private boolean dateByCreated = false;
    private boolean dateByChanged = false;
    
    private int limit = 0;
    private boolean limitUnspecifiedOnly = true;
    
    private Long companyId = null;
    private Long branchId = null;
    private Long businessTypeId = null;
    
    private boolean statusIgnorable = true;
    private boolean statusDedicated = false;
    private Long statusStart = 0L;
    private boolean statusStartEqual = true;
    private Long statusEnd = BusinessCase.CLOSED;
    private boolean statusEndEqual = false;
    
    private Long personId = null;
    private boolean personResponsible = false;
    private boolean personSales = false;
    private boolean personCreated = false;
    
    private boolean closed = false;
    private boolean cancelled = false;
    private boolean stopped = false;
    
    private boolean cached = false;
    
    public boolean isNothingToFilter() {
        return (companyId == null && branchId == null && businessTypeId == null
                && dateFrom == null && dateTil == null && personId == null
                && isPersonIgnorable() && statusIgnorable); 
    }
    
    public boolean isLimitEnabled() {
        return limit > 0 ; 
    }
    
    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public boolean isLimitUnspecifiedOnly() {
        return limitUnspecifiedOnly;
    }

    public void setLimitUnspecifiedOnly(boolean limitUnspecifiedOnly) {
        this.limitUnspecifiedOnly = limitUnspecifiedOnly;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public Long getBusinessTypeId() {
        return businessTypeId;
    }

    public void setBusinessTypeId(Long businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTil() {
        return dateTil;
    }

    public void setDateTil(Date dateTil) {
        this.dateTil = dateTil;
    }
    
    public boolean isDateIgnorable() {
        return (dateFrom == null && dateTil == null)
                || (!dateByAction && !dateByChanged && !dateByCreated);
    }

    public boolean isDateByAction() {
        return dateByAction;
    }

    public void setDateByAction(boolean dateByAction) {
        this.dateByAction = dateByAction;
    }

    public boolean isDateByCreated() {
        return dateByCreated;
    }

    public void setDateByCreated(boolean dateByCreated) {
        this.dateByCreated = dateByCreated;
    }

    public boolean isDateByChanged() {
        return dateByChanged;
    }

    public void setDateByChanged(boolean dateByChanged) {
        this.dateByChanged = dateByChanged;
    }

    public boolean isStatusIgnorable() {
        return statusIgnorable;
    }

    public void setStatusIgnorable(boolean statusIgnorable) {
        this.statusIgnorable = statusIgnorable;
    }

    public boolean isStatusDedicated() {
        return statusDedicated;
    }

    public void setStatusDedicated(boolean statusDedicated) {
        this.statusDedicated = statusDedicated;
    }

    public Long getStatusStart() {
        return statusStart;
    }

    public void setStatusStart(Long statusStart) {
        this.statusStart = statusStart;
    }

    public boolean isStatusStartEqual() {
        return statusStartEqual;
    }

    public void setStatusStartEqual(boolean statusStartEqual) {
        this.statusStartEqual = statusStartEqual;
    }

    public Long getStatusEnd() {
        return statusEnd;
    }

    public void setStatusEnd(Long statusEnd) {
        this.statusEnd = statusEnd;
    }

    public boolean isStatusEndEqual() {
        return statusEndEqual;
    }

    public void setStatusEndEqual(boolean statusEndEqual) {
        this.statusEndEqual = statusEndEqual;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public boolean isPersonResponsible() {
        return personResponsible;
    }

    public void setPersonResponsible(boolean personResponsible) {
        this.personResponsible = personResponsible;
    }

    public boolean isPersonSales() {
        return personSales;
    }

    public void setPersonSales(boolean personSales) {
        this.personSales = personSales;
    }

    public boolean isPersonCreated() {
        return personCreated;
    }

    public void setPersonCreated(boolean personCreated) {
        this.personCreated = personCreated;
    }

    public boolean isPersonIgnorable() {
        return (personId == null || personId == 0) 
                || (!personResponsible && !personSales && !personCreated);
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isStopped() {
        return stopped;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    public boolean isCached() {
        return cached;
    }

    public void setCached(boolean cached) {
        this.cached = cached;
    }
}
