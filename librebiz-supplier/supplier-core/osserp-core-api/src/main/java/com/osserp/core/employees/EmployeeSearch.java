/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 2, 2006 2:06:22 PM 
 * 
 */
package com.osserp.core.employees;

import java.util.List;

import org.jdom2.Element;

import com.osserp.common.Option;

import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.contacts.Search;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EmployeeSearch extends Search {

    /**
     * Tries to get an employee by primary key id
     * @param id of the employee
     * @return employee by id
     */
    Employee findById(Long id);

    /**
     * Provides all activated employees related to a group
     * @param user current user to filter by branch & company or null if all employees should be returned.
     * @param groupKey
     * @return employees by group depending on user
     */
    List<Employee> findByGroup(DomainUser user, String groupKey);

    /**
     * Provides an alphabetical sorted list of all active employees as view objects with current event count
     * @return list all active employees
     */
    List<EmployeeDisplay> findActive();

    /**
     * Provides an alphabetical sorted list of all active employees
     * @return list of all active employees
     */
    List<Employee> findAllActive();

    /**
     * Finds all activated employees filtered by branch.
     * @param user
     * @return active employees
     */
    List<Employee> findAllActive(DomainUser user);

    /**
     * Provides an alphabetical sorted list of all active employee names
     * @return names list of all active employees
     */
    List<Option> findActiveNames();

    /**
     * Provides all employees as xml element
     * @return employees
     */
    Element getAll();

    /**
     * Gets a sorted list of all employee names
     * @return employee names
     */
    List<Option> findNames();

    /**
     * Provides sales person(s) with sales capacity statistics
     * @param user
     * @param employee id or null if all sales persons should be fetched
     * @return list of salespersons by capacity
     */
    List<BusinessCaseRelationStat> findSalesCapacity(DomainUser user, Long employee);

    /**
     * Provides tec person(s) with sales capacity statistics
     * @param user
     * @param employee id or null if all sales persons should be fetched
     * @return list of technicians by capacity
     */
    List<BusinessCaseRelationStat> findTecCapacity(DomainUser user, Long employee);

    /**
     * Provides a role display of all currently configured employees with a given group
     * @param group
     * @return employeeRoleDisplay
     */
    List<EmployeeRoleDisplay> getRoleDisplayByGroup(Long group);
}
