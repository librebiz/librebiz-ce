/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 2, 2006 4:54:42 PM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;

import com.osserp.gui.client.contacts.RelatedContactView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRelatedContactView extends AbstractContactView
        implements RelatedContactView {
    private static Logger log = LoggerFactory.getLogger(AbstractRelatedContactView.class.getName());
    private boolean detailsEditMode = false;

    protected AbstractRelatedContactView() {
        super();
    }
    
    /* (non-Javadoc)
     * @see com.osserp.gui.client.impl.AbstractView#init(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        String exit = RequestUtil.getExit(request);
        if (isSet(exit)) {
            setExitTarget(exit);
        }
        Long id = RequestUtil.fetchId(request);
        if (isSet(id)) {
            refresh(id);
            if (log.isDebugEnabled()) {
                log.debug("init() done by provided id [id=" + id + ", exitTarget=" + getExitTarget() + ", exitId=" + getExitId() + "]");
            }
        }
    }

    public boolean isDetailsEditMode() {
        return detailsEditMode;
    }

    public void setDetailsEditMode(boolean newMode) {
        disableAllModes();
        detailsEditMode = newMode;
    }

    public abstract void updateDetails(Form fh) throws ClientException, PermissionException;

    /**
     * Updates current activated contact bean and list with backend values
     * @param id
     */
    protected void refresh(Long id) {
        try {
            ClassifiedContact updated = (ClassifiedContact) getPersonManager().find(id);
            setBean(updated);
            if (getList() != null && !getList().isEmpty()) {
                refreshListAfterSelection(updated);
            }
        } catch (Exception e) {
            throw new ActionException(e.getMessage(), e);
        }
    }

    /**
     * Updates current activated contact bean and list with values as given
     * @param contact
     */
    protected void refresh(ClassifiedContact contact) {
        try {
            setBean(contact);
            if ((getList() != null) && !getList().isEmpty()) {
                refreshListAfterSelection(contact);
            }
        } catch (Exception e) {
            throw new ActionException(e.getMessage(), e);
        }
    }

    private void refreshListAfterSelection(Contact c) {
        if (getList() != null && !getList().isEmpty()) {
            try {
                CollectionUtil.replaceByProperty(getList(), "id", c);
            } catch (Throwable t) {
                try {
                    CollectionUtil.replaceByProperty(getList(), "contactId", c);
                } catch (Throwable tx) {
                    // we ignore this
                }
            }
        }
    }

    @Override
    protected String getSelectPropertyName() {
        return "id";
    }

    @Override
    public void disableAllModes() {
        super.disableAllModes();
        detailsEditMode = false;
        if (log.isDebugEnabled()) {
            log.debug("disableAllModes() done");
        }
    }

    public boolean isDefaultDisplay() {
        if (getBean() instanceof Contact && getDefaultDisplayGroup() != null) {
            return getDefaultDisplayGroup().equals(((Contact) getBean()).getDefaultGroupDisplay());
        }
        return false;
    }

    public void toggleDefaultDisplay() {
        if (getBean() instanceof Contact) {
            Contact c = (Contact) getBean();
            if (c.getDefaultGroupDisplay() == null) {
                c.setDefaultGroupDisplay(getDefaultDisplayGroup());
            } else {
                c.setDefaultGroupDisplay(null);
            }
            getPersonManager().save(c);
        }
    }

    protected abstract String getDefaultDisplayGroup();
}
