<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<tr>
    <td><fmt:message key="deliveryAddress"/></td>
    <td>
        <c:if test="${!empty record.businessCaseId}">
            <a href="<c:url value="/purchaseOrder.do?method=setDeliveryAddressByBusinessCase"/>">
                <fmt:message key="deliveryAddressBySalesLabel"/>
            </a>
        </c:if>
    </td>
</tr>
