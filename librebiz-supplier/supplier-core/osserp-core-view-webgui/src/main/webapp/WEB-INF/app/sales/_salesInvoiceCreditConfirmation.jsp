<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="invoiceNumberShort" />
                <o:out value="${view.record.number}" />
                <span> / </span>
                <o:date value="${view.record.created}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="selectedActionName"> <v:link url="${view.baseLink}/selectPaymentType" title="selectType">
                            <fmt:message key="action" />
                        </v:link>
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <fmt:message key="creditNote" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="copyInvoiceContentLabel"> <fmt:message key="content" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="checkbox">
                        <label for="copyItems"> <v:checkbox id="copyItems" /> <fmt:message key="copyInvoiceContent" />
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for=""> <fmt:message key="bookingType" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:forEach var="bookingType" items="${view.creditNoteBookingTypes}">
                        <div class="radio">
                            <label> <input type="radio" name="id" value="${bookingType.id}" /> <o:out value="${bookingType.name}" />
                            </label>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <div class="row next">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <o:submit value="create" styleClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
