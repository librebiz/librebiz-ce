<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="userFetchmailInboxView" />
<c:set var="fetchmailInboxExitTarget" scope="request" value="userFetchmailInbox" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="userFetchmailInboxContent">
            <div class="row">
                <c:choose>
                    <c:when test="${empty view.bean}">
                        <c:import url="${viewdir}/mail/_fetchmailInboxList.jsp" />
                    </c:when>
                    <c:when test="${view.editMode}">
                        <v:form url="${view.baseLink}/save" name="userFetchmailInboxForm">
                            <c:import url="_userFetchmailInboxEdit.jsp" />
                        </v:form>
                    </c:when>
                    <c:otherwise>
                        <c:import url="${viewdir}/mail/_fetchmailInboxText.jsp" />
                        <c:import url="${viewdir}/mail/_fetchmailInboxDisplay.jsp" />
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
