/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2010 2:26:51 PM 
 * 
 */
package com.osserp.core.service.requests;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.util.CollectionUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.dao.RequestListCache;
import com.osserp.core.dao.Requests;
import com.osserp.core.requests.RequestListItemComparators;
import com.osserp.core.requests.RequestListItem;
import com.osserp.core.requests.RequestListManager;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestListManagerImpl implements RequestListManager {
    private RequestListCache lists;
    private Requests requests;

    protected RequestListManagerImpl(RequestListCache lists, Requests requests) {
        super();
        this.lists = lists;
        this.requests = requests;
    }

    public List<BusinessType> getRequestTypeSelection(boolean canceled) {
        return requests.getRequestTypesByUsage(canceled);
    }

    public List<RequestListItem> getCurrentRequests() {
        return lists.getList();
    }

    public List<RequestListItem> getLatestRequests(int fetchLimit) {
        List<RequestListItem> result = CollectionUtil.sort(lists.getList(), 
                RequestListItemComparators.createRequestListItemByCreatedComparator(true));
        return result.size() <= fetchLimit ? result : result.subList(0, fetchLimit-1);
    }

    public List<RequestListItem> getCurrentByUser(DomainUser user) {
        return filter(getCurrentRequests(), user);
    }

    protected List<RequestListItem> filter(List<RequestListItem> requestList, DomainUser user) {
        List<RequestListItem> result = new ArrayList<>();
        for (int i = 0, j = requestList.size(); i < j; i++) {
            RequestListItem next = requestList.get(i);
            if (next.getCreatedBy() != null && next.getCreatedBy().equals(user.getId())) {
                result.add(next);
            } else if (next.getSalesId() != null && next.getSalesId().equals(user.getEmployee().getId())) {
                result.add(next);
            } else if (next.getManagerId() != null && next.getManagerId().equals(user.getEmployee().getId())) {
                result.add(next);
            }
        }
        return result;
    }
}
