/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Aug 02, 2011 8:50:59 AM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.TelephoneCallContact;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneCallContactImpl extends AbstractEntity implements TelephoneCallContact {
    private Long callId;
    private Long contactId;

    protected TelephoneCallContactImpl() {
        super();
    }

    public Long getCallId() {
        return callId;
    }

    public void setCallId(Long callId) {
        this.callId = callId;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }
}
