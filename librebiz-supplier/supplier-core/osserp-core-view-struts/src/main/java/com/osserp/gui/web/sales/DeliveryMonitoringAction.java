/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 6, 2007 3:23:21 PM 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.web.RequestUtil;

import com.osserp.gui.client.sales.DeliveryMonitoringView;
import com.osserp.gui.web.products.AbstractStockSelectingAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DeliveryMonitoringAction extends AbstractStockSelectingAction {
    private static Logger log = LoggerFactory.getLogger(DeliveryMonitoringAction.class.getName());

    public ActionForward refresh(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("refresh() request from " + getUser(session).getId());
        }
        DeliveryMonitoringView view = (DeliveryMonitoringView) fetchView(session);
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward toggleMonitoringBySalesMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleMonitoringBySalesMode() request from " + getUser(session).getId());
        }
        DeliveryMonitoringView view = (DeliveryMonitoringView) fetchView(session);
        view.setMonitoringBySalesMode(!view.isMonitoringBySalesMode());
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward toggleListDescendantMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleListDescendantMode() request from " + getUser(session).getId());
        }
        DeliveryMonitoringView view = (DeliveryMonitoringView) fetchView(session);
        view.toggleListDescendantMode();
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward toggleListVacantMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleListVacantMode() request from " + getUser(session).getId());
        }
        DeliveryMonitoringView view = (DeliveryMonitoringView) fetchView(session);
        view.toggleListVacantMode();
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward toggleSnipListMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleSnipListMode() request from " + getUser(session).getId());
        }
        DeliveryMonitoringView view = (DeliveryMonitoringView) fetchView(session);
        view.setSnipListMode(!view.isSnipListMode());
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward snipList(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("snipList() request from " + getUser(session).getId());
        }
        DeliveryMonitoringView view = (DeliveryMonitoringView) fetchView(session);
        view.snipList(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward forwardProductPlanning(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardProductPlanning() request from " + getUser(session).getId());
        }
        DeliveryMonitoringView view = (DeliveryMonitoringView) fetchView(session);
        view.snipList(RequestUtil.getId(request));
        view.setSnipListMode(!view.isSnipListMode());
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    protected Class<DeliveryMonitoringView> getViewClass() {
        return DeliveryMonitoringView.class;
    }

}
