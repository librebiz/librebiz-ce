/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 1, 2008 5:29:23 PM 
 * 
 */
package com.osserp.core.sales;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.core.projects.ProjectFcsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesMonitoringItem extends SalesListItem {

    Date getInstallationDate();

    Date getVerifyDate();

    Date getReleaseDate();

    Date getStartupDate();

    Date getDownpaymentRequest();

    Date getDownpaymentResponse();

    Date getDeliveryInvoiceRequest();

    Date getDeliveryInvoiceResponse();

    Date getFinalInvoiceRequest();

    Date getFinalInvoiceResponse();

    Date getPartialPaymentDate();

    String getPartialPaymentNote();

    Date getStopDate();

    String getStopNote();

    Double getOpenAmount();

    void setOpenAmount(Double openAmount);

    void addFlowControl(
            Long id,
            Date created,
            Long createdBy,
            boolean cancelsAction,
            Long canceledBy,
            Long actionId,
            String note,
            String name,
            Integer orderId,
            Long status,
            Long group,
            boolean cancels,
            boolean stops,
            boolean startsUp,
            boolean managerSelecting,
            boolean initializing,
            boolean releaeseing,
            boolean requests_dp,
            boolean response_dp,
            boolean requests_di,
            boolean response_di,
            boolean requests_fi,
            boolean response_fi,
            boolean closingDelivery,
            boolean enablingDelivery,
            boolean partialDelivery,
            boolean confirming,
            boolean veryfing,
            boolean partialPayment);

    List<ProjectFcsDisplay> getFlowControlSheet();

    Element getValues(Map employees);
}
