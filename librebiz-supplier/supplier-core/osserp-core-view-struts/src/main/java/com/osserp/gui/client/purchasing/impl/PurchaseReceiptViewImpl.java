/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 28, 2008 12:34:40 PM 
 * 
 */
package com.osserp.gui.client.purchasing.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Records;
import com.osserp.core.finance.Stock;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductManager;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductSelectionManager;
import com.osserp.core.purchasing.PurchaseDeliveryNoteManager;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.purchasing.PurchaseReceiptView;

/**
 * 
 * Provides a view component for open purchase delivery notes (e.g. purchase receipt)
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("purchaseReceiptView")
public class PurchaseReceiptViewImpl extends AbstractView implements PurchaseReceiptView {
    private static Logger log = LoggerFactory.getLogger(PurchaseReceiptViewImpl.class.getName());

    private Product selectedProduct = null;
    private Long selectedStock = null;
    private boolean listAllMode = false;
    private String sortMethod = Records.ORDER_BY_CREATED;
    private boolean sortReverse = false;
    private ProductSelection config = null;
    private List<ProductSelectionConfigItem> productSelections = new ArrayList<ProductSelectionConfigItem>();

    protected PurchaseReceiptViewImpl() {
        super();
    }

    /**
     * Tries to initialize view by request params 'stock', 'product' and 'loadAll'. The load all param indicates that a missing product id should be interpreted
     * as all
     * @param request
     */
    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        List<DeliveryNote> notes = new ArrayList<DeliveryNote>();
        selectedStock = fetchStockId(request);
        if (selectedStock != null) {
            Long productId = RequestUtil.fetchLong(request, "product");
            if (productId != null) {
                ProductManager manager = (ProductManager) getService(ProductManager.class.getName());
                selectedProduct = manager.get(productId);
                notes = getDeliveryNoteManager().findOpen(selectedStock, productId);
                if (log.isDebugEnabled()) {
                    log.debug("init() done [product=" + selectedProduct.getProductId()
                            + ", count=" + notes.size() + "]");
                }
            } else {
                boolean loadAll = RequestUtil.getBoolean(request, "loadAll");
                if (loadAll) {
                    notes = getDeliveryNoteManager().findOpen(selectedStock, productId);
                    listAllMode = true;
                    if (log.isDebugEnabled()) {
                        log.debug("init() done [all=true, count=" + notes.size() + "]");
                    }
                }
            }
        }
        if (!notes.isEmpty()) {
            setupList(notes);
        }
    }

    @Override
    public void reload() {
        List<DeliveryNote> notes = new ArrayList<DeliveryNote>();
        if (selectedProduct != null) {
            ProductManager manager = (ProductManager) getService(ProductManager.class.getName());
            selectedProduct = manager.get(selectedProduct.getProductId());
            notes = getDeliveryNoteManager().findOpen(
                    selectedProduct.getCurrentStock(), selectedProduct.getProductId());
            listAllMode = false;
        } else if (selectedStock != null) {
            notes = getDeliveryNoteManager().findOpen(selectedStock, null);
            listAllMode = true;
        }
        if (!notes.isEmpty()) {
            setupList(notes);
        }
    }

    private void setupList(List<DeliveryNote> notes) {
        ProductSelectionManager manager = (ProductSelectionManager)
                getService(ProductSelectionManager.class.getName());
        config = manager.getSelection(getDomainEmployee(), true);
        if (config != null) {
            productSelections = config.getItems();
            if (log.isDebugEnabled()) {
                log.debug("init() config found [id=" + config.getId() + "]");
            }
        }
        Records.sort(notes, sortMethod, sortReverse);
        if (config != null && config.isIgnoreSelections()) {
            setList(notes);
        } else {
            setList(Records.filterByRecord(productSelections, notes));
        }
    }

    public boolean isListAllMode() {
        return listAllMode;
    }

    protected void setListAllMode(boolean listAllMode) {
        this.listAllMode = listAllMode;
    }

    public Product getSelectedProduct() {
        return selectedProduct;
    }

    protected void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

    protected String getSortMethod() {
        return sortMethod;
    }

    protected void setSortMethod(String sortMethod) {
        this.sortMethod = sortMethod;
    }

    @SuppressWarnings("unchecked")
    public void sort(String method) {
        if (method != null) {
            if (method.equals(sortMethod)) {
                sortReverse = !sortReverse;
            } else {
                sortReverse = false;
                sortMethod = method;
            }
            Records.sort(getList(), sortMethod, sortReverse);
        }
    }

    private Long fetchStockId(HttpServletRequest request) {
        Long result = RequestUtil.fetchLong(request, "stock");
        if (result == null) {
            Stock defaultStock = getSystemConfigManager().getDefaultStock();
            result = defaultStock.getId();
        }
        return result;
    }

    private PurchaseDeliveryNoteManager getDeliveryNoteManager() {
        return (PurchaseDeliveryNoteManager) getService(PurchaseDeliveryNoteManager.class.getName());
    }
}
