/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 8:50:57 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;
import java.util.List;

import com.osserp.common.Option;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.customers.Customer;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Customers extends BusinessDetails, ClassifiedContacts {

    /**
     * Creates a new customer with a primaryKey provided by thirdparty app
     * or user input.
     * @param primaryKey
     * @param contact
     * @param createdBy
     * @param reseller
     * @return customer 
     */
    Customer create(Long primaryKey, Contact contact, Long createdBy, boolean reseller);

    /**
     * Loads an existing customer from persistent storage
     * @param id
     * @return customer
     */
    Customer load(Long id);

    /**
     * Provides all customers declared as sales agents
     * @return agents
     */
    List<Customer> findAgents();

    /**
     * Provides all customers declared as tip providers
     * @return tipProviders
     */
    List<Customer> findTipProviders();

    /**
     * Provides customer object of a system company
     * @param company
     * @return customer or null if not exists
     */
    Customer fetchInternal(Long company);

    /**
     * Provides the primary pos account. The pos account is an internal
     * account and not regulary used.
     * @return pos account or null if none exists
     */
    Customer fetchPrimaryPosAccount();

    /**
     * Provides a list of customer value objects with given ids
     * @param customerIds
     * @return customers
     */
    List<Customer> getCustomers(Long[] customerIds);

    /**
     * Updates a customer with provided values. This method is used to update 
     * existing customer with values provided by external form or import sheet.
     * @param customer existing 
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param birthDate
     * @param fax
     * @param phone
     * @param mobile
     * @return customer with updated values
     */
    Customer update(
            Customer customer,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Date birthDate,
            boolean grantEmail,
            boolean grantPhone,
            boolean grantNone,
            String fax,
            String phone,
            String mobile);
}
