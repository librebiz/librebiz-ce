<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="projectTrackingView" />
<c:if test="${!empty obj.endTime && obj.startTime != obj.endTime}">
<c:set var="addDateTime" value="true"/>
</c:if>
<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="bookings" />
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">

                <tbody>
                    <c:forEach var="obj" items="${view.bean.records}" varStatus="s">
                        <tr>
                            <td class="listdate">
                                <c:choose>
                                    <c:when test="${view.bean.closed}">
                                        <c:choose>
                                            <c:when test="${addDateTime == true}"><o:date value="${obj.startTime}" addtime="true" /></c:when>
                                            <c:otherwise><o:date value="${obj.startTime}" /></c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <v:link url="/projects/projectTracking/enableTextEditMode?id=${obj.id}">
                                            <c:choose>
                                                <c:when test="${addDateTime == true}"><o:date value="${obj.startTime}" addtime="true" /></c:when>
                                                <c:otherwise><o:date value="${obj.startTime}" /></c:otherwise>
                                            </c:choose>
                                        </v:link>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <c:choose>
                                <c:when test="${obj.ignore}">
                                    <td class="listname">
                                        <span class="error" title="<fmt:message key="recordIgnoredOnBilling"/>"><o:out value="${obj.name}" /></span>
                                    </td>
                                    <td class="listsummary">
                                        <span class="error"><o:number value="${obj.duration}" format="currency" /></span>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td class="listname">
                                        <o:out value="${obj.name}" />
                                    </td>
                                    <td class="listsummary">
                                        <o:number value="${obj.duration}" format="currency" />
                                    </td>
                                </c:otherwise>
                            </c:choose>
                            <td class="action">
                                <c:if test="${!view.bean.closed}">
                                    <v:link url="/projects/projectTracking/deleteText?id=${obj.id}" confirm="true" message="confirmDeleteBooking" title="deleteBooking">
                                        <o:img name="deleteIcon" styleClass="bigIcon" />
                                    </v:link>
                                    <v:link url="/projects/projectTracking/toggleIgnore?id=${obj.id}">
                                        <c:choose>
                                            <c:when test="${obj.ignore}">
                                                <o:img name="toggleStoppedTrueIcon" styleClass="bigIcon"  title="ignoreOnBillingReverse" />
                                            </c:when>
                                            <c:otherwise>
                                                <o:img name="toggleStoppedFalseIcon" styleClass="bigIcon" title="ignoreOnBillingTitle" />
                                            </c:otherwise>
                                        </c:choose>
                                    </v:link>
                                </c:if>
                            </td>
                        </tr>
                        <c:if test="${!empty obj.description}">
                            <tr>
                                <td><c:if test="${addDateTime == true}"><o:date value="${obj.endTime}" addtime="true" /></c:if> </td>
                                <td colspan="2">
                                    <o:out value="${obj.description}" />
                                </td>
                                <td> </td>
                            </tr>
                        </c:if>
                        
                    </c:forEach>
                </tbody>

            </table>
        </div>

    </div>
</div>
