/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import com.osserp.common.directory.ldap.FetchmailLdapManager
import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.LdapException
import com.osserp.common.directory.ldap.dao.FetchmailAccounts

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class FetchmailLdapManagerImpl extends AbstractLdapManager implements FetchmailLdapManager {

    private FetchmailAccounts fetchmailAccounts

    protected FetchmailLdapManagerImpl() {
        super()
    }

    FetchmailLdapManagerImpl(String configFile) {
        super(configFile)
        fetchmailAccounts = new FetchmailAccountsImpl(config)
    }

    FetchmailLdapManagerImpl(LdapConfig configInstance) {
        super(configInstance)
        fetchmailAccounts = new FetchmailAccountsImpl(config)
    }

    Map lookupFetchmailAccount(Map user) {
        fetchmailAccounts.lookupFetchmailAccount(user)
    }

    List<Map> lookupFetchmailAccounts(String client) {
        fetchmailAccounts.findAll(client)
    }

    List<Map> lookupUsersWithoutFetchmail(String client) {
        List<Map> allUsers = lookupUsers(client)
        List<Map> result = []
        for (user in allUsers) {
            if (!user.mailAccount
            && (!user.mailenabled || user.mailenabled == 'NO')
            && (!user.ou || user.ou != 'Resign')) {
                result << user
            }
        }
        return result.sort() { Map mapA, Map mapB ->
            return mapA.uid.compareToIgnoreCase(mapB.uid)
        }
    }

    String createMboxIdSuggestion(String client) {
        List<Map> existing = users.findAll(client)
        int currentMax = 0
        for (user in existing) {
            int nextCounter = fetchGecosCounter(user.gecos)
            if (nextCounter > currentMax) {
                currentMax = nextCounter
            }
        }
        return sprintf('glxx%04d', (currentMax + 1))
    }

    boolean validateMboxId(String mboxid) {
        return ( (mboxid && (mboxid =~ /glxx\d{4}/))
                && (mboxid.length() == MBOX_GECOS_LENGTH) )
    }

    Map createMbox(Map user, String mail, String fetchmailAccount, String fetchmailPassword) throws LdapException {
        users.addMailaccount(user, mail, fetchmailAccount, fetchmailPassword)
    }

    Map enableMbox(Map mbox) {
        if (mbox && mbox.dn) {
            return fetchmailAccounts.enableAccount(mbox, null)
        }
        return mbox
    }

    Map disableMbox(Map mbox) {
        if (mbox && mbox.dn) {
            return fetchmailAccounts.disableAccount(mbox, null)
        }
        return mbox
    }

    void deleteMbox(Map mbox) {
        fetchmailAccounts.deleteAccount(mbox)
    }

    Map updateMbox(Map mbox, Map values) throws LdapException {
        try {
            return fetchmailAccounts.update(mbox, values)
        } catch (Throwable t) {
            throw new LdapException(t.message, t)
        }
    }

    private int fetchGecosCounter(String gecos) {
        final int gecosStart = 4
        if (validateMboxId(gecos)) {
            try {
                return Integer.valueOf(gecos.substring(gecosStart, MBOX_GECOS_LENGTH))
            } catch (NumberFormatException nfe) {
            }
        }
        return 0
    }
}
