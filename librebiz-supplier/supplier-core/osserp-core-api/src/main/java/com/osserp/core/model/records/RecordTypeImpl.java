/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 3 Mar 2007 09:55:55 
 * 
 */
package com.osserp.core.model.records;

import com.osserp.common.beans.OptionImpl;

import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordTypeImpl extends OptionImpl implements RecordType {
    
    private static final int MIN_NUM_BY_DATE_DIGITS = 2;
    private boolean supportingBookingTypes = false;
    private boolean supportingDeliveryDate = false;
    private boolean supportingDeliveryDateOnItems = false;
    private boolean itemsChangeableWhenReleased = false;
    private boolean planningProductAware = false;
    private boolean supportingDeliveryAck = false;
    private boolean stockAware = false;
    private boolean supportingCorrections = false;
    private boolean addConditionsOnCreate = false;
    private boolean addTermsAndConditionsOnPrint = false;
    private boolean supportingConditions = false;
    private String displayPricePermissions = null;
    private boolean formatNumber = false;
    private boolean addNumberPrefix = false;
    private String numberPrefix = null;
    private String tableName = null;
    private String numberCreatorName = null;
    private boolean numberCreatorFixed = false;
    private int uniqueNumberDigitCount = 2;
    private Long documentTypeId = null;
    private boolean overrideCreatedDate = false;
    private boolean supportingOutpayments = false;
    private boolean supportingPayments = false;
    private boolean supportingPrint = false;
    private boolean displayNotesBelowItems = false;
    private boolean addProductDatasheet = false;
    private boolean addRightToCancelOnPrint = false;
    private boolean paymentOnly = false;
    private Long releasedStatus = Record.STAT_SENT;
    private String permissions;


    protected RecordTypeImpl() {
        super();
    }

    public RecordTypeImpl(String name) {
        super(null, name);
    }

    public RecordTypeImpl(Long id, String name, boolean paymentOnly) {
        super(id, name);
        this.paymentOnly = paymentOnly;
    }

    protected RecordTypeImpl(Long id, String name) {
        super(id, name);
    }

    protected RecordTypeImpl(RecordType o) {
        super(o);
        this.supportingBookingTypes = o.isSupportingBookingTypes();
        this.supportingDeliveryDate = o.isSupportingDeliveryDate();
        this.supportingDeliveryDateOnItems = o.isSupportingDeliveryDateOnItems();
        this.itemsChangeableWhenReleased = o.isItemsChangeableWhenReleased();
        this.planningProductAware = o.isPlanningProductAware();
        this.supportingDeliveryAck = o.isSupportingDeliveryAck();
        this.stockAware = o.isStockAware();
        this.supportingCorrections = o.isSupportingCorrections();
        this.addConditionsOnCreate = o.isAddConditionsOnCreate();
        this.addTermsAndConditionsOnPrint = o.isAddTermsAndConditionsOnPrint();
        this.supportingConditions = o.isSupportingConditions();
        this.displayPricePermissions = o.getDisplayPricePermissions();
        this.formatNumber = o.isFormatNumber();
        this.addNumberPrefix = o.isAddNumberPrefix();
        this.numberPrefix = o.getNumberPrefix();
        this.tableName = o.getTableName();
        this.numberCreatorName = o.getNumberCreatorName();
        this.uniqueNumberDigitCount = o.getUniqueNumberDigitCount();
        this.documentTypeId = o.getDocumentTypeId();
        this.overrideCreatedDate = o.isOverrideCreatedDate();
        this.supportingPrint = o.isSupportingPrint();
        this.addProductDatasheet = o.isAddProductDatasheet();
        this.addRightToCancelOnPrint = o.isAddRightToCancelOnPrint();
        this.permissions = o.getPermissions();
    }
    
    public boolean isPayment() {
        return false;
    }

    public boolean isPaymentOnly() {
        return paymentOnly;
    }

    public void setPaymentOnly(boolean paymentOnly) {
        this.paymentOnly = paymentOnly;
    }

    public boolean isPlanningProductAware() {
        return planningProductAware;
    }

    public void setPlanningProductAware(boolean planningProductAware) {
        this.planningProductAware = planningProductAware;
    }

    public boolean isAddProductDatasheet() {
        return addProductDatasheet;
    }

    public void setAddProductDatasheet(boolean addProductDatasheet) {
        this.addProductDatasheet = addProductDatasheet;
    }

    public boolean isAddRightToCancelOnPrint() {
        return addRightToCancelOnPrint;
    }

    public void setAddRightToCancelOnPrint(boolean addRightToCancelOnPrint) {
        this.addRightToCancelOnPrint = addRightToCancelOnPrint;
    }

    public boolean isAddConditionsOnCreate() {
        return addConditionsOnCreate;
    }

    public void setAddConditionsOnCreate(boolean addConditionsOnCreate) {
        this.addConditionsOnCreate = addConditionsOnCreate;
    }

    public boolean isAddTermsAndConditionsOnPrint() {
        return addTermsAndConditionsOnPrint;
    }

    public void setAddTermsAndConditionsOnPrint(boolean addTermsAndConditionsOnPrint) {
        this.addTermsAndConditionsOnPrint = addTermsAndConditionsOnPrint;
    }

    public boolean isSupportingConditions() {
        return supportingConditions;
    }

    public void setSupportingConditions(boolean supportingConditions) {
        this.supportingConditions = supportingConditions;
    }

    public boolean isSupportingBookingTypes() {
        return supportingBookingTypes;
    }

    public void setSupportingBookingTypes(boolean supportingBookingTypes) {
        this.supportingBookingTypes = supportingBookingTypes;
    }

    public boolean isSupportingDeliveryDate() {
        return supportingDeliveryDate;
    }

    public void setSupportingDeliveryDate(boolean supportingDeliveryDate) {
        this.supportingDeliveryDate = supportingDeliveryDate;
    }

    public boolean isSupportingDeliveryDateOnItems() {
        return supportingDeliveryDateOnItems;
    }

    public void setSupportingDeliveryDateOnItems(
            boolean supportingDeliveryDateOnItems) {
        this.supportingDeliveryDateOnItems = supportingDeliveryDateOnItems;
    }

    public boolean isSupportingDeliveryAck() {
        return supportingDeliveryAck;
    }

    public void setSupportingDeliveryAck(boolean supportingDeliveryAck) {
        this.supportingDeliveryAck = supportingDeliveryAck;
    }

    public boolean isSupportingOutpayments() {
        return supportingOutpayments;
    }

    public void setSupportingOutpayments(boolean supportingOutpayments) {
        this.supportingOutpayments = supportingOutpayments;
    }

    public boolean isSupportingPayments() {
        return supportingPayments;
    }

    public void setSupportingPayments(boolean supportingPayments) {
        this.supportingPayments = supportingPayments;
    }

    public boolean isSupportingPrint() {
        return supportingPrint;
    }

    public void setSupportingPrint(boolean supportingPrint) {
        this.supportingPrint = supportingPrint;
    }

    public boolean isItemsChangeableWhenReleased() {
        return itemsChangeableWhenReleased;
    }

    public void setItemsChangeableWhenReleased(boolean itemsChangeableWhenReleased) {
        this.itemsChangeableWhenReleased = itemsChangeableWhenReleased;
    }

    public boolean isStockAware() {
        return stockAware;
    }

    public void setStockAware(boolean stockAware) {
        this.stockAware = stockAware;
    }

    public boolean isSupportingCorrections() {
        return supportingCorrections;
    }

    public void setSupportingCorrections(boolean supportingCorrections) {
        this.supportingCorrections = supportingCorrections;
    }

    public boolean isFormatNumber() {
        return formatNumber;
    }

    public void setFormatNumber(boolean formatNumber) {
        this.formatNumber = formatNumber;
    }

    public String getNumberPrefix() {
        return numberPrefix;
    }

    public void setNumberPrefix(String numberPrefix) {
        this.numberPrefix = numberPrefix;
    }

    public boolean isAddNumberPrefix() {
        return addNumberPrefix;
    }

    public void setAddNumberPrefix(boolean addNumberPrefix) {
        this.addNumberPrefix = addNumberPrefix;
    }

    public String getDisplayPricePermissions() {
        return displayPricePermissions;
    }

    public void setDisplayPricePermissions(String displayPricePermissions) {
        this.displayPricePermissions = displayPricePermissions;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public boolean isNumberCreatorBySequence() {
        return RecordNumberCreator.SEQUENCE.equalsIgnoreCase(numberCreatorName);
    }

    public String getNumberCreatorName() {
        return numberCreatorName;
    }

    public void setNumberCreatorName(String numberCreatorName) {
        this.numberCreatorName = numberCreatorName;
    }

    public boolean isNumberCreatorFixed() {
        return numberCreatorFixed;
    }

    public void setNumberCreatorFixed(boolean numberCreatorFixed) {
        this.numberCreatorFixed = numberCreatorFixed;
    }

    public int getUniqueNumberDigitCount() {
        return uniqueNumberDigitCount;
    }

    public void setUniqueNumberDigitCount(int uniqueNumberDigitCount) {
        this.uniqueNumberDigitCount = uniqueNumberDigitCount;
    }

    public void updateNumberCreator(Long user, String name, int digits) {
        if (isSet(name)) {
            numberCreatorName = name;
            if (RecordNumberCreator.DATE_SEQUENCE.equals(numberCreatorName) 
                    && digits >= MIN_NUM_BY_DATE_DIGITS) {
                uniqueNumberDigitCount = digits;
            }
            updateChanged(user);
        }
    }

    public Long getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(Long documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public Long getReleasedStatus() {
        return releasedStatus;
    }

    public void setReleasedStatus(Long releasedStatus) {
        this.releasedStatus = releasedStatus;
    }

    public boolean isOverrideCreatedDate() {
        return overrideCreatedDate;
    }

    public void setOverrideCreatedDate(boolean overrideCreatedDate) {
        this.overrideCreatedDate = overrideCreatedDate;
    }

    public boolean isDisplayNotesBelowItems() {
        return displayNotesBelowItems;
    }

    public void setDisplayNotesBelowItems(boolean displayNotesBelowItems) {
        this.displayNotesBelowItems = displayNotesBelowItems;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }
}
