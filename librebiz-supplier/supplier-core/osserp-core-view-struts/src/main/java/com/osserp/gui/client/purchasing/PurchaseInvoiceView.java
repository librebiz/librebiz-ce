/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 26-Jun-2006 14:37:13 
 * 
 */
package com.osserp.gui.client.purchasing;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.Invoice;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.sales.Sales;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("purchaseInvoiceView")
public interface PurchaseInvoiceView extends PurchaseInvoiceAwareView {

    /**
     * Initializes display mode with provided record
     * @param invoice
     * @param exitTarget
     */
    public void initDisplay(Invoice invoice, String exitTarget);

    /**
     * Indicates if invoice amount should be displayed in red if invoice is not paid.
     * @return true if warnings enabled
     */
    public boolean isPurchasePaymentWarnings();

    /**
     * Creates a new invoice by order
     * @param order
     * @throws ClientException if create failed
     * @throws PermissionException if user has no permissions
     */
    public void create(PurchaseOrder order) throws ClientException, PermissionException;

    /**
     * Releases (e.g. closes) purchase invoice
     * @throws ClientException if record has no items
     * @throws PermissionException if user has no permissions
     */
    public void release() throws ClientException, PermissionException;

    /**
     * Indicates that view is in delete closed mode
     * @return deleteClosedMode
     */
    public boolean isDeleteClosedMode();

    /**
     * Enables/disables delete closed mode
     * @param deleteClosedMode
     */
    public void setDeleteClosedMode(boolean deleteClosedMode);

    /**
     * Deletes current -closed- record
     * @param form with info in 'note' field
     * @throws ClientException if note is empty
     * @throws PermissionException if current user has no permissions
     */
    public void deleteClosed(Form form) throws ClientException, PermissionException;

    /**
     * Reopens closed purchase invoice
     * @throws ClientException if operation not supported by sub type
     * @throws PermissionException if user has no permission
     */
    public void openClosed() throws ClientException, PermissionException;

    /**
     * Indicates that current record is reopened
     * @return recordReopened
     */
    public boolean isRecordReopened();

    /**
     * Provides current delivery date until
     * @return deliveredFrom date
     */
    public Date getDeliveredFrom();

    /**
     * Sets the delivery date from
     * @param deliveredFrom
     */
    public void setDeliveredFrom(Date deliveredFrom);

    /**
     * Provides current delivery date until
     * @return deliveredUntil date
     */
    public Date getDeliveredUntil();

    /**
     * Sets the delivery date until
     * @param deliveredUntil
     */
    public void setDeliveredUntil(Date deliveredUntil);

    /**
     * Provides current search value
     * @return value
     */
    public String getValue();

    /**
     * Sets the search value
     * @param value
     */
    public void setValue(String value);

    /**
     * Provides referenced sales if exists
     * @return referencedSales sales or null if not exists
     */
    public Sales getReferencedSales();
    
    /**
     * Loads another purchaseInvoice instead of currently loaded.
     * This method is used when switching from one invoice display
     * to another while keeping other view properties as is.
     * @param id
     */
    public void loadOther(Long id);
}
