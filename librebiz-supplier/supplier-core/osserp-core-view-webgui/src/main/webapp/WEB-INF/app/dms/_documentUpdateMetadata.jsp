<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:form url="${view.baseLink}/updateMetadata" name="documentForm">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="documentEdit" />
                </h4>
            </div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="filename"><fmt:message key="filename" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.fileNameDisplay}" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="label"><fmt:message key="label" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="note" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <c:if test="${view.validityPeriodSupported}">
                <v:date name="validFrom" styleClass="form-control" label="dateStartLabel" picker="true" />
                <v:date name="validTil" styleClass="form-control" label="til" picker="true" />
            </c:if>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/select" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</v:form>
