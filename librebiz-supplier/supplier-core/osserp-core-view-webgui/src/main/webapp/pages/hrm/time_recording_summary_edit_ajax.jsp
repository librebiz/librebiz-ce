<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<c:set var="recordingView" value="${sessionScope.timeRecordingView}" />
<c:set var="recording" value="${recordingView.timeRecording}" />
<c:set var="period" value="${recording.selectedPeriod}" />
<c:set var="config" value="${period.config}" />

<o:ajaxForm name="dynamicForm" onSuccess="" preRequest="" postRequest="" targetElement="periodUpdate_popup" url="/timeRecordings.do">
    <input type="hidden" name="method" value="updatePeriod" />
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <o:out value="${recording.reference}" />
            <span> - </span>
            <oc:employee value="${recording.reference}" />
        </div>
    </div>
    <div class="modalBoxData">

        <div class="col-md-12 panel-area panel-area-default">
            <c:if test="${!empty sessionScope.errors}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="errormessage">
                            <fmt:message key="error" />: <o:out value="${sessionScope.errors}" />
                        </h4>
                    </div>
                </div>
                <o:removeErrors />
            </c:if>

            <div class="panel-body">
                <div class="form-body">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="profile" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${config.name}" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="description" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${config.description}" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="coreTime" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${config.coreTimeStartDisplay}" />
                                <fmt:message key="until" />
                                <o:out value="${config.coreTimeEndDisplay}" />
                                <fmt:message key="clock" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="validFrom"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="validFrom" value="<o:date value="${period.validFrom}"/>" id="validFromInput" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <o:datepicker input="validFromInput"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="validUntil"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${empty period.validTil}">
                                        <input type="text" class="form-control" name="validTil" id="validTilInput" />
                                    </c:when>
                                    <c:otherwise>
                                        <input type="text" class="form-control" name="validTil" value="<o:date value="${period.validTil}"/>" id="validTilInput" />
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <o:datepicker input="validTilInput"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>

                    <div class="row next">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="federalState" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <oc:select name="state" options="federalStates" value="${period.state}" styleClass="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="terminalChipNumber" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="terminalChipNumber" value="<o:out value="${recording.terminalChipNumber}"/>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="annualLeave" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control right" disabled="disabled" name="annualLeaveDisplay" value="<o:number value="${config.annualLeave}" format="currency"/>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="daysLabel" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="extraLeave" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control right" name="extraLeave" value="<o:number value="${period.extraLeave}" format="currency"/>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="daysLabel" />
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="firstYearLeave" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control right" name="firstYearLeave" value="<o:number value="${period.firstYearLeave}" format="currency"/>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="daysLabel" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="carryoverLeaveLabel" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control right" name="carryoverLeave" value="<o:number value="${period.carryoverLeave}" format="currency"/>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="daysLabel" />
                            </div>
                        </div>
                    </div>

                    <div class="row next">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="hoursPerDayLabel" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control right" disabled="disabled" name="hoursDailyDisplay" value="<o:number value="${config.hoursDaily}" format="currency" />" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="hourShort" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="maximumCarryoverHoursLabel" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control right" disabled="disabled" name="carryoverHoursMaxDisplay" value="<o:number value="${config.carryoverHoursMax}" format="currency" />" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="hourShort" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="carryoverHoursLabel" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control right" name="carryoverHours" value="<o:number value="${period.carryoverHours}" format="currency"/>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="hourShort" />
                            </div>
                        </div>
                    </div>

                    <div class="row next">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/timeRecordings.do?method=disableSetup"/>');" value="<fmt:message key="exit"/>" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <o:submit styleClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</o:ajaxForm>
