/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 22, 2009 8:29:45 PM 
 * 
 */
package com.osserp.gui.web.products;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Actions;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;

import com.osserp.core.products.Product;

import com.osserp.gui.client.products.ProductSearchAwareView;
import com.osserp.gui.client.products.ProductView;
import com.osserp.gui.web.struts.AbstractSearchByMethodAction;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public abstract class AbstractProductSearchAction extends AbstractSearchByMethodAction {
    private static Logger log = LoggerFactory.getLogger(AbstractProductSearchAction.class.getName());

    @Override
    protected abstract Class<? extends ProductSearchAwareView> getViewClass();

    /**
     * Updates the product classification
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateClassification(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateClassification() request from " + getUser(session).getId());
        }
        ProductSearchAwareView view = getProductSearchAwareView(session);
        view.updateClassification(new StrutsForm(form));
        return mapping.findForward("productSearchClassification");
    }

    /**
     * Find products by id
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward byProductId(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("byProductId() requested");
        }
        return search(mapping, form, request);
    }

    /**
     * Find products by shortname
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward byMatchcode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("byMatchcode() requested");
        }
        return search(mapping, form, request);
    }

    /**
     * Find products by fullname
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward byName(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("byName() requested");
        }
        return search(mapping, form, request);
    }

    /**
     * Find products by product selection config
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward bySelectionConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("bySelectionConfig() requested");
        }
        return search(mapping, form, request);
    }

    /**
     * Find products by description
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward byDescription(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("byDescription() requested");
        }
        return search(mapping, form, request);
    }

    /**
     * Changes workflow of add product actions
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleKeepViewAfterAdd(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        ProductSearchAwareView view = getProductSearchAwareView(session);
        view.toggleKeepViewAfterAdd();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables/disables lazy ignoring on a lazy ignoring view.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleLazyIgnoring(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleLazyIgnoring() request from " + getUser(session).getId());
        }
        ProductSearchAwareView view = getProductSearchAwareView(session);
        view.setIgnoreLazy(!view.isIgnoreLazy());
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    public ActionForward select(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("select() request from " + getUser(session).getId());
        }
        ProductSearchAwareView view = getProductSearchAwareView(session);
        super.select(mapping, form, request, response);
        Product product = view.getProduct();
        ProductView productView = (ProductView) createView(request, ProductView.class.getName());
        productView.load(product, "productSearchDisplay", RequestUtil.getExitId(request));
        return mapping.findForward(productView.getForwardTarget());
    }

    @Override
    public ActionForward redisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("redisplay() request from " + getUser(session).getId());
        }
        ProductSearchAwareView view = fetchProductSearchAwareView(session);
        if (view == null) {
            return forward(mapping, form, request, response);
        }
        if (view.isListSelectionMode()) {
            view.cutList();
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Sets search methods and forwards to the search form
     * @param mapping
     * @param form
     * @param request
     * @param columnKey
     * @return forward
     * @throws Exception
     */
    private ActionForward search(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("search() request from " + getUser(session).getId());
        }
        ProductSearchAwareView view = getProductSearchAwareView(session);
        try {
            view.search(new StrutsForm(form));
        } catch (Throwable t) {
            // we ignore this, list is empty in this case
            log.warn("search() failure ignored [message=" + t.toString() + "]");
        }
        Object target = view.getLocalValue("searchResultTarget");
        if (target != null) {
            return mapping.findForward(target.toString());
        }
        return mapping.findForward(Actions.RESULT);
    }

    @Override
    public ActionForward save(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("save() request from " + getUser(session).getId());
        }
        ProductSearchAwareView view = getProductSearchAwareView(session);
        try {
            Form f = createForm(form, request);
            view.save(f);
            if (view.isKeepViewAfterAdd()) {
                return mapping.findForward(view.getActionTarget());
            }
            String exitTarget = (view.getExitTarget() == null ? Actions.EXIT : view.getExitTarget());
            removeView(session);
            return mapping.findForward(exitTarget);

        } catch (ClientException c) {
            if (log.isDebugEnabled()) {
                log.debug("save() caught client exception [message=" + c.getMessage() + "]");
            }
            if (c.getErrorId() != null) {
                saveError(request, c.getMessage());
                String exitTarget = (view.getExitTarget() == null ? Actions.EXIT : view.getExitTarget());
                return mapping.findForward(exitTarget);
            }
            return saveErrorAndReturn(request, c.getMessage());

        } catch (PermissionException p) {
            if (log.isDebugEnabled()) {
                log.debug("save() caught permission exception [message=" + p.getMessage() + "]");
            }
            return saveErrorAndReturn(request, p.getMessage());
        }
    }

    public ActionForward selectProductSelectionConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectProductSelectionConfig() request from " + getUser(session).getId());
        }
        ProductSearchAwareView view = getProductSearchAwareView(session);
        view.selectProductSelectionConfig(RequestUtil.fetchId(request));
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward selectProductSelectionConfigItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectProductSelectionConfigItem() request from " + getUser(session).getId());
        }
        ProductSearchAwareView view = getProductSearchAwareView(session);
        view.selectProductSelectionConfigItem(RequestUtil.fetchId(request));
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    protected void removeView(HttpSession session) {
        super.removeView(session);
        session.removeAttribute("searchView");
    }

    protected ProductSearchAwareView getProductSearchAwareView(HttpSession session) {
        ProductSearchAwareView view = fetchProductSearchAwareView(session);
        if (view == null) {
            throw new ActionException(ActionException.NO_VIEW_BOUND);
        }
        return view;
    }

    private ProductSearchAwareView fetchProductSearchAwareView(HttpSession session) {
        Object view = getView(session);
        if (view == null || !(view instanceof ProductSearchAwareView)) {
            return null;
        }
        return (ProductSearchAwareView) view;
    }
}
