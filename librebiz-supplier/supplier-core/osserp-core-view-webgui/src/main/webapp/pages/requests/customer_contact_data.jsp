<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="customer" value="${requestScope.customer}" />

<div class="contentBox">
    <div class="contentBoxHeader">
        <div class="contentBoxHeaderLeft">
            <div class="subHeaderLeft">&nbsp;</div>
            <div class="subHeaderRight">
                <fmt:message key="contactData" />
            </div>
        </div>
    </div>
    <div class="contentBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>
                <table class="valueTable first33">
                    <c:choose>
                        <c:when test="${customer.grantContactNone}">
                            <tr>
                                <td colspan="2"><fmt:message key="blockedDataInfo" /></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td><fmt:message key="phone" /></td>
                                <td><c:if test="${!empty customer.phone.number}">
                                        <oc:phone value="${customer.phone}" />
                                    </c:if></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="fax" /></td>
                                <td><c:if test="${!empty customer.fax.number}">
                                        <oc:phone value="${customer.fax}" />
                                    </c:if></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="mobile" /></td>
                                <td><c:if test="${!empty customer.mobile.number}">
                                        <oc:phone value="${customer.mobile}" />
                                    </c:if></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="email" /></td>
                                <td><c:choose>
                                        <c:when test="${empty customer.email}">
                                            <fmt:message key="common.unknown" />
                                        </c:when>
                                        <c:otherwise>
                                            <o:email value="${customer.email}" limit="30" />
                                        </c:otherwise>
                                    </c:choose></td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                </table>
                <div class="spacer"></div>
            </div>
        </div>

    </div>
</div>
