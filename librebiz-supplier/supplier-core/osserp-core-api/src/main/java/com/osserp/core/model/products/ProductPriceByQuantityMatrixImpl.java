/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 13, 2011 6:58:48 PM 
 * 
 */
package com.osserp.core.model.products;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductPriceByQuantity;
import com.osserp.core.products.ProductPriceByQuantityMatrix;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductPriceByQuantityMatrixImpl extends AbstractEntity implements ProductPriceByQuantityMatrix {

    private Double consumerMargin = Constants.DOUBLE_NULL;
    private Double resellerMargin = Constants.DOUBLE_NULL;
    private Double partnerMargin = Constants.DOUBLE_NULL;

    private Double consumerMinimumMargin = Constants.DOUBLE_NULL;
    private Double resellerMinimumMargin = Constants.DOUBLE_NULL;
    private Double partnerMinimumMargin = Constants.DOUBLE_NULL;
    private List<ProductPriceByQuantity> ranges = new ArrayList<ProductPriceByQuantity>();

    protected ProductPriceByQuantityMatrixImpl() {
        super();
    }

    public ProductPriceByQuantityMatrixImpl(
            Employee user,
            Product reference) {
        super((Long) null, reference.getProductId(), user.getId());
        setConsumerMargin(reference.getGroup().getConsumerMargin());
        setConsumerMinimumMargin(getConsumerMargin());
        setResellerMargin(reference.getGroup().getResellerMargin());
        setResellerMinimumMargin(getResellerMargin());
        setPartnerMargin(reference.getGroup().getPartnerMargin());
        setPartnerMinimumMargin(getPartnerMargin());
    }

    protected ProductPriceByQuantityMatrixImpl(
            Employee user,
            Long reference) {
        super((Long) null, reference, user.getId());
    }

    protected ProductPriceByQuantityMatrixImpl(ProductPriceByQuantityMatrix other) {
        super(other);
    }

    public void deactivate(Employee user) {
        setEndOfLife(true);
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
    }

    public Double getConsumerMargin() {
        return consumerMargin;
    }

    public void setConsumerMargin(Double consumerMargin) {
        this.consumerMargin = consumerMargin;
    }

    public Double getPartnerMargin() {
        return partnerMargin;
    }

    public void setPartnerMargin(Double partnerMargin) {
        this.partnerMargin = partnerMargin;
    }

    public Double getResellerMargin() {
        return resellerMargin;
    }

    public void setResellerMargin(Double resellerMargin) {
        this.resellerMargin = resellerMargin;
    }

    public Double getConsumerMinimumMargin() {
        return consumerMinimumMargin;
    }

    public void setConsumerMinimumMargin(Double consumerMinimumMargin) {
        this.consumerMinimumMargin = consumerMinimumMargin;
    }

    public Double getResellerMinimumMargin() {
        return resellerMinimumMargin;
    }

    public void setResellerMinimumMargin(Double resellerMinimumMargin) {
        this.resellerMinimumMargin = resellerMinimumMargin;
    }

    public Double getPartnerMinimumMargin() {
        return partnerMinimumMargin;
    }

    public void setPartnerMinimumMargin(Double partnerMinimumMargin) {
        this.partnerMinimumMargin = partnerMinimumMargin;
    }

    public void addRange(Employee user, Double value) {
        boolean changed = true;
        ProductPriceByQuantity last = (ranges.isEmpty() ? null : ranges.get(ranges.size() - 1));
        if (last == null) {
            ProductPriceByQuantity obj = new ProductPriceByQuantityImpl(user, this, 0d, value);
            ranges.add(obj);
        } else {
            Double startValue = last.getRangeEnd();
            if (startValue == null || startValue == 0) {
                // current last range is marked as final
                if (value != null && value != 0) {
                    last.update(value);
                    ProductPriceByQuantity obj = new ProductPriceByQuantityImpl(user, this, value, 0d);
                    ranges.add(obj);
                } else {
                    changed = false;
                }
            } else {
                ProductPriceByQuantity obj = new ProductPriceByQuantityImpl(user, this, startValue, value);
                ranges.add(obj);
            }
        }
        if (changed) {
            setChanged(new Date(System.currentTimeMillis()));
            setChangedBy(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
        }
    }

    public void removeRange(Employee user) {
        if (!ranges.isEmpty()) {
            ranges.remove(ranges.size() - 1);
            if (!ranges.isEmpty()) {
                ranges.get(ranges.size() - 1).resetRangeEnd(user);
            }
            setChanged(new Date(System.currentTimeMillis()));
            setChangedBy(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
        }
    }

    public void updateRange(
            Employee user,
            ProductPriceByQuantity priceByQuantity,
            Double consumerPrice,
            Double resellerPrice,
            Double partnerPrice) {
        for (Iterator<ProductPriceByQuantity> i = ranges.iterator(); i.hasNext();) {
            ProductPriceByQuantity pbq = i.next();
            if (pbq.getId().equals(priceByQuantity.getId())) {
                pbq.update(consumerPrice, resellerPrice, partnerPrice);
                setChanged(new Date(System.currentTimeMillis()));
                setChangedBy(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
                break;
            }
        }
    }

    public List<ProductPriceByQuantity> getRanges() {
        return ranges;
    }

    protected void setRanges(List<ProductPriceByQuantity> ranges) {
        this.ranges = ranges;
    }
}
