/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 26, 2009 12:05:19 PM 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.mail.EmailAddress;

import com.osserp.core.Address;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.model.AddressImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractContactAware extends AbstractEntity implements ClassifiedContact {
    private static Logger log = LoggerFactory.getLogger(AbstractContactAware.class.getName());

    private String accountingReference;
    private String shortkey;
    private Long groupId = null;
    private Long statusId = null;
    private Contact rc = null;

    protected AbstractContactAware() {
        super();
    }

    /**
     * Creates a new contactAware with auto-assigned id (sequence by dbms)
     * @param groupId
     * @param createdBy
     * @param rc
     * @param statusId
     */
    protected AbstractContactAware(Long groupId, Long createdBy, Contact rc, Long statusId) {
        super((Long) null, (Long) null, createdBy);
        if (log.isDebugEnabled()) {
            log.debug("<init> invoked [group=" + groupId
                    + ", user=" + createdBy
                    + ", contact=" + (rc == null ? "null" : rc.getContactId())
                    + ", contactClass=" + (rc == null ? "null" : rc.getClass().getName())
                    + "]");
        }
        this.groupId = groupId;
        this.statusId = statusId;
        assignRc(rc);
    }

    /**
     * Creates a new contactAware by dedicated id (no dbms sequence configured)
     * @param id
     * @param groupId
     * @param createdBy
     * @param rc
     * @param statusId
     */
    protected AbstractContactAware(Long id, Long groupId, Long createdBy, Contact rc, Long statusId) {
        super(id, (Long) null, createdBy);
        if (log.isDebugEnabled()) {
            log.debug("<init> invoked [id=" + id
                    + ", group=" + groupId
                    + ", user=" + createdBy
                    + ", contact=" + (rc == null ? "null" : rc.getContactId())
                    + ", contactClass=" + (rc == null ? "null" : rc.getClass().getName())
                    + "]");
        }
        this.groupId = groupId;
        this.statusId = statusId;
        assignRc(rc);
    }

    private void assignRc(Contact contact) {
        if (contact instanceof AbstractContactAware) {
            AbstractContactAware acow = (AbstractContactAware) contact;
            rc = acow.getRc();
        } else {
            rc = contact;
        }
    }

    /**
     * Creates a contact as copy of another contact
     * @param other
     */
    protected AbstractContactAware(ClassifiedContact other) {
        super(other);
        groupId = other.getGroupId();
        statusId = other.getStatusId();
        if (other instanceof AbstractContactAware) {
            rc = ((AbstractContactAware) other).getRc();
        } else if (log.isDebugEnabled()) {
            log.debug("<init> invoked with other of unexpected type [class=" + other.getClass().getName() + "]");
        }
        if (log.isDebugEnabled()) {
            log.debug("<init> done [id=" + other.getId() + ", contactId=" + other.getContactId() + "]");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.common.beans.AbstractEntity#getMapped()
     */
    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "id", getId());
        putIfExists(map, "groupId", groupId);
        putIfExists(map, "statusId", statusId);
        putIfExists(map, "accountingReference", accountingReference);
        if (rc != null) {
            map.putAll(rc.getMapped());
        }
        return map;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ClassifiedContact#getAccountingReference()
     */
    public String getAccountingReference() {
        return accountingReference;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ClassifiedContact#setAccountingReference(java.lang.String)
     */
    public void setAccountingReference(String accountingReference) {
        this.accountingReference = accountingReference;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ClassifiedContact#getShortkey()
     */
    public String getShortkey() {
        return shortkey;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ClassifiedContact#setShortkey(java.lang.String)
     */
    public void setShortkey(String shortkey) {
        this.shortkey = shortkey;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ClassifiedContact#getGroupId()
     */
    public Long getGroupId() {
        return groupId;
    }

    /**
     * Sets the group id. The groupId should be considered as readonly.
     * @param groupId
     */
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ClassifiedContact#getStatusId()
     */
    public Long getStatusId() {
        return statusId;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ClassifiedContact#setStatusId(java.lang.Long)
     */
    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ContactPerson#getBirthDate()
     */
    public Date getBirthDate() {
        return (rc == null ? null : rc.getBirthDate());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ContactPerson#getContactId()
     */
    public Long getContactId() {
        return (rc == null ? null : rc.getContactId());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ContactPerson#getOffice()
     */
    public String getOffice() {
        return (rc == null ? null : rc.getOffice());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.ContactPerson#getPosition()
     */
    public String getPosition() {
        return (rc == null ? null : rc.getPosition());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.ContactPerson#getReference()
     */
    @Override
    public Long getReference() {
        return (rc == null ? null : rc.getReference());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.ContactPerson#getSection()
     */
    public String getSection() {
        return (rc == null ? null : rc.getSection());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getAddressXML()
     */
    public Element getAddressXML() {
        return (rc == null ? null : rc.getAddressXML());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getEmail()
     */
    public String getEmail() {
        return (rc == null ? null : rc.getEmail());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#hasEmail(java.lang.String)
     */
    public boolean hasEmail(String emailAddress) {
        return (rc == null ? false : rc.hasEmail(emailAddress));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getEmailAliases(com.osserp.common.mail.EmailAddress)
     */
    public List<EmailAddress> getEmailAliases(EmailAddress address) {
        return (rc == null ? null : rc.getEmailAliases(address));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getEmails()
     */
    public List<EmailAddress> getEmails() {
        return (rc == null ? null : rc.getEmails());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getFax()
     */
    public Phone getFax() {
        return (rc == null ? null : rc.getFax());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getFaxNumbers()
     */
    public List<Phone> getFaxNumbers() {
        return (rc == null ? null : rc.getFaxNumbers());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getMobile()
     */
    public Phone getMobile() {
        return (rc == null ? null : rc.getMobile());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getMobiles()
     */
    public List<Phone> getMobiles() {
        return (rc == null ? null : rc.getMobiles());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getPhone()
     */
    public Phone getPhone() {
        return (rc == null ? null : rc.getPhone());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getPhoneDeviceNumbers(java.lang.Long)
     */
    public List<Phone> getPhoneDeviceNumbers(Long device) {
        return (rc == null ? null : rc.getPhoneDeviceNumbers(device));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getPhoneNumbers()
     */
    public List<Phone> getPhoneNumbers() {
        return (rc == null ? null : rc.getPhoneNumbers());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getPhones()
     */
    public List<Phone> getPhones() {
        return (rc == null ? null : rc.getPhones());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getWebsite()
     */
    public String getWebsite() {
        return (rc == null ? null : rc.getWebsite());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Person#getExternalReference()
     */
    public String getExternalReference() {
        return (rc == null ? null : rc.getExternalReference());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Person#getExternalUrl()
     */
    public String getExternalUrl() {
        return (rc == null ? null : rc.getExternalUrl());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getXML()
     */
    @Override
    public Element getXML() {
        return (rc == null ? new Element("contact") : rc.getXML());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getXML(java.lang.String)
     */
    @Override
    public Element getXML(String name) {
        Element xml = (rc == null ? new Element(name) : rc.getXML(name));
        xml.addContent(new Element("id").setText(
                (getId() == null ? "" : getId().toString())));
        xml.addContent(new Element("contactGroup").setText(
                (groupId == null ? "" : groupId.toString())));
        xml.addContent(new Element("accountingReference").setText(
                (accountingReference == null ? "" : accountingReference)));
        return xml;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getPrimaryKey()
     */
    @Override
    public Long getPrimaryKey() {
        return (rc == null ? null : rc.getPrimaryKey());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getAddress()
     */
    public Address getAddress() {
        if (rc == null) {
            log.warn("getAddress() contact not assigned!");
        } else if (rc.getAddress() == null) {
            log.warn("getAddress() contact.address not assigned!");
        }
        return (rc == null ? new AddressImpl() : rc.getAddress());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getDisplayName()
     */
    public String getDisplayName() {
        return (rc == null ? null : rc.getDisplayName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getFirstName()
     */
    public String getFirstName() {
        return (rc == null ? null : rc.getFirstName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#isFirstNamePrefix()
     */
    public boolean isFirstNamePrefix() {
        return (rc == null ? false : rc.isFirstNamePrefix());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getLastName()
     */
    public String getLastName() {
        return (rc == null ? null : rc.getLastName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getName()
     */
    public String getName() {
        return (rc == null ? null : rc.getName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getPersonChanged()
     */
    public Date getPersonChanged() {
        return (rc == null ? null : rc.getPersonChanged());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getPersonChangedBy()
     */
    public Long getPersonChangedBy() {
        return (rc == null ? null : rc.getPersonChangedBy());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getPersonCreated()
     */
    public Date getPersonCreated() {
        return (rc == null ? null : rc.getPersonCreated());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getPersonCreatedBy()
     */
    public Long getPersonCreatedBy() {
        return (rc == null ? null : rc.getPersonCreatedBy());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getSalutation()
     */
    public Salutation getSalutation() {
        return (rc == null ? null : rc.getSalutation());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getSalutationDisplay()
     */
    public String getSalutationDisplay() {
        return (rc == null ? null : rc.getSalutationDisplay());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getSalutationDisplayLong()
     */
    public String getSalutationDisplayLong() {
        return (rc == null ? null : rc.getSalutationDisplayLong());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getSalutationDisplayShort()
     */
    public String getSalutationDisplayShort() {
        return (rc == null ? null : rc.getSalutationDisplayShort());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getStatus()
     */
    public Long getStatus() {
        return (rc == null ? null : rc.getStatus());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getTitle()
     */
    public Option getTitle() {
        return (rc == null ? null : rc.getTitle());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#getType()
     */
    public ContactType getType() {
        return (rc == null ? null : rc.getType());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isBranchOffice()
     */
    public boolean isBranchOffice() {
        return (rc == null ? false : rc.isBranchOffice());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isContactPerson()
     */
    public boolean isContactPerson() {
        return (rc == null ? false : rc.isContactPerson());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isCustomer()
     */
    public boolean isCustomer() {
        return (rc == null ? false : rc.isCustomer());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isEmployee()
     */
    public boolean isEmployee() {
        return (rc == null ? false : rc.isEmployee());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isInstaller()
     */
    public boolean isInstaller() {
        return (rc == null ? false : rc.isInstaller());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isClient()
     */
    public boolean isClient() {
        return (rc == null ? false : rc.isClient());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isOther()
     */
    public boolean isOther() {
        return (rc == null ? false : rc.isOther());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isNewsletterConfirmation()
     */
    public boolean isNewsletterConfirmation() {
        return (rc == null ? false : rc.isNewsletterConfirmation());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getNewsletterConfirmationNote()
     */
    public String getNewsletterConfirmationNote() {
        return (rc == null ? null : rc.getNewsletterConfirmationNote());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isSupplier()
     */
    public boolean isSupplier() {
        return (rc == null ? false : rc.isSupplier());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isUser()
     */
    public boolean isUser() {
        return (rc == null ? false : rc.isUser());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#isVip()
     */
    public boolean isVip() {
        return (rc == null ? false : rc.isVip());
    }
    
    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getVipNote()
     */
    public String getVipNote() {
        return (rc == null ? null : rc.getVipNote());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#isBusiness()
     */
    public boolean isBusiness() {
        return (rc == null ? false : rc.isBusiness());
    }

    /* (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#isFreelance()
     */
    public boolean isFreelance() {
        return (rc == null ? false : rc.isFreelance());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#isPublicInstitution()
     */
    public boolean isPerson() {
        return (rc == null ? false : rc.isPerson());
    }

    /* (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#isPrivatePerson()
     */
    public boolean isPrivatePerson() {
        return (rc == null ? false : rc.isPrivatePerson());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#isPublicInstitution()
     */
    public boolean isPublicInstitution() {
        return (rc == null ? false : rc.isPublicInstitution());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#isDeleted()
     */
    public boolean isDeleted() {
        return (rc == null ? false : rc.isDeleted());
    }

    // setters

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#setFirstName(java.lang.String)
     */
    public void setFirstName(String firstName) {
        if (rc != null) {
            rc.setFirstName(firstName);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#setFirstNamePrefix(boolean)
     */
    public void setFirstNamePrefix(boolean firstNamePrefix) {
        if (rc != null) {
            rc.setFirstNamePrefix(firstNamePrefix);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#setLastName(java.lang.String)
     */
    public void setLastName(String lastName) {
        if (rc != null) {
            rc.setLastName(lastName);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#setPersonChanged(java.util.Date)
     */
    public void setPersonChanged(Date personChanged) {
        if (rc != null) {
            rc.setPersonChanged(personChanged);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#setPersonChangedBy(java.lang.Long)
     */
    public void setPersonChangedBy(Long personChangedBy) {
        if (rc != null) {
            rc.setPersonChangedBy(personChangedBy);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#setSalutation(com.osserp.core.contacts.Salutation)
     */
    public void setSalutation(Salutation salutation) {
        if (rc != null) {
            rc.setSalutation(salutation);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#setStatus(java.lang.Long)
     */
    public void setStatus(Long status) {
        if (rc != null) {
            rc.setStatus(status);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#setTitle(com.osserp.common.Option)
     */
    public void setTitle(Option title) {
        if (rc != null) {
            rc.setTitle(title);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Person#setType(java.lang.Long)
     */
    public void setType(ContactType type) {
        if (rc != null) {
            rc.setType(type);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.ContactPerson#setBirthDate(java.util.Date)
     */
    public void setBirthDate(Date birthDate) {
        if (rc != null) {
            rc.setBirthDate(birthDate);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.ContactPerson#setOffice(java.lang.String)
     */
    public void setOffice(String office) {
        if (rc != null) {
            rc.setOffice(office);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.ContactPerson#setPosition(java.lang.String)
     */
    public void setPosition(String position) {
        if (rc != null) {
            rc.setPosition(position);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.ContactPerson#setSection(java.lang.String)
     */
    public void setSection(String section) {
        if (rc != null) {
            rc.setSection(section);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#setNewsletterConfirmation(boolean)
     */
    public void setNewsletterConfirmation(boolean newsletterConfirmation) {
        if (rc != null) {
            rc.setNewsletterConfirmation(newsletterConfirmation);
        }
    }

    public void setNewsletterConfirmationNote(String newsletterConfirmationNote) {
        if (rc != null) {
            rc.setNewsletterConfirmationNote(newsletterConfirmationNote);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactStatus()
     */
    public String getGrantContactStatus() {
        return (rc == null ? null : rc.getGrantContactStatus());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#isGrantContactStatusAvailable()
     */
    public boolean isGrantContactStatusAvailable() {
        return (rc == null ? false : rc.isGrantContactStatusAvailable());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#isGrantContactPerEmail()
     */
    public boolean isGrantContactPerEmail() {
        return (rc == null ? false : rc.isGrantContactPerEmail());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactPerEmail(boolean)
     */
    public void setGrantContactPerEmail(boolean grantContactPerEmail) {
        if (rc != null) {
            rc.setGrantContactPerEmail(grantContactPerEmail);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactPerEmailChanged()
     */
    public Date getGrantContactPerEmailChanged() {
        return (rc == null ? null : rc.getGrantContactPerEmailChanged());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactPerEmailChanged(java.util.Date)
     */
    public void setGrantContactPerEmailChanged(Date grantContactPerEmailChanged) {
        if (rc != null) {
            rc.setGrantContactPerEmailChanged(grantContactPerEmailChanged);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactPerEmailChangedBy()
     */
    public Long getGrantContactPerEmailChangedBy() {
        return (rc == null ? null : rc.getGrantContactPerEmailChangedBy());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactPerEmailChangedBy(java.lang.Long)
     */
    public void setGrantContactPerEmailChangedBy(Long grantContactPerEmailChangedBy) {
        if (rc != null) {
            rc.setGrantContactPerEmailChangedBy(grantContactPerEmailChangedBy);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactPerEmailNote()
     */
    public String getGrantContactPerEmailNote() {
        return (rc == null ? null : rc.getGrantContactPerEmailNote());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactPerEmailNote(java.lang.String)
     */
    public void setGrantContactPerEmailNote(String grantContactPerEmailNote) {
        if (rc != null) {
            rc.setGrantContactPerEmailNote(grantContactPerEmailNote);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#isGrantContactPerPhone()
     */
    public boolean isGrantContactPerPhone() {
        return (rc == null ? false : rc.isGrantContactPerPhone());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactPerPhone(boolean)
     */
    public void setGrantContactPerPhone(boolean grantContactPerPhone) {
        if (rc != null) {
            rc.setGrantContactPerPhone(grantContactPerPhone);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactPerPhoneChanged()
     */
    public Date getGrantContactPerPhoneChanged() {
        return (rc == null ? null : rc.getGrantContactPerPhoneChanged());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactPerPhoneChanged(java.util.Date)
     */
    public void setGrantContactPerPhoneChanged(Date grantContactPerPhoneChanged) {
        if (rc != null) {
            rc.setGrantContactPerPhoneChanged(grantContactPerPhoneChanged);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactPerPhoneChangedBy()
     */
    public Long getGrantContactPerPhoneChangedBy() {
        return (rc == null ? null : rc.getGrantContactPerPhoneChangedBy());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactPerPhoneChangedBy(java.lang.Long)
     */
    public void setGrantContactPerPhoneChangedBy(Long grantContactPerPhoneChangedBy) {
        if (rc != null) {
            rc.setGrantContactPerPhoneChangedBy(grantContactPerPhoneChangedBy);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactPerPhoneNote()
     */
    public String getGrantContactPerPhoneNote() {
        return (rc == null ? null : rc.getGrantContactPerPhoneNote());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactPerPhoneNote(java.lang.String)
     */
    public void setGrantContactPerPhoneNote(String grantContactPerPhoneNote) {
        if (rc != null) {
            rc.setGrantContactPerPhoneNote(grantContactPerPhoneNote);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#isGrantContactNone()
     */
    public boolean isGrantContactNone() {
        return (rc == null ? false : rc.isGrantContactNone());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactNone(boolean)
     */
    public void setGrantContactNone(boolean grantContactNone) {
        if (rc != null) {
            rc.setGrantContactNone(grantContactNone);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactNoneChanged()
     */
    public Date getGrantContactNoneChanged() {
        return (rc == null ? null : rc.getGrantContactNoneChanged());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactNoneChanged(java.util.Date)
     */
    public void setGrantContactNoneChanged(Date grantContactNoneChanged) {
        if (rc != null) {
            rc.setGrantContactNoneChanged(grantContactNoneChanged);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactNoneChangedBy()
     */
    public Long getGrantContactNoneChangedBy() {
        return (rc == null ? null : rc.getGrantContactNoneChangedBy());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactNoneChangedBy(java.lang.Long)
     */
    public void setGrantContactNoneChangedBy(Long grantContactNoneChangedBy) {
        if (rc != null) {
            rc.setGrantContactNoneChangedBy(grantContactNoneChangedBy);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#getGrantContactNoneNote()
     */
    public String getGrantContactNoneNote() {
        return (rc == null ? null : rc.getGrantContactNoneNote());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#setGrantContactNoneNote(java.lang.String)
     */
    public void setGrantContactNoneNote(String grantContactNoneNote) {
        if (rc != null) {
            rc.setGrantContactNoneNote(grantContactNoneNote);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Contact#updateContactGrants(java.lang.Long, boolean, java.lang.String, boolean, java.lang.String, boolean, java.lang.String)
     */
    public boolean updateContactGrants(
            Long changedBy, boolean grantContactPerEmailValue, String grantContactPerEmailNoteValue,
            boolean grantContactPerPhoneValue, String grantContactPerPhoneNoteValue, 
            boolean grantContactNoneValue, String grantContactNoneNoteValue,
            boolean newsletterConfirmationValue, String newsletterConfirmationNoteValue,
            boolean vipValue, String vipNoteValue) {
        if (rc == null) {
            return false;
        }
        return rc.updateContactGrants(
                changedBy, grantContactPerEmailValue, grantContactPerEmailNoteValue, 
                grantContactPerPhoneValue, grantContactPerPhoneNoteValue, 
                grantContactNoneValue, grantContactNoneNoteValue,
                newsletterConfirmationValue, newsletterConfirmationNoteValue,
                vipValue, vipNoteValue);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#setVip(boolean)
     */
    public void setVip(boolean vip) {
        if (rc != null) {
            rc.setVip(vip);
        }
    }

    public void setVipNote(String vipNote) {
        if (rc != null) {
            rc.setVipNote(vipNote);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#setWebsite(java.lang.String)
     */
    public void setWebsite(String website) {
        if (rc != null) {
            rc.setWebsite(website);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#getDefaultGroupDisplay()
     */
    public String getDefaultGroupDisplay() {
        return rc == null ? null : rc.getDefaultGroupDisplay();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#setDefaultGroupDisplay(java.lang.String)
     */
    public void setDefaultGroupDisplay(String defaultGroupDisplay) {
        if (rc != null) {
            rc.setDefaultGroupDisplay(defaultGroupDisplay);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Person#setExternalReference(java.lang.String)
     */
    public void setExternalReference(String externalReference) {
        if (rc != null) {
            rc.setExternalReference(externalReference);
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.Person#setExternalUrl(String)
     */
    public void setExternalUrl(String externalUrl) {
        if (rc != null) {
            rc.setExternalUrl(externalUrl);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#addEmail(java.lang.Long, java.lang.Long, java.lang.String, boolean, java.lang.Long)
     */
    public void addEmail(Long createdBy, Long type, String emailAddress,
            boolean primary, Long aliasOf) throws ClientException {
        if (rc != null) {
            rc.addEmail(createdBy, type, emailAddress, primary, aliasOf);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#addPhone(java.lang.Long, java.lang.Long, java.lang.Long, java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String, boolean)
     */
    public void addPhone(Long createdBy, Long device, Long type,
            String country, String prefix, String number, String note,
            boolean primary) throws ClientException {
        if (rc != null) {
            rc.addPhone(createdBy, device, type, country, prefix, number, note, primary);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#updatePhone(java.lang.Long, com.osserp.core.contacts.Phone, java.lang.Long, java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, boolean)
     */
    public void updatePhone(Long updateBy, Phone phone, Long type,
            String country, String prefix, String number, String note,
            boolean primary) throws ClientException {
        if (rc != null) {
            rc.updatePhone(updateBy, phone, type, country, prefix, number, note, primary);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.Contact#removeEmail(java.lang.Long)
     */
    public void removeEmail(Long id) {
        if (rc != null) {
            rc.removeEmail(id);
        }
    }

    public String getSpouseDisplayName() {
        return (rc == null ? null : rc.getSpouseDisplayName());
    }

    public String getSpouseFirstName() {
        return (rc == null ? null : rc.getSpouseFirstName());
    }

    public String getSpouseLastName() {
        return (rc == null ? null : rc.getSpouseLastName());
    }

    public String getSpouseName() {
        return (rc == null ? null : rc.getSpouseName());
    }

    public Salutation getSpouseSalutation() {
        return (rc == null ? null : rc.getSpouseSalutation());
    }

    public Option getSpouseTitle() {
        return (rc == null ? null : rc.getSpouseTitle());
    }

    public String getUserSalutation() {
        return (rc == null ? null : rc.getUserSalutation());
    }

    public void setUserSalutation(String userSalutation) {
        if (rc != null) {
            this.rc.setUserSalutation(userSalutation);
        }
    }

    public String getUserNameAddressField() {
        return (rc == null ? null : rc.getUserNameAddressField());
    }

    public void setUserNameAddressField(String userNameAddressField) {
        if (rc != null) {
            this.rc.setUserNameAddressField(userNameAddressField);
        }
    }

    public void updateSpouse(Salutation salutation, Option title,
            String firstName, String lastName) {
        if (rc != null) {
            this.rc.updateSpouse(salutation, title, firstName, lastName);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.ContactPerson#getReferenceContact()
     */
    public Contact getReferenceContact() {
        return (rc == null ? null : rc.getReferenceContact());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.contacts.ContactPerson#setReferenceContact(com.osserp.core.contacts.Contact)
     */
    public void setReferenceContact(Contact contact) {
        if (rc != null) {
            rc.setReferenceContact(contact);
        }
    }

    public Contact getRc() {
        return rc;
    }

    protected void setRc(Contact rc) {
        this.rc = rc;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.BusinessNoteAware#getBusinessId()
     */
    public Long getBusinessId() {
        return (rc == null ? getId() : rc.getContactId());
    }

    @Override
    public abstract Object clone();
}
