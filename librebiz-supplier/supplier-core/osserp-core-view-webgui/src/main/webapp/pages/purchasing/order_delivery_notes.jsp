<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:set var="deliveryNoteUrl" value="${requestScope.deliveryNoteUrl}"/>
<div class="recordLabelValue">
<c:choose>
<c:when test="${empty record.deliveryNotes}">
	<o:logger write="record has no delivery notes added..." level="debug"/>
	<fmt:message key="deliveries"/>: 
	<fmt:message key="none"/> 
	<c:if test="${record.unchangeable and !record.canceled and !record.closed}">
	<o:permission role="delivery_note_create,purchase_delivery_create" info="permissionDeliveryNoteCreate">
		<c:if test="${!record.changeableDeliveryAvailable}">
    <a href="javascript:onclick=confirmLink('<fmt:message key="title.delivery.note.create"/>','<c:url value="${deliveryNoteUrl}"><c:param name="method" value="add"/></c:url>');" title="<fmt:message key="title.delivery.note.create"/>">[<fmt:message key="new"/>]</a>
		</c:if>
	</o:permission>
  <c:if test="${!empty sessionScope.recordView.openDeliveries}">
	<c:choose>
	<c:when test="${view.openDeliveryDisplay}">
	<a href="<c:url value="${baseUrl}"><c:param name="method" value="disableOpenDeliveryDisplay"/></c:url>" title="<fmt:message key="disableDisplayOpenDeliveries"/>">[<fmt:message key="off"/>]</a>
	</c:when>
	<c:otherwise>
	<a href="<c:url value="${baseUrl}"><c:param name="method" value="enableOpenDeliveryDisplay"/></c:url>" title="<fmt:message key="displayOpenDeliveries"/>">[<fmt:message key="open"/>]</a>
	</c:otherwise>
	</c:choose>
	</c:if>
	</c:if>
</c:when>
<c:otherwise>
	<o:logger write="record has delivery notes added..." level="debug"/>
	<fmt:message key="deliveries"/>: 
	<c:if test="${record.unchangeable and !record.canceled and !record.closed and !empty view.openDeliveries}">
	<o:permission role="delivery_note_create" info="permissionCreateDeliveryNotes">
		<c:if test="${!record.changeableDeliveryAvailable}">
    <a href="javascript:onclick=confirmLink('<fmt:message key="title.delivery.note.create"/>','<c:url value="${deliveryNoteUrl}"><c:param name="method" value="add"/></c:url>');" title="<fmt:message key="title.delivery.note.create"/>">[<fmt:message key="new"/>]</a>
		</c:if>
	</o:permission>	
	<c:choose>
	<c:when test="${view.openDeliveryDisplay}">
	<a href="<c:url value="${baseUrl}"><c:param name="method" value="disableOpenDeliveryDisplay"/></c:url>" title="<fmt:message key="disableDisplayOpenDeliveries"/>">[<fmt:message key="off"/>]</a>
	</c:when>
	<c:otherwise>
	<a href="<c:url value="${baseUrl}"><c:param name="method" value="enableOpenDeliveryDisplay"/></c:url>" title="<fmt:message key="displayOpenDeliveries"/>">[<fmt:message key="open"/>]</a>
	</c:otherwise>
	</c:choose>
	</c:if>
	<c:forEach var="note" items="${record.deliveryNotes}">
		<c:choose>
			<c:when test="${note.unchangeable}">
			<br/><a href="<c:url value="${deliveryNoteUrl}"><c:param name="method" value="select"/><c:param name="id" value="${note.id}"/></c:url>"><o:out value="${note.number}"/></a> <fmt:message key="from"/> <fmt:formatDate value="${note.created}"/>
			</c:when>
			<c:otherwise>
			<br/><a style="color:red;" href="<c:url value="${deliveryNoteUrl}"><c:param name="method" value="select"/><c:param name="id" value="${note.id}"/></c:url>"><o:out value="${note.number}"/></a> <fmt:message key="from"/> <fmt:formatDate value="${note.created}"/>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</c:otherwise>
</c:choose>
</div>
  