<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.productPriceAwareView}" />
<c:set var="product" value="${view.product}" />
<c:set var="priceAware" value="${view.bean}" />

<div class="table-responsive table-responsive-default">
    <v:form name="productPriceForm" url="/products/productPrice/save">
        <table class="table">
            <tr>
                <th colspan="4"><o:out value="${product.productId}" /> - <o:out value="${product.name}" /></th>
            </tr>
            <o:permission role="purchase_price_view,purchase_price_change" info="permissionPurchasePriceView">
                <tr>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td class="right"><fmt:message key="purchasePriceBase" /></td>
                    <td class="right"><o:permission role="purchase_price_change" info="permissionPurchasePriceChange">
                            <v:number styleClass="form-control priceinput" name="estimatedPurchasePrice" value="${product.calculationPurchasePrice}" format="currency" tabindex="1" />
                        </o:permission> <o:forbidden role="purchase_price_change">
                            <span id="estimatedPurchasePrice"><o:number value="${product.calculationPurchasePrice}" format="currency" /></span>
                        </o:forbidden></td>
                    <td class="right"><fmt:message key="purchasePriceLast" /></td>
                    <td class="right" id="lastPurchasePrice"><o:number value="${product.summary.lastPurchasePrice}" format="currency" /></td>
                </tr>
                <tr>
                    <td colspan="2" class="right"><o:permission role="product_pp_ignore_change" info="permissionPriceUnderPurchasePriceInput">
                            <a href="<v:url value="${view.baseLink}/changePurchasePriceLimitFlag"/>"> <c:choose>
                                    <c:when test="${product.ignorePurchasePriceLimit}">
                                        <fmt:message key="salesPriceUnderPurchasePriceInputEnabled" />
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="salesPriceUnderPurchasePriceInputDisabled" />
                                    </c:otherwise>
                                </c:choose>
                            </a>
                        </o:permission> <o:forbidden role="product_pp_ignore_change">
                            <c:choose>
                                <c:when test="${product.ignorePurchasePriceLimit}">
                                    <fmt:message key="salesPriceUnderPurchasePriceInputEnabled" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="salesPriceUnderPurchasePriceInputDisabled" />
                                </c:otherwise>
                            </c:choose>
                        </o:forbidden></td>
                    <td class="right"><fmt:message key="purchasePriceAverage" /></td>
                    <td class="right"><o:number value="${product.summary.averagePurchasePrice}" format="currency" /></td>
                </tr>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
            </o:permission>
            <o:forbidden role="purchase_price_view,purchase_price_change" info="permissionPurchasePriceView">
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4"><input type="hidden" id="estimatedPurchasePrice" name="estimatedPurchasePrice" value="<o:number value="${product.calculationPurchasePrice}" format="currency"/>" /></td>
                </tr>
            </o:forbidden>
            <tr>
                <th class="right"><fmt:message key="targetGroup" /></th>
                <th class="right"><fmt:message key="price" /> &euro;</th>
                <th class="right"><fmt:message key="priceMinimum" /> &euro;</th>
                <th class="right"><fmt:message key="marginMinimum" /> %</th>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td class="right"><fmt:message key="sale" /></td>
                <td class="right"><v:number name="consumerPrice" value="${priceAware.consumerPrice}" format="currency" styleClass="form-control priceinput" tabindex="2" /></td>
                <td class="right"><v:number name="consumerPriceMinimum" value="${priceAware.consumerPriceMinimum}" format="currency" styleClass="form-control priceinput" readonly="readonly" /></td>
                <td class="right"><v:number name="consumerMinimumMargin" onblur="calculateMinimum(this, 'consumerPriceMinimum','consumerPrice');" value="${priceAware.consumerMinimumMargin}" format="percent" styleClass="form-control priceinput" tabindex="3" /></td>
            </tr>
            <tr>
                <td class="right"><fmt:message key="reseller" /></td>
                <td class="right"><v:number name="resellerPrice" value="${priceAware.resellerPrice}" format="currency" styleClass="form-control priceinput" tabindex="4" /></td>
                <td class="right"><v:number name="resellerPriceMinimum" value="${priceAware.resellerPriceMinimum}" format="currency" styleClass="form-control priceinput" readonly="readonly" /></td>
                <td class="right"><v:number name="resellerMinimumMargin" onblur="calculateMinimum(this, 'resellerPriceMinimum','resellerPrice');" value="${priceAware.resellerMinimumMargin}" format="percent" styleClass="form-control priceinput" tabindex="5" /></td>
            </tr>
            <tr>
                <td class="right"><fmt:message key="partner" /></td>
                <td class="right"><v:number name="partnerPrice" value="${priceAware.partnerPrice}" format="currency" styleClass="form-control priceinput" tabindex="6" /></td>
                <td class="right"><v:number name="partnerPriceMinimum" value="${priceAware.partnerPriceMinimum}" format="currency" styleClass="form-control priceinput" readonly="readonly" /></td>
                <td class="right"><v:number name="partnerMinimumMargin" onblur="calculateMinimum(this, 'partnerPriceMinimum','partnerPrice');" value="${priceAware.partnerMinimumMargin}" format="percent" styleClass="form-control priceinput" tabindex="7" /></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td class="right"><fmt:message key="validFrom" /></td>
                <td class="right"><v:date styleClass="form-control date" name="validFrom" current="true" tabindex="8" /></td>
                <td class="right"><v:submitExit url="${view.baseLink}/disableEditMode" /></td>
                <td class="right"><o:submit value="save" styleClass="form-control" /></td>
            </tr>
            <tr>
                <td colspan="4"></td>
            </tr>
        </table>
    </v:form>
    <c:if test="${!empty view.salesPriceHistory}">
        <table class="table">
            <c:forEach var="obj" items="${view.salesPriceHistory}" varStatus="status">
                <c:if test="${!status.first}">
                    <tr>
                        <th colspan="2"><o:date value="${obj.changed}" /> - <oc:employee value="${obj.changedBy}" />:</th>
                        <th class="right"><fmt:message key="purchasePriceLast" /></th>
                        <th class="right"><o:number value="${obj.lastPurchasePrice}" format="currency" /></th>
                    </tr>
                    <tr>
                        <td><fmt:message key="sale" /></td>
                        <td class="right"><o:number value="${obj.consumerPrice}" format="currency" /></td>
                        <td class="right"><o:number value="${obj.consumerPriceMinimum}" format="currency" /></td>
                        <td class="right"><o:number value="${obj.consumerMinimumMargin * 100}" format="currency" /></td>
                    </tr>
                    <tr>
                        <td><fmt:message key="reseller" /></td>
                        <td class="right"><o:number value="${obj.resellerPrice}" format="currency" /></td>
                        <td class="right"><o:number value="${obj.resellerPriceMinimum}" format="currency" /></td>
                        <td class="right"><o:number value="${obj.resellerMinimumMargin * 100}" format="currency" /></td>
                    </tr>
                    <tr>
                        <td><fmt:message key="partner" /></td>
                        <td class="right"><o:number value="${obj.partnerPrice}" format="currency" /></td>
                        <td class="right"><o:number value="${obj.partnerPriceMinimum}" format="currency" /></td>
                        <td class="right"><o:number value="${obj.partnerMinimumMargin * 100}" format="currency" /></td>
                    </tr>
                </c:if>
            </c:forEach>
        </table>
    </c:if>
</div>
