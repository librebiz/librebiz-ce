/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on 25-Feb-2013 10:22:25
 *
 */
package com.osserp.groovy.web.tag

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dao.JdbcQuery
import com.osserp.common.dao.JdbcParameter
import com.osserp.common.util.DateFormatter


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class QueryInputTag extends AbstractJdbcQueryTag {
    private static Logger logger = LoggerFactory.getLogger(QueryInputTag.class.getName())

    @Override
    protected String execute() {

        if (jdbcQuery == null) {
            logger.debug("execute: jdbcQuery not initialized")
            return ''
        } else {
            StringBuilder buffer = new StringBuilder(512)
            List<JdbcParameter> parameters = jdbcQuery.getParameters()
            for (int i = 0; i < parameters.size(); i++) {
                JdbcParameter next = parameters.get(i)
                buffer
                    .append('<div class="row"><div class="col-md-4"><div class="form-group"><label>')
                    .append(next.label)
                    .append('</label></div></div><div class="col-md-8"><div class="form-group">')
                if ('boolean' == next.typeName) {
                    String checked = ('true' == next.value ? ' checked="checked"' : '')
                    String cb = "<input type=\"checkbox\" name=\"${next.name}\" class=\"form-control\" ${checked}/>"
                    buffer.append(cb)
                } else {
                    String text = "<input type=\"text\" name=\"${next.name}\" class=\"form-control\" value=\"${createValue(next)}\"/>"
                    buffer.append(text)
                }
                buffer.append('</div></div></div>')
            }
            return buffer.toString()
        }
    }

    private String createValue(JdbcParameter parameter) {
        Object value = parameter.value
        if (value == null) {
            value = parameter.defaultValue
            if (!value) {
                return ''
            }
        }
        if (value instanceof Date) {
            return DateFormatter.getDate(value)
        }
        if (value instanceof String) {
            String sval = value
            if (sval == 'null' || sval.length() < 1) {
                return ''
            }
            return value
        }
        return value.toString()
    }

}
