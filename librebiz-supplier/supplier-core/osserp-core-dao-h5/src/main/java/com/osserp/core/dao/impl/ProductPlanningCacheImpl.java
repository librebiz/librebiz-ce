/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 23, 2008 11:59:21 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.dao.Tables;
import com.osserp.common.service.impl.AbstractDatabaseStateSavingCache;

import com.osserp.core.dao.ProductPlanningCache;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.dao.records.SalesOrderQueries;
import com.osserp.core.finance.Stock;
import com.osserp.core.model.products.ProductPlanningImpl;
import com.osserp.core.model.products.ProductPlanningSettingsImpl;
import com.osserp.core.planning.ProductPlanning;
import com.osserp.core.planning.ProductPlanningSettings;
import com.osserp.core.planning.SalesPlanningSummary;
import com.osserp.core.sales.Sales;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Transactional
public class ProductPlanningCacheImpl extends AbstractDatabaseStateSavingCache implements ProductPlanningCache {
    private static Logger log = LoggerFactory.getLogger(ProductPlanningCacheImpl.class.getName());
    private SessionFactory sessionFactory = null;
    private PurchaseOrders purchaseOrders = null;
    private SalesOrderQueries salesOrderQueries = null;
    private SystemCompanies systemCompanies = null;
    private Map<Long, ProductPlanning> _cache = new java.util.HashMap<Long, ProductPlanning>();
    private Long defaultStock = null;

    public ProductPlanningCacheImpl(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            SalesOrderQueries salesOrders,
            PurchaseOrders purchaseOrders,
            SystemCompanies systemCompanies) {

        super(jdbcTemplate, tables, TableKeys.PRODUCT_PLANNING_TASKS);
        this.sessionFactory = sessionFactory;
        this.salesOrderQueries = salesOrders;
        this.systemCompanies = systemCompanies;
        this.purchaseOrders = purchaseOrders;
    }

    public ProductPlanningSettings getSettings(Long reference) {
        assert (reference != null);
        StringBuilder hql = new StringBuilder("from ");
        hql.append(ProductPlanningSettingsImpl.class.getName()).append(" o where o.reference = :referenceId");
        List<ProductPlanningSettings> list = sessionFactory.getCurrentSession().createQuery(
                hql.toString()).setParameter("referenceId", reference).list();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        ProductPlanningSettings aps = new ProductPlanningSettingsImpl(reference, getDefaultStockId());
        sessionFactory.getCurrentSession().saveOrUpdate(ProductPlanningSettingsImpl.class.getName(), aps);
        return aps;
    }

    public void save(ProductPlanningSettings settings) {
        sessionFactory.getCurrentSession().saveOrUpdate(ProductPlanningSettingsImpl.class.getName(), settings);
    }

    public boolean isPlanningAvailable(Long stock) {
        return getCache().containsKey(stock);
    }

    public ProductPlanning getPlanning(Long stock, Date horizon, boolean confirmedPurchaseOnly) {
        ProductPlanning planning = getCache().get(stock);
        if (planning == null) {
            planning = initStock(stock);
            if (planning == null) {
                log.error("getPlanning() planning was null");
                throw new BackendException();
            }
        }
        return horizon == null ? planning.getCopy(confirmedPurchaseOnly) : planning.getCopy(confirmedPurchaseOnly, horizon);
    }

    public boolean isAvailableForDelivery(Long sales, Long status) {
        if (status != null && status < Sales.STATUS_BILLED) {
            for (Iterator<Long> i = getCache().keySet().iterator(); i.hasNext();) {
                Long nextStock = i.next();
                ProductPlanning planning = getCache().get(nextStock);
                if (!planning.isAvailableForDelivery(sales)) {
                    return false;
                }
            }
        }
        return true;
    }

    public List<SalesPlanningSummary> getItems(Long sales, boolean listVacant) {
        List<SalesPlanningSummary> result = new ArrayList<SalesPlanningSummary>();
        for (Iterator<Long> i = getCache().keySet().iterator(); i.hasNext();) {
            Long nextStock = i.next();
            ProductPlanning planning = getCache().get(nextStock);
            List<SalesPlanningSummary> nextSummary = planning.getItems(sales, listVacant);
            if (!nextSummary.isEmpty()) {
                result.addAll(nextSummary);
            }
        }
        return result;
    }

    public void refresh(String eventXml) {
        setRefreshing(true);
        for (Iterator<Long> i = getCache().keySet().iterator(); i.hasNext();) {
            refreshCache(i.next());
        }
        setRefreshing(false);
    }

    private void refreshCache(Long stockId) {
        List<OrderItemsDisplay> openItems = salesOrderQueries.getOpenItems(stockId, null);
        List<OrderItemsDisplay> unreleasedItems = salesOrderQueries.getUnreleasedItems(stockId, null);
        List<OrderItemsDisplay> vacantItems = salesOrderQueries.getVacantItems(stockId, null);
        List<OrderItemsDisplay> openPurchaseItems = purchaseOrders.getOpen(stockId, null);
        getCache().get(stockId).refresh(openItems, openPurchaseItems, unreleasedItems, vacantItems);
    }

    private Map<Long, ProductPlanning> getCache() {
        if (_cache.isEmpty()) {
            initCache();
        }
        return _cache;
    }

    private void initCache() {
        List<Stock> stocks = systemCompanies.getStocks();
        for (Iterator<Stock> i = stocks.iterator(); i.hasNext();) {
            Stock nextStock = i.next();
            _cache.put(nextStock.getId(), new ProductPlanningImpl(nextStock));
            if (nextStock.isDefaultStock()) {
                defaultStock = nextStock.getId();
                if (log.isDebugEnabled()) {
                    log.debug("<init> default stock initialized [id=" + nextStock.getId() + "]");
                }
            } else if (log.isDebugEnabled()) {
                log.debug("<init> stock initialized [id=" + nextStock.getId() + "]");
            }
        }
        refresh("<root><manualRefresh/></root>");
    }

    private ProductPlanning initStock(Long stockId) {
        try {
            Stock stock = systemCompanies.getStockById(stockId);
            _cache.put(stock.getId(), new ProductPlanningImpl(stock));
            refreshCache(stockId);
        } catch (Exception e) {
            log.warn("Failed to init stock by id: " + e.getMessage(), e);
            throw new BackendException(e);
        }
        return _cache.get(stockId);
    }

    private Long getDefaultStockId() {
        if (defaultStock != null) {
            return defaultStock;
        }
        Stock s = systemCompanies.getDefaultStock();
        defaultStock = s != null ? s.getId() : null;
        return defaultStock;
    }
}
