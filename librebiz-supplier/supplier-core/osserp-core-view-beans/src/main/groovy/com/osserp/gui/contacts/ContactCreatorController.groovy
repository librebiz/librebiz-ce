/**
 *
 * Copyright (C) 2006, 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 17, 2006 
 * Created on Nov 7, 2012 (Groovy implementation) 
 * 
 */
package com.osserp.gui.contacts

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.common.web.RequestUtil

import com.osserp.groovy.web.ViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/contacts/contactCreator/*")
@Controller class ContactCreatorController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(ContactCreatorController.class.getName())
    
    @RequestMapping
    def confirm(HttpServletRequest request) {
        logger.debug("confirm: invoked [user=${getUserId(request)}]")
        ContactCreatorView view = getView(request)
        view.confirm()
        save(request)
    }

    @RequestMapping
    def discard(HttpServletRequest request) {
        logger.debug("discard: invoked [user=${getUserId(request)}]")
        Long id = RequestUtil.getId(request)
        ContactCreatorView view = getView(request)
        removeView(request, view)
        redirect(request, "/contacts.do?method=load&id=${id}")
    }

    @RequestMapping
    def selectType(HttpServletRequest request) {
        logger.debug("selectType: invoked [user=${getUserId(request)}]")
        ContactCreatorView view = getView(request)
        view.selectType()
        defaultPage
    }
    
    @Override
    @RequestMapping
    def disableCreateMode(HttpServletRequest request) {
        logger.debug("disableCreateMode: invoked [user=${getUserId(request)}]")
        ContactCreatorView view = getView(request)
        if (view?.selectedType && !view.selectedType.child) {
            view.selectedType = null
            return defaultPage
        }
        exit(request)
    }
    
    @Override
    @RequestMapping
    def save(HttpServletRequest request) {
        ContactCreatorView view = getView(request)
        try {
            view.save()
            Long contactId = view.createdContact
            if (contactId) {
                String target = parentTarget(view)
                if (!target) {
                    target = "/contacts.do?method=loadCreated&id=${contactId}"
                }
                removeView(request, view)
                logger.debug("save: done [contact=${contactId}, target=${target}]")
                return redirect(request, target)
            }
    
        } catch (ClientException e) {
            saveError(request, e.message)
        }
        defaultPage
    }
    
    protected String parentTarget(ContactCreatorView view) {
        Long parentId = view.parent?.contactId
        if (parentId) {
            return '/contacts/contactPersons/reload'
        }
        return null
    }
}
