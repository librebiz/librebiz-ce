<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <v:form name="letterForm" url="/admin/letters/letterTemplateConfig/save">
            <table class="table">
                <thead>
                    <tr>
                        <th class="letterContent"><fmt:message key="contentOfLetter" /></th>
                        <th class="contentAction" colspan="3"></th>
                    </tr>
                </thead>
                <tbody>
                    <c:if test="${!view.bean.ignoreSalutation}">
                        <tr>
                            <td class="letterContent"><o:out value="${view.bean.salutation}" /></td>
                            <td class="contentAction" colspan="3"></td>
                        </tr>
                    </c:if>
                    <c:choose>
                        <c:when test="${empty view.bean.paragraphs}">
                            <tr>
                                <td class="letterContent"><span><fmt:message key="noContentAvailableCurrently" /></span></td>
                                <td class="contentAction" colspan="3" style="text-align: right;"><v:link url="${view.baseLink}/createParagraph" title="createNewParagraph">
                                        <o:img name="newdataIcon" />
                                    </v:link></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="obj" items="${view.bean.paragraphs}">
                                <c:choose>
                                    <c:when test="${view.selectedParagraph.id == obj.id}">
                                        <tr>
                                            <td class="letterContent"><textarea class="form-control" name="content" rows="12"><o:out value="${view.selectedParagraph.text}" /></textarea></td>
                                            <td class="contentAction"></td>
                                            <td class="contentAction"><v:link url="/admin/letters/letterTemplateConfig/deleteParagraph" title="delete">
                                                    <o:img name="deleteIcon" styleClass="bigicon" />
                                                </v:link></td>
                                            <td class="contentAction"><input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="submit" class="bigicon" style="vertical-align: middle;" /></td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                            <td class="letterContent"><o:textOut value="${obj.text}" /></td>
                                            <c:choose>
                                                <c:when test="${!view.bean.closed and empty view.selectedParagraph}">
                                                    <td class="contentAction"><v:link url="${view.baseLink}/selectParagraph?id=${obj.id}" title="select">
                                                            <o:img name="writeIcon" styleClass="bigicon" />
                                                        </v:link></td>
                                                    <td class="contentAction"><v:link url="${view.baseLink}/moveParagraphUp?id=${obj.id}" title="moveUp">
                                                            <o:img name="upIcon" styleClass="bigicon" />
                                                        </v:link></td>
                                                    <td class="contentAction"><v:link url="${view.baseLink}/moveParagraphDown?id=${obj.id}" title="moveDown">
                                                            <o:img name="downIcon" styleClass="bigicon" />
                                                        </v:link></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="contentAction" colspan="3"></td>
                                                </c:otherwise>
                                            </c:choose>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${!view.bean.ignoreGreetings}">
                        <tr>
                            <td class="letterContent"><o:out value="${view.bean.greetings}" /></td>
                            <td class="contentAction" colspan="3"></td>
                        </tr>
                    </c:if>
                </tbody>
            </table>
        </v:form>
    </div>
</div>
