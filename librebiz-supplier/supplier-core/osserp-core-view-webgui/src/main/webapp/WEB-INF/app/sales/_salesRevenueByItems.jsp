<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.salesRevenueView}" />
<c:choose>
    <c:when test="${view.postCalculation}">
        <c:set var="revenue" value="${view.bean.postCalculationRevenue}" />
    </c:when>
    <c:otherwise>
        <c:set var="revenue" value="${view.bean}" />
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${empty view.list}">
        <table>
            <tr>
                <td class="caption" valign="top"><fmt:message key="turnover" /></td>
                <td class="a" valign="top"><o:number value="${revenue.salesVolume}" format="currency" /></td>
                <td style="width: 30px;" valign="top">&euro;</td>
                <td class="a" valign="top"><o:number value="${revenue.salesVolumePercent}" format="currency" /></td>
                <td style="width: 30px;" valign="top">%</td>
                <td style="width: 250px;" valign="top">
                    <fmt:message key="totalPriceByOrder" /> 
                    <c:if test="${revenue.salesCreditVolume > 0}">
                        <fmt:message key="creditNoteAmountIncluded1" />
                        <o:number value="${revenue.salesCreditVolume}" format="currency" /> &euro;
                        <fmt:message key="creditNoteAmountIncluded2" />
                    </c:if>
                </td>
            </tr>

            <tr>
                <td class="caption">
                    <c:choose>
                        <c:when test="${empty revenue.hardwareCostsAItems}">
                            <fmt:message key="costsAProductsLabel" />
                        </c:when>
                        <c:otherwise>
                            <v:ajaxLink url="/sales/salesRevenue/list?name=hardwareCostsAItems" targetElement="salesRevenueView_popup">
                                <fmt:message key="costsAProductsLabel" />
                            </v:ajaxLink>
                        </c:otherwise>
                    </c:choose>
                </td>
                <c:choose>
                    <c:when test="${revenue.hardwareCostsAAvailable}">
                        <td class="a err">-<o:number value="${revenue.hardwareCostsA}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a err">-<o:number value="${revenue.hardwareCostsAPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.hardwareCostsA}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a"><o:number value="${revenue.hardwareCostsAPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:otherwise>
                </c:choose>
                <td><o:out value="${revenue.hardwareCostsADescription}" /></td>
            </tr>

            <tr>
                <td class="caption">
                    <c:choose>
                        <c:when test="${empty revenue.hardwareCostsBItems}">
                            <fmt:message key="costsBProductsLabel" />
                        </c:when>
                        <c:otherwise>
                            <v:ajaxLink url="/sales/salesRevenue/list?name=hardwareCostsBItems" targetElement="salesRevenueView_popup">
                                <fmt:message key="costsBProductsLabel" />
                            </v:ajaxLink>
                        </c:otherwise>
                    </c:choose>
                </td>
                <c:choose>
                    <c:when test="${revenue.hardwareCostsBAvailable}">
                        <td class="a err">-<o:number value="${revenue.hardwareCostsB}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a err">-<o:number value="${revenue.hardwareCostsBPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.hardwareCostsB}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a"><o:number value="${revenue.hardwareCostsBPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:otherwise>
                </c:choose>
                <td><o:out value="${revenue.hardwareCostsBDescription}" /></td>
            </tr>

            <tr>
                <td class="caption">
                    <c:choose>
                        <c:when test="${empty revenue.hardwareCostsCItems}">
                            <fmt:message key="costsCProductsLabel" />
                        </c:when>
                        <c:otherwise>
                            <v:ajaxLink url="/sales/salesRevenue/list?name=hardwareCostsCItems" targetElement="salesRevenueView_popup">
                                <fmt:message key="costsCProductsLabel" />
                            </v:ajaxLink>
                        </c:otherwise>
                    </c:choose>
                </td>
                <c:choose>
                    <c:when test="${revenue.hardwareCostsCAvailable}">
                        <td class="a err">-<o:number value="${revenue.hardwareCostsC}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a err">-<o:number value="${revenue.hardwareCostsCPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.hardwareCostsC}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a"><o:number value="${revenue.hardwareCostsCPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:otherwise>
                </c:choose>
                <td><o:out value="${revenue.hardwareCostsCDescription}" /></td>
            </tr>

            <tr>
                <td class="caption">
                    <c:choose>
                        <c:when test="${empty revenue.otherCostsItems}">
                            <fmt:message key="otherCostsItems" />
                        </c:when>
                        <c:otherwise>
                            <v:ajaxLink url="/sales/salesRevenue/list?name=otherCostsItems" targetElement="salesRevenueView_popup">
                                <fmt:message key="otherCostsItems" />
                            </v:ajaxLink>
                        </c:otherwise>
                    </c:choose>
                </td>
                <c:choose>
                    <c:when test="${revenue.accessoryCostsAvailable}">
                        <td class="a err">-<o:number value="${revenue.accessoryCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a err">-<o:number value="${revenue.accessoryCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.accessoryCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a"><o:number value="${revenue.accessoryCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:otherwise>
                </c:choose>
                <td><o:out value="${revenue.accessoryCostsDescription}" /></td>
            </tr>

            <tr>
                <td class="caption">
                    <c:choose>
                        <c:when test="${empty revenue.foreignCostsItems}">
                            <fmt:message key="foreignCostsItems" />
                        </c:when>
                        <c:otherwise>
                            <v:ajaxLink url="/sales/salesRevenue/list?name=foreignCostsItems" targetElement="salesRevenueView_popup">
                                <fmt:message key="foreignCostsItems" />
                            </v:ajaxLink>
                        </c:otherwise>
                    </c:choose></td>
                <c:choose>
                    <c:when test="${revenue.serviceCostsAvailable}">
                        <td class="a err">-<o:number value="${revenue.serviceCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a err">-<o:number value="${revenue.serviceCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.serviceCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a"><o:number value="${revenue.serviceCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:otherwise>
                </c:choose>
                <td><o:out value="${revenue.serviceCostsDescription}" /></td>
            </tr>

            <tr>
                <td class="caption" style="border-bottom: 1px solid black; margin-top: 20px;" colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td class="caption"><fmt:message key="profitMargin1" /></td>
                <c:choose>
                    <c:when test="${revenue.profit < 0}">
                        <td class="a err" style="font-weight: bold;"><o:number value="${revenue.profit}" format="currency" /></td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.profit}" format="currency" /></td>
                    </c:otherwise>
                </c:choose>
                <td>&euro;</td>
                <c:choose>
                    <c:when test="${revenue.profit < 0 or revenue.profitLowerMinimum}">
                        <td class="a err" style="font-weight: bold;"><o:number value="${revenue.profitPercent}" format="currency" /></td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.profitPercent}" format="currency" /></td>
                    </c:otherwise>
                </c:choose>
                <td>%</td>
                <td><fmt:message key="target" />: <o:number value="${revenue.targetMarginPercent}" format="currency" />%, <fmt:message key="minimal" />: <o:number value="${revenue.minimalMarginPercent}" format="currency" />%</td>
            </tr>
            <%-- sales costs --%>
            <tr>
                <td class="caption" style="border-bottom: 1px solid black; margin-top: 20px;" colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>

            <c:if test="${!empty revenue.salesCosts and revenue.salesCosts > 0}">
                <tr>
                    <td class="caption"><fmt:message key="salesCostsLabel" /></td>
                    <td class="a err">-<o:number value="${revenue.salesCosts}" format="currency" /></td>
                    <td>&euro;</td>
                    <td class="a err">-<o:number value="${revenue.salesCostsPercent}" format="currency" /></td>
                    <td>%</td>
                    <td><fmt:message key="salesCommission" /></td>
                </tr>
            </c:if>
            <tr>
                <td class="caption"><fmt:message key="procurement" /></td>
                <c:choose>
                    <c:when test="${revenue.agentCostsAvailable}">
                        <td class="a err">-<o:number value="${revenue.agentCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a err">-<o:number value="${revenue.agentCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.agentCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a"><o:number value="${revenue.agentCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:otherwise>
                </c:choose>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="caption"><fmt:message key="tipCommission" /></td>
                <c:choose>
                    <c:when test="${revenue.tipCostsAvailable}">
                        <td class="a err">-<o:number value="${revenue.tipCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a err">-<o:number value="${revenue.tipCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.tipCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a"><o:number value="${revenue.tipCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:otherwise>
                </c:choose>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td class="caption" style="border-bottom: 1px solid black; margin-top: 20px;" colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td class="caption"><fmt:message key="profitMargin2" /></td>
                <c:choose>
                    <c:when test="${revenue.totalProfit < 0}">
                        <td class="a err" style="font-weight: bold;"><o:number value="${revenue.totalProfit}" format="currency" /></td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.totalProfit}" format="currency" /></td>
                    </c:otherwise>
                </c:choose>
                <td>&euro;</td>
                <c:choose>
                    <c:when test="${revenue.totalProfit < 0}">
                        <td class="a err" style="font-weight: bold;"><o:number value="${revenue.totalProfitPercent}" format="currency" /></td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.totalProfitPercent}" format="currency" /></td>
                    </c:otherwise>
                </c:choose>
                <td>%</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="caption" style="border-bottom: 1px solid black; margin-top: 20px;" colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td class="caption">
                    <c:choose>
                        <c:when test="${empty revenue.developmentCostsItems}">
                            <fmt:message key="developmentCostsItems" />
                        </c:when>
                        <c:otherwise>
                            <v:ajaxLink url="/sales/salesRevenue/list?name=developmentCostsItems" targetElement="salesRevenueView_popup">
                                <fmt:message key="developmentCostsItems" />
                            </v:ajaxLink>
                        </c:otherwise>
                    </c:choose>
                </td>
                <c:choose>
                    <c:when test="${revenue.projectCostsAvailable}">
                        <td class="a err">-<o:number value="${revenue.projectCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a err">-<o:number value="${revenue.projectCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.projectCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a"><o:number value="${revenue.projectCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:otherwise>
                </c:choose>
                <td></td>
            </tr>

            <tr>
                <td class="caption">
                    <c:choose>
                        <c:when test="${empty revenue.unknownCostsItems}">
                            <fmt:message key="unknownCostsItems" />
                        </c:when>
                        <c:otherwise>
                            <v:ajaxLink url="/sales/salesRevenue/list?name=unknownCostsItems" targetElement="salesRevenueView_popup">
                                <fmt:message key="unknownCostsItems" />
                            </v:ajaxLink>
                        </c:otherwise>
                    </c:choose>
                </td>
                <c:choose>
                    <c:when test="${revenue.unknownCostsAvailable}">
                        <td class="a err">-<o:number value="${revenue.unknownCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a err">-<o:number value="${revenue.unknownCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.unknownCosts}" format="currency" /></td>
                        <td>&euro;</td>
                        <td class="a"><o:number value="${revenue.unknownCostsPercent}" format="currency" /></td>
                        <td>%</td>
                    </c:otherwise>
                </c:choose>
                <td></td>
            </tr>
            <tr>
                <td class="caption" style="border-bottom: 1px solid black; margin-top: 20px;" colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td class="caption"><fmt:message key="profitMargin3" /></td>
                <c:choose>
                    <c:when test="${revenue.finalProfit < 0}">
                        <td class="a err" style="font-weight: bold;"><o:number value="${revenue.finalProfit}" format="currency" /></td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.finalProfit}" format="currency" /></td>
                    </c:otherwise>
                </c:choose>
                <td>&euro;</td>
                <c:choose>
                    <c:when test="${revenue.finalProfit < 0}">
                        <td class="a err" style="font-weight: bold;"><o:number value="${revenue.finalProfitPercent}" format="currency" /></td>
                    </c:when>
                    <c:otherwise>
                        <td class="a"><o:number value="${revenue.finalProfitPercent}" format="currency" /></td>
                    </c:otherwise>
                </c:choose>
                <td>%</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </c:when>
    <c:otherwise>
        <table class="table" style="width: 740px; margin-top: 0px;">
            <tr>
                <th id="number"><fmt:message key="product" /></th>
                <th id="description"><fmt:message key="${view.listName}" /> <span style="float: right; margin-right: 5px;"><v:ajaxLink url="/sales/salesRevenue/list" targetElement="salesRevenueView_popup">[<fmt:message key="back" />]</v:ajaxLink></span></th>
                <th id="quantity" class="amount"><fmt:message key="quantity" /></th>
                <th id="price" class="amount"><fmt:message key="partnerPrice" /></th>
                <th id="total" class="amount"><fmt:message key="total" /></th>
            </tr>
            <c:choose>
                <c:when test="${empty view.list}">
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="5"><fmt:message key="error.record.items.missing" />!</td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach var="item" items="${view.list}" varStatus="s">
                        <tr>
                            <td><o:out value="${item.product.productId}" /></td>
                            <td><o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                                    <o:out value="${item.product.name}" />
                                </o:popupSelect></td>
                            <td class="amount"><o:number value="${item.quantity}" format="decimal" /></td>
                            <td class="amount" id="<o:out value="${item.id}"/>price"><o:number value="${item.partnerPrice}" format="currency" /></td>
                            <td class="amount" id="<o:out value="${item.id}"/>amount"><o:number value="${item.partnerAmount}" format="currency" /></td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </table>
    </c:otherwise>
</c:choose>
