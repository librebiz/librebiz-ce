/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 1, 2008 7:34:33 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Date;

import com.osserp.core.BusinessType;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesMonitoringRow {
    String customerName;
    String shortName;
    Long contact_type;
    Long id;
    Long plan_id;
    BusinessType type;
    Long customer_id;
    Long status;
    Date created;
    Long created_by;
    Long manager_id;
    String managerKey;
    Long manager_sub_id;
    Long sales_id;
    String salesKey;
    BranchOffice branch;
    String name;
    String street;
    String zipcode;
    String city;
    Double plant_capacity;
    boolean stopped;
    boolean cancelled;
    Long installer_id;
    Integer installation_days;
    Date installation_date;
    Date delivery_date;
    Date confirmation_date;
    Long fcs_id;
    Date action_created;
    Long action_created_by;
    boolean action_cancels_other;
    Long action_canceled_by;
    Long action_id;
    String action_note;
    String action_name;
    Integer action_order_id;
    Long action_status;
    Long action_group;
    boolean action_cancels_sales;
    boolean action_stops_sales;
    boolean action_starts_up;
    boolean action_manager_selecting;
    boolean action_initializing;
    boolean action_releaeseing;
    boolean requests_dp;
    boolean response_dp;
    boolean requests_di;
    boolean response_di;
    boolean requests_fi;
    boolean response_fi;
    boolean action_closing_delivery;
    boolean action_enabling_delivery;
    boolean action_partial_delivery;
    boolean action_confirming;
    boolean action_verifying;
    boolean action_partial_payment;
    Long origin_id;
    Long origin_type_id;
    String accounting_ref;
    Long shipping_id;
    Long payment_id;
}
