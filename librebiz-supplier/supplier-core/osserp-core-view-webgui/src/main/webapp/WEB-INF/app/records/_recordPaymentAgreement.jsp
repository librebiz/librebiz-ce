<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="recordPaymentAgreementView"/>
<c:set var="record" value="${view.bean}"/>
<c:set var="payment" value="${record.paymentAgreement}"/>
<c:if test="${empty payment}"><o:logger write="current record has no payment agreement assigned..." level="debug"/></c:if>

<c:choose>
<c:when test="${payment.amountsFixed}">
<input type="hidden" name="fixedAmounts" id="fixedAmountsId" value="true"/>
</c:when>
<c:otherwise>
<input type="hidden" name="fixedAmounts" id="fixedAmountsId" value="false"/>
</c:otherwise>
</c:choose>

<div class="row">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><o:out value="${record.type.name}"/>-<fmt:message key="number"/> <o:out value="${record.number}"/> <fmt:message key="from"/> <o:date value="${record.created}"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="name"><fmt:message key="orderValue"/></label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <span id="price" class="boldtext"><o:number value="${record.amounts.amount}" format="currency"/></span>
                            <span class="small" style="margin-left:15px;">(<fmt:message key="netPrice"/>)</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="paymentCondition"><fmt:message key="payment"/></label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <oc:select name="paymentConditionId" value="${payment.paymentCondition}" options="recordPaymentConditions" styleClass="form-control" prompt="false" autoupdate="true"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="hidePaymentStepsCheckbox"> </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                        <div class="checkbox">
                            <label for="taxFree">
                                <v:checkbox name="hidePayments" value="${payment.hidePayments}" autoupdate="true"/>
                                <fmt:message key="hidePaymentStepsDisplayHint"/>
                            </label>
                        </div>
                        </div>
                    </div>
                </div>

                <c:if test="${!payment.hidePayments}">
                
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="paymentStep1"> </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <span name="percentField">
                                    <oc:selectPercentage name="downpaymentPercent" value="${payment.downpaymentPercent}" options="billingPercentages" styleClass="form-control right" onchange="calculateDownpayment();" prompt="false"/>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <oc:select name="downpaymentTargetId" value="${payment.downpaymentTargetId}" options="recordPaymentTargets" styleClass="form-control" prompt="false"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" id="downpaymentAmount" name="downpaymentAmount" onblur="calculateDownpaymentAmount();" value="<o:number value="${payment.downpaymentAmount}" format="currency"/>" class="form-control right">
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="paymentStep2"> </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <span name="percentField">
                                    <oc:selectPercentage name="deliveryInvoicePercent" value="${payment.deliveryInvoicePercent}" options="billingPercentages" styleClass="form-control right" onchange="calculateDeliveryInvoice();" prompt="false"/>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <oc:select name="deliveryInvoiceTargetId" value="${payment.deliveryInvoiceTargetId}" options="recordPaymentTargets" styleClass="form-control" prompt="false"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" id="deliveryInvoiceAmount" name="deliveryInvoiceAmount" onblur="calculateDeliveryInvoiceAmount();" value="<o:number value="${payment.deliveryInvoiceAmount}" format="currency"/>" class="form-control right">
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="paymentStep3"><input type="hidden" name="finalInvoicePercent" value="<o:out value="${payment.finalInvoicePercent}"/>" /></label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="right" style="margin-right:10px;">
                                <span name="percentField" id="fip">
                                    <c:choose>
                                        <c:when test="${payment.amountsFixed}">---</c:when>
                                        <c:otherwise><o:number value="${payment.finalInvoicePercent}" format="percent"/>%</c:otherwise>
                                    </c:choose>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <oc:select name="finalInvoiceTargetId" value="${payment.finalInvoiceTargetId}" options="recordPaymentTargets" styleClass="form-control" prompt="false"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" id="finalInvoiceAmount" name="finalInvoiceAmount" onclick="invalidEditAttempt();" value="<o:number value="${payment.finalInvoiceAmount}" format="currency"/>" class="form-control right">
                            </div>
                        </div>
                    </div>
                    <c:choose>
                        <c:when test="${payment.amountsFixed}">
                        
                            <div class="row next">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="hidden" name="fixedAmounts" id="fixedAmounts" value="true"/>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <v:link title="advancePaymentAmountSwitchPercentHint" url="/records/recordPaymentAgreement/switchFixedAmounts">
                                            <fmt:message key="advancePaymentAmountFixed"/>
                                        </v:link>
                                    </div>
                                </div>
                            </div>
                    
                        </c:when>
                        <c:otherwise>
                        
                            <div class="row next">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="hidden" name="fixedAmounts" id="fixedAmounts" value="false"/>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <v:link title="advancePaymentAmountSwitchPercentHint" url="/records/recordPaymentAgreement/switchFixedAmounts">
                                            <fmt:message key="advancePaymentAmountPercent"/>
                                        </v:link>
                                    </div>
                                </div>
                            </div>
                    
                        </c:otherwise>
                    </c:choose>
                    
                </c:if>
                
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="specialAgreement">
                                <fmt:message key="specialAgreement"/>
                                <br/><span class="tiny">(<fmt:message key="appearsInPrintOut"/>)</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <textarea class="form-control" name="note" cols="46" rows="6"><o:out value="${payment.note}"/></textarea>
                        </div>
                    </div>
                </div>
                <div class="row next">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" value="saveInput" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"></div>
                            </div>
                        </div>
                    </div>
                </div>


            </div> <%-- form-body --%>
        </div>
    </div>
</div>
