<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="involvedPartyAddTitle" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tbody>
                    <c:forEach var="obj" items="${view.mailMessageContext.recipientSuggestions}">
                        <c:if test="${obj.employee}">
                            <tr>
                                <td class="icon center">
                                    <v:button url="${view.baseLink}/addSuggestedRecipient?email=${obj.email}"
                                        value="recipientTO" styleClass="form-control submit btn-mailto" />
                                </td>
                                <td class="icon center">
                                    <v:button url="${view.baseLink}/addSuggestedRecipient?email=${obj.email}"
                                        rawValue="CC" styleClass="form-control submit btn-mailto" />
                                </td>
                                <td><span title="${obj.email}"><o:out value="${obj.name}" /></span></td>
                                <td><o:out value="${obj.groupName}" /></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="customer" /> <span>/</span> <fmt:message key="supplier" /></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tbody>
                    <c:forEach var="obj" items="${view.mailMessageContext.recipientSuggestions}">
                        <c:if test="${obj.customer or obj.supplier}">
                            <tr>
                                <td class="icon center">
                                    <v:button url="${view.baseLink}/addSuggestedRecipient?email=${obj.email}"
                                        value="recipientTO" styleClass="form-control submit btn-mailto" />
                                </td>
                                <td class="icon center">
                                    <v:button url="${view.baseLink}/addSuggestedRecipient?email=${obj.email}"
                                        rawValue="CC" styleClass="form-control submit btn-mailto" />
                                </td>
                                <td><span title="${obj.email}"><o:out value="${obj.name}" /></span></td>
                                <td><o:out value="${obj.groupName}" /></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                    <tr>
                        <td colspan="3">
                            <v:button url="${view.baseLink}/disableRecipientSelectionMode" value="disableSelection" />
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
