/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Jan-2007 10:48:27 
 * 
 */
package com.osserp.core.service.calc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Lock;
import com.osserp.common.LockException;
import com.osserp.common.beans.DefaultLock;

import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.ItemList;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationManager;
import com.osserp.core.calc.CalculationSearchResult;
import com.osserp.core.calc.CalculationTemplate;
import com.osserp.core.calc.CalculationTemplateService;
import com.osserp.core.dao.Calculations;
import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.ProductSelections;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Record;
import com.osserp.core.model.calc.AbstractItemList;
import com.osserp.core.model.calc.CalculationImpl;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductUtil;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationManagerImpl extends AbstractItemListManager implements CalculationManager {
    private static Logger log = LoggerFactory.getLogger(CalculationManagerImpl.class.getName());
    private CalculationTemplateService calculationTemplateService = null;
    private SalesOrderManager orderManager = null;
    private TaxRates taxRates = null;
    private Long discountId = null;

    /**
     * Creates a new CalculationManager
     * @param products
     * @param productPrices
     * @param productSelections
     * @param calculations
     * @param calculationTemplateService
     * @param orderManager
     * @param taxRates
     * @param discountId
     */
    public CalculationManagerImpl(
            Products products, 
            ProductPrices productPrices, 
            ProductSelections productSelections, 
            Calculations calculations,
            CalculationTemplateService calculationTemplateService, 
            SalesOrderManager orderManager, 
            TaxRates taxRates, 
            SystemConfigs systemConfigs,
            String discountProductId) {
        super(products, productPrices, productSelections, calculations);
        this.calculationTemplateService = calculationTemplateService;
        this.orderManager = orderManager;
        this.taxRates = taxRates;
        this.discountId = Long.valueOf(systemConfigs.getSystemProperty(discountProductId));
    }

    public List<CalculationSearchResult> findByReference(Long reference, String context) {
        return getCalculations().findByReference(reference, context);
    }

    public Calculation create(DomainUser user, BusinessCase businessCase, String name, String calculatorName, Calculation source) throws ClientException {
        if (businessCase == null) {
            log.error("create: businessCase param must not be null");
            throw new BackendException(ErrorCode.INVALID_CONTEXT);
        }
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [user=" + (user == null ? "null" :
                (user.getEmployee() == null ? "null" : user.getEmployee().getId()))
                    + ", businessCase=" + businessCase.getPrimaryKey()
                    + ", salesContext=" + businessCase.isSalesContext()
                    + ", name=" + name
                    + "]");
        }
        Date referenceCreatedDate = (source != null) ? source.getInitialCreated() :
            (businessCase.isSalesContext() ? businessCase.getCreated() :
                new Date(System.currentTimeMillis()));
        Calculations calculations = getCalculations();
        Calculation previous = (Calculation) calculations.getLast(businessCase.getPrimaryKey(), businessCase.getContextName());
        Calculation result = null;
        if (source != null) {
            result = calculations.create(
                    (user == null ? null : user.getEmployee()),
                    businessCase.getPrimaryKey(),
                    businessCase.getContextName(),
                    referenceCreatedDate,
                    businessCase.getType(),
                    name,
                    calculatorName != null ? calculatorName : businessCase.getType().getCalculatorName(),
                    source.isSalesPriceLocked(),
                    source.getMinimalMargin(),
                    source.getTargetMargin());
            result = calculations.copy(businessCase.getType(), result, source);
        } else if (previous != null) {
            result = calculations.create(
                    (user == null ? null : user.getEmployee()),
                    businessCase.getPrimaryKey(),
                    businessCase.getContextName(),
                    referenceCreatedDate,
                    businessCase.getType(),
                    name,
                    calculatorName != null ? calculatorName : businessCase.getType().getCalculatorName(),
                    previous.isSalesPriceLocked(),
                    previous.getMinimalMargin() != null ?
                            previous.getMinimalMargin() : businessCase.getType().getMinimalMargin(),
                    previous.getTargetMargin() != null ?
                            previous.getTargetMargin() : businessCase.getType().getTargetMargin());
            result = calculations.copy(businessCase.getType(), result, previous);
        } else {
            result = calculations.create(
                    (user == null ? null : user.getEmployee()),
                    businessCase.getPrimaryKey(),
                    businessCase.getContextName(),
                    referenceCreatedDate,
                    businessCase.getType(),
                    name,
                    calculatorName != null ? calculatorName : businessCase.getType().getCalculatorName(),
                    false,
                    businessCase.getType().getMinimalMargin(),
                    businessCase.getType().getTargetMargin());
        }
        if (log.isDebugEnabled() && result != null) {
            log.debug("create() done [calculation=" + result.getId() + ", itemCount=" + result.getAllItems().size() + "]");
        }
        return result;
    }

    public Calculation create(DomainUser user, BusinessCase businessCase, Record record) throws ClientException {
        if (!(record instanceof SalesOrder) && !(record instanceof SalesOffer)) {
            throw new ClientException(ErrorCode.PROJECT_SETUP_INVALID);
        }
        Calculation calculation = create(user, businessCase, null, null, null);
        if (!record.getItems().isEmpty()) {
            calculation = addItems(user, businessCase, calculation, record.getItems());
        }
        return calculation;
    }

    public Calculation addItem(
            DomainUser user,
            ItemList _itemList,
            Long positionId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            boolean includePrice,
            String note,
            Long externalId)
            throws ClientException {

        getProducts().checkCalculationValues(product);
        boolean priceAware = includePrice;
        if (product.isPlant()) {
            priceAware = true;
        }
        Calculation calculation = loadCalculation(_itemList.getId());
        List<ItemPosition> positions = calculation.getPositions();
        ItemPosition position = null;
        for (int i = 0, j = positions.size(); i < j; i++) {
            position = positions.get(i);
            if (position.getId().equals(positionId)) {
                break;
            }
        }
        if (position == null) {
            throw new IllegalStateException("position must not be null");
        }
        if (product.getGroup() != null && product.getGroup().isDiscount()) {
            if (quantity == null || price == null || price.doubleValue() == 0) {
                throw new ClientException(ErrorCode.VALUES_MISSING);
            }
            if (quantity.doubleValue() > 1) {
                throw new ClientException(ErrorCode.DISCOUNT_QUANTITY);
            }
            if (quantity.doubleValue() < 0) {
                quantity = Double.valueOf(quantity.doubleValue() * (-1));
            }
            if (price.doubleValue() > 0) {
                price = price.multiply(new BigDecimal(-1.0));
            }
        }
        List<ProductSelectionConfigItem> partnerPriceEditables = getPartnerPriceEditables();
        calculation.addItem(
                position.getId(),
                product,
                customName,
                quantity,
                price,
                calculation.getInitialCreated(),
                false,
                new BigDecimal(getProductPrices().getPartnerPrice(product, calculation.getInitialCreated(), quantity)),
                ProductUtil.isPartnerPriceEditable(partnerPriceEditables, product),
                false,
                new BigDecimal(getProductPrices().getPurchasePrice(product, product.getDefaultStock(), calculation.getInitialCreated())),
                taxRates.getRate(product.isReducedTax()),
                note,
                priceAware,
                externalId);
        return save(user, calculation);
    }

    private Calculation addItems(DomainUser user, BusinessCase businessCase, Calculation calculation, List<Item> items) throws ClientException {
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            for (Iterator<ItemPosition> positions = calculation.getPositions().iterator(); positions.hasNext();) {
                ItemPosition pos = positions.next();
                if (isSet(pos.getGroupId())) {
                    Product product = next.getProduct();
                    if (getCalculations().isMatching(businessCase, pos, product)) {
                        addItem(
                                user,
                                calculation,
                                pos.getId(),
                                product,
                                next.getCustomName(),
                                next.getQuantity(),
                                next.getPrice(),
                                next.isIncludePrice(),
                                next.getNote(),
                                next.getExternalId());
                        applyTemplateIfRequired(user, businessCase, calculation, product);
                        if (log.isDebugEnabled()) {
                            log.debug("addItems() added item [position=" + pos.getId()
                                    + ", product=" + next.getProduct().getProductId() + "]");
                        }
                        break;
                    }
                }
            }
        }
        return loadCalculation(calculation.getId());
    }

    private void applyTemplateIfRequired(
            DomainUser user,
            BusinessCase businessCase, 
            Calculation calculation, 
            Product addedProduct) throws ClientException {
        if ((businessCase != null) && (businessCase.getType() != null)
                && (calculationTemplateService.isTemplateAvailable(
                        addedProduct.getProductId(), businessCase.getType().getId()))) {
            CalculationTemplate template = calculationTemplateService.findByProductAndType(
                    calculation, addedProduct.getProductId(), businessCase.getType().getId());
            applyTemplate(user, calculation, template,
                    (businessCase.getType().isWholeSale() || businessCase.getCustomer().isReseller()));
        }
    }

    public Calculation addDiscount(DomainUser user, Calculation calculation, Long itemToDiscount) throws ClientException {
        Calculation obj = loadCalculation(calculation.getId());
        obj.addDiscount(itemToDiscount, getProducts().load(discountId));
        return save(user, obj);
    }

    public Calculation removeProduct(DomainUser user, Calculation calculation, Long itemId) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.removeItem(itemId);
        return save(user, obj);
    }

    public Calculation moveUpOptionItem(DomainUser user, Calculation calculation, Long itemId) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.moveUpOptionItem(itemId);
        return save(user, obj);
    }

    public Calculation moveDownOptionItem(DomainUser user, Calculation calculation, Long itemId) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.moveDownOptionItem(itemId);
        return save(user, obj);
    }

    public Calculation changePriceDisplayStatus(DomainUser user, Calculation calculation, Long itemId) throws ClientException {
        Calculation obj = loadCalculation(calculation.getId());
        obj.updatePriceDisplayState(itemId);
        return save(user, obj);
    }

    public Calculation changePriceDisplayStatus(DomainUser user, Calculation calculation, boolean enable) throws ClientException {
        Calculation obj = loadCalculation(calculation.getId());
        obj.updatePriceDisplayState(enable);
        return save(user, obj);
    }

    public Calculation createOptions(DomainUser user, Calculation calculation) {
        Calculation obj = loadCalculation(calculation.getId());
        Calculations calculations = getCalculations();
        calculations.createOptions(obj);
        return save(user, obj);
    }

    public Calculation addOptionalProduct(
            DomainUser user,
            Calculation calculation,
            Long currentPosition,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            String note)
            throws ClientException {

        Calculation obj = loadCalculation(calculation.getId());
        obj.addOptionItem(
                currentPosition,
                product,
                customName,
                quantity,
                price,
                calculation.getInitialCreated(),
                new BigDecimal(getProductPrices().getPartnerPrice(product, calculation.getInitialCreated(), quantity)),
                false,
                new BigDecimal(getProductPrices().getPurchasePrice(product, product.getDefaultStock(), calculation.getInitialCreated())),
                taxRates.getRate(product.isReducedTax()),
                note,
                null);
        return save(user, obj);
    }

    public Calculation replaceOptionalItem(
            DomainUser user,
            Calculation calculation,
            Long itemId,
            Long productId,
            Double quantity,
            BigDecimal price,
            String note)
            throws ClientException {

        Calculation obj = loadCalculation(calculation.getId());
        obj.updateOptionItem(
                itemId,
                getProducts().load(productId),
                null,
                quantity,
                price,
                note);
        return save(user, obj);
    }

    public Calculation updateOptionalItem(
            DomainUser user,
            Calculation calculation,
            Long itemId,
            String customName,
            Double quantity,
            BigDecimal price,
            String note)
            throws ClientException {
        if (isNotSet(price)) {
            throw new ClientException(ErrorCode.PRICE_MISSING);
        }
        Calculation obj = loadCalculation(calculation.getId());
        obj.updateOptionItem(itemId, customName, quantity, price, note);
        return save(user, obj);
    }

    public Calculation removeOptionalProduct(DomainUser user, Calculation calculation, Long itemId) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.removeOptionItem(itemId);
        return save(user, obj);
    }

    public Calculation changeCalculator(DomainUser user, Calculation calculation, String name) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.setCalculatorClass(name);
        return save(user, obj);
    }

    public Calculation changeName(DomainUser user, Calculation calculation, String name) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.setName(name);
        return save(user, obj);
    }

    public Calculation toggleSalesPriceLock(DomainUser user, Calculation calculation) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.setSalesPriceLocked(!calculation.isSalesPriceLocked());
        return save(user, obj);
    }

    public void checkLock(BusinessCase bc) throws LockException {
        Calculation c = (Calculation) getActive(bc.getPrimaryKey());

        if (c != null && c.isLocked()) {
            throw new LockException(createLock(c));
        }
    }

    public void checkLock(Record obj) throws LockException {
        SalesOrder order = getSalesOrder(obj);
        if (order != null) {
            Calculation c = fetchCurrent(order);
            if (c != null && c.isLocked()) {
                throw new LockException(createLock(c));
            }
        }
    }

    private Calculation fetchCurrent(SalesOrder order) {
        if (order == null || order.getBusinessCaseId() == null) {
            if (log.isDebugEnabled()) {
                log.debug("fetchCurrent() invoked with null argument: "
                        + (order == null ? "order is null" : " sales of order is null,")
                        + " returning null");
            }
            return null;
        }
        Calculation c = (Calculation) getActive(order.getBusinessCaseId());
        return c;
    }

    private Lock createLock(Calculation calc) {
        DefaultLock lock = new DefaultLock(
                "calculation",
                calc.getLockedBy(),
                calc.getLockTime());
        return lock;
    }

    public boolean isLocked(Record obj) {
        SalesOrder order = getSalesOrder(obj);
        if (order == null) {
            return false;
        }
        Calculation c = (Calculation) getActive(order.getReference());
        return c.isLocked();
    }

    public boolean isLocked(Sales obj) {
        Calculation c = (Calculation) getActive(obj.getId());
        return c.isLocked();
    }

    public Calculation lock(Calculation calculation, DomainUser user) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.lock(user.getId(), true);
        return save(user, obj);
    }

    public Calculation unlock(Calculation calculation, DomainUser user) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.lock(user.getId(), false);
        return save(user, obj);
    }

    private SalesOrder getSalesOrder(Record obj) {
        if (obj instanceof SalesOrder) {
            return (SalesOrder) obj;
        } else if (obj instanceof Invoice) {
            SalesOrder order = (SalesOrder) orderManager.find(obj.getReference());
            return order;
        }
        if (log.isDebugEnabled()) {
            log.debug("getSalesOrder() nothing to do for record "
                    + obj.getId()
                    + " of type "
                    + obj.getClass().getName());
        }
        return null;
    }

    public Calculation updateCreatedBy(DomainUser user, Calculation calculation) {
        Calculation obj = loadCalculation(calculation.getId());
        if (obj instanceof CalculationImpl) {
            AbstractItemList impl = (AbstractItemList) obj;
            impl.setCreatedBy(user.getEmployee().getId());
            getItemLists().save(impl);
            if (log.isDebugEnabled()) {
                log.debug("updateCreatedBy() done [calculation=" + impl.getId()
                        + ", user=" + user.getEmployee().getId() + "]");
            }
        }
        return (Calculation) getItemLists().load(obj.getId());
    }

    public Calculation updatePlanAvailable(DomainUser user, Calculation calculation, boolean planAvailable) {
        Calculation obj = loadCalculation(calculation.getId());
        obj.setPlanAvailable(planAvailable);
        return save(user, obj);
    }

    private Calculations getCalculations() {
        return (Calculations) getItemLists();
    }

    private Calculation loadCalculation(Long id) {
        return (Calculation) getItemLists().load(id);
    }

    public Calculation applyTemplate(DomainUser user, Calculation calculation, CalculationTemplate template, boolean wholeSale) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("addTemplate() invoked [template=" + template.getId() + "]");
        }
        Calculation obj = loadCalculation(calculation.getId());
        Long errorProductId = obj.addTemplate(template, wholeSale, taxRates.getRate(true), taxRates.getRate(false));
        //getItemLists().save(calculation);
        if (errorProductId != null) {
            throw new ClientException(ErrorCode.TEMPLATE_PRODUCT, errorProductId);
        }
        return save(user, obj);
    }

    public Calculation removeTemplate(DomainUser user, Calculation calculation, CalculationTemplate template) {
        if (log.isDebugEnabled()) {
            log.debug("removeTemplate() invoked [template=" + template.getId() + "]");
        }
        Calculation obj = loadCalculation(calculation.getId());
        obj.removeTemplate(template);
        return save(user, obj);
    }

    private Calculation save(DomainUser user, Calculation calculation) {
        calculation.setChangedBy(user.getEmployee().getId());
        calculation.setChanged(new Date(System.currentTimeMillis()));
        getItemLists().save(calculation);
        return (Calculation) getItemLists().load(calculation.getId());
    }
}
