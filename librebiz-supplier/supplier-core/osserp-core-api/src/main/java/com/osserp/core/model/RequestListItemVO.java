/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 13, 2006 2:39:01 PM 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.common.Appointment;
import com.osserp.common.CalendarDisplayFlag;
import com.osserp.common.beans.DefaultAppointment;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.util.NumberFormatter;

import com.osserp.core.BusinessType;
import com.osserp.core.events.Event;
import com.osserp.core.requests.RequestListItem;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestListItemVO extends OptionImpl implements RequestListItem {

    private Long customerId = null;
    private Long originId = null;
    private Long originTypeId = null;
    private String customerName;
    private String shortName = null;
    private Long contactType = null;
    private String street = null;
    private String zipcode = null;
    private String city = null;
    private BusinessType type = null;
    private Long status = null;
    private Long salesId = null;
    private String salesKey = null;
    private Long managerId = null;
    private String managerKey = null;
    private Long branchId = null;
    private String branchKey = null;
    private BranchOffice branch = null;
    private Date salesTalkDate = null;
    private Date presentationDate = null;
    private Integer orderProbability = 0;
    private Date orderProbabilityDate = null;
    private Double capacity = 0.0;
    private Date lastNoteDate = null;
    private Long companyId = null;
    private SystemCompany company = null;
    private boolean stopped = false;
    private String accountingReference = null;
    private Long shippingId = null;
    private Long paymentId = null;
    private Date deliveryDate = null;

    // aggregated values without corresponding column:
    private Event appointment = null;

    private Date appointmentDate = null;
    private List<CalendarDisplayFlag> diagram = new ArrayList<CalendarDisplayFlag>();
    private Date lastActionDate = null;
    private Date confirmationDate = null;

    // strings for columns with id only
    private String capacityUnit = "EUR";
    private String shippingName;

    protected RequestListItemVO() {
        super();
    }

    public RequestListItemVO(
            Long id,
            String name,
            Long customerId,
            String customerName,
            String shortName,
            Long contactType,
            String street,
            String zipcode,
            String city,
            Long originId,
            Long originTypeId,
            BusinessType type,
            Long status,
            Date created,
            Long createdBy,
            Long salesId,
            String salesKey,
            Long managerId,
            String managerKey,
            BranchOffice branch,
            Date salesTalkDate,
            Date presentationDate,
            Integer orderProbability,
            Date orderProbabilityDate,
            Double capacity,
            Date lastNoteDate,
            SystemCompany company,
            boolean stopped,
            String accountingReference,
            Long shippingId,
            Long paymentId,
            Date deliveryDate) {

        super(id, name);
        setCreated(created);
        setCreatedBy(createdBy);
        this.customerName = customerName;
        this.shortName = shortName;
        this.contactType = contactType;
        this.type = type;
        this.customerId = customerId;
        this.salesId = salesId;
        this.salesKey = salesKey;
        this.managerId = managerId;
        this.managerKey = managerKey;
        this.status = status;
        this.street = street;
        this.zipcode = zipcode;
        this.city = city;
        this.originId = originId;
        this.originTypeId = originTypeId;
        this.salesTalkDate = salesTalkDate;
        this.presentationDate = presentationDate;
        this.orderProbability = orderProbability;
        this.orderProbabilityDate = orderProbabilityDate;
        this.capacity = capacity;
        this.lastNoteDate = lastNoteDate;
        this.stopped = stopped;
        this.accountingReference = accountingReference;
        this.shippingId = shippingId;
        this.paymentId = paymentId;
        this.deliveryDate = deliveryDate;
        if (branch != null) {
            this.branch = branch;
            this.branchId = branch.getId();
            this.branchKey = branch.getShortkey();
        }
        if (company != null) {
            this.company = company;
            this.companyId = company.getId();
        } else if (branch != null && branch.getCompany() != null) {
            this.companyId = branch.getCompany().getId();
        }
    }

    public boolean isCancelled() {
        return (getStatus() == null ? false : getStatus().longValue() < -1);
    }

    public final boolean isClosed() {
        return SalesImpl.CLOSED.equals(getStatus());
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Long getContactType() {
        return contactType;
    }

    public void setContactType(Long contactType) {
        this.contactType = contactType;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public BusinessType getType() {
        return type;
    }

    public void setType(BusinessType type) {
        this.type = type;
    }

    public String getTypeKey() {
        return type == null ? null : type.getKey();
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getSalesId() {
        return salesId;
    }

    public void setSalesId(Long salesId) {
        this.salesId = salesId;
    }

    public String getSalesKey() {
        return salesKey;
    }

    public void setSalesKey(String salesKey) {
        this.salesKey = salesKey;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public String getManagerKey() {
        return managerKey;
    }

    public void setManagerKey(String managerKey) {
        this.managerKey = managerKey;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getBranchKey() {
        return branchKey;
    }

    public void setBranchKey(String branchKey) {
        this.branchKey = branchKey;
    }

    public BranchOffice getBranch() {
        return branch;
    }

    public void setBranch(BranchOffice branch) {
        this.branch = branch;
    }

    public Long getOriginId() {
        return originId;
    }

    public void setOriginId(Long originId) {
        this.originId = originId;
    }

    public Long getOriginTypeId() {
        return originTypeId;
    }

    public void setOriginTypeId(Long originTypeId) {
        this.originTypeId = originTypeId;
    }

    public Date getPresentationDate() {
        return presentationDate;
    }

    public void setPresentationDate(Date presentationDate) {
        this.presentationDate = presentationDate;
    }

    public Date getSalesTalkDate() {
        return salesTalkDate;
    }

    public void setSalesTalkDate(Date salesTalkDate) {
        this.salesTalkDate = salesTalkDate;
    }

    public Integer getOrderProbability() {
        return orderProbability;
    }

    public void setOrderProbability(Integer orderProbability) {
        this.orderProbability = orderProbability;
    }

    public Date getOrderProbabilityDate() {
        return orderProbabilityDate;
    }

    public void setOrderProbabilityDate(Date orderProbabilityDate) {
        this.orderProbabilityDate = orderProbabilityDate;
    }

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public String getCapacityFormat() {
        if (type == null || "amount".equals(type.getCapacityCalculationMethod())) {
            return "currency";
        }
        return "decimal";
    }

    public Date getLastNoteDate() {
        return lastNoteDate;
    }

    public void setLastNoteDate(Date lastNoteDate) {
        this.lastNoteDate = lastNoteDate;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public SystemCompany getCompany() {
        return company;
    }

    public void setCompany(SystemCompany company) {
        this.company = company;
    }

    public boolean isStopped() {
        return stopped;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    public String getAccountingReference() {
        return accountingReference;
    }

    public void setAccountingReference(String accountingReference) {
        this.accountingReference = accountingReference;
    }

    public Long getShippingId() {
        return shippingId;
    }

    public void setShippingId(Long shippingId) {
        this.shippingId = shippingId;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    // aggregated / none column values

    public Date getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(Date confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public Date getLastActionDate() {
        return lastActionDate;
    }

    public void setLastActionDate(Date lastActionDate) {
        this.lastActionDate = lastActionDate;
    }

    public String getCapacityUnit() {
        return capacityUnit;
    }

    protected void setCapacityUnit(String capacityUnit) {
        this.capacityUnit = capacityUnit;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public boolean isAppointmentAvailable() {
        return appointment != null;
    }

    public Event getAppointment() {
        return appointment;
    }

    public void setAppointment(Event appointment) {
        this.appointment = appointment;
    }

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public List<CalendarDisplayFlag> getDiagram() {
        return diagram;
    }

    public void setDiagram(List<CalendarDisplayFlag> diagram) {
        this.diagram = diagram;
    }

    public Appointment createAppointment(String url, String target) {
        return new DefaultAppointment(
                getId(),
                getName(),
                createAppointmentHeadline(),
                createAppointmentMessage(),
                url,
                target,
                getAppointmentDate());
    }

    protected String createAppointmentHeadline() {
        StringBuilder msg = new StringBuilder(getId().toString());
        msg.append(" - ").append(NumberFormatter.getValue(getCapacity(), NumberFormatter.DECIMAL));
        if (isSet(getCapacityUnit())) {
            msg.append(" ").append(getCapacityUnit());
        }
        return msg.toString();
    }

    protected String createAppointmentMessage() {
        if (isSet(getShippingName())) {
            StringBuilder txt = new StringBuilder();
            txt.append("Transport: ").append(getShippingName());
            return txt.toString();
        }
        return null;
    }
}
