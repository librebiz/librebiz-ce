/**
 *
 * Copyright (C) 2002 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 03.05.2002 
 * 
 */
package com.osserp.common.util;

import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.osserp.common.util.PhoneUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UPhoneUtil {

    @Test
    public void testClearPhoneNumber() {
        String input = "+49 (171) 111-222";
        assertTrue("49171111222".equals(PhoneUtil.clearPhoneNumber(input, false)));
        input = "0049 (171) 111-222";
        assertTrue("49171111222".equals(PhoneUtil.clearPhoneNumber(input, false)));
        input = "0171 / 111 222";
        assertTrue("0171111222".equals(PhoneUtil.clearPhoneNumber(input, true)));
        assertTrue("171111222".equals(PhoneUtil.clearPhoneNumber(input, false)));
    }

    @Test
    public void testPhonePrefixMapExists() {
        Map<Long, PhonePrefix> map = PhoneUtil.getPrefixMap();
        assertTrue(!map.isEmpty());
    }

    @Test
    public void testPhonePrefixMapContainsFfm() {
        Map<Long, PhonePrefix> map = PhoneUtil.getPrefixMap();
        PhonePrefix ffm = map.get(69L);
        assertTrue(ffm != null && "069".equals(ffm.getPrefix()));
    }

    @Test
    public void testPhonePrefixContainsDeepBavaria() {
        String test = "8368999999";
        PhonePrefix prefix = PhoneUtil.fetchPhonePrefix(test);
        assertTrue(prefix != null);
    }

    @Test
    public void testPhonePrefixNotContains() {
        String test = "1234567890";
        PhonePrefix prefix = PhoneUtil.fetchPhonePrefix(test);
        assertTrue(prefix == null);
    }

    @Test
    public void testPhonePrefixIsMobile() {
        String test = "1791234567";
        PhonePrefix prefix = PhoneUtil.fetchPhonePrefix(test);
        assertTrue(prefix != null && prefix.isMobileNumber());
    }

    @Test
    public void testCountrySetExists() {
        Set<String> countrySet = PhoneUtil.getCountrySet();
        assertTrue(!countrySet.isEmpty());
    }

    @Test
    public void testCountrySetContainsGermany() {
        Set<String> countrySet = PhoneUtil.getCountrySet();
        assertTrue(countrySet.contains("49"));
        assertTrue(countrySet.contains("+49"));
        assertTrue(countrySet.contains("0049"));
    }

    @Test
    public void testGetCountryPrefix() {
        String country = PhoneUtil.getCountryPrefix("+49 (171) 111-222");
        assertTrue("+49".equals(country));
        country = PhoneUtil.getCountryPrefix("0049 (171) 111-222");
        assertTrue("0049".equals(country));
        country = PhoneUtil.getCountryPrefix("49171111222");
        assertTrue("49".equals(country));
    }

    @Test
    public void testCountryPrefixMissing() {
        String country = PhoneUtil.getCountryPrefix("049 (171) 111-222");
        assertTrue(country == null);
        country = PhoneUtil.getCountryPrefix("0171 111-222");
        assertTrue(country == null);
    }
}
