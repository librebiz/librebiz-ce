/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 28, 2015 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.core.finance.PaymentDisplay;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.PaymentDisplayVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PaymentDisplayRowMapper extends BaseRecordDisplayRowMapper {

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new PaymentDisplayRowMapper());
    }
    
    protected static final String RECORD_VALUES =
            "id" +
            ",type_id" +
            ",company_id" +
            ",reference_id" +
            ",contact_id" +
            ",contact_type" +
            ",invoice_id" +
            ",created" +
            ",created_by" +
            ",note" +
            ",paid_on" +
            ",tax_free" +
            ",tax_free_id" +
            ",gross_amount" +
            ",currency_id" +
            ",bank_account_id" +
            ",record_type_id" +
            ",document_id" +
            ",lastname" +
            ",firstname" +
            ",street" +
            ",zipcode" +
            ",city" +
            ",branch_id" +
            ",reduced_tax" +
            ",outflow" +
            ",sales_id";
    /*
    r.id,
    r.type_id,
    r.company_id,
    r.reference_id,
    r.sale,
    r.contact_id,
    r.contact_type,
    r.name,
    r.created,
    r.created_by,
    r.status,
    r.tax_free,
    r.tax_free_id,
    r.base_tax,
    r.reduced_tax,
    r.currency_id,
    r.payment_condition,
    r.net_amount,
    r.tax_amount,
    r.reduced_tax_amount,
    r.gross_amount,
    r.signature_left,
    r.signature_right,
    r.internal,
    r.maturity,
    r.paid
    */

    @Override
    protected void populate(ResultSet rs, Map<String, Object> map) {
        addDouble(map, "amountToPay", rs, "amount_open");
        addDate(map, "paidOn", rs, "paid_on");
        addString(map, "note", rs, "note");
        addLong(map, "invoiceId", rs, "invoice_id");
        addLong(map, "bankAccountId", rs, "bank_account_id");
        addLong(map, "branchId", rs, "branch_id");
        addLong(map, "recordTypeId", rs, "record_type_id");
        addBoolean(map, "outflowOfCash", rs, "outflow");
        addBoolean(map, "taxReduced", rs, "reduced_tax");
        addLong(map, "documentId", rs, "document_id");
    }

    public final static List<PaymentDisplay> createDisplayList(Map<Long, RecordType> recordTypes, List<Map<String, Object>> values) {
        List<PaymentDisplay> result = new ArrayList<PaymentDisplay>();
        int size = values.size();
        for (int i = 0; i < size; i++) {
            Map<String, Object> nextMap = values.get(i);
            Long type = (Long) nextMap.get("type");
            if (type != null) {
                RecordType recordType = recordTypes.get(type);
                if (recordType != null) {
                    nextMap.put("recordType", recordType);
                    type = null;
                }
            }
            type = (Long) nextMap.get("recordTypeId");
            if (type != null) {
                RecordType recordType = recordTypes.get(type);
                if (recordType != null) {
                    nextMap.put("invoiceType", recordType);
                }
            }
            result.add(new PaymentDisplayVO(nextMap));
        }
        return result;
    }
}
