/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 02.09.2004 
 * 
 */
package com.osserp.core.contacts;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Salutation extends Option {

    static final Long UNDEFINED = 0L;
    static final Long MR = 1L;
    static final Long MRS = 2L;
    static final Long COMPANY = 3L;
    static final Long FAMILY = 4L;
    static final Long MARRIED_COUPLE = 5L;

    /**
     * The display name in emails and letters
     * @return displayName
     */
    String getDisplayName();

    /**
     * The display name of address fields in letters
     * @return displayNameLetterAddress
     */
    String getDisplayNameLetterAddress();

    /**
     * Indicates if the name should be added to the display name if true
     * @return addName
     */
    boolean isAddName();

    /**
     * Indicates that salutation is anonymous (e.g. not dedicated to a special person)
     * @return anonymous
     */
    boolean isAnonymous();

    /**
     * Indicates that contacts with a salutation of this type assigned represent a female person
     * @return female
     */
    boolean isFemale();

    /**
     * Indicates free customization
     * @param id
     * @return customization status
     */
    static boolean isCustom(Long id) {
        return Salutation.COMPANY.equals(id)
                || Salutation.FAMILY.equals(id)
                || Salutation.MARRIED_COUPLE.equals(id);
    }
}
