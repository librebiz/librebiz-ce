<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<div class="contentBox">
    <div class="contentBoxHeader">
        <div class="contentBoxHeaderLeft">
            <fmt:message key="bankAccounts" />
        </div>
        <div class="contentBoxHeaderRight">
            <div class="boxnav">
                <c:if test="${empty readonlyView}">
                    <ul>
                        <li>
                            <c:choose>
                                <c:when test="${contactView.bankAccountSelectionMode}">
                                    <a href="<c:url value="/suppliers.do?method=assignBankAccount"/>" title="<fmt:message key="exit"/>">
                                        <o:img name="backIcon" styleClass="smallicon" />
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <a href="<c:url value="/suppliers.do?method=enableBankAccountSelection"/>" title="<fmt:message key="assignBankAccount"/>">
                                        <o:img name="smallForwardIcon" styleClass="smallicon" />
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </li>
                    </ul>
                </c:if>
            </div>
        </div>
    </div>
    <div class="contentBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="smallSpacer"></div>
                <table class="valueTable first33">
                    <tbody>
                        <c:choose>
                            <c:when test="${contactView.bankAccountSelectionMode}">
                                <c:forEach var="obj" items="${contactView.bankAccounts}">
                                    <tr>
                                        <td class="left">
                                            <o:out value="${obj.shortkey}" />
                                        </td>
                                        <td class="left">
                                            <a href="<c:url value="/suppliers.do?method=assignBankAccount&id=${obj.id}"/>" title="<fmt:message key="assignBankAccount>"/>">
                                                <o:out value="${obj.bankAccountNumberIntl}" />
                                            </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="obj" items="${contactView.bankAccounts}">
                                    <tr>
                                        <o:permission role="accounting_bank_display,accounting_bank_documents,organisation_admin" info="permissionViewBankAccounts">
                                            <td class="left">
                                                <v:link url="/accounting/bankAccountDocument/forward?id=${obj.id}&exit=/supplierDisplay.do">
                                                    <fmt:message key="statementOfAccounts"/>
                                                </v:link>
                                                - <o:out value="${obj.shortkey}" />
                                            </td>
                                            <td class="left">
                                                <v:link url="/accounting/paymentRecords/forward?account=${obj.id}&exit=/supplierDisplay.do">
                                                    <o:out value="${obj.bankAccountNumberIntl}" />
                                                </v:link>
                                            </td>
                                        </o:permission>
                                        <o:forbidden role="accounting_bank_display,accounting_bank_documents,organisation_admin">
                                            <td class="left"><o:out value="${obj.shortkey}" /></td>
                                            <td class="left"><o:out value="${obj.bankAccountNumberIntl}" /></td>
                                        </o:forbidden>
                                    </tr>
                                </c:forEach>
                                <c:if test="${empty contactView.bankAccounts}">
                                    <tr>
                                        <td class="left"><fmt:message key="status"/></td>
                                        <td class="left">
                                            <fmt:message key="noBankAccountAssigned"/>
                                        </td>
                                    </tr>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
                <div class="smallSpacer"></div>
            </div>
        </div>
    </div>
</div>