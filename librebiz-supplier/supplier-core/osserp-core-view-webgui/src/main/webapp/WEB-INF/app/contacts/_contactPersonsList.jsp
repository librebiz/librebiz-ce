<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="contactPersonsView" />
<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="contactName"><v:sortLink key="name"><fmt:message key="name" /></v:sortLink></th>
                    <th class="contactPosition"><v:sortLink key="position"><fmt:message key="position" /></v:sortLink></th>
                    <th class="contactSection"><v:sortLink key="section"><fmt:message key="department" /></v:sortLink></th>
                    <th class="contactEmail"><fmt:message key="email" /></th>
                    <th class="contactPhone"><fmt:message key="phone" /></th>
                    <th class="contactPhone"><fmt:message key="mobile" /></th>
                    <th class="action"><fmt:message key="action" /></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="contact" items="${view.list}">
                    <tr>
                        <td class="contactName" valign="top">
                            <a href='<c:url value="/contactPersons.do?method=load&id=${contact.contactId}&exit=list"/>'>
                                <o:out value="${contact.displayName}" />
                            </a>
                        </td>
                        <td class="contactPosition" valign="top"><o:out value="${contact.position}" /></td>
                        <td class="contactSection" valign="top"><o:out value="${contact.section}" /></td>
                        <td class="contactEmail" valign="top">
                            <c:choose>
                                <c:when test="${empty contact.email}">&nbsp;</c:when>
                                <c:otherwise><o:email value="${contact.email}" /></c:otherwise>
                            </c:choose>
                        </td>
                        <td class="contactPhone" valign="top">
                            <c:choose>
                                <c:when test="${empty contact.phone}">&nbsp;</c:when>
                                <c:otherwise><oc:phone value="${contact.phone}" /></c:otherwise>
                            </c:choose>
                        </td>
                        <td class="contactPhone" valign="top">
                            <c:choose>
                                <c:when test="${empty contact.mobile}">&nbsp;</c:when>
                                <c:otherwise>
                                    <oc:phone value="${contact.mobile}" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="action" valign="top"></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
