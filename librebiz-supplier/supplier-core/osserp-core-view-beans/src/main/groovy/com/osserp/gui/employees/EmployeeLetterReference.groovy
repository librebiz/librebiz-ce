/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 31, 2017 
 * 
 */
package com.osserp.gui.employees

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.dms.Letter
import com.osserp.core.employees.Employee

import com.osserp.gui.letters.LetterReference

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class EmployeeLetterReference extends LetterReference {
    private static Logger logger = LoggerFactory.getLogger(EmployeeLetterReference.class.getName())
    public static final Long EMPLOYEE_LETTER_TYPE = 5L

    protected EmployeeLetterReference() {
        super()
    }

    EmployeeLetterReference(Employee employee, String exit) {
        super(EMPLOYEE_LETTER_TYPE, employee, employee, employee.id, null, exit)
    }

    void updateInfos(Letter letter, String[] infos) {
        Employee employee = getEmployee()
        letter.infos.each {

            if (isActivated(infos, it.name)) {
                logger.debug("updateInfos: found activated [name=${it.name}]")

                if ('employeeNo' == it.name) {
                    it.setValue(employee.getId().toString())
                    it.setIgnore(false)
                } else {
                    it.setIgnore(true)
                }
            } else {
                it.setIgnore(true)
            }
        }
        logger.debug('updateInfos: done')
    }

    Long getObjectId() {
        return employee?.id
    }

    private Employee getEmployee() {
        getBusinessObject()
    }
}
