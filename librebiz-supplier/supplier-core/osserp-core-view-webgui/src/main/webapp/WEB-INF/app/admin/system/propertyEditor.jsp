<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="propertyEditorView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="configuration" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:link url="/admin" title="backToMenu">
            <o:img name="homeIcon" />
        </v:link>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="runtimeConfigContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped" style="table-layout:fixed;">
                            <tbody>
                                <tr>
                                    <td colspan="2"><strong>Caution: Changing system settings may result in a damaged system!</strong></td>
                                </tr>
                                <c:choose>
                                    <c:when test="${!empty view.selectedPropertyName}">
                                        <v:form url="/admin/system/propertyEditor/updateProperty">
                                            <c:forEach var="prop" items="${view.list}">
                                                <c:if test="${prop.name == view.selectedPropertyName}">
                                                    <tr>
                                                        <td><o:out value="${prop.name}" /></td>
                                                        <td><input type="text" name="value" value="<o:out value="${prop.value}"/>" class="form-control" />
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;">You now what you're doing?</td>
                                                        <td><input type="submit" value="Submit" class="form-control" style="width:25%" /></td>
                                                    </tr>
                                                </c:if>
                                            </c:forEach>
                                        </v:form>
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${!empty view.list}">
                                            <c:forEach var="prop" items="${view.list}">
                                                <c:if test="${prop.displayable and !prop.setupOnly}">
                                                    <c:choose>
                                                        <c:when test="${prop.unchangeable}">
                                                            <tr>
                                                                <td><o:out value="${prop.name}" /></td>
                                                                <td><o:out value="${prop.value}" /></td>
                                                            </tr>
                                                        </c:when>
                                                        <c:when test="${prop.booleanType}">
                                                            <tr>
                                                                <td><o:out value="${prop.name}" /></td>
                                                                <td>
                                                                    <v:link url="/admin/system/propertyEditor/switchBooleanProperty?name=${prop.name}">
                                                                        <o:out value="${prop.value}" />
                                                                    </v:link>
                                                                </td>
                                                            </tr>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <tr>
                                                                <td>
                                                                    <v:link url="/admin/system/propertyEditor/selectProperty?name=${prop.name}">
                                                                        <o:out value="${prop.name}" />
                                                                    </v:link>
                                                                </td>
                                                                <td><o:out value="${prop.value}" /></td>
                                                            </tr>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </c:otherwise>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
