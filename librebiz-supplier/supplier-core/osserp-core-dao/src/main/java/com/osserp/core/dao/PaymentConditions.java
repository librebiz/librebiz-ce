/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 14-Oct-2006 12:45:55 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.finance.PaymentCondition;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PaymentConditions {

    /**
     * Provides all payment conditions
     * @return payment conditions
     */
    List<PaymentCondition> getPaymentConditions();

    /**
     * Loads a payment condition from persistent storage
     * @param id
     * @return paymentCondition condition
     */
    PaymentCondition load(Long id);

    /**
     * Creates a new payment condition
     * @param name
     * @param paymentTarget
     * @param paid
     * @param withDiscount
     * @param discount1
     * @param discount1Target
     * @param discount2
     * @param discount2Target
     * @return paymentCondition new created
     * @throws ClientException if name exists or validation failed
     */
    PaymentCondition create(
            String name,
            Integer paymentTarget,
            boolean paid,
            boolean withDiscount,
            Double discount1,
            Integer discount1Target,
            Double discount2,
            Integer discount2Target) throws ClientException;

    /**
     * Persists a payment condition
     * @param obj
     */
    void save(PaymentCondition obj);

    /**
     * Provides the default payment condition
     * @param sales indicates that default sales condition is searched
     * @return defaultCondition
     */
    PaymentCondition getPaymentConditionDefault(boolean sales);
}
