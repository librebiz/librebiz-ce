/**
 *
 * Copyright (C) 2007, 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 21, 2007 8:09:54 PM 
 * Modified on Dec 6, 2013 4:51:22 PM (Groovy implementation) 
 * 
 */
package com.osserp.gui.events

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.Parameter
import com.osserp.common.PermissionException
import com.osserp.common.beans.ParameterImpl

import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeSearch
import com.osserp.core.events.EventAction
import com.osserp.core.events.EventConfigManager
import com.osserp.core.events.EventConfigType
import com.osserp.core.events.EventManager

import com.osserp.groovy.web.ViewContextException

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class AppointmentCreatorView extends AbstractEventView {
    private static Logger logger = LoggerFactory.getLogger(AppointmentCreatorView.class.getName())

    EventConfigType configType
    List<EventAction> actions = []
    EventAction selectedAction
    Long recipient
    Long reference
    String description

    AppointmentCreatorView() {
        enablePopupView()
        enablePopupSaveExit()
        headerName = 'appointmentCreate'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            
            Long type = form.getLong('type')
            if (type) {
                configType = eventConfigManager.findType(type)
                if (configType) {
                    actions = eventConfigManager.findActionsByEventType(configType)
                }
            }

            Long primaryKey = form.getLong('id')
            if (primaryKey && viewReference?.list) {
                def refObj
                try {
                    viewReference.list.each {
                        refObj = it
                        if (refObj.primaryKey == primaryKey) {
                            reference = refObj.primaryKey
                            description = refObj.name
                            logger.debug("initRequest: assigned reference by id [reference=${reference}]")
                        }
                    }
                } catch (Exception e) {
                    logger.debug("initRequest: failed on attempt to assign reference and description [obj=${refObj?.getClass().getName()}, message=${e.message}, type=${e.getClass().getName()}]")
                }
            } else {
                try {
                    reference = viewReference?.bean.primaryKey
                    description = viewReference?.bean.name
                    logger.debug("initRequest: assigned reference by bean [reference=${reference}]")
                } catch (Exception e) {
                    logger.debug("initRequest: failed on attempt to assign reference and description [message=${e.message}, type=${e.getClass().getName()}]")
                }
            }
            if (!reference) {
                throw new ViewContextException('missing.reference', this)
            }
            enableCreateMode()
        }
    }

    @Override
    void save() {
        Long action = form.getLong('action')
        selectedAction = actions.find { it.id == action }
        recipient = form.getLong('recipient')
        eventManager.createEvent(
                selectedAction,
                domainEmployee.id,
                reference,
                (!recipient ? domainEmployee.id : recipient),
                description,
                form.getString('note'),
                createParams(),
                createDate('reminder'),
                createDate('appointment'))
        disableCreateMode()
        reloadDependencies()
    }


    protected List<Parameter> createParams() {
        List<Parameter> list = new ArrayList<Parameter>()
        list.add(new ParameterImpl("id", reference.toString()))
        list.add(new ParameterImpl("target", "todo"))
        return list
    }

    protected EventConfigManager getEventConfigManager() {
        getService(EventConfigManager.class.getName())
    }

    @Override
    protected void reloadDependencies() {
        reloadViewReference()
    }
}
