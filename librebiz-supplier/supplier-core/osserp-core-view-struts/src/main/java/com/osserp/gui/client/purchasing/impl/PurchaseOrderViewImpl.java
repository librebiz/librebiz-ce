/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 14, 2006 2:18:21 AM 
 * 
 */
package com.osserp.gui.client.purchasing.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordBackupItem;
import com.osserp.core.finance.RecordImportService;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.products.Product;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.purchasing.PurchaseOrderManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesSearch;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.suppliers.SupplierSearch;
import com.osserp.core.system.BranchOffice;

import com.osserp.gui.client.purchasing.PurchaseOrderView;
import com.osserp.gui.client.records.impl.AbstractOrderView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("purchaseOrderView")
public class PurchaseOrderViewImpl extends AbstractOrderView implements PurchaseOrderView {
    private static Logger log = LoggerFactory.getLogger(PurchaseOrderViewImpl.class.getName());
    private Long company = null;
    private Product product = null;
    private Supplier supplier = null;

    private boolean splittingMode = false;
    private List<OrderItem> splittingListSource = new ArrayList<>();
    private List<OrderItem> splittingListTarget = new ArrayList<>();
    private OrderItem currentSplittingItem = null;
    private boolean deliveryConditionEditMode = false;
    private Sales sales = null;

    protected PurchaseOrderViewImpl() {
        super();
        if (!isDisplayDescription()) {
            changeDisplayDescriptionState();
        }
        deleteAllItemsEnabled();
    }

    @Override
    public boolean isSalesRecordView() {
        return false;
    }

    @Override
    public void setRecord(Record record) {
        super.setRecord(record);
        PurchaseOrder order = (PurchaseOrder) getBean();
        if (order != null && order.getBusinessCaseId() != null) {
            try {
                SalesSearch salesSearch = (SalesSearch) getService(SalesSearch.class.getName());
                sales = salesSearch.fetchSales(order.getBusinessCaseId());
            } catch (Exception e) {
                log.warn("setRecord() failed [message=" + e.getMessage() + "]", e);
            }
        }
    }

    @Override
    public void refresh() {
        super.refresh();
        setSalesIdEditMode(false);
    }

    public void initDisplay(Product selectedProduct, Long selectedStock, String exitTarget) {
        this.product = selectedProduct;
        setExitTarget(exitTarget);
        setSelectedStock(selectedStock);
        refreshOpen();
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        Order order = (Order) getRecord();
        Date lastDelivery = order.getDelivery();
        PurchaseOrderManager manager = (PurchaseOrderManager) getRecordManager();
        manager.update(
                order,
                form.getLong("signatureLeft"),
                form.getLong("signatureRight"),
                form.getLong("personId"),
                form.getString("note"),
                form.getBoolean("taxFree"),
                form.getLong("taxFreeId"),
                form.getDouble("taxRate"),
                form.getDouble("reducedTaxRate"),
                form.getLong("currency"),
                form.getString("language"),
                form.getLong("branch"),
                form.getLong("shippingId"),
                form.getString("customHeader"),
                form.getBoolean("printComplimentaryClose"),
                form.getBoolean("printProjectmanager"),
                form.getBoolean("printSalesperson"),
                form.getBoolean("printBusinessCaseId"),
                form.getBoolean("printBusinessCaseInfo"),
                form.getBoolean("printRecordDate"),
                form.getBoolean("printRecordDateByStatus"),
                form.getBoolean("printPaymentTarget"),
                false,
                form.getBoolean("printCoverLetter"));
        String name = form.getString("name");
        String street = form.getString("street");
        String zipcode = form.getString("zipcode");
        String city = form.getString("city");
        Long country = form.getLong("country");
        if (isSet(name) || isSet(street) || isSet(zipcode) || isSet(city)) {
            manager.update(order, name, street, form.getString("streetAddon"), zipcode, city, country);
        }
        Date newDelivery = form.getDate("delivery");
        if (newDelivery != null && !newDelivery.equals(lastDelivery)) {
            if (log.isDebugEnabled()) {
                log.debug("save() new delivery date found, checking item behaviour...");
                String itemBehaviour = form.getString("deliveryDateOnItems");
                manager.update(
                        getDomainUser().getEmployee(),
                        order,
                        newDelivery,
                        form.getString("deliveryNote"),
                        itemBehaviour,
                        form.getBoolean("deliveryConfirmed"));
            }
        }
        refresh();
    }

    public void create() throws ClientException, PermissionException {
        if (supplier == null) {
            throw new ClientException(ErrorCode.SUPPLIER_UNKNOWN);
        }
        PurchaseOrderManager manager = (PurchaseOrderManager) getRecordManager();
        Employee user = getDomainEmployee();
        BranchOffice office = fetchHeadquarter(company);
        Order order = manager.create(
                user,
                company,
                office.getId(),
                supplier);
        setRecord(order);
        setList(manager.getOpen(true));
    }

    public void createCopy() throws PermissionException {
        PurchaseOrderManager manager = (PurchaseOrderManager) getRecordManager();
        PurchaseOrder current = (PurchaseOrder) getOrder();
        Order copy = manager.create(getDomainEmployee(), current);
        setRecord(copy);
    }

    @Override
    public void delete() throws ClientException, PermissionException {
        PurchaseOrder order = (PurchaseOrder) getRecord();
        PurchaseOrderManager manager = (PurchaseOrderManager) getRecordManager();
        manager.delete(order, getDomainUser().getEmployee());
        setBean(null);
        refreshOpen();
    }

    @Override
    protected void deleteItemValidation(Record record, Item selected) throws ClientException {
        if (selected.getOutstanding() == 0) {
            throw new ClientException(ErrorCode.DELIVERIES_FOUND);
        }
    }

    public void refreshOpen() {
        PurchaseOrderManager manager = getOrderManager();
        if (product != null) {
            if (log.isDebugEnabled()) {
                log.debug("refreshOpen() invoked in product context");
            }
            setList(manager.findOpenReleased(product, true));
            if (!isItemListing()) {
                changeItemListing();
            }
        }
    }

    public Product getProduct() {
        return product;
    }

    public void enableConditionsEdit() {
        // TODO Auto-generated method stub
    }

    public void checkDirectInvoiceBooking() throws ClientException {
        Order order = getOrder();
        List<DeliveryNote> notes = order.getDeliveryNotes();
        if (order.isNotDeliveryNoteAffectingItemsOnly()) {
            return;
        }
        if (notes == null || notes.isEmpty()) {
            throw new ClientException(ErrorCode.DELIVERY_MISSING);
        }
        for (int i = 0, j = notes.size(); i < j; i++) {
            DeliveryNote next = notes.get(i);
            if (next.getItems().isEmpty()) {
                throw new ClientException(ErrorCode.DELIVERY_ITEMS_MISSING);
            }
        }
    }

    public Long getCompany() {
        return company;
    }

    public void setCompany(Long company) {
        this.company = company;
    }

    public void selectSupplier(Long id) {
        if (isNotSet(id)) {
            supplier = null;
            if (log.isDebugEnabled()) {
                log.debug("selectSupplier() supplier reset");
            }
        } else {
            try {
                SupplierSearch supplierSearch = (SupplierSearch) getService(SupplierSearch.class.getName());
                supplier = supplierSearch.findById(id);
            } catch (Exception e) {
                throw new IllegalArgumentException("supplier " + id + " does not exist");
            }
        }
    }

    public Order getOrder() {
        if (getBean() != null && getBean() instanceof Order) {
            return (Order) getBean();
        }
        return null;
    }

    public boolean isConditionsEdit() {
        return false;
    }

    public boolean isSplittingMode() {
        return splittingMode;
    }

    public List<OrderItem> getSplittingListSource() {
        return splittingListSource;
    }

    public List<OrderItem> getSplittingListTarget() {
        return splittingListTarget;
    }

    public OrderItem getCurrentSplittingItem() {
        return currentSplittingItem;
    }

    public void enableSplittingMode() throws ClientException {
        if (!splittingListSource.isEmpty()) {
            splittingListSource.clear();
        }
        if (!splittingListTarget.isEmpty()) {
            splittingListTarget.clear();
        }
        Order order = getOrder();
        if (order == null) {
            throw new ActionException("enableSplittingMode", "no order found");
        }
        int count = order.getOpenDeliveries().size();
        if (count == 0) {
            throw new ClientException(ErrorCode.DELIVERY_ITEMS_MISSING);
        }
        for (int i = 0; i < count; i++) {
            splittingListSource.add((OrderItem) order.getOpenDeliveries().get(i).clone());
        }
        currentSplittingItem = splittingListSource.get(0);
        splittingMode = true;
    }

    public void disableSplittingMode() {
        splittingListSource.clear();
        splittingListTarget.clear();
        splittingMode = false;
        currentSplittingItem = null;
    }

    @Override
    public void setSelection(Long id) {
        Order order = null;
        try {
            super.setSelection(id);
            order = getOrder();
        } catch (Throwable t) {
            order = (Order) getRecordManager().find(id);
            setRecord(order);
        }
        if (order != null) {
            setOpenDeliveries(order.getOpenDeliveries());
        }
    }

    @Override
    public void createNewVersion() throws ClientException, PermissionException {
        Order order = getOrder();
        if (order == null) {
            throw new ActionException("createNewVersion", "no order found");
        }
        if (!order.getDeliveryNotes().isEmpty()) {
            throw new ClientException(ErrorCode.DELIVERY_ITEMS_FOUND);
        }
        PurchaseOrderManager orderManager = getOrderManager();
        orderManager.createNewVersion(getDomainEmployee(), order, false, null);
        setRecord(orderManager.find(order.getId()));
        setSetupMode(false);
    }

    @Override
    public boolean isImportSupported() {
        RecordImportService importService = (RecordImportService) getService(RecordImportService.class.getName());
        return (importService.getAvailableMappers(getRecord().getType()).size() > 0);
    }

    @Override
    public boolean isItemBackupAvailable() {
        if (isImportSupported()) {
            PurchaseOrderManager orderManager = getOrderManager();
            return orderManager.isItemBackupAvailable(getOrder(), RecordBackupItem.CONTEXT_IMPORT);
        }
        return false;
    }

    @Override
    public void restoreItems() {
        PurchaseOrderManager orderManager = getOrderManager();
        setRecord(orderManager.restoreItems(getDomainEmployee(), getOrder(), RecordBackupItem.CONTEXT_IMPORT));
        disableSetupMode();
    }

    @Override
    public RecordManager getRecordManager() {
        return getOrderManager();
    }

    @Override
    protected PurchaseOrderManager getOrderManager() {
        return (PurchaseOrderManager) getService(PurchaseOrderManager.class.getName());
    }

    @Override
    protected boolean checkRecordItemsMatchSalesItems() {
        return true;
    }

    public boolean isSupplierReferenceNumberEditModeDisplayedWhenClosed() {
        return true;
    }

    public void updateDeliveryCondition(Form form) throws ClientException {
        if (form.getLong("deliveryConditionId") != -1L) {
            setLocalValue("deliveryConditionId", form.getLong("deliveryConditionId"));
            PurchaseOrderManager manager = getOrderManager();
            PurchaseOrder order = (PurchaseOrder) getOrder();
            manager.updateDeliveryCondition(order, (Long) getLocalValue("deliveryConditionId"));
        }
    }

    public boolean isDeliveryConditionEditMode() {
        return deliveryConditionEditMode;
    }

    public void setDeliveryConditionEditMode(boolean deliveryConditionEditMode) {
        this.deliveryConditionEditMode = deliveryConditionEditMode;
        PurchaseOrder order = (PurchaseOrder) getOrder();
        if (deliveryConditionEditMode && order.getDeliveryCondition() != null) {
            setLocalValue("deliveryConditionId", order.getDeliveryCondition().getId());
        }
    }

    public Sales getReferencedSales() {
        return sales;
    }

    public void setDeliveryAddressByBusinessCase() throws ClientException {
        if (sales != null && getBean() != null) {
            PurchaseOrder order = (PurchaseOrder) getOrder();
            setBean(getOrderManager().updateDeliveryAddress(order, sales));
        }
    }

    public void resetDeliveryAddress() {
        if (sales != null && getBean() != null) {
            PurchaseOrder order = (PurchaseOrder) getOrder();
            order.getDeliveryAddress().setCity(null);
        }
    }
}
