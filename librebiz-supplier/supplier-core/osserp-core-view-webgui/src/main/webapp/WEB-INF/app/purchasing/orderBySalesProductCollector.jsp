<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="orderBySalesProductCollectorView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="headline">
		<fmt:message key="${view.headerName}" />
	</tiles:put>
	<tiles:put name="headline_right">
        <v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="orderBySalesProductCollectorContent">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><fmt:message key="product"/></th>
                                    <th><fmt:message key="name"/></th>
                                    <th><fmt:message key="quantity"/></th>
                                    <th class="center"><fmt:message key="action"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="item" items="${view.list}">
                                    <tr>
                                        <td><o:out value="${item.product.productId}"/></td>
                                        <td><o:out value="${item.product.name}"/></td>
                                        <td class="right"><o:number value="${item.quantity}" format="decimal"/></td>
                                        <td class="center"><v:link url="/purchasing/orderBySalesProductCollector/collect?id=${item.id}"><o:img name="enabledIcon"/></v:link></td>
                                    </tr>
                                </c:forEach>
                                <c:if test="${!empty view.collected}">
                                    <tr>
                                        <th colspan="3"><fmt:message key="alreadySelectedItemsLabel"/>:</th>
                                        <th class="center"><v:link url="/purchasing/orderBySalesProductCollector/save"><o:img name="saveIcon"/></v:link></th>
                                    </tr>
                                    <c:forEach var="item" items="${view.collected}">
                                        <tr>
                                            <td><o:out value="${item.product.productId}"/></td>
                                            <td><o:out value="${item.product.name}"/></td>
                                            <td class="right"><o:number value="${item.quantity}" format="decimal"/></td>
                                            <td class="center"><v:link url="/purchasing/orderBySalesProductCollector/removeCollected?id=${item.id}"><o:img name="deleteIcon"/></v:link></td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
