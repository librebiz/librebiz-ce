<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${!record.historical}">
<tr>
	<td><fmt:message key="signatureLeft"/></td>
	<td>
		<select name="signatureLeft" size="1" class="input">
		<c:choose>
		<c:when test="${empty record.signatureLeft}">
			<option value="0" selected="selected"><fmt:message key="undefined"/></option>
		</c:when>
		<c:otherwise>	
			<option value="0"><fmt:message key="undefined"/></option>
		</c:otherwise>	
		</c:choose>	
		<c:forEach var="employee" items="${view.signatureLeftEmployees}">
		<c:choose>	
		<c:when test="${(!empty record.signatureLeft) and (record.signatureLeft == employee.id)}">
			<option value="<o:out value="${employee.id}"/>" selected="selected"><o:out value="${employee.name}"/></option>						
		</c:when>
		<c:otherwise>	
			<option value="<o:out value="${employee.id}"/>"><o:out value="${employee.name}"/></option>						
		</c:otherwise>	
		</c:choose>	
		</c:forEach>
		</select>
	</td>
</tr>
<tr>
	<td><fmt:message key="signatureRight"/></td>
	<td>
		<select name="signatureRight" size="1" class="input">
		<c:choose>
		<c:when test="${empty record.signatureRight}">
			<option value="0" selected="selected"><fmt:message key="undefined"/></option>
		</c:when>
		<c:otherwise>	
			<option value="0"><fmt:message key="undefined"/></option>
		</c:otherwise>	
		</c:choose>	
		<c:forEach var="employee" items="${view.signatureRightEmployees}">
		<c:choose>	
		<c:when test="${(!empty record.signatureRight) and (record.signatureRight == employee.id)}">
			<option value="<o:out value="${employee.id}"/>" selected="selected"><o:out value="${employee.name}"/></option>						
		</c:when>
		<c:otherwise>	
			<option value="<o:out value="${employee.id}"/>"><o:out value="${employee.name}"/></option>						
		</c:otherwise>	
		</c:choose>	
		</c:forEach>
		</select>
	</td>
</tr>
<c:if test="${!empty view.contactPersons}">
<tr>
	<td><fmt:message key="attn"/></td>
	<td>
		<select name="personId" size="1" class="input">
		<c:choose>
		<c:when test="${empty record.personId}">
			<option value="0" selected="selected"><fmt:message key="undefined"/></option>
		</c:when>
		<c:otherwise>	
			<option value="0"><fmt:message key="undefined"/></option>
		</c:otherwise>	
		</c:choose>	
		<c:forEach var="person" items="${view.contactPersons}">
		<c:choose>	
		<c:when test="${(!empty record.personId) and (record.personId == person.contactId)}">
			<option value="<o:out value="${person.contactId}"/>" selected="selected"><o:out value="${person.displayName}"/></option>						
		</c:when>
		<c:otherwise>	
			<option value="<o:out value="${person.contactId}"/>"><o:out value="${person.displayName}"/></option>						
		</c:otherwise>	
		</c:choose>	
		</c:forEach>
		</select>
	</td>
</tr>
</c:if>
</c:if>