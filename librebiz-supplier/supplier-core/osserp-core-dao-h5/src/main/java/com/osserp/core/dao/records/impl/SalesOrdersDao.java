/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 4:04:40 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.Calculations;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.ProductQueries;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesOffers;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.ListItem;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Stock;
import com.osserp.core.model.records.SalesOrderDeliveryDateChangedInfoImpl;
import com.osserp.core.model.records.SalesOrderImpl;
import com.osserp.core.model.records.SalesOrderItemChangedInfoImpl;
import com.osserp.core.model.records.SalesOrderTypeImpl;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductUtil;
import com.osserp.core.projects.ProjectRequest;
import com.osserp.core.purchasing.PurchaseRecord;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesOrder;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrdersDao extends AbstractSalesRecords implements SalesOrders {
    private static Logger log = LoggerFactory.getLogger(SalesOrdersDao.class.getName());

    private String salesOrderItemTable = null;
    private String salesOrderTable = null;
    private SalesOffers salesOffers = null;
    private Calculations calculations = null;
    private ProductQueries productQueries = null;
    private ProductSummaryCache summaryCache = null;

    /**
     * Creates new sales orders dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param namesCache
     * @param taxRates
     * @param companies
     * @param infoConfigs
     * @param paymentConditions
     * @param offers
     * @param calculations
     * @param productQueries
     * @param summaryCache
     */
    public SalesOrdersDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            SalesOffers offers,
            Calculations calculations,
            ProductQueries productQueries,
            ProductSummaryCache summaryCache) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs);
        this.salesOrderItemTable = getTable(TableKeys.SALES_ORDER_ITEMS);
        this.salesOrderTable = getTable(TableKeys.SALES_ORDERS);
        this.salesOffers = offers;
        this.calculations = calculations;
        this.productQueries = productQueries;
        this.summaryCache = summaryCache;
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.SALES_ORDER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.impl.AbstractRecords#load(java.lang.Long)
     */
    @Override
    public Record load(Long id) {
        SalesOrderImpl order = (SalesOrderImpl) super.load(id);
        loadAll(order);
        return order;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.impl.AbstractRecords#loadAfterAll(com.osserp.core.finance.Record)
     */
    @Override
    protected void loadAfterAll(Record record) {
        List<Item> items = record.getItems();
        for (int i = 0, j = items.size(); i < j; i++) {
            summaryCache.loadSummary(items.get(i).getProduct());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#checkCreate(com.osserp.core.requests.Request)
     */
    public void checkCreate(Request request) throws ClientException {
        if (request.getType().isDirectSales()) {
            if (log.isDebugEnabled()) {
                log.debug("checkCreate() invoked with direct sales request...");
            }
            return;
        }
        Long offer = salesOffers.findActivated(request.getRequestId());
        if (log.isDebugEnabled()) {
            log.debug("checkCeate() invoked [requestId=" + request.getRequestId()
                    + ", offer="
                    + (offer == null ? "null" : offer)
                    + "]");
        }
        if (offer == null) {
            throw new ClientException(ErrorCode.OFFER_ACTIVATION);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#create(com.osserp.core.employees.Employee, com.osserp.core.sales.Sales, java.lang.String calculatorName,
     * java.util.Date dateConfirmed, java.lang.String note)
     */
    public Order create(Employee user, Sales sales, String calculatorName, SalesOffer offer, Date dateConfirmed, String note) {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [sales=" + sales.getId()
                    + ", offer=" + (offer == null ? "null" : offer.getId())
                    + ", dateConfirmed=" + (dateConfirmed == null ? "null" : dateConfirmed)
                    + ", note=" + (note == null ? "null" : note)
                    + "]");
        }
        boolean itemsEditable = (sales.getType().isDirectSales() || !sales.getType().isRecordByCalculation());
        SalesOrderImpl obj = null;
        RecordType recordType = getRecordType();
        if (offer == null) {

            if (!itemsEditable) {
                try {
                    calculations.create(
                            user,
                            sales.getId(),
                            sales.getContextName(),
                            sales.getCreated(),
                            sales.getType(),
                            null, // use default name
                            calculatorName,
                            false, // sales price not locked by default
                            sales.getType().getMinimalMargin(),
                            sales.getType().getTargetMargin());
                } catch (ClientException e) {
                    // should not happen
                    log.warn("create() failed to create calculation [sales=" + sales.getId()
                            + ", user=" + user.getId()
                            + "]");
                }
            }
            Date createdDate = sales.getType().isDirectSales() ? sales.getCreated() : null;
            obj = new SalesOrderImpl(
                    getNextRecordId(user, sales.getType().getCompany(), recordType, createdDate),
                    recordType,
                    getDefaultBookingType(),
                    sales,
                    fetchPaymentAgreement(sales.getRequest().getCustomer(), sales.getType()),
                    user,
                    taxRate,
                    reducedTaxRate,
                    itemsEditable,
                    itemsEditable,
                    calculatorName,
                    dateConfirmed,
                    note);
            saveInitial(obj);
            try {
                setDefaultInfos(user, obj, sales.getRequest().getType());
            } catch (Exception e) {
                log.warn("create() ignoring failed attempt to add default infos [sales="
                        + sales.getId() + ", type=" + sales.getType().getId() + "]");
            }
        } else {

            // sales is based on regular request
            if (!itemsEditable) {
                Calculation calc = salesOffers.getCalculation(offer);
                if (calc != null) {
                    calc.updateReference(sales.getContextName(), sales.getId());
                    calculations.save(calc);
                    if (isSet(calc.getPlantCapacity())) {
                        sales.setCapacity(calc.getPlantCapacity());
                    } else if (calc.getPrice() != null) {
                        sales.setCapacity(calc.getPrice().doubleValue());
                    }
                } else if (offer.getAmounts() != null && offer.getAmounts().getAmount() != null) {
                    sales.setCapacity(offer.getAmounts().getAmount().doubleValue());
                }
            } else if (offer.getAmounts() != null && offer.getAmounts().getAmount() != null) {
                sales.setCapacity(offer.getAmounts().getAmount().doubleValue());
            }

            getCurrentSession().saveOrUpdate(sales);
            Stock stock = getStockByBusinessCase(sales);
            obj = new SalesOrderImpl(
                    getNextRecordId(user, sales.getType().getCompany(), recordType, null),
                    recordType,
                    getDefaultBookingType(),
                    sales,
                    user,
                    offer,
                    stock,
                    dateConfirmed,
                    note);
            saveInitial(obj);
            if (offer.getDelivery() != null && sales.getRequest() instanceof ProjectRequest) {
                ProjectRequest pr = (ProjectRequest) sales.getRequest();
                pr.addInstallationDate(offer.getCreatedBy(), offer.getDelivery());
            }
            getCurrentSession().saveOrUpdate(sales.getRequest());
        }
        return obj;
    }

    public Order create(Employee user, Sales sales, Product defaultProduct) {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [sales=" + sales.getId()
                    + ", user=" + (user == null ? "null" : user.getId())
                    + ", defaultProduct=" + (defaultProduct == null ? "null" : defaultProduct.getProductId())
                    + "]");
        }
        boolean itemsEditable = true;
        SalesOrderImpl obj = null;
        RecordType recordType = getRecordType();

        obj = new SalesOrderImpl(
                getNextRecordId(user, sales.getType().getCompany(), recordType, null),
                recordType,
                getDefaultBookingType(),
                sales,
                fetchPaymentAgreement(sales.getRequest().getCustomer(), sales.getType()),
                user,
                taxRate,
                reducedTaxRate,
                itemsEditable,
                itemsEditable,
                sales.getType().getCalculatorName(),
                sales.getCreated(),
                null);
        if (defaultProduct != null) {
            obj.addItem(
                defaultProduct.getDefaultStock(),
                defaultProduct,
                null, // customName
                1d,
                defaultProduct.isReducedTax() ? reducedTaxRate : taxRate,
                new BigDecimal(1D),
                new Date(),
                new BigDecimal(1D),
                false,
                false,
                defaultProduct.getEstimatedPurchasePrice() == null ?
                        new BigDecimal(0) :
                            new BigDecimal(defaultProduct.getEstimatedPurchasePrice()),
                null,
                true,
                null); // externalId
        }
        saveInitial(obj);
        try {
            setDefaultInfos(user, obj, sales.getRequest().getType());
        } catch (Exception e) {
            log.warn("create() ignoring failed attempt to add default infos [sales="
                    + sales.getId() + ", type=" + sales.getType().getId() + "]");
        }
        return obj;
    }

    public Order createByRecord(Employee user, Sales sales, Record other, boolean overridePrice) {
        boolean itemsEditable = true;
        RecordType recordType = getRecordType();
        SalesOrderImpl obj = null;
        boolean existingObj = false;
        if (sales.getType().isDirectSales()) {
            obj = (SalesOrderImpl) find(sales);
            existingObj = true;
        }
        if (obj == null) {

            obj = new SalesOrderImpl(
                    getNextRecordId(user, sales.getType().getCompany(), recordType, null),
                    recordType,
                    getDefaultBookingType(),
                    sales,
                    fetchPaymentAgreement(sales.getRequest().getCustomer(), sales.getType()),
                    user,
                    taxRate,
                    reducedTaxRate,
                    itemsEditable,
                    itemsEditable,
                    sales.getType().getCalculatorName(),
                    sales.getCreated(),
                    null);
        }
        if (other != null && !other.getItems().isEmpty()) {
            if (!obj.getItems().isEmpty()) {
                obj.getItems().clear();
            }

            String productPriceMethod = getPropertyString(ProductUtil.PRODUCT_PRICE_SALES_PROPERTY_METHOD);
            String productPriceDefault = getPropertyString(ProductUtil.PRODUCT_PRICE_SALES_PROPERTY_DEFAULT);
            if (ProductUtil.METHOD_LAST_PRICE_PAID.equals(productPriceMethod) && overridePrice) {
                productQueries.addLastPricePaid(other, sales.getCustomer().getId(), true);
            }
            Stock stock = getStockByBusinessCase(sales);
            for (int i = 0, j = other.getItems().size(); i < j; i++) {
                Item next = other.getItems().get(i);
                BigDecimal price = next.getPrice();
                if (overridePrice) {
                    price = new BigDecimal(ProductUtil.getDefaultPrice(
                            next.getProduct(),
                            sales.getCustomer(),
                            productPriceMethod,
                            productPriceDefault));
                }
                obj.addItem(
                        stock != null ? stock.getId() : next.getProduct().getDefaultStock(),
                        next.getProduct(),
                        null, // customName
                        next.getQuantity(),
                        next.getTaxRate(),
                        price,
                        new Date(),
                        new BigDecimal(next.getProduct().getPartnerPrice()),
                        false,
                        false,
                        (other instanceof PurchaseRecord) ? next.getPrice() :
                            new BigDecimal(next.getProduct().getEstimatedPurchasePrice()),
                        null,  // note
                        true,  // includePrice
                        null); // externalId
            }
        }
        if (existingObj) {
            save(obj);
        } else {
            saveInitial(obj);
            try {
                setDefaultInfos(user, obj, sales.getRequest().getType());
            } catch (Exception e) {
                log.warn("createByRecord() ignoring failed attempt to add default infos [sales="
                        + sales.getId() + ", type=" + sales.getType().getId() + "]");
            }
        }
        return obj;
    }

    public Order createOrUpdate(Sales sales, List<Item> items) {
        Order result = fetch(sales);
        RecordType recordType = getRecordType();
        Employee user = fetchEmployee(sales.getCreatedBy());
        boolean existingOrder = true;
        
        if (result == null) {
            existingOrder = false;

            result = new SalesOrderImpl(
                getNextRecordId(user, sales.getType().getCompany(), recordType, null),
                recordType,
                getDefaultBookingType(),
                sales,
                fetchPaymentAgreement(sales.getRequest().getCustomer(), sales.getType()),
                user,
                taxRate,
                reducedTaxRate,
                true,
                true,
                sales.getType().getCalculatorName(),
                sales.getCreated(),
                null);
            saveInitial(result);
            setDefaultInfos(user, result, sales.getType());
        } else {
            result.getItems().clear();
        }
        if (!existingOrder || (!result.isClosed() && !result.isUnchangeable())) {
            result = (Order) updateItems(result, items);
            if (existingOrder) {
                try {
                    Calculation last = (Calculation) calculations.getLast(sales.getId(), sales.getContextName());
                    if (last != null && !last.isHistorical()) {
                        if (last.getAllItems().isEmpty()) {
                            calculations.delete(last);
                            if (log.isDebugEnabled()) {
                                log.debug("createOrUpdate() removed empty calculation "
                                        + "[id=" + last.getId() + "]");
                            }
                        } else {
                            last.setHistorical(true);
                            calculations.save(last);
                            if (log.isDebugEnabled()) {
                                log.debug("createOrUpdate() set last calculation historical "
                                        + "[id=" + last.getId() + "]");
                            }
                        }
                    }
                } catch (Exception e) {
                    log.warn("createOrUpdate() failed on attempt to update calculation "
                            + "[message=" + e.getMessage() + "]", e);
                }
            }
            save(result);
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#create(com.osserp.core.employees.Employee, java.lang.Long, java.lang.Long,
     * com.osserp.core.finance.BookingType, com.osserp.core.customers.Customer, com.osserp.core.sales.Sales)
     */
    public Order create(
            Employee user,
            Long company,
            Long branchId,
            BookingType bookingType,
            Customer customer,
            Sales reference,
            Long sale) {

        RecordType recordType = getRecordType();
        SalesOrderImpl obj = new SalesOrderImpl(
                getNextRecordId(user, company, recordType, null),
                recordType,
                bookingType,
                fetchPaymentAgreement(customer, reference.getType()),
                company,
                branchId,
                reference.getId(),
                customer,
                user,
                sale,
                taxRate,
                reducedTaxRate,
                reference.getType().getMinimalMargin(),
                reference.getType().getTargetMargin());
        saveInitial(obj);
        return obj;
    }

    private PaymentAgreement fetchPaymentAgreement(Customer customer, BusinessType type) {
        PaymentAgreement result = customer.getPaymentAgreement();
        if (result != null && result.getPaymentCondition() != null) {
            return result;
        }
        return getDefaultPaymentAgreement(type);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#clearOrder(com.osserp.core.finance.Order)
     */
    public void clearOrder(Order order) {
        for (Iterator<Item> i = order.getItems().iterator(); i.hasNext();) {
            i.next();
            i.remove();
        }
        save(order);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#createItemChangedHistory(com.osserp.core.employees.Employee, java.util.List, java.util.List)
     */
    public List<ItemChangedInfo> createItemChangedHistory(Employee user, List<Item> current, List<Item> previous) {
        List<ItemChangedInfo> result = new ArrayList<>();
        try {
            List<ItemChangedInfo> list = createChangeset(user, current, previous);
            for (int i = 0, j = list.size(); i < j; i++) {
                ItemChangedInfo vo = list.get(i);
                ItemChangedInfo persistent = new SalesOrderItemChangedInfoImpl(vo);
                getCurrentSession().saveOrUpdate(SalesOrderItemChangedInfoImpl.class.getName(), persistent);
                result.add(persistent);
            }
        } catch (Exception e) {
            log.error("createItemChangedHistory() failed [message=" + e.getMessage() + "]", e);
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#createDeliveryChangedHistory(com.osserp.core.employees.Employee, com.osserp.core.sales.SalesOrder,
     * java.util.List, java.util.List)
     */
    public List<DeliveryDateChangedInfo> createDeliveryChangedHistory(Employee user, SalesOrder order, List<Item> current, List<Item> previous) {
        List<DeliveryDateChangedInfo> result = new ArrayList<>();
        try {
            List<DeliveryDateChangedInfo> list = createDeliveryChangeset(user, order, current, previous);
            for (int i = 0, j = list.size(); i < j; i++) {
                DeliveryDateChangedInfo vo = list.get(i);
                DeliveryDateChangedInfo persistent = new SalesOrderDeliveryDateChangedInfoImpl(vo);
                getCurrentSession().saveOrUpdate(SalesOrderDeliveryDateChangedInfoImpl.class.getName(), persistent);
                result.add(persistent);
            }
        } catch (Exception e) {
            log.error("createDeliveryChangedHistory() failed [message=" + e.getMessage() + "]", e);
        }
        return result;
    }

    protected final List<DeliveryDateChangedInfo> createDeliveryChangeset(Employee user, SalesOrder order, List<Item> currentItems, List<Item> previousItems) {
        List<DeliveryDateChangedInfo> result = new ArrayList<>();

        Map<Long, Item> current = new HashMap<>();
        for (int i = 0, j = currentItems.size(); i < j; i++) {
            Item next = currentItems.get(i);
            current.put(next.getProduct().getProductId(), next);
        }
        Map<Long, Item> previous = new HashMap<>();
        for (int i = 0, j = previousItems.size(); i < j; i++) {
            Item next = previousItems.get(i);
            previous.put(next.getProduct().getProductId(), next);
        }

        for (int i = 0, j = currentItems.size(); i < j; i++) {
            OrderItem currentItem = (OrderItem) currentItems.get(i);
            OrderItem previousItem = (OrderItem) previous.get(currentItem.getProduct().getProductId());
            if (!currentItem.getProduct().isVirtual()) {
                if (previousItem == null || !currentItem.getAmount().equals(previousItem.getAmount())) {
                    DeliveryDateChangedInfo info = new SalesOrderDeliveryDateChangedInfoImpl(
                            user,
                            order,
                            currentItem.getProduct(),
                            currentItem.getQuantity(),
                            currentItem.getStockId(),
                            (previousItem == null) ? null : previousItem.getDelivery(),
                            currentItem.getDelivery(),
                            currentItem.getNote());
                    result.add(info);
                }
            }
        }
        for (int i = 0, j = previousItems.size(); i < j; i++) {
            OrderItem previousItem = (OrderItem) previousItems.get(i);
            OrderItem currentItem = (OrderItem) current.get(previousItem.getProduct().getProductId());
            if (currentItem == null) {
                DeliveryDateChangedInfo info = new SalesOrderDeliveryDateChangedInfoImpl(
                        user,
                        order,
                        previousItem.getProduct(),
                        0d,
                        previousItem.getStockId(),
                        previousItem.getDelivery(),
                        null,
                        previousItem.getNote());
                result.add(info);
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#find(com.osserp.core.sales.Sales)
     */
    public Order find(Sales sales) {
        SalesOrderImpl order = (SalesOrderImpl) fetch(sales);
        if (order == null) {
            return null;
        }
        loadAll(order);
        return order;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#load(com.osserp.core.sales.Sales)
     */
    public Order load(Sales sales) {
        SalesOrderImpl order = (SalesOrderImpl) fetch(sales);
        if (order == null) {
            throw new BackendException("load(sales) invoked in a context" +
                    " where order must exist [sales=" + sales.getId() + "]");
        }
        loadAll(order);
        return order;
    }
    
    protected Order fetch(Sales sales) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.businessCaseId = :salesId order by o.id desc");
        List<SalesOrderImpl> result = getCurrentSession().createQuery(query.toString())
                .setParameter("salesId", sales.getId()).list();
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public List<SalesOrder> getByContact(Long contactId) {
        return (List<SalesOrder>) super.getByContact(contactId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#findServiceOrders(com.osserp.core.sales.Sales)
     */
    public List<Order> getServiceOrders(Sales sales) {
        List<Order> result = new ArrayList<>();
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.reference = :salesId order by o.id desc");
        List<SalesOrderImpl> list = getCurrentSession().createQuery(query.toString())
                .setParameter("salesId", sales.getId()).list();
        if (!result.isEmpty()) {
            for (int i = 0, j = list.size(); i < j; i++) {
                SalesOrderImpl order = list.get(i);
                if (SalesOrder.BOOK_TYPE_SERVICE.equals(order.getBookingType().getId())
                        || SalesOrder.BOOK_TYPE_WARRANTY.equals(order.getBookingType().getId())) {
                    loadAll(order);
                    result.add(order);
                    if (log.isDebugEnabled()) {
                        log.debug("findServiceOrders() order added [sales=" + sales.getId()
                                + ", order=" + order.getId() + "]");
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("findServiceOrders() done [count=" + result.size() + "]");
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#changeBranch(com.osserp.core.sales.Sales)
     */
    public void changeBranch(Sales sales) {
        Long stockId = getStockByBusinessCase(sales).getId();
        StringBuilder sql = new StringBuilder(128);
        sql
                .append("UPDATE ")
                .append(salesOrderTable)
                .append(" SET branch_id = ? WHERE sales_id = ?");
        final Object[] params = { sales.getBranch().getId(), sales.getId() };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
        sql = new StringBuilder(128);
        sql
                .append("UPDATE ")
                .append(salesOrderItemTable)
                .append(" SET stock_id = ? WHERE reference_id IN (SELECT id FROM ")
                .append(salesOrderTable)
                .append(" WHERE sales_id = ?)");
        final Object[] sparams = { stockId, sales.getId() };
        jdbcTemplate.update(sql.toString(), sparams, types);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#findPaymentAgreement(java.lang.Long)
     */
    public RecordPaymentAgreement findPaymentAgreement(Long orderId) {
        List<RecordPaymentAgreement> result = getCurrentSession().createQuery(
                "from SalesOrderPaymentAgreementImpl o where o.reference = :orderId")
                .setParameter("orderId", orderId).list();
        return (empty(result) ? null : result.get(0));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.Orders#update(com.osserp.core.employees.Employee, com.osserp.core.finance.Order, java.util.Date, java.lang.String,
     * java.lang.String, boolean)
     */
    public List<DeliveryDateChangedInfo> update(
            Employee user,
            Order record,
            Date deliveryDate,
            String deliveryNote,
            String itemBehaviour,
            boolean deliveryConfirmed)
            throws ClientException {

        if (!(record instanceof SalesOrderImpl)) {
            throw new BackendException("expected instance of SalesOrderImpl, "
                    + " found " + record.getClass().getName());
        }
        if (itemBehaviour == null || itemBehaviour.length() < 1) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        List<DeliveryDateChangedInfo> infoList = new ArrayList<>();
        if (itemBehaviour.equals("resetItems")) {
            record.setDelivery(null);
            for (int i = 0, j = record.getItems().size(); i < j; i++) {
                OrderItem next = (OrderItem) record.getItems().get(i);
                if (!next.getProduct().isVirtual()) {
                    DeliveryDateChangedInfo info = new SalesOrderDeliveryDateChangedInfoImpl(
                            user,
                            (SalesOrder) record,
                            next.getProduct(),
                            next.getQuantity(),
                            next.getStockId(),
                            next.getDelivery(),
                            null,
                            deliveryNote);
                    infoList.add(info);
                }
                next.setDelivery(null);
                next.setDeliveryConfirmed(false);
                next.setDeliveryDateBy(user.getId());
                next.setDeliveryNote(null);
            }
        } else {

            if (deliveryDate == null) {
                throw new ClientException(ErrorCode.DATE_MISSING);
            }
            Date now = new Date(System.currentTimeMillis());
            if (deliveryDate.before(now) && !DateUtil.isSameDay(deliveryDate, now)) {
                throw new ClientException(ErrorCode.DATE_PAST);
            }
            if (!itemBehaviour.equals("noItems")) {
                Date nextDeliveryDate = deliveryDate;
                boolean allItems = itemBehaviour.equals("allItems");
                boolean empty = itemBehaviour.equals("emptyItems");
                for (int i = 0, j = record.getItems().size(); i < j; i++) {
                    OrderItem next = (OrderItem) record.getItems().get(i);
                    if (allItems || (empty && next.getDelivery() == null)) {
                        if (!next.getProduct().isVirtual()) {
                            DeliveryDateChangedInfo info = new SalesOrderDeliveryDateChangedInfoImpl(
                                    user,
                                    (SalesOrder) record,
                                    next.getProduct(),
                                    next.getQuantity(),
                                    next.getStockId(),
                                    next.getDelivery(),
                                    deliveryDate,
                                    deliveryNote);
                            infoList.add(info);
                        }
                        next.setDelivery(deliveryDate);
                        next.setDeliveryConfirmed(deliveryConfirmed);
                        next.setDeliveryDateBy(user.getId());
                        next.setDeliveryNote(deliveryNote);
                    } else if (next.getDelivery() != null
                            && !next.getDelivery().before(now)
                            && next.getDelivery().before(nextDeliveryDate)) {
                        nextDeliveryDate = next.getDelivery();
                    }
                }
                record.setDelivery(nextDeliveryDate);
            }
        }
        save(record);
        if (!infoList.isEmpty()) {
            for (Iterator<DeliveryDateChangedInfo> i = infoList.iterator(); i.hasNext();) {
                save(i.next());
            }
        }
        return infoList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.Orders#update(com.osserp.core.employees.Employee, com.osserp.core.finance.Order, java.util.Date, java.lang.String,
     * com.osserp.core.Item)
     */
    public DeliveryDateChangedInfo update(
            Employee user,
            Order order,
            Date estimatedDelivery,
            String deliveryNote,
            Item item) throws ClientException {

        if (!(order instanceof SalesOrderImpl)) {
            throw new BackendException("expected instance of SalesOrderImpl, "
                    + " found " + order.getClass().getName());
        }
        Date now = new Date(System.currentTimeMillis());
        if (estimatedDelivery == null) {
            throw new ClientException(ErrorCode.DATE_MISSING);
        }
        if (estimatedDelivery.before(now) && !DateUtil.isSameDay(estimatedDelivery, now)) {
            throw new ClientException(ErrorCode.DATE_PAST);
        }
        SalesOrderImpl obj = (SalesOrderImpl) order;
        Date nextDeliveryDate = estimatedDelivery;
        DeliveryDateChangedInfo result = null;
        for (int i = 0, j = obj.getItems().size(); i < j; i++) {
            OrderItem next = (OrderItem) obj.getItems().get(i);
            if (next.getId().equals(item.getId())) {
                result = new SalesOrderDeliveryDateChangedInfoImpl(
                        user,
                        obj,
                        next.getProduct(),
                        next.getQuantity(),
                        next.getStockId(),
                        next.getDelivery(),
                        estimatedDelivery,
                        deliveryNote);
                next.setDelivery(estimatedDelivery);
                next.setDeliveryNote(deliveryNote);
                next.setDeliveryDateBy(user.getId());

            } else {
                if (next.getDelivery() != null
                        && next.getDelivery().before(nextDeliveryDate)
                        && next.getDelivery().after(now)) {
                    nextDeliveryDate = next.getDelivery();
                }
            }
        }
        obj.setDelivery(nextDeliveryDate);
        save(obj);
        if (result != null) {
            save(result);
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.Records#exists(java.lang.Long)
     */
    public boolean exists(Long recordId) {
        return exists(salesOrderTable, recordId);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.osserp.core.dao.records.Records#locked(java.lang.Long)
     */
    public boolean locked(Long recordId) {
        Calculation calc = (Calculation) calculations.getActive(recordId);
        return (calc != null && calc.isLocked());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.Records#count(java.lang.Long)
     */
    public int count(Long referenceId) {
        return count(salesOrderTable, referenceId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.Records#count(java.lang.Long)
     */
    public int countBySales(Long salesId) {
        return countBySales(salesOrderTable, salesId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.Orders#getDeliveryDateChangedInfos(com.osserp.core.finance.Order)
     */
    public List<DeliveryDateChangedInfo> getDeliveryDateChangedInfos(Order order) {
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(SalesOrderDeliveryDateChangedInfoImpl.class.getName())
                .append(" o where o.reference = :orderId order by o.product.productId, o.id");

        return getCurrentSession().createQuery(hql.toString())
                .setParameter("orderId", order.getId()).list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.Records#getCreated(java.lang.Long)
     */
    public Date getCreated(Long id) {
        return getCreated(salesOrderTable, id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#getItemListing(java.lang.Long)
     */
    public List<ListItem> getItemListing(Long recordId) {
        return getItemListing(salesOrderItemTable, recordId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#getByCompany(java.lang.Long, boolean)
     */
    public List<RecordDisplay> findByCompany(Long company, boolean openonly) {
        StringBuilder query = new StringBuilder("SELECT ");
        query
                .append(RecordDisplayRowMapper.RECORD_VALUES)
                .append(" FROM v_")
                .append(getQueryContext())
                .append("_query WHERE company_id = ?");
        if (openonly) {
            query.append(" AND status < ").append(Record.STAT_CLOSED);
        }
        query.append(" ORDER BY id DESC");
        List<Map<String, Object>> result = (List<Map<String, Object>>) jdbcTemplate.query(
                query.toString(),
                new Object[] { company },
                new int[] { Types.BIGINT },
                RecordDisplayRowMapper.getReader());
        return RecordDisplayRowMapper.createDisplayList(getOptionsCache(), result);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#getByCompany(java.lang.Long, java.lang.Long)
     */
    public List<RecordDisplay> findByCompanyAndReference(Long company, Long reference) {
        StringBuilder query = new StringBuilder("SELECT ");
        query
                .append(RecordDisplayRowMapper.RECORD_VALUES)
                .append(" FROM v_")
                .append(getQueryContext())
                .append("_query WHERE company_id = ? AND reference_id = ?")
                .append(" ORDER BY id DESC");
        List<Map<String, Object>> result = (List<Map<String, Object>>) jdbcTemplate.query(
                query.toString(),
                new Object[] { company, reference },
                new int[] { Types.BIGINT, Types.BIGINT },
                RecordDisplayRowMapper.getReader());
        return RecordDisplayRowMapper.createDisplayList(getOptionsCache(), result);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#getView(java.lang.String, java.lang.Object[], int[], boolean)
     */
    @Override
    public List<RecordDisplay> getView(
            String appendWhereClause,
            Object[] params,
            int[] types,
            boolean ignoreDefaultOrder) {

        return super.getView(
                appendWhereClause,
                params,
                types,
                ignoreDefaultOrder);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.SalesOrders#resetDeliveryDate(com.osserp.core.sales.Sales)
     */
    public void resetDeliveryDate(Sales sales) {
        try {
            SalesOrder order = (SalesOrder) find(sales);
            if (!order.isClosed() && !order.isDelivered()) {
                for (int i = 0, j = order.getItems().size(); i < j; i++) {
                    OrderItem next = (OrderItem) order.getItems().get(i);
                    if (next.getDelivered() == null
                            || next.getDelivered() < next.getQuantity()) {
                        next.setDelivery(null);
                    }
                }
                order.setDelivery(null);
            }
            save(order);
        } catch (Throwable t) {
            log.warn("resetDeliveryDate() throwed ignored exception: "
                    + t.toString(), t);
        }
    }

    @Override
    protected Class<SalesOrderImpl> getPersistentClass() {
        return SalesOrderImpl.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.impl.AbstractRecords#getItemChangedInfoClass()
     */
    @Override
    protected Class<SalesOrderItemChangedInfoImpl> getItemChangedInfoClass() {
        return SalesOrderItemChangedInfoImpl.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.impl.AbstractSubTypeAwareSalesRecords#getDefaultSubTypeId()
     */
    @Override
    public Long getDefaultBookingTypeId() {
        return SalesOrder.BOOK_TYPE_SALES;
    }

    @Override
    public Class<SalesOrderTypeImpl> getPersistentBookingTypeClass() {
        return SalesOrderTypeImpl.class;
    }

    private void save(DeliveryDateChangedInfo info) {
        getCurrentSession().saveOrUpdate(SalesOrderDeliveryDateChangedInfoImpl.class.getName(), info);
    }
}
