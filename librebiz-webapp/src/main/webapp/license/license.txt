Copyright And Software License Information
============================================================================

Librebiz ERP & CRM - http://librebiz.org

----------------------------------------------------------------------------

Copyright (c) 2017-2024 Librebiz Developers - All rights reserved.

This program is free software: you can redistribute it and/or modify 
it under the terms of the GNU Affero General Public License (AGPL) 
version 3 as published by the Free Software Foundation. In accordance 
with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
the effect that the original authors expressly exclude the warranty of
non-infringement of any third-party rights. 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
at http://www.gnu.org/licenses/agpl-3.0.html for more details.

The interactive user interfaces in modified source and object code 
versions must display Appropriate Legal Notices, as required under 
Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
the GNU AGPL you must include a clickable link "Powered by osserp.com"
that leads directly to the URL http://osserp.com in the footer area 
or, if not reasonably feasible for technical reason, as a top-level 
link of the primary navigation of the graphical user interface in
every copy of the program you distribute. 

----------------------------------------------------------------------------

Supplier Libraries - The open, smart and simple ERP - http://osserp.com

Copyright (c) 2001-2024 The original author or authors.

The majority of the code base is copyright by Rainer Kirchner and licensed
under various licenses depending on individual projects starting in 2001.
The licensing switched to AGPL3 along with a change of project specific 
package names to the dedicated namespace 'com.osserp' in 2009.

Some files written by thirdparty contributors were licensed under the terms
of the Apache License.
All thirdparty files initially licensed under the Apache License remain with 
that license.

----------------------------------------------------------------------------

Third-Party Frameworks / Libraries

Some third-party software packages included in the Distribution are licensed 
under the terms of one or more of the following License terms:


 - Apache License Version 2.0, 
   - http://www.apache.org/licenses/
   - ./license/text/apache-2.0

 - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
   - http://www.gnu.org/licenses/lgpl.html
   - ./license/text/lgpl-2.1

----------------------------------------------------------------------------

Trademarks

All brand and product names are trademarks, registered trademarks or service 
marks of their respective holders.
