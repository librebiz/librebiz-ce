/**
 *
 * Copyright (C) 2012 The original author or authors.
 * 
 * Created on 22.12.2012 
 * 
 */
package com.osserp.core.api.defaults

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WSContact
import com.osserp.api.WSContactService
import com.osserp.api.WSObjectException

import com.osserp.common.ClientException
import com.osserp.common.service.Locator

import com.osserp.core.service.impl.AbstractWebService

/**
 *
 * @author rk <rk@osserp.com>
 *
 */
class WsContactServiceProvider extends AbstractWebService implements WSContactService {
    private static Logger logger = LoggerFactory.getLogger(WsContactServiceProvider.class.getName())

    public WsContactServiceProvider(Locator locator) {
        super(locator)
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSContactService#fetchContactByEmail(java.lang.String)
     */
    public WSContact fetchContactByEmail(String email) {
        WSContact result
        try {
            result = wsContactService.fetchContactByEmail(email)
        } catch (Exception e) {
            logger.error("fetchContactByEmail: caught exception [email=${email}, message=${e.message}]", e)
        }
        result
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSContactService#fetchContactById(java.lang.Long)
     */
    public WSContact fetchContactById(Long contactId) {
        WSContact result
        try {
            result = wsContactService.fetchContactById(contactId)
        } catch (Exception e) {
            logger.error("fetchContactById: caught exception [contactId=${contactId}, message=${e.message}]", e)
        }
        result
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSContactService#getContactById(java.lang.Long)
     */
    public WSContact getContactById(Long contactId) throws WSObjectException {
        WSContact result
        try {
            result = wsContactService.getContactById(contactId)
        } catch (Exception e) {
            logger.error("getContactById: caught exception [contactId=${contactId}, message=${e.message}]", e)
            throw e        
        }
        result
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSContactService#createOrUpdate(com.osserp.api.WSContact)
     */
    public WSContact createOrUpdate(WSContact wsContact) {
        WSContact result
        try {
            result = wsContactService.createOrUpdate(wsContact)
        } catch (ClientException c) {
            logger.error("createOrUpdate: caught well known exception [id=${wsContact?.primaryKey}, message=${c.message}]")
        } catch (Exception e) {
            logger.error("createOrUpdate: caught unexpected exception [id=${wsContact?.primaryKey}, message=${e.message}]", e)
        }
        result
    }

    protected WSContactService getWsContactService() {
        getService(WSContactService.class.getName())
    }
}
