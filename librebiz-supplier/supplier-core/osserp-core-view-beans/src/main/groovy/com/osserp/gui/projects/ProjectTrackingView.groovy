/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 2, 2012 9:52:58 AM 
 * 
 */
package com.osserp.gui.projects

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.dms.DocumentData
import com.osserp.common.util.DateUtil

import com.osserp.core.BusinessCase
import com.osserp.core.Options
import com.osserp.core.employees.Employee
import com.osserp.core.projects.ProjectTracking
import com.osserp.core.projects.ProjectTrackingManager
import com.osserp.core.projects.ProjectTrackingRecord
import com.osserp.core.projects.ProjectTrackingRecordType
import com.osserp.core.projects.ProjectTrackingType

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.common.BusinessCaseAwareView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ProjectTrackingView extends BusinessCaseAwareView {
    
    private static Logger logger = LoggerFactory.getLogger(ProjectTrackingView.class.getName())
    
    ProjectTrackingRecord selectedRecord

    ProjectTrackingView() {
        super()
        disableAutoreload()
        providesCreateMode()
        providesEditMode()
        businessCaseRequired = true
        headerName = 'projectTrackingHeader'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            list = projectTrackingManager.findAll(businessCase)
        }
    }
    
    @Override
    void save() {
        
        ProjectTrackingType type
        
        String name = form.getString('name')
        Date startDate = form.getDate('startDate')
        Date endDate = form.getDate('endDate')
        Long typeId =  form.getLong('type')
        if (typeId) {
            type = getOption(Options.PROJECT_TRACKING_TYPES, typeId)
            logger.debug("save: type fetched [id=${typeId}, found=${(type != null)}]")
        }
        if (createMode) {
            if (!type) {
                throw new ClientException(ErrorCode.TYPE_MISSING)
            }
            bean = projectTrackingManager.create(domainUser, 
                businessCase, 
                type, 
                name, 
                startDate, 
                endDate)
            disableCreateMode()
        } else {
            bean = projectTrackingManager.update(domainUser,
                bean,
                type,
                name,
                startDate,
                endDate)

            disableEditMode()
        }
        list = projectTrackingManager.findAll(businessCase)
        createCustomNavigation()
    }
    
    void toggleIgnore() {
        Long id = form.getLong('id')
        if (id && bean && !projectTracking.closed) {
            bean = projectTrackingManager.toggleIgnore(domainUser, projectTracking, id)
        }
    }

    boolean isTextEditMode() {
        env.textEditMode
    }

    void disableTextEditMode() {
        env.textEditMode = false
        selectedRecord = null
        createCustomNavigation()
    }

    void enableTextEditMode() {
        env.textEditMode = true
        Long id = form.getLong('id')
        if (id && bean) {
            selectedRecord = projectTracking.records.find { it.id == id }
        }
        createCustomNavigation()
    }
    
    void deleteText() {
        Long id = form.getLong('id')
        if (id && bean) {
            bean = projectTrackingManager.deleteTrackingData(domainUser, projectTracking, id)
        }
    }
    
    @Override
    public void select() {
        super.select()
        createCustomNavigation()
    }

    void saveText() {
        
        ProjectTrackingRecordType recordType
        Long typeId =  form.getLong('type')
        if (typeId) {
            recordType = getOption(Options.PROJECT_TRACKING_RECORD_TYPES, typeId)
            logger.debug("saveText: recordType fetched [id=${typeId}, found=${(recordType != null)}]")
        }
        
        Date startDate = form.getDate('startTime')
        Date endDate = form.getDate('endTime')
        Double duration = form.getDouble('duration')
        logger.debug("saveText: params fetched [startDate=${startDate}, endDate=${endDate}, duration=${duration}]")
        
        if (!startDate) {
            throw new ClientException(ErrorCode.DATE_MISSING)
        }
        
        if (projectTracking.type.timeExactly) {
            
            if (!endDate) {
                endDate = startDate
            }
            Integer startHours = form.getHours('startHours') 
            Integer startMinutes = form.getMinutes('startMinutes') 
            Integer endHours = form.getHours('endHours')
            Integer endMinutes = form.getMinutes('endMinutes')
            if (!startHours || !endHours) {
                throw new ClientException(ErrorCode.HOURS_INVALID)
            }
            if (startMinutes == null) {
                startMinutes = 0
            }
            if (endMinutes == null) {
                endMinutes = 0
            }
            startDate = DateUtil.createDate(startDate, startHours, startMinutes)
            endDate = DateUtil.createDate(endDate, endHours, endMinutes)
            int tm = DateUtil.getDurationHours(startDate, endDate)
            if (tm <= 0) {
                throw new ClientException(ErrorCode.INVALID_DURATION)
            }
        }
        
        bean = projectTrackingManager.updateTrackingData(
            domainUser, 
            projectTracking, 
            selectedRecord,
            recordType, 
            form.getString('name'),
            form.getString('description'),
            form.getString('internalNote'),
            startDate, 
            endDate, 
            duration)
        list = projectTrackingManager.findAll(businessCase)
        disableTextEditMode()
        createCustomNavigation()
    }
    
    void release() throws ClientException {
        if (bean) {
            projectTrackingManager.close(domainUser, projectTracking)
            list = projectTrackingManager.findAll(businessCase)
            bean = list.find { it.id == bean.id }
            createCustomNavigation()
        }
    }

    DocumentData getPdf() throws ClientException {
        Long id = form.getLong('id')
        if (id) {
            ProjectTracking pt = list.find { it.id == id }
            if (pt) { 
                return projectTrackingManager.getPdf(domainUser, pt)
            }
        }
        return projectTrackingManager.getPdf(domainUser, projectTracking)
    }

    ProjectTracking getProjectTracking() {
        bean
    }
    
    @Override
    void createCustomNavigation() {

        if (textEditMode) {
            
            nav.beanListNavigation = [disableTextEditLink, homeLink]
            
        } else if (projectTracking?.closed) {
            
            nav.beanListNavigation = [selectExitLink, printLink, homeLink]
            
        } else if (projectTracking?.records) {
        
            nav.beanListNavigation = [
                selectExitLink,
                printLink,
                closeTrackingLink,
                editLink,
                homeLink
            ]
        }
    }
    
    /**
     * Provides disableTextEdit link
     * @return disableTextEdit
     */
    protected MenuItem getDisableTextEditLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/disableTextEditMode", icon: 'backIcon', title: 'break')
    }

    /**
     * Provides release tracking link
     * @return closeTrackingLink
     */
    protected MenuItem getCloseTrackingLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/release", icon: 'enabledIcon', title: 'recordRelease', confirmLink: true)
    }

    protected ProjectTrackingManager getProjectTrackingManager() {
        getService(ProjectTrackingManager.class.getName())
    }
}
