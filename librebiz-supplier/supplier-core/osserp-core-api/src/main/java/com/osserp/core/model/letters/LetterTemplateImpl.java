/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 9:06:05 PM 
 * 
 */
package com.osserp.core.model.letters;

import com.osserp.common.ClientException;
import com.osserp.core.Address;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterParagraph;
import com.osserp.core.dms.LetterTemplate;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.AddressImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LetterTemplateImpl extends AbstractLetterContent implements LetterTemplate {

    private boolean defaultTemplate;

    protected LetterTemplateImpl() {
        super();
    }

    protected LetterTemplateImpl(LetterTemplate other) {
        super(other);
    }

    protected LetterTemplateImpl(Long id, Employee user, LetterType type,
            String name, Long branchId) {
        super(id, (Long) null, user, type, name, branchId);
    }

    public LetterTemplateImpl(Long id, Employee user, LetterType type,
            String name, Long branchId, LetterTemplate template) {
        super(id, (Long) null, user, type, name, branchId);
        this.init(user, template);
    }

    public LetterTemplateImpl(Long id, Employee user, Letter letter) {
        super(id, (Long) null, user, letter.getType(), letter.getName(), letter.getBranchId());
        this.init(user, letter);
    }

    public void update(
            Employee user,
            String templateName,
            String subject,
            String salutation,
            String greetings,
            String language,
            Long signatureLeft,
            Long signatureRight,
            boolean ignoreSalutation,
            boolean ignoreGreetings,
            boolean ignoreHeader,
            boolean ignoreSubject,
            boolean embedded,
            boolean embeddedAbove,
            String embeddedSpaceAfter,
            String contentKeepTogether,
            boolean defaultTemplate) throws ClientException {
        super.update(user, subject, salutation, greetings, language,
                signatureLeft, signatureRight, ignoreSalutation,
                ignoreGreetings, ignoreHeader, ignoreSubject, embedded,
                embeddedAbove, embeddedSpaceAfter, contentKeepTogether);
        setName(templateName);
        this.defaultTemplate = defaultTemplate;
    }

    @Override
    public void addParagraph(Employee user) {
        LetterParagraph p = new LetterTemplateParagraphImpl(this, user);
        getParagraphs().add(p);
    }

    public void addAddress(Address address, String header, String name) {
        Address newAddress = new AddressImpl(address);
        if (header != null) {
            newAddress.setHeader(header);
        }
        if (name != null) {
            newAddress.setName(name);
        }
        setAddress(newAddress);
    }

    public boolean isClosed() {
        return false;
    }

    private void init(Employee user, LetterContent content) {
        if (content != null) {
            for (int i = 0, j = content.getParagraphs().size(); i < j; i++) {
                LetterParagraph next = content.getParagraphs().get(i);
                LetterParagraph p = new LetterTemplateParagraphImpl(this, user, next.getText());
                getParagraphs().add(p);
            }
            setGreetings(content.getGreetings());
        }
    }

    public boolean isDefaultTemplate() {
        return defaultTemplate;
    }

    public void setDefaultTemplate(boolean defaultTemplate) {
        this.defaultTemplate = defaultTemplate;
    }
}
