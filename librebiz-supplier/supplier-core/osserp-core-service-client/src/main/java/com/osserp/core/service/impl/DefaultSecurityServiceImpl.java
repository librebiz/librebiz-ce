/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2016 
 * 
 */
package com.osserp.core.service.impl;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.EntityRelation;
import com.osserp.common.PermissionError;
import com.osserp.common.service.AuthenticationProvider;
import com.osserp.common.service.impl.AbstractSecurityService;

import com.osserp.core.dao.SystemConfigs;

/**
 * 
 * Default security service implementation
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DefaultSecurityServiceImpl extends AbstractSecurityService {
    
    private String secureDocumentStoragePath;
    private SystemConfigs systemConfigs = null;

    protected DefaultSecurityServiceImpl() {
        super();
    }

    /**
     * Creates a new security service using a dedicated authentication provider
     * to store and access passwords.
     * @param configsDao
     * @param authenticationProvider provides password storage authenticator 
     */
    protected DefaultSecurityServiceImpl(
            SystemConfigs configsDao, 
            AuthenticationProvider authenticationProvider) {
        super(authenticationProvider);
        systemConfigs = configsDao;
    }
    
    public String getPassword(String contextName, String name) {
        if (name == null || contextName == null) {
            throw new PermissionError();
        }
        return getSystemKey(contextName, name);
    }

    public boolean isPasswordAvailable(String context, String name) {
        return (getSystemKey(context, name) != null);
    }

    public void writeFile(String filename, byte[] plainObject) throws ClientException {
        write(createFilename(filename), plainObject, getMasterPassword());
    }

    public byte[] readFile(String filename) throws ClientException {
        return read(createFilename(filename), getMasterPassword());
    }

    public void writeText(EntityRelation reference, String contextName, String text) {
        if (reference == null || reference.getId() == null 
                || contextName == null || text == null) {
            throw new PermissionError();
        }
        try {
            systemConfigs.saveKey(contextName, reference.getId().toString(), text);
        } catch (ClientException e) {
            throw new BackendException(e.getMessage(), e);
        }
    }

    public String readText(EntityRelation reference, String contextName) {
        if (reference == null || reference.getId() == null 
                || contextName == null) {
            throw new PermissionError();
        }
        return getSystemKey(contextName, reference.getId().toString());
    }

    protected String getSystemKey(String contextName, String name) {
        return systemConfigs.getKeyValue(contextName, name);
    }
    
    protected String createFilename(String name) {
        // TODO add checks and other required operations to make this more save
        return secureDocumentStoragePath + "/" + name;
    }

    protected String getSecureDocumentStoragePath() {
        return secureDocumentStoragePath;
    }

    protected void setSecureDocumentStoragePath(String secureDocumentStoragePath) {
        this.secureDocumentStoragePath = secureDocumentStoragePath;
    }
    
}
