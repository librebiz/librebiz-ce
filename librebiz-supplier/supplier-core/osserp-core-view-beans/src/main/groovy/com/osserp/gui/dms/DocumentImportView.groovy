/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 22, 2017 
 * 
 */
package com.osserp.gui.dms

import java.util.Map;

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.FileObject
import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsReference

import com.osserp.core.dms.CoreDocumentType

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class DocumentImportView extends DocumentListingView {
    private static Logger logger = LoggerFactory.getLogger(DocumentImportView.class.getName())

    DocumentImportView() {
        headerName  = 'documentImports'
    }
    
    protected DmsReference getDmsReference() {
        new DmsReference(CoreDocumentType.DOCUMENT_IMPORT, domainEmployee.id)
    }

    protected Long getTypeId() {
        CoreDocumentType.DOCUMENT_IMPORT
    }
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            uploadDir = System.getProperty('java.io.tmpdir')
        }
    }

    @Override
    void save() {
        List<FileObject> uploads = form.getUploads()
        logger.debug("save: invoked [uploads=${uploads?.size()}]")
        if (uploads) {
            uploads.each { FileObject file ->
                file.referenceFilename
                logger.debug("file received [referenceOnly=${file.referenceOnly}, name=${file.referenceFilename}, originalFilename=${file.fileName}]")
                dmsManager.createDocument(
                        domainUser, 
                        dmsReference, 
                        file, // upload 
                        null, // note 
                        null, // source 
                        null, // categoryId,
                        null, // validFrom,
                        null, // validTil
                        null) // messageId
            }
            reload()
        }
    }

    void delete() {
        DmsDocument doc = dmsManager.getDocument(getLong('id'))
        dmsManager.deleteDocument(doc)
        reload()
    }

    protected DmsManager getDmsManager() {
        getService(DmsManager.class.getName())
    }
}
