/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 20, 2017 
 * 
 */
package com.osserp.gui.projects

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.web.Form

import com.osserp.core.TargetDate
import com.osserp.core.projects.Project
import com.osserp.core.projects.ProjectManager
import com.osserp.core.projects.ProjectRequest
import com.osserp.core.requests.RequestManager

import com.osserp.gui.common.BusinessCaseAwareView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class InstallationDateView extends BusinessCaseAwareView {
    private static Logger logger = LoggerFactory.getLogger(InstallationDateView.class.getName())
    
    List<TargetDate> dates = []
    private ProjectRequest _projectRequest
    
    InstallationDateView() {
        super()
        providesEditMode()
        enablePopupView()
        businessCaseRequired = true
    }
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        
        if (forwardRequest) {
            ProjectRequest req = projectRequest
            dates = req.getInstallationDates()
        }
    }
    
    @Override
    void save() {
        Date date = form.getDate('date')
        projectRequest.addInstallationDate(user.getId(), date)
        requestManager.update(projectRequest)
        
        if (businessCase instanceof Project) {
            Integer duration = form.getInteger('duration')
            if (duration) {
                Project project = (Project) businessCase
                projectManager.updateInstallationDate(project, date, duration)
                logger.debug("save: done [sales=${project.getId()}, date=${date}, duration=${duration}")
            }
        }
        if (env.businessCaseView) {
            env.businessCaseView.reload()
        }
        disableEditMode()
    }

    private ProjectRequest getProjectRequest() {
        if (!_projectRequest) {
            if (request instanceof ProjectRequest) {
                _projectRequest = (ProjectRequest) request
            } else {
                throw new ClientException(ErrorCode.DELIVERY_DATE_NOT_SUPPPORTED)
            }
        }
        return _projectRequest
    }

    protected ProjectManager getProjectManager() {
        getService(ProjectManager.class.getName())
    }
}
