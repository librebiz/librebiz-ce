/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 8:56:27 PM 
 * 
 */
package com.osserp.core.dms;

import java.util.List;
import java.util.Locale;

import com.osserp.common.ClientException;
import com.osserp.core.Address;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface LetterContent extends LetterComponent {

    /**
     * Provides the address
     * @return address
     */
    Address getAddress();

    /**
     * Provides the branch id
     * @return branchId
     */
    Long getBranchId();

    /**
     * Changes the branch id
     * @param branchId
     */
    void setBranchId(Long branchId);

    /**
     * Provides the type of the letter
     * @return type
     */
    LetterType getType();

    /**
     * Provides the subject of the letter
     * @return subject
     */
    String getSubject();

    void setSubject(String subject);

    /**
     * Provides the salutation of the letter
     * @return salutation
     */
    String getSalutation();

    void setSalutation(String salutation);

    /**
     * Provides the employee id for left signature
     * @return signature left or null if not set
     */
    Long getSignatureLeft();

    void setSignatureLeft(Long signatureLeft);

    /**
     * Provides the employee id for right signature
     * @return signature right or null if not set
     */
    Long getSignatureRight();

    void setSignatureRight(Long signatureRight);

    /**
     * Provides the language of the letter
     * @return language
     */
    String getLanguage();

    void setLanguage(String language);

    /**
     * Provides the locale of the letter
     * @return locale
     */
    Locale getLocale();

    /**
     * Updates a paragraph content by id
     * @param user
     * @param paragraphId
     * @param content
     * @throws ClientException if validation failed
     */
    void update(Employee user, Long paragraphId, String content)
            throws ClientException;

    /**
     * Adds a new and empty paragraph
     * @param user adding new paragraph
     */
    void addParagraph(Employee user);

    /**
     * Moves paragraph list down
     * @param id
     */
    void moveParagraphDown(Long id);

    /**
     * Moves paragraph list up
     * @param id
     */
    void moveParagraphUp(Long id);

    /**
     * Provides the paragraphs (the text of the letter)
     * @return paragraphs
     */
    List<LetterParagraph> getParagraphs();

    /**
     * Indicates that content is empty (e.g. no paragraph with content exists)
     * @return contentEmpty
     */
    boolean isContentEmpty();

    /**
     * Provides replacement parameters for letter content
     * @return parameters
     */
    List<LetterParameter> getParameters();

    /**
     * Indicates that letter is closed
     * @return closed
     */
    boolean isClosed();
}
