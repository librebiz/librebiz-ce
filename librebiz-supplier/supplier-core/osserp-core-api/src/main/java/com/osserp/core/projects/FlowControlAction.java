/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 2, 2004 
 * 
 */
package com.osserp.core.projects;

import com.osserp.core.FcsAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FlowControlAction extends FcsAction {

    /**
     * Returns the typeId
     * @return typeId.
     */
    Long getTypeId();

    /**
     * Returns the orderId
     * @return orderId.
     */
    int getOrderId();

    /**
     * Sets the orderId
     * @param orderId
     */
    void setOrderId(int orderId);

    /**
     * Indicates that this action starts up the plant
     * @return isStartingUp
     */
    boolean isStartingUp();

    /**
     * Sets that this action starts up the plant
     * @param startingUp
     */
    void setStartingUp(boolean startingUp);

    /**
     * Indicates that this action indicates that manager was selected
     * @return isManagerSelecting
     */
    boolean isManagerSelecting();

    /**
     * Sets that this action indicates that manager was selected
     * @param managerSelecting
     */
    void setManagerSelecting(boolean managerSelecting);

    /**
     * Indicates that action is initializing the sales process
     * @return initializing
     */
    boolean isInitializing();

    /**
     * Indicates that action is confirming a sales (via customer confirmation)
     * @param true if so
     */
    boolean isConfirming();

    /**
     * Indicates that action is releasing a sales
     * @param true if so
     */
    boolean isReleaseing();

    /**
     * Indicates that action is verifying a sales
     * @return verifying
     */
    boolean isVerifying();

    /**
     * Indicates that action requests for downpayment
     * @return downpaymentRequest
     */
    boolean isDownpaymentRequest();

    /**
     * Indicates that action is a response to a downpayment (e.g. customer payment)
     * @return downpaymentResponse
     */
    boolean isDownpaymentResponse();

    /**
     * Indicates that action requests for delivery invoice
     * @return deliveryInvoiceRequest
     */
    boolean isDeliveryInvoiceRequest();

    /**
     * Indicates that action is a response to a delivery invoice (e.g. customer payment)
     * @return deliveryInvoiceResponse
     */
    boolean isDeliveryInvoiceResponse();

    /**
     * Indicates that action requests for invoice
     * @return invoiceRequest
     */
    boolean isInvoiceRequest();

    /**
     * Indicates that action is a response to an invoice (e.g. customer payment)
     * @return invoiceResponse
     */
    boolean isInvoiceResponse();

    /**
     * Indicates that action enables delivery (e.g. all contract details fixed, downpayments paid, etc.)
     * @return enablingDelivery
     */
    boolean isEnablingDelivery();

    /**
     * Indicates that action closes delivery
     * @return closingDelivery
     */
    boolean isClosingDelivery();

    /**
     * Indicates that action closes an underlying service contract. Closing service does not necessarily mean closing the businessCase.
     * @return closingService
     */
    boolean isClosingService();

    /**
     * Indicates that action is based on a partial delivery
     * @return partialDelivery
     */
    boolean isPartialDelivery();

    /**
     * Indicates that action is a partial payement
     * @return partialPayment
     */
    boolean isPartialPayment();
}
