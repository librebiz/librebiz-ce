<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" scope="page" value="${sessionScope.employeePopupView}"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="${view.headerName}"/>
		<c:if test="${!empty view && !empty view.bean}">
						: <o:out value="${view.bean.firstName}"/> <o:out value="${view.bean.lastName}"/> [<o:out value="${view.bean.id}"/>]
		</c:if>
	</div>
</div>
<div class="modalBoxData">
	<div>
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<c:choose>
					<c:when test="${empty view || empty view.bean}">
						<div class="errormessage">
							<fmt:message key="noEmployeeInformationsAvailable"/>
						</div>
					</c:when>
					<c:otherwise>
						<c:set var="bean" scope="page" value="${view.bean}"/>
						<table class="valueTable">
							<tbody>
								<tr>
									<td><fmt:message key="street"/></td>
									<td><o:out value="${bean.address.street}"/></td>
								</tr>
								<tr>
									<td><fmt:message key="city"/></td>
									<td><o:out value="${bean.address.zipcode}"/> <o:out value="${bean.address.city}"/></td>
								</tr>
                                <c:if test="${!empty bean.email}">
								    <tr>
									   <td><fmt:message key="email"/></td>
									   <td><o:email value="${bean.email}" /></td>
								    </tr>
                                </c:if>
								<c:if test="${!empty bean.internalPhone}">
									<tr>
										<td><fmt:message key="office"/></td>
										<td><oc:phone value="${bean.internalPhone}"/></td>
									</tr>
									<tr>
										<td><fmt:message key="directDial"/></td>
										<td><o:out value="${bean.internalPhone.internal}"/></td>
									</tr>
								</c:if>
                                <c:if test="${!empty bean.phone.number}">
								    <tr>
								        <td><fmt:message key="private"/></td>
							     		<td><oc:phone value="${bean.phone}"/></td>
					       			</tr>
                                </c:if>
                                <c:if test="${!empty bean.mobile.number}">
				    				<tr>
			     						<td><fmt:message key="mobile"/></td>
	       								<td><oc:phone value="${bean.mobile}"/></td>
    								</tr>
                                </c:if>
								<tr>
									<td><fmt:message key="branch"/></td>
									<td><o:out value="${bean.branch.name}"/></td>
								</tr>
							</tbody>
						</table>
					</c:otherwise>
				</c:choose>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>