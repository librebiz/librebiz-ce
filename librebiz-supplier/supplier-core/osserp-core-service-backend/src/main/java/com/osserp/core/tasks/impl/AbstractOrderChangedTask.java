/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.tasks.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.service.impl.AbstractObjectMessageHandler;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.service.events.EventTaskTicketCreator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOrderChangedTask extends AbstractObjectMessageHandler {
    private static Logger log = LoggerFactory.getLogger(AbstractOrderChangedTask.class.getName());
    private EventTaskTicketCreator events = null;

    /**
     * Creates a new order changed task
     * @param templateManager
     */
    protected AbstractOrderChangedTask(EventTaskTicketCreator events) {
        super();
        this.events = events;
    }

    @Override
    protected void execute(Object object, byte[] file) {
        if (object instanceof OrderChangedMessage) {
            try {
                executeOrderChangedEvent((OrderChangedMessage) object);
            } catch (Exception e) {
                log.error("execute() failed [message=" + e.getMessage()
                        + ", exceptionClass=" + e.getClass().getName()
                        + ", message=" + object.toString()
                        + "]");
            }
        } else if (object == null) {
            log.warn("execute() failed by null message!");
        } else {
            log.warn("execute() failed by unknown message type [expected="
                    + OrderChangedMessage.class.getName()
                    + ", receipt=" + object.getClass().getName()
                    + "]");

        }
    }

    protected abstract void executeOrderChangedEvent(OrderChangedMessage message) throws Exception;

    protected final void createEvents(
            OrderChangedMessage ocm,
            String description,
            Long branchId,
            Long managerId,
            Long salesId) {
        if (log.isDebugEnabled()) {
            log.debug("createEvents() invoked [eventName=" + ocm.getEventName()
                    + ", order=" + (ocm.getOrder() == null ? "null" : ocm.getOrder().getId())
                    + ", deliveryChangedInfosCount=" + ocm.getDeliveryDateChangedInfos().size()
                    + ", itemChangedInfosCount=" + ocm.getItemChangedInfos().size()
                    + "]");
        }
        List<DeliveryDateChangedInfo> deliveryChangedinfos = getDeliveryChangedInfos(ocm);
        if (!deliveryChangedinfos.isEmpty()) {
            events.createTickets(
                    ocm.getEventName(),
                    ocm.getEventReference(),
                    description,
                    branchId,
                    managerId,
                    salesId,
                    ocm.getCreatedBy(),
                    createDeliveryChangedMessage(deliveryChangedinfos),
                    null);
        }
        List<ItemChangedInfo> itemChangedinfos = getItemChangedInfos(ocm);
        if (!itemChangedinfos.isEmpty()) {
            events.createTickets(
                    ocm.getEventName(),
                    ocm.getEventReference(),
                    description,
                    branchId,
                    managerId,
                    salesId,
                    ocm.getCreatedBy(),
                    createItemChangedMessage(itemChangedinfos),
                    null);
        }
        if (deliveryChangedinfos.isEmpty() && itemChangedinfos.isEmpty()) {
            events.createTickets(
                    ocm.getEventName(),
                    ocm.getEventReference(),
                    description,
                    branchId,
                    managerId,
                    salesId,
                    ocm.getCreatedBy() != null ? ocm.getCreatedBy() : (ocm.getUser() != null ? ocm.getUser().getId() : Constants.SYSTEM_EMPLOYEE),
                    ocm.getMessage(),
                    null);
        }
    }

    private String createDeliveryChangedMessage(List<DeliveryDateChangedInfo> infos) {
        StringBuilder buffer = new StringBuilder();
        String deliveryNote = null;
        for (int i = 0, j = infos.size(); i < j; i++) {
            DeliveryDateChangedInfo info = infos.get(i);
            if ((i == 0 && info.getNote() != null && info.getNote().length() > 0)
                    || (info.getNote() != null && (!info.getNote().equals(deliveryNote) || deliveryNote == null))) {
                deliveryNote = info.getNote();
                buffer.append(deliveryNote).append(":\n");
            }
            buffer
                    .append(info.getPreviousDate() == null ? "None" : DateFormatter.getDate(info.getPreviousDate()))
                    .append(" => ")
                    .append(DateFormatter.getDate(info.getNewDate()))
                    .append(" : ")
                    .append(info.getProduct().getProductId())
                    .append(" - ")
                    .append(NumberFormatter.getIntValue(info.getQuantity()))
                    .append(" x ")
                    .append(info.getProduct().getName());
            if (i < j - 1) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

    private String createItemChangedMessage(List<ItemChangedInfo> infos) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0, j = infos.size(); i < j; i++) {
            ItemChangedInfo info = infos.get(i);
            buffer
                    .append(NumberFormatter.getValue(info.getPreviousQuantity(), NumberFormatter.DECIMAL))
                    .append(" => ")
                    .append(NumberFormatter.getValue(info.getNewQuantity(), NumberFormatter.DECIMAL))
                    .append(" : ")
                    .append(info.getProduct().getProductId())
                    .append(" - ")
                    .append(info.getProduct().getName());
            if (i < j - 1) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

    private List<DeliveryDateChangedInfo> getDeliveryChangedInfos(OrderChangedMessage message) {
        List<DeliveryDateChangedInfo> infos = message.getDeliveryDateChangedInfos();
        List<DeliveryDateChangedInfo> result = new java.util.ArrayList<DeliveryDateChangedInfo>();
        for (int i = 0, j = infos.size(); i < j; i++) {
            DeliveryDateChangedInfo info = infos.get(i);
            if (info.getProduct().isAffectsStock()) {
                result.add(info);
            }
        }
        return result;
    }

    private List<ItemChangedInfo> getItemChangedInfos(OrderChangedMessage message) {
        List<ItemChangedInfo> infos = message.getItemChangedInfos();
        List<ItemChangedInfo> result = new java.util.ArrayList<ItemChangedInfo>();
        for (int i = 0, j = infos.size(); i < j; i++) {
            ItemChangedInfo info = infos.get(i);
            if (info.getProduct().isAffectsStock()) {
                result.add(info);
            }
        }
        return result;
    }
}
