/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 21, 2006 6:11:40 PM 
 * 
 */
package com.osserp.gui.client.contacts;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.web.ViewName;

import com.osserp.core.BankAccount;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("contactView")
public interface SupplierView extends PaymentAwareContactView, RelatedContactView {

    /**
     * Provides current selected supplier
     * @return supplier
     */
    public Supplier getSupplier();

    /**
     * Changes the activation status of current installer
     */
    public void changeStatus();

    /**
     * Indivcates that this view is in group selection mode
     * @return true if so
     */
    public boolean isGroupSelectionMode();

    /**
     * Enables/disables group selection mode
     * @param groupSelectionMode
     */
    public void setGroupSelectionMode(boolean groupSelectionMode);

    /**
     * Provides all groups not already assigned to current installer
     * @return available groups
     */
    public List getAvailableGroups();

    /**
     * Adds a new group to current installer
     * @param group id
     */
    public void addGroup(Long id);

    /**
     * Removes a group from current installer
     * @param group id
     */
    public void removeGroup(Long id);

    /**
     * Toggles direct invoice booking enabled flag
     */
    public void changeDirectInvoiceBooking();

    /**
     * Sets the supplier type 
     * @param id of the supplier
     */
    public void selectSupplierType(Long id);

    /**
     * Assigns the id of the bank account if supplier provides bank accounts per supplierType setting 
     * @param id or null to disable account selection mode
     */
    public void assignBankAccount(Long id);

    /**
     * Provides assigned bank accounts or assignable accounts if view is in
     * bankAccountSelectionMode
     * @return bankAccounts or empty list if non assigned.
     */
    public List<BankAccount> getBankAccounts();
    
    /**
     * Indicates if bank account selection mode is enabled and assignable 
     * bank accounts available. 
     * @return true if account selection mode enabled
     */
    public boolean isBankAccountSelectionMode();
    
    /**
     * Loads assignable bankAccounts and enables selection mode 
     * @throws ClientException if no assignable bank accounts found.
     */
    public void enableBankAccountSelection() throws ClientException;
}
