/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Nov-2006 08:38:33 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.core.model.BusinessCaseRelationStatVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessCaseRelationStatRowMapper implements RowMapper {
    private static final String GET_VALUES =
            "select " +
                    "id," +
                    "lastname," +
                    "firstname," +
                    "branch," +
                    "active," +
                    "cap_unstopped," +
                    "cap_open," +
                    "cap_closed," +
                    "cap_lost," +
                    "cnt_open," +
                    "cnt_unstopped," +
                    "cnt_closed," +
                    "cnt_lost";

    public static final String SALE_SQL =
            GET_VALUES + " from get_salespersons_with_capacity()";
    public static final String TEC_SQL =
            GET_VALUES + " from get_technicians_with_capacity()";

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new BusinessCaseRelationStatVO(
                Long.valueOf(rs.getLong(1)),
                rs.getString(2),
                rs.getString(3),
                Long.valueOf(rs.getLong(4)),
                rs.getBoolean(5),
                rs.getDouble(6),
                rs.getDouble(7),
                rs.getDouble(8),
                rs.getDouble(9),
                rs.getInt(10),
                rs.getInt(11),
                rs.getInt(12),
                rs.getInt(13));
    }

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new BusinessCaseRelationStatRowMapper());
    }

}
