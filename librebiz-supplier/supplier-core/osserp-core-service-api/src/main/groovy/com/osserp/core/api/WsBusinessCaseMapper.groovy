/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16.12.2012 
 * 
 */
package com.osserp.core.api

import com.osserp.api.WsBusinessCase
import com.osserp.api.WsBusinessCaseItem
import com.osserp.api.WsContact

import com.osserp.common.Constants
import com.osserp.common.OptionsCache

import com.osserp.core.BusinessCase
import com.osserp.core.Item
import com.osserp.core.finance.Record
import com.osserp.core.finance.RecordSearch

/**
 * WsBusinessCaseMapper maps core business cases to ws business cases
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class WsBusinessCaseMapper {
    
    private OptionsCache options
    private RecordSearch recordSearch
    private WsContactMapper contactMapper
    private WsProductMapper productMapper
    
    
    protected WsBusinessCaseMapper(OptionsCache optionsCache, RecordSearch recordSearchService) {
        options = optionsCache
        recordSearch = recordSearchService
        contactMapper = new WsContactMapper()
        productMapper = new WsProductMapper(optionsCache)
    }

    WsBusinessCase map(BusinessCase businessCase) {
        WsContact contactObj = contactMapper.map(businessCase.customer)
        String businessCaseContext = 'request'
        if (businessCase.salesContext) {
            businessCaseContext = 'sales'
        } else {
            contactObj.status = WsContactMapper.DEFAULT_STATUS
        }
        if (businessCase.getCustomer().isReseller()) {
            contactObj.status = 'reseller'
        }
        Record record = recordSearch.findSalesRecord(businessCase)
        List<WsBusinessCaseItem> items = fetchItems(record)
        return new WsBusinessCase(
            primaryKey: businessCase.primaryKey,
            customerId: businessCase.customer.id,
            branchId: businessCase.branch?.id,
            branchKey: businessCase.branch?.shortkey,
            businessType: businessCase.type?.id,
            businessTypeName: businessCase.type?.key,
            context: businessCaseContext,
            contact: contactObj,
            name: businessCase.name,
            requestDate: businessCase.created,
            street: businessCase.address?.street,
            streetAddon: businessCase.address?.streetAddon,
            zipcode: businessCase.address?.zipcode,
            city: businessCase.address?.city,
            //federalState: businessCase.address?.street,
            //country: getOption('countryNames', businessCase.address.country),
            countryId: businessCase.address?.country,
            currency: Constants.getDefaultCurrency(record?.currency),
            items: items
        )
    }
    
    private List<WsBusinessCaseItem> fetchItems(Record record) {
        List<WsBusinessCaseItem> result = []
        if (record) {
            record.items.each {
                result << createItem(it)
            }
        }
        return result
    }
    
    private WsBusinessCaseItem createItem(Item item) {
        return new WsBusinessCaseItem(
            product: productMapper.map(item.product),
            quantity: item.quantity,
            price: item.price,
            note: item.note,
            externalKey: item.externalId
        )
    }
}
