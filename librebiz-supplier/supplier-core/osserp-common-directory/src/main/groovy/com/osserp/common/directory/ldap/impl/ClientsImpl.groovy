/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.apache.directory.groovyldap.LDAP
import org.apache.directory.groovyldap.SearchScope

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.StructuralSchema
import com.osserp.common.directory.ldap.dao.Clients
import com.osserp.common.util.NumberUtil
import com.osserp.common.util.SecurityUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ClientsImpl extends AbstractLdapDao implements Clients {
    private static Logger logger = LoggerFactory.getLogger(ClientsImpl.class.getName())

    StructuralSchema schema

    protected ClientsImpl() {
        super()
    }

    ClientsImpl(LdapConfig configInstance) {
        super(configInstance)
        schema = new ClientsSchema(configInstance)
    }

    List<Map> findAll() {
        LDAP ldap = createConnection()
        ldap.search(schema.query, createBaseDn(null), SearchScope.SUB)
    }

    Map find(String name) {
        LDAP ldap = createConnection()
        def dn = createBaseDn(name)
        if (ldap.exists(dn)) {
            return ldap.read(dn)
        }
        return [:]
    }

    Map create(String name, Long customerId, String description, String mailaddress, boolean mailenabled) {
        Map client = find(name)
        if (!client) {
            def dn = createBaseDn(name)
            def atts = [
                cn: name,
                gidnumber: createGidNumber(),
                osserpcustomer: createCustomerId(customerId),
                description: description,
                mail: mailaddress,
                mailenabled: mailenabled
            ]
            LDAP ldap = createConnection()
            ldap.add(dn, schema.create(atts))
            client = ldap.read(dn)
        }
        return client
    }

    Map changePassword(Map client, String password) {
        if (client && password) {
            LDAP ldap = createConnection()
            def userPassword = SecurityUtil.createPassword(password).getBytes()
            def newValues = [ userpassword: userPassword ]
            ldap.modify(client.dn, 'REPLACE', newValues)
        }
        find(client.cn)
    }

    void delete(Map client) {
        LDAP ldap = createConnection()
        if (client?.dn && ldap.exists(client.dn)) {
            try {
                ldap.delete(client.dn)
            } catch (Throwable t) {
                println "delete: failed [message=${t.message}]"
            }
        }
    }

    private String createGidNumber() {
        Long clientStart = NumberUtil.createLong(config.clientIdStart)
        if (!clientStart) {
            clientStart = 1L
        }
        Long result = clientStart
        List<Map> existingClients = findAll()
        existingClients.each {
            Long existingId = NumberUtil.createLong(it.gidnumber)
            if (existingId && existingId > result) {
                result = existingId
            }
        }
        if (result > clientStart) {
            return (result + 1L).toString()
        }
        result.toString()
    }

    private String createCustomerId(Long customerId) {
        if (customerId) {
            return customerId.toString()
        }
        Long clientStart = 1L
        Long result = clientStart
        List<Map> existingClients = findAll()
        existingClients.each {
            Long existingId = NumberUtil.createLong(it.osserpcustomer)
            if (existingId && existingId > result) {
                result = existingId
            }
        }
        if (result > clientStart) {
            return (result + 1L).toString()
        }
        result.toString()
    }
}
