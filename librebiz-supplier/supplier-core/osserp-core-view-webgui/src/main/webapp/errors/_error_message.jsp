<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:if test="${!empty sessionScope.errors}">
	<c:set var="sessionErrorsFound" value="true"/>
	<o:logger write="current page has errors" level="debug"/>
	<c:choose>
		<c:when test="${!empty sessionScope.errorId}">
			<span class="error"> - </span><span class="error"><o:out value="${sessionScope.errors}"/> / <o:out value="${sessionScope.errorId}"/></span>
			<c:remove var="errorId" scope="session"/>
		</c:when>
		<c:when test="${!empty sessionScope.errorDetails}">
			<o:logger write="current page has errorDetails" level="debug"/>
			<span class="error"> - </span><span class="error"><o:out value="${sessionScope.errors}"/> : <o:out value="${sessionScope.errorDetails}"/></span>
			<c:remove var="errorDetails" scope="session"/>
		</c:when>
		<c:otherwise>
		    <span class="error"> - </span><span class="error"><o:out value="${sessionScope.errors}"/></span>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${empty sessionErrorsFound && !empty sessionScope.error}">
	<c:set var="sessionErrorsFound" value="true"/>
	<span class="error"> - </span><span class="error"><o:out value="${sessionScope.error}"/></span>
</c:if>
<c:if test="${empty sessionErrorsFound && !empty requestScope.error}">
	<span class="error"> - </span><span class="error"><o:out value="${requestScope.error}"/></span>
</c:if>
<o:removeErrors/>
<c:remove var="errorId" scope="session"/><c:remove var="errorDetails" scope="session"/>
