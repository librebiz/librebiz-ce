/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 30, 2008 9:07:13 AM 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;

import com.osserp.gui.client.records.CorrectionAwareRecordView;
import com.osserp.gui.client.records.RecordView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractCorrectionAwareSalesAction extends AbstractSalesRecordAction {
    private static Logger log = LoggerFactory.getLogger(AbstractCorrectionAwareSalesAction.class.getName());

    /**
     * Enables correction create mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableCorrectionCreateMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableCorrectionCreateMode() request from " + getUser(session).getId());
        }
        CorrectionAwareRecordView view = getCorrectionAwareView(session);
        view.setCorrectionCreateMode(true);
        if (log.isDebugEnabled()) {
            log.debug("enableCorrectionCreateMode() done [value=" + view.isCorrectionCreateMode() + "]");
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables correction edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableCorrectionEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableCorrectionEditMode() request from " + getUser(session).getId());
        }
        CorrectionAwareRecordView view = getCorrectionAwareView(session);
        view.setCorrectionEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables correction edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableCorrectionEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableCorrectionEditMode() request from " + getUser(session).getId());
        }
        CorrectionAwareRecordView view = getCorrectionAwareView(session);
        view.setCorrectionEditMode(false);
        view.setCorrectionCreateMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Prints a correction
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward printCorrection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("printCorrection() request from " + getUser(session).getId());
        }
        CorrectionAwareRecordView view = getCorrectionAwareView(session);
        DocumentData pdf = view.printCorrection(RequestUtil.fetchId(request));
        return forwardPdfOutput(mapping, request, pdf);
    }

    /**
     * Releases a correction
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward releaseCorrection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("releaseCorrection() request from " + getUser(session).getId());
        }
        CorrectionAwareRecordView view = getCorrectionAwareView(session);
        view.releaseCorrection();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Saves a correction, e.g. creates in create mode, updates in edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward saveCorrection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("saveCorrection() request from " + getUser(session).getId());
        }
        CorrectionAwareRecordView view = getCorrectionAwareView(session);
        try {
            view.saveCorrection(new StrutsForm(form));
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    protected CorrectionAwareRecordView getCorrectionAwareView(HttpSession session) throws PermissionException {
        RecordView view = getRecordView(session);
        if (view instanceof CorrectionAwareRecordView) {
            return (CorrectionAwareRecordView) view;
        }
        throw new ActionException("CorrectionAwareRecordView expected, found " + view.getClass().getName());
    }
}
