/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 24, 2005 
 * 
 */
package com.osserp.common.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.codec.binary.Base64;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.binary.AES256BinaryEncryptor;
import org.jasypt.util.text.AES256TextEncryptor;
import org.jasypt.util.password.rfc2307.RFC2307SHAPasswordEncryptor;
import org.jasypt.util.password.rfc2307.RFC2307SSHAPasswordEncryptor;

import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionError;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SecurityUtil {
    private static Logger log = LoggerFactory.getLogger(SecurityUtil.class.getName());
    
    public static final String MD5 = "MD5";
    public static final String SHA = "SHA";
    public static final String SSHA = "SSHA";
    
    private static final String KEY_MD5 = "{MD5}";
    private static final String KEY_SHA = "{SHA}";
    private static final String KEY_SSHA = "{SSHA}";
    

    /**
     * Creates a new String, represents a randomized number with length of given digits
     * @param digits length of returned String
     * @return contains randomized number
     */
    public static String createNumber(int digits) {
        Random generator = new Random();
        StringBuffer buf = new StringBuffer(digits);
        for (int i = 0; i < digits; i++) {
            buf.append(Integer.toString(generator.nextInt(10)));
        }
        return buf.toString();
    }

    /**
     * Creates a new String, represents a randomized combination of characters with length of given digits
     * @param maxLen digits length of returned String
     * @param upperCase characters if set true
     * @return contains randomized string
     */
    public static String createString(int maxLen, boolean upperCase) {
        SecureRandom generator = new SecureRandom();
        StringBuffer buf = new StringBuffer(maxLen);
        for (int i = 0; i < maxLen; i++) {
            buf.append(Integer.toString(generator.nextInt(10 + 26), 10 + 26));
        }
        if (upperCase) {
            return buf.toString().toUpperCase();
        }
        return buf.toString();
    }

    public static String createPassword(String text) {
        return createLdapSSHA(text);
    }

    public static String createPassword(String text, String digest) {
        if (MD5.equalsIgnoreCase(digest)) {
            return createLdapMD5(text);
        }
        if (SHA.equalsIgnoreCase(digest)) {
            return createLdapSHA(text);
        }
        return createLdapSSHA(text);
    }
    
    public static boolean checkPassword(String password, String hash) {
        if (password != null && password.length() > 0 
                && hash != null && hash.length() > 0) {
            
            if (checkLdapMD5(password, hash)) {
                log.debug("checkPassword() test md5 ok");
                return true;
            }
            log.debug("checkPassword() test md5 failed...");
            
            if (checkLdapSHA(password, hash)) {
                log.debug("checkPassword() test sha ok");
                return true;
            }
            log.debug("checkPassword() test sha failed...");
            
            if (checkLdapSSHA(password, hash)) {
                log.debug("checkPassword() test ssha ok");
                return true;
            }
            log.debug("checkPassword() test ssha failed...");
        }
        return false;
    }
    
    public static boolean isPasswordHash(String password) {
        if (password == null) {
            throw new PermissionError(ErrorCode.PASSWORD_MISSING);
        }
        if (password.startsWith(KEY_MD5) || password.startsWith(KEY_SHA) 
                || password.startsWith(KEY_SSHA)) {
            return true;
        }
        return false;
    }
    
    private static boolean checkLdapMD5(String password, String hash) {
        if (password != null && password.length() > 0 
                && hash != null && hash.startsWith(KEY_MD5)
                && hash.equals(createLdapMD5(password))) {
            return true;
        }
        return false;
    }
    
    private static String createLdapSHA(String text) {
        RFC2307SHAPasswordEncryptor encryptor = new RFC2307SHAPasswordEncryptor();
        return encryptor.encryptPassword(text);
    }
    
    private static String createLdapSSHA(String text) {
        RFC2307SSHAPasswordEncryptor encryptor = new RFC2307SSHAPasswordEncryptor();
        return encryptor.encryptPassword(text);
    }
    
    private static boolean checkLdapSHA(String text, String hash) {
        RFC2307SHAPasswordEncryptor encryptor = new RFC2307SHAPasswordEncryptor();
        if (hash != null && hash.startsWith(KEY_SHA)) {
            return encryptor.checkPassword(text, hash);
        }
        log.debug("checkLdapSHA() no digest provided [hash=" + StringUtil.cut(hash, 7, true) + "]");
        return false;
    }
    
    private static boolean checkLdapSSHA(String text, String hash) {
        if (hash != null && hash.startsWith(KEY_SSHA)) {
            RFC2307SSHAPasswordEncryptor encryptor = new RFC2307SSHAPasswordEncryptor();
            return encryptor.checkPassword(text, hash);
        }
        log.debug("checkLdapSSHA() no digest provided [hash=" + StringUtil.cut(hash, 7, true) + "]");
        return false;
    }

    private static String createLdapMD5(String text) {
        if (text == null) {
            return null;
        }
        StringBuffer buffer = new StringBuffer(KEY_MD5);
        return buffer.append(encodeMD5(text)).toString();
    }

    private static String encodeMD5(String text) {
        MessageDigest md = null;
        byte[] encryptMsg = null;

        try {
            md = MessageDigest.getInstance(MD5); // getting a 'MD5-Instance'
            encryptMsg = md.digest(text.getBytes("UTF-8")); // solving the MD5-Hash
        } catch (NoSuchAlgorithmException e) {
            // should never happen on MD5
            System.out.println("No Such Algorithm Exception!");
        } catch (UnsupportedEncodingException e) {
            // should never happen on UTF-8
            System.out.println("No Such Encoding Exception!");
        }
        return Base64.encodeBase64String(encryptMsg);
    }

    public static String encrypt(String password, String plaintext) {
        AES256TextEncryptor textEncryptor = new AES256TextEncryptor();
        textEncryptor.setPassword(password);
        return textEncryptor.encrypt(plaintext);
    }

    public static String decrypt(String password, String encryptedtext) {
        AES256TextEncryptor textEncryptor = new AES256TextEncryptor();
        textEncryptor.setPassword(password);
        return textEncryptor.decrypt(encryptedtext);
    }

    public static byte[] encryptFile(String password, byte[] plainObject) {
        AES256BinaryEncryptor encryptor = new AES256BinaryEncryptor();
        encryptor.setPassword(password);
        return encryptor.encrypt(plainObject);
    }

    public static byte[] decryptFile(String password, byte[] encryptedObject) {
        try {
            AES256BinaryEncryptor encryptor = new AES256BinaryEncryptor();
            encryptor.setPassword(password);
            return encryptor.decrypt(encryptedObject);
        } catch (EncryptionOperationNotPossibleException e) {
            throw new PermissionError(ErrorCode.PASSWORD_INVALID);
        }
    }
}
