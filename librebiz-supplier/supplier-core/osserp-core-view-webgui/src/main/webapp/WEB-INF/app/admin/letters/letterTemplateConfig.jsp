<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="letterTemplateConfigView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
        <c:if test="${view.supportingTypeSelection and !empty view.selectedType}"> - <o:out value="${view.selectedType.name}" />
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="letterContent">
            <div class="row">
                <c:choose>
                    <c:when test="${empty view.selectedType}">
                        <c:import url="_letterTemplateConfigTypeList.jsp" />
                    </c:when>
                    <c:when test="${view.createMode}">
                        <c:import url="_letterTemplateConfigCreate.jsp" />
                    </c:when>
                    <c:when test="${view.editMode}">
                        <c:import url="_letterTemplateConfigEdit.jsp" />
                    </c:when>
                    <c:when test="${view.bean != null}">
                        <c:import url="_letterTemplateConfigDisplay.jsp" />
                    </c:when>
                    <c:otherwise>
                        <c:import url="_letterTemplateConfigList.jsp" />
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>