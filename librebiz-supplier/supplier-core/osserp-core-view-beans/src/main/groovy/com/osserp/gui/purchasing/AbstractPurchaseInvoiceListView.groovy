/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 28, 2016 
 * 
 */
package com.osserp.gui.purchasing

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.DateUtil

import com.osserp.core.finance.RecordDisplay
import com.osserp.core.finance.RecordDisplayItem
import com.osserp.core.purchasing.PurchaseInvoiceManager

import com.osserp.gui.records.AbstractRecordDisplayListView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class AbstractPurchaseInvoiceListView extends AbstractRecordDisplayListView {
    private static Logger logger = LoggerFactory.getLogger(AbstractPurchaseInvoiceListView.class.getName())
    
    boolean purchasePaymentWarnings = false
    
    protected AbstractPurchaseInvoiceListView() {
        super()
        enableAutoreload()
    }
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        boolean reload = reloadRequest
        if (forwardRequest || reload) {
            purchasePaymentWarnings = isSystemPropertyEnabled('purchasePaymentWarnings')
            reloadList()
            resetListCounters()
        } else if (reloadRequest) {
            reloadList()
        }
    }
    
    protected abstract List<RecordDisplay> fetchList(Date start, Date until);

    protected void reloadList() {
        long start = System.currentTimeMillis()
        logger.debug("reloadList: invoked [listStartDate=${listStartDate}, listStopDate=${listStopDate}, listSearchValue=${listSearchValue}]")
        Date until = (listStopDate != null) ? DateUtil.addDay(listStopDate) : new Date(System.currentTimeMillis())
        List<RecordDisplay> tmplist = fetchList(listStartDate, until)
        List<RecordDisplay> resultingList = new LinkedList<RecordDisplay>()
        boolean nameSearch = (listSearchValue != null) ? true : false
        if (nameSearch) {
            for (Iterator<RecordDisplay> recordIterator = tmplist.iterator(); recordIterator.hasNext();) {
                RecordDisplay record = recordIterator.next()
                if (record.getContactName() != null
                        && record.getContactName().toLowerCase().indexOf(listSearchValue.toLowerCase()) != -1) {
                    resultingList << record
                } else {
                    for (Iterator<RecordDisplayItem> itemIterator = record.getItems().iterator(); itemIterator.hasNext();) {
                        RecordDisplayItem item = itemIterator.next()
                        if (item.getName() != null
                                && item.getName().toLowerCase().indexOf(listSearchValue.toLowerCase()) != -1) {
                            resultingList << record
                            break
                        }
                    }
                }
            }
            list = resultingList
        } else {
            list = tmplist
        }
        sort()
        logger.debug("reloadList: done [duration=${(System.currentTimeMillis() - start)}ms]")
    }

    protected PurchaseInvoiceManager getPurchaseInvoiceManager() {
        getService(PurchaseInvoiceManager.class.getName())
    }

}
