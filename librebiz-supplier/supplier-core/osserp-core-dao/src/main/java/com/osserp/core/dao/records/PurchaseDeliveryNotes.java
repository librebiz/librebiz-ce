/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 14, 2006 1:24:42 AM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Order;
import com.osserp.core.purchasing.PurchaseDeliveryNote;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PurchaseDeliveryNotes extends DeliveryNotes, PurchaseRecords {

    /**
     * Creates a new delivery note
     * @param user
     * @param order
     * @param type
     * @return new created delivery note
     * @throws ClientException if nothing to deliver found
     */
    DeliveryNote create(Employee user, Order order, DeliveryNoteType type) throws ClientException;

    /**
     * Provides open stock receipts (e.g. purchase delivery notes without invoice, status == 3)
     * @param stockId
     * @param productId
     * @return open delivery notes
     */
    List<DeliveryNote> getOpen(Long stockId, Long productId);

    /**
     * Provides current stock receipt for an product
     * @param productId
     * @return stock receipt quantity
     */
    Double getStockReceiptQuantity(Long productId);

    /**
     * Reopens a closed delivery note
     * @param user
     * @param note
     */
    void reopenClosed(Employee user, PurchaseDeliveryNote note);

}
