<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
	<table class="table table-striped">

		<c:choose>
			<c:when test="${view.fcsMode}">
				<thead>
					<tr>
						<th class="name" ><fmt:message key="fcsAction"/></th>
						<th class="name" ><fmt:message key="isLeadingToJobInformation"/></th>
						<th class="name" ><fmt:message key="receiver"/></th>
					</tr>
				</thead>

				<tbody>
				<c:forEach var="action" items="${view.list}">
					<tr>
						<td valign="top" ><o:out value="${action.sourceName}"/></td>
						<td class="actions"  valign="top"><v:link url="/admin/events/eventConfig/select" parameters="id=${action.id}"><o:out value="${action.targetName}"/></v:link></td>
						<td valign="top" >
							<c:choose>
								<c:when test="${action.recipientSales}"><fmt:message key="sales"/></c:when>
								<c:when test="${action.recipientManager}"><fmt:message key="projector"/></c:when>
								<c:otherwise><o:out value="${action.groupName}"/></c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<thead>
					<tr>
						<th class="name"><fmt:message key="priority"/></th>
						<th class="name"><fmt:message key="name"/></th>
						<th class="name"><fmt:message key="receiver"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="action" items="${view.list}">
						<tr>
							<td valign="top">
								<c:choose>
									<c:when test="${action.priority == 1}">
										<fmt:message key="high"/>
									</c:when>
									<c:when test="${action.priority == 2}">
										<fmt:message key="medium"/>
									</c:when>
									<c:otherwise>
										<fmt:message key="low"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td class="actions" valign="top"><v:link url="/admin/events/eventConfig/select" parameters="id=${action.id}"><o:out value="${action.name}"/></v:link></td>
							<td valign="top">
								<c:choose>
									<c:when test="${action.sendSales}"><fmt:message key="sales"/></c:when>
									<c:when test="${action.sendManager}"><fmt:message key="projector"/></c:when>
									<c:when test="${action.sendPool}"><o:out value="${action.pool.name}"/></c:when>
									<c:otherwise><fmt:message key="undefined"/></c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
                </tbody>
			</c:otherwise>
		</c:choose>
	</table>
</div>
