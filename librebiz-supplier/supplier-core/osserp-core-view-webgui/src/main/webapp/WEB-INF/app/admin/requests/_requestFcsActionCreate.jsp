<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>


<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="flowControlActionCreate" />
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <div class="form-body">

            <c:choose>
                <c:when test="${empty view.createModeContext}">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="type"><fmt:message key="type" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:link url="${view.baseLink}/setCreateModeContext?contextName=action">
                                    <fmt:message key="action" />
                                </v:link>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="alternateType"> </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:link url="${view.baseLink}/setCreateModeContext?contextName=cancellation">
                                    <fmt:message key="cancellation" />
                                </v:link>
                            </div>
                        </div>
                    </div>

                    <div class="row next">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v:submitExit url="${view.baseLink}/disableCreateMode" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </c:when>
                <c:otherwise>

                    <c:import url="${viewdir}/admin/fcs/_fcsConfigInput.jsp" />

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status"><fmt:message key="status" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <oc:select name="status" prompt="false" options="${view.freeStatusList}" />
                            </div>
                        </div>
                    </div>

                    <div class="row next">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v:submitExit url="${view.baseLink}/disableCreateMode" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <o:submit value="create" styleClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </c:otherwise>
            </c:choose>

        </div>
    </div>
</div>
