/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.tasks.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.impl.AbstractObjectMessageHandler;

import com.osserp.core.FcsAction;
import com.osserp.core.FcsItem;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.tasks.Task;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderReleasedTaskImpl extends AbstractObjectMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(SalesOrderReleasedTaskImpl.class.getName());

    private Projects projects = null;
    private FlowControlActions fcsActions = null;
    private SalesOrders salesOrders = null;

    /**
     * Creates a new SalesOrderReleasedTask
     * @param projects
     * @param fcsActions
     * @param salesOrders
     */
    protected SalesOrderReleasedTaskImpl(
            Projects projects,
            FlowControlActions fcsActions,
            SalesOrders salesOrders) {
        super();
        this.projects = projects;
        this.fcsActions = fcsActions;
        this.salesOrders = salesOrders;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.common.service.impl.AbstractObjectMessageHandler#execute(java.lang.Object, byte[])
     */
    @Override
    protected void execute(Object object, byte[] file) {
        if (object instanceof Long) {
            Long orderId = (Long) object;
            SalesOrder order = (SalesOrder) salesOrders.load(orderId);
            if (order.getBusinessCaseId() != null) {

                Sales sales = projects.load(order.getBusinessCaseId());
                if (isNotSet(order.getShippingId()) && isSet(sales.getShippingId())) {
                    sales.getRequest().setShippingId(null);
                    projects.save(sales);
                } else if (isSet(order.getShippingId())) {
                    sales.getRequest().setShippingId(order.getShippingId());
                    projects.save(sales);
                }

                if (order.getOpenDeliveries().isEmpty()) {
                    boolean partialDeliveryExists = false;
                    boolean finalDeliveryExists = false;
                    Long lastPartialDeliveryId = null;
                    Date lastPartialDeliveryDate = null;
                    for (int i = 0, j = sales.getFlowControlSheet().size(); i < j; i++) {
                        FcsItem item = sales.getFlowControlSheet().get(i);
                        FlowControlAction action = (FlowControlAction) item.getAction();
                        if (action.isClosingDelivery()) {
                            finalDeliveryExists = true;
                        } else if (action.isPartialDelivery()) {
                            if (lastPartialDeliveryDate != null) {
                                if (lastPartialDeliveryDate.before(item.getCreated())) {
                                    lastPartialDeliveryId = item.getId();
                                    lastPartialDeliveryDate = item.getCreated();
                                }
                            } else {
                                lastPartialDeliveryId = item.getId();
                                lastPartialDeliveryDate = item.getCreated();
                            }
                            partialDeliveryExists = true;
                        }
                    }
                    if (partialDeliveryExists && !finalDeliveryExists) {
                        // Previous delivery caused partial delivery but open delivery list is empty after sales order release; 
                        // => partial delivery is final delivery now:
                        if (log.isDebugEnabled()) {
                            log.debug("execute() partial delivery fcs exists and no more items to deliver [sales=" + sales.getId()
                                    + ", order=" + order.getId()
                                    + "]");
                        }
                        FlowControlAction finalDeliveryAction = null;
                        List<FcsAction> actions = fcsActions.findByType(sales.getType().getId());
                        // fetch final delivery action
                        for (int i = 0, j = actions.size(); i < j; i++) {
                            FlowControlAction action = (FlowControlAction) actions.get(i);
                            if (action.isClosingDelivery()) {
                                finalDeliveryAction = action;
                                break;
                            }
                        }
                        if (finalDeliveryAction != null && lastPartialDeliveryId != null) {
                            for (int i = 0, j = sales.getFlowControlSheet().size(); i < j; i++) {
                                FcsItem item = sales.getFlowControlSheet().get(i);
                                if (item.getId().equals(lastPartialDeliveryId)) {
                                    item.changeAction(finalDeliveryAction);
                                    projects.save(item);
                                    if (log.isDebugEnabled()) {
                                        log.debug("execute() changed partial delivery fcs to final delivery [sales=" + sales.getId()
                                                + ", order=" + order.getId()
                                                + "]");
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
