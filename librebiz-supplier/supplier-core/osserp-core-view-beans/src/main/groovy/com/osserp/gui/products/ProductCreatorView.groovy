/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 3, 2014, Src. Mar 5, 2009 
 * 
 */
package com.osserp.gui.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.model.products.ProductClassificationConfigVO
import com.osserp.core.products.Manufacturer
import com.osserp.core.products.ManufacturerManager
import com.osserp.core.products.Product
import com.osserp.core.products.ProductClassificationConfig
import com.osserp.groovy.web.MenuItem

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ProductCreatorView extends AbstractProductView {
    private static Logger logger = LoggerFactory.getLogger(ProductCreatorView.class.getName())

    List<ProductClassificationConfig> productClassificationConfigs = []
    ProductClassificationConfig productClassificationConfig
    Product source
    Long suggestionId

    ProductCreatorView() {
        super()
        providesCustomNavigation()
        providesSetupMode()
        overrideExitTarget = true
        dependencies = ['productView', 'productClassificationConfig']
        headerName = 'product_create'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        productClassificationConfigs = productSearch.findClassifications()
        if (forwardRequest) {
            if (env.productView?.product) {
                source = env.productView.product
                productClassificationConfig = new ProductClassificationConfigVO(
                        source.getTypeDefault(), source.getGroup(), source.getCategory())
                form.params['productName'] = source.getName()
                form.params['productDescription'] = source.getDescription()
                form.params['productMatchcode'] = source.getMatchcode()
                form.params['manufacturer'] = source.getManufacturer()
                form.params['packagingUnit'] = source.getPackagingUnit()
                form.params['quantityUnit'] = source.getQuantityUnit()
            } else if (env.productClassificationConfig) {
                productClassificationConfig = env.productClassificationConfig
                form.params['packagingUnit'] = 1L
            }
            if (!form.params['quantityUnit']) {
                form.params['quantityUnit'] = fetchQuantityUnit()
            }
            if (productClassificationConfig) {
                suggestionId = productManager.createIdSuggestion(productClassificationConfig)
            } else {
                enableClassificationMode()
            }
            enableCreateMode()
        }
    }

    @Override
    void save() {
        Long manufacturerId = getLong('manufacturer')
        String newManufacturer = getString('newManufacturer')
        if (!manufacturerId && newManufacturer) {
            Manufacturer manufacturer = manufacturerManager.add(newManufacturer, productClassificationConfig?.group?.id)
            if (manufacturer) {
                manufacturerId = manufacturer.id
                form.params['manufacturer'] = manufacturerId
                form.params['newManufacturer'] = null
                logger.debug("save: added new manufacturer [manufacturerId=${manufacturer.id}, manufacturer=${manufacturer.name}]")
            }
        }
        bean = productManager.create(
                domainEmployee,
                getLong('productId'),
                productClassificationConfig,
                getString('productMatchcode'),
                getString('productName'),
                getString('productDescription'),
                fetchQuantityUnit(),
                getInteger('packagingUnit'),
                manufacturerId,
                null, // TODO add planningMode
                source)
        logger.debug("save: done [product=${bean?.productId}]")

        if (bean?.productId && source?.productId && bean?.productId != source?.productId
                && env.productView?.product?.productId == source?.productId
                && env.productView?.exitTarget) {
            env.productView.load(bean, null, "product${bean.productId}")
            addExitTarget("/products.do?method=redisplay")
        } else if (bean?.productId) {
            addExitTarget("/products.do?method=load&id=${bean.productId}")
        }
    }

    void updateClassification() {
        Long typeId = getLong('typeId')
        Long groupId = getLong('groupId')
        Long categoryId = getLong('categoryId')
        if (typeId && groupId) {
            productClassificationConfigs.each { ProductClassificationConfig cfg ->
                if (cfg.type?.id == typeId && cfg.group?.id == groupId) {
                    if ((cfg.category == null && categoryId == null)
                            || (cfg.category.id == categoryId)) {
                        productClassificationConfig = cfg
                        suggestionId = productManager.createIdSuggestion(productClassificationConfig)
                        disableClassificationMode()
                    }
                }
            }
        } else {
            productClassificationConfig = null
            suggestionId = null
            enableClassificationMode()
        }
    }

    boolean isClassificationMode() {
        env.classificationMode
    }

    void disableClassificationMode() {
        env.classificationMode = false
        headerName = 'product_create'
    }

    void enableClassificationMode() {
        env.classificationMode = true
        headerName = 'productClassificationSelection'
    }

    boolean isClassificationAssigned() {
        productClassificationConfig != null
    }

    protected ManufacturerManager getManufacturerManager() {
        getService(ManufacturerManager.class.getName())

    }

    @Override
    void createCustomNavigation() {
        if (classificationMode) {
            nav.createNavigation = [disableCreateLink, setupLink, homeLink]
        } else {
            nav.createNavigation = [disableCreateLink, homeLink]
        }
    }

    @Override
    MenuItem getSetupLink() {
        return navigationLink("/${context.name}/enableSetupMode", 'configureIcon', 'productClassificationEdit')
    }
}
