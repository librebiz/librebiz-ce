/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2007 9:50:13 AM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.web.Form;

import com.osserp.core.Options;
import com.osserp.core.contacts.ContactPaymentAgreement;
import com.osserp.core.contacts.PaymentAware;
import com.osserp.core.finance.PaymentCondition;

import com.osserp.gui.client.contacts.PaymentAwareContactView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPaymentAwareContactView extends AbstractRelatedContactView
        implements PaymentAwareContactView {
    private static Logger log = LoggerFactory.getLogger(AbstractPaymentAwareContactView.class.getName());
    private boolean paymentAgreementEditMode = false;

    protected AbstractPaymentAwareContactView() {
        super();
    }

    public boolean isPaymentAgreementEditMode() {
        return paymentAgreementEditMode;
    }

    public void setPaymentAgreementEditMode(boolean paymentAgreementEditMode) {
        disableAllModes();
        this.paymentAgreementEditMode = paymentAgreementEditMode;
    }

    public void updatePaymentAgreement(Form fh) throws ClientException {
        PaymentAware contact = fetchPaymentAware();
        ContactPaymentAgreement pa = contact.getPaymentAgreement();
        pa.setDownpaymentPercent(fh.getDouble("downpaymentPercent"));
        pa.setDownpaymentTargetId(fh.getLong("downpaymentTargetId"));
        pa.setDeliveryInvoicePercent(fh.getDouble("deliveryInvoicePercent"));
        pa.setDeliveryInvoiceTargetId(fh.getLong("deliveryInvoiceTargetId"));
        pa.setFinalInvoicePercent(fh.getDouble("finalInvoicePercent"));
        pa.setFinalInvoiceTargetId(fh.getLong("finalInvoiceTargetId"));
        pa.setHidePayments(fh.getBoolean("hidePayments"));
        pa.setTaxFree(fh.getBoolean("taxFree"));
        if (pa.isTaxFree()) {
            pa.setTaxFreeId(fh.getLong("taxFreeId"));
        } else {
            pa.setTaxFreeId(null);
        }
        pa.setNote(fh.getString("note"));
        pa.setBillingAccountId(fh.getLong("billingAccountId"));
        pa.setPaymentMethodId(fh.getLong("paymentMethodId"));
        Long pci = fh.getLong("paymentConditionId");
        Object o = getOptionMap(Options.RECORD_PAYMENT_CONDITIONS).get(pci);
        if (o != null && (o instanceof PaymentCondition)) {
            PaymentCondition pc = (PaymentCondition) o;
            pa.setPaymentCondition(pc);
            if (log.isDebugEnabled()) {
                log.debug("updatePaymentAgreement() set condition " + pc.getId());
            }
        }
        persist(contact);
        refresh();
        disableAllModes();
    }

    protected abstract PaymentAware fetchPaymentAware();

    protected abstract void persist(PaymentAware contact);

    @Override
    public void disableAllModes() {
        super.disableAllModes();
        paymentAgreementEditMode = false;
    }
}
