SET search_path = osserp, pg_catalog;

INSERT INTO system_properties VALUES (228, 'taxRateZeroSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (229, 'taxRateZeroTextPrefix', 'Ermäßigter Steuersatz 0% auf', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (230, 'taxRateZeroTextPostfix', 'gemäß § 12 Absatz 3 UStG', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (231, 'deEst35aSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 231, true);

INSERT INTO record_tax_rates (name, rate, is_default, is_reduced, valid_from) VALUES ('Null-Steuersatz', 1.00, 0, 0, '2023-01-01');

ALTER TABLE sales_invoices ADD COLUMN de_est_35a text;
ALTER TABLE sales_invoices ADD COLUMN de_est_35a_tax text;

ALTER TABLE sales_order_downpayments ADD COLUMN id_canceled bigint;

UPDATE system_properties SET pvalue = '0.15.12', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
