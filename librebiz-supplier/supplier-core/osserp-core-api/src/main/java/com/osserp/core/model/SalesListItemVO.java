/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 13, 2006 3:53:33 PM 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.common.util.DateUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesListItemVO extends RequestListItemVO implements SalesListItem {

    private boolean cancelled = false;
    private Long managerSubId = null;

    private Long installerId = null;
    private Date installationDate = null;
    private int installationDays = 0;
    private int installationDaysTotal = 0;
    private int installationDaysBefore = 0;
    private String installerName;

    private Long supplierId;
    private String supplierName;
    private String supplierGroup;

    protected Long requestId = null;

    private boolean vacant = false;

    protected SalesListItemVO() {
        super();
    }

    public SalesListItemVO(
            String customerName,
            String shortName,
            Long contactType,
            Long id,
            Long requestId,
            BusinessType type,
            Long customerId,
            Long status,
            Date created,
            Long createdBy,
            Long managerId,
            String managerKey,
            Long managerSubId,
            Long salesId,
            String salesKey,
            BranchOffice branch,
            String name,
            String street,
            String zipcode,
            String city,
            Double capacity,
            boolean stopped,
            boolean cancelled,
            Long installerId,
            Date lastActionDate,
            Long originId,
            Long originTypeId,
            String accountingReference,
            Long shippingId,
            Long paymentId,
            Date deliveryDate,
            Date confirmationDate) {
        super(
                id,
                name,
                customerId,
                customerName,
                shortName,
                contactType,
                street,
                zipcode,
                city,
                originId,
                originTypeId,
                type,
                status,
                created,
                createdBy,
                salesId,
                salesKey,
                managerId,
                managerKey,
                branch,
                null, // sales talk date from request
                null, // presentation date from request
                100, // order probability of request
                created, // order probability date of request
                capacity,
                null, // date of last note
                null, // company
                stopped,
                accountingReference,
                shippingId,
                paymentId,
                deliveryDate);
        setLastActionDate(lastActionDate);
        setConfirmationDate(confirmationDate);
        this.requestId = requestId;
        this.cancelled = cancelled;
        this.managerSubId = managerSubId;
        this.installerId = installerId;
    }

    public SalesListItemVO(SalesListItem vo) {
        super(
                vo.getId(),
                vo.getName(),
                vo.getCustomerId(),
                vo.getCustomerName(),
                vo.getShortName(),
                vo.getContactType(),
                vo.getStreet(),
                vo.getZipcode(),
                vo.getCity(),
                vo.getOriginId(),
                vo.getOriginTypeId(),
                vo.getType(),
                vo.getStatus(),
                vo.getCreated(),
                vo.getCreatedBy(),
                vo.getSalesId(),
                vo.getSalesKey(),
                vo.getManagerId(),
                vo.getManagerKey(),
                vo.getBranch(),
                null, // see above
                null,
                vo.getOrderProbability(),
                vo.getOrderProbabilityDate(),
                vo.getCapacity(),
                null,
                vo.getCompany(),
                vo.isStopped(),
                vo.getAccountingReference(),
                vo.getShippingId(),
                vo.getPaymentId(),
                vo.getDeliveryDate());
        setLastActionDate(vo.getLastActionDate());
        setConfirmationDate(vo.getConfirmationDate());
        this.requestId = vo.getRequestId();
        this.cancelled = vo.isCancelled();
        this.managerSubId = vo.getManagerSubId();
        this.installerId = vo.getInstallerId();
        this.installationDate = vo.getInstallationDate();
        this.installationDays = vo.getInstallationDays();
        this.supplierId = vo.getSupplierId();
        this.supplierName = vo.getSupplierName();
        this.supplierGroup = vo.getSupplierGroup();
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    protected void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isReleased() {
        return (getStatus() == null ? false : (getStatus() >= Sales.STATUS_CONFIRMED));
    }

    public boolean isDeliveryClosed() {
        return (getStatus() == null ? false : (getStatus() >= Sales.STATUS_DELIVERY_CLOSED));
    }

    public Long getManagerSubId() {
        return managerSubId;
    }

    protected void setManagerSubId(Long managerSubId) {
        this.managerSubId = managerSubId;
    }

    public Long getInstallerId() {
        return installerId;
    }

    protected void setInstallerId(Long installerId) {
        this.installerId = installerId;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public int getInstallationDays() {
        return installationDays;
    }

    public void setInstallationDays(int installationDays) {
        this.installationDays = installationDays;
    }

    public int getInstallationDaysTotal() {
        return installationDaysTotal;
    }

    protected void setInstallationDaysTotal(int installationDaysTotal) {
        this.installationDaysTotal = installationDaysTotal;
    }

    public int getInstallationDaysBefore() {
        return installationDaysBefore;
    }

    protected void setInstallationDaysBefore(int installationDaysBefore) {
        this.installationDaysBefore = installationDaysBefore;
    }

    public boolean isVacant() {
        return vacant;
    }

    public void setVacant(boolean vacant) {
        this.vacant = vacant;
    }

    public String getInstallerName() {
        return installerName;
    }

    public void setInstallerName(String installerName) {
        this.installerName = installerName;
    }

    protected void initInstallationDays(int installationDays) {
        if (installationDays == 0) {
            installationDays = 1;
        }
        setInstallationDays(installationDays);
        if (getAppointmentDate() != null) {
            Date dateHelp = getAppointmentDate();
            int extendedDuration = installationDays;
            for (int k = 0; k < extendedDuration; k++) {
                int dow = DateUtil.getDayOfWeek(dateHelp);
                if ((dow == 1) || (dow == 7)) {
                    extendedDuration += 1;
                }
                dateHelp = DateUtil.addDays(dateHelp, 1);
            }
            setInstallationDaysTotal(extendedDuration);
        }
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierGroup() {
        return supplierGroup;
    }

    public void setSupplierGroup(String supplierGroup) {
        this.supplierGroup = supplierGroup;
    }
}
