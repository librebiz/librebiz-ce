/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 31, 2005 1:34:11 PM 
 * 
 */
package com.osserp.common;

import java.io.Serializable;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ChartConfig extends Serializable {

    /**
     * Provides the data required to render the chart
     * @return data
     */
    Object getData();

    /**
     * Sets the data required to render the chart
     * @param data
     */
    void setData(Object data);

    /**
     * Provides the rendering quality, default 1.0f
     * @return quality
     */
    public float getQuality();

    /**
     * Sets the rendering quality
     * @param quality
     */
    void setQuality(float quality);

    /**
     * Provides the width of the chart, default 800
     * @return width
     */
    int getWidth();

    /**
     * Sets the width of the chart
     * @param width
     */
    void setWidth(int width);

    /**
     * Provides the height of the chart, default 600
     * @return height
     */
    int getHeight();

    /**
     * Sets the height of the chart
     * @param height
     */
    void setHeight(int height);

    /**
     * Provides the name of the responsible chart renderer service
     * @return rendererServiceName
     */
    String getRendererServiceName();

    /**
     * Returns the value of a additional option with the given name (if option not exists returns false)
     * @param name
     * @return additional option
     */
    boolean getAdditionalOption(String name);

    /**
     * Sets a additional option
     * @param name
     * @param value
     */
    void addAdditionalOption(String name, boolean value);
}
