/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Oct-2006 09:31:18 
 * 
 */
package com.osserp.common.mail;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SimpleMail {

    /**
     * Provides the mail message id as provided by mail server
     * @return messageId
     */
    String getMessageId();

    /**
     * The originator of the email
     * @return the originator.
     */
    String getOriginator();

    /**
     * Sets the originator for the email
     * @param originator to set.
     */
    void setOriginator(String originator);

    /**
     * The subject of the email
     * @return the subject.
     */
    String getSubject();

    /**
     * Sets the subject of the mail
     * @param subject
     */
    void setSubject(String subject);

    /**
     * The email text
     * @return the text.
     */
    String getText();

    /**
     * Sets the email text
     * @param text to set.
     */
    void setText(String text);

    /**
     * Optional message as html
     * @return message as html
     */
    String getHtml();

    /**
     * @param html to set.
     */
    void setHtml(String html);
}
