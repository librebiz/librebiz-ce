/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 20, 2016
 * 
 */
package com.osserp.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessObjectChangeManager;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.Customers;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOffers;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.Order;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.users.DomainUser;

/**
 * BusinessObjectChangeManager is responsible for business object relating
 * operations with an impact to many other objects such as change of an id
 * or reference. 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessObjectChangeManagerImpl extends AbstractCoreService implements BusinessObjectChangeManager {
    
    private Customers customers;
    private Projects projects;
    private Requests requests;
    private SalesOffers salesOffers;
    private SalesOrders salesOrders;
    private SalesInvoices salesInvoices;
    private SalesOrderDownpayments salesOrderDownpayments;

    public BusinessObjectChangeManagerImpl(
            OptionsCache optionsCache,
            Customers customers, 
            Requests requests, 
            Projects projects, 
            SalesOffers salesOffers, 
            SalesOrders salesOrders,
            SalesOrderDownpayments salesOrderDownpayments,
            SalesInvoices salesInvoices) {
        super(optionsCache);
        this.customers = customers;
        this.requests = requests;
        this.projects = projects;
        this.salesOffers = salesOffers;
        this.salesOrders = salesOrders;
        this.salesOrderDownpayments = salesOrderDownpayments;
        this.salesInvoices = salesInvoices;
    }

    public void changeCustomer(DomainUser domainUser, BusinessCase businessCase, Long customerId) throws ClientException {
        if (customerId == null) {
            throw new ClientException(ErrorCode.ID_REQUIRED);
        }
        // TODO check domain user permissions
        Long userId = (domainUser == null ? Constants.SYSTEM_USER : domainUser.getId());
        Customer customer = (Customer) customers.find(customerId);
        if (customer != null) {
            Sales sales = fetchSales(businessCase);
            if (sales != null) {
                Order order = salesOrders.find(sales);
                if (order != null) {
                    if (isInvoiceAvailable(order)) {
                        throw new ClientException(ErrorCode.INVOICES_FOUND_CUSTOMER_CHANGE_DENIED);
                    }
                    sales.getRequest().setCustomer(customer);
                    requests.save(sales.getRequest());
                    if (!order.isUnchangeable()) {
                        order.changeContact(customer, userId);
                        salesOrders.save(order);
                    }
                } else {
                    sales.getRequest().setCustomer(customer);
                    requests.save(sales.getRequest());
                }
            } else if (businessCase instanceof Request) {
                Request request = (Request) businessCase;
                List<Offer> offers = salesOffers.getByRequest(request);
                for (Offer offer : offers) {
                    if (!offer.isUnchangeable()) {
                        offer.changeContact(customer, userId);
                        salesOffers.save(offer);
                    }
                }
                request.setCustomer(customer);
                requests.save(request);
            } else {
                
                throw new BackendException(ErrorCode.APPLICATION_CONFIG);
            }
        } else {
            throw new ClientException(ErrorCode.CUSTOMER_ID_NOT_EXISTING);
        }
    }

    protected boolean isInvoiceAvailable(Order order) {
        if (order != null) {
            List<Invoice> invoices = new ArrayList(salesOrderDownpayments.getByReference(order.getId()));
            if (invoices.isEmpty()) {
                invoices = new ArrayList(salesInvoices.getByReference(order.getId()));
            }
            return !invoices.isEmpty();
        }
        return false;
    }

    protected Sales fetchSales(BusinessCase businessCase) {
        if (businessCase instanceof Sales) {
            return (Sales) businessCase;
        }
        Request request = (Request) businessCase;
        return projects.findByRequest(request.getPrimaryKey());
    }
}
