<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="projectsByEmployeeView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <c:choose>
            <c:when test="${view.salesMode}">
                <fmt:message key="ordersBySalesperson" />
            </c:when>
            <c:otherwise>
                <fmt:message key="ordersByProjector" />
            </c:otherwise>
        </c:choose>
		[<oc:employee value="${view.listReferenceId}" />: <o:listSize value="${view.list}" />]
	</tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="projectsByEmployeeContent">
            <div class="row">
                <c:set var="selectUrl" scope="request" value="/loadSales.do" />
                <c:set var="selectExitTarget" scope="request" value="projectsByEmployee" />
                <c:set var="collection" scope="request" value="${view.list}" />
                <c:import url="_projectList.jsp" />
            </div>
        </div>
    </tiles:put>
</tiles:insert>
