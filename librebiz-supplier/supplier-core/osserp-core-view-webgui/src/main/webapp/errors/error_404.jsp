<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/error_public.jsp" flush="false">
    <tiles:put name="title">404 Not Found</tiles:put>
    <tiles:put name="error">Not Found</tiles:put>
    <tiles:put name="message">The requested URL was not found on this server.</tiles:put>
</tiles:insert>
