<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:choose>
    <c:when test="${!empty sessionScope.contactPersonView}">
        <c:set var="myCountryView" value="${sessionScope.contactPersonView}"/>
        <c:set var="myCountryId" value="${myCountryView.bean.address.country}"/>
        <c:set var="myFederalState" value="${myCountryView.bean.address.federalStateId}"/>
        <c:set var="myFederalStateName" value="${myCountryView.bean.address.federalStateName}"/>
        <c:set var="myCountryUrl" value="/osdb/contactPersons.do"/>
    </c:when>
	<c:otherwise>
		<c:set var="myCountryView" value="${sessionScope.contactView}"/>
		<c:set var="myCountryId" value="${myCountryView.bean.address.country}"/>
		<c:set var="myFederalState" value="${myCountryView.bean.address.federalStateId}"/>
		<c:set var="myFederalStateName" value="${myCountryView.bean.address.federalStateName}"/>
		<c:set var="myCountryUrl" value="/osdb/contacts.do"/>
	</c:otherwise>
</c:choose>
<tr>
	<td><fmt:message key="country"/></td>
	<td><oc:select name="countryId" value="${myCountryId}" options="countryNames" onchange="ojsAjax.ajaxLink('${myCountryUrl}?method=loadFederalStates&id='+this.value, 'countrySelection', false, '', '', '', this);" prompt="false"/></td>
</tr>
<c:choose>
	<c:when test="${myCountryView.customFederalState}">
		<tr>
			<td><fmt:message key="federalState"/></td>
			<td><input type="text" name="federalStateName" value="<o:out value="${myFederalStateName}"/>" /></td>
		</tr>
	</c:when>
	<c:otherwise>
        <tr>
            <td><fmt:message key="federalState"/></td>
			<td><oc:select name="federalStateId" value="${myFederalState}" options="federalStates" id="federalStateId" prompt="false"/></td>
		</tr>
	</c:otherwise>
</c:choose>
