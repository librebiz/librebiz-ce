/**
 *
 * Copyright (C) 2011, 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 3, 2011 3:07:07 PM 
 * 
 */
package com.osserp.core.service.hrm;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.dao.Events;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.hrm.TimeRecordingApprovals;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecordingApproval;
import com.osserp.core.hrm.TimeRecordingApprovalManager;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingApprovalManagerImpl extends AbstractService implements TimeRecordingApprovalManager {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingApprovalManagerImpl.class.getName());

    private TimeRecordingApprovals timeRecordingApprovals = null;
    private Events events = null;
    private SystemConfigs systemConfigs;

    protected TimeRecordingApprovalManagerImpl(
            TimeRecordingApprovals timeRecordingApprovals,
            Events events,
            SystemConfigs systemConfigs) {
        super();
        this.timeRecordingApprovals = timeRecordingApprovals;
        this.events = events;
        this.systemConfigs = systemConfigs;
    }

    public TimeRecordingApproval findApproval(Long id) {
        try {
            return timeRecordingApprovals.getApproval(id);
        } catch (Exception e) {
            log.warn("findApproval() invoked for not existing row [id=" + id + "]");
            return null;
        }
    }

    public List<TimeRecordingApproval> loadApprovals(Employee employee) {
        if (log.isDebugEnabled()) {
            log.debug("loadApprovals() invoked [employee=" + (employee == null ? "null" : employee.getId()) + "]");
        }
        try {
            List<TimeRecordingApproval> l = timeRecordingApprovals.getApprovals(employee);
            if (log.isDebugEnabled()) {
                log.debug("loadApprovals() done [employee=" + (employee == null ? "null" : employee.getId()) +
                        ", count=" + l.size() + "]");
            }
            return l;
        } catch (Exception e) {
            log.warn("loadApprovals() failed [message=" + e.getMessage(), e);
            return new ArrayList<TimeRecordingApproval>();
        }
    }

    public void closeApproval(Employee user, TimeRecordingApproval approval, boolean approved) {
        timeRecordingApprovals.closeApproval(user, approval, approved);
        if (events != null) {
            Long approvalCreatedEventId = systemConfigs.getSystemPropertyId(TimeRecordingApprovals.APPROVAL_EVENT_CREATED);
            if (approvalCreatedEventId != null) {
                events.closeEvents(user.getId(), approval.getId(), approvalCreatedEventId);
            }
            Long approvalUpdatedEventId = systemConfigs.getSystemPropertyId(TimeRecordingApprovals.APPROVAL_EVENT_UPDATED);
            if (approvalUpdatedEventId != null) {
                events.closeEvents(user.getId(), approval.getId(), approvalUpdatedEventId);
            }
        }
    }
}
