<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productClassificationCriteriaSelectionView"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="${view.headerName}"/>
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<div class="table-responsive" style="height: auto;">
				<table class="table table-striped">
					<thead>
						<tr>
							<th><fmt:message key="id"/></th>
							<th><fmt:message key="name"/></th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="item" items="${view.list}">
						<tr>
							<td><o:out value="${item.id}"/></td>
							<td><a href="<c:url value="${view.url}${item.id}"/>"><o:out value="${item.name}"/></a></td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="spacer"></div>
		</div>
	</div>
</div>