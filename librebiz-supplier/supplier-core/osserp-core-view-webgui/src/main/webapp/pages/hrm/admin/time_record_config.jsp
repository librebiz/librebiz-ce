<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.timeRecordConfigView}"><c:redirect url="/errors/error_context.jsp"/></c:if>
<c:set var="configView" value="${sessionScope.timeRecordConfigView}"/>
<c:set var="type" value="${configView.bean}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="javascript" type="string">
        <script type="text/javascript">

            function toggleInterval(checkbox) {
                if (checkbox.checked) {
                    $("intervalPeriod").style.display = "";
                } else {
                    $("intervalPeriod").style.display = "none";
                }
            }

            function toggleIntervalLength(checkbox) {
                if (checkbox.checked) {
                    $("intervalLength").style.display = "none";
                } else {
                    $("intervalLength").style.display = "";
                }
            }

        </script>
    </tiles:put>

    <tiles:put name="headline"><fmt:message key="timeRecordTypeConfigurationHeader"/></tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${configView.editMode}">
                    <li><a href="<c:url value="/timeRecordConfig.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                </c:when>
                <c:when test="${configView.createMode}">
                    <li><a href="<c:url value="/timeRecordConfig.do?method=disableCreate"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                </c:when>
                <c:when test="${!empty type}">
                    <li><a href="<c:url value="/timeRecordConfig.do?method=select"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                    <li><a href="<c:url value="/timeRecordConfig.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon"/></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/timeRecordConfig.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                    <li><a href="<c:url value="/timeRecordConfig.do?method=enableCreate"/>" title="<fmt:message key="createNewProfile"/>"><o:img name="newdataIcon"/></a></li>
                </c:otherwise>
            </c:choose>
            <li><v:link url="/admin" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="timeRecordConfigContent">
            <div class="row">
                <c:choose>
                    <c:when test="${configView.createMode}">
                        <c:import url="/pages/hrm/admin/_timeRecordConfigCreate.jsp"/>
                    </c:when>
                    <c:when test="${configView.editMode}">
                        <c:set var="type" scope="request" value="${type}"/>
                        <c:import url="/pages/hrm/admin/_timeRecordConfigEdit.jsp"/>
                    </c:when>
                    <c:when test="${!empty type}">
                        <c:set var="timeRecordType" scope="request" value="${type}"/>
                        <div class="col-md-6 panel-area panel-area-default">
                            <c:import url="/pages/hrm/shared/time_record_config_display.jsp"/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-12 panel-area">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="name"><fmt:message key="number"/></th>
                                            <th class="name"><fmt:message key="name"/></th>
                                            <th class="name"><fmt:message key="description"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="obj" items="${configView.list}">
                                            <tr>
                                                <td class="center boldtext"><o:out value="${obj.id}"/></td>
                                                <td><a href="<c:url value="/timeRecordConfig.do"><c:param name="method" value="select"/><c:param name="id" value="${obj.id}"/></c:url>"><o:out value="${obj.name}"/></a></td>
                                                <td><o:out value="${obj.description}"/></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
