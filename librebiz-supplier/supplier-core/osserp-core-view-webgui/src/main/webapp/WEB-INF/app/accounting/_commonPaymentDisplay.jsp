<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.type.name}" rawTitle="ID ${view.bean.number}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="issuer" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <a href="<c:url value="/suppliers.do?method=load&id=${view.bean.contact.id}&exit=commonPayment"/>">
                            <o:out value="${view.bean.contact.displayName}" />
                        </a>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="type" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.type.name}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bookingText"><fmt:message key="bookingText" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.note}" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recordDate"><fmt:message key="recordDate" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:date value="${view.bean.created}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bookingText"><fmt:message key="paidOnLabel" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:date value="${view.bean.paid}" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="amount"><fmt:message key="amount" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <span> <o:number value="${view.bean.amount}" format="currency" /> <span style="margin-left: 2px;"><o:out value="${view.bean.currencyKey}" /></span>
                        </span>
                    </div>
                </div>
            </div>

            <c:choose>
                <c:when test="${view.bean.taxFree}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exemptOfTax"><fmt:message key="exemptOfTax" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <fmt:message key="bigYes" />
                                -
                                <oc:options name="taxFreeDefinitions" value="${view.bean.taxFreeId}" />
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="reducedTurnoverTax"><fmt:message key="reducedTurnoverTax" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${view.bean.reducedTax}">
                                        <fmt:message key="bigYes" />
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="bigNo" />
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>

            <c:if test="${!empty view.bankAccount}">
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bankAccountNumber" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.bankAccount.bankAccountNumberIntl}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bankAccountLabel" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.bankAccount.name}" />
                        </div>
                    </div>
                </div>
                <br />
            </c:if>

            <c:if test="${view.bean.templateRecord}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="saveAsTemplate"><fmt:message key="templateLabel" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="bigYes" />
                            -
                            <o:out value="${view.bean.templateName}" />
                        </div>
                    </div>
                </div>
            </c:if>

        </div>
    </div>
</div>
