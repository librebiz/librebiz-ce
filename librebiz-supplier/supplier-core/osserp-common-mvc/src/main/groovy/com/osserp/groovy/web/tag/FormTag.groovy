/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 28, 2013 7:55:22 AM 
 * 
 */
package com.osserp.groovy.web.tag

import com.osserp.common.web.RequestUtil
import com.osserp.common.web.tags.TagUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class FormTag extends AbstractTag {

    String id = ''
    String name = ''
    String url
    String onsubmit
    boolean disableAutoupdate = false
    boolean updateonly = false
	boolean multipart = false

    @Override
    protected String getStartTag() {
        String formUrl = baseUrl
        String[] csrfToken = RequestUtil.getCSRFToken(getRequest(), formUrl);
        TagUtil.createForm(
            id,
            name,
            formUrl,
            renderCommonAttributes(),
            disableAutoupdate,
            updateonly,
            csrfToken[0],
            csrfToken[1])
    }
    
    @Override
    protected String appendSpecificAttributes(StringBuilder commonAttributes) {
        if (multipart) {
            commonAttributes.append(' enctype="multipart/form-data"')
        }
        if (onsubmit) {
            commonAttributes.append(' onsubmit="').append(onsubmit).append('"')
        }
        return commonAttributes.toString()
    }

    @Override
    protected String getEndTag() {
        '</form>'
    }

    protected String getBaseUrl() {
        createUrl(url)
    }
}
