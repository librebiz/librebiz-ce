<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="properties" /></h4>
        </div>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <v:text name="power" styleClass="form-control right" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="power" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <oc:select name="powerUnit" value="${view.product.powerUnit}" options="quantityUnits" prompt="false" styleClass="form-control right" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="powerUnit" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <oc:select name="quantityUnit" value="${product.quantityUnit}" options="quantityUnits" prompt="false" styleClass="form-control right" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="quantityUnit" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <v:text name="packagingUnit" value="${product.packagingUnit}" styleClass="form-control right" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="packagingUnit" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="nullable" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="billingInBundlesWithNull" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="offerUsageOnly" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="billingOfferOnly" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="affectsStock" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="inventory" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="kanban" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="kanban" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="term" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="term" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="virtual" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="virtual" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="plant" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="plant" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="quantityByPlant" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="quantityByPlant" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="bundle" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="bundle" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="reducedTax" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="reducedTurnoverTax" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="component" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="component" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="billingNoteRequired" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="textRequired" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="service" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="service" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="publicAvailable" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="syncClientLabel" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                            <label> 
                                <v:checkbox id="endOfLife" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><fmt:message key="eol" /></label>
                        </div>
                    </div>
                </div>
            </div>                        
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group right">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:submit styleClass="form-control" />
                        </div>
                    </div>
                </div>
            </div>                        
        </div>

    </div>
</div>


