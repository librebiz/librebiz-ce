<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="salesPersonSelectorView"/>
<style type="text/css">
.employeeId { width: 60px; }
.name { width: 200px; }
.branch { width: 200px; }
</style>
<div class="table-responsive table-responsive-default">
<table class="table table-striped" id="salesPersons">
	<thead>
		<tr>
			<th class="employeeId"><fmt:message key="id"/></th>
			<th class="name">
				<v:ajaxForm name="salesPersonSelectorForm" url="${view.baseLink}/selectType" targetElement="${view.name}_popup">
					<select id="salesType" name="type" onchange="document.salesPersonSelectorForm.onsubmit();">
						<c:choose>
							<c:when test="${view.salesCoMode}">
								<option value="coSales" selected="selected"><fmt:message key="coSales"/></option>
								<option value="sales"><fmt:message key="sales"/></option>
							</c:when>
							<c:otherwise>
								<option value="coSales"><fmt:message key="coSales"/></option>
								<option value="sales" selected="selected"><fmt:message key="sales"/></option>
							</c:otherwise>
						</c:choose>
					</select>
				</v:ajaxForm>
			</th>
			<th class="branch"><fmt:message key="branch"/></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="employee" items="${view.list}">
			<c:if test="${(view.salesCoMode and employee.id != view.request.salesCoId) or (!view.salesCoMode and employee.id != view.request.salesId)}">
				<tr>
					<td class="employeeId"><o:out value="${employee.id}"/></td>
					<td class="name"><v:ajaxLink url="${view.baseLink}/select?id=${employee.id}" targetElement="${view.name}_popup"><o:out value="${employee.displayName}"/></v:ajaxLink></td>
					<td class="branch"><o:out value="${employee.branch.name}"/></td>
				</tr>
			</c:if>
		</c:forEach>
	</tbody>
</table>
</div>