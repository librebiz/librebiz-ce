/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.admin.crm

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.AdminViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/admin/crm/campaignConfig/*")
@Controller class CampaignConfigController extends AdminViewController {
    private static Logger logger = LoggerFactory.getLogger(CampaignConfigController.class.getName())

    @RequestMapping
    def selectCompany(HttpServletRequest request) {
        logger.debug("selectCompany: invoked [user=${getUserId(request)}]")
        CampaignConfigView view = getView(request)
        view.selectCompany()
        return defaultPage
    }

    @RequestMapping
    def selectParent(HttpServletRequest request) {
        logger.debug("selectParent: invoked [user=${getUserId(request)}]")
        CampaignConfigView view = getView(request)
        view.selectParent()
        return defaultPage
    }

    @RequestMapping
    def changeParent(HttpServletRequest request) {
        logger.debug("changeParent: invoked [user=${getUserId(request)}]")
        CampaignConfigView view = getView(request)
        try {
            view.changeParent()
        } catch (Exception e) {
            if (logger.debugEnabled) {
                logger.debug("changeParent: caught exception [class=${e.getClass().getName()}, message=${e.message}]")
            }
            saveError(request, e.message)
        }
        return defaultPage
    }

    @RequestMapping
    def enableChangeParentMode(HttpServletRequest request) {
        logger.debug("enableChangeParentMode: invoked [user=${getUserId(request)}]")
        CampaignConfigView view = getView(request)
        view.enableChangeParentMode()
        return defaultPage
    }

    @RequestMapping
    def disableChangeParentMode(HttpServletRequest request) {
        logger.debug("disableChangeParentMode: invoked [user=${getUserId(request)}]")
        CampaignConfigView view = getView(request)
        view.disableChangeParentMode()
        return defaultPage
    }

    @RequestMapping
    def toggleDisplayEol(HttpServletRequest request) {
        logger.debug("toggleDisplayEol: invoked [user=${getUserId(request)}]")
        CampaignConfigView view = getView(request)
        view.toggleDisplayEol()
        return defaultPage
    }

    @RequestMapping
    def delete(HttpServletRequest request) {
        logger.debug("delete: invoked [user=${getUserId(request)}]")
        CampaignConfigView view = getView(request)
        try {
            view.delete()
        } catch (Exception e) {
            if (logger.debugEnabled) {
                logger.debug("delete: caught exception [class=${e.getClass().getName()}, message=${e.message}]")
            }
            saveError(request, e.message)
        }
        return defaultPage
    }
}

