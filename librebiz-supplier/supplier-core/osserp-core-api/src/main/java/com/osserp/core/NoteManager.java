/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 28, 2010 3:02:43 PM 
 * 
 */
package com.osserp.core;

import java.util.Map;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.mail.MailMessageContext;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface NoteManager {

    /**
     * Provides a note type by name
     * @param name
     * @return type
     */
    NoteType getType(String name);

    /**
     * Loads or reloads noteAware
     * @param primaryKey
     * @return noteAware with actual values
     */
    NoteAware load(Long primaryKey);

    /**
     * Adds a note
     * @param user
     * @param noteAware
     * @param headline
     * @param note
     * @throws ClientException if note is empty or other validation error
     */
    NoteAware addNote(Employee user, NoteAware noteAware, String headline, String note) throws ClientException;

    /**
     * Provides sticky note if exists
     * @param referenceId
     * @return note or null
     */
    BusinessNote getSticky(Long referenceId);

    /**
     * Changes sticky flag of existing note
     * @param noteAware
     * @param noteId
     * @param sticky
     * @return object with updated note
     * @throws ClientException
     */
    NoteAware setSticky(NoteAware noteAware, Long noteId, boolean sticky) throws ClientException;

    /**
     * Updates existing note
     * @param user
     * @param noteAware
     * @param noteId
     * @param note
     * @return object with updated note
     * @throws ClientException
     */
    NoteAware updateNote(Employee user, NoteAware noteAware, Long noteId, String note) throws ClientException;

    /**
     * Adds email values to note if note was sent
     * @param noteAware
     * @param noteId
     * @param recipients
     * @param subject
     * @param message
     */
    NoteAware completeNote(NoteAware noteAware, Long noteId, String recipients, String subject, String message);

    /**
     * Creates a mail with initialized recipients
     * @param user
     * @param noteAware
     * @param opts default opts consist of 'successTarget', 'exitTarget' and 'message'
     * @return mail
     * @throws ClientException if mail configuration is invalid or missing
     */
    MailMessageContext setupMail(Employee user, NoteAware noteAware, Map<String, String> opts) throws ClientException;

    /**
     * Creates a mail by existing
     * @param user
     * @param note
     * @param opts default opts consist of 'successTarget', 'exitTarget' and 'message'
     * @return mail
     * @throws ClientException if mail configuration is invalid or missing
     */
    MailMessageContext setupMail(Employee user, BusinessNote note, Map<String, String> opts) throws ClientException;
}
