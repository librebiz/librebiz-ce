/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2013 
 * 
 */
package com.osserp.core.system;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.mail.MailDomain;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface NocClientManager {

    /**
     * Provides the default noc client
     * @return defaultClient
     */
    NocClient getDefaultClient();

    /**
     * Provides a noc client by id
     * @param id
     * @return client by id
     */
    NocClient getClient(Long id);

    /**
     * Provides all available noc clients
     * @return clients
     */
    List<NocClient> getClients();

    /**
     * Provides all available domains
     * @return domains
     */
    List<MailDomain> getDomains();

    /**
     * Provides domains by reference
     * @param referenceId
     * @return domains
     */
    List<MailDomain> getDomainsByReference(Long referenceId);

    /**
     * Creates a new domain
     * @param user
     * @param client
     * @param parentDomain if domain is alias domain
     * @param name
     * @param provider
     * @param domainCreated
     * @param domainExpires
     * @return domain created
     * @throws ClientException if name already exists or primary domain of client exists and parentDomain is missing
     */
    MailDomain createDomain(Long user, NocClient client, MailDomain parentDomain, String name, String provider, Date domainCreated, Date domainExpires)
            throws ClientException;

    /**
     * Updates domain values
     * @param provider
     * @param smtpServerName
     * @param hostIp
     * @param hostMx
     * @param hostSecondaryMx
     * @param hostMxIp
     * @param hostSecondaryMxIp
     * @param domainCreated
     * @param domainExpires
     */
    MailDomain updateDomain(
            MailDomain domain,
            Long user,
            String provider,
            String smtpServerName,
            String hostIp,
            String hostMx,
            String hostSecondaryMx,
            String hostMxIp,
            String hostSecondaryMxIp,
            Date domainCreated,
            Date domainExpires);

}
