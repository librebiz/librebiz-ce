/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 27, 2009 4:55:56 PM 
 * 
 */
package com.osserp.core.model.products;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductSelectionProfile;
import com.osserp.core.products.ProductType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSelectionImpl extends AbstractEntity implements ProductSelection {
    private List<ProductSelectionProfile> profileList = new ArrayList<ProductSelectionProfile>();
    private Employee employee = null;
    private boolean ignoreSelections = false;
    private List<ProductSelectionConfigItem> items = new ArrayList<ProductSelectionConfigItem>();

    protected ProductSelectionImpl() {
        super();
    }

    /**
     * Creates a new purchasing config by purchasingProfile
     * @param profile
     */
    public ProductSelectionImpl(ProductSelectionProfile profile) {
        super();
        setId(0L);
        this.profileList.add(profile);
    }

    /**
     * Creates a new purchasing config
     * @param user creating new config
     * @param employee to configure
     */
    public ProductSelectionImpl(Employee user, Employee employee) {
        super((Long) null, (Long) null, user.getId());
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    protected void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public boolean isIgnoreSelections() {
        return ignoreSelections;
    }

    public void setIgnoreSelections(boolean ignoreSelections) {
        this.ignoreSelections = ignoreSelections;
    }

    public void addProfile(ProductSelectionProfile profile) {
        boolean added = false;
        for (int i = 0, j = profileList.size(); i < j; i++) {
            ProductSelectionProfile next = profileList.get(i);
            if (next.getId().equals(profile.getId())) {
                added = true;
                break;
            }
        }
        if (!added) {
            this.profileList.add(profile);
        }
    }

    public void removeProfile(Long id) {
        for (Iterator<ProductSelectionProfile> i = profileList.iterator(); i.hasNext();) {
            ProductSelectionProfile next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                break;
            }
        }
    }

    public List<ProductSelectionProfile> getProfiles() {
        // List is readonly. Adding and removing from list is provided by explicit calls
        // to addProfile and removeProfile only.
        return new ArrayList<ProductSelectionProfile>(profileList);
    }

    public int getItemCount() {
        return items.size();
    }

    public List<ProductSelectionConfigItem> getItems() {
        if (items.isEmpty()) {
            for (int i = 0, j = profileList.size(); i < j; i++) {
                ProductSelectionProfile profile = profileList.get(i);
                for (int k = 0, l = profile.getProductSelections().size(); k < l; k++) {
                    ProductSelectionConfig selection = profile.getProductSelections().get(k);
                    for (int m = 0, n = selection.getItems().size(); m < n; m++) {
                        ProductSelectionConfigItem item = selection.getItems().get(m);
                        items.add(item);
                    }
                }
            }
        }
        return items;
    }

    public boolean supports(ProductType type) {
        if (ignoreSelections) {
            return true;
        }
        List<ProductSelectionConfigItem> l = getItems();
        for (int i = 0, j = l.size(); i < j; i++) {
            ProductSelectionConfigItem next = l.get(i);
            if (next.getType() != null && next.getType().getId().equals(type.getId())) {
                return true;
            }
        }
        return false;
    }

    public boolean supports(Product product) {
        if (ignoreSelections) {
            return true;
        }
        List<ProductSelectionConfigItem> l = getItems();
        for (int i = 0, j = l.size(); i < j; i++) {
            ProductSelectionConfigItem next = l.get(i);
            if (next.isMatching(product)) {
                return true;
            }
        }
        return false;
    }

    public List<Product> filter(List<Product> products) {
        List<Product> result = new ArrayList<Product>();
        if (ignoreSelections) {
            result.addAll(products);
        } else {
            for (int i = 0, j = products.size(); i < j; i++) {
                Product next = products.get(i);
                if (supports(next)) {
                    result.add(next);
                }
            }
        }
        return result;
    }

    protected List<ProductSelectionProfile> getProfileList() {
        return profileList;
    }

    protected void setProfileList(List<ProductSelectionProfile> profileList) {
        this.profileList = profileList;
    }
}
