/**
 *
 * Copyright (C) 2014, 2016 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on May 15, 2014 
 * 
 */
package com.osserp.api


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class WsSetupData extends WsObject {

    private static final long serialVersionUID = 4L

    String clientName

    Map<String, WsObject> sequenceDeclarations = [
        
        'commonPayment': new WsObject( [ primaryKey : 100L, externalKey: 'commonPayment'] ),
        // sales records
        'salesOffer': new WsObject( [ primaryKey:   1L, externalKey: 'salesOffer'] ),
        'salesOrder': new WsObject( [ primaryKey:   2L, externalKey: 'salesOrder'] ),
        'salesDeliveryNote': new WsObject( [ primaryKey:   3L, externalKey: 'salesDeliveryNote'] ),
        'salesDownpayment': new WsObject( [ primaryKey:   4L, externalKey: 'salesDownpayment'] ),
        'salesInvoice': new WsObject( [ primaryKey:   5L, externalKey: 'salesInvoice'] ),
        'salesCreditNote': new WsObject( [ primaryKey:   9L, externalKey: 'salesCreditNote'] ),
        'salesCancellation': new WsObject( [ primaryKey:  10L, externalKey: 'salesCancellation'] ),
        // purchase records
        'purchaseOffer': new WsObject( [ primaryKey: 101L, externalKey: 'purchaseOffer'] ),
        'purchaseOrder': new WsObject( [ primaryKey: 102L, externalKey: 'purchaseOrder'] ),
        'purchaseDeliveryNote': new WsObject( [ primaryKey: 103L, externalKey: 'purchaseDeliveryNote'] ),
        'purchaseInvoice': new WsObject( [ primaryKey: 105L, externalKey: 'purchaseInvoice'] ),
        // request and sales types
        'request': new WsObject( [ primaryKey: 1, externalKey: 'request'] ),
        'sales': new WsObject( [ primaryKey: 2, externalKey: 'sales'] ),
        'salesDirect': new WsObject( [ primaryKey: 3, externalKey: 'salesDirect'] ),
        'salesProject': new WsObject( [ primaryKey: 4, externalKey: 'salesProject'] ),
        'salesService': new WsObject( [ primaryKey: 5, externalKey: 'salesService'] ),
        'salesPlan': new WsObject( [ primaryKey: 6, externalKey: 'salesPlan'] ),
        // dedicated contact types
        'customer': new WsObject( [ primaryKey: 1, externalKey: 'customer'] ),
        'employee': new WsObject( [ primaryKey: 7, externalKey: 'employee'] ),
        'subcontractor': new WsObject( [ primaryKey: 3, externalKey: 'subcontractor'] ),
        'supplier': new WsObject( [ primaryKey: 2, externalKey: 'supplier'] )
        ]

    // company data

    String companyName
    String companyAffix
    Long legalFormId
    boolean registeredOfficeRequired

    String street
    String zipcode
    String city
    Long country

    String phoneCountry
    String phonePrefix
    String phoneNumber

    String faxCountry
    String faxPrefix
    String faxNumber

    String email
    String website

    String taxId
    String vatId
    String tradeRegisterEntry
    String localCourt

    String bankShortkey
    String bankName
    String bankNameAddon
    String bankAccountNumberIntl
    String bankIdentificationCodeIntl

    List<WsContact> managingDirectors = []
    boolean managingDirectorsComplete

    // contact person

    Long salutationId
    Long titleId
    String lastName
    String firstName

    String contactPhoneCountry
    String contactPhonePrefix
    String contactPhoneNumber

    String contactMobileCountry
    String contactMobilePrefix
    String contactMobileNumber

    String contactEmail

    /**
     * Adds managing director if not already added
     * @param director
     */
    void addManagingDirector(WsContact director) {
        String directorName = "${director.firstName} ${director.lastName}"
        String existing = managingDirectors.find { WsContact obj ->
            directorName == "${obj.firstName} ${obj.lastName}"
        }
        if (!existing) {
            managingDirectors << director
        }
    }

    void updateSequenceDeclaration(String name, Long value) {
        WsObject seq = sequenceDeclarations[name]
        if (seq) {
            sequenceDeclarations[name] = new WsObject( [ 
                primaryKey: seq.getPrimaryKey(), 
                externalKey: name, 
                targetId: value] )
        }
    }
    
}
