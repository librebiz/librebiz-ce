<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.mailingListView}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="contactActions" />
        <c:if test="${!empty view.bean}">
		- <fmt:message key="mailingLists" />: <o:out value="${view.bean.name}" />
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${view.createMode}">
                    <li><a href="<c:url value="/mailingLists.do?method=disableCreate"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                </c:when>
                <c:when test="${view.editMode}">
                    <li><a href="<c:url value="/mailingLists.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                </c:when>
                <c:when test="${empty view.bean}">
                    <o:permission role="mailing_lists_edit,executive" info="permissionCreateNewMailingLists">
                        <li><a href="<c:url value="/mailingLists.do?method=enableCreate"/>" title="<fmt:message key="createNewMailingList"/>"><o:img name="newdataIcon" /></a></li>
                    </o:permission>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/mailingLists.do?method=select"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                    <o:permission role="mailing_lists_edit,executive" info="permissionToggleMailingListsActivation">
                        <c:choose>
                            <c:when test="${view.bean.active}">
                                <li><a href="<c:url value="/mailingLists.do?method=toggleActivation"/>" title="<fmt:message key="deactivateMailingList"/>"><o:img name="disabledIcon" /></a></li>
                            </c:when>
                            <c:otherwise>
                                <li><a href="<c:url value="/mailingLists.do?method=toggleActivation"/>" title="<fmt:message key="activateMailingList"/>"><o:img name="enabledIcon" /></a></li>
                            </c:otherwise>
                        </c:choose>
                    </o:permission>
                </c:otherwise>
            </c:choose>
            <li><a href="<c:url value="/mailingLists.do?method=exit"/>" title="<fmt:message key="backToMenu"/>"><o:img name="homeIcon" /></a></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="contactActionsIndexContent">
            <div class="row">

                <c:choose>
                    <c:when test="${empty view.bean or view.editMode or view.createMode}">
                        <c:import url="_mailing_list_index.jsp" />
                    </c:when>
                    <c:otherwise>
                        <c:import url="_mailing_list_edit.jsp" />
                    </c:otherwise>
                </c:choose>

            </div>
        </div>
    </tiles:put>
</tiles:insert>
