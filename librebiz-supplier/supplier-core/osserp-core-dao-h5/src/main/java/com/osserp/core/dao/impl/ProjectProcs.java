/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 11, 2005 
 * 
 */
package com.osserp.core.dao.impl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectProcs extends Procs {

    /**
     * Provides a record list of all projects related to a given manager. <br>
     * Params: Params are 'manager_id BIGINT' and 'open_only BOOLEAN'
     */
    public static final String MANAGER_PROJECTS =
            "SELECT * FROM " + SCHEMA + "get_projects_by_manager(?,?)";

    /**
     * Provides a record list of all projects with existing final invoice <br>
     * Params: Params are 'date_from TIMESTAMP' and 'date_til TIMESTAMP'
     */
    public static final String PROJECTS_BY_EXISTING_INVOICE =
            "SELECT * FROM " + SCHEMA + "get_projects_by_existing_invoice(?,?)";

    /**
     * Provides a record list of all projects with cancelled status <br>
     * Params: None
     */
    public static final String PROJECTS_BY_CANCELLED_STATUS =
            "SELECT * FROM " + SCHEMA + "get_projects_by_cancelled_status()";

    /**
     * Provides a record list of all projects with stop status <br>
     * Params: None
     */
    public static final String PROJECTS_BY_STOPPED_STATUS =
            "SELECT * FROM " + SCHEMA + "get_projects_by_stopped_status()";

    /**
     * Provides a record list of all projects created in given month of given year <br/>
     * Params: <br/>
     * 'month INT' (0-11) <br/>
     * 'year INT'
     */
    public static final String PROJECTS_CREATED_IN_MONTH =
            "SELECT * FROM " + SCHEMA + "get_projects_created_in_month(?,?)";

    /**
     * Provides a record list of all projects created in given year <br/>
     * Param: <br/>
     * 'year INT'
     */
    public static final String PROJECTS_CREATED_IN_YEAR =
            "SELECT * FROM " + SCHEMA + "get_projects_created_in_year(?)";

    /**
     * Provides a record list of all projects related to a given sales person. <br>
     * Params: Params are 'manager_id BIGINT' and 'open_only BOOLEAN'
     */
    public static final String SALES_PROJECTS =
            "SELECT * FROM " + SCHEMA + "get_projects_by_sales(?,?)";

    /**
     * Provides a record list of all projects of a given customer depending on common options <br>
     * Params: <br>
     * projecttype BIGINT, <br>
     * actionid BIGINT, the action we lookup for <br>
     * date_from TIMESTAMP, <br>
     * date_til TIMESTAMP <br>
     * with_canceled BOOLEAN, false ignores canceled projects <br>
     * with_closed BOOLEAN, false ignores closed projects <br>
     * byzipcode VARCHAR
     */
    public static final String PROJECTS_BY_ACTION =
            "SELECT * FROM " + SCHEMA + "get_projects_by_action(?,?,?,?,?,?,?)";

    /**
     * Provides a record list of all projects with given action in given period <br>
     * Params: <br>
     * projecttype BIGINT, <br>
     * actionid BIGINT, the action that should not exist for project <br>
     * with_canceled BOOLEAN, false ignores canceled projects <br>
     * with_closed BOOLEAN, false ignores closed projects
     */
    public static final String PROJECTS_WITHOUT_ACTION =
            "SELECT * FROM " + SCHEMA + "get_projects_without_action(?,?,?,?,?)";

    /**
     * Provides all sales where last fcs action is longer as given days ago Excludes closed, stopped and canceled sales
     * @param is 'max_days_ago INT'
     * @return RECORD stat_prj_by_last_action
     */
    public static final String PROJECTS_WITHOUT_ACTIVITY =
            "SELECT * FROM " + SCHEMA + "get_projects_without_activity(?)";
}
