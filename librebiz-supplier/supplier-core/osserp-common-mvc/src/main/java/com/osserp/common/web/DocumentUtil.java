/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Apr-2005 15:37:59 
 * 
 */
package com.osserp.common.web;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.dms.DocumentData;
import com.osserp.common.dms.DocumentException;
import com.osserp.common.util.DateFormatter;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DocumentUtil extends SessionUtil {
    private static Logger log = LoggerFactory.getLogger(DocumentUtil.class.getName());
    private static final String PDF_DOCUMENT = "pdfDocument";
    
    public static final boolean NOTE_REQUIRED = true;
    public static final boolean NOTE_OPTIONAL = false;

    public static final Object getPdfDocument(HttpServletRequest request) {
        return getPdfDocument(request.getSession());
    }

    public static final Object getPdfDocument(HttpSession session) {
        return session.getAttribute(PDF_DOCUMENT);
    }

    public static final void removePdfDocument(HttpServletRequest request) {
        removePdfDocument(request.getSession());
    }

    public static final void removePdfDocument(HttpSession session) {
        session.removeAttribute(PDF_DOCUMENT);
    }

    public static void setPdfDocument(HttpSession session, byte[] pdf) {
        session.setAttribute(PDF_DOCUMENT, pdf);
    }

    public static void setPdfDocument(HttpSession session, DocumentData pdf) {
        session.setAttribute(PDF_DOCUMENT, pdf);
    }

    /**
     * Stores given xml document and path to xsl stylesheet in session scope so xslt print servlet could find and process them
     * @param session where we put the objects
     * @param doc
     * @param styleSheet path
     */
    public static void setDocument(HttpSession session, org.jdom2.Document doc, String styleSheet) {
        setXmlDocument(session, doc);
        setStyleSheet(session, styleSheet);
    }

    public static boolean isXmlDocumentAvailable(HttpSession session) {
        return (session.getAttribute(Globals.XML_DOCUMENT) != null);
    }

    public static Document getXmlDocument(HttpSession session) {
        return (Document) getObject(session, Globals.XML_DOCUMENT);
    }

    public static void setXmlDocument(HttpSession session, Document doc) {
        session.setAttribute(Globals.XML_DOCUMENT, doc);
    }

    public static void removeXmlDocument(HttpSession session) {
        session.removeAttribute(Globals.XML_DOCUMENT);
    }

    public static String getXlsDocument(HttpSession session) {
        return (String) getObject(session, Globals.XLS_DOCUMENT);
    }

    public static void setXlsDocument(HttpSession session, String doc) {
        session.setAttribute(Globals.XLS_DOCUMENT, doc);
    }

    public static void removeXlsDocument(HttpSession session) {
        session.removeAttribute(Globals.XLS_DOCUMENT);
    }

    public static String getStyleSheet(HttpSession session) {
        return (String) getObject(session, Globals.XSL_STYLE_SHEET);
    }

    public static void setStyleSheet(HttpSession session, String styleSheet) {
        session.setAttribute(Globals.XSL_STYLE_SHEET, styleSheet);
    }

    public static void removeStyleSheet(HttpSession session) {
        session.removeAttribute(Globals.XSL_STYLE_SHEET);
    }

    /**
     * Creates a new empty xml document object with currentDate below root
     * @param root name of the root element
     * @return new created document
     */
    public static Document createDocument(String root) {
        Document doc = new Document();
        doc.setRootElement(new Element(root));
        Date date = new Date(System.currentTimeMillis());
        doc.getRootElement().addContent(new Element("currentDate").setText(
                DateFormatter.getDate(date)));
        return doc;
    }

    public static void flushDocument(
            HttpServletResponse response,
            String contentType,
            String content)
            throws DocumentException {

        response.setContentLength(content.length());
        response.setContentType(contentType);
        //        response.setHeader("Content-disposition", "attachment; filename=" + name);
        //        response.setHeader("Cache-Control", "max-age=" + MAX_AGE_IN_SECONDS);
        try {
            PrintWriter writer = response.getWriter();
            writer.print(content);
        } catch (Exception e) {
            log.error("flushDocument() failed [message=" + e.getMessage() + "]", e);
            throw new DocumentException(e.getMessage(), e);
        }
    }
}
