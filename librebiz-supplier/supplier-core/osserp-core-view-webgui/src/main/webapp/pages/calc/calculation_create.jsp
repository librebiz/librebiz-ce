<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.calculationView}">
    <o:logger write="No calculation view found in session context!" level="info"/>
</c:if>
<c:set var="view" value="${sessionScope.calculationView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="headline">
    <fmt:message key="newCalculation"/>
        <c:if test="${!empty view.businessCase}">
            <fmt:message key="to"/> <o:out value="${view.businessCase.primaryKey}"/> - <o:out value="${view.businessCase.name}" limit="45"/>
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/calculations.do?method=exitCreate"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="timeRecordingContent">
            <div class="row">
                <div class="col-md-6 panel-area panel-area-default">

                    <o:form name="itemListForm" url="/calculations.do">
                        <input type="hidden" name="method" value="create" />
                        <oc:systemPropertyEnabled name="calculationCalculatorSelectionSupport">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <fmt:message key="${view.calculatorName}" />
                                </div>
                            </div>
                        </oc:systemPropertyEnabled>
                        <div class="panel-body">
                            <div class="form-body">
                                <table class="table table-striped">
                                    <tr>
                                        <th class="left"><fmt:message key="name"/></th>
                                        <th class="center"><fmt:message key="action"/></th>
                                    </tr>
                                    <o:permission role="executive,customer_service" info="permissionCreateNewVersion">
                                        <c:if test="${!empty view.businessCase and view.businessCase.salesContext and !view.businessCase.closed and !view.businessCase.cancelled}">
                                            <tr>
                                                <td><fmt:message key="onlyChangeOrderDetails"/></td>
                                                <td class="center"><a href="<c:url value="/salesOrder.do?method=createNewVersion"/>"><o:img name="enabledIcon" styleClass="bigicon"/></a></td>
                                            </tr>
                                        </c:if>
                                    </o:permission>
                                    <c:forEach var="option" items="${view.calculationNames}" varStatus="s">
                                        <tr>
                                            <td><o:out value="${option.name}"/></td>
                                            <td class="center"><a href="<c:url value="/calculations.do?method=createByName&name=${option.name}"/>"><o:img name="enabledIcon" styleClass="bigicon"/></a></td>
                                        </tr>
                                    </c:forEach>
                                    <tr>
                                        <td><input type="text" name="value" value="" class="form-control" /></td>
                                        <td class="center">
                                            <input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="create" onclick="setMethod('create');" title="<fmt:message key="saveInput"/>"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </o:form>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
