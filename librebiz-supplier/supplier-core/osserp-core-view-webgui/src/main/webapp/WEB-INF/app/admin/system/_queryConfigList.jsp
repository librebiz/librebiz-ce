<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="number"><v:sortLink key="id"><fmt:message key="id"/></v:sortLink></th>
                    <th><v:sortLink key="name"><fmt:message key="queryName"/></v:sortLink></th>
                    <th><v:sortLink key="outputTitle"><fmt:message key="resultName"/></v:sortLink></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty view.list}">
                        <tr><td colspan="3"><fmt:message key="noQueriesDefined"/></td></tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="query" items="${view.list}">
                            <tr>
                                <td><o:out value="${query.id}"/></td>
                                <td><v:link url="${view.baseLink}/select" parameters="id=${query.id}"><o:out value="${query.name}"/></v:link></td>
                                <td><o:out value="${query.outputTitle}"/></td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
