/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2020
 * 
 */
package com.osserp.core;


import com.osserp.common.EntityRelation;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PluginMapping extends EntityRelation {

    static final String DELETE = "delete";
    static final String DISABLED = "disabled";
    static final String MAPPED = "mapped";
    static final String REMOVED = "removed";
    static final String UNMAPPED = "unmapped";

    /**
     * Getter for the name of the mapped entity.
     * @return name
     */
    String getName();

    /**
     * Sets the name of the mapped entity.
     * @param name
     */
    void setName(String name);

    /**
     * Getter for the id of the local entity.
     * @return mappedId
     */
    Long getMappedId();

    /**
     * Setter for the id of the local entity.
     * @param mappedId
     */
    void setMappedId(Long mappedId);

    /**
     * Getter for the id of the remote entity.
     * @return objectId
     */
    String getObjectId();

    /**
     * Setter for the id of the remote entity.
     * @param objectId
     */
    void setObjectId(String objectId);

    /**
     * Logical name of mapped object
     * @return name
     */
    String getObjectName();

    /**
     * Sets the logical name for the mapped object
     * @param objectName
     */
    void setObjectName(String objectName);

    /**
     * Mapping status
     * @return status
     */
    String getObjectStatus();

    /**
     * Sets the mapping status
     * @param objectStatus
     */
    void setObjectStatus(String objectStatus);

    /**
     * Getter for the data type of the remote id.
     * @return objectType
     */
    String getObjectType();

    /**
     * Setter for the data type of the remote id.
     * @param objectType
     */
    void setObjectType(String objectType);
}
