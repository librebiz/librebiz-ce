/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 27, 2013 2:32:18 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.BusinessNote;
import com.osserp.core.NoteType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessNotes {

    /**
     * Provides a type by id
     * @param name
     * @return type
     */
    NoteType getType(String name);

    /**
     * Provides available types by id
     * @return types
     */
    List<NoteType> getTypes();

    /**
     * Provides imported note by messageId of received email
     * @param messageId
     * @return note or null if not exists
     */
    BusinessNote findByMessageId(String messageId);

    /**
     * Finds notes by reference
     * @param typeName
     * @param referenceId
     * @return notes or empty list if non exists
     */
    List<BusinessNote> findByReference(String typeName, Long referenceId);

    /**
     * Provides sticky note if exists
     * @param typeName
     * @param referenceId
     * @return note or null
     */
    BusinessNote findSticky(String typeName, Long referenceId);

    /**
     * Creates a new businessNote
     * @param type
     * @param reference
     * @param createdBy
     * @param note
     * @param headline
     * @return new created note
     */
    BusinessNote create(
            NoteType type,
            Long reference,
            Long createdBy,
            String note,
            String headline);

    /**
     * Creates a new businessNote by received message
     * @param type
     * @param reference
     * @param createdBy
     * @param message
     * @param subject
     * @param messageId
     * @param received indicates sent or reveived message
     * @return new created note
     */
    BusinessNote create(
            NoteType type,
            Long reference,
            Long createdBy,
            String message,
            String subject,
            String messageId,
            boolean received);

    /**
     * Deletes a note
     * @param note
     */
    void delete(BusinessNote note);

    /**
     * Updates a note
     * @param note
     * @return updated note
     */
    BusinessNote save(BusinessNote note);

}
