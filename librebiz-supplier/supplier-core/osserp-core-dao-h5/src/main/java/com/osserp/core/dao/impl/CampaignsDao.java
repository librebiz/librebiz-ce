/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2011 3:45:53 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.Option;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.StringUtil;

import com.osserp.core.Options;
import com.osserp.core.crm.Campaign;
import com.osserp.core.crm.CampaignGroup;
import com.osserp.core.crm.CampaignType;
import com.osserp.core.dao.Campaigns;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.crm.CampaignGroupImpl;
import com.osserp.core.model.crm.CampaignImpl;
import com.osserp.core.model.crm.CampaignTypeImpl;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class CampaignsDao extends AbstractDao implements Campaigns {

    private String campaignTable = null;
    private String requestsTable = null;

    /**
     * Creates a new capaigns dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     */
    public CampaignsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        this.campaignTable = getTable(Options.CAMPAIGNS);
        this.requestsTable = getTable(TableKeys.REQUESTS);
    }

    public List<CampaignGroup> findGroups() {
        return getCurrentSession().createQuery("from " + CampaignGroupImpl.class.getName()
                + " t where t.endOfLife = false order by t.name").list();
    }

    public List<CampaignGroup> getGroups() {
        return getCurrentSession().createQuery("from " + CampaignGroupImpl.class.getName()
                + " t order by t.name").list();
    }

    public List<CampaignType> findTypes() {
        return getCurrentSession().createQuery("from " + CampaignTypeImpl.class.getName()
                + " t where t.endOfLife = false order by t.name").list();
    }

    public List<CampaignType> getTypes() {
        return getCurrentSession().createQuery("from " + CampaignTypeImpl.class.getName()
                + " t order by t.name").list();
    }

    public List<Campaign> findAllCampaigns() {
        return getCurrentSession().createQuery("from " + CampaignImpl.class.getName()
                + " t order by t.name").list();
    }

    public List<Campaign> findSelectableCampaigns() {
        return getCurrentSession().createQuery("from " + CampaignImpl.class.getName()
                + " t where t.endOfLife = false and "
                + "((t.parent = false) or (t.parent = false and t.reference is null)) order by t.name")
                .list();
    }

    public List<Campaign> findSelectedCampaigns() {
        List<Long> ids = (List<Long>) jdbcTemplate.query(
                "SELECT distinct origin_id FROM " + requestsTable + " WHERE status_id >= 0 AND status_id < 100",
                getLongRowMapper());
        if (ids.isEmpty()) {
            return new java.util.ArrayList<Campaign>();
        }
        return getCurrentSession().createQuery("from " + CampaignImpl.class.getName()
                + " t where t.id in (" + StringUtil.createCommaSeparated(ids)
                + ") order by t.name").list();
    }

    public boolean isReferenced(Long campaignId) {
        if (campaignId == null) {
            throw new IllegalArgumentException("campaignId must not be null");
        }
        return (jdbcTemplate.queryForObject("SELECT count(*) FROM " + requestsTable + " WHERE origin_id = "
                + campaignId, Long.class) > 0);
    }

    public Campaign getCampaign(Long id) {
        return (Campaign) getCurrentSession().load(CampaignImpl.class.getName(), id);
    }

    public List<Campaign> getCampaigns() {
        return getCurrentSession().createQuery("from " + CampaignImpl.class.getName()
                + " t where t.reference is null order by t.name").list();
    }

    public List<Campaign> getCampaigns(Long parent) {
        return getCurrentSession().createQuery("from " + CampaignImpl.class.getName()
                + " t where t.reference = :referenceId order by t.name")
                .setParameter("referenceId", parent).list();
    }

    public List<Option> getAvailableParents(Campaign campaign) {
        if (campaign.getReference() != null) {
            // exclude current parent
            return (List<Option>) jdbcTemplate.query(
                    "SELECT id,name FROM "
                            + campaignTable
                            + " WHERE reference_id IS NULL AND NOT end_of_life AND id <> "
                            + campaign.getReference()
                            + " AND id NOT IN (SELECT origin_id FROM "
                            + requestsTable
                            + " WHERE origin_id IS NOT NULL) ORDER BY name",
                    getOptionRowMapper());
        }
        // exclude campaign itself
        return (List<Option>) jdbcTemplate.query(
                "SELECT id,name FROM "
                        + campaignTable
                        + " WHERE reference_id IS NULL AND NOT end_of_life AND id <> "
                        + campaign.getId()
                        + " AND id NOT IN (SELECT origin_id FROM "
                        + requestsTable
                        + " WHERE origin_id IS NOT NULL) ORDER BY name",
                getOptionRowMapper());
    }

    public void delete(Campaign campaign) {
        getCurrentSession().delete(CampaignImpl.class.getName(), campaign);
        if (!campaign.isParent() && campaign.getReference() != null
                && getCampaigns(campaign.getReference()).isEmpty()) {
            Campaign parent = getCampaign(campaign.getReference());
            parent.setParent(false);
            save(parent);
        }
    }

    public void save(Campaign campaign) {
        getCurrentSession().saveOrUpdate(CampaignImpl.class.getName(), campaign);
    }

    public void save(CampaignType campaignType) {
        getCurrentSession().saveOrUpdate(CampaignTypeImpl.class.getName(), campaignType);
    }

    public void save(CampaignGroup campaignGroup) {
        getCurrentSession().saveOrUpdate(CampaignGroupImpl.class.getName(), campaignGroup);
    }
}
