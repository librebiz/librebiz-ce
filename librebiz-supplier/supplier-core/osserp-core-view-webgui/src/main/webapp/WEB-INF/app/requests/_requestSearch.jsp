<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="requestSearchView"/>
<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="businessCaseType"><fmt:message key="type"/></th>
                <th class="businessCaseId"><v:sortLink key="id"><fmt:message key="number"/></v:sortLink></th>
                <th class="businessCaseName"><v:sortLink key="name"><fmt:message key="commission"/></v:sortLink></th>
                <th class="businessCaseDate"><v:sortLink key="created"><fmt:message key="from"/></v:sortLink></th>
                <th class="businessCaseAmount"><v:sortLink key="capacity"><fmt:message key="value"/></v:sortLink></th>
                <th class="branchKey"><v:sortLink key="branchKey"><fmt:message key="branchOfficeShortcut"/></v:sortLink></th>
                <th class="employeeKey"><v:sortLink key="salesKey"><fmt:message key="salesShortcut"/></v:sortLink></th>
                <th class="orderProbability" title="orderProbabilityPercentage"><v:sortLink key="orderProbability"><fmt:message key="orderProbabilityColumnLabel"/></v:sortLink></th>
                <th class="businessCaseDate"><v:sortLink key="orderProbabilityDate"><fmt:message key="expected"/></v:sortLink></th>
                <th class="businessCaseStatus"><v:sortLink key="status">%</v:sortLink></th>
                <o:permission role="notes_requests_deny" info="permissionNotes">
                <th class="center" colspan="3"><fmt:message key="info"/></th>
                </o:permission>
                <o:forbidden role="notes_requests_deny" info="permissionNotes">
                <th class="center" colspan="2"><fmt:message key="info"/></th>
                </o:forbidden>
            </tr>
        </thead>
        <tbody>
            <c:if test="${empty view.list}">
                <tr>
                    <td colspan="11" class="errortext"><fmt:message key="noRequestFound"/></td>
                </tr>
            </c:if>
            <c:forEach var="request" items="${view.list}" varStatus="s">
                <tr id="request${request.id}"<c:if test="${request.stopped}"> class="red"</c:if>>
                    <td class="businessCaseType">
                        <o:out value="${request.type.key}"/>
                    </td>
                    <td class="businessCaseId">
                        <v:link url="/requests/requestSearch/select?id=${request.id}&exitId=request${request.id}" rawtitle="${request.type.name}">
                            <o:out value="${request.id}"/>
                        </v:link>
                    </td>
                    <td class="businessCaseName">
                        <v:link url="/requests/requestSearch/select?id=${request.id}&exitId=request${request.id}" rawtitle="${request.type.name}">
                            <o:out value="${request.name}" limit="48"/>
                        </v:link>
                    </td>
                    <td class="businessCaseDate"><o:date value="${request.created}" casenull="-" addcentury="false"/></td>
                    <td class="businessCaseAmount"><o:number value="${request.capacity}" format="${request.type.capacityFormat}"/></td>
                    <td class="branchKey"><o:out limit="4" value="${request.branch.shortkey}"/></td>
                    <td class="employeeKey">
                        <c:if test="${!empty request.salesId and request.salesId > 0}">
                            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${request.salesId}">
                                <oc:employee initials="true" value="${request.salesId}" />
                            </v:ajaxLink>
                        </c:if>
                    </td>
                    <td class="orderProbability">
                        <o:out value="${request.orderProbability}"/>
                    </td>
                    <td class="businessCaseDate"><o:date value="${request.orderProbabilityDate}" casenull="-" addcentury="false"/></td>
                    <td class="businessCaseStatus">
                        <v:ajaxLink url="/sales/flowControlDisplay/forward?id=${request.id}" styleClass="boldtext" linkId="flowControlDisplayView">
                            <o:out value="${request.status}"/>
                        </v:ajaxLink>
                    </td>
                    <o:permission role="notes_requests_deny" info="permissionNotes">
                        <td class="icon center">
                            <c:choose>
                                <c:when test="${!empty request.lastNoteDate}">
                                    <v:ajaxLink url="/sales/businessCaseDisplay/forward?notes=true&id=${request.id}" linkId="businessCaseDisplayView" title="notes">
                                        <span title="<o:date value="${request.lastNoteDate}" casenull="-" addcentury="false"/>"><o:img name="texteditIcon"/></span>
                                    </v:ajaxLink>
                                </c:when>
                                <c:otherwise>
                                    <o:img name="nosmileIcon" title="noNotesAvailable"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </o:permission>
                    <td class="icon center">
                        <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${request.id}" linkId="businessCaseDisplayView" title="shortinfo">
                            <o:img name="openFolderIcon"/>
                        </v:ajaxLink>
                    </td>
                    <td class="icon center">
                        <c:choose>
                            <c:when test="${request.appointmentAvailable}">
                                <v:link url="/requests/requestSearch/select?id=${request.id}" rawtitle="${request.type.name}">
                                    <o:img name="clockIcon"/>
                                </v:link>
                            </c:when>
                            <c:otherwise>
                                <v:ajaxLink url="/events/appointmentCreator/forward?id=${request.id}&type=4&view=${view.name}" linkId="appointmentCreator" title="appointmentCreate">
                                    <o:img name="bellIcon" />
                                </v:ajaxLink>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<o:removeErrors/>