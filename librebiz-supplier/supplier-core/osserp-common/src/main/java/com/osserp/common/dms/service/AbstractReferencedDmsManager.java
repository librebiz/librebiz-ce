/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 4, 2009 7:55:25 PM 
 * 
 */
package com.osserp.common.dms.service;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dms.DmsConfig;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentTypes;
import com.osserp.common.dms.dao.ReferencedDocuments;
import com.osserp.common.util.FileUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractReferencedDmsManager extends AbstractDmsManager implements DmsManager {
    private DocumentTypes documentTypes;

    protected AbstractReferencedDmsManager(ReferencedDocuments documents) {
        super(documents);
    }

    protected AbstractReferencedDmsManager(ReferencedDocuments documents, DocumentTypes documentTypes) {
        super(documents);
        this.documentTypes = documentTypes;
    }

    public DmsConfig getDmsConfig() {
        return getReferencedDocuments().getDmsConfig(false);
    }

    /**
     * Override method if implementing documentType supports assignment of
     * external documents imported by documentImport.
     * @param user
     * @param reference
     * @param document
     * @throws IllegalStateException if method invoked but not overridden by
     * implementing class (e.g. throws exception by default). 
     */
    public void assignDocument(
            User user, 
            DmsReference reference, 
            DmsDocument document) throws ClientException {
        
        throw new IllegalStateException("selected dmsManager does not support assigning imports");
    }

    /**
     * Indicates if implementing manager supports provided type.
     * Method will be invoked by getDocumentTypes method.
     * @param documentType
     * @return true if documentType is suppported.
     */
    protected abstract boolean isResponsibleFor(DocumentType documentType);

    public final List<DocumentType> getDocumentTypes() {
        List<DocumentType> result = new java.util.ArrayList<DocumentType>();
        if (documentTypes != null) {
            List<DocumentType> list = documentTypes.getList();
            for (int i = 0, j = list.size(); i < j; i++) {
                DocumentType type = list.get(i);
                if (isResponsibleFor(type)) {
                    result.add(type);
                }
            }
        }
        return result;
    }

    public DocumentType getDocumentType(Long id) {
        return getReferencedDocuments().getDocumentType(id);
    }

    public List<DmsDocument> findByReference(DmsReference reference) {
        return getReferencedDocuments().findByReference(reference);
    }

    public DmsDocument findReferenced(DmsReference reference) {
        List<DmsDocument> list = findByReference(reference);
        for (int i = 0, j = list.size(); i < j; i++) {
            DmsDocument doc = list.get(i);
            if (doc.isReferenceDocument()) {
                return doc;
            }
        }
        return null;
    }

    public int getCountByReference(DmsReference reference) {
        return getReferencedDocuments().countByReference(reference);
    }

    public DmsDocument createDocument(
            User user,
            DmsReference reference,
            FileObject file,
            String note,
            Date source,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId)
        throws ClientException {
        return getReferencedDocuments().create(
                user, reference, file, note, source,
                categoryId, validFrom, validTil, messageId);
    }

    public DmsDocument createDocument(
            User user,
            DmsReference reference,
            String fileName,
            String absoluteFile,
            String note,
            Date source,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId)
        throws ClientException {
        return getReferencedDocuments().create(
                user, reference, fileName, absoluteFile, note, source,
                categoryId, validFrom, validTil, messageId);
    }

    public void deleteDocument(DmsDocument doc) {
        getReferencedDocuments().deleteDocument(doc);
    }

    public void deleteDocuments(String messageId) {
        List<DmsDocument> list = getReferencedDocuments().findByMessageId(messageId);
        if (!list.isEmpty()) {
            for (int i = 0, j = list.size(); i < j; i++) {
                DmsDocument doc = list.get(i);
                getReferencedDocuments().deleteDocument(doc);
            }
        }
    }

    public void exportDocument(DmsDocument doc, String exportFile) throws ClientException {
        try {
            DmsConfig config = getReferencedDocuments().getDmsConfig(false); 
            String root = config.getRoot();
            getReferencedDocuments().export(doc, FileUtil.createFileName(root, exportFile));
            
        } catch (Exception e) {
            throw new ClientException(e.getMessage(), e);
        }
    }

    public void deleteExportedDocument(String exportFile) throws ClientException {
        try {
            DmsConfig config = getReferencedDocuments().getDmsConfig(false); 
            String root = config.getRoot();
            getReferencedDocuments().deleteExport(FileUtil.createFileName(root, exportFile));
        } catch (Exception e) {
            throw new ClientException(e.getMessage(), e);
        }
    }

    public DmsDocument getReferenceDocument(DmsReference reference) throws ClientException {
        return getReferencedDocuments().getReferenceDocument(reference);
    }

    protected ReferencedDocuments getReferencedDocuments() {
        return (ReferencedDocuments) getDocumentsDao();
    }
}
