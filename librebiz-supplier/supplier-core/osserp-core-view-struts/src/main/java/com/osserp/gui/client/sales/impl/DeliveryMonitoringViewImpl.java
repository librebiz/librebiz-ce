/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 6, 2007 12:00:10 PM 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.ViewName;

import com.osserp.core.projects.ProjectSearch;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.sales.SalesListProductItem;
import com.osserp.core.users.DomainUser;

import com.osserp.gui.client.products.impl.AbstractStockSelectingView;
import com.osserp.gui.client.sales.DeliveryMonitoringView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("deliveryMonitoringView")
public class DeliveryMonitoringViewImpl extends AbstractStockSelectingView
        implements DeliveryMonitoringView {

    private List<DeliveryMonitoringListItem> monitoringBySales = new ArrayList<DeliveryMonitoringListItem>();
    private boolean monitoringBySalesMode = true;
    private boolean snipListMode = false;
    private SalesListItem sales = null;
    private boolean listDescendantMode = false;
    private boolean listVacantMode = false;

    protected DeliveryMonitoringViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        refresh();
    }

    public List<DeliveryMonitoringListItem> getMonitoringBySales() {
        return monitoringBySales;
    }

    public boolean isMonitoringBySalesMode() {
        return monitoringBySalesMode;
    }

    public void setMonitoringBySalesMode(boolean monitoringBySalesMode) {
        this.monitoringBySalesMode = monitoringBySalesMode;
    }

    public boolean isSnipListMode() {
        return snipListMode;
    }

    public void setSnipListMode(boolean snipListMode) {
        this.snipListMode = snipListMode;
    }

    public void snipList(Long id) {
        List<SalesListProductItem> deliveries = getList();
        for (Iterator<SalesListProductItem> i = deliveries.iterator(); i.hasNext();) {
            SalesListProductItem next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                break;
            }
        }
        for (Iterator<DeliveryMonitoringListItem> i = monitoringBySales.iterator(); i.hasNext();) {
            DeliveryMonitoringListItem next = i.next();
            if (next.getItem().getId().equals(id)) {
                i.remove();
                break;
            }
        }
    }

    public SalesListItem getSales() {
        return sales;
    }

    public boolean isListDescendantMode() {
        return listDescendantMode;
    }

    public void toggleListDescendantMode() {
        this.listDescendantMode = !this.listDescendantMode;
        refresh();
    }

    public boolean isListVacantMode() {
        return listVacantMode;
    }

    public void toggleListVacantMode() {
        this.listVacantMode = !this.listVacantMode;
        refresh();
    }

    @Override
    public void selectStock(Long id) {
        super.selectStock(id);
        refresh();
    }

    public void refresh() {
        if (getSelectedStock() != null) {
            ProjectSearch salesSearch = getSalesSearch();
            List<SalesListProductItem> items = salesSearch.getDeliveryMonitoring(
                    getSelectedStock().getId(),
                    listDescendantMode);
            
            DomainUser domainUser = getDomainUser();
            for (Iterator<SalesListProductItem> i = items.iterator(); i.hasNext();) {
                SalesListProductItem next = i.next();
                if (next.getBranch() != null) {
                    if (!domainUser.isBranchAccessible(next.getBranch())) {
                        i.remove();
                    }
                }
            }
            
            if (!listVacantMode) {
                for (Iterator<SalesListProductItem> i = items.iterator(); i.hasNext();) {
                    SalesListProductItem next = i.next();
                    if (!next.isReleased()) {
                        i.remove();
                    }
                }
            }
            monitoringBySales = createMontoringBySales(items);
            setList(items);
        }
    }

    private List<DeliveryMonitoringListItem> createMontoringBySales(List<SalesListProductItem> items) {
        List<DeliveryMonitoringListItem> deliveryMonitoring = new ArrayList<DeliveryMonitoringListItem>();
        Set<Long> added = new HashSet<Long>();
        for (int i = 0, j = items.size(); i < j; i++) {
            SalesListProductItem nextItem = items.get(i);
            if (!added.contains(nextItem.getId())) {
                DeliveryMonitoringListItem listItem = new DeliveryMonitoringListItem(nextItem);
                deliveryMonitoring.add(listItem);
                added.add(nextItem.getId());
            }
            addItem(deliveryMonitoring, nextItem);
        }
        return deliveryMonitoring;
    }

    private void addItem(List<DeliveryMonitoringListItem> deliveryMonitoring, SalesListProductItem nextItem) {
        for (int i = 0, j = deliveryMonitoring.size(); i < j; i++) {
            DeliveryMonitoringListItem next = deliveryMonitoring.get(i);
            if (next.getItem().getId().equals(nextItem.getId())) {
                next.addPosition(nextItem);
                break;
            }
        }
    }

    private ProjectSearch getSalesSearch() {
        return (ProjectSearch) getService(ProjectSearch.class.getName());
    }
}
