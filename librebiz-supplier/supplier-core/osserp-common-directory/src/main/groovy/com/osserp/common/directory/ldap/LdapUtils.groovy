/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.directory.ldap.impl.FetchmailLdapManagerImpl
import com.osserp.common.directory.ldap.impl.LdapAdminManagerImpl
import com.osserp.common.directory.ldap.impl.LdapManagerImpl
import com.osserp.common.util.StringUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class LdapUtils {
    private static Logger logger = LoggerFactory.getLogger(LdapUtils.class.getName())

    /**
     * Fetchs the client name from dn if exists.
     * @param dn
     * @return client name or null
     */
    static String fetchClientName(String dn) {
        String result = null
        if (dn?.indexOf('ou=clients') > -1) {
            def mappedProperties = StringUtil.getPropertyMap(dn)
            result = mappedProperties['cn']
        }
        if (!result) {
            logger.warn("fetchClientName: not found [dn=${dn}]")
        }
        return result
    }

    static String createClientNocDn(String noNocDn) {
        if (!noNocDn || noNocDn.indexOf('dc=noc') > -1) {
            // dn is null or already noc dn
            return noNocDn
        }
        String[] dnComponents = StringUtil.getTokenArray(noNocDn)
        StringBuilder dnbuffer = new StringBuilder()
        dnComponents.each {
            if (dnbuffer.length() > 0) {
                dnbuffer.append(',')
            }
            if ('ou=clients' == it) {
                dnbuffer.append(it).append(',dc=noc')
            } else {
                dnbuffer.append(it)
            }
        }
        dnbuffer.toString()
    }

    /**
     * Fetches all array members of a ldap entry
     * @param ldapObject
     * @param name
     * @return list of array members
     */
    static List fetchMembers(Map ldapObject, String name) {
        def result = []
        def members = ldapObject[name]
        if (members) {
            if (members instanceof String) {
                result << members
            } else {
                result = members
            }
        }
        return result
    }

    /**
     * Adds the object classes of provided schema to objectclass property of ldapObject
     * @param schema with objectClasses to add
     * @param ldapObject where to add the object classes
     * @return modified ldapObject
     */
    static Map addObjectClasses(Schema schema, Map ldapObject) {
        List objectClasses = ldapObject.objectclass ?: []
        schema.objectClasses.each { objectClasses << it }
        ldapObject.objectclass = objectClasses.unique()
        return ldapObject
    }

    /**
     * Adds the object classes of provided schema to objectclass property of ldapObject
     * @param schema with objectClasses to add
     * @param ldapObject where to add the object classes
     * @return modified ldapObject
     */
    static Map updateObjectClasses(Schema schema, Map values, Map ldapObject) {
        List objectClasses = values.objectclass ?: ldapObject.objectclass ?: []
        schema.objectClasses.each { objectClasses << it }
        values.objectclass = objectClasses.unique()
        return values
    }

    /**
     * Checks if a schema is supported by the objectclass property of a value map.
     * The schema should be a subschema with objectclasses unique for that schema.
     * @param schema to check for.
     * @param values with objectclass property (e.g. ldapObject)
     * @return true if any schema of schema.objectClasses was found in values.objectclass
     */
    static boolean schemaActivated(Schema schema, Map values) {
        boolean result = false
        List schemaClasses = schema.objectClasses
        if (schemaClasses && values.objectclass instanceof List) {
            values.objectclass.each { so ->
                String existing = schemaClasses.find { it == so }
                if (existing) {
                    result = true
                }
            }
        }
        result
    }

    /**
     * Removes all keys with null values
     * @param values
     * @return map without null values
     */
    static Map clearNull(Map values) {
        Map result = [:]
        values.each { key, value ->
            if (value) {
                result[key] = value
            }
        }
        return result
    }

    /**
     * Creates a new ldapManager instance by property file
     * @param propertiesFile
     * @return ldapManager
     */
    static LdapManager createLdapManager(String propertiesFile) {
        new LdapManagerImpl(propertiesFile)
    }

    /**
     * Creates a new ldapAdminManager instance by property file
     * @param propertiesFile
     * @return ldapAdminManager
     */
    static LdapAdminManager createLdapAdminManager(String propertiesFile) {
        new LdapAdminManagerImpl(propertiesFile)
    }

    /**
     * Creates a new fetchmailLdapManager instance by property file
     * @param propertiesFile
     * @return fetchmailLdapManager
     */
    static FetchmailLdapManager createFetchmailLdapManager(String propertiesFile) {
        new FetchmailLdapManagerImpl(propertiesFile)
    }

    /**
     * Creates a new ldapManager instance by property file
     * @param config
     * @return ldapManager
     */
    static LdapManager createLdapManager(LdapConfig config) {
        new LdapManagerImpl(config)
    }

    /**
     * Creates a new ldapAdminManager instance by property file
     * @param config
     * @return ldapAdminManager
     */
    static LdapAdminManager createLdapAdminManager(LdapConfig config) {
        new LdapAdminManagerImpl(config)
    }

    /**
     * Creates a new ldapAdminManager instance by property file
     * @param config
     * @return ldapAdminManager
     */
    static LdapAdminManager createLdapAdminManager(LdapManager ldapManager) {
        new LdapAdminManagerImpl(ldapManager)
    }

    /**
     * Creates a new fetchmailLdapManager instance by property file
     * @param config
     * @return fetchmailLdapManager
     */
    static FetchmailLdapManager createFetchmailLdapManager(LdapConfig config) {
        new FetchmailLdapManagerImpl(config)
    }
}
