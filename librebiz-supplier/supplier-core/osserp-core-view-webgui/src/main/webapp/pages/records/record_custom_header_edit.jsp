<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${view.customHeaderSupported}">
    <tr>
        <td><fmt:message key="recordHeaderCustomLabel"/></td>
        <td><input type="text" name="customHeader" class="recordInput" value="<o:out value="${record.customHeader}"/>"/></td>
    </tr>
</c:if>
