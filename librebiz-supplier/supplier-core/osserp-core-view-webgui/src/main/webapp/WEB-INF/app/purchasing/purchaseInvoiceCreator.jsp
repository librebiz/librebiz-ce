<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>

<v:view viewName="purchaseInvoiceCreatorView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="headline">
		<fmt:message key="purchaseInvoiceCreatorView"/>
        <c:if test="${!empty view.selectedType}"> - <o:out value="${view.selectedType.name}"/></c:if>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area">
            <div class="row">
                <c:choose>
                    <c:when test="${!empty view.selectedBranch and !empty view.selectedType and view.supplierRequired
                            and !empty view.supplierPreselection and empty view.supplier}">
                        <c:import url="_purchaseInvoiceSupplierSelection.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <%-- 1. branch selection --%>
                            <c:when test="${empty view.selectedBranch}">
                                <c:import url="_purchaseInvoiceBranchSelection.jsp"/>
                            </c:when>
                            <%-- 2. invoice type selection --%>
                            <c:when test="${empty view.selectedType}">
                                <c:import url="_purchaseInvoiceTypeSelection.jsp"/>
                            </c:when>
                            <%-- 3.1 supplier selection via search --%>
                            <c:when test="${empty view.supplier and view.supplierRequired}">
                                <c:redirect url="${view.supplierSearchForward}"/>
                            </c:when>
                            <%-- 5. invoice created, forward to display --%>
                            <c:when test="${!empty view.bean}">
                                <c:redirect url="${view.invoiceCreatedForward}"/>
                            </c:when>
                            <%-- 4. display summary and provide create action --%>
                            <c:when test="${!empty view.supplier or !view.supplierRequired}">
                                <c:import url="_purchaseInvoiceCreateSummary.jsp"/>
                            </c:when>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
	</tiles:put>
</tiles:insert>