/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 16, 2010 9:27:42 AM 
 * 
 */
package com.osserp.core.service.system;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.dms.DmsConfig;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.util.FileUtil;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.Contact;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.employees.Employee;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.ManagingDirector;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.system.SystemCompanyManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SystemCompanyManagerImpl extends AbstractService implements SystemCompanyManager {
    private static Logger log = LoggerFactory.getLogger(SystemCompanyManagerImpl.class.getName());

    private final static String LOGO_PATH = "/clients/logos/";
    
    private Contacts contacts = null;
    private SystemCompanies companiesDao = null;
    private DmsManager dmsManager;

    public SystemCompanyManagerImpl(SystemCompanies companies, Contacts contacts, DmsManager dmsManager) {
        super();
        this.companiesDao = companies;
        this.contacts = contacts;
        this.dmsManager = dmsManager;
    }

    public SystemCompany createPoposal(Contact contact) throws ClientException {
        return companiesDao.createCompanySuggestion(contact);
    }

    public SystemCompany create(SystemCompany company) throws ClientException {
        SystemCompany client = companiesDao.createCompany(company);
        contacts.activateRelation(client.getContact(), Contact.CLIENT);
        return client;
    }

    public List<SystemCompany> getAll() {
        return companiesDao.getAll();
    }

    public SystemCompany find(Long id) {
        if (id == null) {
            throw new BackendException("find", "invoked with id param null");
        }
        return companiesDao.find(id);
    }

    public SystemCompany addManagingDirector(Employee user, SystemCompany company, String directorName) throws ClientException {
        company.addManagingDirector(user, directorName);
        companiesDao.save(company);
        return companiesDao.getCompany(company.getId());
    }

    public SystemCompany removeManagingDirector(SystemCompany company, Long directorId) throws ClientException {
        for (Iterator<ManagingDirector> i = company.getManagingDirectors().iterator(); i.hasNext();) {
            ManagingDirector next = i.next();
            if (next.getId().equals(directorId)) {
                i.remove();
                companiesDao.save(company);
                break;
            }
        }
        return companiesDao.getCompany(company.getId());
    }

    public SystemCompany renameManagingDirector(SystemCompany company, Long directorId, String directorName) throws ClientException {
        for (Iterator<ManagingDirector> i = company.getManagingDirectors().iterator(); i.hasNext();) {
            ManagingDirector next = i.next();
            if (next.getId().equals(directorId)) {
                next.setName(directorName);
                companiesDao.save(company);
                break;
            }
        }
        return companiesDao.getCompany(company.getId());
    }

    public List<BankAccount> getBankAccounts() {
        return companiesDao.getBankAccounts();
    }

    public SystemCompany addBankAccount(
            Employee user,
            SystemCompany company,
            String shortkey,
            String bankName,
            String bankNameAddon,
            String bankAccountNumberIntl,
            String bankIdentificationCodeIntl,
            boolean useInDocumentFooter,
            boolean taxAccount) throws ClientException {
        company.addBankAccount(
                user,
                shortkey,
                bankName,
                bankNameAddon,
                bankAccountNumberIntl,
                bankIdentificationCodeIntl,
                useInDocumentFooter,
                taxAccount);
        companiesDao.save(company);
        return companiesDao.getCompany(company.getId());
    }
    
    public BankAccount getBankAccount(Long id) {
        return companiesDao.getBankAccount(id);
    }

    public SystemCompany removeBankAccount(SystemCompany company, Long id) throws ClientException {
        for (Iterator<BankAccount> i = company.getBankAccounts().iterator(); i.hasNext();) {
            BankAccount next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                companiesDao.save(company);
                break;
            }
        }
        return companiesDao.getCompany(company.getId());
    }

    public void assignBankAccount(Long bankAccountId, Long supplierId) {
        companiesDao.assignBankAccount(bankAccountId, supplierId);
    }

    public SystemCompany updateBankAccount(
            SystemCompany company,
            Long id,
            String shortkey,
            String bankName,
            String bankNameAddon,
            String bankAccountNumberIntl,
            String bankIdentificationCodeIntl,
            boolean useInDocumentFooter,
            boolean taxAccount)
            throws ClientException {
        for (Iterator<BankAccount> i = company.getBankAccounts().iterator(); i.hasNext();) {
            BankAccount next = i.next();
            if (next.getId().equals(id)) {
                next.setName(bankName);
                next.setNameAddon(bankNameAddon);
                next.setBankAccountNumberIntl(bankAccountNumberIntl);
                next.setBankIdentificationCodeIntl(bankIdentificationCodeIntl);
                next.setShortkey(shortkey);
                next.setUseInDocumentFooter(useInDocumentFooter);
                next.setTaxAccount(taxAccount);
                companiesDao.save(company);
                break;
            }
        }
        return companiesDao.getCompany(company.getId());
    }

    public SystemCompany update(
            Long id,
            String name,
            String letterWindowName,
            String localCourt,
            String tradeRegisterEntry,
            String taxId,
            String vatId,
            Double internalMargin) throws ClientException {
        SystemCompany company = companiesDao.getCompany(id);
        company.setName(name);
        company.setLetterWindowName(letterWindowName);
        company.setLocalCourt(localCourt);
        company.setTradeRegisterEntry(tradeRegisterEntry);
        company.setVatId(vatId);
        company.setTaxId(taxId);
        company.setInternalMargin(internalMargin);
        companiesDao.save(company);
        return companiesDao.getCompany(id);
    }

    public void updateDocumentFlag(SystemCompany company, Long docType, boolean available) throws ClientException {
        if (docType.equals(SystemCompany.COMPANY_LOGO)) {
            company.setLogoAvailable(available);
            companiesDao.save(company);
            return;
        }
        if (docType.equals(SystemCompany.COMPANY_REGISTRATION)) {
            company.setCompanyRegistrationAvailable(available);
            companiesDao.save(company);
        }
    }

    public void deployLogo(SystemCompany company, DmsDocument document) throws ClientException {
        String filename = createLogoPathAndName(company, document.getFileName());
        dmsManager.exportDocument(document, filename);
        company.setLogoAvailable(true);
        companiesDao.save(company);
        if (log.isDebugEnabled()) {
            log.debug("deployLogo() done [relativePath=" + filename + "]");
        }
    }

    public void removeLogo(SystemCompany company, String sourceFilename) throws ClientException {
        String filename = createLogoPathAndName(company, sourceFilename);
        dmsManager.deleteExportedDocument(filename);
        company.setLogoAvailable(false);
        companiesDao.save(company);
        if (log.isDebugEnabled()) {
            log.debug("removeLogo() done [relativePath=" + filename + "]");
        }
    }
    
    public String getLogoPath(BranchOffice office) {
        DmsConfig config = dmsManager.getDmsConfig();
        if (config == null || config.getRoot() == null) {
            log.warn("getLogoPath() dmsConfig not exists or dmsConfig.root not set");
            return null;
        }
        StringBuilder buffer = new StringBuilder(config.getRoot());
        
        if (office.isLogoAvailable() && isSet(office.getLogoPath())) {
            buffer.append(LOGO_PATH).append(office.getLogoPath());
        } else {
            buffer.append(createLogoPathAndName(office.getCompany(), "example.jpg"));
        }
        if (log.isDebugEnabled()) {
            log.debug("getLogoPath() logo found [branch=" + office.getId() + 
                    ", logo=" + buffer.toString()  + "]");
        }
        return buffer.toString();
    }
    
    private String createLogoPathAndName(SystemCompany company, String sourceFilename) {
        assert company != null;
        StringBuilder pathAndName = new StringBuilder(LOGO_PATH);
        pathAndName
            .append("company-")
            .append(company.getId())
            .append(".")
            .append(FileUtil.getFileType(sourceFilename));
        return pathAndName.toString();
    }
}
