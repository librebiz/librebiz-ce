/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 22, 2005 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.TargetDate;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class TargetDateImpl extends AbstractEntity implements TargetDate {
    private static final long serialVersionUID = 42L;

    private Date date = null;

    protected TargetDateImpl() {
        super();
    }

    public TargetDateImpl(
            Long reference,
            Long createdBy,
            Date date) {

        super(null, reference, createdBy);
        this.date = date;
    }

    public TargetDateImpl(
            Long id,
            Long reference,
            Date date,
            Date created,
            Long createdBy) {

        super(id, reference, created, createdBy);
        this.date = date;
    }

    /**
     * Returns the installation date
     * @return date.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the installation date
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("date", date));
        return root;
    }
}
