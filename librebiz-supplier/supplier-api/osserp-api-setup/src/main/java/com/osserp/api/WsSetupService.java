/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 8, 2016 
 * 
 */
package com.osserp.api;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public interface WsSetupService {
    
    /**
     * Indicates if setup mode is enabled.
     * @return true if setup mode is enabled
     */
    boolean isSetupMode();
    
    /**
     * Indicates if initial setup is closed.
     * @return true if setup is closed
     */
    boolean isSetupClosed();
    
    /**
     * Provides initial or current setup data. 
     * @return setupData 
     */
    WsSetupData getSetupData();

    /**
     * Finally setup the application and initial configuration. Invocation of
     * this method terminates the setup procedure and leaves the instance ready
     * to use. Any other settings can be done inside the app. 
     * @param data
     * @return final setup object
     * @throws WSObjectException if validation or setup failed
     */
    WsSetupData setup(WsSetupData data) throws WSObjectException;

    /**
     * Setup administrator loginname and password.
     * @param data
     * @param uid
     * @param password
     * @param confirmPassword
     * @return final setup object
     * @throws WSObjectException if validation or setup failed
     */
    WsSetupData setupAccount(WsSetupData data, String uid, String password, String confirmPassword) throws WSObjectException;

    /**
     * Just persists the setup data object without any specific validation. 
     * You may use this method during the setup process to save user input.
     * @param data
     * @param status an optional status to set
     * @return setupData 
     * @throws WSObjectException if validation failed
     */
    WsSetupData update(WsSetupData data, String status) throws WSObjectException;
    
    /**
     * Updates a single property
     * @param name the name of the property 
     * @param value the value of the property
     * @return setup data object with updated property
     */
    WsSetupData updateProperty(String name, String value);
    
    /**
     * Removes an existing property
     * @param name
     * @return setup data object without property
     */
    WsSetupData removeProperty(String name);
    
    /**
     * Removes all previous input to start setup again
     * @return initial setup object
     */
    WsSetupData resetSetup();

    /**
     * Updates setup status
     * @param status
     * @return setup data object with updated property
     */
    WsSetupData updateStatus(String status);
    
}
