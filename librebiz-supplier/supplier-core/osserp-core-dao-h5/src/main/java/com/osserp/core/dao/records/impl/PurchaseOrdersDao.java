/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 14, 2006 11:21:37 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateUtil;

import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.PurchaseOrderDeliveryDateChangedDeleted;
import com.osserp.core.model.records.PurchaseOrderDeliveryDateChangedInfoImpl;
import com.osserp.core.model.records.PurchaseOrderDeliveryImpl;
import com.osserp.core.model.records.PurchaseOrderImpl;
import com.osserp.core.model.records.PurchaseOrderItemChangedDeleted;
import com.osserp.core.model.records.PurchaseOrderItemChangedInfoImpl;
import com.osserp.core.model.records.PurchaseOrderTypeImpl;
import com.osserp.core.products.Product;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseOrdersDao extends AbstractPurchaseRecords implements PurchaseOrders {
    private static Logger log = LoggerFactory.getLogger(PurchaseOrdersDao.class.getName());

    private String purchaseOrderView = null;
    private String deliveryTable = null;
    private ProductSummaryCache summaryCache = null;

    public PurchaseOrdersDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Suppliers suppliers,
            ProductSummaryCache summaryCache,
            String purchaseOrderViewKey,
            String purchaseOrderQueryViewKey) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                suppliers,
                TableKeys.PURCHASE_ORDERS,
                TableKeys.PURCHASE_ORDER_ITEMS);

        this.purchaseOrderView = getTable(purchaseOrderViewKey);
        this.deliveryTable = getTable(TableKeys.PURCHASE_DELIVERY_NOTES);
        this.summaryCache = summaryCache;
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.PURCHASE_ORDER;
    }

    public PurchaseOrder create(Employee user, Long company, Long branchId, ClassifiedContact contact, Long sale) {
        Supplier supplier = (Supplier) getSuppliers().find(contact.getId());
        RecordType recordType = getRecordType();
        PurchaseOrderImpl obj = new PurchaseOrderImpl(
                getNextRecordId(user, company, recordType, null),
                recordType,
                getDefaultBookingType(),
                company,
                branchId,
                supplier,
                user,
                sale,
                taxRate,
                reducedTaxRate);
        if (user == null) {
            obj.setCreatedBy(Constants.SYSTEM_EMPLOYEE);
        }
        saveInitial(obj);
        return obj;
    }

    public PurchaseOrder create(Employee user, PurchaseOrder order, List<Item> carryOver) {
        RecordType recordType = getRecordType();
        PurchaseOrderImpl obj = new PurchaseOrderImpl(
                getNextRecordId(user, order.getCompany(), recordType, null),
                (user == null ? null : user.getId()),
                order,
                carryOver);
        if (user == null) {
            obj.setCreatedBy(Constants.SYSTEM_EMPLOYEE);
        }
        saveInitial(obj);
        return obj;
    }

    public PurchaseOrder create(Employee user, PurchaseOrder order) {
        RecordType recordType = getRecordType();
        PurchaseOrderImpl obj = new PurchaseOrderImpl(
                getNextRecordId(user, order.getCompany(), recordType, null),
                (user == null ? null : user.getId()),
                order);
        if (user == null) {
            obj.setCreatedBy(Constants.SYSTEM_EMPLOYEE);
        }
        saveInitial(obj);
        return obj;
    }

    public PurchaseOrder getOrder(PurchaseInvoice invoice) {
        if (invoice != null && invoice.getReference() != null) {
            return (PurchaseOrder) load(invoice.getReference());
        }
        return null;
    }

    @Override
    public List<Record> getOpenReleased(Product product, boolean descendant) {
        List<Long> ids = findReleasedByProduct(product.getProductId(), descendant);
        List<Record> result = new ArrayList<Record>();
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add(load(ids.get(i)));
        }
        return result;
    }

    public List<RecordDisplay> findOpenByConfirmation(boolean confirmed) {
        StringBuilder sql = new StringBuilder("SELECT ");
        sql
                .append(RecordDisplayRowMapper.RECORD_VALUES)
                .append(" FROM v_")
                .append(getQueryContext())
                .append("_query WHERE status <= ")
                .append(Record.STAT_SENT);
        if (confirmed) {
            sql.append(" AND id NOT IN ");

        } else {
            sql.append(" AND id IN ");
        }
        sql
                .append("(SELECT reference_id FROM ")
                .append(getRecordItemTableName())
                .append(" WHERE delivery_ack = false AND reference_id IS NOT NULL) ORDER BY id DESC");
        List<Map<String, Object>> result = (List<Map<String, Object>>) jdbcTemplate.query(
                sql.toString(),
                RecordDisplayRowMapper.getReader());
        List<RecordDisplay> list = RecordDisplayRowMapper.createDisplayList(getOptionsCache(), result);
        if (log.isDebugEnabled()) {
            log.debug("findOpenByConfirmation: done, count: " + list.size() + ", sql:");
            log.debug(sql.toString());
        }
        return list;
    }

    public List<RecordDisplay> findOpenByContact(Long contactId) {
        StringBuilder sql = new StringBuilder("SELECT ");
        sql
                .append(RecordDisplayRowMapper.RECORD_VALUES)
                .append(" FROM v_")
                .append(getQueryContext())
                .append("_query WHERE status <= ")
                .append(Record.STAT_SENT)
                .append(" AND contact_id = ")
                .append(contactId)
                .append(" ORDER BY id DESC");
        List<Map<String, Object>> result = (List<Map<String, Object>>) jdbcTemplate.query(
                sql.toString(),
                RecordDisplayRowMapper.getReader());
        return RecordDisplayRowMapper.createDisplayList(getOptionsCache(), result);
    }

    private List<Long> findReleasedByProduct(Long product, boolean descendant) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT id FROM ")
                .append(getRecordTableName())
                .append(" WHERE status = ")
                .append(Record.STAT_SENT)
                .append(" AND id IN (SELECT reference_id FROM ")
                .append(getRecordItemTableName())
                .append(" WHERE product_id = ? AND reference_id IS NOT NULL)");
        if (descendant) {
            sql.append(" ORDER BY id");
        } else {
            sql.append(" ORDER BY id DESC");
        }
        return (List<Long>) jdbcTemplate.query(
                sql.toString(),
                new Object[] { product },
                new int[] { Types.BIGINT },
                getLongRowMapper());
    }

    public List<PurchaseOrder> getCarryover(PurchaseInvoice invoice) {
        StringBuilder query = new StringBuilder();
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.carryoverFrom = ")
                .append(invoice.getReference());
        return getCurrentSession().createQuery(query.toString()).list();
    }

    public List<DeliveryDateChangedInfo> getDeliveryDateChangedInfos(Order order) {
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(PurchaseOrderDeliveryDateChangedInfoImpl.class.getName())
                .append(" o where o.reference = :orderId order by o.product.productId, o.id");

        return getCurrentSession().createQuery(hql.toString())
                .setParameter("orderId", order.getId()).list();
    }

    protected void save(DeliveryDateChangedInfo info) {
        getCurrentSession().saveOrUpdate(PurchaseOrderDeliveryDateChangedInfoImpl.class.getName(), info);
    }

    public List<Option> findCurrentSuppliers() {
        StringBuilder sql = new StringBuilder("SELECT DISTINCT(contact_id), " +
                "get_supplier_name(contact_id) FROM ");
        sql
                .append(purchaseOrderView)
                .append(" WHERE status <= ")
                .append(Record.STAT_SENT);
        return (List<Option>) jdbcTemplate.query(sql.toString(), getOptionRowMapper());
    }

    public List<OrderItemsDisplay> getOpen(Long stockId, Long productId) {
        return (List<OrderItemsDisplay>) jdbcTemplate.query(
                RecordProcs.PURCHASE_ORDERS_OPEN_ITEMS_BY_STOCK_AND_PRODUCT,
                new Object[] { stockId, productId },
                new int[] { Types.BIGINT, Types.BIGINT },
                OrderItemsDisplayRowMapper.getReader(stockId, summaryCache, false, false));
    }

    public List<DeliveryDateChangedInfo> update(
            Employee user,
            Order record,
            Date deliveryDate,
            String deliveryNote,
            String itemBehaviour,
            boolean deliveryConfirmed)
            throws ClientException {

        if (!(record instanceof PurchaseOrderImpl)) {
            throw new BackendException("expected instance of PurchaseOrderImpl, "
                    + " found " + record.getClass().getName());
        }
        if (itemBehaviour == null || itemBehaviour.length() < 1) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        List<DeliveryDateChangedInfo> infoList = new ArrayList<DeliveryDateChangedInfo>();
        if (itemBehaviour.equals("resetItems")) {
            record.setDelivery(null);
            for (int i = 0, j = record.getItems().size(); i < j; i++) {
                OrderItem next = (OrderItem) record.getItems().get(i);
                DeliveryDateChangedInfo info = new PurchaseOrderDeliveryDateChangedInfoImpl(
                        user,
                        record,
                        next.getProduct(),
                        next.getQuantity(),
                        next.getStockId(),
                        next.getDelivery(),
                        null,
                        deliveryNote);
                infoList.add(info);
                next.setDelivery(null);
                next.setDeliveryConfirmed(false);
                next.setDeliveryDateBy(user.getId());
                next.setDeliveryNote(null);
            }
        } else {
            if (deliveryDate != null) {
                Date now = new Date(System.currentTimeMillis());
                if (deliveryDate.before(now) && !DateUtil.isSameDay(deliveryDate, now)) {
                    throw new ClientException(ErrorCode.DATE_PAST);
                }
                PurchaseOrderDeliveryImpl obj = new PurchaseOrderDeliveryImpl(
                        (PurchaseOrderImpl) record,
                        user,
                        deliveryDate);
                getCurrentSession().saveOrUpdate("PurchaseOrderDeliveryImpl", obj);
                record.setDelivery(deliveryDate);
                boolean allItems = itemBehaviour.equals("allItems");
                boolean empty = itemBehaviour.equals("emptyItems");
                for (int i = 0, j = record.getItems().size(); i < j; i++) {
                    OrderItem next = (OrderItem) record.getItems().get(i);
                    if (allItems || (empty && next.getDelivery() == null)) {
                        DeliveryDateChangedInfo info = new PurchaseOrderDeliveryDateChangedInfoImpl(
                                user,
                                record,
                                next.getProduct(),
                                next.getQuantity(),
                                next.getStockId(),
                                next.getDelivery(),
                                deliveryDate,
                                deliveryNote);
                        infoList.add(info);
                        next.setDelivery(deliveryDate);
                        next.setDeliveryConfirmed(deliveryConfirmed);
                        next.setDeliveryDateBy(user.getId());
                        next.setDeliveryNote(deliveryNote);
                    }
                }
            }
        }
        save(record);
        if (!infoList.isEmpty()) {
            for (Iterator<DeliveryDateChangedInfo> i = infoList.iterator(); i.hasNext();) {
                save(i.next());
            }
        }
        return infoList;
    }

    public DeliveryDateChangedInfo update(
            Employee user,
            Order order,
            Date estimatedDelivery,
            String deliveryNote,
            Item item)
            throws ClientException {
        Date now = new Date(System.currentTimeMillis());
        if (estimatedDelivery == null) {
            throw new ClientException(ErrorCode.DATE_MISSING);
        }
        if (estimatedDelivery.before(now) && !DateUtil.isSameDay(estimatedDelivery, now)) {
            throw new ClientException(ErrorCode.DATE_PAST);
        }
        DeliveryDateChangedInfo result = null;
        Date nextDeliveryDate = estimatedDelivery;
        for (int i = 0, j = order.getItems().size(); i < j; i++) {
            OrderItem next = (OrderItem) order.getItems().get(i);
            if (next.getId().equals(item.getId())) {
                result = new PurchaseOrderDeliveryDateChangedInfoImpl(
                        user,
                        order,
                        next.getProduct(),
                        next.getQuantity(),
                        next.getStockId(),
                        next.getDelivery(),
                        estimatedDelivery,
                        deliveryNote);
                next.setDelivery(estimatedDelivery);
                next.setDeliveryNote(deliveryNote);
                next.setDeliveryDateBy(user.getId());
            } else {
                if (next.getDelivery() != null
                        && next.getDelivery().before(nextDeliveryDate)
                        && next.getDelivery().after(now)) {
                    nextDeliveryDate = next.getDelivery();
                }
            }

        }
        order.setDelivery(nextDeliveryDate);
        save(order);
        if (result != null) {
            save(result);
        }
        return result;
    }

    public void closeOrder(Long recordId) {
        StringBuilder updateDeliveries = new StringBuilder();
        updateDeliveries
                .append("UPDATE ")
                .append(deliveryTable)
                .append(" SET status = ")
                .append(Record.STAT_CLOSED)
                .append(" WHERE reference_id = ?");
        jdbcTemplate.update(
                updateDeliveries.toString(),
                new Object[] { recordId },
                new int[] { Types.BIGINT });
        StringBuilder updateOrder = new StringBuilder();
        updateOrder
                .append("UPDATE ")
                .append(getRecordTableName())
                .append(" SET status = ")
                .append(Record.STAT_CLOSED)
                .append(" WHERE id = ?");
        jdbcTemplate.update(
                updateOrder.toString(),
                new Object[] { recordId },
                new int[] { Types.BIGINT });
    }

    public Order reopenOrder(Long recordId) {
        StringBuilder updateDeliveries = new StringBuilder();
        updateDeliveries
                .append("UPDATE ")
                .append(deliveryTable)
                .append(" SET status = ")
                .append(Record.STAT_SENT)
                .append(" WHERE reference_id = ?");
        jdbcTemplate.update(
                updateDeliveries.toString(),
                new Object[] { recordId },
                new int[] { Types.BIGINT });
        StringBuilder updateOrder = new StringBuilder();
        updateOrder
                .append("UPDATE ")
                .append(getRecordTableName())
                .append(" SET status = ")
                .append(Record.STAT_SENT)
                .append(" WHERE id = ?");
        jdbcTemplate.update(
                updateOrder.toString(),
                new Object[] { recordId },
                new int[] { Types.BIGINT });
        return (Order) load(recordId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.impl.AbstractRecords#delete(com.osserp.core.finance.Record, com.osserp.core.employees.Employee)
     */
    @Override
    public void delete(Record record, Employee employee) {
        StringBuilder query = new StringBuilder();
        query
                .append("from ")
                .append(PurchaseOrderDeliveryDateChangedInfoImpl.class.getName())
                .append(" o where o.reference = ")
                .append(record.getId());
        List<PurchaseOrderDeliveryDateChangedInfoImpl> dhistory = getCurrentSession().createQuery(query.toString()).list();
        for (Iterator<PurchaseOrderDeliveryDateChangedInfoImpl> hi = dhistory.iterator(); hi.hasNext();) {
            PurchaseOrderDeliveryDateChangedInfoImpl next = hi.next();
            PurchaseOrderDeliveryDateChangedDeleted deleted = new PurchaseOrderDeliveryDateChangedDeleted(next);
            getCurrentSession().save(PurchaseOrderDeliveryDateChangedDeleted.class.getName(), deleted);
            getCurrentSession().delete(PurchaseOrderDeliveryDateChangedInfoImpl.class.getName(), next);
        }
        query = new StringBuilder("from ");
        query
                .append(PurchaseOrderItemChangedInfoImpl.class.getName())
                .append(" o where o.reference = ")
                .append(record.getId());
        List<PurchaseOrderItemChangedInfoImpl> ihistory = getCurrentSession().createQuery(query.toString()).list();
        for (Iterator<PurchaseOrderItemChangedInfoImpl> ih = ihistory.iterator(); ih.hasNext();) {
            PurchaseOrderItemChangedInfoImpl next = ih.next();
            PurchaseOrderItemChangedDeleted deleted = new PurchaseOrderItemChangedDeleted(next);
            getCurrentSession().save(PurchaseOrderItemChangedDeleted.class.getName(), deleted);
            getCurrentSession().delete(PurchaseOrderItemChangedInfoImpl.class.getName(), next);
        }
        super.delete(record, employee);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.PurchaseOrders#create(com.osserp.core.finance.ItemChangedInfo)
     */
    public ItemChangedInfo createItemChangedInfo(ItemChangedInfo notPersistentInfo) {
        try {
            ItemChangedInfo persistent = new PurchaseOrderItemChangedInfoImpl(notPersistentInfo);
            getCurrentSession().saveOrUpdate(PurchaseOrderItemChangedInfoImpl.class.getName(), persistent);
            return persistent;
        } catch (Exception e) {
            log.error("createItemChangedInfo() failed [message=" + e.getMessage() + "]", e);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.impl.AbstractRecords#getPersistentClass()
     */
    @Override
    protected Class<PurchaseOrderImpl> getPersistentClass() {
        return PurchaseOrderImpl.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.impl.AbstractRecords#getItemChangedInfoClass()
     */
    @Override
    protected Class<PurchaseOrderItemChangedInfoImpl> getItemChangedInfoClass() {
        return PurchaseOrderItemChangedInfoImpl.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.impl.AbstractSubTypeAwarePurchaseRecords#getDefaultSubTypeId()
     */
    @Override
    protected Long getDefaultBookingTypeId() {
        return PurchaseOrder.ORDERING_GOODS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.impl.AbstractSubTypeAwarePurchaseRecords#getPersistentSubTypeClass()
     */
    @Override
    protected Class<PurchaseOrderTypeImpl> getPersistentBookingTypeClass() {
        return PurchaseOrderTypeImpl.class;
    }

}
