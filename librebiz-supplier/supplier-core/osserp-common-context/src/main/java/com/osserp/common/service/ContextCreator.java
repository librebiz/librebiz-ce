/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 17, 2007 4:51:20 PM 
 * 
 */
package com.osserp.common.service;

import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContextCreator {

    private static Logger log = LoggerFactory.getLogger(ContextCreator.class.getName());

    /**
     * Creates an application context by xml config files
     * @param configFiles
     * @return application context
     */
    public static synchronized ApplicationContext createApplicationContext(String configFiles) {
        if (configFiles == null) {
            log.error("createApplicationContext() no configFiles provided");
            throw new IllegalArgumentException("missing configfile");
        }
        ApplicationContext appCtx = null;
        if (configFiles.indexOf(",") > -1) {
            if (log.isDebugEnabled()) {
                log.debug("createApplicationContext() invoked by multiple configs");
            }
            String[] configFileArray = getStringArray(configFiles);
            appCtx = new ClassPathXmlApplicationContext(configFileArray);
        } else {
            appCtx = new ClassPathXmlApplicationContext(configFiles);
        }
        return appCtx;
    }
    
    public static String[] getStringArray(String configFiles) {
        if (configFiles == null) {
            return null;
        }
        StringTokenizer st = new StringTokenizer(configFiles, ",");
        int count = st.countTokens();
        String[] result = new String[count];
        if (count > 0) {
            for (int i = 1; i <= count; i++) {
                result[i - 1] = st.nextToken();
            }
        }
        return result;
    }
}
