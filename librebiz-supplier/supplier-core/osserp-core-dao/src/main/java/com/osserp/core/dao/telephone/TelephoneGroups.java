/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 2:10:27 PM 
 * 
 */
package com.osserp.core.dao.telephone;

import java.util.List;

import com.osserp.core.telephone.TelephoneGroup;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneGroups {

    /**
     * Tries to find a telephoneGroup by id
     * @param id
     * @return telephoneGroup or null if not found
     */
    TelephoneGroup find(Long id);

    /**
     * Tries to find telephoneGroups by telephoneSystemId
     * @param telephoneSystemId
     * @return telephoneGroups or null if not found
     */
    List<TelephoneGroup> findByTelephoneSystem(Long telephoneSystemId);

    /**
     * Tries to find a telephoneGroup by name and telephoneSystemId
     * @param name
     * @param telephoneSystemId
     * @return telephoneGroup or null if not found
     */
    TelephoneGroup find(String name, Long telephoneSystemId);

    /**
     * Provides all telephoneGroups
     * @return telephoneGroups
     */
    List<TelephoneGroup> getAll();

    /**
     * Creates new telephoneGroup
     * @param telephoneGroup
     * @return telephoneGroup
     */
    TelephoneGroup create(TelephoneGroup telephone);

    /**
     * Persists current telephoneGroup values
     * @param telephoneGroup
     */
    void save(TelephoneGroup telephoneGroup);
}
