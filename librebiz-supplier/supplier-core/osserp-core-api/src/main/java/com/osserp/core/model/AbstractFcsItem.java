/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 3:14:18 PM 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.FcsItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFcsItem extends AbstractEntity implements FcsItem {
    private Long employeeId;
    private FcsAction action;
    private FcsClosing closing;
    private String note;
    private boolean cancels;
    private Long canceledBy;
    private String headline = null;

    protected AbstractFcsItem() {
        super();
    }

    /**
     * Default constructor to add a new item to a sale
     * @param referenceId
     * @param action
     * @param created
     * @param createdBy
     * @param note
     * @param headline
     * @param cancels
     * @param closing optional status
     */
    protected AbstractFcsItem(
            Long referenceId,
            FcsAction action,
            Date created,
            Long createdBy,
            String note,
            String headline,
            boolean cancels,
            FcsClosing closing) {
        super(null, referenceId, ((created == null)
                ? new Date(System.currentTimeMillis())
                : created), createdBy);
        this.employeeId = createdBy;
        this.action = action;
        this.note = note;
        this.headline = headline;
        this.cancels = cancels;
        this.closing = closing;
    }

    /**
     * Constructor for an action item
     * @param id
     * @param referenceId
     * @param employeeId
     * @param action
     * @param created
     * @param note
     * @param cancels
     * @param canceledBy
     * @param headline
    protected AbstractFcsItem(
            Long id,
            Long referenceId,
            Long employeeId,
            FcsAction action,
            Date created,
            String note,
            boolean cancels,
            Long canceledBy,
            String headline) {

        super(id, referenceId, created, null);
        this.employeeId = employeeId;
        this.action = action;
        this.note = note;
        this.cancels = cancels;
        this.canceledBy = canceledBy;
        this.headline = headline;
    }
     */

    /**
     * Constructor for a wastebasket item
     * @param id
     * @param referenceId
     * @param employeeId
     * @param action
     * @param created
     */
    protected AbstractFcsItem(
            Long id,
            Long referenceId,
            Long employeeId,
            FcsAction action,
            Date created) {

        super(id, referenceId, created, null);
        this.employeeId = employeeId;
        this.action = action;
    }

    protected AbstractFcsItem(FcsItem otherValue) {
        super(otherValue);
        this.employeeId = otherValue.getEmployeeId();
        this.action = otherValue.getAction();
        this.note = otherValue.getNote();
        this.cancels = otherValue.isCancels();
        this.canceledBy = otherValue.getCanceledBy();
        this.closing = otherValue.getClosing();
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public FcsAction getAction() {
        return action;
    }

    public void setAction(FcsAction action) {
        this.action = action;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.FcsItem#changeAction(com.osserp.core.FcsAction)
     */
    public void changeAction(FcsAction fcsAction) {
        this.action = fcsAction;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isCancels() {
        return cancels;
    }

    public void setCancels(boolean cancels) {
        this.cancels = cancels;
    }

    public Long getCanceledBy() {
        return canceledBy;
    }

    public void setCanceledBy(Long canceledBy) {
        this.canceledBy = canceledBy;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public FcsClosing getClosing() {
        return closing;
    }

    public void setClosing(FcsClosing closing) {
        this.closing = closing;
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("employeeId", employeeId));
        root.addContent(JDOMUtil.createElement("action", action));
        root.addContent(JDOMUtil.createElement("closing", closing));
        root.addContent(JDOMUtil.createElement("note", note));
        root.addContent(JDOMUtil.createElement("cancels", cancels));
        root.addContent(JDOMUtil.createElement("canceledBy", canceledBy));
        root.addContent(JDOMUtil.createElement("headline", headline));
        return root;
    }
}
