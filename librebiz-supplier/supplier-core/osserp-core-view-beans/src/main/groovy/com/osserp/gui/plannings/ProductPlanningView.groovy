/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on May 7, 2010 at 9:06:54 AM 
 * 
 */
package com.osserp.gui.plannings

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.util.CollectionUtil
import com.osserp.common.util.DateUtil
import com.osserp.core.Comparators
import com.osserp.core.Item
import com.osserp.core.products.Product
import com.osserp.core.products.ProductSearch
import com.osserp.core.planning.ProductPlanning
import com.osserp.core.planning.ProductPlanningAware
import com.osserp.core.planning.ProductPlanningManager
import com.osserp.core.planning.ProductPlanningSettings
import com.osserp.core.planning.SalesPlanningSummary
import com.osserp.core.finance.Order
import com.osserp.core.finance.OrderManager
import com.osserp.core.purchasing.PurchaseOrderManager
import com.osserp.core.sales.SalesOrderManager
import com.osserp.core.views.OrderItemsDisplay
import com.osserp.groovy.web.MenuItem
import com.osserp.gui.CoreView

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 */
class ProductPlanningView extends AbstractPlanningView  {
    private static Logger logger = LoggerFactory.getLogger(ProductPlanningView.class.getName())

    Product product
    ProductPlanning planning
    ProductPlanningAware planningAware
    ProductPlanningSettings settings
    OrderItemsDisplay orderItemDisplay = null
    Boolean vacancyDisplayMode = false
    Boolean listUnreleased = false
    Long editId = null
    Long itemId = null
    Boolean itemSelected = true
    Boolean showRecordLinks = false


    ProductPlanningView() {
        super()
        headerName = 'productPlanning'
        enablePopupView()
        providesSetupMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            Long stockId = form.getLong('stockId')
            if (stockId) {
                selectedStock = availableStocks.find { it.id == stockId }
            }
            vacancyDisplayMode = form.getBoolean('vacant', vacancyDisplayMode)
            showRecordLinks = form.getBoolean('showRecordLinks', showRecordLinks)
        }

        if (!itemId && !orderItemDisplay && itemSelected) {
            Long productId = form.getLong('productId')
            if (!productId) {
                throw new IllegalStateException('view requires productId param')
            }
            product = productSearch.find(productId)
            if (!product) {
                throw new IllegalStateException("product with id ${productId} not found")
            }

            settings = productPlanningManager.getSettings(domainEmployee.id)
            if (!settings.getStock()) {
                settings.setStock(selectedStock.id)
            }

            itemId = form.getLong('itemId')
            if (!itemId) {
                Long id = form.getLong('id')
                if (id) {
                    planningAware = productPlanningManager.find(id)
                }
                Double quantity = form.getDouble('quantity')
                if (quantity && planningAware) {
                    orderItemDisplay = productPlanningManager.createVirtual(
                            planningAware,
                            product,
                            quantity,
                            settings.stock)
                    logger.debug("initRequest: invoked with id and quantity [id=${id}, quantity=${quantity}]")
                } else {
                    logger.debug("initRequest: invoked without selected item")
                    itemSelected = false
                }
            } else {
                logger.debug("initRequest: invoked with itemId [itemId=${itemId}]")
            }
        }

        if (!bean) {
            refresh()
        }
    }

    @Override
    protected void createCustomNavigation() {
        nav.defaultNavigation = [refreshLink, setupLink]
        nav.beanNavigation = [refreshLink, setupLink]
    }

    Long getSelectedItem() {
        itemId ?: orderItemDisplay.itemId
    }

    void togglePurchaseOrderedOnly() {
        settings.setOpenPurchaseOnly(!settings.isOpenPurchaseOnly())
        refresh()
    }

    void toggleVacancyDisplay() {
        vacancyDisplayMode = !vacancyDisplayMode
        refresh()
    }

    void toggleConfirmedPurchaseOnlyFlag() {
        settings.setConfirmedPurchaseOnly(!settings.isConfirmedPurchaseOnly())
        refresh()
    }

    void updateHorizon() {
        Date horizon = form.getDate('horizon')
        if (horizon) {
            horizon = new Date(horizon.getTime() + DateUtil.DAY - 1000)
        }
        settings.setPlanningHorizon(horizon)
        refresh()
    }

    void updateDelivery() throws ClientException {
        Date delivery = form.getDate('delivery')
        logger.debug("updateDelivery: invoked [item=${editId}, delivery=${delivery}]")
        if (delivery && editId) {
            OrderItemsDisplay selectedItem = null
            for (int i = 0; i < bean.items.size(); i++) {
                OrderItemsDisplay item = bean.items.get(i)
                if (item.getItemId().equals(editId)) {
                    item.setDelivery(delivery)
                    selectedItem = item
                    break
                }
            }
            CollectionUtil.sort(bean.items, Comparators.createOrderItemsDeliveryDateComparator())

            Double current = bean.stock
            bean.items.each() {
                if (it.salesItem) {
                    current = current - it.outstanding
                } else {
                    current = current + it.outstanding
                }
                it.expectedStock = current
            }
            OrderManager orderManager = null
            if (selectedItem?.salesItem && salesOrderManager.exists(selectedItem.id)) {
                orderManager = salesOrderManager
            } else if (purchaseOrderManager.exists(selectedItem.id)) {
                orderManager = purchaseOrderManager
            }
            if (orderManager) {
                Order order = orderManager.load(selectedItem.id)
                for (int i = 0; i < order.items.size(); i++) {
                    Item item = order.items.get(i)
                    if (item.id == selectedItem.itemId) {
                        orderManager.update(domainEmployee, order, delivery, null, item)
                        break
                    }
                }
            }
        }
        editId = null
    }

    void editDelivery() {
        Long id = form.getLong('id')
        if (id) {
            editId = id
        }
    }

    private void refresh() {
        bean = null
        logger.debug("refresh: invoked [stock=${settings.stock}, horizon=${settings.getPlanningHorizon() ?:  "disabled"}]")
        planning = productPlanningManager.getPlanning(
                settings.stock,
                settings.getPlanningHorizon(),
                settings.isConfirmedPurchaseOnly())
        if (!planning) {
            throw new IllegalStateException("unsupported stock [id=${settings.stock}]")
        }
        List planningList = planning.createList(
                (product?.productId ?: null),
                null,
                settings.getProductGroup(),
                null,
                listUnreleased,
                vacancyDisplayMode)

        if (orderItemDisplay) {
            planningList = addItemIfRequired(planningList, orderItemDisplay)
        }

        List summaryList = planning.createSummary(planningList)

        for (SalesPlanningSummary next in summaryList) {
            if (next.productId == product.productId) {
                bean = next
                break
            }
        }
    }

    List<OrderItemsDisplay> addItemIfRequired(List<OrderItemsDisplay> list, OrderItemsDisplay item) {
        boolean added = false
        for (int i = 0; i < list.size(); i++) {
            OrderItemsDisplay next = list.get(i)
            if (next.getItemId().equals(item.getItemId())) {
                added = true
            }
        }
        if (!added) {
            list.add(item)
        }
        CollectionUtil.sort(list, Comparators.createOrderItemsDeliveryDateComparator())
        return list
    }

    MenuItem getRefreshLink() {
        navigationLink("/${context.name}/refresh", 'replaceIcon', 'listRefresh')
    }

    @Override
    void selectStock() {
        super.selectStock()
        if (selectedStock) {
            settings.setStock(selectedStock.id)
            refresh()
        }
    }

    private ProductSearch getProductSearch() {
        return getService(ProductSearch.class.getName())
    }

    private ProductPlanningManager getProductPlanningManager() {
        return getService(ProductPlanningManager.class.getName())
    }

    private PurchaseOrderManager getPurchaseOrderManager() {
        return getService(PurchaseOrderManager.class.getName())
    }

    private SalesOrderManager getSalesOrderManager() {
        return getService(SalesOrderManager.class.getName())
    }
}
