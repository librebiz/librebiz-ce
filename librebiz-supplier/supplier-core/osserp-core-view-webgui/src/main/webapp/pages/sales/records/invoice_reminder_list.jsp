<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.invoiceReminderView}" />

<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th><fmt:message key="number" /></th>
                    <th><fmt:message key="customer" /></th>
                    <th class="right"><fmt:message key="billFrom" /></th>
                    <th class="right"><fmt:message key="billAmount" /></th>
                    <th class="right"><fmt:message key="paid" /></th>
                    <th class="right"><fmt:message key="demand" /></th>
                    <th class="right"><fmt:message key="dueToThe" /></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="record" items="${view.list}" varStatus="s">
                    <tr id="record${record.id}">
                        <td>
                            <c:choose>
                                <c:when test="${record.downpayment}">
                                    <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=invoiceReminder&readonly=true&id=${record.id}&exitId=record${record.id}"/>"><o:out value="${record.number}" /></a>
                                </c:when>
                                <c:otherwise>
                                    <a href="<c:url value="/salesInvoice.do?method=display&exit=invoiceReminder&readonly=true&id=${record.id}&exitId=record${record.id}"/>"><o:out value="${record.number}" /></a>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${!empty record.sales and record.sales > 0}">
                                    <a href="<c:url value="/loadSales.do?target=invoiceReminder&id=${record.sales}&exitId=record${record.id}"/>">
                                        <o:out value="${record.contactName}" limit="38" />
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <a href="<c:url value="/loadCustomer.do?exit=invoiceReminder&id=${record.contactId}&exitId=record${record.id}"/>">
                                        <o:out value="${record.contactName}" limit="38" />
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="right"><o:date value="${record.created}" /></td>
                        <td class="right">
                            <c:choose>
                                <c:when test="${record.downpayment}">
                                    <o:number value="${record.prorateGrossAmount}" format="currency" />
                                </c:when>
                                <c:otherwise>
                                    <o:number value="${record.grossAmount}" format="currency" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="right"><o:number value="${record.paidAmount}" format="currency" /></td>
                        <td class="right"><o:number value="${record.dueAmount}" format="currency" /></td>
                        <td class="right"><o:date value="${record.maturity}" /></td>
                        <td class="icon center">
                            <c:if test="${record.unchangeable and !empty record.contextName}">
                                <v:link url="/records/recordPrint/print?name=${record.contextName}&id=${record.id}" title="documentPrint">
                                    <o:img name="printIcon" />
                                </v:link>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>

        </table>
    </div>
</div>
