/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 */
package com.osserp.common.directory.ldap.impl

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.SubSchema

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ZarafaGroupsSchema extends AbstractLdapConfigAware implements SubSchema {

    private static final List OBJECT_CLASSES = ['zarafa-group']

    private static final List AVAILABLE_ATTRIBUTES = [
        'zarafaAccount',
        'zarafaHidden',
        'mail',
        'zarafaAliases',
        'zarafaSecurityGroup',
        'zarafaSendAsPrivilege'
    ]

    ZarafaGroupsSchema(LdapConfig configInstance) {
        super(configInstance)
    }

    /**
     * Creates a new ldap ready user object by default and values provided by map
     * @param config with default values
     * @param values must contain at least uid and uidnumber
     * @return user object without dn
     */
    Map create(Map parent, Map values) {
        return parent
    }

    List<String> getObjectClasses() {
        OBJECT_CLASSES
    }

    List<String> getAvailableAttributes() {
        AVAILABLE_ATTRIBUTES
    }

    Set<String> getAvailableArrays() {
        Set<String> result = new HashSet<String>()
        result.add('zarafaAliases')
        return result
    }

    List<String> getDefaultDisplayAttributes() {
        AVAILABLE_ATTRIBUTES
    }
}
