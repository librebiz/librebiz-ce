/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 28, 2007 9:25:12 PM 
 * 
 */
package com.osserp.common;

import java.util.Date;

import org.jdom2.Element;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Appointment extends Option {

    /**
     * Indicates if day of appointment is same as provided
     * @param date
     * @return true if day matches
     */
    boolean isMatching(Calendar date);

    /**
     * Indicates if day of appointment date is same as provided
     * @param day
     * @return true if day matches day of getDate() result
     */
    boolean isSameDay(Integer day);

    /**
     * Provides an optional headline
     * @return headline
     */
    String getHeadline();

    /**
     * Provides the appointment message
     * @return message
     */
    String getMessage();

    /**
     * Provides an url to link from title to a view
     * @return titleUrl
     */
    String getUrl();

    /**
     * Provides the date of the appointment
     * @return date
     */
    Date getDate();

    /**
     * Provides the day of date as string suited for display and print.
     * @return day
     */
    String getDayDisplay();
    
    /**
     * Provides the day of date as string suited for display and print.
     * @return month
     */
    String getMonthDisplay();

    /**
     * Provides the day of date as string suited for display and print.
     * @return year
     */
    String getYearDisplay();

    /**
     * Provides the day of date as string suited for display and print.
     * @return hour
     */
    String getHourDisplay();

    /**
     * Provides the day of date as string suited for display and print.
     * @return minute
     */
    String getMinuteDisplay();

    /**
     * Provides the day of date as string suited for display and print.
     * @return time
     */
    String getTimeDisplay();
    
    /**
     * Updates date and display properties.
     * @param newDate
     */
    void updateDate(Date newDate);

    /**
     * Override defaults set up by default constructor
     * @param name
     * @param headline
     * @param message
     */
    void overrideDefaults(String name, String headline, String message);

    /**
     * Provides appointment data as xml
     * @return xml
     */
    Element getXml();

}
