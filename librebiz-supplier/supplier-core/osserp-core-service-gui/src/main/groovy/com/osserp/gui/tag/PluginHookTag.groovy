/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.tag

import javax.servlet.jsp.JspException
import javax.servlet.jsp.JspTagException

import com.osserp.common.web.tags.AbstractImportTag

import com.osserp.core.PluginHook

import com.osserp.gui.PluginContext
import com.osserp.gui.PluginUtils

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class PluginHookTag extends AbstractImportTag {

    String hook
    String view
    String exit

    PluginHookTag() {
        super()
    }

    @Override
    public int doStartTag() throws JspException {
        PluginHook link = PluginUtils.getHook(request, hook)
        if (!link?.value) {
            throw new JspTagException("Plugin hook does not exists: ${hook}")
        }
        PluginContext pc = PluginUtils.createContext(request, link)
        pc.viewName = view
        pc.exit = exit
        PluginUtils.saveContext(request, pc)
        if (link.value.startsWith('/')) {
            this.url = "${viewdir}${link.value}"
        } else {
            this.url = "${viewdir}/${link.value}"
        }
        return super.doStartTag()
    }
}
