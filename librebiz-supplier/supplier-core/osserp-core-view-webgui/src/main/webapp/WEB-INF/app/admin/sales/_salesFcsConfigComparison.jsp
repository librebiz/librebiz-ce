<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<o:logger write="comparator page invoked" level="debug"/>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><o:out value="${view.selectedType.name}"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <c:import url="_salesFcsConfigListing.jsp"/>
    </div>
</div>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><o:out value="${view.otherType.name}"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><fmt:message key="action"/></th>
                        <th style="width: 60px;" class="number right"><span style="margin-right: 5px;"><fmt:message key="status"/></span></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="action" items="${view.otherActions}">
                        <c:if test="${view.eolDisplay or !action.endOfLife}">
                            <tr>
                                <td>
                                    <span title="id: ${action.id}"><o:out value="${action.name}"/></span>
                                </td>
                                <td style="width: 60px; text-align: right;">
                                    <span style="text-align: right; margin-right: 5px;"><o:out value="${action.status}"/></span>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
