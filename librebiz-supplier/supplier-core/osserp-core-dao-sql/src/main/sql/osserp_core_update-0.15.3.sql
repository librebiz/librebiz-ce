SET search_path = osserp, pg_catalog;

UPDATE documents SET file_size = 1561 WHERE file_name = 'xsl/documents/shared/page_definitions.vm';
UPDATE documents SET file_size = 31330 WHERE file_name = 'xsl/records/xsl_record_correction.vm';
UPDATE documents SET file_size = 4543 WHERE file_name = 'xsl/records/xsl_record_template.vm';

INSERT INTO record_billing_types VALUES (7, 1, 'Abschlagsrechnung anteilig', NULL, NULL, false, NULL, false, NULL, 4, false, true, false, false, NULL, false, false);

UPDATE system_properties SET pvalue = '0.15.3', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
