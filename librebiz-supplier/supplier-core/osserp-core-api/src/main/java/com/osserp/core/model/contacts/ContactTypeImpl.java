/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 8, 2012 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Person;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactTypeImpl extends AbstractOption implements ContactType {

    private String shortname = null;
    private boolean child = false;


    protected ContactTypeImpl() {
        super();
    }
    
    protected ContactTypeImpl(Element xml) {
        super(xml);
        child = JDOMUtil.fetchBoolean(xml, "child");
        shortname = JDOMUtil.fetchString(xml, "shortname");
    }

    protected ContactTypeImpl(Map<String, Object> map) {
        super(map);
        child = fetchBoolean(map, "child");
        shortname = fetchString(map, "shortname");
    }

    protected ContactTypeImpl(ContactType vo) {
        super(vo);
        child = vo.isChild();
    }
    
    
    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public boolean isChild() {
        return child;
    }

    public void setChild(boolean child) {
        this.child = child;
    }

    public boolean isBusiness() {
        return (isCompany() || isFreelance() || isPublicInstitution());
    }

    public boolean isCompany() {
        return Person.TYPE_BUSINESS.equals(getId());
    }

    public boolean isContactPerson() {
        return Person.TYPE_PERSON.equals(getId());
    }

    public boolean isPerson() {
        return (isPrivatePerson() || isFreelance());
    }

    public boolean isPrivatePerson() {
        return Person.TYPE_PRIVATE.equals(getId());
    }

    public boolean isFreelance() {
        return Person.TYPE_FREELANCE.equals(getId());
    }

    public boolean isPublicInstitution() {
        return Person.TYPE_PUBLIC.equals(getId());
    }
    
    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "shortname", shortname);
        putIfExists(map, "child", child);
        return map;
    }
}
