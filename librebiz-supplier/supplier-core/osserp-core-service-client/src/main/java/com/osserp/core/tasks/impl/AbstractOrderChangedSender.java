/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.tasks.impl;

import java.util.List;

import com.osserp.common.service.MessageSender;
import com.osserp.common.service.impl.AbstractMessageInitiator;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.tasks.OrderChangedSender;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOrderChangedSender extends AbstractMessageInitiator implements OrderChangedSender {

    /**
     * Creates a new order changed event sender
     * @param sender
     * @param destinationQueue
     * @param serviceName
     */
    protected AbstractOrderChangedSender(MessageSender sender, String destinationQueue, String serviceName) {
        super(sender, destinationQueue, serviceName);
    }

    protected abstract String getDeliveryDateChangeEventName();

    protected abstract String getItemChangeEventName();

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.tasks.OrderChangedSender#sendDeliveryDateChangedInfo(com.osserp.core.finance.Order, com.osserp.core.finance.DeliveryDateChangedInfo)
     */
    public void sendDeliveryDateChangedInfo(Order order, DeliveryDateChangedInfo deliveryDateChangedInfo) {
        OrderChangedMessage msg = new OrderChangedMessage(getDeliveryDateChangeEventName(), order, deliveryDateChangedInfo);
        send(msg);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.tasks.OrderChangedSender#sendDeliveryDateChangedInfo(com.osserp.core.finance.Order, java.util.List)
     */
    public void sendDeliveryDateChangedInfo(Order order, List<DeliveryDateChangedInfo> deliveryDateChangedInfos) {
        OrderChangedMessage msg = new OrderChangedMessage(getDeliveryDateChangeEventName(), order, deliveryDateChangedInfos);
        send(msg);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.tasks.OrderChangedSender#sendItemChangedInfo(com.osserp.core.finance.Order, com.osserp.core.finance.ItemChangedInfo)
     */
    public void sendItemChangedInfo(Order order, ItemChangedInfo itemChangedInfo) {
        OrderChangedMessage msg = new OrderChangedMessage(getItemChangeEventName(), order, itemChangedInfo);
        send(msg);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.tasks.OrderChangedSender#sendItemChangedInfo(com.osserp.core.finance.Order, java.util.List)
     */
    public void sendItemChangedInfo(Order order, List<ItemChangedInfo> itemChangedInfos) {
        OrderChangedMessage msg = new OrderChangedMessage(getItemChangeEventName(), order, itemChangedInfos);
        send(msg);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.tasks.OrderChangedSender#sendItemChangedInfo(com.osserp.core.finance.Order, java.util.List)
     */
    public void sendUpdateEvent(Employee user, Order order, String eventTaskName, String message) {
        OrderChangedMessage msg = new OrderChangedMessage(user, eventTaskName, order, message);
        send(msg);
    }
}
