<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="letterView" />
<v:form name="letterForm" url="${view.baseLink}/save">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="settings" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="settingSalutation_1"><fmt:message key="salutation" /> 1</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="header" value="${view.bean.address.header}" objectpath="address.header" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="settingSalutation_2"><fmt:message key="salutation" /> 2</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="name" value="${view.bean.address.name}" objectpath="address.name" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="street"><fmt:message key="street" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="street" value="${view.bean.address.street}" objectpath="address.street" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="zipcodeAndCity"> <fmt:message key="zipcode" /> / <fmt:message key="city" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <v:text styleClass="form-control" name="zipcode" value="${view.bean.address.zipcode}" objectpath="address.zipcode" />
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <v:text styleClass="form-control" name="city" value="${view.bean.address.city}" objectpath="address.city" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="country"><fmt:message key="country" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:select styleClass="form-control" name="country" options="countryNames" value="${view.bean.address.country}" objectpath="address.country" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="language"><fmt:message key="language" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select class="form-control" name="language" id="languageSelection">
                                <c:choose>
                                    <c:when test="${empty view.bean.language}">
                                        <option value="de" selected="selected">DE</option>
                                        <option value="en">EN</option>
                                    </c:when>
                                    <c:when test="${view.bean.language == 'de'}">
                                        <option value="de" selected="selected">DE</option>
                                        <option value="en">EN</option>
                                    </c:when>
                                    <c:when test="${view.bean.language == 'en'}">
                                        <option value="en" selected="selected">EN</option>
                                        <option value="de">DE</option>
                                    </c:when>
                                </c:choose>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="ignoreSubject"><fmt:message key="options" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreSubject"> <v:checkbox name="ignoreSubject" value="${view.bean.ignoreSubject}" /> <fmt:message key="ignoreSubjectLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="ignoreHeader"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreHeader"> <v:checkbox name="ignoreHeader" value="${view.bean.ignoreHeader}" /> <fmt:message key="ignoreHeaderLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreSalutation"> <v:checkbox name="ignoreSalutation" value="${view.bean.ignoreSalutation}" /> <fmt:message key="ignoreSalutationLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="ignoreGreetings"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreGreetings"> <v:checkbox name="ignoreGreetings" value="${view.bean.ignoreGreetings}" /> <fmt:message key="ignoreGreetingsLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <c:forEach var="info" items="${view.bean.infos}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="${info.name}"> </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="${info.name}"> <v:checkbox name="infos" value="${info.ignore}" printvalue="${info.name}" reverse="true" /> <fmt:message key="${info.name}" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
        </div>
    </div>

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="printGreetingsLabel" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <v:date name="printDate" styleClass="form-control" label="date" picker="true" />

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="subject"><fmt:message key="subject" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="subject" value="${view.bean.subject}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="salutation"><fmt:message key="salutation" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="salutation" value="${view.bean.salutation}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="greetings"><fmt:message key="greetingsLabel" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="greetings" value="${view.bean.greetings}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="signatureLeft"><fmt:message key="signatureLeft" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:select styleClass="form-control" name="signatureLeft" options="employees" value="${view.bean.signatureLeft}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="signatureRight"><fmt:message key="signatureRight" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:select styleClass="form-control" name="signatureRight" options="employees" value="${view.bean.signatureRight}" />
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/disableEditMode" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</v:form>
