<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="contact" value="${requestScope.contact}"/>
<div class="contactData">
	<div class="label"><fmt:message key="email"/></div>
	<div class="value">
		<input type="text" class="text" name="email" value="<o:out value="${contact.email}"/>" />    
	</div>
</div>  
