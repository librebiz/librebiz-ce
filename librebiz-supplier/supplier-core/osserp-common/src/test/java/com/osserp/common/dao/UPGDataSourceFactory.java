/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 17, 2011 1:11:03 PM 
 * 
 */
package com.osserp.common.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Test;

import com.osserp.common.dao.DataSourceFactory;
import com.osserp.common.dao.PGDataSourceFactory;
import com.osserp.common.util.ExceptionUtil;

/**
 * Unit tests for PGDataSourceFactory
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class UPGDataSourceFactory {
    private static Logger log = LoggerFactory.getLogger(UPGDataSourceFactory.class.getName());

    @Test
    public void newInstance() {
        try {
            DataSourceFactory factory = PGDataSourceFactory.newInstance();
            assertNotNull(factory.getProperties());
        } catch (Exception e) {
            log.error(ExceptionUtil.stackTraceToString(e));
            fail("newInstance throws exception: " + e.getMessage());
        }
    }

    @Test
    public void propertiesInitialized() {
        try {
            DataSourceFactory factory = PGDataSourceFactory.newInstance();
            assertTrue(!factory.getProperties().isEmpty());
        } catch (Exception e) {
        }
    }

    @Test
    public void allPropertiesRead() {
        try {
            DataSourceFactory factory = PGDataSourceFactory.newInstance();
            assertNotNull(factory.getProperties().get("datasourceName"));
            assertNotNull(factory.getProperties().get("databaseName"));
            assertNotNull(factory.getProperties().get("serverName"));
            assertNotNull(factory.getProperties().get("portNumber"));
            assertNotNull(factory.getProperties().get("user"));
            assertNotNull(factory.getProperties().get("password"));
            assertNotNull(factory.getProperties().get("defaultAutoCommit"));
            assertNotNull(factory.getProperties().get("initialConnections"));
            assertNotNull(factory.getProperties().get("maxConnections"));
        } catch (Exception e) {
        }
    }
}
