/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 27-Sep-2005 11:14:06 
 * 
 */
package com.osserp.gui.client;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class Actions extends com.osserp.common.web.Actions {

    public static final String AJAX = "ajax";
    public static final String BILLING = "billing";
    public static final String CALCULATION_DISPLAY = "calculationDisplay";
    public static final String CUSTOMER = "customer";
    public static final String CUSTOMER_DISPLAY = "customerDisplay";
    public static final String EMPLOYEE_DISPLAY = "employeeDisplay";
    public static final String EMPLOYEE_SELECTION = "employeeSelection";
    public static final String INVOICE = "invoice";
    public static final String OFFER = "offer";
    public static final String ORDER = "order";
    public static final String PLAN = "plan";
    public static final String POPUP = "popup";
    public static final String PROJECT = "project";
    public static final String REFRESH = "refresh";
    public static final String SEND_EMAIL = "sendEmail";
    public static final String SUPPLIER_DISPLAY = "supplierDisplay";

}
