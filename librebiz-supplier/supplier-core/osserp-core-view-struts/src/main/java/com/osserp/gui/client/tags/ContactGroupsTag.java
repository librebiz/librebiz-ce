/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 19, 2005 
 * 
 */
package com.osserp.gui.client.tags;

import java.util.Map;

import javax.servlet.jsp.JspException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Option;
import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.gui.tags.AbstractOptionsTag;
import com.osserp.gui.client.contacts.ContactView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactGroupsTag extends AbstractOptionsTag {
    private static Logger log = LoggerFactory.getLogger(ContactGroupsTag.class.getName());

    private static final String LINK_START = "<a href=\"";
    private static final String TAG_END = ">";
    private static final String LINK_END = "</a>";
    private static final String NEWLINE = "<br />";

    @Override
    public String getOutput() throws JspException {
        String context = getRequest().getContextPath();
        StringBuilder buffer = new StringBuilder(128);
        ContactView view = null;
        Contact c = null;
        if (value == null || !(value instanceof ContactView)) {
            return buffer.append("").toString();
        }
        view = (ContactView) value;
        c = (Contact) view.getBean();
        if (log.isDebugEnabled()) {
            log.debug("getOutput() starting [contact=" + c.getContactId()
                    + ", client=" + c.isClient()
                    + ", class=" + c.getClass().getName()
                    + "]");
        }
        Map<Long, Option> groups = getOptions(Options.CONTACT_GROUPS);
        boolean first = true;
        if (c.isClient()) {
            first = false;
            buffer
                    .append(LINK_START)
                    .append(context)
                    .append("/app/company/clientCompany/forward?contact=")
                    .append(c.getContactId())
                    .append("&exit=/contactDisplay.do")
                    .append("\"");
            addStyleClass(buffer);
            buffer
                    .append(TAG_END)
                    .append(getOptionValue(groups, Contact.CLIENT))
                    .append(LINK_END);

        } else if (c.isBranchOffice()) {
            first = false;
            buffer
                    .append(LINK_START)
                    .append(context)
                    .append("/app/company/branch/forward?contact=")
                    .append(c.getContactId())
                    .append("&exit=/contactDisplay.do")
                    .append("\"");
            addStyleClass(buffer);
            buffer
                    .append(TAG_END)
                    .append(getOptionValue(groups, Contact.BRANCH_OFFICE))
                    .append(LINK_END);

        }
        if (!c.isClient() && !c.isBranchOffice()) {
            if (c.isCustomer()) {
                first = false;
                if (view.isCustomerView()) {
                    buffer.append(getOptionValue(groups, Contact.CUSTOMER));
                } else {
                    buffer
                            .append(LINK_START)
                            .append(context)
                            .append("/customerDisplay.do")
                            .append("\"");
                    addStyleClass(buffer);
                    buffer
                            .append(TAG_END)
                            .append(getOptionValue(groups, Contact.CUSTOMER))
                            .append(LINK_END);
                }
            }
            if (c.isEmployee()) {
                if (!first) {
                    buffer.append(",").append(NEWLINE);
                } else {
                    first = false;
                }
                if (view.isEmployeeView()) {
                    buffer.append(getOptionValue(groups, Contact.EMPLOYEE));
                } else {
                    buffer
                            .append(LINK_START)
                            .append(context)
                            .append("/employeeDisplay.do")
                            .append("\"");
                    addStyleClass(buffer);
                    buffer
                            .append(TAG_END)
                            .append(getOptionValue(groups, Contact.EMPLOYEE))
                            .append(LINK_END);
                }
            }
            if (c.isSupplier()) {
                if (!first) {
                    buffer.append(", ");
                } else {
                    first = false;
                }
                buffer
                        .append(LINK_START)
                        .append(context)
                        .append("/supplierDisplay.do")
                        .append("\"");
                addStyleClass(buffer);
                buffer
                        .append(TAG_END)
                        .append(getOptionValue(groups, Contact.SUPPLIER))
                        .append(LINK_END);
            }
            if (c.isOther() && first) {
                buffer.append(getOptionValue(groups, Contact.OTHER));
            }
        }
        return buffer.toString();
    }
}
