<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="${sessionScope.contactInputViewName}" />
<div class="row">
    <div class="col-md-12 panel-area">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tr>
                    <th><fmt:message key="contactType" /></th>
                    <th><fmt:message key="description" /></th>
                </tr>
                <c:forEach var="type" items="${view.contactTypes}">
                    <c:if test="${!type.selectionLabel and !type.endOfLife and !type.child}">
                        <tr>
                            <td><v:link url="/contacts/contactCreator/selectType?id=${type.id}">
                                    <o:out value="${type.name}" />
                                </v:link></td>
                            <td><o:out value="${type.description}" /></td>
                        </tr>
                    </c:if>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
