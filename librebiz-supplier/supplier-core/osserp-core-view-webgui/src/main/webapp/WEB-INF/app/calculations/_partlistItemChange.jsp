<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.partlistView}"/>
<c:set var="partlist" value="${view.bean}"/>

<c:forEach var="position" items="${partlist.positions}">
	<c:forEach var="item" items="${position.items}">
		<c:if test="${item.id == view.changeItemId}">
			<v:form url="/calculations/partlist/save">
				<input type="hidden" name="partnerPriceSource" value="<o:number value="${item.partnerPrice}" format="decimal"/>"/>
				<table class="table table-striped">
					<thead>
						<tr>
							<th style="width: 80px;"><fmt:message key="product"/></th>
							<th><fmt:message key="description"/></th>
							<th class="right" style="width: 58px;"><fmt:message key="quantity"/></th>
							<th class="center" style="width: 22px;"><fmt:message key="quantityUnitShort"/></th>
							<th class="right" style="width: 86px;"><span title="<fmt:message key="partnerPrice"/>"><fmt:message key="partnerPriceShort"/></span></th>
							<th class="right" style="width: 86px;"><fmt:message key="price"/></th>
							<th class="center" style="width: 60px;"><fmt:message key="actions"/></th>
						</tr>
					</thead>
					<tbody>
						<tr class="lightgrey">
							<td><o:out value="${item.product.productId}"/></td>
							<td><o:out value="${item.product.name}"/></td>
							<td class="right"><input class="number" type="text" name="quantity" value="<o:number value="${item.quantity}" format="decimal"/>"/></td>
							<td class="right"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
							<td class="right">
								<c:choose>
									<c:when test="${item.partnerPriceEditable}">
										<input class="longNumber" type="text" name="partnerPrice" value="<o:number value="${item.partnerPrice}" format="decimal"/>"/>
									</c:when>
									<c:otherwise>
										<input class="longNumber" type="text" name="partnerPrice" value="<o:number value="${item.partnerPrice}" format="decimal"/>" readonly="readonly"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td class="right"><input class="longNumber" type="text" name="price" value="<o:number value="${item.price}" format="decimal"/>"/></td>
							<td class="center">
								<input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="update" onclick="setMethod('update');" title="<fmt:message key='saveInput'/>"/>
							</td>
						</tr>
						<tr class="lightgrey">
							<td></td>
							<td colspan="4"><input  style="width:100%;" class="text" type="text" name="note" value="<o:out value="${item.note}" />"/></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</v:form>
		</c:if>
	</c:forEach>
</c:forEach>
