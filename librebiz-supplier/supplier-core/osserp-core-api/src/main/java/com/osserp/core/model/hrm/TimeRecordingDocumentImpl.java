/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 6, 2008 9:01:07 AM 
 * 
 */
package com.osserp.core.model.hrm;

import com.osserp.common.ErrorCode;
import com.osserp.common.Month;
import com.osserp.common.User;
import com.osserp.common.beans.CalendarMonthImpl;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.model.AbstractDmsDocument;
import com.osserp.core.hrm.TimeRecordingDocument;
import com.osserp.core.hrm.TimeRecordingMonth;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.hrm.TimeRecordingYear;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingDocumentImpl extends AbstractDmsDocument implements TimeRecordingDocument {

    private int monthValue;
    private int yearValue;

    protected TimeRecordingDocumentImpl() {
        super();
    }

    /**
     * Creates a new time recording document for selected month in period. <br/>
     * Throws illegal state exception if month not selected.
     * @param user
     * @param period
     * @param type
     * @param fileName
     * @param fileSize
     */
    public TimeRecordingDocumentImpl(
            User user,
            TimeRecordingPeriod period,
            DocumentType type,
            String fileName,
            long fileSize) {
        super(period.getId(), type, fileName, fileSize, user.getId(),
                null, null, null, null, null);
        TimeRecordingYear year = period.getSelectedYear();
        if (year == null) {
            // invoking class should handle this before
            throw new IllegalStateException(ErrorCode.YEAR_SELECTION_MISSING);
        }
        TimeRecordingMonth month = period.getSelectedYear().getSelectedMonth();
        if (month == null) {
            // invoking class should handle this before
            throw new IllegalStateException(ErrorCode.MONTH_SELECTION_MISSING);
        }
        this.monthValue = month.getMonth();
        this.yearValue = month.getYear();
    }

    public Month getMonth() {
        return new CalendarMonthImpl(monthValue, yearValue);
    }

    protected int getMonthValue() {
        return monthValue;
    }

    protected void setMonthValue(int monthValue) {
        this.monthValue = monthValue;
    }

    protected int getYearValue() {
        return yearValue;
    }

    protected void setYearValue(int yearValue) {
        this.yearValue = yearValue;
    }
}
