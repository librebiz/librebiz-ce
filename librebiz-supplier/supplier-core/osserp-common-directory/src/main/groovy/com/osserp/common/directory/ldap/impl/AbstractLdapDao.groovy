/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.apache.directory.groovyldap.LDAP

import com.osserp.common.ErrorCode
import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.LdapException
import com.osserp.common.util.SecurityUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 */
public abstract class AbstractLdapDao extends AbstractLdapAccess {

    protected AbstractLdapDao(LdapConfig configObj) {
        super(configObj)
    }

    /**
     * Creates a new entry
     * @param name (dn without base)
     * @param values maps objectClass list and other values to assign
     * @param client if client context should be prefixed
     * @return new created or -unmodified- existing if entry already exists
     */
    Map createEntry(String name, Map values, String client) {
        LDAP ldap = createConnection()
        Map result = findByDn(name, client)
        if (!result) {
            def dn = createDn(name, client)
            ldap.add(dn, values)
            result = findByDn(name, client)
        }
        return result
    }

    /**
     * Finds an entry by complete dn
     * @param dn with base part
     * @return values or empty map if no such entry exists
     */
    Map findByCompletedDn(String dn) {
        LDAP ldap = createConnection()
        if (ldap.exists(dn)) {
            return addBusinessAttributes(ldap.read(dn))
        }
        return [:]
    }

    /**
     * Finds an entry by complete dn
     * @param ldap connection
     * @param dn with base part
     * @return values or empty map if no such entry exists
     */
    Map findByCompletedDn(LDAP ldap, String dn) {
        if (ldap.exists(dn)) {
            return addBusinessAttributes(ldap.read(dn))
        }
        return [:]
    }

    /**
     * Finds an entry by name.
     * @param name (dn without base)
     * @param client optional if client context should be prefixed
     * @return values or empty map if no such entry exists
     */
    Map findByDn(String name, String client) {
        LDAP ldap = createConnection()
        def dn = createDn(name, client)
        findByCompletedDn(dn)
    }

    /**
     * Adds -none persistent- business attributes to a map of implementing
     * ldap object type after reading from directory. Method is invoked by
     * findByXXX before returning the resulting map.
     * <br>The default implementation returns the unchanged ldapEntry.
     * @param ldapEntry
     * @return entry with added virtual business attributes if implementing class needs this
     */
    protected Map addBusinessAttributes(Map ldapEntry) {
        return ldapEntry
    }

    /**
     * Updates an entry with provided values. 
     * @param object with existing dn property
     * @param values 
     * @return object entry with updated values
     */
    @Override
    final Map update(Map object, Map values) {
        LDAP ldap = createConnection()
        if (values) {
            ldap.modify(object.dn, 'REPLACE', applySchema(values, object))
        }
        ldap.read(object.dn)
    }

    /**
     * Applies objectClasses, defaults & cleanups against the provided value map if required by schema.
     * Override to perform additional operations before invocation of update method.
     * The default implementation does nothing and returns provided value map.
     * @param values to perform update with
     * @param existing ldap object values if entry already exists.
     * @return checked map
     */
    protected Map applySchema(Map values, Map existing) {
        return values
    }

    /**
     * Creates the base dn
     * @param client (optional) 
     * @return baseDn with prefixed clients tree if client value available (e.g. cn=clientname,ou=clients,dc=...)
     */
    protected String createBaseDn(String client) {
        client ? "cn=${client},ou=clients,${config.base}" : config.base
    }

    /**
     * Creates a dn by prefix, client and base dn 
     * @param prefix 
     * @param client optional client name
     * @return
     */
    protected String createDn(String prefix, String client) {
        def baseDn = createBaseDn(client)
        "$prefix,$baseDn"
    }

    /**
     * Checks a password string against hashed obj.userpassword 
     * @param obj with userpassword field
     * @param password
     * @param method optional hash algorithm to use. Defaults to MD5. 
     * @return entry
     * @throws LdapException if password does not match
     */
    protected final Map checkUserPassword(Map obj, String password, String method) throws LdapException {
        if (!obj) {
            throw new LdapException(ErrorCode.NAME_INVALID)
        }
        String currentPwd = obj.userpassword ? new String(obj.userpassword) : null
        if (!currentPwd) {
            throw new LdapException(ErrorCode.PERMISSION_DENIED)
        }
        if (!SecurityUtil.checkPassword(password, currentPwd)) {
            throw new LdapException(ErrorCode.USER_PASSWORD_INVALID)
        }
        return obj
    }
}

