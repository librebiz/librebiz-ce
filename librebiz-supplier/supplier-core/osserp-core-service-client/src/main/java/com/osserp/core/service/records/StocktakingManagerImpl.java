/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 29, 2008 7:35:11 AM 
 * 
 */
package com.osserp.core.service.records;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;
import com.osserp.core.Options;
import com.osserp.core.dao.records.Stocktakings;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.dms.Stylesheet;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.Stocktaking;
import com.osserp.core.finance.StocktakingGroup;
import com.osserp.core.finance.StocktakingItem;
import com.osserp.core.finance.StocktakingManager;
import com.osserp.core.service.doc.XmlService;
import com.osserp.core.service.impl.AbstractOutputService;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.tasks.ProductPlanningCacheSender;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StocktakingManagerImpl extends AbstractOutputService implements StocktakingManager {
    private ProductPlanningCacheSender productPlanningTask = null;
    private Stocktakings stocktakings = null;

    protected StocktakingManagerImpl(
            ResourceLocator resourceLocator,
            OptionsCache optionsCache, 
            CoreStylesheetService stylesheetService, 
            XmlService xmlService,
            ProductPlanningCacheSender productPlanningTask,
            Stocktakings stocktakings) {
        super(resourceLocator, optionsCache, stylesheetService, xmlService);
        this.productPlanningTask = productPlanningTask;
        this.stocktakings = stocktakings;
    }

    public void close(Employee employee, Stocktaking st) {
        Record result = stocktakings.close(employee, st);
        productPlanningTask.send(result.getItems());
    }

    public Stocktaking create(
            Long company,
            Option type,
            String name,
            Date recordDate,
            Long createdBy) throws ClientException {
        return stocktakings.create(company, type, name, recordDate, createdBy);
    }

    public void delete(Stocktaking st) {
        stocktakings.delete(st);
    }

    public Stocktaking find(Long id) {
        return stocktakings.find(id);
    }

    public List<Option> getClosed(Long company) {
        return stocktakings.getClosed(company);
    }

    public Stocktaking getOpen(Long company) {
        return stocktakings.getOpen(company);
    }

    public List<Option> getTypes() {
        return stocktakings.getTypes();
    }

    public void markListPrinted(Stocktaking st) {
        stocktakings.markListPrinted(st);
    }

    public void refreshItems(Stocktaking st) {
        stocktakings.refreshItems(st);
    }

    public void resetCounters(Stocktaking st) {
        stocktakings.resetCounters(st);
    }

    public void save(Stocktaking st) {
        stocktakings.save(st);
    }

    public void setCountedAsExpected(Stocktaking st) {
        stocktakings.setCountedAsExpected(st);
    }

    public void validate(
            List<Option> availableClosed,
            Stocktaking currentOpen,
            Option typeToValidate,
            String nameToValidate) throws ClientException {
        stocktakings.validate(availableClosed, currentOpen, typeToValidate, nameToValidate);
    }

    public Document createDocument(
            DomainUser user, 
            Stocktaking stocktaking,
            SystemCompany company,
            String stylesheetName) throws ClientException {
        assert (stylesheetName != null && stocktaking != null && company != null);
        
        Document doc = xmlService.createDocument(user.getEmployee());
        Element xml = createRootXml(stocktaking, company);
        if (stylesheetName.equals(Stylesheet.STOCKTAKING_PROTOCOL)) {
            addProtocolItems(xml, stocktaking.getItems());
        } else if (stylesheetName.equals(Stylesheet.STOCKTAKING_LIST)) {
            addListItems(xml, stocktaking.getGroupedItems());
            markListPrinted(stocktaking);
        } else {
            addReportItems(xml, stocktaking.getGroupedItems());
        }
        doc.getRootElement().addContent(xml);
        return doc;
    }

    private Element createRootXml(Stocktaking stocktaking, SystemCompany company) {
        Element xml = new Element("stocktaking");
        xml.addContent(company.getXML("company"));
        xml.addContent(new Element("id").setText(stocktaking.getId().toString()));
        xml.addContent(new Element("type").setText(stocktaking.getType().getName()));
        xml.addContent(new Element("name").setText(stocktaking.getName()));
        if (isNotSet(stocktaking.getCounter())) {
            xml.addContent(new Element("counter"));
        } else {
            xml.addContent(new Element("counter").setText(
                    getEmployeeName(stocktaking.getCounter())));
        }
        if (isNotSet(stocktaking.getWriter())) {
            xml.addContent(new Element("writer"));
        } else {
            xml.addContent(new Element("writer").setText(
                    getEmployeeName(stocktaking.getWriter())));
        }
        xml.addContent(new Element("created").setText(
                DateFormatter.getDate(stocktaking.getCreated())));
        xml.addContent(new Element("recordDate").setText(
                DateFormatter.getDate(stocktaking.getRecordDate())));
        return xml;
    }

    private void addProtocolItems(Element xml, List<StocktakingItem> items) throws ClientException {
        double balanceAmount = 0;
        double stockAmount = 0;
        for (int i = 0, j = items.size(); i < j; i++) {
            StocktakingItem next = items.get(i);
            if (!next.getExpectedQuantity().equals(next.getCountedQuantity())) {
                Element product = new Element("product");
                product.addContent(new Element("id").setText(next.getProduct().getProductId().toString()));
                product.addContent(new Element("name").setText(next.getProduct().getName()));
                product.addContent(new Element("expectedQuantity").setText(
                        NumberFormatter.getValue(next.getExpectedQuantity(), NumberFormatter.DECIMAL)));
                if (next.getCountedQuantity() == null) {
                    throw new ClientException(ErrorCode.VALUES_MISSING);
                }
                product.addContent(new Element("countedQuantity").setText(
                        NumberFormatter.getValue(next.getCountedQuantity(), NumberFormatter.DECIMAL)));
                Double diff = next.getCountedQuantity() - next.getExpectedQuantity();
                product.addContent(new Element("balance").setText(
                        NumberFormatter.getValue(diff, NumberFormatter.DECIMAL)));
                Double diffAmount = diff * next.getAveragePurchasePrice();
                product.addContent(new Element("balanceAmount").setText(
                        NumberFormatter.getValue(diffAmount, NumberFormatter.CURRENCY)));
                balanceAmount = balanceAmount + diffAmount;
                Double amount = next.getCountedQuantity() * next.getAveragePurchasePrice();
                product.addContent(new Element("stockAmount").setText(
                        NumberFormatter.getValue(amount, NumberFormatter.CURRENCY)));
                stockAmount = stockAmount + amount;
                xml.addContent(product);
            }
        }
        xml.addContent(new Element("balanceAmountSummary").setText(
                NumberFormatter.getValue(balanceAmount, NumberFormatter.CURRENCY)));
        xml.addContent(new Element("stockAmountSummary").setText(
                NumberFormatter.getValue(stockAmount, NumberFormatter.CURRENCY)));
    }

    private void addListItems(Element xml, SortedSet<StocktakingGroup> items) {
        for (Iterator<StocktakingGroup> i = items.iterator(); i.hasNext();) {
            StocktakingGroup nextGrp = i.next();
            Element grpXml = new Element("group");
            grpXml.setAttribute("name", nextGrp.getName());
            for (int k = 0, l = nextGrp.getItems().size(); k < l; k++) {
                StocktakingItem nextItem = nextGrp.getItems().get(k);
                Element product = new Element("product");
                product.addContent(new Element("id").setText(nextItem.getProduct().getProductId().toString()));
                product.addContent(new Element("name").setText(nextItem.getProduct().getName()));
                product.addContent(new Element("expectedQuantity").setText(
                        NumberFormatter.getValue(nextItem.getExpectedQuantity(), NumberFormatter.DECIMAL)));
                product.addContent(new Element("countedQuantity"));
                grpXml.addContent(product);
            }
            xml.addContent(grpXml);
        }
    }

    private void addReportItems(Element xml, SortedSet<StocktakingGroup> items) {
        double summaryCount = 0;
        double summaryAmount = 0;
        for (Iterator<StocktakingGroup> i = items.iterator(); i.hasNext();) {
            StocktakingGroup nextGrp = i.next();
            Element grpXml = new Element("group");
            grpXml.setAttribute("name", nextGrp.getName());
            for (int k = 0, l = nextGrp.getItems().size(); k < l; k++) {
                StocktakingItem nextItem = nextGrp.getItems().get(k);
                Element product = new Element("product");
                product.addContent(new Element("id").setText(nextItem.getProduct().getProductId().toString()));
                product.addContent(new Element("name").setText(nextItem.getProduct().getName()));
                product.addContent(new Element("quantityUnit").setText(
                        getQuantityUnit(nextItem.getProduct().getQuantityUnit())));
                product.addContent(new Element("packagingUnit").setText(
                        nextItem.getProduct().getPackagingUnit().toString()));
                product.addContent(new Element("currentStock").setText(
                        NumberFormatter.getValue(nextItem.getCountedQuantity(), NumberFormatter.DECIMAL)));
                product.addContent(new Element("averagePurchasePrice").setText(
                        NumberFormatter.getValue(nextItem.getAveragePurchasePrice(), NumberFormatter.CURRENCY)));
                Double currentAmount = nextItem.getAveragePurchasePrice() * nextItem.getCountedQuantity();
                product.addContent(new Element("averageStockValue").setText(
                        NumberFormatter.getValue(currentAmount, NumberFormatter.CURRENCY)));
                grpXml.addContent(product);
                summaryCount = summaryCount + nextItem.getCountedQuantity();
                summaryAmount = summaryAmount + currentAmount;
            }
            xml.addContent(grpXml);
        }
        xml.addContent(new Element("totalStock").setText(
                NumberFormatter.getValue(Double.valueOf(summaryCount), NumberFormatter.DECIMAL)));
        xml.addContent(new Element("totalAmount").setText(
                NumberFormatter.getValue(Double.valueOf(summaryAmount), NumberFormatter.CURRENCY)));
    }

    private String getQuantityUnit(Long unit) {
        return getOptionName(Options.QUANTITY_UNITS, unit);
    }

    private String getEmployeeName(Long id) {
        return getOptionName(Options.EMPLOYEES, id);
    }

}
