<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${!record.historical}">
    <tr>
        <td><fmt:message key="shippingLabel"/></td>
        <td><oc:select name="shippingId" value="${record.shippingId}" options="shippingCompanies" emptyPrompt="true" /></td>
    </tr>
</c:if>