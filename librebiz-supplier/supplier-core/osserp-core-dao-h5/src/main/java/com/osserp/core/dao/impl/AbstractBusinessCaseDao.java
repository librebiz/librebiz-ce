/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06-Nov-2006 10:05:05 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ActionException;
import com.osserp.common.Details;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.Property;
import com.osserp.common.User;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessContract;
import com.osserp.core.BusinessType;
import com.osserp.core.ContractType;
import com.osserp.core.FcsAction;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.BusinessCaseDao;
import com.osserp.core.dao.Contacts;
import com.osserp.core.model.BusinessCasePropertyImpl;
import com.osserp.core.model.ContactRequestImpl;
import com.osserp.core.model.RequestImpl;
import com.osserp.core.model.SalesContractImpl;
import com.osserp.core.model.SalesRequestImpl;
import com.osserp.core.model.projects.ProjectRequestImpl;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractBusinessCaseDao extends AbstractBusinessCaseContextDao 
        implements BusinessCaseDao {
    private static Logger log = LoggerFactory.getLogger(AbstractBusinessCaseDao.class.getName());
    protected Contacts contactsDao = null;

    public AbstractBusinessCaseDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Contacts contacts,
            OptionsCache optionsCache) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache);
        contactsDao = contacts;
    }

    protected abstract List<FcsAction> findActions(Long businessType);

    protected abstract void persist(BusinessCase businessCase);

    protected abstract BusinessCase initializeBranch(BusinessCase businessCase, boolean force);

    /**
     * Creates a non-persistent request implementation by common values
     * @param businessId
     * @param externalReference
     * @param externalUrl
     * @param createdDate
     * @param businessType
     * @param branchOffice
     * @param customer
     * @param salesPersonId
     * @param projectManagerId
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param countryId
     * @param campaign
     * @param origin
     * @return new created request implementation
     */
    protected final RequestImpl createRequestImpl(
            Long businessId, 
            String externalReference, 
            String externalUrl, 
            Date createdDate, 
            BusinessType businessType, 
            BranchOffice branchOffice,
            Customer customer, 
            Long salesPersonId, 
            Long projectManagerId, 
            String name,
            String street,
            String streetAddon,
            String zipcode, 
            String city, 
            Long countryId, 
            Campaign campaign,
            Option origin) {
        
        RequestImpl request = null;
        if (businessType == null) {
            throw new IllegalArgumentException("businessType must not be null");
        }
        Long requestId = businessId;
        if (isEmpty(requestId)) {
            requestId = getNextRequestId(businessType);
        }
        if (businessType.isInterestContext()) {

            request = new ContactRequestImpl(
                    requestId, 
                    externalReference, 
                    externalUrl, 
                    createdDate, 
                    businessType, 
                    branchOffice, 
                    customer,
                    salesPersonId, 
                    projectManagerId, 
                    name, 
                    street, 
                    streetAddon, 
                    zipcode, 
                    city, 
                    countryId,
                    campaign, 
                    origin);
            
        } else if (businessType.isProjectRequest()) {
            
            request = new ProjectRequestImpl(
                    requestId, 
                    externalReference, 
                    externalUrl, 
                    createdDate, 
                    businessType, 
                    branchOffice, 
                    customer,
                    salesPersonId, 
                    projectManagerId, 
                    name, 
                    street, 
                    streetAddon, 
                    zipcode, 
                    city, 
                    countryId,
                    campaign, 
                    origin);
        } else {
            
            request = new SalesRequestImpl(
                    requestId, 
                    externalReference, 
                    externalUrl, 
                    createdDate, 
                    businessType, 
                    branchOffice, 
                    customer,
                    salesPersonId, 
                    projectManagerId, 
                    name, 
                    street, 
                    streetAddon, 
                    zipcode, 
                    city, 
                    countryId,
                    campaign, 
                    origin);
        }
        if (log.isDebugEnabled()) {
            log.debug("createRequestImpl() done [businessId="
                    + requestId + ", object=" 
                    + request.getClass().getName() + "]");
        }
        return request;
    }

    protected final Long getNextRequestId(BusinessType type) {
        Long result = null;
        String generatorType = getPropertyString(SystemConfig.SALES_ID_GENERATOR);
        if ("request".equalsIgnoreCase(generatorType)) {
            if (type == null || type.getWorkflow() == null) {
                log.warn("getNextRequestId() type or workflow is null while salesIdGenerator is request");
            } else if (type.getWorkflow().getSequenceName() != null 
                        && !type.getWorkflow().isIgnoreSequence()) {
                try {
                    result = getNextLong(type.getWorkflow().getSequenceName());
                } catch (Exception e) {
                    log.warn("getNextRequestId() failed on attempt to get id" 
                        + " by provided workflow [sequenceName=" 
                        + type.getWorkflow().getSequenceName() 
                        + ", salesIdGenerator=request, exception="
                        + e.getMessage() + "]");
                }
                
            } else {
                log.warn("getNextRequestId() workflow does not provide sequence while salesIdGenerator is request");
            }
        }
        if (isEmpty(result)) {
            if (log.isDebugEnabled()) {
                log.debug("getNextRequestId() using default sequence of project_plans");
            }
            result = getNextLong("project_plans_plan_id_seq");
        }
        return result;
    }

    protected String createDefaultFcsNoteIfRequired(
            BusinessCase businessCase, 
            FcsAction action, 
            String externalReference,
            String note) {
        if (action != null && action.isNoteRequired()) {
            if (!isEmpty(externalReference)) {
                note = "Provided by external API";
            } else {
                note = "Hint: Action was configured to require a note but non provided!";
            }
        }
        return note;
    }

    public Details addProperty(User user, Details details, String name, String value) {
        Request request = (Request) details;
        BusinessCasePropertyImpl property = new BusinessCasePropertyImpl(
                (user == null ? null : user.getId()), null, null, request.getRequestId(), name, value);
        getCurrentSession().saveOrUpdate(BusinessCasePropertyImpl.class.getName(), property);
        BusinessCase obj = findById(request.getRequestId());
        request = (obj instanceof Request) ? (Request) obj : null;
        if (request == null) {
            request = (obj instanceof Sales) ? ((Sales) obj).getRequest() : null;
        }
        return request;
    }

    public Details updateProperty(User user, Details details, Property property, String value) {
        Request request = (Request) details;
        if (property != null) {
            property.setValue(value);
            if (user != null) {
                property.updateChanged(user.getId());
            } else {
                log.warn("updateProperty() invoked with user object as null!");
            }
            getCurrentSession().saveOrUpdate(BusinessCasePropertyImpl.class.getName(), property);
            BusinessCase obj = findById(request.getRequestId());
            request = (obj instanceof Request) ? (Request) obj : null;
            if (request == null) {
                request = (obj instanceof Sales) ? ((Sales) obj).getRequest() : null;
            }
        }
        return request;
    }

    public Details removeProperty(User user, Details details, Property property) {
        Request request = (Request) details;
        request.removeProperty((user == null ? null : user.getId()), property);
        persist(request);
        return request;
    }

    public BusinessContract createContract(Long user, BusinessCase businessCase) {
        BusinessContract obj = findContract(businessCase);
        if (obj == null) {
            Long reference = getContractReference(businessCase);
            Long defaultTypeId = businessCase.getType().getDefaultContractType() != null ?
                    businessCase.getType().getDefaultContractType() : 
                        (businessCase.getType().getWorkflow() == null ? null :
                            businessCase.getType().getWorkflow().getDefaultContractType());
            if (!isEmpty(defaultTypeId)) {
                ContractType type = getContractType(defaultTypeId);
                if (type == null) {
                    throw new ActionException(ErrorCode.CONFIG_MISSING);
                }
                obj = new SalesContractImpl(
                        type, 
                        reference, 
                        businessCase.getCustomer().getId(), 
                        user, 
                        businessCase.getName());
                getCurrentSession().saveOrUpdate(SalesContractImpl.class.getName(), obj);
            }
        }
        return obj;
    }

    public BusinessContract loadContract(Long id) {
        return (BusinessContract) getCurrentSession().load(
                SalesContractImpl.class.getName(), id);
    }

    public BusinessContract findContract(BusinessCase reference) {
        StringBuilder hql = new StringBuilder(128);
        hql.append("from ").append(SalesContractImpl.class.getName())
                .append(" o where o.reference = :referenceId order by o.id desc");
        List result = getCurrentSession().createQuery(hql.toString())
                .setParameter("referenceId", getContractReference(reference)).list();
        return result.isEmpty() ? null : (BusinessContract) result.get(0);
    }

    public void update(BusinessContract contract) {
        getCurrentSession().saveOrUpdate(SalesContractImpl.class.getName(), contract);
    }

    protected Long getContractReference(BusinessCase businessCase) {
        if (businessCase instanceof Sales) {
            return ((Sales) businessCase).getRequest().getPrimaryKey();
        }
        return businessCase.getPrimaryKey();
    }
}
