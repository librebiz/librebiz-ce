/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 5, 2004 
 * 
 */
package com.osserp.core;

import java.util.Date;

import com.osserp.common.EntityRelation;
import com.osserp.common.Mappable;
import com.osserp.common.XmlAwareEntity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Address extends EntityRelation, Mappable, XmlAwareEntity {

    static final Long UNDEFINED = 0L;
    static final Long BUSINESS = 1L;
    static final Long HOME = 2L;
    static final Long DELIVERY = 3L;
    static final Long PERSON = 4L;
    static final Long INVOICE = 5L;

    /**
     * 
     * @return addressType
     */
    Long getAddressType();

    /**
     * 
     * @param addressType
     */
    void setAddressType(Long addresstype);

    /**
     * Provides the address header as a prefix before name (e.g. salutation on private contacts)
     * @return header
     */
    String getHeader();

    /**
     * Sets the header
     * @param header
     */
    void setHeader(String header);

    /**
     * A name could be used as address addon (c/o)
     * 
     * @return name
     */
    String getName();

    /**
     * Sets the name
     * 
     * @param name
     */
    void setName(String name);

    /**
     * A note for this address
     * 
     * @return note
     */
    String getNote();

    /**
     * Sets the note for this address
     * 
     * @param note
     */
    void setNote(String note);

    /**
     * the street
     * 
     * @return street
     */
    String getStreet();

    /**
     * Sets the street
     * 
     * @param street
     */
    void setStreet(String street);

    /**
     * the streetAddon
     * 
     * @return streetAddon
     */
    String getStreetAddon();

    /**
     * Sets the streetAddon
     * 
     * @param streetAddon
     */
    void setStreetAddon(String streetAddon);

    /**
     * the zipcode
     * 
     * @return zipcode
     */
    String getZipcode();

    /**
     * Sets the zipcode
     * 
     * @param zipcode
     */
    void setZipcode(String zipcode);

    /**
     * the city
     * 
     * @return city
     */
    String getCity();

    /**
     * Sets the city
     * 
     * @param city
     */
    void setCity(String city);

    /**
     * Returns the country
     * 
     * @return country
     */
    Long getCountry();

    /**
     * Sets the country id
     * 
     * @param country id
     */
    void setCountry(Long country);

    /**
     * Returns the federal state id
     * @return federalStateId
     */
    Long getFederalStateId();

    /**
     * Sets the federal state id
     * @param federalStateId
     */
    void setFederalStateId(Long federalStateId);

    /**
     * Provides a manual federal state input. This is useful if a federal states list is not available for a country
     * @return federalStateName
     */
    String getFederalStateName();

    /**
     * Sets the federal state name
     * @param federalStateName
     */
    void setFederalStateName(String federalStateName);

    /**
     * Returns the district id
     * @return districtId
     */
    Long getDistrictId();

    /**
     * Sets the district id
     * @param districtId
     */
    void setDistrictId(Long districtId);

    /**
     * Is this the primary address
     * @return primary
     */
    boolean isPrimary();

    /**
     * Sets this address as primary address
     * @return primary
     */
    void setPrimary(boolean primary);

    /**
     * The last changed-date of the entity. This value is set at creation time
     * 
     * @return changed
     */
    Date getChanged();

    /**
     * Sets the date when the entity data was changed
     * 
     * @param changed
     */
    void setChanged(Date changed);

    /**
     * The user who changed this entity.
     * 
     * @return changedBy
     */
    Long getChangedBy();

    /**
     * Sets the user who changed the entity data
     * 
     * @param changedBy
     */
    void setChangedBy(Long changedBy);

    /**
     * Provides rfc conform postalAddress string (street and address separated by dollar sign)
     * @return postalAddress or empty string on empty street, zipcode and city
     */
    String getPostalAddressString();
}
