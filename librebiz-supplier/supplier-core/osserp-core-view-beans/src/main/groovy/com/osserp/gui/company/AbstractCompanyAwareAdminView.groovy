
/**
 *
 * Copyright (C) 2013, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 1, 2016 
 * 
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsReference

import com.osserp.core.finance.Stock
import com.osserp.core.system.BranchOfficeManager
import com.osserp.core.system.SystemCompany
import com.osserp.core.system.SystemCompanyManager

import com.osserp.gui.CoreAdminView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractCompanyAwareAdminView extends CoreAdminView {
    private static Logger logger = LoggerFactory.getLogger(AbstractCompanyAwareAdminView.class.getName())

    boolean printConfigTargetAvailable = false
    boolean changeCompanyLogoTargetAvailable = false

    DmsDocument companyLogo
    DmsDocument registration

    Stock stock
    protected abstract void loadStock()

    protected AbstractCompanyAwareAdminView() {
        super()
        enableCompanyPreselection()
    }

    String getStockConfigAdminPermissions() {
        return 'client_admin,organisation_admin,runtime_config,executive_saas'
    }

    void toggleStockActivation() {
        systemConfigManager.toggleStockManagement()
        loadStock()
    }

    boolean isCompanyEditable() {
        isPermissionGrant('client_edit,client_create')
    }

    protected void loadCompanyLogo(Long companyId) {
        try {
            List<DmsDocument> logos = dmsManager.findByReference(new DmsReference(SystemCompany.COMPANY_LOGO, companyId))
            if (logos) {
                logos.each { DmsDocument pic ->
                    if (pic.referenceDocument) {
                        companyLogo = pic
                    }
                }
                logger.debug("loadCompanyLogo: load logo done [found=${(companyLogo != null)}]")
            }
        } catch (Exception e) {
            logger.warn("loadCompanyLogo: unable to locate dms on attempt to load logo [message=${e.message}]")
        }
    }

    protected final DmsManager getDmsManager() {
        getService(DmsManager.class.getName())
    }

    protected final BranchOfficeManager getBranchOfficeManager() {
        getService(BranchOfficeManager.class.getName())
    }

    protected final SystemCompanyManager getSystemCompanyManager() {
        getService(SystemCompanyManager.class.getName())
    }

}
