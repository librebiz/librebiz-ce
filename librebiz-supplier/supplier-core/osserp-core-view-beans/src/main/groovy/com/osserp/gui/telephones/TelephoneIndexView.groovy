/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 16, 2013 at 19:12:56 PM 
 * 
 */
package com.osserp.gui.telephones

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.web.PortalView
import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeManager
import com.osserp.core.telephone.TelephoneConfiguration
import com.osserp.core.telephone.TelephoneConfigurationManager
import com.osserp.core.telephone.TelephoneSystem
import com.osserp.core.telephone.TelephoneSystemManager
import com.osserp.core.users.DomainUser
import com.osserp.core.users.Permissions
import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class TelephoneIndexView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TelephoneIndexView.class.getName())

    Boolean userHasTelephone
    Boolean clickToCallEnabled
    Boolean clickToCallEnabledWizard

    TelephoneIndexView() {
        super()
        headerName = 'telephoneIndexViewLabel'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            PortalView portal = env.portalView
            if (portal?.user instanceof DomainUser && portal.user.employee) {
                Employee employee = portal.user.employee
                if (telephoneConfigurationManager.findByEmployeeId(employee.id) != null) {
                    userHasTelephone = true
                }
                if (employee.branch) {
                    if ((Permissions.hasPermission(domainUser, Permissions.TELEPHONE_EXCHANGE_CLICK_TO_CALL) ||
                    Permissions.hasPermission(domainUser, Permissions.TELEPHONE_EXCHANGE_EDIT) ||
                    Permissions.hasPermission(domainUser, Permissions.TELEPHONE_SYSTEM_ADMIN)) &&
                    !telephoneConfigurationManager.search(employee.branch.id, false, true).isEmpty()) {
                        userHasTelephone = true
                    }
                    TelephoneSystem ts = telephoneSystemManager.findByBranch(employee.branch.id)
                    if (ts?.ipRange) {
                        String[] ip = ts.getIpRange().split("X")
                        if (ip[0] != null && domainUser.getRemoteIp().startsWith(ip[0])) {

                            List<TelephoneConfiguration> tConfigs = []
                            if (Permissions.hasPermission(domainUser, Permissions.TELEPHONE_EXCHANGE_CLICK_TO_CALL)) {

                                tConfigs = telephoneConfigurationManager.search(employee.branch.id, false, true)
                            }
                            TelephoneConfiguration tc = telephoneConfigurationManager.find(employee.id, employee.branch.id)
                            if (tc != null) {
                                tConfigs.add(tc)
                            }
                            tConfigs.each {
                                TelephoneConfiguration next = it
                                if (next.internal) {
                                    clickToCallEnabled = true
                                    clickToCallEnabledWizard = true
                                }
                            }
                        }
                    }
                }
                portal.setTelephoneStatus(userHasTelephone, clickToCallEnabled, clickToCallEnabledWizard)
            }
        }
    }

    private TelephoneConfigurationManager getTelephoneConfigurationManager() {
        getService(TelephoneConfigurationManager.class.getName())
    }

    private TelephoneSystemManager getTelephoneSystemManager() {
        getService(TelephoneSystemManager.class.getName())
    }
}
