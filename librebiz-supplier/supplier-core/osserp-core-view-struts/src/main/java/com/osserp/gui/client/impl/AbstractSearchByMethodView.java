/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 20, 2008 8:28:33 AM 
 * 
 */
package com.osserp.gui.client.impl;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.web.SearchByMethodView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractSearchByMethodView extends AbstractQueryView implements SearchByMethodView {
    private static Logger log = LoggerFactory.getLogger(AbstractSearchByMethodView.class.getName());

    private String selectedValue = null;
    private boolean startsWithSet = false;
    private String selectedMethod = null;
    private List<String> methods = new java.util.ArrayList<String>();

    /**
     * Sets view name if annotated and initializes action target as 'success' No methods will be available.
     */
    protected AbstractSearchByMethodView() {
        super();
    }

    /**
     * Sets search methods and default search method, view name if annotated and action target as 'success'
     * @param methods
     * @param defaultMethod
     */
    public AbstractSearchByMethodView(Set<String> methods, String defaultMethod) {
        super();
        this.methods.addAll(methods);
        this.selectedMethod = defaultMethod;
    }

    public List<String> getMethods() {
        return methods;
    }

    public String getSelectedMethod() {
        return selectedMethod;
    }

    public void selectMethod(String name) {
        for (int i = 0, j = methods.size(); i < j; i++) {
            String next = methods.get(i);
            if (next.equals(name)) {
                this.selectedMethod = next;
                if (log.isDebugEnabled()) {
                    log.debug("selectMethod() done [" + name + "]");
                }
                break;
            }
        }
    }

    public boolean isStartsWithSet() {
        return startsWithSet;
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    protected void setSelectedMethod(String selectedMethod) {
        this.selectedMethod = selectedMethod;
    }

    protected void setMethods(List<String> methods) {
        this.methods = methods;
    }

    protected void setStartsWithSet(boolean startsWithSet) {
        this.startsWithSet = startsWithSet;
    }

    protected void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }
}
