/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 17, 2007 4:15:29 PM 
 * 
 */
package com.osserp.gui.client.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.ViewManager;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseManager;
import com.osserp.core.requests.Request;
import com.osserp.core.requests.RequestManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesManager;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.BusinessCaseDependingView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractBusinessCaseDependingView extends AbstractConfigurableBusinessCaseAwareView
        implements BusinessCaseDependingView {
    private static Logger log = LoggerFactory.getLogger(AbstractBusinessCaseDependingView.class.getName());
    private BusinessCaseView businessCaseView = null;
    private boolean viewRequiredWhileInit = true;

    /**
     * Constructor required for serialization. Business case view must be available on init when this constructor is used.<br/>
     * Action target is initialized as 'success'
     */
    protected AbstractBusinessCaseDependingView() {
        super();
    }

    /**
     * Default constructor for dependecy injection provided by spring <br/>
     * Action target is initialized as 'success'
     * @param viewRequiredWhileInit
     */
    protected AbstractBusinessCaseDependingView(boolean viewRequiredWhileInit) {
        super();
        this.viewRequiredWhileInit = viewRequiredWhileInit;
    }

    /**
     * Tries to fetch businessCaseView. Throws {@link com.osserp.common.ActionException ActionException} if businessCaseView not bound and viewRequiredWhileInit
     * is enabled.
     * @param request
     */
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        HttpSession session = request.getSession();
        businessCaseView = (BusinessCaseView) ViewManager.fetchView(session, BusinessCaseView.class);
        if (businessCaseView == null && viewRequiredWhileInit) {
            log.error("init() failed, required business case view not bound");
            throw new ActionException();
        }
        if (log.isDebugEnabled()) {
            log.debug("init() done [businessCaseView="
                    + (businessCaseView == null ? "null" : businessCaseView.getId())
                    + "]");
        }
    }

    @Override
    public BusinessCase getBusinessCase() {
        return businessCaseView != null ? businessCaseView.getBusinessCase() : null;
    }

    public BusinessCaseView getBusinessCaseView() {
        return businessCaseView;
    }

    protected void setBusinessCaseView(BusinessCaseView businessCaseView) {
        this.businessCaseView = businessCaseView;
    }

    public boolean isFlowControlContext() {
        return (businessCaseView != null && businessCaseView.isFlowControlMode());
    }

    /**
     * Provides the sale where current order belongs to
     * @return sales
     * @deprecated use getSales() instead
     */
    public Sales getSale() {
        return getSales();
    }

    protected Request getRequest() {
        if (businessCaseView == null) {
            return null;
        }
        return businessCaseView.getRequest();
    }

    @Override
    protected boolean isSalesContext() {
        return businessCaseView == null ? false : businessCaseView.isSalesContext();
    }

    protected Sales getSales() {
        return (businessCaseView == null ? null : businessCaseView.getSales());
    }

    /**
     * Refreshs associated businessCase view if available
     */
    protected void refresh() {
        if (businessCaseView != null) {
            this.businessCaseView.reload();
        }
    }

    protected final RequestManager getRequestManager() {
        return (RequestManager) getService(RequestManager.class.getName());
    }

    protected final SalesManager getSalesManager() {
        return (SalesManager) getService(SalesManager.class.getName());
    }

    @Override
    protected BusinessCaseManager getBusinessCaseManager() {
        if (isSalesContext()) {
            return getSalesManager();
        }
        return getRequestManager();
    }
}
