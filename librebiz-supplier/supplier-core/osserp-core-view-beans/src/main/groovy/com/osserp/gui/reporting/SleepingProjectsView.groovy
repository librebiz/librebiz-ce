/**
 *
 * Copyright (C) 2008, 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 6, 2008 10:31:27 AM 
 * Created on Jul 13, 2011 4:05:15 PM (Groovy Implementation) 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.projects.ProjectSearch
import com.osserp.core.sales.SalesUtil

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SleepingProjectsView extends AbstractSalesListReportingView {
    // Invocation of class throws exception  about missing logger if removed
    // (even there is no invocation of logger in this class, not clear why):
    private static Logger logger = LoggerFactory.getLogger(SleepingProjectsView.class.getName())
    
    Integer daysCount = 8

    SleepingProjectsView() {
        selectors.businessType = 'salesMonitoringProjects'
        selectors.branch = 'projectAwareBranchsSelection'
        selectors.status = true
        refreshLink = true
        autosort = false
    }

    protected void init() {
        daysCount = form.getInteger('daysCount')?: daysCount
    }

    @Override
    void loadList() {
        list = SalesUtil.filter(domainUser, projectSearch.findSleepingProjects(daysCount))
        actualSortKey = 'lastActionDate'
    }

    private ProjectSearch getProjectSearch() {
        getService(ProjectSearch.class.getName())
    }
}
