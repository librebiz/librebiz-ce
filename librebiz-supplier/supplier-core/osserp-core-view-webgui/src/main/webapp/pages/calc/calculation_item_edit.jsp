<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:if test="${empty sessionScope.calculationView}">
    <o:logger write="No calculation view found in session context!" level="info" />
</c:if>
<c:if test="${empty sessionScope.calculationView.bean}">
    <o:logger write="View found but no calculation loaded!" level="info" />
</c:if>
<c:set var="view" value="${sessionScope.calculationView}" />
<c:set var="calculation" value="${view.bean}" />
<c:set var="calculator" value="${view.calculator}" />
<c:set var="item" value="${view.calculator.selectedItem}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="changePosition" />
    </tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/calculationItemEdit.do?method=exit"/>"><o:img name="backIcon" /></a></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><fmt:message key="product" /></th>
                            <th><fmt:message key="description" /></th>
                            <th class="right" style=""><fmt:message key="quantity" /></th>
                            <th class="right" style=""><fmt:message key="quantityUnitShort" /></th>
                            <th class="right" style=""><span title="<fmt:message key="partnerPrice"/>"><fmt:message key="partnerPriceShort" /></span></th>
                            <th class="right" style=""><fmt:message key="price" /></th>
                            <th colspan="2" style=""><fmt:message key="actions" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <o:form name="itemListForm" url="/calculationItemEdit.do">
                            <input type="hidden" name="method" value="update" /> <input type="hidden" name="partnerPriceSource" value="<o:number value="${item.partnerPrice}" format="decimal"/>" />
                            <tr class="lightgrey">
                                <td><o:out value="${item.product.productId}" /></td>
                                <td>
                                    <oc:systemPropertyEnabled name="productCustomNameSupport">
                                        <c:choose>
                                            <c:when test="${!item.product.affectsStock and (item.product.group.customNameSupport or item.product.category.customNameSupport)}">
                                                <input type="text" class="form-control recordTextColumn" style="text-align:left;" name="customName" value="<o:out value="${item.productName}"/>"/>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="recordTextColumn"><o:out value="${item.product.name}" /></div>
                                            </c:otherwise>
                                        </c:choose>
                                    </oc:systemPropertyEnabled>
                                    <oc:systemPropertyDisabled name="productCustomNameSupport">
                                        <div class="recordTextColumn"><o:out value="${item.product.name}" /></div>
                                    </oc:systemPropertyDisabled>
                                </td>
                                <td class="right"><input class="form-control right" type="text" name="quantity" value="<o:number value="${item.quantity}" format="decimal"/>" /></td>
                                <td class="right"><oc:options name="quantityUnits" value="${item.product.quantityUnit}" /></td>
                                <td class="right"><c:choose>
                                        <c:when test="${item.partnerPriceEditable or calculationView.overridePartnerPricePermissionGrant}">
                                            <input class="form-control right" type="text" name="partnerPrice" value="<o:number value="${item.partnerPrice}" format="decimal"/>" <c:if test="${!item.partnerPriceEditable and calculationView.overridePartnerPricePermissionGrant}"> style="background-color: #FBC1C5;"</c:if> />
                                        </c:when>
                                        <c:otherwise>
                                            <input class="form-control right" type="text" name="partnerPrice" value="<o:number value="${item.partnerPrice}" format="decimal"/>" readonly="readonly" />
                                        </c:otherwise>
                                    </c:choose></td>
                                <td class="right"><input class="form-control right" type="text" name="price" value="<o:number value="${item.price}" format="decimal"/>" /></td>
                                <td class="center">
                                    <a href="<c:url value="/calculationItemEdit.do?method=exit"/>" title="<fmt:message key='exitEdit'/>"><o:img name="backIcon" /></a> 
                                </td>
                                <td class="center">
                                    <input class="smallicon" type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="update" onclick="setMethod('update');" title="<fmt:message key='saveInput'/>" />
                                </td>
                            </tr>
                            <tr class="lightgrey">
                                <td></td>
                                <td colspan="5">
                                    <textarea name="note" class="form-control recordTextColumn" rows="4"><o:out value="${item.note}" /></textarea>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                        </o:form>
                    </tbody>
                </table>
                <div class="spacer"></div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
