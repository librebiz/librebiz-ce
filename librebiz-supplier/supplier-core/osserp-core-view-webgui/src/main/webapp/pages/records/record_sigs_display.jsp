<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>

<c:if test="${!empty record.personId and !empty view.contactPerson}">
    <div class="recordLabelValue">
        <span><fmt:message key="attn"/>:
            <c:if test="${!empty view.contactPerson.salutation}">
                <oc:options name="salutations" value="${view.contactPerson.salutation}"/>
            </c:if>
            <o:out value="${view.contactPerson.displayName}"/>
        </span>
    </div>
</c:if>
<c:if test="${!empty record.signatureLeft and record.signatureLeft > 0}">
    <div class="recordLabelValue">
        <span><fmt:message key="signatureLeft"/>:
            <o:ajaxLink linkId="signatureLeft" url="${'/employeeInfo.do?id='}${record.signatureLeft}">
                <oc:employee value="${record.signatureLeft}"/>
            </o:ajaxLink>
        </span>
    </div>
</c:if>
<c:if test="${!empty record.signatureRight and record.signatureRight > 0}">
    <div class="recordLabelValue">
        <span><fmt:message key="signatureRight"/>:
            <c:choose>
                <c:when test="${empty record.signatureRight}"><fmt:message key="undefined"/></c:when>
                <c:otherwise>
                    <o:ajaxLink linkId="signatureRight" url="${'/employeeInfo.do?id='}${record.signatureRight}">
                        <oc:employee value="${record.signatureRight}"/>
                    </o:ajaxLink>
                </c:otherwise>
            </c:choose>
        </span>
    </div>
</c:if>
