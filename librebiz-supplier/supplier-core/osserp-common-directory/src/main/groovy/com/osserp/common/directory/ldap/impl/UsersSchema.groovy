/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.StructuralSchema
import com.osserp.common.directory.ldap.SubSchema
import com.osserp.common.util.SecurityUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class UsersSchema extends AbstractLdapConfigAware implements StructuralSchema {
    private static Logger logger = LoggerFactory.getLogger(UsersSchema.class.getName())

    private static final List OBJECT_CLASSES = [
        'top',
        'person',
        'organizationalPerson',
        'inetOrgPerson',
        'posixAccount',
        'shadowAccount',
        'osserpUser'
    ]

    private static final List AVAILABLE_ATTRIBUTES = [
        'uid',
        'uidnumber',
        'gidnumber',
        'osserpuid',
        'gecos',
        'title',
        'cn',
        'sn',
        'givenname',
        'displayname',
        'initials',
        'description',
        'street',
        'postalcode',
        'l',
        'st',
        'homepostaladdress',
        'mail',
        'telephonenumber',
        'mobile',
        'facsimiletelephonenumber',
        'homephone',
        'homedirectory',
        'loginshell',
        'preferredlanguage',
        'userpassword',
        'osserpcategory',
        'employeenumber',
        'employeetype',
        'departmentnumber',
        'internationalisdnnumber',
        'jpegphoto',
        'labeleduri',
        'manager',
        'ou'
    ]

    private static final List DISPLAY_ATTRIBUTES = [
        'uid',
        'uidnumber',
        'gidnumber',
        'osserpuid',
        'employeenumber',
        'osserpcategory',
        'employeetype',
        'title',
        'cn',
        'sn',
        'givenname',
        'displayname',
        'description',
        'street',
        'postalcode',
        'l',
        'st',
        'telephonenumber',
        'mobile',
        'mail',
        'ou'
    ]

    private static final String DEFAULT_OU = 'Users'

    SubSchema dbmailSchema
    SubSchema oxSchema
    SubSchema zarafaSchema

    UsersSchema(LdapConfig configInstance) {
        super(configInstance)
        if (configInstance.oxSupportEnabled) {
            oxSchema = new OXSchema(configInstance)
        }
        if (configInstance.zarafaSupportEnabled) {
            zarafaSchema = new ZarafaUsersSchema(configInstance)
        }
        if (configInstance.dbmailSupportEnabled) {
            dbmailSchema = new DbmailUsersSchema(configInstance)
        }
    }

    /**
     * Creates a new ldap ready user object by default and values provided by map
     * @param config with default values
     * @param values must contain at least uid and uidnumber
     * @return user object without dn
     */
    Map create(Map values) {
        String uid = values.uid
        Long uidNumber = fetchLong(values.uidnumber)
        Map user = [
            uid : uid,
            uidnumber : uidNumber.toString(),
            gidnumber: (values.gidnumber ?: config?.userGroupDefault),
            givenname : values.givenname,
            sn : values.sn,
            cn : values.cn,
            displayname: values.displayname,
            gecos : values.gecos,
            osserpcategory : values.osserpcategory,
            description: values.description,
            o : values.o,
            st : values.st,
            l : values.l,
            postalcode : values.postalcode,
            street : values.street,
            telephonenumber : values.telephonenumber,
            mobile : values.mobile,
            facsimiletelephonenumber : values.facsimiletelephonenumber,
            employeetype : values.employeetype,
            employeenumber : values.employeenumber,
            userwww : (values.userwww ?: values.www),
            homephone : values.homephone,
            homepostaladdress : values.homepostaladdress,
            physicaldeliveryofficename : values.physicaldeliveryofficename,
            mail : values.mail,
            c : (values.c ?: 'DE'),
            ou : (values.ou ?: DEFAULT_OU),
            objectclass: objectClasses,
            loginshell : (config?.userShellDisabled ?: '/bin/false'),
            homedirectory : "${(config?.userHomeDirectory ?: '/home')}/${uid}".toString(),
            mailstoredir : (config?.userMailStoreDir ?: 'maildir:~/Maildir'),
            mailhomedir : createMailHomeDir(uid, values.clientname)
        ]
        if (values.password) {
            user.userpassword = SecurityUtil.createPassword(values.password).getBytes()
        }
        String aliasName = getMailAliasAttribute()
        if (aliasName && values[aliasName]) {
            user[aliasName] = values[aliasName]
        }
        if (config?.dbmailSupportEnabled && dbmailSchema) {
            user = dbmailSchema.create(user, values)
        } else if (config?.zarafaSupportEnabled && zarafaSchema) {
            user = zarafaSchema.create(user, values)
        } else if (config?.oxSupportEnabled && oxSchema) {
            user = oxSchema.create(user, values)
        }
        //logger.debug("create: done [dbmail=${(dbmailSchema != null)}, zarafa=${(zarafaSchema != null)}, ox=${(oxSchema != null)}]\nResulting Values:\n${user}")
        return user
    }

    Map apply(Map values, Map source) {
        if (!source.c && !values.c) {
            values.c = 'DE'
        }
        if (!source.ou && !values.ou) {
            values.ou = DEFAULT_OU
        }
        if (objectClasses != source.objectclass) {
            values.objectclass = objectClasses
        }
        if (!source.loginshell && !values.loginshell) {
            values.loginshell = (config?.userShellDisabled ?: '/bin/false')
        }
        values.homedirectory = "${(config?.userHomeDirectory ?: '/home')}/${source?.uid}".toString()
        values.mailstoredir = (config?.userMailStoreDir ?: 'maildir:~/Maildir')
        String client = fetchClientName(source.dn)
        values.mailhomedir = createMailHomeDir(source?.uid, client)

        if (dbmailSchema && (config?.dbmailSupportEnabled || schemaActivated(dbmailSchema, source))) {
            values = dbmailSchema.apply(values, source)
        } else if (zarafaSchema && (config?.zarafaSupportEnabled || schemaActivated(zarafaSchema, source))) {
            values = zarafaSchema.create(values, source)
        } else if (oxSchema && (config?.oxSupportEnabled || schemaActivated(oxSchema, source))) {
            values = oxSchema.create(values, source)
        }
        // TODO remove a schema if no longer enabled
        values
    }

    /*
     Map deploy(Map values, final Map source) {
         values.sn = source.sn
         values.cn = source.cn
         values.displayname = source.displayname
         values.gecos = source.gecos
         values.osserpcategory = source.osserpcategory
         values.description = source.description
         values.o = source.o
         values.st = source.st
         values.l = source.l
         values.postalcode = source.postalcode
         values.street = source.street
         values.employeetype = source.employeetype
         values.employeenumber = source.employeenumber
         values.userwww = (source.userwww ?: source.www)
         values.homepostaladdress = source.homepostaladdress
         values.physicaldeliveryofficename = source.physicaldeliveryofficename
         values.mail = source.mail
         values
     }
     */

    String getMinimumId() {
        config?.userIdStart ?: '6001'
    }

    String getQuery() {
        config?.userQuery ?: '(uid=*)'
    }

    List<String> getObjectClasses() {
        config?.userObjectClasses ?: OBJECT_CLASSES
    }

    List<String> getAvailableAttributes() {
        AVAILABLE_ATTRIBUTES
    }


    Set<String> getAvailableArrays() {
        Set<String> result = new HashSet<String>()
        if (config?.userMailaliasAttribute) {
            result.add(config.userMailaliasAttribute)
        } else if (config?.dbmailSupportEnabled) {
            result.add('mailalternateaddress')
        } else if (config?.zarafaSupportEnabled) {
            result.add('zarafaAliases')
        } else if (config?.oxSupportEnabled) {
            result.add('alias')
        } else {
            result.add('osserpmailalias')
        }
        return result
    }

    String getMailAliasAttribute() {
        if (config?.userMailaliasAttribute) {
            return config.userMailaliasAttribute
        } else if (config?.dbmailSupportEnabled) {
            return 'mailalternateaddress'
        } else if (config?.zarafaSupportEnabled) {
            return 'zarafaAliases'
        } else if (config?.oxSupportEnabled) {
            return 'alias'
        } else {
            return 'osserpmailalias'
        }
    }

    List<String> getDefaultDisplayAttributes() {
        DISPLAY_ATTRIBUTES
    }

    String createMailHomeDir(String username, String clientname) {
        (clientname != null ? "$clientname/$username" : username)
    }
}

