/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Aug 02, 2011 8:50:59 AM 
 * 
 */
package com.osserp.core.service.telephone;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.dao.telephone.TelephoneCallContacts;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.telephone.TelephoneCallContact;
import com.osserp.core.telephone.TelephoneCallContactManager;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneCallContactManagerImpl extends AbstractService implements TelephoneCallContactManager {
    private static Logger log = LoggerFactory.getLogger(TelephoneCallContactManagerImpl.class.getName());

    private TelephoneCallContacts telephoneCallContactsDao = null;

    public TelephoneCallContactManagerImpl(TelephoneCallContacts telephoneCallContactsDao) {
        super();
        this.telephoneCallContactsDao = telephoneCallContactsDao;
    }

    public List<TelephoneCallContact> find(Long telephoneCallId) {
        if (log.isDebugEnabled()) {
            log.debug("find(" + telephoneCallId + ") invoked ...");
        }
        if (telephoneCallId != null) {
            return telephoneCallContactsDao.find(telephoneCallId);
        }
        return new ArrayList<TelephoneCallContact>();
    }
}
