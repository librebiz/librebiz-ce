/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2020
 * 
 */
package com.osserp.core;


import com.osserp.common.Option;
import com.osserp.common.Parameter;
import com.osserp.common.gui.MenuLink;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PluginHook extends Option, Parameter {

    /**
     * Provides the type of the hook (url, service, ...)
     * @return type of hook
     */
    String getHookType();

    /**
     * Provides the name of the link icon for url based hooks 
     * @return iconname
     */
    String getIconName();

    /**
     * Provides a comma separed list of hook invocation parameter names 
     * @return parameter name string or null if not exists
     */
    String getParamNames();

    /**
     * Provides hook invocation parameter names 
     * @return parameter names or null if none exist
     */
    String[] getParameters();

    /**
     * Provides a comma separated list of required permissions
     * to invoke the hook.
     * @return permissions or null if no permissions defined
     */
    String getPermissions();

    /**
     * Indicates page snippet hook
     * @return true if type of hook is page
     */
    boolean isPage();

    /**
     * Indicates if logged in user is required to invoke hook
     * @return true if logged in user required
     */
    boolean isUserRequired();

    /**
     * Indicates url hook
     * @return true if type of hook is url
     */
    boolean isUrl();

    /**
     * Provides the parent hook if the hook is child of another hook
     * @return parentId or null if not child hook
     */
    Long getParentId();

    /**
     * Provides the order id if hook is member of an ordered list
     * @return order id or 0 if not defined
     */
    int getOrderId();

    /**
     * Provides the url as link
     * @return url as link or null if hook does not represent an url
     */
    MenuLink getLink();
}
