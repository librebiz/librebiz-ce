<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="originator"><fmt:message key="originator" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <o:out value="${view.receivedMail.originator}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="recipients"><fmt:message key="recipient" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <o:out value="${view.receivedMail.recipient}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="owner"><fmt:message key="responsible" /></label>
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <c:choose>
                <c:when test="${!empty view.receivedMail.userId}">
                    <oc:employee value="${view.receivedMail.userId}"/>
                </c:when>
                <c:otherwise>
                    <fmt:message key="undefined" />
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <c:if test="${view.adminMode and empty view.receivedMail.reference
                    and view.user.userEmployee and view.user.id != view.receivedMail.userId}">
                <v:link url="${view.baseLink}/takeover" title="takeOver">
                    <o:img name="enabledIcon" />
                </v:link>
            </c:if>
        </div>
    </div>
</div>
