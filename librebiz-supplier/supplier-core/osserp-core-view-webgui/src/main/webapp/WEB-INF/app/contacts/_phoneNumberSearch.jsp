<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.phoneNumberSearchView}" />
<c:set var="selectUrl" value="${requestScope.selectUrl}" />
<c:set var="selectMethod" value="${requestScope.selectMethod}" />
<c:set var="selectExit" value="${requestScope.selectExit}" />

<c:if test="${!empty sessionScope.statusMessage}">
	<div class="recordheader">
		<p><o:out value="${sessionScope.statusMessage}" /></p>
	</div>
	<c:remove var="statusMessage" scope="session" />
</c:if>
<div class="table-responsive table-responsive-default">
	<table class="table table-striped">
		<thead>
			<tr>
				<th style="width: 180px;" id="phone"><fmt:message key="phone" /></th>
				<th style="width: 35px;" id="type"><fmt:message key="type"/></th>
				<th style="width: 371px;" id="name"><fmt:message key="companySlashName" /></th>
				<th style="width: 175px;" id="city"><fmt:message key="city" /></th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty view.list}">
					<tr>
						<td valign="top" colspan="4">
							<fmt:message key="searchUnsuccessfulPleaseChangeYourInput" />
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="contact" items="${view.list}" varStatus="s">
						<tr>
							<td style="width: 180px;" valign="top">
								<c:choose>
									<c:when test="${contact.fax}">
										<o:out value="${contact.phoneDisplay}" />
									</c:when>
									<c:otherwise>
										<oc:phone provideIcon="true" value="${contact.phoneDisplay}"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td style="width: 35px;" valign="top"><o:out value="${contact.phoneTypeDisplay}" /></td>
							<td style="width: 371px;" valign="top">
								<a href="<v:url value="/contacts/phoneNumberSearch/select?id=${contact.primaryKey}"/>"><o:out value="${contact.displayName}" /></a>
								<c:if test="${!empty contact.parent}"> <a href="<c:url value="/contacts.do?method=load&exit=contactSearchResult&id=${contact.parentContact}"/>">(<o:out value="${contact.parent}"/>)</a></c:if>
							</td>
							<td style="width: 175px;" valign="top"><o:out value="${contact.addressDisplay}" /></td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>
