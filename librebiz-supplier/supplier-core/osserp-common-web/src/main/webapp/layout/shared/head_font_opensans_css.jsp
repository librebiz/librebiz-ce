<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="<c:url value="/layout/fonts/opensans_bold_macroman/stylesheet.css"/>">
<link rel="stylesheet" href="<c:url value="/layout/fonts/opensans_light_macroman/stylesheet.css"/>">
<link rel="stylesheet" href="<c:url value="/layout/fonts/opensans_regular_macroman/stylesheet.css"/>">
<link rel="stylesheet" href="<c:url value="/layout/fonts/opensans_semibold_macroman/stylesheet.css"/>">
