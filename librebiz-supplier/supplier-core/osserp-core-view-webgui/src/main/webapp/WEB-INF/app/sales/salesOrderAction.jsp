<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesOrderActionView" />

<c:set var="recordView" scope="session" value="${sessionScope.salesOrderView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="hadSomethingToDisplay" value="false"/>
<c:if test="${!empty sessionScope.businessCaseView}">
    <c:set var="sales" value="${sessionScope.businessCaseView.sales}" />
</c:if>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="order" />
        <o:out value="${record.number}" /> / <o:out value="${record.contact.displayName}" />
        <c:if test="${!empty sales}">
            <span> / <fmt:message key="project" /> <o:out value="${sales.id}" /></span>
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="salesOrderActionContent">
            <div class="row">

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><fmt:message key="actions" /></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive table-responsive-default">
                            <table class="table">
                                <tbody>

                                    <c:if test="${!empty sales && !sales.closed and !sales.cancelled and !sales.deliveryClosed and !sales.type.recordByCalculation and record.unchangeable and !record.canceled}">
                                        <o:permission role="sales_order_change_closed" info="salesOrderChangeClosed">
                                            <tr>
                                                <td><a href="<c:url value="/salesOrder.do?method=reopen"/>"><fmt:message key="salesOrderChangeClosed" /></a></td>
                                            </tr>
                                            <c:set var="hadSomethingToDisplay" value="true"/>
                                        </o:permission>
                                    </c:if>

                                    <c:if test="${record.unchangeable and !record.canceled and empty record.openDeliveries}">
                                        <o:forbidden role="executive,warranty_replacement_create">
                                            <tr>
                                                <td><fmt:message key="warrantyReplacement"/></td>
                                            </tr>
                                        </o:forbidden>
                                        <o:permission role="executive,warranty_replacement_create" info="permissionWarrantyReplacement">
                                            <tr>
                                                <td><a href="<c:url value="/salesOrder.do?method=forwardWarrantyReplacement"/>"><fmt:message key="warrantyReplacement"/></a></td>
                                            </tr>
                                        </o:permission>
                                        <c:set var="hadSomethingToDisplay" value="true"/>
                                    </c:if>

                                    <c:if test="${record.contact.discountEnabled or record.rebateAvailable}">
                                        <c:choose>
                                            <c:when test="${record.rebateAvailable}">
                                                <tr>
                                                    <td>
                                                        <fmt:message key="rebateEnabled"/>
                                                        <c:if test="${!record.closed}">
                                                            <a href="<c:url value="/salesOrder.do?method=removeRebate"/>">[<fmt:message key="deactivate"/>]</a>
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <tr>
                                                    <td>
                                                        <fmt:message key="rebateDisabled"/>
                                                        <c:if test="${!record.closed}">
                                                            <a href="<c:url value="/salesOrder.do?method=addRebate"/>">[<fmt:message key="activate"/>]</a>
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:set var="hadSomethingToDisplay" value="true"/>
                                    </c:if>

                                    <c:if test="${!hadSomethingToDisplay}">
                                        <tr>
                                            <td><fmt:message key="noActionsAvailableOnCurrentRecordState"/></td>
                                        </tr>
                                    </c:if>

                                </tbody>
                            </table>
                        </div>
                    </div> <!-- panel-body -->
                </div> <!-- panel-area -->

                <v:form url="${view.baseLink}/printAs" name="salesOrderForm">
                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4><fmt:message key="versionArchive"/></h4>
                            </div>
                        </div>
                        <div class="panel-body">

                            <c:forEach var="obj" items="${view.documentVersions}" varStatus="s">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="voucherLabel">
                                                <c:if test="${s.index == 0}">
                                                    <fmt:message key="archive" />
                                                </c:if>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <v:link url="${view.baseLink}/printVersion?id=${obj.id}">
                                                <c:choose>
                                                    <c:when test="${empty obj.headerName}">
                                                        <fmt:message key="version"/> <o:out value="${obj.version}"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:out value="${obj.headerName}"/>
                                                    </c:otherwise>
                                                </c:choose>
                                                <fmt:message key="from"/> <o:date value="${obj.created}"/>
                                            </v:link>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <v:link url="${view.baseLink}/printVersion?id=${obj.id}">
                                                <o:img name="printIcon" />
                                            </v:link>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <v:link url="${view.baseLink}/forwardMail?id=${obj.id}">
                                                <o:img name="mailIcon" />
                                            </v:link>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>

                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="voucherLabel"><fmt:message key="printSalesOrderAsLabel" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select name="selectedHeader" class="form-control">
                                            <c:forEach var="obj" items="${view.headerSelection}" varStatus="s">
                                                <option value="${obj}"><fmt:message key="${obj}" /></option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="customHeaderName"><fmt:message key="customVoucherLabel" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <v:text name="customHeader" styleClass="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label> </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label for="previewCheckbox">
                                                <v:checkbox id="preview" value="${view.preview}" />
                                                <fmt:message key="preview" />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row next">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <o:submit styleClass="form-control" value="print" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </v:form>

            </div> <!-- row -->
        </div> <!-- content-area -->
    </tiles:put>
</tiles:insert>
