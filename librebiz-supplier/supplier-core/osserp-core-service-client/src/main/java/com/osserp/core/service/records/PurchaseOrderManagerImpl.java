/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Feb-2007 17:38:24 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;

import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.DeliveryConditions;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.ProjectSuppliers;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.Users;
import com.osserp.core.dao.records.PurchaseDeliveryNotes;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.dao.records.RecordBackups;
import com.osserp.core.dao.records.RecordVersionsArchive;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryCondition;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.Stock;
import com.osserp.core.products.Product;
import com.osserp.core.projects.ProjectSupplier;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.purchasing.PurchaseOrderManager;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;
import com.osserp.core.tasks.PurchaseOrderChangedSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseOrderManagerImpl extends AbstractOrderManager implements PurchaseOrderManager {
    private static Logger log = LoggerFactory.getLogger(PurchaseOrderManagerImpl.class.getName());
    private DeliveryConditions deliveryConditions = null;
    private PurchaseOrderChangedSender orderChangedSender = null;
    private ProjectSuppliers projectSuppliers = null;
    private Suppliers suppliers = null;

    public PurchaseOrderManagerImpl(
            PurchaseOrders records,
            PurchaseDeliveryNotes deliveryNotes,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            RecordBackups recordBackups,
            RecordVersionsArchive archive,
            Users users,
            DeliveryConditions deliveryConditions,
            Suppliers suppliers,
            ProjectSuppliers projectSuppliers,
            PurchaseOrderChangedSender orderChangedSender) {
        super(records, deliveryNotes, systemConfigManager, products,
                productPlanningTask, recordBackups, archive, users);
        this.deliveryConditions = deliveryConditions;
        this.orderChangedSender = orderChangedSender;
        this.projectSuppliers = projectSuppliers;
        this.suppliers = suppliers;
    }

    @Override
    public Record load(Long recordId) {
        PurchaseOrder order = (PurchaseOrder) super.load(recordId);
        List<DeliveryNote> notes = getDeliveryNotes(order);
        for (int i = 0, j = notes.size(); i < j; i++) {
            DeliveryNote next = notes.get(i);
            order.getDeliveryNotes().add(next);
        }
        return order;
    }

    @Override
    protected boolean deleteItemValidation(Employee user, Record record, Item item) {
        // do not remove item if delivery exists
        return item.getOutstanding() != 0;
    }

    public List<Option> findCurrentSuppliers() {
        return getOrdersDao().findCurrentSuppliers();
    }

    public List<Record> findOpenReleased(Product product, boolean descendant) {
        return getOrdersDao().getOpenReleased(product, descendant);
    }

    public List<RecordDisplay> findOpenByConfirmation(boolean confirmed) {
        return getOrdersDao().findOpenByConfirmation(confirmed);
    }

    public List<RecordDisplay> findOpenByContact(Long contactId) {
        return getOrdersDao().findOpenByContact(contactId);
    }

    public PurchaseOrder create(Employee user, Long company, Long branchId, ClassifiedContact supplier) {
        return (PurchaseOrder) getOrdersDao().create(user, company, branchId, supplier, null);
    }

    public PurchaseOrder create(Employee user, PurchaseOrder order, List<Item> carryOver) {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked for carry over from order " + order.getId());
        }
        return getOrdersDao().create(user, order, carryOver);
    }

    public PurchaseOrder create(Employee user, PurchaseOrder order) {
        return getOrdersDao().create(user, order);
    }

    public PurchaseOrder create(
            Employee user,
            SystemCompany company,
            BranchOffice branch,
            ClassifiedContact supplier,
            Order salesOrder,
            List<Item> selectedOrderItems) {

        PurchaseOrder order = (PurchaseOrder) getOrdersDao().create(
                user,
                company.getId(),
                branch.getId(),
                supplier,
                salesOrder == null ? null : salesOrder.getBusinessCaseId());

        if (selectedOrderItems != null) {
            Stock stock = getStockByRecord(order);
            boolean supplierProductGroupsCollectorEnabled = isSystemPropertyEnabled("supplierProductGroupsCollectorEnabled");
            for (int i = 0, j = selectedOrderItems.size(); i < j; i++) {
                Item item = selectedOrderItems.get(i);
                if (item.getOutstanding() > 0) {
                    try {
                        BigDecimal price = new BigDecimal(item.getProduct().getSummary().getLastPurchasePrice());
                        addItem(
                                user,
                                order,
                                stock != null ? stock.getId() : company.getId(),
                                item.getProduct(),
                                item.getCustomName(),
                                item.getOutstanding(),
                                item.getTaxRate(),
                                price,
                                null);
                        if (supplierProductGroupsCollectorEnabled && suppliers != null
                                && supplier instanceof Supplier) {
                            suppliers.addProductGroup((Supplier) supplier, item.getProduct().getGroup());
                        }
                    } catch (Exception e) {
                        log.error("create() failed on attempt to add an item [message=" + e.getMessage() + "]", e);
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + order.getId() + ", sales=" + (salesOrder != null ? salesOrder.getBusinessCaseId() : null) + "]");
        }
        return order;
    }

    public void updateSalesReference(PurchaseOrder order, BusinessCase businessCase) {
        order.setSalesReference(businessCase);
        getOrdersDao().save(order);
    }

    public void update(
            Order order,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country) throws ClientException {
        order.updateDeliveryAddress(name, street, streetAddon, zipcode, city, country);
        getDao().save(order);
    }

    public void update(
            Employee user,
            Order order,
            Date deliveryDate,
            String deliveryNote,
            String itemBehaviour,
            boolean deliveryConfirmed) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("update() invoked, values [user="
                    + user.getId() + ", order="
                    + order.getId() + ", deliveryDate="
                    + deliveryDate + ", deliveryNote="
                    + deliveryNote + ", itemBehaviour="
                    + itemBehaviour + ", deliveryConfirmed="
                    + deliveryConfirmed + "]");
        }
        List<DeliveryDateChangedInfo> changeInfos = getOrdersDao().update(
                user,
                order,
                deliveryDate,
                deliveryNote,
                itemBehaviour,
                deliveryConfirmed);
        List<Item> items = new ArrayList<Item>();
        items.addAll(new ArrayList<Item>(order.getItems()));
        items.addAll(new ArrayList<Item>(order.getDeliveries()));
        createProductPlanningRefreshTask(order, items);
        if (orderChangedSender != null && !changeInfos.isEmpty()) {
            orderChangedSender.sendDeliveryDateChangedInfo(order, changeInfos);
        }
    }

    public void update(
            Employee user,
            Order order,
            Date deliveryDate,
            String deliveryNote,
            Item item) throws ClientException {
        DeliveryDateChangedInfo changeInfo = getOrdersDao().update(
                user,
                order,
                deliveryDate,
                deliveryNote,
                item);
        List<Item> items = new ArrayList<Item>();
        items.addAll(new ArrayList<Item>(order.getItems()));
        items.addAll(new ArrayList<Item>(order.getDeliveries()));
        createProductPlanningRefreshTask(order, items);
        if (orderChangedSender != null && changeInfo != null) {
            orderChangedSender.sendDeliveryDateChangedInfo(order, changeInfo);
        }
    }

    @Override
    public void delete(Record record, Employee user) throws ClientException {
        PurchaseOrder order = (PurchaseOrder) record;
        if (!order.getInvoices().isEmpty() || !order.getDeliveryNotes().isEmpty()) {
            throw new ClientException(ErrorCode.RECORD_UNCHANGEABLE);
        }
        Long status = record.getStatus() == null ? 0L : record.getStatus();
        super.delete(record, user);
        ProjectSupplier ps = projectSuppliers.findByVoucher(record.getType().getId(), record.getId());
        if (ps != null) {
            ps.resetVoucher();
            projectSuppliers.save(ps);
        }
        if (orderChangedSender != null && status > Record.STAT_PRINT) {
            StringBuilder message = new StringBuilder();
            for (int i = 0, j = record.getItems().size(); i < j; i++) {
                OrderItem next = (OrderItem) record.getItems().get(i);
                message
                        .append(next.getDelivery() == null ? "n/a" : DateFormatter.getDate(next.getDelivery()))
                        .append(" : ")
                        .append(next.getProduct().getProductId())
                        .append(" - ")
                        .append(NumberFormatter.getIntValue(next.getQuantity()))
                        .append(" x ")
                        .append(next.getProduct().getName())
                        .append("\n");
            }
            orderChangedSender.sendUpdateEvent(
                    user,
                    order,
                    "purchaseOrderDeleted",
                    message.toString());
        }
    }

    @Override
    public ItemChangedInfo addItem(
            Employee user,
            Record record,
            Product product,
            Long stockId,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            String note) throws ClientException {
        // we don't provide purchase orders with multiple items
        // of same product
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            Item next = record.getItems().get(i);
            if (next.getProduct() != null && next.getProduct().getProductId().equals(product.getProductId())) {
                throw new ClientException(ErrorCode.ITEM_EXISTING);
            }
        }
        return addItem(user, record, stockId, product, customName, quantity, taxRate, price, note);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.service.records.AbstractRecordManager#deleteItem(com.osserp.core.employees.Employee, com.osserp.core.finance.Record,
     * com.osserp.core.Item)
     */
    @Override
    public ItemChangedInfo deleteItem(Employee user, Record record, Item item) {
        ItemChangedInfo info = super.deleteItem(user, record, item);
        if (info != null) {
            ItemChangedInfo result = getOrdersDao().createItemChangedInfo(info);
            if (orderChangedSender != null && result != null) {
                orderChangedSender.sendItemChangedInfo((Order) record, result);
            }
            if (result != null) {
                return result;
            }
        }
        return info;
    }

    public void changeDeliveryConfirmation(Employee user, Order order, Long itemId) {
        if (log.isDebugEnabled()) {
            log.debug("changeDeliveryConfirmation() invoked [order="
                    + order.getId() + ":item=" + itemId + "]");
        }
        OrderItem item = getSelectedItem(order, itemId);
        if (item != null) {
            if (log.isDebugEnabled()) {
                log.debug("changeDeliveryConfirmation() item fetched");
            }
            item.setDeliveryConfirmed(!item.isDeliveryConfirmed());
            persist(order);
            if (log.isDebugEnabled()) {
                log.debug("changeDeliveryConfirmation() done");
            }
            if (orderChangedSender != null) {
                StringBuilder message = new StringBuilder();
                if (item.isDeliveryConfirmed()) {
                    message.append("OK => ");
                } else {
                    message.append("?! => ");
                }
                message
                        .append(item.getDelivery() == null ? "n/a" : DateFormatter.getDate(item.getDelivery()))
                        .append(" : ")
                        .append(item.getProduct().getProductId())
                        .append(" - ")
                        .append(NumberFormatter.getIntValue(item.getQuantity()))
                        .append(" x ")
                        .append(item.getProduct().getName());
                orderChangedSender.sendUpdateEvent(
                        user,
                        order,
                        "purchaseOrderDeliveryConfirmationChanged",
                        message.toString());
            }
            createProductPlanningRefreshTask(order, null);
        }
    }

    @Override
    protected void doAfterItemUpdate(Record record, ItemChangedInfo result) {
        if (result != null) {
            ItemChangedInfo obj = getOrdersDao().createItemChangedInfo(result);
            if (orderChangedSender != null && obj != null) {
                orderChangedSender.sendItemChangedInfo((Order) record, obj);
            }
        }
    }

    private OrderItem getSelectedItem(Record record, Long itemId) {
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            OrderItem current = (OrderItem) record.getItems().get(i);
            if (current.getId().equals(itemId)) {
                return current;
            }
        }
        return null;
    }

    private PurchaseOrders getOrdersDao() {
        return (PurchaseOrders) getDao();
    }

    public void updateDeliveryCondition(PurchaseOrder order, Long deliveryConditionId) throws ClientException {
        if (order == null) {
            throw new ClientException(ErrorCode.APPLICATION_CONFIG);
        }
        if (deliveryConditionId == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }

        DeliveryCondition deliveryCondition = deliveryConditions.load(deliveryConditionId);
        order.setDeliveryCondition(deliveryCondition);
        getOrdersDao().save(order);
    }
}
