/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 30, 2008 4:46:11 PM 
 * 
 */
package com.osserp.core.contacts;

import java.util.Date;

import com.osserp.common.EntityRelation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PrivateContact extends EntityRelation {

    /**
     * Provides the posix user id of the owner
     * @return owner
     */
    String getOwner();

    /**
     * Provides the associated contact
     * @return contact
     */
    Contact getContact();

    /**
     * Provides the company
     * @return company
     */
    Contact getCompany();

    /**
     * Provides the contact type
     * @return contactType
     */
    String getContactType();

    /**
     * Provides a contact info
     * @return contactInfo
     */
    String getContactInfo();

    /**
     * Provides the country name
     * @return countryName
     */
    String getCountryName();

    /**
     * Provides the date when private contact was created
     * @return created
     */
    Date getCreated();

    /**
     * Provides the date when private contact was changed
     * @return changed
     */
    Date getChanged();

    /**
     * Provides the date of last synchronisation if supported
     * @return lastSync
     */
    Date getLastSync();

    /**
     * Sets the date of last synchronisation if supported
     * @param lastSync
     */
    void setLastSync(Date lastSync);

    /**
     * Indicates if contact is currently unused
     * @return unused
     */
    boolean isUnused();

    /**
     * Enables/disables unused flag
     * @param unused
     */
    void setUnused(boolean unused);

    /**
     * Provides the primary key of an external groupware solution if available. 
     * @return groupwareKey
     */
    String getGroupwareKey();

    /**
     * Sets the primary key of an external groupware solution if available. 
     * @param groupwareKey
     */
    void setGroupwareKey(String groupwareKey);

    /**
     * Provides the primary key of an external website if available. 
     * @return websiteKey
     */
    String getWebsiteKey();

    /**
     * Sets the primary key of an external website if available. 
     * @param websiteKey
     */
    void setWebsiteKey(String websiteKey);

    /**
     * Updates private contact values
     * @param contactType
     * @param contactInfo
     */
    void update(String contactType, String contactInfo);
}
