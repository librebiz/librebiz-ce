/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.core.model.projects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jdom2.Element;

import com.osserp.common.Option;
import com.osserp.common.User;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.TargetDate;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.model.BusinessAddressImpl;
import com.osserp.core.model.SalesRequestImpl;
import com.osserp.core.projects.ProjectRequest;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.SalesRequest;
import com.osserp.core.system.BranchOffice;

/**
 * Value object for ProjectRequestImpl
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class ProjectRequestImpl extends SalesRequestImpl implements ProjectRequest {

    protected List<TargetDate> installationDates = new ArrayList<TargetDate>();

    protected ProjectRequestImpl() {
        super();
    }

    public ProjectRequestImpl(
            User user,
            BusinessType type,
            Customer customer) {
        super(user, type, customer, new BusinessAddressImpl(customer));
    }

    public ProjectRequestImpl(Long user, Request request, BusinessType type) {
        super(user, request, type);
    }

    //  constructor for plan list action; values from database: 	
    //	"plan_id,type_id,sales_id,sales_talk_date,presentation_date," +
    //	"installation_date,status_id,created,created_by";

    public ProjectRequestImpl(
            Long requestId,
            BusinessType type,
            Long salesId,
            Long salesCoId,
            Date salesTalkDate,
            Date presentationDate,
            Long status,
            Date created,
            Long createdBy,
            String name,
            Long origin,
            BranchOffice branch,
            Date deliveryDate,
            List<TargetDate> installationDates) {

        super(
                requestId,
                type,
                salesId,
                salesCoId,
                salesTalkDate,
                presentationDate,
                status,
                created,
                createdBy,
                name,
                origin,
                branch,
                deliveryDate);
        if (installationDates != null) {
            this.installationDates = installationDates;
        }
    }

    public ProjectRequestImpl(
            Long businessId, 
            String externalReference,
            String externalUrl,
            Date createdDate, 
            BusinessType type, 
            BranchOffice office, 
            Customer customer,
            Long salesPersonId, 
            Long projectManagerId, 
            String name, 
            String street, 
            String streetAddon, 
            String zipcode, 
            String city, 
            Long country,
            Campaign campaign, 
            Option origin) {
        super(businessId, externalReference, externalUrl, createdDate, 
                type, office, customer, salesPersonId, projectManagerId, 
                name, street, streetAddon, zipcode, city, country, 
                campaign, origin);
    }

    public ProjectRequestImpl(Request otherValue) {
        super(otherValue);
    }

    public ProjectRequestImpl(ProjectRequest otherValue) {
        super(otherValue);
        this.installationDates = otherValue.getInstallationDates();
    }

    public ProjectRequestImpl(SalesRequest otherValue) {
        super(otherValue);
    }

    public Date getInstallationDate() {
        if (installationDates == null || installationDates.isEmpty()) {
            return null;
        }
        return installationDates.get(0).getDate();
    }

    public List<TargetDate> getInstallationDates() {
        return installationDates;
    }

    protected void setInstallationDates(List<TargetDate> installationDates) {
        this.installationDates = installationDates;
    }

    public void addInstallationDate(Long user, Date date) {
        ProjectInstallationDateImpl newDate = new ProjectInstallationDateImpl(getRequestId(), user, date);
        if (installationDates.isEmpty()) {
            this.installationDates.add(newDate);
        } else {
            this.installationDates.add(0, newDate);
        }
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("installationDates", installationDates));
        return root;
    }
}
