<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="pool" value="${sessionScope.eventConfigView.bean.pool}"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="jobReceiverPool"/>
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
				<table class="table">
					<thead>
						<tr>
							<th><o:out value="${pool.id}"/></th>
							<th><o:out value="${pool.name}"/></th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="member" items="${pool.members}">
						<tr>
							<td><o:out value="${member.recipientId}"/></td>
							<td><oc:employee value="${member.recipientId}"/></td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			<div class="spacer"></div>
		</div>
	</div>
</div>