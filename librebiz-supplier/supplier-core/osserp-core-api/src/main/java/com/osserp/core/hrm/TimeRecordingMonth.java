/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 30, 2008 9:46:25 AM 
 * 
 */
package com.osserp.core.hrm;

import java.util.Date;
import java.util.List;

import com.osserp.common.Month;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordingMonth extends Month {

    /**
     * Provides consumed leave in display format
     * @return leaveDisplay
     */
    String getLeaveDisplay();

    /**
     * Provides completed work in display format
     * @return totalDisplay
     */
    String getTotalDisplay();

    /**
     * Provides the monthly illness in display format
     * @return illnessDisplay
     */
    String getIllnessDisplay();

    /**
     * Provides the monthly illness in minutes
     * @return illness
     */
    int getMinutesIllness();

    /**
     * Provides carryover in display format
     * @return carryoverDisplay
     */
    String getCarryoverDisplay();

    /**
     * Provides the count of working days in month display format
     * @return workingDaysCountDisplay
     */
    String getWorkingDaysCountDisplay();

    /**
     * Provides the summary of consumed leave in minutes
     * @return minutesLeave
     */
    int getMinutesLeave();

    /**
     * Provides the summary of lost minutes
     * @return minutesLeave
     */
    int getMinutesLost();

    /**
     * Provides carryover minutes of this month
     * @return minutesCarryover
     */
    int getMinutesCarryover();

    /**
     * Provides the count of working days in month
     * @return workingDaysCount
     */
    int getWorkingDaysCount();

    /**
     * Indicates that this month is the current month
     * @return true if this month is the current month
     */
    boolean isCurrentMonth();

    /**
     * Provides days of month
     * @return days
     */
    List<TimeRecordingDay> getDays();

    /**
     * Provides days of month in descent order
     * @return reverseDays
     */
    List<TimeRecordingDay> getReverseDays();

    /**
     * Get minutes carryover from the previous month
     * @return prevMinutesCarryover
     */
    int getPrevMinutesCarryover();

    /**
     * Tries to fetch a date.
     * @param mdate
     * @return timeRecordingDay or null if not supported
     */
    TimeRecordingDay fetchDate(Date date);

    /**
     * Provides markers of month
     * @return markers
     */
    List<TimeRecordMarker> getMarkers();

    /**
     * Provides minutes leave markers of month
     * @return minutesLeaveMarker
     */
    int getMinutesLeaveMarker();

    /**
     * Provides the first day of this month as date
     * @return firstDayDate
     */
    Date getFirstDayDate();

    /**
     * Provides marker value of this month
     * @return monthMarkerValue
     */
    double getMonthMarkerValue();

    /**
     * Provides leave marker value of this month
     * @return leaveMarkerValue
     */
    double getLeaveMarkerValue();

    /**
     * Indicates that month has a leave marker
     * @return leaveMarker
     */
    boolean isLeaveMarker();

    /**
     * Indicates that month has a marker
     * @return mothMarker
     */
    boolean isMonthMarker();

    /**
     * Provides remaining leave in minutes
     * @return remainingMinutesLeave
     */
    int getRemainingMinutesLeave();

    /**
     * Sets remaining leave
     * @return remainingMinutesLeave
     */
    void setRemainingMinutesLeave(int remainingMinutesLeave);

    /**
     * Provides remaining leave
     * @return remainingMinutesLeave
     */
    double getRemainingLeave();

    /**
     * Provides expired leave in minutes
     * @return expiredMinutesLeave
     */
    int getExpiredMinutesLeave();

    /**
     * Sets expired leave
     * @return expiredMinutesLeave
     */
    void setExpiredMinutesLeave(int expiredMinutesLeave);

    /**
     * Provides expired leave
     * @return expiredMinutesLeave
     */
    double getExpiredLeave();
}
