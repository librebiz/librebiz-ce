/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 29, 2010 4:12:32 PM 
 * 
 */
package com.osserp.groovy.web

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.TimeoutException

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class MenuContextController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(MenuContextController.class.getName())
    
    /**
     * You must override this to provide the relating menu forward url.
     */
    protected abstract String getMenuForward();
    
    @Override
    @RequestMapping
    def exit(HttpServletRequest request) {
        boolean exitAvailable = (fetchView(request)?.exitTarget != null)
        def target = super.exit(request)
        if (exitAvailable) {
            return target
        }
        redirect(request, menuForward)
    }

    @Override
    @RequestMapping
    def home(HttpServletRequest request) {
        logger.debug("home: invoked [user=${getUserId(request)}]")
        try {
            View view = getView(request)
            if (view.destroyViewOnHome) {
                removeView(request, view)
            }
            return redirect(request, menuForward)

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('home')) {
                logger.debug('home: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        }
    }

}
