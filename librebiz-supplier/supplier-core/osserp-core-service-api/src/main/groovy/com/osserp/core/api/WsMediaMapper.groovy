/**
 *
 * Copyright (C) 2012 The original author or authors.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.core.api

import com.osserp.api.WsMedia
import com.osserp.common.OptionsCache
import com.osserp.common.dms.DmsDocument
import com.osserp.core.Options

/**
 * 
 * WsMediaMapper maps core documents to webservice media objects
 * 
 * 
 * @author rk <rk@osserp.com>
 * 
 */
class WsMediaMapper {
    
    private OptionsCache options
    boolean urlWithContext = true
    
    protected WsMediaMapper(OptionsCache optionsCache) {
        options = optionsCache
    }

    WsMedia map(DmsDocument document) {
        WsMedia obj = new WsMedia(
            primaryKey: document.id,
            name: document.fileName,
            mediaType: getCategoryName(document.categoryId),
            referenceId: document.reference,
            url: createUri(document),
            referenceContext: 'products'
        )
        return obj
    }
    
    protected String createUri(DmsDocument document) {
        String uri = document.realFileName 
        if (urlWithContext && document.type?.contextName) {
            uri = "${document.type.contextName}/${uri}" 
        }
        uri
    }
    
    protected String getCategoryName(Long categoryId) {
        !categoryId ? null : options.getMappedValue(Options.DOCUMENT_CATEGORIES, categoryId)
    }
}
