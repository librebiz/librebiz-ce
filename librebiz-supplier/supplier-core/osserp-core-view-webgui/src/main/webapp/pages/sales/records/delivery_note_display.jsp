<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesDeliveryNoteView or empty sessionScope.salesDeliveryNoteView.bean}">
<o:logger write="delivery_note_display.jsp no view found" level="warn"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesDeliveryNoteView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="baseUrl" scope="request" value="/salesDeliveryNote.do"/>
<c:set var="bookRecordUrl" scope="request" value="/salesDeliveryNote.do"/>
<c:set var="deleteRecordUrl" scope="request" value="/salesDeliveryNote.do"/>
<c:set var="infoUrl" scope="request" value="/salesDeliveryNoteInfos.do"/>
<c:set var="productUrl" scope="request" value="/salesDeliveryNoteProducts.do"/>
<c:set var="productExitTarget" scope="request" value="salesDeliveryNoteReload"/>
<c:set var="productAddEnabled" scope="request" value="false"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
    <style type="text/css">
        <c:import url="/css/records.css"/>
    </style>
</tiles:put>

<tiles:put name="headline">
    <c:import url="/pages/sales/records/voucher_header.jsp"/>
</tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/salesDeliveryNote.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<c:if test="${!recordView.deleteMode}">
<c:if test="${!record.unchangeable and !empty record.items}">
<v:link url="/records/recordPrint/release?view=salesDeliveryNoteView" confirm="true" message="confirmRecordRelease" title="recordRelease"><o:img name="enabledIcon" /></v:link>
</c:if>
<v:link url="/records/recordPrint/print?view=salesDeliveryNoteView" title="documentPrint"><o:img name="printIcon" /></v:link>
</c:if>
<c:choose>
<c:when test="${!record.unchangeable}">
<c:if test="${recordView.noteDeletable}">
<a href="<c:url value="/salesDeliveryNote.do?method=confirmDelete"/>" title="<fmt:message key="title.record.delete"/>"><o:img name="deleteIcon"/></a>
</c:if>
<a href="<c:url value="/salesDeliveryNote.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon"/></a>
</c:when>
<c:when test="${recordView.deleteMode}">
<a href="<c:url value="/salesDeliveryNote.do?method=confirmDelete"/>" title="<fmt:message key="title.record.delete"/>"><o:img name="deleteIcon"/></a>
</c:when>
</c:choose>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                
                    <c:import url="/pages/shared/lock_handler.jsp"/>
                    <c:import url="/pages/records/record_delete_dialog.jsp"/>
                    <c:import url="/pages/records/serial_number_ignorecheck_dialog.jsp"/>
                
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
                
                    <div class="column66l">
                        <c:import url="/pages/records/record_embedded_text.jsp"/>
                        <c:import url="/pages/records/record_note_display.jsp"/>
                    </div>
                    <div class="column33r">
                       <c:import url="/pages/records/record_status.jsp"/>
                    </div>
                    
                </div>
            </div>
        </div>
        <br />
        <c:import url="/pages/records/delivery_items.jsp"/>
    </div>
</div>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
