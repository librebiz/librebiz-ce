/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Oct-2006 09:29:41 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.beans.NoteImpl;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BusinessNote;
import com.osserp.core.NoteType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessNoteImpl extends NoteImpl implements BusinessNote {

    private NoteType type;

    private boolean confirmationExpected;
    private Long confirmationExpectedBy;
    private Date confirmationDate;

    private String messageId = null;
    private Long messageStatus = 0L;
    private String messageType = null;
    private String recipients = null;
    private String originator = null;
    private String subject = null;
    private String text = null;
    private String html = null;

    private String headline = null;
    private boolean email = false;
    private boolean sticky = false;
    private boolean received = false;

    /**
     * Default constructor used by serialization.
     */
    protected BusinessNoteImpl() {
        super();
    }

    public BusinessNoteImpl(
            NoteType type,
            Long reference,
            Long createdBy,
            String note,
            String headline) {
        super(reference, note, createdBy);
        this.type = type;
        this.headline = headline;
    }

    public BusinessNoteImpl(
            NoteType type,
            Long reference,
            Long createdBy,
            String message,
            String subject,
            String messageId,
            boolean received) {
        super(reference, message, createdBy);
        this.type = type;
        this.headline = subject;
        this.messageId = messageId;
        this.received = received;
    }

    public NoteType getType() {
        return type;
    }

    protected void setType(NoteType type) {
        this.type = type;
    }

    public boolean isConfirmationExpected() {
        return confirmationExpected;
    }

    public void setConfirmationExpected(boolean confirmationExpected) {
        this.confirmationExpected = confirmationExpected;
    }

    public Long getConfirmationExpectedBy() {
        return confirmationExpectedBy;
    }

    public void setConfirmationExpectedBy(Long confirmationExpectedBy) {
        this.confirmationExpectedBy = confirmationExpectedBy;
    }

    public Date getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(Date confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public boolean isEmail() {
        return email;
    }

    public void setEmail(boolean email) {
        this.email = email;
    }

    public Long getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(Long messageStatus) {
        this.messageStatus = messageStatus;
    }

    public boolean isSticky() {
        return sticky;
    }

    public void setSticky(boolean sticky) {
        this.sticky = sticky;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("recipients", recipients));
        root.addContent(JDOMUtil.createElement("originator", originator));
        root.addContent(JDOMUtil.createElement("subject", subject));
        root.addContent(JDOMUtil.createElement("text", text));
        root.addContent(JDOMUtil.createElement("headline", headline));
        root.addContent(JDOMUtil.createElement("email", email));
        root.addContent(JDOMUtil.createElement("sticky", sticky));
        root.addContent(JDOMUtil.createElement("messageId", messageId));
        root.addContent(JDOMUtil.createElement("messageStatus", messageStatus));
        root.addContent(JDOMUtil.createElement("messageType", messageType));
        root.addContent(JDOMUtil.createElement("received", received));
        return root;
    }
}
