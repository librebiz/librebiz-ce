/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 02-Feb-2007 02:26:51 
 * 
 */
package com.osserp.core.dao.records;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Payments extends FinanceRecords {

	/**
	 * Provides all payments related to contact
	 * @param contactId
	 * @return payments or empty list
	 */
    List<Payment> getByContact(Long contactId);

    /**
     * Finds all payments related to an order.
     * @param order id used as reference
     * @return payments or empty list if none exist
     */
    List<Payment> getByOrder(Order order);

    /**
     * Provides all payments by bankAccount
     * @param bankAccountId
     * @return payments descendent ordered by paid date
     */
    List<Payment> getByBankAccount(Long bankAccountId);

    /**
     * Finds all payments related to a reference
     * @param id reference
     * @return payments or empty list if none exist
     */
    List<Payment> getByReference(Long id);

    /**
     * Finds all payments related to a payment expecting record
     * @param record
     * @return payments or empty list if none exist
     */
    List<Payment> getByRecord(PaymentAwareRecord record);

    /**
     * Provides a payment by id
     * @param id
     * @return payment or null if not exist
     */
    Payment getPayment(Long id);

    /**
     * Deletes a single payment
     * @param payment
     */
    void delete(Payment payment);

    /**
     * Creates a new payment
     * @param paymentType
     * @param record
     * @param amount
     * @param paid
     * @param bankAccountid
     * @param createdBy
     * @return payment new created
     * @throws ClientException if validation of input failed
     */
    Payment create(
            Long paymentType,
            PaymentAwareRecord record,
            BigDecimal amount,
            Date paid,
            Long bankAccountid,
            Employee createdBy)
            throws ClientException;

    /**
     * Creates a custom payment
     * @param record
     * @param amount
     * @param paid
     * @param customHeader
     * @param note
     * @param bankAccountId
     * @param user
     * @return payment
     * @throws ClientException
     */
    Payment createCustom(
            PaymentAwareRecord record,
            BigDecimal amount,
            Date paid,
            String customHeader,
            String note,
            Long bankAccountId,
            Employee user)
            throws ClientException;

    /**
     * Adds some time to paid-on date to trigger sorting order
     * @param payment
     */
    void increaseTime(Payment payment);
    
    /**
     * Adds some time to paid-on date to trigger sorting order
     * @param payment
     * @param cashDiscount
     */
    void subtract(Payment payment, BigDecimal cashDiscount);

}
