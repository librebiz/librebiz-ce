/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 6, 2008 9:22:32 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.AbstractRowMapper;

import com.osserp.core.Options;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.SerialNumberDisplayVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SerialNumberDisplayRowMapper extends AbstractRowMapper {
    
    public static final String SELECT_FROM =
            "SELECT id, reference_id, created, created_by, is_sales, " +
                    "product_id, serial_number, name, record_type FROM ";
    
    private OptionsCache optionsCache = null;

    protected SerialNumberDisplayRowMapper(OptionsCache optionsCache) {
        super();
        this.optionsCache = optionsCache;
    }

    public Object mapRow(ResultSet rs, int arg1) throws SQLException {
        RecordType recordType = (RecordType) optionsCache.getMapped(
                Options.RECORD_TYPES, Long.valueOf(rs.getLong(9)));
        SerialNumberDisplayVO r = new SerialNumberDisplayVO(
                Long.valueOf(rs.getLong(1)),
                Long.valueOf(rs.getLong(2)),
                rs.getDate(3),
                Long.valueOf(rs.getLong(4)),
                rs.getBoolean(5),
                Long.valueOf(rs.getLong(6)),
                rs.getString(8),
                rs.getString(7),
                recordType);
        return r;
    }

    public static RowMapperResultSetExtractor getReader(OptionsCache optionsCache) {
        return new RowMapperResultSetExtractor(
                new SerialNumberDisplayRowMapper(optionsCache));
    }

}
