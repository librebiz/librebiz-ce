<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.requestListingView}" />
<c:if test="${!empty requestScope.requestSelectExitTarget}">
    <c:set var="exitTarget" value="${requestScope.requestSelectExitTarget}" />
</c:if>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="center"><v:sortLink key="typeKey"><fmt:message key="type" /></v:sortLink></th>
                    <th><fmt:message key="number" /></th>
                    <th><v:sortLink key="name"> <fmt:message key="interestOrCustomer" /></v:sortLink></th>
                    <th><v:sortLink key="created"> <fmt:message key="from" /></v:sortLink></th>
                    <th class="right" title="volumeOfOrders"><v:sortLink key="capacity"><fmt:message key="value" /></v:sortLink></th>
                    <th><fmt:message key="branchOfficeShortcut" /></th>
                    <th title="<fmt:message key="sales"/>"><fmt:message key="salesShortcut"/></th>
                    <th title="orderProbabilityPercentage"><v:sortLink key="orderProbability"> <fmt:message key="orderProbabilityColumnLabel" /></v:sortLink></th>
                    <th><v:sortLink key="orderProbabilityDate"> <fmt:message key="expected" /></v:sortLink></th>
                    <th class="right"><v:sortLink key="status"><span>%</span> </v:sortLink></th>
                    <o:permission role="notes_requests_deny" info="permissionNotes">
                    <th class="center" colspan="3"><fmt:message key="info" /></th>
                    </o:permission>
                    <o:forbidden role="notes_requests_deny" info="permissionNotes">
                    <th class="center" colspan="2"><fmt:message key="info" /></th>
                    </o:forbidden>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty view.list}">
                        <tr>
                            <o:permission role="notes_requests_deny" info="permissionNotes">
                            <td colspan="13"><fmt:message key="noRequestFound" /></td>
                            </o:permission>
                            <o:forbidden role="notes_requests_deny" info="permissionNotes">
                            <td colspan="12"><fmt:message key="noRequestFound" /></td>
                            </o:forbidden>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="obj" items="${view.list}" varStatus="s">
                            <c:set var="nextSelectionUrl">
                                <c:url value="/loadRequest.do">
                                    <c:if test="${!empty exitTarget}">
                                        <c:param name="exit" value="${exitTarget}" />
                                    </c:if>
                                    <c:param name="id" value="${obj.id}" />
                                    <c:param name="exitId" value="request${obj.id}" />
                                </c:url>
                            </c:set>
                            <tr id="request${obj.id}"<c:if test="${obj.stopped}"> class="red"</c:if>>
                                <td class="center"><o:out value="${obj.type.key}" /></td>
                                <td>
                                    <a href="<o:out value="${nextSelectionUrl}"/>"><o:out value="${obj.id}" /></a>
                                </td>
                                <td>
                                    <a href="<o:out value="${nextSelectionUrl}"/>" title="<o:out value="${obj.id} - ${obj.shortName}"/> / <o:out value="${obj.city}"/>"> 
                                        <o:out value="${obj.name}" />
                                    </a>
                                </td>
                                <td><o:date value="${obj.created}" casenull="-" addcentury="false" /></td>
                                <td class="right"><o:number value="${obj.capacity}" format="${obj.capacityFormat}" /></td>
                                <td class="center"><o:out limit="4" value="${obj.branch.shortkey}" /></td>
                                <td title="<fmt:message key="sales"/>">
                                    <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${obj.salesId}">
                                        <oc:employee initials="true" value="${obj.salesId}" />
                                    </v:ajaxLink>
                                </td>
                                <td><o:out value="${obj.orderProbability}" /></td>
                                <td><o:date value="${obj.orderProbabilityDate}" casenull="-" addcentury="false" /></td>
                                <td class="right">
                                    <v:ajaxLink url="/sales/flowControlDisplay/forward?id=${obj.id}" styleClass="boldtext" linkId="flowControlDisplayView">
                                        <o:out value="${obj.status}" />
                                    </v:ajaxLink>
                                </td>
                                <o:permission role="notes_requests_deny" info="permissionNotes">
                                    <td class="icon center">
                                        <c:choose>
                                            <c:when test="${!empty obj.lastNoteDate}">
                                                <v:ajaxLink url="/sales/businessCaseDisplay/forward?notes=true&id=${obj.id}" linkId="businessCaseDisplayView" title="notes">
                                                    <span title="<o:date value="${obj.lastNoteDate}" casenull="-" addcentury="false"/>"><o:img name="texteditIcon"/></span>
                                                </v:ajaxLink>
                                            </c:when>
                                            <c:otherwise>
                                                <o:img name="nosmileIcon" title="noNotesAvailable"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </o:permission>
                                <td class="icon center">
                                    <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${obj.id}" linkId="businessCaseDisplayView" title="shortinfo">
                                        <o:img name="openFolderIcon"/>
                                    </v:ajaxLink>
                                </td>
                                <td class="icon center">
                                    <c:choose>
                                        <c:when test="${obj.appointmentAvailable}">
                                            <a href="<o:out value="${nextSelectionUrl}"/>" title="<fmt:message key="appointmentDisplay"/>"> 
                                                <o:img name="clockIcon" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <v:ajaxLink url="/events/appointmentCreator/forward?id=${obj.id}&type=4&view=${view.name}" linkId="appointmentCreator" title="appointmentCreate">
                                                <o:img name="bellIcon" />
                                            </v:ajaxLink>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
