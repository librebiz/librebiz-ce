/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 12, 2012 
 * 
 */
package com.osserp.core.model.system;

import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.beans.OptionImpl;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.contacts.Legalform;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LegalformImpl extends OptionImpl implements Legalform {

    private boolean statuaryOfficeRequired = false;
    private boolean supervisoryBoardRequired = false;
    private boolean generalPartnerRequired = false;

    protected LegalformImpl() {
        super();
    }

    protected LegalformImpl(Legalform o) {
        super(o);
        this.statuaryOfficeRequired = o.isStatuaryOfficeRequired();
        this.supervisoryBoardRequired = o.isSupervisoryBoardRequired();
    }

    public boolean isStatuaryOfficeRequired() {
        return statuaryOfficeRequired;
    }

    public void setStatuaryOfficeRequired(boolean statuaryOfficeRequired) {
        this.statuaryOfficeRequired = statuaryOfficeRequired;
    }

    public boolean isSupervisoryBoardRequired() {
        return supervisoryBoardRequired;
    }

    public void setSupervisoryBoardRequired(boolean supervisoryBoardRequired) {
        this.supervisoryBoardRequired = supervisoryBoardRequired;
    }

    public boolean isGeneralPartnerRequired() {
        return generalPartnerRequired;
    }

    public void setGeneralPartnerRequired(boolean generalPartnerRequired) {
        this.generalPartnerRequired = generalPartnerRequired;
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "statuaryOfficeRequired", isStatuaryOfficeRequired());
        putIfExists(map, "supervisoryBoardRequired", isSupervisoryBoardRequired());
        putIfExists(map, "generalPartnerRequired", isGeneralPartnerRequired());
        return map;
    }

    @Override
    public Element getXML() {
        return getXML("legalform");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("statuaryOfficeRequired", statuaryOfficeRequired));
        root.addContent(JDOMUtil.createElement("supervisoryBoardRequired", supervisoryBoardRequired));
        root.addContent(JDOMUtil.createElement("generalPartnerRequired", generalPartnerRequired));
        return root;
    }

    @Override
    public Object clone() {
        return new LegalformImpl(this);
    }
}
