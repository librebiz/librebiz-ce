/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Apr 5, 2010 9:53:58 AM 
 * 
 */
package com.osserp.core.contacts;

import java.util.List;

import com.osserp.core.users.DomainUser;

/**
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 */
public interface PrivateContactManager {

    /**
     * Provides all private contacts of a user
     * @param user
     * @return contacts or empty list
     */
    List<PrivateContact> findPrivate(DomainUser user);

    /**
     * Provides all private contacts which inherits the given contactId
     * @param contactId
     * @return contacts or empty list
     */
    List<PrivateContact> findPrivate(Long contactId);

    /**
     * Creates a private contact entry
     * @param user
     * @param contact
     * @param company
     * @return contact created
     */
    PrivateContact createPrivate(DomainUser user, Contact contact, Contact company);

    /**
     * Creates a private contact entry
     * @param user
     * @param contact
     * @return private contact
     */
    PrivateContact getPrivate(DomainUser user, Contact contact);

    /**
     * Indicates that contact is private contact of employee
     * @param user
     * @param contact
     * @return true if contact is associated
     */
    boolean isPrivate(DomainUser user, Contact contact);

    /**
     * Indicates that an existing contact is currently marked as unused
     * @param user
     * @param contact
     * @return true if no private contact exists or existing has unused flag enabled
     */
    boolean isPrivateUnused(DomainUser user, Contact contact);

    /**
     * Indicates that private contacts are available
     * @param user
     * @return private contact available
     */
    boolean isPrivateAvailable(DomainUser user);

    /**
     * Persists private contact
     * @param contact
     */
    void disable(PrivateContact contact);

    /**
     * Updates private contact properties
     * @param contact
     * @param type
     * @param info
     */
    PrivateContact update(PrivateContact contact, String type, String info);

    /**
     * Syncronizes private contact
     * @param contact
     */
    void sync(PrivateContact contact);

}
