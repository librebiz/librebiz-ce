<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="flowControlStatusView"/>

<style type="text/css">
#flowControlStatusDisplay_popup { min-width:40%; }
</style>

<div class="row row-modal">
    <div class="col-md-6 panel-area panel-area-modal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="status"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive table-responsive-default">
                <table class="table">
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.list}">
                                <tr>
                                    <td class="left"><fmt:message key="noActionsAvailableOnCurrentRecordState"/></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="action" items="${view.list}" varStatus="s">
                                    <c:if test="${!empty action.status and !action.cancelling and !action.stopping}">
                                        <c:choose>
                                            <c:when test="${!empty action.changed}">
                                                <tr<c:if test="${!view.businessCase.closed}"> class="altrow"</c:if>>
                                                    <td><o:out value="${action.name}"/></td>
                                                    <td class="right"><o:out value="${action.status}"/>%</td>
                                                    <td class="center"><o:date value="${action.changed}"/></td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <c:if test="${!view.businessCase.closed}">
                                                    <tr>
                                                        <td>
                                                            <c:choose>
                                                                <c:when test="${!empty view.selectAction}">
                                                                    <v:link url="${view.selectAction}/selectAction?id=${action.id}&exit=${view.exitTarget}" title="selectAction">
                                                                        <o:out value="${action.name}"/>
                                                                    </v:link>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <o:out value="${action.name}"/>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                        <td class="right"><o:out value="${action.status}"/>%</td>
                                                        <td class="center"> </td>
                                                    </tr>
                                                </c:if>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
                
        </div>
    </div>
</div>
