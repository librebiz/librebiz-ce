/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 9, 2014 
 * 
 */
package com.osserp.core.purchasing;

import java.util.Date;

import com.osserp.core.finance.RecordSummary;
import com.osserp.core.finance.RecordType;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseRecordSummary extends RecordSummary {
    
    /**
     * Default constructor adding serialializable capability to entities
     */
    protected PurchaseRecordSummary() {
        super();
    }

    /**
     * Creates a new purchaseRecordSummary value object
     * @param id
     * @param type
     * @param reference
     * @param supplier
     * @param name
     * @param status
     * @param created
     * @param createdBy
     * @param taxFree
     * @param amount
     * @param tax
     * @param reducedTax
     * @param gross
     * @param sales
     * @param canceled
     * @param internal
     * @param company
     * @param branch
     * @param paidAmount
     */
    public PurchaseRecordSummary(
            Long id,
            RecordType type, 
            Long reference, 
            RecordType referenceType, 
            Long supplier,
            String name,
            Long status, 
            Date created,
            Long createdBy,
            boolean taxFree, 
            Double amount, 
            Double tax, 
            Double reducedTax,
            Double gross, 
            Long sales, 
            boolean canceled, 
            boolean internal,
            SystemCompany company,
            BranchOffice branch,
            Double paidAmount,
            Double taxRate,
            String note) {
        super(id, type, reference, referenceType, supplier, name, status, created, createdBy, taxFree, 
                amount, tax, reducedTax, gross, sales, canceled, internal, company, branch, paidAmount, taxRate);
        setNote(note);
    }

    @Override
    public boolean isPurchaseRecord() {
        return true;
    }

    public Long getSupplier() {
        return getContactReference();
    }

    public boolean isCancellation() {
        // TODO check recordType
        return false;
    }

    public boolean isCreditNote() {
        return RecordType.PURCHASE_RETURN.equals(getTypeId());
    }

    public boolean isDownpayment() {
        return false;
    }

    public boolean isInvoice() {
        return RecordType.PURCHASE_INVOICE.equals(getTypeId());
    }
}
