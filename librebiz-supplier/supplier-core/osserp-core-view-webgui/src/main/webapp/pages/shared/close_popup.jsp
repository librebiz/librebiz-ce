<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="element" value="${param.targetPopup}"/>
<c:if test="${empty element}"><c:set var="element" value="${requestScope.targetPopup}"/></c:if>
ojsCommon.setVisibility("<o:out value="${element}" />", "hidden");
ojsCommon.setDisplay("<o:out value="${element}" />", "none");
