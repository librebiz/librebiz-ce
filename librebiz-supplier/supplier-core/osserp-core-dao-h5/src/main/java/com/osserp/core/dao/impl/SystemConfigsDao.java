/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2006 1:57:25 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionConfig;
import com.osserp.common.Property;
import com.osserp.common.dao.Tables;
import com.osserp.common.gui.Menu;
import com.osserp.common.gui.model.MenuImpl;
import com.osserp.common.service.AuthenticationProvider;
import com.osserp.common.util.SecurityUtil;

import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.system.BranchSelectionImpl;
import com.osserp.core.model.system.SystemCacheStatus;
import com.osserp.core.model.system.SystemKeyImpl;
import com.osserp.core.model.system.SystemKeyTypeImpl;
import com.osserp.core.model.system.SystemPropertyImpl;
import com.osserp.core.system.BranchSelection;
import com.osserp.core.system.CacheStatus;
import com.osserp.core.system.SystemKey;
import com.osserp.core.system.SystemKeyType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SystemConfigsDao extends AbstractRepository implements SystemConfigs {
    private static Logger log = LoggerFactory.getLogger(SystemConfigsDao.class.getName());

    private String systemCacheStatusTable = null;
    private AuthenticationProvider authenticationProvider = null;

    public SystemConfigsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            AuthenticationProvider authenticationProvider) {
        super(jdbcTemplate, tables, sessionFactory);
        this.systemCacheStatusTable = getTable(TableKeys.SYSTEM_CACHE_STATUS);
        this.authenticationProvider = authenticationProvider;
    }

    public CacheStatus getCacheStatus(String cacheName) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT max(id) FROM ")
                .append(systemCacheStatusTable)
                .append(" WHERE name = '")
                .append(cacheName)
                .append("' AND changed = true");
        List<Long> ids = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        Long id = (ids.isEmpty() ? null : ids.get(0));
        if (id != null) {
            return (CacheStatus) getCurrentSession().load("SystemCacheStatus", id);
        }
        return null;
    }

    public void closeCacheRefresh(CacheStatus status) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("UPDATE ")
                .append(systemCacheStatusTable)
                .append(" SET changed = false WHERE id <= ")
                .append(status.getId());
        jdbcTemplate.update(sql.toString());
    }

    public void createCacheRefresh(String cacheName) {
        SystemCacheStatus status = new SystemCacheStatus(cacheName);
        getCurrentSession().save(SystemCacheStatus.class.getName(), status);
    }

    public BranchSelection getBranchSelection(Long id) {
        return (BranchSelection) getCurrentSession().get(BranchSelectionImpl.class.getName(), id);
    }

    public BranchSelection getBranchSelection(String name) {
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(BranchSelectionImpl.class.getName())
                .append(" obj where obj.name = :branchName order by obj.name");
        List<BranchSelection> list = getCurrentSession().createQuery(hql.toString())
                .setParameter("branchName", name)
                .list();
        return list.isEmpty() ? null : list.get(0);
    }

    public List<BranchSelection> getBranchSelections() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(BranchSelectionImpl.class.getName()).append(" obj order by obj.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public List<Menu> getMenus() {
        List<Menu> list = getCurrentSession().createQuery(
                "from " + MenuImpl.class.getName() + " o").list();
        return list;
    }

    public PermissionConfig getPermissionConfig(String name) {
        List<PermissionConfig> list = getCurrentSession().createQuery(
                "from PermissionConfigImpl o where o.name = :configName")
                .setParameter("configName", name).list();
        return list.isEmpty() ? null : list.get(0);
    }

    public boolean isSystemPropertyEnabled(String name) {
        return systemPropertyEnabled(name);
    }

    public String getSystemProperty(String name) {
        return getPropertyString(name);
    }

    public Long getSystemPropertyId(String name) {
        return getPropertyId(name);
    }

    public List<Property> getProperties() {
        StringBuilder query = new StringBuilder();
        query.append("from ").append(SystemPropertyImpl.class.getName()).append(" prop order by prop.name");
        return getCurrentSession().createQuery(query.toString()).list();
    }

    public final void switchBooleanProperty(String name) {
        try {
            Property result = fetchSystemProperty(name);
            if (result != null && result.isBooleanType()) {
                if (result.isEnabled()) {
                    result.disable();
                } else {
                    result.enable();
                }
                persistSystemProperty(result);
                if (log.isDebugEnabled()) {
                    log.debug("switchBooleanPropertyValue() done [name=" + name + ", value=" + result.getValue() + "]");
                }
            } else {
                log.warn("switchBooleanPropertyValue() ignoring attempt to update a non-existing property [name=" + name + "]");
            }
        } catch (Exception e) {
            log.warn("switchBooleanPropertyValue() systemProperties table not available or not accessible");
        }
    }

    public void updateProperty(String name, String value) {
        updatePropertyValue(name, value);
    }

    // system keys

    public List<SystemKeyType> getKeyTypes() {
        StringBuilder query = new StringBuilder();
        query.append("from ").append(SystemKeyTypeImpl.class.getName())
            .append(" kt order by kt.contextName, kt.name");
        return getCurrentSession().createQuery(query.toString()).list();
    }

    public List<SystemKey> getSecuredKeys() {
        StringBuilder query = new StringBuilder();
        query.append("from ").append(SystemKeyImpl.class.getName())
            .append(" kt order by kt.contextName, kt.name");
        return getCurrentSession().createQuery(query.toString()).list();
    }

    public SystemKey getSecuredKey(String context, String name) {
        if (isEmpty(context)) {
            throw new BackendException("context must not be null");
        }
        List<SystemKey> result = new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query
            .append("from ")
            .append(SystemKeyImpl.class.getName())
            .append(" kt where kt.contextName = :contextName");
        if (!isEmpty(name)) {
            query.append(" and kt.name = :name");
            result = getCurrentSession().createQuery(query.toString())
                    .setParameter("contextName", context)
                    .setParameter("name", name)
                    .list();
        } else {
            result = getCurrentSession().createQuery(query.toString())
                    .setParameter("contextName", context)
                    .list();
        }
        return (result.isEmpty() ? null : result.get(0)); 
    }

    public String getKeyValue(String context, String name) {
        SystemKey key = getSecuredKey(context, name);
        return ((key == null || key.getKeydata() == null) ? null : 
            SecurityUtil.decrypt(
                    (String) authenticationProvider.getAuthenticationToken().getToken(), 
                    key.getKeydata()));
    }

    public void saveKey(String contextName, String name, String plainkey) throws ClientException {
        if (contextName == null) {
            throw new BackendException("context must not be null");
        }
        SystemKey existing = getSecuredKey(contextName, name);
        if (existing != null) {
            existing.setKeydata(encodeKey(plainkey));
            getCurrentSession().merge(SystemKeyImpl.class.getName(), existing);
        } else {
            SystemKeyType type = getOrCreateKeyType(contextName, !isEmpty(name));
            SystemKeyImpl obj = new SystemKeyImpl(
                    type, 
                    name, 
                    encodeKey(plainkey));
            getCurrentSession().save(SystemKeyImpl.class.getName(), obj);
        }        
    }

    public void saveKey(SystemKeyType type, String name, String plainkey) throws ClientException {
        saveKey(type.getContextName(), name, plainkey);
    }

    private String encodeKey(String serialValue) {
        return SecurityUtil.encrypt((String) 
                authenticationProvider.getAuthenticationToken().getToken(), 
                serialValue);
    }

    private SystemKeyType getOrCreateKeyType(String contextName, boolean nameRequired) {
        StringBuilder byContext = new StringBuilder("from ");
        byContext.append(SystemKeyTypeImpl.class.getName())
            .append(" kt where kt.contextName = :contextName");
        
        List<SystemKeyType> existing = getCurrentSession().createQuery(byContext.toString())
                .setParameter("contextName", contextName)
                .list();
        
        if (!existing.isEmpty()) {
            return existing.get(0);
        }
        SystemKeyType kt = new SystemKeyTypeImpl(contextName, null, nameRequired);
        
        getCurrentSession().save(SystemKeyTypeImpl.class.getName(), kt);
        if (log.isDebugEnabled()) {
            log.debug("getOrCreateKeyType() done new [id=" + kt.getId()
                    + ", contextName=" + kt.getContextName()
                    + ", name=" + kt.getName() + "]");
        }
        return kt;
        
    }

    /* by now, no use case is defined for types with default a name  
    private SystemKeyType getOrCreateKeyType(String contextName, String name) {
        
        StringBuilder byContext = new StringBuilder("from ");
        byContext.append(SystemKeyTypeImpl.class.getName())
            .append(" kt where kt.contextName = :contextName");
        
        List<SystemKeyType> existing = getCurrentSession().createQuery(byContext.toString())
                .setParameter("contextName", contextName)
                .list();
        
        if (!existing.isEmpty()) {
            SystemKeyType ktCtxNoname = null;
            SystemKeyType ktCtxOnly = null;
            for (int i = 0, j = existing.size(); i < j; i++) {
                SystemKeyType kt = existing.get(i);
                if (!kt.isNameSupported()) {
                    if (ktCtxNoname == null || 
                            (ktCtxNoname.getId() < kt.getId())) {
                        // use latest for contextName only types
                        ktCtxNoname = kt;
                    }
                    
                } else if (!isEmpty(kt.getName()) && kt.getName().equals(name)
                        || (isEmpty(kt.getName()) && isEmpty(name))) {
                    return kt;
                } else if (isEmpty(kt.getName())) {
                    if (ktCtxOnly == null || 
                            (ktCtxOnly.getId() < kt.getId())) {
                        // use latest for contextName only types
                        ktCtxOnly = kt;
                    }
                }
            }
            if (ktCtxNoname != null) {
                // use a type without name support even if name is provided.
                // this prevents users from ignoring of rules
                return ktCtxNoname;
            }
            if (ktCtxOnly != null) {
                if (isEmpty(name)) {
                    return ktCtxOnly;
                }
            }
        }
        SystemKeyType kt = new SystemKeyTypeImpl(contextName, name);
        getCurrentSession().save(SystemKeyTypeImpl.class.getName(), kt);
        if (log.isDebugEnabled()) {
            log.debug("getOrCreateKeyType() done new [id=" + kt.getId()
                    + ", contextName=" + kt.getContextName()
                    + ", name=" + kt.getName() + "]");
        }
        return kt;
    }
    */
}
