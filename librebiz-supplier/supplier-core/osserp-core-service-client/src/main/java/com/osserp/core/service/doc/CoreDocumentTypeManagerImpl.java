/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 12, 2016 
 * 
 */
package com.osserp.core.service.doc;

import java.util.List;

import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsUtil;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentTypes;
import com.osserp.common.dms.service.DocumentTypeManagerImpl;
import com.osserp.common.service.Locator;

import com.osserp.core.dms.CoreDocumentTypeManager;
import com.osserp.core.tasks.DocumentTypeFormatChangedSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CoreDocumentTypeManagerImpl extends DocumentTypeManagerImpl 
    implements CoreDocumentTypeManager {
    
    private DocumentTypeFormatChangedSender documentTypeFormatChangedSender; 
    private Locator locator;

    public CoreDocumentTypeManagerImpl(
            Locator locator,
            DocumentTypes documentTypes,
            DocumentTypeFormatChangedSender documentTypeFormatChangedSender) {
        super(documentTypes);
        this.locator = locator;
        this.documentTypeFormatChangedSender = documentTypeFormatChangedSender;
    }

    @Override
    public List<DocumentType> findConfigurable() {
        List<DocumentType> configurable = super.findConfigurable();
        for (int i = 0, j = configurable.size(); i < j; i++) {
            DocumentType type = configurable.get(i);
            DmsManager manager = getDmsManager(type);
            if (manager != null) {
                type.setCount(Long.valueOf(manager.countByType(type)));
            }
        }
        return configurable;
    }

    @Override
    public DocumentType changePersistenceFormat(DocumentType type, int format) {
        DocumentType result = super.changePersistenceFormat(type, format);
        if (documentTypeFormatChangedSender != null) {
            documentTypeFormatChangedSender.send(result);
        } else {
            result = unlock(result);
        }
        return result;
    }

    protected DmsManager getDmsManager(DocumentType documentType) {
        return DmsUtil.getDmsManager(locator, documentType);
    }
}
