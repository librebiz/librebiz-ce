/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 24, 2006 3:08:52 PM 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;

import com.osserp.core.finance.Record;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.records.RecordProductSearchView;
import com.osserp.gui.client.records.RecordView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractProductAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(AbstractProductAction.class.getName());

    /**
     * Forwards to product edit page
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward editProducts(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("editProducts() requested");
        }
        // create product view if not exists
        RecordView view = getRecordView(session);
        if (!view.isProductsChangeable()) {
            saveError(request, ErrorCode.RECORD_UNCHANGEABLE);
            return mapping.findForward(Actions.EXIT);
        }
        view.setProductEditMode(true);
        Long itemId = RequestUtil.fetchLong(request, "itemId");
        if (itemId != null) {
            view.setItemEdit(itemId);
        }
        if (view.getRecord().getItems().isEmpty()) {
            return forwardSelection(mapping, form, request, response);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes an item
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward deleteItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("deleteItem() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        RecordView view = getRecordView(session);
        try {
            view.deleteItem(id);
        } catch (ClientException e) {
            if (log.isDebugEnabled()) {
                log.debug("deleteItem() failed [message=" + e.getMessage() + "]");
            }
            return saveErrorAndReturn(request, e.getMessage());
        }
        Record record = view.getRecord();
        if (record.getItems().isEmpty()) {
            // exit products and forward to record display if record is empty
            return mapping.findForward(Actions.EXIT);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes all items
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward deleteItems(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("deleteItems() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        try {
            view.deleteItems();
        } catch (ClientException e) {
            if (log.isDebugEnabled()) {
                log.debug("deleteItems() failed [message=" + e.getMessage() + "]");
            }
            return saveErrorAndReturn(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Selects an item for edit action
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward editItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        Long id = RequestUtil.getId(request);
        if (log.isDebugEnabled()) {
            log.debug("editItem() request from " + getUser(session).getId()
                    + " for item " + id);
        }
        RecordView recordView = getRecordView(session);
        recordView.setItemEdit(id);
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Cuts an item to paste
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward cutItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        RecordView recordView = getRecordView(session);
        recordView.setItemPaste(RequestUtil.getId(request));
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Paste item
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward pasteItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        RecordView recordView = getRecordView(session);
        recordView.pasteItem(RequestUtil.getId(request));
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Selects an item for replace action
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward replaceItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        Long id = RequestUtil.getId(request);
        if (log.isDebugEnabled()) {
            log.debug("replaceItem() request from " + getUser(session).getId()
                    + " for item " + id);
        }
        RecordView recordView = getRecordView(session);
        recordView.setItemReplace(id);
        return forwardSelection(mapping, form, request, response);
    }

    /**
     * Updates item values
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateItem() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        try {
            recordView.updateItem(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Moves an item down in listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward moveDownItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("moveDownItem() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        RecordView view = getRecordView(session);
        view.moveDownItem(id);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Moves an item up in listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward moveUpItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("moveUpItem() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        RecordView view = getRecordView(session);
        view.moveUpItem(id);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Exits item edit
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitItemEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitItemEdit() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        recordView.setItemEdit(null);
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Disables product edit mode, refreshs view and forwards to 'exit' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        if (view.isItemPasteMode()) {
            view.setItemPaste(null);
            return mapping.findForward(view.getActionTarget());
        }
        view.setProductEditMode(false);
        view.setItemEdit(null);
        view.refresh();
        return mapping.findForward(Actions.EXIT);
    }

    /**
     * Forwards to product selection
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardSelection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardSelection() request from " + getUser(session).getId());
        }
        RecordProductSearchView searchView = (RecordProductSearchView) createView(request, RecordProductSearchView.class.getName());
        searchView.init(getRecordView(session));
        return mapping.findForward(searchView.getName());
    }

    protected RecordView getRecordView(HttpSession session) {
        RecordView view = (RecordView) fetchView(session);
        if (view == null) {
            log.warn("getRecordView() no view bound!");
            throw new ActionException();
        }
        return view;
    }
}
