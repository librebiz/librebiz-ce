/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 11, 2007 4:32:31 PM 
 * 
 */
package com.osserp.common;

import java.io.File;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.FileUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class FileObject {
    private static Logger log = LoggerFactory.getLogger(FileObject.class.getName());

    private String fileName = null;
    private byte[] fileData = null;
    private boolean referenceOnly = false;
    private String referenceFilename = null;

    protected FileObject() {
        super();
    }

    public FileObject(String fileName, byte[] fileData) {
        super();
        this.fileName = fileName;
        this.fileData = fileData;
        if (log.isDebugEnabled()) {
            log.debug("<init> invoked, using byte array for payload");
        }
    }

    public FileObject(String fileName, InputStream fileDataStream, int readBufferSize, String temp) throws FileException {
        super();
        this.fileName = fileName;
        if (temp != null) {
            File tempDir = new File(temp);
            if (!tempDir.exists() || !tempDir.isDirectory() || !tempDir.canWrite()) {
                throw new FileException("invalid temp dir location provided [name=" + temp
                        + ", exists=" + tempDir.exists()
                        + ", directory=" + tempDir.isDirectory()
                        + ", writable=" + tempDir.canWrite() + "]");
            }
            referenceFilename = FileUtil.generateRandomFileName(temp, fileName);
            try {
                FileUtil.persist(fileDataStream, referenceFilename);
                referenceOnly = true;
            } catch (Exception e) {
                referenceFilename = null;
                throw new FileException(e.getMessage(), e);
            }
        } else {
            fileData = FileUtil.getBytes(fileDataStream, readBufferSize);
            if (log.isDebugEnabled()) {
                log.debug("<init> invoked, using input stream for payload");
            }
        }
    }

    public String getFileType() {
        return FileUtil.getFileType(fileName);
    }

    public String getFileName() {
        return fileName;
    }

    public File getFile() throws ClientException {
        if (referenceFilename == null) {
            throw new ActionException();
        }
        File tempFile = new File(referenceFilename);
        if (!tempFile.exists() || tempFile.isDirectory() || !tempFile.canRead()) {
            throw new ClientException(ErrorCode.FILE_MISSING);
        }
        return tempFile;
    }

    public byte[] getFileData() {
        if (referenceOnly) {
            File tempFile = new File(referenceFilename);
            if (!tempFile.exists() || tempFile.isDirectory() || !tempFile.canWrite()) {
                throw new IllegalStateException("invalid filename found [name=" 
                        + referenceFilename
                        + ", exists=" + tempFile.exists()
                        + ", file=" + !tempFile.isDirectory()
                        + ", writable=" + tempFile.canWrite() + "]");
            }
            try {
                log.debug("getFileData() trying to read from tempfile " + referenceFilename);
                return FileUtil.getBytes(tempFile);
            } catch (Exception e) {
                throw new IllegalStateException("failed reading file " + tempFile);
            }
        }
        return fileData;
    }
    
    public void cleanupReference() {
        File tempFile = new File(referenceFilename);
        if (tempFile.exists() && !tempFile.isDirectory() && tempFile.canWrite()) {
            try {
                FileUtil.delete(tempFile);
            } catch (Exception e) {
                log.error("cleanupReference() ignoring exception [message=" + e.getMessage() + "]", e);
            }
        }
    }

    public boolean isReferenceOnly() {
        return referenceOnly;
    }

    public String getReferenceFilename() {
        return referenceFilename;
    }
}
