SET search_path = osserp, pg_catalog;

UPDATE system_menu_links SET permissions = 'product_selections_admin,organisation_admin' WHERE id = 46;

UPDATE system_properties SET pvalue = '0.14.7', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
