/**
 *
 * Copyright (C) 2015, 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 20, 2017 
 * 
 */
package com.osserp.core.service.doc

import javax.xml.transform.Source
import javax.xml.transform.stream.StreamSource

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.jdom2.Document

import com.osserp.common.Constants
import com.osserp.common.dms.DmsManager
import com.osserp.common.service.ResourceLocator
import com.osserp.common.service.TemplateManager
import com.osserp.common.util.FileUtil
import com.osserp.common.xml.JDOMUtil

import com.osserp.core.dao.SystemConfigs
import com.osserp.core.dao.records.RecordTypes
import com.osserp.core.dms.CoreStylesheetConfigService
import com.osserp.core.employees.Employee
import com.osserp.core.finance.RecordType
import com.osserp.core.service.doc.XmlService
import com.osserp.core.system.SystemCompanyManager
import com.osserp.core.system.SystemConfigManager

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class CoreStylesheetConfigServiceImpl extends CoreStylesheetServiceImpl implements CoreStylesheetConfigService {
    private static Logger logger = LoggerFactory.getLogger(CoreStylesheetConfigServiceImpl.class.getName())
    private String stylesheetDir
    private String xmlDataDir
    private XmlService xmlService
    private RecordTypes recordTypes
    
    CoreStylesheetConfigServiceImpl(
        ResourceLocator resourceLocator,
        TemplateManager templateManager,
        String userConfigFile,
        SystemConfigManager systemConfigManager,
        SystemCompanyManager systemCompanyManager,
        DmsManager dmsManager,
        RecordTypes recordTypes,
        XmlService xmlService,
        String stylesheetLocation) {
        super(resourceLocator, templateManager, userConfigFile, systemConfigManager, systemCompanyManager, dmsManager)
        this.recordTypes = recordTypes
        this.xmlService = xmlService 
        stylesheetDir = stylesheetLocation
        xmlDataDir = "${stylesheetDir}/data"
    }
    
    byte[] createPdf(Employee employee, String stylesheet, String dataFileName) {
        Source sheet = getStylesheetSource(stylesheet)
        if (sheet) {
            Document xml = JDOMUtil.parse(createXmlFilename(dataFileName))
            if (xml) {
                byte[] pdf = createPdf(xml, sheet)
                if (pdf) {
                    return pdf
                }
            }
        }
        return null
    }
        
    byte[] createRecordPdf(Employee employee, String dataFileName) {
        assert employee != null
        RecordType recordType = fetchRecordType(dataFileName)
        Map<String, Object> values = getMap(employee.branch?.id, new Locale(Constants.DEFAULT_LANGUAGE), [:])
        String xslText = createDefaultText(recordType, values)
        if (logger.isInfoEnabled() && isSheetLoggingEnabled()) {
            //logs the (generated) XSLT Stylesheet
            logger.info("createRecordPdf: generate sheet done\n")
            logger.info(xslText)
        }
        Source sheet = new StreamSource(new StringReader(xslText))
        if (sheet) {
            Document xml = createXmlDocument(employee, dataFileName)
            if (xml) {
                return createPdf(xml, sheet)
            }
        }
        return null
    }
    
    private Document createXmlDocument(Employee employee, String dataFileName) {
        Document record = JDOMUtil.parse(createXmlFilename(dataFileName))
        Document doc = xmlService.createDocument(employee)
        try {
            if (employee.branch?.id) {
                doc.getRootElement().addContent(xmlService.createBranch("branch", employee.branch.id))
            }
            doc = JDOMUtil.addElements(doc, record)
            if (isXmlLoggingEnabled() && logger.isInfoEnabled()) {
                logger.info("createXmlDocument: done, result:\n${JDOMUtil.getPrettyString(doc)}")
            }
        } catch (Exception e) {
            logger.error("createXmlDocument: caught exception [message=${e.message}]", e)
        }
        return doc
    }
    
    private String createXmlFilename(String dataFileName) {
        "${xmlDataDir}/${dataFileName}.xml"
    }
    
    private RecordType fetchRecordType(String dataFileName) {
        List<RecordType> recordTypeList = recordTypes.getRecordTypes()
        RecordType result = null
        recordTypeList.each { RecordType next ->
            if (next.resourceKey == dataFileName) {
                result = next
            }
        }
        logger.debug("fetchRecordType: done [result=${result?.id}]")
        return result
    }
}
