<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${!empty record.deliveryAddress}">
<c:set var="address" value="${record.deliveryAddress}"/>
<div class="recordLabelValue">
    <a onMouseOver="show_info('deliveryAddress')" onMouseOut="hide_info('deliveryAddress')" href="javascript:;" ><fmt:message key="deliveryAddress"/></a>
    <div id="deliveryAddress" class="deliveryAddress">
        <br/><o:out value="${address.name}"/>
        <br/><o:out value="${address.street}"/>
        <br/><o:out value="${address.zipcode}"/> <o:out value="${address.city}"/>
    </div>
</div>
</c:if>