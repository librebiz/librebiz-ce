/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2007 10:34:35 AM 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.util.CollectionUtil;
import com.osserp.core.Comparators;
import com.osserp.core.mail.MailingList;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class MailingListImpl extends AbstractOption implements MailingList {
    private static Logger log = LoggerFactory.getLogger(MailingListImpl.class.getName());

    private boolean active = false;
    private List<EmailAddress> members = new ArrayList<EmailAddress>();

    protected MailingListImpl() {
        super();
    }

    public MailingListImpl(
            String name,
            String description,
            Long reference,
            Long createdBy) {
        super((Long) null, reference, name, description, null, createdBy);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<EmailAddress> getMembers() {
        return members;
    }

    public void setMembers(List<EmailAddress> members) {
        this.members = members;
    }

    public void sortMembers() {
        CollectionUtil.sort(members, Comparators.createEmailComparator(false));
    }

    public String getRecipients() {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0, j = members.size(); i < j; i++) {
            EmailAddress next = members.get(i);
            if (next != null && next.getEmail() != null) {
                if (buffer.length() > 0) {
                    buffer.append(",");
                }
                buffer.append(next.getEmail());
            } else {
                log.warn("getRecipients() ignoring empty address [id="
                        + (next == null || next.getId() == null ? "null]" : (next.getId() + "]")));
            }
        }
        return buffer.toString();
    }
}
