<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<tr><td colspan="2"> </td></tr>
<c:choose>
<c:when test="${!empty requestScope.delivery_address_header}">
    <c:import url="${requestScope.delivery_address_header}" />
</c:when>
<c:otherwise>
    <tr><td><fmt:message key="deliveryAddress"/></td><td> </td></tr>
</c:otherwise>
</c:choose>
<tr>
	<td><fmt:message key="name"/></td>
	<td><input type="text" class="input" name="name" value="<o:out value="${record.deliveryAddress.name}"/>"/></td>
</tr>
<tr>
	<td><fmt:message key="street"/></td>
	<td><input type="text" class="input" name="street" value="<o:out value="${record.deliveryAddress.street}"/>"/></td>
</tr>
<tr>
	<td><fmt:message key="zipCodeCity"/></td>
	<td><input type="text" style="width:15%;" name="zipcode" value="<o:out value="${record.deliveryAddress.zipcode}"/>"/> <input type="text" style="width:84%;" name="city" value="<o:out value="${record.deliveryAddress.city}"/>"/></td>
</tr>
