/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Feb-2007 16:52:25 
 * 
 */
package com.osserp.core.finance;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.core.BusinessCase;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordOutputService {

    /**
     * Adds business case values to a document
     * @param doc
     * @param businessCase
     */
    void addBusinessCase(Document doc, BusinessCase businessCase);

    /**
     * Provides the most current correction as xml if implementing record provides corrections
     * @param record
     * @return correction as xml or null if record doesn't support corrections or no correction exists
     * @throws ClientException if validation failed
     */
    Element createCorrection(Record record) throws ClientException;

    /**
     * Creates xml output of a record
     * @param employee
     * @param record
     * @param preview
     * @return document
     */
    Document createDocument(Employee employee, Record record, boolean preview);

    /**
     * Creates the specific xsl document required to render the pdf
     * @param employee
     * @param record
     * @return xsl document
     * @throws ClientException
     */
    String createStylesheetText(Employee employee, Record record) throws ClientException;

    /**
     * Creates pdf representation of a record document
     * @param record
     * @param doc
     * @return pdf as byte array
     * @throws ClientException if validation failed
     */
    byte[] createPdf(Record record, Document doc) throws ClientException;

    /**
     * Creates a correction pdf document
     * @param doc
     * @return correction pdf
     * @throws ClientException
     */
    byte[] createCorrectionPdf(Document doc, Record record) throws ClientException;
}
