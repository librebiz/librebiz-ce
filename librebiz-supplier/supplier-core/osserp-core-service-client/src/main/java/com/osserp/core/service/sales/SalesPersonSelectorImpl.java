/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 29, 2012 
 * 
 */
package com.osserp.core.service.sales;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.StringUtil;
import com.osserp.core.Comparators;
import com.osserp.core.dao.Employees;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeUtil;
import com.osserp.core.sales.SalesPersonSelector;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.Permissions;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesPersonSelectorImpl implements SalesPersonSelector {
    private static Logger log = LoggerFactory.getLogger(SalesPersonSelectorImpl.class.getName());
    private Employees employees;

    protected SalesPersonSelectorImpl(Employees employees) {
        super();
        this.employees = employees;
    }

    public List<Employee> getSalesPersons(DomainUser user) {
        List<Employee> employeeList = employees.findByGroup(EmployeeGroup.SALES);
        employeeList.addAll(employees.findByPermission(Permissions.SALES));
        List<Employee> result = new ArrayList<Employee>();
        Set<Long> added = new java.util.HashSet<Long>();
        for (int i = 0, j = employeeList.size(); i < j; i++) {
            Employee next = employeeList.get(i);
            if (!added.contains(next.getId())) {
                result.add(next);
                added.add(next.getId());
            }
        }
        result = filter(user, result);
        CollectionUtil.sort(result, Comparators.createEmployeeNameComparator(false));
        return result;
    }

    private List<Employee> filter(DomainUser user, List<Employee> employees) {
        List<Employee> result = new ArrayList<Employee>();
        if (user == null || user.isPermissionGrant(Permissions.BRANCH_IGNORE_PERMISSIONS)) {
            result = employees;
            if (log.isDebugEnabled()) {
                if (user == null) {
                    log.debug("filter() user is null, returning all activated sales employees");
                } else {
                    log.debug("filter() user has required permissions to see all employees [id=" + user.getId()
                            + ", employee=" + user.getEmployee().getId()
                            + ", permissions=" + StringUtil.createCommaSeparated(Permissions.BRANCH_IGNORE_PERMISSIONS) + "]");
                }
            }
        } else if (user.getEmployee().isCompanyExecutive()) {
            result = EmployeeUtil.createByCompanyExecutive(employees, user.getEmployee());
            if (log.isDebugEnabled()) {
                log.debug("filter() user is company executive [id=" + user.getId()
                        + ", employee=" + user.getEmployee().getId() + "]");
            }
        } else {
            result = EmployeeUtil.createBySameBranch(employees, user.getEmployee());
            if (log.isDebugEnabled()) {
                log.debug("filter() user is limited to branch [id=" + user.getId()
                        + ", employee=" + user.getEmployee().getId() + "]");
            }
        }
        return result;
    }

}
