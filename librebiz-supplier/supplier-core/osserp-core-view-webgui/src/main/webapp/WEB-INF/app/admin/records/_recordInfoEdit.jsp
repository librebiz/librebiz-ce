<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${view.editMode}">
                        <fmt:message key="changeText" />
                        </c:when>
                    <c:otherwise>
                        <fmt:message key="createNewText" />
                    </c:otherwise>
                </c:choose>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <div id="noteEditor">
                <v:form name="recordInfoEditForm" url="/admin/records/recordInfoEditor/save">
                    <table class="valueTable">
                        <tr>
                            <td colspan="2"><v:textarea styleClass="form-control" name="name" value="${view.bean.name}" rows="5" /></td>
                        </tr>
                        <tr>
                            <td><input class="form-control cancel" type="button" onclick="gotoUrl('<v:url value="/admin/records/recordInfoEditor/select"/>');" value="<fmt:message key="exit"/>" /></td>
                            <td><o:submit styleClass="form-control" /></td>
                        </tr>
                    </table>
                </v:form>
            </div>
        </div>
    </div>
</div>
