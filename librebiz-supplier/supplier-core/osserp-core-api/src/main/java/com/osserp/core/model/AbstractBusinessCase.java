/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 1, 2009 7:19:55 AM 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.User;
import com.osserp.common.XmlAwareEntity;
import com.osserp.common.beans.AbstractPersistent;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractBusinessCase extends AbstractPersistent implements BusinessCase, XmlAwareEntity {
    private static Logger log = LoggerFactory.getLogger(AbstractBusinessCase.class.getName());

    private BusinessType type;
    private Long status = 0L;

    private Long initialTypeId;

    protected Long getInitialTypeId() {
        return initialTypeId;
    }

    protected void setInitialTypeId(Long initialTypeId) {
        this.initialTypeId = initialTypeId;
    }

    private List<FcsItem> flowControlSheet = new ArrayList<FcsItem>();

    protected AbstractBusinessCase() {
        super();
    }

    protected AbstractBusinessCase(BusinessType type) {
        super();
        setBusinessType(type);
        if (getCreatedBy() == null) {
            setCreatedBy(Constants.SYSTEM_USER);
        }
    }

    protected AbstractBusinessCase(User user, BusinessType type) {
        this(type);
        if (user != null) {
            setCreatedBy(user.getId());
        }
    }

    protected AbstractBusinessCase(Long userId, BusinessType type) {
        this(type);
        if (isSet(userId)) {
            setCreatedBy(userId);
        }
    }

    protected AbstractBusinessCase(BusinessCase other) {
        update(other);
    }

    protected void update(Long user, BusinessCase other) {
        update(other);
        if (isSet(user)) {
            setChanged(new Date(System.currentTimeMillis()));
            setChangedBy(user);
        }
    }

    protected void update(BusinessCase other) {
        if (other instanceof AbstractDetails) {
            super.update((AbstractDetails) other);
        }
        setBusinessType(other.getType());
        status = other.getStatus();
        flowControlSheet = other.getFlowControlSheet();
    }

    public Long getTypeId() {
        return (type == null) ? null : type.getId();
    }

    public BusinessType getType() {
        return type;
    }

    protected void setType(BusinessType type) {
        this.type = type;
    }

    public abstract String getAccountingReference();

    public abstract void setAccountingReference(String accountingReference);

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }


    public List<FcsItem> getFlowControlSheet() {
        return flowControlSheet;
    }

    protected void setFlowControlSheet(List<FcsItem> flowControlSheet) {
        this.flowControlSheet = flowControlSheet;
    }

    public FcsItem getLastFlowControlSheet() {
        if (flowControlSheet != null && !flowControlSheet.isEmpty()) {
            return flowControlSheet.get(0);
        }
        return null;
    }

    public void addCancelling(FcsItem cancelling) {
        flowControlSheet.add(0, cancelling);
        if (cancelling.getAction() != null
                && cancelling.getAction().getStatus() != null
                && cancelling.getAction().getStatus() > 0) {
            decreaseStatus(cancelling.getAction().getStatus());
        }
    }

    public void removeFlowControl(Long actionId) {
        FcsAction action = null;
        for (int i = 0, j = flowControlSheet.size(); i < j; i++) {
            FcsItem current = flowControlSheet.get(i);
            if (current.getAction().getId().equals(actionId)) {
                action = current.getAction();
                flowControlSheet.remove(i);
                break;
            }
        }
        if (action != null && action.getStatus() != null && action.getStatus() > 0) {
            decreaseStatus(action.getStatus());
        }
    }

    protected void addFlowControl(FcsItem item) {
        if (flowControlSheet.isEmpty()) {
            flowControlSheet.add(item);
        } else {
            flowControlSheet.add(0, item);
        }
    }

    protected void checkFlowControlActionNote(FcsAction action, String note) throws ClientException {
        if (action != null && action.isNoteRequired() && (note == null || note.length() < 5)) {
            throw new ClientException(ErrorCode.ACTION_NOTE_REQUIRED);
        }
    }

    public boolean isClosed() {
        return (status != null && status >= 100);
    }

    protected boolean isFlowControlActionCreateable(FcsAction action) {
        if (!action.isDisplayEverytime()) {
            if (!flowControlSheet.isEmpty()) {
                int regular = 0;
                int canceled = 0;
                for (int i = 0, j = flowControlSheet.size(); i < j; i++) {
                    FcsItem next = flowControlSheet.get(i);
                    if (next.getAction().getId().equals(action.getId())) {
                        if (log.isDebugEnabled()) {
                            log.debug("addFlowControl() found existing...");
                        }
                        if (next.isCancels()) {
                            if (log.isDebugEnabled()) {
                                log.debug("addFlowControl() existing cancels");
                            }
                            canceled++;
                        } else {
                            if (log.isDebugEnabled()) {
                                log.debug("addFlowControl() existing is regular");
                            }
                            regular++;
                        }
                    }
                }
                if (regular != canceled) {
                    if (log.isDebugEnabled()) {
                        log.debug("addFlowControl() action already exists");
                    }
                    // action already exists
                    return false;
                }
            }
        }
        return true;
    }

    protected void increaseStatus(Integer increaseBy) {
        if (getStatus() == null || getStatus().intValue() < increaseBy) {
            setStatus(Long.valueOf(increaseBy));
        }
    }

    protected void decreaseStatus(Integer decreaseBy) {
        int previousHighest = 0;
        for (int i = 0, j = flowControlSheet.size(); i < j; i++) {
            FcsItem next = flowControlSheet.get(i);
            if (next.getAction().getStatus() != null
                    && next.getAction().getStatus() > 0
                    && !next.getAction().getStatus().equals(decreaseBy)) {
                if (previousHighest == 0) {
                    previousHighest = next.getAction().getStatus();
                } else if (previousHighest < next.getAction().getStatus()) {
                    previousHighest = next.getAction().getStatus();
                }
            }
        }
        status = Long.valueOf(previousHighest);
    }

    protected void setBusinessType(BusinessType businessType) {
        if (businessType == null) {
            log.warn("setBusinessType() refusing to set type to NULL on business case " + getPrimaryKey());
        } else {
            if (initialTypeId == null) {
                initialTypeId = businessType.getId();
            }
            type = businessType;
            if (businessType.getWorkflow() == null) {
                log.error("setBusinessType() selected misconfigured type without workflow [id=" + businessType.getId() + "]");
                throw new ActionException("misconfigured business type found: " + businessType.getId());
            }
            classDisc = businessType.getContext();
            workflowId = businessType.getWorkflow().getId();
            if (log.isDebugEnabled()) {
                log.debug("setBusinessType() done [type=" + businessType.getName() + ", classDisc=" + classDisc + "]");
            }
        }
    }

    /* Discriminator for orm */

    private String classDisc;

    protected String getClassDisc() {
        return classDisc;
    }

    protected void setClassDisc(String classDisc) {
        this.classDisc = classDisc;
    }

    private Long workflowId = null;

    protected Long getWorkflowId() {
        return workflowId;
    }

    protected void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }

    public String getNoteServiceName() {
        return "salesNoteManager";
    }

    public String getContextName() {
        return isSalesContext() ? "sales" : "request";
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("type", type));
        root.addContent(JDOMUtil.createElement("status", status));
        root.addContent(JDOMUtil.createElement("flowControlSheet", flowControlSheet));
        return root;
    }
}
