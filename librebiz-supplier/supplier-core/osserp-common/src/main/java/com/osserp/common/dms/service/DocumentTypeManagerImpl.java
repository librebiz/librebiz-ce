/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 11, 2007 8:35:15 AM 
 * 
 */
package com.osserp.common.dms.service;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.DocumentTypeManager;
import com.osserp.common.dms.dao.DocumentTypes;
import com.osserp.common.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DocumentTypeManagerImpl extends AbstractService implements DocumentTypeManager {

    private DocumentTypes documentTypes = null;

    /**
     * Creates a new document type manager
     * @param documentTypes
     */
    public DocumentTypeManagerImpl(DocumentTypes documentTypes) {
        this.documentTypes = documentTypes;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.common.dms.DocumentTypeManager#findAll()
     */
    public List<DocumentType> findAll() {
        return documentTypes.getList();
    }

    /* (non-Javadoc)
     * @see com.osserp.common.dms.DocumentTypeManager#findConfigurable()
     */
    public List<DocumentType> findConfigurable() {
        List<DocumentType> result = new ArrayList<>();
        List<DocumentType> all = documentTypes.getList();
        for (int i = 0, j = all.size(); i < j; i++) {
            DocumentType next = all.get(i);
            if (next.isConfigurable()) {
                result.add(next);
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.common.dms.DocumentTypeManager#findById(java.lang.Long)
     */
    public DocumentType findById(Long id) {
        try {
            return documentTypes.get(id);
        } catch (Throwable t) {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.common.dms.DocumentTypeManager#load(java.lang.Long)
     */
    public DocumentType load(Long id) {
        DocumentType result = findById(id);
        if (result == null) {
            throw new IllegalArgumentException("loading doc type failed for id " + id);
        }
        return result;
    }

    public Long countByReference(Long id, Long reference) {
        return documentTypes.countByReference(id, reference);
    }

    public DocumentType changeDownloadName(DocumentType type) {
        return documentTypes.toggleDownloadName(type);
    }
    
    public DocumentType changePersistenceFormat(DocumentType type, int format) {
        return documentTypes.updateFormat(type, format);
    }

    public DocumentType unlock(DocumentType type) {
        return documentTypes.unlock(type);
    }
}
