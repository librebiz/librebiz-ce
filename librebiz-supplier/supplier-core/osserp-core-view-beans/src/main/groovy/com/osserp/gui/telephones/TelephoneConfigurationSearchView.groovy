/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.PermissionException

import com.osserp.core.telephone.TelephoneConfigurationManager
import com.osserp.core.telephone.TelephoneSystem
import com.osserp.core.telephone.TelephoneSystemManager

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 * @author so <so@osserp.com>
 */
public class TelephoneConfigurationSearchView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TelephoneConfigurationSearchView.class.getName())

    private static final String TELEPHONE_EXCHANGE_EDIT = 'telephone_exchange_edit'
    private static final String TELEPHONE_SYSTEN_ADMIN = 'telephone_system_admin'
    private static final String TELEPHONE_EDIT_PERMISSIONS = TELEPHONE_EXCHANGE_EDIT+','+TELEPHONE_SYSTEN_ADMIN
    List<TelephoneSystem> telephoneSystems
    TelephoneSystem telephoneSystem
    boolean includeEmployeeTelephoneSystems
    boolean onlyTelephoneExchanges

    TelephoneConfigurationSearchView() {
        providesCreateMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!telephoneSystems) {
                TelephoneSystemManager manager = getService(TelephoneSystemManager.class.getName())
                telephoneSystems = manager.getAll()
            }
        }
    }

    @Override
    MenuItem getCreateLink() {
        return new MenuItem(link: '/telephones/telephoneConfigurationPopup/enableCreateMode', title: 'createTelephoneConfiguration', icon: 'smallNewIcon', newWindow: false, ajaxPopup: true, ajaxPopupName: 'telephoneConfigurationPopupView', permissions: TELEPHONE_SYSTEN_ADMIN, permissionInfo: 'permissionTelephoneSystemAdmin')
    }

    @Override
    void reload() {
        logger.debug('reload() invoked...')
        TelephoneSystemManager manager = getService(TelephoneSystemManager.class.getName())
        telephoneSystems = manager.getAll()
        TelephoneConfigurationManager tcmanager = getService(TelephoneConfigurationManager.class.getName())
        list = tcmanager.search((telephoneSystem?.getBranchId()) ? telephoneSystem.getBranchId() : null, includeEmployeeTelephoneSystems, onlyTelephoneExchanges)
        if (actualSortKey) {
            String[] keys = actualSortKey.split("\\.")
            def key = keys[0]
            if (keys.length > 1) {
                def childKey = keys[1]
                list.sort{ a, b -> a?."$key"?."$childKey" <=> b?."$key"?."$childKey" }
            } else {
                list.sort{ a, b -> a?."$key" <=> b?."$key" }
            }
        }
    }

    void search() throws PermissionException {
        logger.debug('search() invoked...')
        this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
        TelephoneSystemManager tsmanager = getService(TelephoneSystemManager.class.getName())
        Long id = form.getLong('id')
        if (id) {
            telephoneSystem = tsmanager.find(id)
        } else {
            telephoneSystem = null
        }
        includeEmployeeTelephoneSystems = form.getBoolean('includeEmployeeTelephoneSystems')
        onlyTelephoneExchanges = form.getBoolean('onlyTelephoneExchanges')
        TelephoneConfigurationManager manager = getService(TelephoneConfigurationManager.class.getName())
        list = manager.search((telephoneSystem?.getBranchId()) ? telephoneSystem.getBranchId() : null, includeEmployeeTelephoneSystems, onlyTelephoneExchanges)
    }

    void findTelephoneExchangesByTelephoneSystem() throws PermissionException {
        logger.debug('findTelephoneExchangesByTelephoneSystem() invoked...')
        this.checkPermission(TELEPHONE_EDIT_PERMISSIONS)
        list = []
        Long id = getDomainEmployee()?.getDefaultRoleConfig()?.getBranch()?.getId()
        TelephoneSystemManager manager = getService(TelephoneSystemManager.class.getName())
        telephoneSystem = manager.findByBranch(id)
        onlyTelephoneExchanges = true
        if (telephoneSystem?.getBranchId()) {
            logger.debug('findTelephoneExchangesByTelephoneSystem() [telephoneSystem='+telephoneSystem+']')
            TelephoneConfigurationManager tcmanager = getService(TelephoneConfigurationManager.class.getName())
            list = tcmanager.search(telephoneSystem.getBranchId(), includeEmployeeTelephoneSystems, onlyTelephoneExchanges)
        }
    }
}
