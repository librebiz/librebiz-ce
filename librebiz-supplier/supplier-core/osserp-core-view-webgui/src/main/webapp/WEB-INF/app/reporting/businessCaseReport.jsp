<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="businessCaseReportView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle /></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
.table .recordType {
    text-align: center;
}
.table .recordCustomer {
    text-align: left;
}
.table .recordStatus {
    text-align: right;
}
.table .recordAction {
    text-align: left;
}
.table .recordDate {
    text-align: center;
}
.table .recordEmployee {
    text-align: left;
}
</style>
</tiles:put>
<tiles:put name="headline">
    <fmt:message key="businessCaseReportViewHeader" />
    <c:if test="${!empty view.queryResult.rows and !empty view.selectedBranch}">
    - <o:out value="${view.selectedBranch.name}"/>
    </c:if>
</tiles:put>
<tiles:put name="headline_right"><v:navigation/></tiles:put>
<tiles:put name="content" type="string">
    <div class="content-area" id="projectMonitoringContent">
        <div class="row">
            <c:choose>
                <c:when test="${empty view.queryResult.rows}">
                    <c:import url="_businessCaseReportForm.jsp" />
                </c:when>
                <c:otherwise>
                    <div class="col-lg-12 panel-area">
                        <c:import url="_businessCaseReportList.jsp" />
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</tiles:put>
</tiles:insert>
