/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 02.09.2004 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Map;

import com.osserp.common.Mappable;
import com.osserp.common.beans.OptionImpl;
import com.osserp.core.contacts.Salutation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalutationImpl extends OptionImpl implements Mappable, Salutation {
    private String displayName = null;
    private String displayNameLetterAddress = null;
    private boolean addName = true;
    private boolean anonymous = false;
    private boolean female = false;

    protected SalutationImpl() {
        super();
    }

    public SalutationImpl(Long id, String name) {
        super(id, name);
    }

    public SalutationImpl(
            Long id,
            String name,
            String displayName,
            boolean addName,
            boolean female,
            String displayNameLetterAddress) {

        super(id, name);
        this.displayName = displayName;
        this.addName = addName;
        this.female = female;
        this.displayNameLetterAddress = displayNameLetterAddress;
    }

    protected SalutationImpl(Map<String, Object> map) {
        super(map);
        this.displayName = fetchString(map, "displayName");
        this.addName = fetchBoolean(map, "addName");
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "displayName", displayName);
        putIfExists(map, "addName", addName);
        return map;
    }

    public String getDisplayNameLetterAddress() {
        return displayNameLetterAddress;
    }

    public void setDisplayNameLetterAddress(String displayNameLetterAddress) {
        this.displayNameLetterAddress = displayNameLetterAddress;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isAddName() {
        return addName;
    }

    public void setAddName(boolean addName) {
        this.addName = addName;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public boolean isFemale() {
        return female;
    }

    public void setFemale(boolean female) {
        this.female = female;
    }
}
