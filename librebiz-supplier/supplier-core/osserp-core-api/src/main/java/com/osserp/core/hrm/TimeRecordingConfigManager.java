/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 3, 2011 1:15:46 PM 
 * 
 */
package com.osserp.core.hrm;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public interface TimeRecordingConfigManager {

    /**
     * Creates a new time recording config
     * @param user
     * @param name
     * @param description
     * @return new created config
     * @throws ClientException if name already exists
     */
    TimeRecordingConfig createConfig(Employee user, String name, String description) throws ClientException;

    /**
     * Craetes a new time record marker type
     * @param user
     * @param id
     * @param name
     * @param description
     * @return new created type
     * @throws ClientException if starting name already exists
     */
    TimeRecordMarkerType createMarkerType(Employee user, Long id, String name, String description) throws ClientException;

    /**
     * Craetes a new time record type
     * @param user
     * @param id
     * @param name
     * @param description
     * @return new created type
     * @throws ClientException if starting name already exists
     */
    TimeRecordType createType(Employee user, Long id, String name, String description) throws ClientException;

    /**
     * Provides available time recording configs
     * @return configs
     */
    List<TimeRecordingConfig> findConfigs();

    /**
     * Provides available time record types
     * @return types
     */
    List<TimeRecordType> findTypes();

    /**
     * Provides available time record marker types
     * @return marker types
     */
    List<TimeRecordMarkerType> findMarkerTypes();

    /**
     * Perists time recording config
     * @param user
     * @param config
     */
    void save(Employee user, TimeRecordingConfig config);

    /**
     * Persists a marker type
     * @param user
     * @param marker type
     */
    void save(Employee user, TimeRecordMarkerType type);

    /**
     * Persists a type
     * @param user
     * @param type
     */
    void save(Employee user, TimeRecordType type);

}
