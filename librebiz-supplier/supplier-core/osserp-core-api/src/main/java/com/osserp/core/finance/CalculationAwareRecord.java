/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01-Feb-2007 09:41:05 
 * 
 */
package com.osserp.core.finance;

import java.util.List;

import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CalculationAwareRecord extends Record {

    /**
     * Clears an Updates items and option items by a calculation
     * @param calculation
     */
    void updateByCalculation(Calculation calculation);

    /**
     * Provides a calculation info
     * @return calculationInfo
     */
    String getCalculationInfo();

    /**
     * Sets a calculation info
     * @param calculationInfo
     */
    void setCalculationInfo(String calculationInfo);

    /**
     * Provides the calculator service name
     * @return calculatorName
     */
    String getCalculatorName();

    /**
     * Provides the minimal margin
     * @return minimalMargin
     */
    Double getMinimalMargin();

    /**
     * Provides the target margin
     * @return targetMargin
     */
    Double getTargetMargin();

    /**
     * Provides optional items not included in total amounts
     * @return options
     */
    List<Item> getOptions();
}
