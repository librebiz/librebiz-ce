/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 2:42:27 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.telephone.TelephoneGroups;
import com.osserp.core.model.telephone.TelephoneGroupImpl;
import com.osserp.core.telephone.TelephoneGroup;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneGroupsDao extends AbstractDao implements TelephoneGroups {

    protected TelephoneGroupsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public TelephoneGroup find(Long id) {
        if (id != null) {
            List<TelephoneGroup> list = getCurrentSession().createQuery(
                    "from " + TelephoneGroupImpl.class.getName() + " t where t.id = :pk")
                    .setParameter("pk", id).list();
            if (!list.isEmpty()) {
                return list.get(0);
            }
        }
        return null;
    }

    public List<TelephoneGroup> findByTelephoneSystem(Long telephoneSystemId) {
        List<TelephoneGroup> list = new ArrayList<TelephoneGroup>();
        if (telephoneSystemId != null) {
            list = getCurrentSession().createQuery(
                    "from " + TelephoneGroupImpl.class.getName()
                    + " t where t.telephoneSystem.id = :systemid order by t.name")
                    .setParameter("systemid", telephoneSystemId).list();
        }
        return list;
    }

    public TelephoneGroup find(String name, Long telephoneSystemId) {
        if (name != null && telephoneSystemId != null) {
            List<TelephoneGroup> list = getCurrentSession().createQuery(
                    "from " + TelephoneGroupImpl.class.getName()
                    + " t where t.name = :name AND t.telephoneSystem.id = :systemid")
                    .setParameter("name", name).setParameter("systemid", telephoneSystemId)
                    .list();
            if (!list.isEmpty()) {
                return list.get(0);
            }
        }
        return null;
    }

    public List<TelephoneGroup> getAll() {
        return getCurrentSession().createQuery("from " + TelephoneGroupImpl.class.getName()
                + " t where t.endOfLife = false order by t.telephoneSystem.id, t.name").list();
    }

    public void save(TelephoneGroup telephoneGroup) {
        getCurrentSession().saveOrUpdate(TelephoneGroupImpl.class.getName(), telephoneGroup);
    }

    public TelephoneGroup create(TelephoneGroup telephoneGroup) {
        save(telephoneGroup);
        return telephoneGroup;
    }
}
