<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productStockQueriesView" />
<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="querySelection"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <c:forEach var="query" items="${view.list}" varStatus="s">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.id == query.id}">
                                <span style="font-weight: bold;"><o:out value="${query.name}"/></span>
                            </c:when>
                            <c:otherwise>
                                <v:link url="/products/productStockQueries/select?id=${query.id}"><o:out value="${query.name}"/></v:link>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
