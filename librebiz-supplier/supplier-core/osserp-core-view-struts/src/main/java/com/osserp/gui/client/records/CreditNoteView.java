/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 29, 2006 12:45:17 PM 
 * 
 */
package com.osserp.gui.client.records;

import com.osserp.common.PermissionException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CreditNoteView extends CreateableRecordView {

    /**
     * Changes sales commission status
     * @throws PermissionException if user has no permission to perform desired action
     */
    public void changeSalesCommissionStatus() throws PermissionException;

    /**
     * Indicates that delivery required flag is changeable. E.g. if record is not unchangeable and user has required permissions
     * @return deliveryRequiredFlagChangeable
     */
    public boolean isDeliveryRequiredFlagChangeable();

    /**
     * Changes delivery required flag
     * @throws PermissionException if user is not creator or required permissions not grant
     */
    public void toggleDeliveryRequiredFlag() throws PermissionException;

}
