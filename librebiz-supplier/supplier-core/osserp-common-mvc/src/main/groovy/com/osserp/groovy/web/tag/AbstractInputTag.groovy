/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web.tag



/**
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractInputTag extends AbstractFormTag {

    protected final String execute() {
        createResult()
    }

    /**
     * Creates the rendered default result of the tag.
     * Override this to add own behaviour.
     * @return rendered result
     */
    protected String createResult() {
        StringBuilder buffer = new StringBuilder()
        buffer
                .append('<input type="')
                .append(inputType)
                .append('" name="')
                .append(name)
                .append('"')
                .append(renderCommonAttributes())
                .append(' value="')
                .append(encodeForAttribute(getCurrentValue()))
                .append('"')
        if (readonly) {
            buffer.append(' readonly="readonly"')
        }
        buffer.append('/>')
        buffer.toString()
    } 

    @Override
    protected String renderId() {
        // always render id for input tags
        String idTag = super.renderId()
        if (!idTag) {
            idTag = " id=\"${name}\""
        }
        return idTag
    }

    /**
     * Provides the content of input type attribute. Override if required.
     * @return type input type attribute value, default is 'text'.
     */
    protected String getInputType() {
        'text'
    }
}
