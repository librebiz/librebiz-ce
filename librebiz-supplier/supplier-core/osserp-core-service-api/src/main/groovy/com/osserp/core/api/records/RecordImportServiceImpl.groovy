/**
 *
 * Copyright (C) 2019 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2019 
 * 
 */
package com.osserp.core.api.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ActionException
import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.OptionsCache
import com.osserp.common.service.Locator

import com.osserp.core.Item
import com.osserp.core.Options
import com.osserp.core.api.AbstractProductMapper
import com.osserp.core.dao.Products
import com.osserp.core.dao.QuantityUnits
import com.osserp.core.dao.SystemConfigs
import com.osserp.core.employees.Employee
import com.osserp.core.finance.OrderManager
import com.osserp.core.finance.Record
import com.osserp.core.finance.RecordBackupItem
import com.osserp.core.finance.RecordImportMapper
import com.osserp.core.finance.RecordImportService
import com.osserp.core.finance.RecordManager
import com.osserp.core.finance.RecordSearch
import com.osserp.core.finance.RecordType
import com.osserp.core.products.Product

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RecordImportServiceImpl extends AbstractProductMapper implements RecordImportService {
    private static Logger logger = LoggerFactory.getLogger(RecordImportServiceImpl.class.getName())

    private Locator serviceLocator
    private RecordSearch recordSearch

    public RecordImportServiceImpl(
            Locator serviceLocator,
            SystemConfigs systemConfigs,
            OptionsCache optionsCache,
            RecordSearch recordSearch,
            Products products,
            QuantityUnits quantityUnits) {
        super(optionsCache, systemConfigs, products, quantityUnits)
        this.serviceLocator = serviceLocator
        this.recordSearch = recordSearch
    }

    public List<String> getAvailableMappers(RecordType type) {
        List<String> result = []
        String defaultMapperName = null
        if (type.resourceKey) {
            String mapperByType = "${type.resourceKey}ImportMapper"
            defaultMapperName = getSystemProperty(mapperByType)
        }
        if (!defaultMapperName) {
            defaultMapperName = getSystemProperty('recordImportMapper')
        }
        if (defaultMapperName && defaultMapperName != 'none') {
            result << defaultMapperName
        }
        return result
    }

    public Record run(Employee user, File file, String mapperName, Map<String, Object> options) throws ClientException {
        assert mapperName != null

        Record record = options.existingRecord
        RecordType recordType = getRecordType(record, options)

        Map<String, Object> mapped = parseImportFile(file, mapperName, options)
        Long recordId = record?.id ?: fetchLong(mapped, 'recordId')

        if (recordId && !record) {
            record = recordSearch.findRecord(recordId, recordType.id)
        } else if (recordId && !recordType) {
            logger.warn('Found recordId while record provided and record.id != recordId')
            throw new ActionException()
        }
        if (!record) {
            return createRecord(user, recordType, mapped, options, recordId)
        }
        return updateRecord(user, record, mapped, options)
    }

    protected Map<String, Object> parseImportFile(File file, String mapperName, Map<String, Object> options) throws ClientException {
        assert file && options
        RecordImportMapper importMapper = getRecordImportMapper(mapperName)
        return importMapper.parse(file, options)
    }

    protected RecordImportMapper getRecordImportMapper(String importMapperName) throws ClientException {
        try {
            return serviceLocator.lookupService(importMapperName)
        } catch (Exception e) {
            throw new ClientException(ErrorCode.CONFIG_MISSING)
        }
    }

    protected RecordType getRecordType(Record existingRecord, Map<String, Object> options) throws ClientException {
        RecordType recordType = existingRecord?.type ?: options['recordType']

        if (!recordType && options.containsKey('recordTypeId')) {
            Long recordTypeId = options['recordTypeId']
            if (recordTypeId) {
                recordType = fetchOption(Options.RECORD_TYPES, recordTypeId)
            }
        }
        if (!recordType) {
            throw new ClientException(ErrorCode.OPERATION_NOT_SUPPORTED)
        }
        return recordType
    }

    protected RecordManager fetchRecordManager(RecordType recordType) {
        try {
            if (recordType.resourceKey) {
                String recordManagerName = getSystemProperty("${recordType.resourceKey}Manager")
                return serviceLocator.lookupService(recordManagerName)
            }
        } catch (Exception e) {
            logger.error("Failed to fetch recordManager: ${e.message}", e)
        }
        throw new ClientException(ErrorCode.CONFIG_MISSING)
    }

    protected Record createRecord(Employee user, RecordType type, Map<String, Object> mapped, Map<String, Object> options, Long customId) throws ClientException {
        // TODO implementation
        throw new ClientException(ErrorCode.OPERATION_NOT_SUPPORTED)
    }

    protected Record updateRecord(Employee user, Record existingRecord, Map<String, Object> mapped, Map<String, Object> options) throws ClientException {
        if (!mapped?.items) {
            throw new ClientException(ErrorCode.RECORD_ITEMS_MISSING)
        }
        RecordManager recordManager = fetchRecordManager(existingRecord.type)
        Record record = recordManager.load(existingRecord.id)

        List<Map<String, Object>> items = mapped.items

        boolean createProduct = options.createProducts == true
        boolean keepProductNames = options.keepProductNames == true
        boolean replace = 'replace' == options.action 
        boolean updatePrice = options.updatePrice == true
        boolean uniqueItems = isSystemPropertyEnabled('recordItemsUnique')
        Set<Long> alreadyAdded = new HashSet()
        
        if (replace) {
            if (recordManager instanceof OrderManager) {
                OrderManager orderManager = recordManager
                orderManager.createNewVersion(user, record, true, RecordBackupItem.CONTEXT_IMPORT)
                record = orderManager.load(record.id)
            }
            record = recordManager.deleteItems(user, record)
        } else if (uniqueItems) {
            record.items.each { Item recordItem ->
                alreadyAdded << recordItem.product.productId
            }
            recordManager.updateStatus(record, user, Record.STAT_CHANGED)
            record = recordManager.load(record.id)
        }
        for (int i = 0; i < items.size(); i++) {
            Map<String, Object> item = items.get(i)
            Product product = getProduct(user, item, createProduct)
            if (product) {
                if (!alreadyAdded.contains(product.productId) || !uniqueItems) {
                    record.addItem(
                        product.currentStock,
                        product,
                        null, //customName
                        item.quantity,
                        item.price,
                        new Date(), //priceDate
                        new BigDecimal(0), //partnerPrice
                        false, //partnerPriceEditable
                        false, //partnerPriceOverridden
                        new BigDecimal(0), // purchasePrice
                        null, // note
                        true, // includePrice
                        null) // externalId
                     alreadyAdded << product.productId
                } else {
                    record.items.each { Item recordItem -> 
                        if (product.productId == recordItem.product.productId) {
                            Double qty = recordItem.quantity + item.quantity
                            recordItem.quantity = qty
                            if (updatePrice && item.price) {
                                recordItem.price = item.price
                            }
                        }
                    }
                }
            } 
        }
        recordManager.persist(record)
        return recordManager.load(record.id)
    }

    protected Product getProduct(Employee user, Map<String, Object> item, boolean createProduct) throws ClientException {
        if (item.productId) {
            Long productId = item.productId
            Product product = products.find(productId)
            if (!product && createProduct) {
                String productName = item.name
                String matchcode = item.customName ?: item.name
                String ts = Long.valueOf(System.currentTimeMillis()).toString()
                Product alreadyExistingProduct = products.findExisting(productName)
                if (alreadyExistingProduct) {
                    productName = "${productName}-${ts}"
                }
                product = products.create(
                    user,
                    productId,
                    fetchClassification(item.typeId, item.groupId, item.categoryId),
                    matchcode,
                    productName,
                    null, // description
                    fetchQuantityUnit(item.quantityUnit),
                    1I,  // packagingUnit
                    null, // manufacturer
                    null, // planningMode
                    null) // other product
            }
            return product
        }
        return null
    }

}
