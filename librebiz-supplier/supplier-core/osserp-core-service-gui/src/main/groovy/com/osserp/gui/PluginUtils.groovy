/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui

import javax.servlet.ServletContext
import javax.servlet.http.HttpServletRequest

import com.osserp.common.User
import com.osserp.common.util.StringUtil
import com.osserp.common.web.SessionUtil

import com.osserp.core.Plugin
import com.osserp.core.PluginHook
import com.osserp.core.Plugins
import com.osserp.core.system.SystemConfig

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class PluginUtils {

    static Plugins getPlugins(ServletContext servletContext) {
        return servletContext?.getAttribute(SystemConfig.APP_PLUGINS_PROPERTY)
    }

    static PluginHook getHook(HttpServletRequest request, String name) {
        Plugins plugins = getPlugins(request.servletContext)
        PluginHook link = plugins.getLink(name)
        if (link) {
            if (!link.userRequired) {
                return link
            }
            User user = SessionUtil.lookupUser(request.session)
            if (user && (!link.permissions || user.isPermissionGrant(
                StringUtil.getTokenArray(link.permissions)))) {
                return link
            }
        }
        return null
    }

    static PluginContext createContext(HttpServletRequest request, PluginHook hook) {
        Plugins plugins = getPlugins(request.servletContext)
        Plugin plugin = plugins.getPlugin(hook)
        return new PluginContext(plugin: plugin, hook: hook)
    }

    static void saveContext(HttpServletRequest request, PluginContext context) {
        request.servletContext?.setAttribute(PluginContext.NAME, context)
    }

    static PluginContext getContext(HttpServletRequest request) {
        return request.servletContext?.getAttribute(PluginContext.NAME)
    }
}
