<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.salesCreatorView}"/>
<c:if test="${!empty view}">
    <style type="text/css">

    </style>
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <fmt:message key="createNewOrder"/>
        </div>
        <div class="modalBoxHeaderRight">
            <div class="boxnav">
            </div>
        </div>
    </div>
    <div class="modalBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>
                <o:logger write="salesCreator.jsp" level="debug"/>
                <v:ajaxForm name="dynamicForm" targetElement="${view.name}_popup" preRequest="displayCreateInfo();" url="/sales/salesCreator/save">
                    <table class="table">
                        <c:if test="${!empty sessionScope.errors}">
                            <c:set var="errorsColspanCount" scope="request" value="2"/>
                            <c:import url="/errors/_errors_table_entry.jsp"/>
                        </c:if>
                        <tr>
                            <td><fmt:message key="orderConfirmationOn"/></td>
                            <td><input class="form-control" type="text" name="date" value="<o:date current="true"/>" /></td>
                        </tr>
                        <tr>
                            <td valign="top"><fmt:message key="comment"/></td>
                            <td><textarea class="form-control" rows="4" name="note"></textarea></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td style="text-align: right;">
                                <span id="exitButton">
                                    <v:ajaxLink url="/sales/salesCreator/exit" linkId="exitLink" targetElement="${view.name}_popup">
                                        <input type="button" class="cancel" value="<fmt:message key="exit"/>"/>
                                    </v:ajaxLink>
                                </span>
                                <span id="saveButton">
                                    <o:submit/>
                                </span>
                            </td>
                        </tr>
                    </table>
                </v:ajaxForm>
                <div class="spacer"></div>
            </div>
        </div>
    </div>
</c:if>
