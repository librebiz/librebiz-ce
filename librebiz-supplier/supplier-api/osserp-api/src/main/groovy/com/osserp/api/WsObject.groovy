/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api


import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.Transient

import org.apache.commons.lang.builder.EqualsBuilder
import org.apache.commons.lang.builder.HashCodeBuilder

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@MappedSuperclass
class WsObject implements WSObject {

    private static final long serialVersionUID = 4L

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name='id')
    Long primaryKey

    @Column(name='external_key')
    String externalKey

    Long getExternalId() {
        if (externalKey) {
            try {
                return Long.parseLong(externalKey)
            } catch (Throwable e) {
            }
        }
        return null
    }

    @Column(name='external_url')
    String externalUrl

    @Column(name='target_id')
    Long targetId

    @Transient
    Map<String, Object> getProperties() {
        return [:]
    }

    boolean propertyEnabled(String name) {
        'true' == properties[name]?.toString()
    }

    @Override
    boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other)
    }

    @Override
    int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this)
    }
}
