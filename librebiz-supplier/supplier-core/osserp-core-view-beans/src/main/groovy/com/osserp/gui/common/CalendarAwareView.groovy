/**
 *
 * Copyright (C) 2003, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 9, 2016
 * 
 * Source code derived from calendar.jsp   
 * 
 */
package com.osserp.gui.common

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Appointment
import com.osserp.common.Calendar
import com.osserp.common.ClientException
import com.osserp.common.util.CalendarUtil

import com.osserp.groovy.web.CalendarView

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class CalendarAwareView extends CoreView implements CalendarView {
    private static Logger logger = LoggerFactory.getLogger(CalendarAwareView.class.getName())

    List<Appointment> appointments = []
    Calendar calendar
    boolean dayViewAvailable = false
    boolean dayView = false
    String daySelectionUrl
    String daySelectionTargetUrl

    // TODO replace with selectionExit and selectionTarget
    String invocationTargetUrl
    String invocationTargetExit

    boolean ignoreTime = false
    int maxTextLength = 25

    CalendarAwareView() {
        super()
        calendar = CalendarUtil.createCalendar()
    }

    protected abstract List<Appointment> fetchAppointments()

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            daySelectionUrl = url("${baseLink}/setDay")
            int textLength = systemPropertyAsInt('calendarTextLength')
            if (textLength) {
                maxTextLength = textLength
            }
            if (!appointments) {
                appointments = fetchAppointments()
            } 
        }
    }

    void setDate() {
        calendar.setDate(form.getInteger('d'), form.getInteger('m'), form.getInteger('y'))
        appointments = fetchAppointments()
    }

    void setDay() {
        calendar.setDate(form.getInteger('d'), calendar.month, calendar.year)
        appointments = fetchAppointments()
    }

    List<Appointment> getByDay() {
        List<Appointment> result = []
        appointments.each { Appointment ap ->
            if (ap.isMatching(calendar)) {
                result << ap
            }
        }
        return result
    }
}
