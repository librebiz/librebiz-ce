<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestSearchView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            .businessCaseAmount {
                text-align: right;
            }
            .businessCaseDate {
                text-align: center;
            }
            .businessCaseInfo {
                text-align: center;
            }
            .businessCaseStatus {
                text-align: right;
            }
            .businessCaseType {
                text-align: center;
            }
        </style>
    </tiles:put>
    <tiles:put name="onload" type="string">onload="$('value').focus();"</tiles:put>
    <tiles:put name="headline"><fmt:message key="searchForRequests"/></tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="row">
            <div class="col-lg-12 panel-area">
                <div class="panel-body">
                    <c:import url="${viewdir}/requests/_requestSearchForm.jsp"/>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
