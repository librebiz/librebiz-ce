<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><fmt:message key="id"/></th>
                <th><fmt:message key="name"/></th>
                <th><fmt:message key="type"/></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${view.types}">
                <c:set var="eventType" value="${item.value}"/>
                <c:if test="${eventType.configurable}">
                    <tr>
                        <td><o:out value="${eventType.id}"/></td>
                        <td>
                            <c:choose>
                                <c:when test="${eventType.flowControl && eventType.flowControlByBusinessType}">
                                    <v:ajaxLink url="/selections/businessTypeSelection/forward?selectionAttribute=businessType&selection=salesMonitoringProjects&selectionTarget=/admin/events/eventConfig/selectEventType?id=${eventType.id}" linkId="businessTypeSelection"><o:out value="${eventType.name}"/></v:ajaxLink>
                                </c:when>
                                <c:otherwise>
                                    <v:link url="${view.baseLink}/selectEventType" parameters="id=${eventType.id}"><o:out value="${eventType.name}"/></v:link>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${eventType.flowControl}">
                                    <fmt:message key="fcs"/>
                                </c:when>
                                <c:when test="${eventType.appointment}">
                                    <fmt:message key="appointment"/>
                                </c:when>
                                <c:when test="${eventType.information}">
                                    <fmt:message key="info"/>
                                </c:when>
                                <c:when test="${eventType.task}">
                                    <fmt:message key="job"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="undefined"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <c:choose>
                        <c:when test="${view.syncRequired}">
                            <v:link url="${view.baseLink}/synchronizeFcs"><fmt:message key="synchronizeEventsWithWorkflow"/></v:link>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="synchronizeEventsWithWorkflowSuccess"/>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </tbody>
    </table>
</div>
