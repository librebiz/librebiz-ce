/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 5:45:59 PM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.List;
import java.util.Map;

import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.RecordNumberConfig;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordTypes {

    /**
     * Provides all billing types
     * @return types
     */
    List<BillingType> getBillingTypes();

    /**
     * Provides billing types of given billingClass
     * @param billingClassId
     * @return types
     */
    List<BillingType> getBillingTypes(Long billingClassId);

    /**
     * Provides billing types by context name
     * @param contextName
     * @return types
     */
    List<BillingType> getBillingTypes(String contextName);

    /**
     * Loads the billing type with given id
     * @param id
     * @return billingType
     */
    BillingType loadBillingType(Long id);

    /**
     * Provides billing fcs action relations
     * @return map of fcs id billing type mapping
     */
    Map<Long, Long> findBillingFcsRelations();

    /**
     * Provides all payment types
     * @return paymentTypes
     */
    List<BillingType> getPaymentTypes();

    /**
     * Provides all payment types
     * @return paymentTypes
     */
    Map<Long, BillingType> getPaymentTypeMap();

    /**
     * Provides all record types
     * @return recordTypes
     */
    List<RecordType> getRecordTypes();

    /**
     * Provides a record type by primary key
     * @return recordType
     */
    RecordType getRecordType(Long id);

    /**
     * Persists record type  
     * @param recordType
     */
    void save(RecordType recordType);
    
    /**
     * Provides all recordtypes as map by id
     * @return recordType map
     */
    Map<Long, RecordType> getRecordTypeMap();
    
    /**
     * Provides available record number configs
     * @param company optional restriction
     * @return configs
     */
    List<RecordNumberConfig> getRecordNumberConfigs(Long company);
    
    /**
     * Provides a record number config by recordType.
     * @param type
     * @param company
     * @return config or null if not exists
     */
    RecordNumberConfig getRecordNumberConfig(RecordType type, Long company);

    /**
     * Creates a record number config by recordType.
     * @param type
     * @param company
     * @param yearly
     * @param digits number of digits to append to yearly sequence (4 if 0)
     * @param initialSequenceValue initial value if config belongs to sequence
     * @param defaultSequenceValue
     * @return config
     */
    RecordNumberConfig createRecordNumberConfig(
            RecordType type, Long company, boolean yearly, 
            int digits, Long initialSequenceValue, Long defaultSequenceValue);
    
    /**
     * Persists a config
     * @param config
     */
    void save(RecordNumberConfig config);
    
    /**
     * Persists a config
     * @param type
     * @param company
     * @param dedicated
     */
    void updateSequenceDedicatedFlag(RecordType type, Long company, boolean dedicated);

    /**
     * Restarts an existing sequence or creates one if not exists.
     * @param config
     * @param newValue
     */
    void createOrRestartSequence(RecordNumberConfig config, Long newValue);
    
    /**
     * Restarts an existing sequence or creates one if not exists.
     * @param type
     * @param company
     * @param newValue
     */
    void createOrRestartSequence(RecordType type, Long company, Long newValue);
}
