/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2009 8:51:32 PM 
 * 
 */
package com.osserp.gui.client.products.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.web.Form;

import com.osserp.core.employees.Employee;
import com.osserp.core.model.products.ProductSelectionConfigVO;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductClassificationEntity;
import com.osserp.core.products.ProductSearch;
import com.osserp.core.products.ProductSearchRequest;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductSelectionManager;
import com.osserp.core.products.ProductType;

import com.osserp.gui.client.products.ProductComparator;
import com.osserp.gui.client.products.ProductSearchAwareView;

/**
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 */
public abstract class AbstractProductSearchView extends AbstractProductClassificationAwareView
        implements ProductSearchAwareView {

    private static Logger log = LoggerFactory.getLogger(AbstractProductSearchView.class.getName());
    private static final String BLANK_SEARCH_FETCHSIZE_PROPERTY = "productSearchFetchSize";
    private static final String KEEP_VIEW_AFTER_ADD = "productSearchKeepAfterAdd";
    private List<ProductClassificationEntity> productGroups = new ArrayList<>();
    private List<ProductClassificationEntity> productCategorys = new ArrayList<>();
    private String columnKey = null;
    private String value = null;
    private boolean valueByNumber = false;
    private boolean showAllProducts = false; // true;
    private boolean noClassification = false;
    private boolean ignoreLazy = false;
    private boolean providingSerialNumberSearch = false;
    private boolean productSelectionConfigsAvailable = false;
    private boolean productSelectionConfigsMode = false;
    private boolean keepViewAfterAdd = false;
    private List<ProductSelectionConfig> productSelectionConfigs = new ArrayList<>();
    private ProductSelectionConfig selectedProductSelectionConfig = null;
    private ProductSelectionConfigItem selectedProductSelectionConfigItem = null;
    private ProductComparator comparator = new ProductComparator();
    private ProductSelection employeeProductFilter = null;
    private int blankSearchProductFetchSize = 100;
    private boolean lastSearchWasBlankSearch = false;
    private Long contactReference;
    private boolean contactReferenceCustomer;
    private boolean contactReferenceRequired = true;
    private String productPriceMethod = "default";
    private String productPriceDefault = "default";
    private Long existingProduct = null;
    private Double existingQuantity = null;
    private BigDecimal existingPrice = null;
    private Double existingTaxRate = null;
    private Double existingReducedTaxRate = null;

    /**
     * Constructor required for serialization.
     */
    protected AbstractProductSearchView() {
        super();
    }

    /**
     * Sets search methods and default search method, view name if annotated, action and list target to 'success'
     * @param methods
     * @param defaultMethod
     * @param contactReferenceRequired
     */
    protected AbstractProductSearchView(Set<String> methods, String defaultMethod, boolean contactRefRequired) {
        super(methods, defaultMethod);
        setReloadWhenExistsOnCreate(true);
        setListTarget(getActionTarget());
        setKeepViewWhenExistsOnCreate(true);
        contactReferenceRequired = contactRefRequired;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        productGroups = (getSelectedType() == null ? new ArrayList<>() : fetchGroups(getSelectedType()));
        productCategorys = (getSelectedGroup() == null ? new ArrayList<>() : fetchCategories(getSelectedGroup()));
        String fetchSizeValue = null;
        try {
            fetchSizeValue = getSystemProperty(BLANK_SEARCH_FETCHSIZE_PROPERTY);
            blankSearchProductFetchSize = Integer.parseInt(fetchSizeValue);
        } catch (Exception e) {
            log.warn("init<> systemProperty not defined [name=" + BLANK_SEARCH_FETCHSIZE_PROPERTY
                    + ", fetchSizeValue=" + fetchSizeValue
                    + ", message=" + e.getMessage() + "]");
        }
        initEmployeeProductFilter();
        keepViewAfterAdd = getDomainUser().isPropertyEnabled(KEEP_VIEW_AFTER_ADD);
        if (!contactReferenceRequired) {
            searchBlank();
        }
    }

    public ProductSelection getEmployeeProductFilter() {
        return employeeProductFilter;
    }

    public void setEmployeeProductFilter(ProductSelection employeeProductFilter) {
        this.employeeProductFilter = employeeProductFilter;
    }

    protected void initEmployeeProductFilter() {
        ProductSelectionManager manager = (ProductSelectionManager) getService(ProductSelectionManager.class.getName());
        ProductSelection productFilter = manager.getSelection(getDomainEmployee(), true);
        if (productFilter != null) {
            employeeProductFilter = productFilter;
            if (log.isDebugEnabled()) {
                if (employeeProductFilter == null) {
                    log.debug("setEmployeeProductFilter() no product filter found [employee=" + getDomainEmployee().getId() + "]");
                } else if (isNotSet(employeeProductFilter.getId())) {
                    log.debug("setEmployeeProductFilter() default product filter found [employee=" + getDomainEmployee().getId() + "]");
                } else {
                    log.debug("setEmployeeProductFilter() product filter found [employee=" + getDomainEmployee().getId()
                            + ", id=" + employeeProductFilter.getId() + "]");
                }
            }
            if (employeeProductFilter != null) {
                if (getTypeSelections().isEmpty()) {
                    setTypeSelections(getClassificationConfigManager().getTypesDisplay());
                }
                for (Iterator<ProductClassificationEntity> types = getTypeSelections().iterator(); types.hasNext();) {
                    ProductType type = (ProductType) types.next();
                    if (!employeeProductFilter.supports(type)) {
                        types.remove();
                        if (log.isDebugEnabled()) {
                            log.debug("setEmployeeProductFilter() removed product type [id=" + type.getId() + "]");
                        }
                    } else if (log.isDebugEnabled()) {
                        log.debug("setEmployeeProductFilter() kept product type [id=" + type.getId() + "]");
                    }
                }
            }
        }
    }

    public boolean isIgnoreLazy() {
        return ignoreLazy;
    }

    public void setIgnoreLazy(boolean ignoreLazy) {
        this.ignoreLazy = ignoreLazy;
    }

    public boolean isKeepViewAfterAdd() {
        return keepViewAfterAdd;
    }

    public void setKeepViewAfterAdd(boolean keepViewAfterAdd) {
        this.keepViewAfterAdd = keepViewAfterAdd;
    }

    public void toggleKeepViewAfterAdd() {
        getDomainUserManager().switchProperty(getUser(), getUser(), KEEP_VIEW_AFTER_ADD);
        keepViewAfterAdd = getDomainUser().isPropertyEnabled(KEEP_VIEW_AFTER_ADD);
    }

    public boolean isNoClassification() {
        return noClassification;
    }

    protected void setNoClassification(boolean noClassification) {
        this.noClassification = noClassification;
    }

    public boolean isShowAllProducts() {
        return showAllProducts;
    }

    protected void setShowAllProducts(boolean showAllProducts) {
        this.showAllProducts = showAllProducts;
    }

    public String getColumnKey() {
        return columnKey;
    }

    protected void setColumnKey(String columnKey) {
        this.columnKey = columnKey;
    }

    public String getValue() {
        return value;
    }

    protected void setValue(String value) {
        this.value = value;
    }

    public Long getContactReference() {
        return contactReference;
    }

    protected void setContactReference(Long contactReference) {
        this.contactReference = contactReference;
    }

    public boolean isContactReferenceCustomer() {
        return contactReferenceCustomer;
    }

    protected void setContactReferenceCustomer(boolean contactReferenceCustomer) {
        this.contactReferenceCustomer = contactReferenceCustomer;
    }

    public String getProductPriceMethod() {
        return productPriceMethod;
    }

    protected void setProductPriceMethod(String productPriceMethod) {
        this.productPriceMethod = productPriceMethod;
    }

    public String getProductPriceDefault() {
        return productPriceDefault;
    }

    protected void setProductPriceDefault(String productPriceDefault) {
        this.productPriceDefault = productPriceDefault;
    }

    public List<ProductClassificationEntity> getProductGroups() {
        return productGroups;
    }

    protected void setProductGroups(List<ProductClassificationEntity> productGroups) {
        this.productGroups = productGroups;
    }

    public List<ProductClassificationEntity> getProductCategorys() {
        return productCategorys;
    }

    protected void setProductCategorys(List<ProductClassificationEntity> productCategorys) {
        this.productCategorys = productCategorys;
    }

    public boolean isProductSelectionConfigsAvailable() {
        return productSelectionConfigsAvailable;
    }

    protected void setProductSelectionConfigsAvailable(
            boolean productSelectionConfigsAvailable) {
        this.productSelectionConfigsAvailable = productSelectionConfigsAvailable;
    }

    public boolean isProductSelectionConfigsMode() {
        return productSelectionConfigsMode;
    }

    protected void setProductSelectionConfigsMode(
            boolean productSelectionConfigsMode) {
        this.productSelectionConfigsMode = productSelectionConfigsMode;
    }

    public List<ProductSelectionConfig> getProductSelectionConfigs() {
        return productSelectionConfigs;
    }

    protected void setProductSelectionConfigs(
            List<ProductSelectionConfig> productSelectionConfigs) {
        this.productSelectionConfigs = productSelectionConfigs;
    }

    public int getProductSelectionConfigsCount() {
        return productSelectionConfigs.size();
    }

    public ProductSelectionConfig getSelectedProductSelectionConfig() {
        return selectedProductSelectionConfig;
    }

    protected void setSelectedProductSelectionConfig(
            ProductSelectionConfig selectedProductSelectionConfig) {
        this.selectedProductSelectionConfig = selectedProductSelectionConfig;
    }

    public void selectProductSelectionConfig(Long id) {
        productSelectionConfigsMode = true;
        selectedProductSelectionConfigItem = null;
        if (id == null) {
            selectedProductSelectionConfig = null;
        } else {
            selectedProductSelectionConfig = (ProductSelectionConfig) CollectionUtil.getById(
                    productSelectionConfigs, id);
        }
        if (log.isDebugEnabled()) {
            log.debug("selectProductSelectionConfig() done [config="
                    + (selectedProductSelectionConfig == null ? "null" : selectedProductSelectionConfig.getId())
                    + "]");
        }
        search();
    }

    public ProductSelectionConfigItem getSelectedProductSelectionConfigItem() {
        return selectedProductSelectionConfigItem;
    }

    protected void setSelectedProductSelectionConfigItem(
            ProductSelectionConfigItem selectedProductSelectionConfigItem) {
        this.selectedProductSelectionConfigItem = selectedProductSelectionConfigItem;
    }

    public void selectProductSelectionConfigItem(Long id) {
        productSelectionConfigsMode = true;
        selectedProductSelectionConfig = null;
        selectedProductSelectionConfigItem = null;
        if (id != null) {
            boolean fetched = false;
            for (int i = 0, j = productSelectionConfigs.size(); i < j; i++) {
                if (fetched) {
                    break;
                }
                ProductSelectionConfig nextConfig = productSelectionConfigs.get(i);
                for (int k = 0, l = nextConfig.getItemCount(); k < l; k++) {
                    ProductSelectionConfigItem nextItem = nextConfig.getItems().get(k);
                    if (id.equals(nextItem.getId())) {
                        selectedProductSelectionConfig = nextConfig;
                        selectedProductSelectionConfigItem = nextItem;
                        fetched = true;
                        break;
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("selectProductSelectionConfigItem() done [config="
                    + (selectedProductSelectionConfig == null ? "null" : selectedProductSelectionConfig.getId())
                    + ", item="
                    + (selectedProductSelectionConfigItem == null ? "null" : selectedProductSelectionConfigItem.getId())
                    + "]");
        }
        search();
    }

    public boolean isProvidingSerialNumberSearch() {
        return providingSerialNumberSearch;
    }

    public void setProvidingSerialNumberSearch(boolean providingSerialNumberSearch) {
        this.providingSerialNumberSearch = providingSerialNumberSearch;
    }

    public void updateClassification(Form form) {
        Long typeId = form.getLong("typeId");
        Long groupId = form.getLong("groupId");
        Long categoryId = form.getLong("categoryId");
        if (log.isDebugEnabled()) {
            log.debug("updateClassification() invoked [typeId=" + typeId + ", groupId=" + groupId + ", categoryId=" + categoryId + "]");
        }

        // TYPES
        if (typeId != null) {
            if (typeId == -1L) {
                selectType(null);
                selectCategory(null);
                selectCategory(null);
                selectedProductSelectionConfig = null;
                productGroups = null;
                productCategorys = null;
                return;
            }
            selectType(typeId);
        }

        // GROUPS
        productGroups = (List<ProductClassificationEntity>) (getSelectedType() == null ? new ArrayList<ProductClassificationEntity>() : getSelectedType()
                .getGroups());
        if (groupId != null) {
            selectGroup(groupId);
        }
        // CATEGORIES
        productCategorys = (List<ProductClassificationEntity>) (getSelectedGroup() == null ? new ArrayList<ProductClassificationEntity>()
                : getSelectedGroup().getCategories());
        if (categoryId != null) {
            selectCategory(categoryId);
        }

        if (getSelectedType() != null
                || getSelectedGroup() != null
                || getSelectedCategory() != null) {
            Employee user = getDomainEmployee();
            ProductSelectionConfig productSelectionConfig = new ProductSelectionConfigVO(
                    user,
                    "temporary",
                    "just in use for product search",
                    null,
                    null);
            productSelectionConfig.addItem(
                    user,
                    (getSelectedType() != null ? getSelectedType().getProductType() : null),
                    (getSelectedGroup() != null ? getSelectedGroup().getProductGroup() : null),
                    (getSelectedCategory() != null ? getSelectedCategory().getProductCategory() : null));
            selectedProductSelectionConfig = productSelectionConfig;
        }
    }

    @Override
    public void reload() {
        if (lastSearchWasBlankSearch) {
            searchBlank();
        } else if (columnKey == null) {
            searchNames();
        } else {
            search();
        }
    }

    public final void search(Form form) throws ClientException {
        deployValues(form);
        lastSearchWasBlankSearch = false;
        if (columnKey == null) {
            searchNames();
        } else {
            search();
        }
    }
    
    protected void searchBlank() {
        try {
            ProductSearch search = getProductSearch();
            List<Product> productList = search.find(
                    new ProductSearchRequest(
                            contactReference,
                            contactReferenceCustomer,
                            blankSearchProductFetchSize),
                    employeeProductFilter);
            setList(productList);
            lastSearchWasBlankSearch = true;
        } catch (Exception e) {
            log.warn("searchBlank() failed to perform [message=" + e.getMessage() + "]", e);
        }
    }

    protected void search() {
        if (log.isDebugEnabled()) {
            log.debug("search() invoked");
        }
        List<Product> list = new ArrayList<>();
        if (productSelectionConfigsMode) {
            list = searchPreselected();

        } else if (getValue() != null || isShowAllProducts()) {

            ProductSelectionConfig config = null;
            if (!noClassification) {
                config = selectedProductSelectionConfig;
            }
            list = search(config);
        }
        if (log.isDebugEnabled()) {
            log.debug("search() done [found=" + list.size() + "]");
        }
        setList(list);
    }

    private List<Product> searchPreselected() {
        if (log.isDebugEnabled()) {
            log.debug("searchPreselected() invoked");
        }
        ProductSearch search = getProductSearch();
        List<Product> list = new ArrayList<>();
        if (selectedProductSelectionConfig == null
                && selectedProductSelectionConfigItem == null) {

            if (isNotSet(value) || isNotSet(columnKey)) {
                list = search.find(
                        new ProductSearchRequest(
                                contactReference,
                                contactReferenceCustomer,
                                isIncludeEol(),
                                blankSearchProductFetchSize),
                        productSelectionConfigs);
            } else {
                ProductSearchRequest searchRequest = new ProductSearchRequest(
                        contactReference,
                        contactReferenceCustomer,
                        getColumnKey(),
                        fetchSearchValue(),
                        isStartsWithSet(),
                        isIncludeEol(),
                        blankSearchProductFetchSize);
                list = search.find(searchRequest, productSelectionConfigs);
                if (list.isEmpty()) {
                    Long id = lookupId();
                    if (id != null) {
                        searchRequest.setColumnKey(Constants.SEARCH_BY_ID);
                        list = search.find(searchRequest, productSelectionConfigs);
                    }
                }
            }
        } else {
            ProductSelectionConfig config = null;
            if (selectedProductSelectionConfigItem != null
                    && selectedProductSelectionConfig != null) {
                config = new ProductSelectionConfigVO(
                        selectedProductSelectionConfig,
                        selectedProductSelectionConfigItem);
            } else {
                config = selectedProductSelectionConfig;
            }
            list = search(config);

        }
        if (log.isDebugEnabled()) {
            log.debug("searchPreselected() done [found=" + list.size() + "]");
        }
        return list;
    }

    private List<Product> search(ProductSelectionConfig config) {
        List<Product> list = new ArrayList<>();
        ProductSearch search = getProductSearch();
        String searchVal = fetchSearchValue();
        if (log.isDebugEnabled()) {
            log.debug("search() invoked [showall=" + showAllProducts
                    + ", value=" + showAllProducts
                    + ", columnKey=" + columnKey
                    + ", searchVal=" + searchVal
                    + "]");
        }
        ProductSearchRequest searchRequest = new ProductSearchRequest(
                contactReference,
                contactReferenceCustomer,
                columnKey,
                searchVal,
                isStartsWithSet(),
                isIncludeEol(),
                blankSearchProductFetchSize);
        list = search.find(searchRequest, config);
        if (list.isEmpty()) {
            Long id = lookupId();
            if (id != null) {
                searchRequest.setColumnKey(Constants.SEARCH_BY_ID);
                list = search.find(searchRequest, config);
            }
        }
        return list;
    }

    private String fetchSearchValue() {
        return showAllProducts ? null : value;
    }

    protected final void searchNames() {
        ProductSearch search = getProductSearch();
        ProductSelectionConfig productSelectionConfig =
                (productSelectionConfigsAvailable && productSelectionConfigsMode
                        ? selectedProductSelectionConfig : selectedProductSelectionConfig != null ? selectedProductSelectionConfig : null);

        if (log.isDebugEnabled()) {
            log.debug("searchNames() invoked [productSelectionConfig=" 
                + (productSelectionConfig == null ? "null": productSelectionConfig.getId()) + "]");
        }
        if (!valueByNumber) {
            setList(search.find(
                    new ProductSearchRequest(
                            contactReference,
                            contactReferenceCustomer,
                            Constants.SEARCH_BY_NAME,
                            getValue(),
                            isStartsWithSet(),
                            isIncludeEol(),
                            blankSearchProductFetchSize),
                    productSelectionConfig));
        } else {
            if (log.isDebugEnabled()) {
                log.debug("searchNames() trying search by number");
            }
            if (getValue() != null) {
                setList(search.find(
                        new ProductSearchRequest(
                                contactReference,
                                contactReferenceCustomer,
                                Constants.SEARCH_BY_ID,
                                getValue(),
                                false,
                                isIncludeEol(),
                                blankSearchProductFetchSize),
                        productSelectionConfig));
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("searchNames() done [found=" + getList().size() + "]");
        }
    }

    // helpers

    public Product getProduct() {
        return (Product) getBean();
    }

    protected final ProductSearch getProductSearch() {
        return (ProductSearch) getService(ProductSearch.class.getName());
    }

    @Override
    protected String getSelectPropertyName() {
        return "productId";
    }

    protected void deployValues(Form form) {
        setStartsWithSet(form.getBoolean(Form.STARTS_WITH));
        value = form.getString(Form.VALUE);
        valueByNumber = false;
        if (value == null) {
            String numberValue = form.getString(Form.NUMBER);
            if (numberValue != null) {
                value = numberValue;
                valueByNumber = true;
            }
        }
        if (!productSelectionConfigsMode) {
            setIncludeEol(form.getBoolean(Form.INCLUDE_EOL));
            noClassification = form.getBoolean(Form.NO_CLASSIFICATION);
            showAllProducts = form.getBoolean(Form.SHOW_ALL);
            if (showAllProducts) {
                value = null;
            }
            if (!noClassification) {
                // GROUPS, TYPES, CATEGORIES
                updateClassification(form);
            }
        } else {
            showAllProducts = false;
        }
        if (form.getString(Form.METHOD) != null) {
            selectMethod(form.getString(Form.METHOD));
        }
        String searchType = form.getString(Form.TYPE);
        if (searchType == null) {
            //product search is called with methods like 'byName', 'byDescription', etc
            searchType = getSelectedMethod();
        }
        columnKey = fetchColumnKey(searchType);

        if (log.isDebugEnabled()) {
            log.debug("deployValues() done [selectionMode=" + productSelectionConfigsMode
                    + ", columnKey=" + columnKey
                    + ", value=" + value
                    + ", showAll=" + showAllProducts
                    + "]");
        }
    }

    protected String fetchColumnKey(String searchType) {
        if (isSet(searchType)) {
            if (searchType.equals(Constants.SEARCH_BY_NAME)) {
                return Constants.SEARCH_BY_NAME;
            } else if (searchType.equals(Constants.SEARCH_BY_DESCRIPTION)) {
                return Constants.SEARCH_BY_DESCRIPTION;
            } else if (searchType.equals(Constants.SEARCH_BY_MATCHCODE)) {
                return Constants.SEARCH_BY_MATCHCODE;
            } else if (searchType.equals(Constants.SEARCH_BY_PRODUCT_ID)) {
                return Constants.SEARCH_BY_ID;
            }
        }
        return null;
    }

    protected final Double[] getDouble(Form form, String key) {
        String[] s = form.getIndexedStrings(key);
        if (s != null) {
            Double[] result = new Double[s.length];
            for (int i = 0, j = s.length; i < j; i++) {
                result[i] = NumberUtil.createDouble(s[i]);
            }
            return result;
        }
        return null;
    }

    protected final BigDecimal[] getDecimal(Form form, String key) {
        String[] s = form.getIndexedStrings(key);
        if (s != null) {
            BigDecimal[] result = new BigDecimal[s.length];
            for (int i = 0, j = s.length; i < j; i++) {
                result[i] = NumberUtil.createDecimal(s[i]);
            }
            return result;
        }
        return null;
    }

    protected final Product[] getSelection(Long[] ids) {
        ProductSearch search = getProductSearch();
        Product[] result = new Product[ids.length];
        for (int i = 0, j = ids.length; i < j; i++) {
            result[i] = search.find(ids[i]);
        }
        return result;
    }

    private Long lookupId() {
        if (isShowAllProducts()) {
            return null;
        }
        try {
            return Long.valueOf(getValue());
        } catch (Exception ignorable) {
            // we're not interested in this
            return null;
        }
    }

    public Long getExistingProduct() {
        return existingProduct;
    }

    protected void setExistingProduct(Long existingProduct) {
        this.existingProduct = existingProduct;
    }

    public Double getExistingQuantity() {
        return existingQuantity;
    }

    protected void setExistingQuantity(Double existingQuantity) {
        this.existingQuantity = existingQuantity;
    }

    public BigDecimal getExistingPrice() {
        return existingPrice;
    }

    protected void setExistingPrice(BigDecimal existingPrice) {
        this.existingPrice = existingPrice;
    }

    public Double getExistingTaxRate() {
        return existingTaxRate;
    }

    protected void setExistingTaxRate(Double existingTaxRate) {
        this.existingTaxRate = existingTaxRate;
    }

    public Double getExistingReducedTaxRate() {
        return existingReducedTaxRate;
	}

	protected void setExistingReducedTaxRate(Double existingReducedTaxRate) {
        this.existingReducedTaxRate = existingReducedTaxRate;
	}

	protected int getBlankSearchProductFetchSize() {
        return blankSearchProductFetchSize;
    }

    protected void setBlankSearchProductFetchSize(int blankSearchProductFetchSize) {
        this.blankSearchProductFetchSize = blankSearchProductFetchSize;
    }

    @Override
    protected void setList(List list) {
        if (employeeProductFilter == null) {
            super.setList(list);
        } else {
            super.setList(employeeProductFilter.filter(list));
        }
    }

    @Override
    public void sortList(String method) {
        setListSortMethod(method);
        comparator.sort(method, getList());
    }
}
