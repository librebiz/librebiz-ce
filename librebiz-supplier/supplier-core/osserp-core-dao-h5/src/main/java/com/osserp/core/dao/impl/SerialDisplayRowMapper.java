/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 1, 2007 9:14:02 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.core.products.SerialDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SerialDisplayRowMapper implements RowMapper {

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new SerialDisplayRowMapper());
    }

    /*
     * supported type create view v_product_serials as select r.id, i.product_id, get_product_name(i.product_id) as name, s.id as serial_id, s.serial_number,
     * s.created, s.created_by, true as out from sales_delivery_notes r, sales_delivery_note_items i, sales_delivery_note_serials s where r.id = i.reference_id
     * and i.id = s.reference_id union select r.id, i.product_id, get_product_name(i.product_id) as name, s.id as serial_id, s.serial_number, s.created,
     * s.created_by, false as out from purchase_delivery_notes r, purchase_delivery_note_items i, purchase_delivery_note_serials s where r.id = i.reference_id
     * and i.id = s.reference_id order by created;
     */
    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new SerialDisplay(
                Long.valueOf(rs.getLong(1)),
                Long.valueOf(rs.getLong(2)),
                rs.getString(3),
                Long.valueOf(rs.getLong(4)),
                rs.getString(5),
                rs.getTimestamp(6),
                Long.valueOf(rs.getLong(7)),
                rs.getBoolean(8));
    }

}
