/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 28.04.2011 11:41:27 
 * 
 */
package com.osserp.gui.admin.calculations

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.servlet.http.HttpServletRequest

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.AdminViewController
import com.osserp.groovy.web.ViewController

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/admin/calculations/calculationConfig/*")
@Controller class CalculationConfigController extends AdminViewController {
    private static Logger logger = LoggerFactory.getLogger(CalculationConfigController.class.getName())

    @RequestMapping
    def deleteGroup(HttpServletRequest request) {
        logger.debug("deleteGroup: invoked [user=${getUserId(request)}]")
        CalculationConfigView view = getView(request)
        view.deleteGroup()
        return defaultPage
    }

    @RequestMapping
    def addSelection(HttpServletRequest request) {
        logger.debug("addSelection: invoked [user=${getUserId(request)}]")
        CalculationConfigView view = getView(request)
        view.addSelection()
        return defaultPage
    }

    @RequestMapping
    def removeSelection(HttpServletRequest request) {
        logger.debug("removeSelection: invoked [user=${getUserId(request)}]")
        CalculationConfigView view = getView(request)
        view.removeSelection()
        return defaultPage
    }
}
