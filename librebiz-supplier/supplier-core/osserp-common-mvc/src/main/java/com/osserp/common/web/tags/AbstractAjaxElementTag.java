/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 5, 2008 2:13:15 PM 
 * 
 */
package com.osserp.common.web.tags;

/**
 * 
 * Provides a common tag class for Ajax-Elements.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractAjaxElementTag extends AbstractClassicValueTag {
    protected static final String POPUP_SUFFIX = "_popup";
    private String url = null;
    private String targetElement = null;
    private String activityElement = null;
    private String preRequest = null;
    private String postRequest = null;
    private String onSuccess = null;

    /**
     * The url for the ajax request
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the url for the ajax request
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Provides the id of the target element where the response should be rendered in.
     * @return targetElement
     */
    public String getTargetElement() {
        return targetElement;
    }

    /**
     * Sets the id of the target element where the response should be rendered in.
     * @param targetElement id
     */
    public void setTargetElement(String targetElement) {
        this.targetElement = targetElement;
    }

    /**
     * Provides the optional id of an element where the activity symbol should be rendered in.
     * @return activityElement
     */
    public String getActivityElement() {
        return activityElement;
    }

    /**
     * Sets the optional id of an element where the activity symbol should be rendered in.
     * @param activityElement
     */
    public void setActivityElement(String activityElement) {
        this.activityElement = activityElement;
    }

    /**
     * Provides the code that should be appennded BEFORE the ajax request function.
     * @return preRequest
     */
    public String getPreRequest() {
        return preRequest;
    }

    /**
     * Sets the code that should be appennded BEFORE the ajax request function.
     * @param preRequest
     */
    public void setPreRequest(String preRequest) {
        this.preRequest = preRequest;
    }

    /**
     * Provides the code that should be appennded AFTER the ajax request function.
     * @return postRequest
     */
    public String getPostRequest() {
        return postRequest;
    }

    /**
     * Sets the code that should be appennded AFTER the ajax request function.
     * @param postRequest
     */
    public void setPostRequest(String postRequest) {
        this.postRequest = postRequest;
    }

    /**
     * Provides the code that should be executed AFTER the ajax request is DONE.
     * @return onSuccess
     */
    public String getOnSuccess() {
        return onSuccess;
    }

    /**
     * Sets the code that should be executed AFTER the ajax request is DONE.
     * @param onSuccess
     */
    public void setOnSuccess(String onSuccess) {
        this.onSuccess = onSuccess;
    }

    /**
     * Returns true if the "targetElement" is a popup
     * @return popup
     */
    public boolean isPopup() {
        if (getTargetElement() != null
                && getTargetElement().indexOf(POPUP_SUFFIX) > -1) {
            return true;
        }
        return false;
    }

    @Override
    public void release() {
        url = null;
        targetElement = null;
        activityElement = null;
    }

    protected String createUrl() {
        return createUrl(getUrl());
    }
}
