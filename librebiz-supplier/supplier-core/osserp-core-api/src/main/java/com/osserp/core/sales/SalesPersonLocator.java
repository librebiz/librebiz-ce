/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 20, 2011 11:30:58 AM 
 * 
 */
package com.osserp.core.sales;

import java.util.List;

import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesPersonLocator {

    static final String EXISTING_SALES_OF_ACTIVE_EMPLOYEE = "existingSalesOfActiveEmployee";
    static final String EXISTING_SALES_OF_ACTIVE_EMPLOYEE_VIA_AGENT = "existingSalesOfActiveEmployeeViaAgent";
    static final String EXISTING_SALES_OF_ACTIVE_EMPLOYEE_VIA_AGENT_AND_TIP = "existingSalesOfActiveEmployeeViaAgentAndTip";
    static final String EXISTING_SALES_OF_ACTIVE_EMPLOYEE_VIA_TIP = "existingSalesOfActiveEmployeeViaTip";
    static final String EXISTING_SALES_OF_ACTIVE_EMPLOYEES = "existingSalesOfActiveEmployees";
    static final String EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_AGENT = "existingSalesOfActiveEmployeesViaAgent";
    static final String EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_AGENT_AND_TIP = "existingSalesOfActiveEmployeesViaAgentAndTip";
    static final String EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_TIP = "existingSalesOfActiveEmployeesViaTip";

    static final String REQUEST_TYPE_WITH_MANUAL_SELECTION = "requestTypeWithManualSelection";

    static final String REQUEST_WITH_DEACTIVATED_SALES_REGION = "requestWithDeactivatedSalesRegion";
    static final String REQUEST_WITH_SALES_REGION_OF_DEACTIVATED_SALES = "requestWithSalesRegionOfDeactivatedSales";
    static final String REQUEST_WITH_SALES_REGION_WITHOUT_SALES = "requestWithSalesRegionWithoutSales";
    static final String REQUEST_WITH_SALES_REGION_SETTING_SALES = "requestWithSalesRegionSettingSales";
    static final String REQUEST_WITHOUT_SALES_REGION_ASSIGNED = "requestWithoutSalesRegionAssigned";

    static final String SALES_PERSON_WITHOUT_SALES_REGION = "salesPersonWithoutSalesRegion";

    /**
     * Provides the responsible sales employee by sales request
     * @param user
     * @param request
     * @return sales employee
     */
    SalesPersonLocatorResult find(DomainUser user, Request request);

    /**
     * Provides recipients for request created event infos
     * @param request
     * @return recipients
     */
    List<Employee> findRecipients(Request request);
}
