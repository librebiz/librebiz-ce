/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Option
import com.osserp.common.PermissionException

import com.osserp.core.Options
import com.osserp.core.telephone.TelephoneManager
import com.osserp.core.telephone.TelephoneSystem
import com.osserp.core.telephone.TelephoneSystemManager

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TelephoneView.class.getName())

    private static final String TELEPHONE_EDIT_PERMISSIONS = 'telephone_system_admin'
    String macAddress
    Long type
    List<Option> types
    Long telephoneSystem
    List<TelephoneSystem> telephoneSystems

    TelephoneView() {
        super()
        providesCreateMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            TelephoneSystemManager manager = getService(TelephoneSystemManager.class.getName())
            telephoneSystems = manager.getAll()
            types = getOptions(Options.TELEPHONE_TYPES)
            reload()
        }
    }

    @Override
    MenuItem getCreateLink() {
        return new MenuItem(link: "/telephones/telephoneCreate/forward", title: 'createNewTelephone', icon: 'telephoneAddIcon', newWindow: false, ajaxPopup: true, ajaxPopupName: 'telephoneCreateView', permissions: TELEPHONE_EDIT_PERMISSIONS, permissionInfo: 'permissionTelephoneSystemAdmin')
    }

    @Override
    void reload() {
        TelephoneManager manager = getService(TelephoneManager.class.getName())
        list = manager.find(macAddress, telephoneSystem, type)
        if (actualSortKey != null) {
            String[] keys = actualSortKey.split("\\.")
            def key = keys[0]
            if (keys.length > 1) {
                def childKey = keys[1]
                list.sort{ a, b -> a?."$key"?."$childKey" <=> b?."$key"?."$childKey" }
            } else {
                list.sort{ a, b -> a?."$key" <=> b?."$key" }
            }
        }
    }

    void search() throws PermissionException {
        logger.debug('search() invoked...')
        this.checkPermission(TELEPHONE_EDIT_PERMISSIONS)
        macAddress = form.getString('macAddress')
        if (macAddress) {
            macAddress = macAddress.toUpperCase().replaceAll(':', '').replaceAll('-', '')
        }
        telephoneSystem = form.getLong('system')
        type = form.getLong('type')
        TelephoneManager manager = getService(TelephoneManager.class.getName())
        logger.debug("search() [macAddress=${macAddress}, telephoneSystem=${telephoneSystem}, type=${type}]")
        list = manager.find(macAddress, telephoneSystem, type)
    }

    void delete() throws PermissionException {
        this.checkPermission(TELEPHONE_EDIT_PERMISSIONS)
        Long id = form.getLong('id')

        TelephoneManager manager = getService(TelephoneManager.class.getName())
        manager.delete(id)
        reload()
    }
}
