/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2013 
 * 
 */
package com.osserp.core.system;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface NocClient extends Option {

    /**
     * Indicates if noc client is default (e.g. noc client of system company)
     * @return true if client is default
     */
    boolean isDefaultClient();

    /**
     * Indicates if noc client is configured via directory
     * @return true if directory enabled
     */
    boolean isDirectorySupport();

    /**
     * Sets if noc client is configured via directory
     * @param directorySupport
     */
    void setDirectorySupport(boolean directorySupport);

    /**
     * Indicates if a groupware is available
     * @return true if available
     */
    boolean isGroupwareSupport();

    /**
     * Sets if groupware is available
     * @param groupwareSupport
     */
    void setGroupwareSupport(boolean groupwareSupport);

    /**
     * Provides the logical name of the groupware
     * @return groupware name
     */
    String getGroupwareName();

    /**
     * Sets the groupware name if available
     * @param groupwareName
     */
    void setGroupwareName(String groupwareName);

    /**
     * Indicates if groupware is installed on the same host
     * @return true if local
     */
    boolean isGroupwareLocal();

    /**
     * Sets if groupware is installed on the same host
     * @param groupwareLocal
     */
    void setGroupwareLocal(boolean groupwareLocal);

    /**
     * Indicates if managed mail domains enabled
     * @return true if maildomains supported
     */
    boolean isMaildomainSupport();

    /**
     * Enables/disables maildomain support
     * @param maildomainSupport
     */
    void setMaildomainSupport(boolean maildomainSupport);

    /**
     * Indicates if maildomains are managed on the same host
     * @return true if local
     */
    boolean isMaildomainLocal();

    /**
     * Enables/disables maildomain local
     * @param maildomainLocal
     */
    void setMaildomainLocal(boolean maildomainLocal);

}
