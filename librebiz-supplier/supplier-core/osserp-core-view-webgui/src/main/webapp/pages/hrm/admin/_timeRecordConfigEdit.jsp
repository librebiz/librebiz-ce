<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<o:form id="timeRecordForm" name="timeRecordForm" url="/timeRecordConfig.do">
    <input type="hidden" name="method" value="save" />

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><o:out value="${type.id}"/><span> - </span><o:out value="${type.name}"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="name" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="<o:out value="${type.name}"/>"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="description" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <textarea name="description" class="form-control" rows="4"><o:out value="${type.description}" /></textarea>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="interval" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.interval}">
                                        <label for="interval">
                                            <input type="checkbox" onchange="toggleInterval(this);" name="interval" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="interval">
                                            <input type="checkbox" onchange="toggleInterval(this);" name="interval"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="intervalPeriod" <c:if test="${!type.interval}">style="display:none;"</c:if>>
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="intervalPeriod" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.intervalPeriod}">
                                        <label for="interval">
                                            <input type="checkbox" name="intervalPeriod" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="interval">
                                            <input type="checkbox" name="intervalPeriod"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="lengthByProfileLabel"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.intervalHoursByConfig}">
                                        <label for="interval">
                                            <input type="checkbox" onchange="toggleIntervalLength(this);" name="intervalHoursByConfig" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="interval">
                                            <input type="checkbox" onchange="toggleIntervalLength(this);" name="intervalHoursByConfig"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="intervalLength" <c:if test="${type.intervalHoursByConfig}">style="display:none;"</c:if>>
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="intervalLength"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" class="form-control" name="intervalHours" value="<o:out value="${type.intervalHours}"/>"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" class="form-control" name="intervalMinutes" value="<o:out value="${type.intervalMinutes}"/>"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <fmt:message key="hourShort"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="intervalStart"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" class="form-control" name="intervalStartHours" value="<o:out value="${type.intervalStartHours}"/>"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" class="form-control" name="intervalStartMinutes" value="<o:out value="${type.intervalStartMinutes}"/>"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <fmt:message key="clock"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="needApproval"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.needApproval}">
                                        <label for="needApproval">
                                            <input type="checkbox" name="needApproval" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="needApproval">
                                            <input type="checkbox" name="needApproval"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="manuallyInput"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.manualInputRequired}">
                                        <label for="manuallyInput">
                                            <input type="checkbox" name="manualInputRequired" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="manuallyInput">
                                            <input type="checkbox" name="manualInputRequired"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="ignoreBreakRules"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.ignoreBreakRules}">
                                        <label for="ignoreBreakRules">
                                            <input type="checkbox" name="ignoreBreakRules" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="ignoreBreakRules">
                                            <input type="checkbox" name="ignoreBreakRules"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="visibleBooking"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.visible}">
                                        <label for="visibleBooking">
                                            <input type="checkbox" name="visible" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="visibleBooking">
                                            <input type="checkbox" name="visible"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bookable"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.bookable}">
                                        <label for="bookable">
                                            <input type="checkbox" name="bookable" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="bookable">
                                            <input type="checkbox" name="bookable"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bookingPast"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.bookingPast}">
                                        <label for="bookingPast">
                                            <input type="checkbox" name="bookingPast" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="bookingPast">
                                            <input type="checkbox" name="bookingPast"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bookingFuture"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.bookingFuture}">
                                        <label for="bookingFuture">
                                            <input type="checkbox" name="bookingFuture" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="bookingFuture">
                                            <input type="checkbox" name="bookingFuture"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="illnessLabel"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.illness}">
                                        <label for="illness">
                                            <input type="checkbox" name="illness" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="illness">
                                            <input type="checkbox" name="illness"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="leave"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.leave}">
                                        <label for="leave">
                                            <input type="checkbox" name="leave" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="illness">
                                            <input type="checkbox" name="leave"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="unpaidLeave"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.unpaidLeave}">
                                        <label for="unpaidLeave">
                                            <input type="checkbox" name="unpaidLeave" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="unpaidLeave">
                                            <input type="checkbox" name="unpaidLeave"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="specialLeave"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.specialLeave}">
                                        <label for="specialLeave">
                                            <input type="checkbox" name="specialLeave" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="specialLeave">
                                            <input type="checkbox" name="specialLeave"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="subtractionBooking"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.subtraction}">
                                        <label for="subtractionBooking">
                                            <input type="checkbox" name="subtraction" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="subtractionBooking">
                                            <input type="checkbox" name="subtraction"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="additionBooking"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <c:choose>
                                    <c:when test="${type.addition}">
                                        <label for="additionBooking">
                                            <input type="checkbox" name="addition" checked="checked"/>
                                        </label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="additionBooking">
                                            <input type="checkbox" name="addition"/>
                                        </label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/timeRecordConfig.do?method=disableEdit"/>');" value="<fmt:message key="break"/>" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</o:form>
