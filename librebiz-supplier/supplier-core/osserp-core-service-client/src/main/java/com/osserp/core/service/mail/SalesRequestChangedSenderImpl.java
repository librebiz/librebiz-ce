/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 2, 2007 12:11:29 PM 
 * 
 */
package com.osserp.core.service.mail;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.DateFormatter;
import com.osserp.core.dao.MailTemplates;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;
import com.osserp.core.service.requests.SalesRequestChangedProcessor;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRequestChangedSenderImpl extends AbstractMailTemplateMailSender
        implements SalesRequestChangedProcessor {
    private static Logger log = LoggerFactory.getLogger(SalesRequestChangedSenderImpl.class.getName());

    protected SalesRequestChangedSenderImpl(MailTemplates mailTemplates, String templateName) {
        super(mailTemplates, templateName);
    }

    public void execute(Employee lastSales, Employee newSales, Request request) {
        String recipient = (newSales == null ? null : newSales.getEmail());
        if (newSales != null && recipient != null) {
            Map<String, Object> textValues = new HashMap<String, Object>();
            if (lastSales != null) {
                textValues.put("lastSales", lastSales.getDisplayName());
                String email = lastSales.getEmail();
                if (isSet(email)) {
                    textValues.put("lastSalesEmail", email);
                }
            }
            textValues.put("currentSales", newSales.getSalutationDisplay());
            textValues.put("requestId", request.getRequestId());
            textValues.put("requestName", request.getName());
            textValues.put("requestFrom", DateFormatter.getDate(request.getCreated()));
            if (request.getCustomer().getSalutation() != null) {
                textValues.put("requestCustomerTitle", request.getCustomer().getSalutation().getName());
            }
            textValues.put("requestCustomerDisplayName", request.getCustomer().getDisplayName());
            textValues.put("requestCustomerAddressStreet", request.getCustomer().getAddress().getStreet());
            textValues.put("requestCustomerAddressZipcode", request.getCustomer().getAddress().getZipcode());
            textValues.put("requestCustomerAddressCity", request.getCustomer().getAddress().getCity());
            textValues.put("requestCustomerEmail", request.getCustomer().getEmail());
            textValues.put("requestCustomerMobile", request.getCustomer().getMobile().getFormattedNumber());
            textValues.put("requestCustomerPhone", request.getCustomer().getPhone().getFormattedNumber());
            textValues.put("requestDeliveryName", (request.getAddress().getName() != null) ? 
                    request.getAddress().getName() : request.getCustomer().getDisplayName());
            textValues.put("requestDeliveryAddressStreet", request.getAddress().getStreet());
            textValues.put("requestDeliveryAddressZipcode", request.getAddress().getZipcode());
            textValues.put("requestDeliveryAddressCity", request.getAddress().getCity());
            send(recipient, textValues);
        } else {
            log.warn("execute() found employee without mail address [id=" + 
                    (newSales == null ? "null" : newSales.getId()) + "]");
        }
    }
}
