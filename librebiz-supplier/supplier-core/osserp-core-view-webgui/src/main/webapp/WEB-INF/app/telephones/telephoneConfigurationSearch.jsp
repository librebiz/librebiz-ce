<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="telephoneConfigurationSearchView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="javascript" type="string">
		<script type="text/javascript">
			function checkSearchException() {
				try {
					/*if ($('onlyTelephoneExchanges') && $('includeEmployeeTelephoneSystems')) {
						if ($('onlyTelephoneExchanges').checked == 'checked') {
							$('includeEmployeeTelephoneSystems').disabled = true;
						} else {
							$('includeEmployeeTelephoneSystems').disabled = false;
						}
					}*/
					if ($('ajaxContent')) {
						$('ajaxContent').innerHTML = "<div style='width: 100%; text-align: center;'><div style='text-align:center; border:1px solid #D6DDE6; padding:10px;'><img src='/osdb/img/ajax_loader.gif' style='margin:5px;'/><br/>"+ ojsI18n.loading+"</div></div>";
					}
					return true;
				} catch (e) {
					return true;
				}
			}
		</script>
	</tiles:put>
	<tiles:put name="headline"><fmt:message key="telephoneConfiguration"/></tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
		<script src="<c:url value="/js/wz_tooltip.js"/>" type="text/javascript"></script>
		<c:if test="${!empty sessionScope.error}">
			&nbsp;
			<div class="errormessage">
				<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
			</div>
            <o:removeErrors/>
		</c:if>
		<div class="spacer"></div>
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="contentBox">
					<div class="contentBoxData">
						<div class="spacer"></div>
						<o:ajaxLookupForm name="dynamicForm" url="/app/telephones/telephoneConfigurationSearch/search" targetElement="ajaxContent" events="onkeyup" preRequest="if (checkSearchException()) {" postRequest="}">
							<div class="subcolumns">
								<div class="column33l">
									<div class="subcolumnl">
										<c:choose>
											<c:when test="${empty view.telephoneSystem}"><c:set var="telephoneSystemId" value="0"/></c:when>
											<c:otherwise><c:set var="telephoneSystemId" value="${view.telephoneSystem.id}"/></c:otherwise>
										</c:choose>
										<select <c:if test="${view.onlyTelephoneExchanges}">disabled</c:if> id="id" name="id" style="width: 210px;" onChange="javascript: $('dynamicForm').onkeyup();">
											<option value="0" <c:if test="${telephoneSystemId == '0'}">selected="selected"</c:if>><fmt:message key="allTelephoneConfigurations"/></option>
											<c:forEach var="obj" items="${view.telephoneSystems}">
												<option value="${obj.id}" <c:if test="${obj.id == telephoneSystemId}">selected="selected"</c:if>><o:out value="${obj.name}"/></option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="column66r" style="padding-top:4px;">
									<div class="subcolumnr">
										<c:choose>
											<c:when test="${view.includeEmployeeTelephoneSystems}">
												<input <c:if test="${view.onlyTelephoneExchanges}">disabled</c:if> type="checkbox" id="includeEmployeeTelephoneSystems" name="includeEmployeeTelephoneSystems" checked="checked" onchange="javascript: $('dynamicForm').onkeyup();" />
											</c:when>
											<c:otherwise>
												<input <c:if test="${view.onlyTelephoneExchanges}">disabled</c:if> type="checkbox" id="includeEmployeeTelephoneSystems" name="includeEmployeeTelephoneSystems" onchange="javascript: $('dynamicForm').onkeyup();" />
											</c:otherwise>
										</c:choose>
										<label for="includeEmployeeTelephoneSystems" style="margin-right: 5px; height: 25px; vertical-align: middle; padding-bottom: 8px;"><fmt:message key="ofEmployeesToo"/></label>
									</div>
								</div>
								<!--div class="column33r" style="padding-top:4px;">
									<div class="subcolumnr">
										<c:choose>
											<c:when test="${view.onlyTelephoneExchanges}">
												<input type="checkbox" id="onlyTelephoneExchanges" name="onlyTelephoneExchanges" checked="checked" onchange="javascript: $('dynamicForm').onkeyup();" />
											</c:when>
											<c:otherwise>
												<input type="checkbox" id="onlyTelephoneExchanges" name="onlyTelephoneExchanges" onchange="javascript: $('dynamicForm').onkeyup();" />
											</c:otherwise>
										</c:choose>
										<label for="active" style="margin-right: 5px; height: 25px; vertical-align: middle; padding-bottom: 8px;"><fmt:message key="onlyTelephoneExchanges"/></label>
									</div>
								</div-->
							</div>
						</o:ajaxLookupForm>
						<div class="spacer"></div>
					</div>
				</div>
				<div class="spacer"></div>
				<div id="ajaxDiv">
					<div id="ajaxContent">
						<c:import url="_telephoneConfigurationSearch.jsp"/>
					</div>
				</div>
			</div>
		</div>
	</tiles:put>
</tiles:insert>