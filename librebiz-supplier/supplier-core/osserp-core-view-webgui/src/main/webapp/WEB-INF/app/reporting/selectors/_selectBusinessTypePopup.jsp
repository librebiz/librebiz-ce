<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.selectBusinessTypePopupView}"/>

<c:if test="${!empty view}">
	<div class="modalBoxHeader">
		<div class="modalBoxHeaderLeft">
			<fmt:message key="${view.headerName}"/>
		</div>
		<div class="modalBoxHeaderRight">
			<div class="boxnav">
				<ul>
					<c:forEach var="nav" items="${view.navigation}">
						<li>
							<v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxLink>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
	<div class="modalBoxData">
		<div class="subcolumns">
			<div class="subcolumn">
				<c:if test="${!empty sessionScope.error}">
					<div class="spacer"></div>
					<div class="errormessage">
						<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
					</div>
                    <o:removeErrors/>
				</c:if>
				<div class="spacer"></div>
				<v:ajaxForm name="dynamicForm" targetElement="${view.name}_popup" url="${view.saveLink.link}">
					<table class="valueTable input">
						<tbody>
							<tr>
								<td class="name">
									<select name="businessType" size="1" onchange="$('dynamicForm').onsubmit();">
										<option value=""><fmt:message key="promptSelect"/> / <fmt:message key="reset"/></option>
										<c:forEach var="obj" items="${view.list}" varStatus="s">
											<c:choose>
												<c:when test="${!empty view.bean and view.bean.id == obj.id}">
													<option value="<o:out value="${obj.id}"/>" selected="selected"><o:out value="${obj.name}"/></option>
												</c:when>
												<c:otherwise>
													<option value="<o:out value="${obj.id}"/>"><o:out value="${obj.name}"/></option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</v:ajaxForm>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</c:if>