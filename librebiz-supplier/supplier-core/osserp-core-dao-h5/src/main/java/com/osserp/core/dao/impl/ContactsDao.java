/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 5:34:54 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.common.dao.Tables;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactGroup;
import com.osserp.core.contacts.ContactListEntry;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Emails;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.mail.NewsletterRecipient;
import com.osserp.core.model.NewsletterRecipientVO;
import com.osserp.core.model.contacts.AbstractContactAware;
import com.osserp.core.model.contacts.ContactBackupImpl;
import com.osserp.core.model.contacts.ContactGroupImpl;
import com.osserp.core.model.contacts.ContactImpl;
import com.osserp.core.model.contacts.ContactTypeImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactsDao extends AbstractContactDao implements Contacts {
    private static Logger log = LoggerFactory.getLogger(ContactsDao.class.getName());

    public ContactsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            Emails emails) {
        super(jdbcTemplate, tables, sessionFactory, emails, namesCache);
    }

    public boolean exists(Long id) {
        StringBuilder sql = new StringBuilder(128);
        sql
                .append("SELECT count(*) FROM ")
                .append(contactsTable)
                .append(" WHERE contact_id = ")
                .append(id);
        return (jdbcTemplate.queryForObject(sql.toString(), Integer.class) > 0);
    }

    public Contact loadContact(Long contactId) {
        return (Contact) getCurrentSession().load(ContactImpl.class.getName(), contactId);
    }

    public Contact fetchContact(Long contactId) {
        try {
            return (Contact) getCurrentSession().load(ContactImpl.class, contactId);
        } catch (Exception e) {
            log.warn("fetchContact() ignoring exception [message" + e.getMessage() + "]");
            throw new BackendException(e);
        }
    }

    public Person findByExternalReference(String externalReference) {
        Long contactId = getIdByExternalReference(externalReference);
        return isEmpty(contactId) ? null : loadContact(contactId);
    }

    public Contact fetchByEmailAddress(String emailAddress) {
        List<ContactListEntry> list = findByEmailAddress(emailAddress);
        return list.isEmpty() ? null : loadContact(list.get(0).getId());
    }

    protected List<Contact> find(List<Long> ids) {
        List<Contact> result = new ArrayList<>();
        if (!ids.isEmpty()) {
            StringBuilder hql = new StringBuilder("from ");
            hql.append(ContactImpl.class.getName()).append(" o where o.contactId in (");
            hql.append(CollectionUtil.createEntityIds(ids)).append(")");
            result = getCurrentSession().createQuery(hql.toString()).list();
        }
        if (log.isDebugEnabled()) {
            log.debug("load() done [count=" + ids.size() + "]");
        }
        return result;
    }

    public void changeType(Long changedBy, Contact contactToChange, String company) throws ClientException {
        if (isEmpty(company)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (isCompanyExisting(company)) {
            throw new ClientException(ErrorCode.NAME_EXISTS);
        }
        ContactImpl person = new ContactImpl(
                changedBy,
                getContactType(Contact.TYPE_PERSON),
                contactToChange,
                contactToChange.getSalutation(),
                contactToChange.getTitle(),
                false,
                contactToChange.getFirstName(),
                contactToChange.getLastName(),
                null,
                null,
                null);
        save(person);

        Salutation companySalutation = getSalutation(Constants.COMPANY_SALUTATION);
        contactToChange.setType(getContactType(Contact.TYPE_BUSINESS));
        contactToChange.setSalutation(companySalutation);
        contactToChange.setTitle(null);
        contactToChange.setFirstName(null);
        contactToChange.setLastName(company);
        save(contactToChange);
    }

    private boolean isCompanyExisting(String company) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT count(contact_id) FROM contacts WHERE " +
                "lower(lastname) = lower(?)");
        return (jdbcTemplate.queryForObject(
                sql.toString(),
                new Object[] { company },
                new int[] { Types.VARCHAR }, 
                Integer.class) > 0);
    }

    public List<Option> findExistingEmailContacts(String email) {
        return emails.findEqual(email);
    }

    public void save(Contact contact) {
        log.debug("save(conact): invocation: " + ((contact != null) ? (contact.getContactId()
                + ", class=" + contact.getClass().getName()) : "null"));
        save(contact, true);
    }

    protected final void save(Contact contact, boolean createBackup) {
        Contact obj = (contact instanceof AbstractContactAware ? ((AbstractContactAware) contact).getRc() : contact);
        if (createBackup && exists(obj.getContactId())) {
            try {
                ContactBackupImpl backup = new ContactBackupImpl(obj.getPersonChangedBy(), loadContact(obj.getContactId()));
                if (backup.isChanged(contact)) {
                    getCurrentSession().saveOrUpdate(ContactBackupImpl.class.getName(), backup);
                    log.debug("save: createBackup done [contactId=" + contact.getContactId()
                            + ", backup=" + backup.getId() + "]");
                }
            } catch (Throwable t) {
                // should never happen
                log.debug("save: failed to create backup: " + t.getMessage(), t);
            }
        }
        try {
            getCurrentSession().merge(ContactImpl.class.getName(), obj);
        } catch (Exception e) {
            log.error("save: failed: " + e.getMessage(), e);
            throw e;
        }
    }

    public void activateRelation(Contact contact, Long contactGroup) {
        if (contact instanceof ContactImpl) {
            ContactImpl impl = (ContactImpl) contact;
            if (Contact.CUSTOMER.equals(contactGroup)) {
                impl.setCustomer(true);
                impl.setOther(false);
            } else if (Contact.EMPLOYEE.equals(contactGroup)) {
                impl.setEmployee(true);
                impl.setOther(false);
            } else if (Contact.SUPPLIER.equals(contactGroup)) {
                impl.setSupplier(true);
                impl.setOther(false);
            } else if (Contact.CLIENT.equals(contactGroup)) {
                impl.setClient(true);
                impl.setOther(false);
            } else {
                log.warn("findAllByGroup() invoked with illegal group " + contactGroup);
            }
            save(impl, false);
        }
    }

    public List<Contact> findAllByGroup(Long contactGroup) {
        StringBuilder query = new StringBuilder(256);
        query
            .append("SELECT c.contact_id FROM ")
            .append(contactsTable)
            .append(" c WHERE c.status_id > -10 AND c.contact_id IN (SELECT contact_id FROM ");
        if (Contact.CUSTOMER.equals(contactGroup)) {
            query
                    .append(getTable(TableKeys.CUSTOMERS))
                    .append(" WHERE id IN (SELECT customer_id FROM ")
                    .append(getTable(TableKeys.PLANS))
                    .append(" WHERE status_id > 0 AND status_id < 100)")
                    .append(" OR id IN (SELECT customer_id FROM ")
                    .append(getTable(TableKeys.PROJECTS))
                    .append(" WHERE cancelled = false))");
        } else if (Contact.EMPLOYEE.equals(contactGroup)) {
            query
                    .append(getTable(TableKeys.EMPLOYEES))
                    .append(" WHERE isactive = true) ORDER BY lastname");
        } else if (Contact.SUPPLIER.equals(contactGroup)) {
            query
                    .append(getTable(TableKeys.SUPPLIERS))
                    .append(") ORDER BY lastname");
        } else {
            log.warn("findAllByGroup() invoked with invalid group " + contactGroup);
            return new ArrayList<>();
        }
        return find((List<Long>) jdbcTemplate.query(query.toString(), getLongRowMapper()));
    }

    public List<ContactListEntry> findByEmailAddress(String emailAddress) {
        String address = isEmpty(emailAddress) ? null : emailAddress.toLowerCase();
        if (address == null) {
            return new ArrayList<>();
        }
        StringBuilder sql = ContactListRowMapper.getQuery(contactsTable);
        sql
                .append(" WHERE contact_id IN (SELECT contact_id FROM ")
                .append(getTable(TableKeys.EMAILS))
                .append(" WHERE email = ?)");
        return (List<ContactListEntry>) jdbcTemplate.query(
                sql.toString(),
                new Object[] { address },
                new int[] { Types.VARCHAR }, 
                ContactListRowMapper.getReader(namesCache));
    }
    
    public List<ContactListEntry> findByEmailPattern(String pattern) {
        StringBuilder sql = ContactListRowMapper.getQuery(contactsTable);
        sql
                .append(" WHERE contact_id IN (SELECT contact_id FROM ")
                .append(getTable(TableKeys.EMAILS))
                .append(" WHERE email ~* '^")
                .append(pattern)
                .append("') ORDER BY lastname");
        if (log.isDebugEnabled()) {
            log.debug("findByEmailPattern() invoked [sql=" + sql.toString() + "]");
        }
        return (List<ContactListEntry>) jdbcTemplate.query(
                sql.toString(),
                ContactListRowMapper.getReader(namesCache));
    }

    public List<ContactListEntry> findByZipcodePattern(String pattern, boolean startsWith) {
        StringBuilder sql = ContactListRowMapper.getQuery(contactsTable);
        sql.append(" WHERE status_id > -10 AND zipcode ~* '");
        if (startsWith) {
            sql.append("^");
        }
        sql.append(pattern).append("' ORDER BY lastname");
        return (List<ContactListEntry>) jdbcTemplate.query(
                sql.toString(),
                ContactListRowMapper.getReader(namesCache));
    }

    public List<ContactGroup> getContactGroups() {
        return getCurrentSession().createQuery("from " + ContactGroupImpl.class.getName() + " o order by o.name").list();
    }

    public ContactGroup getContactGroupById(Long contactGroupId) {
        return (ContactGroup) getCurrentSession().load(ContactGroupImpl.class, contactGroupId);
    }

    public List<ContactType> getContactTypes() {
        return getCurrentSession().createQuery("from " + ContactTypeImpl.class.getName() + " o order by o.name").list();
    }


    // CONTACT SEARCH	

    public List<Contact> findExisting(String zipcode, String lastName) {
        String zip = fetchInsecureInput(zipcode);
        String value = fetchInsecureInput(lastName);
        StringBuilder sql = ContactRowMapper.getQuery(contactsTable);
        sql
                .append(" WHERE status_id > -10 AND zipcode = '")
                .append(zip)
                .append("' AND lastname  ~* '^")
                .append(value)
                .append("'");
        String sqlQuery = sql.toString();
        if (log.isDebugEnabled()) {
            log.debug("findExisting() invoked [sql=" + sqlQuery + "]");
        }
        List<Contact> result = (List<Contact>) jdbcTemplate.query(sqlQuery, getContactReader());
        if (log.isDebugEnabled()) {
            log.debug("findExisting() done [count=" + result.size() + "]");
        }
        return result;
    }

    public List<Contact> findByEmail(String email) {
        if (log.isDebugEnabled()) {
            log.debug("findByEmail() invoked [value=" + email + "]");
        }
        List<Contact> result = new ArrayList<>();
        List<ContactListEntry> existing = findByEmailPattern(email);
        if (!existing.isEmpty()) {
            for (int i = 0, j = existing.size(); i < j; i++) {
                ContactListEntry next = existing.get(i);
                try {
                    result.add(loadContact(next.getId()));
                } catch (Throwable ignorable) {
                    log.warn("findByEmail() ignoring failure [id="
                            + next.getId()
                            + ", message=" + ignorable.toString()
                            + "]");
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("findByEmail() done [count=" + result.size() + "]");
        }
        return result;
    }

    // CONTACT PERSON SEARCH	

    public List<Contact> findContactPersons(Long contactId) {
        List<Contact> result = new ArrayList<>();
        if (contactId != null) {
            StringBuilder hql = new StringBuilder("from ");
            hql
                    .append(ContactImpl.class.getName())
                    .append(" o where o.reference = ")
                    .append(contactId)
                    .append(" and o.status > ")
                    .append(Contact.STATUS_DELETED);
            result = getCurrentSession().createQuery(hql.toString()).list();
        }
        return result;
    }

    public EmailAddress findBillingAddress(Contact contact) {
        EmailAddress result = fetchBillingAddress(contact);
        if (result != null) {
            return result;
        }
        List<Contact> persons = findContactPersons(contact.getContactId());
        for (int i = 0, j = persons.size(); i < j; i++) {
            Contact person = persons.get(i);
            result = fetchBillingAddress(person);
            if (result != null) {
                return result;
            }
        }
        result = fetchEmailAddress(contact);
        if (!persons.isEmpty() && (result == null || !result.isBusiness())) {
            for (int i = 0, j = persons.size(); i < j; i++) {
                Contact person = persons.get(i);
                EmailAddress next = fetchEmailAddress(person);
                if (next != null) {
                    if (next.isBusiness() && result != null && !result.isBusiness()) {
                        result = next;
                    } else if (next.isBusiness() && result == null) {
                        result = next;
                    }
                }
            }
        }
        return result;
    }

    private EmailAddress fetchBillingAddress(Contact contact) {
        List<EmailAddress> list = contact.getEmails();
        for (int i = 0, j = list.size(); i < j; i++) {
            EmailAddress next = list.get(i);
            if (EmailAddress.INVOICE.equals(next.getType())) {
                return next;
            }
        }
        return null;
    }

    private EmailAddress fetchEmailAddress(Contact contact) {
        EmailAddress primary = null;
        EmailAddress primaryBusiness = null;
        EmailAddress business = null;
        EmailAddress personal = null;
        List<EmailAddress> list = contact.getEmails();
        for (int i = 0, j = list.size(); i < j; i++) {
            EmailAddress next = list.get(i);
            if (next.isPrimary() && next.isBusiness()) {
                primaryBusiness = next;
            } else if (next.isPrimary()) {
                primary = next;
            } else if (next.isBusiness()) {
                business = next;
            } else {
                personal = next;
            }
        }
        boolean businessContact = (contact.isBusiness() || contact.isContactPerson());
        if (primaryBusiness != null && businessContact) {
            return primaryBusiness;
        }
        if (business != null && businessContact) {
            return business;
        }
        // private contact with primary or other address
        if (primary != null && !businessContact) {
            return primary;
        }
        if (personal != null && !businessContact) {
            return personal;
        }
        return null;
    }

    // NEWSLETTER RECIPIENT SEARCH	

    public List<NewsletterRecipient> findNewsletterRecipients(boolean active) {
        StringBuilder sql = new StringBuilder();
        sql
            .append("SELECT c.contact_id, c.lastname, c.firstname, c.zipcode, c.city, e.email, false FROM ")
            .append(contactsTable)
            .append(" c, ")
            .append(emailTable)
            .append(" e WHERE c.contact_id = e.contact_id AND c.status_id > -10 AND c.type_id <> 4")
            .append(" AND c.newsletter = ").append(active)
            .append(" AND e.isprimary = true UNION SELECT c.contact_id, c.lastname, c.firstname,")
            .append(" cr.zipcode, cr.city, e.email, true FROM ")
            .append(contactsTable)
            .append(" c, ")
            .append(contactsTable)
            .append(" cr, ")
            .append(emailTable)
            .append(" e WHERE c.contact_ref = cr.contact_id AND c.contact_id = e.contact_id AND c.status_id > -10") 
            .append(" AND c.newsletter = ").append(active)
            .append(" AND e.isprimary = true ORDER BY lastname");
        return (List<NewsletterRecipient>) jdbcTemplate.query(sql.toString(), getNewsletterRecipientReader());
    }

    private RowMapperResultSetExtractor getNewsletterRecipientReader() {
        return new RowMapperResultSetExtractor(new NewsletterRecipientRowMapper());
    }

    private class NewsletterRecipientRowMapper extends AbstractRowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            Long contactId = 1L;
            String name = rs.getString(2);
            if (name == null || name.length() < 1) {
                name = "illegal";
            }
            String first = rs.getString(3);
            if (first != null && first.length() > 1) {
                StringBuilder sb = new StringBuilder(name);
                sb.append(", ").append(first);
                name = sb.toString();
            }
            StringBuilder city = new StringBuilder();
            String zip = rs.getString(4);
            if (!isEmpty(zip)) {
                city.append(zip);
            }
            String c = rs.getString(5);
            if (!isEmpty(c)) {
                if (city.length() > 0) {
                    city.append(" ");
                }
                city.append(c);
            }
            return new NewsletterRecipientVO(contactId, name, city.toString(), rs.getString(6), rs.getBoolean(7));
        }
    }

    public void updateNewsletterStatus(Long[] contacts, boolean status) {
        if (contacts == null || contacts.length < 1) {
            log.warn("updateRecipients() attempt to execute method with no contacts to update");
            return;
        }
        StringBuilder sql = new StringBuilder();
        sql
                .append("UPDATE ")
                .append(contactsTable)
                .append(" SET newsletter = ")
                .append(status)
                .append(" WHERE contact_id = ?");
        
        String updateSql = sql.toString();
        final int[] types = { Types.BIGINT };
        int updated = 0;
        int cnt = contacts.length;
        for (int i = 0; i < cnt; i++) {
            final Object[] params = { contacts[i] };
            updated = updated + jdbcTemplate.update(updateSql, params, types);
        }
        if (log.isDebugEnabled()) {
            log.debug("updateRecipients() done [recipients=" + updated + ", status=" + status + "]");
        }
    }

    public void updateBranchFlag(Long id, boolean branch) {
        if (id == null) {
            log.warn("updateBranchFlag() attempt to execute method with no id");
            return;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(contactsTable).append(" SET branch = ").append(branch).append(" WHERE contact_id = ?");
        final Object[] params = { id };
        final int[] types = { Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }
}
