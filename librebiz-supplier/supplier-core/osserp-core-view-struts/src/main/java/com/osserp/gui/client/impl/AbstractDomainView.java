/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 18, 2008 11:39:05 AM 
 * 
 */
package com.osserp.gui.client.impl;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.PermissionError;
import com.osserp.common.User;
import com.osserp.common.UserManager;
import com.osserp.common.service.TemplateService;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.AbstractView;

import com.osserp.core.clients.ClientManager;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeManager;
import com.osserp.core.employees.EmployeeSearch;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.DomainUserManager;
import com.osserp.gui.client.DomainView;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public abstract class AbstractDomainView extends AbstractView implements DomainView {
    private static Logger log = LoggerFactory.getLogger(AbstractDomainView.class.getName());

    /**
     * Initializes view name, actionUrl and selectionUrl as annotated and initializes action target as 'success'
     */
    protected AbstractDomainView() {
        super();
    }

    public final boolean isUserSalesOnly() {
        DomainUser user = getDomainUser();
        return user.isSalesOnly();
    }

    public boolean isUserOfficeManagement() {
        DomainUser user = getDomainUser();
        return user.isOfficeManagement();
    }

    public boolean isUserPropertyEnabled(String propertyName) {
        DomainUser user = getDomainUser();
        return user.isPropertyEnabled(propertyName);
    }

    public boolean isUserOfficeManagementOnly() {
        DomainUser user = getDomainUser();
        if (user.isOfficeManagement()
                && (!user.isExecutive()
                        && !user.isSalesExecutive()
                        && !user.isSales()
                        && !user.isProjectManager()
                        && !user.isAccounting()
                        && !user.isCustomerService())) {
            return true;
        }
        return false;
    }

    /**
     * Indicates that current user is also employee and not from headquarter
     * @return true if user is domain user and not from hq
     */
    @Override
    public boolean isUserFromBranch() {
        Object u = getUser();
        if (u != null && u instanceof DomainUser) {
            DomainUser user = (DomainUser) u;
            return !user.isFromHeadquarter();
        }
        return false;
    }

    /**
     * Indicates that current user is an employee and branch executive
     * @return true if user is branch executive
     */
    @Override
    public boolean isBranchExecutive() {
        if (getUser() != null && getUser() instanceof DomainUser) {
            DomainUser user = getDomainUser();
            return user.isBranchExecutive();
        }
        return false;
    }

    @Override
    protected boolean isDomainView() {
        User user = getUser();
        return (user != null && user instanceof DomainUser);
    }

    protected Employee getDomainEmployee() {
        DomainUser domainUser = getDomainUser();
        Employee employee = domainUser.getEmployee();
        if (employee != null && employee.isActive()) {
            return employee;
        }
        throw new PermissionError();
    }

    protected DomainUser getDomainUser() {
        User user = getUser();
        if (user instanceof DomainUser) {
            return (DomainUser) user;
        }
        throw new PermissionError();
    }

    protected Employee getEmployee(Long id) {
        EmployeeSearch search = getEmployeeSearch();
        return (search == null ? null : search.findById(id));
    }

    protected EmployeeSearch getEmployeeSearch() {
        return (EmployeeSearch) getService(EmployeeSearch.class.getName());
    }

    protected EmployeeManager getEmployeeManager() {
        return (EmployeeManager) getService(EmployeeManager.class.getName());
    }

    protected ClientManager getClientManager() {
        return (ClientManager) getService(ClientManager.class.getName());
    }

    protected DomainUser getDomainUser(Long userId) {
        DomainUserManager userManager = getDomainUserManager();
        return (userManager == null ? null : (DomainUser) userManager.findById(userId));
    }

    protected DomainUserManager getDomainUserManager() {
        return (DomainUserManager) getService(UserManager.class.getName());
    }

    protected final SystemCompany getClientByCurrentUser() {
        SystemCompany result = null;
        Long companyId = getCurrentUserCompanyId();
        if (companyId != null) {
            result = getSystemConfigManager().getCompany(companyId);
        }
        if (result == null) {
            List<SystemCompany> companies = getSystemConfigManager().getActiveCompanies();
            for (int i = 0; i < companies.size(); i++) {
                SystemCompany comp = companies.get(i);
                if (comp.isHolding()) {
                    result = comp;
                    break;
                }
            }
            if (result == null && !companies.isEmpty()) {
                result = companies.get(0);
            }
        }
        return result;
    }
    
    private Long getCurrentUserCompanyId() {
        Long companyId = null;
        if (getDomainUser() != null && getDomainUser().getEmployee() != null 
                && getDomainUser().getEmployee().getDefaultRoleConfig() != null
                && getDomainUser().getEmployee().getDefaultRoleConfig().getBranch() != null
                && getDomainUser().getEmployee().getDefaultRoleConfig().getBranch().getCompany() != null) {
            companyId = getDomainUser().getEmployee().getDefaultRoleConfig().getBranch().getCompany().getId();
        }
        return companyId;
    }

    /**
     * Provides an option
     * @param key
     * @param id
     * @return option or null if not found or backend not available
     */
    @SuppressWarnings("unchecked")
    protected Option getOption(String key, Long id) {
        if (key == null || id == null) {
            return null;
        }
        Map<Long, Option> map = (Map<Long, Option>) getOptionMap(key);
        return (map == null ? null : map.get(id));
    }

    /**
     * Provides the options list specified by key
     * @see com.osserp.core.Options for available lists
     * @param key
     * @return list
     */
    protected List<? extends Option> getOptions(String key) {
        OptionsCache optionsCache = getOptionsCache();
        return (optionsCache == null ? new java.util.ArrayList<Option>() : optionsCache.getList(key));
    }

    /**
     * Returns the option map specified by given option key
     * @see com.osserp.core.Options for available lists
     * @param optionKey
     * @return map of options
     */
    protected Map<Long, ? extends Option> getOptionMap(String optionKey) {
        OptionsCache optionsCache = getOptionsCache();
        return (optionsCache == null ? new java.util.HashMap<Long, Option>() : optionsCache.getMap(optionKey));
    }

    /**
     * Provides a list of all clients
     * @return clients
     */
    public final List<SystemCompany> getClients() {
        return getSystemConfigManager().getCompanies();
    }

    /**
     * Provides a client independent list of all available branchs
     * @return branch offices of all clients
     */
    public final List<BranchOffice> getBranchOffices() {
        return getSystemConfigManager().getBranchs();
    }

    protected BranchOffice fetchBranchOffice(Long id) {
        return (BranchOffice) CollectionUtil.getById(getBranchOffices(), id);
    }

    protected BranchOffice fetchHeadquarter(Long companyId) throws ClientException {
        return getSystemConfigManager().getHeadquarter(companyId);
    }

    /**
     * Loads a system company by id
     * @param id
     * @return systemCompany
     */
    protected SystemCompany getClient(Long id) {
        return getSystemConfigManager().getCompany(id);
    }

    /**
     * The system config manager
     * @return systemConfigManager
     */
    protected SystemConfigManager getSystemConfigManager() {
        return (SystemConfigManager) getService(SystemConfigManager.class.getName());
    }

    /**
     * Provides a system property value.
     * @param name
     * @return value or null if not exists or an error occured on attempt to access system property.
     */
    protected String getSystemProperty(String name) {
        String result = null;
        try {
            SystemConfigManager manager = getSystemConfigManager();
            result = manager.getSystemProperty(name);
        } catch (Exception e) {
            log.warn("getSystemProperty() ignoring failure on attempt to access property [name="
                    + name + "]");
        }
        return result;
    }

    /**
     * Indicates if a system property is enabled
     * @param name
     * @return true if value exists and value is set to 'true'
     */
    protected boolean isSystemPropertyEnabled(String name) {
        boolean result = false;
        try {
            SystemConfigManager manager = getSystemConfigManager();
            result = manager.isSystemPropertyEnabled(name);
        } catch (Exception e) {
            log.warn("isSystemPropertyEnabled() ignoring failure on attempt to access property [name="
                    + name + "]");
        }
        return result;
    }

    /**
     * Creates a text by a template using systems default template manager
     * @param templateName
     * @param values
     * @return createdText or null if no template found or templating not supported
     */
    protected final String createText(String templateName, Map values) {
        try {
            TemplateService service = (TemplateService) getService(TemplateService.class.getName());
            String result = service.createText(templateName, values);
            return result;
        } catch (Throwable t) {
            return null;
        }
    }

    /**
     * Provides the resource string from resource bundle as provided by {@link #getResourceBundle()}. Locale depends on current user or application default if
     * no user logged in during view activity.
     * @param key
     * @return resource or empty string depending on whether a string could be found or not.
     */
    protected String getResourceString(String key) {
        ResourceBundle bundle = fetchResourceBundle();
        String result = null;
        if (bundle != null && key != null) {
            try {
                result = bundle.getString(key);
            } catch (Throwable t) {
                log.warn("getResourceString() failed [message=" + t.getMessage() 
                        + ", key=" + (key.length() > 128 
                                ? (key.substring(0, 10) + "...") : key) + "]");
            }
        }
        return result == null ? "" : result;
    }

    /**
     * Provides the options cache
     * @return optionsCache
     */
    protected OptionsCache getOptionsCache() {
        return (OptionsCache) getService(OptionsCache.class.getName());
    }
}
