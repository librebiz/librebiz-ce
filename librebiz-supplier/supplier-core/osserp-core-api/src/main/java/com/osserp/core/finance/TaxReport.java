/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 14, 2014 9:39:13 AM
 * 
 */
package com.osserp.core.finance;

import java.util.List;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public interface TaxReport extends FinanceReport {
    
    public static final String VAT_BY_ACTUAL = "actual";
    public static final String VAT_BY_TARGET = "target";

    /**
     * Provides the name of the tax method (e.g. 'actual' or 'target')
     * @return tax method
     */
    String getTaxMethod();
    
    /**
     * Provides all inflow of cash records 
     * @return inflow of cash records
     */
    List<FinanceRecord> getInflowOfCash();
    
    /**
     * Adds a list of records to inflow of cash list
     * @param records list of records to add
     */
    void addInflowOfCash(List<FinanceRecord> records);

    /**
     * Provides all outflow of cash records 
     * @return outflow of cash records
     */
    List<FinanceRecord> getOutflowOfCash();
    
    /**
     * Adds a list of records to outflow of cash list
     * @param records list of records to add
     */
    void addOutflowOfCash(List<FinanceRecord> records);
    
    /**
     * Changes the sort order of the inflow list sort order
     */
    void changeInflowSortOrder();
    
    /**
     * Changes the sort order of the outflow list sort order
     */
    void changeOutflowSortOrder();
    
    /**
     * Indicates if report is marked as closed.
     * @return true if unchangeable
     */
    boolean isClosed();
    
    /**
     * Closes report (e.g. set as unchangeable). 
     */
    void close();
}
