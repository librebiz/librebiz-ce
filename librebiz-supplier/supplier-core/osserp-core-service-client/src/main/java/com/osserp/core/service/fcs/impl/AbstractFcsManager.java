/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 7:24:35 PM 
 * 
 */
package com.osserp.core.service.fcs.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.DateFormatter;

import com.osserp.core.BusinessCase;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.FcsItem;
import com.osserp.core.FcsManager;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.dao.FcsActions;
import com.osserp.core.dao.FcsWastebaskets;
import com.osserp.core.employees.Employee;
import com.osserp.core.service.fcs.FcsEventCreator;
import com.osserp.core.service.impl.AbstractService;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFcsManager extends AbstractService implements FcsManager {
    private static Logger log = LoggerFactory.getLogger(AbstractFcsManager.class.getName());
    
    private FcsActions fcsActions = null;
    private FcsEventCreator fcsEventJobs = null;
    private FcsWastebaskets wastebaskets = null;
    private BusinessNotes businessNotes = null;

    public AbstractFcsManager(
            FcsActions actions,
            FcsEventCreator fcsEventJobs,
            FcsWastebaskets wastebaskets,
            BusinessNotes businessNotes) {
        super();
        this.fcsActions = actions;
        this.fcsEventJobs = fcsEventJobs;
        this.wastebaskets = wastebaskets;
        this.businessNotes = businessNotes;
    }

    public void initializeWastebasket(BusinessCase businessCase) {
        List<FcsAction> throwables = getFcsActions().getWastebasketStartUp(businessCase.getType());

        if (throwables != null && !throwables.isEmpty()) {
            for (int i = 0, j = throwables.size(); i < j; i++) {
                FcsAction next = throwables.get(i);
                wastebaskets.add(
                        businessCase.getPrimaryKey(),
                        businessCase.getCreatedBy(),
                        next.getId());
            }
            saveBusinessCase(businessCase);
        }
    }

    public List<FcsClosing> findClosings(Long type) {
        if (log.isDebugEnabled()) {
            log.debug("findClosings() invoked [type=" + type + "]");
        }
        return fcsActions.findClosings(type);
    }

    public FcsClosing createClosing(Employee user, String name, Long type) {
        return fcsActions.createClosing(user.getId(), name, null);
    }

    public List<FcsAction> createActionList(BusinessCase businessCase) {
        List<FcsAction> result = new ArrayList<FcsAction>();
        List<FcsAction> all = fcsActions.createActionList(businessCase);
        for (int i = 0, j = all.size(); i < j; i++) {
            FcsAction next = all.get(i);
            if (!next.isEndOfLife()) {
                result.add(next);
                if (log.isDebugEnabled()) {
                    log.debug("createActionList() added action [id=" + next.getId() 
                            + ", name=" + next.getName() + "]");
                }
            }
        }        
        return result;
    }

    public void checkDependencies(List<FcsAction> todos, FcsAction selected) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("checkDependencies() invoked [todo=" + todos.size()
                    + " caller=" + selected.getId() + "]");
        }
        if (selected.getDependencies().size() > 0 && !selected.isClosingUnconditionally()) {
            for (int i = 0, j = todos.size(); i < j; i++) {
                FcsAction current = todos.get(i);
                if (selected.getDependencies().contains(current.getId())) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkDependencies() failed [selectedAction="
                                + selected.getId()
                                + ", dependsOn=" + current.getId() + "]");
                    }
                    throw new ClientException(ErrorCode.FLOW_CONTROL_MISSING);
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("checkDependencies() done [caller=" + selected.getId() + "]");
        }
    }

    public List<FcsAction> getActionStatusInfo(BusinessCase object) {
        List<FcsAction> result = new ArrayList<FcsAction>();
        List<FcsAction> all = getFcsActions().findByType(object.getType().getId());
        for (int i = 0, j = all.size(); i < j; i++) {
            FcsAction next = all.get(i);
            if (!next.isEndOfLife()) {
                FcsItem item = fetchItem(object.getFlowControlSheet(), next);
                if (item != null) {
                    next.setChanged(item.getCreated());
                } else {
                    next.setChanged(null);
                }
                result.add(next);
            }
        }
        return result;
    }
    
    private FcsItem fetchItem(List<FcsItem> items, FcsAction action) {
        FcsItem result = null;
        for (int i = 0, j = items.size(); i < j; i++) {
            FcsItem next = items.get(i);
            if (next.getAction().getId().equals(action.getId())) {
                if (result == null || result.getCreated().before(next.getCreated())) {
                    result = next;
                }
            }
        }
        return result;
    }

    public boolean cancelFlowControl(
            Long employeeId,
            BusinessCase businessCase,
            FcsItem item,
            String note) throws ClientException {

        if (isNotSet(note)) {
            throw new ClientException(ErrorCode.CANCEL_NOTE_MISSING);
        }
        FcsItem toCancel = null;
        for (int i = 0, j = businessCase.getFlowControlSheet().size(); i < j; i++) {
            FcsItem current = businessCase.getFlowControlSheet().get(i);
            if (current.getId().equals(item.getId())) {
                toCancel = current;
                break;
            }
        }
        if (toCancel != null) {
            FcsAction action = toCancel.getAction();
            FcsItem newItem = createCancelItem(businessCase, action, employeeId, note);
            saveFcsItem(newItem);
            saveBusinessCase(businessCase);
            businessCase.addCancelling(newItem);
            toCancel.setCanceledBy(newItem.getId());
            saveFcsItem(toCancel);
            if (action.isStopping() && businessCase.isStopped()) {
                businessCase.setStopped(false);
            }
            saveBusinessCase(businessCase);

            if (action.isPerformEvents()) {
                createEvents(
                        businessCase,
                        action,
                        new Date(System.currentTimeMillis()),
                        employeeId,
                        businessCase.getPrimaryKey(),
                        note,
                        null,
                        true);
            }
            // perform queue related actions at the end of an action if configured  
            if (action.isPerformAction() && action.isPerformAtEnd()) {

                // TODO perform post actions
                //this.performPostActions(action, employeeId, object, true);
            }
            return true;
        }
        return false;
    }

    protected abstract FcsItem createCancelItem(
            BusinessCase businessCase,
            FcsAction action,
            Long employeeId,
            String note);

    public void throwFlowControl(
            Long employeeId,
            BusinessCase businessCase,
            Long actionId) throws ClientException {

        FcsAction selected = getFcsActions().load(actionId);
        if (!selected.isThrowable()) {
            throw new ClientException(ErrorCode.FLOW_CONTROL_NOT_THROWABLE);
        }
        if (selected.isStopping()) {
            List<FcsAction> todos = createActionList(businessCase);
            boolean otherStoppingActionExists = false;
            for (int i = 0, j = todos.size(); i < j; i++) {
                FcsAction next = todos.get(i);
                if (next.isStopping() && !next.getId().equals(selected.getId())) {
                    otherStoppingActionExists = true;
                    break;
                }
            }
            if (!otherStoppingActionExists) {
                throw new ClientException(ErrorCode.FLOW_CONTROL_STOPPING_EXCLUSIVE);
            }
        }
        for (int i = 0, j = businessCase.getFlowControlSheet().size(); i < j; i++) {
            FcsItem next = businessCase.getFlowControlSheet().get(i);
            if (next.getAction().getId().equals(actionId)) {
                throw new ClientException(ErrorCode.ACTION_ALREADY_USED);
            }
        }
        wastebaskets.add(
                businessCase.getPrimaryKey(),
                employeeId,
                actionId);
        saveBusinessCase(businessCase);
        try {
            if (selected.isPerformEvents()) {
                createEvents(
                        businessCase,
                        selected,
                        new Date(System.currentTimeMillis()),
                        employeeId,
                        businessCase.getPrimaryKey(),
                        null,
                        null,
                        false);
            }
        } catch (Throwable t) {
            log.warn("throwFlowControl() ignored exception [businessCase=" + businessCase.getPrimaryKey()
                    + ", user=" + employeeId + ", fcsAction=" + actionId + ", message=" + t.getMessage() + "]", t);
        }
    }

    protected void cleanupWastebasket(Long businessCaseId) {
        try {
            wastebaskets.cleanup(businessCaseId);
        } catch (Exception e) {
            log.error("cleanupWastebasket() failed [businessCase=" + businessCaseId + ", message=" + e.getMessage() + "]", e);
        }
    }

    public List<FcsItem> getWastebasket(BusinessCase businessCase) {
        if (businessCase == null) {
            throw new BackendException("businessCase must not be null");
        }
        return wastebaskets.getWasteItems(businessCase.getPrimaryKey());
    }

    public void restoreFlowControl(BusinessCase businessCase, Long wastebasketId) {
        if (log.isDebugEnabled()) {
            log.debug("restoreFlowControl() called, selected " + wastebasketId);
        }
        wastebaskets.remove(wastebasketId);
        saveBusinessCase(businessCase);
    }

    public void updateFcsNote(FcsItem item, String userName, String noteUpdate) {
        if (item == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        if (userName != null && isSet(noteUpdate)) {
            // TODO i18n handling 
            StringBuffer buffer = new StringBuffer(160);
            buffer
                    .append("Nachtrag ")
                    .append(DateFormatter.getShortDate(new Date(System.currentTimeMillis())))
                    .append(", ")
                    .append(userName)
                    .append(":\n")
                    .append(noteUpdate)
                    .append("\n---\n");
            if (item.getNote() != null) {
                buffer.append(item.getNote()).append("\n");
            }
            item.setNote(buffer.toString());
            saveFcsItem(item);
        }
    }

    protected FcsActions getFcsActions() {
        return fcsActions;
    }

    protected void createEvents(
            BusinessCase businessCase,
            FcsAction action,
            Date created,
            Long createdBy,
            Long businessId,
            String message,
            String headline,
            boolean canceled) {

        if (log.isDebugEnabled()) {
            log.debug("createEvents() invoked [businessCase="
                    + businessId
                    + ", action="
                    + (action == null ? "null" : action.getId())
                    + "]");
        }
        try {
            fcsEventJobs.createEventTickets(
                    businessCase,
                    action,
                    created,
                    createdBy,
                    businessCase.getPrimaryKey(),
                    message,
                    headline,
                    canceled);
        } catch (Exception e) {
            log.warn("createEvents() ignoring unexpected exception [message=" + e.getMessage()
                    + ", class=" + e.getClass().getName() + "]", e);
        }
        if (log.isDebugEnabled()) {
            log.debug("createEvents() done");
        }
    }

    protected void validateBeforeAdd(
            FcsAction action,
            Date created,
            String note,
            String headline)
            throws ClientException {
        if (created == null) {
            created = new Date(System.currentTimeMillis());
        }
        if (created.after(new Date(System.currentTimeMillis()))) {
            if (log.isDebugEnabled()) {
                log.debug("validateBeforeAdd() failed [message='action date is in future']");
            }
            throw new ClientException(ErrorCode.DATE_FUTURE);
        }
        if (action.isNoteRequired() && isNotSet(note)) {
            if (log.isDebugEnabled()) {
                log.debug("validateBeforeAdd() failed [message='note required']");
            }
            throw new ClientException(ErrorCode.ACTION_NOTE_REQUIRED);
        }
        if (action.isDisplayReverse() &&
                (isNotSet(headline) || headline.length() < 2)) {
            if (log.isDebugEnabled()) {
                log.debug("validateBeforeAdd() failed [message='shortinfo required']");
            }
            throw new ClientException(ErrorCode.SHORTINFO_MISSING);
        }
        if (log.isDebugEnabled()) {
            log.debug("validateBeforeAdd() done, no errors...");
        }
    }
    
    protected void addBusinessNote(Employee employee, BusinessCase businessCase, String note) {
        if (businessNotes != null) {
            try {
                businessNotes.create(
                        businessNotes.getType("salesNote"), 
                        businessCase.getPrimaryKey(), 
                        (employee == null ? Constants.SYSTEM_EMPLOYEE : employee.getId()), 
                        note, 
                        null); //headline
            } catch (Exception e) {
                log.error("addBusinessNote() failed to add note to businessCase [message="
                        + e.getMessage() + "]", e);
            }
        }
    }

    protected FcsEventCreator getFcsEventJobs() {
        return fcsEventJobs;
    }

    protected abstract void saveBusinessCase(BusinessCase businessCase);

    protected abstract void saveFcsItem(FcsItem object);

}
