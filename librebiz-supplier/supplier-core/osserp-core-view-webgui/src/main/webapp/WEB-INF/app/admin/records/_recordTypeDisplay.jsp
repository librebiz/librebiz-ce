<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-app" prefix="oa" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="recordTo" /> <o:out value="${view.bean.name}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="shortkey"><fmt:message key="shortkey" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${empty view.bean.numberPrefix}">
                                <fmt:message key="undefined" />
                            </c:when>
                            <c:otherwise>
                                <o:out value="${view.bean.numberPrefix}"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${!empty view.bean.numberPrefix}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="shortkeyStatus"><fmt:message key="shortkeyStatus" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.bean.addNumberPrefix}">
                                    <fmt:message key="shortkeyActivated" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="shortkeyDeactivated" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recordTypeNotes"><fmt:message key="recordTypeNotes" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.displayNotesBelowItems}">
                                <fmt:message key="displayNotesBelowItems" />
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="displayNotesBelowItemsDisabled" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${view.termsAvailable && view.bean.addTermsAndConditionsOnPrint}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="addTermsOnPrint"><fmt:message key="printRecord" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="addTermsOnPrint" />
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${view.rightToCancelAvailable && view.bean.addRightToCancelOnPrint}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                <c:if test="${!view.termsAvailable || !view.bean.addTermsAndConditionsOnPrint}">
                                    <fmt:message key="printRecord" />
                                </c:if>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="addRightToCancelOnPrint" />
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${view.bean.addProductDatasheet}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><fmt:message key="sendEmail" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="addProductSheetsOnEmail" />
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.numberCreatorFixed}">
                                <label for="numberRange"><fmt:message key="numberRange" /></label>
                            </c:when>
                            <c:otherwise>
                                <o:permission role="record_number_range,executive,organization_admin">
                                    <v:link url="/admin/records/recordTypeConfig/enableNumberConfigMode">
                                        <label for="numberRange"><fmt:message key="numberRange" /></label>
                                    </v:link>
                                </o:permission>
                                <o:forbidden role="record_number_range,executive,organization_admin">
                                    <label for="numberRange"><fmt:message key="numberRange" /></label>
                                </o:forbidden>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <fmt:message key="${view.bean.numberCreatorName}" />
                        <c:if test="${view.bean.numberCreatorName == 'date'}">
                            +  <o:out value="${view.bean.uniqueNumberDigitCount}"/>
                            <fmt:message key="digits" />
                        </c:if>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<c:if test="${!empty view.latestRecords}">
<c:set var="latestRecords" scope="request" value="${view.latestRecords}"/>
<c:set var="latestRecordsExit" scope="request" value="recordTypeConfig"/>
<c:set var="latestRecordsViewExit" scope="request" value="/admin/records/recordTypeConfig/reload"/>
<c:import url="${viewdir}/admin/records/_latestRecordList.jsp" />
</c:if>
