<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="project" value="${view.businessCase}" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="contractDetails" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:if test="${!empty view.bean.type}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="displayName"><fmt:message key="type" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.bean.type.name}" />
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="status"><fmt:message key="status" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="status" options="salesContractStatus" value="${view.bean.status.id}" styleClass="form-control" prompt="false" />
                    </div>
                </div>
            </div>

            <v:date name="contractStart" styleClass="form-control" label="termStart" picker="true" />

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="term"><fmt:message key="term" /></label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <v:text name="monthsRunning" styleClass="form-control" value="${view.bean.monthsRunning}" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="months" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="billingInterval"><fmt:message key="billingInterval" /></label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <v:text name="billingDays" styleClass="form-control" value="${view.bean.billingDays}" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="daysLabel" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recording"><fmt:message key="hoursRecording" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="trackingRequired">
                                <v:checkbox name="trackingRequired" value="${view.bean.trackingRequired}" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recordingType"><fmt:message key="recording" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="trackingTypeDefault" options="projectTrackingTypes" value="${view.bean.trackingTypeDefault}"
                            styleClass="form-control" prompt="true" promptKey="undefined" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/disableEditMode" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

