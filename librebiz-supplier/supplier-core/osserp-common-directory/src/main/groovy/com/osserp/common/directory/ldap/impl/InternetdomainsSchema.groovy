/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.StructuralSchema

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class InternetdomainsSchema extends AbstractLdapConfigAware implements StructuralSchema {

    private static final List OBJECT_CLASSES = [
        'top',
        'dbmailDomain',
        'osserpDomain'
    ]

    InternetdomainsSchema(LdapConfig configObj) {
        super(configObj)
    }

    /**
     * Creates a new ldap ready domain object by default and values provided by map
     * @param values must contain at least 'domainname' as dc
     * @return domain object without dn
     */
    @Override
    Map create(Map values) {
        if (!values.dc) {
            return [ errors : 'missing dc']
        }
        if (!values.mailhost) {
            return [ errors : 'missing mailhost']
        }
        Map domain = [
            objectclass: (config.domainObjectClasses ?: OBJECT_CLASSES),
            dc: values.dc,
            maildomain: values.dc,
            mailhost: values.mailhost,
            domainexpires: 'unkown'
        ]
        return domain
    }

    String getMinimumId() {
        // internetdomain does not support id
        null
    }

    String getQuery() {
        config.domainQuery ?: '(objectClass=osserpDomain)'
    }

    List<String> getObjectClasses() {
        config?.domainObjectClasses ?: OBJECT_CLASSES
    }

    @Override
    List<String> getAvailableAttributes() {
        [
            'dc',
            'mailhost',
            'mailrelay',
            'domainexpires'
        ]
    }

    Set<String> getAvailableArrays() {
        return new HashSet<String>()
    }


    @Override
    List<String> getDefaultDisplayAttributes() {
        getAvailableAttributes()
    }
}
