/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 6, 2009 6:17:44 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionContext;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductSelectionConfigs {

    /**
     * Provides all product selection configs
     * @return all configs
     */
    List<ProductSelectionConfig> findAll();

    /**
     * Provides all product selection configs by context
     * @param context
     * @return all configs
     */
    List<ProductSelectionConfig> findAll(ProductSelectionContext context);

    /**
     * Provides an product selection config by primary key
     * @param id
     * @return config by primary key
     */
    ProductSelectionConfig load(Long id);

    /**
     * Loads all configs by primary keys
     * @param primaryKeys
     * @return list by primary keys
     */
    List<ProductSelectionConfig> load(List<Long> primaryKeys);

    /**
     * Creates an product selection
     * @param user
     * @param context
     * @param name
     * @param description
     * @return new created selection config
     */
    ProductSelectionConfig create(Employee user, ProductSelectionContext context, String name, String description)
            throws ClientException;

    /**
     * Restricts usage of selection to employees of a company and optional branch
     * @param user
     * @param config
     * @param company
     * @param branch
     */
    ProductSelectionConfig restrict(
            Employee user,
            ProductSelectionConfig config,
            SystemCompany company,
            BranchOffice branch) throws ClientException;

    /**
     * Synchronizes database state of an product selection config
     * @param itemId
     */
    void deleteItem(Long itemId) throws ClientException;

    /**
     * Deletes all matching items
     * @param typeId
     * @param groupId
     * @param categoryId
     */
    void deleteItems(Long typeId, Long groupId, Long categoryId);

    /**
     * Toggles the indication whether a config item is an exclusion or not
     * @param itemId
     * @throws ClientException
     */
    void toggleExclusion(Long itemId) throws ClientException;

    /**
     * Synchronizes database state of an product selection config
     * @param userId
     * @param configId
     * @param columnName
     * @param itemId
     */
    void addItem(Long userId, Long configId, String columnName, Long itemId) throws ClientException;

    /**
     * Adds an item
     * @param userId
     * @param configId
     * @param typeColumnName
     * @param typeId
     * @param groupColumnName
     * @param groupId
     * @param categoryColumnName
     * @param categoryId
     * @throws ClientException
     */
    void addItem(Long userId, Long configId, String typeColumnName, Long typeId, String groupColumnName, Long groupId, String categoryColumnName,
            Long categoryId) throws ClientException;

    /**
     * Synchronizes database state of an product selection config
     * @param config
     */
    void save(ProductSelectionConfig config);
}
