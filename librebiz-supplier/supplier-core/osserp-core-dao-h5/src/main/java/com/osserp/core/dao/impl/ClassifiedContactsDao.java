/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 26, 2009 2:28:27 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import org.hibernate.SessionFactory;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.StringUtil;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactListEntry;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.dao.ClassifiedContacts;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Emails;
import com.osserp.core.model.contacts.AbstractContactAware;
import com.osserp.core.model.contacts.ContactImpl;
import com.osserp.core.model.system.SyncTaskImpl;
import com.osserp.core.system.SyncTaskConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class ClassifiedContactsDao extends AbstractContactAwareDao implements ClassifiedContacts {
    private static Logger log = LoggerFactory.getLogger(ClassifiedContactsDao.class.getName());

    public ClassifiedContactsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Contacts contacts,
            Emails emails,
            OptionsCache namesCache) {
        super(jdbcTemplate, tables, sessionFactory, contacts, emails, namesCache);
    }

    /**
     * Provides the basic statement required for queries
     * @return query
     */
    protected abstract StringBuilder getQuery();

    /**
     * Provides the basic statement required for queries
     * @return query
     */
    protected abstract String getOrderedQuery();

    /**
     * Provides the class of the related contact entity
     * @return entity class
     */
    protected abstract Class<? extends ClassifiedContact> getEntityClass();

    /**
     * Provides the table name of the related contact entity
     * @return entity table name
     */
    protected abstract String getEntityTable();

    public void updateContactStatus(ClassifiedContact contact, Long status) {
        contact.setStatus(status);
        save(contact);
    }

    public final boolean exists(Long id) {
        StringBuilder sql = new StringBuilder("SELECT contact_id FROM ");
        sql.append(getEntityTable()).append(" WHERE id = ").append(id);
        List<Long> result = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        return ((result.isEmpty() ? null : result.get(0)) != null);
    }

    public Person findByExternalReference(String externalReference) {
        Long contactId = getIdByExternalReference(externalReference);
        return isEmpty(contactId) ? null : findByContact(contactId);
    }

    public ClassifiedContact findByContact(Long contactId) {
        long start = System.currentTimeMillis();
        StringBuilder sql = new StringBuilder("SELECT id FROM ");
        sql
                .append(getEntityTable())
                .append(" WHERE contact_id = ")
                .append(contactId);
        List<Long> result = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        Long id = (result.isEmpty() ? null : result.get(0));
        if (id == null) {
            if (log.isDebugEnabled()) {
                log.debug("findByContact() invoked with none existing contact_id [contactId=" + contactId + ", table=" + getEntityTable() + "]");
            }
            return null;
        }
        ClassifiedContact contact = (ClassifiedContact) getCurrentSession().load(getEntityClass(), id);
        if (log.isDebugEnabled()) {
            log.debug("findByContact() done [id=" + contact.getId()
                    + ", contactId=" + contactId
                    + ", duration=" + (System.currentTimeMillis() - start) + "ms]");
        }
        return contact;
    }

    public ClassifiedContact findByContact(Contact contact) {
        return findByContact(contact.getContactId());
    }

    public ClassifiedContact findByEmail(String emailAddress) {
        ClassifiedContact result = null;
        List<ContactListEntry> list = contacts.findByEmailAddress(emailAddress);
        if (!list.isEmpty()) {
            if (list.size() == 1) {
                Long contactId = list.get(0).getId();
                result = findByContact(contactId);
            } else {
                for (int i = 0, j = list.size(); i < j; i++) {
                    ContactListEntry next = list.get(i);
                    ClassifiedContact o = findByContact(next.getId());
                    if (result != null) {
                        ClassifiedContact second = o;
                        log.warn("findByEmail: Multiple contacts with same "
                            + "email address [previous=" + result.getId()
                            + ", current=" + second.getId()
                            + "]");
                        Date firstChanged = result.getChanged() != null ?
                                result.getChanged() : result.getCreated();
                        Date secondChanged = second.getChanged() != null ?
                                second.getChanged() : second.getCreated();
                        if (firstChanged.before(secondChanged)) {
                             result = second;
                        }
                    } else {
                         result = o;
                    }
                }
            }
        }
        return result;
    }

    public List<ContactListEntry> findAll() {
        return (List<ContactListEntry>) jdbcTemplate.query(
                getOrderedQuery(),
                ContactListEntryRowMapper.getReader(namesCache));
    }


    public boolean runningTaskExists(SyncTaskConfig config) {
        StringBuilder hql = new StringBuilder(128);
        hql
                .append("from ").append(SyncTaskImpl.class.getName())
                .append(" o where o.config.id = :configId and o.status = 1");
        return !getCurrentSession().createQuery(hql.toString())
                .setParameter("configId", config.getId()).list().isEmpty();
    }

    protected final List<ClassifiedContact> load(List<Long> ids) {
        List<ClassifiedContact> result = new ArrayList<ClassifiedContact>();
        if (!ids.isEmpty()) {
            StringBuilder hql = new StringBuilder(128);
            hql
                    .append("from ")
                    .append(getEntityClass().getName())
                    .append(" o where o.id in (")
                    .append(StringUtil.createCommaSeparated(ids))
                    .append(") order by o.id");
            result = getCurrentSession().createQuery(hql.toString()).list();
        }
        return result;
    }

    public List<Option> findNames() {
        return (List<Option>) jdbcTemplate.query(
                getOrderedQuery(), getNameRowMapper());
    }

    protected List findContactPersonNames(String relationTable) {
        StringBuilder query = new StringBuilder(128);
        query
                .append("SELECT contact_id,salutation,firstname,lastname FROM ")
                .append(contactsTable)
                .append(" WHERE status_id > -10 AND contact_ref IN (SELECT contact_id FROM ")
                .append(relationTable)
                .append(") ORDER BY lastname");
        return (List) jdbcTemplate.query(
                query.toString(),
                ContactPersonNameRowMapper.getReader(namesCache));
    }

    protected RowMapperResultSetExtractor getNameRowMapper() {
        return new RowMapperResultSetExtractor(new NameRowMapper());
    }

    private class NameRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            Long id = Long.valueOf(rs.getLong(1));
            StringBuilder display = new StringBuilder(64);
            String firstName = rs.getString(4);
            String lastName = rs.getString(5);
            if (lastName == null) {
                lastName = "NONE!";
            }
            display.append(lastName);
            if (firstName != null && firstName.length() > 0) {
                display.append(", ").append(firstName);
            }
            return new OptionImpl(id, display.toString());
        }
    }

    public final void save(ClassifiedContact related) {
        if (log.isDebugEnabled()) {
            log.debug("save() invoked [entity=" 
                + (related == null ? "null" : related.getClass().getName()) + "]");
        }
        if (related instanceof AbstractContactAware) {
            AbstractContactAware ca = (AbstractContactAware) related;
            Contact c = ca.getRc();
            if (c instanceof ContactImpl) {
                getCurrentSession().merge(ContactImpl.class.getName(), c);
            }
        }
        getCurrentSession().merge(getEntityClass().getName(), related);
    }

    public final ClassifiedContact update(
            ClassifiedContact classifiedContact,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Date birthDate,
            boolean grantEmail,
            String grantEmailNote,
            boolean grantPhone,
            String grantPhoneNote,
            boolean grantNone,
            String grantNoneNote) {

        if (salutation != null) {
            classifiedContact.setSalutation(salutation);
        }
        if (title != null) {
            classifiedContact.setTitle(title);
        }
        if (isSet(firstName)) {
            classifiedContact.setFirstName(firstName);
        }
        if (isSet(lastName)) {
            classifiedContact.setLastName(lastName);
        }
        if (isSet(street)) {
            classifiedContact.getAddress().setStreet(street);
        }
        if (isSet(streetAddon)) {
            classifiedContact.getAddress().setStreetAddon(streetAddon);
        }
        if (isSet(zipcode)) {
            classifiedContact.getAddress().setZipcode(zipcode);
        }
        if (isSet(city)) {
            classifiedContact.getAddress().setCity(city);
        }
        if (isSet(country)) {
            classifiedContact.getAddress().setCountry(country);
        }
        if (birthDate != null) {
            classifiedContact.setBirthDate(birthDate);
        }
        if (classifiedContact instanceof AbstractContactAware) {
            AbstractContactAware aca = (AbstractContactAware) classifiedContact;
            if (grantEmail && !classifiedContact.isGrantContactPerEmail()) {
                aca.setGrantContactPerEmail(grantEmail);
                if (isSet(grantEmailNote)) {
                    aca.setGrantContactPerEmailNote(grantEmailNote);
                }
            }
            if (grantPhone && !classifiedContact.isGrantContactPerPhone()) {
                aca.setGrantContactPerPhone(grantPhone);
                if (isSet(grantPhoneNote)) {
                    aca.setGrantContactPerPhoneNote(grantPhoneNote);
                }
            }
            if (classifiedContact.isGrantContactNone() && (grantPhone || grantEmail)) {
                aca.setGrantContactNone(false);
                if (isSet(grantNoneNote)) {
                    aca.setGrantContactNoneNote(grantNoneNote);
                }
            }
        }
        save(classifiedContact);
        return classifiedContact;
    }
}
