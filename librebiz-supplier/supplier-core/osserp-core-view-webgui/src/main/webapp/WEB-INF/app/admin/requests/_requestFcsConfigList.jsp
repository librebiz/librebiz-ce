<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-lg-12 panel-area">

    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <tbody>
                <c:forEach var="action" items="${view.list}">
                    <tr>
                        <td>
                            <o:out value="${action.name}" />
                        </td>
                        <td class="icon center">
                            <v:link url="${view.baseLink}/moveUp?id=${action.id}" title="moveUp">
                                <o:img name="upIcon"/>
                            </v:link>
                        </td>
                        <td class="icon center">
                            <v:link url="${view.baseLink}/moveDown?id=${action.id}" title="moveDown">
                                <o:img name="downIcon"/>
                            </v:link>
                        </td>
                        <td style="width: 60px; text-align: right;"><span style="text-align: right; margin-right: 5px;"><o:out value="${action.status}" /></span></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>

</div>
