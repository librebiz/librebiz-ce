/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 21, 2007 1:29:43 PM 
 * 
 */
package com.osserp.core.model.hrm;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordTypeImpl extends AbstractOption implements TimeRecordType {

    private boolean starting = false;
    private boolean interval = false;
    private boolean intervalHoursByConfig = true;
    private int intervalHours = 8;
    private int intervalMinutes = 0;
    private int intervalStartHours = 8;
    private int intervalStartMinutes = 0;
    private Integer orderId = 0;
    private boolean eol = false;
    private boolean leave = false;
    private boolean specialLeave = false;
    private boolean unpaidLeave = false;
    private boolean manualInputRequired = false;
    private boolean ignoreBreakRules = false;
    private boolean ignoreNameOnPrint = false;
    private boolean defaultType = false;
    private boolean illness = false;
    private boolean school = false;
    private boolean bookingFuture = false;
    private boolean bookingPast = false;
    private boolean intervalPeriod = false;
    private boolean subtraction = false;
    private boolean visible = true;
    private boolean bookable = false;
    private boolean addition = false;
    private boolean monthSubtraction = false;
    private boolean daySubtraction = false;
    private boolean needApproval = false;
    private String permissions = "";

    public boolean isMonthSubtraction() {
        return monthSubtraction;
    }

    public void setMonthSubtraction(boolean monthSubtraction) {
        this.monthSubtraction = monthSubtraction;
    }

    public boolean isDaySubtraction() {
        return daySubtraction;
    }

    public void setDaySubtraction(boolean daySubtraction) {
        this.daySubtraction = daySubtraction;
    }

    public boolean isAddition() {
        return addition;
    }

    public void setAddition(boolean addition) {
        this.addition = addition;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isBookable() {
        return bookable;
    }

    public void setBookable(boolean bookable) {
        this.bookable = bookable;
    }

    public boolean isSubtraction() {
        return subtraction;
    }

    public void setSubtraction(boolean subtraction) {
        this.subtraction = subtraction;
    }

    public boolean isBookingFuture() {
        return bookingFuture;
    }

    public void setBookingFuture(boolean bookingFuture) {
        this.bookingFuture = bookingFuture;
    }

    public boolean isBookingPast() {
        return bookingPast;
    }

    public void setBookingPast(boolean bookingPast) {
        this.bookingPast = bookingPast;
    }

    public boolean isIntervalPeriod() {
        return intervalPeriod;
    }

    public void setIntervalPeriod(boolean intervalPeriod) {
        this.intervalPeriod = intervalPeriod;
    }

    protected TimeRecordTypeImpl() {
        super();
    }

    public TimeRecordTypeImpl(Employee user, Long id, String name, String description, Integer orderId) {
        super(id, (Long) null, name, description, null, user.getId());
        this.orderId = orderId;
        this.starting = true;
    }

    public boolean isStarting() {
        return starting;
    }

    protected void setStarting(boolean starting) {
        this.starting = starting;
    }

    public boolean isInterval() {
        return interval;
    }

    public void setInterval(boolean interval) {
        this.interval = interval;
    }

    public boolean isIntervalHoursByConfig() {
        return intervalHoursByConfig;
    }

    public void setIntervalHoursByConfig(boolean intervalHoursByConfig) {
        this.intervalHoursByConfig = intervalHoursByConfig;
    }

    public int getIntervalHours() {
        return intervalHours;
    }

    public void setIntervalHours(int intervalHours) {
        this.intervalHours = intervalHours;
    }

    public int getIntervalMinutes() {
        return intervalMinutes;
    }

    public void setIntervalMinutes(int intervalMinutes) {
        this.intervalMinutes = intervalMinutes;
    }

    public int getIntervalStartHours() {
        return intervalStartHours;
    }

    public void setIntervalStartHours(int intervalStartHours) {
        this.intervalStartHours = intervalStartHours;
    }

    public int getIntervalStartMinutes() {
        return intervalStartMinutes;
    }

    public void setIntervalStartMinutes(int intervalStartMinutes) {
        this.intervalStartMinutes = intervalStartMinutes;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public boolean isEol() {
        return eol;
    }

    public void setEol(boolean eol) {
        this.eol = eol;
    }

    public boolean isLeave() {
        return leave;
    }

    public void setLeave(boolean leave) {
        this.leave = leave;
    }

    public boolean isSpecialLeave() {
        return specialLeave;
    }

    public void setSpecialLeave(boolean specialLeave) {
        this.specialLeave = specialLeave;
    }

    public boolean isIllness() {
        return illness;
    }

    public void setIllness(boolean illness) {
        this.illness = illness;
    }

    public boolean isUnpaidLeave() {
        return unpaidLeave;
    }

    public void setUnpaidLeave(boolean unpaidLeave) {
        this.unpaidLeave = unpaidLeave;
    }

    public boolean isSchool() {
        return school;
    }

    public void setSchool(boolean school) {
        this.school = school;
    }

    public boolean isManualInputRequired() {
        return manualInputRequired;
    }

    public void setManualInputRequired(boolean manualInputRequired) {
        this.manualInputRequired = manualInputRequired;
    }

    public boolean isIgnoreBreakRules() {
        return ignoreBreakRules;
    }

    public void setIgnoreBreakRules(boolean ignoreBreakRules) {
        this.ignoreBreakRules = ignoreBreakRules;
    }

    public boolean isIgnoreNameOnPrint() {
        return ignoreNameOnPrint;
    }

    public void setIgnoreNameOnPrint(boolean ignoreNameOnPrint) {
        this.ignoreNameOnPrint = ignoreNameOnPrint;
    }

    public boolean isDefaultType() {
        return defaultType;
    }

    public void setDefaultType(boolean defaultType) {
        this.defaultType = defaultType;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public boolean isNeedApproval() {
        return needApproval;
    }

    public void setNeedApproval(boolean needApproval) {
        this.needApproval = needApproval;
    }
}
