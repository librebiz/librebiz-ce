/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 26, 2016 
 * 
 */
package com.osserp.core.service.records;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.finance.AnnualReport;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface AnnualReportArchive {

    /**
     * Indicates if report archive exists
     * @param report
     * @return true if report archive exists
     */
    boolean archiveExists(AnnualReport report);
    
    /**
     * Creates the annual report archive.
     * @param report
     * @return name including path of the created archive
     * @throws ClientException if something went wrong
     */
    String createArchive(AnnualReport report) throws ClientException;
    
    /**
     * Deletes an existing archive
     * @param report
     * @throws ClientException if delete failed
     */
    void deleteArchive(AnnualReport report) throws ClientException;

    /**
     * Provides an archive with all report documents to download.
     * @param report
     * @return document data
     * @throws ClientException if data could not be provided
     */
    DocumentData getArchive(AnnualReport report) throws ClientException;
    
    /**
     * Provides all bank account of a year and bank 
     * @param bankAccountId
     * @param year
     * @return documents or empty list if none found
     */
    List<DmsDocument> getBankAccountDocuments(Long bankAccountId, int year);

}
