/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 31, 2005 1:37:08 PM 
 * 
 */
package com.osserp.core.model.projects;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Option;
import com.osserp.common.beans.OptionImpl;

import com.osserp.core.BusinessType;
import com.osserp.core.projects.results.ProjectListItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class ProjectListItemVO extends OptionImpl implements ProjectListItem {
    private static final long serialVersionUID = 42L;
    private static Logger log = LoggerFactory.getLogger(ProjectListItemVO.class.getName());

    private Long planId = null;
    private BusinessType type = null;
    private Integer status = null;
    private Long salesId = null;
    private Long managerId = null;
    private Long branchId = null;
    private Long shippingId = null;
    private String shippingName;
    private Long paymentId = null;
    private String paymentName;

    private List flowControlSheet = null;
    private List flowControlTodos = null;
    private List notes = null;

    private boolean packageListRequired = false;
    private boolean vacant = false;

    protected ProjectListItemVO() {
        super();
    }

    /**
     * Constructor to set all values with one call
     * @param id
     * @param planId
     * @param name
     * @param type
     * @param status
     * @param salesId
     * @param managerId
     * @param branchId
     * @param shippingId
     * @param paymentId
     */
    public ProjectListItemVO(
            Long id,
            Long planId,
            String name,
            BusinessType type,
            Integer status,
            Long salesId,
            Long managerId,
            Long branchId,
            Long shippingId,
            Long paymentId) {

        super(id, name);
        this.planId = planId;
        this.type = type;
        this.status = status;
        this.salesId = salesId;
        this.managerId = managerId;
        this.branchId = branchId;
        this.shippingId = shippingId;
        this.paymentId = paymentId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public BusinessType getType() {
        return type;
    }

    public void setType(BusinessType type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getSalesId() {
        return salesId;
    }

    public void setSalesId(Long salesId) {
        this.salesId = salesId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public Long getShippingId() {
        return shippingId;
    }

    public void setShippingId(Long shippingId) {
        this.shippingId = shippingId;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public boolean isPackageListRequired() {
        return packageListRequired;
    }

    public void setPackageListRequired(boolean packageListRequired) {
        this.packageListRequired = packageListRequired;
    }

    public List getFlowControlSheet() {
        return flowControlSheet;
    }

    public void setFlowControlSheet(List flowControlSheet) {
        this.flowControlSheet = flowControlSheet;
    }

    public List getFlowControlTodos() {
        return flowControlTodos;
    }

    public void setFlowControlTodos(List flowControlTodos) {
        this.flowControlTodos = flowControlTodos;
    }

    public List getNotes() {
        return notes;
    }

    public void setNotes(List notes) {
        this.notes = notes;
    }

    public boolean isVacant() {
        return vacant;
    }

    public void setVacant(boolean vacant) {
        this.vacant = vacant;
    }

    protected String getEmployee(Map map, Long id) {
        if (map == null) {
            log.warn("getEmployee() map was null!");
            return "";
        } else if (id == null) {
            return "";
        } else {
            Option o = (Option) map.get(id);
            if (o == null) {
                return "";
            }
            return o.getName();
        }
    }

}
