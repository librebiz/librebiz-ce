/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Feb-2007 15:31:50 
 * 
 */
package com.osserp.core.service.doc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.CDATA;
import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.OptionsCache;
import com.osserp.common.PermissionException;
import com.osserp.common.XmlAwareEntity;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;

import com.osserp.core.Address;
import com.osserp.core.BusinessCase;
import com.osserp.core.Options;
import com.osserp.core.contacts.OfficeContactManager;
import com.osserp.core.contacts.Phone;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterParagraph;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductDetails;
import com.osserp.core.projects.Project;
import com.osserp.core.projects.ProjectRequest;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class XmlServiceImpl extends AbstractService implements XmlService {
    private static Logger log = LoggerFactory.getLogger(XmlServiceImpl.class.getName());
    private Employees employees = null;
    protected OptionsCache optionsCache = null;
    private SystemConfigs systemConfigs = null;
    private BranchOffices branchOffices = null;
    private OfficeContactManager officeContactManager = null;

    public XmlServiceImpl(
            OptionsCache optionsCache,
            SystemConfigs systemConfigs,
            Employees employees, 
            BranchOffices branchOffices, 
            OfficeContactManager officeContactManager) {
        super();
        this.optionsCache = optionsCache;
        this.systemConfigs = systemConfigs;
        this.employees = employees;
        this.branchOffices = branchOffices;
        this.officeContactManager = officeContactManager;
    }

    public String getCountryName(Long countryId) {
        if (isNotSet(countryId)) {
            return null;
        }
        Long defaultCountry = systemConfigs.getSystemPropertyId("contactDefaultCountry");
        if (isNotSet(defaultCountry)) {
            if (log.isDebugEnabled()) {
                log.debug("Did not find systemProperty: contactDefaultCountry");
            }
            return null;
        }
        boolean addDefault = systemConfigs == null ? false :
            systemConfigs.isSystemPropertyEnabled("documentPrintAddDefaultCountry");
        if (!countryId.equals(defaultCountry) || addDefault) {
            return optionsCache.getMappedValue(Options.COUNTRY_NAMES, countryId);
        }
        return null;
    }

    /**
     * Creates a new xml document with element name 'root' as root element, <br/>
     * employee values as 'currentUser' and elements 'currentDate' and 'currentTime' <br/>
     * as childs
     * @param employee
     */
    public Document createDocument(Employee employee) {
        Element root = new Element("root");
        root.addContent(createEmployee("currentUser", employee));
        Date current = DateUtil.getCurrentDate();
        root.addContent(new Element("currentDate").setText(DateFormatter.getDate(current)));
        root.addContent(new Element("currentTime").setText(DateFormatter.getTime(current)));
        root.addContent(new Element("currentYear").setText(DateFormatter.getYear(current)));
        Document doc = new Document(root);
        return doc;
    }

    public Element createAddress(String elementName, Address address) {
        String name = elementName == null ? "address" : elementName;
        Element xml = new Element(name);
        if (address != null) {
            xml = address.getXML(name);

            String countryName = getCountryName(address.getCountry());
            if (isNotSet(countryName)) {
                xml.addContent(new Element("countryName"));
            } else {
                xml.addContent(new Element("countryName").setText(countryName));
            }
        }
        return xml;
    }

    public Element createAddress(Address address) {
        return createAddress("address", address);
    }
    
    public Element createProductDetails(String elementName, ProductDetails details) {
        return new Element("details");
    }

    public Element createBusinessCase(String elementName, BusinessCase businessCase) {
        if (elementName == null || elementName.length() < 1) {
            elementName = "businessCase";
        }
        Element bc = new Element(elementName);
        if (businessCase == null) {
            return bc;
        }
        Request request = null;
        Sales sales = null;
        if (businessCase instanceof Request) {
            request = (Request) businessCase;
        } else if (businessCase instanceof Sales) {
            sales = (Sales) businessCase;
            request = sales.getRequest();
        }
        if (request == null) {
            return bc;
        }
        if (request.getName() != null) {
            bc.addContent(new Element("name").setText(request.getName()));
        } else {
            bc.addContent(new Element("name").setText(request.getRequestId().toString()));
        }
        if (request.getBranch() != null 
                && (request.getBranch().getId() != null 
                    || request.getBranch().getName() != null)) {
            
            if (request.getBranch().getId() != null) {  
                bc.addContent(new Element("branchId").setText(request.getBranch().getId().toString()));
            }
            if (request.getBranch().getName() != null) {
                bc.addContent(new Element("branchName").setText(request.getBranch().getName()));
            }
            
        } else {
            bc.addContent(new Element("branchId").setText("0"));
            bc.addContent(new Element("branchName"));
        }
        bc.addContent(createAddress("deliveryAddress", request.getAddress()));
        if (sales != null) {
            bc.addContent(new Element("id").setText(sales.getId().toString()));
            bc.addContent(new Element("isRequest").setText("false"));
            bc.addContent(new Element("transactionType").setText(sales.getType().getId().toString()));
            bc.addContent(new Element("transactionTypeName").setText(sales.getType().getName()));
            bc.addContent(new Element("status").setText(sales.getStatus().toString()));
            bc.addContent(new Element("created").setText(DateFormatter.getDate(sales.getCreated())));
            addSalesValues(bc, sales);
        } else {
            bc.addContent(new Element("id").setText(request.getRequestId().toString()));
            bc.addContent(new Element("isRequest").setText("true"));
            bc.addContent(new Element("transactionType").setText(request.getTypeId().toString()));
            bc.addContent(new Element("transactionTypeName").setText(request.getType().getName()));
            bc.addContent(new Element("created").setText(DateFormatter.getDate(request.getCreated())));
            bc.addContent(createEmployee("sales", request.getSalesId()));
            bc.addContent(createEmployee("manager", request.getManagerId()));
        }
        return bc;
    }

    public Element createLetter(Employee user, LetterContent content, String xmlName) {
        Element root = new Element(xmlName);
        Element addressXml = createAddress(content.getAddress());
        root.addContent(addressXml);
        root.addContent(new Element("styleSheetName").setText(createString(content.getType().getStylesheetName())));
        root.addContent(new Element("id").setText(createString(content.getId())));
        root.addContent(new Element("branchId").setText(createString(content.getBranchId())));
        root.addContent(new Element("subject").setText(createString(content.getSubject())));
        root.addContent(new Element("salutation").setText(createString(content.getSalutation())));
        root.addContent(new Element("ignoreSalutation").setText(Boolean.valueOf(content.isIgnoreSalutation()).toString()));
        root.addContent(new Element("ignoreGreetings").setText(Boolean.valueOf(content.isIgnoreGreetings()).toString()));
        root.addContent(new Element("ignoreHeader").setText(Boolean.valueOf(content.isIgnoreHeader()).toString()));
        root.addContent(new Element("ignoreSubject").setText(Boolean.valueOf(content.isIgnoreSubject()).toString()));
        if (content.getGreetings() != null) {
            root.addContent(new Element("greetings").setText(createString(content.getGreetings())));
        }
        if (content.getLanguage() != null) {
            root.addContent(new Element("language").setText(createString(content.getLanguage())));
        }
        Element paragraphs = new Element("paragraphs");
        for (int i = 0, j = content.getParagraphs().size(); i < j; i++) {
            LetterParagraph next = content.getParagraphs().get(i);
            Element paragraph = new Element("paragraph");
            Element text = new Element("value");
            text.addContent(new CDATA(next.getText()));
            paragraph.addContent(text);
            paragraphs.addContent(paragraph);
        }
        root.addContent(paragraphs);
        if (content.getSignatureLeft() != null) {
            Element sig = createEmployee("signatureLeft", content.getSignatureLeft());
            root.addContent(sig);
        } else {
            Element sig = createEmployee("signatureLeft", user);
            root.addContent(sig);
        }
        if (content.getSignatureRight() != null) {
            Element sig = createEmployee("signatureRight", content.getSignatureRight());
            root.addContent(sig);
        }
        return root;
    }

    public Element createOfficeContacts(BusinessCase businessCase) {
        try {
            return officeContactManager.getContacts(businessCase);
        } catch (Throwable t) {
            log.warn("createOfficeContacts() ignoring exception [businessCase=" 
                    + businessCase.getPrimaryKey()
                    + ", sales=" + businessCase.isSalesContext()
                    + ", class=" + t.getClass().getName()
                    + ", message=" + t.getMessage() + "]");
            return new Element("contacts");
        }
    }

    private void addSalesValues(Element xml, Sales sales) {
        xml.addContent(new Element("isReference").setText(Boolean.valueOf(sales.isReference()).toString()));
        xml.addContent(new Element("isInternetReference").setText(Boolean.valueOf(sales.isInternetExportEnabled()).toString()));
        xml.addContent(createEmployee("manager", sales.getManagerId()));
        xml.addContent(createEmployee("sales", sales.getRequest().getSalesId()));
        if (sales.getRequest().getDeliveryDate() != null) {
            xml.addContent(new Element("deliveryDateWeek").setText(
                    DateFormatter.getWeekOfYear(sales.getRequest().getDeliveryDate())));
            xml.addContent(new Element("deliveryDate").setText(
                    DateFormatter.getDate(sales.getRequest().getDeliveryDate())));
        } else {
            xml.addContent(new Element("deliveryDateWeek"));
        }
        if (sales instanceof Project) {
            addProjectValues(xml, (Project) sales);
        }
    }

    private void addProjectValues(Element xml, Project project) {
        if (project.getRequest() instanceof ProjectRequest) {
            Date iDate = ((ProjectRequest) project.getRequest()).getInstallationDate();
            if (iDate != null) {
                xml.addContent(new Element("installationDateWeek").setText(
                        DateFormatter.getWeekOfYear(iDate)));
                xml.addContent(new Element("installationDate").setText(
                        DateFormatter.getDate(iDate)));
            } else {
                xml.addContent(new Element("installationDateWeek"));
                xml.addContent(new Element("installationDate"));
            }
        }
        if (project.getStartUp() == null) {
            xml.addContent(new Element("startUpDate"));
            xml.addContent(new Element("startUpYear"));
        } else {
            xml.addContent(new Element("startUpDate").setText(
                    DateFormatter.getDate(project.getStartUp())));
            xml.addContent(new Element("startUpYear").setText(
                    DateFormatter.getYear(project.getStartUp())));
        }
    }
    
    public Element createBranch(String elementName, Long id) {
        BranchOffice branch = branchOffices.getBranchOffice(id);
        if (!(branch instanceof XmlAwareEntity)) {
            log.warn("createBranch() branchOffice not xmlAware [class="
                    + branch.getClass().getName() + "]");
            return new Element(elementName);
        }
        return ((XmlAwareEntity) branch).getXML(elementName);
    }

    public Element createEmployee(String elementName, Long id) {
        return createEmployee(elementName, getEmployee(id));
    }

    public Element createEmployee(String elementName, Employee employee) {

        Element xml = new Element(elementName);
        if (employee == null) {
            return xml;
        }
        try {
            // id might be a user id so we check this first and lookup for
            // employee id if id is only user id
            String salutation = employee.getSalutation() == null ? null 
                    : employee.getSalutation().getDisplayNameLetterAddress();
            if (salutation != null) {
                xml.addContent(new Element("salutation").setText(salutation));
            } else {
                xml.addContent(new Element("salutation"));
            }
            xml.addContent(new Element("firstName").setText(employee.getFirstName()));
            xml.addContent(new Element("lastName").setText(employee.getLastName()));
            xml.addContent(new Element("name").setText(employee.getName()));
            if (employee.getInitials() != null) {
                xml.addContent(new Element("initials").setText(employee.getInitials()));
            } else {
                xml.addContent(new Element("initials"));
            }

            if (employee.getTitle() != null) {
                xml.addContent(new Element("title").setText(employee.getTitle().getName()));
            } else {
                xml.addContent(new Element("title"));
            }

            addPhone(xml, "phone", employee.getPhoneForRecord());
            addPhone(xml, "mobile", employee.getMobile());
            if (employee.getEmail() == null) {
                xml.addContent(new Element("email"));
            } else {
                xml.addContent(new Element("email").setText(employee.getEmail()));
            }
            xml.addContent(new Element("group").setText(employee.getSignature()));
        } catch (Throwable t) {
            log.warn("createEmployee() ignoring exception " + t.toString(), t);
            // we ignore this 'cause employee could be empty
        }
        return xml;
    }

    public Document getEmployees(Employee user) throws PermissionException {
        Document doc = new Document();
        Element root = new Element("root");
        root.addContent(new Element("currentDate").setText(DateFormatter.getCurrentDate()));
        try {
            root.addContent(new Element("currentUser").setText(user.getName()));
        } catch (Exception e) {
            log.warn("createXmlDocument() no domain user has logged in to current session");
            root.addContent(new Element("currentUser"));
        }
        Element result = new Element("employees");
        List<Employee> all = employees.findActiveEmployees();
        for (int i = 0, j = all.size(); i < j; i++) {
            Employee e = all.get(i);
            result.addContent(e.getXML());
        }
        root.addContent(result);
        doc.setRootElement(root);
        return doc;
    }

    public final String createString(boolean bool) {
        return Boolean.valueOf(bool).toString();
    }

    public final String createString(BigDecimal bd) {
        return (bd == null) ? "" : bd.toString();
    }

    @Override
    public final String createString(Double db) {
        return (db == null) ? "" : db.toString();
    }

    private Employee getEmployee(Long id) {
        if (id != null && id > 0) {
            try {
                return employees.get(id);
            } catch (Throwable t) {
                log.warn("getEmployee() failed [id=" + id + ", message=" + t.getMessage() + "]");
                // we ignore this 'cause employee could be empty
            }
        }
        return null;
    }

    private final void addPhone(Element xml, String name, Phone phone) {
        if (phone == null) {
            xml.addContent(new Element(name));
        } else {
            xml.addContent(new Element(name).setText(phone.getFormattedNumber()));
        }
    }
}
