/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2007 5:28:55 PM 
 * 
 */
package com.osserp.core.events;

import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventDisplay {

    private Long id = null;
    private String name = null;
    private Long salesId = null;
    private String salesName = null;
    private Long managerId = null;
    private String managerName = null;
    private boolean alert = false;
    private Long eventId = null;
    private Long eventType = null;
    private String eventName = null;
    private Long eventConfigId = null;
    private Date eventCreated = null;
    private Date eventExpires = null;
    private Long recipientId = null;
    private String recipientName = null;

    public EventDisplay(Long id, String name, Long salesId, String salesName, Long managerId, String managerName, boolean alert, Long eventId, Long eventType,
            String eventName, Long eventConfigId, Date eventCreated, Date eventExpires, Long recipientId, String recipientName) {
        super();
        this.id = id;
        this.name = name;
        this.salesId = salesId;
        this.salesName = salesName;
        this.managerId = managerId;
        this.managerName = managerName;
        this.alert = alert;
        this.eventId = eventId;
        this.eventType = eventType;
        this.eventName = eventName;
        this.eventConfigId = eventConfigId;
        this.eventCreated = eventCreated;
        this.eventExpires = eventExpires;
        this.recipientId = recipientId;
        this.recipientName = recipientName;
    }

    public boolean isAlert() {
        return alert;
    }

    public Long getEventConfigId() {
        return eventConfigId;
    }

    public Date getEventCreated() {
        return eventCreated;
    }

    public Date getEventExpires() {
        return eventExpires;
    }

    public Long getEventId() {
        return eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public Long getEventType() {
        return eventType;
    }

    public Long getId() {
        return id;
    }

    public Long getManagerId() {
        return managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public String getName() {
        return name;
    }

    public Long getRecipientId() {
        return recipientId;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public Long getSalesId() {
        return salesId;
    }

    public String getSalesName() {
        return salesName;
    }

    // display is readonly, setters are protected

    protected void setAlert(boolean alert) {
        this.alert = alert;
    }

    protected void setEventConfigId(Long eventConfigId) {
        this.eventConfigId = eventConfigId;
    }

    protected void setEventCreated(Date eventCreated) {
        this.eventCreated = eventCreated;
    }

    protected void setEventExpires(Date eventExpires) {
        this.eventExpires = eventExpires;
    }

    protected void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    protected void setEventName(String eventName) {
        this.eventName = eventName;
    }

    protected void setEventType(Long eventType) {
        this.eventType = eventType;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    protected void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    protected void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected void setRecipientId(Long recipientId) {
        this.recipientId = recipientId;
    }

    protected void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    protected void setSalesId(Long salesId) {
        this.salesId = salesId;
    }

    protected void setSalesName(String salesName) {
        this.salesName = salesName;
    }
}
