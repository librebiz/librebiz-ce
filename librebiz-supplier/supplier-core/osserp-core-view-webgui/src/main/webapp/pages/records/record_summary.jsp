<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="record" value="${sessionScope.recordView.record}"/>
<table class="recordSummary">
    <c:choose>
    <c:when test="${record.taxFree}">
        <tr>
            <td class="phead"><fmt:message key="amount"/> <span class="desc">(<fmt:message key="netTurnoverTaxGross"/>)</span></td>
			<td class="pnet"><o:number value="${record.amounts.amount}" format="currency"/></td>
			<td class="ptax"><o:number value="0.00" format="currency"/></td>
			<td class="pamount"><o:number value="${record.amounts.amount}" format="currency"/></td>
		</tr>
	</c:when>
	<c:otherwise>
		<tr>
			<td class="phead"><fmt:message key="amount"/> <span class="desc">(<fmt:message key="netTurnoverTaxGross"/>)</span></td>
			<td class="pnet"><o:number value="${record.amounts.amount}" format="currency"/></td>
			<td class="ptax"><o:number value="${record.amounts.taxAmount}" format="currency"/></td>
			<td class="pamount"><o:number value="${record.amounts.grossAmount}" format="currency"/></td>
		</tr>
	</c:otherwise>
	</c:choose>
	<tr>
		<td colspan="4" class="plast"></td>
	</tr>
</table>
