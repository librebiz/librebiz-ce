/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 18, 2008 2:50:00 PM 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;
import java.util.Map;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractOption;
import com.osserp.common.util.DateUtil;

import com.osserp.core.contacts.ContactUtil;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.FinanceRecordDisplay;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Records;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class FinanceRecordDisplayVO extends AbstractOption implements FinanceRecordDisplay {

    private RecordType recordType = null;
    private BookingType bookingType = null;
    
    private Long company = null;
    private Long branchId = null;
    
    private Long sales = null;
    private Long status = null;
    private Long currency = null;
    
    private boolean taxFree = false;
    private Long taxFreeId = null;
    
    private Double netAmount = null;
    private Double taxAmount = null;
    private Double reducedTaxAmount = null;
    private Double grossAmount = null;

    private Long contactId = null;
    private Long contactType = null;
    private String lastname = null;
    private String firstname = null;
    private String street = null;
    private String zipcode = null;
    private String city = null;
    
    private boolean exportIgnorable = false;
    private Long exportId;
    private Date exportDate;

    private boolean internal = false;
    private Long documentId = null;
    
    protected FinanceRecordDisplayVO() {
        super();
    }
    
    protected FinanceRecordDisplayVO(Double grossAmount) {
        super();
        this.grossAmount = grossAmount;
    }

    protected FinanceRecordDisplayVO(FinanceRecordDisplay o) {
        super(o);
        recordType = o.getRecordType();
        bookingType = o.getBookingType();
        company = o.getCompany();
        branchId = o.getBranchId();
        sales = o.getSales();
        contactId = o.getContactId();
        contactType = o.getContactType();
        status = o.getStatus();
        taxFree = o.isTaxFree();
        taxFreeId = o.getTaxFreeId();
        currency = o.getCurrency();
        netAmount = o.getNetAmount();
        taxAmount = o.getTaxAmount();
        reducedTaxAmount = o.getReducedTaxAmount();
        grossAmount = o.getGrossAmount();

        lastname = o.getLastname();
        firstname = o.getFirstname();
        street = o.getStreet();
        zipcode = o.getZipcode();
        city = o.getCity();
        
        exportId = o.getExportId();
        exportDate = o.getExportDate();
        exportIgnorable = o.isExportIgnorable();
        
        internal = o.isInternal();
        documentId = o.getDocumentId();
    }

    protected FinanceRecordDisplayVO(Map<String, Object> map) {
        super(map);
        recordType = (RecordType) map.get("recordType");
        if (recordType != null && recordType.isSupportingBookingTypes()) {
            bookingType = (BookingType) map.get("bookingType");
        }
        company = (Long) map.get("company");
        branchId = (Long) map.get("branchId");
        contactId = (Long) map.get("contactId");
        contactType = (Long) map.get("contactType");
        status = (Long) map.get("status");
        taxFree = fetchBoolean(map, "taxFree");
        taxFreeId = (Long) map.get("taxFreeId");
        currency = (Long) map.get("currency");
        if (currency == null) {
            currency = Constants.DEFAULT_CURRENCY;
        }
        netAmount = (Double) map.get("netAmount");
        taxAmount = (Double) map.get("taxAmount");
        reducedTaxAmount = (Double) map.get("reducedTaxAmount");
        grossAmount = (Double) map.get("grossAmount");
        sales = (Long) map.get("sales");
        lastname = (String) map.get("lastname");
        firstname = (String) map.get("firstname");
        street = (String) map.get("street");
        zipcode = (String) map.get("zipcode");
        city = (String) map.get("city");
        exportId = (Long) map.get("exportId");
        exportDate = (Date) map.get("exportDate");
        exportIgnorable = fetchBoolean(map, "exportIgnorable");
        internal = fetchBoolean(map, "internal");
        documentId = (Long) map.get("documentId");
    }

    public final String getNumber() {
        return Records.createNumber(recordType, getId(), internal);
    }

    public final String getReferenceNumber() {
        return Records.createReferenceNumber(recordType, getReference(), internal);
    }

    public Long getPrintId() {
        if (recordType != null && Records.isPayment(recordType.getId())) {
            return getReference();
        }
        return getId();
    }

    public String getTypeName() {
        return (recordType != null ? recordType.getResourceKey() : null);
    }

    public String getContextName() {
        return getTypeName();
    }

    public RecordType getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    public BookingType getBookingType() {
        return bookingType;
    }

    public void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public String getContactName() {
        return ContactUtil.createPrivateDisplayName(lastname, firstname);
    }

    public Long getCompany() {
        return company;
    }

    public void setCompany(Long company) {
        this.company = company;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getContactType() {
        return contactType;
    }

    public void setContactType(Long contactType) {
        this.contactType = contactType;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getCurrency() {
        return currency;
    }

    public void setCurrency(Long currency) {
        this.currency = currency;
    }

    public String getCurrencyKey() {
        return Constants.getDefaultCurrency(currency);
    }

    public String getCurrencySymbol() {
        return Constants.getCurrencySymbol(currency);
    }

    public Double getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(Double grossAmount) {
        this.grossAmount = grossAmount;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Long getSales() {
        return sales;
    }

    public void setSales(Long sales) {
        this.sales = sales;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    
    public boolean isTaxFree() {
        return taxFree;
    }

    public void setTaxFree(boolean taxFree) {
        this.taxFree = taxFree;
    }

    public Long getTaxFreeId() {
        return taxFreeId;
    }

    public void setTaxFreeId(Long taxFreeId) {
        this.taxFreeId = taxFreeId;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Double getReducedTaxAmount() {
        return reducedTaxAmount;
    }

    public void setReducedTaxAmount(Double reducedTaxAmount) {
        this.reducedTaxAmount = reducedTaxAmount;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public boolean isExportIgnorable() {
        return exportIgnorable;
    }

    public void setExportIgnorable(boolean exportIgnorable) {
        this.exportIgnorable = exportIgnorable;
    }

    public Long getExportId() {
        return exportId;
    }

    public void setExportId(Long exportId) {
        this.exportId = exportId;
    }

    public Date getExportDate() {
        return exportDate;
    }
    
    public void setExportDate(Date exportDate) {
        this.exportDate = exportDate;
    }

    public int getCreatedInYear() {
        return getCreated() == null ? 0 : DateUtil.getYear(getCreated());
    }

    public String[] getValues() {
        String[] r = new String[12];
        r[0] = createString(getId());
        r[1] = createString(company);
        r[2] = createString(contactId);
        r[3] = getContactName();
        r[4] = createString(street);
        r[5] = createString(zipcode);
        r[6] = createString(city);
        r[7] = createString(getReference());
        r[8] = createString(sales);
        r[9] = createDateForSheet(getCreated());
        r[10] = createDateForSheet(new Date());
        r[11] = createString(grossAmount);
        return r;
    }

    public String[] getValueKeys() {
        return new String[] { 
            "recordId",
            "companyId",
            "contactId",
            "contactName",
            "street",
            "zipcode",
            "city",
            "reference",
            "sales",
            "created",
            "maturity",
            "grossAmount" };
    }
}
