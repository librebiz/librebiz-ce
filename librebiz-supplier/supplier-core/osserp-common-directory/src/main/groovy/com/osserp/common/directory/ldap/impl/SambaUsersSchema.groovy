/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 */
package com.osserp.common.directory.ldap.impl

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.SubSchema

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class SambaUsersSchema extends AbstractLdapConfigAware implements SubSchema {

    private static final List OBJECT_CLASSES = ['sambaSamAccount']

    private static final String SAMBA_ACCOUNT_ENABLED = '[U]'
    protected static final String SAMBA_SID = 'S-1-5-21-3523851798-3655360463-2559965761-'
    private static final List AVAILABLE_ATTRIBUTES = [
        'sambasid',
        'sambaacctflags',
        'sambahomedrive',
        'sambakickofftime',
        'sambalmpassword',
        'sambalogofftime',
        'sambalogontime',
        'sambantpassword',
        'sambaprimarygroupsid',
        'sambapwdcanchange',
        'sambapwdmustchange'
    ]
    private static final List DISPLAY_ATTRIBUTES = []

    SambaUsersSchema(LdapConfig configInstance) {
        super(configInstance)
    }

    /**
     * Creates a new ldap ready user object by default and values provided by map
     * @param config with default values
     * @param values must contain at least uid and uidnumber
     * @return user object without dn
     */
    Map create(Map parent, Map values) {
        parent.sambasid = config.sambaSid + Long.valueOf(values.uidnumber * 2 + 1000).toString()
        parent.sambaacctflags = SAMBA_ACCOUNT_ENABLED
        parent.sambahomedrive = values.homedrive ?: "/home/${values.uid}"
        parent.sambakickofftime = config.sambaKickofftime
        parent.sambalogontime = config.sambaLogontime
        parent.sambalogofftime = config.sambaLogofftime
        parent.sambaprimarygroupsid = config.sambaGid
        parent.sambapwdcanchange = config.sambaPwdcanchange
        parent.sambapwdmustchange = config.sambaPwdmustchange
        return parent
    }

    List<String> getObjectClasses() {
        OBJECT_CLASSES
    }

    List<String> getAvailableAttributes() {
        AVAILABLE_ATTRIBUTES
    }


    Set<String> getAvailableArrays() {
        new HashSet<String>()
    }

    List<String> getDefaultDisplayAttributes() {
        DISPLAY_ATTRIBUTES
    }
}
