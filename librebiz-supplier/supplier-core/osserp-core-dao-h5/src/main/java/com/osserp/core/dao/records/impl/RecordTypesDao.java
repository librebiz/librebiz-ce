/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 5:46:14 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.RecordNumberConfig;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.BillingTypeImpl;
import com.osserp.core.model.records.RecordNumberConfigImpl;
import com.osserp.core.model.records.RecordTypeImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordTypesDao extends AbstractDao implements RecordTypes {
    private static Logger log = LoggerFactory.getLogger(RecordTypesDao.class.getName());
    private String billingTypeFcsRelationsTable = null;

    public RecordTypesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        billingTypeFcsRelationsTable = getTable(TableKeys.BILLING_TYPE_FCS_RELATIONS);
    }

    public List<BillingType> getBillingTypes() {
        return getCurrentSession().createQuery(
                "from " + BillingTypeImpl.class.getName() + " o order by o.id").list();
    }

    public BillingType loadBillingType(Long id) {
        return (BillingType) getCurrentSession().load(BillingTypeImpl.class, id);
    }

    public List<BillingType> getBillingTypes(Long billingClassId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(BillingTypeImpl.class.getName())
            .append(" o where o.billingClass.id = ")
            .append(billingClassId)
            .append(" order by o.id");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public List<BillingType> getBillingTypes(String contextName) {
        List<BillingType> result = new ArrayList<>();
        if (!isEmpty(contextName)) {
            StringBuilder hql = new StringBuilder("from ");
            hql
                .append(BillingTypeImpl.class.getName())
                .append(" o");
            List<BillingType> list = getCurrentSession().createQuery(hql.toString()).list();
            for (int i = 0, j = list.size(); i < j; i++) {
                BillingType next = list.get(i);
                if (next.getBillingClass() != null 
                    && contextName.equals(next.getBillingClass().getContextName())) {
                    result.add(next);
                }
            }
        }
        return CollectionUtil.sort(result, createBillingTypeByOrderIdComparator());
    }

    private Comparator createBillingTypeByOrderIdComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                BillingType bta = (BillingType) a;
                BillingType btb = (BillingType) b;
                Integer la = (bta == null || bta.getOrderId() == null) ? null : bta.getOrderId(); 
                Integer lb = (btb == null || btb.getOrderId() == null) ? null : btb.getOrderId(); 
                if (la == null && lb == null) {
                    return 0;
                }
                if (la == null) {
                    return 1;
                }
                if (lb == null) {
                    return -1;
                }
                return la.compareTo(lb);
            }
        };
    }

    public Map<Long, Long> findBillingFcsRelations() {
        Map<Long, Long> map = new HashMap<Long, Long>();
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT id,type_id FROM ")
                .append(billingTypeFcsRelationsTable);
        List<Long[]> list = (List<Long[]>) jdbcTemplate.query(
                sql.toString(), getBillingFcsRelationReader());
        for (int i = 0, j = list.size(); i < j; i++) {
            Long[] entry = list.get(i);
            map.put(entry[0], entry[1]);
        }
        return map;
    }

    public List<BillingType> getPaymentTypes() {
        // TODO replace 2 with constant 
        return getCurrentSession().createQuery(
                "from " + BillingTypeImpl.class.getName() 
                + " o where o.billingClass.id = 2 order by o.id").list();
    }

    public Map<Long, BillingType> getPaymentTypeMap() {
        List<BillingType> types = getPaymentTypes();
        Map<Long, BillingType> map = new HashMap<Long, BillingType>();
        for (int i = 0, j = types.size(); i < j; i++) {
            BillingType type = types.get(i);
            map.put(type.getId(), type);
        }
        return map;
    }

    public RecordType getRecordType(Long id) {
        return (RecordType) getCurrentSession().load(RecordTypeImpl.class.getName(), id);
    }

    public void save(RecordType recordType) {
        getCurrentSession().saveOrUpdate(RecordTypeImpl.class.getName(), recordType);
    }

    public List<RecordType> getRecordTypes() {
        List<RecordType> result = getCurrentSession().createQuery(
                "from " + RecordTypeImpl.class.getName()
                + " o order by o.id").list();
        // workaround; hibernate loads billingTypes after recordTypes
        for (Iterator<RecordType> i = result.iterator(); i.hasNext();) {
            RecordType next = i.next();
            if (next instanceof BillingType) {
                i.remove();
            }
        }
        return result;
    }

    public Map<Long, RecordType> getRecordTypeMap() {
        List<RecordType> types = getRecordTypes();
        Map<Long, RecordType> map = new HashMap<Long, RecordType>();
        for (int i = 0, j = types.size(); i < j; i++) {
            RecordType type = types.get(i);
            map.put(type.getId(), type);
        }
        return map;
    }
    
    public List<RecordNumberConfig> getRecordNumberConfigs(Long company) {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(RecordNumberConfigImpl.class.getName()).append(" o");
        if (!isEmpty(company)) {
            hql.append(" where o.company = ").append(company);
        }
        hql.append(" order by o.id desc");
        return getCurrentSession().createQuery(hql.toString()).list();
    }
    
    public RecordNumberConfig getRecordNumberConfig(RecordType type, Long company) {
        if (type == null) {
            return null;
        }
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(RecordNumberConfigImpl.class.getName())
            .append(" o where o.reference = ")
            .append(type.getId());
        if (!isEmpty(company)) {
            hql.append(" and o.company = ").append(company);
        }
        hql.append(" order by o.id desc");
        List<RecordNumberConfig> result = getCurrentSession().createQuery(hql.toString()).list();
        return result.isEmpty() ? null : result.get(0);
    }
    
    public RecordNumberConfig createRecordNumberConfig(
            RecordType type, 
            Long company,
            boolean yearly,
            int digits,
            Long initialSequenceValue,
            Long defaultSequenceValue) {
        
        String sequenceName = createSequenceName(type, company);
        Long value = isEmpty(initialSequenceValue) ? defaultSequenceValue : initialSequenceValue;
        if (!sequenceExists(sequenceName)) {
            createSequence(sequenceName, value);
        }
        RecordNumberConfigImpl obj = new RecordNumberConfigImpl(
                type, 
                company,
                sequenceName,
                false, // dedicated
                digits,
                yearly, 
                null, // start (opt. start year if yearly, null == current)
                defaultSequenceValue);  
        save(obj);
        return obj;
    }
    
    public void createOrRestartSequence(RecordNumberConfig config, Long newValue) {
        String sequenceName = config.getName();
        if (isEmpty(sequenceName)) {
            RecordType type = getRecordType(config.getReference());
            sequenceName = createSequenceName(type, config.getCompany());
            config.setName(sequenceName);
            save(config);
        }
        Long value = isEmpty(newValue) ? config.getDefaultNumber() : newValue;
        if (!sequenceExists(sequenceName)) {
            createSequence(sequenceName, value);
        } else if (!isEmpty(value)) {
            restart(config, value);
        }
    }
    
    public void createOrRestartSequence(RecordType type, Long company, Long newValue) {
        RecordNumberConfig config = getRecordNumberConfig(type, company);
        String sequenceName = config.getName();
        if (isEmpty(sequenceName)) {
            sequenceName = createSequenceName(type, config.getCompany());
            config.setName(sequenceName);
            save(config);
        }
        Long value = isEmpty(newValue) ? config.getDefaultNumber() : newValue;
        if (!sequenceExists(sequenceName)) {
            createSequence(sequenceName, value);
        } else if (!isEmpty(value)) {
            restart(config, value);
        }
    }
    
    protected String createSequenceName(RecordType type, Long company) {
        StringBuilder buffer = new StringBuilder();
        if (type.getTableName() != null) {
            buffer.append(type.getTableName());
        } else if (type.getResourceKey() != null) {
            buffer.append(type.getResourceKey());
        } else {
            buffer.append("record_").append(type.getId());
        }
        if (!isEmpty(company)) {
            buffer.append("_").append(company);
        }
        buffer.append("_id_seq");
        return buffer.toString();
    }

    public void save(RecordNumberConfig config) {
        getCurrentSession().saveOrUpdate(RecordNumberConfigImpl.class.getName(), config);
    }
    
    public void restart(RecordNumberConfig config, Long startValue) {
        if (config != null && config.getName() != null && startValue != null) {
            int i = restartSequence(config.getName(), startValue);
            if (log.isDebugEnabled()) {
                log.debug("restart() done [sequence=" + config.getName()
                        + ", value=" + startValue + ", success=" + (i>0) + "]");
            }
        }
    }

    public void updateSequenceDedicatedFlag(RecordType type, Long company, boolean dedicated) {
        RecordNumberConfig config = getRecordNumberConfig(type, company);
        config.setDedicated(dedicated);
        save(config);
    }

    private RowMapperResultSetExtractor getBillingFcsRelationReader() {
        return new RowMapperResultSetExtractor(new BillingFcsRelationRowMapper());
    }

    private class BillingFcsRelationRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new Long[] { Long.valueOf(rs.getLong(1)), Long.valueOf(rs.getLong(2)) };
        }
    }
}
