/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2020
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.Plugin;
import com.osserp.core.PluginMapping;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PluginStore {

    List<Plugin> findAll();

    Plugin findById(Long id);

    Plugin findByName(String name);

    /**
     * Persists current state of the plugin
     * @param plugin
     * @return plugin object with updated values
     */
    Plugin update(Plugin plugin);

    /**
     * Provides an existing mapping
     * @param pluginId
     * @param name logical name of the mapped property
     * @param entityId primary key of the mapped core entity
     * @return mapping or null if no such mapping exists
     */
    PluginMapping getMapping(Long pluginId, String name, Long entityId);

    /**
     * Creates an entity mapping
     * @param pluginId
     * @param name
     * @param mappedId
     * @param objectId
     * @param objectName
     * @param objectType
     * @param objectStatus
     * @return mapping object
     */
    PluginMapping createMapping(
            Long pluginId,
            String name,
            Long mappedId,
            String objectId,
            String objectName,
            String objectType,
            String objectStatus);

    /**
     * Deletes an existing mapping
     * @param mapping
     */
    void deleteMapping(PluginMapping mapping);

    /**
     * Updates mapping
     * @param mapping
     * @return mapping object with updated values
     */
    PluginMapping updateMapping(PluginMapping mapping);

    /**
     * Updates mapping status
     * @param pluginId
     * @param name
     * @param mappedId
     * @param status
     * @return mapping object with updated status
     */
    PluginMapping updateMapping(Long pluginId, String name, Long mappedId, String status);
}
