/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2009 12:38:16 PM 
 * 
 */
package com.osserp.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.util.DateUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.BusinessTypeContext;
import com.osserp.core.BusinessTypeManager;
import com.osserp.core.BusinessTypeSelection;
import com.osserp.core.ContractType;
import com.osserp.core.Options;
import com.osserp.core.dao.BusinessTypes;
import com.osserp.core.dao.DefaultPaymentAgreements;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.dao.RequestFcsActions;
import com.osserp.core.dao.SalesContracts;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.model.RequestTypeImpl;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessTypeManagerImpl extends AbstractService implements BusinessTypeManager {
    private static Logger log = LoggerFactory.getLogger(BusinessTypeManagerImpl.class.getName());

    private BusinessTypes businessTypes = null;
    private FlowControlActions flowControlActions = null;
    private RequestFcsActions requestFcsActions = null;
    private SalesContracts salesContracts = null;

    private OptionsCache optionsCache = null;
    private DefaultPaymentAgreements defaultPaymentAgreements = null;

    protected BusinessTypeManagerImpl(
            BusinessTypes businessTypes,
            FlowControlActions flowControlActions,
            RequestFcsActions requestFcsActions,
            SalesContracts salesContracts,
            OptionsCache optionsCache,
            DefaultPaymentAgreements defaultPaymentAgreements) {
        super();
        this.businessTypes = businessTypes;
        this.flowControlActions = flowControlActions;
        this.requestFcsActions = requestFcsActions;
        this.salesContracts = salesContracts;
        this.optionsCache = optionsCache;
        this.defaultPaymentAgreements = defaultPaymentAgreements;
    }

    public List<BusinessType> findActive() {
        List<BusinessType> result = new ArrayList<>();
        List<BusinessType> types = businessTypes.findAll();
        BusinessType interestOnly = null; 
        for (int i = 0, j = types.size(); i < j; i++) {
            BusinessType next = types.get(i);
            if (next.isActive()) {
                if (next.isRequestContextOnly()) {
                    interestOnly = next;
                } else {
                    result.add(next);
                }
            }
        }
        if (interestOnly != null) {
            result.add(0, interestOnly);
        }
        return result;
    }

    public BusinessType load(Long id) {
        return businessTypes.load(id);
    }

    public List<BusinessType> findAll() {
        return businessTypes.findAll();
    }

    public List<BusinessTypeContext> findByRequestContext() {
        return findContexts(false);
    }

    public List<BusinessTypeContext> findBySalesContext() {
        return findContexts(true);
    }

    public BusinessType findDefaultType(String contextName) {
        return businessTypes.findBySystemProperty(contextName);
    }

    public BusinessTypeSelection findTypeSelection(String name) {
        return businessTypes.findSelection(name);
    }

    public Long getIdSuggestion() {
        return businessTypes.findNextId();
    }

    public BusinessType create(
            Employee user,
            Long id,
            Long company,
            String name,
            String key,
            BusinessType source) throws ClientException {

        if (log.isDebugEnabled()) {
            log.debug("create() invoked [id=" + id
                    + ", company=" + company
                    //+ ", startId=" + startId
                    + ", name=" + name
                    + ", key=" + key
                    + ", source=" + (source == null ? "null" : source.getId())
                    + "]");
        }
        if (source == null) {
            throw new ClientException(ErrorCode.TEMPLATE_REQUIRED);
        }
        if (isNotSet(id)) {
            throw new ClientException(ErrorCode.ID_REQUIRED);
        }
        if (businessTypes.exists(id)) {
            throw new ClientException(ErrorCode.ID_EXISTS);
        }
        if (isNotSet(company)) {
            throw new ClientException(ErrorCode.COMPANY_MISSING);
        }
        if (isNotSet(key)) {
            throw new ClientException(ErrorCode.SHORTKEY_MISSING);
        }
        if (key.length() > 3) {
            throw new ClientException(ErrorCode.SHORTKEY_MAXLENGTH);
        }
        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        List<BusinessType> existing = findAll();
        for (int i = 0, j = existing.size(); i < j; i++) {
            BusinessType next = existing.get(i);
            if (next.getName().equalsIgnoreCase(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
            if (next.getKey().equalsIgnoreCase(key)) {
                throw new ClientException(ErrorCode.SHORTKEY_EXISTS);
            }
        }
        BusinessType created = new RequestTypeImpl(
                id,
                source.getWorkflow(),
                name,
                key,
                source.getContext(),
                source.getSalesContext(),
                company,
                true,
                source.isRequestContextOnly(),
                source.isDirectSales(),
                source.isTrading(),
                source.isPackageRequired(),
                source.isInterestContext(),
                source.isSupportingDownpayments(),
                source.isSupportingPictures(),
                source.isSupportingOffers(),
                source.isCreateSla(),
                source.isIncludePriceByDefault(),
                source.isDummy(),
                source.isService(),
                source.getCommissionType(),
                source.getCapacityFormat(),
                source.getCapacityUnit(),
                source.getCapacityLabel(),
                source.getCalculationConfig());
        businessTypes.save(created);
        if (!created.isDirectSales()) {
            requestFcsActions.createActions(user, source, created);
        }
        if (!created.isRequestContextOnly()) {
            flowControlActions.createActions(user, source, created);
        }
        PaymentAgreement pa = defaultPaymentAgreements.createDefaultPaymentAgreement(user, created);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + created.getId()
                    + ", name=" + name
                    + ", defaultPaymentAgreement=" + pa.getId()
                    + "]");
        }
        optionsCache.refresh(Options.REQUEST_TYPES);
        optionsCache.refresh(Options.REQUEST_TYPE_KEYS);
        return created;
    }

    public List<BusinessType> createTypeSelection(DomainUser user, boolean switchRequest) {
        List<BusinessType> list = findActive();
        List<BusinessType> result = new ArrayList<>();
        for (int i = 0, j = list.size(); i < j; i++) {
            BusinessType bt = list.get(i);
            if (!bt.isDummy()
                    && (user.isExecutive()
                        || user.isFromHeadquarter()
                        || user.isSupportingType(bt)
                        || user.isGlobalBusinessCaseAccessGrant())
                        && (!switchRequest || (switchRequest && !bt.isDirectSales()))) {
                result.add(bt);
            }
        }
        return result;
    }

    public List<BusinessType> createSearchSelection(DomainUser user, boolean requestContext) {
        List<BusinessType> list = findActive();
        List<BusinessType> result = new ArrayList<>();
        for (int i = 0, j = list.size(); i < j; i++) {
            BusinessType bt = list.get(i);
            if (!bt.isDummy() 
                    && !(!requestContext && bt.isRequestContextOnly())
                    && user.isSupportingType(bt)) {
                result.add(bt);
            }
        }
        return result;
    }

    public void update(
            BusinessType type,
            String name,
            String key,
            String context,
            String salesContext,
            boolean active,
            boolean requestContextOnly,
            boolean directSales,
            boolean trading,
            boolean packageRequired,
            boolean interestContext,
            boolean salesPersonByCollector,
            boolean supportingDownpayments,
            boolean supportingPictures,
            boolean supportingOffers,
            boolean createSla,
            boolean includePriceByDefault,
            boolean dummy,
            boolean service,
            boolean subscription,
            boolean wholeSale,
            String commissionType,
            String capacityFormat,
            String capacityUnit,
            String capacityLabel,
            String language,
            boolean recordByCalculation,
            Long calculationConfig,
            Long requestLetterType,
            Long salesLetterType,
            boolean installationDateSupported,
            boolean startupSupported,
            boolean externalIdProvided,
            String externalStatusQualifiedRequestKeys, 
            String externalStatusSalesKeys,
            String externalStatusSalesClosedKey,
            Long defaultContractType,
            Long defaultOrigin,
            Long defaultOriginType) throws ClientException {

        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (isNotSet(key)) {
            throw new ClientException(ErrorCode.SHORTKEY_MISSING);
        }
        if (key.length() > 3) {
            throw new ClientException(ErrorCode.SHORTKEY_MAXLENGTH);
        }
        List<BusinessType> existing = findAll();
        for (int i = 0, j = existing.size(); i < j; i++) {
            BusinessType next = existing.get(i);
            if (!next.getId().equals(type.getId()) && next.getName().equalsIgnoreCase(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
            if (!next.getId().equals(type.getId()) && next.getKey().equalsIgnoreCase(key)) {
                throw new ClientException(ErrorCode.SHORTKEY_EXISTS);
            }
        }
        type.update(
                name,
                key,
                context,
                salesContext,
                active,
                requestContextOnly,
                directSales,
                trading,
                packageRequired,
                interestContext,
                salesPersonByCollector,
                supportingDownpayments,
                supportingPictures,
                supportingOffers,
                createSla,
                includePriceByDefault,
                dummy,
                service,
                subscription,
                wholeSale,
                commissionType,
                capacityFormat,
                capacityUnit,
                capacityLabel,
                language,
                recordByCalculation,
                calculationConfig,
                requestLetterType,
                salesLetterType,
                installationDateSupported,
                startupSupported,
                externalIdProvided,
                externalStatusQualifiedRequestKeys, 
                externalStatusSalesKeys,
                externalStatusSalesClosedKey,
                defaultContractType,
                defaultOrigin,
                defaultOriginType);
        businessTypes.save(type);
        optionsCache.refresh(Options.REQUEST_TYPES);
        optionsCache.refresh(Options.REQUEST_TYPE_KEYS);
    }

    public PaymentAgreement getDefaultPaymentAgreement(BusinessType type) {
        return defaultPaymentAgreements.find(type);
    }

    public List<PaymentAgreement> getDefaultPaymentAgreements() {
        return defaultPaymentAgreements.findAll();
    }

    public void updateDefaultPaymentAgreement(Employee user, PaymentAgreement paymentAgreement)
            throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("updateDefaultPaymentAgreement() invoked "
                    + "[downpayment=" + paymentAgreement.getDownpaymentPercent()
                    + ", delivery=" + paymentAgreement.getDeliveryInvoicePercent()
                    + ", final=" + paymentAgreement.getFinalInvoicePercent()
                    + ", valid=" + paymentAgreement.isPaymentStepsValid()
                    + "]");
        }
        if (!paymentAgreement.isPaymentStepsValid()) {
            throw new ClientException(ErrorCode.PAYMENT_STEPS_INVALID);
        }
        paymentAgreement.setChanged(DateUtil.getCurrentDate());
        paymentAgreement.setChangedBy(user.getId());
        defaultPaymentAgreements.save(paymentAgreement);
    }

    private List<BusinessTypeContext> findContexts(boolean salesContext) {
        List<BusinessTypeContext> result = new ArrayList<>();
        List<BusinessTypeContext> list = businessTypes.findContexts();
        for (int i = 0, j = list.size(); i < j; i++) {
            BusinessTypeContext next = list.get(i);
            if ((salesContext && next.isSalesContext())
                    || (!salesContext && !next.isSalesContext())) {
                result.add(next);
            }
        }
        return result;
    }

    public List<ContractType> getContractTypes() {
        return salesContracts.getTypesList();
    }

}
