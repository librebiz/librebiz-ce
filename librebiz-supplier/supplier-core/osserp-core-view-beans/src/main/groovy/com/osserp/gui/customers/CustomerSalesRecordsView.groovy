/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 6, 2014 
 * 
 */
package com.osserp.gui.customers

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.customers.Customer
import com.osserp.core.sales.SalesQueryManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class CustomerSalesRecordsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(CustomerSalesRecordsView.class.getName())

    Customer customer
    Double accountBalance = 0

    CustomerSalesRecordsView() {
        providesCreateMode()
        enableAutoreload()
        dependencies = ['contactView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            customer = env.contactView?.customer
            if (!customer) {
                logger.warn("initRequest: customer not found, invoke with 'recipient' param to provide the supplier.id]")
                throw new ViewContextException('supplier missing')
            }
            reload()
            logger.debug("initRequest: done [context=forward, recordCount=${list.size()}]")
        }
    }

    @Override
    void reload() {
        list = salesQueryManager.getSummary(customer)
        accountBalance = salesQueryManager.getAccountBalance(customer)
    }

    @Override
    protected void createCustomNavigation() {
        if (customer?.grantContactNone) {
            nav.listNavigation = nav.beanNavigation
        }
    }

    protected SalesQueryManager getSalesQueryManager() {
        getService(SalesQueryManager.class.getName())
    }
}
