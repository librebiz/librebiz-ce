<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.unreleasedDeliveriesView}"/>
<c:choose>
	<c:when test="${view.summaryMode}">
		<c:set var="records" value="${view.summary}"/>
	</c:when>
	<c:otherwise>
		<c:set var="records" value="${view.list}"/>
	</c:otherwise>
</c:choose>
<c:set var="displayPrice" value="false"/>
<o:permission role="purchasing,accounting,executive" info="permissionOpenDeliveryItemListDisplayPreis">
	<c:set var="displayPrice" value="true"/>
</o:permission>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="headline">
		<c:choose>
			<c:when test="${view.productMode}">
				<o:listSize value="${records}"/> <fmt:message key="notReleasedDeliveryNotesTo"/> <o:out value="${view.product.name}"/>
			</c:when>
			<c:when test="${view.summaryMode}">
				<o:listSize value="${records}"/> <fmt:message key="notReleasedDeliveryPositionsAfterProduct"/>
			</c:when>
			<c:otherwise>
				<o:listSize value="${view.list}"/> <fmt:message key="notReleasedDeliveryPositions"/>
			</c:otherwise>
		</c:choose>
	</tiles:put>
	<tiles:put name="headline_right">
		<ul>
			<li><a href="<c:url value="/salesDeliveryUnreleased.do?method=exit"/>"><o:img name="backIcon"/></a></li>
			<c:if test="${!view.productMode}">
				<li><a href="<c:url value="/salesDeliveryUnreleased.do?method=changeSummaryMode"/>"><o:img name="replaceIcon"/></a></li>
			</c:if>
			<li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
		</ul>
	</tiles:put>

	<tiles:put name="content" type="string">
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<c:choose>
					<c:when test="${view.productMode}">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="number"><fmt:message key="number"/></th>
                                        <th class="company"><fmt:message key="consignment"/></th>
                                        <th class="created"><fmt:message key="from"/></th>
                                        <th class="quantity"><span title="<fmt:message key="altogetherOrderedQuantity"/>"><fmt:message key="quantity"/></span></th>
                                    </tr>
                                </thead>
                                <tbody style="height: 550px;">
                                    <c:choose>
                                        <c:when test="${empty records}">
                                            <tr><td colspan="4" style="width: 789px;"><fmt:message key="noDeliveryNotesFound"/></td></tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach var="record" items="${records}" varStatus="s">
                                                <tr>
                                                    <td><a href="<c:url value="/loadSales.do"><c:param name="exit" value="openDeliveries"/><c:param name="id" value="${record.salesId}"/></c:url>"><o:out value="${record.salesId}"/></a></td>
                                                    <td><o:out value="${record.salesName}"/></td>
                                                    <td><o:date value="${record.created}"/></td>
                                                    <td class="quantity"><o:out value="${record.quantity}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </tbody>
                            </table>
                        </div>
					</c:when>
					<c:when test="${view.summaryMode}">
						<div class="table-responsive table-responsive-default">
							<table class="table table-striped">
								<thead>
									<tr>
										<th style="width: 60px;"><fmt:message key="product"/></th>
										<th style="width: 352px;"><fmt:message key="name"/></th>
										<th style="width: 80px;" class="right"><span title="<fmt:message key="accordingDeliveryNoteToDeliverQuantity"/>"><fmt:message key="deliveryNotes"/></span></th>
										<th style="width: 80px;" class="right"><span title="<fmt:message key="currentlyInInventory"/>"><fmt:message key="inventory"/></span></th>
										<th style="width: 82px;" class="right"><span title="<fmt:message key="locatedInStockReceipt"/>"><fmt:message key="stockReceipt"/></span></th>
										<th style="width: 80px;" class="right"><span title="<fmt:message key="orderedBySupplier"/>"><fmt:message key="appointed"/></span></th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty records}">
											<tr><td colspan="6" style="width: 789px;"><fmt:message key="noDeliveryNotesFound"/></td></tr>
										</c:when>
										<c:otherwise>
											<c:forEach var="record" items="${records}" varStatus="s">
												<tr>
													<td style="width: 60px;">
														<o:popupSelect action="/productPopup.do" name="record" property="productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
															<o:out value="${record.productId}"/>
														</o:popupSelect>
													</td>
													<td style="width: 352px;"><o:out value="${record.productName}"/></td>
													<td style="width: 80px;" class="right"><o:number format="decimal" value="${record.quantity}"/></td>
													<td style="width: 80px;" class="right"><o:number format="decimal" value="${record.stock}"/></td>
													<td style="width: 82px;" class="right"><o:number format="decimal" value="${record.receipt}"/></td>
													<td style="width: 80px;" class="right"><o:number format="decimal" value="${record.expected}"/></td>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
					</c:when>
					<c:otherwise>
						<div class="table-responsive table-responsive-default">
							<table class="table table-striped">
								<thead>
									<tr>
										<th style="width: 60px;"><fmt:message key="product"/></th>
										<th style="width: 248px;"><fmt:message key="name"/></th>
										<th style="width: 60px;"><fmt:message key="number"/></th>
										<th style="width: 249px;"><fmt:message key="consignment"/></th>
										<th style="width: 62px;"><span title="<fmt:message key="dateOfDeliveryNote"/>"><fmt:message key="from"/></span></th>
										<th style="width: 55px;" class="right"><span title="<fmt:message key="quantityToDeliver"/>"><fmt:message key="quantity"/></span></th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty records}">
											<tr><td colspan="6" style="width: 789px;"><fmt:message key="noDeliveryNotesFound"/></td></tr>
										</c:when>
										<c:otherwise>
											<c:forEach var="record" items="${records}" varStatus="s">
												<tr>
													<td style="width: 60px;">
														<o:popupSelect action="/productPopup.do" name="record" property="productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
															<o:out value="${record.productId}"/>
														</o:popupSelect>
													</td>
													<td style="width: 248px;"><o:out value="${record.productName}"/></td>
													<td style="width: 60px;"><a href="<c:url value="/loadSales.do?exit=openDeliveries&id=${record.salesId}"/>"><o:out value="${record.salesId}"/></a></td>
													<td style="width: 249px;"><o:out value="${record.salesName}"/></td>
													<td style="width: 62px;"><o:date value="${record.delivery}"/></td>
													<td style="width: 55px;" class="right"><o:number format="decimal" value="${record.quantity}" /></td>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
