<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.id}"/> - <o:out value="${view.bean.name}"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name"><fmt:message key="name" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="name" value="${view.bean.name}" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="shortkey"><fmt:message key="shortkey" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="key" value="${view.bean.key}" styleClass="form-control shortname" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="warning" class="boldtext"><fmt:message key="warningNotification" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <fmt:message key="changeSystemSettingWarning" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="requestContext"><fmt:message key="requestContextLabel"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="requestContext" options="${view.requestContexts}" value="${view.bean.context}" valueProperty="name" styleClass="form-control" prompt="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="salesContext"><fmt:message key="orderContextLabel"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="salesContext" options="${view.salesContexts}" value="${view.bean.salesContext}" valueProperty="name" styleClass="form-control" prompt="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="requestContextExclusiveLabel"><fmt:message key="requestContextLabel"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="requestContextOnly">
                                <v:checkbox name="requestContextOnly" value="${view.bean.requestContextOnly}" />
                                <span class="tab smalltext">(<fmt:message key="inThisContextValidOnly" />)</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="interestOnly"><fmt:message key="interestOnly"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="interestContext">
                                <v:checkbox name="interestContext" value="${view.bean.interestContext}" />
                                <span class="tab smalltext">(<fmt:message key="unspecifiedYet" />)</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="directSales"><fmt:message key="directSales"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="directSales">
                                <v:checkbox name="directSales" value="${view.bean.directSales}" />
                                <span class="tab smalltext">(<fmt:message key="requestJumpOver" />)</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="salesPersonAssignment"><fmt:message key="salesPersonAssignment"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="salesPersonByCollectorCheckbox">
                                <v:checkbox name="salesPersonByCollector" value="${view.bean.salesPersonByCollector}" />
                                <span class="tab smalltext">(<fmt:message key="salesPersonByCollector" />)</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="serviceOrder"><fmt:message key="serviceOrder"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="serviceOrderCheckbox">
                                <v:checkbox name="service" value="${view.bean.service}" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="subscription"><fmt:message key="subscription"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="subscriptionCheckbox">
                                <v:checkbox name="subscription" value="${view.bean.subscription}" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="packageRequired"><fmt:message key="packageRequired"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="packageRequiredCheckbox">
                                <v:checkbox name="packageRequired" value="${view.bean.packageRequired}" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="trading"><fmt:message key="trading"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="tradingCheckbox">
                                <v:checkbox name="trading" value="${view.bean.trading}" /> <span class="tab smalltext">(<fmt:message key="noProject" />)</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="wholesale"><fmt:message key="wholesale"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="wholeSaleCheckbox">
                                <v:checkbox name="wholeSale" value="${view.bean.wholeSale}" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="offerCreation"><fmt:message key="offerCreation"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="supportingOffersCheckbox">
                                <v:checkbox name="supportingOffers" value="${view.bean.supportingOffers}" /> <span class="tab smalltext">(<fmt:message key="possible" />)</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="downpayments"><fmt:message key="downpayments"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="supportingDownpaymentsCheckbox">
                                <v:checkbox name="supportingDownpayments" value="${view.bean.supportingDownpayments}" /> <span class="tab smalltext">(<fmt:message key="possible" />)</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="pictureUpload"><fmt:message key="pictureUpload"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="supportingPicturesCheckbox">
                                <v:checkbox name="supportingPictures" value="${view.bean.supportingPictures}" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="priceDisplayOn"><fmt:message key="priceDisplayOn"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="includePriceByDefaultCheckbox">
                                <v:checkbox name="includePriceByDefault" value="${view.bean.includePriceByDefault}" /> <span class="tab smalltext">(<fmt:message key="calculationContext" />)</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <oc:systemPropertyEnabled name="businessCaseInstallationSupport">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="installationDate"><fmt:message key="installationDate"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="installationDateCheckbox">
                                    <v:checkbox name="installationDateSupported" value="${view.bean.installationDateSupported}" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </oc:systemPropertyEnabled>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recordByCalculationLabel"><fmt:message key="recordByCalculationLabel"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="recordByCalculationCheckbox">
                                <v:checkbox name="recordByCalculation" value="${view.bean.recordByCalculation}" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="calculationConfig"><fmt:message key="calculationConfig"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="calculationConfig" options="${view.calculationConfigs}" value="${view.bean.calculationConfig}" styleClass="form-control" prompt="false" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="contractType"><fmt:message key="contractType"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="defaultContractType" options="${view.contractTypes}" value="${view.bean.defaultContractType}" styleClass="form-control" prompt="true" promptKey="default"  />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="externalIdProvided"><fmt:message key="externalIdProvided"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="externalIdProvidedCheckbox">
                                <v:checkbox name="externalIdProvided" value="${view.bean.externalIdProvided}" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="requestType" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="defaultOriginType" styleClass="form-control" options="requestOriginTypes" value="${view.bean.defaultOriginType}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="origin" /></label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <div class="form-value">
                            <c:choose>
                                <c:when test="${empty view.bean.defaultOrigin}">
                                    <v:ajaxLink linkId="campaignSelectionView" url="/crm/campaignSelection/forward?view=${view.name}&company=${view.user.employee.defaultRoleConfig.branch.company.id}">
                                        <fmt:message key="none" />
                                    </v:ajaxLink>
                                </c:when>
                                <c:otherwise>
                                    <v:ajaxLink linkId="campaignSelectionView" url="/crm/campaignSelection/forward?view=${view.name}&company=${view.user.employee.defaultRoleConfig.branch.company.id}&id=${view.bean.defaultOrigin}">
                                        <oc:options name="campaigns" value="${view.bean.defaultOrigin}" />
                                        <input type="hidden" name="defaultOrigin" value="${view.bean.defaultOrigin}" />
                                    </v:ajaxLink>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <c:if test="${!empty view.bean.defaultOrigin}">
                            <v:link url="${view.baseLink}/resetDefaultOrigin" title="removeSelected">
                                <o:img name="deleteIcon" />
                            </v:link>
                        </c:if>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/disableEditMode" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
