/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Oct-2006 18:44:51 
 * 
 */
package com.osserp.gui.web.projects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.web.RequestUtil;

import com.osserp.core.customers.Customer;
import com.osserp.core.sales.Sales;
import com.osserp.core.users.DomainUser;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.contacts.CustomerView;
import com.osserp.gui.common.UserUtil;
import com.osserp.gui.web.sales.SalesAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectAction extends SalesAction {
    private static Logger log = LoggerFactory.getLogger(ProjectAction.class.getName());

    /**
     * Returns from project display. Destroys view and forwards views exitTarget if exists. Otherwise if an event or customer view is present forward will be
     * 'events' or 'customer'. If none of the above matches we forward to static target 'exit'.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        DomainUser user = UserUtil.getDomainUser(session);
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + user.getId());
        }
        // first we remove the project
        BusinessCaseView view = getBusinessCaseView(session);
        String exitTarget = view.getExitTarget();
        if (exitTarget != null) {
            String exitId = view.getExitId();
            RequestUtil.saveExitId(request, exitId);
            if (log.isDebugEnabled()) {
                log.debug("exit() found exit [target=" + exitTarget + ", exitId=" + exitId + "]");
            }
            removeBusinessCaseView(session);
            return mapping.findForward(exitTarget);
        }
        Sales sale = view.getSales();
        removeBusinessCaseView(session);
        // check wether we come from a customer summary display page
        CustomerView cv = (CustomerView) fetchView(session, CustomerView.class);
        if (cv != null && cv.getSummary() != null) {
            if (((Customer) cv.getBean()).getId().equals(
                    sale.getRequest().getCustomer().getId())) {
                if (log.isDebugEnabled()) {
                    log.debug("exit() found customer with summary,\nforwarding to "
                            + Actions.CUSTOMER);
                }
                cv.refreshSummary();
                return mapping.findForward(Actions.CUSTOMER);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("exit() collection found, examining");
        }
        return mapping.findForward(Actions.EXIT);
    }
}
