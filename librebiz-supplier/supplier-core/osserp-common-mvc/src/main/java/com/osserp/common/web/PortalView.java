/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 12, 2007 1:37:15 PM 
 * 
 */
package com.osserp.common.web;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.gui.HelpLink;
import com.osserp.common.gui.Menu;
import com.osserp.common.service.SysInfo;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("portalView")
public interface PortalView extends LoginView, View {

    public static final String ADMIN_MENU = "admin";
    public static final String HOME_MENU = "home";
    public static final String SHARED_MENU = "sharedMenu";

    User login(String username, String remoteIp) throws PermissionException;

    /**
     * Provides users currently online
     * @return onlineUsers
     */
    List<User> getOnlineUsers();

    /**
     * Indicates backend availability
     * @return true if backend available
     */
    boolean isBackendAvailable();
    
    /**
     * Provides an error message to display if backend is unavailable
     * @return backendErrorMessage or null if not present
     */
    String getBackendErrorMessage();

    /**
     * Indicates if logged in user was authenticated by external service (provided by sso service)
     * @return externalAuthentication
     */
    boolean isExternalAuthentication();

    /**
     * Call setup module if true
     * @return systemStupRequired
     */
    boolean isSystemSetupRequired();

    /**
     * Indicates if system setup mode is enabled
     * @return systemSetupMode
     */
    boolean isSystemSetupMode();
    
    /**
     * Indicates enabled exclusive setup mode. This indicator is mostly used
     * to decide if other than setup navigation is available.
     * 
     * The default implementation reports true if setup menu is enabled, the 
     * logged in user is admin and users startup target points to setup itself.
     * In this case it doesn't make sense to provide a link from setup to home.
     * 
     * @return true if admin users startup target is setup and admin logged in 
     */
    boolean isSystemSetupOnly();
    
    /**
     * Provides the system setup target.
     * @return systemSetupTarget
     */
    String getSystemSetupTarget();
    
    /**
     * Provides the system setup target forward. Note: Do not invoke this 
     * method if systemSetupRequired does not return true. A RuntimeException
     * will be thrown if no setup target exists (e.g. systemSetupRequired
     * reports false). 
     * @return systemSetupTargetForward the url part to forward to
     */
    String getSystemSetupTargetForward();

    /**
     * Indicates that current user has time recording enabled
     * @return timeRecordingRequired
     */
    boolean isTimeRecordingRequired();

    /**
     * Indicates that at least one private contact is available for current user
     * @return true if private contact available
     */
    boolean isPrivateContactAvailable();

    /**
     * Provides the language keys supported by application
     * @return supportedLanguages
     */
    List<String> getSupportedLanguages();

    /**
     * Provides current activated menu
     * @return menu
     */
    Menu getMenu();

    /**
     * Selects a menu
     * @param name
     */
    void selectMenu(String name);

    /**
     * Toggles a menu position activation status
     * @param name
     */
    void toggleMenuHeader(String name);

    /**
     * Updates current user
     */
    void reload();

    /**
     * Reloads the menu
     */
    void reloadMenu();

    /**
     * Indicates if user was logged in when view was created
     * @return userLoggedIn
     */
    boolean isUserLoggedIn();

    /**
     * Provides current page
     * @return page
     */
    Page getPage();

    /**
     * Sets current page
     * @param page
     */
    void activatePage(Page page);

    /**
     * Indicates if an activated help is available for current page
     * @return helpAvailable
     */
    boolean isHelpAvailable();

    /**
     * Provides the url for the help server startpage
     * @return helpServerUrl
     */
    String getHelpServerUrl();

    /**
     * Provides the help link to configure the help system
     * @return helpLink
     */
    HelpLink getHelpLink();

    /**
     * Provides the help link url to access the help system
     * @return helpLinkUrl
     */
    String getHelpLinkUrl();

    /**
     * Provides help link params if required
     * @return helpLinkParameters or empty string
     */
    String getHelpLinkParameters();

    /**
     * Enables help for current page
     */
    void enableHelp();

    /**
     * Disables help for current page
     */
    void disableHelp();

    /**
     * Indicates that portal view is in help config mode
     * @return helpConfigMode
     */
    boolean isHelpConfigMode();

    /**
     * Enables help config mode
     */
    void enableHelpConfigMode();

    /**
     * Disables help config mode
     */
    void disableHelpConfigMode();

    /**
     * Updates current help page
     * @param name
     * @param value
     * @throws ClientException if name already exists
     */
    void updateHelp(String name, String value) throws ClientException;

    /**
     * Provides all permissions required for all features of current page
     * @return permissions
     */
    Map<String, String> getPermissions();

    /**
     * Adds a permission of current page
     * @param name
     * @param description
     */
    void addPermission(String name, String description);

    /**
     * Resets all permissions of current page
     */
    void resetPermissions();
    
    /**
     * Indicates if permission is denied for provided permissions
     * @param permissions 
     * @return true if permissions not empty and not grant to user
     */
    boolean isPermissionDenied(String[] permissions);
    
    /**
     * Indicates if permission is denied for provided permissions
     * @param permissions comma separated list of permissions
     * @return true if permissions not empty and not grant to user
     */
    boolean isPermissionGrant(String permissions);

    /**
     * Provides the default resource bundle for a given locale
     * @param locale
     * @return bundle or null if not found
     */
    ResourceBundle getResourceBundle(Locale locale);

    /**
     * Provides application login target.
     * @return loginTarget
     */
    String getLoginTarget();

    /**
     * Provides application startup target
     * @return logoutTarget
     */
    String getLogoutTarget();

    /**
     * Provides user's or default startupTarget if available
     * @return startupTarget
     */
    String getStartupTarget();

    /**
     * Provides the remote address.
     * @return remoteAddress 
     */
    String getRemoteAddress();
    
    /**
     * Provides the server info providing JVM runtime properties. 
     * @return sysInfo
     */
    SysInfo getSysInfo();

    /**
     * Provides the webapp configuration
     * @return webConfig
     */
    WebConfig getWebConfig();

    /**
     * Sets webapp configuration
     * @param webConfig
     */
    void setWebConfig(WebConfig webConfig);

    /**
     * Indicates if current user has -local- telephone assigned
     * @return true if user has telephone
     */
    boolean isUserHasTelephone();

    /**
     * Indicates if ClickToCall is enabled
     * @return clickToCallEnabled
     */
    boolean isClickToCallEnabled();

    /**
     * Indicates if ClickToCallWizard is enabled
     * @return clickToCallEnabledWizard
     */
    boolean isClickToCallEnabledWizard();

    /**
     * Sets clickToCallEnabledWizard
     * @param clickToCallEnabledWizard
     */
    void setTelephoneStatus(boolean userHasTelephone, boolean clickToCallEnabled, boolean clickToCallEnabledWizard);
}
