/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Aug-2006 13:45:52 
 * 
 */
package com.osserp.core.dao.records;

import java.util.List;
import java.util.Set;

import com.osserp.common.ClientException;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesOrderVolumeExport;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrderVolumeExports {

    /**
     * Provides all orders available for export creation
     * @param config
     * @return newOrders
     * @throws ClientException if config is invalid (a referenced customer is no client, etc)
     */
    List<Order> getAvailableOrders(SalesOrderVolumeExportConfig config) throws ClientException;

    /**
     * Provides count of closed volume invoices
     * @param config
     * @return count
     */
    int getInvoiceArchiveCount(SalesOrderVolumeExportConfig config);

    /**
     * Provides the closed volume invoices
     * @param company
     * @param customer
     * @return existing
     */
    List<Invoice> getInvoiceArchive(SalesOrderVolumeExportConfig config);

    /**
     * Provides count of unreleased volume invoices
     * @param config
     * @return count
     */
    int getOpenInvoiceCount(SalesOrderVolumeExportConfig config);

    /**
     * Provides the unreleased volume invoices
     * @param company
     * @param customer
     * @return existing
     */
    List<Invoice> getOpenInvoices(SalesOrderVolumeExportConfig config);

    /**
     * Provides count of unreleased orders
     * @param config
     * @return count
     */
    int getUnreleasedOrderCount(SalesOrderVolumeExportConfig config);

    /**
     * Provides a list of all unreleased orders related to company and customer
     * @param config
     * @return records
     */
    List<RecordDisplay> getUnreleasedOrders(SalesOrderVolumeExportConfig config);

    /**
     * Provides the primary keys of all exported deliveries
     * @return exportedDeliveries
     */
    Set<Long> getExportedDeliveries();

    /**
     * Creates a new sales order volume export
     * @param config
     * @param invoice
     * @return volumeExport
     */
    SalesOrderVolumeExport create(SalesOrderVolumeExportConfig config, SalesInvoice invoice);

    /**
     * Adds a new order to export
     * @param export
     * @param order to add
     */
    void addItem(SalesOrderVolumeExport export, Order order);

    /**
     * Adds deliveries of an order
     * @param export
     * @param order
     * @param deliveries
     */
    void addDeliveries(SalesOrderVolumeExport export, Order order, List<Long> deliveries);

    /**
     * Synchronizes a sales order export with the backend
     * @param volumeExport
     */
    void save(SalesOrderVolumeExport volumeExport);

}
