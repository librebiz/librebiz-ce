/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.events;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.events.EventConfig;
import com.osserp.core.events.EventConfigType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractEventConfig extends AbstractEntity implements EventConfig {
    private static final long serialVersionUID = 42L;

    private EventConfigType type;
    private Long callerId;
    private boolean alert = false;
    private Long infoActionId;

    protected AbstractEventConfig() {
        super();
    }

    protected AbstractEventConfig(Long id, EventConfigType type, Long callerId) {
        super(id);
        this.type = type;
        this.callerId = callerId;
    }

    protected AbstractEventConfig(EventConfig o) {
        super(o);
        if (o.getType() != null) {
            type = (EventConfigType) o.getType().clone();
        }
        callerId = o.getCallerId();
        alert = o.isAlert();
        infoActionId = o.getInfoActionId();
    }

    public boolean isAction() {
        return false;
    }

    public EventConfigType getType() {
        return type;
    }

    public void setType(EventConfigType type) {
        this.type = type;
    }

    public Long getCallerId() {
        return callerId;
    }

    public void setCallerId(Long callerId) {
        this.callerId = callerId;
    }

    public boolean isAlert() {
        return alert;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    public Long getInfoActionId() {
        return infoActionId;
    }

    public void setInfoActionId(Long infoActionId) {
        this.infoActionId = infoActionId;
    }
}
