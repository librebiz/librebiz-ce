/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Sep 12, 2003 4:14:24 AM
 *
 */
package com.osserp.common.directory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Constants
import com.osserp.common.Property
import com.osserp.common.User
import com.osserp.common.UserPermission
import com.osserp.common.beans.AbstractUser
import com.osserp.common.util.StringUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class AppUser extends AbstractUser {
    private static Logger logger = LoggerFactory.getLogger(AppUser.class.getName())

    private boolean domainUser = false
    private boolean domainEditor = false

    /**
     * Default constructor.
     */
    protected AppUser() {
        super()
    }

    /**
     * Creates a guest user
     * @param companyId
     * @param contactId
     * @param customerId
     * @param loginName
     * @param password
     * @param remoteIp
     */
    AppUser(
    Long companyId,
    Long contactId,
    Long customerId,
    String loginName,
    String password,
    String remoteIp) {
        super()
        setLoginName(loginName)
        setPassword(password)
        setContactId(contactId)
        setCompanyId(companyId)
        setCustomerId(customerId)
        setRemoteIp(remoteIp)
        setLocaleCountry(Constants.DEFAULT_COUNTRY)
        setLocaleLanguage(Constants.DEFAULT_LANGUAGE)
        setCreated(new Date(System.currentTimeMillis()))
        setGuest(true)
    }

    /**
     * Creates a domain user
     * @param user
     * @param groups
     * @param perms
     * @param remoteIp
     * @param editorPermissions
     */
    AppUser(
    DirectoryUser user,
    List<DirectoryGroup> groups,
    List<DirectoryPermission> perms,
    String remoteIp,
    String editorPermissions) {
        super(user.getId(), Long.valueOf(1000), null, (String) user.getValues().get('uidNumber'), null)

        logger.debug("<init> invoked [editorPermissions=${editorPermissions}]")
        setUserEmployee(false)
        setLdapUid((String) user.getValues().get('uid'))
        setLoginName((String) user.getValues().get('displayName'))
        setPassword(user.getUserPasswordDisplay())
        setRemoteIp(remoteIp)
        if (perms) {
            addPermissions(perms)
        }
        if (groups) {
            addPermissions(groups)
        }
        setActive(true)
        setLoginTime(new Date(System.currentTimeMillis()))
        setGuest(false)
        domainUser = true
        if (editorPermissions != null) {
            String[] permissions = StringUtil.getTokenArray(editorPermissions)
            if (permissions == null) {
                logger.warn('<init> system does not permit editing')
            }
            if (isPermissionGrant(permissions)) {
                domainEditor = true
            }
        }
    }

    protected AppUser(User user) {
        super(user)
    }

    @Override
    boolean isDomainUser() {
        return domainUser
    }

    @Override
    boolean isDomainEditor() {
        this.domainEditor
    }

    @Override
    Long getCreatedBy() {
        super.getId()
    }

    @Override
    Object clone() {
        new AppUser(this)
    }

    @Override
    protected Property createProperty(String name, String value) {
        null
    }

    @Override
    protected UserPermission createPermission(Long grantBy, String permission) {
        new AppUserPermission(grantBy, permission)
    }

    private void addPermissions(List pList) {
        pList.each {
            addPermission(Constants.SYSTEM_USER, it.values.cn)
            logger.debug("addPermissions: permission added [name=${it.values.cn}]")
        }
    }
}
