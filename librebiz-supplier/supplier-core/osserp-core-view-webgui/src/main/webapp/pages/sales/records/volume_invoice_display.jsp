<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.salesVolumeInvoiceView}"/>
<c:set var="operation" value="${view.operation}"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="styles" type="string">
		<style type="text/css">
			<c:import url="volume_invoice_display.css"/>
		</style>
	</tiles:put>
	<tiles:put name="javascript" type="string">
		<c:import url="/pages/shared/switch_table_js.jsp"/>
		<c:if test="${view.createMode}">
			<script type="text/javascript">
				try {
					var requestObjects = new Hash();
				} catch(e) {

				}

				function ajaxRequest(requestUrl, targetElement, activityElement, activityMessage, cleanupElement) {
					ojsAjax.showAjaxThrobber(activityElement, activityMessage);
	
					if (requestObjects.get(requestUrl) != null) {
						requestObjects.get(requestUrl).transport.abort();
						requestObjects.set(requestUrl, null);
					}
					var ajaxObj = new Ajax.Request(requestUrl, {
						method: "GET",
						parameters: "",
						onSuccess: function(transport) {
							ojsAjax.hideAjaxThrobber(activityElement);
							if(cleanupElement != null) {
								$(cleanupElement).innerHTML = "";
							}
							if(targetElement != 'empty') {
								$(targetElement).innerHTML = transport.responseText;
							}
							requestObjects.set(requestUrl, null);
						}
					});

					requestObjects.set(requestUrl, ajaxObj);
				}

				function confirmRequest(question) {
					if (confirm(question)) {
						$('execArea').innerHTML = "<span class='boldtext'><fmt:message key="volumeInvoiceInitialized"/></span><span id='activityElement' style='margin-left: 20px;'></span>";
						ajaxRequest('<c:url value="/salesVolumeInvoice.do?method=createVolume"/>', 'resultArea', 'activityElement', '<fmt:message key="waitPleaseWithDots"/>', 'execArea');
					}
				}
			</script>
		</c:if>
	</tiles:put>

	<tiles:put name="headline"><fmt:message key="unitBillings"/></tiles:put>
	<tiles:put name="headline_right">
		<ul>
			<li><a href="<c:url value="/salesVolumeInvoice.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
			<li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
		</ul>
	</tiles:put>

	<tiles:put name="content" type="string">
		<div class="subcolumns">
			<div class="subcolumn">
				<c:choose>
					<c:when test="${view.createMode}">
						<div class="spacer"></div>
						<div class="contentBox">
							<div class="contentBoxHeader"><o:out value="${operation.config.name}"/></div>
							<div class="contentBoxData">
								<div class="subcolumns">
									<div class="subcolumn">
										<div class="spacer"></div>
										<p><span class="boldtext"><o:out value="${operation.exportableOrderCount}"/></span> <fmt:message key="recordsForUnitBillingsFound"/> </p>
										<p>
											<c:if test="${operation.availableOrderCount > 0}">
												<span class="boldtext"><o:out value="${operation.availableOrderCount}"/></span>
												<span id="listExportable"><fmt:message key="selectableRecords"/></span>
												&nbsp;&nbsp;/&nbsp;&nbsp;
											</c:if>
										</p>
										<div class="spacer"></div>
										<p id="execArea">
											<a href="<c:url value="/salesVolumeInvoice.do?method=disableCreate"/>">[<fmt:message key="exit"/>]</a>
											<o:permission role="${view.createPermissions}" info="permissionCreateVolumeInvoice">
											&nbsp;&nbsp;&nbsp;<a href="javascript:onclick=confirmRequest('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="volumeInvoiceCreate"/>?');">[<fmt:message key="volumeInvoiceCreate"/>]</a>
											</o:permission>
										</p>
										<div class="spacer"></div>
									</div>
								</div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div id="topContent">
							<div class="spacer"></div>
							<div class="contentBox">
								<div class="contentBoxHeader">
									<o:out value="${operation.config.name}"/>
								</div>
								<div class="contentBoxData">
									<div class="subcolumns">
										<div class="subcolumn" style="line-height: 1.5em;">
											<div class="spacer"></div>
											<c:if test="${operation.unreleasedInvoiceCount > 0}">
												<p>
													<span class="boldtext">
														<o:out value="${operation.unreleasedInvoiceCount}"/><span class="red"><fmt:message key="notReleasedUnitBillingsFound"/></span>
													</span>
													<a href="<c:url value="/salesVolumeInvoice.do?method=forwardUnreleasedInvoices&exit=volumeInvoiceRefresh"/>">[<fmt:message key="display"/>]</a>
												</p>
											</c:if>
											<c:if test="${operation.unreleasedOrderCount > 0}">
												<p>
													<span class="boldtext">
														<o:out value="${operation.unreleasedOrderCount}"/><span class="red"><fmt:message key="noNotReleasedRecordsForUnitBillingsFound"/></span>
													</span>
													<a href="<c:url value="/salesVolumeInvoice.do?method=forwardUnreleasedOrders&exit=volumeInvoiceRefresh"/>">[<fmt:message key="display"/>]</a>
												</p>
											</c:if>
											<c:if test="${operation.invoiceArchiveCount > 0}">
												<p>
													<span class="boldtext"><o:out value="${operation.invoiceArchiveCount}"/></span> <fmt:message key="unitBillingsInArchive"/>
													<a href="<c:url value="/salesVolumeInvoice.do?method=forwardInvoiceArchive&exit=volumeInvoiceRefresh"/>">[<fmt:message key="display"/>]</a>
												</p>
											</c:if>
											<c:choose>
												<c:when test="${operation.availableOrderCount == 0}">
													<p><fmt:message key="noRecordsForNewUnitBillingsFound"/> <a href="<c:url value="/salesVolumeInvoice.do?method=refresh"/>"> [<fmt:message key="refreshView"/>]</a></p>
												</c:when>
												<c:otherwise>
													<p>
														<span class="boldtext"><o:out value="${operation.availableOrderCount}"/></span>
														<span> <a href="<c:url value="/salesVolumeInvoice.do?method=listExportable"/>"><fmt:message key="recordsForUnitBillingsFound"/></a></span>
													</p>
													<c:if test="${operation.availableOrderCount > 0}">
														<p id="execArea">
															<o:permission role="${view.createPermissions}" info="permissionEnableCreateVolumeInvoice">
																<o:logger write="current user has required create permissions" level="debug"/>
																&nbsp;&nbsp;&nbsp;<a href="<c:url value="/salesVolumeInvoice.do?method=enableCreate"/>">[<fmt:message key="volumeInvoiceCreate"/>]</a>
															</o:permission>
														</p>
													</c:if>
												</c:otherwise>
											</c:choose>
											<c:if test="${view.listMode}">
												<p><fmt:message key="currentLoadedList"/>:
													<span class="boldtext"><fmt:message key="exportableRecords"/></span>
												</p>
											</c:if>
											<div class="spacer"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<c:if test="${view.listMode}">
							<c:import url="volume_invoice_exportables.jsp"/>
						</c:if>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
