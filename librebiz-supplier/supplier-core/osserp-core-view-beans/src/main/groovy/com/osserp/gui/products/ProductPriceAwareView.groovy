/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 09-Jul-2010 
 * 
 */
package com.osserp.gui.products

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.core.products.Product
import com.osserp.core.products.ProductPriceManager
import com.osserp.core.products.PriceAware
import com.osserp.core.purchasing.PurchaseInvoiceManager
import com.osserp.core.system.BranchOffice
import com.osserp.core.system.BranchOfficeManager

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class ProductPriceAwareView extends ProductViewAwareView {


    boolean valuationMode

    void changePurchasePriceLimitFlag() {
        productManager.changePurchasePriceLimitFlag(domainUser, getProduct())
    }

    void updateValuation() throws ClientException {
        Product product = getProduct()
        Double valuation = form.getDouble('value')
        if (!valuation) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }
        BranchOffice branch
        List<BranchOffice> officelist = branchOfficeManager.findByCompany(product?.summary?.stockId)
        officelist.each {
            if (it.isHeadquarter()) {
                branch = it
            }
        }
        if (!branch) {
            throw new ClientException(ErrorCode.BRANCH_MISSING)
        }
        Product updated = purchaseInvoiceManager.createValuation(
                domainEmployee,
                product,
                product?.summary?.stockId,
                branch.id,
                valuation)
        env.productView.reload()
        valuationMode = false
    }

    @Override
    void save() {
        Product productObject = getProduct()
        PriceAware priceAwareObject = getBean()
        Date validFrom = form.getDate('validFrom')
        Double estimatedPurchasePrice = form.getDouble('estimatedPurchasePrice')
        Double consumerPrice = form.getDouble('consumerPrice')
        Double consumerMinimumMargin = form.getPercentage('consumerMinimumMargin')
        Double resellerPrice = form.getDouble('resellerPrice')
        Double resellerMinimumMargin = form.getPercentage('resellerMinimumMargin')
        Double partnerPrice = form.getDouble('partnerPrice')
        Double partnerMinimumMargin = form.getPercentage('partnerMinimumMargin')
        bean = productPriceManager.updateSalesPrice(
                domainEmployee,
                productObject,
                priceAwareObject,
                validFrom,
                estimatedPurchasePrice,
                consumerPrice,
                consumerMinimumMargin,
                resellerPrice,
                resellerMinimumMargin,
                partnerPrice,
                partnerMinimumMargin)

        disableEditMode()
    }

    protected final ProductPriceManager getProductPriceManager() {
        getService(ProductPriceManager.class.getName())
    }

    protected final BranchOfficeManager getBranchOfficeManager() {
        getService(BranchOfficeManager.class.getName())
    }

    protected final PurchaseInvoiceManager getPurchaseInvoiceManager() {
        getService(PurchaseInvoiceManager.class.getName())
    }
}

