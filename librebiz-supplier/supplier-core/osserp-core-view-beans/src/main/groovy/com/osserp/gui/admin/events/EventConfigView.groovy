/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 8, 2011 9:45:16 AM 
 * 
 */
package com.osserp.gui.admin.events

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.FcsActionManager
import com.osserp.core.events.EventAction
import com.osserp.core.events.EventConfigManager
import com.osserp.core.events.EventConfigType
import com.osserp.core.events.EventTermination

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView


/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
class EventConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(EventConfigView.class.getName())

    static final Integer SEND_SALES = 1
    static final Integer SEND_MANAGER = 2
    static final Integer SEND_POOL = 3

    List flowControls = []
    List pools = []
    Map types = [:]
    Long requestTypeId = null
    EventConfigType eventType = null
    boolean syncRequired

    EventConfigView() {
        providesEditMode()
        providesCreateMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            pools = eventConfigManager.findPools()
            eventConfigManager.findTypes().each {
                types.put(it.getId(), it)
            }
        }
    }

    @Override
    void createCustomNavigation() {
        nav.defaultNavigation = [
            exitLink,
            createLink,
            homeLink]
        nav.beanNavigation = [
            selectExitLink,
            deleteLink,
            editLink,
            homeLink
        ]
        nav.beanListNavigation = nav.beanNavigation
        nav.listNavigation = [
            selectEventTypeExitLink,
            createLink,
            homeLink
        ]
    }

    @Override
    MenuItem getEditLink() {
        MenuItem editLink = super.editLink
        editLink.permissions = 'event_config,executive'
        editLink.permissionInfo = 'permissionEventConfig'
        return editLink
    }

    MenuItem getDeleteLink() {
        MenuItem deleteLink = navigationLink("/${context.name}/delete", 'deleteIcon', 'confirmTodoConfigRemove', 'event_config,executive', 'permissionEventConfig')
        deleteLink.confirmLink = true
        return deleteLink
    }

    MenuItem getSelectEventTypeExitLink() {
        navigationLink("/${context.name}/selectEventType", 'backIcon', 'backToLast')
    }

    boolean isFcsMode() {
        return env.fcsMode
    }

    void selectEventType() {
        Long id = form.getLong('id')
        if (id) {
            eventType = types[id]
            Long requestTypeFormId = form.getLong('businessType')
            if (requestTypeFormId) {
                requestTypeId = requestTypeFormId
            }
            reload()
        } else {
            eventType = null
            requestTypeId = null
            list = []
        }
    }

    boolean isEventTypeSelected() {
        eventType != null
    }

    @Override
    void reload() {
        if (requestTypeId) {
            list = eventConfigManager.findStartActionView(eventType, requestTypeId)
            if (eventType?.flowControl && eventType?.flowControlActionManager) {
                FcsActionManager actionManager = getFcsActionManager(eventType.flowControlActionManager)
                flowControls = actionManager.findByType(requestTypeId)
                env.fcsMode = true
            } else {
                env.fcsMode = false
            }
            logger.debug("reload: requestType selection enabled [requestType=${requestTypeId}, eventType=${eventType?.id}, actionCount=${list.size()}, fcsMode=${env.fcsMode}, flowControlCount=${flowControls.size()}]")
        } else if (eventType) {
            if (eventType.flowControl && eventType.flowControlActionManager) {
                list = eventConfigManager.findStartActionView(eventType, null)
                FcsActionManager actionManager = getFcsActionManager(eventType.flowControlActionManager)
                flowControls = actionManager.findByType(null)
                env.fcsMode = true
            } else {
                env.fcsMode = false
                list = eventConfigManager.findActionsByEventType(eventType)
            }
            logger.debug("reload: requestType selection disabled [eventType=${eventType?.id}, actionCount=${list.size()}, fcsMode=${env.fcsMode}, flowControlCount=${flowControls.size()}]")
        }
    }

    @Override
    void select() {
        Long id = form.getLong('id')
        if (id) {
            if (fcsMode) {
                //needed to get proper objects (in fcsMode the objects in the list are instanceof EventDisplayView)
                bean = eventConfigManager.findAction(id)
            } else {
                super.select()
            }
        } else {
            bean = null
        }
    }

    @Override
    void save() {
        if (fcsMode) {
            bean.setCallerId(form.getLong('callerId'))
        }
        Long[] terminationIds = form.getIndexedLong('terminationEventIds')
        boolean closeableByTarget = form.getBoolean('closeableByTarget')
        logger.debug("save: invoked [closeableByTarget=${closeableByTarget}, terminationIds=${terminationIds}")
        if (!terminationIds && !closeableByTarget) {
            throw new ClientException(ErrorCode.TERMINATION_POSSIBILITY_MISSING)
        }
        bean.setCloseableByTarget(closeableByTarget)
        bean.setName(form.getString('name'))
        bean.setPriority(form.getIntVal('priority'))
        bean.setSendAlert(form.getBoolean('sendAlert'))
        bean.setInfo(form.getBoolean('info'))
        bean.setIgnoringBranch(form.getBoolean('ignoringBranch'))
        bean.setTerminationAlert(form.getLong('terminationAlert'))
        bean.setTerminationTime(form.getLong('terminationTime'))
        Integer sendId = form.getInteger('sendId')
        if (SEND_SALES.equals(sendId)) {
            bean.setSendSales(true)
            bean.setSendManager(false)
            bean.setSendPool(false)
            bean.setPool(null)
        } else if (SEND_MANAGER.equals(sendId)) {
            bean.setSendSales(false)
            bean.setSendManager(true)
            bean.setSendPool(false)
            bean.setPool(null)
        } else if (SEND_POOL.equals(sendId)) {
            Long poolId = null
            try {
                poolId = form.getLong('poolId')
            } catch (Throwable t) {
                // ignore
            }
            logger.debug("save: recipient type is pool, selected pool " + poolId)
            if (poolId == null || poolId == 0) {
                throw new ClientException(ErrorCode.VALUES_MISSING)
            }
            if (bean.isSendPool() && (bean.getPool() != null)
            && bean.getPool().getId().equals(poolId)) {
                bean.setSendSales(false)
                bean.setSendManager(false)
            } else {
                bean.setSendSales(false)
                bean.setSendManager(false)
                bean.setSendPool(true)
                bean.setPool(eventConfigManager.findPool(poolId))
            }
        } else if(fcsMode) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }
        setClosings(bean, terminationIds)
        if (!bean.isSendAlert()) {
            if (bean.getAlertEvent() != null) {
                eventConfigManager.close(bean.getAlertEvent())
            }
            bean.setAlertEvent(null)
        } else {
            Long alertPoolId = form.getLong('alertPoolId')
            logger.debug("save: assembled alertPoolId " + alertPoolId)
            if (alertPoolId != null && alertPoolId != 0) {
                eventConfigManager.setAlert(bean, eventConfigManager.findPool(alertPoolId))
            } else {
                bean.setSendAlert(false)
            }
        }
        if (bean.getAlertEvent() != null) {
            setClosings(bean.getAlertEvent(), terminationIds)
        }
        eventConfigManager.save(bean)
        reload()
        bean = eventConfigManager.findAction(bean.id)
        disableEditMode()
        syncRequired = true
    }

    private void setClosings(EventAction action, Long[] terminations) {

        List<EventTermination> existing = action.getClosings()
        if (terminations == null || terminations.length == 0) {
            for (Iterator<EventTermination> i = existing.iterator(); i.hasNext();) {
                EventTermination next = i.next()
                i.remove()
                eventConfigManager.delete(next)
            }
        } else {
            for (Iterator<EventTermination> i = existing.iterator(); i.hasNext();) {
                EventTermination next = i.next()
                if (!containsClosing(terminations, next.getCallerId())) {
                    i.remove()
                    eventConfigManager.delete(next)
                }
            }
            def j = terminations.length
            for (int i = 0; i < j; i++) {
                Long next = terminations[i]
                action.addClosing(next)
            }
        }
        eventConfigManager.save(action)
        syncRequired = true
    }

    private boolean containsClosing(Long[] terminations, Long callerId) {
        if (terminations == null) {
            return false
        }
        int j = terminations.length
        for (int i = 0; i < j; i++) {
            Long next = terminations[i]
            if (next.equals(callerId)) {
                return true
            }
        }
        return false
    }

    void create() {
        Long caller = null
        if (fcsMode) {
            caller = form.getLong('callerId')
        }
        String name = form.getString('name')
        if (name == null || name.length() < 1) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }
        EventAction created = eventConfigManager.createAction(
                requestTypeId,
                eventType,
                name,
                caller)
        logger.debug("create: created new event " + created.getId())
        reload()
        bean = created
        disableCreateMode()
        enableEditMode()
        syncRequired = true
    }

    void delete() {
        if (bean) {
            eventConfigManager.delete(bean)
            bean = null
            reload()
            syncRequired = true
        }
    }
    
    void togglePaused() {
        EventAction action = bean
        action.setPaused(!action.isPaused())
        action.setPausedBy(domainEmployee.id)
        action.setPausedDate(new Date())
        eventConfigManager.save(action)
    }

    void synchronizeFcs() {
        eventConfigManager.synchronizeFcs()
        syncRequired = false
    }

    private EventConfigManager getEventConfigManager() {
        getService(EventConfigManager.class.getName())
    }

    private FcsActionManager getFcsActionManager(String name) {
        getService(name)
    }
}
