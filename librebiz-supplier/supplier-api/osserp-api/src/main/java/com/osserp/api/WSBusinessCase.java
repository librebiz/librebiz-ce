/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api;

import java.util.Date;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface WSBusinessCase extends WSObject {

    /**
     * The id of the customer if not provided by embedded contact object.
     * Setting the customerId also cancels requirement for setting the contact. 
     * @return customer id
     */
    Long getCustomerId();

    /**
     * Provides the id of the referenced sales person
     * @return sales person id or null
     */
    Long getSalesPersonId();

    /**
     * Provides the id of the related project manager
     * @return project manager id or null
     */
    Long getProjectManagerId();
    
    /**
     * Provides the contact if not provided by customer id. 
     * Embedding a contact is required if contact not already exists.
     * @return contact or null on updates with existing and provided customerId
     */
    WsContact getContact();

    /**
     * The primary key of the business case type.
     * @return businessType
     */
    Long getBusinessType();

    /**
     * The name of the associated business case type.
     * @return businessTypeName
     */
    String getBusinessTypeName();
    
    /**
     * Indicates the context of the business case. Possible value are 'request'
     * (businessCase context is 'interest requests for offer or further 
     * information') or 'sales' (businessCase context is 'customer placed order')
     * @return 'sales' or 'request' depending on offer/order status
     */
    String getContext();
    
    /**
     * Provides the id of the assigned branch
     * @return branchId
     */
    Long getBranchId();
    
    /**
     * Sets the id of the branch
     * @param branchId
     */
    void setBranchId(Long branchId);
    
    /**
     * Provides the shortkey of the assigned branch.
     * @return branchKey
     */
    String getBranchKey();
    
    /**
     * Sets the shortkey of the branch.
     * @param branchKey
     */
    void setBranchKey(String branchKey);
    
    /**
     * The name for the businessCase (e.g. the project or commission name).
     * An application typically sets this to 'name, city' by default 
     * @return name
     */
    String getName();
    
    /**
     * Sets the name of the businessCase
     * @param name
     */
    void setName(String name);

    String getStreet();
    void setStreet(String street);

    String getStreetAddon();
    void setStreetAddon(String streetAddon);

    String getCity();
    void setCity(String city);

    String getFederalState();
    void setFederalState(String federalState);

    String getZipcode();
    void setZipcode(String zipcode);

    String getCountry();
    void setCountry(String country);

    Long getCountryId();
    void setCountryId(Long countryId);

    /**
     * The status provides the name of a flowControl action
     * @return status
     */
    String getStatus();
    void setStatus(String status);

    /**
     * The id of the corresponding status if known 
     * @return statusId
     */
    Long getStatusId();
    void setStatusId(Long statusId);

    /**
     * Provides a common note. This is the content of an external
     * contact form in most cases.
     * @return note
     */
    String getNote();
    void setNote(String note);
    
    /**
     * Provides the name of origin as a string value.
     * @return origin name
     */
    String getOriginName();
    void setOriginName(String originName);
    
    /**
     * Provides the corresponding id of the origin if known.
     * @return originId
     */
    Long getOriginId();
    void setOriginId(Long originId);
    
    /**
     * Provides the name of the campaign the businessCase belongs to
     * @return campaign name
     */
    String getCampaignName();
    void setCampaignName(String campaignName);
    
    /**
     * Provides the corresponding id of the campaign if known.
     * @return originId
     */
    Long getCampaignId();
    void setCampaignId(Long campaignId);
    
    /**
     * Provides the external request date as string. This method is used
     * to support text based interfaces (import files/sheets for example)   
     * @return request created date as string
     */
    String getRequestCreated();
    void setRequestCreated(String requestCreated);

    /**
     * Provides the external request date as date.
     * @return request created date as date object
     */
    Date getRequestDate();
    void setRequestDate(Date requestDate);
}
