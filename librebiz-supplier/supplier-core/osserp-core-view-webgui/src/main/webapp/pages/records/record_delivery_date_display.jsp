<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:choose>
	<c:when test="${empty record.delivery}">
    <div class="recordLabelValue"><fmt:message key="deliveryDate"/>: <fmt:message key="undefined"/></div>
	</c:when>
	<c:otherwise>
	<div class="recordLabelValue"><fmt:message key="deliveryDate"/>: <span title="KW <o:date format="week" value="${record.delivery}"/> / <o:date format="year" value="${record.delivery}"/>"><o:date value="${record.delivery}"/></span></div>
	</c:otherwise>
</c:choose>


