/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 5, 2004 
 * 
 */
package com.osserp.core.contacts;

import java.util.Date;
import java.util.List;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Mappable;
import com.osserp.common.Option;
import com.osserp.common.mail.EmailAddress;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Contact extends ContactPerson, Mappable {

    static final Long CLIENT = 0L;
    static final Long CUSTOMER = 1L;
    static final Long SUPPLIER = 2L;
    static final Long EMPLOYEE = 7L;
    static final Long OTHER = 8L;
    static final Long BRANCH_OFFICE = 9L;

    static final String CLIENT_KEY = new String("client");
    static final String CUSTOMER_KEY = new String("customer");
    static final String SUPPLIER_KEY = new String("supplier");
    static final String EMPLOYEE_KEY = new String("employee");

    static final Long ACTIVATED = 1L;
    static final Long DEACTIVATED = 0L;

    /**
     * Provides the spouse salutation
     * @return spouseSalutation
     */
    Salutation getSpouseSalutation();

    /**
     * Provides spouse title
     * @return spouseTitle
     */
    Option getSpouseTitle();

    /**
     * Provides firstname of spouse
     * @return spouseFirstName
     */
    String getSpouseFirstName();

    /**
     * Provides the contact spouse name in alphabetical list format
     * @return 'spouseLastName, spouseFirstName'
     */
    String getSpouseDisplayName();

    /**
     * Provides lastname of spouse
     * @return spouseLastName
     */
    String getSpouseLastName();

    /**
     * Provides the contact spouse name in vcard format
     * @return 'spouseFirstName spouseLastName'
     */
    String getSpouseName();

    /**
     * Update spouse values
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     */
    void updateSpouse(
            Salutation salutation,
            Option title,
            String firstName,
            String lastName);

    /**
     * Provides a user defined name for a letter address field if not createable by default rules
     * @return userSalutation
     */
    String getUserNameAddressField();

    /**
     * Sets a default overriding user defined name for a letter address field
     * @param userNameAddressField
     */
    void setUserNameAddressField(String userNameAddressField);

    /**
     * Provides a user defined salutation overriding defaults if not createable by default rules
     * @return userSalutation
     */
    String getUserSalutation();

    /**
     * Sets a default overriding user defined salutation
     * @param userSalutation
     */
    void setUserSalutation(String userSalutation);

    /**
     * Indicates that this contact is a client company
     * @return client
     */
    boolean isClient();

    /**
     * Indicates that contact is a branch office of a client
     * @return branch
     */
    boolean isBranchOffice();

    /**
     * Indicates if contact is a contact person of another contact (e.g. Contact.TYPE_PERSON == contact.type)
     * @return true if contactPerson
     */
    boolean isContactPerson();

    /**
     * Indicates that this contact is an employee
     * @return employee
     */
    boolean isEmployee();

    /**
     * Indicates that this contact is a customer
     * @return customer
     */
    boolean isCustomer();

    /**
     * Indicates that this contact is a supplier
     * @return supplier
     */
    boolean isSupplier();

    /**
     * Indicates that this contact is an installer
     * @return installer
     */
    boolean isInstaller();

    /**
     * Indicates that this contact is in no group
     * @return other
     */
    boolean isOther();

    /**
     * Indicates that this contact is a vip contact
     * @return vip
     */
    boolean isVip();

    /**
     * Enables/disables vip flag
     * @param vip
     */
    void setVip(boolean vip);

    /**
     * Provides a vip status note
     * @return vip status note
     */
    String getVipNote();

    /**
     * Sets a vip status note
     * @param vipNote
     */
    void setVipNote(String vipNote);

    /**
     * Indicates that this contact has also a user account
     * @return user
     */
    boolean isUser();

    /**
     * Provides the default group display (optional)
     * @return defaultGroupDisplay
     */
    String getDefaultGroupDisplay();

    /**
     * Sets the (optional) default group display value
     * @param defaultGroupDisplay
     */
    void setDefaultGroupDisplay(String defaultGroupDisplay);

    /**
     * Indicates that this contact should receive newsletters
     * @return newsletter confirmation status
     */
    boolean isNewsletterConfirmation();

    /**
     * Enables/disables newsletter confirmation
     * @param newsletterConfirmation
     */
    void setNewsletterConfirmation(boolean newsletterConfirmation);
    
    /**
     * Provides a note relating to newsletter confirmation
     * @return newsletter confirmation note
     */
    String getNewsletterConfirmationNote();
    
    /**
     * Sets a note relating to newsletter confirmation
     * @param newsletter confirmation note
     */
    void setNewsletterConfirmationNote(String newsletterConfirmationNote);
    
    /**
     * Provides a summary of all grantContact* information as i18n key
     * useful to display all information in a human readable format. 
     * @return grantContactStatus
     */
    String getGrantContactStatus();

    /**
     * Indicates if grantContactStatus will produce a result (e.g. is initialized)
     * @return true if invocation of getGrantContactStatus() will not result in null. 
     */
    boolean isGrantContactStatusAvailable();

    /**
     * Indicates if contact allowed contact per email
     * @return true if grant
     */
    boolean isGrantContactPerEmail();

    /**
     * Sets if contact allowed contact per email
     * @param grantContactPerEmail
     */
    void setGrantContactPerEmail(boolean grantContactPerEmail);

    /**
     * Provides the changed date of the allowed contact method  
     * @return changed date
     */
    Date getGrantContactPerEmailChanged();

    /**
     * Sets the changed date of the allowed contact method  
     * @param grantContactPerEmailChanged date
     */
    void setGrantContactPerEmailChanged(Date grantContactPerEmailChanged);

    /**
     * Provides the employee changing the contact method  
     * @return changed by
     */
    Long getGrantContactPerEmailChangedBy();

    /**
     * Sets the id of the employee performing the change   
     * @param grantContactPerEmailChangedBy employee id
     */
    void setGrantContactPerEmailChangedBy(Long grantContactPerEmailChangedBy);

    /**
     * Provides an optional note to the allowed contact method change
     * @return grantContactPerEmailNote
     */
    String getGrantContactPerEmailNote();

    /**
     * Sets an optional note to the allowed contact method change
     * @param grantContactPerEmailNote
     */
    void setGrantContactPerEmailNote(String grantContactPerEmailNote);

    /**
     * Indicates if contact allowed contact per phone call
     * @return true if grant
     */
    boolean isGrantContactPerPhone();

    /**
     * Sets if contact allowed contact per phone call
     * @param grantContactPerPhone true if grant
     */
    void setGrantContactPerPhone(boolean grantContactPerPhone);

    /**
     * Provides the changed date of the allowed contact method  
     * @return changed date
     */
    Date getGrantContactPerPhoneChanged();

    /**
     * Sets the changed date of the allowed contact method  
     * @param grantContactPerPhoneChanged date
     */
    void setGrantContactPerPhoneChanged(Date grantContactPerPhoneChanged);

    /**
     * Provides the employee changing the contact method  
     * @return changed by
     */
    Long getGrantContactPerPhoneChangedBy();

    /**
     * Sets the id of the employee performing the change   
     * @param grantContactPerPhoneChangedBy employee id
     */
    void setGrantContactPerPhoneChangedBy(Long grantContactPerPhoneChangedBy);

    /**
     * Provides an optional note to the allowed contact method change
     * @return grantContactPerPhoneNote
     */
    String getGrantContactPerPhoneNote();

    /**
     * Sets an optional note to the allowed contact method change
     * @param grantContactPerPhoneNote
     */
    void setGrantContactPerPhoneNote(String grantContactPerPhoneNote);

    /**
     * Indicates if contact has forbidden any contact attempts
     * @return true if grant nothing
     */
    boolean isGrantContactNone();

    /**
     * Sets if contact has forbidden any contact attempts
     * @param grantContactNone
     */
    void setGrantContactNone(boolean grantContactNone);

    /**
     * Provides the changed date of the allowed contact method  
     * @return changed date
     */
    Date getGrantContactNoneChanged();

    /**
     * Sets the changed date of the allowed contact method  
     * @param grantContactNoneChanged date
     */
    void setGrantContactNoneChanged(Date grantContactNoneChanged);

    /**
     * Provides the employee changing the contact method  
     * @return changed by
     */
    Long getGrantContactNoneChangedBy();

    /**
     * Sets the id of the employee performing the change   
     * @param grantContactNoneChangedBy employee id
     */
    void setGrantContactNoneChangedBy(Long grantContactNoneChangedBy);

    /**
     * Provides an optional note to the allowed contact method change
     * @return grantContactNoneNote
     */
    String getGrantContactNoneNote();

    /**
     * Sets an optional note to the allowed contact method change
     * @param grantContactNoneNote
     */
    void setGrantContactNoneNote(String grantContactNoneNote);

    /**
     * Updates contact grant flags
     * @param changedBy id of employee performing the update 
     * @param grantContactPerEmailValue
     * @param grantContactPerEmailNoteValue
     * @param grantContactPerPhoneValue
     * @param grantContactPerPhoneNoteValue
     * @param grantContactNoneValue
     * @param grantContactNoneNoteValue
     * @param newsletterConfirmationValue
     * @param newsletterConfirmationNoteValue
     * @param vipValue
     * @param vipNoteValue
     * @return true if invocation changed values
     */
    boolean updateContactGrants(
        Long changedBy,
        boolean grantContactPerEmailValue,
        String grantContactPerEmailNoteValue,
        boolean grantContactPerPhoneValue,
        String grantContactPerPhoneNoteValue,
        boolean grantContactNoneValue,
        String grantContactNoneNoteValue,
        boolean newsletterConfirmationValue,
        String newsletterConfirmationNoteValue,
        boolean vipValue, 
        String vipNoteValue);

    /**
     * Provides the website of the contact
     * @return website
     */
    String getWebsite();

    /**
     * Sets the webiste of the contact
     * @param website
     */
    void setWebsite(String website);

    /**
     * The email list
     * @return emails
     */
    List<EmailAddress> getEmails();

    /**
     * The email aliases of an internal address
     * @param address
     * @return emails
     */
    List<EmailAddress> getEmailAliases(EmailAddress address);

    /**
     * Adds a new email address
     * @param createdBy
     * @param type
     * @param emailAddress
     * @param primary
     * @param aliasOf
     * @throws ClientException if address is not a valid email address
     */
    void addEmail(Long createdBy, Long type, String emailAddress, boolean primary, Long aliasOf)
            throws ClientException;

    /**
     * Removes an email address
     * @param id
     */
    void removeEmail(Long id);

    /**
     * The primary email address
     * @return email
     */
    String getEmail();
    
    /**
     * Indicates if an address is matching any of contacts email addresses. 
     * @param emailAddress the address to lookup for
     * @return true if any email address of contact matches
     */
    boolean hasEmail(String emailAddress);

    /**
     * Provides phone numbers by device
     * @return phoneNumbers
     */
    List<Phone> getPhoneDeviceNumbers(Long device);

    /**
     * Provides all phone numbers of any device
     * @return phoneNumbers
     */
    List<Phone> getPhoneNumbers();

    /**
     * The phone list
     * @return phones
     */
    List<Phone> getPhones();

    /**
     * The primary phone number
     * @return phone
     */
    Phone getPhone();

    /**
     * The fax list
     * @return faxNumbers
     */
    List<Phone> getFaxNumbers();

    /**
     * The primary fax number
     * @return fax
     */
    Phone getFax();

    /**
     * The mobile number list
     * @return mobiles
     */
    List<Phone> getMobiles();

    /**
     * The primary mobile number
     * @return mobile
     */
    Phone getMobile();

    /**
     * Adds a new phone number
     * @param createdBy
     * @param device
     * @param type
     * @param country
     * @param prefix
     * @param number
     * @param note
     * @param primary
     * @throws ClientException if validation failed
     */
    void addPhone(
            Long createdBy,
            Long device,
            Long type,
            String country,
            String prefix,
            String number,
            String note,
            boolean primary)
            throws ClientException;

    /**
     * Adds a new phone number
     * @param updateBy
     * @param phone
     * @param type
     * @param country
     * @param prefix
     * @param number
     * @param note
     * @param primary
     * @throws ClientException if validation failed
     */
    void updatePhone(
            Long updateBy,
            Phone phone,
            Long type,
            String country,
            String prefix,
            String number,
            String note,
            boolean primary)
            throws ClientException;

    /**
     * Provides an xml representation of the name and address
     * @return name and address values
     */
    Element getAddressXML();

    /**
     * Provides an xml representation of contact values
     * @return contact values
     */
    Element getXML();

    /**
     * Provides an xml representation of contact values
     * @param name of the created element
     * @return contact values
     */
    Element getXML(String name);
}
