/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 5, 2007 
 * 
 */
package com.osserp.core.service.records;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import org.apache.velocity.app.VelocityEngine;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.service.impl.AbstractTemplateAwareService;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.dao.Users;
import com.osserp.core.dao.records.RecordsArchive;
import com.osserp.core.dao.records.RecordVersionsArchive;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordCorrection;
import com.osserp.core.finance.RecordOutputService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordPrintServiceImpl extends AbstractTemplateAwareService implements RecordPrintService {
    private static Logger log = LoggerFactory.getLogger(RecordPrintServiceImpl.class.getName());
    private RecordsArchive archive = null;
    private RecordVersionsArchive recordVersionsArchive = null;
    private RecordOutputService recordOutputService = null;
    private Users users = null;

    protected RecordPrintServiceImpl(
            RecordsArchive archive,
            RecordVersionsArchive recordVersionsArchive,
            RecordOutputService service,
            VelocityEngine templateEngine,
            Users users) {
        super(templateEngine);
        this.archive = archive;
        this.recordVersionsArchive = recordVersionsArchive;
        this.recordOutputService = service;
        this.users = users;
    }

    public byte[] createPdf(Employee employee, Record record, boolean preview) throws ClientException {
        Document doc = recordOutputService.createDocument(employee, record, preview);
        return recordOutputService.createPdf(record, doc);
    }

    public byte[] createPdf(
            Employee employee,
            Record record,
            BusinessCase businessCase,
            boolean preview) throws ClientException {

        Document doc = createXml(employee, record, businessCase, preview);
        return recordOutputService.createPdf(record, doc);
    }

    public byte[] createCorrectionPdf(Employee employee, PaymentAwareRecord record, boolean preview) throws ClientException {
        if (record.getCorrectionCount() == 0) {
            return null;
        }
        Document doc = createXml(employee, record, null, preview);
        Element root = doc.getRootElement();
        Element cxml = recordOutputService.createCorrection(record);
        root.addContent(cxml);
        return recordOutputService.createCorrectionPdf(doc, record);
    }

    private Document createXml(Employee employee, Record record, BusinessCase businessCase, boolean preview) {
        Document doc = recordOutputService.createDocument(employee, record, preview);
        if (businessCase != null) {
            recordOutputService.addBusinessCase(doc, businessCase);
        }
        return doc;
    }

    public DocumentData getPdf(Record record) {
        DmsDocument doc = fetchDocument(record);
        if (doc != null) {
            byte[] data = archive.getDocumentData(doc);
            String documentName = doc.getRealFileName();
            if (data != null) {
                if (doc.getType() != null) {
                    if (doc.getType().isUseFilenameForDownload() && isSet(doc.getFileName())) {
                        documentName = StringUtil.replaceBlanks(doc.getFileName());
                        
                    } else if (!doc.getType().isSuffixByFile() && isSet(doc.getType().getSuffix())) {
                        
                        String skey = (record.getContact() != null ? record.getContact().getShortkey() : null);
                        if (isSet(skey)) {
                            documentName = record.getNumber() + "-" + skey + "." + doc.getType().getSuffix();
                        } else {
                            documentName = record.getNumber() + "." + doc.getType().getSuffix();
                        }
                    }
                }
                return new DocumentData(data, documentName, Constants.MIME_TYPE_PDF);
            }
        } else if (record != null && record.isCanceled()) {
            // handle request for canceled document
            try {
                DomainUser user = (DomainUser) users.find(record.getCreatedBy());
                byte[] data = createPdf(user.getEmployee(), record, false);
                if (data != null) {
                    return new DocumentData(data, record.getNumber());
                }
                
            } catch (Exception e) {
                log.warn("getPdf() failed to create cancellation pdf [id="
                        + record.getId() + ", type=" + record.getType().getId()
                        + ", message=" + e.getMessage() + "]", e);
            }
        }
        return null;
    }
    

    public DocumentData getCorrectionPdf(RecordCorrection correction) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("getCorrectionPdf() invoked [id=" + correction.getId() 
                + ", reference=" + correction.getReference() + "]");
        }
        List<DmsDocument> list = archive.findByReference(new DmsReference(correction, correction.getRecordType()));
        if (list.isEmpty()) {
            throw new ClientException(ErrorCode.CORRECTION_DOCUMENT_MISSING);
        }
        DmsDocument doc = list.get(0);
        byte[] data = archive.getDocumentData(doc);
        if (data == null) {
            throw new IllegalStateException("binary not found");
        }
        return new DocumentData(data, doc.getRealFileName());
    }

    public DmsDocument archive(Employee user, Record record, byte[] pdf) {
        try {
            User userObject = users.find(user);
            FileObject fileObject = new FileObject(archive.createFileName(record), pdf);
            DmsDocument doc = archive.create(
                    userObject,
                    new DmsReference(record, record.getType().getId()),
                    fileObject,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null);
            if (log.isDebugEnabled()) {
                log.debug("archive() done [id=" + doc.getId()
                        + ", reference=" + doc.getReference()
                        + ", fileName=" + doc.getFileName()
                        + ", fileSize=" + doc.getFileSize()
                        + "]");
            }
            return doc;
        } catch (Throwable e) {
            log.error("archive() failed [record=" + (record == null ? "null" : record.getId())
                    + ", filesize=" + (pdf == null ? 0 : pdf.length)
                    + ", message=" + e.toString() + "]");
            return null;
        }
    }

    public boolean archiveVersion(Employee user, Record record, byte[] pdf, String filename) {
        try {
            User userObject = users.find(user);
            return recordVersionsArchive.createCustom(userObject, record, pdf, filename);
        } catch (Throwable e) {
            log.error("archiveVersion() failed [record=" + (record == null ? "null" : record.getId())
                    + ", filesize=" + (pdf == null ? 0 : pdf.length)
                    + ", message=" + e.toString() + "]", e);
            return false;
        }
    }

    public DmsDocument archiveCorrection(
            Employee user,
            PaymentAwareRecord record,
            byte[] pdf) {

        FileObject fileObject = new FileObject(archive.createFileName(record), pdf);
        try {
            User userObject = users.find(user);
            DmsDocument doc = archive.create(
                    userObject,
                    new DmsReference(record.getCorrection(), record.getType().getId()),
                    fileObject,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null);
            if (log.isDebugEnabled()) {
                log.debug("archiveCorrection() done [id=" + doc.getId()
                        + ", reference=" + doc.getReference()
                        + ", fileName=" + doc.getFileName()
                        + ", fileSize=" + doc.getFileSize()
                        + "]");
            }
            return doc;
        } catch (Throwable e) {
            log.error("archiveCorrection() failed [record=" + (record == null ? "null" : record.getId())
                    + ", filesize=" + (pdf == null ? 0 : pdf.length)
                    + ", message=" + e.toString() + "]");
            return null;
        }
    }

    public void archiveCleanup(Record record) {
        try {
            List<DmsDocument> documents = archive.findByReference(new DmsReference(record, record.getType().getId()));
            if (!documents.isEmpty()) {
                for (int i = 0, j = documents.size(); i < j; i++) {
                    DmsDocument doc = documents.get(i);
                    archive.deleteDocument(doc);
                    if (log.isDebugEnabled()) {
                        log.debug("archiveCleanup() document removed [id=" + doc.getId() + "]");
                    }
                }
            }
        } catch (Throwable e) {
            log.error("archiveCleanup() failed [record=" + (record == null ? "null" : record.getId())
                    + ", message=" + e.toString() + "]");
        }
    }

    protected DmsDocument fetchDocument(Record record) {
        return findDocument(new DmsReference(record, record.getType().getId()));
    }

    private DmsDocument findDocument(DmsReference dmsReference) {
        List<DmsDocument> list = archive.findByReference(dmsReference);
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
}
