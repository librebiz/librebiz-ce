/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 9, 2008 10:21:45 PM 
 * 
 */
package com.osserp.core.dms;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface LetterManager extends LetterContentManager {

    /**
     * Provides the count of existing letters of a contact
     * @param type
     * @param contact
     * @param businessId
     * @return count
     */
    int getCount(LetterType type, ClassifiedContact contact, Long businessId);

    /**
     * Provides a letter by id
     * @param id
     * @return letter or null if not exists
     */
    Letter findLetter(Long id);

    /**
     * Provides all existing letters to a contact
     * @param type
     * @param contact
     * @param businessId
     * @return letters
     */
    List<Letter> findLetters(LetterType type, ClassifiedContact contact, Long businessId);

    /**
     * Creates a new letter
     * @param user creating new letter
     * @param type of the letter to create
     * @param recipient of the letter
     * @param name
     * @param businessId is optional reference
     * @param branchId
     * @param template
     * @return letter new created
     * @throws ClientException if name already exists
     */
    Letter create(
            DomainUser user,
            LetterType type,
            ClassifiedContact recipient,
            String name,
            Long businessId,
            Long branchId,
            LetterTemplate template)
            throws ClientException;

    /**
     * Updates letter setup
     * @param user
     * @param letter
     * @param header
     * @param name
     * @param street
     * @param zipcode
     * @param city
     * @param country
     * @param subject
     * @param salutation
     * @param greetings
     * @param language
     * @param signatureLeft
     * @param signatureRight
     * @param printDate
     * @param ignoreSalutation
     * @param ignoreGreetings
     * @param ignoreHeader
     * @param ignoreSubject
     * @param embedded
     * @param embeddedAbove
     * @param embeddedSpaceAfter
     * @param contentKeepTogether
     * @throws ClientException if validation failed
     */
    void update(
            DomainUser user,
            Letter letter,
            String header,
            String name,
            String street,
            String zipcode,
            String city,
            Long country,
            String subject,
            String salutation,
            String greetings,
            String language,
            Long signatureLeft,
            Long signatureRight,
            Date printDate,
            boolean ignoreSalutation,
            boolean ignoreGreetings,
            boolean ignoreHeader,
            boolean ignoreSubject,
            boolean embedded,
            boolean embeddedAbove,
            String embeddedSpaceAfter,
            String contentKeepTogether)
            throws ClientException;

    /**
     * Updates parameters of a letter
     * @param user
     * @param letter
     * @param parameters
     */
    void updateParameters(
            DomainUser user,
            Letter letter,
            Map<String, String> parameters);

    /**
     * Closes a letter by freezing content and creating persistent pdf.
     * @param user
     * @param letter
     * @throws ClientException
     */
    void close(DomainUser user, Letter letter) throws ClientException;
}
