/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 14, 2014 11:19:31 AM
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateUtil;

import com.osserp.core.dao.records.CommonPayments;
import com.osserp.core.dao.records.PurchaseInvoices;
import com.osserp.core.dao.records.PurchasePayments;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.dao.records.TaxReports;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.CommonPayment;
import com.osserp.core.finance.FinanceRecord;
import com.osserp.core.finance.TaxReport;
import com.osserp.core.model.records.TaxReportImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TaxReportsDao extends AbstractRecordViews implements TaxReports {
    private static Logger log = LoggerFactory.getLogger(TaxReportsDao.class.getName());

    private CommonPayments commonPayments;
    private PurchaseInvoices purchaseInvoices;
    private PurchasePayments purchasePayments;
    private SalesInvoices salesInvoices;
    private SalesPayments salesPayments;
    
    protected TaxReportsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            RecordTypes recordTypesDao,
            CommonPayments commonPayments,
            PurchaseInvoices purchaseInvoices,
            PurchasePayments purchasePayments,
            SalesInvoices salesInvoices,
            SalesPayments salesPayments) {
        super(jdbcTemplate, tables, sessionFactory, recordTypesDao);
        this.commonPayments = commonPayments;
        this.purchaseInvoices = purchaseInvoices;
        this.purchasePayments = purchasePayments;
        this.salesInvoices = salesInvoices;
        this.salesPayments = salesPayments;
    }

    public List<TaxReport> getReports(Long company) {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(TaxReportImpl.class.getName()).append(" o");
        if (company != null) {
            hql.append(" where o.reference = ").append(company);
        }
        hql.append(" order by o.created desc");
        return getCurrentSession().createQuery(hql.toString()).list();
    }
    
    public TaxReport getReport(Long id) {
        TaxReport report = (TaxReport) getCurrentSession().load(TaxReportImpl.class, id);
        return loadRecords(report);
    }
    
    public TaxReport create(Long user, Long company, String name, String method, Date start, Date end) {
        TaxReport report = new TaxReportImpl(user, company, method, name, start, end);
        getCurrentSession().saveOrUpdate(TaxReportImpl.class.getName(), report);
        return loadRecords(report);
    }
    
    public TaxReport save(TaxReport report) {
        getCurrentSession().saveOrUpdate(TaxReportImpl.class.getName(), report);
        return loadRecords(report);
    }

    public TaxReport loadRecords(TaxReport report) {
        report.getInflowOfCash().clear();
        report.addInflowOfCash(getVatInflow(
                report.getReference(), 
                report.getTaxMethod(), 
                report.getStartDate(), 
                report.getStopDate()));
        report.getOutflowOfCash().clear();
        report.addOutflowOfCash(getVatOutflow(
                report.getReference(), 
                report.getTaxMethod(), 
                report.getStartDate(), 
                report.getStopDate()));
        return report;
    }

    protected List<FinanceRecord> getVatInflow(Long company, String method, Date start, Date end) {
        Date stop = DateUtil.addDays(end, 1);
        List<? extends FinanceRecord> result = new ArrayList<>();
        if (TaxReport.VAT_BY_TARGET.equals(method)) {
            result = salesInvoices.getByDate(start, stop);
            if (log.isDebugEnabled()) {
                log.debug("getVatInflow() done [method=" + method
                        + ", count=" + result.size()
                        + "]");
            }
        } else {
            result = salesPayments.getByDate(start, stop);
            if (log.isDebugEnabled()) {
                log.debug("getVatInflow() done  [method=" + method
                        + ", count=" + result.size()
                        + "]");
            }
        }
        return filterByCompany(result, company);
    }

    protected List<FinanceRecord> getVatOutflow(Long company, String method, Date start, Date end) {
        Date stop = DateUtil.addDays(end, 1);
        List<FinanceRecord> result = new ArrayList(purchaseInvoices.getByDate(start, stop));
        List<? extends FinanceRecord> common = commonPayments.getByDate(start, stop);
        for (int i = 0, j = common.size(); i < j; i++) {
            CommonPayment record = (CommonPayment) common.get(i);
            if (!record.isTaxPayment()) {
                BillingType billingType = (BillingType) record.getType();
                if (!billingType.isSalary() && !billingType.isNonSalary()) {
                    result.add(record);
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getVatOutflow() done [method=" + method 
                    + ", count=" + result.size()
                    + "]");
        }
        return filterByCompany(result, company);
    }

    private List<FinanceRecord> filterByCompany(List<? extends FinanceRecord> list, Long company) {
        List<FinanceRecord> result = new ArrayList<>();
        for (int i = 0, j = list.size(); i < j; i++) {
            FinanceRecord record = list.get(i);
            if (company != null && company.equals(record.getCompany())) {
                result.add(record);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("filterByCompany() done [count=" + result.size() + "]");
        }
        return result;
    }
}
