/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 29, 2013
 *  
 */
package com.osserp.common.directory.ldap.dao

import com.osserp.common.directory.ldap.LdapException

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
interface AdminAccounts {

    /**
     * Finds all admin accounts
     * @param client if search should be limited to client based dn
     * @return accounts or empty list
     */
    List<Map> findAll(String client)

    /**
     * Finds an admin account by uid
     * @param uid
     * @param client if search should be limited to client based dn
     * @return account or empty map if not exists
     */
    Map find(String uid, String client)

    /**
     * Creates an admin account if not exist
     * @param attrs
     * @param client if account should be related to a dedicated client
     * @return new created account
     */
    Map create(Map attrs, String client)

    /**
     * Changes admin account password
     * @param account
     * @param password
     * @return account with updated values
     */
    Map changePassword(Map account, String password)

    /**
     * Deletes an admin account
     * @param account
     */
    void delete(Map account)

    /**
     * Checks password and provides the entry
     * @param uid
     * @param password
     * @param client if account should be related to a dedicated client
     * @return admin account entry
     * @throws LdapException if password does not match
     */
    Map login(String uid, String password, String client) throws LdapException
}
