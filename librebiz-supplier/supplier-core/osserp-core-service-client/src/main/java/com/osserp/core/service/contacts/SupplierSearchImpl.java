/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16-Jan-2007 09:51:12 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.suppliers.SupplierSearch;
import com.osserp.core.suppliers.SupplierType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SupplierSearchImpl extends AbstractContactRelationSearch implements SupplierSearch {

    protected SupplierSearchImpl(
            Contacts contactsDao,
            Suppliers suppliers) {
        super(contactsDao, suppliers);
    }

    public Supplier findById(Long id) throws ClientException {
        return (Supplier) find(id);
    }

    public List<Supplier> findByProductGroup(Long groupId) {
        return getSuppliersDao().findByProductGroup(groupId);
    }

    public List<Supplier> findByDeliveries(Long productId) {
        return getSuppliersDao().findByDeliveries(productId);
    }

    public List<Supplier> findByDirectInvoiceBooking() {
        return getSuppliersDao().findByDirectInvoiceBooking();
    }
    
    public List<Supplier> findBySimpleBilling() {
        return getSuppliersDao().findBySimpleBilling();
    }
    
    public List<Supplier> getSalaryAccounts() {
        return getSimpleBillingAccounts(true);
    }
    
    public List<Supplier> getSimpleBillingAccounts() {
        return getSimpleBillingAccounts(false);
    }
    
    private List<Supplier> getSimpleBillingAccounts(boolean salary) {
        List<Supplier> result = new ArrayList<Supplier>();
        List<Supplier> list = getSuppliersDao().findBySimpleBilling();
        for (int i = 0, j = list.size(); i < j; i++) {
            Supplier next = list.get(i);
            if (salary && next.getSupplierType().isSalaryAccount()) {
                result.add(next);
            } else if (!salary && !next.getSupplierType().isSalaryAccount()) {
                result.add(next);
            }
        }
        return result;
    }

    public List<SupplierType> getSupplierTypeSelection() {
        List<SupplierType> result = new ArrayList<SupplierType>();
        List<SupplierType> list = getSuppliersDao().getSupplierTypes();
        for (int i = 0, j = list.size(); i < j; i++) {
            SupplierType next = list.get(i);
            if (!next.isEndOfLife()) {
                result.add(next);
            }
        }
        return result;
    }
    
    protected Suppliers getSuppliersDao() {
        return (Suppliers) getContactRelationsDao();
    }
}
