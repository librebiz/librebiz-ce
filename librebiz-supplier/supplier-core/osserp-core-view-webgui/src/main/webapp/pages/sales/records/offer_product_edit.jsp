<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesOfferView}"><c:redirect url="/errors/error_context.jsp"/></c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesOfferView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:if test="${empty record.items and empty sessionScope.salesOfferProductForwardExecuted}">
<c:set var="salesOfferProductForwardExecuted" scope="session" value="true"/>		
<c:redirect url="/salesOfferProducts.do?method=forwardSelection"/>
</c:if>
<c:set var="productUrl" scope="request" value="/salesOfferProducts.do"/>
<c:set var="productExitTarget" scope="request" value="salesOfferProductsReload"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records.css"/>
</style>
</tiles:put>
<tiles:put name="headline">
    <c:import url="/pages/records/record_items_edit_headline.jsp"/>
</tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/salesOfferProducts.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<v:link url="/records/recordPrint/print?view=salesOfferView" title="documentPrint"><o:img name="printIcon" /></v:link>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <c:import url="/pages/records/record_items.jsp"/>
    </div>
</div>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
