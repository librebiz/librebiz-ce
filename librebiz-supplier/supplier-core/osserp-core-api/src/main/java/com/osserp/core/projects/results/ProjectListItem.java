/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 31, 2005 1:32:37 PM 
 * 
 */
package com.osserp.core.projects.results;

import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.Option;

import com.osserp.core.BusinessType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectListItem extends Option {

    /**
     * Returns the planId
     * @return planId.
     */
    Long getPlanId();

    /**
     * Sets the planId
     * @param planId
     */
    void setPlanId(Long planId);

    /**
     * Returns the type
     * @return type.
     */
    BusinessType getType();

    /**
     * Sets the type
     * @param type
     */
    void setType(BusinessType type);

    /**
     * Returns the status
     * @return status.
     */
    Integer getStatus();

    /**
     * Sets the status
     * @param status
     */
    void setStatus(Integer status);

    /**
     * Returns the managerId
     * @return managerId.
     */
    Long getManagerId();

    /**
     * Sets the managerId
     * @param managerId
     */
    void setManagerId(Long managerId);

    /**
     * Returns the salesId
     * @return salesId.
     */
    Long getSalesId();

    /**
     * Sets the salesId
     * @param salesId
     */
    void setSalesId(Long salesId);

    /**
     * Returns the branch id
     * @return branch id
     */
    Long getBranchId();

    /**
     * Sets the branch id
     * @param branchId
     */
    void setBranchId(Long branchId);

    /**
     * Returns the shipping id
     * @return shippingId
     */
    public Long getShippingId();

    /**
     * Sets the shipping id
     * @param shippingId
     */
    public void setShippingId(Long shippingId);

    /**
     * Returns the name of shipping (if supported by query)
     * @return shippingName
     */
    public String getShippingName();

    /**
     * Sets the name of shipping (if supported by query)
     * @param shippingName
     */
    public void setShippingName(String shippingName);

    /**
     * Returns the payment id
     * @return paymentId
     */
    public Long getPaymentId();

    /**
     * Sets the payment id
     * @param paymentId
     */
    public void setPaymentId(Long paymentId);

    /**
     * Returns the name of payment (if supported by query)
     * @return paymentName
     */
    public String getPaymentName();

    /**
     * Sets the name of payment (if supported by query)
     * @param paymentName
     */
    public void setPaymentName(String paymentName);

    /**
     * @return returns the flowControlSheet.
     */
    List getFlowControlSheet();

    /**
     * @param flowControlSheet The flowControlSheet to set.
     */
    void setFlowControlSheet(List flowControlSheet);

    /**
     * @return returns the flowControlTodos.
     */
    List getFlowControlTodos();

    /**
     * @param flowControlTodos The flowControlTodos to set.
     */
    void setFlowControlTodos(List flowControlTodos);

    /**
     * @return returns the notes.
     */
    List getNotes();

    /**
     * @param notes The notes to set.
     */
    void setNotes(List notes);

    /**
     * Provides an xml representation of this item
     * @param employeeMap with employee short keys
     * @return with item values
     */
    Element getValues(Map employeeMap);

    /**
     * Indicates that a package list is required for this sales
     * @return packageListRequired
     */
    boolean isPackageListRequired();

    /**
     * Sets that a package list is required for this sales
     * @param packageListRequired
     */
    void setPackageListRequired(boolean packageListRequired);

    /**
     * Indicates that appointment is not completely deliverable as planned, e.g. purchase orders/deliveries missing
     * @return vacant
     */
    boolean isVacant();

    /**
     * Enables/disables vacant flag
     * @param vacant
     */
    void setVacant(boolean vacant);
}
