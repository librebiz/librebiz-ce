<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="recordingView" value="${sessionScope.timeRecordingView}"/>
<c:set var="period" value="${recordingView.timeRecording.selectedPeriod}"/>
<c:set var="config" value="${period.config}"/>
<c:set var="selectedMonth" value="${period.selectedYear.selectedMonth}"/>
<c:set var="deletePermissionGrant" value="false"/>
<c:set var="hrmPermissionGrant" value="false"/>
<o:permission role="time_record_delete" info="permissionTimeRecordingDeleteBooking">
    <c:set var="deletePermissionGrant" value="true"/>
</o:permission>
<o:permission role="hrm,disciplinarian_view,client_edit" info="permissionShowTimeRecordConfigDisplay">
    <c:set var="hrmPermissionGrant" value="true"/>
</o:permission>
<c:if test="${!empty selectedMonth}">
    <o:logger write="${'[selectedMonth='}${selectedMonth.month}${', config='}${config.id}${']'}" level="debug"/>
</c:if>
<c:choose>
    <c:when test="${recordingView.listDescendentMode}">
        <c:set var="days" value="${selectedMonth.reverseDays}"/>
    </c:when>
    <c:otherwise>
        <c:set var="days" value="${selectedMonth.days}"/>
    </c:otherwise>
</c:choose>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 20px;"><a href="<c:url value="/timeRecordings.do?method=toggleListDetailsMode"/>"><fmt:message key="day"/></a></th>
                    <th style="width: 75px;"><a href="<c:url value="/timeRecordings.do?method=toggleListDescendentMode"/>"><fmt:message key="date"/></a></th>
                    <th style="width: 75px;"><fmt:message key="time"/></th>
                    <th style="width: 281px;"><fmt:message key="info"/></th>
                    <th style="width: 50px;"><fmt:message key="hours"/></th>
                    <th style="width: 50px;" class="center" title="<fmt:message key="timeOutsideLimit"/>"><fmt:message key="timeOutsideLimitShortkey"/></th>
                    <th style="width: 50px;" class="center" title="<fmt:message key="timeOutsideTarget"/>"><fmt:message key="timeOutsideTargetShortkey"/></th>
                    <th style="width: 50px;" class="center" title="<fmt:message key="timeBank"/>"><fmt:message key="timeBankShortkey"/></th>
                    <th style="width: 50px;" class="center"><fmt:message key="action"/></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="day" items="${days}">
                    <tr class="<o:out value="${day.weekend or day.publicHoliday ? 'grey' : day.waitForApproval ? 'red' : 'altrow'}"/>">
                        <td><o:out value="${day.dayDisplayKey}"/></td>
                        <td><o:date value="${day.date}"/></td>
                        <c:choose>
                            <c:when test="${!empty day.startTime}">
                                <td>
                                    <c:choose>
                                        <c:when test="${empty day.stopTime}">
                                            <o:time value="${day.startTime}"/> - <fmt:message key="emptyTimeDisplay"/>
                                        </c:when>
                                        <c:otherwise>
                                            <o:time value="${day.startTime}"/> - <o:time value="${day.stopTime}"/>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <o:out value="${day.workingHoursDisplay}"/>
                                    <c:if test="${day.minutesBreak > 0}"> (PA-<o:out value="${day.minutesBreak}"/>)</c:if>
                                </td>
                                <c:choose>
                                    <c:when test="${day.minutesDifference < 0}">
                                        <td class="bolderror"><o:out value="${day.hoursDisplay}"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="boldtext"><o:out value="${day.hoursDisplay}"/></td>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:when test="${!empty day.name}">
                                <td></td>
                                <td colspan="2"><o:out value="${day.name}"/></td>
                            </c:when>
                            <c:when test="${day.publicHoliday}">
                                <td class="boldtext" colspan="3"><o:out value="${day.publicHolidayName}"/></td>
                            </c:when>
                            <c:when test="${day.beyondRange and !day.weekend}">
                                <td></td>
                                <td colspan="2"><fmt:message key="noRecording"/></td>
                            </c:when>
                            <c:when test="${day.past and !day.weekend }">
                                <td></td>
                                <c:choose>
                                    <c:when test="${config.dailyMinutes != 0 and config.recordingHours}">
                                        <td class="error" colspan="2"><fmt:message key="missingLabel"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td  colspan="2"></td>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <td colspan="3"></td>
                            </c:otherwise>
                        </c:choose>
                        <td class="center">
                            <c:choose>
                                <c:when test="${day.minutesLost == 0}">
                                    <c:if test="${day.marker}">
                                        <a href="javascript:void(0)" onmouseover="Tip('<fmt:message key="workingTimeLimitDay"/> <o:number value="${day.markerValue}" format="hours"/> <fmt:message key="hours"/>',TITLE, '<fmt:message key="info"/>')" onmouseout="UnTip()">
                                            <img src="<c:url value="${applicationScope.importantIcon}"/>" class="bigicon"/>
                                        </a>
                                    </c:if>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${day.marker}">
                                            <a href="javascript:void(0)" onmouseover="Tip('<fmt:message key="workingTimeLimitDay"/> <o:number value="${day.markerValue}" format="hours"/> <fmt:message key="hours"/>',TITLE, '<fmt:message key="info"/>')" onmouseout="UnTip()">
                                                <o:out value="${day.lostHoursDisplay}"/>
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${day.lostHoursDisplay}"/>
                                        </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <c:choose>
                            <c:when test="${day.beyondRange or day.ignoreWorkingSummary or day.future or day.today}">
                                <td></td>
                                <td></td>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${day.minutesDifference == 0}">
                                        <td></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="right <c:if test="${day.minutesDifference < 0}">error</c:if>"><o:out value="${day.differenceHoursDisplay}"/></td>
                                    </c:otherwise>
                                </c:choose>
                                <td class="right <c:if test="${day.minutesCarryoverInMonth < 0}">error</c:if>"><o:out value="${day.carryoverHoursInMonthDisplay}"/></td>
                            </c:otherwise>
                        </c:choose>
                        <td class="center">
                            <o:ajaxLink linkId="monthSelection" url="/timeRecordings.do?method=forwardCreate&d=${day.day}&m=${day.month}&y=${day.year}&intervalOnly=true" title="newBooking">
                                <img src="<c:url value="${applicationScope.clockIcon}"/>"/>
                            </o:ajaxLink>
                        </td>
                    </tr>
                    <c:if test="${recordingView.listDetailsMode}">
                        <c:forEach var="record" items="${day.records}">
                            <c:if test="${record.type.visible || hrmPermissionGrant}">
                                <tr>
                                    <td colspan="2"></td>
                                    <td>
                                        <c:if test="${!record.type.addition && !record.type.subtraction}">
                                            <c:choose>
                                                <c:when test="${empty record.closedBy}">
                                                    <o:time value="${record.value}"/> - <fmt:message key="emptyTimeDisplay"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <o:time value="${record.value}"/> - <o:time value="${record.closedBy.value}"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:if>
                                    </td>
                                    <td>
                                        <span style="float:left;">
                                            <c:choose>
                                                <c:when test="${hrmPermissionGrant}">
                                                    <o:ajaxLink linkId="recordType${record.id}" url="${'/timeRecordConfigDisplay.do?id='}${record.type.id}">
                                                        <span title="<fmt:message key="type"/> <o:out value="${record.type.id}"/>">
                                                            <c:choose>
                                                                <c:when test="${record.type.defaultType}">
                                                                    <fmt:message key="comingAndGoingLabel"/>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <o:out value="${record.type.name}"/>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </span>
                                                    </o:ajaxLink>
                                                </c:when>
                                                <c:otherwise>
                                                    <span title="<fmt:message key="type"/> <o:out value="${record.type.id}"/>">
                                                        <c:choose>
                                                            <c:when test="${record.type.defaultType}">
                                                                <fmt:message key="comingAndGoingLabel"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <o:out value="${record.type.name}"/>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </span>
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                                        <span style="float:right;">
                                            <c:if test="${!empty record.note}">
                                                <span onmouseover="Tip('<o:out value="${record.note}"/>',TITLE, '<fmt:message key="comment"/>')" onmouseout="UnTip()">
                                                    <o:img styleClass="bigIcon" name="userCommentIcon"/>
                                                </span>
                                            </c:if>
                                            <c:if test="${record.correctionAvailable}">
                                                <span onmouseover="Tip('<fmt:message key="correctionAvailable"/>',TITLE, '')" onmouseout="UnTip()">
                                                    <o:img styleClass="bigIcon" name="userEditIcon"/>
                                                </span>
                                            </c:if>
                                            <c:if test="${record.waitForApproval}">
                                                <span onmouseover="Tip('<fmt:message key="waitForApproval"/>',TITLE, '')" onmouseout="UnTip()">
                                                    <o:img styleClass="bigIcon" name="groupErrorIcon"/>
                                                </span>
                                            </c:if>
                                            <c:if test="${record.approved}">
                                                <span onmouseover="Tip('<fmt:message key="timeRecordApproved"/>',TITLE, '')" onmouseout="UnTip()">
                                                    <o:img styleClass="bigIcon" name="groupIcon"/>
                                                </span>
                                            </c:if>
                                        </span>
                                    </td>
                                    <td><o:out value="${record.durationDisplay}"/></td>
                                    <td colspan="3">&nbsp;</td>
                                    <td class="center">
                                        <c:if test="${!record.waitForApproval}">
                                            <o:ajaxLink linkId="recordUpdate" url="/timeRecordings.do?method=selectRecord&id=${record.id}&target=recordUpdate">
                                                <img src="<c:url value="${applicationScope.writeIcon}"/>" class="bigicon" title="<fmt:message key="addCorrection"/>"/>
                                            </o:ajaxLink>
                                        </c:if>
                                        <c:if test="${deletePermissionGrant}">
                                            <span style="margin-left: 5px;"><a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteBooking"/>','<c:url value="/timeRecordings.do"><c:param name="method" value="deleteRecord"/><c:param name="id" value="${record.id}"/></c:url>');" title="<fmt:message key="deleteBooking"/>"><o:img name="deleteIcon" styleClass="bigicon"/></a></span>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
