<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<c:if test="${!empty requestScope.recordEmailHistory}">
    <c:set var="list" value="${requestScope.recordEmailHistory}" />
    <table class="table" style="width: 450px;">
        <tr>
            <th id="datum"><fmt:message key="dateTime" /></th>
            <th id="name"><fmt:message key="originator" /> / <fmt:message key="recipient" /></th>
        </tr>
        <c:forEach var="obj" items="${list}">
            <tr class="altrow">
                <td><o:date value="${obj.created}" addtime="true" /></td>
                <td style=""><oc:employee value="${obj.createdBy}" /></td>
            </tr>
            <tr>
                <td><fmt:message key="recipientTO" /></td>
                <td style=""><o:out value="${obj.recipient}" /></td>
            </tr>
            <c:if test="${!empty obj.recipientsCC}">
                <tr>
                    <td>CC</td>
                    <td style=""><o:out value="${obj.recipientsCC}" /></td>
                </tr>
            </c:if>
        </c:forEach>
    </table>
</c:if>