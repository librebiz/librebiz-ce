<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:if test="${view.multipleBranchsAvailable}">
    <tr>
        <td>
            <c:choose>
                <c:when test="${view.record.sales}"><fmt:message key="branchOffice"/></c:when>
                <c:otherwise><fmt:message key="purchaser"/></c:otherwise>
            </c:choose>
        </td>
        <td><oc:select name="branch" value="${view.record.branchId}" options="${view.branchOfficeList}" styleClass="input" prompt="true" /></td>
    </tr>
</c:if>
