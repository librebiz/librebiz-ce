/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.beans

import com.osserp.common.util.PropertyUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class Configurable extends CommonClass {

    /**
     * Provides the default application name. Defaults to 'osserp'.
     * Application name is used for config file directory lookups.
     * if configurable was configured by that file
     */
    String applicationName = 'osserp'

    /**
     * Provides the absolute file and pathname of the config file
     * if configurable was configured by that file
     */
    String configFile

    /**
     * Provides the name of the file used to load the configuration
     */
    String cfgFileName

    /**
     * Indicates that configurable is initialized
     * @return initialized
     */
    abstract boolean getInitialized()


    /**
     * Provides the path and name of the config file
     * @return configFileName
     */
    protected abstract String getConfigFileName()


    /**
     * Tries to read in properties from a config file. The config file is located as follows:
     * <br/>-Method first looks up for a system property matching ${applicationName}.config
     * <br/>-Filename will be initialized with value of ${configFileName}
     * <br/>-Lookup for any of the generated filenames above in the following directories
     * <br/>'/home/{user}', '/home/{user}/.groovy' and '/home/{user}/.groovy/{applicationName}
     * @return properties
     */
    protected Properties findProperties() {
        Properties result = new Properties()
        cfgFileName = System.getProperty("${applicationName}.config")
        if (!cfgFileName) {
            cfgFileName = getConfigFileName()
        }
        findPropertiesByFilename(cfgFileName)
    }


    /**
     * Tries to read in properties from a given config filename.
     * <br/>-Filename lookup will be performed in the following directories
     * <br/>'./', '/home/{user}', '/home/{user}/.groovy' and '/home/{user}/.groovy/{applicationName}
     * @param filename
     * @return properties
     */
    protected Properties findPropertiesByFilename(String filename) {
        Properties result = new Properties()
        cfgFileName = filename
        File file = locate(filename)
        if (file.exists() && !file.isDirectory()) {
            configFile = file.path
            result = PropertyUtil.load(file)
        }
        return result
    }


    /**
     *
     * Tries to locate the file. The locator returns the file as provided by filename 
     * if it's absolute and was immediately found. 
     * If the file does not exist as provided the locator attempts to find the file under
     * default directories as provided by defaultLocations method. Last one found wins.
     * @param filename
     * @return file
     */
    private File locate(String filename) {
        File file = new File(filename)
        if (file.exists()) {
            return file
        }
        def locations = getDefaultLocations()
        locations.each {
            File location = new File(it)
            if (location.exists()) {
                File next = new File("$it/${file.name}")
                if (next.exists() && !next.isDirectory()) {
                    file = next
                } else {
                    // lookup hidden
                    next = new File("$it/.${file.name}")
                    if (next.exists() && !next.isDirectory()) {
                        file = next
                    }
                }
            }
        }
        return file
    }

    /**
     * Provides the default locations for filename lookups.
     * <br/>'/usr/local/{applicationName}/conf'
     * <br/>'/usr/local/{applicationName}/etc'
     * <br/>'/usr/local/{applicationName}'
     * <br/>'/etc/{applicationName}/conf'
     * <br/>'/etc/{applicationName}'
     * <br/>'/etc'
     * <br/>'/home/{user}/.groovy/{applicationName}
     * <br/>'/home/{user}/.groovy'
     * <br/>'/home/{user}'
     * <br/>'./'
     * @return locations
     */
    List<String> getDefaultLocations() {
        def usrLocalApp = "/usr/local/$applicationName"
        def usrLocalAppConf = "/usr/local/$applicationName/conf"
        def usrLocalAppEtc = "/usr/local/$applicationName/etc"
        def etc = "/etc"
        def etcApp = "/etc/$applicationName"
        def etcAppConf = "/etc/$applicationName/conf"
        def currentDir = System.getProperty('user.dir')
        def home = System.getProperty('user.home')
        def groovy = "$home/.groovy"
        def gapp = "$groovy/${getApplicationName()}"
        [
            usrLocalApp,
            usrLocalAppConf,
            usrLocalAppEtc,
            etcAppConf,
            etcApp,
            etc,
            gapp,
            groovy,
            home,
            currentDir
        ]
    }
}
