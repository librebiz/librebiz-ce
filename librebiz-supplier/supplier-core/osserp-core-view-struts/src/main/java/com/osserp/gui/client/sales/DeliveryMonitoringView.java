/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 6, 2007 12:00:53 PM 
 * 
 */
package com.osserp.gui.client.sales;

import java.util.List;

import com.osserp.common.web.ViewName;

import com.osserp.core.sales.SalesListItem;

import com.osserp.gui.client.products.StockSelectingView;
import com.osserp.gui.client.sales.impl.DeliveryMonitoringListItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("deliveryMonitoringView")
public interface DeliveryMonitoringView extends StockSelectingView {

    /**
     * Provides monitoring data sorted by sales
     * @return monitoringBySales
     */
    public List<DeliveryMonitoringListItem> getMonitoringBySales();

    /**
     * Indicates that monitoring by sales is enabled/disabled
     * @return monitoringBySalesMode
     */
    public boolean isMonitoringBySalesMode();

    /**
     * Enables/diables monitoring by sales mode
     * @param monitoringBySalesMode
     */
    public void setMonitoringBySalesMode(boolean monitoringBySalesMode);

    /**
     * Provides the current selected sales after enabling delivery items display
     * @return sales
     */
    public SalesListItem getSales();

    /**
     * Indicates that view is in snip list mode
     * @return snipListMode
     */
    public boolean isSnipListMode();

    /**
     * Enables/disables snipListMode
     * @param snipListMode
     */
    public void setSnipListMode(boolean snipListMode);

    /**
     * Snips list
     * @param id to snip from list
     */
    public void snipList(Long id);

    /**
     * Indicates that view lists values descendant
     * @return listDescendantMode
     */
    public boolean isListDescendantMode();

    /**
     * Toggles between list descendant enabled/disabled
     */
    public void toggleListDescendantMode();

    /**
     * Indicates that view displays vacant orders too
     * @return vacantMode
     */
    public boolean isListVacantMode();

    /**
     * Enables/disables vacant display
     */
    public void toggleListVacantMode();

    /**
     * Refreshs monitoring data
     */
    public void refresh();

}
