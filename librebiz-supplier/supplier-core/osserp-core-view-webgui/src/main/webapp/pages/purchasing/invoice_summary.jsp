<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="actionUrl" value="${requestScope.baseUrl}"/>
<c:set var="record" value="${sessionScope.recordView.record}"/>
<c:set var="amounts" value="${record.amounts}"/>
<table class="recordSummary">
	<tr>
		<td class="phead"><fmt:message key="invoiceAmount"/> <span class="desc">(<fmt:message key="netTurnoverTaxGross"/>)</span></td>
		<c:choose>
			<c:when test="${record.taxFree}">
				<td class="pnet"><o:number value="${amounts.amount}" format="currency"/> </td>
				<td class="ptax"><o:number value="0.00" format="currency"/> </td>
				<td class="pamount"><o:number value="${amounts.amount}" format="currency"/> </td>
			</c:when>
			<c:otherwise>
				<td class="pnet"><o:number value="${amounts.amount}" format="currency"/> </td>
				<td class="ptax"><o:number value="${amounts.taxAmount + amounts.reducedTaxAmount}" format="currency"/> </td>
				<td class="pamount"><o:number value="${amounts.grossAmount}" format="currency"/> </td>
			</c:otherwise>
		</c:choose>
	</tr>
	<tr>
		<td colspan="4" class="pspace"></td>
	</tr>
	<c:forEach var="payment" items="${record.payments}" varStatus="s">
		<tr>
			<c:choose>
				<c:when test="${s.index == 0}">
					<td colspan="2" class="payment"><fmt:message key="payments"/></td>
				</c:when>
				<c:otherwise>
					<td colspan="2" class="payment"></td>
				</c:otherwise>
			</c:choose>
			<td class="pdate" style="text-align: right;">
                <span>
                    <o:date value="${payment.paid}"/>
                </span>
			</td>
			<td class="pamount"><o:number value="${payment.amount}" format="currency"/> </td>
		</tr>
	</c:forEach>
	<tr>
		<td colspan="4" class="pspace"></td>
	</tr>
	<tr>
		<td colspan="2" class="payment"><fmt:message key="openAmount"/></td>
		<td class="pdate"><o:date current="true"/></td>
		<td class="pamount"><o:number value="${record.dueAmount}" format="currency"/> </td>
	</tr>
	<tr>
		<td colspan="4" class="plast"></td>
	</tr>
</table>
