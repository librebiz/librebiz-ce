<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeSelectionView" />

<style type="text/css">
.name {
	
}
</style>

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <c:choose>
            <c:when test="${view.searchMode}">
                <fmt:message key="employeeSearch" />
            </c:when>
            <c:otherwise>
                <fmt:message key="employeeSelection" />
            </c:otherwise>
        </c:choose>
    </div>
</div>

<div class="modalBoxData">
    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <c:if test="${view.searchMode}">
                <div id="employeeSelectionSearch">
                    <o:ajaxLookupForm name="employeeSelectionSearchForm" url="${view.appLink}/search" targetElement="employeeSelectionResult" minChars="0" events="onkeyup" activityElement="value">
                        <input style="background: whitesmoke;" type="text" name="value" id="value" value="<o:out value="${view.searchPattern}"/>" />
                    </o:ajaxLookupForm>
                </div>
            </c:if>
            <div id="employeeSelectionResult">
                <c:import url="_employeeSelectionResult.jsp" />
            </div>
            <div class="spacer"></div>
        </div>
    </div>
</div>
