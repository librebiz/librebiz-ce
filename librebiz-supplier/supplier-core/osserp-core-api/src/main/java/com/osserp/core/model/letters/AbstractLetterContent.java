/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 9:06:35 PM 
 * 
 */
package com.osserp.core.model.letters;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.AddressValidator;
import com.osserp.common.util.CollectionUtil;
import com.osserp.core.Address;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterParagraph;
import com.osserp.core.dms.LetterParameter;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.AddressImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractLetterContent extends AbstractLetterComponent implements LetterContent {

    private Long branchId = null;
    private LetterType type = null;
    private String subject = null;
    private String salutation = null;
    private Long signatureLeft = null;
    private Long signatureRight = null;
    private String language = null;

    private Address address = new AddressImpl();

    private List<LetterParagraph> paragraphs = new ArrayList<>();
    private List<LetterParameter> parameters = new ArrayList<>();

    protected AbstractLetterContent() {
        super();
    }

    protected AbstractLetterContent(LetterContent other) {
        super((AbstractLetterComponent) other);
        this.address = other.getAddress();
        this.type = other.getType();
        this.signatureLeft = other.getSignatureLeft();
        this.signatureRight = other.getSignatureRight();
        this.paragraphs = other.getParagraphs();
        this.parameters = other.getParameters();
        this.branchId = other.getBranchId();
        this.salutation = other.getSalutation();
        this.language = other.getLanguage();
    }

    /**
     * Creates new letter content
     * @param id for the new letter
     * @param reference
     * @param user creating the letter
     * @param type of the letter
     * @param name of the letter
     * @param branchId
     */
    public AbstractLetterContent(
            Long id,
            Long reference,
            Employee user,
            LetterType type,
            String name,
            Long branchId) {

        super(id,
                reference,
                user,
                name,
                type.getGreetings(),
                type.isIgnoreSalutation(),
                type.isIgnoreGreetings(),
                type.isIgnoreHeader(),
                type.isIgnoreSubject(),
                type.isEmbedded(),
                type.isEmbeddedAbove(),
                type.getEmbeddedSpaceAfter(),
                type.getContentKeepTogether());
        this.type = type;
        this.branchId = (branchId == null ? Constants.HEADQUARTER : branchId);
        this.language = Constants.DEFAULT_LANGUAGE;
        this.subject = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    protected void updateAdress(
            String header,
            String name,
            String street,
            String zipcode,
            String city,
            Long country)
            throws ClientException {

        if (address == null) {
            this.address = new AddressImpl();
        }
        this.address.setHeader(header);
        this.address.setName(name);
        this.address.setStreet(street);
        this.address.setZipcode(zipcode);
        this.address.setCity(city);
        this.address.setCountry(country);
        AddressValidator.validateAddress(street, zipcode, city, country, true);
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public LetterType getType() {
        return type;
    }

    protected void setType(LetterType type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public Long getSignatureLeft() {
        return signatureLeft;
    }

    public void setSignatureLeft(Long signatureLeft) {
        this.signatureLeft = signatureLeft;
    }

    public Long getSignatureRight() {
        return signatureRight;
    }

    public void setSignatureRight(Long signatureRight) {
        this.signatureRight = signatureRight;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Locale getLocale() {
        return (language == null
                ? Constants.DEFAULT_LOCALE_OBJECT
                : new Locale(language));
    }

    public void update(
            Employee user,
            String subject,
            String salutation,
            String greetings,
            String language,
            Long signatureLeft,
            Long signatureRight,
            boolean ignoreSalutation,
            boolean ignoreGreetings,
            boolean ignoreHeader,
            boolean ignoreSubject,
            boolean embedded,
            boolean embeddedAbove,
            String embeddedSpaceAfter,
            String contentKeepTogether)
            throws ClientException {

        this.subject = subject;
        this.salutation = salutation;
        this.language = language;
        this.signatureLeft = signatureLeft;
        this.signatureRight = signatureRight;
        setGreetings(greetings);
        setIgnoreSalutation(ignoreSalutation);
        setIgnoreGreetings(ignoreGreetings);
        setIgnoreHeader(ignoreHeader);
        setIgnoreSubject(ignoreSubject);
        setEmbedded(embedded);
        setEmbeddedAbove(embeddedAbove);
        setEmbeddedSpaceAfter(embeddedSpaceAfter);
        setContentKeepTogether(contentKeepTogether);
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(user.getId());
    }

    public List<LetterParagraph> getParagraphs() {
        return paragraphs;
    }

    protected void setParagraphs(List<LetterParagraph> paragraphs) {
        this.paragraphs = paragraphs;
    }

    public abstract void addParagraph(Employee user);

    public void moveParagraphDown(Long id) {
        if (id != null) {
            int itemCount = paragraphs.size();
            if (!(itemCount > 1)) {
                // nothing to do  
                return;
            }
            LetterParagraph itemToMove = (LetterParagraph) CollectionUtil.getById(paragraphs, id);
            int currentOrder = itemToMove.getOrderId();
            if (currentOrder == itemCount) {
                // nothing to do, item is already last
                return;
            }
            for (int i = 0, j = itemCount; i < j; i++) {
                LetterParagraph next = paragraphs.get(i);
                if (next.getOrderId() == currentOrder + 1) {
                    next.setOrderId(currentOrder);
                    break;
                }
            }
            itemToMove.setOrderId(currentOrder + 1);
        }
    }

    public void moveParagraphUp(Long id) {
        if (id != null) {
            int itemCount = paragraphs.size();
            if (!(itemCount > 1)) {
                // nothing to do
                return;
            }
            LetterParagraph itemToMove = (LetterParagraph) CollectionUtil.getById(paragraphs, id);
            int currentOrder = itemToMove.getOrderId();
            if (currentOrder <= 1) {
                // nothing to do, item is already first 
                return;
            }
            for (int i = 0, j = itemCount; i < j; i++) {
                LetterParagraph next = paragraphs.get(i);
                if (next.getOrderId() == currentOrder - 1) {
                    next.setOrderId(currentOrder);
                    break;
                }
            }
            itemToMove.setOrderId(currentOrder - 1);
        }
    }

    public boolean isContentEmpty() {
        if (paragraphs != null && !paragraphs.isEmpty()) {
            for (int i = 0, j = paragraphs.size(); i < j; i++) {
                LetterParagraph next = paragraphs.get(i);
                if (isSet(next.getText()) || next.isTextByTemplate()) {
                    return false;
                }
            }
        }
        return true;
    }

    public void update(Employee user, Long paragraphId, String content)
            throws ClientException {
        if (isNotSet(content)) {
            throw new ClientException(ErrorCode.TEXT_MISSING);
        }
        if (content.lines().count() >= 63) {
            throw new ClientException(ErrorCode.ROW_COUNT_EXCEEDS);
        }
        for (Iterator<LetterParagraph> i = paragraphs.iterator(); i.hasNext();) {
            LetterParagraph next = i.next();
            if (next.getId().equals(paragraphId)) {
                next.setText(content);
                next.setChanged(new Date(System.currentTimeMillis()));
                next.setChangedBy(user.getId());
                break;
            }
        }
    }

    public boolean isManualInputSupported() {
        for (int i = 0, j = parameters.size(); i < j; i++) {
            LetterParameter next = parameters.get(i);
            if (next.isManualInput()) {
                return true;
            }
        }
        return false;
    }

    public List<LetterParameter> getParameters() {
        return parameters;
    }

    protected void setParameters(List<LetterParameter> parameters) {
        this.parameters = parameters;
    }

}
