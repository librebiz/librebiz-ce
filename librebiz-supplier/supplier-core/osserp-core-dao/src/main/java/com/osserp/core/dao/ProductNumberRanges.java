/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 7, 2009 8:18:36 AM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductNumberRange;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductNumberRanges {

    /**
     * Creates a new number range
     * @param user
     * @param name
     * @param start
     * @param end
     * @return numberRange
     * @throws ClientException if start or end is null or end is 0
     */
    ProductNumberRange createNumberRange(Employee user, String name, Long start, Long end) throws ClientException;
    
    /**
     * Provides a number range by start and end value
     * @param start
     * @param end
     * @return range or null if no such range exists
     */
    ProductNumberRange fetchNumberRange(Long start, Long end);

    /**
     * Provides all available number ranges
     * @return numberRanges
     */
    List<ProductNumberRange> getNumberRanges();

    /**
     * Provides an product number range config
     * @param id
     * @return numberRange
     */
    ProductNumberRange getNumberRange(Long id);

    /**
     * Updates a number range
     * @param range
     */
    void save(ProductNumberRange range);

}
