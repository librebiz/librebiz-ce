/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 4, 2009 3:48:28 AM 
 * 
 */
package com.osserp.gui.web.contacts;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.web.Actions;
import com.osserp.common.web.RequestForm;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.gui.client.contacts.PrivateContactsView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PrivateContactsAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(PrivateContactsAction.class.getName());

    @Override
    protected Class<? extends View> getViewClass() {
        return PrivateContactsView.class;
    }

    /**
     * Loads a private contact and forwards to 'display' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward load(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("load() request from " + getUser(session).getId());
        }
        PrivateContactsView view = (PrivateContactsView) fetchView(session);
        if (view == null) {
            view = (PrivateContactsView) createView(request);
        }
        super.disableCreate(mapping, form, request, response);
        view.selectByContact(RequestUtil.fetchId(request));
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Closes private contact popup TODO - currently not working due to missing js-function handling result properly
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward closePopup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("closePopup() request from " + getUser(session).getId());
        }
        this.closePopup(response);
        return null;
    }

    @Override
    public ActionForward enableEdit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        super.enableEdit(mapping, form, request, response);
        return mapping.findForward(Actions.DISPLAY);
    }

    @Override
    public ActionForward disableEdit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        super.disableEdit(mapping, form, request, response);
        return mapping.findForward(Actions.DISPLAY);
    }

    @Override
    public ActionForward enableCreate(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableCreate() request from " + getUser(session).getId());
        }
        PrivateContactsView view = (PrivateContactsView) fetchView(session);
        if (view == null) {
            view = (PrivateContactsView) createView(request);
        }
        view.setCurrentEmployee();
        super.enableCreate(mapping, form, request, response);
        return mapping.findForward(Actions.DISPLAY);
    }

    public ActionForward selectEmployee(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectEmployee() request from " + getUser(session).getId());
        }
        PrivateContactsView view = (PrivateContactsView) fetchView(session);
        view.setCurrentEmployee(RequestUtil.fetchId(request));
        return mapping.findForward(Actions.DISPLAY);
    }

    public ActionForward addContact(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("addContact() request from " + getUser(session).getId());
        }
        PrivateContactsView view = (PrivateContactsView) fetchView(session);
        view.addContact(request);
        return reloadParent(mapping);
    }

    @Override
    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrivateContactsView view = (PrivateContactsView) fetchView(request.getSession(true));
        try {
            if (view != null) {
                view.save(new RequestForm(request));
            }
        } catch (ClientException e) {
            if (log.isDebugEnabled()) {
                log.debug("save() caught client exception: " + e.getMessage());
            }
            saveError(request, e.getMessage());
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    @Override
    public ActionForward redisplay(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        super.redisplay(mapping, form, request, response);
        PrivateContactsView view = (PrivateContactsView) fetchView(request.getSession(true));
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    protected void closePopup(HttpServletResponse response) {
        String closePopup = "closePopup";
        response.setContentLength(closePopup.length());
        response.setContentType("text/html");
        try {
            PrintWriter out = response.getWriter();
            out.println(closePopup);
            out.flush();
        } catch (Exception e) {
            log.error("closePopup() failed [message=" + e.toString() + "]");
        }
    }
}
