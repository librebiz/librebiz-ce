<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.businessCaseView}"/>
<c:set var="plan" scope="request" value="${sessionScope.businessCaseView.request}"/>
<c:if test="${view.salesContext}">
	<c:set var="project" value="${sessionScope.businessCaseView.sales}"/>
	<o:logger write="invoked in sales context" level="debug"/>
</c:if>
<c:set var="salesChange" value="false"/>
<o:permission role="executive,sales_change" info="permissionProjectSalesChange">
	<c:set var="salesChange" value="true"/>
</o:permission>
<o:forbidden role="executive,sales_change">
	<c:if test="${sessionScope.businessCaseView.userRelatedSales}">
		<c:set var="salesChange" value="true"/>
	</c:if>
</o:forbidden>
<tr>
	<td>
		<c:choose>
			<c:when test="${salesChange}">
				<v:ajaxLink url="/sales/salesPersonSelector/forward" linkId="salesPersonSelectorView" title="permissionProjectSalesChange">
					<fmt:message key="sales"/>
				</v:ajaxLink>
			</c:when>
			<c:otherwise>
				<fmt:message key="sales"/>
			</c:otherwise>
		</c:choose>
	</td>
	<td>
		<c:choose>
			<c:when test="${!empty plan.salesCoId}">
				<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${plan.salesId}">
					<oc:employee value="${plan.salesId}"/>
				</v:ajaxLink>
                <span> - <o:number value="${plan.salesPercent}" format="percent"/> <fmt:message key="percent"/></span>
			</c:when>
			<c:otherwise>
				<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${plan.salesId}">
					<oc:employee value="${plan.salesId}"/>
				</v:ajaxLink>
			</c:otherwise>
		</c:choose>
	</td>
</tr>

<c:if test="${!empty plan.salesCoId}">
	<o:forbidden role="executive,sales_change,executive_sales,executive_branch">
		<tr>
			<td><fmt:message key="coSales"/></td>
			<td>
				<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${plan.salesCoId}">
					<oc:employee value="${plan.salesCoId}"/>
				</v:ajaxLink>
                <span> - <o:number value="${plan.salesCoPercent}" format="percent"/> <fmt:message key="percent"/></span> 
			</td>
		</tr>
	</o:forbidden>
	<o:permission role="executive,sales_change,executive_sales,executive_branch" info="permissionCoSales">
		<tr>
			<td>
				<v:ajaxLink url="/sales/salesPersonSelector/forward?coSales=true" linkId="salesPersonSelectorView" title="permissionProjectSalesChange">
					<fmt:message key="coSales"/>
				</v:ajaxLink>
			</td>
			<td>
				<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${plan.salesCoId}">
					<oc:employee value="${plan.salesCoId}"/>
				</v:ajaxLink>
                <v:ajaxLink url="/sales/salesPersonSelector/enableSalesCoPercentMode" linkId="salesPersonSelectorView" title="assignCoSalesPercentage">
                    <span> - <o:number value="${plan.salesCoPercent}" format="percent"/> <fmt:message key="percent"/></span>
                </v:ajaxLink>
			</td>
		</tr>
	</o:permission>
</c:if>
