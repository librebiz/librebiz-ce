/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.apache.directory.groovyldap.LDAP
import org.apache.directory.groovyldap.SearchScope

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.StructuralSchema
import com.osserp.common.directory.ldap.dao.Internetdomains

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class InternetdomainsImpl extends AbstractLdapDao implements Internetdomains {
    private static Logger logger = LoggerFactory.getLogger(InternetdomainsImpl.class.getName())

    StructuralSchema schema

    protected InternetdomainsImpl() {
        super()
    }

    InternetdomainsImpl(LdapConfig configInstance) {
        super(configInstance)
        schema = new InternetdomainsSchema(configInstance)
    }

    Map find(String name, String client) {
        LDAP ldap = createConnection()
        def dn = createDn(name, client)
        if (ldap.exists(dn)) {
            return ldap.read(dn)
        }
        return [:]
    }

    List<Map> findAll(String client) {
        LDAP ldap = createConnection()
        ldap.search(schema.query, createBaseDn(client), SearchScope.SUB)
    }

    Map create(String name, String mailhost, String client) {
        Map domain = find(name, client)
        if (!domain) {
            def dn = createDn(name, client)
            def atts = [
                dc: name,
                mailhost: mailhost,
                maildomain: name
            ]
            LDAP ldap = createConnection()
            ldap.add(dn, schema.create(atts))
            domain = ldap.read(dn)
        }
        return domain
    }

    void delete(Map domain) {
        LDAP ldap = createConnection()
        if (domain?.dn && ldap.exists(domain.dn)) {
            try {
                ldap.delete(domain.dn)
            } catch (Throwable t) {
                logger.error("delete: failed [message=${t.message}]")
            }
        }
    }

    Map updateExpiration(Map domain, String expiration) {
        updateProperties(domain, [domainexpires : expiration ])
    }

    Map updateMailhosts(Map domain, String mailhost, String mailrelay) {
        updateProperties(domain, [mailhost: mailhost, mailrelay: mailrelay])
    }

    private Map updateProperties(Map domain, Map values) {
        LDAP ldap = createConnection()
        if (domain.dn) {
            ldap.modify(domain.dn, 'REPLACE', values)
        }
        (domain.dn ? ldap.read(domain.dn) : [:])
    }

    @Override
    protected String createDn(String name, String client) {
        //dc=domain.org,ou=Domains,dc=osserp,dc=org
        "dc=${name},ou=Domains,${createBaseDn(client)}"
    }
}
