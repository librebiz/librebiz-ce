/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01.10.2004 
 * 
 */
package com.osserp.core;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.mail.MailSender;
import com.osserp.common.web.ResourceUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CoreServiceProvider {

    private static Logger log = LoggerFactory.getLogger(CoreServiceProvider.class.getName());

    /**
     * Fetches a domain service
     * @param servletRequest
     * @param serviceName
     * @return service
     */
    public static Object getService(HttpServletRequest servletRequest, String serviceName) {
        return ResourceUtil.getService(servletRequest, serviceName);
    }

    /**
     * Provides the options cache
     * @return optionsCache
     */
    public static OptionsCache getOptionsCache(HttpServletRequest servletRequest) {
        return (OptionsCache) getService(servletRequest, OptionsCache.class.getName());
    }

    /**
     * Returns an option object by id if available
     * @see com.osserp.core.Options for available lists
     * @param optionKey
     * @param id
     * @return option or null if not exists
     */
    public static Option getOption(HttpServletRequest servletRequest, String optionKey, Long id) {
        return getOptionsCache(servletRequest).getMapped(optionKey, id);
    }

    /**
     * Returns the option list specified by given option key
     * @see com.osserp.core.Options for available lists
     * @param optionKey
     * @return list with options
     */
    public static List<Option> getOptionList(HttpServletRequest servletRequest, String optionKey) {
        return getOptionsCache(servletRequest).getList(optionKey);
    }

    /**
     * Returns the option map specified by given option key
     * @see com.osserp.core.Options for available lists
     * @param optionKey
     * @return map map with options
     */
    public static Map<Long, Option> getOptionMap(HttpServletRequest servletRequest, String optionKey) {
        return getOptionsCache(servletRequest).getMap(optionKey);
    }

    /**
     * Returns the option map specified by given option key
     * @see com.osserp.core.Options for available lists
     * @param optionKey
     * @return map with options
     */
    public static String getOptionValue(HttpServletRequest servletRequest, String optionKey, Long id) {
        Map<Long, Option> map = getOptionsCache(servletRequest).getMap(optionKey);
        if (map != null && id != null) {
            Option o = map.get(id);
            if (o != null) {
                return o.getName();
            }
        }
        return null;
    }

    /**
     * Toggles an options eol flag at the backend and refreshs the options cache
     * @param ctx where we refresh the list after toggling the option
     * @param optionKey of the option list
     * @param id option to toggle
     */
    public static void toggleOptionEol(HttpServletRequest servletRequest, String optionKey, Long id) {
        OptionsCache cache = getOptionsCache(servletRequest);
        cache.toogleEol(optionKey, id);
        cache.refresh(optionKey);
        servletRequest.getServletContext().setAttribute(optionKey, cache.getList(optionKey));
    }

    /**
     * Renames an option at the backend and refreshs the options cache
     * @param servletRequest
     * @param optionKey logical name of list
     * @param id option to rename
     * @param name to rename to
     * @throws ClientException if name is null or already existing under another id
     */
    public static void renameOption(
            HttpServletRequest servletRequest,
            String optionKey,
            Long id,
            String name)
            throws ClientException {

        OptionsCache cache = getOptionsCache(servletRequest);
        cache.rename(optionKey, id, name);
        cache.refresh(optionKey);
        servletRequest.getServletContext().setAttribute(optionKey, cache.getList(optionKey));
    }

    /**
     * Adds a new option to the backend list and refreshs the options cache
     * @param servletRequest
     * @param optionKey of the list where to add the name
     * @param name to add
     * @throws ClientException if name is null or already existing
     */
    public static void addOption(
            HttpServletRequest servletRequest,
            String optionKey,
            String name)
            throws ClientException {

        if (log.isDebugEnabled()) {
            log.debug("addOption() invoked for list " + optionKey
                    + ", new name " + name);
        }
        OptionsCache cache = getOptionsCache(servletRequest);
        cache.add(optionKey, name);
        if (log.isDebugEnabled()) {
            log.debug("addOption() new name added");
        }
        cache.refresh(optionKey);
        if (log.isDebugEnabled()) {
            log.debug("addOption() cache refreshed");
        }
        List<Option> updated = cache.getList(optionKey);
        if (log.isDebugEnabled()) {
            log.debug("addOption() retrieved refreshed list with "
                    + ((updated == null) ? "null" : updated.size())
                    + " members");
        }
        servletRequest.getServletContext().setAttribute(optionKey, updated);
        if (log.isDebugEnabled()) {
            log.debug("addOption() context refreshed");
        }
    }

    /**
     * Refreshs the option list specified by given option key
     * @see com.osserp.core.Options for available lists
     * @param servletRequest
     * @param optionKey
     */
    public static void refreshOptionList(HttpServletRequest servletRequest, String optionKey) {
        getOptionsCache(servletRequest).refresh(optionKey);
    }

    /**
     * Sends an email with default originator to specified recipient
     * @param servletRequest
     * @param recipient
     * @param subject
     * @param text
     */
    public static void sendMail(
            HttpServletRequest servletRequest,
            String recipient,
            String subject,
            String text) {

        try {
            MailSender sender = (MailSender) getService(servletRequest, MailSender.class.getName());
            sender.send(recipient, subject, text);
        } catch (Throwable e) {
            log.error("sendMail() failed [message=" + e.getMessage() + "]", e);
        }
    }

    /**
     * Sends an error email to default error recipient
     * @param servletRequest
     * @param text
     */
    public static void sendError(HttpServletRequest servletRequest, String text) {
        try {
            MailSender sender = (MailSender) getService(servletRequest, MailSender.class.getName());
            sender.sendError(text);
        } catch (Throwable e) {
            log.error("sendError() failed [message=" + e.getMessage() + "]", e);
        }
    }
}
