/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06-May-2005 17:46:09 
 * 
 */
package com.osserp.gui.output;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.osserp.common.ActionException;
import com.osserp.common.web.DocumentUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class XmlServlet extends HttpServlet {
    private static Logger log = LoggerFactory.getLogger(XmlServlet.class.getName());

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        doGet(request, response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        try {
            HttpSession session = request.getSession();
            Document doc = DocumentUtil.getXmlDocument(session);
            DocumentUtil.removeXmlDocument(session);
            renderXML(response, doc);

        } catch (ActionException ax) {
            try {
                PrintWriter out = response.getWriter();
                out.println("<html><head><title>Error</title></head>\n"
                        + "<body><h1>XmlServlet Error</h1><h3>" +
                        "<br>No document found in session context.<br><br>Multiple requests?</body></html>");
            } catch (Exception e) {
                log.error("doGet: caught exception [message=" + e.getMessage() + "]", e);
                throw new ServletException();
            }
        } catch (Exception ex) {
            log.error("doGet() failed: " + ex.toString(), ex);
            try {
                PrintWriter out = response.getWriter();
                out.println("<html><head><title>Error</title></head>\n"
                        + "<body><h1>XmlServlet Error</h1><h3>" +
                        "Please call your network administration.</body></html>");
            } catch (Exception e) {
                log.error("doGet: caught exception [message=" + e.getMessage() + "]", e);
                throw new ServletException();
            }
        }
    }

    /**
     * Renders an XML-Document directly to the response object's OutputStream
     * @param response
     * @param doc
     * @throws ServletException
     */
    public void renderXML(
            HttpServletResponse response,
            Document doc) throws ServletException {

        try {
            response.setContentType("text/xml");
            PrintWriter writer = response.getWriter();
            Format fmt = Format.getPrettyFormat();
            fmt.setEncoding("UTF-8");
            XMLOutputter outputter = new XMLOutputter(fmt);
            outputter.output(doc, writer);
        } catch (Exception ex) {
            log.error("renderXML: caught exception [message=" + ex.getMessage() + "]", ex);
            throw new ServletException(ex);
        }
    }
}
