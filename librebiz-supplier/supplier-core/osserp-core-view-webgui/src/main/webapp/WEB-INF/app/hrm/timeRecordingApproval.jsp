<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="timeRecordingApprovalView" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="timeRecordingApprovalContent">
            <div class="row">
            <c:choose>
                <c:when test="${!empty view.bean}">
                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="table-responsive">
                                    <table class="table table-head">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h4>
                                                        <fmt:message key="approvalFor" />
                                                        <oc:employee value="${view.bean.employeeId}" />
                                                    </h4>
                                                </td>
                                                <td class="action">
                                                    <c:if test="${!view.timeRecordingContext}">
                                                        <a href="<c:url value="/timeRecordings.do?method=forward&exit=timeRecordingApproval&employee=${view.bean.employeeId}"/>">
                                                            <o:img name="importantIcon" styleClass="bigicon" />
                                                        </a>
                                                    </c:if>
                                                 </td>
                                             </tr>
                                         </tbody>
                                     </table>
                                 </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <v:form name="approvalForm" url="/hrm/timeRecordingApproval/save">
                                <input type="hidden" name="approved" value="false" />
                                <div class="form-body">
                                    <c:choose>
                                        <c:when test="${view.bean.record != null}">
                                            <c:set var="recordType" value="${view.bean.record.type}" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="recordType" value="${view.bean.type}" />
                                        </c:otherwise>
                                    </c:choose>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <fmt:message key="type" />
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <c:choose>
                                                    <c:when test="${recordType.defaultType}">
                                                        <fmt:message key="comingAndGoingLabel" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <o:out value="${recordType.name}" />
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <fmt:message key="fromDate" />
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <span class="boldtext">
                                                    <o:date value="${view.bean.startDate}" addtime="true" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <fmt:message key="until" />
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <span class="boldtext"><o:date value="${view.bean.endDate}" addtime="true" /></span>
                                            </div>
                                        </div>
                                    </div>

                                    <c:if test="${!empty view.bean.note and empty view.bean.correction and view.bean.approvalRequired}">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <fmt:message key="statement" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <o:out value="${view.bean.note}" />
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>

                                    <c:if test="${!empty view.bean.correction}">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <fmt:message key="previousFromDate" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <o:date value="${view.bean.correction.value}" addtime="true" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <fmt:message key="until" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <o:date value="${view.bean.correction.stopValue}" addtime="true" />
                                                </div>
                                            </div>
                                        </div>

                                        <c:if test="${!empty view.bean.correction}">
                                            <c:if test="${!empty view.bean.record.note}">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <fmt:message key="statement" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <o:out value="${view.bean.record.note}" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </c:if>

                                    </c:if>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <fmt:message key="status" />
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <c:choose>
                                                    <c:when test="${view.bean.status == 0}">
                                                        <fmt:message key="open" />
                                                    </c:when>
                                                    <c:when test="${view.bean.status == 1}">
                                                        <fmt:message key="approved" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:message key="disapproved" />
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                    </div>

                                    <c:if test="${view.bean.status == 0 or !empty view.bean.note}">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <fmt:message key="comment" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <c:choose>
                                                        <c:when test="${view.bean.status == 0}">
                                                            <textarea class="fullwidth" name="note"></textarea>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <o:out value="${view.bean.note}" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>

                                    <c:if test="${view.bean.status == 0 and view.approvalPermissionGrant}">
                                        <div class="row next">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <c:choose>
                                                        <c:when test="${empty view.list}">
                                                            <input type="button" class="form-control cancel" onclick="gotoUrl('<v:url value="/hrm/timeRecordingApproval/exit"/>');" value="<fmt:message key="exit"/>" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <input type="button" class="form-control cancel" onclick="gotoUrl('<v:url value="/hrm/timeRecordingApproval/select"/>');" value="<fmt:message key="exit"/>" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <o:submit styleClass="form-control" value="disapprove" />
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <o:submit styleClass="form-control" value="approve" onclick="document.approvalForm.approved.value = 'true'; return true;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>

                                </div>
                            </v:form>

                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-12 panel-area">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><v:sortLink key="type.name"><fmt:message key="type" /></v:sortLink></th>
                                        <th><v:sortLink key="employeeId"><fmt:message key="employee" /></v:sortLink></th>
                                        <th><v:sortLink key="startDate"><fmt:message key="fromDate" /></v:sortLink></th>
                                        <th><v:sortLink key="endDate"><fmt:message key="until" /></v:sortLink></th>
                                        <th><v:sortLink key="status"><fmt:message key="status" /></v:sortLink></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="item" items="${view.list}" varStatus="s">
                                        <tr>
                                            <td><o:out value="${item.type.name}" /></td>
                                            <td>
                                                <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${item.employeeId}">
                                                    <oc:employee value="${item.employeeId}" />
                                                </v:ajaxLink>
                                            </td>
                                            <td><o:date value="${item.startDate}" addtime="true" /></td>
                                            <td><o:date value="${item.endDate}" addtime="true" /></td>
                                            <td>
                                                <a href="<v:url value="/hrm/timeRecordingApproval/select?id=${item.id}"/>">
                                                    <c:choose>
                                                        <c:when test="${item.status == 0}">
                                                            <fmt:message key="open" />
                                                        </c:when>
                                                        <c:when test="${item.status == 1}">
                                                            <fmt:message key="approved" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <fmt:message key="disapproved" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
