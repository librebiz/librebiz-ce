<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:if test="${!empty sessionScope.user}">
<v:redirect url="/index"/>
</c:if>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main_public.jsp" flush="false">
<tiles:put name="title"><o:out value="${applicationScope.webConfig.distributionLabel}"/> - Login</tiles:put>
<tiles:put name="styles" type="string">
<link rel="stylesheet" href="<c:url value="/layout/css/osserp-signin-0.1.css"/>" type="text/css"/>
</tiles:put>
<tiles:put name="content" type="string">

    <div class="page-header text-center">
    </div>

    <v:form name="loginForm" url="/login/authenticate" styleClass="form-signin">
        <p class="form-signin-heading text-center" style="color: darkgray;">
            For security reasons, please Log Out and Exit your web browser when
            you are done accessing services that require authentication!
        </p>
        <br />
        <c:if test="${!empty requestScope.errors}">
        <p class="form-signin-heading text-center" style="color: red;">
            <c:choose>
                <%-- Display detailed information about the cause on setup or missing resource only  --%>
                <c:when test="${requestScope.errors.indexOf('etup') > -1
                                or requestScope.errors.indexOf('esource') > -1}">
                    <c:out value="${requestScope.errors}"/>
                </c:when>
                <c:otherwise>
                    <span>Loginname / password invalid...</span>
                </c:otherwise>
            </c:choose>
        </p>
        <br />
        </c:if>
        <label for="inputEmail" class="sr-only">Loginname</label>
        <input type="text" id="loginName" name="loginName" class="form-control" placeholder="Loginname" required="required" autofocus="autofocus">
        <label for="inputPassword" class="sr-only">Passwort</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="required"/>
        <o:submit rawValue="Login" styleClass="btn btn-lg btn-primary btn-block" style="color: #A9A9A9; background-color: transparent; border-color: #CCC;"/>
    </v:form>

</tiles:put>
</tiles:insert>
