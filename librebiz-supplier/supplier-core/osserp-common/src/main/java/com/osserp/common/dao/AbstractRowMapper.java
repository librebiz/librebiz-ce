/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2006 10:46:50 AM 
 * 
 */
package com.osserp.common.dao;

import java.sql.ResultSet;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.RowMapper;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRowMapper extends AbstractDao implements RowMapper {
    private static Logger log = LoggerFactory.getLogger(AbstractRowMapper.class.getName());
    
    private OptionsCache optionsCache;
    
    private boolean columnLogging = false;
    
    protected void disableColumnLogging() {
        columnLogging = false;
    }
    
    protected void enableColumnLogging() {
        columnLogging = true;
    }

    public AbstractRowMapper() {
        super();
    }

    public AbstractRowMapper(OptionsCache optionsCache) {
        super();
        this.optionsCache = optionsCache;
    }

    /**
     * Adds a boolean value under given property name to a map while ignoring any throwable. A value of false will be added when column not exists, use with
     * care!
     * @param map
     * @param propertyName
     * @param rs
     * @param columnName
     */
    protected void addBoolean(Map<String, Object> map, String propertyName, ResultSet rs, String columnName) {
        map.put(propertyName, fetchBoolean(rs, columnName));
    }

    /**
     * Tries to fetch a boolean value of specified column name while ignoring any throwable.
     * @param rs
     * @param columnName
     * @return true or false depending on value or false if column not exists. Use with care!
     */
    protected boolean fetchBoolean(ResultSet rs, String columnName) {
        try {
            return rs.getBoolean(columnName);
        } catch (Throwable t) {
            if (columnLogging && log.isDebugEnabled()) {
                log.debug("fetchBoolean() reports exception on column [name=" + columnName 
                        + ", mapper=" + getClass().getName() 
                        + ", message=" + t.getMessage() 
                        + "]");
            }
            return false;
        }
    }

    /**
     * Tries to add date value under given property name to a map while ignoring any throwable. If value is null or column not exists, nothing will be added
     * @param map
     * @param propertyName
     * @param rs
     * @param columnName
     */
    protected void addDate(Map<String, Object> map, String propertyName, ResultSet rs, String columnName) {
        Date val = fetchDate(rs, columnName);
        if (val != null) {
            map.put(propertyName, val);
        }
    }

    /**
     * Tries to fetch a date value of specified column name while ignoring any throwable.
     * @param rs
     * @param columnName
     * @return date or null depending if column not exists. Use with care!
     */
    protected Date fetchDate(ResultSet rs, String columnName) {
        try {
            return rs.getDate(columnName);
        } catch (Throwable t) {
            if (columnLogging && log.isDebugEnabled()) {
                log.debug("fetchDate() reports exception on column [name=" + columnName 
                        + ", mapper=" + getClass().getName() 
                        + ", message=" + t.getMessage() 
                        + "]");
            }
            return null;
        }
    }

    /**
     * Adds a double value under given property name to a map while ignoring any throwable. If value is null or column not exists, 0 will be added as value.
     * @param map
     * @param propertyName
     * @param rs
     * @param columnName
     */
    protected void addDouble(Map<String, Object> map, String propertyName, ResultSet rs, String columnName) {
        map.put(propertyName, fetchDouble(rs, columnName));
    }

    /**
     * Tries to fetch a double value of specified column name while ignoring any throwable.
     * @param rs
     * @param columnName
     * @return value or 0 if column not exists
     */
    protected Double fetchDouble(ResultSet rs, String columnName) {
        try {
            double res = rs.getDouble(columnName);
            return res == 0 ? 0d : Double.valueOf(res);
        } catch (Throwable t) {
            if (columnLogging && log.isDebugEnabled()) {
                log.debug("fetchDouble() reports exception on column [name=" + columnName 
                    + ", mapper=" + getClass().getName() 
                    + ", message=" + t.getMessage() 
                    + "]");
            }
            return 0d;
        }
    }

    /**
     * Tries to add Integer value under given property name to a map while ignoring any throwable. If value is null or column not exists, nothing will be added
     * @param map
     * @param propertyName
     * @param rs
     * @param columnName
     */
    protected void addInteger(Map<String, Object> map, String propertyName, ResultSet rs, String columnName) {
        Integer val = fetchInteger(rs, columnName);
        if (val != null) {
            map.put(propertyName, val);
        }
    }

    /**
     * Tries to fetch an integer value of specified column name while ignoring any throwable.
     * @param rs
     * @param columnName
     * @return value or 0 if value is null or column not exists
     */
    protected int fetchInt(ResultSet rs, String columnName) {
        Integer result = fetchInteger(rs, columnName);
        return result == null ? 0 : result;
    }

    /**
     * Tries to fetch an integer value of specified column name while ignoring any throwable.
     * @param rs
     * @param columnName
     * @return value or null if value is null or column not exists
     */
    protected Integer fetchInteger(ResultSet rs, String columnName) {
        try {
            int res = rs.getInt(columnName);
            return res == 0 ? null : Integer.valueOf(res);
        } catch (Throwable t) {
            if (columnLogging && log.isDebugEnabled()) {
                log.debug("fetchInteger() reports exception on column [name=" + columnName 
                        + ", mapper=" + getClass().getName() 
                        + ", message=" + t.getMessage() 
                        + "]");
            }
            return null;
        }
    }

    /**
     * Tries to add Long value under given property name to a map while ignoring any throwable. If value is null or column not exists, nothing will be added
     * @param map
     * @param propertyName
     * @param rs
     * @param columnName
     */
    protected void addLong(Map<String, Object> map, String propertyName, ResultSet rs, String columnName) {
        Long val = fetchLong(rs, columnName);
        if (val != null) {
            map.put(propertyName, val);
        }
    }

    /**
     * Tries to fetch a long value of specified column name while ignoring any throwable.
     * @param rs
     * @param columnName
     * @return value or null if value is null or column not exists
     */
    protected Long fetchLong(ResultSet rs, String columnName) {
        try {
            long res = rs.getLong(columnName);
            return res == 0 ? null : Long.valueOf(res);
        } catch (Throwable t) {
            if (columnLogging && log.isDebugEnabled()) {
                log.debug("fetchLong() reports exception on column [name=" + columnName 
                        + ", mapper=" + getClass().getName() 
                        + ", message=" + t.getMessage() 
                        + "]");
            }
            return null;
        }
    }

    /**
     * Gets a long value of specified column name while ignoring any throwable and always returning a result.
     * @param rs
     * @param columnName
     * @return value or 0 if value is null or column not exists
     */
    protected Long getLong(ResultSet rs, String columnName) {
        Long result = fetchLong(rs, columnName);
        return result == null ? 0 : result;
    }

    /**
     * Tries to add String value under given property name to a map while ignoring any throwable. If value is null or column not exists, nothing will be added
     * @param map
     * @param propertyName
     * @param rs
     * @param columnName
     */
    protected void addString(Map<String, Object> map, String propertyName, ResultSet rs, String columnName) {
        String val = fetchString(rs, columnName);
        if (val != null) {
            map.put(propertyName, val);
        }
    }

    /**
     * Tries to fetch a string value of specified column name while ignoring any throwable.
     * @param rs
     * @param columnName
     * @return value or null if value is null or column not exists
     */
    protected String fetchString(ResultSet rs, String columnName) {
        try {
            return rs.getString(columnName);
        } catch (Throwable t) {
            if (columnLogging && log.isDebugEnabled()) {
                log.debug("fetchString() reports exception on column [name=" + columnName 
                    + ", mapper=" + getClass().getName() 
                    + ", message=" + t.getMessage() 
                    + "]");
            }
            return null;
        }
    }
    
    protected final Option fetchOption(String optionsName, Long optionId) {
        if (optionsCache != null && optionsName != null && optionId != null) {
            return optionsCache.getMapped(optionsName, optionId);
        }
        return null;
    }
    
    protected final String fetchOptionValue(String optionsName, Long optionId) {
        Option result = fetchOption(optionsName, optionId);
        return result == null ? null : result.getName();
    }

    protected OptionsCache getOptionsCache() {
        return optionsCache;
    }

    protected void setOptionsCache(OptionsCache optionsCache) {
        this.optionsCache = optionsCache;
    }
}
