/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 18, 2014 5:16:15 PM
 * 
 */
package com.osserp.core.suppliers;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SupplierConfig {

    /**
     * Indicates if supplier supports payments and incomings without related
     * purchase invoice or credit notes.
     * This billing method is mostly used for payments and receipts to or from
     * public institutions such as taxes, simple bills, etc.
     * @return true if supplier supports common recordless billing
     */
    boolean isSimpleBilling();
    
    /**
     * Enables/disables simple billing
     * @param simpleBilling
     */
    void setSimpleBilling(boolean simpleBilling);

    /**
     * Indicates if simple invoice booking is available. The result depends on
     * simpleInvoiceBookingEnabled and simpleInvoiceBookingProduct setting. 
     * @return true if simpleInvoiceBookingEnabled and product assigned.
     */
    boolean isSimpleInvoiceBookingAvailable();

    /**
     * Indicates if simple invoice booking is enabled. Simple invoice booking
     * is based on a purchaseOrder with a fixed product assignment.
     * @return true if simple invoice booking enabled
     */
    boolean isSimpleInvoiceBookingEnabled();
    
    /**
     * Enables/disables simple invoice booking
     * @param simpleInvoiceBookingEnabled
     */
    void setSimpleInvoiceBookingEnabled(boolean simpleInvoiceBookingEnabled);

    /**
     * Provides the fixed product assignment required for imple invoice booking.
     * @return simple invoice booking product id
     */
    Long getSimpleInvoiceBookingProduct();
    
    /**
     * Sets the product id required by simple invoice booking
     * @param simpleInvoiceBookingProduct
     */
    void setSimpleInvoiceBookingProduct(Long simpleInvoiceBookingProduct);

    /**
     * Indicates if direct invoice booking is enabled. Direct invoice booking
     * is based on a purchaseOrder with a dedicated product selection assigned.
     * @return true if direct invoice booking enabled
     */
    boolean isDirectInvoiceBookingEnabled();
    
    /**
     * Enables/disables direct invoice booking
     * @param directInvoiceBookingEnabled
     */
    void setDirectInvoiceBookingEnabled(boolean directInvoiceBookingEnabled);

    /**
     * Provides the product selection id for direct invoice booking
     * @return direct invoice booking product selection id
     */
    Long getDirectInvoiceBookingSelection();

    /**
     * Sets the product selection id for direct invoice booking
     * @param directInvoiceBookingSelection
     */
    void setDirectInvoiceBookingSelection(Long directInvoiceBookingSelection);

}
