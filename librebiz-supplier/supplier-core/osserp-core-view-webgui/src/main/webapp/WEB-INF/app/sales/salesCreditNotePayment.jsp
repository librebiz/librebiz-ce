<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesCreditNotePaymentView" />
<c:set var="record" value="${view.record}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="paymentRecordHeader" /> <o:out value="${record.contact.displayName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="salesCreditNotePaymentContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.paymentDeleteMode}">
                        <c:import url="_salesCreditNotePaymentDelete.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <v:form url="/sales/salesCreditNotePayment/save" name="salesCreditNotePaymentForm">
                            <c:import url="_salesCreditNotePaymentInput.jsp"/>
                        </v:form>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>