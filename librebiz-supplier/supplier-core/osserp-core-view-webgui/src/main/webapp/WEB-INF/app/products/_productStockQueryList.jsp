<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="productStockQueriesView" />
<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <v:queryResultList jdbcQuery="${view.bean}"/>
       </table>
   </div>
</div>
