/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 03, 2009 13:21:53 PM 
 * 
 */
package com.osserp.core.hrm;

import com.osserp.common.Option;

/**
 * 
 * @author jg <jg@osserp.com>
 * 
 */
public interface TimeRecordMarkerType extends Option {

    /**
     * Indicates that this marker type is expire month
     * @return expireMonth
     */
    boolean isExpireMonth();

    /**
     * Sets marker type as expire month
     * @param expireMonth
     */
    void setExpireMonth(boolean expireMonth);

    /**
     * Indicates that this marker type is expire day
     * @return expireDay
     */
    boolean isExpireDay();

    /**
     * Sets marker type as expire day
     * @param expireDay
     */
    void setExpireDay(boolean expireDay);

    /**
     * Indicates that this marker type is expire leave
     * @return expireLeave
     */
    boolean isExpireLeave();

    /**
     * Sets marker type as expire leave
     * @param expireLeave
     */
    void setExpireLeave(boolean expireLeave);

    /**
     * Provides the order id of the marker type
     * @return orderId
     */
    Integer getOrderId();

    /**
     * Sets the order id of the marker type
     * @param orderId
     */
    void setOrderId(Integer orderId);
}
