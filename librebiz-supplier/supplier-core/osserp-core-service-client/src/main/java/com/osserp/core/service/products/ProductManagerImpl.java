/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 03-Jan-2007 11:37:17 
 * 
 */
package com.osserp.core.service.products;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Details;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;

import com.osserp.core.dao.BusinessProperties;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.ResourceProvider;
import com.osserp.core.dms.CoreDocumentType;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.Manufacturer;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductDescription;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductManager;
import com.osserp.core.products.ProductPlanningMode;
import com.osserp.core.products.ProductType;
import com.osserp.core.products.ProductTypeConfig;
import com.osserp.core.products.SerialDisplay;
import com.osserp.core.service.impl.AbstractDetailsManager;
import com.osserp.core.tasks.ProductChangedSender;
import com.osserp.core.users.DomainUser;
import com.osserp.core.views.StockReportDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductManagerImpl extends AbstractDetailsManager implements ProductManager {
    private static Logger log = LoggerFactory.getLogger(ProductManagerImpl.class.getName());
    private static final String PRODUCT_PROPERTY_I18N_CONTEXT = "productProperty";
    private Products products = null;
    private ProductChangedSender productChangedSender = null;

    public ProductManagerImpl(
            Products products, 
            ResourceProvider resourceProvider,
            BusinessProperties businessProperties,
            ProductChangedSender productChangedSender) {
        super(resourceProvider, businessProperties, products);
        this.products = products;
        this.productChangedSender = productChangedSender;
    }

    @Override
    protected String getI18nContextName() {
        return PRODUCT_PROPERTY_I18N_CONTEXT;
    }

    public Product get(Long id) {
        try {
            return products.load(id);
        } catch (Throwable t) {
            log.warn("get() invoked for none existing product [id=" + id + "]");
            throw new BackendException(BackendException.INVALID_PARAM);
        }
    }

    public List<StockReportDisplay> getStockReport(Long stockId, Long productId) {
        if (log.isDebugEnabled()) {
            log.debug("getStockReport() invoked [stockId=" + stockId + ", productId=" + productId + "]");
        }
        return products.getStockReport(stockId, productId);
    }

    public List<Product> getStockAware(ProductCategory category) {
        return products.getStockAware(category, true);
    }

    public List<Product> getStockAware(ProductGroup group) {
        return products.getStockAware(group, true);
    }

    public List<Product> getStockAware(ProductType type) {
        return products.getStockAware(type, true);
    }

    public List<SerialDisplay> getSerialDisplay() {
        return products.getSerialDisplay();
    }

    public List<SerialDisplay> getSerialDisplay(Long productId) {
        return products.getSerialDisplay(productId);
    }

    public Product create(
            Employee user,
            Long productId,
            ProductClassificationConfig config,
            String matchcode,
            String name,
            String description,
            Long quantityUnit,
            Integer packagingUnit,
            Long manufacturer,
            ProductPlanningMode planningMode,
            Product source) throws ClientException {
        return products.create(
                user,
                productId,
                config,
                matchcode,
                name,
                description,
                quantityUnit,
                packagingUnit,
                manufacturer,
                planningMode,
                source);
    }

    public Long createIdSuggestion(ProductClassificationConfig config) {
        return products.createIdSuggestion(config);
    }

    public Product removeType(Product product, ProductType type) throws ClientException {
        if (product.getTypes().size() < 2) {
            throw new ClientException(ErrorCode.ALTERNATIVE_SOLUTION_REQUIRED);
        }
        Product result = products.removeType(product, type);
        sendChangedEvent(result);
        return result;
    }

    public void update(Product product) throws ClientException {
        products.checkUpdate(product.getProductId(), product.getName(), product.getMatchcode());
        /*
         * final product flag definitions required first if (product.isAffectsStock() && product.isKanban()) { throw new
         * ClientException(ErrorCode.KANBAN_NOT_STOCK_AFFECTING); }
         */
        if (isNotSet(product.getQuantityUnit())) {
            throw new ClientException(ErrorCode.QUANTITY_UNIT_MISSING);
        }
        if (isNotSet(product.getPackagingUnit())) {
            throw new ClientException(ErrorCode.PACKAGING_UNIT_MISSING);
        }
        if (isSet(product.getGtin13()) && product.getGtin13().length() > 13) {
            throw new ClientException(ErrorCode.PRODUCT_GTIN13_MAXLENGTH);
        }
        if (isSet(product.getGtin14()) && product.getGtin14().length() > 14) {
            throw new ClientException(ErrorCode.PRODUCT_GTIN14_MAXLENGTH);
        }
        if (isSet(product.getGtin8()) && product.getGtin8().length() > 8) {
            throw new ClientException(ErrorCode.PRODUCT_GTIN8_MAXLENGTH);
        }
        if (product.isEndOfLife()) {
            products.loadSummary(product);
            if (product.isAvailableOnAnyStock()) {
                throw new ClientException(ErrorCode.PRODUCT_HAS_STOCK);
            }
        }
        products.save(product);
        sendChangedEvent(product);
    }

    public Product updateClassification(
            Product product,
            ProductTypeConfig type,
            ProductGroupConfig group,
            ProductCategoryConfig category) throws ClientException {
        Product result = products.updateClassification(product, type, group, category);
        sendChangedEvent(result);
        return result;
        
    }

    public void updateDetails(Product product) throws ClientException {
        products.save(product);
        sendChangedEvent(product);
    }

    public void updateDocumentFlag(Product product, Long docType, boolean available) {
        // Hint: Do not sendChangeEvent in this method 'cause a dedicated media change
        // event is fired by media manager.
        if (docType.equals(CoreDocumentType.PRODUCT_PICTURE)) {
            product.setPictureAvailable(available);
            products.save(product);
        } else if (docType.equals(CoreDocumentType.PRODUCT_DATASHEET)) {
            product.setDatasheetAvailable(available);
            products.save(product);
        }
    }

    public void syncClient(Product product, Long docType, Long docId) {
        if (productChangedSender != null) {
            try {
                productChangedSender.sendMediaChanged(product.getProductId(), docType, docId);
            } catch (Exception e) {
                log.warn("sendMediaChangedEvent() failed [message=" + e.getMessage() + "]", e);
            }
        }
    }
    
    public void syncPublicAvailable() {
        if (productChangedSender != null) {
            try {
                productChangedSender.syncPublic();
            } catch (Exception e) {
                log.warn("syncPublicAvailable() failed [message=" + e.getMessage() + "]", e);
            }
        }
    }
    
    public void syncPublicAvailableMedia() {
        if (productChangedSender != null) {
            try {
                productChangedSender.syncPublicMedia();
            } catch (Exception e) {
                log.warn("syncPublicAvailableMedia() failed [message=" + e.getMessage() + "]", e);
            }
        }
    }

    public void updateDescription(Long user, Product product, String language, String name, String description, String datasheetText) throws ClientException {
        products.saveDescription(user, product, language, name, description, datasheetText);
        sendChangedEvent(product);
    }

    public List<ProductDescription> findDescriptions(Product product) {
        return products.findDescriptions(product.getProductId());
    }

    public void changePurchasePriceLimitFlag(DomainUser domainUser, Product product) throws PermissionException {
        products.changePurchasePriceLimitFlag(domainUser, product);
    }

    public void refreshSummary(Product product) {
        products.loadSummary(product);
    }

    public String getName(Long id) {
        return products.getName(id);
    }

    public Manufacturer findManufacturer(Product product) {
        return products.findManufacturer(product);
    }

    protected Products getProducts() {
        return products;
    }
    
    @Override
    protected void sendChangedEvent(Details details) {
        Product product = details instanceof Product ? (Product) details : null;
        if (productChangedSender != null && product != null) {
            try {
                productChangedSender.sendChanged(product.getProductId());
            } catch (Exception e) {
                log.warn("sendChangedEvent() failed [message=" + e.getMessage() + "]", e);
            }
        }
    }
}
