/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 28, 2015 
 * 
 */
package com.osserp.core.service.records;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Calendar;
import com.osserp.common.beans.CalendarImpl;
import com.osserp.common.util.CollectionUtil;
//import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
//import com.osserp.common.util.NumberFormatter;


import com.osserp.core.BankAccount;
import com.osserp.core.dao.records.CommonPayments;
import com.osserp.core.dao.records.PurchasePayments;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.finance.AccountingManager;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentDisplay;
import com.osserp.core.dao.records.RecordQueries;
import com.osserp.core.model.records.PaymentDisplayVO;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AccountingManagerImpl extends AbstractService implements AccountingManager {
    private static Logger log = LoggerFactory.getLogger(AccountingManagerImpl.class.getName());

    private RecordQueries recordQueries;
    private CommonPayments commonPayments;
    private PurchasePayments purchasePayments;
    private SalesPayments salesPayments;

    public AccountingManagerImpl(RecordQueries recordQueries, CommonPayments commonPayments, PurchasePayments purchasePayments, SalesPayments salesPayments) {
        super();
        this.recordQueries = recordQueries;
        this.commonPayments = commonPayments;
        this.purchasePayments = purchasePayments;
        this.salesPayments = salesPayments;
    }

    public List<PaymentDisplay> findOutstandingPayments(Long company) {
        LinkedList<PaymentDisplay> result = new LinkedList();
        List<PaymentDisplay> all = recordQueries.findPayments(company);
        for (int i = 0, j = all.size(); i < j; i++) {
            PaymentDisplay next = all.get(i);
            if (isNotSet(next.getGrossAmount())) {
                result.add(next);
            } 
        }
        return result;
    }

    public List<PaymentDisplay> findPayments(Long company) {
        LinkedList<PaymentDisplay> result = new LinkedList();
        List<PaymentDisplay> all = recordQueries.findPayments(company);
        for (int i = 0, j = all.size(); i < j; i++) {
            PaymentDisplay next = all.get(i);
            if (isSet(next.getGrossAmount())) {
                result.add(next);
            } 
        }
        return result;
    }
    
    public List<PaymentDisplay> findPayments(BankAccount bankAccount) {
        LinkedList<PaymentDisplay> result = new LinkedList();
        LinkedList<PaymentDisplay> selected = new LinkedList();
        if (bankAccount != null) {
            List<PaymentDisplay> all = recordQueries.findPayments(bankAccount.getReference());
            for (int i = 0, j = all.size(); i < j; i++) {
                PaymentDisplay next = all.get(i);
                if (bankAccount.getId().equals(next.getBankAccountId()) && isSet(next.getGrossAmount())) {
                    selected.add(next);
                } 
            }
            result = createAccountSummary(selected);
        }
        if (log.isDebugEnabled()) {
            log.debug("findPayments() done [count=" + selected.size() + "]");
        }
        return CollectionUtil.reverse(result);
    }
    
    protected LinkedList<PaymentDisplay> createAccountSummary(LinkedList<PaymentDisplay> paymentsByBankAccount) {
        LinkedList<PaymentDisplay> result = new LinkedList();
        double summary = 0;
        for (ListIterator<PaymentDisplay> i = paymentsByBankAccount.listIterator(paymentsByBankAccount.size()); i.hasPrevious();) {
            PaymentDisplay next = i.previous();
            if (!next.isAccountStatusOnly()) {
                if (next.isOutflowOfCash()) {
                    summary = summary - next.getGrossAmount();
                    //log.debug("createAccountSummary() next - [date=" 
                    //        + DateFormatter.getDate(next.getPaidOn())
                    //        + ", amount=" + NumberFormatter.getValue(next.getGrossAmount(), NumberFormatter.CURRENCY)
                    //        + ", summary=" + NumberFormatter.getValue(summary, NumberFormatter.CURRENCY)
                    //        + ", text=" + getText(next)
                    //        + "]");
                } else {
                    summary = summary + next.getGrossAmount();
                    //log.debug("createAccountSummary() next + [date=" 
                    //        + DateFormatter.getDate(next.getPaidOn())
                    //        + ", amount=" + NumberFormatter.getValue(next.getGrossAmount(), NumberFormatter.CURRENCY)
                    //        + ", summary=" + NumberFormatter.getValue(summary, NumberFormatter.CURRENCY)
                    //        + ", text=" + getText(next)
                    //        + ", dom" + DateUtil.getDaysOfMonthCount(next.getPaidOn())
                    //        + "]");
                }
                next.setAccountStatusSummary(summary);
            }

        }
        PaymentDisplay last = null;
        double lastSummary = 0;

        for (ListIterator<PaymentDisplay> i = paymentsByBankAccount.listIterator(paymentsByBankAccount.size()); i.hasPrevious();) {
            PaymentDisplay next = i.previous();
            if (!next.isAccountStatusOnly()) {

                if (next.isOutflowOfCash()) {
                    lastSummary = lastSummary - next.getGrossAmount();
                } else {
                    lastSummary = lastSummary + next.getGrossAmount();
                }

                if (last != null) {
                    int lastMonth = DateUtil.getMonth(last.getPaidOn());
                    int lastYear = DateUtil.getYear(last.getPaidOn());

                    int currentMonth = DateUtil.getMonth(next.getPaidOn());
                    int currentYear = DateUtil.getYear(next.getPaidOn());

                    if (currentYear == lastYear && lastMonth < currentMonth) {
                        int days = DateUtil.getDaysOfMonthCount(last.getPaidOn());
                        Calendar cal = new CalendarImpl(days, lastMonth, lastYear);
                        //log.debug("createAccountSummary() adding summary [last=" + DateFormatter.getDate(last.getPaidOn())
                        //        + ", date=" + DateFormatter.getDate(cal.getDate())
                        //        + ", summary=" + NumberFormatter.getValue(last.getAccountStatusSummary(), NumberFormatter.CURRENCY)
                        //        + "]");
                        result.add(new PaymentDisplayVO(cal.getDate(), last.getAccountStatusSummary()));

                    } else if (lastYear < currentYear) {
                        Calendar cal = new CalendarImpl(31, 11, lastYear);
                        //log.debug("createAccountSummary() adding summary [last=" + DateFormatter.getDate(last.getPaidOn())
                        //        + ", date=" + DateFormatter.getDate(cal.getDate())
                        //        + ", summary=" + NumberFormatter.getValue(last.getAccountStatusSummary(), NumberFormatter.CURRENCY)
                        //        + "]");
                        result.add(new PaymentDisplayVO(cal.getDate(), last.getAccountStatusSummary()));
                    }
                }
                result.add(next);
                last = next;
            }

        }

        if (last != null) {
            PaymentDisplay accountStatus = new PaymentDisplayVO(last.getPaidOn(), lastSummary);
            result.add(accountStatus);
        } else {
            PaymentDisplay accountStatus = new PaymentDisplayVO(new Date(System.currentTimeMillis()), lastSummary);
            result.add(accountStatus);
        }
        if (log.isDebugEnabled()) {
            log.debug("createAccountSummary() done [count=" + paymentsByBankAccount.size() + "]");
        }
        return result;
    }
    
    protected String getText(PaymentDisplay p) {
        return p.isCommonPayment() ? p.getNote() : 
            (p.isPurchaseInvoicePayment() ? "PURCHASE" : "SALES");
    }

    public void increasePaymentTime(String context, Long id) {
        try {
            if ("salesPayment".equals(context)) {
                Payment payment = salesPayments.getPayment(id); 
                salesPayments.increaseTime(payment);
            } else if ("purchasePayment".equals(context)) {
                Payment payment = purchasePayments.getPayment(id); 
                purchasePayments.increaseTime(payment);
            } else if ("commonPayment".equals(context)) {
                Payment payment = commonPayments.getPayment(id); 
                commonPayments.increaseTime(payment);
            } else {
                log.warn("increasePaymentTime() invalid context provided [name=" 
                        + context + ", id=" + id + "]");
            }
        } catch (Exception e) {
            log.warn("increasePaymentTime() failed [context=" 
                    + context + ", id=" + id + ", message=" 
                    + e.getMessage() + "]", e);
        }
    }
}
