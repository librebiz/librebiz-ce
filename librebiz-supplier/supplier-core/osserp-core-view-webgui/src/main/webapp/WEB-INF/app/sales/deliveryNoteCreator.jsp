<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="deliveryNoteCreatorView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="newDeliveryNote" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="deliveryNoteCreatorContent">
            <div class="row">
                <v:form url="${view.baseLink}/save" name="deliveryNoteCreatorForm">

                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <o:out value="${view.salesOrder.number}" />
                                    <o:out value="${view.salesOrder.contact.displayName}" />
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">

                            <c:choose>
                                <c:when test="${empty view.selectedBookingType}">
                                    <div class="table-responsive table-responsive-default">
                                        <table class="table">
                                            <tbody>
                                                <c:forEach var="bt" items="${view.bookingTypes}">
                                                    <tr>
                                                        <td>
                                                            <v:link url="${view.baseLink}/selectBookingType=${bt.id}">
                                                                <fmt:message key="${bt.name}" />
                                                            </v:link>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="bookingType">
                                                    <fmt:message key="record" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <o:out value="${view.selectedBookingType.name}" />
                                            </div>
                                        </div>
                                    </div>

                                    <v:date name="recordDate" label="recordDate" picker="true" styleClass="form-control" />

                                    <div class="row next">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <v:submitExit url="${view.baseLink}/exit" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <o:submit styleClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:otherwise>
                            </c:choose>

                        </div>
                    </div>

                </v:form>
            </div>
        </div>

    </tiles:put>
</tiles:insert>
