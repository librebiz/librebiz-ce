<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesDeliveryNoteView}">
	<o:logger write="delivery_note_display.jsp no view found" level="warn"/>
	<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesDeliveryNoteView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="records" value="${recordView.list}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>

<tiles:put name="headline">
	<c:choose>
		<c:when test="${!empty sessionScope.businessCaseView}">
			<fmt:message key="deliveryNotes"/> <o:out value="${sessionScope.businessCaseView.request.name}"/>
		</c:when>
		<c:otherwise>
            <fmt:message key="deliveryNotesWithoutRelease"/>
		</c:otherwise>
	</c:choose>
</tiles:put>
<tiles:put name="headline_right">
	<a href="<c:url value="/salesDeliveryNote.do?method=exitList"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
	<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>
	
<tiles:put name="content" type="string">
<div class="content-area" id="deliveryNoteListContent">
    <div class="row">
        <div class="col-lg-12 panel-area">
            <c:if test="${!empty sessionScope.message}">
                <div class="recordheader" style="margin-left: 20px;">
                    <p class="boldtext">
                        <o:out value="${sessionScope.message}"/>
                        <c:remove var="message" scope="session"/>
                    </p>
                </div>
            </c:if>
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="recordNumber"><fmt:message key="deliveryNumber"/></th>
                            <th class="recordDate"><fmt:message key="created"/></th>
                            <th class="recordCreator"><fmt:message key="by"/></th>
                            <th class="recordNumber"><fmt:message key="reference"/></th>
                            <th class="recordStatus"><fmt:message key="status"/></th>
                            <th class="action"><fmt:message key="action"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="record" items="${records}" varStatus="s">                   
                            <tr>
                                <td class="recordNumber"><a href="<c:url value="/salesDeliveryNote.do?method=select&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                <td class="recordDate"><o:date value="${record.created}"/></td>
                                <td class="recordCreator">
                                    <o:ajaxLink linkId="createdBy" url="${'/employeeInfo.do?id='}${record.createdBy}">
                                        <oc:employee value="${record.createdBy}"/>
                                    </o:ajaxLink>
                                </td>
                                <td class="recordNumber"><o:out value="${record.reference}"/></td>
                                <td class="recordStatus"><oc:options name="recordStatus" value="${record.status}"/></td>
                                <td class="action">
                                    <c:choose>
                                        <c:when test="${record.unchangeable}">
                                            <v:link url="/records/recordPrint/print?name=salesDeliveryNote&id=${record.id}" title="documentPrint"><o:img name="printGreenIcon" /></v:link>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="/records/recordPrint/print?name=salesDeliveryNote&id=${record.id}" title="documentPrint"><o:img name="printRedIcon" /></v:link>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
