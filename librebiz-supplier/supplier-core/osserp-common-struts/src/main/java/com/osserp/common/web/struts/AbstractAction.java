/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06.08.2004 
 * 
 */
package com.osserp.common.web.struts;

import java.util.ResourceBundle;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.extras.actions.DispatchAction;

import com.osserp.common.TimeoutException;
import com.osserp.common.User;
import com.osserp.common.web.Form;
import com.osserp.common.web.Globals;
import com.osserp.common.web.RequestForm;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.SessionUtil;

/**
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractAction extends DispatchAction {
    
    private static Logger log = LoggerFactory.getLogger(AbstractAction.class.getName());
    
    /** Represents an undefined long value selection; value = 0 */
    protected static final Long UNDEFINED = 0L;

    /**
     * Gets the current logged in user
     * @param request
     * @return user
     * @throws TimeoutException if no user logged in
     */
    protected User getUser(HttpServletRequest request) throws TimeoutException {
        return getUser(request.getSession(true));
    }

    /**
     * Gets the current logged in user
     * @param session
     * @return user
     * @throws TimeoutException if no user logged in
     */
    protected User getUser(HttpSession session) throws TimeoutException {
        User u = null;
        if (session != null) {
            u = (User) session.getAttribute(Globals.USER);
        }
        if (u != null) {
            return u;
        }
        throw new TimeoutException();
    }

    /**
     * Checks if given string wether null nor empty
     * @param s
     * @return true if so
     */
    protected boolean isSet(String s) {
        return (s != null && s.length() > 0);
    }

    /**
     * Checks if given integer wether null nor 0
     * @param i integer
     * @return true if so
     */
    protected boolean isSet(Integer i) {
        return (i != null && i.intValue() > 0);
    }

    /**
     * Tries to get an object specified as Long in form definition
     * @param form where we try to get the value
     * @param key / name of the form field
     * @return value or null if not found
     */
    protected Long getLong(DynaActionForm form, String key) {
        try {
            return (Long) form.get(key);
        } catch (Exception ignore) {
            log.error("getLong(" + key + ") ignoring of exception:\n"
                    + ignore.toString());
            return null;
        }
    }

    // COMMON HELPER METHODS
    
    /**
     * Indicates if a previous action or method invocation set an Globals.ERRORS key 
     * with an error message (indicates that something was wrong on this) 
     * @param request
     * @return true if Globals.ERRORS key exist
     */
    protected final boolean isErrorAvailable(HttpServletRequest request) {
        return SessionUtil.isErrorAvailable(request.getSession());
    }

    /**
     * Saves the given error string under Globals.ERRORS key as ActionError
     * @param request
     * @param errorKey key
     */
    protected void saveError(HttpServletRequest request, String errorKey) {
        SessionUtil.saveError(
                request.getSession(),
                RequestUtil.getResourceString(request, errorKey));
    }

    /**
     * Saves the given error string under Globals.ERRORS key as ActionError
     * @param request
     * @param errorMessage
     */
    protected void saveNativeError(HttpServletRequest request, String errorMessage) {
        RequestUtil.saveNativeError(request, errorMessage);
    }

    /**
     * Saves given error details text under 'errorDetails' key in session scope
     * @param errorDetails
     */
    protected void saveErrorDetails(HttpSession session, String errorDetails) {
        SessionUtil.saveErrorDetails(session, errorDetails);
    }

    /**
     * Resolves a message via message key and stores text in session scope under key 'message'.
     * @param request for session lookup
     * @param messageKey of the message text to store
     */
    protected void saveMessage(HttpServletRequest request, String messageKey) {
        SessionUtil.saveMessage(request, messageKey);
    }

    /**
     * Flushes forward target as 'redirect:/contextPath/forwardPath' to output stream
     * @param request
     * @param response
     * @param forward
     * @throws Exception
     */
    protected final void redirectAjax(HttpServletRequest request, HttpServletResponse response, ActionForward forward) throws Exception {
        String url = "redirect:" + request.getContextPath() + forward.getPath();
        ServletOutputStream outStream = response.getOutputStream();
        outStream.write(url.getBytes());
        outStream.flush();
    }

    /**
     * Provides the resource string from resource bundle as provided by {@link #getResourceBundle()}. Locale depends on current user or application default if
     * no user logged in during view activity.
     * @param key
     * @return resource or empty string depending on whether a string could be found or not.
     */
    protected String getResourceString(HttpServletRequest request, String key) {
        ResourceBundle bundle = getResourceBundle(request);
        String result = null;
        if (bundle != null) {
            try {
                result = bundle.getString(key);
            } catch (Throwable t) {
            }
        }
        return result == null ? key : result;
    }

    /**
     * Provides the resource bundle for current users locale or default locale of the app if view does not depend on a user. You may use
     * {@link #getResourceString(String)} to pick up just a particular string
     * @param request
     * @return resource bundle or null if none exists
     */
    private ResourceBundle getResourceBundle(HttpServletRequest request) {
        return RequestUtil.getResourceBundle(request);
    }

    /**
     * Creates form by form or request if none provided
     * @param form
     * @param request
     * @return form
     */
    protected final Form createForm(ActionForm form, HttpServletRequest request) {
        if (form == null) {
            if (log.isDebugEnabled()) {
                log.debug("createForm() no struts form found, using request...");
            }
            return new RequestForm(request);
        }
        if (log.isDebugEnabled()) {
            log.debug("createForm() struts form found, using...");
        }
        return new StrutsForm(form);
    }

}
