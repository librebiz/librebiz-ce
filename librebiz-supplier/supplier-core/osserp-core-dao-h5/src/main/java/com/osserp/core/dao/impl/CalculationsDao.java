/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 10, 2006 11:55:38 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.CalculationSearchResult;
import com.osserp.core.dao.CalculationConfigs;
import com.osserp.core.dao.Calculations;
import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.ProductQueries;
import com.osserp.core.dao.ProductSelections;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.calc.CalculationImpl;
import com.osserp.core.model.calc.CalculationPositionImpl;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationsDao extends AbstractItemLists implements Calculations {
    private static Logger log = LoggerFactory.getLogger(CalculationsDao.class.getName());

    private ProductPrices productPrices = null;
    private ProductQueries productQueries = null;
    private ProductSelections productSelections = null;
    private CalculationConfigs configs = null;
    private String defaultName = null;

    public CalculationsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            ProductPrices productPrices,
            ProductSelections productSelections,
            ProductQueries productQueries,
            CalculationConfigs configs,
            String defaultName) {

        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                TableKeys.CALCULATIONS,
                TableKeys.CALCULATION_POSITIONS,
                TableKeys.CALCULATION_ITEMS);
        this.productPrices = productPrices;
        this.productQueries = productQueries;
        this.productSelections = productSelections;
        this.configs = configs;
        this.defaultName = defaultName;
    }

    @Override
    protected Class<CalculationImpl> getItemListClass() {
        return CalculationImpl.class;
    }

    public boolean isMatching(BusinessCase businessCase, ItemPosition position, Product product) {
        CalculationConfigGroup group = configs.findGroup(position.getGroupId());
        List<ProductSelectionConfig> productSelectionConfigs = group.getProductSelections();
        if ((!productSelectionConfigs.isEmpty()) && (productQueries != null)) {
            return productQueries.isMatching(productSelectionConfigs, product.getProductId());
        }
        log.warn("isMatching() missing businessCase.type.calculationConfig or calculation.position.groupId [businessCase=" + businessCase.getPrimaryKey()
                + ", businessType=" + businessCase.getType().getId() + ", calculation=" + position.getReference() + ", position=" + position.getId() + "]");

        return false;
    }

    private BigDecimal createSalesPriceByBusinessType(BusinessType bt, Product product) {
        if (bt == null || product == null) {
            return new BigDecimal(0);
        }
        if (bt.isWholeSale()) {
            return new BigDecimal(product.getResellerPrice() == null ? 0d : product.getResellerPrice());
        }
        return new BigDecimal(product.getConsumerPrice() == null ? 0d : product.getConsumerPrice());
    }

    public CalculationConfigGroup fetchGroup(CalculationConfig config, Product product) {
        List<CalculationConfigGroup> grps = config.getGroups();
        if (log.isDebugEnabled()) {
            log.debug("fetchGroup() invoked [config=" + config.getId()
                    + ", groups=" + grps.size() 
                    + ", product=" + product.getProductId()
                    + "]");
        }
        CalculationConfigGroup group = null;
        for (int i = 0, j = grps.size(); i < j; i++) {
            if (group != null) {
                break;
            }
            CalculationConfigGroup next = grps.get(i);
            List<ProductSelectionConfig> selectionConfigs = next.getProductSelections();
            if (log.isDebugEnabled()) {
                log.debug("fetchGroup() examining calculationConfigGroup [id=" + next.getId()
                        + ", selectionConfigs=" + selectionConfigs.size() + "]");
            }
            for (int k = 0, l = selectionConfigs.size(); k < l; k++) {
                ProductSelectionConfig cfg = selectionConfigs.get(k);
                boolean matching = cfg.isMatching(product);
                if (log.isDebugEnabled()) {
                    log.debug("fetchGroup() examining productSelectionConfig [id=" + cfg.getId()
                            + ", product=" + product.getProductId() 
                            + ", matching=" + matching + "]");
                }
                if (matching) {
                    group = next;
                    break;
                }
            }
        }
        return group;
    }

    public void createOptions(Calculation calculation) {
        if (calculation.getOptions().isEmpty() && calculation instanceof CalculationImpl) {
            CalculationImpl target = (CalculationImpl) calculation;
            for (int i = 0, j = target.getPositions().size(); i < j; i++) {
                CalculationPositionImpl next = (CalculationPositionImpl) target.getPositions().get(i);
                target.addOptionPosition(next.getName(), next.getGroupId());
            }
            save(target);
        }
    }

    public Calculation create(
            Employee user,
            Long referenceId,
            String contextName,
            Date referenceCreated,
            BusinessType requestType,
            String name,
            String calculatorName,
            boolean salesPriceLocked,
            Double minimalMargin,
            Double targetMargin)
            throws ClientException {

        if (log.isDebugEnabled()) {
            log.debug("create() invoked [reference=" + referenceId + ", type=" + requestType.getId() + "]");
        }
        if (referenceId == null || requestType == null || requestType.getCalculationConfig() == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        CalculationConfig config = configs.load(requestType.getCalculationConfig());
        CalculationImpl obj = new CalculationImpl(
                calculatorName,
                referenceId,
                contextName,
                requestType.isWholeSale(),
                isEmpty(name) ? defaultName : name,
                getCount(referenceId) + 1,
                (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()),
                referenceCreated,
                isNotSet(minimalMargin) ? requestType.getMinimalMargin() : minimalMargin,
                isNotSet(targetMargin) ? requestType.getTargetMargin() : targetMargin);
        obj.setSalesPriceLocked(salesPriceLocked);
        if (config != null) {
            for (Iterator<CalculationConfigGroup> i = config.getGroups().iterator(); i.hasNext();) {
                CalculationConfigGroup grp = i.next();
                obj.addPosition(
                        grp.getName(),
                        grp.getId(),
                        grp.isDiscounts(),
                        false,
                        grp.isPartList(),
                        grp.getPartListId());
                obj.addOptionPosition(grp.getName(), grp.getId());
            }
        } else {
            log.warn("No calculation config provided [requestType=" + requestType.getId() +
                    ", calculatorName=" + requestType.getCalculatorName() + "]");
        }
        getCurrentSession().save(obj);
        return obj;
    }

    public Calculation copy(BusinessType businessType, Calculation targetObj, Calculation source) {
        CalculationImpl target = getCurrentSession().load(CalculationImpl.class, targetObj.getId());
        if (log.isDebugEnabled()) {
            log.debug("copy: invoked [source=" + source.getId() + ", target=" + target.getId() + "]");
        }
        target.setSalesPriceLocked(source.isSalesPriceLocked());
        List<ItemPosition> existingPositions = source.getPositions();
        int copied = 0;
        int ignored = 0;
        List<ProductSelectionConfigItem> partnerPriceEditables = productSelections.findByEditablePartnerPrice();
        for (int i = 0, j = existingPositions.size(); i < j; i++) {
            ItemPosition existingPos = existingPositions.get(i);
            ItemPosition newPosition = target.getPositionByGroup(existingPos.getGroupId());
            if (newPosition != null) {
                // old position exists also in new calculation
                List<Item> items = existingPos.getItems();
                for (int k = 0, l = items.size(); k < l; k++) {
                    Item item = existingPos.getItems().get(k);
                    BigDecimal price = item.getPrice() == null ? new BigDecimal(0) : item.getPrice();
                    if (price.doubleValue() == 0 && !item.getProduct().isPlant()) {
                        price = createSalesPriceByBusinessType(businessType, item.getProduct());
                    }
                    try {
                        BigDecimal partnerPrice = item.getPartnerPrice();
                        if (!item.isPartnerPriceOverridden()) {
                            partnerPrice = new BigDecimal(productPrices.getPartnerPrice(item.getProduct(), target.getInitialCreated(), item.getQuantity()));
                        }
                        target.addItem(
                                newPosition.getId(),
                                item.getProduct(),
                                item.getCustomName(),
                                item.getQuantity(),
                                price,
                                target.getInitialCreated(),
                                item.isPriceOverridden(),
                                (partnerPrice == null ? new BigDecimal(0) : partnerPrice),
                                ProductUtil.isPartnerPriceEditable(partnerPriceEditables, item.getProduct()),
                                item.isPartnerPriceOverridden(),
                                new BigDecimal(productPrices.getPurchasePrice(item.getProduct(), item.getProduct().getDefaultStock(),
                                        target.getInitialCreated())),
                                item.getTaxRate(),
                                item.getNote(),
                                item.isIncludePrice(),
                                item.getExternalId());

                        copied++;
                    } catch (ClientException c) {
                        if (log.isInfoEnabled()) {
                            log.info("copy: ignoring failed attempt to copy an item ["
                                    + "id=" + item.getId()
                                    + ", position=" + existingPos.getId()
                                    + ", calculation=" + source.getId()
                                    + ", productId=" + (item.getProduct() == null ? "null" : item.getProduct().getProductId())
                                    + "]");
                        }
                        ignored++;
                    }
                }
            }
        }
        copied = 0;
        ignored = 0;
        List<ItemPosition> existingOptions = source.getOptions();
        if (!existingOptions.isEmpty()) {
            for (int i = 0, j = existingOptions.size(); i < j; i++) {
                ItemPosition pos = existingOptions.get(i);
                ItemPosition newPosition = target.getOptionPositionByGroup(pos.getGroupId());
                if (newPosition != null) {
                    // old position exists also in new calculation
                    List<Item> items = pos.getItems();
                    for (int k = 0, l = items.size(); k < l; k++) {
                        Item item = pos.getItems().get(k);
                        try {
                            target.addOptionItem(
                                    newPosition.getId(),
                                    item.getProduct(),
                                    item.getCustomName(),
                                    item.getQuantity(),
                                    item.getPrice(),
                                    target.getInitialCreated(),
                                    new BigDecimal(productPrices.getPartnerPrice(
                                            item.getProduct(),
                                            target.getInitialCreated(),
                                            item.getQuantity())),
                                    false,
                                    new BigDecimal(productPrices.getPurchasePrice(
                                            item.getProduct(),
                                            item.getProduct().getDefaultStock(),
                                            target.getInitialCreated())),
                                    item.getTaxRate(),
                                    item.getNote(),
                                    item.getExternalId());
                            copied++;
                        } catch (ClientException e) {
                            if (log.isInfoEnabled()) {
                                log.info("copy: ignoring failed attempt to copy an optional item ["
                                        + "id=" + item.getId()
                                        + ", position=" + pos.getId()
                                        + ", calculation=" + source.getId()
                                        + ", productId=" + (item.getProduct() == null ? "null" : item.getProduct().getProductId())
                                        + "]");
                            }
                            ignored++;
                        }
                    }
                }
            }
        }
        getCurrentSession().merge(target);
        setOldHistorical(target.getId(), target.getReference());
        return target;
    }

    public List<CalculationSearchResult> findByReference(Long reference, String context) {
        StringBuilder sql = new StringBuilder(CalculationSearchResultRowMapper.SELECT_FROM);
        sql.append(getItemListsTable()).append(" WHERE reference_id = ? AND context = ? ORDER BY id desc");
        return (List<CalculationSearchResult>) jdbcTemplate.query(
                sql.toString(), 
                new Object[] { reference, context }, 
                new int[] { Types.BIGINT, Types.VARCHAR },
                CalculationSearchResultRowMapper.getReader());
    }

}
