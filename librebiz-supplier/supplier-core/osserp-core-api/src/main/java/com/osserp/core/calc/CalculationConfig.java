/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 26, 2005 
 * 
 */
package com.osserp.core.calc;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CalculationConfig extends Option {

    /**
     * The type of the calculation config
     * @return calculationType
     */
    CalculationType getType();

    /**
     * Provides a group by id
     * @param id
     * @return group
     */
    CalculationConfigGroup getGroup(Long id);

    /**
     * Returns the groups
     * @return groups.
     */
    List<CalculationConfigGroup> getGroups();

    /**
     * Updates a group
     * @param groupId
     * @param name
     * @param discounts
     * @param option
     * @return group
     * @throws ClientException if name already exists under another group
     */
    CalculationConfigGroup updateGroup(Long groupId, String name, boolean discounts, boolean option) throws ClientException;

    /**
     * Adds a new product selection config
     * @param groupId
     * @param selectionConfig
     */
    void addProductSelection(Long groupId, ProductSelectionConfig selectionConfig);

    /**
     * Removes an product selection config
     * @param groupId
     * @param configId
     */
    void removeProductSelection(Long groupId, Long configId);

    /**
     * By default it's not allowed to add an product n-times in the same calculation. Members returned by this list are exluded from the check.
     * @return allowedRedundancies
     */
    List<Product> getAllowedRedundancies();

    /**
     * Removes a group
     * @param id
     */
    void removeGroup(Long id);
}
