/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 10, 2017 
 * 
 */
package com.osserp.gui.requests

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.util.CollectionUtil

import com.osserp.core.BusinessType
import com.osserp.core.Options
import com.osserp.core.crm.CampaignManager
import com.osserp.core.requests.RequestListItem
import com.osserp.core.requests.RequestListItemComparators
import com.osserp.core.requests.RequestListManager
import com.osserp.core.users.DomainUser

import com.osserp.gui.QueryIndexView

/**
 *
 * @author <a href="mailto:rk@osserp.com">rk</a>
 *
 */
abstract class AbstractRequestQueryView extends QueryIndexView {
    private static Logger logger = LoggerFactory.getLogger(AbstractRequestQueryView.class.getName())
    
    Long selectedBranchId
    Long selectedBusinessType
    Long selectedOrigin
    DomainUser selectedUser
    boolean canceled = false

    protected void findByBranch(Long branchId) throws ClientException {
        selectedBranchId = branchId
        selectedBranch = getOption(Options.BRANCH_OFFICES, selectedBranchId)
        List<RequestListItem> items = requestSearch.getCurrentRequests()
        List tmpList = []
        items.each { RequestListItem next ->
            if (next.getBranch() != null
                    && next.getBranch().getId().equals(selectedBranch.getId())
                    && domainEmployee.isSupportingBranch(next.getBranch())) {
                tmpList << next
            }
        }
        list = sortByCreatedReverse(tmpList)
        if (!list) {
            selectedBranch = null
            selectedBranchId = null
            throw new ClientException(ErrorCode.SEARCH)
        }
    }
    
    protected List<RequestListItem> findByDate() throws ClientException {
        List<RequestListItem> items = requestSearch.getLatestRequests(requestListFetchSize)
        List tmpList = []
        items.each { RequestListItem next ->
            if (domainEmployee.isSupportingBranch(next.getBranch())) {
                tmpList << next
            }
        }
        return sortByCreatedReverse(tmpList)
    }

    protected void findByRequestType(Long businessTypeId) throws ClientException {
        selectedBusinessType = businessTypeId
        List<RequestListItem> items = requestSearch.getCurrentRequests()
        List tmpList = []
        items.each { RequestListItem next ->
            if (next.type?.id == selectedBusinessType
                    && domainEmployee.isSupportingBranch(next.getBranch())) {
                tmpList << next
            }
        }
        list = sortByCreatedReverse(tmpList)
        if (!list) {
            selectedBusinessType = null
            throw new ClientException(ErrorCode.SEARCH)
        }
    }

    protected void findByOrigin(Long originId) throws ClientException {
        selectedOrigin = originId
        List<RequestListItem> items = requestSearch.getCurrentRequests()
        List tmpList = []
        items.each { RequestListItem next ->
            if (next.getOriginId() != null && next.getOriginId().equals(selectedOrigin)
                    && domainEmployee.isSupportingBranch(next.getBranch())) {
                tmpList << next
            }
        }
        list = sortByCreatedReverse(tmpList)
        if (!list) {
            selectedOrigin = null
            throw new ClientException(ErrorCode.SEARCH)
        }
    }

    protected void findBySales(Long salesId) throws ClientException {
        selectUserByIdOrInternal(salesId)
        List<RequestListItem> items = requestSearch.getCurrentRequests()
        List tmpList = []
        items.each { RequestListItem next ->
            if (next.getSalesId() != null && next.getSalesId().equals(salesId)
                    && domainEmployee.isSupportingBranch(next.getBranch())) {
                tmpList << next
            }
        }
        list = sortByCreatedReverse(tmpList)
        if (!list) {
            selectedUser = null
            throw new ClientException(ErrorCode.SEARCH)
        }
    }

    protected void findByManager(Long managerId) throws ClientException {
        selectUserByIdOrInternal(managerId)
        List<RequestListItem> items = requestSearch.getCurrentRequests()
        List tmpList = []
        items.each { RequestListItem next ->
            if (next.getManagerId() != null && next.getManagerId().equals(managerId)
                    && domainEmployee.isSupportingBranch(next.getBranch())) {
                tmpList << next
            }
        }
        list = sortByCreatedReverse(tmpList)
        if (!list) {
            selectedUser = null
            throw new ClientException(ErrorCode.SEARCH)
        }
    }
    
    protected final void selectUserByIdOrInternal(Long id) {
        if (id == null) {
            selectedUser = domainUser
        } else {
            selectedUser = getDomainUser(id)
        }
    }

    protected List<RequestListItem> sortByCreatedReverse(List<RequestListItem> items) {
        return CollectionUtil.sort(items,
            RequestListItemComparators.createRequestListItemByCreatedComparator(true));
    }

    protected int getRequestListFetchSize() {
        systemPropertyAsInt('recordFetchSize') ?: 250
    }
    
    protected final List<BusinessType> getRequestTypeSelection() {
        requestSearch.getRequestTypeSelection(canceled)
    }

    protected CampaignManager getCampaignManager() {
        getService(CampaignManager.class.getName())
    }

    protected RequestListManager getRequestSearch() {
        getService(RequestListManager.class.getName())
    }

}
