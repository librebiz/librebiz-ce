/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * 
 */
package com.osserp.gui.admin.products

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.Options
import com.osserp.core.products.ProductClass
import com.osserp.core.products.ProductGroupManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ProductGroupEditorView extends CoreView {

    List<String> groupClassifications = []


    ProductGroupEditorView() {
        headerName = 'productGroupEdit'
        enablePopupView()
        enablePopupSaveReloadParent()
        dependencies = ['classificationConfigsView']
        groupClassifications.add('A')
        groupClassifications.add('B')
        groupClassifications.add('C')
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!env.classificationConfigsView) {
            throw new ViewContextException()
        }
        bean = env.classificationConfigsView.selectedGroup
        if (!bean) {
            throw new ClientException(ErrorCode.TYPE_MISSING)
        }
    }

    @Override
    void save() {
        def grp = productGroupManager.load(bean.id)
        def name = form.getString('name')
        if (name) {
            grp.name = name
        }
        grp.classification = getClassification(form.getString('classification'))
        grp.quantityUnit = form.getLong('quantityUnit')
        grp.powerUnit = form.getLong('powerUnit')
        grp.consumerMargin = form.getPercentage('consumerMargin', false)
        grp.resellerMargin = form.getPercentage('resellerMargin', false)
        grp.partnerMargin = form.getPercentage('partnerMargin', false)
        grp.calculateSales = form.getBoolean('calculateSales')
        grp.customNameSupport = form.getBoolean('customNameSupport')
        grp.providingDatasheet = form.getBoolean('providingDatasheet')
        grp.providingPicture = form.getBoolean('providingPicture')
        grp.gtin13Available = form.getBoolean('gtin13Available')
        grp.gtin14Available = form.getBoolean('gtin14Available')
        grp.gtin8Available = form.getBoolean('gtin8Available')
        
        productGroupManager.update(domainEmployee, grp)
        env.classificationConfigsView.reload()
    }

    private ProductClass getClassification(String name) {
        if (!name) {
            return null
        }
        List classes = getOptions(Options.PRODUCT_CLASSES)
        ProductClass result
        classes.each {
            if (it.name == name) {
                result = it
            }
        }
        return result
    }

    private ProductGroupManager getProductGroupManager() {
        getService(ProductGroupManager.class.getName())
    }
}

