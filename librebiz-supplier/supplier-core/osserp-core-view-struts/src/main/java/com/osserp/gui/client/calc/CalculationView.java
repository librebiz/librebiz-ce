/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13-Aug-2006 15:51:52 
 * 
 */
package com.osserp.gui.client.calc;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.web.ViewName;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.Calculator;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.sales.SalesRevenue;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("calculationView")
public interface CalculationView extends CalculationAwareView {

    /**
     * Initializes view for given calculation
     * @param calculation
     */
    public void init(Calculation calculation);

    /**
     * Performs calculation
     * @throws ClientException if validation failed
     */
    public void calculate() throws ClientException;

    /**
     * Provides the sales revenue calculation
     * @return revenue
     */
    public SalesRevenue getRevenue();

    /**
     * Indicates that the calculator was initialized and is available
     * @return calculatorAvailable
     */
    public boolean isCalculatorAvailable();

    /**
     * Provides the calculator when view is in edit mode
     * @return calculator
     */
    public Calculator getCalculator();

    /**
     * Synchronizes current history list and current calculation with backend
     */
    public void refresh();

    /**
     * Provides the related offer if calculation invoked in request context
     * @return offer
     */
    public SalesOffer getOffer();

    /**
     * Provides the related order if calculation invoked in sales context
     * @return order
     */
    public SalesOrder getOrder();

    /**
     * Indicates if calculation is changeable
     * @return true if changeable
     */
    public boolean isCalculationChangeable();

    /**
     * Indicates if a new calculation could be created 
     * @return true if no open calculations exist
     */
    public boolean isCalculationCreateable();

    /**
     * Initializes a create action by creating available names
     * @throws ClientException if an open calculation already exists
     */
    public void initCreate() throws ClientException;

    /**
     * Creates a new calculation by name as a result of client input
     * @param name
     * @throws ClientException if name already exists
     * @throws PermissionException if user has no permissions to do that
     */
    public void create(String name) throws ClientException, PermissionException;

    /**
     * Creates a new calculation as copy of the calculation given offer's based on
     * @param offer
     * @param name
     * @throws ClientException if name already exists
     */
    public void createCopy(SalesOffer offer, String name) throws ClientException;

    /**
     * Provides the available calculation names
     * @return names
     */
    public List<Option> getCalculationNames();

    /**
     * Provides available calculator names
     * @return calculatorNames
     */
    public List<String> getCalculatorNames();

    /**
     * Provides current selected calculator names
     * @return calculatorName
     */
    public String getCalculatorName();

    /**
     * Sets the calculator name for next calculation
     * @param name
     */
    public void setCalculatorName(String name);

    /**
     * Changes calculator for current calculation
     * @param name
     */
    public void changeCalculator(String name);

    /**
     * Toggles calculation sales price lock
     */
    public void toggleSalesPriceLock();

    /**
     * Is override partnerPrice permission grant (even if partnerPrice is not editable)
     * @return true if overridePartnerPricePermissionGrant
     */
    public boolean isOverridePartnerPricePermissionGrant();
}
