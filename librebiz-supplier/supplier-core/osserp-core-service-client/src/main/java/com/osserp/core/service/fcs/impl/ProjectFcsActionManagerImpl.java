/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Feb-2007 10:09:18 
 * 
 */
package com.osserp.core.service.fcs.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.Parameter;
import com.osserp.common.beans.ParameterImpl;
import com.osserp.core.FcsAction;
import com.osserp.core.Options;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.projects.ProjectFcsActionManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectFcsActionManagerImpl extends AbstractFcsActionManager implements ProjectFcsActionManager {
    private static Logger log = LoggerFactory.getLogger(ProjectFcsActionManagerImpl.class.getName());

    public ProjectFcsActionManagerImpl(FlowControlActions actions, OptionsCache optionsCache) {
        super(actions, optionsCache);
    }

    @Override
    protected String getCachedName() {
        return Options.FLOW_CONTROL_ACTIONS;
    }

    public List<Parameter> getStartActionNames() {
        List<Parameter> result = new ArrayList<Parameter>();
        result.add(new ParameterImpl("finalInvoiceResponse", "finalInvoiceResponse"));
        result.add(new ParameterImpl("invoiceRequest", "invoiceRequest"));
        result.add(new ParameterImpl("deliveryInvoiceResponse", "deliveryInvoiceResponse"));
        result.add(new ParameterImpl("deliveryRequest", "deliveryRequest"));
        result.add(new ParameterImpl("paymentResponse", "paymentResponse"));
        result.add(new ParameterImpl("partialDeliveryRequest", "partialDeliveryRequest"));
        result.add(new ParameterImpl("orderRequest", "orderRequest"));
        result.add(new ParameterImpl("downpaymentResponse", "downpaymentResponse"));
        result.add(new ParameterImpl("mainComponentDeliveryRequest", "mainComponentDeliveryRequest"));
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.projects.ProjectFcsActionManager#createAction(java.lang.Long, java.lang.String, java.lang.String, java.lang.Long, boolean)
     */
    public List<FcsAction> createAction(Long typeId, String name, String description, Long groupId, boolean displayEverytime) throws ClientException {
        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        List<FcsAction> list = getActions().findByType(typeId);
        for (int i = 0, j = list.size(); i < j; i++) {
            FcsAction next = list.get(i);
            if (next.getName() != null && next.getName().equals(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        List<FcsAction> activated = getActivated(list);
        FlowControlActions actions = (FlowControlActions) getActions();
        FlowControlAction action = (FlowControlAction) actions.createAction(
                typeId,
                name,
                description,
                groupId,
                displayEverytime,
                activated.size() + 1);
        updateEol(action, action.getOrderId() + 1);
        return findByType(typeId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.projects.ProjectFcsActionManager#updateOrder(java.lang.Long, java.lang.Long[], java.lang.Integer[])
     */
    public void updateOrder(Long typeId, Long[] id, Integer[] order)
            throws ClientException {
        if (typeId == null || id == null || order == null) {
            throw new BackendException(BackendException.INVALID_PARAM);
        }
        if (id.length != order.length) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < order.length; i++) {
            if (set.contains(order[i])) {
                throw new ClientException(ErrorCode.VALUES_DUPLICATE);
            }
            set.add(order[i]);
        }
        Map<Long, Integer> map = new HashMap<Long, Integer>();
        for (int i = 0; i < id.length; i++) {
            map.put(id[i], order[i]);
        }
        List<FcsAction> list = getActions().findByType(typeId);
        for (Iterator<FcsAction> i = list.iterator(); i.hasNext();) {
            FlowControlAction action = (FlowControlAction) i.next();
            Integer newOrder = map.get(action.getId());
            if (newOrder != null && newOrder != action.getOrderId()) {
                if (log.isDebugEnabled()) {
                    log.debug("updateOrder() setting order [action=" + action.getId()
                            + ", order=" + newOrder + "]");
                }
                action.setOrderId(newOrder);
                getActions().save(action);
            }
        }
        getCache().refresh(getCachedName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.projects.ProjectFcsActionManager#changeListPosition(com.osserp.core.FcsAction, boolean)
     */
    public List<FcsAction> changeListPosition(FcsAction flowControlAction, boolean moveUp) {
        if (!(flowControlAction instanceof FlowControlAction)) {
            throw new BackendException(BackendException.INVALID_PARAM);
        }
        FlowControlAction action = (FlowControlAction) flowControlAction;
        List<FcsAction> list = getActions().findByType(action.getTypeId());
        List<FcsAction> activated = getActivated(list);
        if ((moveUp && action.getOrderId() == 1) || (!moveUp && action.getOrderId() == activated.size())) {
            // nothing to do, beginning/end of list reached
            return list;
        }
        Integer newOrder = action.getOrderId() + (moveUp ? -1 : 1);
        for (Iterator<FcsAction> i = list.iterator(); i.hasNext();) {
            FlowControlAction nextAction = (FlowControlAction) i.next();
            if (nextAction.getOrderId() == newOrder) {
                nextAction.setOrderId(action.getOrderId());
                action.setOrderId(newOrder);
                getActions().save(nextAction);
                getActions().save(action);
            }
        }
        return getActions().findByType(action.getTypeId());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.projects.ProjectFcsActionManager#changeEolFlag(com.osserp.core.FcsAction)
     */
    public List<FcsAction> changeEolFlag(FcsAction flowControlAction) {
        if (!(flowControlAction instanceof FlowControlAction)) {
            throw new BackendException(BackendException.INVALID_PARAM);
        }
        FlowControlAction action = (FlowControlAction) flowControlAction;
        if (action.isEndOfLife()) {
            activateAction(action);
        } else {
            deactivateAction(action);
        }
        return getActions().findByType(action.getTypeId());
    }

    private void activateAction(FlowControlAction action) {
        List<FlowControlAction> activated = getActivated(action);
        action.setOrderId(activated.size() + 1);
        action.setEndOfLife(false);
        int eolOrderId = action.getOrderId() + 1;
        getActions().save(action);
        updateEol(action, eolOrderId);
    }

    private void deactivateAction(FlowControlAction action) {
        List<FlowControlAction> activated = getActivated(action);
        int deactivatedOrderId = action.getOrderId();
        int eolOrderId = activated.size();
        action.setOrderId(eolOrderId);
        action.setEndOfLife(true);
        getActions().save(action);
        for (int i = 0, j = activated.size(); i < j; i++) {
            FlowControlAction next = activated.get(i);
            if (next.getOrderId() > deactivatedOrderId) {
                next.setOrderId(next.getOrderId() - 1);
                getActions().save(next);
            }
        }
        updateEol(action, eolOrderId);
    }

    private void updateEol(FlowControlAction action, int eolOrderId) {
        List<FcsAction> other = getActions().findByType(action.getTypeId());
        for (int i = 0, j = other.size(); i < j; i++) {
            FlowControlAction next = (FlowControlAction) other.get(i);
            if (next.isEndOfLife() && !next.getId().equals(action.getId())) {
                next.setOrderId(eolOrderId);
                getActions().save(next);
            }
        }
    }

    private List<FlowControlAction> getActivated(FlowControlAction action) {
        List<FlowControlAction> result = new ArrayList<FlowControlAction>();
        List<FcsAction> all = getActions().findByType(action.getTypeId());
        for (int i = 0, j = all.size(); i < j; i++) {
            FlowControlAction next = (FlowControlAction) all.get(i);
            if (!next.isEndOfLife()) {
                result.add(next);
            }
        }
        return result;
    }

    private List<FcsAction> getActivated(List<FcsAction> actions) {
        List<FcsAction> result = new ArrayList<FcsAction>();
        for (int i = 0, j = actions.size(); i < j; i++) {
            FcsAction next = actions.get(i);
            if (!next.isEndOfLife()) {
                result.add(next);
            }
        }
        return result;
    }
}
