/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.dao


/**
 * 
 * LDAP Domain data access interface
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
interface Internetdomains extends LdapDao {

    /**
     * Finds domain by name
     * @param name
     * @param client if client based dn required
     * @return domain or empty map if not exists
     */
    Map find(String name, String client)

    /**
     * Provides all available domains
     * @param client if client based dn required
     * @return domains
     */
    List<Map> findAll(String client)

    /**
     * Creates a new domain entry
     * @param name
     * @param mailhost
     * @param client
     * @return new created domain or existing if domain already exists
     */
    Map create(String name, String mailhost, String client)

    /**
     * Deletes a domain
     * @param domain
     */
    void delete(Map domain)

    /**
     * Updates domain expiration date
     * @param domain
     * @param expiration
     * @return updated domain 
     */
    Map updateExpiration(Map domain, String expiration)

    /**
     * Updates the mailhosts of a domain
     * @param domain
     * @param mailhost
     * @param mailrelay
     * @return updated domain 
     */
    Map updateMailhosts(Map domain, String mailhost, String mailrelay)
}
