<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<tr>
    <td><o:fileUpload /></td>
    <td><v:date name="sourceDate" picker="true" title="optionalDateLabel" styleClass="form-control date inline" /></td>
    <td<c:if test="${dmsDocumentView.syncClientEnabled and !empty dmsDocumentView.list}"> colspan="2"</c:if>><v:text name="note" styleClass="form-control" placeholder="${dmsDocumentView.documentNotePlaceholder}" /></td>
    <td><o:submitFile /></td>
</tr>
