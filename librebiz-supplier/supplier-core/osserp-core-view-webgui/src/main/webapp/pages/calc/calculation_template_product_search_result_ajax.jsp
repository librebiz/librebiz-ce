<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.calculationTemplateProductSearchView}"/>

<div class="table-responsive table-responsive-default">
	<c:set var="includeLazy" value="${!view.ignoreLazy}"/>
	<v:form url="/admin/calculations/calculationTemplates/save">
		<input type="hidden" name="selectedGroup" value="${view.selectedConfigGroup.id}" />
		<c:if test="${view.selectedItemId != null}">
			<input type="hidden" name="selectedItemId" value="${view.selectedItemId}" />
		</c:if>
        <table class="table table-striped">
			<thead>
				<tr>
					<th class="productSelection"><input type="image" name="submit" value="all" src="<c:url value="${applicationScope.saveIcon}"/>" class="bigicon" title="<fmt:message key="saveSelectedItems"/>"/></th>
					<th class="productNumber"><a href="<c:url value="/calculationTemplateProductSearch.do?method=sort&order=byId" />"><fmt:message key="product"/></a></th>
					<th class="productName"><a href="<c:url value="/calculationTemplateProductSearch.do?method=sort&order=byName" />"><fmt:message key="name"/></a></th>
					<th class="action"><fmt:message key="action"/></th>
				</tr>
			</thead>
			<tbody style="<c:choose><c:when test="${view.productSelectionConfigsMode}">height:410px;</c:when><c:otherwise>height:350px;</c:otherwise></c:choose>">
				<c:forEach var="product" items="${view.list}" varStatus="s">
					<c:if test="${includeLazy or !product.lazy}">
						<tr>
							<td class="productSelection">
								<input type="checkbox" name="added" value="<o:out value="${product.productId}"/>" />
							</td>
							<td class="productNumber">
								<a href="<c:url value="/products.do?method=load&id=${product.productId}&exit=calculationTemplateProductSearch"/>">
									<o:out value="${product.productId}"/><input type="hidden" name="productId" value="<o:out value="${product.productId}"/>" />
								</a>
							</td>
							<td class="productName">
								<c:choose>
									<c:when test="${empty sessionScope.openSalesOrdersView}">
										<a href="<c:url value="/calculations.do?method=forwardProductPlanning&id=${product.productId}&exit=calculationTemplateProductSearch"/>" title="<fmt:message key="deliveryStatus"/>">
											<o:out value="${product.name}"/>
										</a>
									</c:when>
									<c:otherwise>
										<o:out value="${product.name}"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td class="action">
								<input type="image" name="submit" value="<o:out value="${product.productId}"/>" src="<c:url value="${applicationScope.saveIcon}"/>" class="bigicon" title="<fmt:message key="saveSelectedItems"/>"/>
							</td>
						</tr>
						<tr>
                            <td class="productSelection"> </td>
                            <td class="productNumber"> </td>
							<td class="productNote">
								<input type="text" class="form-control noteInput" name="note" value=""/>
                                <input type="hidden" name="customName" value=""/>
							</td>
							<td class="action"> </td>
						</tr>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
	</v:form>
</div>
