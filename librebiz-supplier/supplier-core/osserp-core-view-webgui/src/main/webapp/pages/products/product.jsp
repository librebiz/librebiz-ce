<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.productView}"/>
<c:set var="view" scope="request" value="${sessionScope.productView}"/>
<c:set var="isPopup" scope="request" value="false"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>

	<tiles:put name="headline">
		<c:choose>
			<c:when test="${view.salesVolumeMode}">
				<fmt:message key="endCustomerSale"/>
			</c:when>
			<c:when test="${view.stockReportMode}">
				<fmt:message key="entrancesAndOutlets"/>
			</c:when>
			<c:otherwise>
				<fmt:message key="product"/>
			</c:otherwise>
		</c:choose>
        <o:out value="${view.product.productId}"/> - <o:out value="${view.product.name}" limit="75"/>
	</tiles:put>

    <tiles:put name="headline_right">
        <c:choose>
            <c:when test="${view.salesVolumeMode}">
                <a href="<c:url value="/products.do?method=disableSalesVolumeDisplay"/>"><o:img name="backIcon" /></a>
            </c:when>
            <c:when test="${view.stockReportMode}">
                <a href="<c:url value="/products.do?method=disableStockReport"/>"><o:img name="backIcon" /></a>
            </c:when>
            <c:otherwise>
                <c:set var="product" value="${view.product}"/>
                <a href="<c:url value="/products.do?method=exit"/>"><o:img name="backIcon" /></a>
                <o:permission role="product_create,executive" info="permissionProductCopyCreate">
                    <v:link url="/products/productCreator/forward?exit=/productDisplay.do" title="createByCopy"><o:img name="copyIcon" /></v:link>
                </o:permission>
                <o:permission role="product_create,product_edit,executive" info="permissionProductEdit">
                    <v:link url="/products/productEditor/forward?exit=/products.do?method=redisplay" title="edit"><o:img name="writeIcon" /></v:link>
                </o:permission>
                <v:link url="/products/productSettings/forward?exit=/products.do?method=redisplay" title="settings"><o:img name="configureIcon"/></v:link>
                <o:permission role="product_create,product_edit,product_details_edit,product_description_edit,product_edit_even_locked,product_datasheet_upload,product_datasheet_upload_admin,executive" info="permissionNotes">
                    <v:link url="/notes/note/forward?id=${product.productId}&name=productNote&exit=/products.do?method=reload" title="displayAndWriteNotes"><o:img name="texteditIcon"/></v:link>
                </o:permission>
            </c:otherwise>
        </c:choose>
        <v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
    </tiles:put>
    <tiles:put name="content" type="string">
        <c:choose>
            <c:when test="${view.salesVolumeMode}">
                <c:import url="/pages/products/shared/product_sales_volume.jsp"/>
            </c:when>
            <c:when test="${view.stockReportMode}">
                <div class="content-area">
                    <div class="row">
                        <c:import url="/pages/products/shared/product_stock_report.jsp"/>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <c:import url="/pages/products/shared/product_display.jsp"/>
            </c:otherwise>
        </c:choose>
    </tiles:put>
</tiles:insert>
