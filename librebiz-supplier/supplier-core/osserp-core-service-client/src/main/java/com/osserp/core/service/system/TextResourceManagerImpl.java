/**
 *
 * Copyright (C) 2018 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 3, 2018 
 * 
 */
package com.osserp.core.service.system;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.osserp.common.Option;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.Comparators;
import com.osserp.core.dao.ResourceProvider;
import com.osserp.core.system.TextResourceManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TextResourceManagerImpl implements TextResourceManager {

    private ResourceLocator resourceLocator;
    private ResourceProvider resourceProvider;

    protected TextResourceManagerImpl(
            ResourceLocator resourceLocator,
            ResourceProvider resourceProvider) {
        super();
        this.resourceLocator = resourceLocator;
        this.resourceProvider = resourceProvider;
    }

    public List<Option> getResources(Locale locale) {
        List<Option> result = new ArrayList<>();
        ResourceBundle bundle = resourceLocator.getResourceBundle(locale);
        Enumeration e = bundle.getKeys();
        while (e.hasMoreElements()) {
            String i18nkey = (String) e.nextElement();
            String value = bundle.getString(i18nkey);
            if (value != null && value.indexOf(".png") < 0
                    && value.indexOf(".jpg") < 0
                    && value.indexOf(".gif") < 0) {
                result.add(new OptionImpl(null, value, i18nkey));
            }
        }
        CollectionUtil.sort(result, Comparators.createOptionNameComparator(false));
        return result;
    }

    public List<Option> update(Locale locale, String i18nkey, String value) {
        resourceProvider.updateResource(locale.getLanguage(), i18nkey, value);
        resourceLocator.reload(locale);
        return getResources(locale);
    }

    protected void setResourceLocator(ResourceLocator resourceLocator) {
        this.resourceLocator = resourceLocator;
    }
}
