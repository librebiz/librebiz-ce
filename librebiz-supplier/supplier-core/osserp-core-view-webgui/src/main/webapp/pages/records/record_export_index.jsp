<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.recordExportView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="styles" type="string">
        <style type="text/css">
        <c:import url="/pages/records/record_export.css"/>
        </style>
    </tiles:put>

    <tiles:put name="headline"><fmt:message key="recordJournal"/></tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${view.typeSelectionMode}">
                    <li><a href="<c:url value="/recordExports.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                </c:when>
                <c:when test="${!view.typeSelectionMode}">
                    <li><a href="<c:url value="/recordExports.do?method=exitSelectType"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                </c:when>
            </c:choose>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="voucherExportContent">
            <div class="row">

                <c:choose>
                    <c:when test="${view.typeSelectionMode}">
                        <div class="col-md-6 panel-area panel-area-default">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4><fmt:message key="selectionRecordType"/></h4>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive table-responsive-default">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td class="typeName"> </td>
                                                <td class="typeCount"><fmt:message key="new"/></td>
                                                <td class="typeCount"><fmt:message key="exports"/></td>
                                            </tr>
                                            <c:forEach var="obj" items="${view.recordTypes}">
                                                <tr>
                                                    <td class="typeName"><a href="<c:url value="/recordExports.do?method=selectType&id=${obj.type}"/>"><o:out value="${obj.name}"/></a></td>
                                                    <td class="typeCount"><o:out value="${obj.available}"/></td>
                                                    <td class="typeCount"><o:out value="${obj.archived}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-6 panel-area panel-area-default">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4><fmt:message key="selectedRecordType"/>: <o:out value="${view.exportType.name}"/></h4>
                                </div>
                            </div>
                            <div class="panel-body">

                                <o:form name="recordExportForm" url="/recordExports.do">
                                    <div class="table-responsive table-responsive-default">
                                        <table class="table table-plain">
                                            <tbody>
                                                <c:choose>
                                                    <c:when test="${!empty view.availableCount and view.availableCount > 0}">
                                                        <tr>
                                                            <td class="name">
                                                                <a href="<c:url value="/recordExports.do?method=listUnexported"/>">
                                                                    <o:out value="${view.availableCount}"/> <fmt:message key="openNewRecordsFound"/>
                                                                </a>
                                                            </td>
                                                            <td class="action">
                                                                <c:if test="${!view.exportConfirmationMode and empty view.selected}">
                                                                    <a href="<c:url value="/recordExports.do?method=createExport"/>">[<fmt:message key="export"/>]</a>
                                                                </c:if>
                                                            </td>
                                                        </tr>

                                                        <c:if test="${!empty view.selected}">
                                                            <tr>
                                                                <td class="name">
                                                                    <a href="<c:url value="/recordExports.do?method=listSelected"/>">
                                                                        <o:out value="${view.selected.size()}"/> <fmt:message key="openNewRecordsSelected"/>
                                                                    </a>
                                                                </td>
                                                                <td class="action">
                                                                    <c:if test="${!view.exportConfirmationMode}">
                                                                        <a href="<c:url value="/recordExports.do?method=createExport"/>">[<fmt:message key="export"/>]</a>
                                                                    </c:if>
                                                                </td>
                                                            </tr>
                                                        </c:if>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <tr>
                                                            <td class="name"><fmt:message key="noRecordsToExportAvailable"/></td>
                                                            <td class="action"> </td>
                                                        </tr>
                                                    </c:otherwise>
                                                </c:choose>

                                                <c:choose>
                                                    <c:when test="${view.exportConfirmationMode}">
                                                        <tr>
                                                            <td class="name" colspan="2"><input type="hidden" name="method" value="save"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name" colspan="2">
                                                                <p class="boldtext"><fmt:message key="recordExport"/></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name" colspan="2">
                                                                <fmt:message key="recordExportHint1"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name" colspan="2">
                                                                <fmt:message key="recordExportHint2"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name" colspan="2">
                                                                <fmt:message key="recordExportHint3"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name" colspan="2">
                                                                <fmt:message key="recordExportHint4"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name" colspan="2">
                                                                <fmt:message key="recordExportHint5"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name" colspan="2">
                                                                <o:out value="${view.selected.size()}"/> <fmt:message key="previousSelected"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name" colspan="2">
                                                                <fmt:message key="recordExportHint6"/> (<o:out value="${view.availableCount}"/>).
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name"><fmt:message key="fromDate"/></td>
                                                            <td class="action"><input type="text" name="start" class="form-control" value=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name"><fmt:message key="til"/> (<fmt:message key="exclusive"/>)</td>
                                                            <td class="action"><input type="text" name="end" class="form-control" value=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name">
                                                            </td>
                                                            <td class="action">
                                                                <input type="submit" name="submit" class="form-control" value="<fmt:message key="confirmRecordExport"/>"/>
                                                            </td>
                                                        </tr>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <tr>
                                                            <td class="name" colspan="2"> </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="name" colspan="2">
                                                                <c:choose>
                                                                    <c:when test="${view.archiveAvailable}">
                                                                        <a href="<c:url value="/recordExports.do?method=displayArchive"/>">
                                                                            <o:out value="${view.archiveCount}"/> <fmt:message key="historicalRecordExports"/>
                                                                        </a>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <span>0</span> <fmt:message key="historicalRecordExports"/>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </td>
                                                        </tr>
                                                    </c:otherwise>
                                                </c:choose>
                                            </tbody>
                                        </table>
                                    </div>
                                </o:form>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
