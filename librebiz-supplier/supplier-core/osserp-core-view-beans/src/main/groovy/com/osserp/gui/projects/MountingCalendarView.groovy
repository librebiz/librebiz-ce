/**
 *
 * Copyright (C) 2007, 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.projects

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Appointment
import com.osserp.common.Calendar
import com.osserp.common.ClientException
import com.osserp.common.beans.DefaultAppointment
import com.osserp.common.util.CalendarUtil
import com.osserp.common.util.DateUtil
import com.osserp.common.util.StringUtil
import com.osserp.core.employees.EmployeeRole
import com.osserp.core.employees.EmployeeRoleConfig
import com.osserp.core.events.Event
import com.osserp.core.events.EventManager
import com.osserp.core.projects.ProjectSearch
import com.osserp.core.sales.SalesListItem
import com.osserp.core.users.DomainUser
import com.osserp.gui.common.CalendarAwareView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class MountingCalendarView extends CalendarAwareView {
    private static Logger logger = LoggerFactory.getLogger(MountingCalendarView.class.getName())

    boolean supplierDisplayMode = false

    MountingCalendarView() {
        super()
        headerName = 'mountingCalendar'
        dayViewAvailable = false
        invocationTargetUrl = '/loadSales.do?id=@@&exit=mountingCalendarReload'
    }

    /*
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            daySelectionTargetUrl = "/projects/mountingCalendarDay/forward?exit=${baseLink}/reload"
        }
    }
    */

    @Override
    protected List<Appointment> fetchAppointments() {
        List<Appointment> result = []
        List<SalesListItem> items = projectSearch.findMountings()
        items.each { SalesListItem item ->

            if (item.getAppointmentDate() != null) {
                if (!sameDayAlreadyAdded(result, item)) {
                    Appointment ap = item.createAppointment(invocationTargetUrl, null)
                    String msg = "${item.supplierGroup} - ${item.supplierName}"
                    ap.overrideDefaults(null, null, msg)
                    result.add(ap)
                }
            }
        }
        return result
    }

    private boolean sameDayAlreadyAdded(List<Appointment> result, SalesListItem item) {
        for (Iterator<Appointment> iterator = result.iterator(); iterator.hasNext();) {
            Appointment next = iterator.next();
            if (next.getId().equals(item.getId())) {
                if (DateUtil.isSameDay(next.getDate(), item.getAppointmentDate())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isLogisticUserOnly() {
        DomainUser user = getDomainUser()
        List<EmployeeRoleConfig> configs = user.getEmployee().getRoleConfigs()
        boolean inLogistic = false;
        boolean inOtherGroups = false;
        configs.each { EmployeeRoleConfig nextConfig ->
            nextConfig.getRoles().each { EmployeeRole role ->
                if (role.getGroup().isLogistics()) {
                    inLogistic = true;
                } else {
                    inOtherGroups = true;
                }
            }
        }
        return (inOtherGroups ? false : inLogistic);
    }

    private ProjectSearch getProjectSearch() {
        return getService(ProjectSearch.class.getName())
    }

}
