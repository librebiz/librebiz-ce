<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="projectSupplierConfigView" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="javascript" type="string">
    </tiles:put>
    <tiles:put name="headline">
        <o:out value="${view.businessCase.type.name}"/>
        <o:out value="${view.businessCase.id}"/>
        <span> - <fmt:message key="involvedPartiesHeader" /></span>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="projectSupplierConfigContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.deleteMode}">
                        <v:form url="${view.baseLink}/delete" id="projectSupplierConfigForm" name="projectSupplierConfigForm">
                            <c:import url="_projectSupplierConfigDelete.jsp" />
                        </v:form>
                    </c:when>
                    <c:when test="${view.createMode}">
                        <v:form url="${view.baseLink}/save" id="projectSupplierConfigForm" name="projectSupplierConfigForm">
                            <c:import url="_projectSupplierConfigCreate.jsp" />
                        </v:form>
                    </c:when>
                    <c:when test="${view.editMode}">
                        <v:form url="${view.baseLink}/save" id="projectSupplierConfigForm" name="projectSupplierConfigForm">
                            <c:import url="_projectSupplierConfigEdit.jsp" />
                        </v:form>
                    </c:when>
                    <c:otherwise>
                        <%-- <c:import url="_projectSupplierConfig.jsp" /> --%>
                    </c:otherwise>
                </c:choose>
            </div> <!-- row -->
        </div> <!-- content-area -->
    </tiles:put>
</tiles:insert>
