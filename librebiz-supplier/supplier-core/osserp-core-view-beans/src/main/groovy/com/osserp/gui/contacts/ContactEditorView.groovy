/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 17, 2016 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ActionException
import com.osserp.common.Option

import com.osserp.core.AddressLocality
import com.osserp.core.Options
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactCountry
import com.osserp.core.contacts.PersonManager
import com.osserp.core.contacts.Salutation

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ContactEditorView extends ContactEditingView {
    private static Logger logger = LoggerFactory.getLogger(ContactEditorView.class.getName())


    ContactEditorView() {
        super()
        headerName = 'contactEdit'
        dependencies = ['contactView', 'contactPersonView']
    }
    
    Contact getContact() {
        bean
    }
    
    boolean isTenantSelected() {
        contact?.client
    }
    
    boolean isOfficeSelected() {
        contact?.branchOffice
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            Long id = form.getLong('id')
            if (id) {
                bean = contactSearch.find(id)
                if (bean) {
                    if (contact.contactPerson && contact.reference) {
                        parent = contactSearch.find(contact.reference)
                        headerName = 'contactPersonEdit'
                        selectedType = contactManager.contactPersonType
                    } else {
                        selectedType = contact.type
                    }
                }
            }
            enableEditMode()
        }
    }
    
    void save() {
        if (!contact) {
            throw new ActionException()
        }
        if (updateOnly) {
            
            countrySelection(form.getLong('country'))
            localitySelection(form.getString('zipcode'))
            
        } else if (typeChangeMode) {
        
            String company = form.getString('company')
            contactManager.changeType(domainEmployee, contact, company)
            if (env.contactView) {
                env.contactView.refresh()
            }

        } else {
            validateContact()
            validateAddress(isContactPersonSelected() ? OPTIONAL : REQUIRED)
            Salutation salutation = getSalutation(form.getLong('salutation'))
            Option title = getTitle(form.getLong('title'))
            bean = contactManager.updateContact(
                    domainUser, 
                    contact, 
                    salutation, 
                    title, 
                    form.getBoolean('firstNamePrefix'), 
                    form.getString('firstName'), 
                    form.getString('lastName'), 
                    form.getString('street'), 
                    form.getString('zipcode'), 
                    form.getString('city'), 
                    form.getLong('federalStateId'), 
                    form.getString('federalStateName'), 
                    form.getLong('country'),
                    form.getString('office'),
                    form.getString('position'),
                    form.getString('section'),
                    form.getString('website'), 
                    form.getDate('birthDate'), 
                    getSalutation(form.getLong('spouseSalutation')),
                    getTitle(form.getLong('spouseTitle')),
                    form.getString('spouseFirstName'),
                    form.getString('spouseLastName'),
                    form.getBoolean('grantContactPerEmail'), 
                    form.getString('grantContactPerEmailNote'), 
                    form.getBoolean('grantContactPerPhone'), 
                    form.getString('grantContactPerPhoneNote'),
                    form.getBoolean('grantContactNone'),
                    form.getString('grantContactNoneNote'),
                    form.getBoolean('newsletterConfirmation'),
                    form.getString('newsletterConfirmationNote'),
                    form.getBoolean('vip'),
                    form.getString('vipNote'))

            if (env.contactView) {
                env.contactView.refresh()
            } 
            if (env.contactPersonView) {
                env.contactPersonView.refresh()
            }
        }
    }
    
    boolean isTypeChangeMode() {
        env.typeChangeMode
    }

    boolean isTypeChangeModeAvailable() {
        !typeChangeMode && !contactPersonSelected && !contact.business
    }
    
    void enableTypeChangeMode() {
        env.typeChangeMode = true
    }
    
    void disableTypeChangeMode() {
        env.typeChangeMode = false
    }
    
    protected void countrySelection(Long id) {
        contact?.address?.country = id
        if (contact?.address?.country) {
            ContactCountry obj = getOption(Options.COUNTRY_NAMES, contact.address.country)
            if (obj.customFederalState) {
                customFederalState = true
            } else {
                customFederalState = false
            }
        }
        logger.debug("countrySelection: done [country=${contact?.address?.country}, customFederalState=${customFederalState}]")
    }
    
    protected void localitySelection(String zipcode) {
        contact?.address?.zipcode = zipcode
        if (contact?.address?.zipcode) {
            List<AddressLocality> localities = systemConfigManager.getLocalitySelection(contact.address.zipcode)
            if (localities.size() == 1) {
                contact.address.city = localities.get(0).city
                form.setValue('city', contact.address.city)
                logger.debug("localitySelection: done [zipcode=${contact.address.zipcode}, city=${contact.address.city}]")
            }
        }
    }

    protected PersonManager getPersonManager() {
        contactManager
    }
}
