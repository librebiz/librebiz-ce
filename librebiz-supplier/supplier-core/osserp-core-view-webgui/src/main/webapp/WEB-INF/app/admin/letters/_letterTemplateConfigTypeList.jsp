<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="typeName"><fmt:message key="name" /></th>
                    <th class="typeDescription"><fmt:message key="description" /></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty view.availableTypes}">
                        <tr>
                            <td colspan="2"><fmt:message key="noLetterTypesAvailable" /></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="obj" items="${view.availableTypes}">
                            <tr>
                                <td class="typeName"><v:link url="/admin/letters/letterTemplateConfig/selectType?id=${obj.id}">
                                        <o:out value="${obj.name}" />
                                    </v:link></td>
                                <td class="typeDescription"><o:out value="${obj.description}" /></td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
