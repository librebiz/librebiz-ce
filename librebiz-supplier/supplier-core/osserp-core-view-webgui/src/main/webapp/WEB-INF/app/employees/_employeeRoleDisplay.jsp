<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeRoleDisplayView" />

<div class="row row-modal">
    <div class="col-md-6 panel-area panel-area-modal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <o:out value="${view.group.id}" />
                    -
                    <o:out value="${view.group.name}" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="center">UID</th>
                            <th class="small"><fmt:message key="user" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.list}">
                                <tr>
                                    <td colspan="2" class="left"><fmt:message key="noRolesDefined" /></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="obj" items="${view.list}">
                                    <tr>
                                        <td class="center"><o:out value="${obj.employeeId}" /></td>
                                        <td class="left"><o:out value="${obj.employee}" /></td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

