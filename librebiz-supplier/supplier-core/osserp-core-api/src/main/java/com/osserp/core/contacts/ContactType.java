/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 15, 2009 8:52:42 AM 
 * 
 */
package com.osserp.core.contacts;

import com.osserp.common.Mappable;
import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ContactType extends Mappable, Option {
    
    /**
     * Provides a short name of the type. This name is 
     * dedicated to display a short type key in list views.
     * @return shortname
     */
    String getShortname();

    /**
     * Indicates if contacts of this type are references to other contact,
     * e.g. contact persons of a company or institution
     * @return true if strict referenced by another contact
     */
    boolean isChild();

    /**
     * Indicates that person is a business (eg. company, freelance or public institution)
     * @return business if contact type public, business or freelance
     */
    boolean isBusiness();

    /**
     * Indicates that person is a business but not public institution
     * @return if contact type is business only
     */
    boolean isCompany();

    /**
     * Indicates that person is a freelancer business, using firstname and lastname for person and office as company name
     * @return freelancer business if contact type freelancer
     */
    boolean isFreelance();

    /**
     * Indicates that person is a public institution or interest body, using lastname as name of instition. 
     * Contact persons are stored as contacts of type person.
     * @return true if contact type is public institution
     */
    boolean isPublicInstitution();

    /**
     * Indicates that contact is privatePerson or freelancer. The context for contacts of this
     * type does not provide methods to add contact persons.
     * @return private contact or freelancer
     */
    boolean isPerson();

    /**
     * Indicates that contact is a private person, e.g. consumer
     * @return private contact
     */
    boolean isPrivatePerson();
    
    /**
     * Indicates that person is a contact of another contact
     * @return true if contact is related to another contact
     */
    boolean isContactPerson();
}
