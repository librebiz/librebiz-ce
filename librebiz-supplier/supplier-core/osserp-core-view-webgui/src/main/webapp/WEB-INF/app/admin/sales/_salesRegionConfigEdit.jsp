<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="column25l">
    <div class="subcolumnl">
        <div class="spacer"></div>
    </div>
</div>

<div class="column50l">
    <div class="subcolumnc">

        <div class="spacer"></div>
        <div class="contentBox">
            <div class="contentBoxHeader">
                <div class="contentBoxHeaderLeft">
                    <fmt:message key="salesRegionEdit" />
                </div>
            </div>
            <div class="contentBoxData">
                <div class="subcolumns">
                    <div class="subcolumn">
                        <div class="spacer"></div>

                        <v:form name="salesRegionConfigForm" url="/admin/sales/salesRegionConfig/save">
                            <table class="valueTable first33">
                                <c:import url="_salesRegionConfigInput.jsp" />
                                <tr>
                                    <td class="row-submit"><input class="cancel" type="button" onclick="gotoUrl('<v:url value="/admin/sales/salesRegionConfig/disableEditMode"/>');" value="<fmt:message key="exit"/>" /></td>
                                    <td class="row-submit"><o:submit /></td>
                                </tr>
                            </table>
                        </v:form>

                        <div class="spacer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="spacer"></div>

    </div>
</div>

<div class="column25r">
    <div class="subcolumnr">
        <div class="spacer"></div>
    </div>
</div>