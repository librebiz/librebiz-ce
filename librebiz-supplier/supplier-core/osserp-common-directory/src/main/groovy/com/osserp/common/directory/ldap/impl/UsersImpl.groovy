/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.apache.directory.groovyldap.LDAP
import org.apache.directory.groovyldap.SearchScope

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.LdapException
import com.osserp.common.directory.ldap.StructuralSchema
import com.osserp.common.directory.ldap.dao.FetchmailAccounts
import com.osserp.common.directory.ldap.dao.UserContacts
import com.osserp.common.directory.ldap.dao.Users
import com.osserp.common.util.SecurityUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class UsersImpl extends AbstractLdapDao implements Users {
    private static Logger logger = LoggerFactory.getLogger(UsersImpl.class.getName())

    FetchmailAccounts fetchmailAccounts
    UserContacts userContacts
    StructuralSchema schema

    protected UsersImpl() {
        super()
        logger.debug('<init> done without configInstance')
    }

    UsersImpl(LdapConfig configInstance) {
        super(configInstance)
        fetchmailAccounts = new FetchmailAccountsImpl(configInstance)
        schema = new UsersSchema(configInstance)
        userContacts = new UserContactsImpl(configInstance)
    }

    List<Map> findAll(String client) {
        LDAP ldap = createConnection()
        ldap.search(schema.query, createBaseDn(client), SearchScope.SUB)
    }

    Map lookup(String uid, String client) {
        def dn = createDn(uid, client)
        lookupByDn(dn)
    }

    Map create(Map atts, String client) {
        LDAP ldap = createConnection()
        if (!atts.uid) {
            atts.errors = 'error.uid.required'
            return atts
        }
        def dn = createDn(atts.uid, client)
        if (ldap.exists(dn)) {
            atts.errors = 'error.uid.exists'
            return atts
        }
        if (client) {
            atts.clientname = client
        }
        Map valueObject = clearNull(schema.create(atts))
        ldap.add(dn, valueObject)
        logger.debug("create: done [dn=${dn}]")
        ldap.read(dn)
    }

    Map changePassword(Map user, String password) {
        LDAP ldap = createConnection()
        def userPassword = SecurityUtil.createPassword(password).getBytes()
        def newValues = [ userpassword: userPassword ]
        ldap.modify(user.dn, 'REPLACE', newValues)
        lookupByDn(user.dn)
    }

    void delete(Map user) {
        LDAP ldap = createConnection()
        if (user?.dn && ldap.exists(user.dn)) {
            try {
                ldap.delete(user.dn)
                logger.debug("delete: done [dn=${user.dn}]")
            } catch (Throwable t) {
                logger.error("delete: failed [message=${t.message}]")
            }
        }
    }

    List<String> getAllAttributes() {
        schema.getAvailableAttributes()
    }

    List<String> getDisplayAttributes() {
        schema.getDefaultDisplayAttributes()
    }

    Set<String> getArrayAttributes() {
        schema.getAvailableArrays()
    }

    Map addMailaccount(
            Map user,
            String mailAddress,
            String fetchmailAccount,
            String fetchmailPassword) throws LdapException {

        LDAP ldap = createConnection()

        if (mailAddress && (!user.mail || user.mail != mailAddress)) {
            def newValues = [ mail: mailAddress ]
            ldap.modify(user.dn, 'REPLACE', newValues)
        }
        Map result = ldap.read(user.dn)
        if (config.fetchmailSupportEnabled) {
            Map account = fetchmailAccounts.createFetchmailAccount(
                    result,
                    fetchmailAccount,
                    fetchmailPassword)
            if (account) {
                result.mailAccount = account
            }
        }
        return result
    }

    Map addMailaddress(Map user, String mailAddress) throws LdapException {

        LDAP ldap = createConnection()

        if (mailAddress && (!user.mail || user.mail != mailAddress)) {
            def newValues = [ mail: mailAddress ]
            ldap.modify(user.dn, 'REPLACE', newValues)
        }
        ldap.read(user.dn)
    }

    Map addMailalias(Map user, String alias) {
        LDAP ldap = createConnection()
        if (!config.userMailaliasAttribute) {
            logger.warn('addMailAlias: alias not added [message=error.missing.config.attribute.name]')
            return user
        }
        if (alias && user) {
            boolean alreadyAdded = false
            List existing = []
            // alias attribute value may be a string if list has just one element
            def aliasVal = user[config.userMailaliasAttribute]
            if (aliasVal) {
                existing = aliasVal instanceof String ? [aliasVal]: aliasVal
            }
            existing.each {
                if (it == alias) {
                    alreadyAdded = true
                }
            }
            if (!alreadyAdded) {
                existing.add(alias)
            }
            def newValues = [ (config.userMailaliasAttribute): existing ]
            ldap.modify(user.dn, 'REPLACE', newValues)
        }
        ldap.read(user.dn)
    }

    Map removeMailalias(Map user, String alias) {
        LDAP ldap = createConnection()

        if (alias && user) {
            boolean removed = false
            List existing = user[config.userMailaliasAttribute]
            for (Iterator i = existing.iterator(); i.hasNext();) {
                def existingAlias = i.next()
                if (existingAlias == alias) {
                    i.remove()
                    removed = true
                    break
                }
            }
            if (removed) {
                def newValues = [ (config.userMailaliasAttribute): existing ]
                ldap.modify(user.dn, 'REPLACE', newValues)
            }
        }
        ldap.read(user.dn)
    }

    void enableFetchmailAccount(String uid, String client) {
        Map user = lookup(uid, client)
        if (user) {
            fetchmailAccounts.enableAccount(user, null)
        }
    }

    void disableFetchmailAccount(String uid, String client) {
        Map user = lookup(uid, client)
        if (user) {
            fetchmailAccounts.disableAccount(user, null)
        }
    }

    final Map lookupByDn(String dn) {
        logger.debug("lookupByDn: invoked [dn=${dn}]")
        Map result = [:]
        LDAP ldap = createConnection()
        if (ldap.exists(dn)) {
            result = ldap.read(dn)
            if (config.fetchmailSupportEnabled) {
                Map fm = fetchmailAccounts.lookupFetchmailAccount(result)
                if (fm) {
                    result.mailAccount = fm
                }
            }
        }
        return result
    }

    @Override
    protected Map applySchema(Map values, Map existing) {
        schema.apply(values, existing)
    }

    @Override
    protected String createDn(String uid, String client) {
        super.createDn("uid=${uid},ou=Users", client)
    }
}

