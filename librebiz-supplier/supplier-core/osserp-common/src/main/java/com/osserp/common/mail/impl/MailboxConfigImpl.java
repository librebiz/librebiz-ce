/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 31, 2008 2:22:06 PM 
 * 
 */
package com.osserp.common.mail.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.mail.MailboxConfig;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class MailboxConfigImpl extends AbstractOption implements MailboxConfig {

    private String aliases = null;
    private String defaultAlias = null;
    private String host = null;
    private String user = null;
    private String password = null;
    private String port = null;
    private boolean auth = false;
    private String socketFactoryClass = null;
    private String protocol = null;

    private String attachmentsStore = null;
    private String inboxName = null;
    private String inboxArchiveName = null;
    private String inboxDraftsName = null;
    private String inboxFailedName = null;
    private String inboxSentName = null;
    private String inboxSpamName = null;
    private String inboxTrashName = null;

    protected MailboxConfigImpl() {
        super();
        port = MailboxConfig.DEFAULT_IMAP_SSL_PORT;
        protocol = MailboxConfig.DEFAULT_PROTOCOL;
        inboxName = MailboxConfig.DEFAULT_IMAP_INBOX;
        inboxArchiveName = MailboxConfig.DEFAULT_IMAP_INBOX_ARCHIVE;
        inboxDraftsName = MailboxConfig.DEFAULT_IMAP_INBOX_DRAFTS;
        inboxFailedName = MailboxConfig.DEFAULT_IMAP_INBOX_FAILED;
        inboxSentName = MailboxConfig.DEFAULT_IMAP_INBOX_SENT;
        inboxSpamName = MailboxConfig.DEFAULT_IMAP_INBOX_SPAM;
        inboxTrashName = MailboxConfig.DEFAULT_IMAP_INBOX_TRASH;
    }

    /**
     * Provides a simple IMAP SSL config
     * @param hostname
     * @param port
     * @param username
     * @param password
     * @return mailbox config
     */
    public static final MailboxConfig createIMAPSSL(
            String hostname,
            Integer port,
            String username,
            String password,
            String attachmentsStore) {
        MailboxConfigImpl mbc = new MailboxConfigImpl();
        mbc.setHost(hostname);
        if (port != null && port > 0) {
            mbc.setPort(port.toString());
        }
        mbc.setUser(username);
        mbc.setPassword(password);
        if (attachmentsStore != null) {
            mbc.setAttachmentsStore(attachmentsStore);
        }
        return mbc;
    }

    /**
     * All values constructor
     * @param id
     * @param reference
     * @param name
     * @param aliases
     * @param defaultAlias
     * @param host
     * @param user
     * @param password
     * @param port
     * @param auth
     * @param protocol
     */
    public MailboxConfigImpl(
            Long id,
            Long reference,
            String name,
            String aliases,
            String defaultAlias,
            String host,
            String user,
            String password,
            String port,
            boolean auth,
            String protocol) {

        super(id, reference, name, (String) null);
        this.aliases = aliases;
        this.defaultAlias = defaultAlias;
        this.host = host;
        this.user = user;
        this.password = password;
        if (port != null) {
            this.port = port;
        }
        this.auth = auth;
        if (protocol != null) {
            this.protocol = protocol;
        }
    }

    public String getDefaultAlias() {
        return defaultAlias;
    }

    public void setDefaultAlias(String defaultAlias) {
        this.defaultAlias = defaultAlias;
    }

    public List<String> getAliasList() {
        List<String> list = StringUtil.getTokenList(aliases, StringUtil.COMMA);
        return list;
    }

    public String getAliases() {
        return aliases;
    }

    protected void setAliases(String aliases) {
        this.aliases = aliases;
    }

    public void addAlias(String alias) {
        List<String> list = getAliasList();
        boolean added = false;
        for (int i = 0, j = list.size(); i < j; i++) {
            String next = list.get(i);
            if (next.equalsIgnoreCase(alias)) {
                added = true;
                break;
            }
        }
        if (!added) {
            list.add(alias);
            this.aliases = StringUtil.createCommaSeparated(list);
        }
    }

    public void removeAlias(String alias) {
        List<String> list = getAliasList();
        for (Iterator<String> i = list.iterator(); i.hasNext();) {
            String next = i.next();
            if (next.equalsIgnoreCase(alias)) {
                i.remove();
                break;
            }
        }
        this.aliases = StringUtil.createCommaSeparated(list);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getAttachmentsStore() {
        return attachmentsStore;
    }

    public void setAttachmentsStore(String attachmentsStore) {
        this.attachmentsStore = attachmentsStore;
    }

    public String getInboxName() {
        return inboxName;
    }

    public void setInboxName(String inboxName) {
        this.inboxName = inboxName;
    }

    public String getInboxArchiveName() {
        return inboxArchiveName;
    }

    public void setInboxArchiveName(String inboxArchiveName) {
        this.inboxArchiveName = inboxArchiveName;
    }

    public String getInboxDraftsName() {
        return inboxDraftsName;
    }

    public void setInboxDraftsName(String inboxDraftsName) {
        this.inboxDraftsName = inboxDraftsName;
    }

    public String getInboxFailedName() {
		return inboxFailedName;
	}

	public void setInboxFailedName(String inboxFailedName) {
		this.inboxFailedName = inboxFailedName;
	}

	public String getInboxSentName() {
        return inboxSentName;
    }

    public void setInboxSentName(String inboxSentName) {
        this.inboxSentName = inboxSentName;
    }

    public String getInboxSpamName() {
        return inboxSpamName;
    }

    public void setInboxSpamName(String inboxSpamName) {
        this.inboxSpamName = inboxSpamName;
    }

    public String getInboxTrashName() {
        return inboxTrashName;
    }

    public void setInboxTrashName(String inboxTrashName) {
        this.inboxTrashName = inboxTrashName;
    }

    public String[] getImapFolders() {
        return new String[] {
                getInboxName(),
                getInboxArchiveName(),
                getInboxDraftsName(),
                getInboxFailedName(),
                getInboxSentName(),
                getInboxSpamName(),
                getInboxTrashName()
        };
    }

    public String getSocketFactoryClass() {
        return socketFactoryClass;
    }

    public void setSocketFactoryClass(String socketFactoryClass) {
        this.socketFactoryClass = socketFactoryClass;
    }

    public Properties getMailProperties() {
        Properties props = new Properties();
        if (isSet(host)) {
            props.setProperty("mail." + protocol + ".host", host);
        }
        if (isSet(user)) {
            props.setProperty("mail." + protocol + ".user", user);
        }
        if (isSet(password)) {
            props.setProperty("mail." + protocol + ".password", password);
        }
        if (isSet(port)) {
            props.setProperty("mail." + protocol + ".port", port);
        }
        //props.setProperty("mail." + protocol + ".auth", Boolean.valueOf(auth).toString());
        if (isSet(socketFactoryClass)) {
            props.setProperty("mail." +  protocol + ".socketFactory.class", socketFactoryClass);
        }
        return props;
    }

    @Override
    public Object clone() {
        throw new UnsupportedOperationException("clone method not implemented yet");
    }
}
