/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 21, 2011 2:35:47 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.Date;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateUtil;
import com.osserp.core.contacts.ZipcodeSearch;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.ZipcodeSearches;
import com.osserp.core.model.ZipcodeSearchImpl;

/**
 * 
 * @author jg <jg@osserp.com>
 */
public class ZipcodeSearchesDao extends AbstractRepository implements ZipcodeSearches {

    private String zipcodeSearchesTable = null;

    public ZipcodeSearchesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        this.zipcodeSearchesTable = getTable(TableKeys.ZIPCODE_SEARCHES);
    }

    public ZipcodeSearch load(Long id) {
        return (ZipcodeSearch) getCurrentSession().load(ZipcodeSearchImpl.class.getName(), id);
    }

    public void save(ZipcodeSearch zipcodeSearch) {
        getCurrentSession().saveOrUpdate(ZipcodeSearch.class.getName(), zipcodeSearch);
    }

    public Integer countTodaysSearches() {
        Date dayStart = DateUtil.getCurrentDate();
        dayStart = DateUtil.resetTime(dayStart);
        Date dayEnd = DateUtil.addHours(dayStart, 24);
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT count(*) FROM ")
                .append(zipcodeSearchesTable)
                .append(" WHERE created  BETWEEN ? AND  ?");
        final Object[] params = { dayStart, dayEnd };
        final int[] types = { Types.TIMESTAMP, Types.TIMESTAMP };
        return jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
    }

}
