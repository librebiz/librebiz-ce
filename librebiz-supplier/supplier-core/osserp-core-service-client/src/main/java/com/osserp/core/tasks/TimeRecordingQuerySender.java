/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 15, 2010 9:16:15 AM 
 * 
 */
package com.osserp.core.tasks;

/**
 * 
 * @author jg <jg@osserp.com>
 * 
 */
public interface TimeRecordingQuerySender {

    /**
     * Sends a query request for the given date and email and query type
     * @param date
     * @param email
     * @param type
     */
    void send(String date, String mail, Long typeId);
}
