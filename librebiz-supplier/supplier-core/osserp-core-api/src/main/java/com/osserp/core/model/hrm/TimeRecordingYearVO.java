/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 8, 2008 11:58:15 AM 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.CalendarMonth;
import com.osserp.common.Month;
import com.osserp.common.beans.AbstractYear;
import com.osserp.common.util.DateUtil;
import com.osserp.core.hrm.TimeRecordingMonth;
import com.osserp.core.hrm.TimeRecordingYear;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingYearVO extends AbstractYear implements TimeRecordingYear {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingYearVO.class.getName());

    private boolean first = false;
    private int carryoverMinutes;

    private int annualMinutesLeave;
    private int carryoverMinutesLeave;
    private int consumedMinutesLeave;
    private int expiredMinutesLeave;
    private int remainingMinutesLeave;
    private int minutesLost;

    private TimeRecordingPeriodImpl period = null;
    private List<TimeRecordingMonth> supportedMonths = new ArrayList<TimeRecordingMonth>();
    private TimeRecordingMonth selectedMonth = null;

    protected TimeRecordingYearVO() {
        super();
    }

    protected TimeRecordingYearVO(
            TimeRecordingPeriodImpl period,
            Integer year,
            int carryoverMinutes,
            int carryoverLeaveMinutes,
            int annualLeaveMinutes,
            int previousMinutesLost) {
        super(year);
        if (log.isDebugEnabled()) {
            log.debug("<init> invoked [recording=" + period.getReference()
                    + ", period=" + period.getId()
                    + ", year=" + year
                    + ", carryoverMinutes=" + carryoverMinutes
                    + ", carryoverLeaveMinutes=" + carryoverLeaveMinutes
                    + ", annualLeaveMinutes=" + annualLeaveMinutes
                    + ", previousMinutesLost=" + previousMinutesLost
                    + "]");
        }
        long start = System.currentTimeMillis();
        this.period = period;
        CalendarMonth validFrom = DateUtil.createMonth(period.getValidFrom());
        CalendarMonth validTil = null;
        if (period.getValidTil() == null) {
            Date validTilDate = DateUtil.addYears(DateUtil.getCurrentDate(), 1);
            validTil = DateUtil.createMonth(validTilDate);
        } else {
            validTil = DateUtil.createMonth(period.getValidTil());
        }
        if (validFrom.getYear().equals(year)) {
            this.first = true;
        }
        this.consumedMinutesLeave = 0;
        this.expiredMinutesLeave = 0;
        this.carryoverMinutes = carryoverMinutes;
        this.minutesLost = previousMinutesLost;

        this.carryoverMinutesLeave = carryoverLeaveMinutes;
        this.annualMinutesLeave = annualLeaveMinutes;
        this.remainingMinutesLeave = carryoverMinutesLeave + annualMinutesLeave - this.consumedMinutesLeave - this.expiredMinutesLeave;
        int tempCarryoverMinutesLeave = carryoverLeaveMinutes;

        List<CalendarMonth> months = createMonths();
        for (int i = 0; i < 12; i++) {
            CalendarMonth next = months.get(i);
            if (!next.isBefore(validFrom) && !next.isAfter(validTil)) {
                TimeRecordingMonth month = new TimeRecordingMonthVO(this, next, carryoverMinutes, minutesLost);

                this.carryoverMinutes = month.getMinutesCarryover();
                this.minutesLost = month.getMinutesLost();

                this.consumedMinutesLeave = consumedMinutesLeave + month.getMinutesLeave();

                int markerMinutes = month.getMinutesLeaveMarker();

                if (tempCarryoverMinutesLeave > markerMinutes) {
                    this.expiredMinutesLeave = expiredMinutesLeave + tempCarryoverMinutesLeave - markerMinutes;
                    tempCarryoverMinutesLeave = markerMinutes;
                }
                if (tempCarryoverMinutesLeave > month.getMinutesLeave()) {
                    tempCarryoverMinutesLeave -= month.getMinutesLeave();
                } else {
                    tempCarryoverMinutesLeave = 0;
                }

                this.remainingMinutesLeave = carryoverMinutesLeave + annualMinutesLeave - this.consumedMinutesLeave - this.expiredMinutesLeave;
                month.setRemainingMinutesLeave(remainingMinutesLeave);
                month.setExpiredMinutesLeave(expiredMinutesLeave);
                this.supportedMonths.add(month);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("<init> invoked [recording=" + period.getReference()
                    + ", period=" + period.getId()
                    + ", year=" + year
                    + ", duration=" + (System.currentTimeMillis() - start)
                    + "ms]");
        }
    }

    public boolean isCurrent() {
        return Integer.valueOf(DateUtil.getCurrentYear()).equals(getYear());
    }

    public boolean isFirst() {
        return first;
    }

    public int getMinutesLost() {
        return minutesLost;
    }

    public List<TimeRecordingMonth> getSupportedMonths() {
        return supportedMonths;
    }

    public TimeRecordingMonth getSelectedMonth() {
        return selectedMonth;
    }

    public void selectMonth(Month month) {
        if (month != null) {
            for (int i = 0, j = supportedMonths.size(); i < j; i++) {
                TimeRecordingMonth next = supportedMonths.get(i);
                if (next.equals(month)) {
                    this.selectedMonth = next;
                    break;
                }
            }
        } else {
            if (!supportedMonths.isEmpty()) {
                this.selectedMonth = supportedMonths.get(supportedMonths.size() - 1);
            }
        }
    }

    public TimeRecordingMonth fetchMonth(Month month) {
        for (int i = 0, j = supportedMonths.size(); i < j; i++) {
            TimeRecordingMonth next = supportedMonths.get(i);
            if (next.equals(month)) {
                return next;
            }
        }
        return null;
    }

    public int getCarryoverMinutes() {
        return carryoverMinutes;
    }

    public int getConsumedMinutesLeave() {
        return consumedMinutesLeave;
    }

    public int getCarryoverMinutesLeave() {
        return carryoverMinutesLeave;
    }

    public double getCarryoverLeave() {
        return carryoverMinutesLeave / this.period.getConfig().getHoursDaily() / 60;
    }

    public int getRemainingMinutesLeave() {
        return remainingMinutesLeave;
    }

    public double getAnnualLeave() {
        return annualMinutesLeave / this.period.getConfig().getHoursDaily() / 60;
    }

    public double getExpiredLeave() {
        return expiredMinutesLeave / this.period.getConfig().getHoursDaily() / 60;
    }

    public double getConsumedLeave() {
        return consumedMinutesLeave / this.period.getConfig().getHoursDaily() / 60;
    }

    public double getRemainingLeave() {
        return remainingMinutesLeave / this.period.getConfig().getHoursDaily() / 60;
    }

    /**
     * Not necessary yet. When needed some changes are required in this methods
     */

    /**
     * public Element getXml(boolean listAll) { Element root = new Element("timeRecordingYear"); root.addContent(new Element("annualLeave").setText(
     * NumberFormatter.getValue(period.getConfig().getAnnualLeave(), NumberFormatter.CURRENCY))); root.addContent(new Element("carroverLeave").setText(
     * NumberFormatter.getValue(getCarryoverLeave(), NumberFormatter.CURRENCY))); root.addContent(new Element("remainingLeave").setText(
     * NumberFormatter.getValue(getRemainingLeave(), NumberFormatter.CURRENCY))); if (listAll) { Element list = new Element("timeRecordingMonths"); for
     * (int i = 0, j = supportedMonths.size(); i < j; i++) { list.addContent(supportedMonths.get(i).getXml()); } root.addContent(list); } else if
     * (selectedMonth != null) { root.addContent(selectedMonth.getXml()); } return root; }
     */

    protected TimeRecordingPeriodImpl getPeriod() {
        return period;
    }
}
