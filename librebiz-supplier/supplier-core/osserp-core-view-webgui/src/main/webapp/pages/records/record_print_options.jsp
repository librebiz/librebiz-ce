<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="recordView" value="${sessionScope.recordView}" />
<c:set var="record" value="${recordView.record}" />
<c:if test="${!record.historical}">
    <tr>
        <td class="boldtext"><fmt:message key="printOptions" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><fmt:message key="printOptionsGlobalDefaultsLabel" /></td>
        <td align="right"><input type="checkbox" name="printOptionTypeDefaults" title="printOptionsGlobalDefaultsLabel" /></td>
    </tr>
    <tr>
        <td><fmt:message key="businessCaseIdLabel" /></td>
        <td align="right"><v:checkbox name="printBusinessCaseId" value="${record.printBusinessCaseId}" title="businessCaseIdLabel" /></td>
    </tr>
    <tr>
        <td><fmt:message key="businessCaseInfoLabel" /></td>
        <td align="right"><v:checkbox name="printBusinessCaseInfo" value="${record.printBusinessCaseInfo}" title="businessCaseInfoLabel" /></td>
    </tr>
    <tr>
        <td><fmt:message key="recordDate" /></td>
        <td align="right"><v:checkbox name="printRecordDate" value="${record.printRecordDate}" title="recordDate" /></td>
    </tr>
    <tr>
        <td><fmt:message key="recordDateByStatus" /></td>
        <td align="right"><v:checkbox name="printRecordDateByStatus" value="${record.printRecordDateByStatus}" title="recordDateByStatus" /></td>
    </tr>
    <tr>
        <td><fmt:message key="salesEmployee" /></td>
        <td align="right"><v:checkbox name="printSalesperson" value="${record.printSalesperson}" title="salesEmployee" /></td>
    </tr>
    <tr>
        <td><fmt:message key="responsible" /></td>
        <td align="right"><v:checkbox name="printProjectmanager" value="${record.printProjectmanager}" title="responsible" /></td>
    </tr>
    <tr>
        <td><fmt:message key="paymentTargetPrint" /></td>
        <td align="right"><v:checkbox name="printPaymentTarget" value="${record.printPaymentTarget}" title="paymentTargetPrint" /></td>
    </tr>
    <tr>
        <td><fmt:message key="complimentaryClose" /></td>
        <td align="right"><v:checkbox name="printComplimentaryClose" value="${record.printComplimentaryClose}" title="complimentaryClose" /></td>
    </tr>
    <c:if test="${!empty recordView.embeddedTextAbove}">
        <tr>
            <td><fmt:message key="printCoverLetter" /></td>
            <td align="right"><v:checkbox name="printCoverLetter" value="${record.printCoverLetter}" title="printCoverLetter" /></td>
        </tr>
    </c:if>
    <c:if test="${recordView.documentImportable}">
        <tr>
            <td><fmt:message key="externalDocument" /></td>
            <td align="right"><v:checkbox name="historical" value="${record.historical}" title="externalDocument" /></td>
        </tr>
    </c:if>
</c:if>
