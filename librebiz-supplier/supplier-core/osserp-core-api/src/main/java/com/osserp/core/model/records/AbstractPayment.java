/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 20:31:31 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.util.DateUtil;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.Records;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPayment extends AbstractFinanceRecord implements Payment {
    private static Logger log = LoggerFactory.getLogger(AbstractPayment.class.getName());

    private BigDecimal amount = new BigDecimal(Constants.DOUBLE_NULL);
    private BigDecimal amountToPay = new BigDecimal(Constants.DOUBLE_NULL);
    private Date paid;
    private Long recordId;
    private Long recordTypeId;
    private Long bankAccountId;
    private boolean outflowOfCash = false;
    private boolean reducedTax = false;
    private boolean taxPayment = false;
    private Double taxRate = 1d;

    /**
     * Creates an empty payment used by serialization
     */
    protected AbstractPayment() {
        super();
    }

    /**
     * Creates a new payment with required values
     * @param recordToPay
     * @param type
     * @param paid
     * @param user
     * @param amount
     * @param bankAccountId
     */
    protected AbstractPayment(
            Record recordToPay,
            BillingType billingType,
            Date paid,
            Employee user,
            BigDecimal amount,
            Long bankAccountId) {
        super(
                null,
                recordToPay.getCompany(),
                recordToPay.getBranchId(),
                billingType,
                recordToPay.getReference(),
                recordToPay.getContact(),
                user,
                recordToPay.getBusinessCaseId());
        this.bankAccountId = bankAccountId;
        this.amount = amount;
        this.paid = (paid == null ? DateUtil.getCurrentDate() : paid);
        this.recordId = recordToPay.getId();
        this.recordTypeId = recordToPay.getType().getId();
        applyType(billingType);
        setCurrency(recordToPay.getCurrency());
    }

    /**
     * Creates a new payment with required values
     * @param recordToPay
     * @param paid
     * @param customHeader
     * @param note
     * @param user
     * @param amount
     * @param bankAccountId
     */
    protected AbstractPayment(
            Record recordToPay,
            BillingType billingType,
            Date paid,
            String customHeader,
            String note,
            Employee user,
            BigDecimal amount,
            Long bankAccountId) {
        super(
                null,
                recordToPay.getCompany(),
                recordToPay.getBranchId(),
                billingType,
                recordToPay.getReference(),
                recordToPay.getContact(),
                user,
                recordToPay.getBusinessCaseId());
        this.bankAccountId = bankAccountId;
        this.amount = amount;
        this.paid = (paid == null ? DateUtil.getCurrentDate() : paid);
        this.recordId = recordToPay.getId();
        this.recordTypeId = recordToPay.getType().getId();
        setCustomHeader(customHeader);
        setNote(note);
        applyType(billingType);
        setCurrency(recordToPay.getCurrency());
    }

    /**
     * Creates an independent payment with no reference to another record.
     * Use this constructor to persist payments of simple bills. 
     * @param user
     * @param contact
     * @param company
     * @param branch
     * @param currency
     * @param type
     * @param recordCreated
     * @param paid the date the amount was paid. Amount will be stored as amountToPay if empty.
     * @param amount the amount paid or the amount to pay if date paid is not provided.
     * @param note
     * @param reducedTax
     * @param taxFree
     * @param taxFreeId
     * @param taxRate
     */
    protected AbstractPayment(
            Employee user,
            ClassifiedContact contact,
            Long company,
            Long branch,
            Long bankAccountId,
            Long currency,
            BillingType billingType,
            Date recordCreated,
            Date paid,
            BigDecimal amount,
            String note,
            boolean reducedTax,
            boolean taxFree, 
            Long taxFreeId,
            Double taxRate) {
        super(user, contact, company, branch, currency, billingType, recordCreated,
                note, taxFree, taxFreeId);
        this.bankAccountId = bankAccountId;
        this.reducedTax = reducedTax;
        if (isSet(taxRate)) {
            this.taxRate = taxRate;
        }
        updateAmount(amount,paid);
        applyType(billingType);
    }
    
    /**
     * Creates a payment by other payment. Note: Method copies all properties
     * including the primary key. You have to reset the id if object should be 
     * used to create a new persistent record.
     * @param payment
     */
    protected AbstractPayment(Payment payment) {
        super(payment);
        this.amount = payment.getAmount();
        this.amountToPay = payment.getAmountToPay();
        this.paid = payment.getPaid();
        this.recordId = payment.getRecordId();
        this.recordTypeId = payment.getRecordTypeId();
        this.bankAccountId = payment.getBankAccountId();
        this.outflowOfCash = payment.isOutflowOfCash();
        this.reducedTax = payment.isReducedTax();
        this.taxPayment = payment.isTaxPayment();
    }

    /**
     * Updates provided payment data
     * @param user
     * @param billingType
     * @param currency
     * @param amount
     * @param recordDate
     * @param paid
     * @param note
     * @param reducedTax
     * @param taxFree
     * @param taxFreeId
     * @param customHeader
     */
    protected void update(
            Employee user,
            BillingType billingType,
            Long currency,
            BigDecimal amount,
            Date recordDate,
            Date paid,
            String note,
            boolean reducedTax,
            boolean taxFree, 
            Long taxFreeId,
            String customHeader) {
        
        if (billingType != null) {
            setType(billingType);
            applyType(billingType);
        }
        if (isSet(currency)) {
            setCurrency(currency);
        }
        if (recordDate != null) {
            setCreated(recordDate);
        }
        setCustomHeader(customHeader);
        setNote(note);
        setTaxFree(taxFree);
        setTaxFreeId(taxFreeId);
        this.reducedTax = reducedTax;
        updateAmount(amount,paid);
        updateChanged(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
    }
    
    private void applyType(BillingType billingType) {
        if (billingType != null) {
            if (billingType.getBillingClass() != null) {
                outflowOfCash = billingType.getBillingClass().isOutflowOfCash();
            } else {
                log.warn("applyType() invoked with invalid billingType");
            }
            taxPayment = billingType.isTaxPayment();
        } else {
            log.warn("applyType() invoked with missing billingType");
        }
    }
    
    private void updateAmount(BigDecimal amount, Date paid) {
        this.paid = paid;
        if (this.paid == null) {
            this.amountToPay = amount;
        } else {
            this.amount = amount;
            this.amountToPay = amount;
        }
    }

    @Override
    public String getReferenceNumber() {
        String result = Records.createReferenceNumber(getType(), getRecordId(), false);
        if (isNotSet(result)) {
            StringBuilder buff = new StringBuilder();
            if (isSet(getType().getNumberPrefix())) {
                buff = new StringBuilder(getType().getNumberPrefix());
            }
            buff.append(getId());
            result = buff.toString();

        }
        return result;
    }

    public void subtract(BigDecimal cashDiscount) {
        if (amount != null) {
            amount = amount.subtract(cashDiscount);
        } else {
            log.warn("subtract() invocation while amount is null [id=" + getId() + "]");
        }
    }

    public BigDecimal getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(BigDecimal amountToPay) {
        this.amountToPay = amountToPay;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getPaid() {
        return paid;
    }

    public void setPaid(Date paid) {
        this.paid = paid;
    }

    @Override
    public Date getTaxPointDate() {
        return paid;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Long getRecordId() {
        return recordId;
    }
    
    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(Long recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    public boolean isOutflowOfCash() {
        return outflowOfCash;
    }

    public void setOutflowOfCash(boolean outflowOfCash) {
        this.outflowOfCash = outflowOfCash;
    }

    public boolean isReducedTax() {
        return reducedTax;
    }

    public void setReducedTax(boolean reducedTax) {
        this.reducedTax = reducedTax;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public boolean isTaxPayment() {
        return taxPayment;
    }

    public void setTaxPayment(boolean taxPayment) {
        this.taxPayment = taxPayment;
    }

}
