/**
*
* Copyright (C) 2012 The original author or authors.
*
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
* 
* Created on Dec 23, 2012 
* 
*/
package com.osserp.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.PhoneUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.dao.ContactCreator;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Emails;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.model.contacts.ContactImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactCreatorDao extends AbstractContactAwareDao implements ContactCreator {
    private static Logger log = LoggerFactory.getLogger(ContactCreatorDao.class.getName());
    
    private static final String NEWSLETTER_DEFAULT = "contactCreateNewsletterStatus";
    private SystemConfigs systemConfigs;
    

    public ContactCreatorDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Contacts contacts,
            Emails emails,
            OptionsCache namesCache,
            SystemConfigs systemConfigs) {
        super(jdbcTemplate, tables, sessionFactory, contacts, emails, namesCache);
        this.systemConfigs = systemConfigs;
    }

    // consumer contact
    public Contact create(
            Long createdBy,
            Long type,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            Salutation spouseSalutation,
            Option spouseTitle,
            String spouseFirstName,
            String spouseLastName,
            String userSalutation,
            String userNameAddressField,
            Date birthDate,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException {

        Contact obj = create(
                createdBy,
                type,
                salutation,
                title,
                firstName,
                lastName,
                birthDate,
                street,
                streetAddon,
                zipcode,
                city,
                federalState,
                federalStateName,
                country,
                null, // private contacts does not provide website property
                email,
                phoneNumber,
                faxNumber,
                mobileNumber,
                initialStatus);
        obj.updateSpouse(spouseSalutation, spouseTitle, spouseFirstName, spouseLastName);
        obj.setUserSalutation(userSalutation);
        obj.setUserNameAddressField(userNameAddressField);
        obj.setNewsletterConfirmation(newsletterDefaultStatus());
        save(obj);
        return obj;
    }

    // contact person - initial only
    public Contact create(
            Contact contactRef,
            Long createdBy,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office)
            throws ClientException {

        ContactImpl obj = new ContactImpl(
                createdBy,
                getContactType(Contact.TYPE_PERSON),
                contactRef,
                salutation,
                title,
                false,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city,
                federalState,
                federalStateName,
                country,
                null,
                position,
                section,
                office);
        obj.setNewsletterConfirmation(newsletterDefaultStatus());
        save(obj);
        return obj;
    }

    // contact person
    public Contact create(
            Contact contactRef,
            Long createdBy,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            Date birthDate,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber)
            throws ClientException {

        ContactType selectedType = getContactType(Contact.TYPE_PERSON); 
        ContactImpl obj = new ContactImpl(
                createdBy,
                selectedType,
                contactRef,
                salutation,
                title,
                false,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city,
                federalState,
                federalStateName,
                country,
                birthDate,
                position,
                section,
                office);
        obj.setNewsletterConfirmation(newsletterDefaultStatus());
        save(obj);
        createCommunication(obj, email, phoneNumber, faxNumber, mobileNumber);
        return obj;
    }

    // company
    public Contact create(
            Long createdBy,
            Long type,
            String companyAffix,
            String companyName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalStateId,
            String federalStateName,
            Long country,
            String website,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException {
        
        return create(
                createdBy, 
                type, 
                (Salutation) null, 
                (Option) null, // title 
                companyAffix, 
                companyName, 
                (Date) null, // birthdate input not provided by business, sme or freelance create forms 
                street, 
                streetAddon, 
                zipcode, 
                city, 
                federalStateId, 
                federalStateName, 
                country, 
                website, 
                email, 
                phoneNumber,
                faxNumber,
                mobileNumber,
                initialStatus);
    }

    // freelancer or sme
    public Contact create(
            Long createdBy,
            Long type,
            String companyAffix,
            String companyName,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalStateId,
            String federalStateName,
            Long country,
            String website,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException {

        Contact obj = create(
                createdBy,
                type,
                salutation,
                title,
                companyAffix,
                companyName,
                (Date) null, // birthdate input not provided by business, sme or freelance create forms
                street,
                streetAddon,
                zipcode,
                city,
                federalStateId,
                federalStateName,
                country,
                website,
                email,
                phoneNumber,
                faxNumber,
                mobileNumber,
                initialStatus);
        obj.updateSpouse(salutation, title, firstName, lastName);
        obj.setNewsletterConfirmation(newsletterDefaultStatus());
        save(obj);
        return obj;
    }

    // internet form contact
    public Contact create(
            Long createdBy,
            Long type,
            String company,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            Date birthDate,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String email,
            String phone,
            String mobile,
            String fax,
            boolean grantEmail,
            boolean grantPhone,
            boolean grantNone,
            String externalReference,
            String externalUrl) throws ClientException {

        ContactType selectedType = getContactType(type);
        boolean business = selectedType.isBusiness();
        String name = business && isSet(company) ? company :
            createLastNameIfNotProvided(firstName, lastName, email, phone, mobile, fax);
        
        ContactImpl obj = new ContactImpl(
                (!isEmpty(createdBy) ? createdBy : Constants.SYSTEM_EMPLOYEE), 
                selectedType,
                salutation,
                title,
                false,
                business ? null : firstName,
                name,
                street,
                streetAddon,
                zipcode,
                city,
                country,
                null, // federalState,
                null, // federalStateName,
                null,
                birthDate,
                Contact.STATUS_EXTERNAL,
                isValidateExternalEnabled(),
                externalReference,
                externalUrl);
        obj.setNewsletterConfirmation(false);
        obj.updateContactGrants(
                Constants.SYSTEM_EMPLOYEE, 
                grantEmail, 
                null,   // grantEmailNote 
                grantPhone, 
                null,   // grantPhoneNote 
                grantNone, 
                null,   // grantNoneNote
                false,  
                null,   // newsletterConfirmationNote
                false,
                null);  // vipNote
        save(obj);
        obj = createCommunication(obj, email, phone, mobile, fax);
        if (business && isSet(lastName)) {
            create(obj, // contactRef 
                    createdBy, 
                    salutation, 
                    title, 
                    firstName, 
                    lastName, 
                    street, 
                    streetAddon, 
                    zipcode, 
                    city, 
                    null,  // federalState, 
                    null,  // federalStateName, 
                    country, 
                    null,  // position, 
                    null,  // section, 
                    null); // office);
        }
        return obj;
    }

    private Contact create(
            Long createdBy,
            Long typeId,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            Date birthDate,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String website,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException {

        ContactType selectedType = getContactType(typeId);

        ContactImpl obj = new ContactImpl(
                createdBy,
                selectedType,
                salutation,
                title,
                false,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city,
                country,
                federalState,
                federalStateName,
                null,
                birthDate,
                initialStatus,
                true,   // validate
                null,   // externalReference
                null);  // externalUrl
        obj.setNewsletterConfirmation(newsletterDefaultStatus());
        obj.setWebsite(website);
        save(obj);
        createCommunication(obj, email, phoneNumber, faxNumber, mobileNumber);
        return obj;
    }
    
    protected ContactImpl createCommunication(
            ContactImpl obj, 
            String email,
            String phone,
            String mobile,
            String fax) {
        
        obj = addEmail(obj.getPersonCreatedBy(), obj, email);
        if (!isEmpty(phone)) {
            obj = addPhone(obj, phone);
        }
        if (!isEmpty(mobile)) {
            obj = addPhone(obj, mobile);
        }
        if (!isEmpty(fax)) {
            obj = addPhone(obj, fax);
        }
        return obj;
    }
    
    private ContactImpl addEmail(Long createdBy, ContactImpl obj, String email) {
        if (email != null && email.length() > 0) {
            try {
                List<Option> existing = emails.findEqual(email);
                if (existing.isEmpty()) {
                    obj.addEmail(createdBy, getEmailTypeByContactType(obj.getType()), email, true, null);
                    save(obj);
                } else {
                    log.warn("addEmail() ignoring attempt to add existing [contact=" 
                            + obj.getPrimaryKey() + ", email=" + email + "]");
                }
            } catch (Exception c) {
                // we ignore this, user should add this later
            }
        }
        return obj;
    }
    
    private ContactImpl addPhone(ContactImpl obj, String number) {
        String[] pn = !isEmpty(number) ? PhoneUtil.createPhoneNumber(number) : null;
        //  pn: { country, prefix, number, "device" , "validationStatus" }
        if (pn != null) {
            try {
                Long deviceId = ("mobile" == pn[3] ? PhoneType.MOBILE : PhoneType.PHONE);
                boolean primary = getPhoneCount(obj.getContactId(), deviceId) > 0;
                obj.addPhone(
                    obj.getPersonCreatedBy(), 
                    deviceId, 
                    obj.isPrivatePerson() ? Phone.PRIVATE : Phone.BUSINESS, 
                    pn[0], 
                    pn[1], 
                    pn[2], 
                    pn[4], 
                    primary);
                save(obj);
            } catch (ClientException c) {
                log.warn("addPhone() ignoring failure [contact=" 
                    + obj.getPrimaryKey() + ", phone=" + number
                    + ", country=" + pn[0] 
                    + ", prefix=" + pn[1] 
                    + ", number=" + pn[2] 
                    + ", vstatus=" + pn[4] 
                    + "]");
            }
        }            
        return obj;
    }
    
    private String createLastNameIfNotProvided(
            String firstName,
            String lastName,
            String email,
            String phone,
            String mobile,
            String fax) throws ClientException {

        String name = lastName;
        if (isEmpty(name)) {
            if (!isEmpty(firstName)) {
                name = firstName;
            } else if (!isEmpty(email)) {
                name = email;
            } else if (!isEmpty(phone)) {
                name = phone;
            } else if (!isEmpty(mobile)) {
                name = mobile;
            } else if (!isEmpty(fax)) {
                name = fax;
            }
            if (isEmpty(name)) {
                log.warn("createLastNameIfNotProvided() failed on attempt to "
                        + "create a name placeholder:\nWe did not receive a"
                        + " name and did not find any other string to set in "
                        + "place.\nPlease make sure that any of lastName, "
                        + "firstName, email, phone, mobile or fax is set!");
                throw new ClientException(ErrorCode.LASTNAME_MISSING);
            }
        }
        return name;
    }

    public Salutation findSalutation(Long salutationId, String salutationName, boolean createMissing) {
        if (salutationId != null) {
            return getSalutation(salutationId);
        }
        // TODO Add create method if createMissing enabled
        return findSalutation(salutationName);
    }

    public Option findTitle(Long titleId, String titleName, boolean createMissing) {
        if (titleId != null) {
            return getTitle(titleId);
        }
        // TODO Add create method if createMissing enabled
        return findTitle(titleName);
    }

    public Person findByExternalReference(String externalReference) {
        return contacts.findByExternalReference(externalReference);
    }
    
    protected boolean isValidateExternalEnabled() {
        // TODO set by systemProperty
        return true;
    }
    
    private boolean newsletterDefaultStatus() {
        if (systemConfigs != null) {
            return systemConfigs.isSystemPropertyEnabled(NEWSLETTER_DEFAULT);
        }
        return false;
    }

    private void save(Contact obj) {
        getCurrentSession().saveOrUpdate(ContactImpl.class.getName(), obj);
    }

}
