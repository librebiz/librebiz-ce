<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productCreatorView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="styles" type="string">
		<style type="text/css">
		</style>
	</tiles:put>
	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="productCreatorContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.classificationMode}">
                        <c:import url="_productClassificationSelection.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <v:form name="productCreatorForm" url="/products/productCreator/save">
                            <c:import url="_productCreatorForm.jsp"/>
                        </v:form>
                    </c:otherwise>
                </c:choose>
            </div>
		</div>
	</tiles:put>
</tiles:insert>
