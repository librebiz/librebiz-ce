/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 24-Sep-2006 08:55:52 
 * 
 */
package com.osserp.gui.web.sales;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;

import com.osserp.gui.client.sales.SalesPlanningView;
import com.osserp.gui.web.products.AbstractStockSelectingAction;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 */
public class SalesPlanningAction extends AbstractStockSelectingAction {
    private static Logger log = LoggerFactory.getLogger(SalesPlanningAction.class.getName());

    @Override
    protected Class<SalesPlanningView> getViewClass() {
        return SalesPlanningView.class;
    }

    /**
     * Toggles summary mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeSummaryMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeSummaryMode() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        view.changeSummaryMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Refreshs values from backend and forward to open order page
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward redisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("redisplay() request from " + getUser(session).getId());
        }
        SalesPlanningView view = fetchSalesPlanningView(session);
        if (view == null) {
            return forward(mapping, form, request, response);
        }
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to offer listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward refresh(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("refresh() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables product summary display mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableProductSummary(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableProductSummary() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        view.setProductSummaryMode(null);
        if (view.isDisplaySummaryAndExit()) {
            String exitTarget = view.getExitTarget();
            removeView(request.getSession());
            return mapping.findForward(exitTarget);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables product summary listing mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableProductSummary(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableProductSummary() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        Long id = RequestUtil.getId(request);
        view.setProductSummaryMode(id);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables product group specific display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableProductGroup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableProductGroup() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        view.selectProductGroup(null);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Selects an product type to limit display to
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectProductType(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectProductType() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        Long id = RequestUtil.getId(request);
        view.selectProductType(id > -1 ? id : null);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Selects an product group to limit display to
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectProductGroup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectProductGroup() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        Long id = RequestUtil.getId(request);
        view.selectProductGroup(id > -1 ? id : null);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Selects an product category to limit display to
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectProductCategory(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectProductCategory() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        Long id = RequestUtil.getId(request);
        view.selectProductCategory(id > -1 ? id : null);
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward filter(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("filter() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);

        Long categoryId = RequestUtil.fetchLong(request, Form.CATEGORY_ID);
        Long groupId = RequestUtil.fetchLong(request, Form.GROUP_ID);
        Long typeId = RequestUtil.fetchLong(request, Form.TYPE_ID);
        if (categoryId != null) {
            view.selectProductCategory(categoryId);
        } else if (groupId != null) {
            view.selectProductGroup(groupId);
        } else if (typeId != null) {
            view.selectProductType(typeId);
        }

        Long sotckId = RequestUtil.fetchLong(request, Form.STOCK_ID);
        if (sotckId != null) {
            view.selectStock(sotckId);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Limits listig to display entries with open purchase only
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeConfirmedPurchaseOnlyFlag(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeConfirmedPurchaseOnlyFlag() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        view.toggleConfirmedPurchaseOrderOnly();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Limits listig to display entries with open purchase only
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeOpenPurchaseOnlyFlag(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeOpenPurchaseOnlyFlag() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        view.togglePurchaseOrderedOnly();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles vacancy display flag
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleVacancyDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleVacancyDisplay() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        view.toggleVacancyDisplay();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Resets planning horizon
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward resetHorizon(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("resetHorizon() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        view.setPlanningHorizon(null);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates planning horizon
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateHorizon(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateHorizon() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        Date horizon = RequestUtil.getDate(request, "horizon");
        view.setPlanningHorizon(horizon);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to the product planning of an product provided by 'id' param and 'exitTarget' as required
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardProductPlanning(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardProductPlanning() request from " + getUser(session).getId());
        }
        Long productId = RequestUtil.getId(request);
        String exitTarget = RequestUtil.getExit(request);
        SalesPlanningView openOrders = (SalesPlanningView) createView(request);
        openOrders.enableSummaryAndExit(productId, exitTarget);
        return mapping.findForward(openOrders.getActionTarget());
    }

    /**
     * Fetches 'id' param, selects item and stores object in request scope under key 'deliveryNoteItem'.<br/>
     * Forwards to 'deliveryNoteDisplay' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward displayDeliveryNote(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("displayDeliveryNote() request from " + getUser(session).getId());
        }
        SalesPlanningView view = getSalesPlanningView(session);
        Object item = view.fetchItem(RequestUtil.fetchId(request));
        if (item != null) {
            request.setAttribute("deliveryNoteItem", item);
        }
        return mapping.findForward("deliveryNoteDisplay");
    }

    private SalesPlanningView fetchSalesPlanningView(HttpSession session) throws PermissionException {
        return (SalesPlanningView) fetchView(session);
    }

    private SalesPlanningView getSalesPlanningView(HttpSession session) throws PermissionException {
        SalesPlanningView view = fetchSalesPlanningView(session);
        if (view == null) {
            throw new ActionException(ActionException.NO_VIEW_BOUND, SalesPlanningView.class);
        }
        return view;
    }
}
