/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 14, 2006 11:16:03 AM 
 * 
 */
package com.osserp.core.purchasing;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderManager;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.products.Product;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PurchaseOrderManager extends OrderManager {

    /**
     * Creates a new order for a carry over from another order
     * @param user
     * @param order
     * @param carryOver
     * @return new created record
     */
    PurchaseOrder create(Employee user, PurchaseOrder order, List<Item> carryOver);

    /**
     * Creates new purchase order
     * @param user creating new order
     * @param company placing the order
     * @param branchId
     * @param supplier where the order is placed
     * @return new created record
     */
    Order create(Employee user, Long company, Long branchId, ClassifiedContact supplier);

    /**
     * Creates a new order as copy from another order
     * @param user creating new order
     * @param order
     * @return new created record
     */
    Order create(Employee user, PurchaseOrder order);

    /**
     * Creates a new purchase order by a sales order and product selection
     * @param user
     * @param company
     * @param branch
     * @param supplier
     * @param salesOrder
     * @param selectedOrderItems
     * @return new created record
     */
    PurchaseOrder create(
            Employee user,
            SystemCompany company,
            BranchOffice branch,
            ClassifiedContact supplier,
            Order salesOrder,
            List<Item> selectedOrderItems);

    /**
     * Updates the delivery address of the order
     * @param order
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @throws ClientException if any given value is empty or null
     */
    void update(
            Order order,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country) throws ClientException;

    /**
     * updates the delivery condition
     * @param order
     * @param deliveryConditionId
     * @return deliveryCondition
     */
    void updateDeliveryCondition(PurchaseOrder order, Long deliveryConditionId) throws ClientException;

    /**
     * Provides an option list of all suppliers with orders currently open
     * @return currentSuppliers
     */
    List<Option> findCurrentSuppliers();

    /**
     * Provides all open and released purchase orders by product
     * @param product
     * @param descendant
     * @return open orders
     */
    List<Record> findOpenReleased(Product product, boolean descendant);

    /**
     * Provides all open purchase orders with confirmed or unconfirmed status
     * @param confirmed
     * @return openByConfirmation
     */
    List<RecordDisplay> findOpenByConfirmation(boolean confirmed);

    /**
     * Provides open records by referenced contact
     * @param contactId id of related contact
     * @return open orders by contact
     */
    List<RecordDisplay> findOpenByContact(Long contactId);
}
