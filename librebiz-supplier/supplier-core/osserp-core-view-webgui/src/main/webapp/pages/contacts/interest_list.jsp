<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${requestScope.interestView}"/>

<div class="subcolumns">
	<div class="subcolumn">
		<div class="spacer"></div>
		<c:if test="${!empty sessionScope.statusMessage}">
			<div class="recordheader">
				<p><o:out value="${sessionScope.statusMessage}"/></p>
			</div>
			<c:remove var="statusMessage" scope="session"/>
		</c:if>
		<div class="table-responsive table-responsive-default">
			<table class="table table-striped">
				<thead>
					<tr>
						<th style="width: 75px;"><fmt:message key="date"/></th>
						<th style="width: 100px;"><fmt:message key="name"/></th>
						<th style="width: 100px;"><fmt:message key="city"/></th>
						<th style="width: 481px;"><fmt:message key="note"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="contact" items="${view.list}"	varStatus="s">
						<tr>
							<td style="width: 75px;"><o:date value="${contact.personCreated}"/></td>
							<td style="width: 100px;"><a href="<c:url value="/interests.do?method=select&id=${contact.id}"/>"><o:out value="${contact.displayName}"/></a></td>
							<td style="width: 100px;"><o:out value="${contact.address.city}"/></td>
							<td style="width: 481px;"><o:out value="${contact.description}" limit="150"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>