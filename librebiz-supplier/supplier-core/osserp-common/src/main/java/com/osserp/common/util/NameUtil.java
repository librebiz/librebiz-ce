/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 17, 2006 10:17:01 PM 
 * 
 */
package com.osserp.common.util;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class NameUtil extends AbstractValidator {

    public static String create(String firstName, String lastName) {
        StringBuffer name = new StringBuffer(64);
        if (isSet(firstName) && isSet(lastName)) {
            name.append(lastName).append(", ").append(firstName);
        } else if (isSet(firstName)) {
            name.append(firstName);
        } else if (isSet(lastName)) {
            name.append(lastName);
        } else {
            return "";
        }
        return name.toString();
    }

    public static String createInitials(String firstName, String lastName) {
        StringBuffer name = new StringBuffer(64);
        if (isSet(firstName) && isSet(lastName)) {
            name.append(firstName.substring(0, 1).toUpperCase()).append(lastName.substring(0, 1).toUpperCase());
        } else if (isSet(firstName) && firstName.length() > 1) {
            name.append(firstName.substring(0, 2).toUpperCase());
        } else if (isSet(firstName)) {
            name.append(firstName.substring(0, 1).toUpperCase());
        } else if (isSet(lastName) && lastName.length() > 1) {
            name.append(lastName.substring(0, 2).toUpperCase());
        } else if (isSet(lastName)) {
            name.append(lastName.substring(0, 1).toUpperCase());
        } else {
            return "";
        }
        return name.toString();
    }

    public static String createAccountName(String prefix, String firstname, String lastname, Long id) {
        assert prefix != null && id != null;
        StringBuilder result = new StringBuilder(prefix);
        if (isSet(firstname)) {
            result.append(Character.toLowerCase(
                    StringUtil.replaceUmlauts(firstname.substring(0, 1)).charAt(0)));
        }
        if (isSet(lastname)) {
            result.append(Character.toLowerCase(
                    StringUtil.replaceUmlauts(lastname.substring(0, 1)).charAt(0)));
        }
        result.append(id);
        return result.toString();
    }
}
