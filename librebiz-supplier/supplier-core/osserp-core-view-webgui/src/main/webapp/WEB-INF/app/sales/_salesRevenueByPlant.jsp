<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.salesRevenueView}" />
<c:set var="revenue" value="${view.bean}" />
<table>
	<tr>
		<td class="caption"><fmt:message key="turnover" /></td>
		<td class="a"><o:number value="${revenue.salesVolume}" format="currency" /></td>
		<td style="width: 30px;">&euro;</td>
		<td class="a"><o:number value="${revenue.salesVolumePercent}" format="currency" /></td>
		<td style="width: 30px;">%</td>
		<td style="width: 250px;"><fmt:message key="totalPriceByOrder" /></td>
	</tr>

	<tr>
		<td class="caption"><fmt:message key="costsAProductsLabel" /></td>
		<td class="a err">-<o:number value="${revenue.hardwareCostsA}" format="currency" /></td>
		<td>&euro;</td>
		<td class="a err">-<o:number value="${revenue.hardwareCostsAPercent}" format="currency" /></td>
		<td>%</td>
		<td> </td>
	</tr>

	<tr>
		<td class="caption"><fmt:message key="costsBProductsLabel" /></td>
		<td class="a err">-<o:number value="${revenue.hardwareCostsB}" format="currency" /></td>
		<td>&euro;</td>
		<td class="a err">-<o:number value="${revenue.hardwareCostsBPercent}" format="currency" /></td>
		<td>%</td>
		<td> </td>
	</tr>

	<tr>
		<td class="caption"><fmt:message key="costsCProductsLabel" /></td>
		<td class="a err">-<o:number value="${revenue.hardwareCostsC}" format="currency" /></td>
		<td>&euro;</td>
		<td class="a err">-<o:number value="${revenue.hardwareCostsCPercent}" format="currency" /></td>
		<td>%</td>
		<td> </td>
	</tr>

	<tr>
		<td class="caption"><fmt:message key="costsAccessoryLabel" /></td>
		<td class="a err">-<o:number value="${revenue.accessoryCosts}" format="currency" /></td>
		<td>&euro;</td>
		<td class="a err">-<o:number value="${revenue.accessoryCostsPercent}" format="currency" /></td>
		<td>%</td>
		<td> </td>
	</tr>

	<tr>
		<td class="caption"><fmt:message key="costsServiceLabel" /></td>
		<td class="a err">-<o:number value="${revenue.serviceCosts}" format="currency" /></td>
		<td>&euro;</td>
		<td class="a err">-<o:number value="${revenue.serviceCostsPercent}" format="currency" /></td>
		<td>%</td>
		<td> </td>
	</tr>

	<tr>
		<td class="caption" style="border-bottom: 1px solid black; margin-top: 20px;" colspan="6">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="6">&nbsp;</td>
	</tr>
	<tr>
		<td class="caption"><fmt:message key="profitMargin1" /></td>
		<c:choose>
			<c:when test="${revenue.profit < 0}">
				<td class="a err" style="font-weight: bold;"><o:number value="${revenue.profit}" format="currency" /></td>
			</c:when>
			<c:otherwise>
				<td class="a"><o:number value="${revenue.profit}" format="currency" /></td>
			</c:otherwise>
		</c:choose>
		<td>&euro;</td>
		<c:choose>
			<c:when test="${revenue.profit < 0}">
				<td class="a err" style="font-weight: bold;"><o:number value="${revenue.profitPercent}" format="currency" /></td>
			</c:when>
			<c:otherwise>
				<td class="a"><o:number value="${revenue.profitPercent}" format="currency" /></td>
			</c:otherwise>
		</c:choose>
		<td>%</td>
		<td>&nbsp;</td>
	</tr>
	<c:if test="${(!empty revenue.salesCosts and revenue.salesCosts > 0) or (!empty revenue.agentCosts and revenue.agentCosts > 0) or (!empty revenue.tipCosts and revenue.tipCosts > 0)}">
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td class="caption"	style="border-bottom: 1px solid black; margin-top: 20px;" colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>

		<c:if test="${!empty revenue.salesCosts and revenue.salesCosts > 0}">
			<tr>
				<td class="caption"><fmt:message key="salesCostsLabel" /></td>
				<td class="a err">-<o:number value="${revenue.salesCosts}" format="currency" /></td>
				<td>&euro;</td>
				<td class="a err">-<o:number value="${revenue.salesCostsPercent}" format="currency" /></td>
				<td>%</td>
				<td><fmt:message key="salesCommission" /></td>
			</tr>
		</c:if>
		<c:if test="${!empty revenue.agentCosts and revenue.agentCosts > 0}">
			<tr>
				<td class="caption"><fmt:message key="procurement" /></td>
				<td class="a err">-<o:number value="${revenue.agentCosts}" format="currency" /></td>
				<td>&euro;</td>
				<td class="a err">-<o:number value="${revenue.agentCostsPercent}" format="currency" /></td>
				<td>%</td>
				<td>&nbsp;</td>
			</tr>
		</c:if>
		<c:if test="${!empty revenue.tipCosts and revenue.tipCosts > 0}">
			<tr>
				<td class="caption"><fmt:message key="tipCommission" /></td>
				<td class="a err">-<o:number value="${revenue.tipCosts}" format="currency" /></td>
				<td>&euro;</td>
				<td class="a err">-<o:number value="${revenue.tipCostsPercent}" format="currency" /></td>
				<td>%</td>
				<td>&nbsp;</td>
			</tr>
		</c:if>

		<tr>
			<td class="caption" style="border-bottom: 1px solid black; margin-top: 20px;" colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td class="caption"><fmt:message key="profitMargin2" /></td>
			<c:choose>
				<c:when test="${revenue.totalProfit < 0}">
					<td class="a err" style="font-weight: bold;"><o:number value="${revenue.totalProfit}" format="currency" /></td>
				</c:when>
				<c:otherwise>
					<td class="a"><o:number value="${revenue.totalProfit}" format="currency" /></td>
				</c:otherwise>
			</c:choose>
			<td>&euro;</td>
			<c:choose>
				<c:when test="${revenue.totalProfit < 0}">
					<td class="a err" style="font-weight: bold;"><o:number value="${revenue.totalProfitPercent}" format="currency" /></td>
				</c:when>
				<c:otherwise>
					<td class="a"><o:number value="${revenue.totalProfitPercent}" format="currency" /></td>
				</c:otherwise>
			</c:choose>
			<td>%</td>
			<td>&nbsp;</td>
		</tr>
	</c:if>
</table>
