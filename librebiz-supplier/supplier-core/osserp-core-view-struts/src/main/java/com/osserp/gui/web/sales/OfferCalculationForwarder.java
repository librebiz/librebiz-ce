/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 22, 2006 1:58:54 PM 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.gui.client.calc.CalculationView;
import com.osserp.gui.client.sales.SalesOfferView;
import com.osserp.gui.web.records.AbstractRecordForwarder;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OfferCalculationForwarder extends AbstractRecordForwarder {
    private static Logger log = LoggerFactory.getLogger(OfferCalculationForwarder.class.getName());

    @Override
    protected Class<SalesOfferView> getViewClass() {
        return SalesOfferView.class;
    }

    /**
     * Forwards to offer recalculation
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("execute() requested");
        }
        SalesOfferView view = (SalesOfferView) getRecordView(session);
        CalculationView cview = (CalculationView) createView(request, CalculationView.class.getName());
        cview.init(view.getCalculation());
        return super.execute(mapping, form, request, response);
    }
}
