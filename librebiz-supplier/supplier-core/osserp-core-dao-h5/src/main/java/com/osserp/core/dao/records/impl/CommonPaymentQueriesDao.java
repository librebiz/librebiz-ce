/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 18, 2016 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.CommonPaymentQueries;
import com.osserp.core.finance.CommonPaymentSummary;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CommonPaymentQueriesDao  extends AbstractDao implements CommonPaymentQueries {

    private OptionsCache options = null;
    private String commonRecordSummaryView;

    /**
     * Creates a new query dao for common payemnts
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param options
     */
    public CommonPaymentQueriesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache options) {
        super(jdbcTemplate, tables, sessionFactory);
        this.options = options;
        commonRecordSummaryView = getTable("commonRecordSummaryView");
    }
    
    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.CommonPaymentQueries#getSummary(com.osserp.core.suppliers.Supplier)
     */
    public List<CommonPaymentSummary> getSummary(Supplier supplier) {
        StringBuilder sql = new StringBuilder(CommonPaymentSummaryRowMapper.SELECT_FROM);
        sql.append(commonRecordSummaryView).append(" WHERE supplier = ? ORDER BY created desc");
        final Object[] params = { supplier.getId() };
        final int[] types = { Types.BIGINT };
        return (List<CommonPaymentSummary>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                CommonPaymentSummaryRowMapper.getReader(options));
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.AccountingRecordQueries#getSummaryByDate(java.lang.Long, java.util.Date, java.util.Date)
     */
    public List<CommonPaymentSummary> getSummaryByDate(Long company, Date startDate, Date stopDate) {
        assert startDate != null && stopDate != null;
        StringBuilder sql = new StringBuilder(CommonPaymentSummaryRowMapper.SELECT_FROM);
        sql.append(commonRecordSummaryView)
            .append(" WHERE company = ? AND created >= ? AND created <= ? ORDER BY created desc");
        final Object[] params = { company, startDate, stopDate };
        final int[] types = { Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP };
        return (List<CommonPaymentSummary>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                CommonPaymentSummaryRowMapper.getReader(options));
    }
}
