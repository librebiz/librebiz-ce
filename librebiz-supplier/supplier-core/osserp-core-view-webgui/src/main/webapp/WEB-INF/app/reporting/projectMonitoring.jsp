<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:userContext/>
<o:resource/>
<v:view viewName="projectMonitoringView"/>

<%-- SET PARAMS TO USE IN TAGS --%>
<c:set var="employee">
    <fmt:message key="employee"/>
</c:set>
<c:set var="displayContactData">
    <fmt:message key="displayContactData"/>
</c:set>
<c:choose>
    <c:when test="${view.employeeMode}">
        <c:set var="exitTarget" value="projectMonitoringEmployeeForward"/>
    </c:when>
    <c:otherwise>
        <c:set var="exitTarget" value="projectMonitoring"/>
    </c:otherwise>
</c:choose>
<%-- SET PARAMS TO USE IN TAGS --%>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
        .table tr.vacant {
        background-color: #FBC1C5;
        }
       </style>
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="projectMonitoring"/>
        [<fmt:message key="projectMonitoringCount"/>: <o:listSize value="${view.list}"/> /
        <fmt:message key="projectMonitoringTotalPower"/>: <o:number value="${view.totalCapacity}"/> Euro /
        <fmt:message key="projectMonitoringAveragePower"/>: <o:number value="${view.averageCapacity}"/> Euro]
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="projectMonitoringContent">
            <div class="row">
                <div class="col-lg-12 panel-area">

                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="center"><fmt:message key="type" /></th>
                                    <th><v:sortLink key="id"><fmt:message key="number"/></v:sortLink></th>
                                    <th><v:sortLink key="name"><fmt:message key="commission"/></v:sortLink></th>
                                    <th class="right"><v:sortLink key="capacity"><fmt:message key="value"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="branchKey"><fmt:message key="branchOfficeShortcut"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="salesKey"><fmt:message key="salesShortcut"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="managerKey"><fmt:message key="projectorShort"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="created"><fmt:message key="order"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="verifyDate"><fmt:message key="calculationCreatedShort"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="confirmationDate"><fmt:message key="ok"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="releaseDate"><fmt:message key="orderConfirmationShort"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="deliveryDate"><fmt:message key="deliveryDateShort"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="installationDate"><fmt:message key="installationDateShort"/></v:sortLink></th>
                                    <th class="status right"><v:sortLink key="status">%</v:sortLink></th>
                                    <th class="center" colspan="4"><fmt:message key="info"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:if test="${empty view.list}">
                                    <td colspan="18" class="left"><fmt:message key="noOrdersFound"/> - <v:link url="/sales/salesSearch/forward"><fmt:message key="search"/></v:link></td>
                                </c:if>
                                <c:forEach var="project" items="${view.list}">
                                    <tr id="sales${project.id}"<c:if test="${project.vacant}"> class="vacant"</c:if>>
                                        <td class="center"><o:out value="${project.type.key}" /></td>
                                        <td><a href="<c:url value="/loadSales.do?id=${project.id}&exit=${exitTarget}&exitId=sales${project.id}"/>"><o:out value="${project.id}"/></a></td>
                                        <td><a href="<c:url value="/loadSales.do?id=${project.id}&exit=${exitTarget}&exitId=sales${project.id}"/>"><o:out value="${project.name}" limit="35"/></a></td>
                                        <td class="right"><o:number value="${project.capacity}" format="currency"/></td>
                                        <td class="center"><o:out limit="4" value="${project.branch.shortkey}"/></td>
                                        <td class="center">
                                            <c:choose>
                                                <c:when test="${!empty project.salesId and project.salesId > 0}">
                                                    <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${project.salesId}"><oc:employee initials="true" value="${project.salesId}" /></v:ajaxLink>
                                                </c:when>
                                                <c:otherwise>
                                                    &nbsp;
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td class="center">
                                            <c:choose>
                                                <c:when test="${!empty project.managerId and project.managerId > 0}">
                                                    <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${project.managerId}"><oc:employee initials="true" value="${project.managerId}" /></v:ajaxLink>
                                                </c:when>
                                                <c:otherwise>
                                                    &nbsp;
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td class="center"><o:date value="${project.created}" casenull="-" addcentury="false"/></td>
                                        <td class="center"><o:date value="${project.verifyDate}" casenull="-" format="noyear"/></td>
                                        <td class="center"><o:date value="${project.confirmationDate}" casenull="-" format="noyear"/></td>
                                        <td class="center"><o:date value="${project.releaseDate}" casenull="-" format="noyear"/></td>
                                        <td class="center"><o:date value="${project.deliveryDate}" casenull="-" format="noyear"/></td>
                                        <td class="center"><o:date value="${project.installationDate}" casenull="-" format="noyear"/></td>
                                        <td class="status right">
                                            <v:ajaxLink url="/sales/flowControlDisplay/forward?view=${view.name}&id=${project.id}" styleClass="boldtext" linkId="flowControlDisplayView">
                                                <o:out value="${project.status}"/>
                                            </v:ajaxLink>
                                        </td>
                                        <td class="icon right">
                                        <o:permission role="notes_sales_deny" info="permissionNotes">
                                                <v:ajaxLink url="/sales/businessCaseDisplay/forward?notes=true&id=${project.id}" linkId="businessCaseDisplayView">
                                                    <o:img name="texteditIcon"/>
                                                </v:ajaxLink>
                                            </o:permission>
                                        </td>
                                        <td class="icon right">
                                            <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${project.id}" linkId="businessCaseDisplayView">
                                                <o:img name="openFolderIcon"/>
                                            </v:ajaxLink>
                                        </td>
                                        <td class="icon right">
                                            <c:choose>
                                                <c:when test="${!project.released}">
                                                    <span title="<fmt:message key="orderNotReleased"/>"><o:img name="importantDisabledIcon"/></span>
                                                </c:when>
                                                <c:when test="${project.deliveryClosed}">
                                                    <span title="<fmt:message key="materialPlanningFinished"/>"><o:img name="importantDisabledIcon"/></span>
                                                </c:when>
                                                <c:otherwise>
                                                    <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${project.id}" linkId="deliveryStatus" title="materialPlanning">
                                                        <o:img name="importantIcon"/>
                                                    </v:ajaxLink>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td class="icon right">
                                            <c:choose>
                                                <c:when test="${project.appointmentAvailable}">
                                                    <a href="<c:url value="/loadSales.do?id=${project.id}&exit=${exitTarget}&exitId=sales${project.id}"/>">
                                                        <o:img name="clockIcon"/>
                                                    </a>
                                                </c:when>
                                                <c:otherwise>
                                                    <v:ajaxLink url="/events/appointmentCreator/forward?id=${project.id}&type=5&view=projectMonitoringView" linkId="appointmentCreator"><o:img name="bellIcon"/></v:ajaxLink>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
