<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="supplierSelection" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="supplierId"><fmt:message key="id" /></th>
                        <th class="supplierName"><fmt:message key="company" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="contact" items="${view.supplierPreselection}" varStatus="s">
                        <tr>
                            <td><o:out value="${contact.id}" /></td>
                            <td valign="top"><v:link url="/purchasing/purchaseInvoiceCreator/selectSupplier?id=${contact.id}">
                                    <o:out value="${contact.displayName}" />
                                </v:link></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
