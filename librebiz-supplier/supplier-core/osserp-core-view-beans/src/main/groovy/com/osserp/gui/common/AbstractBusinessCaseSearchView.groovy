/**
 *
 * Copyright (C) 2008, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 6, 2016 
 * 
 */
package com.osserp.gui.common

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.BusinessCaseSearchRequest
import com.osserp.core.BusinessType
import com.osserp.core.BusinessTypeManager


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractBusinessCaseSearchView extends AbstractSearchView {
    private static Logger logger = LoggerFactory.getLogger(AbstractBusinessCaseSearchView.class.getName())
    
    List<BusinessType> requestTypes = []
    boolean renderBranch
    boolean renderManager
    boolean renderSalesPerson = true
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            
            requestTypes = businessTypeManager.createSearchSelection(
                domainUser, includeRequestOnlyBusinessType)
            branchOfficeList = systemConfigManager.getBranchs(domainEmployee)
            reload()
        }
    }

    @Override
    protected String getInitialSearchPattern() {
        return null
    }

    protected BusinessCaseSearchRequest createSearchRequest() {
        if (searchRequest) {
            return new BusinessCaseSearchRequest(
                    searchRequest,
                    getString('column'),
                    getString('value'),
                    getBoolean('startsWith'),
                    getBoolean('includeCanceled'),
                    getBoolean('openOnly'),
                    getLong('branch'),
                    getLong('businessType'))
        }
        return new BusinessCaseSearchRequest(
                domainUser,
                getString('column'),
                getString('value'),
                getBoolean('startsWith'),
                getBoolean('includeCanceled'),
                getBoolean('openOnly'),
                getLong('branch'),
                getLong('businessType'))
    }

    protected BusinessCaseSearchRequest createInitialSearchRequest() {
        BusinessCaseSearchRequest sr = createSearchRequest()
        sr = initSearchRequest(sr)
        sr.pattern = initialSearchPattern
        sr.openOnly = true
        return sr
    }

    protected boolean getIncludeRequestOnlyBusinessType() {
        false
    }

    protected BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }
}
