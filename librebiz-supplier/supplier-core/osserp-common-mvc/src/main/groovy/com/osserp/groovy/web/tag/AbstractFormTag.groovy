/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web.tag

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.groovy.web.View
import com.osserp.groovy.web.WebUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractFormTag extends AbstractGroovyTag {
    private static Logger logger = LoggerFactory.getLogger(AbstractFormTag.class.getName())
    Boolean autoupdate = false
    Boolean disabled = false
    String name
    String readonly
    Object value
    String objectpath

    protected abstract String execute()

    /**
     * Provides the current value to set on the input element.
     * The method attempts to retrieve the value reading 
     * - actual value provided by form
     * - value as provided by view.bean.property and view.bean.objectpath
     * - value provided by tag
     * @return value or empty string if non found
     */
    protected final String getCurrentValue() {
        if (isIgnoreValue()) {
            return ''
        }
        View view = getCurrentView()
        String result = WebUtil.getFormValue(view, name, objectpath, isIgnoreModes())
        if (!result) {
            return getObjectValue()
        }
        result
    }

    protected boolean isIgnoreValue() {
        false
    }

    @Override
    protected String renderOnChange() {
        if (autoupdate) {
            return " onchange=\"enableUpdateOnly(); sendForm();\""
        }
        return super.renderOnChange()
    }

    protected String renderDisabled() {
        if (disabled) {
            return ' disabled="disabled"'
        }
        return ''
    }

    protected String renderReadonly() {
        if (readonly) {
            return ' readonly="readonly"'
        }
        return ''
    }

    /**
     * Override if additional formatting is required in case of missing form value 
     * but an object was provided by corresponding value attribute. 
     * @return object value as provided or empty string if null
     */
    protected String getObjectValue() {
        value ?: ''
    }

    /**
     * Indicates if fetching of form value depends on enabled editMode or createMode.
     * Override and return true if modes should be ignored (always fetch).
     * @return false by default
     */
    protected boolean isIgnoreModes() {
        false
    }

    protected boolean propertyExists(Object bean, String property) {
        WebUtil.propertyExists(bean, property)
    }
}
