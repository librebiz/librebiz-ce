/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 11, 2011 12:21:55 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.util.StringUtil;

import com.osserp.core.Item;
import com.osserp.core.dao.ProjectSuppliers;
import com.osserp.core.dao.records.SalesRevenueCostCollector;
import com.osserp.core.finance.Record;
import com.osserp.core.model.records.PurchaseInvoiceImpl;
import com.osserp.core.model.records.SalesInvoiceItemImpl;
import com.osserp.core.projects.ProjectSupplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRevenueCostCollectorByDeliveryDao extends AbstractSalesRevenueCostCollector implements SalesRevenueCostCollector {
    private static Logger log = LoggerFactory.getLogger(SalesRevenueCostCollectorByDeliveryDao.class.getName());

    private ProjectSuppliers projectSuppliers;

    public SalesRevenueCostCollectorByDeliveryDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            ProjectSuppliers projectSuppliers) {
        super(jdbcTemplate, tables, sessionFactory);
        this.projectSuppliers = projectSuppliers;
    }

    public List<Item> getCosts(Record order) {
        List<Item> result = new ArrayList<Item>();
        if (order == null) {
            return result;
        }
        StringBuilder sql = new StringBuilder("SELECT id FROM ");
        sql.append(getTable("salesDeliveryNotes")).append(" WHERE reference_id = ").append(order.getId());
        String ids = StringUtil.createCommaSeparated((List) jdbcTemplate.query(sql.toString(), getLongRowMapper()));
        if (ids != null && ids.length() > 0) {
            List<Item> items = getCurrentSession().createQuery(
                    "from " + SalesInvoiceItemImpl.class.getName() 
                    + " o where o.accountingReferenceId in (" + ids + ")").list();
            for (int i = 0, j = items.size(); i < j; i++) {
                Item item = items.get(i);
                Item clone = (Item) item.clone();
                clone.setPartnerPrice(clone.getPrice());
                result.add(clone);
            }
        }
        return result;
    }

    @Override
    public boolean isProvidingThirdpartyCosts() {
        return true;
    }

    public List<Item> getThirdpartyCosts(Record order) {
        List<Item> result = new ArrayList<Item>();
        if (order == null) {
            return result;
        }
        List<ProjectSupplier> suppliers = projectSuppliers.getSuppliers(order.getBusinessCaseId());
        List<Record> records = getCurrentSession().createQuery(
                "from " + PurchaseInvoiceImpl.class.getName() +
                " o where o.businessCaseId = :salesId")
                .setParameter("salesId", order.getBusinessCaseId())
                .list();
        for (int i = 0, j = records.size(); i < j; i++) {
            Record next = records.get(i);
            ProjectSupplier supplier = fetchSupplier(suppliers, next);
            if (supplier != null) {
                for (int k = 0, l = next.getItems().size(); k < l; k++) {
                    Item clone = (Item) next.getItems().get(k).clone();
                    clone.setPartnerPrice(clone.getPrice());
                    result.add(clone);
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getThirdpartyCosts() done [order=" + order.getId()
                    + ", sales=" + order.getBusinessCaseId()
                    + ", itemCount=" + result.size() + "]");
        }
        return result;
    }

    private ProjectSupplier fetchSupplier(List<ProjectSupplier> suppliers, Record record) {
        for (int i = 0, j = suppliers.size(); i < j; i++) {
            ProjectSupplier next = suppliers.get(i);
            if (record.getContact().getId().equals(next.getSupplier().getId())) {
                return next;
            }
        }
        return null;
    }
}
