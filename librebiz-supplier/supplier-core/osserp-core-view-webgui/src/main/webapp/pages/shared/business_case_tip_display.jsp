<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.businessCaseView}"/>
<c:set var="obj" value="${view.request}"/>
<c:set var="contact" value="${obj.tip}"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="tipProvider"/>: <o:out value="${contact.displayName}"/> [<o:out value="${contact.id}"/>]
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<table class="valueTable first33">
				<tbody>
					<tr>
						<td><fmt:message key="street"/></td>
						<td><o:out value="${contact.address.street}"/></td>
					</tr>
					<tr>
						<td><fmt:message key="city"/></td>
						<td><o:out value="${contact.address.zipcode}"/> <o:out value="${contact.address.city}"/></td>
					</tr>
					<c:if test="${!empty contact.email}">
						<tr>
							<td><fmt:message key="email"/></td>
							<td><o:email value="${contact.email}" limit="32"/></td>
						</tr>
					</c:if>
					<c:if test="${!empty contact.phone and !empty contact.phone.number}">
						<tr>
							<td><fmt:message key="private"/></td>
							<td><oc:phone value="${contact.phone}"/></td>
						</tr>
					</c:if>
					<c:if test="${!empty contact.mobile and !empty contact.mobile.number}">
						<tr>
							<td><fmt:message key="mobile"/></td>
							<td><oc:phone value="${contact.mobile}"/></td>
						</tr>
					</c:if>
					<c:if test="${!empty obj.tipCommission and obj.tipCommission > 0}">
						<tr>
							<td><fmt:message key="tipCommission"/></td>
							<td><o:number value="${obj.tipCommission}" format="currency"/> <fmt:message key="euro"/></td>
						</tr>
					</c:if>
				</tbody>
			</table>
			<div class="spacer"></div>
		</div>
	</div>
</div>
