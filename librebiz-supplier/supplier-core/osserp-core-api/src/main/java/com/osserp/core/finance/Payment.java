/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Jun-2005 14:47:42 
 * 
 */
package com.osserp.core.finance;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Payment extends FinanceRecord {

    static final Long OUTPAYMENT = 61L;

    /**
     * The paid amount
     * @return amount
     */
    BigDecimal getAmount();

    /**
     * The amount to pay provides an optional place to save the amount if the 
     * record represents an amount to pay in the future (outstanding payment).
     * @return amount to pay
     */
    BigDecimal getAmountToPay();

    /**
     * The id of the related bank account
     * @return bankAccountId
     */
    Long getBankAccountId();

    /**
     * The associated branch office. Note: This is an optional setting and
     * not all payment implementations support this setting as this information
     * is provided by related businessCase in most cases. 
     * Only payments not related to a purchase or sales order may provide this
     * value as a persistent column. Expecting null as a valid result is ok.  
     * @return optional branch assignment
     */
    @Override
    Long getBranchId();

    /**
     * The id of the related downpayment, invoice or creditNote.
     * @return recordId
     */
    Long getRecordId();

    /**
     * The type of the related record (e.g. downpayment, invoice or creditNote)
     * @return recordTypeId
     */
    Long getRecordTypeId();

    /**
     * Provides the date of payment
     * @return paid
     */
    Date getPaid();
    
    /**
     * Updates date paid on
     * @param paid
     */
    void setPaid(Date paid);

    /**
     * Indicates payment with reduced tax. Note: Use of this property does only
     * make sense in the context of standalone payments without dedicated items
     * and without reference to an invoice or other taxable record.
     * @return reducedTax
     */
    boolean isReducedTax();

    /**
     * Provides the base or reduced tax rate if amount includes tax. The rate
     * is only required if context of payment is standalone.
     * @return taxRate to calculate the net amount of a common payment
     */
    Double getTaxRate();

    /**
     * Indicates if type represents a tax payment
     * @return taxPayment
     */
    boolean isTaxPayment();
    
    /**
     * Indicates if payment is outflow of cash or an incoming.
     * @return true if outflow
     */
    boolean isOutflowOfCash();
    
    /**
     * Allows to change amount
     * @param cashDiscount
     */
    void subtract(BigDecimal cashDiscount);
}
