<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="businessCaseReportView" />
<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="recordType"> </th>
                <th class="recordNumber"><fmt:message key="number" /></th>
                <th class="recordCustomer"><fmt:message key="name" /></th>
                <th class="recordStatus">%</th>
                <th class="recordAction"><fmt:message key="status" /></th>
                <th class="recordDate"><fmt:message key="action" /></th>
                <th class="recordEmployee"><fmt:message key="sales" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="obj" items="${view.queryResult.rows}" varStatus="s">
                <tr>
                    <c:choose>
                        <c:when test="${obj['is_sales']}">
                            <td class="recordType">
                                <fmt:message key="typeKeySales"/>
                            </td>
                            <td class="recordNumber">
                                <a href="<c:url value="/loadSales.do?exit=businessCaseReport&id=${obj['id']}"/>"><o:out value="${obj['id']}" /></a>
                            </td>
                        </c:when>
                        <c:otherwise>
                            <td class="recordType">
                                <fmt:message key="typeKeyRequest"/>
                            </td>
                            <td class="recordNumber">
                                <a href="<c:url value="/loadRequest.do?exit=businessCaseReport&id=${obj['id']}"/>"><o:out value="${obj['id']}" /></a>
                            </td>
                        </c:otherwise>
                    </c:choose>
                    <td class="recordCustomer">
                        <o:out value="${obj['name']}" limit="50" />
                    </td>
                    <td class="recordStatus">
                        <o:out value="${obj['status']}" />
                    </td>
                    <td class="recordAction">
                        <o:out value="${obj['action_name']}" />
                    </td>
                    <td class="recordDate"><o:date value="${obj['action_created']}" addcentury="false" /></td>
                    <td class="recordEmployee">
                        <o:ajaxLink linkId="salesDisplay" url="${'/employeeInfo.do?id='}${obj['sales_id']}">
                            <oc:employee value="${obj['sales_id']}" limit="20" />
                        </o:ajaxLink>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
