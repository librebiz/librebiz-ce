--
-- osserp-core - Initial data import (required)
--
-- Copyright (C) 2001-2024 Rainer Kirchner <rk@osserp.com>
--
-- This file is part of osserp-core, the open, smart and simple ERP solution
-- for Debian GNU/Linux and most other operating systems.
--  
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU Affero General Public License (AGPL) 
-- version 3 as published by the Free Software Foundation. In accordance 
-- with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
-- the effect that the original authors expressly exclude the warranty of
-- non-infringement of any third-party rights. 
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
-- at http://www.gnu.org/licenses/agpl-3.0.html for more details.
--
-- The interactive user interfaces in modified source and object code 
-- versions must display Appropriate Legal Notices, as required under 
-- Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
-- the GNU AGPL you must include a clickable link "Powered by osserp.com"
-- that leads directly to the URL http://osserp.com in the footer area 
-- or, if not reasonably feasible for technical reason, as a top-level 
-- link of the primary navigation of the graphical user interface in
-- every copy of the program you distribute. 
--
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = osserp, pg_catalog;

--
-- Data for Name: accounting_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO accounting_types VALUES (1, 'Aktiva/Mittelverwendung', 'A');
INSERT INTO accounting_types VALUES (2, 'Passiva', 'Q');
INSERT INTO accounting_types VALUES (3, 'Kosten', 'C');
INSERT INTO accounting_types VALUES (4, 'Erlöskonto', 'I');
INSERT INTO accounting_types VALUES (5, 'Aufwandskonto', 'E');
INSERT INTO accounting_types VALUES (6, 'Passiva/Mittelherkunft', 'L');


--
-- Data for Name: contacts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contacts VALUES (1000000000, NULL, 2, 0, NULL, NULL, NULL, 'Meine Firma XYZ', '1970-01-01 00:00:00', '1970-01-01 00:00:00', 6000, NULL, NULL, false, true, true, false, false, false, NULL, NULL, NULL, false, NULL, 'Ganz Lange Str. 999', '60999', 'Frankfurt', 53, 7, 0, NULL, 'www.example.org', true, true, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO contacts VALUES (1000000001, NULL, 2, -10, NULL, NULL, 'Address component', 'My Company', '1970-01-01 00:00:00', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false, false, false, NULL, NULL, NULL, false, NULL, 'Address Street', 'ABC DEF', 'London', 60, NULL, 0, NULL, 'example.org', false, true, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'London', false, false, NULL, NULL, NULL, false, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO contacts VALUES (1000000002, NULL, 1, 50, 1, NULL, 'Setup', 'Admin', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, true, false, false, false, false, false, NULL, NULL, NULL, false, NULL, 'Ganz Lange Str. 999', '60999', 'Frankfurt', 53, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO contacts VALUES (1000000003, NULL, 1, -10, 1, NULL, 'Barverkauf', 'Kasse', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, false, true, false, false, false, false, NULL, NULL, NULL, false, NULL, 'Ganz Lange Str. 999', '60999', 'Frankfurt', 53, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, false, NULL, NULL, NULL, false, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- Data for Name: employee_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO employee_status VALUES (1, 'Virtuell', false, false, NULL, false, false);
INSERT INTO employee_status VALUES (2, 'Geschäftsführer', false, false, NULL, false, false);
INSERT INTO employee_status VALUES (3, 'Angestellter', false, false, NULL, false, false);
INSERT INTO employee_status VALUES (4, 'Gewerblicher Angestellter', false, false, NULL, false, false);
INSERT INTO employee_status VALUES (5, 'Freelancer', false, false, NULL, false, false);
INSERT INTO employee_status VALUES (6, 'Auszubildender', false, false, NULL, false, false);
INSERT INTO employee_status VALUES (7, 'Praktikant', false, false, NULL, false, false);
INSERT INTO employee_status VALUES (8, 'Teilzeit', false, false, NULL, false, false);
INSERT INTO employee_status VALUES (9, 'Student', false, false, NULL, false, false);
INSERT INTO employee_status VALUES (10, 'Schüler', false, false, NULL, false, false);


--
-- Data for Name: employee_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO employee_types VALUES (1, 'Mitarbeiter', 'employee_type.employee', true, false, false, false);
INSERT INTO employee_types VALUES (2, 'Handelsvertreter', 'employee_type.salesagent', false, true, false, false);
INSERT INTO employee_types VALUES (3, 'Extern (Mitarbeiter)', 'employee_type.external', false, false, true, false);
INSERT INTO employee_types VALUES (4, 'Partner', 'employee_type.partner', false, false, false, true);


--
-- Data for Name: system_company_legalforms; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_company_legalforms VALUES (1, 'Einzelunternehmen', false, false, false);
INSERT INTO system_company_legalforms VALUES (2, 'e.K.', false, false, false);
INSERT INTO system_company_legalforms VALUES (3, 'GbR', false, false, false);
INSERT INTO system_company_legalforms VALUES (4, 'OHG', false, false, false);
INSERT INTO system_company_legalforms VALUES (5, 'GmbH', false, false, false);
INSERT INTO system_company_legalforms VALUES (6, 'GmbH & CoKG', false, false, false);
INSERT INTO system_company_legalforms VALUES (7, 'AG', false, true, false);
INSERT INTO system_company_legalforms VALUES (8, 'e.V.', false, false, false);
INSERT INTO system_company_legalforms VALUES (9, 'Ltd.', true, false, false);
INSERT INTO system_company_legalforms VALUES (10, 'UG (haftungsbeschränkt)', false, false, false);
INSERT INTO system_company_legalforms VALUES (11, 'Freie Berufe', false, false, false);
INSERT INTO system_company_legalforms VALUES (12, 'Ltd. & CoKG', true, false, false);


--
-- Data for Name: system_companies; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_companies VALUES (100, 'Meine Firma XYZ', 1000000000, 1.05, 'Frankfurt am Main', '999 999', NULL, 'DE999999999', true, false, false, true, false, false, 'DE', true, 5, 2, NULL, NULL, false, NULL, NULL, NULL);


--
-- Data for Name: system_company_branchs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_company_branchs VALUES (1, 100, 'HQ', 'hq', true, '1970-01-01 00:00:00', 'DE', NULL, 1000000000, NULL, NULL, NULL, 'info@example.org', 1000, NULL, false, false, NULL, NULL, NULL, false, false, false, false, NULL, NULL, NULL, true);
INSERT INTO system_company_branchs VALUES (2, 100, 'Registered Office', 'Registered Office', false, '1970-01-01 00:00:00', 'XX', NULL, 1000000001, NULL, NULL, NULL, 'mail@example.org', 1001, NULL, true, true, 'Country name', '00000000', 'Registered in COUNTRY0', false, false, false, false, NULL, NULL, NULL, false);


--
-- Data for Name: system_company_stocks; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_company_stocks VALUES (100, 100, 'LG1', 'Lager 1', 'L1', '1970-01-01 00:00:00', true, true, true);


--
-- Data for Name: employees; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO employees VALUES (6000, 1000000000, 0, 1, 1, true, '1970-01-01 00:00:00', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 'SYS', NULL, NULL, NULL, NULL, true, false, NULL, false, NULL, false, true, false, false, NULL, 'DE', 'DE', NULL, false, false, false, false, false, false, false, 'group', NULL, NULL, NULL, NULL, true);
INSERT INTO employees VALUES (65534, 1000000001, 0, 1, 1, true, '1970-01-01 00:00:00', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 'GUEST', NULL, NULL, NULL, NULL, true, false, NULL, false, NULL, false, true, false, false, NULL, 'DE', 'DE', NULL, false, false, false, false, false, false, false, 'group', NULL, NULL, NULL, NULL, true);
INSERT INTO employees VALUES (6001, 1000000002, NULL, NULL, NULL, true, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 'CL', NULL, NULL, NULL, NULL, false, false, NULL, false, NULL, false, false, false, false, NULL, 'DE', 'DE', 1, false, false, false, false, false, false, false, NULL, NULL, NULL, NULL, NULL, true);


--
-- Data for Name: accounting_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: accounting_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('accounting_groups_id_seq', 1, false);


--
-- Name: accounting_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('accounting_types_id_seq', 6, true);


--
-- Data for Name: product_backups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: plugin_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO plugin_types (id, name, core_plugin) VALUES (1, 'auth', true);
INSERT INTO plugin_types (id, name, core_plugin) VALUES (2, 'ldap', true);
INSERT INTO plugin_types (id, name, core_plugin) VALUES (3, 'mail', true);
INSERT INTO plugin_types (id, name, core_plugin) VALUES (4, 'crm', false);
INSERT INTO plugin_types (id, name, core_plugin) VALUES (5, 'cal', false);


--
-- Name: plugin_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('plugin_types_id_seq', 5, true);



--
-- Data for Name: plugins; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO plugins (id, type_id, vendor, name, description, i18nkey, is_default) VALUES (1, 1, 'core', 'auth','Authenticators','authPlugin', true);
INSERT INTO plugins (id, type_id, vendor, name, description, i18nkey, is_disabled, is_default) VALUES (2, 2, 'core', 'ldap','LDAP Support','ldapPlugin', true, true);
INSERT INTO plugins (id, type_id, vendor, name, description, i18nkey, is_disabled, is_default) VALUES (3, 3, 'core', 'mail','Mailbox Support','mailPlugin', true, true);
INSERT INTO plugins (id, type_id, vendor, name, description, i18nkey, is_disabled, is_default) VALUES (4, 4, 'core', 'crm','CRM Support','crmPlugin', true, true);
INSERT INTO plugins (id, type_id, vendor, name, description, i18nkey, is_disabled, is_default) VALUES (5, 5, 'core', 'cal','Calendar Support','calPlugin', true, true);


--
-- Name: plugins_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('plugins_id_seq', 100, false);


--
-- Name: product_backups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_backups_id_seq', 1, false);


--
-- Data for Name: product_number_ranges; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_number_ranges VALUES (1, 'Standard', 10000, 89999, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO product_number_ranges VALUES (2, 'Geschäftsfälle', 90000, 99999, '1970-01-01 00:00:00', 6000, NULL, NULL);


--
-- Data for Name: product_categories; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_categories VALUES (301, 'Abrechnung', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, false, 2, false);
INSERT INTO product_categories VALUES (302, 'Rabatte', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, false, 2, false);


--
-- Name: product_categories_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_categories_id_seq', 302, true);


--
-- Data for Name: product_classes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_classes VALUES (1, 'A', 'A-Produkte', 90001);
INSERT INTO product_classes VALUES (2, 'B', 'B-Produkte', 90002);
INSERT INTO product_classes VALUES (3, 'C', 'C-Produkte', 90003);
INSERT INTO product_classes VALUES (4, 'D', 'Dienstleistungen, Geschäftsfälle, Steuerartikel, d.h. kein Lagerbestand', NULL);
INSERT INTO product_classes VALUES (5, 'E', 'Büroartikel, Werkzeuge', NULL);


--
-- Data for Name: product_colors; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_colors_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_colors_id_seq', 1, false);


--
-- Data for Name: product_descriptions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_descriptions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_descriptions_id_seq', 1, false);


--
-- Data for Name: product_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_groups VALUES (200, 'Betriebsbedarf', false, 11, 0.31, 0.22, 0.05, true, 4, false, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 2, false, false, false, false, false, false, true, false, false);
INSERT INTO product_groups VALUES (201, 'Geschäftsfälle', false, 11, 0.31, 0.22, 0.05, true, 4, false, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 2, false, false, false, false, false, false, false, false, false);
INSERT INTO product_groups VALUES (202, 'Allgemein', false, 11, 0.31, 0.22, 0.05, true, 4, false, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 1, false, false, false, false, false, false, true, false, false);


--
-- Data for Name: product_groups_categories_relation; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_groups_id_seq', 202, true);


--
-- Data for Name: product_manufacturers; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_manufacturers VALUES (0, 'undefiniert', NULL);
INSERT INTO product_manufacturers VALUES (1, 'Verbrauch intern', NULL);
INSERT INTO product_manufacturers VALUES (2, 'Dienstleistung', NULL);
INSERT INTO product_manufacturers VALUES (3, 'Entwicklung', NULL);
INSERT INTO product_manufacturers VALUES (4, 'Produktion', NULL);


--
-- Name: product_manufacturers_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_manufacturers_id_seq', 100, true);


--
-- Name: product_number_ranges_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_number_ranges_id_seq', 2, true);


--
-- Data for Name: product_planning_modes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_planning_modes VALUES (1, 'Bestandsgeführt', 'Disposition richtet sich nach dem Bestand', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true);
INSERT INTO product_planning_modes VALUES (2, 'Bedarfsgesteuert', 'Disposition richtet sich nach dem Bedarf', '1970-01-01 00:00:00', 6000, NULL, NULL, false, true, false);


--
-- Name: product_planning_modes_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_planning_modes_id_seq', 2, true);


--
-- Data for Name: product_planning_settings; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_planning_settings_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_planning_settings_id_seq', 1, false);


--
-- Data for Name: product_planning_tasks; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_planning_tasks_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_planning_tasks_id_seq', 1, false);


--
-- Data for Name: product_quantity_units; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_quantity_units VALUES (1, 'Stk', 'common.pieces.short', 0, false);
INSERT INTO product_quantity_units VALUES (2, 'mm', NULL, 2, false);
INSERT INTO product_quantity_units VALUES (3, 'm', NULL, 2, false);
INSERT INTO product_quantity_units VALUES (4, 'm²', NULL, 2, false);
INSERT INTO product_quantity_units VALUES (5, 'm³', NULL, 2, false);
INSERT INTO product_quantity_units VALUES (6, 'kW', NULL, 2, false);
INSERT INTO product_quantity_units VALUES (7, 'kWp', NULL, 3, false);
INSERT INTO product_quantity_units VALUES (8, 'Wp', NULL, 0, false);
INSERT INTO product_quantity_units VALUES (9, 'kg', NULL, 2, false);
INSERT INTO product_quantity_units VALUES (10, 'l', NULL, 2, false);
INSERT INTO product_quantity_units VALUES (11, ' ', NULL, 3, false);
INSERT INTO product_quantity_units VALUES (12, 'Std', 'common.hours.short', 2, false);
INSERT INTO product_quantity_units VALUES (13, 'km', NULL, 2, false);
INSERT INTO product_quantity_units VALUES (14, 't', NULL, 2, false);
INSERT INTO product_quantity_units VALUES (15, 'W', NULL, 0, false);
INSERT INTO product_quantity_units VALUES (16, 'J', NULL, 0, false);
INSERT INTO product_quantity_units VALUES (17, 'M', NULL, 0, false);
INSERT INTO product_quantity_units VALUES (18, 'MT', NULL, 0, false);
INSERT INTO product_quantity_units VALUES (19, 'Tag', NULL, 0, false);
INSERT INTO product_quantity_units VALUES (20, 'psch.', NULL, 0, false);


--
-- Data for Name: product_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_types VALUES (101, 'Waren', 'Waren im steuerlichen Sinne', '1970-01-01 00:00:00', 6000, NULL, NULL, false);
INSERT INTO product_types VALUES (102, 'Steuerung', 'Geschäftsfälle, Steuerung Workflow', '1970-01-01 00:00:00', 6000, NULL, NULL, false);
INSERT INTO product_types VALUES (103, 'Dienstleistung', 'Leistungserbringung durch Dienstleistung', '1970-01-01 00:00:00', 6000, NULL, NULL, false);
INSERT INTO product_types VALUES (104, 'Nicht Waren', 'Nicht Waren im steuerlichen Sinne', '1970-01-01 00:00:00', 6000, NULL, NULL, false);


--
-- Data for Name: products; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO products VALUES (90000, 201, 11, 0, 'RABATT', 'Rabatt', NULL, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 0, 0, 0, false, false, false, false, true, true, 0.31, 0.22, 0.05, NULL, 1, true, false, false, false, false, 0, 0, 0, 0.31, 0.22, 0.05, 0, false, false, false, false, false, false, false, 302, 'de', 100, NULL, false, NULL, false, 0, NULL, true, false, 0, 0, 0, 'mm', 0.000, 'Kg', NULL, NULL, false, NULL, NULL, false, NULL, false, false);
INSERT INTO products VALUES (90001, 201, 11, 0, 'RABATTA', 'Rabatt A-Produkte', NULL, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 0, 0, 0, false, false, false, false, true, true, 0.31, 0.22, 0.05, NULL, 1, true, false, false, false, false, 0, 0, 0, 0.31, 0.22, 0.05, 0, false, false, false, false, false, false, false, 302, 'de', 100, NULL, false, NULL, false, 0, NULL, true, false, 0, 0, 0, 'mm', 0.000, 'Kg', NULL, NULL, false, NULL, NULL, false, NULL, false, false);
INSERT INTO products VALUES (90002, 201, 11, 0, 'RABATTB', 'Rabatt B-Produkte', NULL, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 0, 0, 0, false, false, false, false, true, true, 0.31, 0.22, 0.05, NULL, 1, true, false, false, false, false, 0, 0, 0, 0.31, 0.22, 0.05, 0, false, false, false, false, false, false, false, 302, 'de', 100, NULL, false, NULL, false, 0, NULL, true, false, 0, 0, 0, 'mm', 0.000, 'Kg', NULL, NULL, false, NULL, NULL, false, NULL, false, false);
INSERT INTO products VALUES (90003, 201, 11, 0, 'RABATTC', 'Rabatt C-Produkte', NULL, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 0, 0, 0, false, false, false, false, true, true, 0.31, 0.22, 0.05, NULL, 1, true, false, false, false, false, 0, 0, 0, 0.31, 0.22, 0.05, 0, false, false, false, false, false, false, false, 302, 'de', 100, NULL, false, NULL, false, 0, NULL, true, false, 0, 0, 0, 'mm', 0.000, 'Kg', NULL, NULL, false, NULL, NULL, false, NULL, false, false);
INSERT INTO products VALUES (99999, 201, 11, 0, 'ABRECHNUNG', '###   Abrechnungsposition            ###', NULL, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 0, 0, 0, false, false, false, false, true, true, 0.31, 0.22, 0.05, NULL, 1, true, false, false, false, false, 0, 0, 0, 0.31, 0.22, 0.05, 0, false, false, false, false, false, false, false, 301, 'de', 100, NULL, false, NULL, false, 0, NULL, true, false, 0, 0, 0, 'mm', 0.000, 'Kg', NULL, NULL, false, NULL, NULL, false, NULL, false, false);


--
-- Data for Name: product_price_by_quantity_defaults; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: product_price_by_quantity_default_ranges; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_price_by_quantity_default_ranges_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_price_by_quantity_default_ranges_id_seq', 1, false);


--
-- Name: product_price_by_quantity_defaults_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_price_by_quantity_defaults_id_seq', 1, false);


--
-- Data for Name: product_price_by_quantity_matrix; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_price_by_quantity_matrix_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_price_by_quantity_matrix_id_seq', 1, false);


--
-- Data for Name: product_price_by_quantity_values; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: product_price_by_quantity_value_history; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_price_by_quantity_value_history_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_price_by_quantity_value_history_id_seq', 1, false);


--
-- Name: product_price_by_quantity_values_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_price_by_quantity_values_id_seq', 1, false);


--
-- Name: product_quantity_units_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_quantity_units_id_seq', 20, true);


--
-- Data for Name: product_sales_price_histories; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_sales_price_histories_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_sales_price_histories_id_seq', 1, false);


--
-- Data for Name: product_sales_price_history_date_changed; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_sales_price_history_date_changed_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_sales_price_history_date_changed_id_seq', 1, false);


--
-- Data for Name: product_selection_contexts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_selection_contexts VALUES (1, 'Kalkulation', 'calculation', false);
INSERT INTO product_selection_contexts VALUES (2, 'Produktion', 'production', false);
INSERT INTO product_selection_contexts VALUES (3, 'Deckungsbeitrag', 'salesRevenue', true);
INSERT INTO product_selection_contexts VALUES (4, 'Beschaffung', 'sourcing', false);


--
-- Data for Name: product_selection_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_selection_configs VALUES (1, 'Sammelrechnung', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, true, false, false);
INSERT INTO product_selection_configs VALUES (2, 'Waren', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 'Waren, die (im Gegensatz zu NICHT-Waren) in der Inventur erfasst werden', NULL, false, false, false, false);
INSERT INTO product_selection_configs VALUES (3, 'Geschäftsfälle Zahlungen', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 'Filter wird zur Vorauswahl für Geschäftsfälle beim Eintragen von Zahlungen im Projektkontext verwendet. ', NULL, false, false, false, false);
INSERT INTO product_selection_configs VALUES (4, 'Dienstleistungsrechnung', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, false, false, false);
INSERT INTO product_selection_configs VALUES (5, 'Paket', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 1, false, false, false, false);
INSERT INTO product_selection_configs VALUES (6, 'Dienstleistung', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 1, false, false, false, false);
INSERT INTO product_selection_configs VALUES (7, 'Betriebsmittel', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 'Laufende Kosten, Betriebs- und Bürobedarf', 4, false, false, false, false);
INSERT INTO product_selection_configs VALUES (10, 'A-Produkte', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 3, false, true, false, false);
INSERT INTO product_selection_configs VALUES (11, 'B-Produkte', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 3, false, true, false, false);
INSERT INTO product_selection_configs VALUES (12, 'C-Produkte', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 3, false, true, false, false);
INSERT INTO product_selection_configs VALUES (13, 'Restmaterial', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 3, false, false, false, false);
INSERT INTO product_selection_configs VALUES (14, 'Fremdleistung', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 3, false, true, false, false);
INSERT INTO product_selection_configs VALUES (15, 'Projektierungskosten', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 3, false, true, false, false);
INSERT INTO product_selection_configs VALUES (16, 'Nicht zugeordnet', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 3, true, false, false, false);
INSERT INTO product_selection_configs VALUES (30, 'Produktion', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 'Produkte der Produktion', 2, false, false, false, false);


--
-- Data for Name: product_selection_config_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_selection_config_items VALUES (1, 2, '1970-01-01 00:00:00', 6000, 101, NULL, NULL, NULL, false, false, NULL);


--
-- Name: product_selection_config_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_selection_config_items_id_seq', 1, true);


--
-- Name: product_selection_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_selection_configs_id_seq', 30, true);


--
-- Name: product_selection_contexts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_selection_contexts_id_seq', 4, true);


--
-- Data for Name: product_selection_profiles; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: product_selection_profiles_product_selection_configs_relation; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_selection_profiles_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_selection_profiles_id_seq', 1, false);


--
-- Data for Name: product_selections; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: product_selections_product_selection_profiles_relation; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: product_selections_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_selections_id_seq', 1, false);


--
-- Data for Name: product_types_groups_relation; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_types_groups_relation VALUES (101, 202);


--
-- Name: product_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('product_types_id_seq', 104, true);


--
-- Data for Name: product_types_relations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO product_types_relations VALUES (90000, 102, 0);
INSERT INTO product_types_relations VALUES (90001, 102, 0);
INSERT INTO product_types_relations VALUES (90002, 102, 0);
INSERT INTO product_types_relations VALUES (90003, 102, 0);
INSERT INTO product_types_relations VALUES (99999, 102, 0);


--
-- Data for Name: business_note_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO business_note_types VALUES (1, 'productNote', false, false, NULL, false, true, false, false, true, false, NULL);
INSERT INTO business_note_types VALUES (2, 'contactNote', false, false, NULL, false, true, false, false, true, false, NULL);
INSERT INTO business_note_types VALUES (3, 'salesNote', false, false, NULL, false, true, true, false, true, false, NULL);
INSERT INTO business_note_types VALUES (4, 'purchaseOrderNote', false, false, NULL, false, true, false, false, true, false, NULL);


--
-- Name: business_note_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('business_note_types_id_seq', 4, true);


--
-- Data for Name: business_notes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: business_notes_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('business_notes_id_seq', 1, false);


--
-- Data for Name: business_properties; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: business_properties_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('business_properties_id_seq', 1, false);


--
-- Data for Name: business_property_templates; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: business_property_templates_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('business_property_templates_id_seq', 1, false);


--
-- Data for Name: business_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO business_status VALUES (0, 'created', false);
INSERT INTO business_status VALUES (1, 'changed', false);
INSERT INTO business_status VALUES (2, 'printed', false);
INSERT INTO business_status VALUES (3, 'released', true);
INSERT INTO business_status VALUES (5, 'billed', true);


--
-- Name: business_status_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('business_status_id_seq', 11, false);


--
-- Data for Name: business_template_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: business_template_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('business_template_types_id_seq', 1, false);


--
-- Data for Name: business_templates; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: business_templates_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('business_templates_id_seq', 1, false);


--
-- Data for Name: calculation_by_template_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculation_by_template_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_by_template_configs_id_seq', 1, false);


--
-- Data for Name: contact_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_groups VALUES (0, 'Mandant', false, true, 'client');
INSERT INTO contact_groups VALUES (1, 'Kunde/Interessent', false, false, 'customerInterest');
INSERT INTO contact_groups VALUES (2, 'Lieferant', false, false, 'supplier');
INSERT INTO contact_groups VALUES (3, 'Dienstleister', false, false, 'installer');
INSERT INTO contact_groups VALUES (7, 'Mitarbeiter', false, false, 'employee');
INSERT INTO contact_groups VALUES (8, 'Allgemein', true, false, 'common');
INSERT INTO contact_groups VALUES (9, 'Niederlassung', false, false, 'branchOffice');


--
-- Data for Name: letter_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO letter_types VALUES (1, 'Korrespondenz Aufträge', 'Briefe an Kunden zu Aufträgen', 'customerNo,projectNo', 1, 'com.osserp.core.customers.CustomerSearch', 'sales', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, false, false, '2cm', false, false);
INSERT INTO letter_types VALUES (2, 'Korrespondenz Anfragen', 'Briefe an Kunden zu Anfragen', 'customerNo,requestNo', 1, 'com.osserp.core.customers.CustomerSearch', 'request', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, false, false, '2cm', false, false);
INSERT INTO letter_types VALUES (3, 'Korrespondenz Kunden', 'Briefe an Kunden', 'customerNo', 1, 'com.osserp.core.customers.CustomerSearch', 'customers', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, false, false, '2cm', false, false);
INSERT INTO letter_types VALUES (4, 'Korrespondenz Lieferant', 'Briefe an Lieferanten', 'supplierNo', 2, 'com.osserp.core.suppliers.SupplierSearch', 'suppliers', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, false, false, '2cm', false, false);
INSERT INTO letter_types VALUES (5, 'Korrespondenz Mitarbeiter', 'Briefe an Mitarbeiter', 'employeeNo', 7, 'com.osserp.core.employees.EmployeeSearch', 'employees', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, false, false, '2cm', false, false);
INSERT INTO letter_types VALUES (6, 'Texte zum Angebot', 'Texte zum Einbetten in Angebote', NULL, 1, 'com.osserp.core.customers.CustomerSearch', 'salesOffer', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, true, true, '2cm', true, true);
INSERT INTO letter_types VALUES (7, 'Texte zum Auftrag', 'Texte zum Einbetten in Aufträge', NULL, 1, 'com.osserp.core.customers.CustomerSearch', 'salesOrder', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, true, true, '2cm', true, true);
INSERT INTO letter_types VALUES (8, 'Texte zum Lieferschein', 'Texte zum Einbetten in Lieferscheine', NULL, 1, 'com.osserp.core.customers.CustomerSearch', 'salesDeliveryNote', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, true, true, '2cm', true, true);
INSERT INTO letter_types VALUES (9, 'Texte zur Anzahlung', 'Texte zum Einbetten in Anzahlungsrechnungen', NULL, 1, 'com.osserp.core.customers.CustomerSearch', 'salesDownpayment', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, true, true, '2cm', true, true);
INSERT INTO letter_types VALUES (10, 'Texte zur Rechnung', 'Texte zum Einbetten in Rechnungen', NULL, 1, 'com.osserp.core.customers.CustomerSearch', 'salesInvoice', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, true, true, '2cm', true, true);
INSERT INTO letter_types VALUES (11, 'Texte zur Gutschrift', 'Texte zum Einbetten in Gutschriften', NULL, 1, 'com.osserp.core.customers.CustomerSearch', 'salesCreditNote', 'xsl/documents/letter', 'Mit freundlichen Grüßen', NULL, false, false, false, true, true, '2cm', true, true);


--
-- Data for Name: project_contract_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO project_contract_types (id, end_of_life, name, created_by, label) VALUES (0, false, 'Standard', 6000, 'Standard');
INSERT INTO project_contract_types (id, end_of_life, name, created_by, label) VALUES (1, true, 'Dienstleistungsvertrag', 6000, 'Time & Material');
INSERT INTO project_contract_types (id, end_of_life, name, created_by, label) VALUES (2, true, 'Vermietung', 6000, 'Tenancy Agreement');


--
-- Data for Name: request_workflows; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_workflows VALUES (1, 'Projekt', NULL, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 'sales_type_1_id_seq', true, 'Workflow Projekt - Angebot / Auftrag / Implementierung', NULL, true);
INSERT INTO request_workflows VALUES (5, 'Dienstleistung', NULL, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 'sales_type_5_id_seq', true, 'Workflow Dienstleistung - Time & Material', NULL, true);
INSERT INTO request_workflows VALUES (10, 'Laufzeit', NULL, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 'sales_type_10_id_seq', true, 'Workflow Dienstleistung - Laufzeit / Miete / Pacht', NULL, true);
INSERT INTO request_workflows VALUES (11, 'Auftrag', NULL, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 'sales_type_11_id_seq', true, 'Workflow Auftrag - mit Angebotsphase', NULL, true);
INSERT INTO request_workflows VALUES (15, 'Direkt', NULL, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 'sales_type_15_id_seq', false, 'Workflow Auftrag - ohne Angebotsphase', NULL, true);
INSERT INTO request_workflows VALUES (100, 'Interessent', NULL, false, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, false, 'Allgemeine Adress- und Interessentenverwaltung', NULL, true);
INSERT INTO request_workflows VALUES (102, 'Einkauf', NULL, false, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, false, 'Used by system - Do not change!', NULL, true);


--
-- Data for Name: request_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_types VALUES (1, 'Interessent', 'INT', 'sales', 'generic', 100, true, true, false, false, true, true, true, false, true, false, false, 'none', false, NULL, NULL, 'de', 2, false, false, 100, 'calculatorByItems', 17, 22, NULL, NULL, false, 1, false, false, NULL, false, false, 'amount', 'currency', 2, 1, false, true, false, false, false, false, NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO request_types VALUES (2, 'Auftrag', 'AU', 'sales', 'sales', 100, true, false, false, false, false, false, true, true, true, false, false, 'none', false, 'EUR', 'turnover', 'de', 2, false, false, 11, 'calculatorByItems', 17, 22, NULL, NULL, false, 1, false, false, NULL, false, false, 'amount', 'currency', 2, 1, false, true, false, false, false, false, NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO request_types VALUES (3, 'Direktauftrag', 'DA', 'sales', 'sales', 100, true, false, true, false, true, false, true, true, true, false, false, 'none', false, 'EUR', 'turnover', 'de', 2, false, false, 15, 'calculatorByItems', 17, 22, NULL, NULL, false, 1, false, false, NULL, false, false, 'amount', 'currency', 2, 1, false, true, false, false, false, false, NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO request_types VALUES (4, 'Projekt', 'PRJ', 'project', 'generic', 100, false, false, false, true, false, false, true, true, false, false, false, NULL, false, 'EUR', 'turnover', NULL, NULL, false, false, 1, 'calculatorByItems', 17, 22, NULL, NULL, false, 1, false, false, NULL, false, false, 'amount', 'currency', 2, 2, false, true, false, false, false, false, NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO request_types VALUES (5, 'Time & Material', 'TM', 'project', 'generic', 100, false, false, false, false, false, false, false, true, true, false, false, 'none', false, 'EUR', 'turnover', 'de', NULL, true, false, 5, 'calculatorByItems', 20, 25, NULL, NULL, false, 1, false, false, NULL, true, false, 'amount', 'currency', 2, 1, false, true, false, false, false, false, NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO request_types VALUES (6, 'Laufzeit', 'LZ', 'sales', 'sales', 100, false, false, false, false, false, false, false, true, true, false, false, 'none', false, 'EUR', 'turnover', 'de', NULL, true, false, 10, 'calculatorByItems', 20, 25, NULL, NULL, false, 1, false, false, NULL, true, false, 'amount', 'currency', 2, 1, false, true, false, false, false, false, NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO request_types VALUES (102, 'Einkauf', 'EK', 'purchasing', 'purchasing', 100, false, false, false, false, true, false, false, false, true, true, false, 'none', false, NULL, NULL, 'de', 2, false, true, 102, 'calculatorByItems', 17.0, 22.0, NULL, NULL, false, 1, false, false, NULL, false, false, 'amount', 'currency', NULL, NULL, false, true, false, false, false, false, NULL, NULL, NULL, NULL, true, NULL);


--
-- Data for Name: calculation_templates; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: calculation_by_template_configs_templates_relation; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: calculation_config_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO calculation_config_types VALUES (1, 'Kalkulation', 'Liefer- und Leistungsbeschreibung', false, true);
INSERT INTO calculation_config_types VALUES (2, 'Stückliste', 'Stückliste für Kommissionierung', false, false);


--
-- Data for Name: calculation_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO calculation_configs VALUES (1, 1, 100, 'Standard-Kalkulation');
INSERT INTO calculation_configs VALUES (2, 2, 100, 'Standard-Stückliste');


--
-- Data for Name: calculation_config_allowed_redundancy_relation; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: calculation_config_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculation_config_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_config_groups_id_seq', 1, false);


--
-- Name: calculation_config_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_config_types_id_seq', 2, false);


--
-- Name: calculation_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_configs_id_seq', 2, false);


--
-- Data for Name: calculation_group_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculation_group_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_group_configs_id_seq', 1, false);


--
-- Data for Name: calculation_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculation_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_items_id_seq', 1, false);


--
-- Data for Name: calculation_names; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculation_names_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_names_id_seq', 1, false);


--
-- Data for Name: calculation_option_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculation_option_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_option_items_id_seq', 1, false);


--
-- Data for Name: calculation_option_positions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculation_option_positions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_option_positions_id_seq', 1, false);


--
-- Data for Name: calculation_positions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculation_positions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_positions_id_seq', 1, false);


--
-- Data for Name: calculation_template_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculation_template_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_template_items_id_seq', 1, false);


--
-- Name: calculation_templates_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculation_templates_id_seq', 1, false);


--
-- Data for Name: calculations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: calculations_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calculations_id_seq', 1, false);


--
-- Data for Name: calendar_months; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO calendar_months VALUES (0, 'Januar', 'Jan', false);
INSERT INTO calendar_months VALUES (1, 'Februar', 'Feb', false);
INSERT INTO calendar_months VALUES (2, 'März', 'Mrz', false);
INSERT INTO calendar_months VALUES (3, 'April', 'Apr', false);
INSERT INTO calendar_months VALUES (4, 'Mai', 'Mai', false);
INSERT INTO calendar_months VALUES (5, 'Juni', 'Jun', false);
INSERT INTO calendar_months VALUES (6, 'Juli', 'Jul', false);
INSERT INTO calendar_months VALUES (7, 'August', 'Aug', false);
INSERT INTO calendar_months VALUES (8, 'September', 'Sep', false);
INSERT INTO calendar_months VALUES (9, 'Oktober', 'Okt', false);
INSERT INTO calendar_months VALUES (10, 'November', 'Nov', false);
INSERT INTO calendar_months VALUES (11, 'Dezember', 'Dez', false);


--
-- Data for Name: calendar_years; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO calendar_years VALUES (2010, '2010', false);
INSERT INTO calendar_years VALUES (2011, '2011', false);
INSERT INTO calendar_years VALUES (2012, '2012', false);
INSERT INTO calendar_years VALUES (2013, '2013', false);
INSERT INTO calendar_years VALUES (2014, '2014', false);
INSERT INTO calendar_years VALUES (2015, '2015', false);
INSERT INTO calendar_years VALUES (2016, '2016', false);
INSERT INTO calendar_years VALUES (2017, '2017', false);
INSERT INTO calendar_years VALUES (2018, '2018', false);
INSERT INTO calendar_years VALUES (2019, '2019', false);
INSERT INTO calendar_years VALUES (2020, '2020', false);
INSERT INTO calendar_years VALUES (2021, '2021', false);
INSERT INTO calendar_years VALUES (2022, '2022', false);
INSERT INTO calendar_years VALUES (2023, '2023', false);
INSERT INTO calendar_years VALUES (2024, '2024', false);
INSERT INTO calendar_years VALUES (2025, '2025', false);
INSERT INTO calendar_years VALUES (2026, '2026', false);
INSERT INTO calendar_years VALUES (2027, '2027', false);
INSERT INTO calendar_years VALUES (2028, '2028', false);
INSERT INTO calendar_years VALUES (2029, '2029', false);
INSERT INTO calendar_years VALUES (2030, '2030', false);


--
-- Data for Name: contact_countries; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_countries VALUES (1, 'Afghanistan', '93', 'AF', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (360, 'Montenegro', '382', 'ME', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (403, 'Serbia', '381', 'RS', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (241, 'Andorra', '376', 'AD', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (436, 'Vereinigte Arabische Emirate', '971', 'AE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (244, 'Antigua und Barbuda', '1', 'AG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (243, 'Anguilla', '1', 'AI', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (238, 'Albanien', '355', 'AL', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (246, 'Armenien', '374', 'AM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (367, 'Niederländische Antillen', '599', 'AN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (242, 'Angola', '244', 'AO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (245, 'Argentinien', '54', 'AR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (240, 'Samoa', '1', 'AS', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (172, 'Österreich', '43', 'AT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (248, 'Australien', '61', 'AU', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (247, 'Aruba', '297', 'AW', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (249, 'Aserbaidschan', '994', 'AZ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (260, 'Bosnien-Herzegowina', '387', 'BA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (253, 'Barbados', '1', 'BB', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (252, 'Bangladesh', '880', 'BD', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (28, 'Belgien', '32', 'BE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (264, 'Burkina Faso', '226', 'BF', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (39, 'Bulgarien', '359', 'BG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (251, 'Bahrain', '973', 'BH', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (265, 'Burundi', '257', 'BI', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (256, 'Benin', '229', 'BJ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (257, 'Bermudas', '1', 'BM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (263, 'Brunei', '673', 'BN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (259, 'Bolivien', '591', 'BO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (262, 'Brasilien', '55', 'BR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (250, 'Bahamas', '1', 'BS', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (258, 'Bhutan', '975', 'BT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (261, 'Botswana', '267', 'BW', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (254, 'Weißrussland', '375', 'BY', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (268, 'Kanada', '1', 'CA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (275, 'Kokosinseln', '61', 'CC', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (279, 'Demokratische Republik Kongo', '243', 'CD', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (271, 'Zentralafrikanische Republik', '236', 'CF', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (278, 'Kongo', '242', 'CG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (197, 'Schweiz', '41', 'CH', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (285, 'Elfenbeinküste', '225', 'CI', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (280, 'Cook-Inseln', '682', 'CK', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (273, 'Chile', '56', 'CL', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (267, 'Kamerun', '237', 'CM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (47, 'China', '86', 'CN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (276, 'Kolumbien', '57', 'CO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (281, 'Costa Rica', '506', 'CR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (283, 'Kuba', '53', 'CU', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (269, 'Kap Verde', '238', 'CV', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (274, 'Christmas Island', '618', 'CX', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (284, 'Zypern', '357', 'CY', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (227, 'Tschechische Republik', '420', 'CZ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (56, 'Djibuti', '253', 'DJ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (286, 'Dänemark', '45', 'DK', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (287, 'Dominika', '1', 'DM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (288, 'Dominikanische Republik', '1', 'DO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (239, 'Algerien', '213', 'DZ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (289, 'Ecuador', '593', 'EC', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (294, 'Estland', '372', 'EE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (290, 'Ägypten', '20', 'EG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (293, 'Eritrea', '291', 'ER', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (206, 'Spanien', '34', 'ES', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (295, 'Äthiopien', '251', 'ET', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (298, 'Finnland', '358', 'FI', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (297, 'Fidschi-Inseln', '679', 'FJ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (63, 'Falkland-Inseln', '500', 'FK', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (356, 'Mikronesien', '691', 'FM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (296, 'Färöer Inseln', '298', 'FO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (67, 'Frankreich', '33', 'FR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (301, 'Gabun', '241', 'GA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (307, 'Grenada', '1', 'GD', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (303, 'Georgien', '995', 'GE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (299, 'französisch Guyana', '594', 'GF', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (304, 'Ghana', '233', 'GH', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (305, 'Gibraltar', '350', 'GI', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (306, 'Grönland', '299', 'GL', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (302, 'Gambia', '220', 'GM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (311, 'Guinea', '224', 'GN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (308, 'Guadeloupe', '590', 'GP', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (292, 'Äquatorial Guinea', '240', 'GQ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (78, 'Griechenland', '30', 'GR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (310, 'Guatemala', '502', 'GT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (309, 'Guam', '1', 'GU', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (312, 'Guinea Bissau', '245', 'GW', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (313, 'Guyana', '592', 'GY', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (317, 'Hong Kong', '852', 'HK', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (316, 'Honduras', '504', 'HN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (282, 'Kroatien', '385', 'HR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (314, 'Haiti', '509', 'HT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (318, 'Ungarn', '36', 'HU', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (320, 'Indonesien', '62', 'ID', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (322, 'Irland', '353', 'IE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (323, 'Israel', '972', 'IL', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (91, 'Indien', '91', 'IN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (321, 'Irak', '964', 'IQ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (94, 'Iran', '98', 'IR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (319, 'Island', '354', 'IS', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (98, 'Italien', '39', 'IT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (324, 'Jamaika', '1', 'JM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (326, 'Jordanien', '962', 'JO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (325, 'Japan', '81', 'JP', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (328, 'Kenia', '254', 'KE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (333, 'Kirgisistan', '996', 'KG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (266, 'Kambodscha', '855', 'KH', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (329, 'Kiribati', '686', 'KI', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (277, 'Komoren', '269', 'KM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (394, 'St. Kitts Nevis Anguilla', '1', 'KN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (330, 'Nordkorea', '850', 'KP', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (331, 'Südkorea', '82', 'KR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (332, 'Kuwait', '965', 'KW', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (270, 'Kaiman-Inseln', '1', 'KY', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (327, 'Kasachstan', '7', 'KZ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (334, 'Laos', '856', 'LA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (336, 'Libanon', '961', 'LB', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (395, 'Saint Lucia', '1', 'LC', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (131, 'Liechtenstein', '423', 'LI', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (412, 'Sri Lanka', '94', 'LK', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (338, 'Liberia', '231', 'LR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (337, 'Lesotho', '266', 'LS', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (340, 'Litauen', '370', 'LT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (341, 'Luxemburg', '352', 'LU', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (335, 'Lettland', '371', 'LV', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (339, 'Libyen', '218', 'LY', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (362, 'Marokko', '212', 'MA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (358, 'Monaco', '377', 'MC', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (357, 'Moldavien', '373', 'MD', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (344, 'Madagaskar', '261', 'MG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (350, 'Marshall-Inseln', '692', 'MH', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (343, 'Mazedonien', '389', 'MK', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (348, 'Mali', '223', 'ML', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (364, 'Myanmar', '95', 'MM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (359, 'Mongolei', '976', 'MN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (342, 'Macao', '853', 'MO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (375, 'Marianen', '1', 'MP', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (351, 'Martinique', '596', 'MQ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (352, 'Mauretanien', '222', 'MR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (361, 'Montserrat', '1', 'MS', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (349, 'Malta', '356', 'MT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (353, 'Mauritius', '230', 'MU', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (347, 'Malediven', '960', 'MV', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (345, 'Malawi', '265', 'MW', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (355, 'Mexiko', '52', 'MX', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (346, 'Malaysia', '60', 'MY', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (363, 'Mocambique', '258', 'MZ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (365, 'Namibia', '264', 'NA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (368, 'Neukaledonien', '687', 'NC', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (371, 'Niger', '227', 'NE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (374, 'Norfolk-Inseln', '672', 'NF', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (372, 'Nigeria', '234', 'NG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (370, 'Nicaragua', '505', 'NI', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (163, 'Niederlande', '31', 'NL', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (376, 'Norwegen', '47', 'NO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (423, 'Nepal', '977', 'NP', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (366, 'Nauru', '674', 'NR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (373, 'Niue', '683', 'NU', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (369, 'Neuseeland', '64', 'NZ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (377, 'Oman', '968', 'OM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (381, 'Panama', '507', 'PA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (384, 'Peru', '51', 'PE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (300, 'Französisch-Polynesien', '689', 'PF', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (382, 'Papua Neuguinea', '675', 'PG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (385, 'Philippinen', '63', 'PH', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (378, 'Pakistan', '92', 'PK', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (387, 'Polen', '48', 'PL', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (396, 'St. Pierre und Miquelon', '508', 'PM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (386, 'Pitcairn', '872', 'PN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (388, 'Puerto Rico', '1', 'PR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (380, 'Palästinensische Selbstverwaltungsgebiete', '970', 'PS', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (183, 'Portugal', '351', 'PT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (379, 'Palau', '680', 'PW', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (383, 'Paraguay', '595', 'PY', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (389, 'Qatar', '974', 'QA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (392, 'Reunion', '262', 'RE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (390, 'Rumänien', '40', 'RO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (188, 'Russland', '7', 'RU', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (391, 'Ruanda', '250', 'RW', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (401, 'Saudi-Arabien', '966', 'SA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (409, 'Solomon-Inseln', '677', 'SB', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (404, 'Seychellen', '248', 'SC', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (413, 'Sudan', '249', 'SD', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (417, 'Schweden', '46', 'SE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (393, 'St. Helena', '290', 'SH', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (408, 'Slowenien', '386', 'SI', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (415, 'Svalbard und Jan Mayen Islands', '47', 'SJ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (407, 'Slowakei (Slowakische Republik)', '421', 'SK', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (405, 'Sierra Leone', '232', 'SL', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (399, 'San Marino', '378', 'SM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (402, 'Senegal', '221', 'SN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (410, 'Somalia', '252', 'SO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (414, 'Surinam', '597', 'SR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (400, 'Sao Tome', '239', 'ST', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (291, 'El Salvador', '503', 'SV', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (418, 'Syrien', '963', 'SY', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (416, 'Swasiland', '268', 'SZ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (432, 'Turks- und Kaikos-Inseln', '1', 'TC', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (272, 'Tschad', '235', 'TD', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (425, 'Togo', '228', 'TG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (422, 'Thailand', '66', 'TH', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (420, 'Tadschikistan', '992', 'TJ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (426, 'Tokelau', '690', 'TK', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (424, 'Ost-Timor', '670', 'TL', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (431, 'Turkmenistan', '993', 'TM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (429, 'Tunesien', '216', 'TN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (427, 'Tonga', '676', 'TO', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (430, 'Türkei', '90', 'TR', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (428, 'Trinidad Tobago', '1', 'TT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (433, 'Tuvalu', '688', 'TV', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (419, 'Taiwan', '886', 'TW', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (421, 'Tansania', '255', 'TZ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (435, 'Ukraine', '380', 'UA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (434, 'Uganda', '256', 'UG', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (437, 'Uruguay', '598', 'UY', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (438, 'Usbekistan', '998', 'UZ', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (315, 'Vatikan', '39', 'VA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (397, 'St. Vincent', '1', 'VC', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (440, 'Venezuela', '58', 'VE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (441, 'Vietnam', '84', 'VN', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (439, 'Vanuatu', '678', 'VU', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (443, 'Wallis et Futuna', '681', 'WF', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (398, 'Samoa', '685', 'WS', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (444, 'Jemen', '967', 'YE', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (354, 'Mayotte', '269', 'YT', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (411, 'Südafrika', '27', 'ZA', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (445, 'Sambia', '260', 'ZM', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (446, 'Zimbabwe', '263', 'ZW', false, 230, false, true, false);
INSERT INTO contact_countries VALUES (53, 'Deutschland', '49', 'DE', false, 230, true, false, false);
INSERT INTO contact_countries VALUES (255, 'Belize', '501', 'BZ', false, 230, false, true, true);
INSERT INTO contact_countries VALUES (60, 'Großbritannien (UK)', '44', 'GB', false, 230, false, true, true);
INSERT INTO contact_countries VALUES (406, 'Singapur', '65', 'SG', false, 230, false, true, true);
INSERT INTO contact_countries VALUES (237, 'Vereinigte Staaten von Amerika', '1', 'US', false, 230, false, true, true);
INSERT INTO contact_countries VALUES (37, 'Virgin Island (Brit.)', '1', 'VG', false, 230, false, true, true);
INSERT INTO contact_countries VALUES (442, 'Virgin Island (USA)', '1', 'VI', false, 230, false, true, true);


--
-- Data for Name: calendar_public_holiday; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO calendar_public_holiday VALUES (401, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (402, 2015, 53, 'Heilige Drei Könige', '2015-01-06 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (403, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (404, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (405, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (406, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (407, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (408, 2015, 53, 'Fronleichnam', '2015-06-04 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (409, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (410, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (411, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 1);
INSERT INTO calendar_public_holiday VALUES (412, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (413, 2015, 53, 'Heilige Drei Könige', '2015-01-06 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (414, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (415, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (416, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (417, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (418, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (419, 2015, 53, 'Fronleichnam', '2015-06-04 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (420, 2015, 53, 'Mariä Himmelfahrt', '2015-08-15 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (421, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (422, 2015, 53, 'Allerheiligen', '2015-11-01 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (423, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (424, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 2);
INSERT INTO calendar_public_holiday VALUES (425, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 3);
INSERT INTO calendar_public_holiday VALUES (426, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 3);
INSERT INTO calendar_public_holiday VALUES (427, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 3);
INSERT INTO calendar_public_holiday VALUES (428, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 3);
INSERT INTO calendar_public_holiday VALUES (429, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 3);
INSERT INTO calendar_public_holiday VALUES (430, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 3);
INSERT INTO calendar_public_holiday VALUES (431, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 3);
INSERT INTO calendar_public_holiday VALUES (432, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 3);
INSERT INTO calendar_public_holiday VALUES (433, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 3);
INSERT INTO calendar_public_holiday VALUES (434, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (435, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (436, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (437, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (438, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (439, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (440, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (441, 2015, 53, 'Reformationstag', '2015-10-31 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (442, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (443, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 4);
INSERT INTO calendar_public_holiday VALUES (444, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 5);
INSERT INTO calendar_public_holiday VALUES (445, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 5);
INSERT INTO calendar_public_holiday VALUES (446, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 5);
INSERT INTO calendar_public_holiday VALUES (447, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 5);
INSERT INTO calendar_public_holiday VALUES (448, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 5);
INSERT INTO calendar_public_holiday VALUES (449, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 5);
INSERT INTO calendar_public_holiday VALUES (450, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 5);
INSERT INTO calendar_public_holiday VALUES (451, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 5);
INSERT INTO calendar_public_holiday VALUES (452, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 5);
INSERT INTO calendar_public_holiday VALUES (453, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 6);
INSERT INTO calendar_public_holiday VALUES (454, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 6);
INSERT INTO calendar_public_holiday VALUES (455, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 6);
INSERT INTO calendar_public_holiday VALUES (456, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 6);
INSERT INTO calendar_public_holiday VALUES (457, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 6);
INSERT INTO calendar_public_holiday VALUES (458, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 6);
INSERT INTO calendar_public_holiday VALUES (459, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 6);
INSERT INTO calendar_public_holiday VALUES (460, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 6);
INSERT INTO calendar_public_holiday VALUES (461, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 6);
INSERT INTO calendar_public_holiday VALUES (462, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (463, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (464, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (465, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (466, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (467, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (468, 2015, 53, 'Fronleichnam', '2015-06-04 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (469, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (470, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (471, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 7);
INSERT INTO calendar_public_holiday VALUES (472, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (473, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (474, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (475, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (476, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (477, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (478, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (479, 2015, 53, 'Reformationstag', '2015-10-31 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (480, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (481, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 8);
INSERT INTO calendar_public_holiday VALUES (482, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 9);
INSERT INTO calendar_public_holiday VALUES (483, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 9);
INSERT INTO calendar_public_holiday VALUES (484, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 9);
INSERT INTO calendar_public_holiday VALUES (485, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 9);
INSERT INTO calendar_public_holiday VALUES (486, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 9);
INSERT INTO calendar_public_holiday VALUES (487, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 9);
INSERT INTO calendar_public_holiday VALUES (488, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 9);
INSERT INTO calendar_public_holiday VALUES (489, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 9);
INSERT INTO calendar_public_holiday VALUES (490, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 9);
INSERT INTO calendar_public_holiday VALUES (491, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (492, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (493, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (494, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (495, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (496, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (497, 2015, 53, 'Fronleichnam', '2015-06-04 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (498, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (499, 2015, 53, 'Allerheiligen', '2015-11-01 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (500, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (501, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 10);
INSERT INTO calendar_public_holiday VALUES (502, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (503, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (504, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (505, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (506, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (507, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (508, 2015, 53, 'Fronleichnam', '2015-06-04 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (509, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (510, 2015, 53, 'Allerheiligen', '2015-11-01 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (511, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (512, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 11);
INSERT INTO calendar_public_holiday VALUES (513, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (514, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (515, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (516, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (517, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (518, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (519, 2015, 53, 'Fronleichnam', '2015-06-04 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (520, 2015, 53, 'Mariä Himmelfahrt', '2015-08-15 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (521, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (522, 2015, 53, 'Allerheiligen', '2015-11-01 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (523, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (524, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 12);
INSERT INTO calendar_public_holiday VALUES (525, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (526, 2015, 53, 'Heilige Drei Könige', '2015-01-06 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (527, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (528, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (529, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (530, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (531, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (532, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (533, 2015, 53, 'Reformationstag', '2015-10-31 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (534, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (535, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 13);
INSERT INTO calendar_public_holiday VALUES (536, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (537, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (538, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (539, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (540, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (541, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (542, 2015, 53, 'Fronleichnam', '2015-06-04 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (543, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (544, 2015, 53, 'Reformationstag', '2015-10-31 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (545, 2015, 53, 'Buß- und Bettag', '2015-11-18 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (546, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (547, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 14);
INSERT INTO calendar_public_holiday VALUES (548, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 15);
INSERT INTO calendar_public_holiday VALUES (549, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 15);
INSERT INTO calendar_public_holiday VALUES (550, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 15);
INSERT INTO calendar_public_holiday VALUES (551, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 15);
INSERT INTO calendar_public_holiday VALUES (552, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 15);
INSERT INTO calendar_public_holiday VALUES (553, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 15);
INSERT INTO calendar_public_holiday VALUES (554, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 15);
INSERT INTO calendar_public_holiday VALUES (555, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 15);
INSERT INTO calendar_public_holiday VALUES (556, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 15);
INSERT INTO calendar_public_holiday VALUES (557, 2015, 53, 'Neujahr', '2015-01-01 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (558, 2015, 53, 'Karfreitag', '2015-04-03 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (559, 2015, 53, 'Ostermontag', '2015-04-06 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (560, 2015, 53, 'Maifeiertag', '2015-05-01 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (561, 2015, 53, 'Christi Himmelfahrt', '2015-05-14 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (562, 2015, 53, 'Pfingstmontag', '2015-05-25 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (563, 2015, 53, 'Fronleichnam', '2015-06-04 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (564, 2015, 53, 'Tag der Deutschen Einheit', '2015-10-03 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (565, 2015, 53, 'Reformationstag', '2015-10-31 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (566, 2015, 53, '1. Weihnachtstag', '2015-12-25 00:00:00', NULL, 16);
INSERT INTO calendar_public_holiday VALUES (567, 2015, 53, '2. Weihnachtstag', '2015-12-26 00:00:00', NULL, 16);


--
-- Name: calendar_public_holiday_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('calendar_public_holiday_id_seq', 600, false);


--
-- Data for Name: campaign_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO campaign_groups VALUES (1, false, 'Empfehlung', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_groups VALUES (2, false, 'Internet', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_groups VALUES (3, false, 'Promotion', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_groups VALUES (4, false, 'Sponsoring', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_groups VALUES (5, false, 'Veranstaltung', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_groups VALUES (6, false, 'Print', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_groups VALUES (7, false, 'Sonstiges', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);


--
-- Data for Name: campaign_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO campaign_types VALUES (1, false, 'Internet allgemein', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_types VALUES (2, false, 'Internet Landing Page', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_types VALUES (3, false, 'Presse Anzeige', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_types VALUES (4, false, 'Presse Artikel', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_types VALUES (5, false, 'Gewinnspiel', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_types VALUES (6, false, 'Mailing', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO campaign_types VALUES (7, false, 'Sonstiges', NULL, '1970-01-01 00:00:00', 6000, NULL, NULL);


--
-- Data for Name: campaigns; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO campaigns VALUES (1, NULL, false, 'Akquise', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, true, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (2, 1, false, 'Akquise (direkt durch Vertrieb)', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 7, 7, false, 'reachCompanyUp', 100, 1, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (3, NULL, false, 'Empfehlungen', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, true, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (4, 3, false, 'Empfehlungen (ohne Aktion)', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 1, 7, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (5, NULL, false, 'Adressverzeichnisse', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, true, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (6, 5, false, 'Das Örtliche', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 2, 3, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, 100);
INSERT INTO campaigns VALUES (7, 5, false, 'Das Telefonbuch', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 2, 3, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, 105);
INSERT INTO campaigns VALUES (8, 5, false, 'Gelbe Seiten', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 2, 3, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (9, NULL, false, 'Anzeigen (kampagnenunabhängig)', 'Einzelanzeigen, die alleine für sich stehend geschaltet werden und keiner Kampagne zugehörig sind.', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (10, NULL, false, 'Internet (kampagnenunabhängig)', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, true, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (11, 10, false, 'Online Recherche', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 2, 7, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (12, NULL, false, 'Kontakt über Lieferant/Partner', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, true, 'reachCompanyUp', 100, 1, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (13, 12, false, 'Lieferant', 'Empfehlung über Lieferant', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 1, 7, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (14, 12, false, 'Partnerunternehmen', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 1, 7, false, 'reachCompanyUp', 100, 1, '1970-01-01 00:00:00', NULL, 17);
INSERT INTO campaigns VALUES (15, NULL, false, 'Adresskauf', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, true, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (16, 15, false, 'Schober', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 2, 7, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, 108);
INSERT INTO campaigns VALUES (17, NULL, false, 'Bestandskunde', 'Wiederholter Kauf.', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (18, NULL, false, 'Sonstige Herkunft', NULL, NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, true, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (19, 18, false, 'Firma bekannt', 'Sollte die Herkunft keiner genauen Kampagne zugeordnet werden können.', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 7, 7, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);
INSERT INTO campaigns VALUES (20, 18, false, 'Importiert', 'Datensatz wurde importiert', NULL, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 7, 7, false, 'reachCompanyUp', 100, NULL, '1970-01-01 00:00:00', NULL, NULL);


--
-- Data for Name: customer_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO customer_status VALUES (1, 'Endverbraucher', false);
INSERT INTO customer_status VALUES (2, 'Gewerbetrieb', false);
INSERT INTO customer_status VALUES (3, 'KMU', false);
INSERT INTO customer_status VALUES (4, 'Konzern', false);
INSERT INTO customer_status VALUES (5, 'Handel', false);
INSERT INTO customer_status VALUES (6, 'Systemhaus', false);
INSERT INTO customer_status VALUES (7, 'Freie Berufe', false);
INSERT INTO customer_status VALUES (8, 'Kooperationspartner', false);
INSERT INTO customer_status VALUES (9, 'Öffentliche Einrichtung', false);


--
-- Data for Name: customer_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO customer_types VALUES (1, 'Endverbraucher', false);
INSERT INTO customer_types VALUES (2, 'Wiederverkäufer', false);
INSERT INTO customer_types VALUES (3, 'Barverkauf', true);
INSERT INTO customer_types VALUES (4, 'Geschäftspartner', true);
INSERT INTO customer_types VALUES (5, 'Mandant', true);
INSERT INTO customer_types VALUES (6, 'Holding', true);


--
-- Data for Name: customers; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO customers VALUES (30000, 1000000000, 5, 2, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, false, 0, 0, 0, false, false, false, 0, false, NULL, NULL, true, NULL, false, 0, NULL, NULL);
INSERT INTO customers VALUES (30001, 1000000003, 3, 1, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, false, 0, 0, 0, false, false, false, 0, false, NULL, NULL, true, NULL, false, 0, NULL, 'POS');
UPDATE system_company_branchs SET pos_account_id = 30001 WHERE id = 1;

--
-- Data for Name: request_origin_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_origin_types VALUES (1, 'Telefon', false, 0);
INSERT INTO request_origin_types VALUES (2, 'Fax', false, 0);
INSERT INTO request_origin_types VALUES (3, 'Email', false, 0);
INSERT INTO request_origin_types VALUES (4, 'Akquise', false, 0);
INSERT INTO request_origin_types VALUES (5, 'Callcenter', false, 0);
INSERT INTO request_origin_types VALUES (6, 'Postweg', false, 0);
INSERT INTO request_origin_types VALUES (7, 'Kontaktformular', false, 0);
INSERT INTO request_origin_types VALUES (8, 'Vermittlung', false, 0);


--
-- Data for Name: request_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_status VALUES (-6, 'Storniert, versehentlich angelegt', false);
INSERT INTO request_status VALUES (-5, 'Storniert, jetzt neue Anfrage', false);
INSERT INTO request_status VALUES (-4, 'Storniert, kein Interesse', false);
INSERT INTO request_status VALUES (-3, 'Storniert, an Wettbewerb verloren', false);
INSERT INTO request_status VALUES (-2, 'Storniert, verloren, sonstiges', false);
INSERT INTO request_status VALUES (-1, 'gestoppt, wartend', false);
INSERT INTO request_status VALUES (0, 'neu', false);
INSERT INTO request_status VALUES (1, 'importiert', false);
INSERT INTO request_status VALUES (2, 'Zurückgesetzt', false);
INSERT INTO request_status VALUES (5, 'Unvollständig', false);
INSERT INTO request_status VALUES (10, 'Anfrage erfasst', false);
INSERT INTO request_status VALUES (15, 'Vertrieb zugeordnet', false);
INSERT INTO request_status VALUES (20, 'Kontakt aufgenommen', false);
INSERT INTO request_status VALUES (22, 'Informationsmaterial per Mail', false);
INSERT INTO request_status VALUES (24, 'Informationsmaterial per Post', false);
INSERT INTO request_status VALUES (25, 'Informationsmaterial versendet', false);
INSERT INTO request_status VALUES (30, 'Gesprächstermin', false);
INSERT INTO request_status VALUES (45, 'Datenerfassung abgeschlossen', false);
INSERT INTO request_status VALUES (50, 'Kalkulation/Angebot', false);
INSERT INTO request_status VALUES (55, 'Kalkulation/Angebot', false);
INSERT INTO request_status VALUES (60, 'Angebot versendet', false);
INSERT INTO request_status VALUES (85, 'Abschlusstermin vereinbart', false);
INSERT INTO request_status VALUES (100, 'Auftrag', false);


--
-- Data for Name: project_plans; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: campaign_assignment_history; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: campaign_assignment_history_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('campaign_assignment_history_id_seq', 1, false);


--
-- Name: campaign_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('campaign_groups_id_seq', 7, true);


--
-- Name: campaign_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('campaign_types_id_seq', 7, true);


--
-- Name: campaigns_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('campaigns_id_seq', 20, true);


--
-- Data for Name: contact_backups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: contact_backups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_backups_id_seq', 1, false);


--
-- Data for Name: contact_bank_accounts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: contact_bank_accounts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_bank_accounts_id_seq', 1, false);


--
-- Name: contact_countries_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_countries_id_seq', 446, true);


--
-- Data for Name: contact_districts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_districts VALUES (1, 'Aachen', 'AC', 10, false);
INSERT INTO contact_districts VALUES (2, 'Ahrweiler [Bad Neuenahr-Ahrweiler]', 'AW', 11, false);
INSERT INTO contact_districts VALUES (3, 'Aichach-Friedberg [Aichach]', 'AIC', 2, false);
INSERT INTO contact_districts VALUES (4, 'Alb-Donau-Kreis [Ulm]', 'UL', 1, false);
INSERT INTO contact_districts VALUES (5, 'Altenburger Land [Altenburg]', 'ABG', 16, false);
INSERT INTO contact_districts VALUES (6, 'Altenkirchen', 'AK', 11, false);
INSERT INTO contact_districts VALUES (7, 'Altmarkkreis Salzwedel [Salzwedel]', 'SAW', 14, false);
INSERT INTO contact_districts VALUES (8, 'Altötting', 'AÖ', 2, false);
INSERT INTO contact_districts VALUES (9, 'Alzey-Worms [Alzey]', 'AZ', 11, false);
INSERT INTO contact_districts VALUES (10, 'Amberg, Stadt', 'AM', 2, false);
INSERT INTO contact_districts VALUES (11, 'Amberg-Sulzbach [Amberg]', 'AS', 2, false);
INSERT INTO contact_districts VALUES (12, 'Ammerland [Westerstede]', 'WST', 9, false);
INSERT INTO contact_districts VALUES (13, 'Anhalt-Zerbst [Zerbst]', 'AZE', 14, false);
INSERT INTO contact_districts VALUES (14, 'Annaberg [Annaberg-Buchholz]', 'ANA', 13, false);
INSERT INTO contact_districts VALUES (15, 'Ansbach', 'AN', 2, false);
INSERT INTO contact_districts VALUES (16, 'Aschaffenburg', 'AB', 2, false);
INSERT INTO contact_districts VALUES (17, 'Aschersleben-Staßfurt [Aschersleben]', 'ASL', 14, false);
INSERT INTO contact_districts VALUES (18, 'Aue-Schwarzenberg [Aue]', 'ASZ', 13, false);
INSERT INTO contact_districts VALUES (19, 'Augsburg', 'A', 2, false);
INSERT INTO contact_districts VALUES (20, 'Aurich', 'AUR', 9, false);
INSERT INTO contact_districts VALUES (21, 'Bad Doberan', 'DBR', 8, false);
INSERT INTO contact_districts VALUES (22, 'Bad Dürkheim', 'DÜW', 11, false);
INSERT INTO contact_districts VALUES (23, 'Baden-Baden, Stadt', 'BAD', 1, false);
INSERT INTO contact_districts VALUES (24, 'Bad Kissingen', 'KG', 2, false);
INSERT INTO contact_districts VALUES (25, 'Bad Kreuznach', 'KH', 11, false);
INSERT INTO contact_districts VALUES (26, 'Bad Tölz-Wolfratshausen [Bad Tölz]', 'TÖL', 2, false);
INSERT INTO contact_districts VALUES (27, 'Bamberg', 'BA', 2, false);
INSERT INTO contact_districts VALUES (28, 'Barnim [Eberswalde]', 'BAR', 4, false);
INSERT INTO contact_districts VALUES (29, 'Bautzen', 'BZ', 13, false);
INSERT INTO contact_districts VALUES (30, 'Bayreuth', 'BT', 2, false);
INSERT INTO contact_districts VALUES (31, 'Berchtesgadener Land [Bad Reichenhall]', 'BGL', 2, false);
INSERT INTO contact_districts VALUES (32, 'Bergstraße [Heppenheim]', 'HP', 7, false);
INSERT INTO contact_districts VALUES (33, 'Berlin', 'B', 3, false);
INSERT INTO contact_districts VALUES (34, 'Bernburg', 'BBG', 14, false);
INSERT INTO contact_districts VALUES (35, 'Bernkastel-Wittlich [Wittlich]', 'WIL', 11, false);
INSERT INTO contact_districts VALUES (36, 'Biberach', 'BC', 1, false);
INSERT INTO contact_districts VALUES (37, 'Bielefeld, Stadt', 'BI', 10, false);
INSERT INTO contact_districts VALUES (38, 'Birkenfeld', 'BIR', 11, false);
INSERT INTO contact_districts VALUES (39, 'Bitburg-Prüm [Bitburg]', 'BIT', 11, false);
INSERT INTO contact_districts VALUES (40, 'Bitterfeld', 'BTF', 14, false);
INSERT INTO contact_districts VALUES (41, 'Böblingen', 'BB', 1, false);
INSERT INTO contact_districts VALUES (42, 'Bochum, Stadt', 'BO', 10, false);
INSERT INTO contact_districts VALUES (43, 'Bodenseekreis [Friedrichshafen]', 'FN', 1, false);
INSERT INTO contact_districts VALUES (44, 'Bonn, Stadt', 'BN', 10, false);
INSERT INTO contact_districts VALUES (45, 'Bördekreis [Oschersleben]', 'BÖ', 14, false);
INSERT INTO contact_districts VALUES (46, 'Borken', 'BOR', 10, false);
INSERT INTO contact_districts VALUES (47, 'Bottrop, Stadt', 'BOT', 10, false);
INSERT INTO contact_districts VALUES (48, 'Brandenburg, Stadt', 'BRB', 4, false);
INSERT INTO contact_districts VALUES (49, 'Braunschweig, Stadt', 'BS', 9, false);
INSERT INTO contact_districts VALUES (50, 'Breisgau-Hochschwarzwald [Freiburg]', 'FR', 1, false);
INSERT INTO contact_districts VALUES (51, 'Burgenlandkreis [Naumburg]', 'BLK', 14, false);
INSERT INTO contact_districts VALUES (52, 'Calw', 'CW', 1, false);
INSERT INTO contact_districts VALUES (53, 'Celle', 'CE', 9, false);
INSERT INTO contact_districts VALUES (54, 'Cham', 'CHA', 2, false);
INSERT INTO contact_districts VALUES (55, 'Chemnitzer Land [Glauchau]', 'GC', 13, false);
INSERT INTO contact_districts VALUES (56, 'Chemnitz, Stadt', 'C', 13, false);
INSERT INTO contact_districts VALUES (57, 'Cloppenburg', 'CLP', 9, false);
INSERT INTO contact_districts VALUES (58, 'Coburg', 'CO', 2, false);
INSERT INTO contact_districts VALUES (59, 'Cochem-Zell [Cochem]', 'COC', 11, false);
INSERT INTO contact_districts VALUES (60, 'Coesfeld', 'COE', 10, false);
INSERT INTO contact_districts VALUES (61, 'Cottbus, Stadt', 'CB', 4, false);
INSERT INTO contact_districts VALUES (62, 'Cuxhaven', 'CUX', 9, false);
INSERT INTO contact_districts VALUES (63, 'Dachau', 'DAH', 2, false);
INSERT INTO contact_districts VALUES (64, 'Dahme-Spreewald [Lübben]', 'LDS', 4, false);
INSERT INTO contact_districts VALUES (65, 'Darmstadt-Dieburg [Darmstadt]', 'DA', 7, false);
INSERT INTO contact_districts VALUES (66, 'Daun', 'DAU', 11, false);
INSERT INTO contact_districts VALUES (67, 'Deggendorf', 'DEG', 2, false);
INSERT INTO contact_districts VALUES (68, 'Delitzsch', 'DZ', 13, false);
INSERT INTO contact_districts VALUES (69, 'Delmenhorst, Stadt', 'DEL', 9, false);
INSERT INTO contact_districts VALUES (70, 'Demmin', 'DM', 8, false);
INSERT INTO contact_districts VALUES (71, 'Dessau, Stadt', 'DE', 14, false);
INSERT INTO contact_districts VALUES (72, 'Diepholz', 'DH', 9, false);
INSERT INTO contact_districts VALUES (73, 'Dillingen a.d. Donau', 'DLG', 2, false);
INSERT INTO contact_districts VALUES (74, 'Dingolfing-Landau [Dingolfing]', 'DGF', 2, false);
INSERT INTO contact_districts VALUES (75, 'Dithmarschen [Heide]', 'HEI', 15, false);
INSERT INTO contact_districts VALUES (76, 'Döbeln', 'DL', 13, false);
INSERT INTO contact_districts VALUES (77, 'Donau-Ries [Donauwörth]', 'DON', 2, false);
INSERT INTO contact_districts VALUES (78, 'Donnersbergkreis [Kirchheimbolanden]', 'KIB', 11, false);
INSERT INTO contact_districts VALUES (79, 'Dortmund, Stadt', 'DO', 10, false);
INSERT INTO contact_districts VALUES (80, 'Dresden, Stadt', 'DD', 13, false);
INSERT INTO contact_districts VALUES (81, 'Duisburg, Stadt', 'DU', 10, false);
INSERT INTO contact_districts VALUES (82, 'Düren', 'DN', 10, false);
INSERT INTO contact_districts VALUES (83, 'Düsseldorf, Stadt', 'D', 10, false);
INSERT INTO contact_districts VALUES (84, 'Ebersberg', 'EBE', 2, false);
INSERT INTO contact_districts VALUES (85, 'Eichsfeld [Heiligenstadt]', 'EIC', 16, false);
INSERT INTO contact_districts VALUES (86, 'Eichstätt', 'EI', 2, false);
INSERT INTO contact_districts VALUES (87, 'Eisenach, Stadt', 'EA', 16, false);
INSERT INTO contact_districts VALUES (88, 'Elbe-Elster [Herzberg]', 'EE', 4, false);
INSERT INTO contact_districts VALUES (89, 'Emden, Stadt', 'EMD', 9, false);
INSERT INTO contact_districts VALUES (90, 'Emmendingen', 'EM', 1, false);
INSERT INTO contact_districts VALUES (91, 'Emsland [Meppen]', 'EL', 9, false);
INSERT INTO contact_districts VALUES (92, 'Ennepe-Ruhr-Kreis [Schwelm]', 'EN', 10, false);
INSERT INTO contact_districts VALUES (93, 'Enzkreis [Pforzheim]', 'PF', 1, false);
INSERT INTO contact_districts VALUES (94, 'Erding', 'ED', 2, false);
INSERT INTO contact_districts VALUES (95, 'Erfurt, Stadt', 'EF', 16, false);
INSERT INTO contact_districts VALUES (96, 'Erlangen-Höchstadt [Erlangen]', 'ERH', 2, false);
INSERT INTO contact_districts VALUES (97, 'Erlangen, Stadt', 'ER', 2, false);
INSERT INTO contact_districts VALUES (98, 'Essen, Stadt', 'E', 10, false);
INSERT INTO contact_districts VALUES (99, 'Esslingen', 'ES', 1, false);
INSERT INTO contact_districts VALUES (100, 'Euskirchen', 'EU', 10, false);
INSERT INTO contact_districts VALUES (101, 'Flensburg, Stadt', 'FL', 15, false);
INSERT INTO contact_districts VALUES (102, 'Forchheim', 'FO', 2, false);
INSERT INTO contact_districts VALUES (103, 'Frankenthal/Pfalz, Stadt', 'FT', 11, false);
INSERT INTO contact_districts VALUES (104, 'Frankfurt/Main, Stadt', 'F', 7, false);
INSERT INTO contact_districts VALUES (105, 'Frankfurt (Oder), Stadt', 'FF', 4, false);
INSERT INTO contact_districts VALUES (106, 'Freiberg', 'FG', 13, false);
INSERT INTO contact_districts VALUES (107, 'Freising', 'FS', 2, false);
INSERT INTO contact_districts VALUES (108, 'Freudenstadt', 'FDS', 1, false);
INSERT INTO contact_districts VALUES (109, 'Freyung-Grafenau [Freyung]', 'FRG', 2, false);
INSERT INTO contact_districts VALUES (110, 'Friesland [Jever]', 'FRI', 9, false);
INSERT INTO contact_districts VALUES (111, 'Fulda', 'FD', 7, false);
INSERT INTO contact_districts VALUES (112, 'Fürstenfeldbruck', 'FFB', 2, false);
INSERT INTO contact_districts VALUES (113, 'Fürth', 'FÜ', 2, false);
INSERT INTO contact_districts VALUES (114, 'Garmisch-Partenkirchen', 'GAP', 2, false);
INSERT INTO contact_districts VALUES (115, 'Gelsenkirchen, Stadt', 'GE', 10, false);
INSERT INTO contact_districts VALUES (116, 'Gera, Stadt', 'G', 16, false);
INSERT INTO contact_districts VALUES (117, 'Germersheim', 'GER', 11, false);
INSERT INTO contact_districts VALUES (118, 'Gießen', 'GI', 7, false);
INSERT INTO contact_districts VALUES (119, 'Gifhorn', 'GF', 9, false);
INSERT INTO contact_districts VALUES (120, 'Göppingen', 'GP', 1, false);
INSERT INTO contact_districts VALUES (121, 'Görlitz, Stadt', 'GR', 13, false);
INSERT INTO contact_districts VALUES (122, 'Goslar', 'GS', 9, false);
INSERT INTO contact_districts VALUES (123, 'Gotha', 'GTH', 16, false);
INSERT INTO contact_districts VALUES (124, 'Göttingen', 'GÖ', 9, false);
INSERT INTO contact_districts VALUES (125, 'Grafschaft Bentheim [Nordhorn]', 'NOH', 9, false);
INSERT INTO contact_districts VALUES (126, 'Greiz', 'GRZ', 16, false);
INSERT INTO contact_districts VALUES (127, 'Groß-Gerau', 'GG', 7, false);
INSERT INTO contact_districts VALUES (128, 'Günzburg', 'GZ', 2, false);
INSERT INTO contact_districts VALUES (129, 'Güstrow', 'GÜ', 8, false);
INSERT INTO contact_districts VALUES (130, 'Gütersloh', 'GT', 10, false);
INSERT INTO contact_districts VALUES (131, 'Hagen, Stadt', 'HA', 10, false);
INSERT INTO contact_districts VALUES (132, 'Halberstadt', 'HBS', 14, false);
INSERT INTO contact_districts VALUES (133, 'Halle, Stadt', 'HAL', 14, false);
INSERT INTO contact_districts VALUES (134, 'Hameln-Pyrmont [Hameln]', 'HM', 9, false);
INSERT INTO contact_districts VALUES (135, 'Hamm, Stadt', 'HAM', 10, false);
INSERT INTO contact_districts VALUES (136, 'Hannover', 'H', 9, false);
INSERT INTO contact_districts VALUES (137, 'Hansestadt Bremen Bremerhaven, Stadt', 'HB', 5, false);
INSERT INTO contact_districts VALUES (138, 'Hansestadt Greifswald', 'HGW', 8, false);
INSERT INTO contact_districts VALUES (139, 'Hansestadt Hamburg', 'HH', 6, false);
INSERT INTO contact_districts VALUES (140, 'Hansestadt Lübeck', 'HL', 15, false);
INSERT INTO contact_districts VALUES (141, 'Hansestadt Rostock', 'HRO', 8, false);
INSERT INTO contact_districts VALUES (142, 'Hansestadt Stralsund', 'HST', 8, false);
INSERT INTO contact_districts VALUES (143, 'Hansestadt Wismar', 'HWI', 8, false);
INSERT INTO contact_districts VALUES (144, 'Harburg [Winsen (Luhe)]', 'WL', 9, false);
INSERT INTO contact_districts VALUES (145, 'Haßberge [Haßfurt]', 'HAS', 2, false);
INSERT INTO contact_districts VALUES (146, 'Havelland [Rathenow]', 'HVL', 4, false);
INSERT INTO contact_districts VALUES (147, 'Heidenheim', 'HDH', 1, false);
INSERT INTO contact_districts VALUES (148, 'Heilbronn', 'HN', 1, false);
INSERT INTO contact_districts VALUES (149, 'Heinsberg', 'HS', 10, false);
INSERT INTO contact_districts VALUES (150, 'Helmstedt', 'HE', 9, false);
INSERT INTO contact_districts VALUES (151, 'Herford', 'HF', 10, false);
INSERT INTO contact_districts VALUES (152, 'Herne, Stadt', 'HER', 10, false);
INSERT INTO contact_districts VALUES (153, 'Hersfeld-Rotenburg [Bad Hersfeld]', 'HEF', 7, false);
INSERT INTO contact_districts VALUES (154, 'Herzogtum Lauenburg [Ratzeburg]', 'RZ', 15, false);
INSERT INTO contact_districts VALUES (155, 'Hildburghausen', 'HBN', 16, false);
INSERT INTO contact_districts VALUES (156, 'Hildesheim', 'HI', 9, false);
INSERT INTO contact_districts VALUES (157, 'Hochsauerlandkreis [Meschede]', 'HSK', 10, false);
INSERT INTO contact_districts VALUES (158, 'Hochtaunuskreis [Bad Homburg v. d. Höhe]', 'HG', 7, false);
INSERT INTO contact_districts VALUES (159, 'Hof', 'HO', 2, false);
INSERT INTO contact_districts VALUES (160, 'Hohenlohekreis [Künzelsau]', 'KÜN', 1, false);
INSERT INTO contact_districts VALUES (161, 'Holzminden', 'HOL', 9, false);
INSERT INTO contact_districts VALUES (162, 'Höxter', 'HX', 10, false);
INSERT INTO contact_districts VALUES (163, 'Hoyerswerda, Stadt', 'HY', 13, false);
INSERT INTO contact_districts VALUES (164, 'Ilm-Kreis [Arnstadt]', 'IK', 16, false);
INSERT INTO contact_districts VALUES (165, 'Ingolstadt, Stadt', 'IN', 2, false);
INSERT INTO contact_districts VALUES (166, 'Jena, Stadt', 'J', 16, false);
INSERT INTO contact_districts VALUES (167, 'Jerichower Land [Burg]', 'JL', 14, false);
INSERT INTO contact_districts VALUES (168, 'Kaiserslautern', 'KL', 11, false);
INSERT INTO contact_districts VALUES (169, 'Kamenz', 'KM', 13, false);
INSERT INTO contact_districts VALUES (170, 'Karlsruhe', 'KA', 1, false);
INSERT INTO contact_districts VALUES (171, 'Kassel', 'KS', 7, false);
INSERT INTO contact_districts VALUES (172, 'Kaufbeuren, Stadt', 'KF', 2, false);
INSERT INTO contact_districts VALUES (173, 'Kelheim', 'KEH', 2, false);
INSERT INTO contact_districts VALUES (174, 'Kempten (Allgäu), Stadt', 'KE', 2, false);
INSERT INTO contact_districts VALUES (175, 'Kiel, Stadt', 'KI', 15, false);
INSERT INTO contact_districts VALUES (176, 'Kitzingen', 'KT', 2, false);
INSERT INTO contact_districts VALUES (177, 'Kleve', 'KLE', 10, false);
INSERT INTO contact_districts VALUES (178, 'Koblenz, Stadt', 'KO', 11, false);
INSERT INTO contact_districts VALUES (179, 'Köln, Stadt', 'K', 10, false);
INSERT INTO contact_districts VALUES (180, 'Konstanz', 'KN', 1, false);
INSERT INTO contact_districts VALUES (181, 'Konstanz, Kreis, Gemeinde Büsingen am Hochrhein', 'BÜS', 1, false);
INSERT INTO contact_districts VALUES (182, 'Köthen/Anhalt [Köthen]', 'KÖT', 14, false);
INSERT INTO contact_districts VALUES (183, 'Krefeld, Stadt', 'KR', 10, false);
INSERT INTO contact_districts VALUES (184, 'Kronach', 'KC', 2, false);
INSERT INTO contact_districts VALUES (185, 'Kulmbach', 'KU', 2, false);
INSERT INTO contact_districts VALUES (186, 'Kusel', 'KUS', 11, false);
INSERT INTO contact_districts VALUES (187, 'Kyffhäuser-Kreis [Sondershausen]', 'KYF', 16, false);
INSERT INTO contact_districts VALUES (188, 'Lahn-Dill-Kreis [Wetzlar]', 'LDK', 7, false);
INSERT INTO contact_districts VALUES (189, 'Landau, Stadt', 'LD', 11, false);
INSERT INTO contact_districts VALUES (190, 'Landsberg am Lech', 'LL', 2, false);
INSERT INTO contact_districts VALUES (191, 'Landshut', 'LA', 2, false);
INSERT INTO contact_districts VALUES (192, 'Leer', 'LER', 9, false);
INSERT INTO contact_districts VALUES (193, 'Leipziger Land [Leipzig]', 'L', 13, false);
INSERT INTO contact_districts VALUES (194, 'Leverkusen, Stadt', 'LEV', 10, false);
INSERT INTO contact_districts VALUES (195, 'Lichtenfels', 'LIF', 2, false);
INSERT INTO contact_districts VALUES (196, 'Limburg-Weilburg [Limburg]', 'LM', 7, false);
INSERT INTO contact_districts VALUES (197, 'Lindau (Bodensee)', 'LI', 2, false);
INSERT INTO contact_districts VALUES (198, 'Lippe [Detmold]', 'LIP', 10, false);
INSERT INTO contact_districts VALUES (199, 'Löbau-Zittau [Zittau]', 'ZI', 13, false);
INSERT INTO contact_districts VALUES (200, 'Lörrach', 'LÖ', 1, false);
INSERT INTO contact_districts VALUES (201, 'Lüchow-Dannenberg [Lüchow]', 'DAN', 9, false);
INSERT INTO contact_districts VALUES (202, 'Ludwigsburg', 'LB', 1, false);
INSERT INTO contact_districts VALUES (203, 'Ludwigslust', 'LWL', 8, false);
INSERT INTO contact_districts VALUES (204, 'Lüneburg', 'LG', 9, false);
INSERT INTO contact_districts VALUES (205, 'Magdeburg, Stadt', 'MD', 14, false);
INSERT INTO contact_districts VALUES (206, 'Main-Kinzig-Kreis [Hanau]', 'HU', 7, false);
INSERT INTO contact_districts VALUES (207, 'Main-Spessart [Karlstadt]', 'MSP', 2, false);
INSERT INTO contact_districts VALUES (208, 'Main-Tauber-Kreis [Tauberbischofsheim]', 'TBB', 1, false);
INSERT INTO contact_districts VALUES (209, 'Main-Taunus-Kreis [Hofheim / Ts.]', 'MTK', 7, false);
INSERT INTO contact_districts VALUES (210, 'Mainz-Bingen [Ingelheim]', 'MZ', 11, false);
INSERT INTO contact_districts VALUES (211, 'Mannheim, Stadt', 'MA', 1, false);
INSERT INTO contact_districts VALUES (212, 'Mansfelder Land [Eisleben]', 'ML', 14, false);
INSERT INTO contact_districts VALUES (213, 'Marburg-Biedenkopf [Marburg-Cappel]', 'MR', 7, false);
INSERT INTO contact_districts VALUES (214, 'Märkischer Kreis [Lüdenscheid]', 'MK', 10, false);
INSERT INTO contact_districts VALUES (215, 'Märkisch-Oderland [Seelow]', 'MOL', 4, false);
INSERT INTO contact_districts VALUES (216, 'Mayen-Koblenz [Koblenz]', 'MYK', 11, false);
INSERT INTO contact_districts VALUES (217, 'Mecklenburg-Strelitz [Neustrelitz]', 'MST', 8, false);
INSERT INTO contact_districts VALUES (218, 'Meißen', 'MEI', 13, false);
INSERT INTO contact_districts VALUES (219, 'Memmingen, Stadt', 'MM', 2, false);
INSERT INTO contact_districts VALUES (220, 'Merseburg-Querfurt [Merseburg]', 'MQ', 14, false);
INSERT INTO contact_districts VALUES (221, 'Merzig-Wadern [Merzig]', 'MZG', 12, false);
INSERT INTO contact_districts VALUES (222, 'Mettmann', 'ME', 10, false);
INSERT INTO contact_districts VALUES (223, 'Miesbach', 'MB', 2, false);
INSERT INTO contact_districts VALUES (224, 'Miltenberg', 'MIL', 2, false);
INSERT INTO contact_districts VALUES (225, 'Minden-Lübbecke [Minden]', 'MI', 10, false);
INSERT INTO contact_districts VALUES (226, 'Mittlerer Erzgebirgskreis [Marienberg]', 'MEK', 13, false);
INSERT INTO contact_districts VALUES (227, 'Mittweida', 'MW', 13, false);
INSERT INTO contact_districts VALUES (228, 'Mönchengladbach, Stadt', 'MG', 10, false);
INSERT INTO contact_districts VALUES (229, 'Mühldorf am Inn', 'MÜ', 2, false);
INSERT INTO contact_districts VALUES (230, 'Muldentalkreis [Grimma]', 'MTL', 13, false);
INSERT INTO contact_districts VALUES (231, 'Muldentalkreis in Grimma, Kreis', 'MU', 13, false);
INSERT INTO contact_districts VALUES (232, 'Mülheim a. d. Ruhr, Stadt', 'MH', 10, false);
INSERT INTO contact_districts VALUES (233, 'München', 'M', 2, false);
INSERT INTO contact_districts VALUES (234, 'Münster, Stadt', 'MS', 10, false);
INSERT INTO contact_districts VALUES (235, 'Müritz [Waren]', 'MÜR', 8, false);
INSERT INTO contact_districts VALUES (236, 'Neckar-Odenwald-Kreis [Mosbach]', 'MOS', 1, false);
INSERT INTO contact_districts VALUES (237, 'Neubrandenburg, Stadt', 'NB', 8, false);
INSERT INTO contact_districts VALUES (238, 'Neuburg-Schrobenhausen [Neuburg an der Donau]', 'ND', 2, false);
INSERT INTO contact_districts VALUES (239, 'Neumarkt/Oberpfalz', 'NM', 2, false);
INSERT INTO contact_districts VALUES (240, 'Neumünster, Stadt', 'NMS', 15, false);
INSERT INTO contact_districts VALUES (241, 'Neunkirchen [Ottweiler]', 'NK', 12, false);
INSERT INTO contact_districts VALUES (242, 'Neustadt a.d. Waldnaab', 'NEW', 2, false);
INSERT INTO contact_districts VALUES (243, 'Neustadt/Aisch-Bad Windsheim [Neustadt a.d. Aisch]', 'NEA', 2, false);
INSERT INTO contact_districts VALUES (244, 'Neustadt/Weinstraße, Stadt', 'NW', 11, false);
INSERT INTO contact_districts VALUES (245, 'Neu-Ulm', 'NU', 2, false);
INSERT INTO contact_districts VALUES (246, 'Neuwied', 'NR', 11, false);
INSERT INTO contact_districts VALUES (247, 'Niederschlesischer Oberlausitzkreis [Niesky]', 'NOL', 13, false);
INSERT INTO contact_districts VALUES (248, 'Nienburg / Weser [Nienburg]', 'NI', 9, false);
INSERT INTO contact_districts VALUES (249, 'Nordfriesland [Husum]', 'NF', 15, false);
INSERT INTO contact_districts VALUES (250, 'Nordhausen', 'NDH', 16, false);
INSERT INTO contact_districts VALUES (251, 'Nordvorpommern [Grimmen]', 'NVP', 8, false);
INSERT INTO contact_districts VALUES (252, 'Nordwestmecklenburg [Grevesmühlen]', 'NWM', 8, false);
INSERT INTO contact_districts VALUES (253, 'Northeim', 'NOM', 9, false);
INSERT INTO contact_districts VALUES (254, 'Nürnberger Land [Lauf a.d. Pegnitz]', 'LAU', 2, false);
INSERT INTO contact_districts VALUES (255, 'Nürnberg, Stadt', 'N', 2, false);
INSERT INTO contact_districts VALUES (256, 'Oberallgäu [Sonthofen]', 'OA', 2, false);
INSERT INTO contact_districts VALUES (257, 'Oberbergischer Kreis [Gummersbach]', 'GM', 10, false);
INSERT INTO contact_districts VALUES (258, 'Oberhausen, Stadt', 'OB', 10, false);
INSERT INTO contact_districts VALUES (259, 'Oberhavel [Oranienburg]', 'OHV', 4, false);
INSERT INTO contact_districts VALUES (260, 'Oberspreewald-Lausitz [Senftenberg]', 'OSL', 4, false);
INSERT INTO contact_districts VALUES (261, 'Odenwaldkreis [Erbach / Odw.]', 'ERB', 7, false);
INSERT INTO contact_districts VALUES (262, 'Oder-Spree [Beeskow]', 'LOS', 4, false);
INSERT INTO contact_districts VALUES (263, 'Offenbach [Dietzenbach]', 'OF', 7, false);
INSERT INTO contact_districts VALUES (264, 'Ohrekreis [Haldensleben]', 'OK', 14, false);
INSERT INTO contact_districts VALUES (265, 'Oldenburg [Wildeshausen]', 'OL', 9, false);
INSERT INTO contact_districts VALUES (266, 'Olpe', 'OE', 10, false);
INSERT INTO contact_districts VALUES (267, 'Ortenaukreis [Offenburg]', 'OG', 1, false);
INSERT INTO contact_districts VALUES (268, 'Osnabrück', 'OS', 9, false);
INSERT INTO contact_districts VALUES (269, 'Ostalbkreis [Aalen]', 'AA', 1, false);
INSERT INTO contact_districts VALUES (270, 'Ostallgäu [Marktoberdorf]', 'OAL', 2, false);
INSERT INTO contact_districts VALUES (271, 'Osterholz [Osterholz-Scharmbeck]', 'OHZ', 9, false);
INSERT INTO contact_districts VALUES (272, 'Osterode am Harz', 'OHA', 9, false);
INSERT INTO contact_districts VALUES (273, 'Ostholstein [Eutin]', 'OH', 15, false);
INSERT INTO contact_districts VALUES (274, 'Ostprignitz-Ruppin [Neuruppin]', 'OPR', 4, false);
INSERT INTO contact_districts VALUES (275, 'Ostvorpommern [Anklam]', 'OVP', 8, false);
INSERT INTO contact_districts VALUES (276, 'Paderborn', 'PB', 10, false);
INSERT INTO contact_districts VALUES (277, 'Parchim', 'PCH', 8, false);
INSERT INTO contact_districts VALUES (278, 'Passau', 'PA', 2, false);
INSERT INTO contact_districts VALUES (279, 'Peine', 'PE', 9, false);
INSERT INTO contact_districts VALUES (280, 'Pfaffenhofen a. d. Ilm', 'PAF', 2, false);
INSERT INTO contact_districts VALUES (281, 'Pinneberg', 'PI', 15, false);
INSERT INTO contact_districts VALUES (282, 'Plauen, Stadt', 'PL', 13, false);
INSERT INTO contact_districts VALUES (283, 'Plön', 'PLÖ', 15, false);
INSERT INTO contact_districts VALUES (284, 'Potsdam-Mittelmark [Belzig]', 'PM', 4, false);
INSERT INTO contact_districts VALUES (285, 'Potsdam, Stadt', 'P', 4, false);
INSERT INTO contact_districts VALUES (286, 'Prignitz [Perleberg]', 'PR', 4, false);
INSERT INTO contact_districts VALUES (287, 'Quedlinburg', 'QLB', 14, false);
INSERT INTO contact_districts VALUES (288, 'Rastatt', 'RA', 1, false);
INSERT INTO contact_districts VALUES (289, 'Ravensburg', 'RV', 1, false);
INSERT INTO contact_districts VALUES (290, 'Recklinghausen', 'RE', 10, false);
INSERT INTO contact_districts VALUES (291, 'Regen', 'REG', 2, false);
INSERT INTO contact_districts VALUES (292, 'Regensburg', 'R', 2, false);
INSERT INTO contact_districts VALUES (293, 'Remscheid, Stadt', 'RS', 10, false);
INSERT INTO contact_districts VALUES (294, 'Rems-Murr-Kreis [Waiblingen]', 'WN', 1, false);
INSERT INTO contact_districts VALUES (295, 'Rendsburg-Eckernförde [Rendsburg]', 'RD', 15, false);
INSERT INTO contact_districts VALUES (296, 'Reutlingen', 'RT', 1, false);
INSERT INTO contact_districts VALUES (297, 'Rhein-Erft-Kreis [Bergheim]', 'BM', 10, false);
INSERT INTO contact_districts VALUES (298, 'Rheingau-Taunus-Kreis [Bad Schwalbach]', 'RÜD', 7, false);
INSERT INTO contact_districts VALUES (299, 'Rhein-Hunsrück-Kreis [Simmern]', 'SIM', 11, false);
INSERT INTO contact_districts VALUES (300, 'Rheinisch-Bergischer-Kreis [Bergisch-Gladbach]', 'GL', 10, false);
INSERT INTO contact_districts VALUES (301, 'Rhein-Kreis Neuss [Grevenbroich]', 'NE', 10, false);
INSERT INTO contact_districts VALUES (302, 'Rhein-Lahn-Kreis [Bad Ems]', 'EMS', 11, false);
INSERT INTO contact_districts VALUES (303, 'Rhein-Neckar-Kreis [Heidelberg]', 'HD', 1, false);
INSERT INTO contact_districts VALUES (304, 'Rhein-Pfalz-Kreis [Ludwigshafen]', 'LU', 11, false);
INSERT INTO contact_districts VALUES (305, 'Rhein-Sieg-Kreis [Siegburg]', 'SU', 10, false);
INSERT INTO contact_districts VALUES (306, 'Rhön-Grabfeld [Bad Neustadt a.d. Saale]', 'NES', 2, false);
INSERT INTO contact_districts VALUES (307, 'Riesa-Großenhain [Großenhain]', 'RG', 13, false);
INSERT INTO contact_districts VALUES (308, 'Rosenheim', 'RO', 2, false);
INSERT INTO contact_districts VALUES (309, 'Rotenburg (Wümme)', 'ROW', 9, false);
INSERT INTO contact_districts VALUES (310, 'Roth', 'RH', 2, false);
INSERT INTO contact_districts VALUES (311, 'Rottal-Inn [Pfarrkirchen]', 'PAN', 2, false);
INSERT INTO contact_districts VALUES (312, 'Rottweil', 'RW', 1, false);
INSERT INTO contact_districts VALUES (313, 'Rügen [Bergen]', 'RÜG', 8, false);
INSERT INTO contact_districts VALUES (314, 'Saale-Holzland-Kreis [Eisenberg]', 'SHK', 16, false);
INSERT INTO contact_districts VALUES (315, 'Saale-Orla-Kreis [Schleiz]', 'SOK', 16, false);
INSERT INTO contact_districts VALUES (316, 'Saalfeld-Rudolstadt [Saalfeld]', 'SLF', 16, false);
INSERT INTO contact_districts VALUES (317, 'Saalkreis [Halle]', 'SK', 14, false);
INSERT INTO contact_districts VALUES (318, 'Saarlouis', 'SLS', 12, false);
INSERT INTO contact_districts VALUES (319, 'Saarpfalz-Kreis [Homburg]', 'HOM', 12, false);
INSERT INTO contact_districts VALUES (320, 'Sächsische Schweiz [Pirna]', 'PIR', 13, false);
INSERT INTO contact_districts VALUES (321, 'Salzgitter, Stadt', 'SZ', 9, false);
INSERT INTO contact_districts VALUES (322, 'Sangerhausen', 'SGH', 14, false);
INSERT INTO contact_districts VALUES (323, 'Schaumburg [Stadthagen]', 'SHG', 9, false);
INSERT INTO contact_districts VALUES (324, 'Schleswig-Flensburg [Schleswig]', 'SL', 15, false);
INSERT INTO contact_districts VALUES (325, 'Schmalkalden-Meiningen [Meiningen]', 'SM', 16, false);
INSERT INTO contact_districts VALUES (326, 'Schönebeck', 'SBK', 14, false);
INSERT INTO contact_districts VALUES (327, 'Schwabach, Stadt', 'SC', 2, false);
INSERT INTO contact_districts VALUES (328, 'Schwäbisch-Hall', 'SHA', 1, false);
INSERT INTO contact_districts VALUES (329, 'Schwalm-Eder-Kreis [Homberg / Efze]', 'HR', 7, false);
INSERT INTO contact_districts VALUES (330, 'Schwandorf', 'SAD', 2, false);
INSERT INTO contact_districts VALUES (331, 'Schwarzwald-Baar-Kreis [Villingen-Schwenningen]', 'VS', 1, false);
INSERT INTO contact_districts VALUES (332, 'Schweinfurt', 'SW', 2, false);
INSERT INTO contact_districts VALUES (333, 'Schwerin, Stadt', 'SN', 8, false);
INSERT INTO contact_districts VALUES (334, 'Segeberg [Bad Segeberg]', 'SE', 15, false);
INSERT INTO contact_districts VALUES (335, 'Siegen-Wittgenstein [Siegen]', 'SI', 10, false);
INSERT INTO contact_districts VALUES (336, 'Sigmaringen', 'SIG', 1, false);
INSERT INTO contact_districts VALUES (337, 'Soest', 'SO', 10, false);
INSERT INTO contact_districts VALUES (338, 'Solingen, Stadt', 'SG', 10, false);
INSERT INTO contact_districts VALUES (339, 'Soltau-Fallingbostel [Fallingbostel]', 'SFA', 9, false);
INSERT INTO contact_districts VALUES (340, 'Sömmerda', 'SÖM', 16, false);
INSERT INTO contact_districts VALUES (341, 'Sonneberg', 'SON', 16, false);
INSERT INTO contact_districts VALUES (342, 'Speyer, Stadt', 'SP', 11, false);
INSERT INTO contact_districts VALUES (343, 'Spree-Neiße [Forst]', 'SPN', 4, false);
INSERT INTO contact_districts VALUES (344, 'Stade', 'STD', 9, false);
INSERT INTO contact_districts VALUES (345, 'Stadtverband Saarbrücken [Saarbrücken]', 'SB', 12, false);
INSERT INTO contact_districts VALUES (346, 'Starnberg', 'STA', 2, false);
INSERT INTO contact_districts VALUES (347, 'Steinburg [Itzehoe]', 'IZ', 15, false);
INSERT INTO contact_districts VALUES (348, 'Steinfurt', 'ST', 10, false);
INSERT INTO contact_districts VALUES (349, 'Stendal', 'SDL', 14, false);
INSERT INTO contact_districts VALUES (350, 'St. Ingbert, Stadt', 'IGB', 12, false);
INSERT INTO contact_districts VALUES (351, 'Stollberg', 'STL', 13, false);
INSERT INTO contact_districts VALUES (352, 'Stormarn [Bad Oldesloe]', 'OD', 15, false);
INSERT INTO contact_districts VALUES (353, 'Straubing-Bogen [Straubing]', 'SR', 2, false);
INSERT INTO contact_districts VALUES (354, 'Stuttgart, Stadt', 'S', 1, false);
INSERT INTO contact_districts VALUES (355, 'St. Wendel', 'WND', 12, false);
INSERT INTO contact_districts VALUES (356, 'Südliche Weinstraße [Landau]', 'SÜW', 11, false);
INSERT INTO contact_districts VALUES (357, 'Südwestpfalz [Pirmasens]', 'PS', 11, false);
INSERT INTO contact_districts VALUES (358, 'Suhl, Stadt', 'SHL', 16, false);
INSERT INTO contact_districts VALUES (359, 'Teltow-Fläming [Luckenwalde]', 'TF', 4, false);
INSERT INTO contact_districts VALUES (360, 'Tirschenreuth', 'TIR', 2, false);
INSERT INTO contact_districts VALUES (361, 'Torgau-Oschatz [Torgau]', 'TO', 13, false);
INSERT INTO contact_districts VALUES (362, 'Traunstein', 'TS', 2, false);
INSERT INTO contact_districts VALUES (363, 'Trier-Saarburg [Trier]', 'TR', 11, false);
INSERT INTO contact_districts VALUES (364, 'Tübingen', 'TÜ', 1, false);
INSERT INTO contact_districts VALUES (365, 'Tuttlingen', 'TUT', 1, false);
INSERT INTO contact_districts VALUES (366, 'Uckermark [Prenzlau]', 'UM', 4, false);
INSERT INTO contact_districts VALUES (367, 'Uecker-Randow [Pasewalk]', 'UER', 8, false);
INSERT INTO contact_districts VALUES (368, 'Uelzen', 'UE', 9, false);
INSERT INTO contact_districts VALUES (369, 'Unna', 'UN', 10, false);
INSERT INTO contact_districts VALUES (370, 'Unstrut-Hainich-Kreis [Mühlhausen]', 'UH', 16, false);
INSERT INTO contact_districts VALUES (371, 'Unterallgäu [Mindelheim]', 'MN', 2, false);
INSERT INTO contact_districts VALUES (372, 'Vechta', 'VEC', 9, false);
INSERT INTO contact_districts VALUES (373, 'Verden [Verden (Aller)]', 'VER', 9, false);
INSERT INTO contact_districts VALUES (374, 'Viersen', 'VIE', 10, false);
INSERT INTO contact_districts VALUES (375, 'Vogelsbergkreis [Lauterbach]', 'VB', 7, false);
INSERT INTO contact_districts VALUES (376, 'Vogtlandkreis [Plauen]', 'V', 13, false);
INSERT INTO contact_districts VALUES (377, 'Völklingen, Stadt', 'VK', 12, false);
INSERT INTO contact_districts VALUES (378, 'Waldeck-Frankenberg [Korbach]', 'KB', 7, false);
INSERT INTO contact_districts VALUES (379, 'Waldshut', 'WT', 1, false);
INSERT INTO contact_districts VALUES (380, 'Warendorf', 'WAF', 10, false);
INSERT INTO contact_districts VALUES (381, 'Wartburgkreis [Bad Salzungen]', 'WAK', 16, false);
INSERT INTO contact_districts VALUES (382, 'Weiden i. d. OPf., Stadt', 'WEN', 2, false);
INSERT INTO contact_districts VALUES (383, 'Weilheim-Schongau [Weilheim/Obb.]', 'WM', 2, false);
INSERT INTO contact_districts VALUES (384, 'Weimarer Land [Apolda]', 'AP', 16, false);
INSERT INTO contact_districts VALUES (385, 'Weimar, Stadt', 'WE', 16, false);
INSERT INTO contact_districts VALUES (386, 'Weißenburg-Gunzenhausen [Weißenburg i. Bay.]', 'WUG', 2, false);
INSERT INTO contact_districts VALUES (387, 'Weißenfels', 'WSF', 14, false);
INSERT INTO contact_districts VALUES (388, 'Weißeritzkreis [Dippoldiswalde]', 'DW', 13, false);
INSERT INTO contact_districts VALUES (389, 'Wernigerode', 'WR', 14, false);
INSERT INTO contact_districts VALUES (390, 'Werra-Meißner-Kreis [Eschwege]', 'ESW', 7, false);
INSERT INTO contact_districts VALUES (391, 'Wesel', 'WES', 10, false);
INSERT INTO contact_districts VALUES (392, 'Wesermarsch [Brake]', 'BRA', 9, false);
INSERT INTO contact_districts VALUES (393, 'Westerwaldkreis [Montabaur]', 'WW', 11, false);
INSERT INTO contact_districts VALUES (394, 'Wetteraukreis [Friedberg]', 'FB', 7, false);
INSERT INTO contact_districts VALUES (395, 'Wiesbaden, Stadt', 'WI', 7, false);
INSERT INTO contact_districts VALUES (396, 'Wilhelmshaven, Stadt', 'WHV', 9, false);
INSERT INTO contact_districts VALUES (397, 'Wittenberg', 'WB', 14, false);
INSERT INTO contact_districts VALUES (398, 'Wittmund', 'WTM', 9, false);
INSERT INTO contact_districts VALUES (399, 'Wolfenbüttel', 'WF', 9, false);
INSERT INTO contact_districts VALUES (400, 'Wolfsburg, Stadt', 'WOB', 9, false);
INSERT INTO contact_districts VALUES (401, 'Worms, Stadt', 'WO', 11, false);
INSERT INTO contact_districts VALUES (402, 'Wunsiedel/Fichtelgeb.', 'WUN', 2, false);
INSERT INTO contact_districts VALUES (403, 'Wuppertal, Stadt', 'W', 10, false);
INSERT INTO contact_districts VALUES (404, 'Würzburg', 'WÜ', 2, false);
INSERT INTO contact_districts VALUES (405, 'Zollernalbkreis [Balingen]', 'BL', 1, false);
INSERT INTO contact_districts VALUES (406, 'Zweibrücken, Stadt', 'ZW', 11, false);
INSERT INTO contact_districts VALUES (407, 'Zwickauer Land [Werdau]', 'Z', 13, false);


--
-- Name: contact_districts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_districts_id_seq', 407, true);


--
-- Data for Name: contact_email_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_email_types VALUES (1, 'privat', false);
INSERT INTO contact_email_types VALUES (2, 'geschäftlich', false);
INSERT INTO contact_email_types VALUES (3, 'intern', false);
INSERT INTO contact_email_types VALUES (4, 'Alias', false);
INSERT INTO contact_email_types VALUES (5, 'Rechnung', false);


--
-- Name: contact_email_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_email_types_id_seq', 5, true);


--
-- Data for Name: contact_emails; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_emails VALUES (1, 1000000000, 2, 'info@example.org', true, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 'validatedNothing');
INSERT INTO contact_emails VALUES (2, 1000000002, 2, 'yourname@example.org', true, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, 'validatedNothing');


--
-- Name: contact_emails_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_emails_id_seq', 2, true);


--
-- Data for Name: contact_federal_states; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_federal_states VALUES (0, 'Auswahl', 53, false);
INSERT INTO contact_federal_states VALUES (1, 'Baden-Württemberg', 53, false);
INSERT INTO contact_federal_states VALUES (2, 'Bayern', 53, false);
INSERT INTO contact_federal_states VALUES (3, 'Berlin', 53, false);
INSERT INTO contact_federal_states VALUES (4, 'Brandenburg', 53, false);
INSERT INTO contact_federal_states VALUES (5, 'Bremen', 53, false);
INSERT INTO contact_federal_states VALUES (6, 'Hamburg', 53, false);
INSERT INTO contact_federal_states VALUES (7, 'Hessen', 53, false);
INSERT INTO contact_federal_states VALUES (8, 'Mecklenburg-Vorpommern', 53, false);
INSERT INTO contact_federal_states VALUES (9, 'Niedersachsen', 53, false);
INSERT INTO contact_federal_states VALUES (10, 'Nordrhein-Westfalen', 53, false);
INSERT INTO contact_federal_states VALUES (11, 'Rheinland-Pfalz', 53, false);
INSERT INTO contact_federal_states VALUES (12, 'Saarland', 53, false);
INSERT INTO contact_federal_states VALUES (13, 'Sachsen', 53, false);
INSERT INTO contact_federal_states VALUES (14, 'Sachsen-Anhalt', 53, false);
INSERT INTO contact_federal_states VALUES (15, 'Schleswig-Holstein', 53, false);
INSERT INTO contact_federal_states VALUES (16, 'Thüringen', 53, false);


--
-- Name: contact_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_groups_id_seq', 9, true);


--
-- Data for Name: contact_mailing_lists; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: contact_mailing_list_members; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: contact_mailing_lists_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_mailing_lists_id_seq', 2, false);


--
-- Data for Name: contact_phone_devices; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_phone_devices VALUES (1, 'Telefon', false);
INSERT INTO contact_phone_devices VALUES (2, 'Mobiltelefon', false);
INSERT INTO contact_phone_devices VALUES (3, 'Fax', false);
INSERT INTO contact_phone_devices VALUES (4, 'Büro', false);


--
-- Data for Name: contact_phone_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_phone_types VALUES (1, 'privat', false);
INSERT INTO contact_phone_types VALUES (2, 'geschäftlich', false);


--
-- Data for Name: contact_phones; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_phones VALUES (1, 1000000000, 1, 2, '49', '69', '999 999 1', NULL, true, '1970-01-01 00:00:00', 6000, NULL, NULL, '49699999991', '0699999991', false, 'validatedNothing');
INSERT INTO contact_phones VALUES (2, 1000000000, 3, 2, '49', '69', '999 999 9', NULL, true, '1970-01-01 00:00:00', 6000, NULL, NULL, '49699999999', '0699999999', false, 'validatedNothing');


--
-- Name: contact_phones_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_phones_id_seq', 2, true);


--
-- Data for Name: contact_salutations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_salutations VALUES (0, 'Auswahl', 'Sehr geehrte Damen und Herren', false, false, true, false, 'Auswahl', NULL);
INSERT INTO contact_salutations VALUES (1, 'Herr', 'Sehr geehrter Herr', true, false, false, false, 'Herrn', 'MR');
INSERT INTO contact_salutations VALUES (2, 'Frau', 'Sehr geehrte Frau', true, false, false, true, 'Frau', 'MRS');
INSERT INTO contact_salutations VALUES (3, 'Firma', 'Sehr geehrte Damen und Herren', false, false, true, false, 'Firma', 'COM');
INSERT INTO contact_salutations VALUES (4, 'Familie', 'Sehr geehrte Familie', true, false, true, false, 'Familie', 'FAM');
INSERT INTO contact_salutations VALUES (5, 'Eheleute', 'Sehr geehrte Dame, Sehr geehrter Herr', false, false, true, false, 'Eheleute', 'MC');


--
-- Name: contact_salutations_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_salutations_id_seq', 5, true);


--
-- Data for Name: contact_titles; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_titles VALUES (0, '', false);
INSERT INTO contact_titles VALUES (1, 'Dr.', false);
INSERT INTO contact_titles VALUES (2, 'Prof.', false);
INSERT INTO contact_titles VALUES (3, 'Prof. Dr.', false);


--
-- Name: contact_titles_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_titles_id_seq', 3, true);


--
-- Data for Name: contact_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO contact_types VALUES (0, 'Auswahl', false, 'Auswahl der Kontaktart', 'selection', false, NULL);
INSERT INTO contact_types VALUES (1, 'privat', false, 'Privatpersonen und Familien', 'contactTypePrivate', false, 'Prv');
INSERT INTO contact_types VALUES (2, 'gewerblich', false, 'Einzelunternehmen und Kapitalgesellschaften', 'contactTypeCompany', false, 'Gew');
INSERT INTO contact_types VALUES (3, 'öffentlich', false, 'Behörden, Schulen/Universitäten', 'contactTypePublic', false, 'OE');
INSERT INTO contact_types VALUES (4, 'AP', false, 'Ansprechpartner einer Firma oder Behörde', 'contactTypePerson', true, 'AP');
INSERT INTO contact_types VALUES (5, 'freiberuflich', false, 'Freie Berufe, Freelancer, Kleingewerbe', 'contactTypeFreelance', false, 'FB');


--
-- Name: contact_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_types_id_seq', 5, true);


--
-- Data for Name: contact_zipcode_searches; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: contact_zipcode_searches_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contact_zipcode_searches_id_seq', 1, false);


--
-- Data for Name: contact_zipcodes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: contacts_contact_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('contacts_contact_id_seq', 1000000003, true);


--
-- Data for Name: customer_origins; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO customer_origins VALUES (0, 'unbekannt');
INSERT INTO customer_origins VALUES (1, 'Kundenempfehlung');
INSERT INTO customer_origins VALUES (2, 'Messe');
INSERT INTO customer_origins VALUES (3, 'Mailing');
INSERT INTO customer_origins VALUES (4, 'Fachzeitschrift');
INSERT INTO customer_origins VALUES (5, 'Akquise');
INSERT INTO customer_origins VALUES (6, 'Internet');
INSERT INTO customer_origins VALUES (7, 'Presse');
INSERT INTO customer_origins VALUES (8, 'Infostand');
INSERT INTO customer_origins VALUES (9, 'Flyer');


--
-- Name: customer_origins_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('customer_origins_id_seq', 9, true);


--
-- Data for Name: customer_payment_agreements; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO customer_payment_agreements VALUES (1, 30000, 0, 0, 0, 7, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 1, 2, 3, true, 2, 1, true);
INSERT INTO customer_payment_agreements VALUES (2, 30001, 0, 0, 0, 7, NULL, '1970-01-01 00:00:00', 6000, NULL, NULL, 1, 2, 3, true, 2, 1, true);


--
-- Name: customer_payment_agreements_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('customer_payment_agreements_id_seq', 2, true);


--
-- Name: customer_status_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('customer_status_id_seq', 9, true);


--
-- Name: customer_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('customer_types_id_seq', 6, true);


--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('customers_id_seq', 30001, true);


--
-- Data for Name: document_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO document_configs VALUES (1, 'Dokumentenuploads', '/var/lib/osserp/dms', 'https://myapp.osserp.com/dms', '/app/dms/document/print');
INSERT INTO document_configs VALUES (2, 'Templates', '/var/lib/osserp/vmtpl', 'https://myapp.osserp.com/vmtpl', '/app/dms/document/print');


--
-- Data for Name: document_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO document_types VALUES (1, 'Belege', 1, NULL, 'pdf', false, NULL, 0, false, true, false, false, NULL, NULL, 'com.osserp.core.finance.RecordArchiveManager', 'record_documents', false, true, 'db', 0, false, NULL, false, false, false, false);
INSERT INTO document_types VALUES (2, 'Belegversionen', 1, NULL, 'pdf', false, NULL, 0, false, true, false, false, NULL, NULL, 'DIRECT_ACCESS', 'record_document_versions', false, true, 'db', 0, false, NULL, false, false, false, false);
INSERT INTO document_types VALUES (3, 'Korrespondenz', 1, NULL, 'pdf', false, NULL, 0, false, true, false, false, NULL, NULL, 'com.osserp.core.letters.LetterDocumentManager', 'letter_documents', false, false, 'db', 0, false, NULL, false, false, false, false);
INSERT INTO document_types VALUES (4, 'Zeiterfassung', 1, NULL, 'pdf', false, NULL, 0, false, false, false, false, NULL, NULL, 'com.osserp.core.hrm.TimeRecordingDocumentManager', 'time_recording_documents', false, false, 'db', 0, false, NULL, false, false, false, false);
INSERT INTO document_types VALUES (5, 'Mandantenlogo', 1, '5000000000', 'jpg', false, '2000000', 1, false, false, false, false, NULL, '/clients/pictures/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, false, false, false, false);
INSERT INTO document_types VALUES (6, 'Gewerbeschein', 1, '5000000000', NULL, true, '3000000', 1, false, true, false, false, NULL, '/clients/registrations/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, false, true, false, false);
INSERT INTO document_types VALUES (7, 'Mitarbeiter', 1, '5000000000', 'jpg', false, '1000000', 1, false, false, false, false, NULL, '/employees/pictures/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, false, true, false, false);
INSERT INTO document_types VALUES (8, 'Dokumente zum Kontakt', 1, '6000000000', NULL, true, '1000000', 1, false, false, false, false, NULL, '/contacts/documents/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, true, false, false);
INSERT INTO document_types VALUES (9, 'Datenblatt', 1, '3000000000', 'pdf', false, '1000000', 1, false, false, false, false, NULL, '/products/documents/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, false, false, false);
INSERT INTO document_types VALUES (10, 'Produktbild', 1, '3000000000', 'jpg', false, '400000', 1, false, true, false, false, NULL, '/products/pictures/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, false, false, false);
INSERT INTO document_types VALUES (11, 'Bilder zur Anfrage', 1, '1000000000', NULL, true, '1000000', 1, false, false, false, false, NULL, '/requests/pictures/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, false, false, false);
INSERT INTO document_types VALUES (12, 'Dokumente zur Anfrage', 1, '1000000000', NULL, true, '200000', 1, false, false, true, false, NULL, '/requests/documents/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, true, false, false);
INSERT INTO document_types VALUES (13, 'Bilder zum Auftrag', 1, '2000000000', NULL, true, '2000000', 1, false, false, false, false, NULL, '/sales/pictures/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, false, false, false);
INSERT INTO document_types VALUES (14, 'Dokumente zum Auftrag', 1, '4000000000', NULL, true, '1000000', 1, false, false, true, false, NULL, '/sales/documents/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, true, false, false);
INSERT INTO document_types VALUES (15, 'Einkaufsrechnung', 1, '7000000000', NULL, true, '5000000', 1, false, false, true, false, NULL, '/purchasing/invoices/', 'com.osserp.common.dms.DmsManager', 'documents', true, false, 'dynamic', 0, false, NULL, true, true, false, false);
INSERT INTO document_types VALUES (16, 'Mailattachments', 1, '8000000000', NULL, true, '1000000', 1, false, false, false, false, NULL, '/attachments/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, false, false, false, false);
INSERT INTO document_types VALUES (17, 'Vertriebsrechnungen', 1, '4000000000', NULL, true, '2000000', 1, false, false, true, false, NULL, '/sales/invoices/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, true, false, false);
INSERT INTO document_types VALUES (18, 'Gutschriften Vertrieb', 1, '4000000000', NULL, true, '3000000', 1, false, false, true, false, NULL, '/sales/creditnotes/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, true, false, false);
INSERT INTO document_types VALUES (19, 'Bankunterlagen', 1, '5000000000', NULL, true, '4000000', 1, false, false, true, false, NULL, '/clients/bankaccounts/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, false, true, true, false);
INSERT INTO document_types VALUES (20, 'Druck- und Emailvorlagen', 2, NULL, NULL, true, NULL, 2, false, true, false, false, 'stylesheets', NULL, 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'fixed', 0, false, NULL, false, false, false, false);
INSERT INTO document_types VALUES (21, 'Eigene Vorlagen', 2, NULL, NULL, true, NULL, 2, false, true, false, false, 'templates', NULL, 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'custom', 0, false, NULL, false, false, false, false);
INSERT INTO document_types VALUES (22, 'Buchungsbelege', 1, '5000000000', NULL, true, '5000000', 1, false, false, true, false, NULL, '/clients/records/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, true, true, false, false);
INSERT INTO document_types VALUES (23, 'Leistungsnachweis', 1, NULL, 'pdf', false, NULL, 0, false, true, false, false, NULL, NULL, 'com.osserp.core.projects.ProjectTrackingDocumentManager', 'project_tracking_documents', false, false, 'db', 0, true, NULL, true, false, false, false);
INSERT INTO document_types VALUES (24, 'Dokument Import', 1, '8000000000', NULL, true, '4000000', 1, false, false, false, false, NULL, '/incoming/', 'com.osserp.common.dms.DmsManager', 'documents', false, false, 'dynamic', 0, false, NULL, false, false, false, true);

--
-- Data for Name: document_categories; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: document_categories_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('document_categories_id_seq', 1, false);


--
-- Name: document_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('document_configs_id_seq', 2, true);


--
-- Name: document_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('document_types_id_seq', 24, true);


--
-- Data for Name: documents; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO documents VALUES (1, NULL, NULL, 20, 'custom/internet_registration_mail.vm', NULL, 117, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Registrierung - Eigener Text', 'CustomerRegistrationEmailText', false, 0);
INSERT INTO documents VALUES (2, NULL, NULL, 20, 'custom/mail_footer.vm', NULL, 306, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Fusszeile', 'footerEmailPart', false, 0);
INSERT INTO documents VALUES (3, NULL, NULL, 20, 'custom/purchase_order_mail.vm', NULL, 242, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Bestellung - Eigener Text', 'purchaseOrderEmailText', false, 0);
INSERT INTO documents VALUES (4, NULL, NULL, 20, 'custom/request_reset_warning_mail.vm', NULL, 867, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Zeitüberschreitung Anfrage - Eigener Text', 'RequestAlertEmailText', true, 0);
INSERT INTO documents VALUES (5, NULL, NULL, 20, 'xsl/custom/complimentary_close.vm', NULL, 458, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Schlussformel', 'xslDocumentComplementaryClose', false, 0);
INSERT INTO documents VALUES (6, NULL, NULL, 20, 'xsl/custom/terms_and_conditions.vm', NULL, 21306, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Anlage AGB', 'xslDocumentTermsAndConditions', false, 0);
INSERT INTO documents VALUES (7, NULL, NULL, 20, 'xsl/documents/account_ballance.vm', NULL, 16238, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Ausstehende Zahlungen nach Rechnung', 'xslDocumentAccountBallance', false, 0);
INSERT INTO documents VALUES (8, NULL, NULL, 20, 'xsl/documents/letter.vm', NULL, 8243, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Geschäftsbrief - Freitext', 'xslDocumentLetter', false, 0);
INSERT INTO documents VALUES (9, NULL, NULL, 20, 'xsl/documents/outstanding_payments.vm', NULL, 20785, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Ausstehende Zahlungen nach Projekt', 'xslDocumentOutstandingPayments', false, 0);
INSERT INTO documents VALUES (10, NULL, NULL, 20, 'xsl/documents/package_list.vm', NULL, 10679, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Packliste, Rüstschein', 'xslDocumentPackageList', false, 0);
INSERT INTO documents VALUES (11, NULL, NULL, 20, 'xsl/documents/phone_list.vm', NULL, 10727, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Telefonliste intern', 'xslDocumentPhoneList', false, 0);
INSERT INTO documents VALUES (12, NULL, NULL, 20, 'xsl/documents/project_tracking.vm', NULL, 13151, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Stunden-, Leistungsnachweis', 'xslDocumentProjectTracking', false, 0);
INSERT INTO documents VALUES (13, NULL, NULL, 20, 'xsl/documents/stocktaking_list.vm', NULL, 12999, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Inventur - Zählliste', 'xslDocumentStocktakingList', false, 0);
INSERT INTO documents VALUES (14, NULL, NULL, 20, 'xsl/documents/stocktaking_protocol.vm', NULL, 14739, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Inventur - Protokoll', 'xslDocumentStocktakingProtocol', false, 0);
INSERT INTO documents VALUES (15, NULL, NULL, 20, 'xsl/documents/stocktaking_report.vm', NULL, 16713, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Inventur - Report', 'xslDocumentStocktakingList', false, 0);
INSERT INTO documents VALUES (16, NULL, NULL, 20, 'xsl/documents/time_recording_journal.vm', NULL, 22437, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Dokument - Zeiterfassung Auswertung', 'xslDocumentStocktakingList', false, 0);
INSERT INTO documents VALUES (17, NULL, NULL, 20, 'xsl/documents/shared/attachments.vm', NULL, 298, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Anlagenverzeichnis', 'xslImportAttachments', false, 0);
INSERT INTO documents VALUES (18, NULL, NULL, 20, 'xsl/documents/shared/customer_address.vm', NULL, 1633, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Kopf - Kundenadresse', 'xslImportCustomerAddress', false, 0);
INSERT INTO documents VALUES (19, NULL, NULL, 20, 'xsl/documents/shared/customer_salutation.vm', NULL, 341, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Kopf - Anrede', 'xslImportCustomerSalutation', false, 0);
INSERT INTO documents VALUES (20, NULL, NULL, 20, 'xsl/documents/shared/page_definitions.vm', NULL, 1561, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Seitendefinition', 'xslImportPageDefinitions', false, 0);
INSERT INTO documents VALUES (21, NULL, NULL, 20, 'xsl/documents/shared/subject_and_date.vm', NULL, 219, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Kopf - Datum und Betreff', 'xslImportSubjectAndDate', false, 0);
INSERT INTO documents VALUES (22, NULL, NULL, 20, 'xsl/records/xsl_record_template.vm', NULL, 4543, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Dokument - Primäre Vorlage', 'xslRecordDocumentTemplate', false, 0);
INSERT INTO documents VALUES (23, NULL, NULL, 20, 'xsl/records/xsl_record_correction.vm', NULL, 31330, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Dokument - Rechnungsberichtigung', 'xslRecordDocumentCorrection', false, 0);
INSERT INTO documents VALUES (24, NULL, NULL, 20, 'xsl/records/xsl_rtp_1.vm', NULL, 11671, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Vertrieb - Angebot', 'xslRecordSalesOffer', false, 0);
INSERT INTO documents VALUES (25, NULL, NULL, 20, 'xsl/records/xsl_rtp_2.vm', NULL, 9276, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Vertrieb - Auftrag', 'xslRecordSalesOrder', false, 0);
INSERT INTO documents VALUES (26, NULL, NULL, 20, 'xsl/records/xsl_rtp_3.vm', NULL, 9412, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Vertrieb - Lieferschein', 'xslRecordSalesDelivery', false, 0);
INSERT INTO documents VALUES (27, NULL, NULL, 20, 'xsl/records/xsl_rtp_4.vm', NULL, 35927, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Vertrieb - Anzahlungsrechnung', 'xslRecordSalesDownpayment', false, 0);
INSERT INTO documents VALUES (28, NULL, NULL, 20, 'xsl/records/xsl_rtp_5.vm', NULL, 40059, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Vertrieb - Rechnung', 'xslRecordSalesInvoice', false, 0);
INSERT INTO documents VALUES (29, NULL, NULL, 20, 'xsl/records/xsl_rtp_9.vm', NULL, 6852, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Vertrieb - Gutschrift', 'xslRecordSalesCreditNote', false, 0);
INSERT INTO documents VALUES (30, NULL, NULL, 20, 'xsl/records/xsl_rtp_10.vm', NULL, 4441, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Vertrieb - Storno', 'xslRecordSalesCancellation', false, 0);
INSERT INTO documents VALUES (31, NULL, NULL, 20, 'xsl/records/xsl_rtp_102.vm', NULL, 8518, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Einkauf - Bestellung', 'xslRecordPurchaseOrder', false, 0);
INSERT INTO documents VALUES (32, NULL, NULL, 20, 'xsl/records/xsl_rtp_105.vm', NULL, 3358, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Einkauf - Inventurkorrektur', 'xslRecordPurchaseInvoice', false, 0);
INSERT INTO documents VALUES (33, NULL, NULL, 20, 'xsl/records/shared/business_case_info.vm', NULL, 995, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Projektname', 'xslRecordImportBusinessCaseInfo', false, 0);
INSERT INTO documents VALUES (34, NULL, NULL, 20, 'xsl/records/shared/complimentary_close.vm', NULL, 1399, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Schlussformel', 'xslRecordImportComplementaryClose', false, 0);
INSERT INTO documents VALUES (35, NULL, NULL, 20, 'xsl/records/shared/contact_persons.vm', NULL, 1400, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Ansprechpartner', 'xslRecordImportContactPersons', false, 0);
INSERT INTO documents VALUES (36, NULL, NULL, 20, 'xsl/records/shared/delivery_address.vm', NULL, 2065, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Lieferadresse', 'xslRecordImportDeliveryAddress', false, 0);
INSERT INTO documents VALUES (37, NULL, NULL, 20, 'xsl/records/shared/item_listing_columns.vm', NULL, 764, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Produktliste - Spaltendefinition', 'xslRecordImportItemListingColumns', false, 0);
INSERT INTO documents VALUES (38, NULL, NULL, 20, 'xsl/records/shared/item_listing_header.vm', NULL, 1663, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Produktliste Kopfzeile', 'xslRecordImportItemListingHeader', false, 0);
INSERT INTO documents VALUES (39, NULL, NULL, 20, 'xsl/records/shared/item_listing_item.vm', NULL, 2551, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Produktliste Position', 'xslRecordImportItemListingItem', false, 0);
INSERT INTO documents VALUES (40, NULL, NULL, 20, 'xsl/records/shared/item_listing_summary.vm', NULL, 3906, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Produktliste - Summe', 'xslRecordImportItemListingSummary', false, 0);
INSERT INTO documents VALUES (41, NULL, NULL, 20, 'xsl/records/shared/item_listing.vm', NULL, 587, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Produktliste - Definition', 'xslRecordImportItemListing', false, 0);
INSERT INTO documents VALUES (42, NULL, NULL, 20, 'xsl/records/shared/order_placed.vm', NULL, 1529, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Angebot - Annahmetext', 'xslRecordImportOrderPlaced', false, 0);
INSERT INTO documents VALUES (43, NULL, NULL, 20, 'xsl/records/shared/payment_agreement.vm', NULL, 4128, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Angebot / Auftrag - Zahlungsbedingungen', 'xslRecordImportPaymentAgreement', false, 0);
INSERT INTO documents VALUES (44, NULL, NULL, 20, 'xsl/records/shared/record_customer_address.vm', NULL, 1744, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Adresse - Kunde', 'xslRecordImportCustomerAddress', false, 0);
INSERT INTO documents VALUES (45, NULL, NULL, 20, 'xsl/records/shared/signatures.vm', NULL, 1560, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Unterschriften', 'xslRecordImportSignatures', false, 0);
INSERT INTO documents VALUES (46, NULL, NULL, 20, 'xsl/records/shared/supplier_address.vm', NULL, 1024, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Adresse - Lieferant', 'xslRecordImportSupplierAddress', false, 0);
INSERT INTO documents VALUES (48, NULL, NULL, 20, 'xsl/shared/footer.vm', NULL, 6559, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Fusszeile', 'xslImportFooter', false, 0);
INSERT INTO documents VALUES (49, NULL, NULL, 20, 'xsl/shared/header_logo_row.vm', NULL, 709, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Kopf - Logo', 'xslImportHeaderLogoRow', false, 0);
INSERT INTO documents VALUES (50, NULL, NULL, 20, 'xsl/shared/header.vm', NULL, 4746, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Kopf - Kopfdaten', 'xslImportHeader', false, 0);
INSERT INTO documents VALUES (51, NULL, NULL, 20, 'xsl/shared/html_template_matchers.vm', NULL, 826, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'System - Wird intern benötigt', 'system', true, 0);
INSERT INTO documents VALUES (52, NULL, NULL, 20, 'CustomerRegistrationEmailTemplate.vm', NULL, 146, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Registrierung - Bestätigung', 'CustomerRegistrationEmail', true, 0);
INSERT INTO documents VALUES (53, NULL, NULL, 20, 'InvalidProductConfigEmailTemplate.vm', NULL, 190, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Produkte ungültig', 'InvalidProductConfigEmailText', false, 0);
INSERT INTO documents VALUES (54, NULL, NULL, 20, 'purchaseOrderEmailTemplate.vm', NULL, 152, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Bestellung - Vorlage', 'purchaseOrderEmail', true, 0);
INSERT INTO documents VALUES (55, NULL, NULL, 20, 'RequestAlertEmailTemplate.vm', NULL, 170, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Zeitüberschreitung Anfrage - Vorlage', 'RequestAlertEmail', true, 0);
INSERT INTO documents VALUES (56, NULL, NULL, 20, 'RequestChangedEmailTemplate.vm', NULL, 857, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Zuständigkeit Anfrage erhalten', 'RequestChangedEmailText', false, 0);
INSERT INTO documents VALUES (57, NULL, NULL, 20, 'SupportTicketInfoTemplate.vm', NULL, 180, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Bestätigung Anfrage an Support', 'emailSupportTicketInfo', true, 0);
INSERT INTO documents VALUES (58, NULL, 100, 5, 'firmenlogo.jpg', NULL, 16046, '1970-01-01 00:00:00', 6000, NULL, NULL, 'Dummy firmenLogo', NULL, true, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, false, 0);
INSERT INTO documents VALUES (59, NULL, NULL, 20, 'salesCancellationEmailTemplate.vm', NULL, 156, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Storno Kunde - Vorlage', 'salesCancellationEmail', true, 0);
INSERT INTO documents VALUES (60, NULL, NULL, 20, 'custom/sales_cancellation_mail.vm', NULL, 117, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Storno Kunde - Eigener Text', 'salesCancellationEmailText', false, 0);
INSERT INTO documents VALUES (61, NULL, NULL, 20, 'salesCreditNoteEmailTemplate.vm', NULL, 155, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Gutschrift Kunde - Vorlage', 'salesCreditNoteEmail', true, 0);
INSERT INTO documents VALUES (62, NULL, NULL, 20, 'custom/sales_credit_note_mail.vm', NULL, 115, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Gutschrift Kunde - Eigener Text', 'salesCreditNoteEmailText', false, 0);
INSERT INTO documents VALUES (63, NULL, NULL, 20, 'salesDownpaymentEmailTemplate.vm', NULL, 155, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Abschlagsrechnung - Vorlage', 'salesDownpaymentEmail', true, 0);
INSERT INTO documents VALUES (64, NULL, NULL, 20, 'custom/sales_downpayment_mail.vm', NULL, 122, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Abschlagsrechnung - Eigener Text', 'salesDownpaymentEmailText', false, 0);
INSERT INTO documents VALUES (65, NULL, NULL, 20, 'salesInvoiceEmailTemplate.vm', NULL, 151, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Rechnung Kunde - Vorlage', 'salesInvoiceEmail', true, 0);
INSERT INTO documents VALUES (66, NULL, NULL, 20, 'custom/sales_invoice_mail.vm', NULL, 113, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Rechnung Kunde - Eigener Text', 'salesInvoiceEmailText', false, 0);
INSERT INTO documents VALUES (67, NULL, NULL, 20, 'salesOfferEmailTemplate.vm', NULL, 149, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Angebot Kunde - Vorlage', 'salesOfferEmail', true, 0);
INSERT INTO documents VALUES (68, NULL, NULL, 20, 'custom/sales_offer_mail.vm', NULL, 89, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Angebot Kunde - Eigener Text', 'salesOfferEmailText', false, 0);
INSERT INTO documents VALUES (69, NULL, NULL, 20, 'salesOrderEmailTemplate.vm', NULL, 149, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Auftragsbestätigung - Vorlage', 'salesOrderEmail', true, 0);
INSERT INTO documents VALUES (70, NULL, NULL, 20, 'custom/sales_order_mail.vm', NULL, 185, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Auftragsbestätigung - Eigener Text', 'salesOrderEmailText', false, 0);
INSERT INTO documents VALUES (71, NULL, NULL, 20, 'custom/mail_headline.vm', NULL, 73, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Fusszeile', 'headlineEmailPart', false, 0);
INSERT INTO documents VALUES (72, NULL, NULL, 20, 'custom/mail_signature.vm', NULL, 396, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Email - Fusszeile', 'signatureEmailPart', false, 0);
INSERT INTO documents VALUES (73, NULL, NULL, 20, 'xsl/records/shared/record_notes.vm', NULL, 462, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Konditionen', 'xslRecordImportNotes', false, 0);
INSERT INTO documents VALUES (74, NULL, NULL, 20, 'xsl/shared/letter_content.vm', NULL, 3429, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Dokument - Geschäftsbrief - Inhalt', 'xslImportLetterContent', false, 0);
INSERT INTO documents VALUES (75, NULL, NULL, 20, 'xsl/records/shared/cover_letter.vm', NULL, 535, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Geschäftsbrief', 'xslRecordImportCoverLetter', false, 0);
INSERT INTO documents VALUES (76, NULL, NULL, 20, 'xsl/custom/right_to_cancel_de.vm', NULL, 8326, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Beleg - Vertrieb - Auftrag - Widerufsrecht', 'xslRecordSalesOrderRightToCancel', false, 0);
INSERT INTO documents VALUES (77, NULL, NULL, 20, 'xsl/records/shared/time_of_supply.vm', NULL, 1253, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Leistungsdatum/-ort', 'xslRecordTimeOfSupply', false, 0);

--
-- Name: documents_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('documents_id_seq', 77, true);


--
-- Data for Name: employee_contacts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: employee_contacts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_contacts_id_seq', 1, false);


--
-- Data for Name: system_i18n_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_i18n_configs VALUES (1, 'de');
INSERT INTO system_i18n_configs VALUES (2, 'en');


--
-- Data for Name: employee_descriptions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO employee_descriptions VALUES (1, 6000, 1, 'Administrator', 'Der virtuelle Mitarbeiter Administrator wird systemseitig eingetragen, wenn Datensätze automatisch generiert werden und ein angemeldeter Mitarbeiter erforderlich ist.', true, '2013-01-17 00:00:00', 6000, NULL, NULL);
INSERT INTO employee_descriptions VALUES (2, 65534, 1, 'Gast', 'Der virtuelle Mitarbeiter Gast wird systemseitig eingetragen, wenn Datensätze automatisch aus externen Quellen generiert werden und ein angemeldeter Mitarbeiter erforderlich ist.', true, '2013-01-17 00:00:00', 6000, NULL, NULL);


--
-- Name: employee_descriptions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_descriptions_id_seq', 2, true);


--
-- Data for Name: employee_disciplinarians; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: employee_disciplinarians_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_disciplinarians_id_seq', 1, false);


--
-- Data for Name: employee_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO employee_groups VALUES (49, 'DE', 'DE', false, 5049, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6000, 'Administratoren', 'ITO', false, 6000, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, true);
INSERT INTO employee_groups VALUES (6001, 'Mitarbeiter', 'MA', false, 6001, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6002, 'Geschäftsleitung', 'GSL', true, 6002, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, true, false, false);
INSERT INTO employee_groups VALUES (6003, 'Vorstand', 'CEO', false, 6003, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6004, 'Organisation', 'ORG', false, 6004, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6005, 'IT', 'IT', false, 6005, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6006, 'Einkauf', 'EK', true, 6006, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6007, 'Logistik', 'LG', false, 6007, false, false, true, false, false, true, false, false, false, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6008, 'Vertrieb', 'VK', false, 6008, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, true, false, false, false);
INSERT INTO employee_groups VALUES (6009, 'Produktion', 'PM', true, 6009, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6010, 'Personal', 'P', false, 6010, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6011, 'Finanzen', 'F', true, 6011, false, false, true, false, false, false, false, false, false, true, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6012, 'Marketing', 'M', false, 6012, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false);
INSERT INTO employee_groups VALUES (6013, 'Vertriebspartner', 'VP', false, 6013, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, '1970-01-01 00:00:00', 6000, NULL, NULL, true, false, false, false);


--
-- Data for Name: employee_group_permissions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO employee_group_permissions VALUES (1, 6000, 'client_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (2, 6000, 'organisation_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (3, 6000, 'permission_grant', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (4, 6002, 'product_planning', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (5, 6002, 'close_todos', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (6, 6002, 'contact_assign', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (7, 6002, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (8, 6002, 'customer_service', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (9, 6002, 'executive', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (10, 6002, 'executive_branch', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (11, 6002, 'executive_logistics', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (12, 6002, 'executive_sales', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (13, 6002, 'global_business_case_access', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (14, 6002, 'ignore_price_limit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (15, 6002, 'manager_add', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (16, 6002, 'margin_change', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (17, 6002, 'open_project_list', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (18, 6002, 'requests_cancel', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (19, 6002, 'sales_change', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (20, 6002, 'sales_monitoring_branch_selection', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (21, 6002, 'technics', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (22, 6002, 'time_recording_approvals', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (23, 6002, 'time_recording_display', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (24, 6004, 'close_todos', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (25, 6004, 'contact_assign', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (26, 6004, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (27, 6004, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (28, 6004, 'mailing_lists_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (29, 6004, 'office_management', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (30, 6004, 'requests_queries_by_branch', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (31, 6004, 'sales', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (32, 6004, 'sales_agent_config', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (33, 6004, 'sales_change', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (34, 6004, 'secretary', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (35, 6004, 'xsl_download', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (36, 6005, 'product_selections_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (37, 6005, 'branch_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (38, 6005, 'branch_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (39, 6005, 'contact_assign', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (40, 6005, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (41, 6005, 'contact_import', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (42, 6005, 'contact_person_delete', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (43, 6005, 'employee_assign', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (44, 6005, 'event_pool_config', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (45, 6005, 'global_business_case_access', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (46, 6005, 'hrm', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (47, 6005, 'it', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (48, 6005, 'ldap_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (49, 6005, 'ldap_mail_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (50, 6005, 'letter_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (51, 6005, 'letter_template_delete', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (52, 6005, 'manager_add', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (53, 6005, 'option_add', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (54, 6005, 'option_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (55, 6005, 'organisation_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (56, 6005, 'permission_grant', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (57, 6005, 'query_config_display', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (58, 6005, 'record_export', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (59, 6005, 'requests_queries_by_branch', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (60, 6005, 'template_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (61, 6005, 'xsl_download', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (62, 6005, 'xsl_upload', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (63, 6006, 'product_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (64, 6006, 'product_datasheet_upload', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (65, 6006, 'product_datasheet_upload_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (66, 6006, 'product_description_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (67, 6006, 'product_details_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (68, 6006, 'product_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (69, 6006, 'product_group_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (70, 6006, 'product_planning', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (71, 6006, 'close_todos', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (72, 6006, 'contact_assign', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (73, 6006, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (74, 6006, 'projects_by_employee', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (75, 6006, 'purchase_order_change', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (76, 6006, 'purchase_order_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (77, 6006, 'supplier_change', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (78, 6007, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (79, 6007, 'delivery_note_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (80, 6007, 'executive_logistics', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (81, 6007, 'purchase_delivery_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (82, 6008, 'product_planning', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (83, 6008, 'product_sales_volume', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (84, 6008, 'campaign_config', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (85, 6008, 'close_todos', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (86, 6008, 'contact_assign', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (87, 6008, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (88, 6008, 'create_offer_copy', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (89, 6008, 'customer_service', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (90, 6008, 'fetch_new_project', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (91, 6008, 'global_business_case_access', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (92, 6008, 'ignore_price_limit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (93, 6008, 'mailing_lists_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (94, 6008, 'manager_add', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (95, 6008, 'office_management', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (96, 6008, 'open_project_list', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (97, 6008, 'projects_by_employee', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (98, 6008, 'request_customer_change', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (99, 6008, 'requests_cancel', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (100, 6008, 'requests_queries_by_branch', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (101, 6008, 'sales', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (102, 6008, 'sales_agent_config', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (103, 6008, 'sales_change', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (104, 6008, 'sales_monitoring_branch_selection', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (105, 6008, 'technics', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (106, 6009, 'product_planning', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (107, 6009, 'cancel_fcs', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (108, 6009, 'close_todos', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (109, 6009, 'contact_assign', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (110, 6009, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (111, 6009, 'customer_service', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (112, 6009, 'fetch_new_project', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (113, 6009, 'global_business_case_access', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (114, 6009, 'manager_add', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (115, 6009, 'manager_change', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (116, 6009, 'projects_by_employee', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (117, 6009, 'requests_queries_by_branch', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (118, 6009, 'sales_monitoring_branch_selection', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (119, 6009, 'sales_order_reset', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (120, 6009, 'technics', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (121, 6012, 'campaign_config', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (122, 6012, 'contact_assign', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (123, 6012, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (124, 6012, 'contact_export', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (125, 6012, 'create_news', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (126, 6012, 'employee_internet_config', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (127, 6012, 'internet_contact_form', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (128, 6012, 'newsletter_config', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (129, 6012, 'newsletter_sending', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (130, 6012, 'option_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (131, 6012, 'project_as_reference', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (132, 6012, 'project_internet_config', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (133, 6012, 'website_config', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (134, 6013, 'create_offer_copy', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (135, 6013, 'executive_branch', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (136, 6013, 'menu_admin_deny', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (137, 6013, 'menu_products_deny', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (138, 6013, 'menu_contacts_deny', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (139, 6013, 'notes_sales_deny', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (140, 6013, 'projects_by_employee', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (141, 6013, 'requests_cancel', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (142, 6013, 'sales', '1970-01-01 00:00:00', 6000);
INSERT INTO employee_group_permissions VALUES (143, 6013, 'sales_change', '1970-01-01 00:00:00', 6000);


--
-- Name: employee_group_permissions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_group_permissions_id_seq', 143, true);


--
-- Name: employee_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_groups_id_seq', 6013, true);


--
-- Data for Name: employee_groupware_calendars; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: employee_groupware_calendars_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_groupware_calendars_id_seq', 1, false);


--
-- Data for Name: office_phones; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: employee_phones_relation; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: employee_role_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO employee_role_configs VALUES (1, 6000, NULL, NULL, true, 1, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO employee_role_configs VALUES (2, 6001, NULL, NULL, true, 1, '1970-01-01 00:00:00', 6000, NULL, NULL);


--
-- Name: employee_role_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_role_configs_id_seq', 2, true);


--
-- Data for Name: employee_roles; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO employee_roles VALUES (1, 1, 'Administratoren', NULL, true, 6000, 1, true, '1970-01-01 00:00:00', 6000);
INSERT INTO employee_roles VALUES (2, 2, 'Geschäftsleitung', NULL, true, 6002, 2, false, '1970-01-01 00:00:00', 6000);


--
-- Name: employee_roles_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_roles_id_seq', 2, true);


--
-- Name: employee_status_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_status_id_seq', 10, true);


--
-- Name: employee_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employee_types_id_seq', 4, true);


--
-- Name: employees_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('employees_id_seq', 6001, true);


--
-- Name: event_config_contact_appointments_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_config_contact_appointments_id_seq', 300004, true);


--
-- Name: event_config_fcs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_config_fcs_id_seq', 1000023, true);


--
-- Name: event_config_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_config_infos_id_seq', 9000002, false);


--
-- Name: event_config_purchase_changed_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_config_purchase_changed_id_seq', 700003, true);


--
-- Name: event_config_request_appointments_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_config_request_appointments_id_seq', 400004, true);


--
-- Name: event_config_sales_appointments_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_config_sales_appointments_id_seq', 500004, true);


--
-- Name: event_config_sales_changed_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_config_sales_changed_id_seq', 600005, true);


--
-- Name: event_config_tasks_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_config_tasks_id_seq', 8000002, true);


--
-- Data for Name: event_config_terminations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO event_config_terminations VALUES (9000001, 1, 1000008, 39);
INSERT INTO event_config_terminations VALUES (9000002, 1, 1000010, 41);
INSERT INTO event_config_terminations VALUES (9000003, 1, 1000011, 46);
INSERT INTO event_config_terminations VALUES (9000004, 1, 1000012, 52);
INSERT INTO event_config_terminations VALUES (9000005, 1, 1000012, 53);
INSERT INTO event_config_terminations VALUES (9000006, 1, 1000013, 54);
INSERT INTO event_config_terminations VALUES (9000007, 1, 1000015, 55);
INSERT INTO event_config_terminations VALUES (9000008, 1, 1000012, 56);
INSERT INTO event_config_terminations VALUES (9000009, 1, 1000012, 57);
INSERT INTO event_config_terminations VALUES (9000010, 1, 1000013, 60);
INSERT INTO event_config_terminations VALUES (9000011, 1, 1000017, 60);
INSERT INTO event_config_terminations VALUES (9000012, 1, 1000018, 61);
INSERT INTO event_config_terminations VALUES (9000013, 1, 1000011, 61);
INSERT INTO event_config_terminations VALUES (9000014, 1, 1000015, 61);
INSERT INTO event_config_terminations VALUES (9000015, 2, 2000002, 14);
INSERT INTO event_config_terminations VALUES (9000016, 2, 2000008, 17);
INSERT INTO event_config_terminations VALUES (9000017, 2, 2000009, 17);


--
-- Name: event_config_terminations_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_config_terminations_id_seq', 9000017, true);


--
-- Data for Name: event_config_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO event_config_types VALUES (1, 'Workflow - Auftrag', 7, 'event_config_fcs_id_seq', false, true, false, false, true, 'project_fcs_actions', 'v_event_fcs_starts_projects', '/loadSales.do', true, 'com.osserp.core.projects.ProjectFcsActionManager', 'flowControlActions', NULL);
INSERT INTO event_config_types VALUES (2, 'Workflow - Anfrage', 7, 'event_configs_id_seq', false, true, false, false, true, 'request_fcs_actions', 'v_event_fcs_starts_requests', '/loadRequest.do', false, 'com.osserp.core.requests.RequestFcsActionManager', 'requestFcsActions', NULL);
INSERT INTO event_config_types VALUES (3, 'Termin (Kontakt)', 7, 'event_config_contact_appointments_id_seq', true, false, false, false, true, NULL, NULL, NULL, false, NULL, NULL, NULL);
INSERT INTO event_config_types VALUES (4, 'Termin (Anfrage)', 7, 'event_config_request_appointments_id_seq', true, false, false, false, true, NULL, NULL, NULL, false, NULL, NULL, NULL);
INSERT INTO event_config_types VALUES (5, 'Termin (Auftrag)', 7, 'event_config_sales_appointments_id_seq', true, false, false, false, true, NULL, NULL, NULL, false, NULL, NULL, NULL);
INSERT INTO event_config_types VALUES (6, 'Änderung Auftrag', 7, 'event_config_sales_changed_id_seq', false, false, true, false, true, NULL, NULL, NULL, false, NULL, NULL, NULL);
INSERT INTO event_config_types VALUES (7, 'Änderung Bestellung', 7, 'event_config_purchase_changed_id_seq', false, false, true, false, true, NULL, NULL, NULL, false, NULL, NULL, NULL);
INSERT INTO event_config_types VALUES (8, 'Zeiterfassung', 7, 'event_config_tasks_id_seq', false, false, false, true, true, NULL, NULL, NULL, false, NULL, NULL, NULL);
INSERT INTO event_config_types VALUES (9, 'Änderung Anfrage', 7, 'event_configs_id_seq', false, false, true, false, true, NULL, NULL, '/loadRequest.do', false, NULL, NULL, NULL);
--INSERT INTO event_config_types VALUES (10, 'Neue E-Mail', 7, 'event_configs_id_seq', false, false, true, false, true, NULL, NULL, '/loadRequest.do', false, NULL, NULL, NULL);


--
-- Data for Name: event_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO event_configs VALUES (1000001, 1, 6, 22, 'Anzahlungsrechnung bezahlt', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000002, 1, 17, 22, 'Lieferrechnung bezahlt', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000003, 1, 25, 22, 'Teilrechnung bezahlt', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000004, 1, 23, 22, 'Teilzahlung erfolgt', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000005, 1, 19, 22, 'Schlussrechnung bezahlt', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000006, 1, 27, 22, 'Nachberechnung bezahlt', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000007, 1, 35, 22, 'Schlussrechnung bezahlt', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000008, 1, 38, 1, 'Neues Projekt', NULL, '/loadSales.do', 172800000, 0, NULL, NULL, false, true, 1, false, false, false, false, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000009, 1, 39, 1, 'Übernahme durch Zuständigen', NULL, '/loadSales.do', 259200000, 345600000, NULL, NULL, false, false, NULL, false, true, false, false, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000010, 1, 40, 1, 'Kalkulation erstellt', NULL, '/loadSales.do', 86400000, 0, NULL, NULL, false, true, 1, false, false, false, false, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000011, 1, 45, 1, 'Anzahlung erwartet', NULL, '/loadSales.do', 172800000, 0, NULL, NULL, false, false, NULL, true, false, false, false, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000012, 1, 46, 1, 'Anzahlungsrechnung bezahlt', NULL, '/loadSales.do', 172800000, 0, NULL, NULL, false, false, NULL, false, true, false, false, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000013, 1, 52, 22, 'Teillieferung durchgeführt', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, false, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000014, 1, 53, 22, 'Lieferung abgeschlossen', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, false, NULL, false, true, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000015, 1, 54, 1, 'Zahlung zur Lieferung erwartet', NULL, '/loadSales.do', 172800000, 0, NULL, NULL, false, false, NULL, true, false, false, false, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000016, 1, 56, 2, 'Beginn der Installation', NULL, '/loadSales.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000017, 1, 57, 2, 'Installation abgeschlossen', NULL, '/loadSales.do', 432000000, 172800000, NULL, NULL, false, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000018, 1, 60, 1, 'Schlusszahlung erwartet', NULL, '/loadSales.do', 172800000, NULL, NULL, NULL, false, false, NULL, true, false, false, false, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000019, 1, 61, 2, 'Schlussrechnung bezahlt', NULL, '/loadSales.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000020, 1, 73, 2, 'Projekt gestoppt', NULL, '/loadSales.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000021, 1, 74, 2, 'Projekt gecancelt', NULL, '/loadSales.do', 172800000, 0, NULL, NULL, true, false, NULL, true, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000022, 1, 70, 22, 'Teilrechnung bezahlt', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (1000023, 1, 48, 22, 'Auslieferung autorisiert', NULL, '/loadProject.do', 172800000, 0, NULL, NULL, true, false, NULL, true, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000001, 2, 14, 1, 'Neue Anfrage', NULL, '/loadRequest.do', 86400000, NULL, NULL, NULL, true, false, NULL, true, false, false, false, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000002, 2, 7, 1, 'Vertrieb zuordnen', NULL, '/loadRequest.do', 86400000, NULL, NULL, NULL, true, true, 1, false, false, false, false, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000003, 2, 8, 1, 'Beratungstermin vereinbaren', NULL, '/loadRequest.do', 604800000, NULL, NULL, NULL, true, false, NULL, true, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000004, 2, 17, 1, 'Infopaket wurde versendet', NULL, '/loadRequest.do', 172800000, NULL, NULL, NULL, true, false, NULL, true, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000005, 2, 17, 1, 'Informationsmaterial wurde versendet', NULL, '/loadRequest.do', 172800000, NULL, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000006, 2, 18, 1, 'Datenerfassung durchgeführt', NULL, '/loadRequest.do', 172800000, NULL, NULL, NULL, true, false, NULL, true, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000007, 2, 10, 1, 'Angebot versenden', NULL, '/loadRequest.do', 172800000, NULL, NULL, NULL, true, true, 1, false, false, false, false, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000008, 2, 15, 1, 'Infomaterial per Mail versenden', NULL, '/loadRequest.do', 172800000, NULL, NULL, NULL, false, true, 1, false, false, false, false, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000009, 2, 16, 1, 'Infomaterial per Post versenden', NULL, '/loadRequest.do', 172800000, NULL, NULL, NULL, false, true, 1, false, false, false, false, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000010, 2, 18, 1, 'Anfragedetails geklärt', NULL, '/loadRequest.do', 172800000, NULL, NULL, NULL, true, true, 1, false, false, false, true, false, false, false, false, NULL);
INSERT INTO event_configs VALUES (300000, 3, NULL, 2, 'Termin vereinbaren', NULL, '/contacts.do?method=load', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (300001, 3, NULL, 2, 'Termin', NULL, '/contacts.do?method=load', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (300002, 3, NULL, 2, 'Kontaktaufnahme', NULL, '/contacts.do?method=load', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (300003, 3, NULL, 2, 'Wichtige Info', NULL, '/contacts.do?method=load', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, true, NULL);
INSERT INTO event_configs VALUES (300004, 3, NULL, 2, 'Wiedervorlage', NULL, '/contacts.do?method=load', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, true, false, NULL);
INSERT INTO event_configs VALUES (400000, 4, NULL, 2, 'Kontaktaufnahme', NULL, '/loadRequest.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (400001, 4, NULL, 2, 'Datenerfassung', NULL, '/loadRequest.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (400002, 4, NULL, 2, 'Besuchstermin', NULL, '/loadRequest.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (400003, 4, NULL, 2, 'Wichtige Info', NULL, '/loadRequest.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, true, NULL);
INSERT INTO event_configs VALUES (400004, 4, NULL, 2, 'Wiedervorlage', NULL, '/loadRequest.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, true, false, NULL);
INSERT INTO event_configs VALUES (500000, 5, NULL, 2, 'Termin', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (500001, 5, NULL, 2, 'Kontaktaufnahme', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (500002, 5, NULL, 2, 'Wiedervorlage', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (500003, 5, NULL, 2, 'Wichtige Info', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, true, NULL);
INSERT INTO event_configs VALUES (500004, 5, NULL, 2, 'Wiedervorlage', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, true, false, NULL);
INSERT INTO event_configs VALUES (600000, 6, NULL, 1, 'Auftrag: Liefertermin geändert', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (600001, 6, NULL, 1, 'Auftrag: kommissionierbereit', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (600002, 6, NULL, 1, 'Auftrag: Liefertermin geändert', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (600003, 6, NULL, 1, 'Auftrag: kommissionierbereit', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (600004, 6, NULL, 1, 'Auftrag: Liefermenge geändert', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (600005, 6, NULL, 1, 'Auftrag: Liefermenge geändert', NULL, '/loadSales.do', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (700000, 7, NULL, 1, 'Bestellung: Liefertermin geändert', NULL, '/purchaseOrder.do?method=display', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (700001, 7, NULL, 1, 'Bestellung: Liefermenge geändert', NULL, '/purchaseOrder.do?method=display', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (700002, 7, NULL, 1, 'Bestellung: Änderung Bestellbestätigung', NULL, '/purchaseOrder.do?method=display', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (700003, 7, NULL, 1, 'Bestellung: Im System gelöscht', NULL, '/recordPdfDisplay.do', NULL, NULL, NULL, NULL, true, true, 1, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (8000001, 8, NULL, 1, 'Zeiterfassung: Intervallbuchung; Genehmigung erwartet', NULL, '/app/hrm/timeRecordingApproval/forward', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (8000002, 8, NULL, 1, 'Zeiterfassung: Korrekturbuchung; Genehmigung erwartet', NULL, '/app/hrm/timeRecordingApproval/forward', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000011, 9, NULL, 1, 'Herkunft geändert - Kontaktaufnahme', 'Wird ausgelöst, wenn Herkunft nachträglich geändert wird.', '/loadRequest.do', NULL, NULL, NULL, NULL, true, false, NULL, true, false, false, true, true, false, false, false, NULL);
INSERT INTO event_configs VALUES (2000012, 9, NULL, 1, 'Neue Anfrage eingestellt', 'Benachrichtigung über neue Anfrage', '/loadRequest.do', NULL, NULL, NULL, NULL, true, false, NULL, false, false, false, true, true, false, false, false, NULL);


--
-- Name: event_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_configs_id_seq', 2000012, true);


--
-- Data for Name: event_history; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: event_notes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: event_notes_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_notes_id_seq', 1, false);


--
-- Data for Name: event_recipient_pools; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO event_recipient_pools VALUES (1, 'Auftragsabwicklung', NULL, NULL);


--
-- Name: event_recipient_pools_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_recipient_pools_id_seq', 1, true);


--
-- Data for Name: event_recipients; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: event_recipients_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_recipients_id_seq', 1, true);


--
-- Data for Name: event_tasks; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: event_task_event_config_relations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: event_tasks_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_tasks_id_seq', 1, false);


--
-- Data for Name: event_tickets; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: event_ticket_parameters; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: event_ticket_parameters_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_ticket_parameters_id_seq', 1, false);


--
-- Data for Name: event_ticket_recipients; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: event_ticket_recipients_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_ticket_recipients_id_seq', 1, false);


--
-- Name: event_tickets_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('event_tickets_id_seq', 1, false);


--
-- Data for Name: events; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('events_id_seq', 1, false);



--
-- Data for Name: letter_document_obj; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: letter_document_obj_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letter_document_obj_id_seq', 1, false);


--
-- Data for Name: letter_documents; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: letter_documents_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letter_documents_id_seq', 1, false);


--
-- Data for Name: letter_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO letter_status VALUES (0, 'created', false);
INSERT INTO letter_status VALUES (1, 'changed', false);
INSERT INTO letter_status VALUES (2, 'printed', false);
INSERT INTO letter_status VALUES (3, 'released', true);


--
-- Data for Name: letters; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: letter_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: letter_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letter_infos_id_seq', 1, false);


--
-- Data for Name: letter_paragraphs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: letter_paragraphs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letter_paragraphs_id_seq', 1, false);


--
-- Data for Name: letter_parameters; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: letter_parameters_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letter_parameters_id_seq', 1, false);


--
-- Name: letter_status_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letter_status_id_seq', 3, true);


--
-- Data for Name: letter_templates; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: letter_template_paragraphs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: letter_template_paragraphs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letter_template_paragraphs_id_seq', 1, false);


--
-- Name: letter_templates_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letter_templates_id_seq', 1, false);


--
-- Name: letter_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letter_types_id_seq', 11, true);


--
-- Name: letters_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('letters_id_seq', 1, false);


--
-- Data for Name: office_contacts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO office_contacts VALUES (1, 'Vertrieb', NULL, NULL);
INSERT INTO office_contacts VALUES (2, 'Technik', NULL, NULL);


--
-- Name: office_contacts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('office_contacts_id_seq', 2, true);


--
-- Name: office_phones_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('office_phones_id_seq', 1, false);


--
-- Data for Name: partlists; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: partlist_positions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: partlist_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: partlist_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('partlist_items_id_seq', 1, false);


--
-- Name: partlist_positions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('partlist_positions_id_seq', 1, false);


--
-- Name: partlists_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('partlists_id_seq', 1, false);


--
-- Data for Name: project_contract_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO project_contract_status (id, name, i18nkey, running) VALUES (1, 'Entwurf', NULL, false);
INSERT INTO project_contract_status (id, name, i18nkey, running) VALUES (2, 'Angebot', NULL, false);
INSERT INTO project_contract_status (id, name, i18nkey, running) VALUES (3, 'Laufend', NULL, true);
INSERT INTO project_contract_status (id, name, i18nkey, running) VALUES (4, 'Beendet', NULL, false);


--
-- Name: project_contract_status_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_contract_status_id_seq', 4, true);


--
-- Data for Name: project_contract_terminations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_contract_terminations_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_contract_terminations_id_seq', 1, false);


--
-- Data for Name: project_contract_terms; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_contract_terms_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_contract_terms_id_seq', 1, false);


--
-- Data for Name: project_contract_terms_properties; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_contract_terms_properties_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_contract_terms_properties_id_seq', 1, false);


--
-- Data for Name: project_contracts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: project_contract_terms_values; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_contract_terms_values_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_contract_terms_values_id_seq', 1, false);


--
-- Name: project_contract_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_contract_types_id_seq', 2, true);


--
-- Name: project_contracts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_contracts_id_seq', 1, false);


--
-- Data for Name: project_fcs_actions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO project_fcs_actions VALUES (1, 2, 1, 'Auftrag', NULL, 5, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (2, 2, 2, 'Kalkulation erstellt', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (3, 2, 3, 'Übernahme durch Zuständigen', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (4, 2, 4, 'Auftragsbestätigung versendet', NULL, 15, false, true, 6009, false, false, 'orderRequest', false, NULL, true, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (5, 2, 5, 'Anzahlungsrechnung gestellt', NULL, 30, false, true, 6009, false, false, 'invoiceRequest', false, NULL, true, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (6, 2, 6, 'Anzahlungsrechnung bezahlt', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (7, 2, 7, 'Zahlung/Zahlverfahren geklärt', 'Zeigt an, das die Finanzierung kundenseitig bestätigt ist', 0, false, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (8, 2, 8, 'Auslieferung autorisiert', 'Zeigt an, das die Auslieferung ungeachtet Randbedigungen autorisiert ist', 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (9, 2, 9, 'Material disponiert', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (10, 2, 10, 'Wareneingang erfolgt', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (11, 2, 11, 'Kommissionierschein übergeben', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (12, 2, 12, 'lieferbereit kommissioniert', 'Der Auftrag ist komplett kommissioniert, und steht zur Auslieferung bereit.', 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (13, 2, 13, 'Lieferschein erstellt', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (14, 2, 14, 'Teillieferung durchgeführt', NULL, 0, true, true, 6009, true, false, 'partialDeliveryRequest', false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (15, 2, 15, 'Lieferung abgeschlossen', NULL, 90, false, true, 6009, false, false, 'deliveryRequest', true, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, true, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (16, 2, 16, 'Lieferrechung gestellt', NULL, 60, false, true, 6009, false, false, 'invoiceRequest', false, NULL, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (17, 2, 17, 'Lieferrechnung bezahlt', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (18, 2, 18, 'Schlussrechnung gestellt', NULL, 95, false, true, 6009, false, false, 'invoiceRequest', false, NULL, true, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (19, 2, 19, 'Schlussrechnung bezahlt', NULL, 0, false, true, 6009, false, false, 'finalInvoiceResponse', false, NULL, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (20, 2, 20, 'Auftrag abgeschlossen', NULL, 100, false, true, 6009, false, false, 'deliveryRequest', false, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (21, 2, 21, 'Info an Auftragsabwicklung', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, true, false, false, true, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (22, 2, 22, 'Info an Zuständigen', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, true, false, false, true, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (23, 2, 23, 'Teilzahlung erfolgt', NULL, 0, true, false, 6009, true, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (24, 2, 24, 'Teilrechnung gestellt', NULL, 85, true, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (25, 2, 25, 'Teilrechnung bezahlt', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (26, 2, 26, 'Nachberechnung erstellt', NULL, 0, true, true, 6009, false, false, 'invoiceRequest', false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (27, 2, 27, 'Nachberechnung bezahlt', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (28, 2, 28, 'Auftrag gestoppt', 'Aktion wird gesetzt, wenn das Auftrag unterbrochen wird. Der Grund soll als Bemerkung angegeben werden', 0, false, false, 6009, true, false, NULL, false, NULL, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (29, 2, 29, 'Auftrag gecancelt', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (902, 2, 30, 'Auftrag importiert', NULL, 100, false, false, 6000, false, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, 1, false, false, true);
INSERT INTO project_fcs_actions VALUES (905, 2, 31, 'Auftrag abbrechen', NULL, 100, false, false, 6000, true, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, 1, false, false, false, false, true, 'sales_close_unconditionally');
INSERT INTO project_fcs_actions VALUES (30, 3, 1, 'Auftrag', NULL, 5, false, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (31, 3, 2, 'Auftragsbestätigung', NULL, 15, false, true, 6009, false, false, 'orderRequest', false, NULL, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (32, 3, 3, 'Auslieferung autorisiert', 'Zeigt an, das die Auslieferung ungeachtet Randbedigungen autorisiert ist', 0, false, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (33, 3, 4, 'Lieferung abgeschlossen', NULL, 90, false, true, 6009, false, false, 'deliveryRequest', true, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, true, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (34, 3, 5, 'Schlussrechnung gestellt', NULL, 95, false, true, 6009, false, false, 'invoiceRequest', false, NULL, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (35, 3, 6, 'Schlussrechnung bezahlt', NULL, 0, false, true, 6009, false, false, 'finalInvoiceResponse', false, NULL, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (36, 3, 7, 'Abgeschlossen', NULL, 100, false, true, 6009, false, false, 'deliveryRequest', false, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (37, 3, 8, 'Gecancelt', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (906, 3, 9, 'Auftrag abbrechen', NULL, 100, false, false, 6000, true, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, 1, false, false, false, false, true, 'sales_close_unconditionally');
INSERT INTO project_fcs_actions VALUES (38, 4, 1, 'Auftrag', NULL, 5, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (39, 4, 2, 'Übernahme durch Zuständigen', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (40, 4, 3, 'Kalkulation erstellt', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (41, 4, 4, 'Projektfreigabe an Vertrieb', NULL, 0, false, true, 6009, false, false, 'orderRequest', false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (42, 4, 5, 'Projektfreigabe bestätigt', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (43, 4, 6, 'Kalkulation geprüft', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (44, 4, 7, 'Auftragsbestätigung versendet', NULL, 15, false, true, 6009, false, false, 'orderRequest', false, NULL, true, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (45, 4, 8, 'Anzahlungsrechnung gestellt', NULL, 30, false, true, 6009, false, false, 'invoiceRequest', false, NULL, true, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (46, 4, 9, 'Anzahlungsrechnung bezahlt', NULL, 0, false, true, 6009, false, false, 'downpaymentResponse', false, NULL, true, false, false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (47, 4, 10, 'Finanzierung bestätigt', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (48, 4, 11, 'Auslieferung autorisiert', 'Zeigt an, das die Auslieferung ungeachtet Randbedigungen autorisiert ist', 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (49, 4, 12, 'Hauptkomponenten/Material disponiert', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (50, 4, 13, 'Hauptkomponenten kommissioniert', 'Das Projekt ist komplett kommissioniert, und steht zur Auslieferung bereit.', 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (51, 4, 14, 'Hauptkomponenten geliefert', NULL, 0, false, true, 6009, false, false, 'mainComponentDeliveryRequest', false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (52, 4, 15, 'Teillieferung durchgeführt', NULL, 0, true, true, 6009, true, false, 'partialDeliveryRequest', false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (53, 4, 16, 'Lieferung abgeschlossen', NULL, 90, false, true, 6009, false, false, 'deliveryRequest', true, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, true, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (54, 4, 17, 'Lieferrechnung gestellt', NULL, 60, false, true, 6009, false, false, 'invoiceRequest', false, NULL, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (55, 4, 18, 'Lieferrechnung bezahlt', NULL, 0, false, true, 6009, false, false, 'deliveryInvoiceResponse', false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (56, 4, 19, 'Beginn der Installation', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (57, 4, 20, 'Installation abgeschlossen', NULL, 0, false, true, 6009, false, false, NULL, true, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (58, 4, 21, 'Inbetriebnahme terminiert', 'Zeigt an, das ein Inbetriebnahmedatum vereinbart wurde', 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (59, 4, 22, 'Inbetriebnahme', NULL, 0, false, true, 6009, false, false, NULL, true, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', true, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (60, 4, 23, 'Schlussrechnung gestellt', NULL, 95, false, true, 6009, false, false, 'invoiceRequest', false, NULL, true, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (61, 4, 24, 'Schlussrechnung bezahlt', NULL, 0, false, true, 6009, false, false, 'finalInvoiceResponse', false, NULL, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (62, 4, 25, 'Projektunterlagen abschlussbereit', 'Zeigt an, das die Abschlussunterlagen erstellt sind', 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (63, 4, 26, 'Abschlussunterlagen an Vertrieb', NULL, 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (64, 4, 27, 'Abschlussunterlagen an Kunden', 'Zeigt an, das die Abschlussunterlagen an den Kunden übergeben wurden', 0, false, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (65, 4, 28, 'Projekt abgeschlossen', NULL, 100, false, true, 6009, false, false, 'deliveryRequest', false, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (66, 4, 29, 'Info an Auftragsabwicklung', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, true, false, false, true, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (67, 4, 30, 'Info an Projektabwicklung', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, true, false, false, true, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (68, 4, 31, 'Teilzahlung erfolgt', NULL, 0, true, true, 6009, true, false, 'paymentResponse', false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (69, 4, 32, 'Teilrechnung gestellt', NULL, 85, true, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (70, 4, 33, 'Teilrechnung bezahlt', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (71, 4, 34, 'Nachberechnung erstellt', NULL, 0, true, true, 6009, false, false, 'invoiceRequest', false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (72, 4, 35, 'Nachberechnung bezahlt', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (73, 4, 36, 'Projekt gestoppt', 'Aktion wird gesetzt, wenn das Projekt unterbrochen wird', 0, false, false, 6009, true, false, NULL, false, NULL, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (74, 4, 37, 'Projekt gecancelt', NULL, 0, false, false, 6009, true, false, NULL, false, NULL, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (904, 4, 38, 'Auftrag importiert', NULL, 100, false, false, 6000, false, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, 1, false, false, true);
INSERT INTO project_fcs_actions VALUES (907, 4, 39, 'Auftrag abbrechen', NULL, 100, false, false, 6000, true, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, 1, false, false, false, false, true, 'sales_close_unconditionally');
INSERT INTO project_fcs_actions VALUES (80, 5, 1, 'Auftrag', NULL, 5, false, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (81, 5, 2, 'Auftrag autorisiert', 'Zeigt an, das Auftrag/Leistung ungeachtet Randbedigungen autorisiert ist', 0, false, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (82, 5, 3, 'Auftragsbestätigung', NULL, 15, false, true, 6009, false, false, 'orderRequest', false, NULL, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (83, 5, 4, 'Beginn der Leistungserbringung', 'Zeigt an, das die Leistungserbringung begonnen hat', 0, false, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (84, 5, 5, 'Leistungszeitraum abgeschlossen', NULL, 90, false, true, 6009, false, false, 'deliveryRequest', true, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, true, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (85, 5, 6, 'Leistungsrechnung gestellt', NULL, 95, false, true, 6009, false, false, 'invoiceRequest', false, NULL, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (86, 5, 7, 'Leistungsrechnung bezahlt', NULL, 0, false, true, 6009, false, false, 'finalInvoiceResponse', false, NULL, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (87, 5, 8, 'Abgeschlossen', NULL, 100, false, true, 6009, false, false, 'deliveryRequest', false, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (88, 5, 9, 'Auftrag unterbrochen', 'Aktion wird gesetzt, wenn das Projekt unterbrochen wird', 0, false, false, 6009, true, false, NULL, false, NULL, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (89, 5, 10, 'Gecancelt', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (908, 5, 11, 'Auftrag abbrechen', NULL, 100, false, false, 6000, true, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, 1, false, false, false, false, true, 'sales_close_unconditionally');
INSERT INTO project_fcs_actions VALUES (100, 6, 1, 'Auftrag', NULL, 5, false, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (101, 6, 3, 'Auftrag autorisiert', 'Zeigt an, das Auftrag/Leistung ungeachtet Randbedigungen autorisiert ist', 0, false, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (102, 6, 2, 'Auftragsbestätigung', NULL, 15, false, true, 6009, false, false, 'orderRequest', false, NULL, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (103, 6, 4, 'Beginn der Leistung', 'Zeigt an, das Leistungserbringung begonnen hat', 0, false, false, 6009, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (104, 6, 5, 'Leistungszeitraum abgeschlossen', NULL, 0, false, true, 6009, false, false, 'deliveryRequest', true, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, true, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (105, 6, 6, 'Leistungsrechnung gestellt', NULL, 50, false, true, 6009, false, false, 'invoiceRequest', false, NULL, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (106, 6, 7, 'Leistungsrechnung bezahlt', NULL, 90, false, true, 6009, false, false, 'finalInvoiceResponse', false, NULL, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (107, 6, 8, 'Abgeschlossen', NULL, 100, false, true, 6009, false, false, 'deliveryRequest', false, 'com.osserp.core.sales.SalesDeliveryClosingExecutor', false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (108, 6, 9, 'Auftrag unterbrochen', 'Aktion wird gesetzt, wenn das Projekt unterbrochen wird', 0, false, false, 6009, true, false, NULL, false, NULL, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (109, 6, 10, 'Gecancelt', NULL, 0, true, false, 6009, false, false, NULL, false, NULL, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, false);
INSERT INTO project_fcs_actions VALUES (909, 6, 11, 'Auftrag abbrechen', NULL, 100, false, false, 6000, true, false, NULL, false, NULL, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, 1, false, false, false, false, true, 'sales_close_unconditionally');

--
-- Data for Name: sales_status_closed; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO sales_status_closed VALUES (1, 'Abgeschlossen', false, true, 'Auftrag wurde regulär abgeschlossen', NULL, '1970-01-01 00:00:00', 6000, false, false, false);
INSERT INTO sales_status_closed VALUES (2, 'Abgebrochen', false, true, 'Ausfall Kunde, Auftrag abgebrochen', NULL, '1970-01-01 00:00:00', 6000, false, false, false);
INSERT INTO sales_status_closed VALUES (3, 'Deaktiviert', false, true, 'Kunde deaktiviert, Auftrag abgebrochen', NULL, '1970-01-01 00:00:00', 6000, false, false, false);


--
-- Data for Name: projects; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: project_fcs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_fcs_actions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_fcs_actions_id_seq', 1000, true);


--
-- Data for Name: project_fcs_dependencies; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO project_fcs_dependencies VALUES (1, 20, 19);
INSERT INTO project_fcs_dependencies VALUES (2, 36, 35);
INSERT INTO project_fcs_dependencies VALUES (3, 65, 61);


--
-- Name: project_fcs_dependencies_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_fcs_dependencies_id_seq', 3, true);


--
-- Name: project_fcs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_fcs_id_seq', 1, false);


--
-- Data for Name: project_fcs_wastebasket_defaults; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_fcs_wastebasket_defaults_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_fcs_wastebasket_defaults_id_seq', 1, false);


--
-- Data for Name: project_fcs_wastebaskets; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_fcs_wastebaskets_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_fcs_wastebaskets_id_seq', 1, false);


--
-- Data for Name: project_installation_dates; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_installation_dates_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_installation_dates_id_seq', 1, false);


--
-- Data for Name: project_manager_states; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO project_manager_states VALUES (1, 'Vertretung', false);
INSERT INTO project_manager_states VALUES (2, 'Mitwirkung', false);


--
-- Name: project_manager_states_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_manager_states_id_seq', 2, true);


--
-- Name: project_plans_plan_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_plans_plan_id_seq', 100001, false);


--
-- Data for Name: project_tracking_document_obj; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_tracking_document_obj_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_tracking_document_obj_id_seq', 1, false);


--
-- Data for Name: project_tracking_documents; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_tracking_documents_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_tracking_documents_id_seq', 1, false);


--
-- Data for Name: project_tracking_record_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_tracking_record_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_tracking_record_types_id_seq', 1, false);


--
-- Data for Name: project_tracking_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO project_tracking_types VALUES (1, 'Datum - Stunden a 15 Min', 'Je angefangene 15 Minuten, Eingabe erfolgt dezimal', '1970-01-01 00:00:00', 6000, NULL, NULL, true, false, false, false, 'xsl/documents/project_tracking');
INSERT INTO project_tracking_types VALUES (2, 'Datum - Zeit', 'Exakte Stunderfassung mit Start und Endzeit', '1970-01-01 00:00:00', 6000, NULL, NULL, false, true, false, false, 'xsl/documents/project_tracking');


--
-- Data for Name: project_trackings; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sales_invoices; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: project_tracking_records; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: project_tracking_records_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_tracking_records_id_seq', 1, false);


--
-- Name: project_tracking_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_tracking_types_id_seq', 2, true);


--
-- Name: project_trackings_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('project_trackings_id_seq', 1, false);


--
-- Name: purchase_delivery_note_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_delivery_note_100_id_seq', 40300001, false);


--
-- Data for Name: record_delivery_conditions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_delivery_conditions VALUES (1, 'ab Werk', NULL, NULL, false);
INSERT INTO record_delivery_conditions VALUES (2, 'frei Haus', NULL, NULL, false);


--
-- Data for Name: purchase_orders; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: record_delivery_note_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_delivery_note_types VALUES (1, 'Lieferung', 'Warenlieferung zum Kunde', 1, true, true, false, true, false, true, true, true, false, NULL);
INSERT INTO record_delivery_note_types VALUES (2, 'Garantieaustausch', 'Austausch (Garantieleistung)', 4, true, false, true, true, false, true, true, false, false, NULL);
INSERT INTO record_delivery_note_types VALUES (3, 'Einlagerung', 'Warenlieferung vom Lieferant', 8, false, true, false, true, false, true, true, false, false, NULL);
INSERT INTO record_delivery_note_types VALUES (4, 'Rücksendung', 'Rücksendung zum Lieferant', 9, false, false, false, true, false, true, true, false, false, NULL);
INSERT INTO record_delivery_note_types VALUES (5, 'Rechnung', 'Buchung Rechnung über Belegjournal', 5, true, false, false, true, false, true, true, false, false, NULL);
INSERT INTO record_delivery_note_types VALUES (6, 'Rücklieferung', 'Austausch/Rücklieferung vom Kunde', 3, true, false, true, true, false, true, true, false, false, NULL);
INSERT INTO record_delivery_note_types VALUES (7, 'Rechnungsstorno', 'Rückbuchung aus Rechnung über Belegjournal', 6, true, false, true, true, false, true, true, false, false, NULL);
INSERT INTO record_delivery_note_types VALUES (8, 'Gutschrift', 'Buchung Gutschrift über Belegjournal', 7, true, false, true, true, false, true, true, false, false, NULL);
INSERT INTO record_delivery_note_types VALUES (9, 'POS', 'Warenabgabe Kasse', 7, true, false, false, true, false, true, true, false, false, NULL);
INSERT INTO record_delivery_note_types VALUES (10, 'Kleinteile', 'Nicht im Auftrag vorhandene Artikel', 2, true, false, false, true, true, false, true, true, false, NULL);


--
-- Data for Name: purchase_delivery_notes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: purchase_delivery_note_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_delivery_note_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_delivery_note_items_id_seq', 1, false);


--
-- Data for Name: purchase_delivery_note_serials; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_delivery_note_serials_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_delivery_note_serials_id_seq', 1, false);


--
-- Name: purchase_invoice_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_invoice_100_id_seq', 40500001, false);


--
-- Data for Name: purchase_invoice_cancellations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: purchase_invoice_cancellation_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_invoice_cancellation_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_invoice_cancellation_items_id_seq', 1, false);


--
-- Data for Name: purchase_invoices; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: purchase_invoice_item_changesets; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_invoice_item_changesets_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_invoice_item_changesets_id_seq', 1, false);


--
-- Data for Name: purchase_invoice_item_history; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_invoice_item_history_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_invoice_item_history_id_seq', 1, false);


--
-- Data for Name: purchase_invoice_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_invoice_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_invoice_items_id_seq', 1, false);


--
-- Data for Name: purchase_invoice_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO purchase_invoice_types VALUES (201, 'Wareneinkauf', 'WE', 1, true, true, true, false, false, false, false, false, true);
INSERT INTO purchase_invoice_types VALUES (205, 'Dienstleistung', 'DI', 2, true, false, false, false, false, false, false, false, true);
INSERT INTO purchase_invoice_types VALUES (208, 'Betriebsbedarf', 'BB', 8, true, true, true, false, false, false, true, false, true);
INSERT INTO purchase_invoice_types VALUES (210, 'Lagerbewertung', 'BW', 7, false, false, true, false, true, true, false, false, false);
INSERT INTO purchase_invoice_types VALUES (251, 'Inventur', 'IV', 3, false, false, true, false, false, true, false, false, false);
INSERT INTO purchase_invoice_types VALUES (252, 'Abschreibung (defekt)', 'AD', 4, false, false, true, false, false, true, false, false, false);
INSERT INTO purchase_invoice_types VALUES (253, 'Abschreibung (Schwund)', 'AS', 5, false, false, true, false, false, true, false, false, false);
INSERT INTO purchase_invoice_types VALUES (254, 'Leihgabe', 'LE', 6, false, false, true, false, false, true, false, false, false);
INSERT INTO purchase_invoice_types VALUES (255, 'Lieferantengutschrift', 'LG', 9, false, true, true, false, false, true, false, true, true);


--
-- Name: purchase_offer_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_offer_100_id_seq', 40100001, false);


--
-- Data for Name: purchase_offer_delivery_dates; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_offer_delivery_dates_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_offer_delivery_dates_id_seq', 1, false);


--
-- Data for Name: purchase_offer_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_offer_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_offer_infos_id_seq', 1, false);


--
-- Data for Name: purchase_offer_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_offer_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_offer_items_id_seq', 1, false);


--
-- Data for Name: purchase_offer_payment_agreements; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_offer_payment_agreements_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_offer_payment_agreements_id_seq', 1, false);


--
-- Data for Name: purchase_offer_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_offer_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_offer_types_id_seq', 1, false);


--
-- Data for Name: purchase_offers; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_order_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_order_100_id_seq', 40200001, false);


--
-- Data for Name: purchase_order_delivery_date_history; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: purchase_order_delivery_date_history_deleted; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_order_delivery_date_history_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_order_delivery_date_history_id_seq', 1, false);


--
-- Data for Name: purchase_order_delivery_dates; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_order_delivery_dates_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_order_delivery_dates_id_seq', 1, false);


--
-- Data for Name: purchase_order_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_order_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_order_infos_id_seq', 1, false);


--
-- Data for Name: purchase_order_item_history; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: purchase_order_item_history_deleted; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_order_item_history_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_order_item_history_id_seq', 1, false);


--
-- Data for Name: purchase_order_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_order_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_order_items_id_seq', 1, false);


--
-- Data for Name: purchase_order_payment_agreements; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_order_payment_agreements_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_order_payment_agreements_id_seq', 1, false);


--
-- Data for Name: purchase_order_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO purchase_order_types VALUES (1, 'Waren', 'WA', 1, NULL, false);
INSERT INTO purchase_order_types VALUES (5, 'Dienstleistung', 'DI', 2, NULL, false);
INSERT INTO purchase_order_types VALUES (8, 'IT-Bedarf', 'IT', 3, NULL, false);


--
-- Data for Name: suppliers; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO suppliers VALUES (10000, 1000000000, 0, NULL, 1, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, NULL, false, false, NULL, NULL, NULL);


--
-- Data for Name: system_company_bank_accounts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_company_bank_accounts VALUES (1, 100, 'Kasse / Terminal', 'Barzahlung', 'Zentrale', '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, false, false, 'BAR', NULL, false, true);
INSERT INTO system_company_bank_accounts VALUES (2, 100, 'Bank Firmenkonto', 'DE99 9999 9999 9999 9999 99', 'GEXXXXXXXXX', '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, true, true, 'KTO', NULL, false, false);


--
-- Data for Name: purchase_payments; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: purchase_payments_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('purchase_payments_id_seq', 1, false);


--
-- Data for Name: record_billing_percentages; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_billing_percentages VALUES (0, '0', false);
INSERT INTO record_billing_percentages VALUES (5, '0.05', false);
INSERT INTO record_billing_percentages VALUES (10, '0.1', false);
INSERT INTO record_billing_percentages VALUES (15, '0.15', false);
INSERT INTO record_billing_percentages VALUES (20, '0.2', false);
INSERT INTO record_billing_percentages VALUES (25, '0.25', false);
INSERT INTO record_billing_percentages VALUES (30, '0.3', false);
INSERT INTO record_billing_percentages VALUES (35, '0.35', false);
INSERT INTO record_billing_percentages VALUES (40, '0.4', false);
INSERT INTO record_billing_percentages VALUES (45, '0.45', false);
INSERT INTO record_billing_percentages VALUES (50, '0.5', false);
INSERT INTO record_billing_percentages VALUES (55, '0.55', false);
INSERT INTO record_billing_percentages VALUES (60, '0.6', false);
INSERT INTO record_billing_percentages VALUES (65, '0.65', false);
INSERT INTO record_billing_percentages VALUES (70, '0.7', false);
INSERT INTO record_billing_percentages VALUES (75, '0.75', false);
INSERT INTO record_billing_percentages VALUES (80, '0.8', false);
INSERT INTO record_billing_percentages VALUES (85, '0.85', false);
INSERT INTO record_billing_percentages VALUES (90, '0.9', false);
INSERT INTO record_billing_percentages VALUES (95, '0.95', false);
INSERT INTO record_billing_percentages VALUES (100, '1.0', false);


--
-- Data for Name: record_billing_type_classes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_billing_type_classes VALUES (1, 'Kundenrechnung', false, false, 'salesInvoices');
INSERT INTO record_billing_type_classes VALUES (2, 'Kundenzahlung', true, false, 'salesPayments');
INSERT INTO record_billing_type_classes VALUES (3, 'Lieferantenrechnung', false, false, 'purchaseInvoices');
INSERT INTO record_billing_type_classes VALUES (4, 'Auszahlung', true, true, 'salesOutPayments');
INSERT INTO record_billing_type_classes VALUES (5, 'Zahlung an Lieferant', true, true, 'purchasePayments');
INSERT INTO record_billing_type_classes VALUES (6, 'Rückzahlung Lieferant', true, false, 'purchaseOutPayments');
INSERT INTO record_billing_type_classes VALUES (7, 'Geldeingang', true, false, 'simpleBilling');
INSERT INTO record_billing_type_classes VALUES (8, 'Geldabgang', true, true, 'simpleBilling');


--
-- Data for Name: record_billing_type_fcs_relations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: record_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_types VALUES (1, 'Angebot', 'salesOffer', true, false, false, false, false, false, false, false, false, NULL, true, false, true, 'AG', false, 'sales_offers', 'sequence', 2, NULL, false, false, false, true, true, true, 3, false);
INSERT INTO record_types VALUES (2, 'Auftrag', 'salesOrder', true, true, false, true, false, true, false, false, true, NULL, true, false, false, 'AU', false, 'sales_orders', 'sequence', 2, NULL, false, false, false, true, true, true, 3, false);
INSERT INTO record_types VALUES (3, 'Lieferschein', 'salesDeliveryNote', false, false, false, true, false, true, false, false, false, NULL, true, false, false, 'LS', false, 'sales_delivery_notes', 'sequence', 2, NULL, false, false, false, true, true, true, 3, false);
INSERT INTO record_types VALUES (4, 'Abschlagsrechung', 'salesDownpayment', false, false, false, false, false, false, true, false, false, NULL, true, false, false, 'AR', false, 'sales_order_downpayments', 'yearlySequence', 2, NULL, false, false, true, true, true, true, 3, true);
INSERT INTO record_types VALUES (5, 'Rechnung', 'salesInvoice', false, false, false, true, false, false, true, false, false, NULL, true, false, true, 'RE', false, 'sales_invoices', 'yearlySequence', 2, 17, true, false, true, true, true, true, 3, true);
INSERT INTO record_types VALUES (6, 'Rücklagerung', NULL, false, false, false, true, false, false, false, false, false, NULL, false, false, false, 'RL', false, 'sales_delivery_notes', 'sequence', 2, NULL, false, false, false, true, false, true, 3, false);
INSERT INTO record_types VALUES (9, 'Gutschrift', 'salesCreditNote', false, false, false, true, false, false, false, false, true, NULL, true, false, false, 'GU', false, 'sales_credit_notes', 'yearlySequence', 2, 18, true, true, false, true, true, true, 3, true);
INSERT INTO record_types VALUES (10, 'Storno', 'salesCancellation', false, false, false, true, false, false, false, false, false, NULL, true, false, false, 'ST', false, 'sales_cancellations', 'yearlySequence', 2, NULL, false, true, false, true, true, true, 3, true);
INSERT INTO record_types VALUES (100, 'Sonstige', 'commonPayment', false, false, false, false, false, false, false, false, false, NULL, false, false, false, 'AZ', false, 'record_payments_other', 'sequence', 2, NULL, false, false, false, true, false, true, 3, false);
INSERT INTO record_types VALUES (101, 'Lieferantenanfrage', 'purchaseOffer', true, true, false, false, false, false, false, false, true, NULL, false, false, false, 'BA', false, 'purchase_offers', 'sequence', 2, NULL, false, false, false, true, false, true, 3, false);
INSERT INTO record_types VALUES (102, 'Bestellung', 'purchaseOrder', true, true, true, true, true, true, false, false, true, NULL, false, false, false, 'BE', false, 'purchase_orders', 'sequence', 2, NULL, false, false, false, true, true, true, 3, false);
INSERT INTO record_types VALUES (103, 'Wareneingang', 'purchaseDeliveryNote', false, false, false, true, false, true, false, false, false, NULL, false, false, false, 'WE', false, 'purchase_delivery_notes', 'sequence', 2, NULL, false, false, false, true, false, true, 3, false);
INSERT INTO record_types VALUES (105, 'Lieferrechnung', 'purchaseInvoice', false, false, false, true, false, true, false, false, true, NULL, false, false, false, 'LR', false, 'purchase_invoices', 'sequence', 2, 15, true, true, false, true, false, true, 3, false);
INSERT INTO record_types VALUES (106, 'Rücksendung', 'purchaseReturn', false, false, false, true, false, false, false, false, false, NULL, false, false, false, 'RM', false, 'purchase_delivery_notes', 'sequence', 2, NULL, false, false, true, true, false, true, 3, false);
INSERT INTO record_types VALUES (107, 'Zulieferrechnung', 'installation', false, false, false, false, false, false, false, false, false, NULL, false, false, false, 'ZR', false, 'purchase_invoices', 'sequence', 2, NULL, false, true, false, true, false, true, 3, false);


--
-- Data for Name: record_billing_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_billing_types VALUES (0, 1, 'Auswahl', NULL, NULL, false, NULL, false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (1, 1, 'Anzahlungsrechnung', NULL, NULL, false, NULL, false, NULL, 4, false, true, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (2, 1, 'Lieferrechnung', NULL, NULL, false, NULL, false, NULL, 4, false, true, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (3, 1, 'Schlussrechnung', NULL, NULL, false, NULL, false, NULL, 5, true, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (4, 1, 'Rechnung manuell', NULL, NULL, false, NULL, false, NULL, 5, true, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (5, 1, 'Teilrechnung', NULL, NULL, false, NULL, false, NULL, 5, true, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (6, 1, 'Abschlagsrechnung manuell', NULL, NULL, false, NULL, false, NULL, 4, false, true, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (7, 1, 'Abschlagsrechnung anteilig', NULL, NULL, false, NULL, false, NULL, 4, false, true, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (21, 3, 'Dienstleistungsrechnung', NULL, NULL, false, NULL, false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (22, 3, 'Warenrechnung', NULL, NULL, false, NULL, false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (51, 2, 'Zahlung', 2, 'payment', false, 'ZA', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (52, 2, 'Teilzahlung', 1, 'partialPayment', false, 'TZ', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (53, 2, 'Gutschrift', 4, 'creditNote', false, 'ZG', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (54, 2, 'Skonto', 3, 'cashDiscount', false, 'SK', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (55, 2, 'Storno', 5, 'cancellation', false, 'ST', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (56, 5, 'Zahlung', 2, 'payment', false, 'ZA', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (57, 5, 'Teilzahlung', 1, 'partialPayment', false, 'TZ', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (58, 5, 'Skonto', 3, 'cashDiscount', false, 'SK', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (59, 2, 'Clearing', 6, 'clearingDecimalAmount', false, 'CL', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (61, 4, 'Auszahlung', 1, 'outpayment', false, 'AZ', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (71, 8, 'Betriebsbedarf', 1, 'costs', false, 'KB', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (72, 8, 'Bankgebühren', 2, 'bankFees', false, 'BG', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (73, 8, 'Sonst.Gebühren', 3, 'otherFees', false, 'SG', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (74, 8, 'Steuerzahlung', 4, 'taxPayment', false, 'SZ', false, NULL, NULL, false, false, false, false, NULL, true, false);
INSERT INTO record_billing_types VALUES (75, 8, 'Lohn-/Gehaltszahlung', 5, 'salary', false, 'LK', false, NULL, NULL, false, false, false, false, NULL, false, true);
INSERT INTO record_billing_types VALUES (76, 8, 'Lohnnebenkosten', 6, 'salaryCosts', false, 'LN', false, NULL, NULL, false, false, false, false, NULL, false, true);
INSERT INTO record_billing_types VALUES (77, 7, 'Steuererstattung', 7, 'taxRefund', false, 'SE', false, NULL, NULL, false, false, false, false, NULL, true, false);
INSERT INTO record_billing_types VALUES (78, 8, 'Lohnsteuerzahlung', 8, 'taxPaymentSalary', false, 'LS', false, NULL, NULL, false, false, false, false, NULL, true, true);
INSERT INTO record_billing_types VALUES (79, 7, 'Lohnsteuererstattung', 9, 'taxRefundSalary', false, 'ES', false, NULL, NULL, false, false, false, false, NULL, true, true);
INSERT INTO record_billing_types VALUES (80, 7, 'Gutschrift Kleinbtr.', 12, 'costs', false, 'KB', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (81, 7, 'Gebührenerstattung', 13, 'otherFeesRefund', false, 'SG', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (82, 7, 'Erstattung Bankgeb.', 14, 'bankFeesRefund', false, 'BG', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (83, 7, 'Erstattung Lohn', 15, 'salaryCostsRefund', false, 'LE', false, NULL, NULL, false, false, false, false, NULL, false, true);
INSERT INTO record_billing_types VALUES (84, 7, 'Gesellschafterdarlehen', 16, 'shareholderLoan', false, 'GD', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (85, 8, 'Darlehensrückzahlung', 17, 'shareholderLoan', false, 'DR', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (86, 8, 'Anteilsrückerwerb', 18, 'shares', false, 'GR', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (87, 7, 'Gesellschaftsanteile', 19, 'shares', false, 'GA', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (88, 8, 'Lohnnebenkosten', 10, 'otherSalaryCosts', false, 'LK', false, NULL, NULL, false, false, false, false, NULL, false, true);
INSERT INTO record_billing_types VALUES (89, 7, 'Erst.Lohnnebenk.', 11, 'otherSalaryCostsRefund', false, 'LE', false, NULL, NULL, false, false, false, false, NULL, false, true);
INSERT INTO record_billing_types VALUES (90, 8, 'Auszahlung', 20, 'booksOutpayment', false, 'UA', false, NULL, NULL, false, false, false, false, NULL, false, false);
INSERT INTO record_billing_types VALUES (91, 7, 'Einzahlung', 21, 'booksPayment', false, 'UE', false, NULL, NULL, false, false, false, false, NULL, false, false);


--
-- Data for Name: record_cash_deadlines; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_cash_deadlines VALUES (2, 'Gut', 1, 7);
INSERT INTO record_cash_deadlines VALUES (3, 'Normal', 2, 30);
INSERT INTO record_cash_deadlines VALUES (4, 'Prekaer', 3, 60);
INSERT INTO record_cash_deadlines VALUES (5, 'Schlecht', 4, 9999);


--
-- Name: record_cash_deadlines_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_cash_deadlines_id_seq', 5, true);


--
-- Data for Name: record_deleted_archive; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_delivery_conditions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_delivery_conditions_id_seq', 2, true);


--
-- Name: record_delivery_note_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_delivery_note_types_id_seq', 9, true);


--
-- Data for Name: record_document_obj; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_document_obj_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_document_obj_id_seq', 1, false);


--
-- Data for Name: record_document_version_obj; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_document_version_obj_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_document_version_obj_id_seq', 1, false);


--
-- Data for Name: record_document_versions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_document_versions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_document_versions_id_seq', 1, false);


--
-- Data for Name: record_documents; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_documents_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_documents_id_seq', 1, false);


--
-- Data for Name: record_exports; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: record_export_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_export_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_export_items_id_seq', 1, false);


--
-- Name: record_exports_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_exports_id_seq', 1, false);


--
-- Data for Name: record_info_config_defaults; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_info_config_defaults_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_info_config_defaults_id_seq', 1, false);


--
-- Data for Name: record_info_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_info_configs VALUES (1, 'Technische Änderungen und Irrtümer vorbehalten. Es gelten unsere AGB.', NULL, false);


--
-- Name: record_info_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_info_configs_id_seq', 1, true);


--
-- Data for Name: record_mail_logs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_mail_logs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_mail_logs_id_seq', 1, false);


--
-- Data for Name: record_payment_conditions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_payment_conditions VALUES (1, 'sofort / bar netto', 0, false, 0, 0, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (2, 'bereits bezahlt', 0, false, 0, 0, 0, 0, true, 'de', false);
INSERT INTO record_payment_conditions VALUES (3, 'Ohne Zahlung', 0, false, 0, 0, 0, 0, true, 'de', false);
INSERT INTO record_payment_conditions VALUES (4, 'Vorkasse', 0, false, 0, 0, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (5, 'Vorkasse mit 2 % Skonto', 0, true, 0, 0.02, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (6, 'Vorkasse mit 3 % Skonto', 0, true, 0, 0.03, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (7, '7 Tage netto', 7, false, 0, 0, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (8, '14 Tage netto', 14, false, 0, 0, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (9, '21 Tage netto', 21, false, 0, 0, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (10, '28 Tage netto', 28, false, 0, 0, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (11, '30 Tage netto', 30, false, 0, 0, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (12, '7 Tage 2 % Skonto, 28 Tage netto', 28, true, 7, 0.02, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (13, '10 Tage 2 % Skonto, 30 Tage netto', 30, true, 10, 0.02, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (14, '14 Tage 2 % Skonto, 28 Tage netto', 28, true, 14, 0.02, 0, 0, false, 'de', false);
INSERT INTO record_payment_conditions VALUES (15, 'Spende', 0, false, 0, 0, 0, 0, true, 'de', false, true);


--
-- Data for Name: record_payment_targets; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_payment_targets VALUES (0, '-/-', false);
INSERT INTO record_payment_targets VALUES (1, '14 Tage netto nach Eingang der Rechnung.', true);
INSERT INTO record_payment_targets VALUES (2, '28 Tage netto nach Eingang der Rechnung.', true);
INSERT INTO record_payment_targets VALUES (3, 'bei Auftrag', false);
INSERT INTO record_payment_targets VALUES (4, 'nach Beginn der Implementierung', false);
INSERT INTO record_payment_targets VALUES (5, 'bei Schlussrechnung', false);
INSERT INTO record_payment_targets VALUES (6, 'nach Auftragsbestätigung', false);
INSERT INTO record_payment_targets VALUES (7, 'nach Finanzierungszusage und technischer Klärung', true);
INSERT INTO record_payment_targets VALUES (8, 'nach erfolgter Inbetrieb- und Abnahme des Systems', false);
INSERT INTO record_payment_targets VALUES (9, 'nach Finanzierungszusage', true);
INSERT INTO record_payment_targets VALUES (10, 'bei Installation', true);
INSERT INTO record_payment_targets VALUES (11, 'nach Lieferung', false);
INSERT INTO record_payment_targets VALUES (12, 'nach Inbetriebnahme', true);
INSERT INTO record_payment_targets VALUES (13, 'Zahlung nach Vertragsunterzeichnung', true);


--
-- Data for Name: record_payment_agreement_defaults; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_payment_agreement_defaults VALUES (1, 1, 0.0, 0.0, 1.0, 0, 0, 1, 1, '1970-01-01 00:00:00', 6000, NULL, NULL, false, true);
INSERT INTO record_payment_agreement_defaults VALUES (2, 2, 0.0, 0.0, 1.0, 0, 0, 1, 1, '1970-01-01 00:00:00', 6000, NULL, NULL, false, true);
INSERT INTO record_payment_agreement_defaults VALUES (3, 3, 0.0, 0.0, 1.0, 0, 0, 1, 1, '1970-01-01 00:00:00', 6000, NULL, NULL, false, true);
INSERT INTO record_payment_agreement_defaults VALUES (4, 4, 0.0, 0.0, 1.0, 0, 0, 1, 1, '1970-01-01 00:00:00', 6000, NULL, NULL, false, true);


--
-- Name: record_payment_agreement_defaults_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_payment_agreement_defaults_id_seq', 4, true);


--
-- Data for Name: record_payment_condition_defaults; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_payment_condition_defaults VALUES (1, true, 1, 8);


--
-- Name: record_payment_condition_defaults_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_payment_condition_defaults_id_seq', 1, true);


--
-- Name: record_payment_conditions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_payment_conditions_id_seq', 15, true);


--
-- Data for Name: record_payment_days; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_payment_days VALUES (7, '7');
INSERT INTO record_payment_days VALUES (14, '14');
INSERT INTO record_payment_days VALUES (21, '21');
INSERT INTO record_payment_days VALUES (28, '28');
INSERT INTO record_payment_days VALUES (30, '30');


--
-- Data for Name: record_payment_target_defaults; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_payment_target_defaults VALUES (1, 1, 1, 1);
INSERT INTO record_payment_target_defaults VALUES (2, 2, 1, 1);
INSERT INTO record_payment_target_defaults VALUES (3, 3, 1, 1);
INSERT INTO record_payment_target_defaults VALUES (4, 1, 2, 1);
INSERT INTO record_payment_target_defaults VALUES (5, 2, 2, 1);
INSERT INTO record_payment_target_defaults VALUES (6, 3, 2, 1);
INSERT INTO record_payment_target_defaults VALUES (7, 1, 3, 1);
INSERT INTO record_payment_target_defaults VALUES (8, 2, 3, 1);
INSERT INTO record_payment_target_defaults VALUES (9, 3, 3, 1);
INSERT INTO record_payment_target_defaults VALUES (10, 1, 102, 0);
INSERT INTO record_payment_target_defaults VALUES (11, 2, 102, 0);
INSERT INTO record_payment_target_defaults VALUES (12, 3, 102, 0);


--
-- Name: record_payment_target_defaults_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_payment_target_defaults_id_seq', 12, true);


--
-- Name: record_payment_targets_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_payment_targets_id_seq', 13, true);


--
-- Data for Name: record_payments_other; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_payments_other_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_payments_other_id_seq', 1, false);


--
-- Data for Name: record_print_option_defaults; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_print_option_defaults (type_id, name, tvalue, created_by) values (2, 'printDeliveryDate', 'false', 6000);
INSERT INTO record_print_option_defaults (type_id, name, tvalue, created_by) values (102, 'printDeliveryDate', 'false', 6000);


--
-- Name: record_print_option_defaults_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_print_option_defaults_id_seq', 2, true);


--
-- Data for Name: record_sequences; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_sequences VALUES (1, 100, 1, 'sales_offer_100_id_seq', false, false, true, false, 2018, 4, 20100000);
INSERT INTO record_sequences VALUES (2, 100, 2, 'sales_order_100_id_seq', false, false, true, false, 2018, 4, 20200000);
INSERT INTO record_sequences VALUES (3, 100, 3, 'sales_delivery_note_100_id_seq', false, false, true, false, 2018, 4, 20300000);
--INSERT INTO record_sequences VALUES (4, 100, 4, 'sales_downpayments_100_id_seq', false, false, false, true, 2018, 4, 20400000);
INSERT INTO record_sequences VALUES (4, 100, 4, 'sales_invoice_100_id_seq', false, false, false, true, 2018, 4, 20400000);
INSERT INTO record_sequences VALUES (5, 100, 5, 'sales_invoice_100_id_seq', false, false, false, true, 2018, 4, 20500000);
INSERT INTO record_sequences VALUES (6, 100, 6, 'sales_delivery_note_100_id_seq', false, false, true, false, 2018, 4, 20300000);
INSERT INTO record_sequences VALUES (7, 100, 9, 'sales_credit_note_100_id_seq', false, false, false, true, 2018, 4, 20900000);
INSERT INTO record_sequences VALUES (8, 100, 10, 'sales_cancellation_100_id_seq', false, false, false, true, 2018, 4, 21000000);
INSERT INTO record_sequences VALUES (9, 100, 101, 'purchase_offer_100_id_seq', false, false, true, false, 2018, 4, 40100000);
INSERT INTO record_sequences VALUES (10, 100, 102, 'purchase_order_100_id_seq', false, false, true, false, 2018, 4, 40200000);
INSERT INTO record_sequences VALUES (11, 100, 103, 'purchase_delivery_note_100_id_seq', false, false, true, false, 2018, 4, 40300000);
INSERT INTO record_sequences VALUES (12, 100, 105, 'purchase_invoice_100_id_seq', false, false, true, false, 2018, 4, 40500000);
INSERT INTO record_sequences VALUES (13, 100, 106, 'purchase_delivery_note_100_id_seq', false, false, true, false, 2018, 4, 40300000);
INSERT INTO record_sequences VALUES (14, 100, 107, 'purchase_invoice_100_id_seq', false, false, true, false, 2018, 4, 40500000);


--
-- Name: record_sequences_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_sequences_id_seq', 14, true);


--
-- Data for Name: record_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_status VALUES (0, 'erstellt', false);
INSERT INTO record_status VALUES (1, 'geändert', false);
INSERT INTO record_status VALUES (2, 'angezeigt', false);
INSERT INTO record_status VALUES (3, 'freigegeben', false);
INSERT INTO record_status VALUES (4, 'importiert', false);
INSERT INTO record_status VALUES (5, 'partiell gebucht', false);
INSERT INTO record_status VALUES (6, 'gebucht', false);
INSERT INTO record_status VALUES (10, 'abgeschlossen', false);
INSERT INTO record_status VALUES (99, 'storniert', false);
INSERT INTO record_status VALUES (199, 'Storno-Reset', false);


--
-- Data for Name: record_tax_free_definitions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_tax_free_definitions VALUES (1, 'Auslandsumsatz EU', false);
INSERT INTO record_tax_free_definitions VALUES (2, 'Auslandsumsatz', false);
INSERT INTO record_tax_free_definitions VALUES (3, 'Kleinunternehmer gem. §19 UStG', false);
INSERT INTO record_tax_free_definitions VALUES (4, 'Steuerbefreiung', false);
INSERT INTO record_tax_free_definitions VALUES (5, 'nicht steuerbar gem. § 2 (2) Nr. 2 UStG Organschaft', false);
INSERT INTO record_tax_free_definitions VALUES (6, 'Bauleister gemäß § 13b UStG', false);
INSERT INTO record_tax_free_definitions VALUES (7, 'Foreign turnover EU', false);
INSERT INTO record_tax_free_definitions VALUES (8, 'Foreign turnover', false);
INSERT INTO record_tax_free_definitions VALUES (9, 'tax free according to §4 Nr. 1a UStG in conjunction with §6 UStG', false);


--
-- Name: record_tax_free_definitions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_tax_free_definitions_id_seq', 9, true);


--
-- Data for Name: record_tax_rates; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO record_tax_rates VALUES (1, 'Ermäßigter Steuersatz', 1.07, 1, 1, '2007-01-01', '2020-06-30 23:59:59');
INSERT INTO record_tax_rates VALUES (2, 'Regelsteuersatz', 1.19, 1, 0, '2007-01-01', '2020-06-30 23:59:59');
INSERT INTO record_tax_rates VALUES (3, 'Ermäßigter Steuersatz', 1.05, 1, 1, '2020-07-01', '2020-12-31 23:59:59');
INSERT INTO record_tax_rates VALUES (4, 'Regelsteuersatz', 1.16, 1, 0, '2020-07-01', '2020-12-31 23:59:59');
INSERT INTO record_tax_rates VALUES (5, 'Null-Steuersatz', 1.00, 0, 0, '2023-01-01', NULL);


--
-- Name: record_tax_rates_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_tax_rates_id_seq', 5, true);


--
-- Data for Name: record_tax_reports; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: record_tax_report_account_docs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_tax_report_account_docs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_tax_report_account_docs_id_seq', 1, false);


--
-- Data for Name: record_tax_report_documents; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_tax_report_documents_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_tax_report_documents_id_seq', 1, false);


--
-- Data for Name: record_tax_report_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_tax_report_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_tax_report_items_id_seq', 1, false);


--
-- Name: record_tax_reports_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_tax_reports_id_seq', 1, false);


--
-- Data for Name: record_version_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: record_version_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('record_version_items_id_seq', 1, false);


--
-- Data for Name: request_customer_changes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: request_customer_changes_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_customer_changes_id_seq', 1, false);


--
-- Data for Name: request_fcs_action_closings; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: request_fcs_actions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_fcs_actions VALUES (1, 6008, 'Anfrage temporär stoppen', NULL, NULL, -1, false, false, NULL, false, NULL, false, false, false, true, false, false, false, true, false, false, false, 44, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (2, 6008, 'Stornieren, jetzt neue Anfrage', NULL, NULL, -5, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, 32, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (3, 6008, 'Stornieren, versehentlich angelegt', NULL, NULL, -6, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, 33, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (4, 6008, 'Stornieren, an Wettbewerb verloren', NULL, NULL, -3, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, 15, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (5, 6008, 'Stornieren, Interesse verloren', NULL, NULL, -4, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, 17, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (6, 6008, 'Stornieren, verloren (sonstiges)', NULL, NULL, -2, false, false, NULL, false, NULL, false, false, false, false, false, false, true, false, false, false, false, 14, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (7, 6008, 'Anfrage erfasst', NULL, NULL, 10, false, false, NULL, false, NULL, true, false, false, false, false, false, false, false, false, false, false, 1, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (8, 6008, 'Kontaktaufnahme', NULL, NULL, 20, false, false, NULL, false, NULL, true, false, false, true, false, false, false, false, false, false, false, 100, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (9, 6008, 'Gesprächstermin', NULL, NULL, 30, false, false, NULL, false, NULL, false, false, false, true, false, false, false, false, false, false, false, 101, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (10, 6008, 'Kalkulation/Angebot', NULL, NULL, 55, false, false, NULL, false, NULL, true, false, false, true, false, false, false, false, false, false, false, 102, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (11, 6008, 'Angebot versendet', NULL, NULL, 60, false, false, NULL, false, NULL, false, false, false, true, false, false, false, false, false, false, false, 103, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (12, 6008, 'Anfragetyp zuordnen', NULL, NULL, 45, true, true, 'assign', false, NULL, false, false, false, true, false, false, false, false, true, false, false, 43, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (13, 6008, 'Angebot OK, Auftrag erteilt', NULL, NULL, 100, true, true, 'offerSelection', false, NULL, false, false, false, false, false, false, false, false, false, false, false, 10, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (14, 6008, 'Vertrieb zugeordnet', NULL, NULL, 15, false, false, NULL, false, NULL, true, false, false, false, false, false, false, false, false, false, false, 1, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (15, 6008, 'Informationsmaterial per Mail', NULL, NULL, 22, false, false, NULL, false, NULL, true, false, false, false, false, false, false, false, false, false, false, 1, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (16, 6008, 'Informationsmaterial per Post', NULL, NULL, 24, false, false, NULL, false, NULL, true, false, false, false, false, false, false, false, false, false, false, 1, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (17, 6008, 'Informationsmaterial versendet', NULL, NULL, 25, false, false, NULL, false, NULL, true, false, false, false, false, false, false, false, false, false, false, 1, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (18, 6008, 'Anfragedetails klären', NULL, NULL, 50, false, false, NULL, false, NULL, true, false, false, false, false, false, false, false, false, false, false, 1, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (19, 6008, 'Abschlusstermin vereinbart', NULL, NULL, 85, false, false, NULL, false, NULL, false, false, false, false, false, false, false, false, false, false, false, 1, false, false, 2, false, false);
INSERT INTO request_fcs_actions VALUES (20, 6000, 'Anfrage importiert', NULL, NULL, 1, false, false, NULL, false, NULL, true, false, false, false, false, false, false, false, false, false, false, 1, false, false, 2, false, true);


--
-- Data for Name: request_fcs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: request_fcs_action_closings_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_fcs_action_closings_id_seq', 1, false);


--
-- Data for Name: request_fcs_action_excludes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: request_fcs_action_excludes_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_fcs_action_excludes_id_seq', 1, false);


--
-- Name: request_fcs_actions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_fcs_actions_id_seq', 20, true);


--
-- Data for Name: request_fcs_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_fcs_configs VALUES (1, 1, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO request_fcs_configs VALUES (2, 2, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO request_fcs_configs VALUES (3, 4, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO request_fcs_configs VALUES (4, 5, '1970-01-01 00:00:00', 6000, NULL, NULL);
INSERT INTO request_fcs_configs VALUES (5, 6, '1970-01-01 00:00:00', 6000, NULL, NULL);


--
-- Data for Name: request_fcs_config_action_relations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_fcs_config_action_relations VALUES (1, 7, 0);
INSERT INTO request_fcs_config_action_relations VALUES (1, 14, 1);
INSERT INTO request_fcs_config_action_relations VALUES (1, 8, 2);
INSERT INTO request_fcs_config_action_relations VALUES (1, 18, 3);
INSERT INTO request_fcs_config_action_relations VALUES (1, 15, 4);
INSERT INTO request_fcs_config_action_relations VALUES (1, 16, 5);
INSERT INTO request_fcs_config_action_relations VALUES (1, 17, 6);
INSERT INTO request_fcs_config_action_relations VALUES (1, 9, 7);
INSERT INTO request_fcs_config_action_relations VALUES (1, 12, 8);
INSERT INTO request_fcs_config_action_relations VALUES (1, 1, 9);
INSERT INTO request_fcs_config_action_relations VALUES (1, 3, 10);
INSERT INTO request_fcs_config_action_relations VALUES (1, 4, 11);
INSERT INTO request_fcs_config_action_relations VALUES (1, 5, 12);
INSERT INTO request_fcs_config_action_relations VALUES (1, 6, 13);
INSERT INTO request_fcs_config_action_relations VALUES (2, 7, 0);
INSERT INTO request_fcs_config_action_relations VALUES (2, 14, 1);
INSERT INTO request_fcs_config_action_relations VALUES (2, 18, 2);
INSERT INTO request_fcs_config_action_relations VALUES (2, 15, 3);
INSERT INTO request_fcs_config_action_relations VALUES (2, 16, 4);
INSERT INTO request_fcs_config_action_relations VALUES (2, 17, 5);
INSERT INTO request_fcs_config_action_relations VALUES (2, 9, 6);
INSERT INTO request_fcs_config_action_relations VALUES (2, 10, 7);
INSERT INTO request_fcs_config_action_relations VALUES (2, 11, 8);
INSERT INTO request_fcs_config_action_relations VALUES (2, 12, 9);
INSERT INTO request_fcs_config_action_relations VALUES (2, 19, 10);
INSERT INTO request_fcs_config_action_relations VALUES (2, 13, 11);
INSERT INTO request_fcs_config_action_relations VALUES (2, 1, 12);
INSERT INTO request_fcs_config_action_relations VALUES (2, 3, 13);
INSERT INTO request_fcs_config_action_relations VALUES (2, 4, 14);
INSERT INTO request_fcs_config_action_relations VALUES (2, 5, 15);
INSERT INTO request_fcs_config_action_relations VALUES (2, 6, 16);
INSERT INTO request_fcs_config_action_relations VALUES (3, 7, 0);
INSERT INTO request_fcs_config_action_relations VALUES (3, 14, 1);
INSERT INTO request_fcs_config_action_relations VALUES (3, 18, 2);
INSERT INTO request_fcs_config_action_relations VALUES (3, 15, 3);
INSERT INTO request_fcs_config_action_relations VALUES (3, 16, 4);
INSERT INTO request_fcs_config_action_relations VALUES (3, 17, 5);
INSERT INTO request_fcs_config_action_relations VALUES (3, 9, 6);
INSERT INTO request_fcs_config_action_relations VALUES (3, 12, 7);
INSERT INTO request_fcs_config_action_relations VALUES (3, 10, 8);
INSERT INTO request_fcs_config_action_relations VALUES (3, 11, 9);
INSERT INTO request_fcs_config_action_relations VALUES (3, 19, 10);
INSERT INTO request_fcs_config_action_relations VALUES (3, 13, 11);
INSERT INTO request_fcs_config_action_relations VALUES (3, 1, 12);
INSERT INTO request_fcs_config_action_relations VALUES (3, 3, 13);
INSERT INTO request_fcs_config_action_relations VALUES (3, 4, 14);
INSERT INTO request_fcs_config_action_relations VALUES (3, 5, 15);
INSERT INTO request_fcs_config_action_relations VALUES (3, 6, 16);
INSERT INTO request_fcs_config_action_relations VALUES (4, 7, 0);
INSERT INTO request_fcs_config_action_relations VALUES (4, 14, 1);
INSERT INTO request_fcs_config_action_relations VALUES (4, 18, 2);
INSERT INTO request_fcs_config_action_relations VALUES (4, 15, 3);
INSERT INTO request_fcs_config_action_relations VALUES (4, 16, 4);
INSERT INTO request_fcs_config_action_relations VALUES (4, 17, 5);
INSERT INTO request_fcs_config_action_relations VALUES (4, 9, 6);
INSERT INTO request_fcs_config_action_relations VALUES (4, 12, 7);
INSERT INTO request_fcs_config_action_relations VALUES (4, 10, 8);
INSERT INTO request_fcs_config_action_relations VALUES (4, 11, 9);
INSERT INTO request_fcs_config_action_relations VALUES (4, 19, 10);
INSERT INTO request_fcs_config_action_relations VALUES (4, 13, 11);
INSERT INTO request_fcs_config_action_relations VALUES (4, 1, 12);
INSERT INTO request_fcs_config_action_relations VALUES (4, 3, 13);
INSERT INTO request_fcs_config_action_relations VALUES (4, 4, 14);
INSERT INTO request_fcs_config_action_relations VALUES (4, 5, 15);
INSERT INTO request_fcs_config_action_relations VALUES (4, 6, 16);
INSERT INTO request_fcs_config_action_relations VALUES (5, 7, 0);
INSERT INTO request_fcs_config_action_relations VALUES (5, 14, 1);
INSERT INTO request_fcs_config_action_relations VALUES (5, 18, 2);
INSERT INTO request_fcs_config_action_relations VALUES (5, 15, 3);
INSERT INTO request_fcs_config_action_relations VALUES (5, 16, 4);
INSERT INTO request_fcs_config_action_relations VALUES (5, 17, 5);
INSERT INTO request_fcs_config_action_relations VALUES (5, 9, 6);
INSERT INTO request_fcs_config_action_relations VALUES (5, 12, 7);
INSERT INTO request_fcs_config_action_relations VALUES (5, 10, 8);
INSERT INTO request_fcs_config_action_relations VALUES (5, 11, 9);
INSERT INTO request_fcs_config_action_relations VALUES (5, 19, 10);
INSERT INTO request_fcs_config_action_relations VALUES (5, 13, 11);
INSERT INTO request_fcs_config_action_relations VALUES (5, 1, 12);
INSERT INTO request_fcs_config_action_relations VALUES (5, 3, 13);
INSERT INTO request_fcs_config_action_relations VALUES (5, 4, 14);
INSERT INTO request_fcs_config_action_relations VALUES (5, 5, 15);
INSERT INTO request_fcs_config_action_relations VALUES (5, 6, 16);


--
-- Name: request_fcs_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_fcs_configs_id_seq', 5, true);


--
-- Data for Name: request_fcs_dependencies; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: request_fcs_dependencies_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_fcs_dependencies_id_seq', 1, false);


--
-- Name: request_fcs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_fcs_id_seq', 1, false);


--
-- Data for Name: request_fcs_wastebasket_defaults; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: request_fcs_wastebasket_defaults_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_fcs_wastebasket_defaults_id_seq', 1, false);


--
-- Data for Name: request_fcs_wastebaskets; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: request_fcs_wastebaskets_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_fcs_wastebaskets_id_seq', 1, false);


--
-- Name: request_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_id_seq', 1, false);


--
-- Name: request_origin_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_origin_types_id_seq', 8, true);


--
-- Data for Name: request_reset_warning_tasks; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: request_reset_warning_tasks_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_reset_warning_tasks_id_seq', 1, false);


--
-- Data for Name: request_type_contexts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_type_contexts VALUES (1, 'sales', false, false);
INSERT INTO request_type_contexts VALUES (2, 'project', false, false);
INSERT INTO request_type_contexts VALUES (3, 'purchasing', false, true);
INSERT INTO request_type_contexts VALUES (4, 'purchasing', true, true);
INSERT INTO request_type_contexts VALUES (5, 'sales', true, false);
INSERT INTO request_type_contexts VALUES (6, 'generic', true, false);


--
-- Name: request_type_contexts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_type_contexts_id_seq', 6, true);


--
-- Data for Name: request_type_selection_relations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_type_selection_relations VALUES (1, 2, 0);
INSERT INTO request_type_selection_relations VALUES (1, 3, 1);
INSERT INTO request_type_selection_relations VALUES (1, 4, 2);
INSERT INTO request_type_selection_relations VALUES (2, 2, 0);
INSERT INTO request_type_selection_relations VALUES (4, 3, 0);


--
-- Data for Name: request_type_selections; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO request_type_selections VALUES (1, 'salesMonitoringProjects');
INSERT INTO request_type_selections VALUES (2, 'salesRelationCreator');
INSERT INTO request_type_selections VALUES (3, 'requestCreator');
INSERT INTO request_type_selections VALUES (4, 'salesByPurchase');


--
-- Name: request_type_selections_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_type_selections_id_seq', 4, true);


--
-- Name: request_workflows_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('request_workflows_id_seq', 200, true);


--
-- Name: sales_cancellation_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_cancellation_100_id_seq', 21000001, false);


--
-- Data for Name: sales_cancellation_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_cancellation_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_cancellation_infos_id_seq', 1, false);


--
-- Data for Name: sales_cancellation_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_cancellation_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_cancellation_items_id_seq', 1, false);


--
-- Data for Name: sales_cancellations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_credit_note_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_credit_note_100_id_seq', 20900001, false);


--
-- Data for Name: sales_credit_note_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_credit_note_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_credit_note_infos_id_seq', 1, false);


--
-- Data for Name: sales_credit_note_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_credit_note_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_credit_note_items_id_seq', 1, false);


--
-- Data for Name: sales_credit_note_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO sales_credit_note_types VALUES (1, 'Gutschrift', 'GU', 0, false);
INSERT INTO sales_credit_note_types VALUES (2, 'Forderungsausfall', 'FA', 1, false);


--
-- Name: sales_credit_note_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_credit_note_types_id_seq', 2, true);


--
-- Data for Name: sales_credit_notes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_delivery_note_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_delivery_note_100_id_seq', 20300001, false);


--
-- Data for Name: sales_delivery_note_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_delivery_note_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_delivery_note_infos_id_seq', 1, false);


--
-- Data for Name: sales_delivery_note_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_delivery_note_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_delivery_note_items_id_seq', 1, false);


--
-- Data for Name: sales_delivery_note_serials; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_delivery_note_serials_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_delivery_note_serials_id_seq', 1, false);


--
-- Data for Name: sales_delivery_notes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_downpayments_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_downpayments_100_id_seq', 20400001, false);


--
-- Name: sales_invoice_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_invoice_100_id_seq', 20500001, false);


--
-- Data for Name: sales_invoice_corrections; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sales_invoice_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_invoice_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_invoice_infos_id_seq', 1, false);


--
-- Data for Name: sales_invoice_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_invoice_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_invoice_items_id_seq', 1, false);


--
-- Data for Name: sales_monitoring_cache_tasks; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_monitoring_cache_tasks_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_monitoring_cache_tasks_id_seq', 1, false);


--
-- Name: sales_offer_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_offer_100_id_seq', 20100001, false);


--
-- Data for Name: sales_offer_calculations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_offer_calculations_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_offer_calculations_id_seq', 1, false);


--
-- Data for Name: sales_offers; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sales_offer_discounts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_offer_discounts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_offer_discounts_id_seq', 1, false);


--
-- Data for Name: sales_offer_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_offer_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_offer_infos_id_seq', 1, false);


--
-- Data for Name: sales_offer_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_offer_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_offer_items_id_seq', 1, false);


--
-- Data for Name: sales_offer_option_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_offer_option_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_offer_option_items_id_seq', 1, false);


--
-- Data for Name: sales_offer_payment_agreements; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_offer_payment_agreements_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_offer_payment_agreements_id_seq', 1, false);


--
-- Name: sales_order_100_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_100_id_seq', 20200001, false);


--
-- Data for Name: sales_order_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO sales_order_types VALUES (1, 'Verkauf', 'VK', 0, NULL, false);
INSERT INTO sales_order_types VALUES (2, 'Service', 'SE', 1, NULL, false);
INSERT INTO sales_order_types VALUES (3, 'Garantie', 'GA', 2, NULL, false);
INSERT INTO sales_order_types VALUES (4, 'Installation', 'IN', 3, NULL, true);


--
-- Data for Name: sales_orders; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sales_order_delivery_date_history; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_delivery_date_history_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_delivery_date_history_id_seq', 1, false);


--
-- Data for Name: sales_order_discounts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_discounts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_discounts_id_seq', 1, false);


--
-- Data for Name: sales_order_downpayment_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_downpayment_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_downpayment_infos_id_seq', 1, false);


--
-- Data for Name: sales_order_downpayment_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_downpayment_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_downpayment_items_id_seq', 1, false);


--
-- Data for Name: sales_order_downpayments; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sales_order_infos; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_infos_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_infos_id_seq', 1, false);


--
-- Data for Name: sales_order_item_history; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_item_history_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_item_history_id_seq', 1, false);


--
-- Data for Name: sales_order_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_items_id_seq', 1, false);


--
-- Data for Name: sales_order_option_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_option_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_option_items_id_seq', 1, false);


--
-- Data for Name: sales_order_payment_agreements; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_payment_agreements_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_payment_agreements_id_seq', 1, false);


--
-- Name: sales_order_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_types_id_seq', 4, true);


--
-- Data for Name: sales_order_volume_export_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_volume_export_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_volume_export_configs_id_seq', 1, false);


--
-- Data for Name: sales_order_volume_exports; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sales_order_volume_export_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sales_order_volume_export_deliveries; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_order_volume_export_deliveries_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_volume_export_deliveries_id_seq', 1, false);


--
-- Name: sales_order_volume_export_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_volume_export_items_id_seq', 1, false);


--
-- Name: sales_order_volume_exports_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_order_volume_exports_id_seq', 1, false);


--
-- Name: sales_orders_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_orders_id_seq', 1, false);


--
-- Data for Name: sales_payments; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_payments_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_payments_id_seq', 1, false);


--
-- Data for Name: sales_region_config; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO sales_region_config VALUES (1, 6000, 6000, true);


--
-- Name: sales_region_config_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_region_config_id_seq', 1, true);


--
-- Data for Name: sales_regions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO sales_regions VALUES (1, 'Deutschland', 6000, NULL, 6000, false);


--
-- Data for Name: sales_region_sales_history; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_region_sales_history_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_region_sales_history_id_seq', 1, false);


--
-- Data for Name: sales_region_zipcodes; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_region_zipcodes_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_region_zipcodes_id_seq', 1, false);


--
-- Name: sales_regions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_regions_id_seq', 1, true);


--
-- Data for Name: sales_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO sales_status VALUES (5, 'Auftrag', false);
INSERT INTO sales_status VALUES (15, 'Auftragsbestätigung', false);
INSERT INTO sales_status VALUES (30, 'Anzahlungsrechnung gestellt', false);
INSERT INTO sales_status VALUES (60, 'Lieferrechnung gestellt', false);
INSERT INTO sales_status VALUES (90, 'Lieferung abgeschlossen', false);
INSERT INTO sales_status VALUES (95, 'Schlussrechnung gestellt', false);
INSERT INTO sales_status VALUES (100, 'Abgeschlossen', false);


--
-- Name: sales_status_closed_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_status_closed_id_seq', 3, true);


--
-- Name: sales_type_10_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_type_10_id_seq', 900001, false);


--
-- Name: sales_type_11_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_type_11_id_seq', 600001, false);


--
-- Name: sales_type_15_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_type_15_id_seq', 500001, false);


--
-- Name: sales_type_1_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_type_1_id_seq', 700001, false);


--
-- Name: sales_type_5_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_type_5_id_seq', 800001, false);


--
-- Data for Name: sales_volume_report_tasks; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sales_volume_report_tasks_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sales_volume_report_tasks_id_seq', 1, false);


--
-- Data for Name: sfa_phone_book_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sfa_phone_book_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sfa_phone_book_items_id_seq', 1, false);


--
-- Data for Name: sfa_subscriber; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sfa_subscriber_call_sync_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sfa_subscriber_call_sync_status_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sfa_subscriber_call_sync_status_id_seq', 1, false);


--
-- Name: sfa_subscriber_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sfa_subscriber_id_seq', 1, false);


--
-- Data for Name: telephone_system_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO telephone_system_types VALUES (1, 'Standard Setup', 'http://api.rz.osserp/sfa-live-service/');


--
-- Data for Name: telephone_systems; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: telephone_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sfa_telephone_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: sfa_subscriber_to_telephone_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sfa_subscriber_to_telephone_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sfa_subscriber_to_telephone_groups_id_seq', 1, false);


--
-- Data for Name: sfa_telephone_actions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: sfa_telephone_actions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sfa_telephone_actions_id_seq', 1, false);


--
-- Name: sfa_telephone_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('sfa_telephone_groups_id_seq', 1, false);


--
-- Data for Name: stocktaking_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO stocktaking_status VALUES (0, 'angelegt');
INSERT INTO stocktaking_status VALUES (2, 'Zählliste gedruckt');
INSERT INTO stocktaking_status VALUES (5, 'Daten eingegeben');
INSERT INTO stocktaking_status VALUES (10, 'Abgeschlossen');


--
-- Data for Name: stocktaking_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO stocktaking_types VALUES (0, 'Undefiniert', false);
INSERT INTO stocktaking_types VALUES (1, 'Jahresinventur', false);
INSERT INTO stocktaking_types VALUES (2, 'Zwischeninventur', false);


--
-- Data for Name: stocktakings; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: stocktaking_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: stocktaking_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('stocktaking_items_id_seq', 1, false);


--
-- Name: stocktakings_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('stocktakings_id_seq', 1, false);


--
-- Data for Name: supplier_payment_agreements; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO supplier_payment_agreements VALUES (1, 10000, NULL, NULL, 1, 7, NULL, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, false, NULL, 8, true);


--
-- Name: supplier_payment_agreements_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('supplier_payment_agreements_id_seq', 1, true);


--
-- Data for Name: supplier_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO supplier_types VALUES (1, 'Gesellschafter', 'Kapitalgeber', true, false, true, true, NULL, false, false, false, false, NULL, false, false, NULL, false, false, false);
INSERT INTO supplier_types VALUES (2, 'Bankinstitut', 'Konten, Geldverkehr Mandant', false, false, true, true, NULL, true, false, false, false, NULL, false, false, NULL, false, false, false);
INSERT INTO supplier_types VALUES (3, 'Betriebsbedarf', 'Verwaltungskosten, Bürobedarf', false, false, true, false, NULL, false, false, false, false, NULL, false, false, NULL, false, false, false);
INSERT INTO supplier_types VALUES (4, 'Steueramt', 'Zahlung von Steuern und Abgaben', false, false, true, true, NULL, false, false, true, false, NULL, false, false, NULL, false, false, false);
INSERT INTO supplier_types VALUES (5, 'Abgaben', 'Abgaben, Beiträge und Gebühren', false, false, true, true, NULL, false, true, false, false, NULL, false, false, NULL, false, false, false);
INSERT INTO supplier_types VALUES (6, 'Lieferant', 'Einkauf von Waren und Dienstleistungen', false, true, false, false, NULL, false, false, false, false, NULL, false, false, NULL, false, false, false);
INSERT INTO supplier_types VALUES (7, 'Personal', 'Zahlungen an Mitarbeiter', true, false, true, true, NULL, false, false, false, false, NULL, false, false, NULL, true, true, false);
INSERT INTO supplier_types VALUES (8, 'Beteiligung', 'Beteiligung, Investment', true, false, true, true, NULL, false, false, false, true, NULL, false, false, NULL, true, false, false);
INSERT INTO supplier_types VALUES (9, 'Transport', 'Paketdienste, Spedition', false, false, true, true, NULL, false, true, false, false, NULL, false, false, NULL, false, false, true);


--
-- Name: supplier_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('supplier_types_id_seq', 9, true);


--
-- Data for Name: suppliers_product_groups_relation; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: suppliers_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('suppliers_id_seq', 10001, false);


--
-- Data for Name: system_cache_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_cache_status VALUES (1, 'com.osserp.core.planning.ProductPlanningCache', false, '1970-01-01 00:00:00');


--
-- Name: system_cache_status_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_cache_status_id_seq', 1, true);


--
-- Name: system_companies_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_companies_id_seq', 200, false);


--
-- Name: system_company_bank_accounts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_company_bank_accounts_id_seq', 2, true);


--
-- Data for Name: system_company_branch_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: system_company_branch_selections; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: system_company_branch_selection_relations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_company_branch_selections_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_company_branch_selections_id_seq', 1, false);


--
-- Name: system_company_branchs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_company_branchs_id_seq', 2, true);


--
-- Data for Name: system_company_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_company_legalforms_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_company_legalforms_id_seq', 12, true);


--
-- Data for Name: system_company_managing_directors; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_company_managing_directors VALUES (1, 100, 'Chef Le Grande', '1970-01-01 00:00:00', 6000, NULL, NULL);


--
-- Name: system_company_managing_directors_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_company_managing_directors_id_seq', 1, true);


--
-- Data for Name: system_currencies; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_currencies VALUES (1, 'EUR', 'Euro', false);
INSERT INTO system_currencies VALUES (2, 'USD', 'Dollar', false);
INSERT INTO system_currencies VALUES (3, 'GBP', 'Pound', true);


--
-- Data for Name: system_help_links; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_help_links VALUES (1, '/WEB-INF/app/login.jsp', NULL, NULL, 0);


--
-- Name: system_help_links_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_help_links_id_seq', 1, true);


--
-- Data for Name: system_help_pages; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_help_pages_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_help_pages_id_seq', 1, false);


--
-- Data for Name: system_i18n; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_i18n VALUES (1, 1, 'productionProductSelectionProfile', 'Produktion');
INSERT INTO system_i18n VALUES (2, 1, 'productionProductSelectionProfileDescription', 'Standard-Produktprofil (Produkte der Produktion)');
INSERT INTO system_i18n VALUES (3, 1, 'itProductSelectionProfile', 'IT');
INSERT INTO system_i18n VALUES (4, 1, 'itProductSelectionProfileDescription', 'Produktprofil der internen IT');


--
-- Data for Name: system_i18n_config_locales; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_i18n_config_locales VALUES (1, 1, 'de');
INSERT INTO system_i18n_config_locales VALUES (2, 1, 'de_DE');
INSERT INTO system_i18n_config_locales VALUES (3, 1, 'de_AT');
INSERT INTO system_i18n_config_locales VALUES (4, 1, 'de_CH');


--
-- Name: system_i18n_config_locales_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_i18n_config_locales_id_seq', 4, true);


--
-- Name: system_i18n_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_i18n_id_seq', 4, true);


--
-- Data for Name: system_key_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_key_types VALUES (1, 'documentTypes', '/admin/dms/documentTypeConfig/forward', NULL, true, false, true, true, NULL);
INSERT INTO system_key_types VALUES (2, 'nocClientAccount', '/admin/system/nocClient/forward', NULL, false, true, true, true, NULL);


--
-- Name: system_key_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_key_types_id_seq', 2, true);


--
-- Data for Name: system_keys; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_keys_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_keys_id_seq', 1, false);


--
-- Name: system_ldap_gid_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_ldap_gid_id_seq', 1, false);


--
-- Name: system_ldap_uid_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_ldap_uid_id_seq', 1, false);


--
-- Data for Name: system_noc_clients; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: system_mail_domains; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: mail_accounts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: mail_account_addresses; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: mail_accounts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('mail_accounts_id_seq', 1, false);


--
-- Name: system_mail_domains_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_mail_domains_id_seq', 1, false);


--
-- Data for Name: system_mail_templates; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_mail_templates VALUES (1, 'Einkauf - Email Bestellung', 'purchaseOrderEmailTemplate', NULL, 'Unsere Bestellung', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, false, false, false, false);
INSERT INTO system_mail_templates VALUES (2, 'Verkauf - Email Stornobleg', 'salesCancellationEmailTemplate', NULL, 'Storno', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, false, false, false, false);
INSERT INTO system_mail_templates VALUES (3, 'Verkauf - Email Gutschrift', 'salesCreditNoteEmailTemplate', NULL, 'Gutschrift', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, false, false, false, false);
INSERT INTO system_mail_templates VALUES (4, 'Verkauf - Email Abschlagsrechnung', 'salesDownpaymentEmailTemplate', NULL, 'Rechnung', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, false, false, false, false);
INSERT INTO system_mail_templates VALUES (5, 'Verkauf - Email Rechnung', 'salesInvoiceEmailTemplate', NULL, 'Rechnung', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, false, false, false, false);
INSERT INTO system_mail_templates VALUES (6, 'Verkauf - Email Angebot', 'salesOfferEmailTemplate', NULL, 'Angebot', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, false, false, false, false);
INSERT INTO system_mail_templates VALUES (7, 'Verkauf - Email Auftragsbestätigung', 'salesOrderEmailTemplate', NULL, 'Auftragsbestätigung', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, false, false, false, false);
INSERT INTO system_mail_templates VALUES (8, 'Angebot - Zeitüberschreitung', 'RequestAlertEmailTemplate', NULL, 'Zeitüberschreitung Angebotserstellung', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, true, false, false, true);
INSERT INTO system_mail_templates VALUES (9, 'Anmeldung - Bestätigung', 'CustomerRegistrationEmailTemplate', NULL, 'Ihre Anmeldung', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, true, false, false, true);
INSERT INTO system_mail_templates VALUES (10, 'Benachrichtigung neue Anfrage', 'RequestChangedEmailTemplate', NULL, 'Neue Anfrage erhalten', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, true, false, false, true);
INSERT INTO system_mail_templates VALUES (11, 'Artikel in Kalkulation unvollständig', 'InvalidProductConfigEmailTemplate', NULL, 'Unvollständige Artikeldaten', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, false, false, true, false, false, true);
INSERT INTO system_mail_templates VALUES (12, 'Notizen zu Kontakten', 'contactNoteEmailTemplate', NULL, 'Information zu', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, true, true, false, false, false, false);
INSERT INTO system_mail_templates VALUES (13, 'Notizen zu Bestellungen', 'purchaseOrderNoteEmailTemplate', NULL, 'Information zu', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, true, true, false, false, false, false);
INSERT INTO system_mail_templates VALUES (14, 'Notizen zu Artikel', 'productNoteEmailTemplate', NULL, 'Information zu', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, true, true, true, false, false, false, false);
INSERT INTO system_mail_templates VALUES (15, 'Notizen zu Auftrag/Projekt', 'salesNoteEmailTemplate', NULL, 'Information zu', NULL, NULL, NULL, true, false, false, true, true, NULL, true, NULL, false, true, false, false, true, false, false, false, true, true, false, false, false, false);
UPDATE system_mail_templates SET parameters = '{ "recordId":"[Belegnummer]", "recordDate":"[Belegdatum]", "contactId":"[Kundennummer]" }' WHERE id < 8;


--
-- Name: system_mail_templates_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_mail_templates_id_seq', 15, true);


--
-- Data for Name: system_menus; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_menus VALUES (1, NULL, 'home', true, true, true);
INSERT INTO system_menus VALUES (2, NULL, 'admin', false, false, false);
INSERT INTO system_menus VALUES (3, NULL, 'setup', false, false, false);


--
-- Data for Name: system_menu_headers; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_menu_headers VALUES (1, 1, 0, 'contacts', 'menu_contacts_deny', false, false, false, true);
INSERT INTO system_menu_headers VALUES (2, 1, 1, 'requests', NULL, false, true, false, true);
INSERT INTO system_menu_headers VALUES (3, 1, 2, 'orders', NULL, false, true, false, true);
INSERT INTO system_menu_headers VALUES (4, 1, 3, 'purchasing', 'purchasing,accounting,executive,purchase_delivery_create', false, true, false, true);
INSERT INTO system_menu_headers VALUES (5, 1, 4, 'products', 'menu_products_deny', false, true, false, true);
INSERT INTO system_menu_headers VALUES (6, 1, 5, 'account', NULL, false, false, false, true);
INSERT INTO system_menu_headers VALUES (7, 1, 6, 'common', NULL, false, false, false, true);
INSERT INTO system_menu_headers VALUES (21, 2, 0, 'accounts', 'ldap_admin,ldap_password,role_configs_admin,organisation_admin,hrm,client_create,executive_saas,runtime_config', false, true, false, true);
INSERT INTO system_menu_headers VALUES (22, 2, 1, 'products', 'product_create,product_edit,product_selections_admin,executive,organisation_admin,manufacturer_edit', false, true, false, true);
INSERT INTO system_menu_headers VALUES (23, 2, 2, 'requests', 'campaign_config,sales_region_config,fcs_admin,organisation_admin,runtime_config', false, true, false, true);
INSERT INTO system_menu_headers VALUES (24, 2, 3, 'todos', 'close_todos,organisation_admin,executive', false, true, false, true);
INSERT INTO system_menu_headers VALUES (25, 2, 4, 'orders', 'organisation_admin,runtime_config,calc_config,fcs_admin', false, true, false, true);
INSERT INTO system_menu_headers VALUES (26, 2, 8, 'system', 'query_config,query_config_display,option_edit,campaign_config,calc_config,payment_condition_config,hrm,synchronize,organisation_admin,runtime_config', false, true, false, true);
INSERT INTO system_menu_headers VALUES (27, 2, 9, 'phone', 'telephone_system_admin', true, true, false, true);
INSERT INTO system_menu_headers VALUES (28, 2, 6, 'documents', 'payment_condition_config,executive', false, true, false, true);
INSERT INTO system_menu_headers VALUES (29, 2, 7, 'templates', 'executive,template_admin_email,template_admin,organisation_admin,runtime_config', false, true, false, true);
INSERT INTO system_menu_headers VALUES (30, 2, 5, 'records', 'organisation_admin,payment_condition_config,accounting', false, true, false, true);
INSERT INTO system_menu_headers VALUES (31, 3, 0, 'systemCompany', 'runtime_config,executive_saas', false, true, false, true);
INSERT INTO system_menu_headers VALUES (32, 3, 1, 'system', 'runtime_config,executive_saas', false, true, false, true);


--
-- Data for Name: system_menu_links; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_menu_links VALUES (1, 1, 0, 'search', 'contact_search_deny', false, '/contacts/contactSearch/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (2, 1, 1, 'create', 'contact_create,contact_assign,contact_delete,accounting,customer_service,sales,hrm,employee_assign,employee_activation,technics,executive,executive_branch,executive_customer_service,executive,accounting_accountant_deny', false, '/contacts/contactCreator/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (3, 1, 2, 'actions', 'contact_create,contact_assign,contact_delete,accounting,customer_service,sales,hrm,employee_assign,employee_activation,technics,executive,executive_branch,executive_customer_service,executive_logistic,executiv_sales,executive_tecstaff,accounting_accountant_deny', false, '/mailingLists.do?method=forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (4, 2, 0, 'search', NULL, false, '/requests/requestSearch/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (5, 2, 2, 'queries', 'accounting,customer_service,sales,sales_change,sales_agent_config,technics,campaign_config,requests_queries_by_branch,requests_cancel,purchasing,organisation_admin,executive,executive_branch,executive_customer_service,executive_sales,executive_tecstaff', false, '/requests/requestQueryIndex/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (6, 3, 0, 'search', NULL, false, '/sales/salesSearch/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (7, 3, 1, 'queries', 'accounting,customer_service,sales,sales_change,sales_agent_config,technics,campaign_config,requests_queries_by_branch,requests_cancel,purchasing,product_create,organisation_admin,executive,executive_branch,executive_customer_service,executive_sales,executive_tecstaff', false, '/reporting/projectQueries/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (8, 3, 2, 'planning', 'accounting,customer_service,sales,sales_change,sales_agent_config,technics,campaign_config,requests_queries_by_branch,requests_cancel,purchasing,product_create,organisation_admin,executive,executive_branch,executive_customer_service,executive_sales,executive_tecstaff', true, '/salesPlanning.do?method=forward&exit=index&unreleased=false', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (9, 4, 1, 'purchaseOrders', 'purchasing,accounting,executive,purchase_delivery_create', false, '/purchaseOrderQueries.do?method=forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (10, 4, 3, 'openPurchaseInvoiceLabel', 'purchasing,accounting,executive', false, '/purchasing/purchaseInvoiceReceipt/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (11, 4, 4, 'purchaseInvoiceArchiveLabel', 'purchasing,accounting,accounting_accountant,purchase_delivery_create,executive', false, '/purchasing/purchaseInvoiceArchive/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (12, 5, 0, 'search', NULL, false, '/productSearch.do?method=forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (13, 5, 1, 'create', 'product_create,executive', false, '/products/productCreator/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (14, 6, 0, 'appointments', NULL, false, '/users/userCalendar/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (16, 7, 0, 'accountingLabel', 'accounting,accounting_accountant,accounting_documents,accounting_bank_documents,customer_service,requests_queries_by_branch,requests_cancel,executive,executive_branch,executive_customer_service,executive_sales,executive_tecstaff,organisation_admin', false, '/accounting/accountingIndex/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (17, 7, 2, 'phonelist', 'phone_list_deny,accounting_accountant_deny', false, '/dms/pdfcreator/print?name=phone_list', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (18, 6, 1, 'password', NULL, false, '/login/loginChange', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (19, 30, 2, 'paymentTargets', 'organisation_admin,payment_condition_config,accounting', false, '/admin/records/paymentTargetConfig/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (20, 22, 0, 'groups', 'product_create,product_edit,product_selections_admin,executive,organisation_admin', false, '/admin/products/classificationConfigs/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (21, 22, 2, 'manufacturer', 'manufacturer_edit,executive', false, '/admin/products/manufacturerConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (22, 23, 1, 'fcsActionsLabel', 'organisation_admin,fcs_admin,runtime_config', false, '/admin/requests/requestFcsConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (24, 24, 0, 'ongoingLabel', 'close_todos,organisation_admin,executive', false, '/events/employeeEvents/forward?exit=/admin', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (25, 24, 1, 'configuration', 'event_config,organisation_admin', false, '/admin/events/eventConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (26, 24, 2, 'pools', NULL, false, '/admin/events/eventPools/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (27, 25, 1, 'fcsActionsLabel', 'organisation_admin,fcs_admin,runtime_config', false, '/admin/sales/salesFcsConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (31, 26, 0, 'queries', 'query_config,query_config_display', true, '/admin/system/queryConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (32, 26, 1, 'selectionLists', 'option_edit,campaign_config,calc_config,organisation_admin,payment_condition_config', false, '/admin/system/optionsAdmin/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (33, 30, 1, 'conditions', 'organisation_admin,payment_condition_config,accounting', false, '/admin/records/recordInfoConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (34, 28, 1, 'letters', 'letter_admin,executive', false, '/admin/letters/letterTemplateConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (36, 6, 2, 'settings', NULL, false, '/users/userSettings/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (38, 5, 2, 'planning', 'sales,technics,purchasing,executive,product_planning', true, '/productStockReport.do?method=forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (40, 4, 0, 'searchPurchaseOrdersShort', 'purchasing,accounting,executive,purchase_delivery_create', false, '/purchasing/purchaseOrderSearch/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (41, 21, 0, 'employeeGroupsMenuLabel', 'organisation_admin,role_configs_admin,hrm,client_create', false, '/admin/employees/employeeGroupConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (43, 4, 2, 'deliveries', 'purchasing,accounting,executive,purchase_delivery_create', false, '/purchaseReceipt.do?method=forward&loadAll=true', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (44, 26, 2, 'timeRecording', 'organisation_admin,time_recording_admin,client_create', false, '/timeRecordingConfig.do?method=forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (45, 25, 0, 'orderTypes', 'organisation_admin,runtime_config', false, '/admin/projects/businessTypeConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (46, 22, 1, 'filter', 'product_selections_admin,organisation_admin', false, '/selections/productSelectionConfigsSelection/forward?context=2', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (47, 26, 3, 'synchronization', 'synchronize,executive', true, '/synchronize.do?method=forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (48, 25, 2, 'calculation', 'calc_config,organisation_admin', true, '/admin/calculations/calculationConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (49, 27, 0, 'phonelist', 'telephone_system_admin', true, '/telephones/telephone/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (50, 27, 2, 'telephoneConfiguration', 'telephone_system_admin', true, '/telephones/telephoneConfigurationSearch/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (51, 26, 4, 'adminServices', 'runtime_config', false, '/admin/system/runtimeConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (54, 27, 1, 'telephoneGroups', 'telephone_system_admin', true, '/telephones/telephoneGroup/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (56, 21, 1, 'ldapAccounts', 'ldap_admin,ldap_password', true, '/directory/users/forward?exit=/admin', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (57, 21, 3, 'ldapMailboxAccounts', 'ldap_admin,ldap_mailbox', true, '/directory/mailbox/forward?exit=/admin', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (60, 25, 3, 'calculationTemplatesShort', 'calc_config,organisation_admin', true, '/admin/calculations/calculationTemplates/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (61, 23, 0, 'campaigns', 'it,organisation_admin,campaign_config', false, '/admin/crm/campaignConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (62, 23, 2, 'salesRegions', 'it,organisation_admin,sales_region_config', false, '/admin/sales/salesRegionConfig/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (63, 21, 4, 'network', 'it,organisation_admin,executive', true, '/admin/system/nocClient/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (64, 21, 2, 'ldapMailDomains', 'ldap_admin,ldap_mailbox', true, '/directory/mailDomain/forward?exit=/admin', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (65, 6, 4, 'sendEmail', 'menu_admin_deny,accounting_accountant_deny', true, '/mail/mailEditor/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (66, 29, 0, 'printTemplates', 'template_admin,it,organisation_admin,runtime_config', false, '/admin/dms/stylesheetDms/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (67, 23, 3, 'documents', 'template_admin,it,organisation_admin', true, '/admin/dms/templateDms/forward?context=request', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (68, 25, 5, 'documents', 'template_admin,it,organisation_admin', true, '/admin/dms/templateDms/forward?context=sales', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (69, 29, 3, 'recordTemplates', 'template_admin,it,organisation_admin', true, '/admin/dms/templateDms/forward?context=records', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (70, 29, 2, 'customFiles', 'template_admin,it,organisation_admin', true, '/admin/dms/templateDms/forward?context=files', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (71, 28, 0, 'settings', 'document_admin,organisation_admin,runtime_config,executive_saas', false, '/admin/dms/documentTypeConfig/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (72, 26, 5, 'security', 'runtime_config,executive_saas', false, '/admin/system/credentialConfig/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (73, 30, 0, 'settings', 'organisation_admin,payment_condition_config,accounting', false, '/admin/records/recordTypeConfig/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (74, 26, 6, 'configuration', 'runtime_config,executive_saas,client_admin', false, '/admin/system/propertyEditor/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (75, 22, 3, 'importActionLabel', 'runtime_config,executive_saas', false, '/admin/products/productImport/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (76, 25, 4, 'costs', 'product_selections_admin,calc_config,organisation_admin', false, '/selections/productSelectionConfigsSelection/forward?context=3', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (77, 29, 1, 'emailTemplates', 'template_admin_email,template_admin,executive,it,organisation_admin', false, '/admin/mail/mailTemplateConfig/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (78, 26, 7, 'labels', 'client_admin,i18n_admin,organisation_admin,runtime_config,executive_saas', false, '/admin/system/textResourceEditor/forward', true, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (79, 4, 5, 'purchaseInvoiceSearchLabel', 'purchasing,accounting,accounting_accountant,purchase_delivery_create,executive', false, '/purchasing/purchaseInvoiceSearch/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (80, 6, 3, 'inboxLabel', NULL, true, '/users/userFetchmailInbox/forward', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (81, 2, 1, 'create', 'accounting,customer_service,sales,sales_change,requests_cancel,purchasing,organisation_admin,executive,executive_branch,executive_customer_service,executive_sales', false, '/contacts/contactSearch/forward?selectionExit=ignore&selectionTarget=/requests/requestCreator/forward&context=customer', false, false, NULL, false, true);
INSERT INTO system_menu_links VALUES (82, 7, 1, 'emailImport', 'fetchmail_admin', false, '/mail/fetchmailInbox/forward', true, false, NULL, false, true);


--
-- Name: system_menu_links_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_menu_links_id_seq', 82, true);


--
-- Name: system_menu_headers_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_menu_headers_id_seq', 32, true);


--
-- Name: system_menus_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_menus_id_seq', 3, true);


--
-- Name: system_noc_clients_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_noc_clients_id_seq', 1, false);


--
-- Data for Name: system_permission_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_permission_configs VALUES (1, 'confirmDiscountPermissions');
INSERT INTO system_permission_configs VALUES (2, 'salesCancellationPermissions');
INSERT INTO system_permission_configs VALUES (3, 'calculationDiscountPermissions');
INSERT INTO system_permission_configs VALUES (4, 'salesMonitoringDisplayAllPermissions');
INSERT INTO system_permission_configs VALUES (5, 'requestCancellationPermissions');
INSERT INTO system_permission_configs VALUES (6, 'overridePartnerPricePermissions');
INSERT INTO system_permission_configs VALUES (7, 'mailDomainAdministration');
INSERT INTO system_permission_configs VALUES (8, 'contactDelete');
INSERT INTO system_permission_configs VALUES (9, 'customerExport');


--
-- Data for Name: user_permissions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO user_permissions VALUES ('accounting', 'Buchhaltung allgemein', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('accounting_accountant', 'Buchhaltung extern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('accounting_accountant_deny', 'Buchhaltung extern gesperrt', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('accounting_bank_display', 'Bankdaten anzeigen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('accounting_bank_documents', 'Bankdokumente bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('accounting_documents', 'Belege und Buchungen bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('accounting_reports', 'Buchhaltung Auswertungen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('booking_part_time', 'Buchung Arbeitsfrei Teilzeit', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('booking_school', 'Buchung Berufsschule', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('booking_study', 'Buchung Studium Plus', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('booking_study_master', 'Buchung Master Studium Plus', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('branch_create', 'Niederlassung anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('branch_edit', 'Niederlassung bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('calc_config', 'Stücklisten konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('campaign_config', 'Kampagnenkonfiguration', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('cancel_fcs', 'FCS stornieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('close_todos', 'Aufgaben als erledigt markieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('contact_assign', 'Kontaktart zuweisen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('contact_create', 'Kontakte anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('contact_delete', 'Kontakt löschen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('contact_export', 'Kontaktdaten exportieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('contact_import', 'Kontakte importieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('contact_person_delete', 'Ansprechpartner löschen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('contact_search_deny', 'Kontaktsuche ausblenden', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('create_news', 'News einstellen', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('create_offer_copy', 'Angebot kopieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('create_suborder_ignore_branch', 'Niederlassing bei Auswahl Sub-Auftrag ignorieren', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('customer_export', 'Kunden exportieren', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('customer_service', 'Auftragssteuerung', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('delete_contact_documents', 'Kontaktdokumente loeschen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('delete_payments', 'Zahlungen löschen', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('delete_sales_documents', 'Auftragsunterlagen löschen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('delete_sales_pictures', 'Bilder zum Auftrag löschen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('delivery_note_correction_change', 'Lieferscheinkorrektur bearbeiten', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('delivery_note_create', 'Lieferscheine anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('delivery_note_delete', 'Lieferscheine löschen', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('disciplinarian_edit', 'Vorgesetzte bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('disciplinarian_view', 'Vorgesetzte anzeigen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('document_admin', 'DMS Administrieren', false, true, false, NULL, false);
INSERT INTO user_permissions VALUES ('employee_activation', 'Mitarbeiterberechtigungen verändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('employee_assign', 'Mitarbeiter anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('employee_groups_admin', 'Mitarbeitergruppen administrieren', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('employee_internet_config', 'Mitarbeiter Bild & Beschreibung', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('event_config', 'Aufgaben konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('event_config_delete', 'Aufgabenkonfiguration löschen', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('event_pool_config', 'Aufgabenpools konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('executive', 'Geschäftsleitung allgemein', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('executive_accounting', 'Leitung Buchhaltung', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('executive_branch', 'Auftragszugriff gesamte Niederlassung', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('executive_customer_service', 'Leitung Auftragsabwicklung', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('executive_hrm', 'Personalleitung', true, true, false, NULL, false);
INSERT INTO user_permissions VALUES ('executive_logistics', 'Leitung Logistik', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('executive_saas', 'Systemadministration', true, true, false, NULL, false);
INSERT INTO user_permissions VALUES ('executive_sales', 'Vertriebsleitung', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('executive_tecstaff', 'Leitung Technik', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('fcs_admin', 'FCS-Aktionen administrieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('fcs_closing_create', 'FCS-Statustexte anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('feature_testing', 'Neue Funktionen testen', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('fetch_new_project', 'Auftrag (neu) übernehmen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('fetchmail_admin', 'Inbox-Administration', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('global_business_case_access', 'Auftragszugriff für alle Niederlassungen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('guest', 'Gast', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('help_edit', 'Hilfelinks einstellen', false, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('hrm', 'Personalwesen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('ignore_not_existing_serial', 'Seriennummer ignorieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('ignore_price_limit', 'Preise unter Limit fakturieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('internet_contact_form', 'Internet-Kontakte einsehen & bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('invoice_unspecified_discounts', 'Blanko Nachlässe fakturieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('it', 'IT allgemein', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('ldap_admin', 'LDAP-Administration', false, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('ldap_group_create', 'LDAP-Gruppe anlegen', false, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('ldap_group_delete', 'LDAP-Gruppe löschen', false, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('ldap_mail_admin', 'LDAP-Mailaccounts konfigurieren', false, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('ldap_password', 'Passwort-Reset', false, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('letter_admin', 'Anschreiben administrieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('letter_template_delete', 'Anschreiben Vorlage löschen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('logistics_only', 'Logistik (Beschränkung!)', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('mailing_lists_edit', 'Email-Verteiler bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('manager_add', 'Auftragszuständigen zuweisen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('manager_change', 'Auftragszuständigen ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('client_admin','Anwendung konfigurieren', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('client_create', 'Mandant anlegen', false, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('client_edit', 'Mandant bearbeiten', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('manufacturer_edit', 'Hersteller bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('margin_change', 'Marge ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('margin_view', 'Marge einsehen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('menu_admin_deny', 'Menueintrag Admin deaktivieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('menu_products_deny', 'Menueintrag Produkte ausblenden', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('menu_contacts_deny', 'Menueintrag Kontakte ausblenden', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('newsletter_config', 'Newsletter Konfiguration', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('newsletter_sending', 'Newsletter versenden', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('notes_contacts_deny', 'Notizen zu Kontakten ausblenden', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('notes_requests_deny', 'Notizen zu Anfragen ausblenden', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('notes_sales_deny', 'Notizen zu Aufträgen ausblenden', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('office_management', 'Office Management allgemein', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('open_project_list', 'Offene Projekte mit Zahlungen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('option_add', 'Auswahllisten Eintrag hinzufügen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('option_edit', 'Auswahllisten bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('organisation_admin', 'Organisation Administration', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('page_permission_display', 'Rechte einer Seite anzeigen', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('payment_condition_config', 'Zahlungskonditionen konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('permission_grant', 'Rechte einräumen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('phone_list_deny', 'Telefonliste ausblenden', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_create', 'Produkt anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_datasheet_upload', 'Produkt Datenblätter einstellen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_datasheet_upload_admin', 'Produkt Datenblätter einstellen Admin', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_description_edit', 'Produktname und Beschreibung ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_details_edit', 'Produktdetails bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_edit', 'Produkt bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_edit_even_locked', 'Produktflags ändern wenn Produkt gesperrt', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_group_edit', 'Produktgruppen anlegen/ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_import', 'Produkte importieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_planning', 'Produktdisposition', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_price_by_quantity_config', 'Produktpreis nach Menge konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_price_by_quantity_edit', 'Produktpreis nach Menge bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_price_edit', 'Produkt: Verkaufspreise bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_price_model_config', 'Produktpreismodell konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_price_under_pp_view', 'Produkte mit VK unter EK listen', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_sales_volume', 'Produktumsatz anzeigen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_selections_admin', 'Produktfilter konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_serialflag_change', 'Produkt Seriennummerneinstellung ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_sp_lower_pp', 'VK < EK einstellen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('product_valuation_change', 'Produktbewertung ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('project_as_reference', 'Referenzprojekt Einstellungen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('project_billing', 'Rechnungen einsehen/anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('project_billing_delete', 'Rechnungen/Zahlungen löschen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('project_internet_config', 'Internet-Referenzprojekte bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('project_monitoring_service', 'Auftragssteuerung Service', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('project_subcontractor_export', 'Subunternehmerprojekte exportieren', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('projects_by_employee', 'Suche nach Aufträgen je Mitarbeiter', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchase_delivery_create', 'Wareneingang anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchase_delivery_note_change', 'Warenlieferung ändern', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchase_invoice_change', 'Wareineinkauf ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchase_invoice_create', 'Lieferantenrechnung einbuchen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchase_invoice_delete', 'Wareneingang löschen', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchase_order_change', 'Bestellung ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchase_order_create', 'Bestellung anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchase_price_change', 'Einkaufspreis ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchase_price_view', 'Einkaufspreis einsehen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('purchasing', 'Einkauf allgemein', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('query_config', 'Abfragen konfigurieren', false, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('query_config_display', 'Abfragekonfiguration einsehen', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('record_custom_payment_input', 'Benutzerdefinierten Zahlungstyp setzen', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('record_export', 'Belegjournal exportieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('record_historical_import', 'Historische Belege importieren', false, true, false, NULL, false);
INSERT INTO user_permissions VALUES ('record_manual_date_input', 'Manuelles Belegdatum setzen', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('record_manual_number_input', 'Manuelle Belegnummer setzen', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('record_sequence_override', 'Manuelle Belegnummer bei aktiver Sequenz', false, true, false, NULL, false);
INSERT INTO user_permissions VALUES ('request_customer_change', 'Interessent/Kunde ändern', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('requests_cancel', 'Anfragen canceln', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('requests_queries_by_branch', 'Anfragen abfragen (alle Niederlassungen)', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('role_configs_admin', 'Mitarbeitergruppen konfigurieren', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('runtime_config', 'Runtime Parameter Konfiguration', false, true, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales', 'Vertrieb allgemein', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_agent_config', 'Vermittler konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_change', 'Vertriebsmitarbeiter ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_close_unconditionally', 'Auftrag abbrechen', false, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('sales_credit_note_delete', 'Gutschrift löschen (Achtung!)', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_credit_note_payment', 'Zahlungen eintragen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_invoice_change_id', 'Rechnungsnummer ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_invoice_change_id_closed', 'Nummer freigegebener Rechnung ändern (Achtung!)', false, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('sales_invoice_delete', 'Rechnung löschen (Achtung!)', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_monitoring_branch_selection', 'Auftragssteuerung alle Niederlassungen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_offer_change_status', 'Angebotsfreigabe aufheben', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_order_change_closed', 'Auftragsfreigabe aufheben', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_order_reset', 'Anfragestatus wiederherstellen (Auftrag löschen!)', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_override_partner_price', 'Partnerpreis in Kalkulation überschreiben', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_region_config', 'Vertriebsgebiete konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_reopen_closed', 'Auftragstatus abgeschlossen löschen', false, true, false, NULL, false);
INSERT INTO user_permissions VALUES ('sales_revenue', 'Deckungsbeitragsrechnung', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('secretary', 'Sekretärsfunktion', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('stocktaking_admin', 'Inventur anlegen/bearbeiten', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('supplier_booking_change', 'Lieferantenbuchungsmodi ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('supplier_change', 'Lieferantendaten ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('supplier_config', 'Lieferantenart ändern', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('synchronize', 'Kontakte, Gruppen und User synchronisieren', false, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('technics', 'Technik allgemein', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('telephone_configuration_own_edit', 'Telefonkonfiguration (eigene) konfigurieren', true, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('telephone_exchange_calls', 'Telefonlisten der Zentraltelefone einsehen', true, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('telephone_exchange_click_to_call', 'ClickToCall mit Zentraltelefon', true, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('telephone_exchange_edit', 'Telefonzentrale konfigurieren', true, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('telephone_system_admin', 'Telefonsysteme administrieren', true, true, true, NULL, false);
INSERT INTO user_permissions VALUES ('template_admin', 'Druckvorlagen aktualisieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('template_admin_email', 'Email-Vorlagen aktualisieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('ticket_system_admin', 'Tickets administrieren', false, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('ticket_system_editor', 'Tickets editieren', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('time_record_delete', 'Zeiterfassung Buchung löschen', false, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('time_recording_admin', 'Zeiterfassung administrieren', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('time_recording_approvals', 'Zeiterfassung Buchung genehmigen', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('time_recording_config', 'Zeiterfassung konfigurieren', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('time_recording_display', 'Zeiterfassung einsehen', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('time_recording_marker', 'Zeiterfassung Marker bearbeiten', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('volume_invoice_create', 'Sammelrechnungen erstellen', true, false, true, NULL, false);
INSERT INTO user_permissions VALUES ('warranty_replacement_create', 'Garantieaustausch anlegen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('website_config', 'Internetseite konfigurieren', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('wholesale', 'Grosshandel allgemein', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('xsl_download', 'Dokumentvorlage einsehen', true, false, false, NULL, false);
INSERT INTO user_permissions VALUES ('xsl_upload', 'Dokumentvorlagen einstellen', true, false, false, NULL, false);


--
-- Data for Name: system_permission_config_items; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_permission_config_items VALUES (1, 1, 'executive');
INSERT INTO system_permission_config_items VALUES (2, 1, 'executive_branch');
INSERT INTO system_permission_config_items VALUES (3, 2, 'executive');
INSERT INTO system_permission_config_items VALUES (4, 2, 'executive_branch');
INSERT INTO system_permission_config_items VALUES (5, 3, 'executive');
INSERT INTO system_permission_config_items VALUES (6, 3, 'executive_branch');
INSERT INTO system_permission_config_items VALUES (7, 3, 'technics');
INSERT INTO system_permission_config_items VALUES (8, 3, 'customer_service');
INSERT INTO system_permission_config_items VALUES (9, 3, 'invoice_unspecified_discounts');
INSERT INTO system_permission_config_items VALUES (10, 4, 'sales_monitoring_branch_selection');
INSERT INTO system_permission_config_items VALUES (11, 4, 'executive');
INSERT INTO system_permission_config_items VALUES (12, 5, 'executive');
INSERT INTO system_permission_config_items VALUES (13, 5, 'executive_branch');
INSERT INTO system_permission_config_items VALUES (14, 5, 'requests_cancel');
INSERT INTO system_permission_config_items VALUES (15, 6, 'sales_override_partner_price');
INSERT INTO system_permission_config_items VALUES (16, 7, 'ldap_mail_admin');
INSERT INTO system_permission_config_items VALUES (17, 8, 'contact_delete');
INSERT INTO system_permission_config_items VALUES (18, 8, 'executive_sales');
INSERT INTO system_permission_config_items VALUES (19, 8, 'organisation_admin');
INSERT INTO system_permission_config_items VALUES (20, 9, 'customer_export');
INSERT INTO system_permission_config_items VALUES (21, 9, 'contact_export');
INSERT INTO system_permission_config_items VALUES (22, 9, 'organisation_admin');


--
-- Name: system_permission_config_items_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_permission_config_items_id_seq', 22, true);


--
-- Name: system_permission_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_permission_configs_id_seq', 7, true);


--
-- Data for Name: system_properties; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_properties VALUES (0, 'appConfigStatus', 'setupInitial', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (1, 'appName', 'osdb', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (2, 'appLogoutTarget', 'login', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (3, 'companyHeadquarter', '1', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (4, 'companyLtdRecordsEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (5, 'internetSite', 'http://osserp.org', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (6, 'urlHelpServer', NULL, 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (7, 'supportedLocale', 'de_DE', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (8, 'attachmentPath', '/var/lib/osserp/dms/attachments', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (9, 'purchaseOfferManager', 'com.osserp.core.purchasing.PurchaseOfferManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (10, 'purchaseOrderManager', 'com.osserp.core.purchasing.PurchaseOrderManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (11, 'purchaseDeliveryNoteManager', 'com.osserp.core.purchasing.PurchaseDeliveryNoteManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (12, 'purchaseInvoiceManager', 'com.osserp.core.purchasing.PurchaseInvoiceManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (13, 'salesOfferManager', 'com.osserp.core.sales.SalesOfferManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (14, 'salesOrderManager', 'com.osserp.core.sales.SalesOrderManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (15, 'salesDeliveryNoteManager', 'com.osserp.core.sales.SalesDeliveryNoteManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (16, 'salesDownpaymentManager', 'com.osserp.core.sales.SalesDownpaymentManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (17, 'salesInvoiceManager', 'com.osserp.core.sales.SalesInvoiceManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (18, 'salesCancellationManager', 'com.osserp.core.sales.SalesCancellationManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (19, 'salesCreditNoteManager', 'com.osserp.core.sales.SalesCreditNoteManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (20, 'contactNoteManager', 'com.osserp.core.contacts.ContactNoteManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (21, 'salesNoteManager', 'com.osserp.core.sales.SalesNoteManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (22, 'productNoteManager', 'com.osserp.core.products.ProductNoteManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (23, 'purchaseOrderNoteManager', 'com.osserp.core.purchasing.PurchaseOrderNoteManager', 'java.lang.String', false, false, 'serviceDeclaration', false, 0);
INSERT INTO system_properties VALUES (24, 'ldapMapperEmployees', 'com.osserp.core.service.directory.DefaultEmployeeMapper', 'java.lang.String', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (25, 'salesBillingProductSelection', '3', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (26, 'salesCampaignEmptyBranchPublic', 'true', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (27, 'salesInvoiceClosedIdChange', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (28, 'salesPersonSelectorByGroup', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (29, 'projectManagerSelectorByGroup', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (30, 'productSearchFetchSize', '25', 'java.lang.Integer', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (31, 'productTypeOpenOrders', '101', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (32, 'discountProduct', '90000', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (33, 'volumeInvoiceProcessorSeparatorProduct', '99999', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (34, 'websiteSyncSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (35, 'websiteReceiptSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (36, 'telephoneSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (37, 'timeRecordingApprovalCreatedEvent', '8000001', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (38, 'timeRecordingApprovalUpdatedEvent', '8000002', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (39, 'appointmentDefaultsHours', '9', 'java.lang.Integer', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (40, 'appointmentDefaultsMinutes', '0', 'java.lang.Integer', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (41, 'ldapUserAlias', 'mailalternateaddress', 'java.lang.String', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (42, 'ldapClientName', 'domain0', 'java.lang.String', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (43, 'ldapEnableFetchmail', 'false', 'java.lang.Boolean', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (44, 'ldapFetchmailMode', 'poll', 'java.lang.String', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (45, 'ldapFetchmailServerName', 'mail.osserp.org', 'java.lang.String', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (46, 'ldapFetchmailServerType', 'pop3', 'java.lang.String', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (47, 'ldapGroupStartId', '5003', 'java.lang.Long', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (48, 'ldapGroupQuery', 'default', 'java.lang.String', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (49, 'ldapUserStartIdSystem', '5001', 'java.lang.Long', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (50, 'ldapUserStartIdMailbox', '5101', 'java.lang.Long', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (51, 'ldapUserQuery', 'default', 'java.lang.String', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (52, 'ldapUserGroupId', '5004', 'java.lang.Long', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (53, 'ldapEnableDbmail', 'true', 'java.lang.Boolean', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (54, 'ldapAdminClientOu', 'Services', 'java.lang.String', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (55, 'ldapNocAdministrationEnabled', 'false', 'java.lang.Boolean', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (56, 'syncNocEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (57, 'syncNocTaskId', '1', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (58, 'documentFooterLayoutCentered', 'false', 'java.lang.Boolean', false, true, 'documentConfig', false, -1);
INSERT INTO system_properties VALUES (59, 'documentOutputDebugMap', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 51);
INSERT INTO system_properties VALUES (60, 'documentOutputDebugXSL', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 52);
INSERT INTO system_properties VALUES (61, 'documentOutputDebugXML', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 53);
INSERT INTO system_properties VALUES (62, 'recordTermsAndConditionsAvailable', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (63, 'documentPrintHeader', 'false', 'java.lang.Boolean', false, true, 'documentConfig', false, -1);
INSERT INTO system_properties VALUES (64, 'documentPrintHeaderLogo', 'true', 'java.lang.Boolean', true, false, 'documentConfig', false, 10);
INSERT INTO system_properties VALUES (65, 'documentPrintHeaderLogoWidth', '8.66cm', 'java.lang.String', true, false, 'documentConfig', false, 12);
INSERT INTO system_properties VALUES (66, 'documentPrintHeaderLogoHeight', '2.0cm', 'java.lang.String', true, false, 'documentConfig', false, 13);
INSERT INTO system_properties VALUES (67, 'documentPrintHeaderTop', '0.8cm', 'java.lang.String', true, false, 'documentConfig', false, 11);
INSERT INTO system_properties VALUES (68, 'documentPrintHeaderHeight', '2.5cm', 'java.lang.String', true, false, 'documentConfig', false, 22);
INSERT INTO system_properties VALUES (69, 'documentPrintHeaderCompanySpace', '0.5cm', 'java.lang.String', true, false, 'documentConfig', false, 18);
INSERT INTO system_properties VALUES (70, 'documentPrintFooterLayout', 'default', 'java.lang.String', false, false, 'documentConfig', false, 37);
INSERT INTO system_properties VALUES (71, 'documentPrintHeaderWidthAddress', '11.2cm', 'java.lang.String', true, false, 'documentConfig', false, 19);
INSERT INTO system_properties VALUES (72, 'documentPrintHeaderWidthCompany', '5.8cm', 'java.lang.String', true, false, 'documentConfig', false, 20);
INSERT INTO system_properties VALUES (73, 'ldapAvailable', 'false', 'java.lang.Boolean', false, false, 'ldapSupport', false, 0);
INSERT INTO system_properties VALUES (74, 'salesOrderPurchasingProductSelectionConfig', NULL, 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (75, 'contactB2BMobilePhoneEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (76, 'stockReportDefaultGroup', NULL, 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (77, 'stockReportIgnoreGroups', NULL, 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (78, 'contactCreateNewsletterStatus', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (79, 'contactImportAvailable', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (80, 'contactImportPath', '/var/lib/osserp/dms/imports', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (81, 'contactImportConfig', '2', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (82, 'salesIdGenerator', 'request', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (83, 'taxReportEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (84, 'orderCreateEmptyOnMissingDefault', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (85, 'orderCreateEmptyOnMissingProduct', '100000', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (86, 'zipcodeSearchExternal', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (87, 'businessCaseAccountingRefName', 'Referenz-Nr.', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (88, 'businessCaseAccountingRefSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (89, 'syncClientEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (90, 'syncClientTaskId', '3', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (91, 'wsProductReceiptEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (92, 'wsBusinessCaseReceiptEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (93, 'wsContactReceiptEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (94, 'requestRestartOfferResetEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (95, 'eventDistributionEnabled', 'true', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (96, 'contactImportOrigin', '20', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (97, 'documentCustomTemplatePath', 'xsl/branchs', 'java.lang.String', false, true, 'documentConfig', false, -1);
INSERT INTO system_properties VALUES (98, 'wsBusinessCaseBranchRequired', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (99, 'wsBusinessCaseBusinessIdRequired', 'true', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (100, 'wsBusinessCaseCreateMissingOrigin', 'true', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (101, 'wsBusinessCaseCreateMissingCampaign', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (102, 'documentTemplateAddUnassigned', 'false', 'java.lang.Boolean', false, true, 'documentConfig', false, -1);
INSERT INTO system_properties VALUES (103, 'documentConditionsFormatted', 'false', 'java.lang.Boolean', false, true, 'documentConfig', false, -1);
INSERT INTO system_properties VALUES (104, 'businessCaseExternalUrlSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (105, 'eventsIgnoreOnCanceledFCS', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (106, 'documentPrintHeaderLogoRepeat', 'false', 'java.lang.Boolean', true, false, 'documentConfig', false, 15);
INSERT INTO system_properties VALUES (107, 'recordItemsUnique', 'true', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (108, 'documentPrintHeaderLogoRepeatMarginTop', '2.5cm', 'java.lang.String', true, false, 'documentConfig', false, 16);
INSERT INTO system_properties VALUES (109, 'eventFcsCloseOther', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (110, 'wsBusinessCaseTypeDefaultRequest', '1', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (111, 'wsBusinessCaseTypeDefaultSales', '2', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (112, 'discountCheckEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (113, 'employeeSearchIgnoreBranch', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (114, 'salesCampaignEmptyBranchIgnoreByGlobal', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (115, 'businessCasePropertySupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (116, 'customerPropertySupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (117, 'productPropertySupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (118, 'businessCaseInstallationSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (119, 'stocktakingSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (120, 'recordFetchLatestStart', '0', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (121, 'businessCaseReportColumns', 'id,action_name,origin_name,person_sales,branchkey,lastname,firstname,street,zipcode,city,phone,mobile,mailaddress,created,action_created,accounting_ref,is_sales,origin,initial_created,contactgrant,installer', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (122, 'purchasePaymentWarnings', 'true', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (123, 'stockManagement', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (124, 'volumeInvoiceSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (125, 'timeRecordingSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (126, 'businessCaseReopenSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (127, 'salesInvoicePaymentFcsSupport', 'true', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (128, 'recordFetchSize', '250', 'java.lang.Integer', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (129, 'companyBranchMaxLengthShortkey', '3', 'java.lang.Integer', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (130, 'appLabel', NULL, 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (131, 'eventCacheEnabled', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (132, 'productImportAvailable', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (133, 'productImportPath', '/var/lib/osserp/dms/imports', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (134, 'productImportConfig', '4', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (135, 'productImportQuantityUnitDefault', '1', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (136, 'documentPrintHeaderLogoAreaLeft', '10cm', 'java.lang.String', false, true, 'documentConfig', false, -1);
INSERT INTO system_properties VALUES (137, 'documentPrintHeaderLogoAreaRight', '7cm', 'java.lang.String', false, true, 'documentConfig', false, -1);
INSERT INTO system_properties VALUES (138, 'stocktakingProductSelectionConfig', '2', 'java.lang.Long', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (139, 'stocktakingProductListStep', '100', 'java.lang.Integer', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (140, 'supportedOptions', 'productColors,requestOriginTypes,campaignGroups,campaignTypes,recordDeliveryConditions,taxFreeDefinitions', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (141, 'supplierProductGroupsCollectorEnabled', 'true', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (142, 'recordClearDecimalAmountLimit', '1.00', 'java.lang.Double', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (143, 'calculationPartlistSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (144, 'calculationCalculatorSelectionSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (145, 'recordCustomHeaderSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (146, 'documentPrintOffice', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 33);
INSERT INTO system_properties VALUES (147, 'dbmsSchema', '0', 'java.lang.Long', false, false, 'dbConfig', false, 0);
INSERT INTO system_properties VALUES (148, 'dbmsSchemaCustom', '0', 'java.lang.Long', false, false, 'dbConfig', false, 0);
INSERT INTO system_properties VALUES (149, 'dbmsSchemaEE', '0', 'java.lang.Long', false, false, 'dbConfig', false, 0);
INSERT INTO system_properties VALUES (150, 'productCustomNameSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (151, 'footerDisplayUserEnabled', 'false', 'java.lang.Boolean', false, false, 'guiConfig', false, 0);
INSERT INTO system_properties VALUES (152, 'customerExportAvailable', 'false', 'java.lang.Boolean', false, false, 'dataExport', false, 0);
INSERT INTO system_properties VALUES (153, 'customerlistLastSalesBefore', 'Y1', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (154, 'salesInvoicePartialOnDownpaymentSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (155, 'salesInvoiceTaxPointNow', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (156, 'dbmsSchemaVersion', '0.16.4', 'java.lang.String', false, false, 'dbConfig', false, 0);
INSERT INTO system_properties VALUES (157, 'salesInvoiceFinalOnPartialSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (158, 'purchaseInvoiceRecordDateCurrent', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (159, 'appContext', 'default', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (160, 'appointmentDefaults', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (161, 'requestToOfferMaxDays', '3', 'java.lang.Integer', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (162, 'contactValidationMinimal', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (163, 'businessNotesEditable', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (164, 'requestSearchFetchSize', '100', 'java.lang.Integer', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (165, 'requestSearchFetchSizeDisabled', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (166, 'requestSearchOrderByColumn', 'created', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (167, 'requestSearchOrderByDescendent', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (168, 'salesSearchFetchSize', '100', 'java.lang.Integer', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (169, 'salesSearchFetchSizeDisabled', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (170, 'salesSearchOrderByColumn', 'created', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (171, 'salesSearchOrderByDescendent', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (172, 'purchaseOrderSearchFetchSize', '100', 'java.lang.Integer', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (173, 'purchaseOrderSearchFetchSizeDisabled', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (174, 'purchaseOrderSearchOrderByColumn', 'created', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (175, 'purchaseOrderSearchOrderByDescendent', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (176, 'purchaseOrderQueryOrderByColumn', 'created', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (177, 'purchaseOrderQueryOrderByDescendent', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (178, 'productPriceMethodPurchasing', 'default', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (179, 'productPriceMethodSales', 'default', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (180, 'productPriceDefaultPurchasing', 'default', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (181, 'productPriceDefaultSales', 'default', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (182, 'recordImportMapper', 'none', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (183, 'recordImportCreateProducts', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (184, 'recordImportUpdatePrice', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (185, 'purchaseOrderImportMapper', 'none', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (186, 'salesOrderImportMapper', 'none', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (187, 'salesByActionDefaultType', '2', 'java.lang.Long', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (188, 'documentFormatQuantityByDbms', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 35);
INSERT INTO system_properties VALUES (189, 'systemPrintInitialContext', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (190, 'systemPrintEnvironment', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (191, 'documentPrintFooterLastnameFirstname', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 32);
INSERT INTO system_properties VALUES (192, 'documentPrintHeaderLastnameFirstname', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 31);
INSERT INTO system_properties VALUES (193, 'documentPrintAddDefaultCountry', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 34);
INSERT INTO system_properties VALUES (194, 'contactDefaultCountry', '53', 'java.lang.Long', false, true, null, false, 0);
INSERT INTO system_properties VALUES (195, 'documentPrintFontFamily', 'Helvetica', 'java.lang.String', false, false, 'documentConfig', true, 30);
INSERT INTO system_properties VALUES (196, 'documentPrintFontSizeFooter', '7pt', 'java.lang.String', true, false, 'documentConfig', false, 9);
INSERT INTO system_properties VALUES (197, 'documentPrintFontSizeHeader', '8pt', 'java.lang.String', true, false, 'documentConfig', false, 1);
INSERT INTO system_properties VALUES (198, 'documentPrintFontSizeHeaderName', '10pt', 'java.lang.String', true, false, 'documentConfig', false, 4);
INSERT INTO system_properties VALUES (199, 'documentPrintFontSizeLetter', '10pt', 'java.lang.String', true, false, 'documentConfig', false, 8);
INSERT INTO system_properties VALUES (200, 'documentPrintFontSizeRecords', '9pt', 'java.lang.String', true, false, 'documentConfig', false, 6);
INSERT INTO system_properties VALUES (201, 'documentPrintHeaderAddressSpace', '2.2cm', 'java.lang.String', true, false, 'documentConfig', false, 17);
INSERT INTO system_properties VALUES (202, 'documentPrintHeaderContentSpaceLetter', '2cm', 'java.lang.String', true, false, 'documentConfig', false, 24);
INSERT INTO system_properties VALUES (203, 'documentPrintHeaderHeightTotal', '3.0cm', 'java.lang.String', true, false, 'documentConfig', false, 21);
INSERT INTO system_properties VALUES (204, 'documentOutputDownloadMap', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 54);
INSERT INTO system_properties VALUES (205, 'documentOutputDownloadXSL', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 55);
INSERT INTO system_properties VALUES (206, 'documentOutputDownloadXML', 'false', 'java.lang.Boolean', false, false, 'documentConfig', false, 56);
INSERT INTO system_properties VALUES (207, 'recordRightToCancelAvailable', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (208, 'documentPrintDownpaymentPayments', 'true', 'java.lang.Boolean', false, false, 'documentConfig', false, 36);
INSERT INTO system_properties VALUES (209, 'salesInvoiceCancelDownpayments', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (210, 'customerLetterSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (211, 'salesRevenueCostCollector', 'costCollectorByPurchase', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (212, 'mailSenderAlwaysBccFromAddress', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (213, 'documentPrintHeaderLogoLeft', 'false', 'java.lang.Boolean', true, false, 'documentConfig', false, 14);
INSERT INTO system_properties VALUES (214, 'addressAutocompletionSupport', 'false', 'java.lang.Boolean', false, false, null, false, 0);
INSERT INTO system_properties VALUES (215, 'mailMessageSizeLimit', '10240000', 'java.lang.Integer', true, false, null, false, 0);
INSERT INTO system_properties VALUES (216, 'documentPrintHeaderContentSpace', '0.1cm', 'java.lang.String', true, false, 'documentConfig', false, 23);
INSERT INTO system_properties VALUES (217, 'documentPrintFontSizeHeaderAddress', '10pt', 'java.lang.String', true, false, 'documentConfig', false, 2);
INSERT INTO system_properties VALUES (218, 'documentPrintFontSizeHeaderCompany', '8pt', 'java.lang.String', true, false, 'documentConfig', false, 3);
INSERT INTO system_properties VALUES (219, 'projectSupplierRelationSupport', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (220, 'purchaseInvoicePrintHeaderDefault', 'Retoure', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (221, 'documentPrintFontSizeHeaderSubject', '11pt', 'java.lang.String', true, false, 'documentConfig', false, 5);
INSERT INTO system_properties VALUES (222, 'contactAddressMapSupport', 'false', 'java.lang.Boolean', true, false, null, false, 0);
INSERT INTO system_properties VALUES (223, 'projectAddressMapSupport', 'true', 'java.lang.Boolean', true, false, null, false, 0);
INSERT INTO system_properties VALUES (224, 'calendarTextLength', '25', 'java.lang.Integer', true, false, null, false, 0);
INSERT INTO system_properties VALUES (225, 'productClassificationOrderByColumns', 'type_id, group_name', 'java.lang.String', true, false, null, false, 0);
INSERT INTO system_properties VALUES (226, 'fetchmailInboxSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (227, 'fetchmailAutoImportBusinessCase', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (228, 'taxRateZeroSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (229, 'taxRateZeroTextPrefix', 'Ermäßigter Steuersatz 0% auf', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (230, 'taxRateZeroTextPostfix', 'gemäß § 12 Absatz 3 UStG', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (231, 'deEst35aSupport', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (232, 'salesDownpaymentHeaderByType', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (233, 'yearlySequenceIgnorables', NULL, 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (234, 'popupPageSize', 'default', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (235, 'baseTagEnabled', 'false', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (236, 'voucherExportSortReverse', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (237, 'dmsListThumbnailHeight', '128px', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (238, 'dmsGalleryThumbnailCols', 'col-sm-6 col-md-3', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (239, 'dmsGalleryThumbnailHeight', '240px', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (240, 'dmsGalleryImageHeight', '720px', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (241, 'documentPrintFontSizeTerms', '8pt', 'java.lang.String', true, false, 'documentConfig', false, 7);
INSERT INTO system_properties VALUES (242, 'documentPrintVoucherNoteSpaceBefore', '1pt', 'java.lang.String', true, false, 'documentConfig', false, 25);
INSERT INTO system_properties VALUES (243, 'documentPrintVoucherTermsSpaceBefore', '5pt', 'java.lang.String', true, false, 'documentConfig', false, 26);
INSERT INTO system_properties VALUES (244, 'documentPrintVoucherTermsSpaceRight', '0.5cm', 'java.lang.String', true, false, 'documentConfig', false, 27);


--
-- Name: system_properties_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_properties_id_seq', 244, true);


--
-- Data for Name: system_query_context; Type: TABLE DATA; Schema: osserp; Owner: osserp
--


INSERT INTO system_query_context (id, name, context) VALUES (1, 'Abfragen allgemein', 'common');
INSERT INTO system_query_context (id, name, context) VALUES (2, 'Kontakte', 'contact');
INSERT INTO system_query_context (id, name, context) VALUES (3, 'Anfragen', 'request');
INSERT INTO system_query_context (id, name, context) VALUES (4, 'Aufträge', 'sales');
INSERT INTO system_query_context (id, name, context) VALUES (5, 'Einkauf', 'purchase');
INSERT INTO system_query_context (id, name, context) VALUES (6, 'Buchhaltung', 'accounting');
INSERT INTO system_query_context (id, name, context) VALUES (7, 'Produkte', 'products');


--
-- Data for Name: system_queries; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_queries_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_queries_id_seq', 100, false);


--
-- Data for Name: system_query_outputs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_query_outputs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_query_outputs_id_seq', 1000, false);


--
-- Data for Name: system_query_parameters; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_query_parameters_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_query_parameters_id_seq', 1000, false);


--
-- Data for Name: system_query_permissions; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_query_permissions_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_query_permissions_id_seq', 1000, false);


--
-- Data for Name: system_setup; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_setup VALUES (1, 'Initial setup', false, 'contactPasswordComplete');


--
-- Name: system_setup_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_setup_id_seq', 1, false);


--
-- Data for Name: system_setup_properties; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_setup_properties_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_setup_properties_id_seq', 1, false);


--
-- Data for Name: system_sync_mappings; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_sync_mappings_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_sync_mappings_id_seq', 1, false);


--
-- Data for Name: system_sync_task_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO system_sync_task_configs VALUES (1, 'nocSynchronization', false, 'com.osserp.core.service.directory.NocSyncTaskExecutor');
INSERT INTO system_sync_task_configs VALUES (2, 'contactImport', false, 'com.osserp.core.contacts.ContactImportManager');
INSERT INTO system_sync_task_configs VALUES (3, 'productExport', false, 'com.osserp.core.api.client.ProductSyncTaskExecutor');
INSERT INTO system_sync_task_configs VALUES (4, 'productImport', false, 'com.osserp.core.products.ProductImportManager');


--
-- Name: system_sync_task_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_sync_task_configs_id_seq', 4, true);


--
-- Data for Name: system_sync_tasks; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: system_sync_tasks_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('system_sync_tasks_id_seq', 1, false);


--
-- Data for Name: telephone_calls; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: telephone_call_contacts; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: telephone_call_contacts_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('telephone_call_contacts_id_seq', 1, false);


--
-- Name: telephone_calls_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('telephone_calls_id_seq', 1, false);


--
-- Data for Name: telephone_configuration_changesets; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: telephone_configuration_changesets_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('telephone_configuration_changesets_id_seq', 1, false);


--
-- Data for Name: telephone_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: telephones; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: telephone_configurations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: telephone_configurations_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('telephone_configurations_id_seq', 1, false);


--
-- Name: telephone_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('telephone_groups_id_seq', 1, false);


--
-- Name: telephone_system_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('telephone_system_types_id_seq', 1, false);


--
-- Name: telephone_systems_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('telephone_systems_id_seq', 1, false);


--
-- Name: telephone_types_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('telephone_types_id_seq', 1, false);


--
-- Name: telephones_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('telephones_id_seq', 1, false);


--
-- Data for Name: time_record_status; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO time_record_status VALUES (0, 'noApprovalNeeded');
INSERT INTO time_record_status VALUES (10, 'waitForApproval');
INSERT INTO time_record_status VALUES (15, 'approved');
INSERT INTO time_record_status VALUES (20, 'correctionWaitForApproval');
INSERT INTO time_record_status VALUES (25, 'correctionApproved');


--
-- Data for Name: time_record_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO time_record_types VALUES (0, 'Gehen', false, false, true, 8, 0, 8, 0, 'Ende einer beliebigen Startbuchung', 0, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false, true, false, false, false, false, false, false, false, false, true, false, false, false, false, true, NULL, false);
INSERT INTO time_record_types VALUES (100, 'Beginn', true, false, true, 8, 0, 8, 0, 'Standard Startbuchung', 1, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false, true, true, false, false, false, false, false, false, false, true, false, false, false, false, true, NULL, false);
INSERT INTO time_record_types VALUES (101, 'Intervall', true, true, true, 8, 0, 8, 0, 'Buchung vor Aktuellen', 2, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, false, true, false, false, false, false, false, true, false, false, true, false, false, false, false, true, NULL, true);
INSERT INTO time_record_types VALUES (102, 'Dienstgang', true, false, true, 8, 0, 8, 0, 'Startbuchung Dienstgang', 3, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, true, NULL, false);
INSERT INTO time_record_types VALUES (103, 'Homeoffice', true, false, true, 8, 0, 8, 0, 'Startbuchung Homeoffice', 4, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, true, NULL, false);
INSERT INTO time_record_types VALUES (104, 'Dienstreise', true, true, true, 8, 0, 8, 0, 'Dienstreise', 33, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, false, false, '', false);
INSERT INTO time_record_types VALUES (200, 'Urlaub', true, true, true, 8, 0, 8, 0, 'Standard Urlaubstag', 5, '1970-01-01 00:00:00', 6000, NULL, NULL, false, true, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, false, true, NULL, true);
INSERT INTO time_record_types VALUES (201, 'Gleitzeit (Überstundenfrei)', true, true, true, 8, 0, 8, 0, 'Ausgleich Zeitkonto', 6, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, true, true, true, true, false, true, false, false, false, true, true, NULL, true);
INSERT INTO time_record_types VALUES (210, 'Sonderurlaub', true, true, true, 8, 0, 8, 0, 'Sonderurlaub (Geburt, Hochzeit, Sterbefall)', 11, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (211, 'Bildungsurlaub', true, true, true, 8, 0, 8, 0, 'Bildungsurlaub', 12, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (300, 'Berufsschule', true, true, true, 8, 0, 8, 0, 'Berufsschule', 7, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, true, false, true, true, false, false, true, false, false, false, false, true, 'booking_school', false);
INSERT INTO time_record_types VALUES (301, 'Weiterbildung', true, true, true, 8, 0, 8, 0, 'Weiterbildung', 13, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, false, false, false, false, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (302, 'Weiterbildung m. Zeitkontenabzug', true, true, true, 8, 0, 8, 0, 'Weiterbildung mit Zeitkontenabzug', 14, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, true, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (400, 'Krankheit', true, true, true, 8, 0, 8, 0, 'Buchung Erkrankung', 9, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, true, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (401, 'Kurzerkrankung', true, true, true, 8, 0, 8, 0, 'Kurzerkrankung bis zu 3 Tage', 15, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, true, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (402, 'Krank o. EFZ', true, true, true, 8, 0, 8, 0, 'Krankheit ohne Entgeltfortzahlung', 16, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, true, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (403, 'Arbeitsunfall', true, true, true, 8, 0, 8, 0, 'Arbeitsunfall', 17, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, true, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (404, 'Arbeitsunfall o. EFZ', true, true, true, 8, 0, 8, 0, 'Arbeitsunfall ohne Entgeltfortzahlung', 18, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, true, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (405, 'Heilverfahren', true, true, true, 8, 0, 8, 0, 'Heilverfahren', 19, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, true, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (406, 'Heilverfahren o. EFZ', true, true, true, 8, 0, 8, 0, 'Heilverfahren ohne Entgeltfortzahlung', 20, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, true, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (410, 'Mutterschutz', true, true, true, 8, 0, 8, 0, 'Buchung Mutterschutz', 12, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, true, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (411, 'Elternzeit', true, true, true, 8, 0, 8, 0, 'Buchung Elternzeit', 13, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, true, false, false, true, true, true, false, true, false, false, false, false, false, NULL, false);
INSERT INTO time_record_types VALUES (500, 'Wehrdienst o. EFZ', true, true, true, 8, 0, 8, 0, 'Wehrdienst ohne Entgeltfortzahlung', 21, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (501, 'Wehrübung m. EFZ', true, true, true, 8, 0, 8, 0, 'Wehrübung mit Entgeltfortzahlung', 22, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (502, 'Zivildienst o. EFZ', true, true, true, 8, 0, 8, 0, 'Zivildienst ohne Entgeltfortzahlung', 23, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (503, 'Zivildienst m. EFZ', true, true, true, 8, 0, 8, 0, 'Zivildienst mit Entgeltfortzahlung', 24, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (600, 'Freistellung m. EFZ', true, true, true, 8, 0, 8, 0, 'Freistellung mit Entgeltfortzahlung', 25, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (601, 'Freistellung o. EFZ', true, true, true, 8, 0, 8, 0, 'Freistellung ohne Entgeltfortzahlung', 26, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (605, 'Keine Zeiterfassung', true, true, true, 8, 0, 8, 0, 'Keine Zeiterfassung ohne Zeitkontenabzug', 29, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (606, 'Theoriephase StudiumPlus', true, true, true, 8, 0, 8, 0, 'Keine Zeiterfassung ohne Zeitkontenabzug', 31, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, true, 'booking_study', false);
INSERT INTO time_record_types VALUES (607, 'Master StudiumPlus', true, true, true, 8, 0, 8, 0, 'Theoriephase mit Zeitkontenabzug', 32, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, true, true, true, true, false, true, false, false, false, true, true, 'booking_study_master', false);
INSERT INTO time_record_types VALUES (610, 'Arbeitsfrei (nur Teilzeitkräfte) mit Zeitkontenabzug', true, true, true, 8, 0, 8, 0, 'Arbeitsfrei (nur Teilzeitkräfte) mit Zeitkontenabzug', 27, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, true, true, true, true, false, true, false, false, false, true, true, 'booking_part_time', false);
INSERT INTO time_record_types VALUES (620, 'ATZ Freistellungsphase', true, true, true, 8, 0, 8, 0, 'Altersteilzeit Freistellungsphase', 28, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, true, false, true, false, false, false, true, false, NULL, false);
INSERT INTO time_record_types VALUES (630, 'Abzug Zeitkonto', true, true, false, 24, 0, 0, 0, 'Abzug Zeitkonto nach Auszahlung', 30, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, true, false, false, false, false, false, true, true, false, true, true, false, false, false, false, false, NULL, false);


--
-- Data for Name: time_recording_configs; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO time_recording_configs VALUES (1, 'Standard', 'Standardeinstellung nach Neuanlage', '1970-01-01 00:00:00', 6000, NULL, NULL, false, true, 28, 5, 40, false, 8, 9, 0, 16, 0, 0, 3);
INSERT INTO time_recording_configs VALUES (2, '8,00', '8,00 50 Std. ', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 8, 9, 0, 16, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (3, 'Service 8,00', 'Service Soll 8,00 200 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 8, 7, 30, 16, 0, 200, 3);
INSERT INTO time_recording_configs VALUES (4, '5,40', 'Soll 5,40 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 5.4, 0, 0, 0, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (5, '5,00', 'Soll 5,00 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 5, 0, 0, 0, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (6, '4,00', 'Soll 4,00 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 4, 0, 0, 0, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (7, 'Mini-Job', 'Mini-Job Soll 0,00 0,0 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 0, 5, 40, true, 0, 0, 0, 0, 0, 0, 3);
INSERT INTO time_recording_configs VALUES (8, '4,00 0 Std.', 'Soll 4,00 0 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 4, 0, 0, 0, 0, 0, 3);
INSERT INTO time_recording_configs VALUES (9, '3,60', 'Soll 3,60 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 3.6, 0, 0, 0, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (10, '6,40', 'Soll 6,40 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 6.4, 0, 0, 0, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (11, '3,20', 'Soll 3,20 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 3.2, 0, 0, 0, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (12, '6,00', 'Soll 6,00 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 6, 9, 0, 14, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (13, '5,60', 'Soll 5,60 50 Std. ', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 5.6, 0, 0, 0, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (14, '7,20', 'Soll 7,20 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 7.2, 0, 0, 0, 0, 50, 3);
INSERT INTO time_recording_configs VALUES (15, '8,00 0 Std.', 'Soll 8,00 0 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 8, 0, 0, 0, 0, 0, 0);
INSERT INTO time_recording_configs VALUES (16, '4,50 ', 'Soll 4,50 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 4.5, 0, 0, 0, 0, 50, 0);
INSERT INTO time_recording_configs VALUES (17, '6,40 0 Std.', 'Soll 6,40 0 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 6.4, 0, 0, 0, 0, 0, 0);
INSERT INTO time_recording_configs VALUES (18, '7,60', 'Soll 7,60 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 7.6, 9, 0, 16, 0, 50, 0);
INSERT INTO time_recording_configs VALUES (19, '4,80', 'Soll 4,80 50 Std.', '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, 28, 5, 40, true, 4.8, 0, 0, 0, 0, 50, 0);


--
-- Data for Name: time_recordings; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: time_recording_periods; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: time_records; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Data for Name: time_record_corrections; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: time_record_corrections_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_record_corrections_id_seq', 1, false);


--
-- Data for Name: time_record_marker_types; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO time_record_marker_types VALUES (0, 'Zusatzstunden täglich', 'Anzahl der Stunden die an diesem Tag zusätzlich zu 10 Stunden gearbeitet werden dürfen)', 1, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, true, false);
INSERT INTO time_record_marker_types VALUES (1, 'Stundenübertrag monatlich', 'Anzahl der Überstunden die in diesem Monat zusätzlich zu im Profil eingstellten Wert gemacht werden dürfen', 2, '1970-01-01 00:00:00', 6000, NULL, NULL, false, true, false, false);
INSERT INTO time_record_marker_types VALUES (2, 'Urlaubsübertrag', 'Anzahl der Urlaubstage aus dem Übetrag die in diesen Montat übernommen werden', 0, '1970-01-01 00:00:00', 6000, NULL, NULL, false, false, false, true);


--
-- Data for Name: time_record_markers; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: time_record_markers_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_record_markers_id_seq', 1, false);


--
-- Name: time_record_status_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_record_status_id_seq', 1, false);


--
-- Data for Name: time_recording_approvals; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: time_recording_approvals_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_recording_approvals_id_seq', 1, false);


--
-- Name: time_recording_configs_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_recording_configs_id_seq', 19, true);


--
-- Data for Name: time_recording_documents; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: time_recording_documents_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_recording_documents_id_seq', 1, false);


--
-- Data for Name: time_recording_documents_obj; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: time_recording_documents_obj_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_recording_documents_obj_id_seq', 1, false);


--
-- Data for Name: time_recording_period_holiday_relations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: time_recording_periods_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_recording_periods_id_seq', 1, false);


--
-- Name: time_recordings_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_recordings_id_seq', 1, false);


--
-- Name: time_records_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('time_records_id_seq', 1, false);


--
-- Data for Name: user_permission_groups; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO user_permission_groups VALUES (0, 'System', 'Systemeinstellungen', NULL, NULL, false);
INSERT INTO user_permission_groups VALUES (1, 'Admin', 'Verwaltende Rechte', NULL, NULL, false);
INSERT INTO user_permission_groups VALUES (2, 'Global', 'Übergreifende oder nicht klar zuordbare Rechte', NULL, NULL, false);
INSERT INTO user_permission_groups VALUES (3, 'Kontakte', 'Rechte die Kontakte betreffen', NULL, NULL, false);
INSERT INTO user_permission_groups VALUES (4, 'Anfragen', 'Rechte die Anfragen betreffen', NULL, NULL, false);
INSERT INTO user_permission_groups VALUES (5, 'Aufträge', 'Rechte die Aufträge betreffen', NULL, NULL, false);
INSERT INTO user_permission_groups VALUES (6, 'Einkauf', 'Rechte die den Wareneinkauf betreffen', NULL, NULL, false);
INSERT INTO user_permission_groups VALUES (7, 'Produkte', 'Rechte die Artikel/Produkt betreffen', NULL, NULL, false);
INSERT INTO user_permission_groups VALUES (8, 'Buchhaltung', 'Rechte die Buchungsvorgänge betreffen', NULL, NULL, false);
INSERT INTO user_permission_groups VALUES (9, 'Personal', 'Rechte die Personal betreffen', NULL, NULL, false);


--
-- Name: user_permission_groups_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('user_permission_groups_id_seq', 9, true);


--
-- Data for Name: user_startup_targets; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO user_startup_targets VALUES (1, 'Termine', 'calendarStartup', '/users/userCalendar/forward', false);
INSERT INTO user_startup_targets VALUES (2, 'Aufgaben', 'eventsStartup', '/events/userEvents/forward', false);
INSERT INTO user_startup_targets VALUES (3, 'Wer ist Online?', 'onlineUsers', '/onlineUsers', false);
INSERT INTO user_startup_targets VALUES (4, 'Auslieferungskalender', 'deliveryCalendarStartup', '/sales/salesDeliveryCalendar/forward', false);
INSERT INTO user_startup_targets VALUES (5, 'Buchhaltung', 'accounting', '/accounting/accountingIndex/forward', false);
INSERT INTO user_startup_targets VALUES (6, 'Setup', 'setup', '/setup', false);
INSERT INTO user_startup_targets VALUES (7, 'Auftragsbestand', 'orderBacklog', '/reporting/projectQueries/forward', false);
INSERT INTO user_startup_targets VALUES (8, 'Meine Aufträge', 'myOrders', '/reporting/projectsByEmployee/forward', false);
INSERT INTO user_startup_targets VALUES (9, 'Meine Anfragen', 'myRequests', '/requests/myRequests/forward', false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO users VALUES (65534, 1000000001, 'Gast', NULL, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 1, 'guest', NULL, 'DE', 'de', NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO users VALUES (6000, 1000000000, 'System', NULL, false, '1970-01-01 00:00:00', 6000, NULL, NULL, 6, 'admin', NULL, 'DE', 'de', NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO users VALUES (6001, 1000000002, 'Setup Admin', '{SSHA}cH8n7WMjWXFmuWVfU2Omi9VuMJwc6c2hpS+uUg==', true, '1970-01-01 00:00:00', 6000, NULL, NULL, 1, 'osserp-setup', NULL, 'DE', 'de', NULL, NULL, NULL, NULL, NULL, 0);


--
-- Data for Name: user_permission_relations; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO user_permission_relations VALUES (1, 6000, 'client_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (2, 6000, 'runtime_config', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (3, 6000, 'ldap_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (4, 6001, 'product_planning', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (5, 6001, 'close_todos', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (6, 6001, 'contact_assign', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (7, 6001, 'contact_create', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (8, 6001, 'customer_service', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (9, 6001, 'executive', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (10, 6001, 'executive_branch', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (11, 6001, 'executive_logistics', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (12, 6001, 'executive_sales', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (13, 6001, 'global_business_case_access', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (14, 6001, 'ignore_price_limit', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (15, 6001, 'manager_add', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (24, 6001, 'client_edit', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (27, 6001, 'client_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (16, 6001, 'margin_change', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (17, 6001, 'open_project_list', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (25, 6001, 'organisation_admin', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (26, 6001, 'permission_grant', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (18, 6001, 'requests_cancel', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (19, 6001, 'sales_change', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (20, 6001, 'sales_monitoring_branch_selection', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (21, 6001, 'technics', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (29, 6001, 'template_admin_email', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (22, 6001, 'time_recording_approvals', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (23, 6001, 'time_recording_display', '1970-01-01 00:00:00', 6000);
INSERT INTO user_permission_relations VALUES (28, 6001, 'campaign_config', '1970-01-01 00:00:00', 6000);


--
-- Name: user_permission_relations_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('user_permission_relations_id_seq', 29, true);


--
-- Data for Name: user_properties; Type: TABLE DATA; Schema: osserp; Owner: osserp
--



--
-- Name: user_properties_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('user_properties_id_seq', 1, false);


--
-- Data for Name: user_property_defaults; Type: TABLE DATA; Schema: osserp; Owner: osserp
--

INSERT INTO user_property_defaults VALUES (1, 'news', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (2, 'events', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (3, 'eventFilterCategory', NULL, 'java.lang.String');
INSERT INTO user_property_defaults VALUES (4, 'projectMonitoring', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (5, 'requestsByEmployee', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (6, 'salesBySales', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (7, 'salesByManager', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (8, 'salesMonitoring', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (9, 'installationCalendar', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (10, 'deliveryCalendar', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (11, 'salesNoteMailAutoCC', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (12, 'salesNoteMailAutoBCC', 'false', 'java.lang.Boolean');


--
-- Name: user_property_defaults_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('user_property_defaults_id_seq', 12, true);


--
-- Name: user_startup_targets_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('user_startup_targets_id_seq', 9, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: osserp; Owner: osserp
--

SELECT pg_catalog.setval('users_id_seq', 6001, true);


--
-- PostgreSQL database dump complete
--
