<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>

<c:set var="view" value="${sessionScope.timeRecordingView}" />
<c:set var="recording" value="${view.timeRecording}" />
<c:set var="period" value="${recording.selectedPeriod}" />
<c:set var="booking" value="${view.selectedRecord}" />

<o:ajaxForm name="dynamicForm" onSuccess="" preRequest="" postRequest="" targetElement="recordUpdate_popup" url="/timeRecordings.do">
    <input type="hidden" name="method" value="updateRecord" />
    <c:if test="${!empty sessionScope.errors}">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="errormessage">
                    <fmt:message key="error" />: <o:out value="${sessionScope.errors}" />
                </h4>
            </div>
        </div>
        <o:removeErrors />
    </c:if>

    <div class="panel-body">
        <div class="form-body">
            <c:choose>
                <c:when test="${empty booking}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="booking"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <fmt:message key="noBookingSelected" />
                            </div>
                        </div>
                    </div>

                </c:when>
                <c:otherwise>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="booking" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${booking.type.defaultType}">
                                        <fmt:message key="comingAndGoingLabel" />
                                    </c:when>
                                    <c:otherwise>
                                        <o:out value="${booking.type.name}" />
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${stopBooking}">
                                        <fmt:message key="time" />
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="startBooking" />
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="startValue" value="<o:out value="${view}" param="startValue"/>" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <fmt:message key="atTime"/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input  class="form-control" type="text" name="startValueHours" value="<o:out value="${view}" param="startValueHours"/>" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="startValueMinutes" value="<o:out value="${view}" param="startValueMinutes"/>" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <fmt:message key="clock" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <c:if test="${!empty booking.closedBy}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="stopBooking" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="stopValue" value="<o:out value="${view}" param="stopValue"/>" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <fmt:message key="atTime"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="stopValueHours" value="<o:out value="${view}" param="stopValueHours"/>" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="stopValueMinutes" value="<o:out value="${view}" param="stopValueMinutes"/>" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <fmt:message key="clock" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="correctionNote" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea name="note" class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                    </div>

                </c:otherwise>
            </c:choose>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/timeRecordings.do?method=selectRecord"/>');" value="<fmt:message key="exit"/>" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" value="save" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</o:ajaxForm>
