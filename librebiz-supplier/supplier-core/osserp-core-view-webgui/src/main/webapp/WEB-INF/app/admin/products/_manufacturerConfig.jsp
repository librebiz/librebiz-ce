<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:form url="${view.baseLink}/save" name="manufacturerConfigForm">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="manufacturer" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive table-responsive-default">
                <table class="table">
                    <tbody>
                        <c:choose>
                            <c:when test="${view.createMode}">
                                <tr>
                                    <td><v:text name="name" styleClass="form-control" /></td>
                                    <td><o:submit styleClass="form-control" value="create" /></td>
                                </tr>
                            </c:when>
                            <c:when test="${view.editMode}">
                                <tr>
                                    <td><v:text name="name" styleClass="form-control" /></td>
                                    <td><o:submit styleClass="form-control" /></td>
                                </tr>
                            </c:when>
                            <c:when test="${!empty view.bean}">
                                <td><o:out value="${obj.name}" /></td>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="obj" items="${view.list}">
                                    <tr>
                                        <td><v:link url="${view.baseLink}/select?id=${obj.id}" title="manufacturerEdit">
                                                <o:out value="${obj.name}" />
                                            </v:link></td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</v:form>
