/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13-Aug-2006 12:13:38 
 * 
 */
package com.osserp.gui.web.calc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;

import com.osserp.core.calc.Calculation;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.calc.CalculationView;
import com.osserp.gui.client.sales.SalesOfferView;
import com.osserp.gui.client.sales.SalesPlanningView;
import com.osserp.gui.client.sales.SalesUtil;

/**
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationAction extends AbstractCalculationAction {
    private static Logger log = LoggerFactory.getLogger(CalculationAction.class.getName());

    /**
     * Forwards to display current calculation. A new calculation will be created if method is invoked in request context and no existing calculation was found
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forward() request from " + getUser(session).getId());
        }
        try {
            CalculationView view = createCalculationView(request);
            String exit = RequestUtil.getExit(request);
            if (exit != null) {
                view.setExitTarget(exit);
            }
            if (RequestUtil.isAvailable(request, "enableEditMode")) {
                view.setEditMode(true);
                view.getCalculator().markChanged();
                return mapping.findForward(Actions.EDIT);
            }
            return mapping.findForward(Actions.DISPLAY);
        } catch (Throwable t) {
            
            if (t instanceof ClientException) {
                log.debug("forward() caught client exception [message=" + t.getMessage() + "]");
                return saveErrorAndReturn(request, t.getMessage());
            }
            BusinessCaseView bcv = fetchBusinessCaseView(session);
            if (bcv != null && bcv.isSalesContext()) {
                return saveErrorAndReturn(request, ErrorCode.HISTORICAL_DATA);
            }
            if (bcv != null) {
                saveError(request, t.getMessage());
                log.debug("forward() error message saved [target=" + bcv.getForwardTarget() + "]");
                return mapping.findForward(bcv.getForwardTarget());
            }
            return mapping.findForward(Actions.INDEX);
        }
    }

    /**
     * Forwards to display current calculation. A new calculation will be created if method is invoked in request context and no existing calculation was found
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward redisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("redisplay() request from " + getUser(session).getId());
        }
        CalculationView view = fetchCalculationView(session);
        if (view == null) {
            return forward(mapping, form, request, response);
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Forwards to display current calculation. A new calculation will be created if method is invoked in request context and no existing calculation was found
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward redisplayEditor(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("redisplayEditor() request from " + getUser(session).getId());
        }
        CalculationView view = fetchCalculationView(session);
        if (view == null || !view.isEditMode()) {
            throw new IllegalStateException("calculation view not exists or editMode disabled");
        }
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Exits from calculation display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        BusinessCaseView bcv = fetchBusinessCaseView(session);
        CalculationView calculationView = getCalculationView(session);
        if (calculationView.getBusinessCase().isSalesContext()) {
            if (log.isDebugEnabled()) {
                log.debug("exit() calculation context is sales context");
            }
            if (bcv == null) {
                if (log.isDebugEnabled()) {
                    log.debug("exit() invoked in sales context but sales" +
                            " was removed, loading new");
                }
                bcv = SalesUtil.createSalesView(
                        servlet.getServletContext(),
                        request,
                        (Sales) calculationView.getBusinessCase());
            }
            String exit = calculationView.getExitTarget();
            removeView(session, CalculationView.class);
            return mapping.findForward(((exit != null) ? exit : Actions.PROJECT));
        }
        if (log.isDebugEnabled()) {
            log.debug("exit() calculation context is request context");
        }
        try {
            Calculation last = calculationView.getCalculation();
            SalesOfferView offerView = (SalesOfferView) createView(request, SalesOfferView.class.getName());
            if (bcv == null) {
                bcv = SalesUtil.createRequestView(
                        servlet.getServletContext(),
                        request,
                        (Request) calculationView.getBusinessCase());
            }
            offerView.load(bcv, last);
            removeView(session, CalculationView.class);
            return mapping.findForward(Actions.OFFER);
        } catch (ClientException c) {
            String exit = calculationView.getExitTarget();
            removeView(session, CalculationView.class);
            return mapping.findForward(((exit != null) ? exit : Actions.PLAN));
        }
    }

    /**
     * Forwards to display current calculation. A new calculation will be created if method is invoked in request context and no existing calculation was found
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward edit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("edit() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        if (view.getOrder() != null) {
            if (view.getOrder().isChangeableDeliveryAvailable()) {
                return saveErrorAndReturn(request, ErrorCode.RECORD_CHANGEABLE);
            }
        }
        view.setEditMode(true);
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Initializes available names and forwards to create form
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardCreate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardCreate() request from " + getUser(session).getId());
        }
        CalculationView view = fetchCalculationView(session);
        if (view == null) {
            view = createCalculationView(request);
        }
        try {
            view.initCreate();
        } catch (ClientException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(Actions.DISPLAY);
        }
        return mapping.findForward(Actions.CREATE);
    }

    /**
     * Exits create form
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitCreate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitCreate() request from " + getUser(session).getId());
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Creates a new calculation by name from text input
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward create(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {
        Form fh = new StrutsForm(form);
        return create(mapping, request, fh.getString(Form.VALUE));
    }

    /**
     * Creates a new calculation by name from request param
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createByName(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        return create(mapping, request, RequestUtil.getString(request, "name"));
    }

    /**
     * Creates a new calculation by name
     * @param mapping
     * @param request
     * @param name
     * @return forward
     * @throws Exception
     */
    private ActionForward create(
            ActionMapping mapping,
            HttpServletRequest request,
            String name)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("create() request from " + getUser(session).getId());
        }
        CalculationView view = fetchCalculationView(session);
        if (view == null) {
            view = createCalculationView(request);
        }
        try {
            view.create(name);
            view.setEditMode(true);
            return mapping.findForward(Actions.EDIT);

        } catch (ClientException e) {
            log.debug("create() validation failed [message=" + e.getMessage() + "]");
            saveError(request, e.getMessage());
            return mapping.findForward(Actions.CREATE);
        }
    }

    /**
     * Selects a calculation
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward select(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("select() request from " + getUser(session).getId());
        }
        CalculationView view = fetchCalculationView(session);
        if (view == null) {
            view = createCalculationView(request);
        }
        view.setSelection(RequestUtil.fetchId(request));
        if (RequestUtil.getTarget(request) != null) {
            return mapping.findForward(RequestUtil.getTarget(request));
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Toggles calculation sales price lock
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleSalesPriceLock(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleSalesPriceLock() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        view.toggleSalesPriceLock();
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Exits create form
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardProductPlanning(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardProductPlanning() request from " + getUser(session).getId());
        }
        Long productId = RequestUtil.getId(request);
        String exitTarget = RequestUtil.getExit(request);
        SalesPlanningView openOrders = (SalesPlanningView) createView(request, SalesPlanningView.class.getName());
        openOrders.enableSummaryAndExit(productId, exitTarget);
        return mapping.findForward("openOrders");
    }

    /**
     * Changes the calculator class
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeCalculator(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeCalculator() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        String calculatorClassName = RequestUtil.getString(request, "name");
        if (isSet(calculatorClassName)) {
            view.changeCalculator(calculatorClassName);
        }
        return mapping.findForward(Actions.CREATE);
    }
}
