/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 08-Feb-2007 17:52:48 
 * 
 */
package com.osserp.core.model.records;

import com.osserp.core.finance.BillingClass;
import com.osserp.core.finance.BillingType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BillingTypeImpl extends RecordTypeImpl implements BillingType {

    private BillingClass billingClass;
    private Long recordType = null;
    private boolean invoice = false;
    private boolean downpayment = false;
    private boolean reducedTax = false;
    private boolean taxPayment = false;
    private boolean taxFree = false;
    private Long taxFreeId = null;
    private Integer orderId = null;

    protected BillingTypeImpl() {
        super();
    }

    public BillingTypeImpl(Long id, String name, BillingClass billingClass) {
        super(id, name);
        this.billingClass = billingClass;
    }

    public BillingClass getBillingClass() {
        return billingClass;
    }

    public Long getRecordType() {
        return recordType;
    }

    public void setRecordType(Long recordType) {
        this.recordType = recordType;
    }

    @Override
    public boolean isPayment() {
        return (billingClass != null && billingClass.isPayment());
    }

    public boolean isInvoice() {
        return invoice;
    }

    public boolean isCashDiscount() {
        return (BillingType.TYPE_CASH_DISCOUNT.equals(getId())
                || BillingType.TYPE_PURCHASE_CASH_DISCOUNT.equals(getId()));
    }

    public void setInvoice(boolean invoice) {
        this.invoice = invoice;
    }

    public boolean isDownpayment() {
        return downpayment;
    }

    public void setDownpayment(boolean downpayment) {
        this.downpayment = downpayment;
    }

    public boolean isReducedTax() {
        return reducedTax;
    }

    public void setReducedTax(boolean reducedTax) {
        this.reducedTax = reducedTax;
    }

    public boolean isTaxFree() {
        return taxFree;
    }

    public void setTaxFree(boolean taxFree) {
        this.taxFree = taxFree;
    }

    public Long getTaxFreeId() {
        return taxFreeId;
    }

    public void setTaxFreeId(Long taxFreeId) {
        this.taxFreeId = taxFreeId;
    }

    public boolean isTaxPayment() {
        return taxPayment;
    }

    public void setTaxPayment(boolean taxPayment) {
        this.taxPayment = taxPayment;
    }

    public Integer getOrderId() {
        return orderId;
    }

    protected void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    protected void setBillingClass(BillingClass billingClass) {
        this.billingClass = billingClass;
    }

    public boolean isSalary() {
        if (isSet(getResourceKey()) && getResourceKey().startsWith("salary")) {
            return true;
        }
        return false;
    }

    public boolean isNonSalary() {
        if (isSet(getResourceKey()) && !isSalary() 
                && (getResourceKey().indexOf("alary") > 1)) {
            return true;
        }
        return false;
    }
    
    
}
