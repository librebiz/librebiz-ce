<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="taxReports" value="${requestScope.taxReportList}" />
<c:set var="taxReportCreate" value="${requestScope.taxReportCreateUrl}" />
<c:set var="taxReportExit" value="${requestScope.taxReportExitTarget}" />
<c:set var="taxReportSelect" value="${requestScope.taxReportSelectUrl}" />


<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="taxReports" />
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="reportName"><fmt:message key="taxReportLabel" /></th>
                    <th class="reportDate"><fmt:message key="fromDate" /></th>
                    <th class="reportDate"><fmt:message key="til" /></th>
                    <th class="reportStatus"><fmt:message key="status" /></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty taxReports}">
                        <tr>
                            <td colspan="4"><c:choose>
                                    <c:when test="${!empty taxReportExit}">
                                        <v:link url="${taxReportCreate}?exit=${taxReportExit}">
                                            <fmt:message key="taxReportListEmpty" />
                                        </v:link>
                                    </c:when>
                                    <c:otherwise>
                                        <v:link url="${taxReportCreate}" title="taxReportNew">
                                            <fmt:message key="taxReportListEmpty" />
                                        </v:link>
                                    </c:otherwise>
                                </c:choose></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="obj" items="${taxReports}">
                            <tr>
                                <td class="reportName"><c:choose>
                                        <c:when test="${!empty taxReportExit}">
                                            <v:link url="${taxReportSelect}?id=${obj.id}&exit=${taxReportExit}">
                                                <o:out value="${obj.name}" />
                                            </v:link>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="${taxReportSelect}?id=${obj.id}">
                                                <o:out value="${obj.name}" />
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose></td>
                                <td class="reportDate"><o:date value="${obj.startDate}" /></td>
                                <td class="reportDate"><o:date value="${obj.stopDate}" /></td>
                                <td class="reportStatus"><c:choose>
                                        <c:when test="${obj.closed}">
                                            <fmt:message key="closed" />
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="inProgress" />
                                        </c:otherwise>
                                    </c:choose></td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>