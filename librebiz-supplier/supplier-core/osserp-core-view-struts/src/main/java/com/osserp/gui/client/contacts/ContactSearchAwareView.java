/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 25, 2010 2:18:08 PM 
 * 
 */
package com.osserp.gui.client.contacts;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.web.Form;
import com.osserp.common.web.SearchByMethodView;
import com.osserp.core.contacts.Contact;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface ContactSearchAwareView extends SearchByMethodView {

    public Contact getSelectedContact(Long id);

    /**
     * Performs an product search
     * @param form with search values
     * @throws ClientException if result expected is true and search returns empty list
     */
    public void search(Form form) throws ClientException;

    /**
     * Indicates if implementing search provides search by contact itself or search via a contact relation such as employee, customer, etc.
     * @return relatedContactView
     */
    public boolean isRelatedContactView();

    /**
     * Returns options of ContactTypes
     * @return contactTypes
     */
    public List<? extends Option> getContactTypes();

    /**
     * Returns the ContactTypeId
     * @return contactTypeId
     */
    public Long getContactTypeId();

    /**
     * Returns options of ContactGroups
     * @return contactGroups
     */
    public List<? extends Option> getContactGroups();

    /**
     * Returns the current ContactGroupId
     * @return contactGroupId
     */
    public Long getContactGroupId();

    /**
     * @return true if type selection is enabled
     */
    public boolean isEnableTypeSelection();

    /**
     * @param sets wether type selection is enabled
     */
    public void setEnableTypeSelection(boolean enableTypeSelection);
}
