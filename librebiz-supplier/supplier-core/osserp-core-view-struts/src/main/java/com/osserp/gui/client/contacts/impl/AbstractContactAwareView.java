/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 6, 2007 6:50:48 AM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactManager;
import com.osserp.gui.client.contacts.ContactAwareView;
import com.osserp.gui.client.contacts.ContactView;
import com.osserp.gui.client.impl.AbstractView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractContactAwareView extends AbstractView implements ContactAwareView {
    private static Logger log = LoggerFactory.getLogger(AbstractContactAwareView.class.getName());
    private String contactViewName = null;
    private ContactView contactView = null;

    protected AbstractContactAwareView() {
        super();
    }

    protected AbstractContactAwareView(String contactViewName) {
        super();
        this.contactViewName = contactViewName;
    }

    public boolean isContactViewAvailable() {
        return contactView != null;
    }

    public ContactView getContactView() {
        return contactView;
    }

    /**
     * Looks up for a contact view in session scope. View name may set by request param or attribute 'viewName' or by setting the property contactViewName in
     * the constructor of the view class (in that order).
     * @param request
     */
    @Override
    public void init(HttpServletRequest request) {
        String viewNameParam = request.getParameter("viewName");
        if (viewNameParam == null) {
            Object obj = request.getAttribute("viewName");
            if (obj != null && obj instanceof String) {
                viewNameParam = (String) obj;
            }
        }
        HttpSession session = request.getSession();
        if (viewNameParam != null) {
            Object obj = session.getAttribute(viewNameParam);
            if (obj != null && obj instanceof ContactView) {
                this.contactView = (ContactView) obj;
                if (log.isDebugEnabled()) {
                    log.debug("init() contact view bound by request param or attribute");
                }
            }
        }
        if (contactView == null && contactViewName != null) {
            this.contactView = (ContactView) session.getAttribute(contactViewName);
            if (contactView != null && log.isDebugEnabled()) {
                log.debug("init() contact view bound by view name property");
            }
        }
    }

    /**
     * Fetches selected contact of associated contact view if exists
     * @return contact or null if not bound
     */
    protected Contact fetchContact() {
        return ((contactView == null || contactView.getBean() == null) ? null : ((Contact) contactView.getBean()));
    }

    /**
     * Provides contact of associated contact view.
     * @return contact
     * @throws ActionException if contact not bound
     */
    protected Contact getContact() throws ActionException {
        Contact c = fetchContact();
        if (c == null) {
            throw new ActionException(ActionException.NO_OBJECT_BOUND, Contact.class);
        }
        return c;
    }

    /**
     * Provides common contact manager
     * @return contactManager
     */
    protected ContactManager getContactManager() {
        return (ContactManager) getService(ContactManager.class.getName());
    }

    /**
     * Refreshs associated contact view if available
     */
    protected void refresh() {
        if (contactView != null) {
            this.contactView.refresh();
        }
    }
}
