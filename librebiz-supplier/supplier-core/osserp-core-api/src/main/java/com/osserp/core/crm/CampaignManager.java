/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2011 5:17:21 PM 
 * 
 */
package com.osserp.core.crm;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.employees.Employee;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CampaignManager {

    /**
     * Creates a new campaign group
     * @param user
     * @param name
     */
    void createGroup(Employee user, String name);

    /**
     * Provides all activated campaign groups
     * @return groups
     */
    List<CampaignGroup> findGroups();

    /**
     * Creates a new campaign type
     * @param user
     * @param name
     */
    void createType(Employee user, String name);

    /**
     * Provides all activated campaign types
     * @return types
     */
    List<CampaignType> findTypes();

    /**
     * Provides all top level campaigns (including eol)
     * @return all
     */
    List<Campaign> find();

    /**
     * Provides all sub campaigns (including eol)
     * @param parent id of parent campaign
     * @return all
     */
    List<Campaign> find(Long parent);

    /**
     * Provides all selectable campaigns (excluding eol and parents)
     * @return selectable
     */
    List<Campaign> findSelectable();

    /**
     * Provides campaigns selected in open requests and related to user branch.
     * This selection provides a selection useful for queries by campaign.
     * @return campaigns
     */
    List<Campaign> findSelected(DomainUser user);

    /**
     * Provides available parents
     * @param campaign
     * @return campaigns all active campaigns excluding current parent and campaign itself
     */
    List<Option> getAvailableParents(Campaign campaign);

    /**
     * Loads campaign by id
     * @param id campaign id
     * @return campaign
     */
    Campaign getCampaign(Long id);

    /**
     * Creates a new top level campaign
     * @param user
     * @param name
     * @param reach
     * @param description
     * @param parent
     * @param company
     * @param branch
     * @param campaignGroup
     * @param campaignType
     * @param startDate
     * @param endDate
     * @return
     */
    Campaign create(
            Employee user,
            String name,
            String reach,
            String description,
            Campaign parent,
            Long company,
            Long branch,
            CampaignGroup campaignGroup,
            CampaignType campaignType,
            Date startDate,
            Date endDate);

    /**
     * Indicates if campaign is referenced by salesRequest
     * @param campaign
     * @return true if referenced
     */
    boolean isReferenced(Campaign campaign);

    /**
     * Deletes a campaign if not already referenced
     * @param campaign
     * @throws ClientException if campaign is already referenced
     */
    void delete(Campaign campaign) throws ClientException;

    /**
     * Updates a campaign
     * @param campaign
     */
    void update(Campaign campaign);

    /**
     * Updates the parent reference of a campaign
     * @param campaign
     * @param id
     * @return campaign with updated values
     */
    Campaign updateParent(Campaign campaign, Long id);
}
