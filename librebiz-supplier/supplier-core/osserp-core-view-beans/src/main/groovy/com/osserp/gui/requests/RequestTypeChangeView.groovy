/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.requests

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.core.BusinessCase
import com.osserp.core.BusinessType
import com.osserp.core.BusinessTypeManager
import com.osserp.core.requests.Request
import com.osserp.core.requests.RequestFcsAction
import com.osserp.core.requests.RequestManager
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RequestTypeChangeView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(RequestTypeChangeView.class.getName())

    private List<BusinessType> requestTypes = []
    String changeTarget
    RequestFcsAction fcsAction

    RequestTypeChangeView() {
        super()
        dependencies = ['businessCaseView', 'requestFcsView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        
        if (!env.businessCaseView) {
            throw new ViewContextException('businessCaseView not bound')
        }

        if (forwardRequest) {
            changeTarget = form.getString('target')
            requestTypes = businessTypeManager.createTypeSelection(domainUser, true)
            logger.debug("initRequest: done [requestTypeCount=${requestTypes.size()}, changeTarget=${changeTarget}, fcsAction=${fcsAction?.id}]")
        }
    }
    
    @Override
    void save() {
        Long id = form.getLong('id')
        Request request = getBusinessCase()
        BusinessType currentType = request.getType()
        BusinessType newType = requestTypes.find { it.id == id }
        Request transfered = requestManager.transferRequest(user, request, newType)
        env.businessCaseView.reload()
        
        // check if transfer was invoked by fcs action and perform this
        RequestFcsView requestFcsView = env.requestFcsView 
        if (requestFcsView?.selectedAction) {
            RequestFcsAction action = requestFcsView.selectedAction
            if (action.isTransfering()) {
                try {
                    requestFcsView.addTransferAction("${currentType.name} => ${newType.name}")
                } catch (Exception e) {
                    logger.debug("save: failed on attempt to add corresponding fcs action [request=${request?.primaryKey}, message=${e.message}]", e)
                }
            }
        }
        logger.debug("save: requestType changed [request=${request?.primaryKey}, type=${request?.type.id}]")
    }

    List<BusinessType> getTransferTypes() {
        BusinessCase bc = getBusinessCase()
        List result = []
        requestTypes.each {
            if (it.id != bc.type.id 
                && bc.type.company == it.company
                && domainUser.isSupportingType(it)) {
                result << it
            }
        }
        result
    }
    
    BusinessCase getBusinessCase() {
        env.businessCaseView.request
    }

    protected BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }
    
    protected RequestManager getRequestManager() {
        getService(RequestManager.class.getName())
    }

}
