<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<o:resource />
<o:logger write="page invoked" level="debug"/>
<html>
	<head>
		<title><fmt:message key="sessionTerminated"/></title>
	</head>
	<body onLoad="self.close();">
		<div style="margin-left: 450; margin-top: 250px;">
			<h4><a href="javascript:self.close();"><fmt:message key="loggedOut"/>!</a></h4>
		</div>
	</body>
</html>
