/**
 *
 * Copyright (C) 2016, 2023 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 * Created on Nov 23, 2023
 * 
 */
package com.osserp.core.service.impl;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dms.DmsConfig;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.util.FileUtil;

import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.service.records.RecordPrintService;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public abstract class ExportArchiveService extends AbstractCoreService {
    private static Logger log = LoggerFactory.getLogger(ExportArchiveService.class.getName());

    private RecordSearch recordSearch;
    private RecordPrintService recordPrintService;
    private DmsManager dmsManager;

    public ExportArchiveService(
            OptionsCache optionsCache,
            DmsManager dmsManager,
            RecordSearch recordSearch,
            RecordPrintService recordPrintService) {
        super(optionsCache);
        this.recordPrintService = recordPrintService;
        this.recordSearch = recordSearch;
        this.dmsManager = dmsManager;
    }

    protected String getDmsExportRoot() {
        DmsConfig dmsConfig = dmsManager.getDmsConfig();
        return dmsConfig.getRoot() + "/exports";
    }

    protected File createDirectory(String directory) throws ClientException {
        File dir = new File(directory);
        dir.mkdirs();
        if (!dir.exists() || !dir.canWrite()) {
            log.warn("createDirectory: Target not exists or not writable [directory=" + directory + "]");
            throw new ClientException(ErrorCode.DIRECTORY_ACCESS);
        }
        return dir;
    }

    protected void exportVoucher(String directory, Long voucherId, Long voucherType) {
        Record record = recordSearch.findRecord(voucherId, voucherType);
        if (record != null) {
            DocumentData doc = null;
            if (record.getType().getDocumentTypeId() != null) {
                DmsDocument dmsDocument = dmsManager.findReferenced(
                        new DmsReference(record.getType().getDocumentTypeId(), record.getId()));
                if (dmsDocument != null) {
                    doc = dmsManager.getDocumentData(dmsDocument);
                }
            }
            if (doc == null && record.isUnchangeable()) {
                doc = recordPrintService.getPdf(record);
            }
            if (doc != null) {
                write(doc, directory);
            }
        } else {
            log.warn("exportVoucher: Did not find corresponding record [summary="
                    + voucherId + ", type=" + voucherType + "]");
        }
    }

    protected boolean write(DocumentData data, String directory) {
        if (data != null) {
            String fileName = null;
            try {
                fileName = directory + "/" + data.getRealFilename();
                FileUtil.persist(data.getBytes(), fileName);
                return true;
            } catch (Exception e) {
                log.warn("write: Failed to add [file=" + fileName 
                        + ", message=" + e.getMessage() + "]", e);
            }
        }
        return false;
    }

}
