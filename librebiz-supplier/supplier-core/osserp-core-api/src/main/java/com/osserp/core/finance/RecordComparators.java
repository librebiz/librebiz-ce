/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 19, 2014 
 * 
 */
package com.osserp.core.finance;

import java.util.Comparator;
import java.util.Date;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordComparators {

    /**
     * Compares objects of type <code>com.osserp.core.finance.RecordDisplay</code> 
     * @param reverse
     * @return Comparator by recordDisplay.created
     */
    public static Comparator createRecordDisplayByCreatedComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RecordDisplay recordA = (RecordDisplay) a;
                    RecordDisplay recordB = (RecordDisplay) b;
                    Date dateA = recordA.getCreated();
                    Date dateB = recordB.getCreated();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RecordDisplay recordA = (RecordDisplay) a;
                RecordDisplay recordB = (RecordDisplay) b;
                Date dateA = recordA.getCreated();
                Date dateB = recordB.getCreated();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    /**
     * Compares objects of type <code>com.osserp.core.finance.RecordExport</code> 
     * @param reverse
     * @return Comparator by recordExport.created
     */
    public static Comparator createExportViewByCreatedComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RecordExport recordA = (RecordExport) a;
                    RecordExport recordB = (RecordExport) b;
                    Date dateA = recordA.getCreated();
                    Date dateB = recordB.getCreated();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RecordExport recordA = (RecordExport) a;
                RecordExport recordB = (RecordExport) b;
                Date dateA = recordA.getCreated();
                Date dateB = recordB.getCreated();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    /**
     * Compares objects of type <code>com.osserp.core.finance.PaymentDisplay</code> 
     * @param reverse
     * @return Comparator by paymentDisplay.paidOn
     */
    public static Comparator createPaymentDisplayByPaidComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    PaymentDisplay recordA = (PaymentDisplay) a;
                    PaymentDisplay recordB = (PaymentDisplay) b;
                    Date dateA = recordA.getPaidOn();
                    Date dateB = recordB.getPaidOn();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                PaymentDisplay recordA = (PaymentDisplay) a;
                PaymentDisplay recordB = (PaymentDisplay) b;
                Date dateA = recordA.getPaidOn();
                Date dateB = recordB.getPaidOn();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

}
