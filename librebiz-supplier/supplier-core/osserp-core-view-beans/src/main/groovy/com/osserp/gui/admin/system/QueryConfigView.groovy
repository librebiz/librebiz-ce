/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 11.05.2011 09:42:43 
 * 
 */
package com.osserp.gui.admin.system

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.dao.QueryConfigurator
import com.osserp.groovy.web.MenuItem
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
public class QueryConfigView extends CoreView {

    private final String EDIT_PERMISSIONS = 'query_config'

    QueryConfigView() {
        providesEditMode()
        providesCreateMode()
        permissions = 'query_config,query_config_display'
        headerName = 'sqlQuery'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            list = queryConfigurator.findAll()
            actualSortKey = 'name'
        }
    }

    @Override
    void enableEditMode() {
        if (isPermissionGrant(EDIT_PERMISSIONS)) {
            super.enableEditMode()
        }
    }

    @Override
    void enableCreateMode() {
        if (isPermissionGrant(EDIT_PERMISSIONS)) {
            super.enableCreateMode()
        }
    }

    @Override
    void createCustomNavigation() {
        nav.beanListNavigation = [selectExitLink, editLink, toggleActivationLink, homeLink]
    }

    @Override
    MenuItem getEditLink() {
        MenuItem link = super.editLink
        link.permissions = EDIT_PERMISSIONS
        link.permissionInfo = 'permissionQueryConfig'
        return link
    }

    @Override
    MenuItem getCreateLink() {
        MenuItem link = super.createLink
        link.permissions = EDIT_PERMISSIONS
        link.permissionInfo = 'permissionQueryConfig'
        return link
    }

    MenuItem getToggleActivationLink() {
        MenuItem link = null
        if (bean?.deleted) {
            link = navigationLink("/${context.name}/toggleActivation", 'enabledIcon', 'activate')
        } else {
            link = navigationLink("/${context.name}/toggleActivation", 'disabledIcon', 'deactivate')
        }
        link?.permissions = EDIT_PERMISSIONS
        link?.permissionInfo = 'permissionQueryConfig'
        return link
    }

    @Override
    void select() {
        super.select()
        createCustomNavigation()
    }

    @Override
    void reload() {
        list = queryConfigurator.findAll()
        actualSortKey = 'name'
        if (bean) {
            bean = list.find { it.id == bean.id }
        }
    }

    @Override
    void save() {
        String name = form.getString('name')
        String title = form.getString('title')
        String query = form.getString('query')
        if (!name || !title || !query) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }
        if (createMode) {
            bean = queryConfigurator.create(name, title, query)
        } else if (editMode) {
            if (!name == bean.name) {
                //name has changed, check that name is not used by another query
                List existing = queryConfigurator.findAll()
                if (existing.any { it.name == name }) {
                    throw new ClientException(ErrorCode.NAME_EXISTS)
                }
            }
            bean.update(name, title, query)
            queryConfigurator.save(bean)
        }
        disableAllModes()
        reload()
    }

    void clearParameters() {
        bean.clearParameters()
        queryConfigurator.save(bean)
        reload()
    }

    void clearOutput() {
        bean.clearOutput()
        queryConfigurator.save(bean)
        reload()
    }

    void toggleActivation() {
        if (bean.deleted) {
            bean.setDeleted(false)
        } else {
            bean.setDeleted(true)
        }
        queryConfigurator.save(bean)
        reload()
        createCustomNavigation()
    }

    void toggleFullscreen() {
        bean.switchFullscreen()
        queryConfigurator.save(bean)
        reload()
    }

    private QueryConfigurator getQueryConfigurator() {
        getService(QueryConfigurator.class.getName())
    }
}
