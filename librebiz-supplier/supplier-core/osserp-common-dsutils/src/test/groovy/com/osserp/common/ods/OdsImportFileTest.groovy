package com.osserp.common.ods

import static org.junit.Assert.*

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.junit.Test

import com.osserp.common.ds.ImportFile

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class OdsImportFileTest {
    private static Logger logger = LoggerFactory.getLogger(OdsImportFileTest.class.getName())

    private static final String FILE = './src/test/files/payments-no-header.ods'
    
    @Test
    void testConstructor() {
        if (testFileExists) {
            ImportFile importFile = createImportFile()
            assertNotNull(importFile.filename)
        }
    }
    
    @Test
    void testVirtualColumnNames() {
        if (testFileExists) {
            ImportFile importFile = createImportFile()
            assertFalse(importFile.columns.empty)
            assertTrue(importFile.columns.get(0) == 'c1')
        }
    }

    @Test
    void testHasNext() {
        if (testFileExists) {
            ImportFile importFile = createImportFile()
            int currentRow = 0
            while (currentRow < importFile.getRowCount()) {
                assertTrue(importFile.hasNext())
                importFile.getNextRow()
                currentRow++
            }
            assertFalse(importFile.hasNext())
        }
    }

    @Test
    void testIteration() {
        if (testFileExists) {
            ImportFile importFile = createImportFile()
            int currentRow = 0
            while (importFile.hasNext()) {
                importFile.getNextRow()
                currentRow++
            }
            assertTrue(currentRow == importFile.getRowCount())
        }
    }

    private OdsImportFile createImportFile() {
        OdsImportFile importFile = null
        try {
            File file = new File(FILE)
            if (file.exists() && file.canRead()) {
                importFile = new OdsImportFile(file, [ providesHeader: false ])
            } else {
            }
        } catch (Exception e) {
            logger.error("Create importfile failed: ${e.message}", e)
            // we ignore this
        }
        return importFile
    }

    private boolean getTestFileExists() {
        File file = new File(FILE)
        if (!file.exists()) {
            logger.warn("Testfile not exists: ${FILE}")
            return false
        }
        return true
    }
}
