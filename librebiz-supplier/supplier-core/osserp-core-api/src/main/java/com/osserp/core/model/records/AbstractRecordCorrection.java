/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 12, 2008 2:43:23 PM 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.util.DateFormatter;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordCorrection;
import com.osserp.core.model.AddressImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecordCorrection extends AddressImpl
        implements RecordCorrection {

    private Long status = Record.STAT_NEW;

    protected AbstractRecordCorrection() {
        super();
    }

    protected AbstractRecordCorrection(
            PaymentAwareRecord record,
            Employee user,
            String name,
            String note,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country) {
        super(
                null, //id,
                record.getId(), // reference
                AddressImpl.INVOICE, // addressType
                name,
                note,
                street,
                streetAddon,
                zipcode,
                city,
                ((country == null || country == 0) ? Constants.GERMANY : country),
                null, // federalState,
                null, // federalStateName,
                null, // district,
                false, // primarySet,
                new Date(System.currentTimeMillis()), //created,
                (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()),
                null, //changed,
                null); //changedBy);
        setId(createId(record));
    }

    private Long createId(PaymentAwareRecord record) {
        StringBuilder buffer = new StringBuilder(record.getId().toString());
        buffer.append(100 + (record.getCorrections().size() + 1));
        return Long.valueOf(buffer.toString());
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public boolean isUnchangeable() {
        return (status == null ? false : status >= Record.STAT_SENT);
    }

    public Element getXml() {
        Element root = new Element("correction");
        root.addContent(new Element("id").setText(getId().toString()));
        if (getChanged() != null && getChanged().after(getCreated())) {
            root.addContent(new Element("created").setText(DateFormatter.getDate(getChanged())));
        } else {
            root.addContent(new Element("created").setText(DateFormatter.getDate(getCreated())));
        }
        Element address = new Element("address");
        address.addContent(new Element("name").setText(getName()));
        address.addContent(new Element("street").setText(getStreet()));
        StringBuilder buffer = new StringBuilder(96);
        buffer.append(getZipcode()).append(" ").append(getCity());
        address.addContent(new Element("city").setText(buffer.toString()));
        if (getCountry() != null && getCountry() != 0) {
            address.addContent(new Element("country").setText(getCountry().toString()));
        }
        root.addContent(address);
        return root;
    }
}
