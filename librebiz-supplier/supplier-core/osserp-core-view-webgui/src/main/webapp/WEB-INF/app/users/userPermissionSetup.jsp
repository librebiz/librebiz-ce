<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="userPermissionSetupView"/>
<c:set var="availablePermissions" value="${view.availablePermissions}"/>
<c:set var="currentPermissions" value="${view.currentPermissions}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="styles" type="string">
		<style type="text/css">
		</style>
	</tiles:put>
	<tiles:put name="headline">
		<fmt:message key="appAccess"/> - <o:out value="${view.bean.id}"/> - <o:out value="${view.bean.loginName}"/> 
        - <fmt:message key="status"/>: <c:choose><c:when test="${view.bean.active}"><fmt:message key="activated"/></c:when><c:otherwise><fmt:message key="deactivated"/></c:otherwise></c:choose> 
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area" id="userPermissionSetupContent">
            <div class="row">

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <fmt:message key="activatedPermissions"/>
                            </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <tbody>
                                    <c:forEach var="perm" items="${currentPermissions}" varStatus="s">
                                        <tr>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${perm.displayable}"><o:out value="${perm.description}"/></c:when>
                                                    <c:otherwise><span class="error"><o:out value="${perm.description}"/></span></c:otherwise>
                                                </c:choose>
                                            </td>
                                            <td class="icon center">
                                                <o:permission role="hrm,permission_grant,client_edit,executive" info="permissionEditPermissions">
                                                    <v:link url="/users/userPermissionSetup/revokePermission?name=${perm.permission}">
                                                        <o:img name="deleteIcon" styleClass="bigIcon" />
                                                    </v:link>
                                                </o:permission>
                                            </td>
                                            <td class="icon center">
                                                <v:ajaxLink linkId="enabled${perm.permission}" url="/users/userPermissions/forward?name=${perm.permission}">
                                                    <span title="<fmt:message key="displayMembers"/>"><o:img name="searchIcon" styleClass="bigIcon" /></span>
                                                </v:ajaxLink>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <fmt:message key="deactivatedPermissions"/>
                            </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <tbody>
                                    <c:forEach var="perm" items="${availablePermissions}" varStatus="s">
                                        <c:choose>
                                            <c:when test="${perm.displayable}">
                                                <tr>
                                                    <td><o:out value="${perm.description}"/></td>
                                                    <td class="icon center">
                                                        <o:permission role="hrm,permission_grant,client_edit,executive" info="permissionEditPermissions">
                                                            <v:link url="/users/userPermissionSetup/grantPermission?name=${perm.permission}">
                                                                <o:img name="enabledIcon" styleClass="bigIcon" />
                                                            </v:link>
                                                        </o:permission>
                                                    </td>
                                                    <td class="icon center">
                                                        <v:ajaxLink linkId="disabled${perm.permission}" url="/users/userPermissions/forward?name=${perm.permission}">
                                                            <span title="<fmt:message key="displayMembers"/>"><o:img name="searchIcon" styleClass="bigIcon" /></span>
                                                        </v:ajaxLink>
                                                    </td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <%-- only accounts with runtime_config permission are allowed to see and assign not-displayable permissions --%>
                                                <o:permission role="client_admin,runtime_config">
                                                    <tr>
                                                        <td><span class="error"><o:out value="${perm.description}"/></span></td>
                                                        <td class="icon center">
                                                            <v:link url="/users/userPermissionSetup/grantPermission?name=${perm.permission}">
                                                                <o:img name="enabledIcon" styleClass="bigIcon" />
                                                            </v:link>
                                                        </td>
                                                        <td class="icon center">
                                                            <v:ajaxLink linkId="disabled${perm.permission}" url="/users/userPermissions/forward?name=${perm.permission}">
                                                                <span title="<fmt:message key="displayMembers"/>"><o:img name="searchIcon" styleClass="bigIcon" /></span>
                                                            </v:ajaxLink>
                                                        </td>
                                                    </tr>
                                                </o:permission>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
			</div>
		</div>
	</tiles:put>
</tiles:insert>
