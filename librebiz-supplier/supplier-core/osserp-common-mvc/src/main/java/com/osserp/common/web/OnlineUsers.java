/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 13, 2004 
 * 
 */
package com.osserp.common.web;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OnlineUsers {

    private static Logger log = LoggerFactory.getLogger(OnlineUsers.class.getName());

    private static OnlineUsers factory;
    private Map<Long, User> users;
    private Map<String, Long> sessions;

    /**
     * private constructor for skeleton implementation
     */
    private OnlineUsers() {
        users = Collections.synchronizedMap(new ConcurrentHashMap<Long, User>());
        sessions = Collections.synchronizedMap(new ConcurrentHashMap<String, Long>());
    }

    /**
     * @return onlineUsers instance
     */
    public static OnlineUsers newInstance() {
        if (OnlineUsers.factory == null) {
            OnlineUsers.factory = new OnlineUsers();
        }
        return OnlineUsers.factory;
    }

    /**
     * looks for the user specified by the id.
     * @param id
     * @return true if online
     */
    public boolean isOnline(Long id) {
        return users.containsKey(id);
    }

    /**
     * Adds the user related to the given session
     * @param session id
     * @param user id
     */
    public void add(String session, User user) {
        user.setSessionId(session);
        users.put(user.getId(), user);
        sessions.put(session, user.getId());
        Set<String> sessionKeys = sessions.keySet();
        for (Iterator<String> i = sessionKeys.iterator(); i.hasNext();) {
            String nextKey = i.next();
            Long next = sessions.get(nextKey);
            if (next != null && next.equals(user.getId())
                    && !nextKey.equals(session)) {
                // clear previous sessions of the same user
                sessions.remove(nextKey);
            }
        }
        if (log.isInfoEnabled()) {
            log.info("user logged in [id="
                    + user.getId()
                    + ", session="
                    + session
                    + ", ip="
                    + user.getRemoteIp()
                    + "]");
        }
    }

    /**
     * Removes the given session id and corresponding user
     * @param user 
     */
    public void remove(User user) {
        Set<Long> userKeys = users.keySet();
        for (Iterator<Long> i = userKeys.iterator(); i.hasNext();) {
            Long next = i.next();
            if (next.equals(user.getId())) {
                users.remove(next);
                break;
            }
        }
        Set<String> sessionKeys = sessions.keySet();
        for (Iterator<String> i = sessionKeys.iterator(); i.hasNext();) {
            String nextKey = i.next();
            Long next = sessions.get(nextKey);
            if (next != null && next.equals(user.getId())) {
                sessions.remove(nextKey);
            }
        }
        if (log.isInfoEnabled()) {
            log.info("user logged out [id=" + user.getId() + ", session="
                    + user.getSessionId() + ", ip=" + user.getRemoteIp() + "]");
        }
    }

    /**
     * A collection of all users currently online
     * @return of user ids
     */
    public Collection getUsers() {
        return users.values();
    }

    /**
     * The count of users currently online
     * @return count
     */
    public int getCount() {
        return users.size();
    }
}
