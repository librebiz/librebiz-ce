/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Apr-2005 08:09:00 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationSearchResult;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Calculations extends ItemLists {

    /**
     * Creates a new calculation
     * @param user
     * @param referenceId
     * @param contextName
     * @param referenceCreated
     * @param requestType
     * @param name
     * @param calculatorName
     * @param salesPriceLocked
     * @param minimalMargin
     * @param targetMargin
     * @return calculation
     * @throws ClientException
     */
    Calculation create(
            Employee user,
            Long referenceId,
            String contextName,
            Date referenceCreated,
            BusinessType requestType,
            String name,
            String calculatorName,
            boolean salesPriceLocked,
            Double minimalMargin,
            Double targetMargin)
            throws ClientException;

    /**
     * Copies content of source to target
     * @param businessType
     * @param targetObj
     * @param source
     * @return target with items provided by source
     */
    Calculation copy(BusinessType businessType, Calculation targetObj, Calculation source);

    /**
     * Creates option positions if none exist
     * @param calculation
     */
    void createOptions(Calculation calculation);

    /**
     * Provides a list of all calculations of a reference
     * @param reference
     * @param context
     * @return list of calculation entries
     */
    List<CalculationSearchResult> findByReference(Long reference, String context);

    /**
     * Indicates if product is matching an assigned productSelection. 
     * @param businessCase
     * @param position
     * @param product
     * @return true if product is matching an productSelection referenced to
     * calculationConfigGroup referenced by position.groupId. 
     */
    boolean isMatching(BusinessCase businessCase, ItemPosition position, Product product);
}
