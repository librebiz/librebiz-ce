/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 10, 2013
 *  
 */
package com.osserp.common.directory.ldap.impl

import org.junit.Test

import static org.junit.Assert.*

import com.osserp.TestConfig
import com.osserp.common.directory.ldap.impl.AdminAccountsImpl
import com.osserp.common.directory.ldap.LdapConfig

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class UAbstractLdapConfigAware {

    @Test
    void getObjectClasses() {
        TStructuralSchema source = new TStructuralSchema()
        assertTrue('[top, posixAccount, inetOrgPerson, person]' == source.objectClasses.toString())
    }

    @Test
    void addObjectClasses() {
        TStructuralSchema source = new TStructuralSchema()
        TStructuralSchema target = new TStructuralSchema()
        target.testObjectClasses = ['top', 'posixAccount', 'osserpUser', 'dbmailUser']
        TAbstractLdapConfigAware object = new TAbstractLdapConfigAware()
        Map ldapObject = [objectclass:source.objectClasses]
        ldapObject = object.addObjectClasses(target, ldapObject)
        assertTrue('[top, posixAccount, inetOrgPerson, person, osserpUser, dbmailUser]' == ldapObject.objectclass.toString())
    }

    @Test
    void schemaActivated() {
        TStructuralSchema source = new TStructuralSchema()
        source.testObjectClasses = ['dbmailUser']
        Map values = [objectclass: ['top', 'posixAccount', 'osserpUser', 'dbmailUser']]
        TAbstractLdapConfigAware object = new TAbstractLdapConfigAware()
        assertTrue(object.schemaActivated(source, values))
    }

    @Test
    void schemaNotActivated() {
        TStructuralSchema source = new TStructuralSchema()
        source.testObjectClasses = ['zarafaUser']
        Map values = [objectclass: ['top', 'posixAccount', 'osserpUser', 'dbmailUser']]
        TAbstractLdapConfigAware object = new TAbstractLdapConfigAware()
        assertTrue(!object.schemaActivated(source, values))
    }
}
