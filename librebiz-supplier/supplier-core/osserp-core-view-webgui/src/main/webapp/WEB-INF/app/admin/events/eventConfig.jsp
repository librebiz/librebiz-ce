<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="eventConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="javascript" type="string">
	<script type="text/javascript">

	function checkRecipient(value) {
		if (value == 1 || value == 2) {
			poolDisplayState = false;
			hideElement('pool');
			hideElement('poolSettings');
		} else {
			poolDisplayState = true;
			showElement('pool');
			showElement('poolSettings');
		}
	}
	
	var dayInMillis = 86400000;
		
	function calculateMsByDays(days) {
	  if (isNaN(days)) {
	    return '';
	  } else {
	    return dayInMillis * days;
	  }
	}

	function setMsDisplay(days, elementName) {
	  if (isNaN(days)) {
	    alert(ojsI18n.illegalDayValue + days);
	  } else {
	    document.getElementById(elementName).firstChild.nodeValue = calculateMsByDays(days);
	  }
	}

	function setMsEdit(days, formName, elementName) {
	  if (isNaN(days)) {
	    alert(ojsI18n.illegalDayValue + days);
	  } else {
	    document.forms[formName].elements[elementName].value = calculateMsByDays(days);
	  }
	}

	</script>
</tiles:put>
	<tiles:put name="headline">
		<c:choose>
			<c:when test="${!view.eventTypeSelected}">
				<fmt:message key="jobConfiguration"/> - <fmt:message key="selectionEventType"/>
			</c:when>
			<c:when test="${!empty view.requestTypeId}">
				<fmt:message key="configuredJobsTo"/> <o:out value="${view.eventType.name}"/> - <oc:options name="requestTypes" value="${view.requestTypeId}"/>
			</c:when>
			<c:otherwise>
				<fmt:message key="configuredJobsTo"/> <o:out value="${view.eventType.name}"/>
			</c:otherwise>
		</c:choose>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string">
        <div class="content-area" id="eventConfigContent">
            <div class="row">
				<c:choose>
					<c:when test="${!view.eventTypeSelected}">
                        <div class="col-lg-12 panel-area">
                            <c:import url="_eventConfigTypeSelection.jsp"/>
                        </div>
					</c:when>
					<c:when test="${view.editMode}">
						<c:import url="_eventConfigEdit.jsp"/>
					</c:when>
					<c:when test="${view.createMode}">
						<c:import url="_eventConfigCreate.jsp"/>
					</c:when>
					<c:when test="${view.bean != null}">
						<c:import url="_eventConfigDisplay.jsp"/>
					</c:when>
					<c:otherwise>
                        <div class="col-lg-12 panel-area">
                            <c:import url="_eventConfigList.jsp"/>
                        </div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
