/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 30, 2013 
 * 
 */
package com.osserp.gui.calculations

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.NumberFormatter
import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException
import com.osserp.core.ItemListManager
import com.osserp.core.products.Product
import com.osserp.core.calc.Calculation
import com.osserp.core.calc.PartlistManager
import com.osserp.core.calc.Partlist
import com.osserp.core.calc.PartlistReference

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class PartlistView extends AbstractItemListView {
    private static Logger logger = LoggerFactory.getLogger(PartlistView.class.getName())

    Calculation calculation
    Long groupId
    Product product
    Long changeItemId

    PartlistView() {
        dependencies = ['calculationView']
        headerName = 'partlist.pageTitle'
        providesEditMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!env.calculationView?.calculation) {
            logger.warn("initRequest: not bound [user=${user?.id}, name=calculationView]")
            throw new ViewContextException(this)
        }

        if (!list && !bean) {
            calculation = env.calculationView.calculation

            def positionId = form.getLong('positionId')
            if (!positionId) {
                throw new IllegalStateException('view requires positionId param')
            }
            def position = calculation.getPosition(positionId)
            if (!position) {
                throw new IllegalStateException("no valid position found with positionId=$positionId")
            }

            groupId = position.groupId
            position.items.each { list << it }

            if (list.size() == 1) {
                product = list[0].product
                bean = partlistManager.findPartlist(calculation, product)
                if (!bean) {
                    bean = partlistManager.create(domainEmployee, calculation, product, groupId)
                }
                list = []
            }
        }
    }

    @Override
    protected void createCustomNavigation() {
        nav.editNavigation = [disableEditLink, calculateLink, homeLink]
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            product = bean.product
            bean = partlistManager.findPartlist(calculation, product)
            if (!bean) {
                bean = partlistManager.create(domainEmployee, calculation, product, groupId)
            }
        }
    }

    @Override
    void save() {
        if (editMode && itemChangeMode) {
            BigDecimal partnerPrice = form.getDecimal('partnerPrice')
            if (partnerPrice == null) {
                partnerPrice = new BigDecimal(0)
            }
            BigDecimal partnerPriceSource = form.getDecimal('partnerPriceSource')
            if (partnerPriceSource == null) {
                partnerPriceSource = new BigDecimal(0)
            }
            boolean partnerPriceChanged = !partnerPrice.equals(partnerPriceSource)
            Double quantity = form.getDouble('quantity')
            BigDecimal price = form.getDecimal('price')
            String note = form.getString('note')

            logger.debug('update() fetched partner prices [itemId=' + changeItemId
                    + ', partnerPriceChanged=' + partnerPriceChanged
                    + ', partnerPriceSource=' + partnerPriceSource
                    + ', partnerPriceInput=' + partnerPrice
                    + ']')

            if (changeItemId && quantity > 1) {

                itemListManager.updateItem(
                        bean,
                        changeItemId,
                        quantity,
                        partnerPrice,
                        partnerPriceChanged,
                        price,
                        note)
                bean = itemListManager.getList(bean.id)
            }
            disableItemChangeMode()
        } else if (editMode) {
            Long[] productIds = form.getIndexedLong('productId')
            String[] quantities = form.getIndexedStrings('quantity')
            String[] prices = form.getIndexedStrings('price')
            String[] notes = form.getIndexedStrings('note')
            Long selectedPosition = form.getLong('selectedPosition')
            Long selectedItem = form.getLong('selectedItem')

            if (selectedPosition) {
                if (selectedItem) {
                    bean.removeItem(selectedItem)
                }
                int length = productIds.length
                if (length == quantities.length && length == prices.length && length == notes.length) {
                    for (int i = 0; i < length; i++) {
                        Double quantity = NumberFormatter.createDouble(quantities[i], false)
                        if (quantity > 0) {
                            Product product = productManager.get(productIds[i])
                            logger.debug('save: add product to partlist [selectedPosition=' + selectedPosition +
                                    ', product=' + product.productId +
                                    ', quantity=' + quantity +
                                    ', price=' + NumberFormatter.createDecimal(prices[i], false) +
                                    ', note=' + notes[i] + ']')
                            itemListManager.addItem(
                                    bean,
                                    selectedPosition,
                                    product,
                                    quantity,
                                    NumberFormatter.createDecimal(prices[i], false),
                                    true,
                                    notes[i])
                            bean = itemListManager.getList(bean.id)
                        }
                    }
                }
            }
        }
    }

    void calculate() {
        if (bean) {
            partlistManager.save(domainUser, bean)
        }
        disableEditMode()
    }

    MenuItem getCalculateLink() {
        return navigationLink("/${context.name}/calculate", 'calcIcon', 'partlist.calculate')
    }

    void disableItemChangeMode() {
        changeItemId = null
        env.itemChangeMode = false
    }

    void enableItemChangeMode() {
        if (editMode) {
            changeItemId = form.getLong('itemId')
            if (changeItemId) {
                env.itemChangeMode = true
                nav.currentNavigation = [disableItemChangeLink, homeLink]
            }
        }
    }

    boolean isItemChangeMode() {
        env.itemChangeMode
    }

    MenuItem getDisableItemChangeLink() {
        return navigationLink("/${context.name}/disableItemChangeMode", 'backIcon', 'partlist.disableItemChangeMode')
    }

    @Override
    protected ItemListManager getItemListManager() {
        partlistManager
    }

    protected PartlistManager getPartlistManager() {
        getService(PartlistManager.class.getName())
    }
}
