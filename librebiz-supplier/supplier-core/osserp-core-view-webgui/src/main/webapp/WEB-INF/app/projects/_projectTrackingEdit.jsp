<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="projectTrackingView" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="projectTrackingSettings" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            
            <c:import url="${viewdir}/projects/_projectTrackingInputMain.jsp"/>
            
            <div class="row next">
                <div class="col-md-4"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="/projects/projectTracking/disableEditMode"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="save" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
