/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 2, 2010 1:15:59 PM 
 * 
 */
package com.osserp.core.telephone;

import java.util.List;

import com.osserp.common.ClientException;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneGroupManager {

    /**
     * Provides all telephoneGroups
     * @return telephoneGroups
     */
    List<TelephoneGroup> getAll();

    /**
     * Creates telephoneGroup
     * @param createdBy
     * @param name
     * @param telephoneSystemId
     * @return telephoneGroup
     */
    TelephoneGroup create(Long createdBy, String name, Long telephoneSystemId) throws ClientException;

    /**
     * Tries to update the name of a telephoneGroup with given id
     * @param changedBy
     * @param id
     * @param name
     */
    void updateName(Long changedBy, Long id, String name) throws ClientException;

    /**
     * Tries to find a telephoneGroup by id
     * @param id
     * @return telephoneGroup or null if not found
     */
    TelephoneGroup find(Long id);

    /**
     * Tries to find a telephoneGroup by TelephoneSystem
     * @param id
     * @return telephoneGroups or null if not found
     */
    List<TelephoneGroup> findByTelephoneSystem(Long id);

    /**
     * Tries to delete a telephoneGroup by id
     * @param id
     * @param changedBy
     */
    void delete(Long id, Long changedBy);
}
