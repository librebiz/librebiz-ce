/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.directory.DirectoryGroup
import com.osserp.common.directory.DirectoryPermission
import com.osserp.common.directory.DirectoryUser
import com.osserp.common.directory.ldap.LdapManager

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class AbstractDirectoryManager {
    private static Logger logger = LoggerFactory.getLogger(AbstractDirectoryManager.class.getName())

    protected LdapManager ldapManager

    protected AbstractDirectoryManager() {
        // required ldapManager must be set via public setter
    }

    protected AbstractDirectoryManager(LdapManager ldapManager) {
        this.ldapManager = ldapManager
    }

    Boolean isConnected() {
        ldapManager?.config?.domainAvailable
    }

    DirectoryUser loadUser(String dn) {
        createUserResult(ldapManager.lookupUserByDn(dn))
    }

    DirectoryGroup loadGroup(String dn) {
        createGroupResult(ldapManager.lookupGroupByDn(dn))
    }

    List<DirectoryGroup> findGroups(String client) {
        createGroupsResult(ldapManager.lookupGroups(client))
    }

    List<DirectoryPermission> findPermissions(String client) {
        createPermissionsResult(ldapManager.lookupPermissions(client))
    }

    List<DirectoryUser> findUsers(String client) {
        findUsersByCategory(DirectoryUser.BUSINESS_CATEGORY_EMPLOYEE, client)
    }

    List<DirectoryUser> findMailboxUsers(String client) {
        findUsersByCategory(DirectoryUser.BUSINESS_CATEGORY_MAILBOX, client)
    }
    
    List<DirectoryUser> findSubscribers(String client) {
        findUsersByCategory(DirectoryUser.BUSINESS_CATEGORY_SUBSCRIBER, client)
    }

    List<DirectoryUser> findSystemUsers(String client) {
        findUsersByCategory(DirectoryUser.BUSINESS_CATEGORY_SYSTEM, client)
    }

    /**
     * Provides the next unused uidnumber
     * @param businesscategory one of Employee, Mailbox, Subscriber or System
     * @param client optional name of the directory client
     */
    Map createDefaultUserAttributes(String osserpcategory, String client) {
        Map values = [
            uidnumber: nextUidNumber(osserpcategory, client),
            gidnumber: getDefaultGidNumber(osserpcategory, client)
            ]
        if (DirectoryUser.BUSINESS_CATEGORY_MAILBOX == osserpcategory) {
            values.cn = '...'
            values.osserpcategory = DirectoryUser.BUSINESS_CATEGORY_MAILBOX
            values.description = "${DirectoryUser.BUSINESS_CATEGORY_MAILBOX} ..."
            values.gecos = 'system'
            values.mail = '@osserp.com'
            values.mailboxUser = 'osxx....'
            values.sn = DirectoryUser.BUSINESS_CATEGORY_MAILBOX
            values.uid = 'mbox-...'
        } else if (DirectoryUser.BUSINESS_CATEGORY_SUBSCRIBER == osserpcategory) {
            values.osserpcategory = DirectoryUser.BUSINESS_CATEGORY_SUBSCRIBER
            values.sn = DirectoryUser.BUSINESS_CATEGORY_SUBSCRIBER
            values.cn = DirectoryUser.BUSINESS_CATEGORY_SUBSCRIBER
            if (values.uidnumber) {
                String gecos = "sub${values.uidnumber}"
            } else {
                values.gecos = 'sub'
            }
        } else if (DirectoryUser.BUSINESS_CATEGORY_EMPLOYEE == osserpcategory) {
            values.osserpcategory = DirectoryUser.BUSINESS_CATEGORY_EMPLOYEE
            values.gecos = 'osXXxxxx'
        } else {
            values.osserpcategory = DirectoryUser.BUSINESS_CATEGORY_SYSTEM
            values.gecos = 'System User'
        }
        values
    }
    
    protected String nextUidNumber(String osserpcategory, String client) {
        String min = ldapManager?.config?.userUidnumberMin
        String max = ldapManager?.config?.userUidnumberMax
        if (DirectoryUser.BUSINESS_CATEGORY_MAILBOX == osserpcategory) {
            if (ldapManager?.config?.userMailboxMin) {
                min = ldapManager?.config?.userMailboxMin
            }
            if (ldapManager?.config?.userMailboxMax) {
                min = ldapManager?.config?.userMailboxMax
            }
        } else if (DirectoryUser.BUSINESS_CATEGORY_SUBSCRIBER == osserpcategory) {
            if (ldapManager?.config?.userSubscriberMin) {
                min = ldapManager?.config?.userSubscriberMin
            }
            if (ldapManager?.config?.userSubscriberMax) {
                min = ldapManager?.config?.userSubscriberMax
            }
        }
        ldapManager.createUserIdSuggestion(min, max, client)
    }
    
    protected String getDefaultGidNumber(String osserpcategory, String client) {
        ldapManager?.config?.userGroupDefault
    }

    protected final DirectoryGroup createGroupResult(Map ldapEntry) {
        (ldapEntry?.dn ? new DirectoryGroup(values:ldapEntry) : null)
    }

    protected final List<DirectoryGroup> createGroupsResult(List<Map> list) {
        List<DirectoryGroup> result = []
        list.each {
            result << new DirectoryGroup(values:it)
        }
        result
    }

    protected final List<DirectoryPermission> createPermissionsResult(List<Map> list) {
        List<DirectoryPermission> result = []
        list.each {
            result << new DirectoryPermission(values:it)
        }
        result
    }

    protected final DirectoryUser createUserResult(Map ldapEntry) {
        (ldapEntry?.uid ? new DirectoryUser(values:ldapEntry) : null)
    }

    private List<DirectoryUser> findUsersByCategory(String osserpcategory, String client) {
        List<DirectoryUser> result = []
        List<Map> users = ldapManager.lookupUsers(client)
        users.each {
            if (it.osserpcategory == osserpcategory) {
                result << new DirectoryUser(values:it)
            }
        }
        result
    }
}

