/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16-Jan-2007 09:41:17 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.Comparators;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeDisplay;
import com.osserp.core.employees.EmployeeRoleDisplay;
import com.osserp.core.employees.EmployeeSearch;
import com.osserp.core.employees.EmployeeUtil;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.Permissions;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeSearchImpl extends AbstractContactRelationSearch implements EmployeeSearch {
    private static Logger log = LoggerFactory.getLogger(EmployeeSearchImpl.class.getName());

    // TODO make permission name more common
    private static final String IGNORE_BRANCH = "employeeSearchIgnoreBranch";
    private SystemConfigs systemConfigs;

    protected EmployeeSearchImpl(
            SystemConfigs systemConfigs,
            Contacts contactsDao,
            Employees employees) {
        super(contactsDao, employees);
        this.systemConfigs = systemConfigs;
    }

    public Employee findById(Long id) {
        try {
            return (Employee) find(id);
        } catch (ClientException e) {
            log.warn("findById() employee not found [id=" + id + "]");
            throw new ActionException(ErrorCode.EMPLOYEE_ID_INVALID);
        }
    }

    public List<Employee> findByGroup(DomainUser user, String groupKey) {
        List<Employee> list = getEmployeesDao().findByGroup(groupKey);
        return filterAndSort(user, list);
    }

    public List<Employee> findAllActive(DomainUser user) {
        List<Employee> list = getEmployeesDao().findActiveEmployees();
        return filterAndSort(user, list);
    }

    public List<Employee> findAllActive() {
        List<Employee> list = getEmployeesDao().findActiveEmployees();
        CollectionUtil.sort(list, Comparators.createEmployeeNameComparator(false));
        return list;
    }

    public List<EmployeeDisplay> findActive() {
        List<EmployeeDisplay> list = getEmployeesDao().findActive();
        CollectionUtil.sort(list, Comparators.createEmployeeDisplayNameComparator(false));
        return list;
    }

    public List<Option> findActiveNames() {
        List<Option> list = getEmployeesDao().findActiveNames();
        CollectionUtil.sort(list, Comparators.createOptionNameComparator(false));
        return list;
    }

    public Element getAll() {
        Element result = new Element("employees");
        List<Employee> employees = findAllActive();
        for (int i = 0, j = employees.size(); i < j; i++) {
            Employee e = employees.get(i);
            result.addContent(e.getXML());
        }
        return result;
    }

    public List<Option> findNames() {
        return getEmployeesDao().findNames();
    }

    public List<BusinessCaseRelationStat> findSalesCapacity(DomainUser user, Long employee) {
        Employees employeesDao = getEmployeesDao();
        List<BusinessCaseRelationStat> list = employeesDao.getSalesCapacity(employee);
        return employeesDao.filterBySupportedBranch(list, user);
    }

    public List<BusinessCaseRelationStat> findTecCapacity(DomainUser user, Long employee) {
        Employees employeesDao = getEmployeesDao();
        List<BusinessCaseRelationStat> list = employeesDao.getTecCapacity(employee);
        return employeesDao.filterBySupportedBranch(list, user);
    }

    public List<EmployeeRoleDisplay> getRoleDisplayByGroup(Long group) {
        List<EmployeeRoleDisplay> list = getEmployeesDao().getEmployeeRoleDisplay();
        List<EmployeeRoleDisplay> result = new ArrayList<EmployeeRoleDisplay>();
        Set<Long> added = new HashSet<Long>();
        if (group != null) {
            for (int i = 0, j = list.size(); i < j; i++) {
                EmployeeRoleDisplay next = list.get(i);
                if (group.equals(next.getGroupId()) && !added.contains(next.getEmployeeId())) {
                    result.add(next);
                    added.add(next.getEmployeeId());
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getRoleDisplayByGroup() done [count=" + result.size() + "]");
        }
        return result;
    }

    private List<Employee> filterAndSort(DomainUser user, List<Employee> list) {
        List<Employee> result = new ArrayList<Employee>();
        if (ignoreBranch() || user == null || user.isPermissionGrant(Permissions.BRANCH_IGNORE_PERMISSIONS)) {
            result = list;
            if (log.isDebugEnabled()) {
                if (user != null) {
                    log.debug("filterAndSort() user has required permissions to see all employees [id="
                            + user.getId() + ", employee=" + user.getEmployee().getId()
                            + ", permissions=" + StringUtil.createCommaSeparated(Permissions.BRANCH_IGNORE_PERMISSIONS) + "]");
                }
            }
        } else if (user.getEmployee().isCompanyExecutive()) {
            result = EmployeeUtil.createByCompanyExecutive(list, user.getEmployee());
            if (log.isDebugEnabled()) {
                log.debug("filterAndSort() user is company executive [id=" + user.getId()
                        + ", employee=" + user.getEmployee().getId() + "]");
            }
        } else {
            result = EmployeeUtil.createBySameBranch(list, user.getEmployee());
            if (log.isDebugEnabled()) {
                log.debug("filterAndSort() user is limited to branch [id=" + user.getId()
                        + ", employee=" + user.getEmployee().getId() + "]");
            }
        }
        CollectionUtil.sort(result, Comparators.createEmployeeNameComparator(false));
        return result;
    }

    private boolean ignoreBranch() {
        if (systemConfigs != null) {
            return systemConfigs.isSystemPropertyEnabled(IGNORE_BRANCH);
        }
        return false;
    }

    private Employees getEmployeesDao() {
        return (Employees) getContactRelationsDao();
    }
}
