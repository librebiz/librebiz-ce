/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2008 1:42:11 PM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.Form;

import com.osserp.core.BusinessNote;
import com.osserp.core.NoteManager;
import com.osserp.core.Options;
import com.osserp.core.contacts.ContactNoteManager;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.PersonManager;
import com.osserp.core.contacts.Salutation;

import com.osserp.gui.client.contacts.PersonView;
import com.osserp.gui.client.impl.AbstractView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPersonView extends AbstractView implements PersonView {

    private BusinessNote stickyNote;

    public void load(Long id) throws ClientException {
        PersonManager manager = getPersonManager();
        setBean(manager.find(id));
    }

    public void load(PersonView view) throws ClientException {
        if (view.getExitId() != null) {
            setExitId(view.getExitId());
        }
        if (view.getExitTarget() != null) {
            setExitTarget(view.getExitTarget());
        }
        if (view.getActionTarget() != null) {
            view.setActionTarget(view.getActionTarget());
        }
        if (view.getBean() != null && view.getBean() instanceof Person) {
            load((Person) view.getBean());
        }
    }

    public void load(Person person) throws ClientException {
        PersonManager manager = getPersonManager();
        setBean(manager.find(person.getPrimaryKey()));
        NoteManager noteManager = (NoteManager) getService(ContactNoteManager.class.getName());
        stickyNote = noteManager.getSticky(person.getPrimaryKey());
    }

    @Override
    public void reload() {
        try {
            if (getBean() instanceof Person) {
                load((Person) getBean());
            }
        } catch (ClientException e) {
            throw new ActionException(e.getMessage(), e);
        }
    }

    @Override
    public void setSelection(Long id) {
        if (id == null) {
            setBean(null);
        } else {
            PersonManager manager = getPersonManager();
            setBean(manager.find(id));
        }
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        Person contact = fetchPerson();
        if (contact != null) {
            Salutation salutation = getSalutation(form.getLong(Form.SALUTATION));
            Option title = getTitle(form.getLong(Form.TITLE));
            contact = getPersonManager().updatePerson(
                    getDomainUser(), 
                    contact, 
                    salutation, 
                    title, 
                    form.getBoolean(Form.FIRSTNAME_PREFIX), 
                    form.getString(Form.FIRSTNAME), 
                    form.getString(Form.LASTNAME), 
                    form.getString(Form.STREET), 
                    form.getString(Form.ZIPCODE), 
                    form.getString(Form.CITY), 
                    form.getLong(Form.FEDERAL_STATE_ID), 
                    form.getString(Form.FEDERAL_STATE_NAME), 
                    form.getLong(Form.COUNTRY_ID));
            setBean(contact);
        }
    }

    public Salutation getSalutation(Long id) {
        if (id == null) {
            id = Constants.LONG_NULL;
        }
        return (Salutation) getOption(Options.SALUTATIONS, id);
    }

    public Option getTitle(Long id) {
        return getOption(Options.TITLES, id);
    }

    public BusinessNote getStickyNote() {
        return stickyNote;
    }

    protected void setStickyNote(BusinessNote stickyNote) {
        this.stickyNote = stickyNote;
    }

    protected Person fetchPerson() {
        Object bean = getBean();
        if (bean != null && bean instanceof Person) {
            return (Person) bean;
        }
        return null;
    }

    public void refresh() {
        PersonManager manager = getPersonManager();
        if (manager != null && getBean() != null) {
            Person current = fetchPerson();
            Person updated = manager.find(current.getPrimaryKey());
            setBean(updated);
            if (getList() != null && !getList().isEmpty()) {
                CollectionUtil.replaceByProperty(getList(), "personId", updated);
            }
        }
    }

    protected abstract PersonManager getPersonManager();
}
