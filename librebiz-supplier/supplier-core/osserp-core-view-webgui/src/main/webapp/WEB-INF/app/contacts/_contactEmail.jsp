<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="contactEmailView"/>
<style type="text/css">
label {
    font-weight: normal;
}
.modalBoxData input[type="button"],
.modalBoxData input[type="submit"] {
    width: 110px;
}
.modalBoxData input[type="button"] {
   margin-right: 20px;
}
</style>
<c:if test="${!empty view}">
	<div class="modalBoxHeader">
		<div class="modalBoxHeaderLeft">
			<fmt:message key="email"/>
		</div>
		<div class="modalBoxHeaderRight">
			<div class="boxnav">
				<ul>
					<c:if test="${!view.createMode and !view.editMode}">
						<c:choose>
							<c:when test="${empty view.bean}">
								<li><v:ajaxLink url="/contacts/contactEmail/enableCreateMode" linkId="createLink" targetElement="contact_emails_popup"><o:img name="newIcon"/></v:ajaxLink></li>
							</c:when>
							<c:otherwise>
								<li><v:ajaxLink url="/contacts/contactEmail/select" linkId="exitLink" targetElement="contact_emails_popup"><o:img name="backIcon"/></v:ajaxLink></li>
								<li><v:ajaxLink url="/contacts/contactEmail/enableEditMode" linkId="editLink" targetElement="contact_emails_popup"><o:img name="writeIcon"/></v:ajaxLink></li>
							</c:otherwise>
						</c:choose>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
	<div class="modalBoxData">
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<c:choose>
					<c:when test="${view.createMode or view.editMode}">
						<%-- create/edit mode --%>
						<o:logger write="create or edit mode enabled..." level="debug"/>
						<v:ajaxForm name="dynamicForm" targetElement="contact_emails_popup" url="/contacts/contactEmail/save">
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">
											<c:choose>
												<c:when test="${view.createMode}"><fmt:message key="createNewEmailAddress"/></c:when>
												<c:otherwise><fmt:message key="changeEmailAddress"/></c:otherwise>
											</c:choose>
										</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${!empty sessionScope.errors}">
										<c:set var="errorsColspanCount" scope="request" value="2"/>
										<c:import url="/errors/_errors_table_entry.jsp"/>
									</c:if>
									<tr>
										<td><fmt:message key="emailAddress"/></td>
										<td><v:text styleClass="form-control" name="email" value="${view.bean.email}" /></td>
									</tr>
                                    <c:choose>
                                        <c:when test="${!empty view.selectedType}">
                                            <c:set var="selectedType" value="${view.selectedType}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="selectedType" value="${view.bean.type}"/>
                                        </c:otherwise>
                                    </c:choose>
									<tr>
										<td><fmt:message key="type"/></td>
										<td>
											<c:set var="ajax"><v:ajaxFormAction url="/contacts/contactEmail/selectType" formObject="$('dynamicForm')" targetElement="contact_emails_popup"/></c:set>
											<oc:select styleClass="form-control" name="type" value="${selectedType}" options="${view.emailTypes}" onchange="${ajax}"/>
										</td>
									</tr>
									<c:if test="${view.selectedTypeAlias}">
										<c:set var="selectedAlias" value="${view.aliasOf}"/>
										<tr>
											<td><fmt:message key="aliasOfLabel"/></td>
											<td>
												<select class="form-control" name="aliasOf" size="1">
													<option value="0"><fmt:message key="promptSelect"/>:</option>
													<c:forEach var="obj" items="${view.list}">
														<c:if test="${obj.internal}">
															<c:choose>
																<c:when test="${!empty selectedAlias and (selectedAlias == obj.id)}">
																	<option value="<o:out value="${obj.id}"/>" selected="selected"><o:out value="${obj.email}"/></option>
																</c:when>
																<c:otherwise>
																	<option value="<o:out value="${obj.id}"/>"><o:out value="${obj.email}"/></option>
																</c:otherwise>
															</c:choose>
														</c:if>
													</c:forEach>
												</select>
											</td>
										</tr>
									</c:if>
									<tr>
										<td><fmt:message key="status"/></td>
										<td>
											<input type="checkbox" id="primary" name="primary" <c:if test="${view.bean.primary}">checked="checked"</c:if> title="<fmt:message key="mainAddress"/>"/>
											<label for="primary"><fmt:message key="addressIsMainAddress"/></label>
										</td>
									</tr>
									<tr>
										<td> 
                                            <c:choose>
                                                <c:when test="${view.createMode}">
                                                    <v:ajaxLink url="/contacts/contactEmail/disableCreateMode" linkId="exitLink" targetElement="contact_emails_popup">
                                                        <input type="button" class="form-control cancel" value="<fmt:message key="exit"/>"/>
                                                    </v:ajaxLink>
                                                </c:when>
                                                <c:otherwise>
                                                    <v:ajaxLink url="/contacts/contactEmail/disableEditMode" linkId="exitLink" targetElement="contact_emails_popup">
                                                        <input type="button" class="form-control cancel" value="<fmt:message key="exit"/>"/>
                                                    </v:ajaxLink>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
										<td style="text-align: right;">
											<span style="margin-right: 5px;">
												<o:submit styleClass="form-control " />
											</span>
										</td>
									</tr>
								</tbody>
							</table>
						</v:ajaxForm>
					</c:when>
					<c:when test="${!empty view.bean}">
						<%-- bean mode --%>
						<o:logger write="invoked, address selected..." level="debug"/>
						<table class="table">
							<tr>
								<th colspan="2"><fmt:message key="addressDetails"/></th>
							</tr>
							<tr>
								<td><fmt:message key="emailAddress"/></td>
								<td><o:email value="${view.bean.email}"/></td>
							</tr>
							<tr>
								<td><fmt:message key="type"/></td>
								<td><oc:options name="emailTypes" value="${view.bean.type}"/></td>
							</tr>
							<c:if test="${!empty view.bean.aliasOf}">
								<tr>
									<td><fmt:message key="aliasOfLabel"/></td>
									<td>
										<c:forEach var="obj" items="${view.list}">
											<c:if test="${view.bean.aliasOf == obj.id}">
                                                <o:email value="${obj.email}"/>
											</c:if>
										</c:forEach>
									</td>
								</tr>
							</c:if>
							<c:if test="${view.bean.primary}">
								<tr>
									<td><fmt:message key="status"/></td>
									<td><fmt:message key="addressIsMainAddress"/></td>
								</tr>
							</c:if>
						</table>
					</c:when>
					<c:otherwise>
						<%-- list mode --%>
						<o:logger write="invoked, no address selected..." level="debug"/>
						<table class="table">
							<tr>
								<th><fmt:message key="type"/></th>
								<th><fmt:message key="emailAddress"/></th>
								<th>&nbsp;</th>
							</tr>
							<c:if test="${!empty sessionScope.errors}">
								<c:set var="errorsColspanCount" scope="request" value="4"/>
								<c:import url="/errors/_errors_table_entry.jsp"/>
							</c:if>
							<c:forEach var="obj" items="${view.list}">
								<tr>
									<td><oc:options name="emailTypes" value="${obj.type}"/></td>
									<td>
										<v:ajaxLink url="/contacts/contactEmail/select?id=${obj.id}" linkId="selectLink" targetElement="contact_emails_popup">
											<o:out value="${obj.email}"/>
										</v:ajaxLink>
									</td>
									<td class="right">
										<c:set var="exitUrl"><v:url value="/contacts/contactEmail/exit"/></c:set>
										<v:ajaxLink url="/contacts/contactEmail/deleteAddress?id=${obj.id}" linkId="deleteLink" targetElement="contact_emails_popup"><o:img name="deleteIcon" /></v:ajaxLink>
									</td>
								</tr>
							</c:forEach>
						</table>
					</c:otherwise>
				</c:choose>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</c:if>
