/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 22, 2008 2:23:45 PM 
 * 
 */
package com.osserp.gui.client.records.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;

import com.osserp.core.finance.DiscountAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDiscount;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDiscountAwareRecordView extends AbstractRecordView {
    private static Logger log = LoggerFactory.getLogger(AbstractDiscountAwareRecordView.class.getName());
    private boolean confirmDiscountMode = false;
    private String[] confirmDiscountPermissions = null;
    private String confirmDiscountPermissionsString = null;
    private Double requiredDiscount = null;

    protected AbstractDiscountAwareRecordView() {
        super();
    }

    /**
     * Sets view name as annotated, initializes action target as 'display'
     * @param confirmDiscountPermissions
     */
    protected AbstractDiscountAwareRecordView(String confirmDiscountPermissions) {
        super();
        confirmDiscountPermissionsString = confirmDiscountPermissions;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        initConfirmDiscountPermissions(confirmDiscountPermissionsString);
    }

    @Override
    public void checkRelease() throws ClientException, PermissionException {
        checkDiscount();
    }

    /**
     * Override this to check implementing views record for illegal discounts
     * @throws ClientException if illegal discount found
     */
    protected void checkDiscount() throws ClientException {
        // we do nothing here for default
    }
    
    protected boolean isDiscountCheckEnabled() {
        return isSystemPropertyEnabled("discountCheckEnabled");
    }

    public boolean isConfirmDiscountMode() {
        return confirmDiscountMode;
    }

    protected void setConfirmDiscountMode(boolean confirmDiscountMode) {
        this.confirmDiscountMode = confirmDiscountMode;
    }

    public void disableConfirmDiscountMode() {
        this.confirmDiscountMode = false;
    }

    public Double getRequiredDiscount() {
        return requiredDiscount;
    }

    protected void setRequiredDiscount(Double requiredDiscount) {
        this.requiredDiscount = requiredDiscount;
    }

    public void confirmDiscount(Form form) throws ClientException {
        String note = form.getString(Form.NOTE);
        Double discount = form.getDouble(Form.AMOUNT);
        DiscountAwareRecord record = getDiscountAware();
        if (isNotSet(note)) {
            if (record.getDiscountHistory().isEmpty()) {
                throw new ClientException(ErrorCode.ACTION_NOTE_REQUIRED);
            }
            RecordDiscount last = record.getDiscountHistory().get(0);
            if (isNotSet(last.getNote())) {
                throw new ClientException(ErrorCode.ACTION_NOTE_REQUIRED);
            }
            note = last.getNote();
        }
        if (isNotSet(discount)) {
            throw new ClientException(ErrorCode.AMOUNT_MISSING);
        }
        record.addDiscount(getDomainEmployee(), discount, note);
        getRecordManager().persist(record);
        this.confirmDiscountMode = false;
        refresh();
    }

    protected DiscountAwareRecord getDiscountAware() {
        Record obj = getRecord();
        if (!(obj instanceof DiscountAwareRecord)) {
            log.warn("getDiscountAware() failed; expected DiscountAwareRecord, found [class="
                    + obj.getClass().getName() + "]");
            throw new com.osserp.common.ActionException();
        }
        return (DiscountAwareRecord) obj;
    }

    protected String[] getConfirmDiscountPermissions() {
        return confirmDiscountPermissions;
    }

    private void initConfirmDiscountPermissions(String confirmDiscountPermissionName) {
        SystemConfigManager manager = getSystemConfigManager();
        if (confirmDiscountPermissionName != null) {
            confirmDiscountPermissions = manager.getPermissionConfig(confirmDiscountPermissionName);
        }
    }
}
