/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 16, 2006 2:00:06 PM 
 * 
 */
package com.osserp.core.finance;

import java.math.BigDecimal;
import java.util.List;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.ClassifiedContact;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PaymentManager {

    /**
     * Provides all payments related to a bank account
     * @param bankAccount
     * @return payments
     */
    List<Payment> getByBankAccount(BankAccount bankAccount);
    
    /**
     * Provides all payments related to a dedicated contact.
     * Note: This methods looks up for payment reference as relatedContact.id 
     * only. E.g. contact.contactId will be ignored.
     * @param contact
     * @return payments
     */
    List<Payment> getByContact(ClassifiedContact contact);

    /**
     * Provides all payments related to an order
     * @param order
     * @return payments
     */
    List<Payment> getByOrder(Order order);

    /**
     * Provides all payments related to a payment aware record (e.g. invoice, credit note)
     * @param record
     * @return payments
     */
    List<Payment> getByRecord(PaymentAwareRecord record);

    /**
     * Provides a payment by its primary key
     * @param id primary key of payment
     * @return payment
     */
    Payment getPayment(Long id);

    /**
     * Deletes a payment
     * @param payment
     */
    void delete(Payment payment);
    
    /**
     * Substracts provided amount from payment amount
     * @param payment
     * @param cashDiscount
     */
    void subtract(Payment payment, BigDecimal cashDiscount);
}
