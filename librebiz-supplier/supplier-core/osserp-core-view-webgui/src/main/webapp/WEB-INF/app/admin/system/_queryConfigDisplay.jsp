<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="name"><fmt:message key="queryName" /></label>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <o:out value="${view.bean.name}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="resultName"><fmt:message key="resultName" /></label>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <o:out value="${view.bean.outputTitle}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="fullscreen"><fmt:message key="output" /></label>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.fullscreen}">
                                <v:link url="${view.baseLink}/toggleFullscreen" title="reverse"><fmt:message key="fullWidth"/></v:link>
                            </c:when>
                            <c:otherwise>
                                <v:link url="${view.baseLink}/toggleFullscreen" title="reverse"><fmt:message key="withMenu"/></v:link>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="query"><fmt:message key="query" /></label>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <v:textarea name="query" value="${view.bean.query}" styleClass="form-control query-textarea" readonly="true" />
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<c:set var="paramsColsCnt" value="4"/>
<c:set var="outputColsCnt" value="5"/>
<o:permission role="query_config">
<c:set var="paramsColsCnt" value="6"/>
<c:set var="outputColsCnt" value="7"/>
</o:permission>

<div class="col-md-6 panel-area panel-area-default">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr><th colspan="${paramsColsCnt}"><fmt:message key="parameter"/></th></tr>
                <tr>
                    <th><fmt:message key="labelText"/></th>
                    <th><fmt:message key="name"/></th>
                    <th><fmt:message key="type"/></th>
                    <th><fmt:message key="input"/></th>
                    <o:permission role="query_config" info="permissionQueryConfig">
                        <th class="icon center">
                            <v:ajaxLink url="/admin/system/queryDetailConfigurator/forward?mode=input" linkId="queryDetailConfiguratorView" title="addParameter">
                                <o:img name="newdataIcon" styleClass="bigicon"/>
                            </v:ajaxLink>
                        </th>
                        <th class="icon center">
                            <v:link url="${view.baseLink}/clearParameters" title="delete" confirm="true" message="confirmRemoveAllParameters">
                                <o:img name="deleteIcon" styleClass="bigicon" />
                            </v:link>
                        </th>
                    </o:permission>
                </tr>
            </thead>
            <tbody class="scrollTableBody" style="height:100px;">
                <c:choose>
                    <c:when test="${empty view.bean.parameters}">
                        <tr><td colspan="${paramsColsCnt}"><fmt:message key="noParametersDefined"/></td></tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="item" items="${view.bean.parameters}">
                            <tr>
                                <td><o:out value="${item.label}"/></td>
                                <td><o:out value="${item.name}"/></td>
                                <td><o:out value="${item.typeName}"/></td>
                                <td><o:out value="${item.inputType.name}"/></td>
                                <o:permission role="query_config" info="permissionQueryConfig">
                                    <td class="icon center"> </td>
                                    <td class="icon center">
                                        <v:ajaxLink url="/admin/system/queryDetailConfigurator/forward?id=${item.id}&mode=input" linkId="queryDetailConfiguratorView" title="change">
                                            <o:img name="writeIcon" styleClass="bigicon" />
                                        </v:ajaxLink>
                                    </td>
                                </o:permission>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th colspan="${outputColsCnt}"><fmt:message key="output"/></th>
                </tr>
                <tr>
                    <th><fmt:message key="name"/></th>
                    <th><fmt:message key="type"/></th>
                    <th><fmt:message key="display"/></th>
                    <th><fmt:message key="size"/></th>
                    <th><fmt:message key="alignment"/></th>
                    <o:permission role="query_config" info="permissionQueryConfig">
                        <th class="icon center">
                            <v:ajaxLink url="/admin/system/queryDetailConfigurator/forward?mode=output" linkId="queryDetailConfiguratorView" title="addOutputCleft">
                                <o:img name="newdataIcon" styleClass="bigicon" />
                            </v:ajaxLink>
                        </th>
                        <th class="icon center">
                            <v:link url="${view.baseLink}/clearOutput" title="deleteAllColumns"  confirm="true" message="confirmRemoveAllColumns">
                                <o:img name="deleteIcon" styleClass="bigicon" />
                            </v:link>
                        </th>
                    </o:permission>
                </tr>
            </thead>
            <tbody class="scrollTableBody" style="height: 157px;">
                <c:choose>
                    <c:when test="${empty view.bean.outputs}">
                        <tr><td colspan="${outputColsCnt}"><fmt:message key="noQueriesDefined"/></td></tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="item" items="${view.bean.outputs}">
                            <tr>
                                <td><o:out value="${item.name}"/></td>
                                <td><o:out value="${item.typeName}"/></td>
                                <td class="center">
                                    <c:choose>
                                        <c:when test="${item.display}"><fmt:message key="yes"/></c:when>
                                        <c:otherwise><fmt:message key="no"/></c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="center"><o:out value="${item.width}"/></td>
                                <td><o:out value="${item.alignment}"/></td>
                                <o:permission role="query_config" info="permissionQueryConfig">
                                    <td class="icon center"> </td>
                                    <td class="icon center">
                                        <v:ajaxLink url="/admin/system/queryDetailConfigurator/forward?id=${item.id}&mode=output" linkId="queryDetailConfiguratorView" title="change">
                                            <o:img name="writeIcon" styleClass="bigicon" />
                                        </v:ajaxLink>
                                    </td>
                                </o:permission>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>

    </div>
</div>
