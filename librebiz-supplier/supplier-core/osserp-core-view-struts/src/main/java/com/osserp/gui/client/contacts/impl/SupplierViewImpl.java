/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 21, 2006 7:28:07 PM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.BankAccount;
import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PaymentAware;
import com.osserp.core.contacts.PersonManager;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductGroupManager;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.suppliers.SupplierType;
import com.osserp.core.suppliers.SupplierManager;
import com.osserp.core.system.SystemCompanyManager;

import com.osserp.gui.client.contacts.SupplierView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("contactView")
public class SupplierViewImpl extends AbstractPaymentAwareContactView implements SupplierView {
    private static Logger log = LoggerFactory.getLogger(SupplierViewImpl.class.getName());
    private boolean groupSelectionMode = false;
    private List<ProductGroup> availableGroups = null;
    private BankAccount bankAccount = null;
    private List<BankAccount> bankAccounts = new ArrayList<BankAccount>();
    private boolean bankAccountSelectionMode = false;
    
    /**
     * Default constructor needed to implement serializable Do not use for normal operation!
     */
    protected SupplierViewImpl() {
        super();
    }

    @Override
    public boolean isRelatedContactView() {
        return true;
    }

    @Override
    public boolean isSupplierView() {
        return true;
    }

    @Override
    public void disableAllModes() {
        super.disableAllModes();
        groupSelectionMode = false;
    }

    @Override
    public void updateDetails(Form fh) throws ClientException, PermissionException {
        setForm(fh);
        refresh();
        disableAllModes();
    }

    public void changeStatus() {
        Supplier current = getSupplier();
        if (current.isActivated()) {
            current.setStatusId(Supplier.DEACTIVATED);
        } else {
            current.setStatusId(Supplier.ACTIVATED);
        }
        getPersonManager().save(current);
    }

    public boolean isGroupSelectionMode() {
        return groupSelectionMode;
    }

    public void setGroupSelectionMode(boolean groupSelectionMode) {
        disableAllModes();
        this.groupSelectionMode = groupSelectionMode;
        if (groupSelectionMode) {
            availableGroups = new ArrayList<ProductGroup>();
            Set<ProductGroup> current = getCurrentGroups();
            List<ProductGroup> groups = getAllProductGroups();
            for (int i = 0, j = groups.size(); i < j; i++) {
                ProductGroup grp = groups.get(i);
                if (!current.contains(grp)) {
                    availableGroups.add(grp);
                }
            }
        } else {
            availableGroups = null;
        }
    }

    public List<ProductGroup> getAvailableGroups() {
        return availableGroups;
    }

    public void addGroup(Long id) {
        if (id != null) {
            setBean(((SupplierManager) getPersonManager()).addProductGroup(
                    getSupplier(), getSelectedGroup(id)));
        }
        disableAllModes();
    }

    /**
     * Removes a group from current installer
     * @param group id
     */
    public void removeGroup(Long id) {
        if (id != null) {
            setBean(((SupplierManager) getPersonManager()).removeProductGroup(
                    getSupplier(), getSelectedGroup(id))); 
        }
        disableAllModes();
    }

    public void changeDirectInvoiceBooking() {
        Supplier supplier = getSupplier();
        supplier.setDirectInvoiceBookingEnabled(!supplier.isDirectInvoiceBookingEnabled());
        getPersonManager().save(supplier);
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }
    protected void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }
    protected void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }
    
    public boolean isBankAccountSelectionMode() {
        return bankAccountSelectionMode;
    }
    protected void setBankAccountSelectionMode(boolean bankAccountSelectionMode) {
        this.bankAccountSelectionMode = bankAccountSelectionMode;
    }
    
    public void enableBankAccountSelection() throws ClientException {
        List<BankAccount> alist = getSystemCompanyManager().getBankAccounts();
        if (alist.isEmpty()) {
            throw new ClientException(ErrorCode.BANKACCOUNTS_UNDEFINED);
        }
        bankAccounts = alist;
        bankAccountSelectionMode = true;
        if (log.isDebugEnabled()) {
            log.debug("enableBankAccountSelection() done [count=" + bankAccounts.size() + "]");
        }
    }

    public void assignBankAccount(Long id) {
        if (isSet(id)) {
            Supplier supplier = getSupplier();
            Long supplierId = (id == null || id == 0) ? null : supplier.getId();
            getSystemCompanyManager().assignBankAccount(id, supplierId);
        }
        bankAccounts = new ArrayList<>();
        bankAccountSelectionMode = false;
        loadSupplierDetails();
    }
    
    protected SystemCompanyManager getSystemCompanyManager() {
        return (SystemCompanyManager) getService(SystemCompanyManager.class.getName());
    }

    public Supplier getSupplier() {
        if (getBean() == null) {
            throw new ActionException();
        }
        return (Supplier) getCurrent();
    }

    public void selectSupplierType(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("selectSupplierType() invoked [id=" + id + "]");
        }
        SupplierType selected = null;
        if (id != null) {
            selected = (SupplierType) getOption(Options.SUPPLIER_TYPES, id);
        }
        if (selected != null) {
            Supplier s = getSupplier();
            s.changeSupplierType(selected);
            getPersonManager().save(s);
            setBean(s);
            if (log.isDebugEnabled()) {
                log.debug("selectSupplierType() done [supplier=" + s.getId() 
                        + ", type=" + selected.getId() + "]");
            }
        }
    }
    
    protected void loadSupplierDetails() {
        Supplier supplier = getSupplier(); 
        if (supplier.getSupplierType() != null 
                && supplier.getSupplierType().isProvidesBankaccounts()) {
            List<BankAccount> all = getSystemCompanyManager().getBankAccounts();
            bankAccounts = new ArrayList<>();
            if (!all.isEmpty()) {
                for (int i = 0, j = all.size(); i < j; i++) {
                    BankAccount next = all.get(i);
                    if (next.getSupplierId() != null && next.getSupplierId().equals(supplier.getId())) {
                        bankAccounts.add(next);
                    }
                }
            }
        }
    }

    @Override
    protected void setBean(Object bean) {
        super.setBean(bean);
        loadSupplierDetails();
    }

    @Override
    protected PaymentAware fetchPaymentAware() {
        return getSupplier();
    }

    @Override
    protected void persist(PaymentAware contact) {
        getPersonManager().save((Supplier) contact);
    }

    @Override
    protected PersonManager getPersonManager() {
        return (PersonManager) getService(SupplierManager.class.getName());
    }

    @Override
    protected String getDefaultDisplayGroup() {
        return Contact.SUPPLIER_KEY;
    }

    private Set<ProductGroup> getCurrentGroups() {
        Set<ProductGroup> groups = new HashSet<ProductGroup>();
        Supplier supplier = getSupplier();
        for (int i = 0, j = supplier.getProductGroups().size(); i < j; i++) {
            groups.add(supplier.getProductGroups().get(i));
        }
        return groups;
    }

    private List<ProductGroup> getAllProductGroups() {
        ProductGroupManager manager = getProductGroupManager();
        return new ArrayList(manager.getList());
    }

    private ProductGroup getSelectedGroup(Long id) {
        ProductGroupManager manager = getProductGroupManager();
        try {
            return manager.findById(id);
        } catch (ClientException e) {
            throw new ActionException();
        }
    }

    private ProductGroupManager getProductGroupManager() {
        return (ProductGroupManager) getService(ProductGroupManager.class.getName());
    }

}
