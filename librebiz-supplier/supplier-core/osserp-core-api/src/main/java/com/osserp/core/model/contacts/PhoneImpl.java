/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Date;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.util.PhoneUtil;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;

/**
 * Value object for a Phone.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class PhoneImpl extends AbstractEntity implements Phone {

    private Long contactId;
    private Long device;
    private Long type;
    private String country;
    private String prefix;
    private String number;
    private String note;
    private boolean primary;
    private String phoneKey = null;
    private String listedNumber = null;
    private boolean published = false;
    private String validationStatus = EmailAddress.VALIDATED_NOTHING;

    protected PhoneImpl() { // NO_UCD
        super();
    }

    protected PhoneImpl(Phone o) {
        super(o);
        this.contactId = o.getContactId();
        this.device = o.getDevice();
        this.type = o.getType();
        this.country = o.getCountry();
        this.prefix = o.getPrefix();
        this.number = o.getNumber();
        this.note = o.getNote();
        this.primary = o.isPrimary();
        this.phoneKey = o.getPhoneKey();
        this.listedNumber = o.getListedNumber();
        this.published = o.isPublished();
    }

    public PhoneImpl(
            Long createdBy,
            Long contactId,
            Long device,
            Long type,
            String country,
            String prefix,
            String number,
            String note,
            boolean primary) {

        super(null, null, createdBy);
        this.contactId = contactId;
        this.device = device;
        this.type = type;
        if (isSet(country)) {
            if (country.startsWith("0")) {
                this.country = country.substring(1);
            } else {
                this.country = country;
            }
        }
        if (isSet(prefix)) {
            if (prefix.startsWith("0")) {
                this.prefix = prefix.substring(1);
            } else {
                this.prefix = prefix;
            }
        }
        this.number = number;
        this.note = note;
        this.primary = primary;
    }

    public PhoneImpl(
            Long device,
            ContactType contactType,
            String country,
            String prefix,
            String number) {

        super();
        this.device = device;
        if (isSet(country)) {
            if (country.startsWith("0")) {
                this.country = country.substring(1);
            } else {
                this.country = country;
            }
        }
        if (isSet(prefix)) {
            if (prefix.startsWith("0")) {
                this.prefix = prefix.substring(1);
            } else {
                this.prefix = prefix;
            }
        }
        this.number = number;
        this.primary = true;
        if (contactType != null && 
                (contactType.isBusiness() || contactType.isPublicInstitution() || contactType.isFreelance())) {
            type = Phone.BUSINESS;
        } else {
            type = Phone.PRIVATE;
        }
    }

    @Override
    public Object clone() {
        return new PhoneImpl(this);
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getDevice() {
        return device;
    }

    public void setDevice(Long device) {
        this.device = device;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "contactId", contactId);
        putIfExists(map, "country", country);
        putIfExists(map, "device", device);
        putIfExists(map, "formattedNumber", getFormattedNumber());
        putIfExists(map, "formattedOutlookNumber", getFormattedOutlookNumber());
        putIfExists(map, "note", note);
        putIfExists(map, "number", number);
        putIfExists(map, "prefix", prefix);
        putIfExists(map, "type", type);
        putIfExists(map, "validationStatus", validationStatus);
        return map;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public final boolean isBusiness() {
        return Phone.BUSINESS.equals(type);
    }

    public boolean isInternal() {
        return false;
    }

    public final boolean isPrivate() {
        return Phone.PRIVATE.equals(type);
    }

    public String getPhoneKey() {
        return phoneKey;
    }

    protected void setPhoneKey(String phoneKey) { // NO_UCD - sql-sp
        this.phoneKey = phoneKey;
    }

    public String getListedNumber() {
        return listedNumber;
    }

    protected void setListedNumber(String listedNumber) { // NO_UCD - sql-sp
        this.listedNumber = listedNumber;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public String getFormattedNumber() {
        return PhoneUtil.createInternational(country, prefix, number);
    }

    public String getFormattedOutlookNumber() {
        return PhoneUtil.createOutlook(country, prefix, number);
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    protected void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }
    
    public void updateValidationStatus(Long validator, String status) {
        if (status != null) {
            validationStatus = status;
            setChangedBy(validator == null ? Constants.SYSTEM_EMPLOYEE : validator);
            setChanged(new Date(System.currentTimeMillis()));
        }
    }

    public Element getXml() {
        return getXml(getTypeString());
    }

    public Element getXml(String name) {
        Element root = new Element(name);
        if (getId() != null) {
            root.addContent(new Element("id").setText(getId().toString()));
        } else {
            root.addContent(new Element("id"));
        }
        if (device != null) {
            root.addContent(new Element("device").setText(device.toString()));
        } else {
            root.addContent(new Element("device"));
        }
        if (type != null) {
            root.addContent(new Element("type").setText(type.toString()));
        } else {
            root.addContent(new Element("type"));
        }
        if (country != null) {
            root.addContent(new Element("country").setText(country));
        } else {
            root.addContent(new Element("country"));
        }
        if (prefix != null) {
            root.addContent(new Element("prefix").setText(prefix));
        } else {
            root.addContent(new Element("prefix"));
        }
        if (number != null) {
            root.addContent(new Element("number").setText(number));
        } else {
            root.addContent(new Element("number"));
        }
        if (note != null) {
            root.addContent(new Element("note").setText(note));
        } else {
            root.addContent(new Element("note"));
        }
        if (validationStatus != null) {
            root.addContent(new Element("validationStatus").setText(validationStatus));
        } else {
            root.addContent(new Element("validationStatus"));
        }
        root.addContent(new Element("primary").setText(Boolean.toString(primary)));
        return root;

    }

    private String getTypeString() {
        if (PhoneType.FAX.equals(device)) {
            return "fax";
        } else if (PhoneType.MOBILE.equals(device)) {
            return "mobile";
        } else {
            return "phone";
        }
    }
}
