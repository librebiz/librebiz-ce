/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 20-Sep-2006 06:30:21 
 * 
 */
package com.osserp.core.views;

import java.io.Serializable;
import java.util.Date;

import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OrderItemsDisplay implements Serializable {

    private Long id = null;
    private Date created = null;
    private Long branchId = null;
    private Long status = null;
    private Long referenceStatus = null;
    private Date delivery = null;
    private Long salesId = null;
    private String salesName = null;
    private Long itemId = null;
    private Long productId = null;
    private String productName = null;
    private Long productCategory = null;
    private Long productGroup = null;
    private Long manufacturerId = null;
    private Double quantity = null;
    private Double delivered = null;
    private boolean deliveryConfirmed = false;
    private String deliveryNote = null;
    private Double stock = null;
    private Double ordered = null;
    private Double vacantOrdered = null;
    private Double receipt = null;
    private Double salesReceipt = null;
    private Double expected = null;
    private boolean salesItem = true;
    private boolean vacant = false;
    private Long stockId = null;

    private Double expectedStock = 0d;

    protected OrderItemsDisplay() {
        super();
    }

    public OrderItemsDisplay(
            Long salesId,
            String salesName,
            Long id,
            Date created,
            Long branchId,
            Long status,
            Long referenceStatus,
            Date delivery,
            Long itemId,
            Long productId,
            String productName,
            Long productCategory,
            Long productGroup,
            Long manufacturerId,
            Double quantity,
            Double delivered,
            boolean deliveryConfirmed,
            String deliveryNote,
            Double stock,
            Double ordered,
            Double vacantOrdered,
            Double receipt,
            Double expected,
            Double salesReceipt,
            boolean salesItem,
            boolean vacant,
            Long stockId) {
        super();

        this.salesId = salesId;
        this.salesName = salesName;
        this.id = id;
        this.created = created;
        this.branchId = (branchId == null ? 1L : branchId);
        this.status = (status == null ? 0L : status);
        this.referenceStatus = referenceStatus;
        this.delivery = delivery;
        this.itemId = itemId;
        this.productId = productId;
        this.productName = productName;
        this.productCategory = productCategory;
        this.productGroup = productGroup;
        this.manufacturerId = manufacturerId;
        this.quantity = quantity;
        this.delivered = delivered;
        this.deliveryConfirmed = deliveryConfirmed;
        this.deliveryNote = deliveryNote;
        this.stock = stock;
        this.ordered = ordered;
        this.vacantOrdered = vacantOrdered;
        this.receipt = receipt;
        this.expected = expected;
        this.salesReceipt = salesReceipt;
        this.salesItem = salesItem;
        this.vacant = vacant;
        this.stockId = stockId;
    }

    public OrderItemsDisplay(
            Long salesId,
            String salesName,
            Long id,
            Date created,
            Date delivery,
            Product product,
            Double quantity,
            boolean salesItem,
            boolean vacant,
            Long stockId) {
        super();

        this.salesId = salesId;
        this.salesName = salesName;
        this.id = id;
        this.created = created;
        this.branchId = 1L;
        this.status = 0L;
        this.delivery = delivery;
        this.itemId = Long.MAX_VALUE;
        this.productId = product.getProductId();
        this.productName = product.getName();
        this.productCategory = product.getCategory() == null ? null : product.getCategory().getId();
        this.productGroup = product.getGroup() == null ? null : product.getGroup().getId();
        this.manufacturerId = product.getManufacturer();
        this.quantity = quantity;
        this.delivered = 0d;
        this.deliveryConfirmed = false;
        if (product.getSummary() != null) {
            this.stock = product.getSummary().getStock();
            this.ordered = product.getSummary().getOrdered();
            this.vacantOrdered = product.getSummary().getVacant();
            this.receipt = product.getSummary().getReceipt();
            this.expected = product.getSummary().getExpected();
            this.salesReceipt = product.getSummary().getSalesReceipt();
        }
        this.salesItem = salesItem;
        this.vacant = vacant;
        this.stockId = stockId;
    }

    public OrderItemsDisplay(OrderItemsDisplay other) {
        super();
        this.salesId = other.salesId;
        this.salesName = other.salesName;
        this.id = other.id;
        this.created = other.created;
        this.branchId = (other.branchId == null ? 1L : other.branchId);
        this.status = (other.status == null ? 0L : other.status);
        this.referenceStatus = other.referenceStatus;
        this.delivery = other.delivery;
        this.itemId = other.itemId;
        this.productId = other.productId;
        this.productName = other.productName;
        this.productCategory = other.productCategory;
        this.productGroup = other.productGroup;
        this.manufacturerId = other.manufacturerId;
        this.quantity = other.quantity;
        this.delivered = other.delivered;
        this.deliveryConfirmed = other.deliveryConfirmed;
        this.deliveryNote = other.deliveryNote;
        this.stock = other.stock;
        this.ordered = other.ordered;
        this.vacantOrdered = other.vacantOrdered;
        this.receipt = other.receipt;
        this.expected = other.expected;
        this.salesReceipt = other.salesReceipt;
        this.salesItem = other.salesItem;
        this.vacant = other.vacant;
        this.stockId = other.stockId;
    }

    public Long getSalesId() {
        return salesId;
    }

    public String getSalesName() {
        return salesName;
    }

    public Date getCreated() {
        return created;
    }

    public Long getBranchId() {
        return branchId;
    }

    public Long getStatus() {
        return status;
    }

    public Long getReferenceStatus() {
        return referenceStatus;
    }

    public Date getDelivery() {
        return delivery;
    }

    public boolean isDeliveryConfirmed() {
        return deliveryConfirmed;
    }

    public String getDeliveryNote() {
        return deliveryNote;
    }

    public Long getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public Long getProductCategory() {
        return productCategory;
    }

    public Long getProductGroup() {
        return productGroup;
    }

    public Long getManufacturerId() {
        return manufacturerId;
    }

    public Long getId() {
        return id;
    }

    public Long getItemId() {
        return itemId;
    }

    public Double getQuantity() {
        return quantity;
    }

    public Double getDelivered() {
        return delivered;
    }

    public Double getStock() {
        return stock;
    }

    public Double getOrdered() {
        return ordered;
    }

    public Double getVacantOrdered() {
        return vacantOrdered;
    }

    public Double getReceipt() {
        return receipt;
    }

    public Double getExpected() {
        return expected;
    }

    public Double getSalesReceipt() {
        return salesReceipt;
    }

    public Double getOutstanding() {
        return (getDouble(quantity) - getDouble(delivered));
    }

    public boolean isSalesItem() {
        return salesItem;
    }

    public boolean isVacant() {
        return vacant;
    }

    public Double getExpectedStock() {
        return expectedStock;
    }

    public void setExpectedStock(Double expectedStock) {
        this.expectedStock = expectedStock;
    }

    public Long getStockId() {
        return stockId;
    }

    public boolean isLazy() {
        return (expectedStock != null && expectedStock < 0);
    }

    // setters are readonly, view is readonly

    protected void setProductId(Long productId) {
        this.productId = productId;
    }

    protected void setProductName(String productName) {
        this.productName = productName;
    }

    protected void setProductCategory(Long productCategory) {
        this.productGroup = productCategory;
    }

    protected void setProductGroup(Long productGroup) {
        this.productGroup = productGroup;
    }

    protected void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    protected void setDelivered(Double delivered) {
        this.delivered = delivered;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    protected void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    protected void setStatus(Long status) {
        this.status = status;
    }

    protected void setReferenceStatus(Long referenceStatus) {
        this.referenceStatus = referenceStatus;
    }

    protected void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    protected void setOrdered(Double ordered) {
        this.ordered = ordered;
    }

    protected void setVacantOrdered(Double vacantOrdered) {
        this.vacantOrdered = vacantOrdered;
    }

    protected void setSalesId(Long salesId) {
        this.salesId = salesId;
    }

    protected void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    protected void setCreated(Date created) {
        this.created = created;
    }

    protected void setDelivery(Date delivery) {
        this.delivery = delivery;
    }

    protected void setDeliveryConfirmed(boolean deliveryConfirmed) {
        this.deliveryConfirmed = deliveryConfirmed;
    }

    protected void setDeliveryNote(String deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    protected void setExpected(Double expected) {
        this.expected = expected;
    }

    protected void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    protected void setReceipt(Double receipt) {
        this.receipt = receipt;
    }

    protected void setStock(Double stock) {
        this.stock = stock;
    }

    protected void setSalesItem(boolean salesItem) {
        this.salesItem = salesItem;
    }

    protected void setVacant(boolean vacant) {
        this.vacant = vacant;
    }

    protected void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    protected void setSalesReceipt(Double salesReceipt) {
        this.salesReceipt = salesReceipt;
    }

    public void updateExpected(Double expected) {
        this.expected = expected;
    }

    private double getDouble(Double d) {
        return (d == null ? 0 : d);
    }

}
