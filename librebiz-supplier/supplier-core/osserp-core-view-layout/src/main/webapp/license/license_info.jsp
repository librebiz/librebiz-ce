<p>
    The majority of the code base is written by Rainer Kirchner and
    licensed under various licenses depending on individual projects 
    starting in 2001. 
    The licensing changed to AGPL3 along with the change of project specific
    package names to the dedicated namespace com.osserp in 2009.
</p>
<p>
    Some files written by thirdparty contributors are licensed under the
    terms of the Apache License.
</p>
