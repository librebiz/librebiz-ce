/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.osserp.common.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.X509TrustAllManager

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class APIConfig {
    
    private static Logger logger = LoggerFactory.getLogger(APIConfig.class)

    String serviceUri
    String apiVersionHeader
    boolean logResponse

    private String username
    private String password

    APIConfig() {
        super()
    }

    APIConfig(String serviceUri,
            String username,
            String password,
            String apiVersionHeader,
            Boolean logResponse,
            Boolean trustAllCerts) {
        super()
        this.serviceUri = serviceUri
        this.username = username
        this.password = password
        this.apiVersionHeader = apiVersionHeader
        this.logResponse = logResponse
        if (trustAllCerts) {
            logger.warn('trustAllCerts enabled')
            X509TrustAllManager.trustSelfSignedSSL()
        } else {
            logger.debug('trustAllCerts disabled')
        }
    }

    protected String getUsername() {
        return username
    }

    public void setUsername(String username) {
        this.username = username
    }

    protected String getPassword() {
        return password
    }

    public void setPassword(String password) {
        this.password = password
    }
}
