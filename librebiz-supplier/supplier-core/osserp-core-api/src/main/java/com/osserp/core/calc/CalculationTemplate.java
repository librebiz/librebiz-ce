/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 29, 2011 1:57:57 PM 
 * 
 */
package com.osserp.core.calc;

import java.util.List;

import com.osserp.common.Option;

import com.osserp.core.BusinessType;
import com.osserp.core.products.Product;

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public interface CalculationTemplate extends Option {

    /**
     * Provides the product for this template
     * @return productId
     */
    Product getProduct();

    /**
     * Provides the business type for this template
     * @return businessType
     */
    BusinessType getBusinessType();

    /**
     * Provides the items in this template
     * @return
     */
    List<CalculationTemplateItem> getItems();

    /**
     * Adds an item to this template
     * @param item
     */
    void addItem(CalculationTemplateItem item);

    /**
     * Remove an item from template
     * @param itemId
     */
    void removeItem(Long itemId);

    /**
     * Toggles item.optional
     * @param itemId
     */
    void toggleOptionalItem(Long itemId);

    /**
     * Check if the given product already added
     * @param product
     * @return true if product is already added
     */
    boolean isAlreadyAdded(Product product);
}
