/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 13, 2010 05:22:32 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Tag renders an Div executing an ajax request on mouseover. The function invoked depends on given params.
 * 
 * @author so <so@osserp.com>
 * 
 */
public class AjaxInfoboxTag extends AbstractAjaxLinkTag {
    private static Logger log = LoggerFactory.getLogger(AjaxInfoboxTag.class.getName());
    private String iconUrl = null;

    /**
     * Provides iconUrl for the icon
     * @return iconUrl
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * Sets an iconUrl for the icon
     * @param iconUrl
     */
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    private String createIconUrl() {
        return getContextPath() + getIconUrl();
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            if (log.isDebugEnabled()) {
                log.debug("doStartTag() invoked: [id=" + getId() + ", iconUrl=" + createIconUrl() + ", url=" + createUrl() + "]");
            }

            StringBuilder buffer = new StringBuilder();
            buffer.append("<span style=\"float:right;\"><a href=\"javascript:void(0)\" onmouseover=\"ojsAjax.showInfoBox(event, '")
                    .append(getId())
                    .append("', '")
                    .append(createUrl())
                    .append("');\" onmouseout=\"ojsAjax.hideInfoBox('")
                    .append(getId())
                    .append("');\">")
                    .append("<img src=\"")
                    .append(createIconUrl())
                    .append("\" class=\"icon\" style=\"vertical-align:middle;\" /></a>")
                    .append("<span id=\"")
                    .append(getId())
                    .append("_border\" style=\"display:none;\">")
                    .append("<span id=\"")
                    .append(getId())
                    .append("\" class=\"infoBox\"></span></span></span>");

            JspWriter out = pageContext.getOut();
            out.print(buffer.toString());
        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }
}
