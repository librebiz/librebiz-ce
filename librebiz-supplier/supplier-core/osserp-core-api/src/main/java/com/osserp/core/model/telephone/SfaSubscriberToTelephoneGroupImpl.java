/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 4, 2010 2:49:17 PM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.SfaSubscriberToTelephoneGroup;
import com.osserp.core.telephone.SfaTelephoneGroup;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class SfaSubscriberToTelephoneGroupImpl extends AbstractEntity implements SfaSubscriberToTelephoneGroup {
    private boolean synced = false;
    private boolean unused = false;
    private String subscriberUid = null;
    private Long telephoneSystemId = null;
    private SfaTelephoneGroup sfaTelephoneGroup = null;

    public SfaSubscriberToTelephoneGroupImpl() {
        super();
    }

    public SfaSubscriberToTelephoneGroupImpl(String subscriberUid, Long telephoneSystemId, SfaTelephoneGroup sfaTelephoneGroup) {
        super();
        this.subscriberUid = subscriberUid;
        this.telephoneSystemId = telephoneSystemId;
        this.sfaTelephoneGroup = sfaTelephoneGroup;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isUnused() {
        return unused;
    }

    public void setUnused(boolean unused) {
        this.unused = unused;
    }

    public String getSubscriberUid() {
        return subscriberUid;
    }

    public void setSubscriberUid(String subscriberUid) {
        this.subscriberUid = subscriberUid;
    }

    public Long getTelephoneSystemId() {
        return telephoneSystemId;
    }

    public void setTelephoneSystemId(Long telephoneSystemId) {
        this.telephoneSystemId = telephoneSystemId;
    }

    public SfaTelephoneGroup getSfaTelephoneGroup() {
        return sfaTelephoneGroup;
    }

    public void setSfaTelephoneGroup(SfaTelephoneGroup sfaTelephoneGroup) {
        this.sfaTelephoneGroup = sfaTelephoneGroup;
    }
}
