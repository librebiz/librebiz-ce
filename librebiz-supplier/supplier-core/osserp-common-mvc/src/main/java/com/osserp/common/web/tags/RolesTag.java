/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2009 2:25:49 PM 
 * 
 */
package com.osserp.common.web.tags;

import javax.servlet.jsp.JspException;

/**
 * 
 * @author eh <eh@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class RolesTag extends AbstractTagSupport {

    private boolean subtagEntered;
    private boolean showOtherwise;

    public RolesTag() {
        super();
        init();
    }

    @Override
    public void release() {
        super.release();
        init();
    }

    /**
     * Returns status indicating whether a subtag should run or not.
     * 
     * @return <tt>true</tt> if the subtag should evaluate its condition and decide whether to run, <tt>false</tt> otherwise.
     */
    public boolean gainPermission() {
        if (!subtagEntered) {
            subtagEntered = true;
            return (true);
        }
        return (false);
    }

    /**
     * A subtag calls this when it has finished successfully (with EVAL_BODY_INCLUDE).
     */
    public void subtagSucceeded() {
        if (!subtagEntered) {
            throw new IllegalStateException("The subtag was not entered yet");
        }
        subtagEntered = false;
        showOtherwise = false;
    }

    /**
     * A subtag calls this when it has finished without showing it's body (SKIP_BODY).
     */
    public void subtagSkipped() {
        subtagEntered = false;
    }

    /**
     * Indicates whether the otherwise tag has to be displayed or not.
     * @return true if the otherwise tag has to be displayed, false if not.
     */
    public boolean showOtherwise() {
        return showOtherwise;
    }

    @Override
    public int doStartTag() throws JspException {
        subtagEntered = false;
        showOtherwise = true;
        return EVAL_BODY_INCLUDE;
    }

    private void init() {
        subtagEntered = false;
        showOtherwise = true;
    }
}
