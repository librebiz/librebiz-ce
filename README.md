# Librebiz ERP and CRM

Librebiz ist eine ERP und CRM-Software mit integrierter Projekt- und
Dokumentenverwaltung.

Die leicht zu erlernende Webanwendung unterstützt alle Unternehmensprozesse
von Angebot bis Zeiterfassung. Dokumente werden im Format PDF erstellt und
gespeichert.

Die Software kann auf Linux-Systemen mit aktueller Java-Laufzeitumgebung und
PostgreSQL-Datenbank installiert werden.

Nach der Installation auf dem Server erfolgt die Bedienung unabhängig vom
Betriebssystem einfach über den Webbrowser.


### Anleitungen

* [Schnellanleitung für Debian GNU/Linux und Ubuntu Server Edition](INSTALL.md)
* [Installation auf GNU/Linux-Systemen wie CentOS, openSUSE, etc.](https://librebiz.org/manuals/de/index)
* [Setup-Anleitung](https://librebiz.org/manuals/de/setup)

### Lizenz

Librebiz steht unter der Affero GNU Public License Version 3 und basiert
ausschliesslich auf freier Software. 
