/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 11, 2016 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.BusinessCase
import com.osserp.core.projects.ProjectSearch

import com.osserp.groovy.web.MenuItem

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ProjectsBySupplierView extends AbstractSalesListReportingView {
    private static Logger logger = LoggerFactory.getLogger(ProjectsBySupplierView.class.getName())
    
    ProjectsBySupplierView() {
        super()
        exportLink = true
        refreshLink = true
        selectors.businessType = 'salesMonitoringProjects'
        selectors.status = true
        values.statusValue = BusinessCase.CLOSED
        env.salesMode = true
        values.closedIgnore = true
        closedIgnoreLinkAvailable = true
    }
    
    @Override
    protected void init() {
        if (forwardRequest) {
            listReferenceId = form.getLong('id')
            logger.debug("init: done [supplier=${listReferenceId}]")
        }
    }
  
    @Override
    void loadList() {
        list = projectSearch.getSubcontractorMonitoring(listReferenceId)
        logger.debug("loadList: done [size=${list.size()}]")
    }
    
    @Override
    MenuItem getSalesListExportLink() {
        new MenuItem(
                link: "${bareExportLink}",
                permissions: 'project_subcontractor_export',
                icon: 'tableIcon',
                title: 'displaySheet')
    }
    
    @Override
    void toggleClosedIgnore() {
        super.toggleClosedIgnore()
        nav.listNavigation.each { MenuItem item ->
            if (item.link.indexOf('subcontractorProjectExport') > -1) {
                item.link = bareExportLink
            }
        }
    }
    
    @Override
    void createCustomNavigation() {
        super.createCustomNavigation()
        nav.defaultNavigation = [exitLink, closedIgnoreLink, homeLink] 
    }

    private String getBareExportLink() {
        "${context.contextPath}/output/suppliers/subcontractorProjectExport/forward?id=${listReferenceId}&ignoreClosed=${values.closedIgnore}"
    }

    private ProjectSearch getProjectSearch() {
        getService(ProjectSearch.class.getName())
    }

}
