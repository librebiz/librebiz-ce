/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.requests;

import java.util.Comparator;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestListItemComparators {
    private static Logger log = LoggerFactory.getLogger(RequestListItemComparators.class.getName());

    public static Comparator createComparator(String comparator, boolean descendent) {
        if (comparator != null) {
            if ("capacity".equals(comparator)) {
                return createRequestListItemByCapacityComparator(descendent);
            }
            if ("created".equals(comparator)) {
                return createRequestListItemByCreatedComparator(descendent);
            }
            if ("lastNoteDate".equals(comparator)) {
                return createRequestListItemByLastNoteDateComparator(descendent);
            }
            if ("name".equals(comparator)) {
                return createRequestListItemByNameComparator(descendent);
            }
            if ("orderProbability".equals(comparator)) {
                return createRequestListItemByOrderProbabilityComparator(descendent);
            }
            if ("orderProbabilityDate".equals(comparator)) {
                return createRequestListItemByOrderProbabilityDateComparator(descendent);
            }
            if ("status".equals(comparator)) {
                return createRequestListItemByStatusComparator(descendent);
            }
            if ("requestType".equals(comparator)) {
                return createRequestListItemByRequestTypeComparator(descendent);
            }
            log.warn("createComparator() invoked for unsupported comparator [name=" + comparator + "]");
        }
        return createRequestListItemByOrderProbabilityDateComparator();
    }

    public static Comparator createRequestListItemByCapacityComparator(boolean descendent) {
        if (descendent) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RequestListItem itemA = (RequestListItem) a;
                    RequestListItem itemB = (RequestListItem) b;
                    return itemA.getCapacity().compareTo(itemB.getCapacity()) * (-1);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RequestListItem itemA = (RequestListItem) a;
                RequestListItem itemB = (RequestListItem) b;
                return itemA.getCapacity().compareTo(itemB.getCapacity());
            }
        };
    }

    public static Comparator createRequestListItemByCreatedComparator(boolean descendent) {
        if (descendent) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RequestListItem itemA = (RequestListItem) a;
                    RequestListItem itemB = (RequestListItem) b;
                    return itemA.getCreated().compareTo(itemB.getCreated()) * (-1);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RequestListItem itemA = (RequestListItem) a;
                RequestListItem itemB = (RequestListItem) b;
                return itemA.getCreated().compareTo(itemB.getCreated());
            }
        };
    }

    public static Comparator createRequestListItemByLastNoteDateComparator(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RequestListItem itemA = (RequestListItem) a;
                    RequestListItem itemB = (RequestListItem) b;
                    Date dateA = itemA.getLastNoteDate();
                    Date dateB = itemB.getLastNoteDate();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RequestListItem itemA = (RequestListItem) a;
                RequestListItem itemB = (RequestListItem) b;
                Date dateA = itemA.getLastNoteDate();
                Date dateB = itemB.getLastNoteDate();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static Comparator createRequestListItemByNameComparator(boolean descendent) {
        if (descendent) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RequestListItem itemA = (RequestListItem) a;
                    RequestListItem itemB = (RequestListItem) b;
                    return itemA.getName().compareTo(itemB.getName()) * (-1);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RequestListItem itemA = (RequestListItem) a;
                RequestListItem itemB = (RequestListItem) b;
                return itemA.getName().compareTo(itemB.getName());
            }
        };
    }

    public static Comparator createRequestListItemByOrderProbabilityComparator(boolean descendent) {
        if (descendent) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RequestListItem itemA = (RequestListItem) a;
                    RequestListItem itemB = (RequestListItem) b;
                    Integer opA = itemA.getOrderProbability();
                    Integer opB = itemB.getOrderProbability();
                    return opA.compareTo(opB) * (-1);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RequestListItem itemA = (RequestListItem) a;
                RequestListItem itemB = (RequestListItem) b;
                Integer opA = itemA.getOrderProbability();
                Integer opB = itemB.getOrderProbability();
                return opA.compareTo(opB);
            }
        };
    }

    public static Comparator createRequestListItemByOrderProbabilityDateComparator(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RequestListItem itemA = (RequestListItem) a;
                    RequestListItem itemB = (RequestListItem) b;
                    Date dateA = itemA.getOrderProbabilityDate();
                    Date dateB = itemB.getOrderProbabilityDate();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RequestListItem itemA = (RequestListItem) a;
                RequestListItem itemB = (RequestListItem) b;
                Date dateA = itemA.getOrderProbabilityDate();
                Date dateB = itemB.getOrderProbabilityDate();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static Comparator createRequestListItemByOrderProbabilityDateComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                RequestListItem itemA = (RequestListItem) a;
                RequestListItem itemB = (RequestListItem) b;
                Date dateA = itemA.getOrderProbabilityDate();
                Date dateB = itemB.getOrderProbabilityDate();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static Comparator createRequestListItemByStatusComparator(boolean descendent) {
        if (descendent) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RequestListItem itemA = (RequestListItem) a;
                    RequestListItem itemB = (RequestListItem) b;
                    return itemA.getStatus().compareTo(itemB.getStatus()) * (-1);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RequestListItem itemA = (RequestListItem) a;
                RequestListItem itemB = (RequestListItem) b;
                return itemA.getStatus().compareTo(itemB.getStatus());
            }
        };
    }

    public static Comparator createRequestListItemByRequestTypeComparator(boolean descendent) {
        if (descendent) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RequestListItem itemA = (RequestListItem) a;
                    RequestListItem itemB = (RequestListItem) b;
                    String typeA = itemA.getType().getKey();
                    String typeB = itemB.getType().getKey();
                    return typeA.compareTo(typeB) * (-1);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RequestListItem itemA = (RequestListItem) a;
                RequestListItem itemB = (RequestListItem) b;
                String typeA = itemA.getType().getKey();
                String typeB = itemB.getType().getKey();
                return typeA.compareTo(typeB);
            }
        };
    }
}
