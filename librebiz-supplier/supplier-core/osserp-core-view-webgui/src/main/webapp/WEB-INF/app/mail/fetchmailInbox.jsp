<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="fetchmailInboxView" />
<c:set var="fetchmailInboxExitTarget" scope="request" value="fetchmailInbox" />
<c:set var="displayUserColumn" scope="request" value="true" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
        <c:if test="${view.adminMode and not empty view.selectedUser}">
            <span> - </span><oc:employee value="${view.selectedUser}" />
        </c:if>
        <span> - <o:out value="${view.listCount}" /> <fmt:message key="emails" /></span>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="fetchmailInboxContent">
            <div class="row">
                <c:choose>
                    <c:when test="${empty view.bean}">
                        <c:import url="_fetchmailInboxList.jsp" />
                    </c:when>
                    <c:when test="${view.editMode}">
                        <v:form url="${view.baseLink}/save" name="fetchmailInboxForm">
                            <c:import url="_fetchmailInboxEdit.jsp" />
                        </v:form>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_fetchmailInboxText.jsp" />
                        <c:import url="_fetchmailInboxDisplay.jsp" />
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
