/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 16, 2011 1:44:51 PM 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.util.JsonObject
import com.osserp.common.util.JsonUtil

import com.osserp.core.contacts.ZipcodeSearchManager

import com.osserp.gui.CoreView

/**
 *
 * @author jg <jg@osserp.com>
 * 
 */
class ZipcodeSearchPopupView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ZipcodeSearchPopupView.class.getName())

    private static String SEARCH_URL = 'http://www.deutschepost.de/plzsuche/PlzAjaxServlet'

    def resultType = ''

    def finda = 'city'
    def street = ''
    def zipcode = ''
    def city = ''

    def callFrom
    def contactId

    ZipcodeSearchPopupView() {
        super()
        enablePopupView()
        headerName = 'zipcodeSearch'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!callFrom && !contactId) {
            callFrom = form.getString('callFrom')
            contactId = form.getLong('contactId')
        }
    }

    @Override
    void save() {
        list = []
        resultType = ''
        street = form.getString('street') ?: ''
        zipcode = form.getString('zipcode') ?: ''
        city = form.getString('city') ?: ''

        def findaTemp = form.getString('finda')
        if (findaTemp != finda) {
            finda = findaTemp
            return
        } else if (finda == 'city') {
            list = findCityByZipcode(zipcode)
        } else if (finda == 'plz') {
            list = findZipcodeByAddress(street, city)
        }
    }

    private List parseResult(String result) {
        logger.debug("parseResult: invoked, data:\n${result}\n")
        List results = []
        try {
            JsonObject json = JsonUtil.parse(result)
            if (json.getInt('count') == 0) {
                resultType = 'none'
            } else {
                JsonObject header = json.getJsonObject('header')
                JsonObject header2 = header.getJsonObject('2')
                JsonObject header3 = header.getJsonObject('3')
                if (header3.containsValue('street') || header2.containsValue('street')) {
                    resultType = 'street'
                } else if (header2.containsValue('district')) {
                    resultType = 'district'
                } else if (header2.containsValue('cityaddition')) {
                    resultType = 'cityaddition'
                } else {
                    resultType = 'city'
                }
                List<JsonObject> rows = json.getJsonList('rows')
                if (rows) {
                    for (int i = 0; i < rows.size(); i++) {
                        JsonObject row = rows.get(i)
                        results << new ZipcodeSearchResult(
                                id: i+1,
                                zipcode: row.getString('plz'),
                                city: row.getString('city'),
                                district: row.getString('district'),
                                street: row.getString('street'),
                                number: row.getString('number'),
                                cityaddition: row.getString('cityaddition'))
                    }
                }
            }
        } catch (Exception e) {
            throw new ClientException(ErrorCode.ZIPCODE_SEARCH)
        }
        return results
    }

    private List findZipcodeByAddress(String street, String city) {
        logger.debug("findZipcodeByAddress: invoked [street=$street, city=$city]")

        String data = URLEncoder.encode("finda", "UTF-8") + "=" + URLEncoder.encode("plz", "UTF-8")
        data += "&" + URLEncoder.encode("plz_street", "UTF-8") + "=" + URLEncoder.encode(street, "UTF-8")
        data += "&" + URLEncoder.encode("plz_city", "UTF-8") + "=" + URLEncoder.encode(city, "UTF-8")
        data += "&" + URLEncoder.encode("lang", "UTF-8") + "=" + URLEncoder.encode("de_DE", "UTF-8")

        String result = zipcodeSearchManager.callZipcodeServlet(data)
        zipcodeSearchManager.saveSearch(domainEmployee, callFrom, contactId, 'zipcode', null, city, street, null)
        return parseResult(result)
    }

    private List findCityByZipcode(String zipcode) {
        logger.debug("findCityByZipcode: invoked [zipcode=$zipcode]")

        String data = URLEncoder.encode("finda", "UTF-8") + "=" + URLEncoder.encode("city", "UTF-8")
        data += "&" + URLEncoder.encode("city", "UTF-8") + "=" + URLEncoder.encode(zipcode, "UTF-8")
        data += "&" + URLEncoder.encode("lang", "UTF-8") + "=" + URLEncoder.encode("de_DE", "UTF-8")

        String result = zipcodeSearchManager.callZipcodeServlet(data)
        zipcodeSearchManager.saveSearch(domainEmployee, callFrom, contactId, 'city', zipcode, null, null, null)
        return parseResult(result)
    }

    private List findStreets(String zipcode, String city, String district) {
        logger.debug("findStreets: invoked [zipcode=$zipcode, city=$city, district=$district]")

        String data = URLEncoder.encode("finda", "UTF-8") + "=" + URLEncoder.encode("streets", "UTF-8")
        data += "&" + URLEncoder.encode("plz_city", "UTF-8") + "=" + URLEncoder.encode(city, "UTF-8")
        data += "&" + URLEncoder.encode("plz_plz", "UTF-8") + "=" + URLEncoder.encode(zipcode, "UTF-8")
        data += "&" + URLEncoder.encode("plz_district", "UTF-8") + "=" + URLEncoder.encode(district, "UTF-8")
        data += "&" + URLEncoder.encode("lang", "UTF-8") + "=" + URLEncoder.encode("de_DE", "UTF-8")

        String result = zipcodeSearchManager.callZipcodeServlet(data)
        zipcodeSearchManager.saveSearch(domainEmployee, callFrom, contactId, 'street', zipcode, city, null, district)
        return parseResult(result)
    }

    protected ZipcodeSearchManager getZipcodeSearchManager() {
        getService(ZipcodeSearchManager.class.getName())
    }

    @Override
    void select() {
        super.select()
        list = findStreets(bean.zipcode, bean.city, bean.district)
        bean = null
    }
}

