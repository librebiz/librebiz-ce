/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 10, 2006 3:02:12 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;
import java.util.List;

import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.BusinessCaseSearchRequest;
import com.osserp.core.finance.ReceivableDisplay;
import com.osserp.core.projects.results.ProjectByActionDate;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.sales.SalesListProductItem;
import com.osserp.core.sales.SalesMonitoringItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectQueries {

    /**
     * Finds sales by pattern and settings of provided search request.
     * This is the main search method for sales 
     * @param request
     * @return list of sales or empty list.
     */
    List<SalesListItem> find(BusinessCaseSearchRequest request);

    /**
     * Provides all projects with deliveries
     * @return pending deliveries
     */
    List<SalesListProductItem> getProjectsByDelivery();

    /**
     * Provides all projects with pending installation
     * @return pending installations
     */
    List<SalesListItem> getProjectsByMounting();

    /**
     * Provides all sales with receivables
     * @return byReceivables
     */
    List<ReceivableDisplay> getByReceivables();

    /**
     * Provides all sales where all items are delivered and no final invoice exists
     * @return byMissingInvoices
     */
    List<SalesListItem> getByMissingInvoices();

    /**
     * Provides a list of all projects of given type where the given action was performed in the specified time period.
     * @param projectType
     * @param actionId
     * @param from
     * @param til
     * @param withCanceled includes canceled projects
     * @param withClosed includes closed projects
     * @param zipcode
     * @return byActionAndDate
     */
    List<ProjectByActionDate> getByActionAndDate(
            Long projectType,
            Long actionId,
            Date from,
            Date til,
            boolean withCanceled,
            boolean withClosed,
            String zipcode);

    /**
     * Provides a list of all projects of given type where the given action was not performed
     * @param projectType
     * @param missingAction
     * @param withAction
     * @param withCanceled includes canceled projects
     * @param withClosed includes closed projects
     * @return byMissingAction
     */
    List<ProjectByActionDate> getByMissingAction(
            Long projectType,
            Long missingAction,
            Long withAction,
            boolean withCanceled,
            boolean withClosed);

    /**
     * Provides all projects related to given manager
     * @param managerId
     * @param openOnly
     * @return byManager
     */
    List<SalesListItem> getByManager(Long managerId, boolean openOnly);

    /**
     * Provides all projects related to given salesperson
     * @param salesId
     * @param openOnly
     * @return bySalesperson
     */
    List<SalesListItem> getBySalesperson(Long salesId, boolean openOnly);

    /**
     * Provides all projects with existing -final- invoice
     * @param managerId
     * @param openOnly
     * @return byExistingInvoice
     */
    List<SalesListItem> getByExistingInvoice(Date from, Date til);

    /**
     * Provides all sales with activated cancelled flag
     * @return byCancelledStatus
     */
    List<SalesListItem> getByCancelledStatus();

    /**
     * Provides all sales with activated cancelled flag by year
     * @param year
     * @return byCancelledStatus
     */
    List<SalesListItem> getByCancelledStatus(int year);

    /**
     * Provides all sales with activated stop flag
     * @return byStoppedStatus
     */
    List<SalesListItem> getByStoppedStatus();

    /**
     * Provides all sales with activated stop flag by year
     * @param year
     * @return byStoppedStatus
     */
    List<SalesListItem> getByStoppedStatus(int year);

    /**
     * Provides all projects created in given month of given year
     * @param month (0-11)
     * @param year
     * @return createdInMonth
     */
    List<SalesListItem> getCreatedInMonth(int month, int year);

    /**
     * Provides all projects created in given year
     * @param year
     * @return createdInYear
     */
    List<SalesListItem> getCreatedInYear(int year);

    /**
     * Provides delivery monitoring values
     * @param stockId
     * @param descendant
     * @return deliveryMonitoring
     */
    List<SalesListProductItem> getDeliveryMonitoring(Long stockId, boolean descendant);

    /**
     * Provides sales monitoring by request type selection
     * @param requestTypeSelection
     * @return salesMonitoring
     */
    List<SalesMonitoringItem> getSalesMonitoring(String requestTypeSelection);

    /**
     * Provides open sales monitoring
     * @return salesMonitoring
     */
    List<SalesMonitoringItem> getSalesMonitoring(Long company);

    /**
     * Provides a list if all projects whose last fcs action is longer than given days ago
     * @param maxDaysAgo
     * @return sleeping project items
     */
    List<SalesListItem> getSleepingProjects(int maxDaysAgo);

    /**
     * Looksup for service orders related to provided sales
     * @param salesId
     * @return service sales list
     */
    List<SalesListItem> findServiceSales(Long salesId);

    /**
     * Provides a list of all subcontractors relating to one or more projects.
     * @return subconstractors
     */
    List<BusinessCaseRelationStat> getSubcontractors();

    /**
     * Provides project items by subcontractor.
     * @param contractorId null if all requestet
     * @return project items
     */
    List<SalesMonitoringItem> getSubcontractorMonitoring(Long contractorId);

}
