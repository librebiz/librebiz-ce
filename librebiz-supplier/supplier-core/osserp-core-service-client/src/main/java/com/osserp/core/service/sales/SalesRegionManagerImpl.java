/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 19, 2011 11:46:14 AM 
 * 
 */
package com.osserp.core.service.sales;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.SalesRegions;
import com.osserp.core.employees.Employee;
import com.osserp.core.sales.SalesRegion;
import com.osserp.core.sales.SalesRegionManager;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class SalesRegionManagerImpl extends AbstractService implements SalesRegionManager {
    private static Logger log = LoggerFactory.getLogger(SalesRegionManagerImpl.class.getName());
    private Employees employees;
    private SalesRegions salesRegions;

    protected SalesRegionManagerImpl(SalesRegions salesRegions, Employees employees) {
        super();
        this.salesRegions = salesRegions;
        this.employees = employees;
    }

    public SalesRegion create(Employee user, String name, Employee sales, Employee salesExecutive) throws ClientException {
        this.checkSalesExecutive(salesExecutive);
        return salesRegions.create(user, name, sales, salesExecutive);
    }

    public SalesRegion change(Employee user, SalesRegion region, String name, Employee sales, Employee salesExecutive) throws ClientException {
        this.checkSalesExecutive(salesExecutive);
        boolean changed = false;
        if (sales == null) {
            sales = employees.get(Constants.SYSTEM_EMPLOYEE);
        }
        if (region.getSales() == null || !region.getSales().getId().equals(sales.getId())) {
            region.changeSales(user, sales);
            changed = true;
        }
        if (region.getSalesExecutive() == null || !region.getSalesExecutive().getId().equals(salesExecutive.getId())) {
            region.changeSalesExecutive(user, salesExecutive);
            changed = true;
        }
        if ((region.getName() == null && name != null) || (region.getName() != null && !region.getName().equals(name))) {
            region.setName(name);
            changed = true;
        }
        if (changed) {
            region.setChangedBy(user.getId());
            region.setChanged(new Date(System.currentTimeMillis()));
            this.salesRegions.save(region);
        }
        return salesRegions.getRegion(region.getId());
    }

    public SalesRegion addZipcode(Employee user, SalesRegion region, String zipcode) throws ClientException {
        if (zipcode != null) {
            String zip = zipcode.trim();
            List<SalesRegion> all = findAll();
            for (int i = 0, j = all.size(); i < j; i++) {
                SalesRegion next = all.get(i);
                List<String> zipcodes = next.getZipcodes();
                for (int k = 0, l = zipcodes.size(); k < l; k++) {
                    String nextZip = zipcodes.get(k);
                    if (nextZip.equals(zip)) {
                        throw new ClientException(ErrorCode.REGION_ZIPCODE_EXISTS);
                    }
                }
            }
            region.addZipcode(zip);
            region.setChangedBy(user.getId());
            region.setChanged(new Date(System.currentTimeMillis()));
            this.salesRegions.save(region);
            return salesRegions.getRegion(region.getId());
        }
        return region;
    }

    public SalesRegion removeZipcode(Employee user, SalesRegion region, String zipcode) {
        region.removeZipcode(zipcode);
        region.setChangedBy(user.getId());
        region.setChanged(new Date(System.currentTimeMillis()));
        this.salesRegions.save(region);
        return salesRegions.getRegion(region.getId());
    }

    public List<SalesRegion> findAll() {
        return salesRegions.findAll();
    }

    public SalesRegion findRegion(Long id) {
        try {
            return salesRegions.getRegion(id);
        } catch (Exception e) {
            log.info("findRegion() failed to find region [id=" + id + ", message=" + e.getMessage() + "]");
            return null;
        }
    }

    public void toggleEol(SalesRegion region) {
        region.toggleEol();
        this.salesRegions.save(region);
    }

    public void deleteRegion(Long id) {
        this.salesRegions.delete(id);
    }

    private void checkSalesExecutive(Employee salesExecutive) throws ClientException {
        if (salesExecutive == null) {
            throw new ClientException(ErrorCode.SALESEXECUTIVE_REQUIRED);
        }
    }
}
