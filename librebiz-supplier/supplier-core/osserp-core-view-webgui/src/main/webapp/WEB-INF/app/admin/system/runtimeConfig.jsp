<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="runtimeConfigView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">System Runtime Config</tiles:put>
    <tiles:put name="headline_right">
        <v:link url="/admin" title="backToMenu">
            <o:img name="homeIcon" />
        </v:link>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="runtimeConfigContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped" style="table-layout: fixed;">
                            <tbody>
                                <tr>
                                    <td>com.osserp.common.util.EmailValidator.checkMx</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${view.checkMX}">
                                                <v:link url="/admin/system/runtimeConfig/disableMXCheck">true</v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/admin/system/runtimeConfig/enableMXCheck">false</v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <td>com.osserp.common.util.EmailValidator.checkSmtp</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${view.checkSMTP}">
                                                <v:link url="/admin/system/runtimeConfig/disableSMTPCheck">true</v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/admin/system/runtimeConfig/enableSMTPCheck">false</v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <td>ProductSyncClient</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${requestScope.productSynchronizationActivated}">
                                                syncJobActivated
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/admin/system/runtimeConfig/synchronizePublicProducts">synchronize</v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <td>ProductMediaSyncClient</td>
                                    <td><c:choose>
                                            <c:when test="${requestScope.productMediaSynchronizationActivated}">
                                                syncJobActivated
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/admin/system/runtimeConfig/synchronizePublicProductMedia">synchronize</v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <td>ProductSummaryCache</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${requestScope.productSummaryReloaded}">
                                                reload done
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/admin/system/runtimeConfig/reloadProductSummary">reload</v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <td>OptionsCache</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${requestScope.optionsReload}">
                                                reload done
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/admin/system/runtimeConfig/reloadOptions">reload</v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SalesMonitoringCache</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${requestScope.salesMonitoringCacheReloaded}">
                                                reload done
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/admin/system/runtimeConfig/reloadSalesMonitoringCache">reload</v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SalesRevenueCalculator</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${requestScope.salesRevenueCalculatorConfigReloaded}">
                                                reload done
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/admin/system/runtimeConfig/reloadSalesRevenueCalculatorConfigs">reload</v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <v:form url="/admin/system/runtimeConfig/save">
                            <table class="table table-striped" style="table-layout:fixed;">
                                <tbody>
                                    <tr>
                                        <td>Upload read buffer size in byte. Leave empty for default</td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><input type="text" name="uploadReadBufferSize" value="<o:out value="${applicationScope.uploadReadBufferSize}"/>" class="form-control" /></td>
                                                    <td><input type="submit" value="Submit" class="form-control" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </v:form>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
