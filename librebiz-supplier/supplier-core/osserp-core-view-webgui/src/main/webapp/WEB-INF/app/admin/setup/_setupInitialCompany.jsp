<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="companyData" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="companyName"><fmt:message key="company" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="company" styleClass="form-control" title="companyNameLabelHint" />
                    </div>
                </div>
            </div>

            <c:import url="${viewdir}/contacts/_contactAddressInput.jsp" />

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="website" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="website" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="phone"><fmt:message key="phone" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <oc:selectCountry name="pcountry" styleClass="form-control phone-prefix" type="prefixes" prompt="false" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <v:text name="pprefix" styleClass="form-control" />
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <v:text name="pnumber" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="fax"><fmt:message key="fax" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <oc:selectCountry name="fcountry" styleClass="form-control phone-prefix" type="prefixes" prompt="false" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <v:text name="fprefix" styleClass="form-control" />
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <v:text name="fnumber" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="email"><fmt:message key="email" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="email" styleClass="form-control" />
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

