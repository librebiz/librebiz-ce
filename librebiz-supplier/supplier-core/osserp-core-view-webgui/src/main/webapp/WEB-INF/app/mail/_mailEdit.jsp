<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="writeEmailLabel" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <div id="mailEditor">
                <v:form name="mailForm" url="${view.baseLink}/save">
                    <input type="hidden" name="sendMail" value="false" />

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="mailFrom"><fmt:message key="mailFrom"/></label>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <v:text name="originator" value="${view.mail.originator}" styleClass="form-control" readonly="true" />
                            </div>
                        </div>
                    </div>

                    <c:choose>
                        <c:when test="${view.mailMessageContext.recipientsDisabled}">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="mailTo">
                                        <c:choose>
                                            <c:when test="${view.ignoreFixedRecipients}">
                                                <input type="submit" class="form-control" onclick="enableUpdateOnly('/mail/mailEditor/toggleIgnoreFixedRecipients');"
                                                        value="<fmt:message key="offLabel"/>" title="<fmt:message key="enableFixedRecipientDefaults"/>" />
                                            </c:when>
                                            <c:otherwise>
                                                <input type="submit" class="form-control" onclick="enableUpdateOnly('/mail/mailEditor/toggleIgnoreFixedRecipients');"
                                                        value="<fmt:message key="mailTo"/>" title="<fmt:message key="disableFixedRecipientDefaults"/>" />
                                            </c:otherwise>
                                        </c:choose>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <v:text name="recipients" value="${view.recipients}" styleClass="form-control" readonly="readonly" />
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="mailTo">
                                        <input type="submit" class="form-control" value="<fmt:message key="mailTo"/>" onclick="enableUpdateOnly('/contacts/emailSearch/forward?exit=/mail/mailEditor/addRecipients');" />
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <v:text name="recipients" value="${view.recipients}" styleClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>

                    <c:if test="${!view.mailMessageContext.recipientsCCDisabled}">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="recipientsCC">
                                        <input type="submit" class="form-control" value="CC" onclick="enableUpdateOnly('/contacts/emailSearch/forward?exit=/mail/mailEditor/addRecipientsCC');" />
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <v:text name="recipientsCC" value="${view.recipientsCC}" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${!view.mailMessageContext.recipientsBCCDisabled}">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="recipientsBCC">
                                        <input type="submit" class="form-control" value="BCC" onclick="enableUpdateOnly('/contacts/emailSearch/forward?exit=/mail/mailEditor/addRecipientsBCC');" />
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <v:text name="recipientsBCC" value="${view.recipientsBCC}" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="subject"><fmt:message key="subject" /></label>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <v:text name="subject" value="${view.subject}" styleClass="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <v:textarea styleClass="form-control" name="text" cols="70" rows="12" value="${view.text}" />
                            </div>
                        </div>
                    </div>

                    <c:if test="${view.mailMessageContext.attachmentsEnabled || view.attachmentCount > 0}">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="attachments">
                                    <c:choose>
                                        <c:when test="${view.mailMessageContext.attachmentsEnabled}">
                                            <input type="submit" value="<fmt:message key="attachments"/>" class="form-control" onclick="enableUpdateOnly('/mail/mailAttachment/forward?exit=/mail/mailEditor/reload');" />
                                        </c:when>
                                        <c:otherwise>
                                            <input type="submit" value="<fmt:message key="attachments"/>" class="form-control" disabled="disabled" title="<fmt:message key="addAttachmentDisabled"/>" />
                                        </c:otherwise>
                                    </c:choose>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${view.attachmentCount == 0}">
                                            <fmt:message key="noFilesAttached" />
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${view.attachments}" />
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <div class="row next">
                        <div class="col-md-8">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="submit" value="<fmt:message key="mailSend"/>" onclick="enableSendMail();" class="form-control" />
                            </div>
                        </div>
                    </div>
                </v:form>
            </div>
        </div>
    </div>
</div>
