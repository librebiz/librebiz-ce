/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 9, 2014 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.PurchaseQueries;
import com.osserp.core.purchasing.PurchaseRecordSummary;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseQueriesDao extends AbstractDao implements PurchaseQueries {

    private OptionsCache options = null;
    private String purchaseRecordSummaryView;

    /**
     * Creates a new purchaseQueriesDao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param options
     */
    public PurchaseQueriesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache options) {
        super(jdbcTemplate, tables, sessionFactory);
        this.options = options;
        purchaseRecordSummaryView = getTable("purchaseRecordSummaryView");
    }
    

    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.PurchaseQueries#getAccountBalance(com.osserp.core.suppliers.Supplier)
     */
    public Double getAccountBalance(Supplier supplier) {
        String sql = "SELECT get_supplier_account_balance(?)";
        final Object[] params = { supplier.getId() };
        final int[] types = { Types.BIGINT };
        List<Double> result = (List<Double>) jdbcTemplate.query(
                sql,
                params,
                types,
                getDoubleRowMapper());
        return (empty(result) ? 0d : result.get(0));
    }
    

    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.PurchaseQueries#getSummary(com.osserp.core.suppliers.Supplier)
     */
    public List<PurchaseRecordSummary> getSummary(Supplier supplier) {
        StringBuilder sql = new StringBuilder(PurchaseRecordSummaryRowMapper.SELECT_FROM);
        sql.append(purchaseRecordSummaryView).append(" WHERE supplier = ? ORDER BY created desc");
        final Object[] params = { supplier.getId() };
        final int[] types = { Types.BIGINT };
        return (List<PurchaseRecordSummary>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                PurchaseRecordSummaryRowMapper.getReader(options));
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.AccountingRecordQueries#getSummaryByDate(java.lang.Long, java.util.Date, java.util.Date)
     */
    public List<PurchaseRecordSummary> getSummaryByDate(Long company, Date startDate, Date stopDate) {
        assert startDate != null && stopDate != null;
        StringBuilder sql = new StringBuilder(PurchaseRecordSummaryRowMapper.SELECT_FROM);
        sql.append(purchaseRecordSummaryView)
            .append(" WHERE company = ? AND created >= ? AND created <= ? ORDER BY created desc");
        final Object[] params = { company, startDate, stopDate };
        final int[] types = { Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP };
        return (List<PurchaseRecordSummary>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                PurchaseRecordSummaryRowMapper.getReader(options));
    }
}
