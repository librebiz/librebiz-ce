<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:choose>
<c:when test="${!empty view.stylesList}"><c:set var="stylesList" value="${view.stylesList}"/></c:when>
<c:when test="${!empty portalView.stylesList}"><c:set var="stylesList" value="${portalView.stylesList}"/></c:when>
</c:choose>
<c:choose>
<c:when test="${!empty stylesList}">
<c:forEach var="stylesName" items="${stylesList}">
<c:import url="/layout/shared/head_${stylesName}.jsp" />
</c:forEach>
</c:when>
<c:otherwise>
<c:import url="/layout/shared/head_bs3.jsp" />
</c:otherwise>
</c:choose>
