/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Apr 14, 2006 2:17:12 AM 
 * 
 */
package com.osserp.gui.client.purchasing;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.products.Product;
import com.osserp.core.sales.Sales;

import com.osserp.gui.client.records.OrderView;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
@ViewName("purchaseOrderView")
public interface PurchaseOrderView extends OrderView, PurchaseRecordView {

    /**
     * Initializes view for orders with given product
     * @param product
     * @param selectedStock
     * @param exitTarget
     */
    public void initDisplay(Product product, Long selectedStock, String exitTarget);

    /**
     * Checks that current order is direct bookable (e.g. without creating delivery notes). This is the case if order only contains none serial aware items.
     * @throws ClientException if check failed
     */
    public void checkDirectInvoiceBooking() throws ClientException;

    /**
     * Provides the product if view is created in product context
     * @return product or null if no product context
     */
    public Product getProduct();

    /**
     * Updates the delivery condition
     * @param form
     */
    public void updateDeliveryCondition(Form form) throws ClientException;

    /**
     * @return true if delivery condition edit mode is enabled
     */
    public boolean isDeliveryConditionEditMode();

    /**
     * sets delivery condition edit mode
     * @param deliveryConditionsEditMode
     */
    public void setDeliveryConditionEditMode(boolean deliveryConditionsEditMode);

    /**
     * Provides the selected stock if init display was invoked with stock param
     * @return selectedStock
     */
    public Long getSelectedStock();

    /**
     * Creates a new purchase order
     * @throws ClientException if no headquarter configured for selected company
     * @throws PermissionException if user is no domain user (e.g. emloyee)
     */
    public void create() throws ClientException, PermissionException;

    /**
     * Creates a new purchase order by copy of current order
     * @throws PermissionException if user is no domain user (e.g. emloyee) or hasn't required permissions
     */
    public void createCopy() throws PermissionException;

    /**
     * Provides the company all following created purchase orders belong to
     * @return company or null if not set
     */
    public Long getCompany();

    /**
     * Sets the company all following created purchase orders belong to
     * @param company id
     */
    public void setCompany(Long company);

    /**
     * Selects the supplier for create actions
     * @param id
     */
    public void selectSupplier(Long id);

    /**
     * Provides referenced sales if available
     * @return sales
     */
    public Sales getReferencedSales();

    /**
     * Overrides delivery address with business case delivery address
     * @throws ClientException
     */
    public void setDeliveryAddressByBusinessCase() throws ClientException;

    /**
     * Resets / deletes delivery address
     */
    public void resetDeliveryAddress();
}
