/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2009 2:25:49 PM 
 * 
 */
package com.osserp.common.web.tags;

import javax.servlet.jsp.JspTagException;

/**
 * 
 * @author eh <eh@osserp.com>
 * 
 */
public class OtherwiseTag extends AbstractTagSupport {

    @Override
    public int doStartTag() throws JspTagException {
        RolesTag parent = null;
        if (!(getParent() instanceof RolesTag)) {
            throw new JspTagException("The otherwise tag mus not be used outside the roles tag");
        }
        parent = (RolesTag) getParent();
        if (parent.showOtherwise()) {
            return EVAL_BODY_INCLUDE;
        }
        return SKIP_BODY;
    }
}
