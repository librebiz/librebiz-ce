<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesCreditNoteView or empty sessionScope.salesCreditNoteView.bean}">
<o:logger write="credit_note_display.jsp no view found" level="warn"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesCreditNoteView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="baseUrl" scope="request" value="/salesCreditNote.do"/>
<c:set var="infoUrl" scope="request" value="/salesCreditNoteInfos.do"/>
<c:set var="productUrl" scope="request" value="/salesCreditNoteProducts.do"/>
<c:set var="productExitTarget" scope="request" value="salesCreditNoteReload"/>
<c:set var="productAddEnabled" scope="request" value="false"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records.css"/>
</style>
</tiles:put>

<tiles:put name="headline">
    <c:import url="/pages/sales/records/voucher_header.jsp"/>
</tiles:put>

<c:choose>
<c:when test="${recordView.setupMode}">
<tiles:put name="headline_right">
<a href="<c:url value="/salesCreditNote.do?method=disableSetup"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>
<tiles:put name="content" type="string">
<c:import url="/pages/sales/records/credit_note_settings.jsp"/>
</tiles:put>
</c:when>

<c:otherwise>

<tiles:put name="headline_right">
<a href="<c:url value="/salesCreditNote.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<o:permission role="accounting,customer_service,executive" info="permissionPaymentAdd">
<v:link url="/sales/salesCreditNotePayment/forward" title="outpaymentAdd"><o:img name="moneyIcon" /></v:link>
</o:permission>
<v:link url="/sales/salesCreditNoteDocument/forward?exit=/salesCreditNote.do?method=reload" title="documents"><o:img name="docIcon" /></v:link>
<c:choose>
<c:when test="${!record.historical}">
<c:if test="${!record.unchangeable and !empty record.items}">
<v:link url="/records/recordPrint/release?view=salesCreditNoteView" confirm="true" message="confirmRecordRelease" title="recordRelease"><o:img name="enabledIcon" /></v:link>
</c:if>
<v:link url="/records/recordPrint/print?view=salesCreditNoteView" title="documentPrint"><o:img name="printIcon" /></v:link>
</c:when>
<c:otherwise>
<c:if test="${!empty recordView.document}">
<c:if test="${record.itemsChangeable or record.itemsEditable}">
<a href="<c:url value="/salesCreditNote.do?method=release"/>" title="<fmt:message key="release"/>"><o:img name="enabledIcon" /></a>
</c:if>
<v:link url="/records/recordPrint/print?view=salesCreditNoteView" title="documentPrint"><o:img name="printIcon" /></v:link>
</c:if>
</c:otherwise>
</c:choose>
<c:choose>
<c:when test="${record.unchangeable and !record.historical}">
<v:link url="/records/recordMail/forward?view=salesCreditNoteView&exit=/salesCreditNote.do?method=reload" title="sendViaEmail"><o:img name="mailIcon" /></v:link>
</c:when>
<c:otherwise>
<o:permission role="accounting,customer_service,executive" info="permissionCancelCreditNote">
<a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmCreditNoteDelete"/>','<c:url value="/salesCreditNote.do?method=delete"/>');" title="<fmt:message key="recordDelete"/>"><o:img name="deleteIcon"/></a>
</o:permission>
<c:if test="${!record.unchangeable or (record.historical and (record.itemsChangeable or record.itemsEditable))}">
<a href="<c:url value="/salesCreditNote.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon"/></a>
</c:if>
</c:otherwise>
</c:choose>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                    
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
                    <div class="column66l">
                        <c:import url="/pages/records/record_sigs_display.jsp"/>
                        <c:import url="/pages/records/record_taxfree_display.jsp"/>
                        <c:import url="/pages/records/record_embedded_text.jsp"/>
                        <c:if test="${!record.type.displayNotesBelowItems}">
                            <c:import url="/pages/records/record_note_display.jsp"/>
                        </c:if>

                        <div class="recordLabelValue">
                            <span><fmt:message key="invoice"/>:
                                <c:choose>
                                    <c:when test="${!empty recordView.invoice}">
                                        <a href="<c:url value="/salesInvoice.do?method=display&id=${recordView.invoice.id}&exit=salesCreditNoteView&readonly=true"/>" title="<fmt:message key="displayInvoice"/>">
                                            <o:out value="${recordView.invoice.number}"/> / <o:date value="${recordView.invoice.created}"/>
                                        </a>
                                    </c:when>
                                    <c:when test="${!empty recordView.relationSuggestions}">
                                        <a href="<c:url value="/salesCreditNote.do?method=enableSetup"/>" title="<fmt:message key="createInvoiceRelation"/>">
                                            <fmt:message key="none"/>
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="none"/>
                                    </c:otherwise>
                                </c:choose>
                            </span>
                        </div>
                        <br />
                    </div>
                    <div class="column33r">
                        <c:import url="/pages/records/record_status.jsp"/>
                        
                        <c:if test="${record.canceled}">
                            <div class="recordLabelValue">
                                <a href="<c:url value="/salesCancellation.do?method=forward&id=${record.id}&exit=salesCreditNoteView&canceledRecordView=salesCreditNoteView"/>"><fmt:message key="displayReverseDocument"/></a>
                            </div>
                        </c:if>
                        
                        <o:permission role="executive,executive_sales" info="permissionChangeCommissionStatus">
                        <c:choose>
                        <c:when test="${record.reducingCommission}">
                            <div class="recordLabelValue">
                                <fmt:message key="provision"/> <a href="<c:url value="/salesCreditNote.do?method=changeCommissionStatus"/>" title="<fmt:message key="change"/>"><fmt:message key="willBeReduced"/></a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="recordLabelValue">
                                <fmt:message key="provision"/> <a href="<c:url value="/salesCreditNote.do?method=changeCommissionStatus"/>" title="<fmt:message key="change"/>"><fmt:message key="willNotBeReduced"/></a>
                            </div>
                        </c:otherwise>
                        </c:choose>
                        </o:permission>
                        
                        <o:forbidden role="executive,executive_sales">
                        <c:choose>
                        <c:when test="${record.reducingCommission}">
                            <div class="recordLabelValue">
                                <fmt:message key="provisionWillBeReduced"/>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="recordLabelValue">
                                <fmt:message key="provisionWillNotBeReduced"/>
                            </div>
                        </c:otherwise>
                        </c:choose>
                        </o:forbidden>
        
                        <c:choose>
                        <c:when test="${recordView.deliveryRequiredFlagChangeable}">
        
                        <c:choose>
                        <c:when test="${record.deliveryExisting}">
                            <div class="recordLabelValue">
                                <fmt:message key="stockItemEntry"/>: <a href="<c:url value="/salesCreditNote.do?method=toggleDeliveryRequiredFlag"/>" title="<fmt:message key="change"/>"><fmt:message key="bigYes"/></a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="recordLabelValue">
                                <fmt:message key="stockItemEntry"/>: <a href="<c:url value="/salesCreditNote.do?method=toggleDeliveryRequiredFlag"/>" title="<fmt:message key="change"/>"><fmt:message key="bigNo"/></a>
                            </div>
                        </c:otherwise>
                        </c:choose>
        
                        </c:when>
                        <c:otherwise>
            
                        <c:choose>
                        <c:when test="${record.deliveryExisting}">
                            <div class="recordLabelValue">
                                <fmt:message key="stockItemEntry"/>: <fmt:message key="bigYes"/>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="recordLabelValue">
                                <fmt:message key="stockItemEntry"/>: <fmt:message key="bigNo"/>
                            </div>
                        </c:otherwise>
                        </c:choose>
        
                        </c:otherwise>
                        </c:choose>
                        <br />
                    </div>
                    <c:import url="/pages/sales/records/credit_note_summary.jsp"/>
                </div>
            </div>
        </div>
 
        <c:import url="/pages/records/record_items.jsp"/>
        <c:if test="${record.type.displayNotesBelowItems}">
            <c:import url="/pages/records/record_note_display.jsp"/>
        </c:if>
        <c:import url="/pages/records/record_conditions.jsp"/>
    </div>
</div>
</tiles:put>
</c:otherwise>
</c:choose>
</tiles:insert>
<c:remove var="recordView" scope="session"/>
