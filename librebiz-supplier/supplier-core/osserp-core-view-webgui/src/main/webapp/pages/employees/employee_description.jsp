<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${contactView.bean}" />
<c:set var="descObj" value="${contact.description}"/>
<div class="contentBox">
	<c:choose>
		<c:when test="${!contactView.descriptionEditMode}">
			<div class="contentBoxHeader">
				<div class="contentBoxHeaderLeft">
					<fmt:message key="description"/>
				</div>
				<div class="contentBoxHeaderRight">
					<div class="boxnav">
						<ul>
							<c:if test="${!contact.systemAccount and internetConfigPermission and !contactView.editMode and !contactView.detailsEditMode}">
								<li><a href="<c:url value="/employees.do?method=enableDescriptionEdit"/>" title="<fmt:message key="changeDescription"/>"><o:img name="writeIcon" /></a></li>
							</c:if>
						</ul>
					</div>
				</div>
			</div>
			<div class="contentBoxData">
				<div class="subcolumns">
					<div class="subcolumn">
						<div class="smallSpacer"></div>
						<table class="valueTable first33">
							<tbody>
								<tr>
									<td style="text-align: left;">
										<c:choose>
											<c:when test="${empty contactView.picture}">
												<img src="<c:url value="/img/contactplaceholder.jpg"/>" title="<fmt:message key="noPictureAvailableYet"/>" style="height:${contactView.pictureHeight};width:auto;"/>
												<br/><br/>
												<c:if test="${!contact.systemAccount and internetConfigPermission}">
												    <v:link url="/employees/employeePicture/forward?exit=/employees.do?method=reload" title="changePicture"><fmt:message key="uploadPicturePrompt"/></v:link>
												</c:if>
											</c:when>
											<c:otherwise>
												<img src="<oc:img value="${contactView.picture}"/>" style="height:${contactView.pictureHeight};width:auto;"/>
                                                <br/>
												<c:if test="${!contact.systemAccount and internetConfigPermission}">
                                                    <v:link url="/employees/employeePicture/forward?exit=/employees.do?method=reload" title="changePicture"><fmt:message key="pictures"/></v:link>
												</c:if>
											</c:otherwise>
										</c:choose>
									</td>
									<td>
										<c:if test="${!empty descObj.name}">
											Job: <o:out value="${descObj.name}"/><br /><br />
										</c:if>
										<c:choose>
											<c:when test="${empty descObj or empty descObj.description}">
												<fmt:message key="noDescriptionAvailable"/>
											</c:when>
											<c:otherwise>
												<o:out value="${descObj.description}"/>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="smallSpacer"></div>
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="contentBox">
				<div class="contentBoxHeader">
					<div class="contentBoxHeaderLeft">
						<fmt:message key="description"/>
					</div>
				</div>
				<div class="contentBoxData">
					<div class="subcolumns">
						<div class="subcolumn">
							<div class="smallSpacer"></div>
							<o:form name="employeeForm" url="/employees.do">
								<table class="valueTable first33">
									<tbody>
										<tr>
											<td>
												<c:choose>
													<c:when test="${empty contactView.picture}">
														<fmt:message key="noPictureAvailableYet"/>
													</c:when>
													<c:otherwise>
														<img src="<oc:img value="${contactView.picture}"/>" style="width:100px;"/>
													</c:otherwise>
												</c:choose>
											</td>
											<td>
												<input type="hidden" name="method" value="updateDescription"/>
												<input type="text" name="job" class="longText" value="<o:out value="${descObj.name}"/>"/>
												<div class="smallSpacer"></div>
												<textarea class="form-control" name="description" rows="6"><o:out value="${descObj.description}"/></textarea>
											</td>
										</tr>
                                        <tr>
                                            <td class="row-submit">
                                                <a href="<c:url value="/employees.do?method=disableDescriptionEdit"/>"><input type="button" value="<fmt:message key="exit"/>" class="cancel"/></a>
                                            </td>
                                            <td class="row-submit">
                                                <o:submit/>
                                            </td>
                                        </tr>
									</tbody>
								</table>
							</o:form>
							<div class="smallSpacer"></div>
						</div>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</div>
<div class="spacer"></div>
