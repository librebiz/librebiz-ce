/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 11, 2006 2:19:26 PM 
 * 
 */
package com.osserp.common.dms.dao.impl;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.springframework.jdbc.core.JdbcTemplate;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.dao.DocumentReference;
import com.osserp.common.dms.dao.DocumentStorage;

/**
 * 
 * Persists binary data to a database.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DatabaseDocumentStorage extends AbstractTablesAwareSpringDao implements DocumentStorage {

    private String binaryTable = null;

    public DatabaseDocumentStorage(JdbcTemplate jdbcTemplate, Tables tables, String binaryTableKey) {
        super(jdbcTemplate, tables);
        binaryTable = getTable(binaryTableKey);
    }

    public boolean isAvailable(DocumentReference documentReference) {
        Long docId = (Long) documentReference.getObject();
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT count(*) FROM ")
                .append(binaryTable)
                .append(" WHERE doc_id = ?");
        final Object[] params = { docId };
        final int[] types = { Types.BIGINT };
        return (jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0);
    }

    public byte[] getBinary(DocumentReference documentReference) throws ClientException {
        Long docId = (Long) documentReference.getObject();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT document FROM ")
                .append(binaryTable)
                .append(" WHERE doc_id = ?");
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setLong(1, docId.longValue());
            rs = ps.executeQuery();
            if (rs.next()) {
                InputStream stream = rs.getBinaryStream(1);
                if (stream != null) {
                    byte[] bytes = new byte[stream.available()];
                    stream.read(bytes);
                    return bytes;
                }
            }
            throw new ClientException();

        } catch (Exception x) {
            throw new BackendException(x.getMessage(), x);
        } finally {
            close(rs, ps, con);
        }
    }

    public boolean removeBinary(DocumentReference documentReference) {
        Long docId = (Long) documentReference.getObject();
        StringBuilder sql = new StringBuilder();
        sql
                .append("DELETE FROM ")
                .append(binaryTable)
                .append(" WHERE doc_id = ?");
        final Object[] params = { docId };
        final int[] types = { Types.BIGINT };
        return (jdbcTemplate.update(sql.toString(), params, types) > 0);
    }
    
    public boolean storeBinary(DocumentReference documentReference, byte[] bytes) {
        if (isAvailable(documentReference)) {
            return updateBinary(documentReference, bytes);
        }
        return createBinary(documentReference, bytes);
    }

    protected boolean createBinary(DocumentReference documentReference, byte[] bytes) {
        Long docId = (Long) documentReference.getObject();
        Connection con = null;
        PreparedStatement ps = null;
        StringBuilder sql = new StringBuilder();
        sql
                .append("INSERT INTO ")
                .append(binaryTable)
                .append(" (doc_id,document) VALUES (?,?)");

        try {
            con = jdbcTemplate.getDataSource().getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setLong(1, docId.longValue());
            ps.setBytes(2, bytes);
            return (ps.executeUpdate() > 0);
        } catch (SQLException x) {
            throw new BackendException(x.getMessage(), x);
        } finally {
            close(null, ps, con);
        }
    }

    protected boolean updateBinary(DocumentReference documentReference, byte[] bytes) {
        Long docId = (Long) documentReference.getObject();
        Connection con = null;
        PreparedStatement ps = null;
        StringBuffer sql = new StringBuffer();
        sql
                .append("UPDATE ")
                .append(binaryTable)
                .append(" SET document = ? WHERE doc_id = ?");
        try {
            con = jdbcTemplate.getDataSource().getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setBytes(1, bytes);
            ps.setLong(2, docId.longValue());
            return (ps.executeUpdate() > 0);

        } catch (SQLException x) {
            throw new BackendException(x.getMessage(), x);
        } finally {
            close(null, ps, con);
        }
    }
}
