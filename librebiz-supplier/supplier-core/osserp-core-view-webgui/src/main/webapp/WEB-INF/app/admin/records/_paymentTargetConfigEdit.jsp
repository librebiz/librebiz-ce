<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${view.createMode}">
                        <fmt:message key="createNewPaymentTarget" />
                    </c:when>
                    <c:otherwise>
                        <o:out value="${view.bean.name}" />
                    </c:otherwise>
                </c:choose>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name"><fmt:message key="name" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="name" value="${view.bean.name}" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="paymentTarget"><fmt:message key="paymentTarget" /></label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <v:text name="paymentTarget" value="${view.bean.paymentTarget}" styleClass="form-control right" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-hint">
                            <fmt:message key="daysLabel" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="paid"><fmt:message key="paid" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="paid"> <v:checkbox id="paid" /> <fmt:message key="pointOfAccounting" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <c:if test="${!view.createMode}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="donation"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="donation">
                                    <v:checkbox id="donation" /> <fmt:message key="donation" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="discount"><fmt:message key="discount" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="donation">
                                <v:checkbox name="withDiscount" value="${view.bean.withDiscount}" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="discountOne"><fmt:message key="discountOne" /></label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <v:text name="discount1" value="${view.bean.discount1}" styleClass="form-control right" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-hint">&percnt;</div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="paymentTargetDiscountOne"><fmt:message key="paymentTargetDiscountOne" /></label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <v:text name="discount1Target" value="${view.bean.discount1Target}" styleClass="form-control right" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-hint">
                            <fmt:message key="daysLabel" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="discountTwo"><fmt:message key="discountTwo" /></label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <v:text name="discount2" value="${view.bean.discount2}" styleClass="form-control right" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-hint">&percnt;</div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="paymentTargetDiscountTwo"><fmt:message key="paymentTargetDiscountTwo" /></label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <v:text name="discount2Target" value="${view.bean.discount2Target}" styleClass="form-control right" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-hint">
                            <fmt:message key="daysLabel" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${view.createMode}">
                                        <v:submitExit url="${view.baseLink}/disableCreateMode"/>
                                    </c:when>
                                    <c:otherwise>
                                        <v:submitExit url="${view.baseLink}/disableEditMode"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
