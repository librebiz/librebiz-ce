<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="url" value="/loadSales.do" />
<c:if test="${!empty requestScope.selectUrl}">
    <c:set var="url" value="${requestScope.selectUrl}" />
</c:if>
<c:if test="${!empty requestScope.selectExitTarget}">
    <c:set var="exitTarget" value="${requestScope.selectExitTarget}" />
</c:if>
<c:if test="${!empty requestScope.selectMethod}">
    <c:set var="method" value="${requestScope.selectMethod}" />
</c:if>

<c:set var="list" value="${requestScope.collection}" />

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="center"><fmt:message key="type" /></th>
                    <th><v:sortLink key="id"><fmt:message key="number" /></v:sortLink></th>
                    <th><v:sortLink key="name"><fmt:message key="commission" /></v:sortLink></th>
                    <th class="right"><v:sortLink key="capacity" title="inEuro"><fmt:message key="turnover" /></v:sortLink></th>
                    <th class="center"><v:sortLink key="branchKey"><fmt:message key="branchOfficeShortcut" /></v:sortLink></th>
                    <th class="center"><v:sortLink key="salesKey"><fmt:message key="salesShortcut" /></v:sortLink></th>
                    <th class="center"><v:sortLink key="managerKey"><fmt:message key="projectorShort" /></v:sortLink></th>
                    <th class="center"><v:sortLink key="created"><fmt:message key="order" /></v:sortLink></th>
                    <th class="right"><v:sortLink key="status">%</v:sortLink></th>
                    <th class="center" colspan="3"><fmt:message key="info"/></th>
                </tr>
            </thead>
            <tbody>
                <c:if test="${empty list}">
                    <tr>
                        <td colspan="12"><fmt:message key="adviceNoActualOrdersFound" /></td>
                    </tr>
                </c:if>
                <c:forEach var="sales" items="${list}" varStatus="s">
                    <c:set var="nextSelectionUrl">
                        <c:url value="${url}">
                            <c:if test="${!empty exitTarget}">
                                <c:param name="exit" value="${exitTarget}" />
                            </c:if>
                            <c:if test="${!empty method}">
                                <c:param name="method" value="${method}" />
                            </c:if>
                            <c:param name="id" value="${sales.id}" />
                            <c:param name="exitId" value="sales${sales.id}" />
                        </c:url>
                    </c:set>
                    <tr id="sales${sales.id}"<c:if test="${sales.stopped}"> class="red"</c:if>>
                        <td class="center"><o:out value="${sales.type.key}" /></td>
                        <td>
                            <a href="<o:out value="${nextSelectionUrl}"/>" title="<o:out value="${sales.type.name}"/>">
                                <span <c:if test="${sales.status < 15}"> style="font-style:italic;"</c:if>><o:out value="${sales.id}" /></span>
                            </a>
                        </td>
                        <td>
                            <a href="<o:out value="${nextSelectionUrl}"/>" title="<o:out value="${sales.customerName}"/>">
                                <span <c:if test="${sales.status < 15}"> style="font-style:italic;"</c:if>><o:out value="${sales.name}" limit="50" /></span>
                            </a>
                        </td>
                        <td class="right"><o:number value="${sales.capacity}" format="currency" /></td>
                        <td class="center"><o:out value="${sales.branchKey}" limit="6" /></td>
                        <td class="center">
                            <c:choose>
                                <c:when test="${!empty sales.salesId and sales.salesId > 0}">
                                    <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${sales.salesId}">
                                        <oc:employee initials="true" value="${sales.salesId}" />
                                    </v:ajaxLink>
                                </c:when>
                                <c:otherwise>
                                    &nbsp;
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="center">
                            <c:choose>
                                <c:when test="${!empty sales.managerId and sales.managerId > 0}">
                                    <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${sales.managerId}">
                                        <oc:employee initials="true" value="${sales.managerId}" />
                                    </v:ajaxLink>
                                </c:when>
                                <c:otherwise>
                                    &nbsp;
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="center"><o:date value="${sales.created}" casenull="-" addcentury="false" /></td>
                        <td class="status right">
                            <v:ajaxLink url="/sales/flowControlDisplay/forward?id=${sales.id}" styleClass="boldtext" linkId="flowControlDisplayView">
                                <o:out value="${sales.status}" />
                            </v:ajaxLink>
                        </td>
                        <td class="icon center">
                            <o:permission role="notes_sales_deny" info="permissionNotes">
                                <v:ajaxLink url="/sales/businessCaseDisplay/forward?notes=true&id=${sales.id}" linkId="businessCaseDisplayView">
                                    <o:img name="texteditIcon" />
                                </v:ajaxLink>
                            </o:permission>
                        </td>
                        <td class="icon center">
                            <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${sales.id}" linkId="businessCaseDisplayView">
                                <o:img name="openFolderIcon" />
                            </v:ajaxLink>
                        </td>
                        <td class="icon center">
                            <c:choose>
                                <c:when test="${!sales.released}">
                                    <span title="<fmt:message key="orderNotReleased"/>"><o:img name="importantDisabledIcon" /></span>
                                </c:when>
                                <c:when test="${sales.deliveryClosed}">
                                    <span title="<fmt:message key="materialPlanningFinished"/>"><o:img name="importantDisabledIcon" /></span>
                                </c:when>
                                <c:otherwise>
                                    <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${sales.id}" linkId="deliveryStatus" title="materialPlanning">
                                        <o:img name="importantIcon" />
                                    </v:ajaxLink>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
