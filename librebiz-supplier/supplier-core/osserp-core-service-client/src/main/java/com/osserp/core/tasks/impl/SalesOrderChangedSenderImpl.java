/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.tasks.impl;

import com.osserp.common.service.MessageSender;
import com.osserp.core.employees.Employee;
import com.osserp.core.sales.Sales;
import com.osserp.core.tasks.SalesOrderChangedSender;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderChangedSenderImpl extends AbstractOrderChangedSender implements SalesOrderChangedSender {

    /**
     * Creates a new sale order changed sender
     * @param sender
     * @param destinationQueue
     */
    public SalesOrderChangedSenderImpl(MessageSender sender, String destinationQueue) {
        super(sender, destinationQueue, "salesOrderChangedTask");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.tasks.impl.AbstractOrderChangedSender#getDeliveryDateChangeEventName()
     */
    @Override
    protected String getDeliveryDateChangeEventName() {
        return "salesOrderDeliveryDateChanged";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.tasks.impl.AbstractOrderChangedSender#getItemChangeEventName()
     */
    @Override
    protected String getItemChangeEventName() {
        return "salesOrderItemChanged";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.tasks.SalesOrderChangedSender#sendSalesUpdateEvent(com.osserp.core.employees.Employee, com.osserp.core.sales.Sales,
     * java.lang.String)
     */
    public void sendSalesUpdateEvent(Employee user, String eventChangedName, Sales sales, String message) {
        OrderChangedMessage msg = new OrderChangedMessage(user, eventChangedName, sales, message);
        send(msg);
    }
}
