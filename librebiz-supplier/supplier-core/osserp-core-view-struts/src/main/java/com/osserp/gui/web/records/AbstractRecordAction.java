/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 22, 2006 4:24:11 PM 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;

import com.osserp.core.finance.Record;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.purchasing.PurchaseRecordView;
import com.osserp.gui.client.records.DiscountAwareRecordView;
import com.osserp.gui.client.records.RebateAwareRecordView;
import com.osserp.gui.client.records.RecordView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecordAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(AbstractRecordAction.class.getName());
    protected static final Long PAYMENT_DELETE_ACTION = -1L;

    /**
     * Fetches the selected record id, loads record and forwards to 'display' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward display(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("display() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.fetchId(request);
        try {
            String exitId = RequestUtil.getExitId(request);
            String exitTarget = RequestUtil.getString(request, "exit");
            String target = RequestUtil.getString(request, "target");
            boolean readonly = RequestUtil.getBoolean(request, "readonly");
            RecordView view = (RecordView) createView(request);
            view.loadRecord(id, exitTarget, exitId, readonly);
            if (isSet(target)) {
                view.setSelectionTarget(target);
            }
            return mapping.findForward(view.getActionTarget());
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("display() failed [id=" + id + ", message=" + getUser(session).getId() + "]");
            }
            return saveErrorAndReturn(request, ErrorCode.RECORD_NOT_AVAILABLE);
        }
    }

    /**
     * Forwards to order edit form
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward select(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("select() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        String exitId = RequestUtil.getExitId(request);
        String exitTarget = RequestUtil.getExit(request);
        RecordView view = fetchRecordView(session);
        if (view == null) {
            if (log.isDebugEnabled()) {
                log.debug("select() no view found; trying to create " +
                        "new readonly view for record [" + id + "]");
            }
            view = (RecordView) createView(request);
            view.loadRecord(id, exitTarget, exitId, true);
            return mapping.findForward(view.getActionTarget());
        }
        view.setSelection(id);
        if (exitTarget != null) {
            if (log.isDebugEnabled()) {
                log.debug("select() exit target found [name=" + exitTarget + "]");
            }
            view.setExitTarget(exitTarget);
        }
        if (view.getSelectionTarget() != null) {
            if (log.isDebugEnabled()) {
                log.debug("select() using selection target [name=" + view.getSelectionTarget() + "]");
            }
            return mapping.findForward(view.getSelectionTarget());
        }
        if (log.isDebugEnabled()) {
            log.debug("select() using action target [name=" + view.getSelectionTarget() + "]");
        }
        return mapping.findForward(view.getActionTarget());
    }

    // RECORD HEADER EDIT ACTIONS

    @Override
    public ActionForward enableEdit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("edit() requested");
        }
        RecordView view = getRecordView(session);
        if (view.getRecord().isUnchangeable() 
        		&& !(view.getRecord().isHistorical() 
        				&& (view.getRecord().isItemsChangeable() 
        					|| view.getRecord().isItemsEditable()))) {
            RequestUtil.saveError(request, ErrorCode.RECORD_UNCHANGEABLE);
            return mapping.findForward(view.getActionTarget());
        }
        view.setEditMode(true);
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Fetches the selected id param sets the selected object from the backend and forwards to 'success' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward update(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("update() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        try {
            view.save(new StrutsForm(form));
        } catch (ClientException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(Actions.EDIT);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Fetches the selected id param sets the selected object from the backend and forwards to 'success' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeDescriptionDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeDescriptionDisplay() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        view.changeDisplayDescriptionState();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles ignoreInDocument property
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeItemIgnoreInDocument(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeItemIgnoreInDocument() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        view.changeItemIgnoreInDocument(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Marks record as sent (released) and creates unchangeable pdf
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward release(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("release() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        Record record = view.getRecord();
        try {
            if (record.getItems().isEmpty()) {
                throw new ClientException(ErrorCode.ITEM_MISSING);
            }
            if (record.isHistorical()) {
            	view.releaseHistorical();
            } else if (!record.isUnchangeable()) {
                //view.createPdf(Record.STAT_SENT);
                if (log.isDebugEnabled()) {
                    log.debug("release() pdf created, record released");
                }
            }
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Invokes recordView.refresh() if available and forwards to display target. Invokes forward if no view is bound.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public final ActionForward reload(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("reload() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        view.refresh();
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Forwards to next page when view is in listing mode and displays not all records per default
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward lastList(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("lastList() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        view.lastList();
        return mapping.findForward(getListTarget(request));
    }

    /**
     * Forwards to next page when view is in listing mode and displays not all records per default
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward nextList(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("nextList() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        view.nextList();
        return mapping.findForward(getListTarget(request));
    }

    /**
     * Switches order listing between record or item listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeListingMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeListingMode() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        view.changeItemListing();
        return mapping.findForward(getListTarget(request));
    }

    /**
     * Enables salesId edit mode for current selected record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableSalesIdEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableSalesIdEditMode() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        recordView.setSalesIdEditMode(true);
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Disables salesId edit mode for current selected record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableSalesIdEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableSalesIdEditMode() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        recordView.setSalesIdEditMode(false);
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Updates sales id of current selected record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateSalesId(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateSalesId() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        try {
            Form formObj = new StrutsForm(form);
            recordView.updateSalesId(formObj.getLong(Form.SALES_ID));
        } catch (ClientException e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Enables suppliers reference number on current selected record for record views of type purchase view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableSupplierReferenceNumberEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableSupplierReferenceNumberEditMode() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        if (recordView instanceof PurchaseRecordView) {
            PurchaseRecordView view = (PurchaseRecordView) recordView;
            view.setSupplierReferenceNumberEditMode(true);
            view.setSetupMode(false);
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Disables suppliers reference number on current selected record for record views of type purchase view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableSupplierReferenceNumberEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableSupplierReferenceNumberEditMode() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        if (recordView instanceof PurchaseRecordView) {
            PurchaseRecordView view = (PurchaseRecordView) recordView;
            view.setSupplierReferenceNumberEditMode(false);
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Updates suppliers reference number on current selected record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateSupplierReferenceNumber(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateSupplierReferenceNumber() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        if (recordView instanceof PurchaseRecordView) {
            PurchaseRecordView view = (PurchaseRecordView) recordView;
            Form formObj = new StrutsForm(form);
            try {
                view.updateSupplierReferenceNumber(formObj.getString(Form.NUMBER), formObj.getDate(Form.CREATED_DATE));
            } catch (ClientException e) {
                saveError(request, e.getMessage());
            }
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Confirms an illegal discount
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward confirmDiscount(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("confirmDiscount() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        if (recordView instanceof DiscountAwareRecordView) {
            DiscountAwareRecordView view = (DiscountAwareRecordView) recordView;
            try {
                view.confirmDiscount(new StrutsForm(form));
            } catch (Exception e) {
                saveError(request, e.getMessage());
            }
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Disables illegal discount confirmation input
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableConfirmDiscountMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableConfirmDiscountMode() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        if (recordView instanceof DiscountAwareRecordView) {
            DiscountAwareRecordView view = (DiscountAwareRecordView) recordView;
            view.disableConfirmDiscountMode();
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Adds rebates to current record if implementing record and view are rebate aware
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward addRebate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("addRebate() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        if (recordView instanceof RebateAwareRecordView) {
            RebateAwareRecordView view = (RebateAwareRecordView) recordView;
            try {
                view.addRebate();
            } catch (Exception e) {
                saveError(request, e.getMessage());
            }
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Adds rebates to current record if implementing record and view are rebate aware
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward removeRebate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("removeRebate() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        if (recordView instanceof RebateAwareRecordView) {
            RebateAwareRecordView view = (RebateAwareRecordView) recordView;
            try {
                view.removeRebate();
            } catch (Exception e) {
                saveError(request, e.getMessage());
            }
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Updates sales id of current selected record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward unlockItems(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("unlockItems() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        try {
            recordView.unlockItems();
            
        } catch (ClientException e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
        return mapping.findForward(recordView.getActionTarget());
    }

    /**
     * Updates sales id of current selected record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward getRecordDocument(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("getRecordDocument() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        try {
            return forwardXmlOutput(
                    mapping,
                    request,
                    recordView.getRecordDocument(true));

        } catch (Exception e) {
            log.error("getRecordDocument() failed: " + e.getMessage(), e);
            return forwardXmlOutput(mapping, request, createErrorXml(request, e));
        }
    }

    /**
     * Updates sales id of current selected record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward getRecordStylesheet(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("getRecordStylesheet() request from " + getUser(session).getId());
        }
        RecordView recordView = getRecordView(session);
        try {
            return forwardXmlOutput(
                    mapping,
                    request,
                    recordView.getRecordStyleheet());

        } catch (Exception e) {
            log.error("getRecordStylesheet() failed: " + e.getMessage(), e);
            return forwardXmlOutput(mapping, request, createErrorXml(request, e));
        }
    }

    /**
     * Override this if list target differs from 'list'
     * @param request
     * @return listTarget
     */
    protected String getListTarget(HttpServletRequest request) {
        return Actions.LIST;
    }

    /**
     * Fetches current view if exists
     * @param session
     * @return view or null if none exist
     * @throws PermissionException
     */
    protected RecordView fetchRecordView(HttpSession session) throws PermissionException {
        return (RecordView) fetchView(session);
    }

    /**
     * Provides current view, throws runtime exception if view not available
     * @param session
     * @return view
     * @throws PermissionException
     */
    protected RecordView getRecordView(HttpSession session) throws PermissionException {
        RecordView view = fetchRecordView(session);
        if (view == null) {
            log.warn("getRecordView() required view not bound!");
            throw new ActionException();
        }
        return view;
    }
}
