<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
                        
<o:form id="timeRecordForm" name="timeRecordForm" url="/timeRecordConfig.do">
    <input type="hidden" name="method" value="save" />

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="createNewTimeRecordType"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="number" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="number" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="name" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="description" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <textarea name="description" class="form-control" rows="4"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/timeRecordConfig.do?method=disableCreate"/>');" value="<fmt:message key="break"/>" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</o:form>
