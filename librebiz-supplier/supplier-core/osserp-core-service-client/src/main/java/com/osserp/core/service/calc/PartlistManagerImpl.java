/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 7, 2009 4:39:03 PM 
 * 
 */
package com.osserp.core.service.calc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

import com.osserp.core.Item;
import com.osserp.core.ItemList;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.Partlist;
import com.osserp.core.calc.PartlistManager;
import com.osserp.core.calc.PartlistReference;
import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.ProductSelections;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Partlists;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductUtil;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PartlistManagerImpl extends AbstractItemListManager implements PartlistManager {
    private static Logger log = LoggerFactory.getLogger(PartlistManagerImpl.class.getName());

    private TaxRates taxRates = null;

    protected PartlistManagerImpl(Products products, ProductPrices productPrices, ProductSelections productSelections, Partlists partlists, TaxRates taxRates) {
        super(products, productPrices, productSelections, partlists);
        this.taxRates = taxRates;
    }

    public Partlist create(
            Employee user,
            PartlistReference reference,
            Product product,
            Long groupId) throws ClientException {

        Partlists pls = getPartlists();
        Partlist pl = pls.create(
                reference,
                product,
                user.getId(),
                groupId);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + pl.getId()
                    + ", name=" + pl.getName()
                    + "]");
        }
        return pl;
    }

    public Partlist addItem(
            DomainUser user,
            ItemList itemList,
            Long currentPosition,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            boolean includePrice,
            String note,
            Long externalId)
            throws ClientException {

        Partlist partlist = (Partlist) getPartlists().load(itemList.getId());
        List<ItemPosition> positions = partlist.getPositions();
        ItemPosition position = null;
        for (int i = 0, j = positions.size(); i < j; i++) {
            position = positions.get(i);
            if (position.getId().equals(currentPosition)) {
                break;
            }
        }
        if (position == null) {
            throw new IllegalStateException("position must not be null");
        }
        if (product.getGroup() != null && product.getGroup().isDiscount()) {
            throw new ClientException(ErrorCode.DISCOUNT_DISABLED);
        }
        List<ProductSelectionConfigItem> partnerPriceEditables = getPartnerPriceEditables();
        partlist.addItem(
                position.getId(),
                product,
                customName,
                quantity,
                price,
                partlist.getInitialCreated(),
                false,
                new BigDecimal(getProductPrices().getPartnerPrice(product, partlist.getInitialCreated(), quantity)),
                ProductUtil.isPartnerPriceEditable(partnerPriceEditables, product),
                false,
                new BigDecimal(getProductPrices().getPurchasePrice(product, product.getDefaultStock(), partlist.getInitialCreated())),
                taxRates.getRate(product.isReducedTax()),
                note,
                includePrice,
                externalId);
        partlist.setChangedBy(user.getEmployee().getId());
        partlist.setChanged(new Date(System.currentTimeMillis()));
        getItemLists().save(partlist);
        return (Partlist) getPartlists().load(itemList.getId());
    }

    public void save(DomainUser user, Partlist partlist) {
        partlist.setChangedBy(user.getEmployee().getId());
        partlist.setChanged(new Date(System.currentTimeMillis()));
        getItemLists().save(partlist);
    }

    public void copyPartlists(Calculation oldCalc, Calculation newCalc) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("copyPartlists() invoked [oldCalcId=" + oldCalc.getId()
                    + ", newCalcId=" + newCalc.getId() + "]");
        }
        for (int i = 0, j = oldCalc.getPositions().size(); i < j; i++) {
            ItemPosition pos = oldCalc.getPositions().get(i);
            if (pos.isPartlist()) {
                for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                    Item item = pos.getItems().get(k);
                    Partlist partlist = getPartlists().findByProduct(oldCalc.getId(), item.getProduct().getProductId());
                    if (partlist != null) {
                        getPartlists().create(
                                newCalc,
                                getProducts().load(partlist.getProductId()),
                                partlist.getCreatedBy(),
                                partlist.getConfigId(),
                                partlist);
                    }
                }
            }
        }
    }

    public Partlist findPartlist(PartlistReference reference, Product product) {
        return getPartlists().findByProduct(reference.getId(), product.getProductId());
    }

    public void deletePartlist(Long referenceId, Long productId) {
        Partlist partlist = getPartlists().findByProduct(referenceId, productId);
        if (productId != null && productId.equals(partlist.getProductId())) {
            getPartlists().delete(partlist);
        }
    }

    public Calculation loadPartlistDetails(Calculation calc) {
        if (calc != null) {
            // TODO load referenced collection or initialize a pointer 
            for (ItemPosition position : calc.getPositions()) {
                if (position.isPartlist()) {
                    // TODO check if referenced collection contains matching partlist and
                    // position.setPartlistAvailable(true);
                }
            }
        }
        return calc;
    }

    private Partlists getPartlists() {
        return (Partlists) getItemLists();
    }
}
