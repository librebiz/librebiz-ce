/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 17, 2006 8:55:54 PM 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.MessageCode;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.core.sales.SalesOffer;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.calc.CalculationView;
import com.osserp.gui.client.sales.SalesOfferView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OfferAction extends AbstractSalesRecordAction {
    private static Logger log = LoggerFactory.getLogger(OfferAction.class.getName());

    @Override
    protected Class<SalesOfferView> getViewClass() {
        return SalesOfferView.class;
    }

    /**
     * Creates view if none exists and forwards to offer list or display an existing. <br/>
     * If only one offer exists it will be displayed straightforward, a selection list will be displayed if more than one offer exists.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forward() requested");
        }
        BusinessCaseView businessView = getBusinessCaseView(session);
        SalesOfferView offerView = (SalesOfferView) createView(request);
        try {
            offerView.load(businessView);
        } catch (ClientException e) {
            if (log.isDebugEnabled()) {
                log.debug("forward() failed [message=" + e.getMessage()
                        + ", forwardTarget=" + businessView.getForwardTarget()
                        + "]");
            }
            saveError(request, e.getMessage());
            return mapping.findForward(businessView.getForwardTarget());
        }
        if (businessView.isFlowControlMode()) {
            if (log.isDebugEnabled()) {
                log.debug("forward() invoked in fcs context, checking offer availability");
            }
            // check that a released offer is available
            if (offerView.isUnreleasedOfferAvailable()) {
                if (log.isDebugEnabled()) {
                    log.debug("forward() failed; unreleased offer found"
                            + "[forwardTarget=" + businessView.getForwardTarget() + "]");
                }
                saveError(request, ErrorCode.RECORD_CHANGEABLE);
                businessView.disableAllModes();
                return mapping.findForward(businessView.getForwardTarget());
            }
            saveMessage(request, MessageCode.SELECT_OFFER_FOR_ORDER);
            return mapping.findForward(offerView.getListTarget());
        }
        if (offerView.isOnlyOneOfferAvailable()) {
            if (log.isDebugEnabled()) {
                log.debug("forward() forwarding to display");
            }
            return mapping.findForward(offerView.getActionTarget());
        }
        if (log.isDebugEnabled()) {
            log.debug("forward() forwarding to list");
        }
        return mapping.findForward(offerView.getListTarget());
    }

    /**
     * Forwards to offer listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardSelection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardSelection() requested");
        }
        SalesOfferView offerView = (SalesOfferView) fetchView(session);
        BusinessCaseView businessView = getBusinessCaseView(session);
        if (offerView == null ||
                (offerView.getBusinessCaseView() != null
                && !offerView.getBusinessCaseView().getId().equals(businessView.getId()))) {
            offerView = (SalesOfferView) createView(request);
            try {
                offerView.load(businessView);
            } catch (ClientException e) {
                saveError(request, e.getMessage());
                return mapping.findForward(businessView.getForwardTarget());
            }
        }
        String target = RequestUtil.getTarget(request);
        offerView.setSelectionTarget(target);
        return mapping.findForward(offerView.getListTarget());
    }

    /**
     * Exits offer display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        SalesOfferView view = (SalesOfferView) fetchView(session);
        if (view == null) {
            if (log.isDebugEnabled()) {
                log.debug("exit() no offer view bound, looking up business case view...");
            }
            BusinessCaseView bcv = fetchBusinessCaseView(session);
            if (bcv != null && bcv.getForwardTarget() != null) {
                if (log.isDebugEnabled()) {
                    log.debug("exit() business case view with forward target found ["
                            + bcv.getForwardTarget() + "]");
                }
                return mapping.findForward(bcv.getForwardTarget());
            }
            if (log.isDebugEnabled()) {
                log.debug("exit() no business case view with forward target found, using index as target");
            }
            return mapping.findForward(Actions.INDEX);
        } else if (view.isConfirmDiscountMode()) {
            view.disableConfirmDiscountMode();
        }
        if (view.isExitTargetAvailable()) {
            RequestUtil.saveExitId(request, view.getExitId());
            return mapping.findForward(view.getExitTarget());
        }
        if (view.isOnlyOneOfferAvailable() && view.isBusinessCaseClosed()) {
            return mapping.findForward(Actions.PLAN);
        }
        if (view.isOnlyOneOfferAvailable() && view.getBusinessCaseView() != null
                && view.getBusinessCaseView().getForwardTarget() != null) {
            return mapping.findForward(view.getBusinessCaseView().getForwardTarget());
        }

        BusinessCaseView businessView = getBusinessCaseView(session);
        try {
            view.load(businessView);
        } catch (ClientException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(view.getListTarget());
        }
        return mapping.findForward(view.getListTarget());
    }

    /**
     * Exists calculation listing. Forwards to record views exit target or current business case views forward target if none provided.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitList(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitList() request from " + getUser(session).getId());
        }
        SalesOfferView view = (SalesOfferView) getRecordView(session);
        view.setSelectionTarget(null);
        if (view.isExitTargetAvailable()) {
            return mapping.findForward(view.getExitTarget());
        }
        return mapping.findForward(view.getBusinessCaseView().getForwardTarget());
    }

    /**
     * Enables copy mode so view displays calculation name input form
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableCopyMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableCopyMode() request from " + getUser(session).getId());
        }
        SalesOfferView view = (SalesOfferView) getRecordView(session);
        if (view.isUnreleasedOfferAvailable()) {
            saveError(request, ErrorCode.UNRELEASED_RECORD);
        } else {
            view.setCopyMode(true);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables copy mode so view displays calculation name input form
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableCopyMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableCopyMode() request from " + getUser(session).getId());
        }
        SalesOfferView view = (SalesOfferView) getRecordView(session);
        view.setCopyMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to new calculation based on current offers calculation
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createCopy(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createCopy() request from " + getUser(session).getId());
        }
        SalesOfferView view = (SalesOfferView) getRecordView(session);
        if (view.isCopyMode()) {
            CalculationView cview = (CalculationView) createView(request, CalculationView.class.getName());
            try {
                cview.createCopy((SalesOffer) view.getRecord(), new StrutsForm(form).getString(Form.NAME));
            } catch (ClientException e) {
                return saveErrorAndReturn(request, e.getMessage());
            }
            return mapping.findForward(Actions.CALCULATION_DISPLAY);
        }
        try {
            view.createCopy(new StrutsForm(form));
            return mapping.findForward(view.getActionTarget());
        } catch (ClientException e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
    }

    /**
     * Creates copy from offer and forward to reload of offer list
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createOfferCopy(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createOfferCopy() request from " + getUser(session).getId());
        }

        SalesOfferView view = (SalesOfferView) getRecordView(session);
        CalculationView calculationView = (CalculationView) createView(request, CalculationView.class.getName());
        String target = view.getActionTarget();
        try {
            calculationView.createCopy((SalesOffer) view.getRecord(), new StrutsForm(form).getString(Form.NAME));
            view.createOffer(calculationView.getCalculation(), true);
            target = "calculation";
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        view.setCopyMode(false);
        if (view.isBeanAvailable()) {
            return mapping.findForward(target);
        }
        return mapping.findForward(view.getListTarget());
    }

    public ActionForward selectForCopy(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectForCopy() request from " + getUser(session).getId());
        }

        ActionForward target = select(mapping, form, request, response);
        SalesOfferView view = (SalesOfferView) getRecordView(session);
        if (view.isUnreleasedOfferAvailable()) {
            saveError(request, ErrorCode.UNRELEASED_RECORD);
            return mapping.findForward(view.getListTarget());
        }
        view.setCopyMode(true);
        return target;
    }

    /**
     * Resets current status
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward resetStatus(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("resetStatus() request from " + getUser(session).getId());
        }
        SalesOfferView view = (SalesOfferView) getRecordView(session);
        view.resetStatus();
        view.setSetupMode(false);
        return mapping.findForward(view.getActionTarget());
    }

}
