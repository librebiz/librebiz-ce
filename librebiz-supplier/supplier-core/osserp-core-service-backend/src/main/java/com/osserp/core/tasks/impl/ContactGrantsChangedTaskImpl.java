/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.tasks.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.Property;
import com.osserp.common.service.Locator;
import com.osserp.common.service.impl.AbstractXmlMessageHandler;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.util.StringUtil;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.Plugin;
import com.osserp.core.PluginManager;
import com.osserp.core.Plugins;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactGrantsChangedTask;
import com.osserp.core.dao.Contacts;
import com.osserp.core.tasks.Task;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ContactGrantsChangedTaskImpl extends AbstractXmlMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(ContactGrantsChangedTaskImpl.class.getName());

    private static final String TASK_PROPERTY_NAME = "contactGrantsChangedTask";

    private Contacts contacts;
    private Locator locator;
    private PluginManager pluginManager;

    protected ContactGrantsChangedTaskImpl(
            Locator locator,
            PluginManager pluginManager,
            Contacts contacts) {
        super();
        this.locator = locator;
        this.pluginManager = pluginManager;
        this.contacts = contacts;
    }

    @Override
    protected void execute(Document doc, byte[] file) {
        Element root = doc.getRootElement();
        String type = root.getChildText("type");
        Long id = NumberUtil.createLong(root.getChildText("id"));
        if (id == null) {
            log.warn("execute() invoked with missing contact id [xml=" + JDOMUtil.getPrettyString(doc)
                + ", root=" + JDOMUtil.getPrettyString(root) + ", text=" + root.getText() + "]");
            return;
        }
        Contact contact = contacts.loadContact(id);
        Plugins plugins = pluginManager.getPlugins();
        List<Plugin> availablePlugins = plugins.getPluginsByProperty(TASK_PROPERTY_NAME);
        if (!availablePlugins.isEmpty()) {
            availablePlugins.forEach(plugin -> {

                Property property = plugin.getProperties().get(TASK_PROPERTY_NAME);
                if (!plugin.isDisabled() && !plugin.isEndOfLife()
                        && property != null && property.getValue() != null) {
                    List<String> taskNames = StringUtil.getTokenList(property.getValue(), StringUtil.COMMA);
                    taskNames.forEach(taskName -> {
                        executeTask(taskName, contact, type);
                    });
                } else if (property != null && property.getValue() != null && log.isDebugEnabled()) {
                    log.debug("execute: Ignoring disabled or plugin with EOL status [id=" + plugin.getId() + "]");
                }
            });
        } else if (log.isDebugEnabled()) {
            log.debug("execute: Did not find any plugin providing required service(s)");
        }
    }

    protected void executeTask(String taskName, Contact contact, String type) {
        try {
            Object serviceObj = locator.lookupService(taskName);
            if (serviceObj instanceof ContactGrantsChangedTask) {
                ContactGrantsChangedTask task = (ContactGrantsChangedTask) serviceObj;
                Object result = task.executeTask(contact, type);
                if (result == null || (result instanceof Boolean && !((Boolean)result).booleanValue())) {
                    log.warn("executeTask: Did not produce a result [taskName=" + taskName +
                            ", contact=" + contact.getContactId() + ", type=" + type + "]");
                }
            }
        } catch (Exception e) {
            log.error("executeTask: Failed [message=" + e.getMessage() + ", taskName=" + taskName +
                    ", contact=" + contact.getContactId() + ", type=" + type + "]", e);
        }
    }
}
