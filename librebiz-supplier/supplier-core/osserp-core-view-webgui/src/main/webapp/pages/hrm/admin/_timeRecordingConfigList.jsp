<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="center"><fmt:message key="number" /></th>
                    <th><fmt:message key="name" /></th>
                    <th><fmt:message key="description" /></th>
                    <th class="center"><fmt:message key="recording" /></th>
                    <th class="center"><fmt:message key="info" /></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="obj" items="${configView.list}">
                    <tr>
                        <td class="center boldtext"><o:out value="${obj.id}" /></td>
                        <td><a href="<c:url value="/timeRecordingConfig.do"><c:param name="method" value="select"/><c:param name="id" value="${obj.id}"/></c:url>"><o:out value="${obj.name}" /></a></td>
                        <td><o:out value="${obj.description}" /></td>
                        <td class="center">
                            <c:choose>
                                <c:when test="${obj.recordingHours}">
                                    <fmt:message key="bigYes" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="center">
                            <o:ajaxLink linkId="recipientId" url="${'/timeRecordingConfigDisplay.do?id='}${obj.id}">
                                <o:img name="importantIcon" />
                            </o:ajaxLink>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
