/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2008 12:23:54 AM 
 * 
 */
package com.osserp.gui.letters

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Constants
import com.osserp.common.util.CollectionUtil

import com.osserp.core.dms.LetterContentManager
import com.osserp.core.dms.LetterParagraph
import com.osserp.core.dms.LetterTemplate
import com.osserp.core.dms.LetterTemplateManager
import com.osserp.core.dms.LetterType
import com.osserp.core.system.BranchOffice

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractLetterView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractLetterView.class.getName())

    LetterType selectedType
    LetterParagraph selectedParagraph

    protected AbstractLetterView() {
        env.supportingTypeSelection = false
    }

    @Override
    void select() {
        super.select()
        createCustomNavigation()
    }

    /**
     * Provides available branchs
     */
    List<BranchOffice> getBranchOffices() {
        systemConfigManager?.branchs
    }

    /**
     * Provides the current branch of the user. Default is headquarter.
     * Implementing class may override this to provide users branch.
     */
    Long getCurrentBranch() {
        return Constants.HEADQUARTER
    }

    /**
     * Provides the salutation display
     * @return salutationDisplay
     */
    String getSalutationDisplay() {
        if (bean?.salutation) {
            return bean?.salutation
        }
        return letterContentManager.getDefaultSalutation(bean)
    }

    /**
     * Indicates if implementing view supports letterType selection
     * @return true if support is enabled
     */
    boolean isSupportingTypeSelection() {
        env.supportingTypeSelection
    }

    /**
     * Creates a new paragraph	
     */
    void createParagraph() {
        selectedParagraph = letterContentManager.createParagraph(domainUser, bean)
        reload()
    }

    /**
     * Deletes the selected paragraph	
     */
    void deleteParagraph() {
        if (selectedParagraph) {
            letterContentManager.deleteParagraph(domainUser, bean, selectedParagraph)
            bean = letterContentManager.find(bean.id)
            selectedParagraph = null
            reload()
        }
    }

    /**
     * Moves a paragraph down	
     */
    void moveParagraphDown() {
        Long id = form.getLong('id')
        if (id) {
            letterContentManager.moveParagraphDown(bean, id)
            reload()
        }
    }

    /**
     * Moves a paragraph up	
     */
    void moveParagraphUp() {
        Long id = form.getLong('id')
        if (id) {
            letterContentManager.moveParagraphUp(bean, id)
            bean = letterContentManager.find(bean.id)
            reload()
        }
    }

    /**
     * Selects a paragraph from bean.paragraphs
     */
    void selectParagraph() {
        Long id = form.getLong('id')
        if (id) {
            selectedParagraph = CollectionUtil.getById(bean.paragraphs, id)
        } else {
            selectedParagraph = null
        }
        createCustomNavigation()
    }

    /**
     * Updates selectedParagraph and disables paragraph selection
     */
    protected void updateParagraph() {
        letterContentManager.updateParagraph(
                domainUser,
                bean,
                selectedParagraph,
                form.getString('content'))
        selectedParagraph = null
    }

    /**
     * Provides the createParagraph link item	
     * @return createParagraphLink
     */
    protected MenuItem getCreateParagraphLink() {
        navigationLink("/${context.name}/createParagraph", 'newIcon', 'newParagraph')
    }

    /**
     * Provides the deselectParagraph link	
     * @return deselectParagraphLink
     */
    protected MenuItem getDeselectParagraphLink() {
        navigationLink("/${context.name}/selectParagraph", 'backIcon', 'exitEdit')
    }

    protected LetterTemplate getSelectedTemplate() {
        Long id = form.getLong('templateId')
        if (id) {
            return letterTemplateManager.find(id)
        }
        return null
    }

    protected String getContentKeepTogetherValue() {
        return form.getBoolean('contentKeepTogetherAlways') ? 'always' : 'auto'
    }

    protected abstract String getPrintLetterTitle();

    protected abstract LetterContentManager getLetterContentManager();

    protected LetterTemplateManager getLetterTemplateManager() {
        getService(LetterTemplateManager.class.getName())
    }
}
