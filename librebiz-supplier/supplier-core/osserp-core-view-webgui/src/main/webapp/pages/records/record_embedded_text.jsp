<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${(!record.closed and !record.unchangeable) && (!empty view.embeddedTextAbove || !empty view.embeddedTextType)}">
<div class="recordLabelValue">
<c:choose>
<c:when test="${!empty view.embeddedTextAbove}">
<v:link url="/records/recordText/forward?view=${view.name}&exit=${requestScope.baseUrl}?method=reload"><fmt:message key="coverLetter"/></v:link>
</c:when>
<c:otherwise>
<v:link url="/records/recordText/forward?view=${view.name}&embed=above&exit=${requestScope.baseUrl}?method=reload"><fmt:message key="coverLetterAdd"/></v:link>
</c:otherwise>
</c:choose>
</div>
</c:if>
