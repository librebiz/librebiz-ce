/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2006 4:39:24 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.Manufacturer;
import com.osserp.core.products.Package;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductPlanningMode;
import com.osserp.core.products.ProductType;
import com.osserp.core.products.SerialDisplay;
import com.osserp.core.users.DomainUser;
import com.osserp.core.views.StockReportDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Products extends ProductEntities, BusinessDetails {

    /**
     * Provides all product ids whose public available flag is true
     * @return list of all public product ids
     */
    List<Long> getPublicIds();

    /**
     * Checks if an product with given name already exists or name and
     * matchcode exceeds maxlength
     * @param productId
     * @param name
     * @param matchcode
     * @throws ClientException if name already exists
     */
    void checkUpdate(Long productId, String name, String matchcode) throws ClientException;

    /**
     * Provides an id suggestion by classification config or highest id 
     * if classification does not provide a number configuration
     * @param config
     * @return id suggestion 
     */
    Long createIdSuggestion(ProductClassificationConfig config);

    /**
     * Creates a new product
     * @param user
     * @param productId
     * @param config
     * @param matchcode
     * @param name
     * @param description
     * @param quantityUnit
     * @param packagingUnit
     * @param manufacturer
     * @param planningMode
     * @param other
     * @return product new created
     * @throws ClientException
     */
    Product create(
            Employee user,
            Long productId,
            ProductClassificationConfig config,
            String matchcode,
            String name,
            String description,
            Long quantityUnit,
            Integer packagingUnit,
            Long manufacturer,
            ProductPlanningMode planningMode,
            Product other) throws ClientException;

    /**
     * Changes the purchase price limit flag
     * @param domainUser
     * @param product
     * @throws PermissionException if user has no permissions
     */
    void changePurchasePriceLimitFlag(DomainUser domainUser, Product product) throws PermissionException;

    /**
     * Removes a type
     * @param product
     * @param type
     * @return reloaded product
     */
    Product removeType(Product product, ProductType type);

    /**
     * Updates product classification
     * @param product
     * @param type
     * @param group
     * @param category
     * @return reloaded product
     * @throws ClientException if group category relation is ambiguous
     */
    Product updateClassification(Product product, ProductType type, ProductGroup group, ProductCategory category) throws ClientException;

    /**
     * Fetchs existing or creates new classification by provided values
     * @param typeId
     * @param typeName
     * @param groupId
     * @param groupName
     * @param categoryId
     * @param categoryName
     * @param rangeStart
     * @param rangeEnd
     * @return existing, new created or default classification
     */
    ProductClassificationConfig fetchOrCreate(
            Long typeId, 
            String typeName, 
            Long groupId, 
            String groupName, 
            Long categoryId, 
            String categoryName, 
            Long rangeStart, 
            Long rangeEnd);

    /**
     * Saves a description
     * @param user
     * @param product
     * @param language
     * @param name
     * @param description
     * @param datasheetText (optional)
     * @throws ClientException if validation failed
     */
    void saveDescription(Long user, Product product, String language, String name, String description, String datasheetText) throws ClientException;

    /**
     * Provides all stock aware products
     * @param includeKanban indicates that kanban managed products should be included
     * @return stockAware
     */
    List<Product> getStockAware(boolean includeKanban);

    /**
     * Provides all stock aware products of given group
     * @param category
     * @param includeKanban
     * @return stockAware
     */
    List<Product> getStockAware(ProductCategory category, boolean includeKanban);

    /**
     * Provides all stock aware products of given group
     * @param group
     * @param includeKanban
     * @return stockAware
     */
    List<Product> getStockAware(ProductGroup group, boolean includeKanban);

    /**
     * Provides all stock aware products of given group
     * @param group
     * @param includeKanban
     * @return stockAware
     */
    List<Product> getStockAware(ProductType type, boolean includeKanban);

    /**
     * Provides all stock input and output of an product and stock
     * @param stockId
     * @param productId
     * @return stockReport
     */
    List<StockReportDisplay> getStockReport(Long stockId, Long productId);

    /**
     * Provides a list of all products member of specified group
     * @return list of all products of specified group ordered by name
     */
    List<Product> findByGroup(Long groupId);

    /**
     * Provides all products with matching code
     * @param matchcode
     * @return matching products
     */
    List<Product> findByMatchcode(String matchcode);

    /**
     * Finds product with same name
     * @param name
     * @return product or null if not exists
     */
    Product findExisting(String name);

    /**
     * Provides all products with quantityUnitTime enabled
     * @return
     */
    List<Product> findTrackable();

    /**
     * Provides the full name of the product with specified id
     * @param productId
     * @return name
     */
    String getName(Long productId);

    /**
     * Provides the id of the product with provided name
     * @param name
     * @return null if name not exists
     */
    Long getExistingName(String name);

    /**
     * Validates whether product meets required dependencies for caluclation
     * @param product
     * @throws ClientException if validation failed
     */
    void checkCalculationValues(Product product) throws ClientException;

    /**
     * Fetches the package from a list of items
     * @param packages
     * @param items
     * @return package
     */
    Item getPackage(Map<Long, Package> packages, List<Item> items);

    /**
     * Provides all manufacturers
     * @return manufacturers
     */
    List<Manufacturer> getManufacturers();

    /**
     * Provides all manufacturers related to an product group
     * @param groupId
     * @return manufacturers
     */
    List<Manufacturer> getManufacturersByGroup(Long groupId);

    /**
     * Adds a new manufacturer
     * @param groupId
     * @param name
     * @return new created manufacturer
     * @throws ClientException if name already exists under group
     */
    Manufacturer addManufacturer(Long groupId, String name) throws ClientException;

    /**
     * Renames an existing manufacturer
     * @param id
     * @param name
     * @throws ClientException if name already exists
     */
    void updateManufacturer(Long id, String name) throws ClientException;

    /**
     * Updates productSummary for all items affecting stock
     * @param items
     */
    void updateSummary(List<Item> items);

    /**
     * Provides all serials as record related serial display
     * @return serialDisplay
     */
    List<SerialDisplay> getSerialDisplay();

    /**
     * Provides all serials as record related serial display by product
     * @param productId
     * @return serialDisplay
     */
    List<SerialDisplay> getSerialDisplay(Long productId);

    /**
     * Provides the product table name as defined in table config
     * @return product table name
     */
    String getProductTableName();

    /**
     * Finds the product's manufacturer
     * @return manufacturer
     */
    Manufacturer findManufacturer(Product product);
}
