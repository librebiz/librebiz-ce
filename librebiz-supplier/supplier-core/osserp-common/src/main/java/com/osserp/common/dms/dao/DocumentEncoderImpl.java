/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2016 
 * 
 */
package com.osserp.common.dms.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.dms.DocumentType;
import com.osserp.common.util.SecurityUtil;

/**
 * Default document encoder implementation
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DocumentEncoderImpl implements DocumentEncoder {
    private static Logger log = LoggerFactory.getLogger(DocumentEncoderImpl.class.getName());

    public byte[] decode(byte[] encodedData, int format, String secret) {
        if (DocumentType.FORMAT_CRYPT == format) {
            if (log.isDebugEnabled()) {
                log.debug("decode() invoked [size=" 
                        + (encodedData == null ? "0" : encodedData.length)
                        + ", format=" + format + ", secretProvided=" 
                        + (secret != null) + "]");
            }
            return SecurityUtil.decryptFile(secret, encodedData);
        }
        return encodedData;
    }

    public byte[] encode(byte[] decodedData, int format, String secret) {
        if (DocumentType.FORMAT_CRYPT == format) {
            if (log.isDebugEnabled()) {
                log.debug("encode() invoked [size=" 
                        + (decodedData == null ? "0" : decodedData.length)
                        + ", format=" + format + ", secretProvided=" 
                        + (secret != null) + "]");
            }
            return SecurityUtil.encryptFile(secret, decodedData);
        }
        return decodedData;
    }
}
