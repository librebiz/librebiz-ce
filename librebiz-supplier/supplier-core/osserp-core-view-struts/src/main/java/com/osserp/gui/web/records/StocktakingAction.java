/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Dec-2006 11:24:09 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.core.finance.Stocktaking;
import com.osserp.gui.client.records.StocktakingView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StocktakingAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(StocktakingAction.class.getName());
    private static final int DEFAULT_LIST_STEP = 25;
    private static final int MIN_LIST_STEP = 10;

    /**
     * Forwards to create new stocktaking
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward enableCreate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableCreate() request from " + getUser(session).getId());
        }
        StocktakingView view = getStocktakingView(session);
        if (view.getBean() != null) {
            Stocktaking stocktaking = (Stocktaking) view.getBean();
            if (!stocktaking.isClosed()) {
                saveError(request, ErrorCode.RECORD_CHANGEABLE);
                return mapping.findForward(view.getActionTarget());
            }
        }
        view.setCreateMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to product display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception >
     */
    public ActionForward forwardProducts(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardProducts() request from " + getUser(session).getId());
        }
        StocktakingView view = getStocktakingView(session);
        view.setProductEditMode(true);
        Integer step = RequestUtil.getInteger(request, "count");
        step = (step == null || step < MIN_LIST_STEP) ? DEFAULT_LIST_STEP : step;
        view.setListStep(step);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Returns from product display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitProducts(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitProducts() request from " + getUser(session).getId());
        }
        StocktakingView view = getStocktakingView(session);
        view.setProductEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates product counters
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateProducts(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateProducts() request from " + getUser(session).getId());
        }
        StocktakingView view = getStocktakingView(session);
        try {
            view.updateProducts(new StrutsForm(form));
            if (view.isLastList()) {
                view.setProductEditMode(false);
            } else {
                view.nextList();
            }
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Sets current quantity as count
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward synchronizeQuantity(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("synchronizeQuantity() request from " + getUser(session).getId());
        }
        StocktakingView view = getStocktakingView(session);
        view.synchronizeQuantity();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Freezing stocktaking
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward close(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("close() request from " + getUser(session).getId());
        }
        StocktakingView view = getStocktakingView(session);
        try {
            view.close();
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes selected stock object if created by same user and not finalized
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("delete() request from " + getUser(session).getId());
        }
        StocktakingView view = getStocktakingView(session);
        try {
            view.delete();
        } catch (ClientException|PermissionException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    private StocktakingView getStocktakingView(HttpSession session) throws PermissionException {
        StocktakingView view = (StocktakingView) fetchView(session);
        if (view == null) {
            log.warn("getStocktakingView() failed, no view bound!");
            throw new com.osserp.common.ActionException();
        }
        return view;
    }

    @Override
    protected Class<? extends View> getViewClass() {
        return StocktakingView.class;
    }
}
