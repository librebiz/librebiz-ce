<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:set var="url" value="${requestScope.recipientUrl}"/>
<c:set var="urlTitle" value="${requestScope.recipientUrlTitle}"/>
<c:set var="correctionUrl" value="${requestScope.correctionUrl}"/>

<c:if test="${!empty record.contact}">
<div class="recordLabelValue">
    <span>
        <c:choose>
        <c:when test="${!empty url and !record.unchangeable}">
            <c:choose>
                <c:when test="${!empty urlTitle}">
                    <v:link url="${url}" title="${urlTitle}"><fmt:message key="recipient"/>: </v:link>
                </c:when>
                <c:otherwise>
                    <v:link url="${url}"><fmt:message key="recipient"/>: </v:link>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <fmt:message key="recipient"/>: 
        </c:otherwise>
        </c:choose>
        <o:out value="${record.contact.displayName}"/>
    </span>
    <c:if test="${record.type.supportingCorrections and !empty correctionUrl and !empty record.corrections and record.correction.unchangeable}">
        <span> - </span>
        <span>
            <a href="<c:url value="${correctionUrl}"/>">
                <fmt:message key="recordCorrectionAvailable"/>
            </a>
        </span>
    </c:if>
</div>
</c:if>
