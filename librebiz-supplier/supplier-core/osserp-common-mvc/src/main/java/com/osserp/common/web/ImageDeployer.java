/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 18, 2010 3:02:31 PM 
 * 
 */
package com.osserp.common.web;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.ResourceLocator;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class ImageDeployer {
    private static Logger log = LoggerFactory.getLogger(ImageDeployer.class.getName());
    private static Map<String, String> ICONS = new java.util.HashMap<String, String>();

    static {
        ICONS.put("actionLockedIcon", "img.icon.actionLocked");
        ICONS.put("actionReturnsIcon", "img.icon.actionReturns");
        ICONS.put("archiveIcon", "img.icon.archive");
        ICONS.put("backIcon", "img.icon.back");
        ICONS.put("barchartIcon", "img.icon.barchart");
        ICONS.put("bellIcon", "img.icon.bell");
        ICONS.put("branchIcon", "img.icon.branch");
        ICONS.put("cancelIcon", "img.small.cancel");
        ICONS.put("calcIcon", "img.icon.calc");
        ICONS.put("calendarIcon", "img.icon.calendar");
        ICONS.put("certificateIcon", "img.icon.certificate");
        ICONS.put("clockIcon", "img.icon.clock");
        ICONS.put("closedFolderIcon", "img.icon.folder.closed");
        ICONS.put("coinsIcon", "img.icon.coins");
        ICONS.put("coinsChartIcon", "img.icon.coinsChart");
        ICONS.put("copyIcon", "img.icon.copy");
        ICONS.put("configureIcon", "img.icon.configure");
        ICONS.put("connectorIcon", "img.icon.connector");
        ICONS.put("cutIcon", "img.icon.cut");
        ICONS.put("deleteIcon", "img.icon.delete");
        ICONS.put("disabledIcon", "img.icon.disabled");
        ICONS.put("docIcon", "img.icon.doc");
        ICONS.put("downIcon", "img.icon.down");
        ICONS.put("downloadIcon", "img.icon.download");
        ICONS.put("editIcon", "img.icon.edit");
        ICONS.put("enableIcon", "img.icon.enable");
        ICONS.put("enabledIcon", "img.icon.enabled");
        ICONS.put("exemptionCertificateIcon", "img.icon.exemptionCertificate");
        ICONS.put("eyesIcon", "img.icon.eyes");
        ICONS.put("filterIcon", "img.icon.filter");
        ICONS.put("forwardIcon", "img.icon.forward");
        ICONS.put("groupIcon", "img.icon.group");
        ICONS.put("groupAddIcon", "img.icon.group.add");
        ICONS.put("groupErrorIcon", "img.icon.group.error");
        ICONS.put("helpIcon", "img.icon.question");
        ICONS.put("hideDetailsIcon", "img.icon.details.hide");
        ICONS.put("homeIcon", "img.icon.home");
        ICONS.put("importantIcon", "img.icon.important");
        ICONS.put("importantDisabledIcon", "img.icon.important.disabled");
        ICONS.put("infoIcon", "img.icon.info");
        ICONS.put("leftDisabledIcon", "img.icon.left.disabled");
        ICONS.put("leftIcon", "img.icon.left");
        ICONS.put("leftarrowIcon", "img.icon.leftarrow");
        ICONS.put("letterIcon", "img.icon.letter");
        ICONS.put("linechartIcon", "img.icon.linechart");
        ICONS.put("lockIcon", "img.icon.lock");
        ICONS.put("mailIcon", "img.icon.mail");
        ICONS.put("mailSentIcon", "img.icon.mailSent");
        ICONS.put("minusIcon", "img.icon.minus");
        ICONS.put("moneyIcon", "img.icon.money");
        ICONS.put("moneyChartIcon", "img.icon.moneyChart");
        ICONS.put("mountingInstructionIcon", "img.icon.mountingInstruction");
        ICONS.put("moveIcon", "img.icon.move");
        ICONS.put("newIcon", "img.icon.new");
        ICONS.put("newdataIcon", "img.icon.newdata");
        ICONS.put("newsIcon", "img.icon.news");
        ICONS.put("nosmileIcon", "img.icon.nosmile");
        ICONS.put("numbersIcon", "img.icon.numbers");
        ICONS.put("openFolderIcon", "img.icon.folder.open");
        ICONS.put("organizerIcon", "img.icon.organizer");
        ICONS.put("passwordIcon", "img.icon.password");
        ICONS.put("peopleIcon", "img.icon.people");
        ICONS.put("personalIcon", "img.icon.personal");
        ICONS.put("pdfIcon", "img.icon.pdf");
        ICONS.put("phoneIcon", "img.icon.phone");
        ICONS.put("plusIcon", "img.icon.plus");
        ICONS.put("printIcon", "img.icon.print");
        ICONS.put("printGreenIcon", "img.icon.print.green");
        ICONS.put("printRedIcon", "img.icon.print.red");
        ICONS.put("relationIcon", "img.icon.relation");
        ICONS.put("reloadIcon", "img.icon.reload");
        ICONS.put("replaceIcon", "img.icon.replace");
        ICONS.put("rightDisabledIcon", "img.icon.right.disabled");
        ICONS.put("rightIcon", "img.icon.right");
        ICONS.put("rightarrowIcon", "img.icon.rightarrow");
        ICONS.put("saveIcon", "img.icon.save");
        ICONS.put("searchIcon", "img.icon.search");
        ICONS.put("selectBranchIcon", "img.icon.selectBranch");
        ICONS.put("selectBusinessTypeIcon", "img.icon.selectBusinessType");
        ICONS.put("selectCompanyIcon", "img.icon.selectCompany");
        ICONS.put("selectStatusIcon", "img.icon.selectStatus");
        ICONS.put("showDetailsIcon", "img.icon.details.show");
        ICONS.put("smallCancelIcon", "img.small.cancel");
        ICONS.put("smallDownIcon", "img.small.down");
        ICONS.put("smallEnabledIcon", "img.small.enabled");
        ICONS.put("smallForwardIcon", "img.small.forward");
        ICONS.put("smallNewIcon", "img.small.new");
        ICONS.put("smallReplaceIcon", "img.small.replace");
        ICONS.put("smallUpIcon", "img.small.up");
        ICONS.put("smileIcon", "img.icon.smiley");
        ICONS.put("snapshotIcon", "img.icon.snapshot");
        ICONS.put("stampIcon", "img.icon.stamp");
        ICONS.put("summaryIcon", "img.icon.summary");
        ICONS.put("syncDocumentIcon", "img.icon.syncDocument");
        ICONS.put("tableIcon", "img.icon.table");
        ICONS.put("telephoneAddIcon", "img.icon.telephoneAdd");
        ICONS.put("telephoneErrorIcon", "img.icon.telephoneError");
        ICONS.put("telephoneGoIcon", "img.icon.telephoneGo");
        ICONS.put("telephoneLinkIcon", "img.icon.telephoneLink");
        ICONS.put("texteditIcon", "img.icon.textedit");
        ICONS.put("todoIcon", "img.icon.todo");
        ICONS.put("toggleConfirmedFalseIcon", "img.icon.toggleConfirmedFalse");
        ICONS.put("toggleConfirmedTrueIcon", "img.icon.toggleConfirmedTrue");
        ICONS.put("toggleStoppedFalseIcon", "img.icon.toggleStoppedFalse");
        ICONS.put("toggleStoppedTrueIcon", "img.icon.toggleStoppedTrue");
        ICONS.put("trashIcon", "img.icon.trash");
        ICONS.put("unlockIcon", "img.icon.unlock");
        ICONS.put("upIcon", "img.icon.up");
        ICONS.put("uploadIcon", "img.icon.upload");
        ICONS.put("userAddIcon", "img.icon.user.add");
        ICONS.put("userCommentIcon", "img.icon.user.comment");
        ICONS.put("userEditIcon", "img.icon.user.edit");
        ICONS.put("usersIcon", "img.icon.users");
        ICONS.put("writeIcon", "img.icon.write");
    }

    public static void deployImages(ServletContext ctx, ResourceLocator locator) {
        java.util.ResourceBundle bundle = locator.getResourceBundle(locator.getDefaultLocale());
        if (bundle != null) {
            for (Iterator<Map.Entry<String, String>> i = ICONS.entrySet().iterator(); i.hasNext();) {
                Map.Entry<String, String> next = i.next();
                String path = "";
                if (bundle.containsKey(next.getValue())) {
                    path = bundle.getString(next.getValue());
                } else {
                    log.warn("deployImages() key '" + next.getValue() + "' wasn't found in resource file");
                }
                ctx.setAttribute(next.getKey(), path);
                if (log.isDebugEnabled()) {
                    log.debug("deployImages() added icon [key=" + next.getKey()
                            + ", value=" + next.getValue()
                            + ", path=" + path
                            + "]");
                }
            }
        }
    }
}
