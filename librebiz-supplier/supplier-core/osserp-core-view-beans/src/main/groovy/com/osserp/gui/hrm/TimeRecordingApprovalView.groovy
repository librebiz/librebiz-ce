/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Nov 15, 2010 09:09:47 AM 
 * 
 */
package com.osserp.gui.hrm

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.core.employees.EmployeeManager
import com.osserp.core.hrm.TimeRecordingApprovalManager
import com.osserp.gui.CoreView


/**
 *
 * @author cf <cf@osserp.com>
 *
 */
class TimeRecordingApprovalView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TimeRecordingApprovalView.class.getName())

    TimeRecordingApprovalView() {
        super()
        headerName = 'timerecording.approvals.pageTitle'
        dependencies = ['timeRecordingView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        Long id = form.getLong('id')
        if (id) {
            logger.debug("initRequest: forward by approval invoked [id=$id]")
            bean = timeRecordingApprovalManager.findApproval(id)
            if (bean) {
                logger.debug("initRequest: approval found [id=${bean.id}, approvalRequired=${bean.approvalRequired}, note=${bean.note}, correction=${bean.correction?.id}]")
            }
            env.timeRecordingEmployee = employeeManager.find(bean.employeeId)
        } else if (forwardRequest && env.timeRecordingView?.employee) {
            list = timeRecordingApprovalManager.loadApprovals(env.timeRecordingView.employee)
            env.timeRecordingEmployee = env.timeRecordingView.employee
            logger.debug("initRequest: forward by employee invoked [employee=${env.timeRecordingView.employee?.id}, approvalCount=${list.size()}]")
        }
        if (!bean && !list) {
            logger.debug("initRequest: did not find approval or list, using current user [employee=${domainUser?.id}]")
            list = timeRecordingApprovalManager.loadApprovals(domainUser.employee)
            env.timeRecordingEmployee = domainUser.employee
            logger.debug("initRequest: done [employee=${env.timeRecordingEmployee?.id}]")
        }
    }

    @Override
    void save() {
        if (!approvalPermissionGrant) {
            throw new ClientException(ErrorCode.PERMISSION_DENIED)
        }
        Boolean approved = form.getBoolean('approved')
        logger.debug("save: invoked [approved=$approved]")
        bean.note = form.getString('note')
        timeRecordingApprovalManager.closeApproval(domainUser.employee, bean, approved)
        if (timeRecordingContext) {
            env.timeRecordingView.refresh()
            logger.debug('save: done [context=timeRecording]')
        } else {
            logger.debug('save: done [context=events]')
        }
    }

    boolean getApprovalPermissionGrant() {
        (employeeManager.isDisciplinarian(env.timeRecordingEmployee, domainUser.employee)
                || isPermissionGrant('time_recording_approvals'))
    }

    boolean getTimeRecordingContext() {
        env.timeRecordingView
    }

    protected TimeRecordingApprovalManager getTimeRecordingApprovalManager() {
        getService(TimeRecordingApprovalManager.class.getName())
    }

    protected EmployeeManager getEmployeeManager() {
        getService(EmployeeManager.class.getName())
    }
}
