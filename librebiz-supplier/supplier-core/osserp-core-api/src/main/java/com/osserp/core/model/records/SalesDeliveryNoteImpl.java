/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 20:02:45 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.Constants;
import com.osserp.common.Option;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Product;
import com.osserp.core.products.SerialNumber;
import com.osserp.core.sales.SalesDeliveryNote;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesDeliveryNoteImpl extends AbstractDeliveryNote
        implements SalesDeliveryNote {

    /**
     * Default constructor required by Serializable
     */
    protected SalesDeliveryNoteImpl() {
        super();
    }

    /**
     * Creates a new delivery note by sales order.
     * @param id
     * @param type
     * @param bookingType
     * @param record
     * @param createdBy
     * @param created
     * @param toDeliver
     * @param reverse
     */
    public SalesDeliveryNoteImpl(
            Long id,
            RecordType type,
            DeliveryNoteType bookingType,
            Order record,
            Employee createdBy,
            Date created,
            List<Item> toDeliver,
            boolean reverse) {
        super(
                id,
                record.getCompany(),
                record.getBranchId(),
                record.getShippingId(),
                type,
                bookingType,
                record.getId(),
                record.getContact(),
                createdBy,
                record.getBusinessCaseId(),
                bookingType != null ? bookingType.isCustomContent() : false,
                bookingType != null ? bookingType.isCustomContent() : false);
        if (created != null) {
            setCreated(created);
        }
        addItems(toDeliver, reverse);
    }

    /**
     * Creates a new delivery note by invoice or credit note.
     * @param id
     * @param type
     * @param bookingType
     * @param record
     * @param createdBy
     * @param toDeliver
     * @param reverse
     */
    public SalesDeliveryNoteImpl(
            Long id,
            RecordType type,
            DeliveryNoteType bookingType,
            PaymentAwareRecord record,
            Employee createdBy,
            List<Item> toDeliver,
            boolean reverse) {
        super(
                id,
                record.getCompany(),
                record.getBranchId(),
                record.getShippingId(),
                type,
                bookingType,
                record.getId(),
                record.getContact(),
                createdBy,
                record.getBusinessCaseId(),
                bookingType != null ? bookingType.isCustomContent() : false,
                bookingType != null ? bookingType.isCustomContent() : false);
        setCreated(record.getCreated());
        setInitialDate(new Date(System.currentTimeMillis()));
        addItems(toDeliver, reverse);
    }

    private void addItems(List<Item> toDeliver, boolean reverse) {
        for (int i = 0, j = toDeliver.size(); i < j; i++) {
            Item next = toDeliver.get(i);
            if (reverse) {
                addItem(
                        next.getStockId(),
                        next.getProduct(),
                        next.getCustomName(),
                        next.getQuantity() * (-1),
                        next.getNote());
            } else {
                addItem(
                        next.getStockId(),
                        next.getProduct(),
                        next.getCustomName(),
                        next.getQuantity(),
                        next.getNote());
            }
        }
    }

    private void addItem(Long stockId, Product product, String customName, Double quantity, String note) {
        Item item = new SalesDeliveryNoteItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                new BigDecimal(Constants.DOUBLE_NULL),
                new java.util.Date(),
                new BigDecimal(Constants.DOUBLE_NULL),
                false,
                false,
                new BigDecimal(Constants.DOUBLE_NULL),
                product.isReducedTax() ?
                        getAmounts().getReducedTaxRate() :
                            getAmounts().getTaxRate(),
                note,
                true,
                getItems().size(),
                null);
        addItem(item);
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new SalesDeliveryNoteItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }

    @Override
    protected SerialNumber createSerial(Item item, String number, Employee user) {
        return new SalesDeliveryNoteSerialImpl(item, number, user);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        return new SalesDeliveryNoteInfoImpl(this, info, user);
    }

    @Override
    public boolean isSales() {
        return true;
    }
}
