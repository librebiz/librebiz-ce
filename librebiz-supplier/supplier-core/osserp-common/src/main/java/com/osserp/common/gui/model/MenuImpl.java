/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 18, 2007 9:01:20 PM 
 * 
 */
package com.osserp.common.gui.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.User;
import com.osserp.common.beans.AbstractOption;
import com.osserp.common.gui.Menu;
import com.osserp.common.gui.MenuItem;
import com.osserp.common.gui.MenuLink;
import com.osserp.common.gui.MenuHeader;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class MenuImpl extends AbstractOption implements Menu {
    private static Logger log = LoggerFactory.getLogger(MenuImpl.class.getName());

    private boolean activated = false;
    private boolean defaultMenu = false;
    private boolean sharedMenu = false;
    private boolean modulesEnabled = true;
    private boolean staticLinksEnabled = true;

    private List<MenuHeader> headers = new ArrayList<MenuHeader>();

    protected MenuImpl() {
        super();
    }
    
    public MenuImpl(String name, boolean defaultMenu) {
        super(name);
        this.defaultMenu = defaultMenu;
    }

    public MenuImpl(
            String name, 
            boolean defaultMenu, 
            boolean sharedMenu, 
            boolean modulesEnabled, 
            boolean staticLinksEnabled, 
            List<MenuHeader> headers) {
        this(name, defaultMenu);
        this.activated = defaultMenu;
        this.sharedMenu = !defaultMenu;
        this.modulesEnabled = modulesEnabled;
        this.staticLinksEnabled = staticLinksEnabled;
        this.headers = headers;
    }

    public MenuImpl(List<MenuHeader> headers) {
        super();
        this.headers = headers;
    }

    private MenuImpl(Menu menu) {
        super(menu);
        activated = menu.isActivated();
        defaultMenu = menu.isDefaultMenu();
        sharedMenu = menu.isSharedMenu();
        modulesEnabled = menu.isModulesEnabled();
        staticLinksEnabled = menu.isStaticLinksEnabled();
        headers = new ArrayList<MenuHeader>();
        for (int i = 0, j = menu.getHeaders().size(); i < j; i++) {
            MenuHeader pos = menu.getHeaders().get(i);
            headers.add((MenuHeader) pos.clone());
        }
    }

    /**
     * Mapped constructor, copying all values found including id! You have to reset or reassign the id manually if value only copying intended.
     * @param map
     */
    public MenuImpl(Map<String, Object> map) {
        super(map);
        activated = fetchBoolean(map, "activated");
        defaultMenu = fetchBoolean(map, "defaultMenu");
        sharedMenu = fetchBoolean(map, "sharedMenu");
        modulesEnabled = fetchBoolean(map, "modulesEnabled");
        staticLinksEnabled = fetchBoolean(map, "staticLinksEnabled");
        headers = new ArrayList<MenuHeader>();
        List<Map<String, Object>> mappedHeaders = (List<Map<String, Object>>) map.get("headers");
        if (mappedHeaders != null) {
            for (int i = 0, j = mappedHeaders.size(); i < j; i++) {
                Map<String, Object> mappedHeader = mappedHeaders.get(i);
                headers.add(new MenuHeaderImpl(mappedHeader));
            }
        }
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "activated", activated);
        putIfExists(map, "defaultMenu", defaultMenu);
        putIfExists(map, "sharedMenu", sharedMenu);
        putIfExists(map, "modulesEnabled", modulesEnabled);
        putIfExists(map, "staticLinksEnabled", staticLinksEnabled);
        List<Map<String, Object>> mappedHeaders = new ArrayList<Map<String, Object>>();
        for (int i = 0, j = headers.size(); i < j; i++) {
            MenuHeader menuHeader = headers.get(i);
            mappedHeaders.add(menuHeader.getMapped());
        }
        map.put("headers", mappedHeaders);
        return map;
    }

    private MenuImpl(Menu menu, User user) {
        super(menu);
        activated = menu.isActivated();
        defaultMenu = menu.isDefaultMenu();
        sharedMenu = menu.isSharedMenu();
        modulesEnabled = menu.isModulesEnabled();
        staticLinksEnabled = menu.isStaticLinksEnabled();
        headers = new ArrayList<MenuHeader>();
        for (int i = 0, j = menu.getHeaders().size(); i < j; i++) {
            MenuHeader pos = menu.getHeaders().get(i);
            if (isDisplayable(pos, user)) {
                List<MenuLink> lnks = new ArrayList<>();
                for (int k = 0, l = pos.getLinks().size(); k < l; k++) {
                    MenuLink next = pos.getLinks().get(k);
                    if (isDisplayable(next, user)) {
                        lnks.add(next);
                    }
                }
                if (!lnks.isEmpty()) {
                    headers.add(new MenuHeaderImpl(pos, lnks));
                }
            }
         }
    }
    
    protected boolean isDisplayable(MenuItem menuItem, User user) {
        if (menuItem != null) {
            if ( (menuItem.isHiddenForPublic() && user == null)
                    || (menuItem.isHiddenForGuest() && (user == null || user.isGuest()))
                    || (menuItem.isHiddenForAdmin() && user != null && user.isAdmin())
                    || menuItem.isEndOfLife() || menuItem.isDisabled()) {
                
                if (log.isDebugEnabled()) {
                    log.debug("isDisplayable() 1st level check failed [type="
                        + ((menuItem instanceof MenuHeader) ? "pos" : "link")
                        + ", name=" + menuItem.getName()
                        + ", disabled=" + menuItem.isDisabled()
                        + ", public=" + !menuItem.isHiddenForGuest()
                        + ", guestaccess=" + !menuItem.isHiddenForGuest()
                        + ", guest=" + (user == null ? "null" : user.isGuest())
                        + ", adminaccess=" + !menuItem.isHiddenForAdmin()
                        + ", admin=" + (user != null && user.isAdmin())
                        + ", eol=" + menuItem.isEndOfLife()
                        + "]");
                }    
                return false;
            }
            if (isNotSet(menuItem.getPermissions()) 
                    || "all".equals(menuItem.getPermissions())) {
                if (log.isDebugEnabled()) {
                    log.debug("isDisplayable() ok [type="
                        + ((menuItem instanceof MenuHeader) ? "pos" : "link")
                        + ", name=" + menuItem.getName()
                        + ", result=no.permission.required"
                        + "]");
                }
                return true;
            }
            if (user != null && user.isPermissionGrant(
                    StringUtil.getTokenArray(menuItem.getPermissions()))) {
                
                if (log.isDebugEnabled()) {
                    log.debug("isDisplayable() ok [type="
                        + ((menuItem instanceof MenuHeader) ? "pos" : "link")
                        + ", name=" + menuItem.getName()
                        + ", perms=" + menuItem.getPermissions()
                        + ", result=permission.grant"
                        + "]");
                }
                return true;
            }
            if (log.isDebugEnabled()) {
                log.debug("isDisplayable() 2nd level check failed [type="
                        + ((menuItem instanceof MenuHeader) ? "pos" : "link")
                        + ", name=" + menuItem.getName()
                        + ", perms=" + menuItem.getPermissions()
                        + ", result=permission.denied"
                        + "]");
            }
        } else {
            log.warn("isDisplayable() invoked without item [user="
                    + (user == null ? "null" : user.getId()) + "]");
        }
        return false;
    }

    public Menu getCustomized(User user) {
        return new MenuImpl(this, user);
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean active) {
        this.activated = active;
    }

    public boolean isDefaultMenu() {
        return defaultMenu;
    }

    protected void setDefaultMenu(boolean defaultMenu) {
        this.defaultMenu = defaultMenu;
    }

    public boolean isSharedMenu() {
        return sharedMenu;
    }

    protected void setSharedMenu(boolean sharedMenu) {
        this.sharedMenu = sharedMenu;
    }

    public boolean isModulesEnabled() {
        return modulesEnabled;
    }

    public void setModulesEnabled(boolean modulesEnabled) {
        this.modulesEnabled = modulesEnabled;
    }

    public boolean isStaticLinksEnabled() {
        return staticLinksEnabled;
    }

    public void setStaticLinksEnabled(boolean staticLinksEnabled) {
        this.staticLinksEnabled = staticLinksEnabled;
    }

    public List<MenuHeader> getHeaders() {
        return headers;
    }

    protected void setHeaders(List<MenuHeader> headers) {
        this.headers = headers;
    }
    
    public void addHeader(String name) {
        boolean added = false;
        for (int i = 0, j = headers.size(); i < j; i++) {
            MenuHeader pos = headers.get(i);
            if (pos.getName().equals(name)) {
                added = true;
            }
        }
        if (!added) {
            MenuHeader pos = new MenuHeaderImpl(headers.size(), name);
            headers.add(pos);
        }
    }

    public void toggleHeader(String name) {
        for (int i = 0, j = headers.size(); i < j; i++) {
            MenuHeader next = headers.get(i);
            if (next.getName().equals(name)) {
                next.setActivated(!next.isActivated());
                break;
            }
        }
    }
    
    public void addLink(String position, String url, String name, String permissions) {
        MenuHeader selected = fetchHeader(position);
        if (selected == null) {
            addHeader(position);
            selected = fetchHeader(position);
        }
        if (selected != null) {
            selected.addLink(url, name, permissions);
        }
    }

    protected MenuHeader fetchHeader(String name) {
        for (int i = 0, j = headers.size(); i < j; i++) {
            MenuHeader next = headers.get(i);
            if (next.getName().equals(name)) {
                return next;
            }
        }
        return null;
    }

    @Override
    public Object clone() {
        return new MenuImpl(this);
    }

    public Menu createShared(String url) {
        return null;
    }

}
