/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Aug-2006 15:45:52 
 * 
 */
package com.osserp.core.sales;

import java.util.List;

import com.osserp.common.EntityRelation;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrderVolumeExport extends EntityRelation {

    /**
     * Provides the associated config
     * @return config
     */
    SalesOrderVolumeExportConfig getConfig();

    /**
     * Adds an order
     * @param order
     */
    void addItem(Long orderId);

    /**
     * Adds closed deliveries if export by delivery enabled
     * @param order
     * @param deliveries
     */
    void addDeliveries(Order order, List<Long> deliveries);

    /**
     * Fetches item if exists
     * @return item
     */
    SalesOrderVolumeExportItem fetchItem(Order order);

    /**
     * Provides all export items
     * @return items
     */
    List<SalesOrderVolumeExportItem> getItems();

    /**
     * Provides the open deliveries by order
     * @param order
     * @return open
     */
    List<DeliveryNote> getOpen(Order order);

    /**
     * Indicates that delivery note has been already added
     * @param note
     * @return true if already added
     */
    boolean added(DeliveryNote note);

    boolean isIgnorable();

}
