/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2013 
 * 
 */
package com.osserp.gui.admin.system

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.system.SystemKey
import com.osserp.core.system.SystemKeyManager
import com.osserp.core.system.SystemKeyType

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CredentialConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(CredentialConfigView.class.getName())

    List<SystemKeyType> types = []
    SystemKeyType selectedType
    List<SystemKey> availableKeys = []
    SystemKey selectedKey
    
    boolean staticContext
    boolean credentialsSaved = false
    boolean selectNewName = false
    String credentialContext
    String credentialName

    CredentialConfigView() {
        headerName  = 'credentialConfigView'
        permissions = 'organisation_admin,runtime_config,executive_saas'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            List tpl = systemKeyManager.types
            List res = []
            tpl.each { SystemKeyType kt ->
                if (!kt.contextOnly) {
                    res << kt
                }
            }
            types = res

            credentialContext = form.getString('context')
            credentialName = form.getString('name')
            if (credentialContext && credentialName) {
                selectedType = systemKeyManager.getType(credentialContext, credentialName)
                staticContext = true
            }
            reload()
        }
    }
    
    @Override
    void reload() {
        if (!staticContext) {
            if (credentialContext) {
                
                if (selectedType?.contextName != credentialContext) {
                    selectedType = types.find { SystemKeyType type -> 
                        type.contextName == credentialContext || 
                            type.id.toString() == credentialContext 
                        }
                    if (selectedType) {
                        credentialContext = selectedType.contextName
                    }
                }
                availableKeys = systemKeyManager.getKeys(credentialContext)
                logger.debug("reload: done [context=${selectedType.contextName}, bean=${selectedType?.id}, keys=${availableKeys.size()}]")
            } else {
                availableKeys = []
                selectedType = null
                
                logger.debug("reload: done reset")
            }
        }
    }

    @Override
    void save() {
        if (!staticContext) {
            Long id = form.getLong('context')
            if (id) {
                selectedType = types.find { SystemKeyType type -> 
                    type.id == id }
                credentialContext = selectedType?.contextName
                if (credentialContext) {
                    availableKeys = systemKeyManager.getKeys(credentialContext)
                }
            } else {
                String s = form.getString('context')
                if (s) {
                    selectedType = systemKeyManager.getType(s, null)
                    credentialContext = selectedType?.contextName
                } else {
                    selectedType = null
                    credentialContext = null
                    selectNewName = false
                }
            }
            if (selectedType || credentialContext) {
                if (selectNewName) {
                    credentialName = form.getString('newName')
                    
                } else {
                    id = form.getLong('name')
                    if (id && id < 0) {
                        selectNewName = true
                    
                    } else if (id) {
                        selectedKey = availableKeys.find { SystemKey key -> (key.id == id) }
                        credentialName = selectedKey?.name
                        
                    } else {
                        selectedKey = null
                        credentialName = null
                    }
                }
            }
        }
        
        if (!updateOnly) {
            if (!credentialName || !credentialContext) {
                throw new ClientException(ErrorCode.VALUES_MISSING)
            }
            
            String password = form.getString('password')
            String confirmPassword = form.getString('confirmPassword')
    
            if (!password || !confirmPassword) {
                throw new ClientException(ErrorCode.PASSWORD_MISSING)
            }
            if (password != confirmPassword) {
                throw new ClientException(ErrorCode.PASSWORD_MATCH)
            }
             
            systemConfigManager.saveCredentials(credentialContext, credentialName, password)
            credentialsSaved = true
        }
    }
    
    protected SystemKeyManager getSystemKeyManager() {
        getService(SystemKeyManager.class.getName())
    }
}
