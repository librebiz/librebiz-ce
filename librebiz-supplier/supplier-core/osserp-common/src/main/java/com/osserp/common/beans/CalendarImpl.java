/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 22, 2007 8:07:43 AM 
 * 
 */
package com.osserp.common.beans;

import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalendarImpl extends AbstractCalendar {

    /**
     * Creates a new instance of Calendar initialized with current date
     */
    public CalendarImpl() {
        super();
    }

    /**
     * Creates a new instance of Calendar initialized with a specific date
     * @param day
     * @param month
     * @param year
     */
    public CalendarImpl(int day, int month, int year) {
        super(day, month, year);
    }

    /**
     * Creates a new instance of Calendar initialized with a specific date
     * @param date
     */
    public CalendarImpl(Date date) {
        super(date);
    }
}
