/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01.10.2004 
 * 
 */
package com.osserp.core.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.SelectOption;

import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.contacts.OfficePhone;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeDisciplinarian;
import com.osserp.core.employees.EmployeeDisplay;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeRoleDisplay;
import com.osserp.core.employees.EmployeeStatus;
import com.osserp.core.employees.EmployeeType;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Employees extends ClassifiedContacts {

    /**
     * Fetches cached employee
     * @param id of the employee or id from related system user
     * @return employee
     */
    Employee get(Long id);

    /**
     * Provides all employees related to a group key.
     * 
     * @param groupKey as accounting, cs, executive, hrm, it, logistics, om, 
     * purchasing, sales, technician or wholesale.
     * @return employees
     */
    List<Employee> findByGroup(String groupKey);

    /**
     * Provides all employees whose user is account has a dedicated permission
     * @param permission
     * @return employees
     */
    List<Employee> findByPermission(String permission);

    /**
     * Provides all active employees
     * @return values
     */
    List<Employee> findActiveEmployees();

    /**
     * Provides a list of all active employees ordered by lastname
     * @return activeNames
     */
    List<Option> findActiveNames();

    /**
     * Finds all active employees as view object
     * @return active
     */
    List<EmployeeDisplay> findActive();

    /**
     * Filters an employee list by branchs of given user
     * @param list
     * @param domainUser
     * @return employeesBySupportedBranch
     */
    List<BusinessCaseRelationStat> filterBySupportedBranch(List<BusinessCaseRelationStat> list, DomainUser domainUser);

    /**
     * Finds disciplinarians of an employee
     * @param employee
     * @return disciplinarians
     */
    List<EmployeeDisciplinarian> findDisciplinarians(Employee employee);

    /**
     * Adds a disciplinarian
     * @param user
     * @param employee
     * @param disciplinarian
     * @throws ClientException
     */
    void addDisciplinarian(Employee user, Employee employee, Employee disciplinarian) throws ClientException;

    /**
     * Removes a disciplinarian
     * @param user
     * @param disciplinarian
     * @throws ClientException
     */
    void removeDisciplinarian(Employee user, EmployeeDisciplinarian disciplinarian) throws ClientException;

    /**
     * Provides sales person(s) with sales capacity statistics
     * @param employee id or null if all sales persons should be fetched
     * @return list
     */
    List<BusinessCaseRelationStat> getSalesCapacity(Long employee);

    /**
     * Provides tec person(s) with sales capacity statistics
     * @param employee id or null if all sales persons should be fetched
     * @return list
     */
    List<BusinessCaseRelationStat> getTecCapacity(Long employee);

    /**
     * Provides a list of employee id's whose are members of given group
     * @param groupKey as 'accounting, cs, executive, hrm, it, logistics, sales or technician'
     * @return list with id's of group members
     */
    List<Long> findUserIdsByGroup(String groupKey);

    /**
     * Provides a list of employee's short keys
     * @return list with id and shortkey as Option objects
     */
    List<Option> findShortKeys();

    /**
     * Provides the employee id related to given user
     * @param userId
     * @return employeeId
     */
    Long getEmployeeByUser(Long userId);

    /**
     * Provides employees email address
     * @param employeeId
     * @return email
     */
    String getEmail(Long employeeId);

    /**
     * Provides a list of SelectOptions with email and firstname lastname of all active employees whose email is not found in ignorable
     * @param ignorable email addresses
     * @return list of options where email is property and name is labelProperty
     */
    List<SelectOption> findEmailsAndNames(Set<String> ignorable);

    /**
     * Provides all available office phones
     * @param branchId
     * @return officePhones
     */
    List<OfficePhone> getOfficePhones(Long branchId);

    /**
     * Creates a new office phone number
     * @param userId createing new phone entry
     * @param contactId of the office
     * @param device of the phone
     * @param type private or business
     * @param country
     * @param prefix
     * @param number
     * @param directDial
     * @param branchId
     * @return phone
     * @throws ClientException if validation failed
     */
    OfficePhone createOfficePhoneNumber(
            Long userId,
            Long contactId,
            Long device,
            Long type,
            String country,
            String prefix,
            String number,
            String directDial,
            Long branchId) throws ClientException;

    /**
     * Provides all available employee groups
     * @return groups
     */
    List<EmployeeGroup> getEmployeeGroups();

    /**
     * Fetches an employee group by id
     * @param id
     * @return employeeGroup or null if no longer exists
     */
    EmployeeGroup getEmployeeGroup(Long id);

    /**
     * Indicates that group is existing
     * @param group
     * @return true if group exists
     */
    boolean exists(EmployeeGroup group);

    /**
     * Creates a new employee group
     * @param user
     * @param name
     * @param key
     * @return new created group
     * @throws ClientException
     */
    EmployeeGroup createGroup(Employee user, String name, String key) throws ClientException;

    /**
     * Deletes an employee group
     * @param group
     */
    void delete(EmployeeGroup group);

    /**
     * Persists an employee group
     * @param group
     */
    void save(EmployeeGroup group);

    /**
     * Provides all fcs aware employee groups
     * @return fcsAwareGroups
     */
    List<EmployeeGroup> getFcsAwareEmployeeGroups();

    /**
     * Provides all employee groups mapped to ldap groups
     * @return ldapGroupMap
     */
    Map<Long, EmployeeGroup> getLdapGroupMap();

    /**
     * Provides all groups referencing a permission
     * @param permission
     * @return groups as id/name values
     */
    List<Option> findGroupsByPermission(String permission);

    /**
     * Provides all available employee group keys
     * @return keys
     */
    List<Option> getEmployeeGroupKeys();

    /**
     * Provides all available employee status
     * @return status
     */
    List<EmployeeStatus> getEmployeeStatus();

    /**
     * Provides an employee type by primary key
     * @param id
     * @return employeeType or null if not exists
     */
    EmployeeType getEmployeeType(Long id);

    /**
     * Provides employee types
     * @return employeeTypes
     */
    List<EmployeeType> getEmployeeTypes();

    /**
     * Provides a display of all currently configured employee roles
     * @return employeeRoleDisplay
     */
    List<EmployeeRoleDisplay> getEmployeeRoleDisplay();

    /**
     * Reloads cached employee
     * @param id
     */
    void reload(Long id);
}
