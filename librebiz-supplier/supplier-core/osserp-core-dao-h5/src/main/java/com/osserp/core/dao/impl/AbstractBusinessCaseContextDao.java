/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2012 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessType;
import com.osserp.core.ContractType;
import com.osserp.core.Options;
import com.osserp.core.contacts.ContactCountry;
import com.osserp.core.crm.Campaign;
import com.osserp.core.model.SalesContractTypeImpl;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractBusinessCaseContextDao extends AbstractDao {
    private static Logger log = LoggerFactory.getLogger(AbstractBusinessCaseContextDao.class.getName());

    public AbstractBusinessCaseContextDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache);
    }

    public double getAccountBalance(Long saleId) {
        String sql = "SELECT get_sales_invoice_open_amount(?)";
        final Object[] params = { saleId };
        final int[] types = { Types.BIGINT };
        List<Double> result = (List<Double>) jdbcTemplate.query(sql, params, types, getDoubleRowMapper());
        return (empty(result) ? 0 : result.get(0));
    }

    public BusinessType fetchRequestType(Long id, String name) {
        BusinessType type = null;
        List types = getOptions(Options.REQUEST_TYPES);
        for (int i = 0, j = types.size(); i < j; i++) {
            type = fetchRequestType(((BusinessType) types.get(i)), id, name);
            if (type != null) {
                break;
            }
        }
        return type;
    }

    private BusinessType fetchRequestType(BusinessType type, Long id, String name) {
        if (type.getId().equals(id)) {
            log.debug("fetchRequestType: found by primary key [id=" + id + "]");
            return type; 
        } 
        if (!isEmpty(name)) {
            
            if (name.equals(type.getName())) {
                log.debug("fetchRequestType: found by name [id=" + id
                        + ", name=" + name + "]");
                return type;
            }
            if (name.equalsIgnoreCase(type.getKey())) {
                log.debug("fetchRequestType: found by key [id=" + id
                        + ", key=" + name + "]");
                return type;
            }
        }
        return null;
    }

    protected ContractType getContractType(Long id) {
        return (ContractType) getCurrentSession().load(SalesContractTypeImpl.class, id);
    }

    public BranchOffice fetchBranch(Long id, String name, boolean required) {
        BranchOffice branch = null;
        if (!isEmpty(id)) {
            branch = (BranchOffice) getOption(Options.BRANCH_OFFICES, id);
            if (branch != null) {
                log.debug("fetchBranch: found by primary key [id=" + id + "]");
                return branch;
            } 
            log.warn("fetchBranch: invoked with unknown primary key [id=" + id + "]");
        }
        List<BranchOffice> branchs = new ArrayList(getOptions(Options.BRANCH_OFFICES));
        for (int i = 0; i < branchs.size(); i++) {
            branch = fetchBranch(branchs.get(i), name);
            if (branch != null) {
                break;
            }
        }
        if (isSet(name)) {
            log.warn("fetchBranch: invoked with unknown name [name=" + name + "]");
        }
        if (branch == null && !required) {
            // no branch provided but not required, lookup for headquarter for default
            for (int i = 0, j = branchs.size(); i < j; i++) {
                branch = branchs.get(i);
                if (branch.isHeadquarter()) {
                    log.debug("fetchBranch: using default [id=" + branch.getId()
                            + ", key=" + branch.getName() + "]");
                    break;
                }
            }
        }
        return branch;
    }

    private BranchOffice fetchBranch(BranchOffice branch, String name) {
        BranchOffice result = null;
        if (!isEmpty(name)) {
            if (name.equals(branch.getName())) {
                log.debug("fetchBranchOffice: found by name [id=" + branch.getId()
                        + ", name=" + branch.getName() + "]");
                result = branch;
            } else if (name.equals(branch.getShortname())) {
                log.debug("fetchBranchOffice: found by name [id=" + branch.getId()
                        + ", name=" + branch.getName() 
                        + ", shortname=" + branch.getShortname() 
                        + "]");
                result = branch;
            } else if (name.equals(branch.getShortkey())) {
                log.debug("fetchBranchOffice: found by name [id=" + branch.getId()
                        + ", name=" + branch.getName() 
                        + ", shortkey=" + branch.getShortkey() 
                        + "]");
                result = branch;
            }
        }
        return result;
    }

    // features and checks depending on configuration

    protected boolean forceCreateNonExisting(String propertyName) {
        return isEmpty(propertyName) ? false : systemPropertyEnabled(propertyName);
    }

    protected Option fetchOption(String optionKey, Long id, String name, String createNonExistingPropertyName) {
        Option option = (!isEmpty(id) ? getOption(optionKey, id) : null);
        if (option != null) {
            log.debug("fetchOption() found by primary key [type=" + optionKey
                    + ", id=" + id + ", name=" + option.getName() + "]");
            return option;
        }
        String tname = (name != null ? name.trim() : null);
        if (!isEmpty(id) && isEmpty(tname)) {
            log.warn("fetchOption() invoked with invalid primary key [type=" 
                    + optionKey + ", id=" + id + "]");
            return null;
        }
        
        if (!isEmpty(id)) {
            log.warn("fetchOption() invoked with invalid primary key but name provided;"
                    + " this is confusing so we ignore this [type=" + optionKey
                    + ", id=" + id + ", name=" + tname + "]");
            // TODO may we implement an override mode?
            return null;
        }
        if (!isEmpty(tname)) {
            List<Option> options = new ArrayList(getOptions(optionKey));
            for (int i = 0, j = options.size(); i < j; i++) {
                Option next = options.get(i);
                if (next.getName().equals(tname)) {
                    option = next;
                    log.debug("fetchOption() found by name [id=" 
                            + option.getId() + ", name=" + option.getName() + "]");
                    return option;
                }
            }
            if (forceCreateNonExisting(createNonExistingPropertyName)) {
                log.debug("fetchOption() not found while createNonExisting is enabled [option=" 
                        + optionKey + ", value=" + tname + "]");
                option = createOption(optionKey, tname);
            }
        }
        if (option != null) {
            log.debug("fetchOption() done [option=" + optionKey 
                + ", id=" + option.getId() + ", name=" + option.getName() + "]");
        } else {
            log.debug("fetchOption() done without result [option=" + optionKey 
                    + ", id=" + id + ", name=" + tname + "]");
        }
        return option;
    }

    protected Option createOption(String optionKey, String name) {
        if (Options.CAMPAIGNS.equals(optionKey)) {
            return createCampaign(name);
        }
        if (Options.REQUEST_ORIGIN_TYPES.equals(optionKey)) {
            return createOrigin(name);
        }
        return null;
    }

    protected Campaign createCampaign(String name) {
        log.warn("createCampaign() invoked but campaign create not implemented yet!"
                + " [name" + name + "]");
        return null;
    }

    protected Option createOrigin(String name) {
        Option result = null;
        try {
            getOptionsCache().add(Options.REQUEST_ORIGIN_TYPES, name);
            result = fetchOption(Options.REQUEST_ORIGIN_TYPES, null, name, null);
            if (result != null) {
                if (log.isDebugEnabled()) {
                    log.debug("createOrigin() done [id=" + result.getId()
                        + ", name" + name + "]");
                }
            } else {
                log.warn("createOrigin() did not find just created origin! [name" 
                        + name + "]");
            }
        } catch (Exception e) {
            log.warn("createOrigin() failed [name" + name 
                    + ", message=" + e.getMessage()
                    + ", exeption=" + e.getClass().getName()
                    + "]");
        }
        return result;
    }

    public ContactCountry fetchCountry(Long id, String name) {
        ContactCountry result = (ContactCountry) fetchOption(Options.COUNTRY_NAMES, id, name, null);
        if (result == null && !isEmpty(name)) {
            List<ContactCountry> list = new ArrayList(getOptions(Options.COUNTRY_NAMES));
            for (int i = 0, j = list.size(); i < j; i++) {
                ContactCountry country = list.get(i);
                if (name.equalsIgnoreCase(country.getCountryCode())) {
                    result = country;
                    break;
                }
            }
        }
        return result;
    }
}
