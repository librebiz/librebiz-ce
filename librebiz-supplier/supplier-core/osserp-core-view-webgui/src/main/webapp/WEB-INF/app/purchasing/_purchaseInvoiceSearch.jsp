<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="purchaseInvoiceSearchView" />
<c:set var="displayPrice" value="false" />
<c:if test="${view.displayPricePermission}">
    <c:set var="displayPrice" value="true" />
</c:if>
<div class="table-responsive table-responsive-default">
    <table class="table">
        <thead>
            <tr>
                <th style="width: 80px; text-align:right;"><fmt:message key="recordNumberShort" /></th>
                <th style="width: 552px;"><fmt:message key="company" /></th>
                <th style="width: 62px; text-align:right;"><fmt:message key="created" /></th>
                <th style="width: 62px;"><fmt:message key="shortEmployeeLabel" /></th>
                <th style="width: 110px;"></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="record" items="${view.list}" varStatus="s">
                <tr class="altrow">
                    <td style="width: 80px;" class="right"><v:link url="${view.baseLink}/select?id=${record.id}">
                            <o:out value="${record.number}" />
                        </v:link></td>
                    <td style="width: 552px;"><o:out value="${record.lastname}" /></td>
                    <td style="width: 62px;" class="right"><o:date value="${record.created}" /></td>
                    <td style="width: 62px;"><o:ajaxLink linkId="${'createdBy'}${record.id}" url="${'/employeeInfo.do?id='}${record.createdBy}">
                            <oc:employee initials="true" value="${record.createdBy}" />
                        </o:ajaxLink></td>
                    <td style="width: 110px;" class="right"><c:choose>
                            <c:when test="${!record.paid}">
                                <span style="font-style: italic;"><o:number value="${record.grossAmount}" /></span>
                            </c:when>
                            <c:otherwise>
                                <o:number value="${record.grossAmount}" />
                            </c:otherwise>
                        </c:choose></td>
                </tr>
                <c:forEach var="item" items="${record.items}">
                    <tr>
                        <td class="right"><a href="<c:url value="/products.do?method=load&id=${item.productId}&exit=purchaseInvoiceSearchDisplay"/>"><o:out value="${item.productId}" /></a></td>
                        <td><o:out value="${item.name}" /></td>
                        <td class="right"><o:number value="${item.quantity}" format="currency" /></td>
                        <td style="width: 62px;"></td>
                        <td class="right"><o:date value="${item.delivery}" /></td>
                    </tr>
                </c:forEach>
            </c:forEach>
        </tbody>
    </table>
</div>
<o:removeErrors />
