/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 28-Dec-2006 20:44:09 
 * 
 */
package com.osserp.core.products;

import com.osserp.common.Entity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductSalesPrice extends Entity {

    /**
     * Returns the product id
     * @return groupId
     */
    Long getProductId();

    /**
     * The consumer price
     * @return consumerPrice
     */
    Double getConsumerPrice();

    /**
     * Sets the consumer price
     * @param consumerPrice
     */
    void setConsumerPrice(Double consumerPrice);

    /**
     * The reseller price
     * @return resellerPrice
     */
    Double getResellerPrice();

    /**
     * Sets the reseller price
     * @param resellerPrice
     */
    void setResellerPrice(Double resellerPrice);

    /**
     * The partner price
     * @return partnerPrice
     */
    Double getPartnerPrice();

    /**
     * Sets the partner price
     * @param partnerPrice
     */
    void setPartnerPrice(Double partnerPrice);

    /**
     * Provides the average purchase price
     * @return averagePurchasePrice
     */
    Double getAveragePurchasePrice();

    /**
     * Sets the average purchase price
     * @param averagePurchasePrice
     */
    void setAveragePurchasePrice(Double averagePurchasePrice);

    /**
     * Provides the estimated purchase price
     * @return estimatedPurchasePrice
     */
    Double getEstimatedPurchasePrice();

    /**
     * Sets the estimated purchase price
     * @param estimatedPurchasePrice
     */
    void setEstimatedPurchasePrice(Double estimatedPurchasePrice);

    /**
     * Provides the last purchase price
     * @return lastPurchasePrice
     */
    Double getLastPurchasePrice();

    /**
     * Sets the last purchase price
     * @param lastPurchasePrice
     */
    void setLastPurchasePrice(Double lastPurchasePrice);

    /**
     * Provides the consumer price minimum
     * @return consumerPriceMinimum
     */
    Double getConsumerPriceMinimum();

    /**
     * Sets the consumer price minimum
     * @param consumerPriceMinimum
     */
    void setConsumerPriceMinimum(Double consumerPriceMinimum);

    /**
     * Provides the reseller price minimum
     * @return resellerPriceMinimum
     */
    Double getResellerPriceMinimum();

    /**
     * Sets the reseller price minimum
     * @param resellerPriceMinimum
     */
    void setResellerPriceMinimum(Double resellerPriceMinimum);

    /**
     * Provides the partner price minimum
     * @return partnerPriceMinimum
     */
    Double getPartnerPriceMinimum();

    /**
     * Sets the partner price minimum
     * @param partnerPriceMinimum
     */
    void setPartnerPriceMinimum(Double partnerPriceMinimum);

    /**
     * Provides the consumer minimum margin
     * @return consumerMinimumMargin
     */
    Double getConsumerMinimumMargin();

    /**
     * Sets the consumer minimum margin
     * @param consumerMinimumMargin
     */
    void setConsumerMinimumMargin(Double consumerMinimumMargin);

    /**
     * Provides the reseller minimum margin
     * @return resellerMinimumMargin
     */
    Double getResellerMinimumMargin();

    /**
     * Sets the reseller minimum margin
     * @param resellerMinimumMargin
     */
    void setResellerMinimumMargin(Double resellerMinimumMargin);

    /**
     * Provides the partner minimum margin
     * @return partnerMinimumMargin
     */
    Double getPartnerMinimumMargin();

    /**
     * Sets the partner minimum margin
     * @param partnerMinimumMargin
     */
    void setPartnerMinimumMargin(Double partnerMinimumMargin);
}
