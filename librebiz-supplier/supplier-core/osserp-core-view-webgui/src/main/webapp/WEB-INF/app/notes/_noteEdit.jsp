<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="noteEdit" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <div id="noteEditor">
                <v:form name="noteForm" url="/notes/note/save">
                    <table class="table">
                        <tr>
                            <td colspan="2"><v:textarea styleClass="form-control" name="message" rows="22" value="${view.bean.note}" /></td>
                        </tr>
                        <tr>
                            <td><v:checkbox name="sendMail" value="${view.sendMail}" /> <fmt:message key="sendNoteAsEmail" />?</td>
                            <td style="text-align:right; width:15%;"><o:submit styleClass="form-control" title="saveNote" /></td>
                        </tr>
                    </table>
                </v:form>
            </div>
        </div>
    </div>
</div>
