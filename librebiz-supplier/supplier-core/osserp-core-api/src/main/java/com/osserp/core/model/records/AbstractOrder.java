/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 19:18:06 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Option;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Package;
import com.osserp.core.products.Product;
import com.osserp.core.products.SerialNumber;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOrder extends AbstractPaymentAgreementAwareRecord implements Order {
    private static Logger log = LoggerFactory.getLogger(AbstractOrder.class.getName());
    private static final Long DUMMY_ORDER_PRODUCT = 10001L;
    private static final Integer VERSION_START = 0;
    private Integer version = VERSION_START;

    private Date confirmationDate;
    private String confirmationNote;
    private Date delivery;
    private List<DeliveryNote> deliveryNotes = new ArrayList<DeliveryNote>();
    private boolean kanbanDeliveryEnabled = false;

    /**
     * Default constructor required by Serializable
     */
    protected AbstractOrder() {
        super();
    }

    /**
     * Creates a new empty order.
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param bookingType
     * @param reference
     * @param contact
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected AbstractOrder(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            BookingType bookingType,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {

        super(id, company, branchId, type, reference, contact, createdBy, sale,
                taxRate, reducedTaxRate, itemsChangeable, itemsEditable);
        setBookingType(bookingType);
    }

    /**
     * Creates a new order with initialized items
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param bookingType
     * @param reference
     * @param contact
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     * @param items
     */
    protected AbstractOrder(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            BookingType bookingType,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable,
            List<Item> items) {

        super(id, company, branchId, type, reference, contact, createdBy, sale,
                taxRate, reducedTaxRate, itemsChangeable, itemsEditable);
        setBookingType(bookingType);
        addItems(items);
    }

    @Override
    protected void addItems(List<Item> items) {
        super.addItems(items);
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            copyDeliveryDate(next);
        }
    }

    public Date getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(Date confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public String getConfirmationNote() {
        return confirmationNote;
    }

    public void setConfirmationNote(String confirmationNote) {
        this.confirmationNote = confirmationNote;
    }

    public Date getDelivery() {
        return delivery;
    }

    public void setDelivery(Date delivery) {
        this.delivery = delivery;
    }

    public boolean isKanbanDeliveryEnabled() {
        return kanbanDeliveryEnabled;
    }

    public void setKanbanDeliveryEnabled(boolean kanbanDeliveryEnabled) {
        this.kanbanDeliveryEnabled = kanbanDeliveryEnabled;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public void createVersion(Long employeeId) {
        version = (version == null ? 1 : version + 1);
        setChanged(DateUtil.getCurrentDate());
        setChangedBy(employeeId);
    }

    public List<DeliveryNote> getDeliveryNotes() {
        return deliveryNotes;
    }

    protected void setDeliveryNotes(List<DeliveryNote> deliveryNotes) {
        if (deliveryNotes != null) {
            this.deliveryNotes = deliveryNotes;
        }
    }

    public boolean isRecalculationRequired() {
        if (getItems() == null || getItems().isEmpty()) {
            return true;
        }
        if (getItems().size() == 1) {
            Item item = getItems().get(0);
            return (DUMMY_ORDER_PRODUCT.equals(
                    item.getProduct().getProductId()));
        }
        if (amounts == null
                || amounts.getAmount() == null
                || amounts.getAmount().doubleValue() == 0) {
            Package pkg = null;
            for (int i = 0, j = getItems().size(); i < j; i++) {
                Item next = getItems().get(i);
                if (next.getProduct().isPlant() && next.getProduct().getDetails() != null) {
                    Object details = next.getProduct().getDetails();
                    if (details instanceof Package) {
                        pkg = (Package) details;
                        break;
                    }
                } else if (next.getProduct().isPlant()) {
                    log.warn("isRecalculationRequired() found package but details not available [product=" + next.getProduct().getProductId() + "]");
                }
            }
            if (pkg != null && pkg.isFreeService()) {
                if (log.isDebugEnabled()) {
                    log.debug("isRecalculationRequired() summary 0 ok, free service found [product=" + pkg.getReference() + "]");
                }
                return false;
            }
            return true;
        }
        return false;
    }

    public boolean isChangeableDeliveryAvailable() {
        if (log.isDebugEnabled()) {
            log.debug("isChangeableDeliveryAvailable() invoked, examining "
                    + deliveryNotes.size() + " delivery notes");
        }
        for (int i = 0, j = deliveryNotes.size(); i < j; i++) {
            DeliveryNote note = deliveryNotes.get(i);
            if (!note.isUnchangeable()) {
                if (log.isDebugEnabled()) {
                    log.debug("isChangeableDeliveryAvailable() found "
                            + note.getId());
                }
                return true;
            }
        }
        return false;
    }

    public boolean isNotDeliveryNoteAffectingItemsOnly() {
        if (isUnchangeable() && !getItems().isEmpty()) {
            for (int i = 0, j = getItems().size(); i < j; i++) {
                Item next = getItems().get(i);
                if (next.isDeliveryNoteAffecting()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    protected RecordPaymentAgreement createPaymentAgreement() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isSales() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    protected Item createItem(Long stockId, Product product, String customName, Double quantity, Double taxRate, BigDecimal price, Date priceDate, BigDecimal partnerPrice,
            boolean partnerPriceEditable, boolean partnerPriceOverridden, BigDecimal purchasePrice, String note, boolean includePrice, Long externalId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean isNoneKanbanAvailable() {
        for (int i = 0, j = getItems().size(); i < j; i++) {
            Item next = getItems().get(i);
            if (!next.getProduct().isKanban()) {
                return true;
            }
        }
        return false;
    }

    public final List<Item> getOpenDeliveries() {
        List<Item> result = new ArrayList<Item>();
        for (int i = 0, j = getItems().size(); i < j; i++) {
            Item next = getItems().get(i);
            // we don't deliver irrelevant products
            if (next.isDeliveryNoteAffecting()) {
                if (log.isDebugEnabled()) {
                    log.debug("getOpenDeliveries() found delivery note affecting [product="
                            + next.getProduct().getProductId()
                            + ", quantity=" + next.getQuantity()
                            + ", delivered=" + next.getDelivered()
                            + ", open=" + next.getOutstanding()
                            + "]");
                }
                if (next.getOutstanding() > 0) {
                    addOpenDelivery(result, next);
                }
            }
        }
        return result;
    }

    private void addOpenDelivery(List<Item> result, Item item) {
        if (log.isDebugEnabled()) {
            log.debug("addOpenDelivery() invoked [item=" + item.getId()
                    + ", product=" + item.getProduct().getProductId()
                    + ", quantity=" + item.getQuantity()
                    + ", open=" + item.getOutstanding()
                    + "]");
        }
        Item listItem = fetchSameBooking(result, item);
        if (listItem == null) {
            // item is not existing yet
            listItem = (Item) item.clone();
            if (item.getQuantity() < 0) {
                listItem.setQuantity(item.getOutstanding() * (-1));
            } else {
                listItem.setQuantity(item.getOutstanding());
            }
            result.add(listItem);
            if (log.isDebugEnabled()) {
                log.debug("addOpenDelivery() add done [product="
                        + listItem.getProduct().getProductId()
                        + ", open=" + listItem.getQuantity()
                        + "]");
            }
        } else {
            if (item.getQuantity() < 0) {
                listItem.setQuantity(listItem.getQuantity() + (item.getOutstanding() * (-1)));
            } else {
                listItem.setQuantity(listItem.getQuantity() + item.getOutstanding());
            }
            if (log.isDebugEnabled()) {
                log.debug("addOpenDelivery() update done [product="
                        + listItem.getProduct().getProductId()
                        + ", open=" + listItem.getQuantity()
                        + "]");
            }
        }
    }

    public final List<Item> getDeliveries() {
        List<Item> deliveredGoods = new ArrayList<Item>();
        for (int i = 0, j = deliveryNotes.size(); i < j; i++) {
            DeliveryNote deliveryNote = deliveryNotes.get(i);
            if (log.isDebugEnabled()) {
                log.debug("getDeliveries() found delivery note [id=" + deliveryNote.getId() + "]");
            }
            for (int k = 0, l = deliveryNote.getItems().size(); k < l; k++) {
                Item item = deliveryNote.getItems().get(k);
                Item existing = fetchSameBooking(deliveredGoods, item);
                if (log.isDebugEnabled()) {
                    log.debug("getDeliveries() examining item [product="
                            + (item.getProduct() != null ? item.getProduct().getProductId() : "null")
                            + ", quantity=" + item.getQuantity()
                            + ", existing=" + (existing != null ? ("true, existingQty=" + existing.getQuantity()) : "false")
                            + "]");
                }
                if (existing != null) {
                    existing.setQuantity(existing.getQuantity() + item.getQuantity());
                    if (!item.getSerials().isEmpty()) {
                        for (int m = 0, n = item.getSerials().size(); m < n; m++) {
                            SerialNumber clone = (SerialNumber) item.getSerials().get(m).clone();
                            existing.getSerials().add(clone);
                        }
                    }
                } else {
                    deliveredGoods.add((Item) item.clone());
                }
            }
        }
        if (log.isDebugEnabled()) {
            for (Iterator<Item> i = deliveredGoods.iterator(); i.hasNext();) {
                Item next = i.next();
                log.debug("getDeliveries() result: [product=" + next.getProduct().getProductId()
                        + ", quantity=" + next.getQuantity()
                        + ", serials=" + (next.getSerials() == null || next.getSerials().isEmpty() ? "none" : next.getSerialsDisplay())
                        + "]");
            }
        }
        return deliveredGoods;
    }

    public void refreshDeliveries() {
        Map<Long, Item> allDeliveredGoods = getReleasedDeliveries(false);
        Map<Long, Item> allDeliveredCorrections = getReleasedDeliveries(true);
        boolean exchangeRecord = false;
        for (int i = 0, j = getItems().size(); i < j; i++) {
            Item next = getItems().get(i);
            if (next.getQuantity() < 0) {
                exchangeRecord = true;
                break;
            }
        }
        if (exchangeRecord) {
            refreshExchangeDeliveries(allDeliveredGoods, allDeliveredCorrections);
        } else {
            refreshDeliveries(allDeliveredGoods, allDeliveredCorrections);
        }
        refreshPackageAwareDeliveries();
    }

    private void refreshDeliveries(Map<Long, Item> allDeliveredGoods, Map<Long, Item> allDeliveredCorrections) {
        dumpItems("refreshDeliveries", allDeliveredGoods, allDeliveredCorrections);
        for (int i = 0, j = getItems().size(); i < j; i++) {
            Item next = getItems().get(i);
            if (!next.getProduct().isPlant() && next.isDeliveryNoteAffecting()) {
                if (log.isDebugEnabled()) {
                    log.debug("refreshDeliveries() examining item [product="
                            + next.getProduct().getProductId() + ", quantity="
                            + next.getQuantity() + "]");
                }
                double open = NumberUtil.round(next.getQuantity(), 3);
                double done = 0;
                if (allDeliveredGoods.containsKey(next.getProduct().getProductId())) {

                    Item delivered = allDeliveredGoods.get(next.getProduct().getProductId());
                    done = NumberUtil.round(delivered.getQuantity(), 3);
                    if (log.isDebugEnabled()) {
                        log.debug("refreshDeliveries() found delivery [product="
                                + delivered.getProduct().getProductId()
                                + ", delivered=" + done + "]");
                    }
                    if (allDeliveredCorrections.containsKey(next.getProduct().getProductId())) {
                        Item redelivered = allDeliveredCorrections.get(next.getProduct().getProductId());
                        double rolledin = NumberUtil.round(redelivered.getQuantity(), 3);
                        allDeliveredCorrections.remove(next.getProduct().getProductId());
                        done = done + rolledin;
                        if (log.isDebugEnabled()) {
                            log.debug("refreshDeliveries() found correction [product="
                                    + delivered.getProduct().getProductId()
                                    + ", rolledIn=" + rolledin + "]");
                        }
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("refreshDeliveries() calculated result [product="
                                + delivered.getProduct().getProductId()
                                + ", delivered=" + done + "]");
                    }
                    if (done == open) {
                        next.setDelivered(delivered.getQuantity());
                        allDeliveredGoods.remove(next.getProduct().getProductId());
                        if (log.isDebugEnabled()) {
                            log.debug("refreshDeliveries() delivered == ordered [id="
                                    + next.getId()
                                    + ", quantity="
                                    + next.getQuantity()
                                    + ", delivered="
                                    + next.getDelivered() + "]");
                        }

                    } else if (done > open) {
                        next.setDelivered(open);
                        delivered.setQuantity(done - open);
                        if (log.isDebugEnabled()) {
                            log.debug("refreshDeliveries() delivered > ordered [id="
                                    + next.getId()
                                    + ", quantity="
                                    + next.getQuantity()
                                    + ", delivered="
                                    + next.getDelivered() + "]");
                        }
                    } else {
                        // delivered < ordered
                        next.setDelivered(done);
                        allDeliveredGoods.remove(next.getProduct()
                                .getProductId());
                        if (log.isDebugEnabled()) {
                            log
                                    .debug("refreshDeliveries() delivered < ordered [id="
                                            + next.getId()
                                            + ", quantity="
                                            + next.getQuantity()
                                            + ", delivered="
                                            + next.getDelivered() + "]");
                        }
                    }
                } else if (next.isDeliveryNoteAffecting() && next.getDelivered() > 0) {

                    if (log.isDebugEnabled()) {
                        log
                                .debug("refreshDeliveries() resetting cancelled delivery [id="
                                        + next.getId()
                                        + ", quantity="
                                        + next.getQuantity()
                                        + ", delivered="
                                        + next.getDelivered() + "]");
                    }
                    next.setDelivered(0d);
                }
            }
        }
    }

    private void refreshExchangeDeliveries(Map<Long, Item> allDeliveredGoods, Map<Long, Item> allDeliveredCorrections) {
        dumpItems("refreshExchangeDeliveries", allDeliveredGoods, allDeliveredCorrections);
        for (int i = 0, j = getItems().size(); i < j; i++) {
            Item next = getItems().get(i);
            if (!next.getProduct().isPlant() && next.isDeliveryNoteAffecting()) {
                if (next.getQuantity() > 0) {
                    Item delivered = allDeliveredGoods.get(next.getProduct().getProductId());
                    if (delivered != null) {
                        next.setDelivered(delivered.getQuantity());
                        if (log.isDebugEnabled()) {
                            log.debug("refreshExchangeDeliveries() update item [product="
                                    + next.getProduct().getProductId()
                                    + ", quantity=" + next.getQuantity()
                                    + ", delivered=" + next.getDelivered()
                                    + ", name=" + next.getProduct().getName()
                                    + "]");
                        }
                    }
                } else if (next.getQuantity() < 0) {
                    Item delivered = allDeliveredCorrections.get(next.getProduct().getProductId());
                    if (delivered != null) {
                        next.setDelivered(delivered.getQuantity());
                        if (log.isDebugEnabled()) {
                            log.debug("refreshExchangeDeliveries() update item [product="
                                    + next.getProduct().getProductId()
                                    + ", quantity=" + next.getQuantity()
                                    + ", delivered=" + next.getDelivered()
                                    + ", name=" + next.getProduct().getName()
                                    + "]");
                        }
                    }

                } else {
                    log.warn("refreshExchangeDeliveries() found item with quantity of 0 [order" + getId()
                            + ", item=" + next.getId()
                            + ", product=" + (next.getProduct() == null ? "null" : next.getProduct().getProductId())
                            + "]");
                }
            }
        }
    }

    private void refreshPackageAwareDeliveries() {
        if (log.isDebugEnabled()) {
            log.debug("refreshPackageAwareDeliveries() invoked [order=" + getId() + "]");
        }
        for (int i = 0, j = getItems().size(); i < j; i++) {
            Item next = getItems().get(i);
            if (next.isDeliveryNoteAffecting()
                    && next.getProduct().isQuantityByPlant()) {
                double result = (
                        Math.abs(next.getQuantity() == null ? 0 : next.getQuantity())
                        - Math.abs(next.getDelivered() == null ? 0 : next.getDelivered()));
                if (result > -0.1 && result < 0.1) {
                    if (log.isDebugEnabled()) {
                        log.debug("refreshPackageAwareDeliveries() correcting quantity by plant [id="
                                + next.getId()
                                + ", product=" + next.getProduct().getProductId()
                                + ", quantity=" + next.getQuantity()
                                + ", delivered=" + next.getDelivered()
                                + "]");
                    }
                    next.setDelivered(next.getQuantity());
                }
            }
        }
    }

    private Map<Long, Item> getReleasedDeliveries(boolean corrections) {
        if (log.isDebugEnabled()) {
            log.debug("getReleasedDeliveries() invoked [order=" + getId()
                    + ", corrections=" + corrections + "]");

        }
        Map<Long, Item> deliveredGoods = new HashMap<Long, Item>();
        List<DeliveryNote> notes = new ArrayList<DeliveryNote>(deliveryNotes);
        for (Iterator<DeliveryNote> i = notes.iterator(); i.hasNext();) {
            DeliveryNote next = i.next();
            if (!next.isUnchangeable()) {
                if (log.isDebugEnabled()) {
                    log.debug("getReleasedDeliveries() removing unreleased delivery [id="
                            + next.getId() + "]");

                }
                i.remove();
            }
        }
        for (int i = 0, j = notes.size(); i < j; i++) {
            DeliveryNote deliveryNote = notes.get(i);

            for (int k = 0, l = deliveryNote.getItems().size(); k < l; k++) {
                Item item = deliveryNote.getItems().get(k);
                if ((item.getQuantity() < 0 && corrections)
                        || (item.getQuantity() > 0 && !corrections)) {
                    if (deliveredGoods.containsKey(item.getProduct().getProductId())) {
                        Item existing = deliveredGoods.get(item.getProduct().getProductId());
                        if (log.isDebugEnabled()) {
                            log.debug("getReleasedDeliveries() updating delivery [product="
                                    + existing.getProduct().getProductId()
                                    + ", existingDelivery=" + existing.getQuantity()
                                    + ", nextDelivery=" + item.getQuantity()
                                    + "]");
                        }
                        existing.setQuantity(existing.getQuantity() + item.getQuantity());
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("getReleasedDeliveries() adding delivery [product="
                                    + item.getProduct().getProductId()
                                    + ", delivery=" + item.getQuantity()
                                    + "]");
                        }
                        deliveredGoods.put(
                                item.getProduct().getProductId(),
                                (Item) item.clone());
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            for (Iterator<Long> i = deliveredGoods.keySet().iterator(); i.hasNext();) {
                Long id = i.next();
                Item next = deliveredGoods.get(id);
                log.debug("getReleasedDeliveries() result: [product=" + next.getProduct().getProductId()
                        + ", quantity=" + next.getQuantity() + "]");
            }
        }
        return deliveredGoods;
    }

    private void copyDeliveryDate(Item item) {
        if (item instanceof OrderItem) {
            OrderItem orderItem = (OrderItem) item;
            if (orderItem.getDelivery() != null) {
                for (Iterator<Item> i = getItems().iterator(); i.hasNext();) {
                    OrderItem next = (OrderItem) i.next();
                    if (next.getProduct().getProductId().equals(orderItem.getProduct().getProductId())) {
                        next.setDelivery(orderItem.getDelivery());
                        next.setDeliveryDateBy(orderItem.getDeliveryDateBy());
                        next.setDeliveryConfirmed(orderItem.isDeliveryConfirmed());
                        if (log.isDebugEnabled()) {
                            log.debug("copyDeliveryDate() updated item [product=" + next.getProduct().getProductId()
                                    + ", delivery=" + orderItem.getDelivery()
                                    + ", confirmed=" + orderItem.isDeliveryConfirmed()
                                    + "]");
                        }
                        break;
                    }
                }
            }
        }
    }

    private Item fetchSameBooking(List<Item> items, Item src) {
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.getProduct().getProductId().equals(src.getProduct().getProductId())
                    && isSameBookingType(src, next)) {
                return next;
            }
        }
        return null;
    }

    private boolean isSameBookingType(Item src, Item tgt) {
        if (src.getQuantity() > 0 && tgt.getQuantity() > 0) {
            return true;
        }
        if (src.getQuantity() < 0 && tgt.getQuantity() < 0) {
            return true;
        }
        return false;
    }

    /**
     * Dumps all items of order and parameter maps when class logger's debug mode is enabled
     * @param method name
     * @param allDeliveredGoods map
     * @param allDeliveredCorrections map
     */
    private void dumpItems(String method, Map<Long, Item> allDeliveredGoods, Map<Long, Item> allDeliveredCorrections) {
        if (log.isDebugEnabled()) {
            log.debug(method + "() invoked [order=" + getId()
                    + ", deliveries=" + allDeliveredGoods.size()
                    + ", corrections=" + allDeliveredCorrections.size() + "]");
            for (int i = 0, j = getItems().size(); i < j; i++) {
                Item next = getItems().get(i);
                if (log.isDebugEnabled()) {
                    log.debug(method + "() ordered item [product="
                            + next.getProduct().getProductId()
                            + ", quantity=" + next.getQuantity()
                            + ", delivered=" + next.getDelivered()
                            + ", name=" + next.getProduct().getName()
                            + "]");
                }
            }
            for (Iterator<Long> i = allDeliveredGoods.keySet().iterator(); i.hasNext();) {
                Item next = allDeliveredGoods.get(i.next());
                if (log.isDebugEnabled()) {
                    log.debug(method + "() allDeliveredGoods content: [product="
                            + next.getProduct().getProductId()
                            + ", quantity=" + next.getQuantity()
                            + ", delivered=" + next.getDelivered()
                            + ", name=" + next.getProduct().getName()
                            + "]");
                }
            }
            for (Iterator<Long> i = allDeliveredCorrections.keySet().iterator(); i.hasNext();) {
                Item next = allDeliveredCorrections.get(i.next());
                if (log.isDebugEnabled()) {
                    log.debug(method + "() allDeliveredCorrections content: [product="
                            + next.getProduct().getProductId()
                            + ", quantity=" + next.getQuantity()
                            + ", delivered=" + next.getDelivered()
                            + ", name=" + next.getProduct().getName()
                            + "]");
                }
            }
        }
    }
}
