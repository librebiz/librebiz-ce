/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 3, 2009 9:17:12 AM 
 * 
 */
package com.osserp.core.service.products;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

import com.osserp.core.dao.ProductSelectionConfigs;
import com.osserp.core.dao.ProductSelectionContexts;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigManager;
import com.osserp.core.products.ProductSelectionContext;
import com.osserp.core.products.ProductType;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSelectionConfigManagerImpl extends AbstractService implements ProductSelectionConfigManager {
    private ProductSelectionConfigs selectionConfigs = null;
    private ProductSelectionContexts selectionContexts = null;

    protected ProductSelectionConfigManagerImpl(ProductSelectionConfigs selectionConfigs, ProductSelectionContexts selectionContexts) {
        super();
        this.selectionConfigs = selectionConfigs;
        this.selectionContexts = selectionContexts;
    }

    public ProductSelectionContext getContext(Long id) {
        return selectionContexts.load(id);
    }

    public List<ProductSelectionContext> getSelectionContexts() {
        return selectionContexts.findAll();
    }

    public List<ProductSelectionConfig> getSelectionConfigs(ProductSelectionContext context) {
        return selectionConfigs.findAll(context);
    }

    public List<ProductSelectionConfig> getSelectionConfigs() {
        return selectionConfigs.findAll();
    }

    public ProductSelectionConfig getSelectionConfig(Long id) {
        return selectionConfigs.load(id);
    }

    public ProductSelectionConfig create(
            Employee user, ProductSelectionContext context, String name, String description)
            throws ClientException {
        return selectionConfigs.create(user, context, name, description);
    }

    public ProductSelectionConfig restrict(
            Employee user,
            ProductSelectionConfig config,
            SystemCompany company,
            BranchOffice branch) throws ClientException {
        return selectionConfigs.restrict(user, config, company, branch);
    }

    public ProductSelectionConfig update(
            Employee user,
            ProductSelectionConfig config,
            ProductSelectionContext context,
            String name,
            String description,
            boolean disabled) throws ClientException {
        validateName(user, name, config);
        config.setName(name);
        config.setDescription(description);
        config.setContext(context);
        config.setDisabled(disabled);
        selectionConfigs.save(config);
        return selectionConfigs.load(config.getId());
    }

    public ProductSelectionConfig addSelection(
            Employee user,
            ProductSelectionConfig config,
            ProductType type,
            ProductGroup group,
            ProductCategory category) throws ClientException {
        Long typeId = null;
        Long groupId = null;
        Long categoryId = null;
        if (type != null) {
            typeId = type.getId();
        }
        if (group != null) {
            groupId = group.getId();
        }
        if (category != null) {
            categoryId = category.getId();
        }
        selectionConfigs.addItem(user.getId(), config.getId(), "product_type_id", typeId, "product_group_id", groupId, "product_category_id", categoryId);
        return selectionConfigs.load(config.getId());
    }

    public ProductSelectionConfig addSelection(
            Employee user,
            ProductSelectionConfig config,
            Product product) throws ClientException {
        selectionConfigs.addItem(user.getId(), config.getId(), "product_id", product.getProductId());
        return selectionConfigs.load(config.getId());
    }

    public ProductSelectionConfig removeSelection(
            Employee user,
            ProductSelectionConfig config,
            Long id) throws ClientException {
        selectionConfigs.deleteItem(id);
        return selectionConfigs.load(config.getId());
    }

    public void toggleExclusion(Long itemId) throws ClientException {
        selectionConfigs.toggleExclusion(itemId);
    }

    /**
     * Validates a name update depending on operation context
     * @param user updating name
     * @param name to update
     * @param config if opration is update, null if create context
     * @throws ClientException if product is null or empty or name already exists
     * @throws IllegalArgumentException if user is null
     */
    private void validateName(Employee user, String name, ProductSelectionConfig config) throws ClientException {
        if (user == null) {
            throw new IllegalArgumentException("user must not be null");
        }
        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        List<ProductSelectionConfig> existing = selectionConfigs.findAll();
        for (int i = 0, j = existing.size(); i < j; i++) {
            ProductSelectionConfig next = existing.get(i);
            if (next.getName() != null && (config == null || !next.getId().equals(config.getId()))) {
                String existingName = next.getName().toLowerCase();
                if (existingName.equals(name.toLowerCase())) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }
    }
}
