<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.templateView}"/>
<c:set var="dmsUrl" value="${requestScope.templateDocumentUrl}"/>

<v:form url="${dmsUrl}/save" multipart="true">
<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${view.filesMode}">
                        <fmt:message key="templateCreateFileHeader"/>
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="templateCreate"/>
                    </c:otherwise>
                </c:choose>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:choose>
                <c:when test="${view.recordMode}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="recordType"><fmt:message key="recordType" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="categoryId" size="1" id="categoryId" class="form-control">
                                    <c:forEach var="obj" items="${view.recordTypes}">
                                        <c:if test="${obj.supportingPrint}">
                                            <option value="<o:out value="${obj.id}"/>" <c:if test="${view.recordType.id == obj.id}">selected="selected"</c:if>>
                                                <o:out value="${obj.name}"/>
                                            </option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:when test="${view.filesMode}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name"><fmt:message key="name" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:text name="name" styleClass="form-control" value="${view.bean.template.name}"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="uploadHints"> </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <fmt:message key="templateCreateFileHintName1"/><br />
                                <fmt:message key="templateCreateFileHintName2"/>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name"><fmt:message key="name" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:text name="name" styleClass="form-control" value="${view.bean.template.name}"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="uploadHints"> </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <fmt:message key="templateCreateCustomHintName"/>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${view.branchSelectionAvailable}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="branchSelection"><fmt:message key="branch" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <oc:select name="branchId" value="${view.selectedBranch.id}" options="${view.branchOfficeList}" styleClass="form-control" prompt="true" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="branchHint"> </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <fmt:message key="templateCreateCustomHintBranch"/>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="branchSelection"><fmt:message key="branch" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <oc:select name="branchId" value="${view.selectedBranch.id}" options="${view.branchOfficeList}" styleClass="form-control" disabled="true" />
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
            <c:if test="${!view.recordMode}">
                <c:if test="${!view.filesMode}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="description"><fmt:message key="description" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:textarea styleClass="form-control" name="note" value="${view.bean.template.description}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="filename"><fmt:message key="filename" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text name="filename" styleClass="form-control" value="${view.bean.filename}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="templateCreateHint"><fmt:message key="attention" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:if test="${view.filesMode}">
                                <fmt:message key="templateCreateFileHintFilename"/><br />
                            </c:if>
                            <fmt:message key="templateCreateCustomHintFilename1"/><br/>
                            <fmt:message key="templateCreateCustomHintFilename2"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="fileUpload"><fmt:message key="templateLabel"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:fileUpload />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${dmsUrl}/disableCreateMode"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="save" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</v:form>
