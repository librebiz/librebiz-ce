/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 21, 2005 9:58:36 AM 
 * 
 */
package com.osserp.core.calc;

import java.math.BigDecimal;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.LockException;

import com.osserp.core.BusinessCase;
import com.osserp.core.ItemListManager;
import com.osserp.core.finance.Record;
import com.osserp.core.products.Product;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CalculationManager extends ItemListManager {

    /**
     * Provides a list of all calculations of a reference
     * @param reference
     * @param context
     * @return list of calculation entries
     */
    List<CalculationSearchResult> findByReference(Long reference, String context);

    /**
     * Creates a new calculation.
     * @param user
     * @param businessCase
     * @param name
     * @param calculatorName
     * @param source
     * @return calculation
     * @throws ClientException
     */
    Calculation create(DomainUser user, BusinessCase businessCase, String name, String calculatorName, Calculation source) throws ClientException;

    /**
     * Creates a new calculation by existing record.
     * @param user creating new calculation
     * @param businessCase new calculation belongs to
     * @param record not backed by calculation before
     * @return new created calculation with record items added
     * @throws ClientException if required values missing 
     */
    Calculation create(DomainUser user, BusinessCase businessCase, Record record) throws ClientException;

    /**
     * Adds a discount to calculation based on given item
     * @param user
     * @param calculation
     * @param itemToDiscount
     * @return calculation
     * @throws ClientException if nothing discountable found
     */
    Calculation addDiscount(DomainUser user, Calculation calculation, Long itemToDiscount) throws ClientException;

    /**
     * Removes given product from given calculation
     * @param user
     * @param calculation
     * @param itemId
     * @return calculation
     */
    Calculation removeProduct(DomainUser user, Calculation calculation, Long itemId);

    /**
     * Changes the display status of the price
     * @param user
     * @param calculation
     * @param itemId
     * @return calculation
     * @throws ClientException if price should be disabled and item does not support this
     */
    Calculation changePriceDisplayStatus(DomainUser user, Calculation calculation, Long itemId)
            throws ClientException;

    /**
     * Changes the display status of all prices
     * @param user
     * @param calculation
     * @param enable
     * @return calculation
     * @throws ClientException if operation not supported by calculation
     */
    Calculation changePriceDisplayStatus(DomainUser user, Calculation calculation, boolean enable)
            throws ClientException;

    /**
     * Moves up an item
     * @param user
     * @param calculation
     * @param itemId
     * @return calculation
     */
    Calculation moveUpOptionItem(DomainUser user, Calculation calculation, Long itemId);

    /**
     * Moves down an item
     * @param user
     * @param calculation
     * @param itemId
     * @return calculation
     */
    Calculation moveDownOptionItem(DomainUser user, Calculation calculation, Long itemId);

    /**
     * Creates calculation options if none exist.
     * @param user
     * @param calculation
     * @return calculation
     */
    Calculation createOptions(DomainUser user, Calculation calculation);

    /**
     * Adds an optional product to given calculation
     * @param user
     * @param calculation
     * @param currentPosition
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param note
     * @return calculation
     * @throws ClientException if validation failed
     */
    Calculation addOptionalProduct(
            DomainUser user,
            Calculation calculation,
            Long currentPosition,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            String note)
            throws ClientException;

    /**
     * Removes an optional product
     * @param user
     * @param calculation
     * @param itemId
     * @return calculation
     */
    Calculation removeOptionalProduct(DomainUser user, Calculation calculation, Long itemId);

    /**
     * Updates the values of an existing optional item
     * @param user
     * @param calculation
     * @param itemId
     * @param customName
     * @param quantity
     * @param price
     * @param note
     * @return calculation
     * @throws ClientException if validation failed
     */
    Calculation updateOptionalItem(
            DomainUser user,
            Calculation calculation,
            Long itemId,
            String customName,
            Double quantity,
            BigDecimal price,
            String note)
            throws ClientException;

    /**
     * Replaces an existing product
     * @param user
     * @param calculation
     * @param itemId
     * @param replaceWithProduct
     * @param quantity
     * @param price
     * @param note
     * @return calculation
     * @throws ClientException if quantity is not set
     */
    Calculation replaceOptionalItem(
            DomainUser user,
            Calculation calculation,
            Long itemId,
            Long replaceWithProductId,
            Double quantity,
            BigDecimal price,
            String note)
            throws ClientException;

    /**
     * Changes calculator
     * @param user
     * @param calculation
     * @param name
     * @return calculation
     */
    Calculation changeCalculator(DomainUser user, Calculation calculation, String name);

    /**
     * Changes calculator
     * @param user
     * @param calculation
     * @param name
     * @return calculation
     */
    Calculation changeName(DomainUser user, Calculation calculation, String name);

    /**
     * Toggles calculation sales price lock status
     * @param user
     * @param calculation
     * @return calculation
     */
    Calculation toggleSalesPriceLock(DomainUser user, Calculation calculation);

    /**
     * Checks that calculation related to given sales is not locked
     * @param bc
     * @throws LockException if calculation is locked
     */
    void checkLock(BusinessCase bc) throws LockException;

    /**
     * Checks that calculation related to given record is not locked
     * @param obj
     * @throws LockException if calculation is locked
     */
    void checkLock(Record obj) throws LockException;

    /**
     * Locks a calculation
     * @param calculation
     * @param user
     * @return calculation
     */
    Calculation lock(Calculation calculation, DomainUser user);

    /**
     * Unlocks a calculation
     * @param calculation
     * @param user
     * @return calculation
     */
    Calculation unlock(Calculation calculation, DomainUser user);

    /**
     * Updates createdBy property if necessary
     * @param user
     * @param calculation
     * @return calculation
     */
    Calculation updateCreatedBy(DomainUser user, Calculation calculation);

    /**
     * Set plan available in a calculation
     * @param user
     * @param calculation
     * @param planAvailable
     * @return calculation
     */
    Calculation updatePlanAvailable(DomainUser user, Calculation calculation, boolean planAvailable);

    /**
     * Adds an template to calculation
     * @param user
     * @param calculation
     * @param template
     * @param wholeSale
     * @return calculation
     */
    Calculation applyTemplate(DomainUser user, Calculation calculation, CalculationTemplate template, boolean wholeSale) throws ClientException;

    /**
     * Removes an template from calculation
     * @param user
     * @param calculation
     * @param template
     * @return calculation
     */
    Calculation removeTemplate(DomainUser user, Calculation calculation, CalculationTemplate template);
}
