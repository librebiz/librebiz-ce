<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="branchEdit"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <c:if test="${view.multipleClientsAvailable}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status"><fmt:message key="contact"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${view.bean.contact.displayName}" styleClass="form-value"/>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="companyReference"><fmt:message key="company"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <oc:select styleClass="form-control" name="reference" options="${view.companies}" value="${view.bean.company.id}" prompt="false" />
                            </div>
                        </div>
                    </div>
                </c:if>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="branchName"><fmt:message key="branch"/> / <fmt:message key="name"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="name"/>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="shortkey"><fmt:message key="shortkey"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="shortkey"/>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="shortname"><fmt:message key="logicalName"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="shortname"/>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="settings"><fmt:message key="settings"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="headquarter">
                                    <v:checkbox id="headquarter" /> 
                                    <fmt:message key="headquarter" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="thirdparty"> 
                                    <v:checkbox id="thirdparty" /> 
                                    <fmt:message key="dedicatedLegalform" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="thirdparty"> 
                                    <v:checkbox id="customTemplates" /> 
                                    <fmt:message key="customTemplates" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="recordTemplatesDefault"> 
                                    <v:checkbox id="recordTemplatesDefault" /> 
                                    <fmt:message key="defaultRecordTemplates" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email"><fmt:message key="email"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="email"/>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="costCenter"><fmt:message key="costCenter"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="costCenter"/>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="phone"><fmt:message key="phone"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <oc:selectCountry name="phoneCountry" styleClass="form-control" type="prefixes" prompt="false" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <v:text name="phonePrefix" styleClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v:text name="phoneNumber" styleClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row next">
                    <div class="col-md-4"> </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="/company/branch/disableEditMode"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
            </div>
        </div>
    </div>
        
</div>
