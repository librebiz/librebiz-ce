/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 15, 2007 3:44:11 PM 
 * 
 */
package com.osserp.gui.web.struts;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.web.DocumentUtil;
import com.osserp.common.web.PortalView;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewManager;
import com.osserp.common.web.struts.AbstractAction;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.ServiceManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractViewAction extends AbstractAction {
    private static Logger log = LoggerFactory.getLogger(AbstractViewAction.class.getName());

    /**
     * Sends javascript request hiding named popup
     * @param mapping
     * @param request
     * @param popupName
     * @return forward
     * @throws Exception
     */
    protected ActionForward closePopup(
            ActionMapping mapping,
            HttpServletRequest request,
            String popupName)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("closePopup() invoked...");
        }
        String targetPopup = popupName;
        if (targetPopup.indexOf("_popup") < 0) {
            targetPopup = targetPopup + "_popup";
        }
        request.setAttribute("targetPopup", targetPopup);
        return mapping.findForward(Actions.CLOSE_POPUP);
    }

    /**
     * Sends javascript response reloading last requested (current) page
     * @param mapping
     * @return forward
     * @throws Exception
     */
    protected ActionForward reloadParent(ActionMapping mapping) throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("reloadParent() invoked...");
        }
        return mapping.findForward(Actions.PARENT_RELOAD_POPUP);
    }

    /**
     * Stores stylesheet and xml document in session scope and forwards to 'pdf' target
     * @param mapping
     * @param request
     * @param styleSheet name
     * @param xml document
     * @return forward
     * @throws Exception
     */
    protected ActionForward forwardPdfOutput(
            ActionMapping mapping,
            HttpServletRequest request,
            String styleSheet,
            Document xml)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("forwardPdfOutput() invoked with xsl...");
        }
        DocumentUtil.setDocument(request.getSession(true), xml, styleSheet);
        return mapping.findForward(Actions.PDF);
    }

    /**
     * Stores document binary in session scope and forwards to 'pdf' target
     * @param mapping
     * @param request
     * @param pdf
     * @return forward
     * @throws Exception
     */
    protected ActionForward forwardPdfOutput(
            ActionMapping mapping,
            HttpServletRequest request,
            byte[] pdf)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("forwardPdfOutput() invoked with binary output...");
        }
        DocumentUtil.setPdfDocument(request.getSession(true), pdf);
        return mapping.findForward(Actions.PDF);
    }

    /**
     * Stores document data in session scope and forwards to 'pdf' target
     * @param mapping
     * @param request
     * @param pdf
     * @return forward
     * @throws Exception
     */
    protected ActionForward forwardPdfOutput(
            ActionMapping mapping,
            HttpServletRequest request,
            DocumentData pdf)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("forwardPdfOutput() invoked with binary output...");
        }
        DocumentUtil.setPdfDocument(request.getSession(true), pdf);
        return mapping.findForward(Actions.PDF);
    }

    /**
     * Stores xml document in session scope and forwards to 'xml' target
     * @param mapping
     * @param request
     * @param xml document
     * @return forward
     * @throws Exception
     */
    protected ActionForward forwardXmlOutput(
            ActionMapping mapping,
            HttpServletRequest request,
            Document xml)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("forwardXmlOutput() invoked...");
        }
        DocumentUtil.setXmlDocument(request.getSession(true), xml);
        return mapping.findForward(Actions.XML);
    }

    /**
     * Stores xls spreadsheet in session scope and forwards to 'xml' target
     * @param mapping
     * @param request
     * @param spreadsheet data as tab separated columns as string
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardXlsOutput(
            ActionMapping mapping,
            HttpServletRequest request,
            String spreadsheet)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardXlsOutput() invoked...");
        }
        DocumentUtil.setXlsDocument(session, spreadsheet);
        return mapping.findForward(Actions.XLS);
    }

    /**
     * Creates an error xml document
     * @param request
     * @param exception
     * @return document
     */
    public Document createErrorXml(HttpServletRequest request, Exception exception) {
        Document doc = new Document();
        Element root = new Element("root").setAttribute("type", "error");
        root.addContent(new Element("error").setText(RequestUtil.getResourceString(request, exception.getMessage())));
        doc.setRootElement(root);
        return doc;
    }

    /**
     * Tries to fetch a view
     * @param session
     * @param viewClass
     * @return view or null if none available
     */
    protected View fetchView(HttpSession session, Class<? extends View> viewClass) {
        View view = ServiceManager.fetchView(session, viewClass);
        return view;
    }

    /**
     * Provides current portal view
     * @param session
     * @return portal view
     */
    protected PortalView fetchPortalView(HttpSession session) {
        PortalView view = (PortalView) this.fetchView(session, PortalView.class);
        return view;
    }

    /**
     * Creates a new view by view name (e.g. qualified class name of view interface
     * @param request
     * @param name
     * @return new created view
     * @throws ClientException if any required validation failed
     * @throws PermissionException if current user has no permission to create a view of given type
     */
    protected View createView(HttpServletRequest request, String name) throws ClientException, PermissionException {
        View view = ServiceManager.createView(
                servlet.getServletContext(), request, name);
        return view;
    }

    /**
     * Removes a view from session context
     * @param session
     * @param _class
     */
    protected void removeView(HttpSession session, Class<? extends View> _class) {
        ServiceManager.removeView(session, _class);
    }

    /**
     * Solves error key and stores message found in session scope
     * @param request
     * @param errorKey
     * @return forward to last displayed page
     */
    protected ActionForward saveErrorAndReturn(HttpServletRequest request, String errorKey) {
        saveError(request, errorKey);
        try {
            return new ActionForward(fetchPortalView(request.getSession()).getPage().getName());
        } catch (Throwable t) {
            log.error("saveErrorAndReturn() failed [message=" + t.getMessage() + "]", t);
            throw new ActionException("page could not be resolved");
        }
    }

    /**
     * Forwards to last displayed page
     * @param request
     * @return forward to last displayed page
     */
    protected ActionForward returnToCurrent(HttpServletRequest request) {
        try {
            return new ActionForward(fetchPortalView(request.getSession()).getPage().getName());
        } catch (Throwable t) {
            log.error("returnToCurrent() failed [message=" + t.getMessage() + "]", t);
            throw new ActionException("page could not be resolved");
        }
    }

    /**
     * Stores a dataset under viewName + 'Data' key in session scope.
     * @param request
     * @param viewName
     * @param data
     */
    protected void saveViewData(HttpSession session, String viewName, Map<String, Object> data) {
        ViewManager.storeViewData(session, viewName, data);
    }
}
