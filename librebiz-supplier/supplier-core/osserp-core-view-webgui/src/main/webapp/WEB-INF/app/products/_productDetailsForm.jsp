<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${requestScope.detailsView}"/>

<c:choose>
    <c:when test="${view.createMode}">
        <v:form name="detailsForm" url="${view.baseLink}/save">

            <table class="valueTable">
                <c:if test="${!empty view.existingProperties}">
                    <tr>
                        <td style="width: 50%;"><fmt:message key="existingProperty" /></td>
                        <td><select name="name" class="existingPropertiesOption" size="1">
                                <option value="undefined"><fmt:message key="or" /></option>
                                <c:forEach var="opt" items="${view.existingProperties}">
                                    <option value="<o:out value="${opt.name}"/>"><o:out value="${opt.value}" /></option>
                                </c:forEach>
                        </select></td>
                    </tr>
                </c:if>
                <tr>
                    <td style="width: 50%;"><fmt:message key="newProperty" /></td>
                    <td><v:text name="label" /></td>
                </tr>
                <tr>
                    <td style="width: 50%;"><fmt:message key="value" /></td>
                    <td><v:text name="value" /></td>
                </tr>
                <tr>
                    <td><input type="button" onclick="gotoUrl('<v:url value="${view.baseLink}/disableCreateMode"/>');" value="<fmt:message key="exit"/>" class="submit submitExit" /></td>
                    <td><o:submit /></td>
                </tr>
            </table>
        </v:form>
    </c:when>
    <c:when test="${view.editMode}">
        <v:form name="detailsForm" url="${view.baseLink}/save">

            <table class="valueTable">
                <tr>
                    <td style="width: 50%;"><fmt:message key="${view.bean.name}" /></td>
                    <td><v:text name="value" value="${view.bean.value}" /></td>
                </tr>
                <tr>
                    <td><input type="button" onclick="gotoUrl('<v:url value="${view.baseLink}/disableEditMode"/>');" value="<fmt:message key="exit"/>" class="submit submitExit" /></td>
                    <td><o:submit /></td>
                </tr>
            </table>
        </v:form>
    </c:when>
    <c:otherwise>
        <table class="valueTable">
            <c:choose>
                <c:when test="${!empty view.list}">
                    <c:forEach var="prop" items="${view.list}">
                        <tr>
                            <td style="width: 50%;"><v:link url="${view.baseLink}/select?id=${prop.id}" title="change">
                                    <fmt:message key="${prop.name}" />
                                </v:link></td>
                            <td><o:out value="${prop.value}" /></td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td colspan="2" style="text-align: left;"><v:link url="${view.baseLink}/enableCreateMode" title="newEntry">
                                <fmt:message key="addNewProperty" />
                            </v:link></td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </c:otherwise>
</c:choose>
