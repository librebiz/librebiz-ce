/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 7, 2004 
 * 
 */
package com.osserp.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AddressValidator extends AbstractValidator {
    private static Logger log = LoggerFactory.getLogger(AddressValidator.class.getName());

    private static final int ZIPCODE_MAXLENGTH = 10;
    private static final int ZIPCODE_MAXLENGTH_DE = 5;

    /**
     * Validates a contact address
     * @param street
     * @param zipcode
     * @param city
     * @param country
     * @param required if a complete address is required
     * @throws ClientException if validation failed
     */
    public static void validateAddress(
            String street,
            String zipcode,
            String city,
            Long country,
            boolean required)
            throws ClientException {

        boolean streetSet = isSet(street);
        boolean zipcodeSet = isSet(zipcode);
        boolean citySet = isSet(city);

        if (zipcodeSet && zipcode.length() > ZIPCODE_MAXLENGTH) {
            throw new ClientException(ErrorCode.ZIPCODE_LENGTH);
        }
        if (required && (!streetSet || !zipcodeSet || !citySet)) {
            throw new ClientException(ErrorCode.ADDRESS_INCOMPLETE);
        }
        if (streetSet) {
            if (Character.isWhitespace(street.charAt(0))) {
                throw new ClientException(ErrorCode.STREET_SPACE);
            } else if (Character.isLowerCase(street.charAt(0))) {
                throw new ClientException(ErrorCode.STREET_LOWER);
            }
        }
        if (citySet) {
            if (Character.isWhitespace(city.charAt(0))) {
                throw new ClientException(ErrorCode.CITY_SPACE);
            } else if (Character.isLowerCase(city.charAt(0))) {
                throw new ClientException(ErrorCode.CITY_LOWER);
            }
        }
        if (country != null && country.equals(Constants.GERMANY)) {
            if (zipcodeSet) {
                checkZipcode(country, zipcode);
            }
        }
    }

    /**
     * Checks if given string represents a valid zipcode format
     * @param country
     * @param zipcode
     * @throws ClientException if country is null or zipcode invalid
     */
    public static void checkZipcode(Long country, String zipcode)
            throws ClientException {

        if (log.isDebugEnabled()) {
            log.debug("checkZipcode() invoked [country=" + country + ", zipcode=" + zipcode + "]");
        }

        if (zipcode == null) {
            throw new ClientException(ErrorCode.ZIPCODE_MISSING);
        }
        if (zipcode.length() > ZIPCODE_MAXLENGTH) {
            throw new ClientException(ErrorCode.ZIPCODE_LENGTH);
        }
        if (country == null) {
            throw new ClientException(ErrorCode.COUNTRY_MISSING);
        }
        if (country.equals(Constants.GERMANY)) {
            if (zipcode.length() != ZIPCODE_MAXLENGTH_DE) {
                throw new ClientException(ErrorCode.ZIPCODE_LENGTH);
            }
            try {
                Long.parseLong(zipcode);
            } catch (NumberFormatException nfx) {
                throw new ClientException(ErrorCode.ZIPCODE_INVALID);
            }
        }
    }
}
