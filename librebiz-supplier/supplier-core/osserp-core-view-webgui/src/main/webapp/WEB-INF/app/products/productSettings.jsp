<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productSettingsView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <o:out  value="${view.productName}"/>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="productSettingsContent">
            <div class="row">

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><fmt:message key="stock"/></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="activated"><fmt:message key="currentlyActivated"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="selectAction" size="1" onChange="gotoUrl(this.value);" class="form-control">
                                        <c:forEach var="obj" items="${view.availableStocks}">
                                            <c:choose>
                                                <c:when test="${obj.id == view.selectedStock}">
                                                    <option value="<v:url value="/products/productSettings/selectStock?id=${obj.id}"/>" selected="selected"><o:out value="${obj.name}"/></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="<v:url value="/products/productSettings/selectStock?id=${obj.id}"/>"><o:out value="${obj.name}"/></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><fmt:message key="furtherSettingsLabel"/></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <c:choose>
                            <c:when test="${view.priceByQuantity}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="priceByQuantity">
                                                <o:permission role="product_price_by_quantity_config" info="permissionProductPriceByQuantityConfig">
                                                    <v:ajaxLink linkId="productPriceByQuantityConfiguratorView" url="/products/productPriceByQuantityConfigurator/forward"><fmt:message key="priceByQuantityLabel"/></v:ajaxLink>
                                                </o:permission>
                                                <o:forbidden role="product_price_by_quantity_config">
                                                    <fmt:message key="priceByQuantityLabel"/>
                                                </o:forbidden>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <fmt:message key="bigYes"/>
                                            <o:permission role="product_price_by_quantity_config" info="permissionProductPriceByQuantityConfig">
                                                [<v:link url="/products/productSettings/disablePriceByQuantity"><fmt:message key="deactivate"/></v:link>]
                                            </o:permission>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="priceByQuantity">
                                                <fmt:message key="priceByQuantityLabel"/>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <fmt:message key="bigNo"/>
                                            <o:permission role="product_price_by_quantity_config">
                                                [<v:link url="/products/productSettings/enablePriceByQuantity"><fmt:message key="activate"/></v:link>]
                                            </o:permission>
                                        </div>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>

            </div>
        </div>

    </tiles:put>
</tiles:insert>
