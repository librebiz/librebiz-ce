/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 4, 2007 10:03:11 AM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventConfigType;
import com.osserp.core.events.EventTask;
import com.osserp.core.events.EventTermination;
import com.osserp.core.events.RecipientPool;
import com.osserp.core.views.EventDisplayView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EventConfigs {

    /**
     * Creates a new action
     * @param type
     * @param name
     * @return new created action
     */
    EventAction createAction(EventConfigType type, String name);

    /**
     * Creates a new fcs related action
     * @param requestType
     * @param type
     * @param name
     * @param caller
     * @return new created action
     */
    EventAction createAction(
            Long requestType,
            EventConfigType type,
            String name,
            Long caller);

    /**
     * Sets an alert on given action
     * @param action
     * @param pool
     */
    void setAlert(EventAction action, RecipientPool pool);

    /**
     * Creates a new pool
     * @param name
     * @return new created pool
     */
    RecipientPool createPool(String name);

    /**
     * Provides all event action of given caller
     * @param eventType
     * @param callerId
     * @return actions
     */
    List<EventAction> findActionsByCaller(Long eventType, Long callerId);

    /**
     * Provides all event actions related to flow control actions of given configType and requestType
     * @param configType
     * @param requestType an optional param if event-fcs relations are request type dependant
     * @return actions
     */
    List<EventAction> findFlowControlActions(EventConfigType configType, Long requestType);

    /**
     * Provides all event actions of given event type
     * @param eventType
     * @return actions
     */
    List<EventAction> findActionsByEventType(Long eventType);

    /**
     * Provides all event terminations of given caller
     * @param eventType
     * @param callerId
     * @return terminations
     */
    List<EventTermination> findTerminationsByCaller(Long eventType, Long callerId);

    /**
     * Loads an action
     * @param id
     * @return action
     */
    EventAction loadAction(Long id);
    
    /**
     * Provides all existing actions
     * @return actions or empty list if non defined
     */
    List<EventAction> loadActions();

    /**
     * Loads a termination
     * @param id
     * @return termination
     */
    EventTermination loadTermination(Long id);

    /**
     * Loads a pool by id
     * @param id
     * @return pool
     */
    RecipientPool loadPool(Long id);

    /**
     * Provides all recipient pools
     * @return pools
     */
    List<RecipientPool> findPools();

    /**
     * Loads a type by id
     * @param id
     * @return type
     */
    EventConfigType loadType(Long id);

    /**
     * Provides all config types
     * @return types
     */
    List<EventConfigType> findTypes();

    /**
     * Persists given action
     * @param action
     */
    void save(EventAction action);

    /**
     * Deletes given action
     * @param action
     * @throws ClientException if action could not be deleted
     */
    void delete(EventAction action) throws ClientException;

    /**
     * Persists given termination
     * @param termination
     */
    void save(EventTermination termination);

    /**
     * Deletes a given termination
     * @param termination
     */
    void delete(EventTermination termination);

    /**
     * Persists a pool
     * @param pool to persist
     */
    void save(RecipientPool pool);

    /**
     * Provides an event task by name
     * @param name
     * @return task or null if not exists
     */
    EventTask getTask(String name);

    /**
     * Provides an ordered view on fcs actions starting events
     * @param configType
     * @param requestType
     * @return startActionView
     */
    List<EventDisplayView> findStartActionView(EventConfigType configType, Long requestType);

    /**
     * Synchronizes fcs actions event flag with current event configs. E.g. enables all fcs actions event flags whose are referenced by events and disables all
     * whose are not referenced.
     */
    void synchronizeFcsActions();

    /**
     * Closes all open events of an action. Use this if an action should be deleted.
     * @param action
     */
    void closeEvents(EventAction action);

}
