/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31.05.2012 
 * 
 */
package com.osserp.common.beans;

import com.osserp.common.Value;
import com.osserp.common.util.NumberUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractValue extends OptionImpl implements Value {

    private String objectType = Value.STRING;
    private String key;
    private String styleClass;
    private String style;
    private String formType;
    private String defaultValue;
    private int idx = 0;

    private Object _value = null;

    protected AbstractValue() {
        super();
    }

    public Object getObject() {
        return _value;
    }

    public void setObjectValue(String value) {
        if (Value.BOOLEAN.equalsIgnoreCase(objectType)) {
            if ("true".equalsIgnoreCase(value)) {
                _value = true;
            } else {
                _value = false;
            }
        } else if (Value.INT.equalsIgnoreCase(objectType)) {
            _value = NumberUtil.createInteger(value);
        } else if (Value.LONG.equalsIgnoreCase(objectType)) {
            _value = NumberUtil.createLong(value);
        } else if (Value.DOUBLE.equalsIgnoreCase(objectType)) {
            _value = NumberUtil.createDouble(value);
        } else if (Value.PERCENT.equalsIgnoreCase(objectType)) {
            _value = NumberUtil.createDouble(value);
        } else {
            _value = value;
        }
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }
}
