/**
 *
 * Copyright (C) 2022 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ActionException
import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.PermissionException
import com.osserp.core.BusinessCaseSearch
import com.osserp.core.Item
import com.osserp.core.finance.DeliveryNote
import com.osserp.core.finance.DeliveryNoteManager
import com.osserp.core.finance.DeliveryNoteType
import com.osserp.core.finance.Order
import com.osserp.core.finance.RecordManager
import com.osserp.core.sales.SalesDeliveryNoteManager
import com.osserp.core.sales.SalesOrder

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class DeliveryNoteCreatorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(DeliveryNoteCreatorView.class.getName())

    List<DeliveryNoteType> bookingTypes = []
    DeliveryNoteType selectedBookingType
    List<Item> outOfStockList = []

    DeliveryNoteCreatorView() {
        super()
        dependencies = [ 'salesOrderView' ]
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            if (!salesOrder) {
                logger.warn("initRequest: view or bean not bound [name=salesOrderView.order]")
                throw new ViewContextException(this)
            }
            if (!salesOrder.isUnchangeable()) {
                if (logger.isDebugEnabled()) {
                    logger.debug("initRequest: order is changeable [order=${salesOrder.getId()}]")
                }
                throw new ClientException(ErrorCode.UNRELEASED_RECORD)
            }
            loadBookingTypes()
            Long preselectedTypeId = form.getLong('typeId')
            if (preselectedTypeId) {
                selectedBookingType = deliveryNoteManager.getBookingType(preselectedTypeId)
            }
            salesOrder.openDeliveries.each { Item item ->
                if (!item.product.kanban && item.outOfStockCount > 0) {
                    outOfStockList << item
                }
            }
        }
    }

    void selectType() {
        Long type = form.getLong('typeId')
        if (type) {
            selectedBookingType = deliveryNoteManager.getBookingType(type)
        } else {
            selectedBookingType = null
        } 
    }

    @Override
    void save() {
        if (selectedBookingType) {
            Date deliveryDate = form.getDate('deliveryDate')
            logger.debug("save: invoked [date=$deliveryDate]")
            if (salesOrder.isChangeableDeliveryAvailable()) {
                throw new ClientException(ErrorCode.UNRELEASED_RECORD)
            }
            DeliveryNote note = deliveryNoteManager.create(
                    getDomainUser().getEmployee(),
                    salesOrder,
                    selectedBookingType,
                    deliveryDate)

            if (logger.isDebugEnabled()) {
                logger.debug("create() done [record=${(note == null ? "null" : note.getId())}]")
            }
            setBean(note)
        }
    }

    private void loadBookingTypes() {
        List<DeliveryNoteType> all = deliveryNoteManager.bookingTypes
        all.each { DeliveryNoteType dnt ->
            logger.debug("loadTypes: examining type [id=${dnt.id}, name=${dnt.name}]")
            if (dnt.manualSelection) {
                bookingTypes << dnt
            }
        }
        logger.debug("loadTypes: done [count=${bookingTypes.size()}]")
    }

    SalesOrder getSalesOrder() {
        return env.salesOrderView?.order
    }

    public SalesDeliveryNoteManager getDeliveryNoteManager() {
        return getService(SalesDeliveryNoteManager.class.getName())
    }
}
