<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="documentTypeConfigView" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/popup.jsp" flush="true">
    <tiles:put name="headline">
        <fmt:message key="credentialDisplayHeader" />
    </tiles:put>
    <tiles:put name="content" type="string">
        <fmt:message key="password" />: <span style="margin-left: .5em;"><o:out value="${view.credentials}"/></span>
    </tiles:put>
</tiles:insert>
