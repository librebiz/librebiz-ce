/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 1, 2007 9:35:27 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Parameter;
import com.osserp.common.service.impl.AbstractService;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.Comparators;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.records.DeliveryDateHistories;
import com.osserp.core.dao.records.PurchaseDeliveryNotes;
import com.osserp.core.dao.records.PurchaseInvoices;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.dao.records.RecordActions;
import com.osserp.core.dao.records.RecordQueries;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.dao.records.Records;
import com.osserp.core.dao.records.SalesCancellations;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesDeliveryNotes;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOffers;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.FinanceRecord;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordDisplayItem;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.finance.RecordSearchRequest;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordSearchImpl extends AbstractService implements RecordSearch {
    private static Logger log = LoggerFactory.getLogger(RecordSearchImpl.class.getName());

    private DeliveryDateHistories deliveryDateHistories;
    private PurchaseDeliveryNotes purchaseDeliveryNotes;
    private PurchaseInvoices purchaseInvoices;
    private PurchaseOrders purchaseOrders;
    private SalesCancellations salesCancellations;
    private SalesCreditNotes salesCreditNotes;
    private SalesDeliveryNotes salesDeliveryNotes;
    private SalesInvoices salesInvoices;
    private SalesOffers salesOffers;
    private SalesOrderDownpayments salesDownpayments;
    private SalesOrders salesOrders;
    private RecordQueries recordQueries;
    private RecordTypes recordTypes;
    private RecordActions recordActions;

    /**
     * Creates a new recordSearch
     * @param purchaseDeliveryNotes
     * @param purchaseInvoices
     * @param purchaseOrders
     * @param salesCancellations
     * @param salesCreditNotes
     * @param salesDeliveryNotes
     * @param salesInvoices
     * @param salesOffers
     * @param salesDownpayments
     * @param salesOrders
     * @param recordActions
     * @param recordQueries
     * @param recordTypes
     * @param deliveryDateHistories
     */
    protected RecordSearchImpl(
            PurchaseDeliveryNotes purchaseDeliveryNotes,
            PurchaseInvoices purchaseInvoices,
            PurchaseOrders purchaseOrders,
            SalesCancellations salesCancellations,
            SalesCreditNotes salesCreditNotes,
            SalesDeliveryNotes salesDeliveryNotes,
            SalesInvoices salesInvoices,
            SalesOffers salesOffers,
            SalesOrderDownpayments salesDownpayments,
            SalesOrders salesOrders,
            RecordActions recordActions,
            RecordQueries recordQueries,
            RecordTypes recordTypes,
            DeliveryDateHistories deliveryDateHistories) {
        this.purchaseDeliveryNotes = purchaseDeliveryNotes;
        this.purchaseInvoices = purchaseInvoices;
        this.purchaseOrders = purchaseOrders;
        this.salesCancellations = salesCancellations;
        this.salesCreditNotes = salesCreditNotes;
        this.salesDeliveryNotes = salesDeliveryNotes;
        this.salesInvoices = salesInvoices;
        this.salesOffers = salesOffers;
        this.salesDownpayments = salesDownpayments;
        this.salesOrders = salesOrders;
        this.recordActions = recordActions;
        this.recordQueries = recordQueries;
        this.recordTypes = recordTypes;
        this.deliveryDateHistories = deliveryDateHistories;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordSearch#isProductReferenced(java.lang.Long)
     */
    public boolean isProductReferenced(Long productId) {
        return recordActions.isProductReferenced(productId);
    }

    public List<RecordType> getCommonRecordTypes() {
        List<RecordType> result = new ArrayList<RecordType>();
        List<RecordType> all = getRecordTypes();
        for (int i = 0, j = all.size(); i < j; i++) {
            RecordType recordType = all.get(i);
            if (RecordType.SALES_INVOICE.equals(recordType.getId())
                    || RecordType.SALES_CREDIT_NOTE.equals(recordType.getId())) {
                result.add(recordType);
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordSearch#getRecordTypes()
     */
    public List<RecordType> getRecordTypes() {
        return recordTypes.getRecordTypes();
    }

    public List<RecordType> getSalesRecordTypes() {
        List<RecordType> result = new ArrayList<RecordType>();
        List<RecordType> all = getRecordTypes();
        for (int i = 0, j = all.size(); i < j; i++) {
            RecordType recordType = all.get(i);
            if (RecordType.SALES_INVOICE.equals(recordType.getId())
                    || RecordType.SALES_CREDIT_NOTE.equals(recordType.getId())) {
                result.add(recordType);
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordSearch#findSales(java.lang.Long)
     */
    public Record findSales(Long id) {
        if (salesOffers.exists(id)) {
            return salesOffers.load(id);

        } else if (salesOrders.exists(id)) {
            return salesOrders.load(id);

        } else if (salesInvoices.exists(id)) {
            return salesInvoices.load(id);

        } else if (salesDownpayments.exists(id)) {
            return salesDownpayments.load(id);

        } else if (salesDeliveryNotes.exists(id)) {
            return salesDeliveryNotes.load(id);

        } else if (salesCreditNotes.exists(id)) {
            return salesCreditNotes.load(id);

        } else if (salesCancellations.exists(id)) {
            return salesCancellations.load(id);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordSearch#findPurchase(java.lang.Long)
     */
    public Record findPurchase(Long id) {
        if (purchaseOrders.exists(id)) {
            return purchaseOrders.load(id);

        } else if (purchaseInvoices.exists(id)) {
            return purchaseInvoices.load(id);

        } else if (purchaseDeliveryNotes.exists(id)) {
            return purchaseDeliveryNotes.load(id);
        }
        return null;
    }

    public List<RecordDisplay> findPurchases(Long salesId) {
        List<RecordDisplay> result = new ArrayList<>();
        List<RecordDisplay> invoices = purchaseInvoices.getBySalesId(salesId);
        List<RecordDisplay> orders = purchaseOrders.getBySalesId(salesId);
        for (Iterator<RecordDisplay> ordersList = orders.iterator(); ordersList.hasNext();) {
            RecordDisplay order = ordersList.next();
            if (order.isCompleted()) {
                for (Iterator<RecordDisplay> invoiceList = invoices.iterator(); invoiceList.hasNext();) {
                    RecordDisplay inv = invoiceList.next();
                    if (order.getId().equals(inv.getReference())) {
                        ordersList.remove();
                    }
                }
            }
        }
        result.addAll(orders);
        result.addAll(invoices);
        return result;
    }

    public List<RecordDisplayItem> findPurchaseItems(Long salesId) {
        List<RecordDisplay> recordList = findPurchases(salesId);
        List<RecordDisplayItem> result = new ArrayList<>();
        for (Iterator<RecordDisplay> records = recordList.iterator(); records.hasNext();) {
            RecordDisplay purchase = records.next();
            for (Iterator<RecordDisplayItem> itemList = purchase.getItems().iterator(); itemList.hasNext();) {
                RecordDisplayItem item = (RecordDisplayItem) itemList.next().clone();
                RecordDisplayItem addedItem = fetchItem(result, item.getProductId());
                if (addedItem == null) {
                    result.add(item);
                } else {
                    item.setQuantity(item.getQuantity().doubleValue() + addedItem.getQuantity().doubleValue());
                    result.add(item);
                }
            }
        }
        return result;
    }

    private RecordDisplayItem fetchItem(List<RecordDisplayItem> list, Long productId) {
        RecordDisplayItem result = null;
        for (Iterator<RecordDisplayItem> itemList = list.iterator(); itemList.hasNext();) {
            RecordDisplayItem item = itemList.next();
            if (item.getProductId().equals(productId)) {
                result = item;
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordSearch#findRecord(java.lang.Long, java.lang.Long)
     */
    public Record findRecord(Long id, Long type) {
        Records dao = getRecordDao(type);
        if (dao != null && dao.exists(id)) {
            return dao.load(id);
        }
        return null;
    }
    
    public Long[] getCurrentNumberMinMax(Long type, Long company) {
        Long[] result = new Long[2];
        Records records = getRecordDao(type);
        if (records != null) {
            result[0] = records.getLowestId(company);
            result[1] = records.getHighestId(company);
        }
        return result;
    }

    public Record findSalesRecord(BusinessCase businessCase) {
        boolean offer = businessCase.isSalesContext() ? false : true;
        Long type = !offer ? RecordType.SALES_ORDER : RecordType.SALES_OFFER;
        Records dao = getRecordDao(type);
        List<RecordDisplay> list = dao.getBySalesId(businessCase.getPrimaryKey());
        if (list.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("findSalesRecord() did not find a result [businessCase=" 
                        + businessCase.getPrimaryKey() + "]");
            }
            return null;
        }
        if (!offer || list.size() == 1) {
            RecordDisplay rc = list.get(0);
            return dao.load(rc.getId());
        } 
        Long id = null;
        for (int i = 0, j = list.size(); i < j; i++) {
            RecordDisplay rc = list.get(i);
            if (id == null || id.longValue() < rc.getId().longValue()) {
                id = rc.getId();
            }
        }
        if (id != null) {
            return dao.load(id);
        }
        return null;
    }
    
    public List<FinanceRecord> findByDate(Long type, Date from, Date til) {
        List<FinanceRecord> result = new ArrayList<FinanceRecord>();
        Records recordsDao = getRecordDao(type);
        if (recordsDao != null) {
            result = new ArrayList(recordsDao.getByDate(from, til));
        }
        return sortByCreated(result);
    }

    public List<FinanceRecord> findLatest(Long type) {
        List<FinanceRecord> result = new ArrayList<FinanceRecord>();
        Records recordsDao = getRecordDao(type);
        if (recordsDao != null) {
            result = new ArrayList(recordsDao.getLatest(null));
        }
        return sortByCreated(result);
    }
    
    protected List<FinanceRecord> sortByCreated(List<FinanceRecord> list) {
        return CollectionUtil.sort(list, Comparators.createEntityByCreatedComparator(true));
    }
    
    public List<Parameter> getSearchRequestColumns() {
        return recordQueries.getColumns();
    }
    
    /**
     * Finds all records by provided search options
     * @param request
     * @return records or empty list if none found
     */
    public List<RecordDisplay> find(RecordSearchRequest request) {
        return recordQueries.find(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordSearch#isInvolvedInPurchase(com.osserp.core.contacts.RelatedContact)
     */
    public boolean isInvolvedInPurchase(ClassifiedContact contact) {
        return (purchaseDeliveryNotes.isContactInvolved(contact)
                || purchaseInvoices.isContactInvolved(contact)
                || purchaseOrders.isContactInvolved(contact));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordSearch#findDeliveryDateHistoryByProduct(java.lang.Long)
     */
    public List<DeliveryDateChangedInfo> findDeliveryDateHistoryByProduct(Long productId) {
        return deliveryDateHistories.findByProduct(productId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordSearch#findDeliveryDateHistoryByRecord(java.lang.Long)
     */
    public List<DeliveryDateChangedInfo> findDeliveryDateHistoryByRecord(Long id) {
        return deliveryDateHistories.findByRecord(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordSearch#findItemHistoryByRecord(java.lang.Long)
     */
    public List<ItemChangedInfo> findItemHistoryByRecord(Long id) {
        if (salesOrders.exists(id)) {
            return salesOrders.getItemChangedInfos(id);
        } else if (purchaseOrders.exists(id)) {
            return purchaseOrders.getItemChangedInfos(id);
        }
        return new java.util.ArrayList<ItemChangedInfo>();
    }
    
    public int getInvoiceCount(Long salesId) {
        return salesInvoices.countBySales(salesId);
    }
    
    public int getInvoiceCountAll(Long salesId) {
        return getInvoiceCount(salesId) + salesDownpayments.countBySales(salesId);
    }

    private Records getRecordDao(Long type) {
        if (RecordType.SALES_OFFER.equals(type)) {
            return salesOffers;
        }
        if (RecordType.SALES_ORDER.equals(type)) {
            return salesOrders;
        }
        if (RecordType.SALES_INVOICE.equals(type)) {
            return salesInvoices;
        }
        if (RecordType.SALES_DOWNPAYMENT.equals(type)) {
            return salesDownpayments;
        }
        if (RecordType.SALES_DELIVERY_NOTE.equals(type)) {
            return salesDeliveryNotes;
        }
        if (RecordType.SALES_CREDIT_NOTE.equals(type)) {
            return salesCreditNotes;
        }
        if (RecordType.SALES_CANCELLATION.equals(type)) {
            return salesCancellations;
        }
        if (RecordType.PURCHASE_INVOICE.equals(type)) {
            return purchaseInvoices;
        }
        if (RecordType.PURCHASE_ORDER.equals(type)) {
            return purchaseOrders;
        }
        if (RecordType.PURCHASE_RECEIPT.equals(type)) {
            return purchaseDeliveryNotes;
        }
        return null;
    }

}
