/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.purchasing

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.CollectionUtil

import com.osserp.core.Comparators
import com.osserp.core.Item
import com.osserp.core.products.Product
import com.osserp.core.finance.Order
import com.osserp.core.purchasing.PurchaseOrderManager
import com.osserp.core.suppliers.Supplier
import com.osserp.core.suppliers.SupplierSearch
import com.osserp.core.system.BranchOffice
import com.osserp.core.system.SystemCompany

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class OrderBySalesCreatorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(OrderBySalesCreatorView.class.getName())

    List<Item> items = []
    List<Supplier> suppliers = []
    Supplier selectedSupplier

    OrderBySalesCreatorView() {
        super()
        enableCompanyPreselection()
        dependencies = ['orderBySalesProductCollectorView']
        headerName = 'purchase_order_create'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!env.orderBySalesProductCollectorView) {
                logger.warn("initRequest: not bound [user=${user?.id}, name=orderBySalesProductCollectorView]")
                throw new ViewContextException(this)
            }
            if (env.orderBySalesProductCollectorView.supplierId) {
                selectedSupplier = supplierSearch.findById(
                    env.orderBySalesProductCollectorView.supplierId)
            }
            items = env.orderBySalesProductCollectorView.collected
            if (!selectedSupplier && items) {
                Set suppliersAdded = new HashSet()
                List supplierList = []
                items.each {
                    Product product = it.getProduct()
                    List<Supplier> productSuppliers = supplierSearch.findByDeliveries(product.productId)
                    productSuppliers.each {
                        if (!suppliersAdded.contains(it.id)) {
                            suppliersAdded << it.id
                            supplierList << it
                        }
                    }
                    productSuppliers = supplierSearch.findByProductGroup(product.group.id)
                    productSuppliers.each {
                        if (!suppliersAdded.contains(it.id)) {
                            suppliersAdded << it.id
                            supplierList << it
                        }
                    }
                    suppliers = CollectionUtil.sort(supplierList, Comparators.createContactNameComparator(false))
                    logger.debug("initRequest: suppliers fetched [count=${suppliers.size()}]")
                }
            }
            if (!companies) {
                companies = systemConfigManager.getActiveCompanies()
            }
            if (env.orderBySalesProductCollectorView?.salesOrder?.company != null) {
                selectCompany(env.orderBySalesProductCollectorView.salesOrder.company,
                    env.orderBySalesProductCollectorView.salesOrder.branchId)
            }
            env.creatorTarget = createTarget('/purchaseOrder.do?method=display')
        }
    }

    void selectSupplier() {
        Long id = form.getLong('id')
        if (!id) {
            selectedSupplier = null
        } else {
            selectedSupplier = CollectionUtil.getById(suppliers, id)
            if (!selectedSupplier) {
                // invocation by search result
                selectedSupplier = supplierSearch.findById(id)
            }
        }
        logger.debug("selectSupplier: done [id=${selectedSupplier?.id}]")
    }

    @Override
    void save() {
        logger.debug("save: invoked [company=${selectedCompany?.id}, branch=${selectedBranch?.id}, supplier=${selectedSupplier?.id}]")
        Order order = purchaseOrderManager.create(
                domainEmployee,
                selectedCompany,
                selectedBranch,
                selectedSupplier,
                env.orderBySalesProductCollectorView.salesOrder,
                items)
        logger.debug("save: order created [id=${order?.id}]")
        bean = order
    }

    String getCreatorTarget() {
        "${env.creatorTarget}&id=${bean?.id}&exit=salesOrderDisplay"
    }

    Order getSalesOrder() {
        env.orderBySalesProductCollectorView.salesOrder
    }

    private PurchaseOrderManager getPurchaseOrderManager() {
        getService(PurchaseOrderManager.class.getName())
    }

    private SupplierSearch getSupplierSearch() {
        getService(SupplierSearch.class.getName())
    }
}

