<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="invoiceNumberShort" />
                <o:out value="${view.record.number}" />
                <span> / </span>
                <o:date value="${view.record.created}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="selectedActionName"> <v:link url="${view.baseLink}/selectPaymentType" title="selectType">
                            <fmt:message key="action" />
                        </v:link>
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <fmt:message key="clearDecimalAmount"/>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="dueAmount">
                        <fmt:message key="amount" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <span id="amount" class="form-value">
                        <o:number value="${view.record.dueAmount}" format="currency"/>
                    </span>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <o:submit value="clearAmount" styleClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
