<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="eventPoolsView" />

<v:form name="eventPoolsForm" url="/admin/events/eventPools/save">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <c:choose>
                        <c:when test="${view.createMode}">
                            <fmt:message key="newEventRecipientPool" />
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="changeEventRecipientPool" />
                        </c:otherwise>
                    </c:choose>
                </h4>
            </div>
        </div>
        <div class="panel-body">

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="uniqueName"><fmt:message key="uniqueName" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text styleClass="form-control" name="name" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${view.createMode}">
                                        <v:submitExit url="/admin/events/eventPools/disableCreateMode" />
                                    </c:when>
                                    <c:otherwise>
                                        <v:submitExit url="/admin/events/eventPools/disableEditMode" />
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</v:form>
