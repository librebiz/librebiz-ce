/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2007 8:49:49 AM 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.ViewName;

import com.osserp.core.projects.ProjectSearch;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.users.DomainUser;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.sales.BillingMonitoringView;
import com.osserp.gui.client.sales.SalesListItemComparator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("billingMonitoringView")
public class BillingMonitoringViewImpl extends AbstractView implements BillingMonitoringView {
    private static Logger log = LoggerFactory.getLogger(BillingMonitoringViewImpl.class.getName());
    private SalesListItemComparator comparator = new SalesListItemComparator();

    protected BillingMonitoringViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        refresh();
    }

    @Override
    public void sortList(String method) {
        comparator.sort(method, getList());
    }

    public void refresh() {
        ProjectSearch search = (ProjectSearch) getService(ProjectSearch.class.getName());
        List<SalesListItem> list = search.findByMissingInvoices();
        DomainUser domainUser = getDomainUser();
        for (Iterator<SalesListItem> i = list.iterator(); i.hasNext();) {
            SalesListItem next = i.next();
            if (next.getBranch() != null) {
                if (!domainUser.isBranchAccessible(next.getBranch())) {
                    i.remove();
                }
            }
        }
        comparator.sort(list);
        setList(list);
        if (log.isDebugEnabled()) {
            log.debug("refresh() done");
        }
    }
}
