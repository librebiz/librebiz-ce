/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 6, 2009 5:00:48 PM 
 * 
 */
package com.osserp.core.model.products;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductType;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class ProductSelectionConfigVO extends AbstractProductSelectionConfig {

    protected ProductSelectionConfigVO() {
        super();
    }

    public ProductSelectionConfigVO(ProductSelectionConfig config, ProductSelectionConfigItem item) {
        super(config);
        ProductSelectionConfigItemVO obj = new ProductSelectionConfigItemVO(this, item);
        getItems().add(obj);
    }

    public ProductSelectionConfigVO(
            Employee user,
            String name,
            String description,
            SystemCompany company,
            BranchOffice branch) {
        super(user, null, name, description);
        setCompany(company);
        setBranch(branch);
        setId(-1L);
    }

    @Override
    public void addItem(
            Employee user,
            ProductType type,
            ProductGroup group,
            ProductCategory category) {
        ProductSelectionConfigItemVO obj = new ProductSelectionConfigItemVO(
                user.getId(),
                this,
                type,
                group,
                category);
        getItems().add(obj);
    }
}
