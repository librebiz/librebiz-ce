/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2016 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.service.CacheJob;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateUtil;

import com.osserp.core.Comparators;
import com.osserp.core.dao.EventQueries;
import com.osserp.core.dao.Events;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.events.Event;
import com.osserp.core.model.events.EventImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventQueriesDao extends AbstractDao implements EventQueries {
    private static Logger log = LoggerFactory.getLogger(EventQueriesDao.class.getName());
    
    private SystemConfigs systemConfigs;
    private Events events;
    private List<Event> allEvents = new ArrayList<>();
    
    private transient boolean refreshing = false;

    public EventQueriesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            SystemConfigs systemConfigs,
            Events events) {

        super(jdbcTemplate, tables, sessionFactory);
        this.events = events;
        this.systemConfigs = systemConfigs;
    }

    public List<Event> findByRecipient(Long recipient, boolean includeDeactivated, String orderBy, boolean descending, String category) {
        if (!systemConfigs.isSystemPropertyEnabled("eventCacheEnabled")) {
            return events.findByRecipient(recipient, includeDeactivated, orderBy, descending, category);
        }
        List<Event> result = new ArrayList<>();
        List<Event> events = getAll();
        Date currentDate = DateUtil.getCurrentDate();
        //Set<Long> alreadyAdded = new HashSet<Long>();
        for (Iterator<Event> i = events.iterator(); i.hasNext();) {
            Event next = i.next();
            if (!isEmpty(next.getRecipientId()) 
                    && next.getRecipientId().equals(recipient)) {
                
                //if (!alreadyAdded.contains(next.getAction().getId())) {
                if (next.getActivation() != null && next.getActivation().before(currentDate)) {
                    if (isEmpty(category) ||
                            (("flowControl".equals(category) && next.getAction().getType().isFlowControl())
                                    || ("appointment".equals(category) && next.getAction().getType().isAppointment())
                                    || ("information".equals(category) && next.getAction().getType().isInformation()))) {
                        result.add(next);
                        // alreadyAdded.add(next.getAction().getId());
                    }
                }
                //}
            }
        }
        return sort(result, orderBy, descending);
    }

    public List<Event> findByReference(Long reference) {
        if (!systemConfigs.isSystemPropertyEnabled("eventCacheEnabled")) {
            return events.findByReference(reference);
        }
        List<Event> result = new ArrayList<>();
        List<Event> events = getAll();
        Date currentDate = DateUtil.getCurrentDate();
        Set<Long> alreadyAdded = new HashSet<Long>();
        for (Iterator<Event> i = events.iterator(); i.hasNext();) {
            Event next = i.next();
            if (!isEmpty(next.getReferenceId()) 
                    && next.getReferenceId().equals(reference)) {
                
                if (!alreadyAdded.contains(next.getAction().getId())) {
                    if (next.getActivation() != null && next.getActivation().before(currentDate)) {
                        result.add(next);
                        alreadyAdded.add(next.getAction().getId());
                    }
                }
            }
        }
        return events;
    }
    

    public void refresh(String eventXml) {
        if (log.isDebugEnabled()) {
            log.debug("refresh() invoked [event=" + eventXml + "]");
        }
        long time = System.currentTimeMillis();
        refreshing = true;
        load();
        refreshing = false;
        if (log.isDebugEnabled()) {
            log.debug("refresh() done [duration=" + (System.currentTimeMillis() - time) + "ms]");
        }
    }

    public void closeRunningTasks(CacheJob current) {
        if (log.isInfoEnabled()) {
            log.info("closeRunningTasks() empty method invoked!");
        }
    }

    public CacheJob getCurrentTask() {
        if (log.isInfoEnabled()) {
            log.info("getCurrentTask() empty method invoked, returning null!");
        }
        return null;
    }

    public boolean isRefreshing() {
        return refreshing;
    }

    public void setModified(String reference) {
        if (log.isInfoEnabled()) {
            log.info("setModified() invoked");
        }
        refresh(reference);
    }

    private List<Event> getAll() {
        if (allEvents.isEmpty()) {
            load();
        }
        return new ArrayList(allEvents);
    }
    
    private List<Event> sort(List<Event> events, String orderBy, boolean descending) {
        if ("referenceId".equalsIgnoreCase(orderBy)) {
            return CollectionUtil.sort(events, Comparators.createEventReferenceComparator(descending));
        }
        if ("created".equalsIgnoreCase(orderBy)) {
            return CollectionUtil.sort(events, Comparators.createEventDateComparator(descending, true));
        }
        return CollectionUtil.sort(events, Comparators.createEventDateComparator(descending, false));
    }

    private synchronized void load() {
        allEvents = Collections.synchronizedList(getCurrentSession().createQuery(
                "from " + EventImpl.class.getName() + " event where event.closed = false order by event.id desc").list());
    }
}
