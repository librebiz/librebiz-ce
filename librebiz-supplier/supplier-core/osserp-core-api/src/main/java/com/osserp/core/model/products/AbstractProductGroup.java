/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 27, 2009 9:48:43 PM 
 * 
 */
package com.osserp.core.model.products;

import java.util.Date;

import org.jdom2.Element;

import org.apache.commons.beanutils.BeanUtils;

import com.osserp.common.Constants;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.products.ProductClass;
import com.osserp.core.products.ProductGroup;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractProductGroup extends AbstractProductClassification
        implements ProductGroup {

    ProductClass classification;
    private boolean detailAvailable;
    private Long quantityUnit;
    private Long powerUnit;
    private Double consumerMargin = null;
    private Double resellerMargin = null;
    private Double partnerMargin = null;
    private boolean calculateSales = false;
    private boolean discount = false;
    private boolean providingDatasheet = false;
    private boolean providingPicture = false;
    private boolean serialAvailable = false;
    private boolean serialRequired = false;
    private boolean gtin13Available = false;
    private boolean gtin14Available = false;
    private boolean gtin8Available = false;

    /**
     * Serialization constructor
     */
    protected AbstractProductGroup() {
        super();
    }

    public AbstractProductGroup(Element root) throws Exception {
        super(root);
        for (Object child : root.getChildren()) {
            Element el = (Element) child;
            try {
                if (BeanUtils.getProperty(this, el.getName()) == null) {
                    BeanUtils.setProperty(this, el.getName(), JDOMUtil.getObjectFromElement(el));
                }
            } catch (Exception ex) {
            }
        }
    }

    /**
     * Constructor for new product groups
     * @param name
     * @param description
     * @param createdBy
     * @param classification
     */
    protected AbstractProductGroup(String name, String description, Long createdBy, ProductClass classification) {
        super(name, description, createdBy);
        this.classification = classification;
        this.detailAvailable = false;
        this.quantityUnit = Constants.DEFAULT_QUANTITY_UNIT;
        this.consumerMargin = Constants.DOUBLE_NULL;
        this.resellerMargin = Constants.DOUBLE_NULL;
        this.partnerMargin = Constants.DOUBLE_NULL;
        this.calculateSales = false;
    }

    /**
     * All value constructor
     * @param id
     * @param name
     * @param description
     * @param created
     * @param createdBy
     * @param changed
     * @param changedBy
     * @param resourceKey
     * @param endOfLife
     * @param customNameSupport
     * @param numberRangeId
     * @param classification
     * @param detailAvailable
     * @param quantityUnit
     * @param powerUnit
     * @param consumerMargin
     * @param resellerMargin
     * @param partnerMargin
     * @param calculateSales
     * @param discount
     * @param providingDatasheet
     * @param providingPicture
     * @param serialAvailable
     * @param serialRequired
     */
    protected AbstractProductGroup(
            Long id,
            String name,
            String description,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            String resourceKey,
            boolean endOfLife,
            boolean customNameSupport,
            Long numberRangeId,
            ProductClass classification,
            boolean detailAvailable,
            Long quantityUnit,
            Long powerUnit,
            Double consumerMargin,
            Double resellerMargin,
            Double partnerMargin,
            boolean calculateSales,
            boolean discount,
            boolean providingDatasheet,
            boolean providingPicture,
            boolean serialAvailable,
            boolean serialRequired,
            boolean gtin13Available,
            boolean gtin14Available,
            boolean gtin8Available) {
        super(id, 
                name, 
                description, 
                created, 
                createdBy, 
                changed, 
                changedBy, 
                resourceKey, 
                endOfLife,
                customNameSupport,
                numberRangeId);

        this.classification = classification;
        this.detailAvailable = detailAvailable;
        this.quantityUnit = quantityUnit;
        this.powerUnit = powerUnit;
        this.consumerMargin = consumerMargin;
        this.resellerMargin = resellerMargin;
        this.partnerMargin = partnerMargin;
        this.calculateSales = calculateSales;
        this.discount = discount;
        this.providingDatasheet = providingDatasheet;
        this.providingPicture = providingPicture;
        this.serialAvailable = serialAvailable;
        this.serialRequired = serialRequired;
        this.gtin13Available = gtin13Available;
        this.gtin14Available = gtin14Available;
        this.gtin8Available = gtin8Available;
    }

    /**
     * Clone constructor copying all values
     * @param other
     */
    protected AbstractProductGroup(ProductGroup other) {
        this(
                other.getId(),
                other.getName(),
                other.getDescription(),
                other.getCreated(),
                other.getCreatedBy(),
                other.getChanged(),
                other.getChangedBy(),
                other.getResourceKey(),
                other.isEndOfLife(),
                other.isCustomNameSupport(),
                other.getNumberRangeId(),
                other.getClassification(),
                other.isDetailAvailable(),
                other.getQuantityUnit(),
                other.getPowerUnit(),
                other.getConsumerMargin(),
                other.getResellerMargin(),
                other.getPartnerMargin(),
                other.isCalculateSales(),
                other.isDiscount(),
                other.isProvidingDatasheet(),
                other.isProvidingPicture(),
                other.isSerialAvailable(),
                other.isSerialRequired(),
                other.isGtin13Available(),
                other.isGtin14Available(),
                other.isGtin8Available());
    }

    public ProductClass getClassification() {
        return classification;
    }

    public void setClassification(ProductClass classification) {
        this.classification = classification;
    }

    public boolean isDetailAvailable() {
        return detailAvailable;
    }

    public void setDetailAvailable(boolean detailAvailable) {
        this.detailAvailable = detailAvailable;
    }

    public boolean isSerialAvailable() {
        return serialAvailable;
    }

    public void setSerialAvailable(boolean serialAvailable) {
        this.serialAvailable = serialAvailable;
    }

    public boolean isSerialRequired() {
        return serialRequired;
    }

    public void setSerialRequired(boolean serialRequired) {
        this.serialRequired = serialRequired;
    }

    public boolean isGtin13Available() {
        return gtin13Available;
    }

    public void setGtin13Available(boolean gtin13Available) {
        this.gtin13Available = gtin13Available;
    }

    public boolean isGtin14Available() {
        return gtin14Available;
    }

    public void setGtin14Available(boolean gtin14Available) {
        this.gtin14Available = gtin14Available;
    }

    public boolean isGtin8Available() {
        return gtin8Available;
    }

    public void setGtin8Available(boolean gtin8Available) {
        this.gtin8Available = gtin8Available;
    }

    public Long getPowerUnit() {
        return powerUnit;
    }

    public void setPowerUnit(Long powerUnit) {
        this.powerUnit = powerUnit;
    }

    public Long getQuantityUnit() {
        return quantityUnit;
    }

    public void setQuantityUnit(Long quantityUnit) {
        this.quantityUnit = quantityUnit;
    }

    public Double getConsumerMargin() {
        return consumerMargin;
    }

    public void setConsumerMargin(Double consumerMargin) {
        this.consumerMargin = consumerMargin;
    }

    public Double getResellerMargin() {
        return resellerMargin;
    }

    public void setResellerMargin(Double resellerMargin) {
        this.resellerMargin = resellerMargin;
    }

    public Double getPartnerMargin() {
        return partnerMargin;
    }

    public void setPartnerMargin(Double partnerMargin) {
        this.partnerMargin = partnerMargin;
    }

    public boolean isCalculateSales() {
        return calculateSales;
    }

    public void setCalculateSales(boolean calculateSales) {
        this.calculateSales = calculateSales;
    }

    public boolean isDiscount() {
        return discount;
    }

    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

    public boolean isClassA() {
        return (classification == null ? false
                : ProductClass.CLASS_A.equals(
                        this.classification.getId()));
    }

    public boolean isClassB() {
        return (classification == null ? false
                : ProductClass.CLASS_B.equals(
                        this.classification.getId()));
    }

    public boolean isClassC() {
        return (classification == null ? false
                : ProductClass.CLASS_C.equals(
                        this.classification.getId()));
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("classification", classification));
        root.addContent(JDOMUtil.createElement("consumerMargin", consumerMargin));
        root.addContent(JDOMUtil.createElement("partnerMargin", partnerMargin));
        root.addContent(JDOMUtil.createElement("quantityUnit", quantityUnit));
        root.addContent(JDOMUtil.createElement("powerUnit", powerUnit));
        root.addContent(JDOMUtil.createElement("resellerMargin", resellerMargin));
        root.addContent(JDOMUtil.createElement("calculateSales", calculateSales));
        root.addContent(JDOMUtil.createElement("detailAvailable", detailAvailable));
        root.addContent(JDOMUtil.createElement("providingDatasheet", providingDatasheet));
        root.addContent(JDOMUtil.createElement("providingPicture", providingPicture));
        root.addContent(JDOMUtil.createElement("serialAvailable", serialAvailable));
        root.addContent(JDOMUtil.createElement("serialRequired", serialRequired));
        root.addContent(JDOMUtil.createElement("gtin13Available", gtin13Available));
        root.addContent(JDOMUtil.createElement("gtin14Available", gtin14Available));
        root.addContent(JDOMUtil.createElement("gtin8Available", gtin8Available));
        return root;
    }

    public boolean isProvidingDatasheet() {
        return providingDatasheet;
    }

    public void setProvidingDatasheet(boolean providingDatasheet) {
        this.providingDatasheet = providingDatasheet;
    }

    public boolean isProvidingPicture() {
        return providingPicture;
    }

    public void setProvidingPicture(boolean providingPicture) {
        this.providingPicture = providingPicture;
    }

}
