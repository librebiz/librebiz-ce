/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29.12.2013 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.util.NumberUtil;
import com.osserp.core.dao.SyncMappings;
import com.osserp.core.system.SyncMapping;
import com.osserp.core.model.system.SyncMappingImpl;

/**
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SyncMappingsDao extends AbstractRepository implements SyncMappings {

    protected SyncMappingsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public SyncMapping create(String systemName, String typeName, Long reference, String objectId) {
        SyncMapping mapping = new SyncMappingImpl(reference, systemName, typeName, objectId);
        getCurrentSession().save(SyncMappingImpl.class.getName(), mapping);
        return mapping;
    }

    public String getExternal(String systemName, String typeName, Long reference) {
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(SyncMappingImpl.class.getName())
                .append(" o where o.name = :systemName and o.typeName = :typeName and o.reference = :referenceId");
        List<SyncMapping> mappings = getCurrentSession().createQuery(hql.toString())
                .setParameter("systemName", systemName)
                .setParameter("typeName", typeName)
                .setParameter("referenceId", reference)
                .list();
        return (mappings.isEmpty() ? null : mappings.get(0).getObjectId());
    }

    public Long getInternal(String systemName, String typeName, String objectId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(SyncMappingImpl.class.getName())
                .append(" o where o.name = :systemName and o.typeName = :typeName and o.objectId = :objectId");
        List<SyncMapping> mappings = getCurrentSession().createQuery(hql.toString())
                .setParameter("systemName", systemName)
                .setParameter("typeName", typeName)
                .setParameter("objectId", objectId)
                .list();
        return (mappings.isEmpty() ? null : NumberUtil.createLong(mappings.get(0).getObjectId()));
    }
}
