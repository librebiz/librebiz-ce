/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web

import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.FilterConfig
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.apache.commons.fileupload.FileItemIterator
import org.apache.commons.fileupload.FileItemStream
import org.apache.commons.fileupload.servlet.ServletFileUpload
import org.apache.commons.fileupload.util.Streams

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.FileException
import com.osserp.common.FileObject
import com.osserp.common.web.RequestUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractHttpFilter implements Filter {
    private static Logger logger = LoggerFactory.getLogger(AbstractHttpFilter.class.getName())
    FilterConfig config

    void init(FilterConfig config) {
        this.config = config
    }

    void destroy() {
        config = null
    }

    void doFilter(
            ServletRequest req,
            ServletResponse res,
            FilterChain chain)
    throws IOException, ServletException {

        if (!(req instanceof HttpServletRequest)) {
            chain.doFilter(req, res)
        } else {
            filterHttpRequest((HttpServletRequest) req, (HttpServletResponse) res, chain)
        }
    }

    protected Object fetchContextAttribute(String name) {
        config?.getServletContext()?.getAttribute(name)
    }

    protected Map getRequestAttributes(HttpServletRequest request) {
        def result = [:]
        Enumeration attributes = request.getAttributeNames()
        while (attributes.hasMoreElements()) {
            String name = attributes.nextElement()
            Object value = request.getAttribute(name)
            result[name] = value
        }
        return result
    }

    protected Map getRequestHeaders(HttpServletRequest request) {
        def result = [:]
        Enumeration headerNames = request.getHeaderNames()
        while (headerNames.hasMoreElements()) {
            String name = headerNames.nextElement()
            String value = request.getHeader(name)
            result[name] = value
        }
        return result
    }

    protected Map getRequestParameters(HttpServletRequest request, String uploadDir) {
        if (ServletFileUpload.isMultipartContent(request)) {
            return parseMultipartRequest(request, uploadDir)
        }
        parseRequest(request)
    }

    private Map parseMultipartRequest(HttpServletRequest request, String uploadDir) {
        def result = [:]
        result.uploads = []
        ServletFileUpload fileUpload = new ServletFileUpload()
        try {
            // Note: uri not provided here since multipart urls were excluded by CsrfGuard config
            // This may cause some side effects but none found with concerning pages
            String[] csrfToken = RequestUtil.getCSRFToken(request, null)
            FileItemIterator i = fileUpload.getItemIterator(request)
            while (i.hasNext()) {
                FileItemStream item = i.next()
                String name = item.getFieldName()
                InputStream stream = item.openStream()
                if (item.isFormField()) {
                    String value = Streams.asString(stream)
                    result[name] = value
                    logger.debug("parseMultipartRequest: detected form field [name=$name, value=$value]")
                    checkCSRFToken(csrfToken, name, value)
                } else {
                    logger.debug("parseMultipartRequest: detected file field [name=$name, file=${item.name}]")
                    result.uploads << new FileObject(item.name, stream, 8192, uploadDir)
                }
            }
        } catch (ViewContextException vcx) {
            logger.error("parseMultipartRequest: caught context exception [message=${vcx.message}, exception=${vcx.getClass().getName()}]")
            throw vcx
        } catch (Exception ux) {
            if (ux instanceof FileException && ux.cause?.message) {
                logger.error("parseMultipartRequest: caught exception while parsing multipart content [message=${ux.cause.message}, exception=${ux.getCause().getClass().getName()}]")
            } else {
                logger.error("parseMultipartRequest: caught exception while parsing multipart content [message=${ux.message}, exception=${ux.getClass().getName()}]")
            }
        }
        return result
    }
    
    private void checkCSRFToken(String[] csrfToken, String name, String value) {
        if (csrfToken && csrfToken[0] && csrfToken[1] && csrfToken[0] == name) {
            if (csrfToken[1] != value) {
                logger.warn("checkCSRFToken: found invalid CSRF Token [name=${name}, expected=${csrfToken[1]}, value=${value}]")
                throw new ViewContextException('CSRF Token violation')
            }
        }
    }

    private Map parseRequest(HttpServletRequest request) {
        def result = [:]
        Enumeration paramNames = request.getParameterNames()
        while (paramNames.hasMoreElements()) {
            String name = paramNames.nextElement()
            String[] values = request.getParameterValues(name)
            if (values.length > 1) {
                result[name] = values
            } else {
                result[name] = request.getParameter(name)
            }
        }
        return result
    }

    protected abstract void filterHttpRequest(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException

    protected void redirect(HttpServletRequest request, HttpServletResponse response, String page) {
        config.servletContext.getRequestDispatcher(page).forward(request, response)
    }

    protected boolean isContextAttributeEnabled(String name) {
        Object obj = fetchContextAttribute(name)
        if (obj instanceof String && 'true' == obj) {
            return true
        }
        if (obj instanceof Boolean) {
            return obj
        }
        return false
    }
}
