<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="businessCaseMailView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
        <c:if test="${!empty view.businessCase}">
            <span> - </span><o:out value="${view.businessCase.primaryKey}" />
            <span> - </span><o:out value="${view.businessCase.name}" />
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="businessCaseMailContent">
            <div class="row">
                <c:import url="_businessCaseMailDisplay.jsp" />
            </div>
        </div>
    </tiles:put>
</tiles:insert>
