/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 14.12.2012 
 * 
 */
package com.osserp.core.api

import com.osserp.api.WSProduct
import com.osserp.api.WSProductService
import com.osserp.api.WSObjectException

import com.osserp.core.products.Product
import com.osserp.core.dao.Products
import com.osserp.core.dao.SystemConfigs

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class WsProductServiceImpl implements WSProductService {

    private SystemConfigs systemConfigs
    private Products products
    private WsProductMapper wsProductMapper
    private CoreProductService coreProductService

    protected WsProductServiceImpl(SystemConfigs systemConfigs, 
        Products products, 
        WsProductMapper wsProductMapper, 
        CoreProductService coreProductService) {
        this.systemConfigs = systemConfigs
        this.products = products
        this.wsProductMapper = wsProductMapper
        this.coreProductService = coreProductService
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSProductService#fetchProductById(java.lang.Long)
     */
    public WSProduct fetchProductById(Long productId) {
        Product product = products.find(productId) 
        !product ? null : wsProductMapper.map(product) 
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSProductService#getProductById(java.lang.Long)
     */
    public WSProduct getProductById(Long productId) throws WSObjectException {
        WSProduct result = fetchProductById(productId)
        if (!result) {
            throw new WSObjectException("product.id ${productId} not exists")
        }
        result
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSProductService#createOrUpdate(com.osserp.api.WSProduct)
     */
    public WSProduct createOrUpdate(WSProduct wsProduct) {
        WSProduct result = wsProduct
        if (systemConfigs.isSystemPropertyEnabled('wsProductReceiptEnabled') && coreProductService) {
            Product createdOrUpdated = coreProductService.createOrUpdate(wsProduct)
            if (createdOrUpdated) {
                result = wsProductMapper.map(createdOrUpdated)
            } 
        }
        result
    }
}
