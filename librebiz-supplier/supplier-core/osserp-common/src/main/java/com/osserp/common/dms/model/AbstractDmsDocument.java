/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31 Mar 2007 16:17:58 
 * 
 */
package com.osserp.common.dms.model;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsUtil;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.util.FileUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDmsDocument extends AbstractEntity implements DmsDocument {

    private DocumentType type;
    private String fileName;
    private long fileSize = 0;
    private int persistenceFormat = DocumentType.FORMAT_PLAIN;
    private String contextName;
    private String description;
    private String headerName;
    private String note;
    private boolean referenceDocument = false;
    private boolean unused = false;
    private String language;
    private String suffix;
    private Long referenceType;
    private Date sourceDate;
    private Long categoryId;
    private int orderId = 0;
    private Date validFrom;
    private Date validTil;
    private String messageId;
    private boolean thumbnailAvailable;

    protected AbstractDmsDocument() {
        super();
    }

    public AbstractDmsDocument(DmsDocument otherValue) {
        super(otherValue);
        type = otherValue.getType();
        fileName = otherValue.getFileName();
        fileSize = otherValue.getFileSize();
        referenceDocument = otherValue.isReferenceDocument();
        contextName = otherValue.getContextName();
        headerName = otherValue.getHeaderName();
        description = otherValue.getDescription();
        note = otherValue.getNote();
        language = otherValue.getLanguage();
        suffix = otherValue.getSuffix();
        referenceType = otherValue.getReferenceType();
        categoryId = otherValue.getCategoryId();
        persistenceFormat = otherValue.getPersistenceFormat();
        validFrom = otherValue.getValidFrom();
        validTil = otherValue.getValidTil();
        messageId = otherValue.getMessageId();
        thumbnailAvailable = otherValue.isThumbnailAvailable();
    }

    protected AbstractDmsDocument(
            Long reference,
            DocumentType type,
            String fileName,
            long fileSize,
            Long createdBy,
            String note,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId) {

        this(null, reference, type, fileName, fileSize, createdBy,
                note, categoryId, validFrom, validTil, messageId);
    }

    protected AbstractDmsDocument(
            Long id,
            Long reference,
            DocumentType type,
            String fileName,
            long fileSize,
            Long createdBy,
            String note,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId) {

        super(id, reference, createdBy);
        assert type != null;
        this.type = type;
        this.persistenceFormat = type.getPersistenceFormat(); 
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.note = note;
        this.categoryId = categoryId;
        this.validFrom = validFrom;
        this.validTil = validTil;
        this.messageId = messageId;
        if (type.isSuffixByFile()) {
            suffix = FileUtil.getFileType(fileName);
        }
    }

    public Date getDisplayDate() {
        return sourceDate != null ? sourceDate : getCreated();
    }

    public String getDisplayName() {
        return isSet(note) ? note : fileName;
    }

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public DocumentType getType() {
        return type;
    }

    protected void setType(DocumentType type) {
        this.type = type;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public String getFileNameDisplay() {
        if (isSet(fileName) && type != null 
                && type.isFilesystemStore()) {
            return StringUtil.deleteBlanks(fileName);
        }
        return fileName; 
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getPersistenceFormat() {
        return persistenceFormat;
    }

    public void setPersistenceFormat(int persistenceFormat) {
        this.persistenceFormat = persistenceFormat;
    }

    public boolean isFormatPlain() {
        return DocumentType.FORMAT_PLAIN == persistenceFormat;
    }

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(boolean referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public boolean isUnused() {
        return unused;
    }

    public void setUnused(boolean unused) {
        this.unused = unused;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public final String getRealFileName() {
        if (type == null || 
                (!type.isFilesystemStore() && !type.isTemplate())) {
            return null;
        }
        if (type.isTemplate()) { // another criteria required?
            return fileName;
        }
        return DmsUtil.getRealFileName(
            getId(), 
            getReference(), 
            getType().isSuffixByFile(), 
            getType().getSuffix(), 
            getType().getPrefix(), 
            getType().getPostfix(), 
            suffix, 
            fileName);
    }

    public String getUrl() {
        StringBuilder buffer = new StringBuilder(type.getPrintUrl());
        buffer.append("?id=").append(getId()).append("&type=").append(type.getId());
        return buffer.toString();
    }

    public Long getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(Long referenceType) {
        this.referenceType = referenceType;
    }

	public Date getSourceDate() {
		return sourceDate;
	}

	public void setSourceDate(Date sourceDate) {
		this.sourceDate = sourceDate;
	}

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTil() {
        return validTil;
    }

    public void setValidTil(Date validTil) {
        this.validTil = validTil;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public boolean isThumbnailAvailable() {
        return thumbnailAvailable;
    }

    public void setThumbnailAvailable(boolean thumbnailAvailable) {
        this.thumbnailAvailable = thumbnailAvailable;
    }

}
