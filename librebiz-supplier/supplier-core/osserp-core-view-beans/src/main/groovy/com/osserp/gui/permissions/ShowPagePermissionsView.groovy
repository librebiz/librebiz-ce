/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.permissions

import com.osserp.common.web.PortalView
import com.osserp.gui.CoreView

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author eh <eh@osserp.com>
 *
 */
class ShowPagePermissionsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ShowPagePermissionsView.class.getName())

    ShowPagePermissionsView() {
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
    }

    void setPortalView(PortalView portalView) {
        if (portalView == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("setPortalView() portalView == null")
            }
        } else {
            bean = portalView.getPermissions()
        }
    }
}
