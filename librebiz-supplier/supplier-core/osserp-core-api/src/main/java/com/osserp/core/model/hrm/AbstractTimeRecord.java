/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 1:56:37 PM 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.Date;

import com.osserp.common.Calendar;
import com.osserp.common.PublicHoliday;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordStatus;
import com.osserp.core.hrm.TimeRecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractTimeRecord extends AbstractEntity implements TimeRecord {

    private TimeRecordType type = null;
    private TimeRecordStatus status = null;
    private Date value = null;
    private String note = null;
    private boolean correctionAvailable = false;
    private Long closing = null;
    private boolean systemTime = false;
    private TimeRecord closedBy = null;
    private PublicHoliday publicHoliday = null;
    private boolean ignoreBreakRules = false;
    private boolean ignoreUpperLimit = false;
    private Long terminalId = null;

    protected AbstractTimeRecord() {
        super();
    }

    public AbstractTimeRecord(
            TimeRecordType type,
            TimeRecordStatus status,
            Long reference,
            Employee user,
            Date value,
            String note,
            boolean systemTime) {
        super(null, reference, DateUtil.getCurrentDate(), user.getId());
        this.type = type;
        this.status = status;
        this.note = note;
        this.value = (value == null ? DateUtil.getCurrentDate() : value);
        this.systemTime = systemTime;
    }

    protected AbstractTimeRecord(
            TimeRecordType type,
            TimeRecordStatus status,
            Long reference,
            Long user,
            Date value,
            String note,
            boolean systemTime) {
        super(null, reference, DateUtil.getCurrentDate(), user);
        this.type = type;
        this.status = status;
        this.note = note;
        this.value = (value == null ? DateUtil.getCurrentDate() : value);
        this.systemTime = systemTime;
    }

    public Long getTerminalId() {
        return terminalId;
    }

    protected void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public TimeRecordType getType() {
        return type;
    }

    protected void setType(TimeRecordType type) {
        this.type = type;
    }

    public TimeRecordStatus getStatus() {
        return status;
    }

    protected void setStatus(TimeRecordStatus status) {
        this.status = status;
    }

    public boolean isWaitForApproval() {
        if (status != null
                && (status.getId().equals(TimeRecordStatus.WAITFORAPPROVAL) || status.getId().equals(TimeRecordStatus.CORRECTIONWAITFORAPPROVAL))) {
            return true;
        }
        return false;
    }

    public boolean isApproved() {
        if (status != null && (status.getId().equals(TimeRecordStatus.APPROVED) || status.getId().equals(TimeRecordStatus.CORRECTIONAPPROVED))) {
            return true;
        }
        return false;
    }

    public Date getValue() {
        return value;
    }

    protected void setValue(Date value) {
        this.value = value;
    }

    public String getNote() {
        return note;
    }

    protected void setNote(String note) {
        this.note = note;
    }

    public boolean isBooked(Date date) {
        if (closedBy == null || date == null) {
            return false;
        }
        return value.before(date) && date.before(closedBy.getValue());
    }

    public boolean isCurrentDay() {
        return DateUtil.isToday(value);
    }

    public boolean isMemberOf(int year) {
        int thisYear = DateUtil.getYear(value);
        return (thisYear == year);
    }

    public boolean isMemberOf(int month, int year) {
        int thisMonth = DateUtil.getMonth(value);
        return (thisMonth == month && isMemberOf(year));
    }

    public boolean isDay(Calendar day) {
        if (isMemberOf(day.getMonth(), day.getYear())) {
            return day.getDay().equals(DateUtil.getDay(value));
        }
        return false;
    }

    public boolean isCorrectionAvailable() {
        return correctionAvailable;
    }

    protected void setCorrectionAvailable(boolean correctionAvailable) {
        this.correctionAvailable = correctionAvailable;
    }

    public Long getClosing() {
        return closing;
    }

    protected void setClosing(Long closing) {
        this.closing = closing;
    }

    public void update(TimeRecordStatus status, Date value) {
        if (status != null) {
            this.status = status;
        }
        if (value != null) {
            this.value = value;
        }
    }

    public boolean isSystemTime() {
        return systemTime;
    }

    public void setSystemTime(boolean systemTime) {
        this.systemTime = systemTime;
    }

    public PublicHoliday getPublicHoliday() {
        return publicHoliday;
    }

    public void setPublicHoliday(PublicHoliday publicHoliday) {
        this.publicHoliday = publicHoliday;
    }

    public TimeRecord getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(TimeRecord closedBy) {
        this.closedBy = closedBy;
    }

    public Long getDuration() {
        if (closedBy == null) {
            return 0L;
        }
        return DateUtil.getMinutes(
                Long.valueOf(closedBy.getValue().getTime() - value.getTime()));
    }

    public String getDurationDisplay() {
        long duration = getDuration();
        if (duration == 0) {
            return "";
        }
        StringBuilder buff = new StringBuilder();
        long minutes = duration % 60;
        buff.append(DateFormatter.getFormatted((int) duration / 60)).append(":").append(DateFormatter.getFormatted((int) minutes));
        return buff.toString();
    }

    public boolean isIgnoreBreakRules() {
        return ignoreBreakRules;
    }

    public void setIgnoreBreakRules(boolean ignoreBreakRules) {
        this.ignoreBreakRules = ignoreBreakRules;
    }

    public boolean isIgnoreUpperLimit() {
        return ignoreUpperLimit;
    }

    public void setIgnoreUpperLimit(boolean ignoreUpperLimit) {
        this.ignoreUpperLimit = ignoreUpperLimit;
    }

    public boolean isNullBooking() {
        if (closedBy != null && getDuration() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public Object clone() {
        try {
            super.clone();
        } catch (Exception e) {
        }
        return null;
    }

    protected AbstractTimeRecord(TimeRecord other) {
        super();
        setId(other.getId());
        setCreated(other.getCreated());
        setCreatedBy(other.getCreatedBy());
        this.type = other.getType();
        this.status = other.getStatus();
        this.value = other.getValue();
        this.note = other.getNote();
        this.correctionAvailable = other.isCorrectionAvailable();
        this.closing = other.getClosing();
        this.systemTime = other.isSystemTime();
        this.closedBy = other.getClosedBy();
        this.publicHoliday = other.getPublicHoliday();
        this.ignoreBreakRules = other.isIgnoreBreakRules();
        this.ignoreUpperLimit = other.isIgnoreUpperLimit();
        this.terminalId = other.getTerminalId();
    }
}
