/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 30, 2016 
 * 
 */
package com.osserp.core.finance;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Parameter;

import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordConfigManager {

    /**
     * Provides all record types
     * @return list of record types
     */
    List<RecordType> getRecordTypes();
    
    /**
     * Persists a record type with updated values
     * @param user
     * @param recordType
     * @return recordType with updated properties
     * @throws ClientException if validation of new values failed 
     */
    RecordType update(DomainUser user, RecordType recordType) throws ClientException;
    
    /**
     * Changes/updates the number generator config
     * @param user
     * @param recordType
     * @param numberConfigName
     * @param value
     * @return recordType with updated properties
     * @throws ClientException
     */
    RecordType updateNumberConfig(
            DomainUser user, 
            RecordType recordType,
            String numberConfigName,
            Long value) throws ClientException;
    
    /**
     * Provides available recordNumber create methods
     * @return methods
     */
    List<Parameter> getRecordNumberMethods();
    
    /**
     * Provides available record number configs
     * @param company optional restriction
     * @return configs
     */
    List<RecordNumberConfig> getRecordNumberConfigs(Long company);
    
    /**
     * Provides a record number config by recordType.
     * @param type
     * @param company
     * @return config or null if not exists
     */
    RecordNumberConfig getRecordNumberConfig(RecordType type, Long company);
    
    /**
     * Persists a config
     * @param config
     */
    void save(RecordNumberConfig config);
}
