<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="request" value="${sessionScope.openSalesOrdersView}" />

<c:choose>
    <c:when test="${view.summaryMode}">
        <c:set var="records" scope="request" value="${view.summary}" />
    </c:when>
    <c:otherwise>
        <c:set var="records" scope="request" value="${view.list}" />
    </c:otherwise>
</c:choose>

<c:set var="displayPrice" value="false" />
<o:permission role="purchasing,accounting,executive" info="permissionOpenOrderItemListDisplayPrice">
    <c:set var="displayPrice" value="true" />
</o:permission>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles">
        <style type="text/css">
            .table td.canceled {
               text-decoration: line-through;
            }
        </style>
    </tiles:put>

    <tiles:put name="headline">
        <c:choose>
            <c:when test="${view.productMode}">
                <o:logger write="view is in product mode..." level="debug" />
                <c:choose>
                    <c:when test="${view.listUnreleased}">
                        <o:listSize value="${records}" />
                        <fmt:message key="notReleasedOrderPositionsTo" />
                        <o:out value="${view.product.name}" />
                    </c:when>
                    <c:otherwise>
                        <o:listSize value="${records}" />
                        <fmt:message key="openOrderPositionsTo" />
                        <o:out value="${view.product.name}" />
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:when test="${view.summaryMode}">
                <o:logger write="view is in summary mode..." level="debug" />
                <c:choose>
                    <c:when test="${view.productSummaryMode && !empty view.productSummaryDisplayId}">
                        <fmt:message key="expectedMovesAfterProduct" />:
                        <span title="<o:out value="${view.productSummaryDisplayName}"/>"> <o:out limit="65" value="${view.productSummaryDisplayName}" /></span>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${view.listUnreleased}">
                                <o:listSize value="${records}" />
                                <fmt:message key="notReleasedSinglePositionsAfterProduct" />
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${view.openPurchaseOnly}">
                                        <fmt:message key="orderPositionWithOpenOrderings" />
                                    </c:when>
                                    <c:otherwise>
                                        <o:listSize value="${records}" />
                                        <fmt:message key="openPositionByProduct" />
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <o:logger write="order list mode..." level="debug" />
                <c:choose>
                    <c:when test="${view.listUnreleased}">
                        <o:listSize value="${view.list}" />
                        <fmt:message key="notReleasedOrderPositions" />
                    </c:when>
                    <c:otherwise>
                        <o:listSize value="${view.list}" />
                        <fmt:message key="openOrderPositions" />
                    </c:otherwise>
                </c:choose>
            </c:otherwise>
        </c:choose>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${view.setupMode}">
                    <li><a href="<c:url value="/salesPlanning.do?method=disableSetup"/>"><o:img name="backIcon" /></a></li>
                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${view.productSummaryMode && !empty view.productSummaryDisplayId}">
                            <li><a href="<c:url value="/salesPlanning.do?method=disableProductSummary"/>"><o:img name="backIcon" /></a></li>
                            <li><a href="<c:url value="/salesPlanning.do?method=refresh"/>" title="<fmt:message key="reloadList"/>"><o:img name="replaceIcon" /></a></li>
                            <c:choose>
                                <c:when test="${view.vacancyDisplayMode}">
                                    <li><a href="<c:url value="/salesPlanning.do?method=toggleVacancyDisplay"/>" title="<fmt:message key="displayConfirmedGreater15Only"/>"><o:img name="enabledIcon" /></a></li>
                                </c:when>
                                <c:otherwise>
                                    <li><a href="<c:url value="/salesPlanning.do?method=toggleVacancyDisplay"/>" title="<fmt:message key="displayAllBetweenZeroAndHundred"/>"><o:img name="disabledIcon" /></a></li>
                                </c:otherwise>
                            </c:choose>
                            <li><a href="<c:url value="/salesPlanning.do?method=enableSetup"/>"><o:img name="configureIcon" /></a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="<c:url value="/salesPlanning.do?method=exit"/>"><o:img name="backIcon" /></a></li>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${!view.productMode and !view.productSummaryMode}">
                        <li><a href="<c:url value="/salesPlanning.do?method=refresh"/>" title="<fmt:message key="reloadList"/>"><o:img name="replaceIcon" /></a></li>
                        <li><a href="<c:url value="/salesPlanning.do?method=enableSetup"/>"><o:img name="configureIcon" /></a></li>
                        <c:choose>
                            <c:when test="${view.productMode or view.summaryMode}">
                                <li><a href="<c:url value="/salesPlanning.do?method=changeSummaryMode"/>" title="<fmt:message key="orderData"/>" ><o:img name="reloadIcon" /></a></li>
                            </c:when>
                            <c:otherwise>
                                <li><a href="<c:url value="/salesPlanning.do?method=changeSummaryMode"/>" title="<fmt:message key="productList"/>" ><o:img name="reloadIcon" /></a></li>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                </c:otherwise>
            </c:choose>
            <li><v:link url="/index" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link>
            </li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="salesPlanningListContent">
            <div class="row">
                <div class="col-md-12 panel-area">
                <c:choose>
                    <c:when test="${view.setupMode}">
                        <c:import url="sales_planning_item_list_settings.jsp" />
                    </c:when>
                    <c:when test="${view.productMode}">
                        <c:import url="sales_planning_item_list_products.jsp" />
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${view.summaryMode}">
                                <div class="table-responsive table-responsive-default">
                                    <c:choose>
                                        <c:when test="${empty records}">
                                            <span class="boldtext"><fmt:message key="noOrdersFound" /></span>
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${view.productSummaryMode && !empty view.productSummaryDisplayId}">
                                                    <c:forEach var="record" items="${records}" varStatus="s">
                                                        <c:if test="${view.productSummaryDisplayId == record.productId}">
                                                            <c:set var="warning" value="${record.purchaseOrdered}" />
                                                            <table class="table">
                                                                <thead style="margin-top: -41px;">
                                                                    <tr>
                                                                        <th><a href="<c:url value="/salesPlanning.do?method=sort&order=byIdOrSalesId"/>" title="<fmt:message key="productNumber"/>"><fmt:message key="product" /></a></th>
                                                                        <th><a href="<c:url value="/salesPlanning.do?method=sort&order=bySalesName"/>"><fmt:message key="name" /></a></th>
                                                                        <th class="right"><span title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="order" /></span></th>
                                                                        <th class="right"><span title="<fmt:message key="currentlyInInventory"/>"><fmt:message key="inventory" /></span></th>
                                                                        <th class="right"><span title="<fmt:message key="locatedInStockReceipt"/>"><fmt:message key="stockReceiptShort" /></span></th>
                                                                        <th class="right"><span title="<fmt:message key="orderedBySupplier"/>"><fmt:message key="appointedShortcut" /></span></th>
                                                                    </tr>
                                                                    <tr style="font-weight: normal; padding: 3px;" class="row-alt">
                                                                        <td><a href="<c:url value="/products.do?method=load&id=${record.productId}&stockId=${view.selectedStock.id}&exit=openOrders"/>"><o:out value="${record.productId}" /></a></td>
                                                                        <td><a href="<c:url value="/productStockReport.do?method=forwardGroup&id=${record.productGroup}&exit=openOrders"/>"><o:out value="${record.productName}" /></a></td>
                                                                        <td class="right"><o:number value="${record.quantity}" format="integer" /> <c:if test="${view.vacancyDisplayMode}">(<o:number value="${record.vacant}" format="integer" />)</c:if></td>
                                                                        <td class="right"><o:number value="${record.stock}" format="integer" /></td>
                                                                        <td class="right"><o:number value="${record.receipt}" format="integer" /></td>
                                                                        <td class="right"><o:number value="${record.expected}" format="integer" /></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <c:forEach var="item" items="${record.items}" varStatus="s">
                                                                        <c:choose>
                                                                            <c:when test="${item.salesItem}">
                                                                                <c:if test="${!item.vacant or view.vacancyDisplayMode}">
                                                                                    <tr <c:if test="${item.vacant}">class="red"</c:if>>
                                                                                        <td><a href="<c:url value="/loadSales.do?exit=openOrders&id=${item.salesId}"/>"><o:out value="${item.salesId}" /></a></td>
                                                                                        <td><o:out value="${item.salesName}" /></td>
                                                                                        <td class="right"><o:number value="${item.outstanding * (-1)}" format="integer" /></td>
                                                                                        <td class="right"><o:number value="${item.expectedStock}" format="integer" /></td>
                                                                                        <td class="right" colspan="2" <c:if test="${item.lazy}">class="canceled"</c:if>>
                                                                                            <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${item.salesId}" linkId="businessCaseDisplayView">
                                                                                                <o:date value="${item.delivery}" />
                                                                                            </v:ajaxLink>
                                                                                        </td>
                                                                                    </tr>
                                                                                </c:if>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <tr <c:if test="${!item.deliveryConfirmed}">class="red"</c:if>>
                                                                                    <td><a href="<c:url value="/purchaseOrder.do?method=display&exit=openOrders&id=${item.id}"/>"><o:out value="${item.id}" /></a></td>
                                                                                    <td><o:out value="${item.salesName}" /></td>
                                                                                    <td class="right"><o:number value="${item.outstanding}" format="integer" /></td>
                                                                                    <td class="right"><o:number value="${item.expectedStock}" format="integer" /></td>
                                                                                    <td class="right" colspan="2">
                                                                                        <c:choose>
                                                                                            <c:when test="${empty item.deliveryNote}">
                                                                                                <o:date value="${item.delivery}" />
                                                                                            </c:when>
                                                                                            <c:otherwise>
                                                                                                <o:ajaxLink linkId="deliveryNoteDisplay${item.itemId}" url="/salesPlanning.do?method=displayDeliveryNote&id=${item.itemId}">
                                                                                                    <o:date value="${item.delivery}" />
                                                                                                </o:ajaxLink>
                                                                                            </c:otherwise>
                                                                                        </c:choose>
                                                                                    </td>
                                                                                </tr>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:forEach>
                                                                </tbody>
                                                            </table>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:when>
                                                <c:otherwise>
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th><a href="<c:url value="/salesPlanning.do?method=sort&order=byProductId"/>"><fmt:message key="product" /></a></th>
                                                                <th><a href="<c:url value="/salesPlanning.do?method=sort&order=byProductName"/>"><fmt:message key="name" /></a></th>
                                                                <th class="right"><a href="<c:url value="/salesPlanning.do?method=sort&order=byQuantity"/>" title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="order" /></a></th>
                                                                <th class="right"><a href="<c:url value="/salesPlanning.do?method=sort&order=byStock"/>" title="<fmt:message key="currentlyInInventory"/>"><fmt:message key="inventory" /></a></th>
                                                                <th class="right"><a href="<c:url value="/salesPlanning.do?method=sort&order=byReceipt"/>" title="<fmt:message key="locatedInStockReceipt"/>"><fmt:message
                                                                            key="stockReceiptShort" /></a></th>
                                                                <th class="right"><a href="<c:url value="/salesPlanning.do?method=sort&order=byExpected"/>" title="<fmt:message key="orderedBySupplier"/>"><fmt:message
                                                                            key="appointedShortcut" /></a></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <c:forEach var="record" items="${records}" varStatus="s">
                                                                <c:set var="warning" value="${record.lazy}" />
                                                                <c:if test="${!view.openPurchaseOnly or (view.openPurchaseOnly and warning)}">
                                                                    <tr<c:if test="${!warning}"> class="altrow"</c:if>>
                                                                        <td><a href="<c:url value="/salesPlanning.do?method=enableProductSummary&id=${record.productId}"/>"><o:out value="${record.productId}" /></a></td>
                                                                        <td <c:if test="${warning}">class="error"</c:if>><o:out value="${record.productName}" /></td>
                                                                        <td class="right"><o:number value="${record.quantity}" format="integer" /> <c:if test="${view.vacancyDisplayMode}"> (<o:number value="${record.vacant}"
                                                                                    format="integer" />)</c:if></td>
                                                                        <td class="right"><o:number value="${record.stock}" format="integer" /></td>
                                                                        <td class="right"><o:number value="${record.receipt}" format="integer" /></td>
                                                                        <td class="right"><o:number value="${record.expected}" format="integer" /></td>
                                                                    </tr>
                                                                </c:if>
                                                            </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="table-responsive table-responsive-default">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><a href="<c:url value="/salesPlanning.do?method=sort&order=bySalesId"/>"><fmt:message key="number" /></a></th>
                                                <th><a href="<c:url value="/salesPlanning.do?method=sort&order=bySalesName"/>"><fmt:message key="consignment" /></a></th>
                                                <th><a href="<c:url value="/salesPlanning.do?method=sort&order=byProductId"/>"><fmt:message key="product" /></a></th>
                                                <th><a href="<c:url value="/salesPlanning.do?method=sort&order=byProductName"/>"><fmt:message key="name" /></a></th>
                                                <th><a href="<c:url value="/salesPlanning.do?method=sort&order=byDelivery"/>" title="<fmt:message key="plannedDeliveryDate"/>"><fmt:message key="plannedDeliveryDateShortcut" /></a></th>
                                                <th class="right">
                                                    <c:choose>
                                                        <c:when test="${view.listUnreleased}">
                                                            <a href="<c:url value="/salesPlanning.do?method=sort&order=byQuantity"/>" title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="open" /></a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="<c:url value="/salesPlanning.do?method=sort&order=byOpen"/>" title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="open" /></a>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:choose>
                                                <c:when test="${empty records}">
                                                    <tr>
                                                        <td colspan="7"><fmt:message key="noOrdersFound" /></td>
                                                    </tr>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:forEach var="record" items="${records}" varStatus="s">
                                                        <tr id="sales${record.salesId}product${record.productId}"<c:if test="${record.vacant}"> class="red"</c:if>>
                                                            <td><a href="<c:url value="/loadSales.do?exit=openOrders&id=${record.salesId}&exitId=sales${record.salesId}product${record.productId}"/>"><o:out value="${record.salesId}" /></a></td>
                                                            <td><o:out value="${record.salesName}" /></td>
                                                            <td>
                                                                <o:popupSelect action="/productPopup.do" name="record" property="productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                                                                    <o:out value="${record.productId}" />
                                                                </o:popupSelect>
                                                            </td>
                                                            <td><o:out value="${record.productName}" /></td>
                                                            <td class="right">
                                                                <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${record.salesId}" linkId="businessCaseDisplayView">
                                                                    <o:date value="${record.delivery}" />
                                                                </v:ajaxLink>
                                                            </td>
                                                            <td class="right"><c:choose>
                                                                    <c:when test="${view.listUnreleased}">
                                                                        <o:out value="${record.quantity}" />
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${record.salesId}" linkId="deliveryStatus" title="deliveryStatus">
                                                                            <o:out value="${record.outstanding}" />
                                                                        </v:ajaxLink>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:otherwise>
                                            </c:choose>
                                        </tbody>
                                    </table>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
