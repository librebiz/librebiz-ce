<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.view}"/>

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <tr>
            <th class="actionName"><fmt:message key="deletedActions"/></th>
            <th class="actionDate"><fmt:message key="date"/></th>
            <th class="actionUser"><fmt:message key="employee"/></th>
            <th class="action"><fmt:message key="action"/></th>
        </tr>
        <c:forEach var="fcs" items="${view.wastebasket}">
            <tr>
                <td class="actionName"><o:out value="${fcs.action.name}"/></td>
                <td class="actionDate"><o:date value="${fcs.created}"/></td>
                <td class="actionUser"><oc:employee value="${fcs.employeeId}"/></td>
                <td class="action">
                    <v:link url="${view.baseLink}/restoreAction?id=${fcs.id}" title="restore">
                        <o:img name="enabledIcon" styleClass="bigicon"/>
                    </v:link>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
