/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Jul-2006 
 * 
 */
package com.osserp.core.model.calc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.beans.AbstractOption;

import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.CalculationType;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationConfigImpl extends AbstractOption implements CalculationConfig {

    private CalculationType type = null;
    private List<CalculationConfigGroup> groups = new ArrayList<CalculationConfigGroup>();
    private List<Product> allowedRedundancies = new ArrayList<Product>();

    protected CalculationConfigImpl() {
        super();
    }

    public CalculationConfigImpl(String name, CalculationType calculationType) {
        super(name);
        this.type = calculationType;
    }

    public CalculationType getType() {
        return type;
    }

    protected void setType(CalculationType calculationType) {
        this.type = calculationType;
    }

    public CalculationConfigGroup getGroup(Long id) {
        for (int i = 0, j = groups.size(); i < j; i++) {
            CalculationConfigGroup next = groups.get(i);
            if (next.getId().equals(id)) {
                return next;
            }
        }
        return null;
    }

    public List<CalculationConfigGroup> getGroups() {
        return groups;
    }

    protected void setGroups(List<CalculationConfigGroup> groups) {
        this.groups = groups;
    }

    public CalculationConfigGroup addGroup(Long id, String name, boolean discounts, boolean option) throws ClientException {
        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        for (int i = 0, j = groups.size(); i < j; i++) {
            CalculationConfigGroup next = groups.get(i);
            if (next.getName().equals(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        CalculationConfigGroupImpl obj = new CalculationConfigGroupImpl(
                id,
                getId(),
                name,
                groups.size() + 1,
                discounts,
                option);
        groups.add(obj);
        return obj;
    }

    public CalculationConfigGroup updateGroup(Long groupId, String name, boolean discounts, boolean option) throws ClientException {
        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        for (int i = 0, j = groups.size(); i < j; i++) {
            CalculationConfigGroup next = groups.get(i);
            if (next.getName().equals(name) && !next.getId().equals(groupId)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        for (int i = 0, j = groups.size(); i < j; i++) {
            CalculationConfigGroup next = groups.get(i);
            if (next.getId().equals(groupId)) {
                next.setName(name);
                next.setDiscounts(discounts);
                next.setOption(option);
                return next;
            }
        }
        return null;
    }

    public void addProductSelection(Long groupId, ProductSelectionConfig selectionConfig) {
        for (Iterator<CalculationConfigGroup> i = groups.iterator(); i.hasNext();) {
            CalculationConfigGroup next = i.next();
            if (next.getId().equals(groupId)) {
                next.addProductSelection(selectionConfig);
                break;
            }
        }
    }

    public void removeProductSelection(Long groupId, Long configId) {
        for (Iterator<CalculationConfigGroup> i = groups.iterator(); i.hasNext();) {
            CalculationConfigGroup next = i.next();
            if (next.getId().equals(groupId)) {
                next.removeProductSelection(configId);
                break;
            }
        }
    }

    public void configurePartlist(Long groupId, CalculationConfig partlistConfig) {
        for (Iterator<CalculationConfigGroup> i = groups.iterator(); i.hasNext();) {
            CalculationConfigGroupImpl next = (CalculationConfigGroupImpl) i.next();
            if (next.getId().equals(groupId)) {
                next.updatePartList(partlistConfig);
                break;
            }
        }
    }

    public void removeGroup(Long id) {
        for (Iterator<CalculationConfigGroup> i = groups.iterator(); i.hasNext();) {
            CalculationConfigGroup next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                break;
            }
        }
        for (int i = 0, j = groups.size(); i < j; i++) {
            CalculationConfigGroup next = groups.get(i);
            next.setOrderId(i);
        }
    }

    public List<Product> getAllowedRedundancies() {
        return allowedRedundancies;
    }

    protected void setAllowedRedundancies(List<Product> allowedRedundancies) {
        this.allowedRedundancies = allowedRedundancies;
    }

    @Override
    public Object clone() {
        throw new IllegalStateException("clone currently not supported");
    }
}
