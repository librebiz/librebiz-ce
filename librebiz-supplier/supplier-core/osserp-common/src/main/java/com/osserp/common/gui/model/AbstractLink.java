/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13 Apr 2007 21:36:01 
 * 
 */
package com.osserp.common.gui.model;

import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.gui.MenuLink;
import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractLink extends AbstractMenuItem implements MenuLink {

    private String url = null;

    private boolean directLink = false;
    private String directLinkTarget;

    protected AbstractLink() {
        super();
    }

    protected AbstractLink(int orderId, String url, String name, String permissions) {
        super(name, orderId, permissions);
        this.url = url;
    }

    protected AbstractLink(MenuLink vo) {
        super(vo);
        url = vo.getUrl();
        directLink = vo.isDirectLink();
        directLinkTarget = vo.getDirectLinkTarget();
    }

    /**
     * Mapped constructor, copying all values found including id! You have to reset or reassign the id manually if value only copying intended.
     * @param map
     */
    protected AbstractLink(Map<String, Object> map) {
        super(map);
        url = fetchString(map, "url");
        directLink = fetchBoolean(map, "directLink");
        directLinkTarget = fetchString(map, "directLinkTarget");
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "url", url);
        putIfExists(map, "directLink", directLink);
        putIfExists(map, "directLinkTarget", directLinkTarget);
        return map;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isDirectLink() {
        return directLink;
    }

    public void setDirectLink(boolean directLink) {
        this.directLink = directLink;
    }

    public String getDirectLinkTarget() {
        return directLinkTarget;
    }

    public void setDirectLinkTarget(String directLinkTarget) {
        this.directLinkTarget = directLinkTarget;
    }

    public void prefixUrl(String serverUrl) {
        if (url == null || url.length() < 1) {
            url = serverUrl;
        } else if (url.indexOf("http") < 0) {
            url = serverUrl + url;
        }
    }

    @Override
    public Element getXML() {
        return getXML("link");
    }

    @Override
    public Element getXML(String name) {
        Element root = super.getXML(name);
        root.addContent(JDOMUtil.createElement("url", url));
        root.addContent(JDOMUtil.createElement("directLink", directLink));
        root.addContent(JDOMUtil.createElement("directLinkTarget", directLinkTarget));
        return root;
    }

    @Override
    public abstract Object clone();
}
