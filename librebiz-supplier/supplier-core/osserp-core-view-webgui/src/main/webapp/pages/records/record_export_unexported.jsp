<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.recordExportView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="styles" type="string">
        <style type="text/css">
        <c:import url="/pages/records/record_export.css"/>
        </style>
    </tiles:put>

    <tiles:put name="headline">
        <o:out value="${view.exportType.name}"/> - 
        <c:choose>
            <c:when test="${view.selectedDisplayMode}">
                <fmt:message key="recordExportSelectedLabel"/>
            </c:when>
            <c:otherwise>
                <fmt:message key="recordExportUnexportedLabel"/>
            </c:otherwise>
        </c:choose>
    </tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${view.selectedDisplayMode}">
                    <li><a href="<c:url value="/recordExports.do?method=exitSelected"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                    <li><a href="<c:url value="/recordExports.do?method=listUnexported"/>" title="<fmt:message key="displayUnselected"/>"><o:img name="smileIcon"/></a></li>
                    <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
                    <c:set var="recordExportList" scope="request" value="${view.selected}" />
                    <c:set var="recordExportSelection" scope="request" value="/recordExports.do?method=selectUnexported"/>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/recordExports.do?method=exitUnexported"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                    <li><a href="<c:url value="/recordExports.do?method=listSelected"/>" title="<fmt:message key="displaySelected"/>"><o:img name="nosmileIcon"/></a></li>
                    <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
                    <c:set var="recordExportList" scope="request" value="${view.unexported}" />
                    <c:set var="recordExportSelection" scope="request" value="/recordExports.do?method=selectUnexported"/>
                </c:otherwise>
            </c:choose>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="voucherExportUnexportedContent">
            <o:form name="recordSelection" url="/recordExports.do">
                <input type="hidden" name="method" value="selectUnexported">
                <c:import url="/pages/records/record_export_list.jsp"/>
            </o:form>
        </div>
    </tiles:put>
</tiles:insert>
