/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * 
 */
package com.osserp.gui.customers

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.web.RequestUtil

import com.osserp.groovy.web.AbstractController
import com.osserp.groovy.web.ViewContextException

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/customers/customerLetter/*")
@Controller class CustomerLetterController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(CustomerLetterController.class.getName())

    @RequestMapping
    def forward(HttpServletRequest request) {
        logger.debug("forward: invoked [user=${getUserId(request)}]")
        def customerView = request.session.getAttribute('contactView')
        def exitTarget = RequestUtil.getExit(request)
        if (!customerView?.bean) {
            logger.warn('forward: did not find contactView')
            throw new ViewContextException('contactView not bound')
        }
        logger.debug("forward: found required objects [customer=${customerView.bean.id}, exit=${exitTarget}]")
        CustomerLetterReference reference = new CustomerLetterReference(customerView.bean, exitTarget)
        request.session.setAttribute(CustomerLetterReference.NAME, reference)
        redirect(request, '/letters/letter/forward?exit=/customerDisplay.do')
    }

    @RequestMapping
    def exit(HttpServletRequest request) {
        logger.debug("exit: invoked [user=${getUserId(request)}]")
        CustomerLetterReference reference = request.session.getAttribute(CustomerLetterReference.NAME)
        def exitTarget = reference.exitTarget
        request.session.removeAttribute(CustomerLetterReference.NAME)
        if (!exitTarget) {
            logger.debug("exit: target available [customer=${reference?.businessId}, exitTarget=${reference?.exitTarget}]")
            exitTarget = '/index'
        }
        redirect(request, exitTarget)
    }
}
