<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="clientCompanyView"/>
<c:set var="companyContext" scope="request" value="true"/>
<c:set var="companyExit" scope="request" value="${view.baseLink}/reload"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>

	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
	</tiles:put>

	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string">
        <div class="content-area" id="clientCompanyContent">
            <c:choose>
                <c:when test="${view.editMode}">
                    <v:form id="clientCompanyEditForm" url="/company/clientCompany/save">
                        <c:import url="_clientCompanyEdit.jsp"/>
                    </v:form>
                </c:when>
                <c:otherwise>
                    <c:import url="_clientCompanyDisplay.jsp"/>
                </c:otherwise>
            </c:choose>
        </div>
	</tiles:put>
</tiles:insert>
