<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.businessCaseView}"><c:redirect url="/errors/error_context.jsp"/></c:if>
<c:set var="view" value="${sessionScope.businessCaseView}"/>
<c:set var="plan" value="${sessionScope.businessCaseView.request}"/>
<c:set var="customer" scope="request" value="${plan.customer}"/>
<c:set var="customerExitUrl" scope="request" value="/planDispatcher.do?method=exitDisplay"/>
<c:set var="contactDisplayUrl" scope="request" value="/requests.do?method=displayCustomer"/>
<c:set var="projectContext" value="false"/>
<c:set var="otherSales" value="${view.userForeignSales}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            <c:choose>
            <c:when test="${view.addressEditMode}">
            .valueTable.inputs200 select,
            .valueTable.inputs200 input[type="text"] { width:99%; }
            .valueTable.inputs200 input[type="text"].addressZipcode { width:32%; }
            .valueTable.inputs200 input[type="text"].addressCity { width:66%; }
            </c:when>
            <c:otherwise>
            .statusHeaderMargin {  margin-left: 8px; }
            </c:otherwise>
            </c:choose>
        </style>
    </tiles:put>
    <tiles:put name="javascript" type="string">
        <script type="text/javascript">
            function jump(url) {
                window.location.href = url;
            }
        </script>
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="request"/>
        <c:choose>
            <c:when test="${plan.cancelled}">
                <span class="bolderror"><fmt:message key="cancelled"/>: <o:out value="${plan.type.name}"/></span>
            </c:when>
            <c:when test="${plan.stopped}">
                <span class="bolderror">Stop =&gt;</span> <o:out value="${plan.type.name}"/> <span class="bolderror"> &lt;= Stop</span>
            </c:when>
            <c:otherwise>
                <o:out value="${plan.type.name}"/>
            </c:otherwise>
        </c:choose>
    </tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/planDispatcher.do?method=exitDisplay"/>" ><o:img name="backIcon"/></a></li>
            <c:if test="${!plan.cancelled and !plan.closed}">
                <li><v:ajaxLink url="/events/appointmentCreator/forward?type=4&view=businessCaseView" linkId="appointmentCreator"><o:img name="bellIcon" /></v:ajaxLink></li>
            </c:if>
            <o:permission role="notes_requests_deny" info="permissionNotes">
                <li><v:link url="/notes/note/forward?id=${plan.requestId}&name=salesNote&exit=/requestDisplay.do" title="displayAndWriteNotes"><o:img name="texteditIcon"/></v:link></li>
            </o:permission>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <c:import url="/pages/shared/message.jsp"/>
        <div class="subolumns">
            <div class="column50l">
                <div class="subcolumnl">
                    <div class="spacer"></div>
                    <c:import url="customer_data.jsp"/>
                    <div class="spacer"></div>

                    <div class="contentBox">
                        <div class="contentBoxHeader">
                            <div class="contentBoxHeaderLeft">
                                <div class="subHeaderLeft">
                                </div>
                                <div class="subHeaderRight">
                                    <o:out value="${plan.type.key}"/> <o:out value="${plan.requestId}"/>
                                </div>
                            </div>
                            <div class="contentBoxHeaderRight">
                                <div class="boxnav">
                                    <oc:systemPropertyEnabled name="businessCasePropertySupport">
                                        <o:permission role="customer_service,sales,technics,organisation_admin" info="propertyEdit">
                                            <ul>
                                                <li>
                                                <v:ajaxLink linkId="businessDetails" url="/requests/requestDetails/forward">
                                                    <o:img name="editIcon"/>
                                                </v:ajaxLink>
                                                </li>
                                            </ul>
                                        </o:permission>
                                    </oc:systemPropertyEnabled>
                                </div>
                            </div>
                        </div>
                        <div class="contentBoxData">
                            <div class="subcolumns">
                                <div class="subcolumn">
                                    <div class="spacer"></div>
                                    <table class="valueTable first33 inputs200">
                                        <oc:systemPropertyEnabled name="businessCaseExternalUrlSupport">
                                            <c:if test="${!empty plan.externalUrl}">
                                                <tr>
                                                    <td style="vertical-align: top;"><fmt:message key="businessCaseExternalUrl"/><td>
                                                    <a href="<o:out value="${plan.externalUrl}"/>"><o:out value="${plan.externalUrl}" limit="36"/></a>
                                                </td>
                                                </tr>
                                            </c:if>
                                        </oc:systemPropertyEnabled>
                                        <c:import url="/pages/requests/request_accounting_reference.jsp"/>
                                        <c:import url="/pages/requests/request_name.jsp"/>
                                        <c:import url="/pages/requests/request_address.jsp"/>
                                        <c:import url="/pages/requests/request_branch.jsp"/>
                                        <c:import url="/pages/requests/request_sales_persons.jsp"/>

                                        <c:if test="${!plan.type.interestContext}">
                                            <c:if test="${!((plan.closed or plan.cancelled) and empty plan.managerId)}">
                                                <tr>
                                                    <td>
                                                <c:choose>
                                                    <c:when test="${plan.cancelled or plan.closed}">
                                                        <fmt:message key="responsible"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <o:permission role="manager_change,executive_branch" info="permissionEnableManagerEmployeeSelection">
                                                            <a href="<c:url value="/requests.do?method=enableEmployeeSelection&target=manager"/>"><fmt:message key="responsible"/></a>
                                                        </o:permission>
                                                        <o:forbidden role="manager_change,executive_branch">
                                                            <fmt:message key="responsible"/>
                                                        </o:forbidden>
                                                    </c:otherwise>
                                                </c:choose>
                                                </td>
                                                <td>
                                                <c:choose>
                                                    <c:when test="${!empty plan.managerId}">
                                                        <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${plan.managerId}">
                                                            <oc:employee value="${plan.managerId}"/>
                                                        </v:ajaxLink>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:message key="notAssigned"/>
                                                    </c:otherwise>
                                                </c:choose>
                                                </td>
                                                </tr>
                                            </c:if>
                                        </c:if>

                                        <c:if test="${!((plan.closed or plan.cancelled) and empty plan.agent)}">
                                            <tr>
                                                <td>
                                            <c:choose>
                                                <c:when test="${plan.cancelled or plan.closed}">
                                                    <fmt:message key="agent"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <o:permission role="executive,executive_branch,executive_sales,sales_agent_config" info="permissionEnableAgentConfig">
                                                        <c:set var="title"><fmt:message key="agentConfigEditorTitle"/></c:set>
                                                        <o:ajaxLink linkId="agentConfig" url="/requests.do?method=enableAgentConfig"><fmt:message key="agent"/></o:ajaxLink>
                                                    </o:permission>
                                                    <o:forbidden role="executive,executive_branch,executive_sales,sales_agent_config">
                                                        <fmt:message key="agent"/>
                                                    </o:forbidden>
                                                </c:otherwise>
                                            </c:choose>
                                            </td>
                                            <td>
                                            <c:choose>
                                                <c:when test="${!empty plan.agent}">
                                                    <o:ajaxLink linkId="agentDisplay" url="/requests.do?method=enableAgentDisplay">
                                                        <o:out value="${plan.agent.displayName}"/>
                                                    </o:ajaxLink>
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:message key="undefined"/>
                                                </c:otherwise>
                                            </c:choose>
                                            </td>
                                            </tr>
                                        </c:if>

                                        <c:if test="${!((plan.closed or plan.cancelled) and empty plan.tip)}">
                                            <tr>
                                                <td>
                                            <c:choose>
                                                <c:when test="${plan.cancelled or plan.closed}">
                                                    <fmt:message key="tipProvider"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <o:permission role="executive,executive_branch,executive_sales,sales_agent_config" info="permissionEnableTipConfig">
                                                        <c:set var="title"><fmt:message key="tipConfigEditorTitle"/></c:set>
                                                        <o:ajaxLink linkId="tipConfig" url="/requests.do?method=enableTipConfig"><fmt:message key="tipProvider"/></o:ajaxLink>
                                                    </o:permission>
                                                    <o:forbidden role="executive,executive_branch,executive_sales,sales_agent_config">
                                                        <fmt:message key="tipProvider"/>
                                                    </o:forbidden>
                                                </c:otherwise>
                                            </c:choose>
                                            </td>
                                            <td>
                                            <c:choose>
                                                <c:when test="${!empty plan.tip}">
                                                    <o:ajaxLink linkId="tipDisplay" url="/requests.do?method=enableTipDisplay">
                                                        <o:out value="${plan.tip.displayName}"/>
                                                    </o:ajaxLink>
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:message key="undefined"/>
                                                </c:otherwise>
                                            </c:choose>
                                            </td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${!empty view.stickyNote}">
                                            <tr>
                                                <td><fmt:message key="info" /></td>
                                            <td><o:out value="${view.stickyNote.note}" /></td>
                                            </tr>
                                        </c:if>
                                        <c:forEach var="prop" items="${plan.propertyList}" varStatus="s">
                                            <tr>
                                                <td><fmt:message key="${prop.name}"/></td>
                                            <td><o:out value="${prop.value}"/></td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                    <div class="spacer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="spacer"></div>
                </div>
            </div>
            <div class="column50r">
                <div class="subcolumnr">
                    <div class="spacer"></div>
                    <c:import url="customer_contact_data.jsp"/>
                    <div class="spacer"></div>

                    <div class="contentBox">
                        <div class="contentBoxHeader">
                            <div class="contentBoxHeaderLeft">
                                <div class="subHeaderLeft">
                                </div>
                                <div class="subHeaderRight">
                                    <c:choose>
                                        <c:when test="${otherSales or plan.closed}">
                                            <span class="statusHeaderMargin"><fmt:message key="status"/></span>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="/requests/requestFcs/forward?exit=/planDisplayForward.do">
                                                <span class="statusHeaderMargin"><fmt:message key="status"/></span>
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose>
                                    <o:out value="${plan.status}"/> <fmt:message key="percent"/>
                                    <c:if test="${!empty view.events and !plan.closed and !plan.cancelled}">
                                        <span class="statusHeaderMargin">-</span>
                                        <span class="statusHeaderMargin">
                                            <v:link url="/events/salesEvents/forward?exit=/requestDisplay.do" title="openTodos">
                                                <fmt:message key="jobs"/>
                                            </v:link>
                                        </span>
                                    </c:if>
                                    <c:if test="${!plan.closed}">
                                        <oc:pluginHookAvailable hook="crmEvents">
                                            <oc:pluginHook hook="crmEvents" view="businessCaseView"  exit="/planDisplayForward.do" />
                                        </oc:pluginHookAvailable>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        <div class="contentBoxData">
                            <div class="subcolumns">
                                <div class="subcolumn">
                                    <div class="spacer"></div>
                                    <table class="valueTable first33">
                                        <tr>
                                            <td><fmt:message key="requestFrom"/></td>
                                        <td><o:date value="${plan.salesTalkDate}"/></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="enteredAt"/></td>
                                        <td>
                                        <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${plan.createdBy}">
                                            <o:date value="${plan.created}" addtime="true"/>
                                        </v:ajaxLink>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="offerUntil"/></td>
                                        <td><o:date value="${plan.presentationDate}"/></td>
                                        </tr>
                                        <tr>
                                            <td>
                                        <v:ajaxLink url="/requests/requestOriginSelector/forward?exit=/requestDisplay.do" linkId="requestOriginSelector">
                                            <fmt:message key="requestPer"/>
                                        </v:ajaxLink>
                                        </td>
                                        <td>
                                        <c:choose>
                                            <c:when test="${!empty plan.originType}">
                                                <oc:options name="requestOriginTypes" value="${plan.originType}"/>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:message key="unknown"/>
                                            </c:otherwise>
                                        </c:choose>
                                        </td>
                                        </tr>
                                        <c:if test="${!empty plan.origin}">
                                            <tr>
                                                <td>
                                            <o:permission role="executive,executive_branch,sales_change" info="permissionChangeRequestOrigin">
                                                <v:ajaxLink linkId="campaignSelectionView" url="/crm/campaignSelection/forward?view=businessCaseView&company=${plan.type.company}&id=${plan.origin}">
                                                    <fmt:message key="origin"/>
                                                </v:ajaxLink>
                                            </o:permission>
                                            <o:forbidden role="executive,executive_branch,sales_change">
                                                <fmt:message key="origin"/>
                                            </o:forbidden>
                                            </td>
                                            <td>
                                            <c:choose>
                                                <c:when test="${view.originHistoryAvailable}">
                                                    <v:ajaxLink linkId="originHistoryView" url="/requests/requestOriginHistory/forward" title="changes">
                                                        <oc:options name="campaigns" value="${plan.origin}"/>
                                                    </v:ajaxLink>
                                                </c:when>
                                                <c:otherwise>
                                                    <oc:options name="campaigns" value="${plan.origin}"/>
                                                </c:otherwise>
                                            </c:choose>
                                            </td>
                                            </tr>
                                        </c:if>

                                        <c:if test="${!plan.cancelled}">
                                            <c:choose>
                                                <c:when test="${plan.type.directSales}">
                                                    <tr>
                                                        <td><fmt:message key="anticipation"/></td>
                                                    <td><fmt:message key="directSales"/></td>
                                                    </tr>
                                                </c:when>
                                                <c:when test="${!otherSales and !plan.type.interestContext and !plan.closed}">
                                                    <tr>
                                                        <td><fmt:message key="anticipation"/></td>
                                                    <td>
                                                    <o:ajaxLink url="/orderProbability.do?method=forward" linkId="order_probability">
                                                        <fmt:message key="orderReceipt"/> =&gt; <o:out value="${plan.orderProbability}"/> %
                                                    </o:ajaxLink>
                                                    </td>
                                                    </tr>
                                                </c:when>
                                                <c:otherwise>
                                                    <tr>
                                                        <td><fmt:message key="anticipation"/></td>
                                                    <td><fmt:message key="orderReceipt"/> =&gt; <o:out value="${plan.orderProbability}"/>%</td>
                                                    </tr>
                                                </c:otherwise>
                                            </c:choose>
                                            <tr>
                                                <td><fmt:message key="incomingToTheDate"/></td>
                                            <td>
                                            <c:choose>
                                                <c:when test="${!empty plan.orderProbabilityDate}">
                                                    <o:date value="${plan.orderProbabilityDate}"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:message key="undefined"/>
                                                </c:otherwise>
                                            </c:choose>
                                            </td>
                                            </tr>
                                        </c:if>
                                        <c:set scope="page" var="documentLabelSet" value="false"/>
                                        <c:if test="${!otherSales or view.branchExecutive}">
                                            <c:if test="${!plan.type.interestContext}">

                                                <c:if test="${plan.type.supportingOffers and !plan.type.directSales}">
                                                    <c:set scope="page" var="documentLabelSet" value="true"/>
                                                    <tr>
                                                        <td><fmt:message key="documents"/></td>
                                                    <td>
                                                        <a href="<c:url value="/salesOffer.do?method=forward"/>">
                                                           <fmt:message key="offers"/> [<o:out value="${view.recordCount}"/>]
                                                        </a>
                                                    </td>
                                                    </tr>
                                                </c:if>
                                                <c:if test="${!plan.cancelled and !plan.closed and plan.type.supportingOffers and plan.type.recordByCalculation and !plan.type.directSales}">
                                                    <tr>
                                                        <td> </td>
                                                        <td><a href="<c:url value="/calculations.do?method=forward&exit=plan"/>"><fmt:message key="calculation"/></a></td>
                                                    </tr>
                                                </c:if>
                                                <c:if test="${plan.closed and view.requestDocumentCount > 0}">
                                                    <tr>
                                                        <td> </td>
                                                        <td><v:link url="/requests/requestDocument/forward?exit=/requestDisplay.do"><fmt:message key="documents"/> [<o:out value="${view.requestDocumentCount}"/>]</v:link></td>
                                                    </tr>
                                                </c:if>
                                            </c:if>
                                            <c:if test="${plan.type.supportingPictures and (!plan.closed or view.requestPictureCount > 0)}">
                                                <tr>
                                                    <td> </td>
                                                    <td><v:link url="/requests/requestPicture/forward?exit=/requestDisplay.do"><fmt:message key="pictures"/> [<o:out value="${view.requestPictureCount}"/>]</v:link></td>
                                                </tr>
                                            </c:if>
                                            <c:if test="${!plan.closed}">
                                                <tr>
                                                    <td> </td>
                                                    <td><v:link url="/requests/requestDocument/forward?exit=/requestDisplay.do"><fmt:message key="documents"/> [<o:out value="${view.requestDocumentCount}"/>]</v:link></td>
                                                </tr>
                                                <tr>
                                                    <td> </td>
                                                    <td><v:link url="/requests/requestLetter/forward?exit=/requestDisplay.do"><fmt:message key="letters"/> [<o:out value="${view.letterCount}"/>]</v:link></td>
                                                </tr>
                                                <c:if test="${view.fetchmailInboxCount > 0}">
                                                    <tr>
                                                        <td> </td>
                                                        <td>
                                                            <v:link url="/mail/businessCaseInbox/forward?exit=/requestDisplay.do">
                                                                <fmt:message key="inboxLabel" /> [<o:out value="${view.fetchmailInboxCount}" />]
                                                            </v:link>
                                                        </td>
                                                    </tr>
                                                </c:if>
                                            </c:if>
                                        </c:if>
                                    </table>
                                    <div class="spacer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="spacer"></div>
                </div>
            </div>
        </div>

        <c:if test="${!plan.closed and (view.branchExecutive or !otherSales) and !empty view.appointments}">
            <div class="subcolumns">
                <div class="subcolumn">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><fmt:message key="appointment"/></th>
                        <th><fmt:message key="description"/></th>
                        <th><fmt:message key="appointmentOf"/></th>
                        <th class="action"><fmt:message key="action"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="app" items="${view.appointments}" varStatus="s">
                            <tr class="altrow">
                                <td style="width: 100px;">
                                    <span><o:date value="${app.appointmentDate}" addcentury="false" /></span>
                                    <span><o:time value="${app.appointmentDate}"/></span>
                                </td>
                                <td><o:out value="${app.action.name}"/></td>
                            <td>
                            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${app.recipientId}">
                                <oc:employee value="${app.recipientId}"/>
                            </v:ajaxLink>
                            </td>
                            <td class="center">
                            <v:ajaxLink url="/events/eventEditor/forward?id=${app.id}&view=businessCaseView" linkId="eventEditor"><o:img name="texteditIcon" /></v:ajaxLink>
                            <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmCloseAppointment"/>','<c:url value="/requests.do"><c:param name="method" value="closeAppointmentNote"/><c:param name="exit" value="display"/><c:param name="id" value="${app.id}"/></c:url>');" title="<fmt:message key="removeAppointment"/>"><o:img name="deleteIcon"/></a>
                            </td>
                            </tr>
                            <c:if test="${!empty app.message or !empty app.notes}">
                                <tr>
                                    <td></td>
                                    <td colspan="3">
                                <c:forEach var="note" items="${app.notes}">
                                    <span class="boldtext"><o:date value="${note.created}" addtime="true"/> <oc:employee value="${note.createdBy}"/>:</span><br/>
                                    <o:out value="${note.note}"/><c:if test="${!s.last or !empty app.message}"><br/></c:if>
                                </c:forEach>
                                <c:if test="${!empty app.message}">
                                    <c:if test="${!empty app.notes}">---<br/></c:if>
                                    <c:if test="${app.createdBy != app.recipientId}"><span class="boldtext"><oc:employee value="${app.createdBy}"/>: </span></c:if><o:out value="${app.message}"/>
                                </c:if>
                                </td>
                                </tr>
                            </c:if>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="spacer"></div>
        </c:if>
    </tiles:put>
</tiles:insert>
