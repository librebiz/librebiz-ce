<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:choose>
<c:when test="${!empty sessionScope.errors}">
<span class="errortext"><fmt:message key="volumeInvoiceCreateFailed"/>: <fmt:message key="${sessionScope.errors}"/></span>
<o:removeErrors/>
</c:when>
<c:otherwise>
<span class="boldtext"><fmt:message key="volumeInvoiceCreated"/></span>
<span style="margin-left: 10px;"><a href="<c:url value="/salesVolumeInvoice.do?method=redisplay"/>">[<fmt:message key="reloadModule"/>]</a></span>
</c:otherwise>
</c:choose>
