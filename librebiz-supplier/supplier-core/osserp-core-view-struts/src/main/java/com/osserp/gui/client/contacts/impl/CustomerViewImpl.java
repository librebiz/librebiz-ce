/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 03-Jun-2006 09:16:28 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseSearch;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PaymentAware;
import com.osserp.core.contacts.PersonManager;
import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerManager;
import com.osserp.core.customers.CustomerSummary;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;

import com.osserp.gui.client.contacts.CustomerView;
import com.osserp.gui.client.contacts.PersonView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("contactView")
public class CustomerViewImpl extends AbstractPaymentAwareContactView implements CustomerView {
    private static Logger log = LoggerFactory.getLogger(CustomerViewImpl.class.getName());

    private CustomerSummary summary = null;
    private Sales sales = null;
    private Request request = null;
    private boolean salesListingMode = false;
    private boolean requestListingMode = false;
    private boolean cancelledListingMode = false;
    private boolean displayAllBusinessCases = false;
    private boolean discountEditMode = false;

    protected CustomerViewImpl() {
        super();
    }

    @Override
    public void load(PersonView view) throws ClientException {
        super.load(view);
        refreshSummary();
    }
    
    @Override
    public void load(Long id) throws ClientException {
        super.load(id);
        refreshSummary();
    }

    public void load(BusinessCase businessCase) {
        setBean(businessCase.getCustomer());
        if (businessCase instanceof Sales) {
            sales = (Sales) businessCase;
        } else {
            request = (Request) businessCase;
        }
        refreshSummary();
    }

    @Override
    public boolean isRelatedContactView() {
        return true;
    }

    @Override
    public boolean isCustomerView() {
        return true;
    }

    public CustomerSummary getSummary() {
        return summary;
    }

    public boolean isRequestMode() {
        return request != null;
    }

    public boolean isSaleMode() {
        return sales != null;
    }

    public boolean isRequestListingMode() {
        return requestListingMode;
    }
    
    public void enableRequestListingMode(boolean cancelled) {
        disableAllModes();
        requestListingMode = true;
        cancelledListingMode = cancelled;
    }

    public boolean isSalesListingMode() {
        return salesListingMode;
    }

    public void enableSalesListingMode(boolean cancelled, boolean closed) {
        disableAllModes();
        this.salesListingMode = true;
        this.displayAllBusinessCases = closed;
        this.cancelledListingMode = cancelled;
    }

    public boolean isIncludeAllBusinessCases() {
        return displayAllBusinessCases;
    }

    protected void setDisplayAllBusinessCases(boolean displayAllBusinessCases) {
        this.displayAllBusinessCases = displayAllBusinessCases;
    }

    public void toggleIncludeAllBusinessCasesMode() {
        this.displayAllBusinessCases = !this.displayAllBusinessCases;
    }

    public boolean isCancelledListingMode() {
        return cancelledListingMode;
    }

    @Override
    public void updateDetails(Form form) throws ClientException, PermissionException {
        disableAllModes();
        throw new IllegalStateException();
    }

    public boolean isDiscountEditMode() {
        return discountEditMode;
    }

    public void setDiscountEditMode(boolean discountEditMode) {
        disableAllModes();
        this.discountEditMode = discountEditMode;
    }

    public void updateDiscount(Form fh) throws ClientException, PermissionException {
        Customer customer = getCustomer();
        customer.setDiscountEnabled(fh.getBoolean("discountEnabled"));
        customer.setDiscountA(fh.getPercentage("discountA"));
        customer.setDiscountB(fh.getPercentage("discountB"));
        customer.setDiscountC(fh.getPercentage("discountC"));
        getCustomerManager().save(customer);
        refresh();
        disableAllModes();
    }

    public void refreshSummary() {
        if (log.isDebugEnabled()) {
            log.debug("refreshSummay() invoked");
        }
        BusinessCaseSearch search = (BusinessCaseSearch) getService(BusinessCaseSearch.class.getName());
        summary = search.getCustomerSummary(getCustomer().getId());
        if (log.isDebugEnabled()) {
            log.debug("refreshSummay() done");
        }
    }

    public Customer getCustomer() {
        Contact contact = getCurrent();
        if (contact instanceof Customer) {
            return (Customer) contact;
        }
        throw new ActionException("customer expected, found "
                + ((contact == null) ? "null" : contact.getClass().getName()));
    }

    // ***   protected setters   *** // 

    protected void setRequestListingMode(boolean requestListingMode) {
        this.requestListingMode = requestListingMode;
    }

    protected void setSalesListingMode(boolean salesListingMode) {
        this.salesListingMode = salesListingMode;
    }

    protected void setCancelledListingMode(boolean cancelledListingMode) {
        this.cancelledListingMode = cancelledListingMode;
    }

    // ***   Inherited and utility methods   *** // 

    @Override
    public void disableAllModes() {
        this.cancelledListingMode = false;
        this.requestListingMode = false;
        this.salesListingMode = false;
        this.discountEditMode = false;
        super.disableAllModes();
    }

    @Override
    protected PaymentAware fetchPaymentAware() {
        return getCustomer();
    }

    @Override
    protected void persist(PaymentAware contact) {
        getCustomerManager().save((Customer) contact);
    }

    @Override
    protected PersonManager getPersonManager() {
        return getCustomerManager();
    }

    @Override
    protected String getDefaultDisplayGroup() {
        return Contact.CUSTOMER_KEY;
    }

    protected CustomerManager getCustomerManager() {
        return (CustomerManager) getService(CustomerManager.class.getName());
    }
}
