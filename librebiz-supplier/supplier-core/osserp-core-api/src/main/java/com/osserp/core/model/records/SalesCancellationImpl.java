/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 19:42:02 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.Option;
import com.osserp.common.util.DateUtil;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesCancellationImpl extends AbstractCancellingRecord {

    /**
     * Default constructor required by Serializable
     */
    protected SalesCancellationImpl() {
        super();
    }

    /**
     * Creates a cancellation
     * @param id
     * @param type
     * @param user
     * @param cancellable
     * @param date
     */
    public SalesCancellationImpl(
            Long id,
            RecordType type,
            Employee user,
            CancellableRecord cancellable,
            Date date) {
        super(id, type, user, cancellable, true, true);
        updateDateCreatedIfRequired(cancellable, date);
    }

    private void updateDateCreatedIfRequired(Record record, Date date) {
        if (record != null) {
            if (record.getCreated().after(getCreated())) {
                setCreated(DateUtil.addMinutes(record.getCreated(), 1));
                setInitialDate(getCreated());
            }
            Date now = new Date(System.currentTimeMillis());
            if (date != null && !date.after(now)) {
                if (date.after(record.getCreated())) {
                    setCreated(date);
                } else if (DateUtil.isSameDay(date, record.getCreated())) {
                    setCreated(DateUtil.addMinutes(record.getCreated(), 1));
                }
            }
        }
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new SalesCancellationItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        return new SalesCancellationInfoImpl(this, info, user);
    }

    @Override
    protected Payment createCustomPayment(Employee user, BillingType type, BigDecimal amount, Date paid, String customHeader, String note, Long bankAccountId) {
        throw new IllegalStateException();
    }

    @Override
    protected Payment createPayment(Employee user, BillingType type, BigDecimal amount, Date paid, Long bankAccountId) {
        return new SalesPaymentImpl(
                this,
                type,
                paid,
                user,
                amount.multiply(new BigDecimal(-1)),
                bankAccountId);
    }

    @Override
    public boolean isSales() {
        return true;
    }
}
