<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${view.createMode}">
                        <fmt:message key="createQuery" />
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="editQuery" />
                    </c:otherwise>
                </c:choose>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="name"><fmt:message key="queryName" /></label>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <v:text name="name" value="${view.bean.name}" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="resultName"><fmt:message key="resultName" /></label>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <v:text name="title" value="${view.bean.outputTitle}" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="query"><fmt:message key="query" /></label>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <v:textarea name="query" value="${view.bean.query}" styleClass="form-control query-textarea" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${view.createMode}">
                                        <v:submitExit url="${view.baseLink}/disableCreateMode" value="exit" />
                                    </c:when>
                                    <c:otherwise>
                                        <v:submitExit url="${view.baseLink}/disableEditMode" value="exit" />
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
