<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="${sessionScope.contactInputViewName}" />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="companyName"><fmt:message key="companyNameLabel" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="lastName" styleClass="form-control" title="companyNameLabelHint" value="${view.bean.lastName}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="companyAffix"><fmt:message key="companyNameAddonLabel" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="firstName" styleClass="form-control" title="companyNameAddonLabelHint" value="${view.bean.firstName}" />
        </div>
    </div>
</div>

<c:import url="_contactAddressInput.jsp" />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="website" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="website" styleClass="form-control" value="${view.bean.website}" />
        </div>
    </div>
</div>
