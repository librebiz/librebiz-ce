/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 19, 2008 7:02:54 PM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.contacts.PrivateContactManager;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeManager;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.DomainUserManager;

import com.osserp.gui.client.contacts.ContactPersonView;
import com.osserp.gui.client.contacts.ContactView;
import com.osserp.gui.client.contacts.PrivateContactsView;
import com.osserp.gui.client.impl.AbstractView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("privateContactsView")
public class PrivateContactsViewImpl extends AbstractView implements PrivateContactsView {
    private static Logger log = LoggerFactory.getLogger(PrivateContactsViewImpl.class.getName());

    private List<PrivateContact> unused = new ArrayList<>();
    private List<Option> employees = new ArrayList<>();
    private Employee currentEmployee = null;

    protected PrivateContactsViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        employees = getEmployeeManager().findAllActiveEmployees();
        refresh();
    }

    public void selectByContact(Long contactId) {
        List<PrivateContact> list = getList();
        for (Iterator<PrivateContact> i = list.iterator(); i.hasNext();) {
            PrivateContact next = i.next();
            if (next.getContact().getContactId().equals(contactId)) {
                setBean(next);
                if (log.isDebugEnabled()) {
                    log.debug("selectByContact() done [id=" + next.getId()
                            + ", contactId=" + next.getContact().getContactId()
                            + "]");
                }
                break;
            }
        }
    }

    public void addContact(HttpServletRequest request) {
        DomainUserManager userManager = getDomainUserManager();
        DomainUser user = (DomainUser) userManager.find(getCurrentEmployee());
        ContactView view = null;
        ContactView contactView = (ContactView) fetchView(request, ContactView.class);
        ContactPersonView contactPersonView = (ContactPersonView) fetchView(request, ContactPersonView.class);
        if (contactPersonView != null) {
            view = contactPersonView;
        } else if (contactView != null) {
            view = contactView;
        }
        if (view != null && view.getBean() != null) {
            PrivateContactManager manager = getPrivateContactManager();
            Contact selected = (Contact) view.getBean();
            manager.createPrivate(user, selected, null);
            if (manager.isPrivate(user, selected)) {
                if (log.isDebugEnabled()) {
                    log.debug("addContact() done [employee="
                            + user.getId()
                            + ", contact="
                            + selected.getContactId()
                            + "]");
                }
            }
            if (manager.isPrivate(getDomainUser(), selected) && !manager.isPrivateUnused(getDomainUser(), selected)) {
                view.setPrivateContactOfCurrentEmployee(true);
            }
        } else {
            log.warn("addContact() invoked. No ContactView bound. Cant create private contact without contact.");
        }
        disableCreateMode();
    }

    public void removeContact(Long id) {
        List<PrivateContact> list = getList();
        for (Iterator<PrivateContact> i = list.iterator(); i.hasNext();) {
            PrivateContact next = i.next();
            if (next.getId().equals(id)) {
                getPrivateContactManager().disable(next);
                i.remove();
                this.unused.add(next);
                break;
            }
        }
    }

    public void refresh() {
        DomainUser current = getDomainUser();
        setList(getPrivateContactManager().findPrivate(current));
        this.unused = new ArrayList<PrivateContact>();
        for (Iterator<PrivateContact> i = getList().iterator(); i.hasNext();) {
            PrivateContact next = i.next();
            if (next.isUnused()) {
                this.unused.add(next);
                i.remove();
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("refresh() done [usedCount=" + getList().size()
                    + ", unusedCount=" + unused.size() + "]");
        }
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        PrivateContact selected = (PrivateContact) getBean();
        if (selected == null) {
            log.warn("save() invoked when no contact selected!");
        } else {
            setBean(getPrivateContactManager().update(
                    selected,
                    form.getString(Form.TYPE),
                    form.getString(Form.INFO)));
        }
        disableEditMode();
    }

    public List<PrivateContact> getUnused() {
        return unused;
    }

    public List<Option> getEmployees() {
        return employees;
    }

    public Employee getCurrentEmployee() {
        return currentEmployee;
    }

    public void setCurrentEmployee() {
        setCurrentEmployee(getDomainUser().getEmployee().getId());
    }

    public void setCurrentEmployee(Long employeeId) {
        EmployeeManager employeeManager = getEmployeeManager();
        Employee employee = (Employee) employeeManager.find(employeeId);
        this.currentEmployee = (employee == null) ? getDomainUser().getEmployee() : employee;
    }

    private PrivateContactManager getPrivateContactManager() {
        return (PrivateContactManager) getService(PrivateContactManager.class.getName());
    }

}
