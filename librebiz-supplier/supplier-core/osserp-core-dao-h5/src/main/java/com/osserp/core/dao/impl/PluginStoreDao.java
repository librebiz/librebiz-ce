/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2020 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateAndSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.Plugin;
import com.osserp.core.PluginMapping;
import com.osserp.core.dao.PluginStore;
import com.osserp.core.model.PluginImpl;
import com.osserp.core.model.PluginMappingImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PluginStoreDao extends AbstractHibernateAndSpringDao implements PluginStore {
    private static Logger log = LoggerFactory.getLogger(PluginStoreDao.class.getName());

    public PluginStoreDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    @Override
    public List<Plugin> findAll() {
        List<Plugin> existing = getCurrentSession().createQuery(
                "from " + PluginImpl.class.getName() + " plugin order by plugin.name")
                .list();
        return existing;
    }

    @Override
    public Plugin findById(Long id) {
        if (id == null) {
            throw new IllegalStateException("required param 'id' null");
        }
        Plugin existing = (Plugin) getCurrentSession().get(PluginImpl.class, id);
        return existing;
    }

    @Override
    public Plugin findByName(String name) {
        if (name == null) {
            throw new IllegalStateException("required param 'name' null");
        }
        List<Plugin> existing = getCurrentSession().createQuery(
                "from " + PluginImpl.class.getName() + " plugin where plugin.name=:name")
                .setParameter("name", name)
                .list();
        if (existing.size() > 0) {
            return existing.get(0);
        }
        return null;
    }

    @Override
    public Plugin update(Plugin plugin) {
        try {
            getCurrentSession().merge(plugin);
        } catch (Exception e) {
            log.error("update plugin caught exception: " + e.getMessage(), e);
            throw e;
        }
        return plugin;
    }

    @Override
    public PluginMapping getMapping(Long pluginId, String name, Long entityId) {
        List<PluginMapping> existing = getCurrentSession().createQuery(
                "from " + PluginMappingImpl.class.getName() +
                " mapping where mapping.reference=:referenceId" +
                " and mapping.name=:name and mapping.mappedId=:entityId")
                .setParameter("referenceId", pluginId)
                .setParameter("name", name)
                .setParameter("entityId", entityId)
                .list();
        if (existing.size() > 0) {
            return existing.get(0);
        }
        return null;
    }

    @Override
    public PluginMapping createMapping(
            Long pluginId,
            String name,
            Long mappedId,
            String objectId,
            String objectName,
            String objectType,
            String objectStatus) {

        PluginMapping mapping = getMapping(pluginId, name, mappedId);
        if (mapping == null) {
            mapping = new PluginMappingImpl(
                    pluginId,
                    name,
                    mappedId,
                    objectId,
                    objectName,
                    objectType,
                    objectStatus);
            try {
                getCurrentSession().save(PluginMappingImpl.class.getName(), mapping);
                mapping = (PluginMapping) getCurrentSession().load(
                        PluginMappingImpl.class.getName(), mapping.getId());
            } catch (Exception e) {
                log.error("Create mapping caught exception: " + e.getMessage(), e);
                throw e;
            }
        }
        return mapping;
    }

    @Override
    public void deleteMapping(PluginMapping mapping) {
        getCurrentSession().delete(PluginMapping.class.getName(), mapping);
    }

    @Override
    public PluginMapping updateMapping(PluginMapping mapping) {
        getCurrentSession().merge(PluginMappingImpl.class.getName(), mapping);
        return mapping;
    }

    @Override
    public PluginMapping updateMapping(Long pluginId, String name, Long mappedId, String status) {
        PluginMapping mapping = getMapping(pluginId, name, mappedId);
        if (mapping != null) {
            mapping.setObjectStatus(status);
            mapping.setChanged(new Date(System.currentTimeMillis()));
            getCurrentSession().merge(PluginMappingImpl.class.getName(), mapping);
            mapping = (PluginMapping) getCurrentSession().load(
                    PluginMappingImpl.class.getName(), mapping.getId());
        }
        return mapping;
    }
}
