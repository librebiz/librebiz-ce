<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="customerQueryIndexView" />

<div class="col-md-12">
    <div class="col-md-6 panel-area panel-area-default">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="querySelection" />
                </h4>
            </div>
        </div>
        <div class="panel-body">

            <div class="table-responsive table-responsive-default">
                <table class="table">
                    <tbody>
                        <c:forEach var="item" items="${view.availableQueries}">
                            <tr>
                                <td><v:link url="/customers/customerQueryIndex/selectQuery?type=${item}">
                                        <fmt:message key="customer${item}" />
                                        <c:if test="${item == 'listLastSalesBefore' && !empty view.dateBefore}">
                                            <o:date value="${view.dateBefore}" />
                                        </c:if>
                                    </v:link></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
