/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 5, 2017 
 * 
 */
package com.osserp.core.sql;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.osserp.common.service.UnixShell;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class DatabaseScriptTask extends NativeDatabaseService implements DatabaseTask {
    private static Logger log = LoggerFactory.getLogger(DatabaseSetupTask.class.getName());

    public static final String DATABASE_SCRIPT_DEFAULT_PROPERTY = "dbmsSchema";

    private static final String WORKING_DIR = "/tmp/";
    private static final String TMP_FILENAME = "dbinit.sql";

    private ResourcePatternResolver pathResolver;
    private boolean useConfiguredUserOnly;

    protected DatabaseScriptTask(
            ResourcePatternResolver pathResolver,
            String dbDriver,
            String datasourceString,
            String dbUser,
            String dbPwd,
            boolean useConfiguredUserOnly) {
        super(dbDriver, datasourceString, dbUser, dbPwd);
        this.pathResolver = pathResolver;
        this.useConfiguredUserOnly = useConfiguredUserOnly;
        if (dbDriver != null && isDatabaseSettingsAvailable()) {
            try {
                Class.forName(dbDriver);
            } catch (Exception e) {
                throw new DatabaseException("Class.forName(" + dbDriver + ") failed", e);
            }
        }
    }

    /**
     * The pattern for the path containing the database script(s).
     * The pattern is used as an argument for ResourcePatternResolver.
     * See {@link org.springframework.core.io.support.ResourcePatternResolver}
     * for details.
     * @param version
     * @return script path pattern, e.g. 'classpath:/directory/*.sql'
     */
    protected abstract String getDatabaseScriptPathPattern(String version);

    /**
     * The name of the systemProperty used to retrieve and increase the latest script id.
     * @return script property name
     */
    protected abstract String getDatabaseScriptPropertyName();

    /**
     * Provides the name of the import file excluding path.
     * @return {@value #TMP_FILENAME} by default
     */
    protected String getImportFilename() {
        return TMP_FILENAME;
    }

    /**
     * Provides the working dir including trailing slash.
     * @return {@value #WORKING_DIR} by default
     */
    protected String getWorkingDir() {
        return WORKING_DIR;
    }

    /**
     * Provides the temporary import file including path. Override
     * importFilename and workingDir getters to change defaults.
     * @return name of the importfile
     */
    protected final String getImportFile() {
        return (getWorkingDir() != null ? getWorkingDir() : WORKING_DIR)
                + (getImportFilename() != null ? getImportFilename()
                        : TMP_FILENAME);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.osserp.core.sql.DatabaseTask#isDatabaseAvailable()
     */
    public boolean isDatabaseAvailable() {
        try {
            return (DriverManager.getConnection(getDatabaseUrl(),
                    getDbUser(), getDbPwd()) != null);
        } catch (Throwable e) {
            log.error("Connection failed: " + e.getMessage(), e);
            return false;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.osserp.core.sql.DatabaseTask#isDatabaseInitialized()
     */
    public boolean isDatabaseInitialized() {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            st = con.createStatement();
            String sql = "SELECT * FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog'"
                    + "AND schemaname != 'information_schema'";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            log.error("Connection error: " + e.getMessage(), e);
        } finally {
            close(rs, st, con);
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.osserp.core.sql.DatabaseTask#runTask(String, boolean)
     */
    public boolean runTask(String version, boolean verbose) {
        if (version != null) log.info("runTask: Version: " + version);
        if (isDatabaseAvailable()) {

            String filename = getImportFile();
            FileWriter writer = null;
            try {
                List<DatabaseScript> files = fetchResources(version);
                if (files != null && files.size() > 0) {
                    if (!isPsqlAvailable(verbose)) {
                        log.warn("Database reachable and accessible but update "
                                + "script(s) exists and 'psql' not found.");
                        return false;
                    }
                    Long lastId = 0L;
                    int added = 0;
                    File tempFile = createImportFile(filename);
                    writer = new FileWriter(tempFile);
                    for (int i = 0; i < files.size(); i++) {
                        DatabaseScript script = files.get(i);
                        if (writeScript(writer, script.getResource())) {
                            lastId = script.getId();
                            added++;
                            log.debug("Wrote file id " + lastId + ", name " +
                                    script.getResource().getFilename());
                        }
                    }
                    close(writer);
                    if (added > 0) {
                        int exitval = executeScript(filename, verbose);
                        if (exitval == 0 && lastId > 0) {
                            updateScriptId(version != null ? version : lastId.toString());
                            removeImportFile(tempFile);
                            log.info("Database update done, last script id " +
                                    (version != null ? version : lastId));
                        } else {
                            return false;
                        }
                    } else {
                        log.info("Script does not exist: " + version);
                    }
                } else {
                    log.info("Did not find a new script to run");
                }
            } catch (Exception e) {
                log.error("Database update error: " + e.getMessage(), e);
                return false;
            } finally {
                close(writer);
            }
        }
        return true;
    }

    private int executeScript(String filename, boolean verbose) {
        StringBuilder cmd = new StringBuilder();
        if (useConfiguredUserOnly || !isPostgresUserAvailable(verbose)) {
            // try configured remote user
            cmd.append("PGPASSWORD=\"").append(getDbPwd())
                    .append("\" psql --username=").append(getDbUser())
                    .append(" --username=").append(getDbUser())
                    .append(" --host=").append(getDbHost());
            if (getDbPort() > 0) {
                cmd.append(" --port=").append(getDbPort());
            }
            cmd.append(" ").append(getDbName()).append(" < ").append(filename);
        } else {
            // use local postgres user
            cmd.append("su - postgres -c \"psql ");
            cmd.append(getDbName()).append(" < ").append(filename).append("\"");
        }
        return UnixShell.execute(cmd.toString(), verbose);
    }

    private boolean isPsqlAvailable(boolean verbose) {
        int exitval = UnixShell.execute("psql --version", verbose);
        if (exitval == 0) {
            return true;
        }
        return false;
    }

    private boolean isPostgresUserAvailable(boolean verbose) {
        int exitval = UnixShell.execute("id postgres", verbose);
        if (exitval == 0) {
            return true;
        }
        return false;
    }

    private void close(FileWriter writer) {
        try {
            if (writer != null) {
                writer.close();
            }
        } catch (Exception e) {
        }
    }

    protected List<DatabaseScript> fetchResources(String version) {
        List<DatabaseScript> scripts = new ArrayList<DatabaseScript>();
        String lastScriptId = getLastScriptId();
        if (version == null || !version.equals(lastScriptId)) {
            long lastRunId = version != null ? 0 : fetchLong(lastScriptId);
            log.info("Trying to fetchResources, last script was " + lastScriptId);
            if (lastRunId > -1) {
                try {
                    Resource[] files = pathResolver.getResources(getDatabaseScriptPathPattern(version));
                    if (files != null && files.length > 0) {
                        for (int i = 0; i < files.length; i++) {
                            Resource resource = files[i];
                            if (version == null) {
                                Object scriptIdObj = getScriptId(resource.getFilename());
                                Long scriptId = scriptIdObj instanceof Long ? (Long) scriptIdObj :
                                    fetchLong(scriptIdObj.toString());
                                log.debug("Checking resource '" + resource.getFilename() + "', id " + scriptId);
                                if (lastRunId == 0 || scriptId.longValue() > lastRunId) {
                                    scripts.add(new DatabaseScript(scriptId, resource));
                                }
                            } else {
                                scripts.add(new DatabaseScript(version, resource));
                            }
                        }
                    }
                } catch (Exception e) {
                    log.error("Failed fetching database scripts: " + e.getMessage(), e);
                }
            }
        } else {
            log.info("fetchResources: Ignoring well known script: " + version);
        }
        return DatabaseScript.sort(scripts);
    }

    private long fetchLong(String val) {
        try {
            return Long.valueOf(val);
        } catch (Exception e) {
            return -1;
        }
    }

    private boolean writeScript(FileWriter writer, Resource resource) {
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(resource.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                writer.append(line).append("\n");
            }
            return true;
        } catch (Exception e) {
            log.error("Error writing to file: " + e.getMessage(), e);
        }
        return false;
    }

    private File createImportFile(String importFilename) throws IOException {
        File tmpFile = new File(importFilename);
        removeImportFile(tmpFile);
        tmpFile.createNewFile();
        return tmpFile;
    }

    private void removeImportFile(File impFile) {
        if (impFile != null && impFile.exists() && impFile.canWrite()) {
            impFile.delete();
        }
    }

    protected Object getScriptId(String filename) {
        if (filename != null && filename.indexOf('-') > -1) {
            String p1 = filename.substring(filename.indexOf('-') + 1, filename.length());
            return createLong(p1.substring(0, p1.indexOf('-')));
        }
        return null;
    }

    protected String getLastScriptId() {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        boolean conOK = false;
        try {
            con = getConnection();
            st = con.createStatement();
            conOK = true;
            if (!isDatabaseInitialized()) {
                return "0";
            }
            String sql = "SELECT pvalue FROM osserp.system_properties WHERE name = '"
                    + getDatabaseScriptPropertyName() + "'";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                return rs.getString(1);
            }
            log.warn("getLastScriptId query did not return a result");
        } catch (Exception e) {
            if (!conOK) {
                throw new DatabaseException("Connection error: " + e.getMessage(), e);
            }
            log.error("Caught exception " + e.getMessage(), e);
        } finally {
            close(rs, st, con);
        }
        return "-1";
    }

    private void updateScriptId(String id) {
        if (id != null) {
            Connection con = null;
            Statement st = null;
            try {
                con = getConnection();
                st = con.createStatement();
                String sql = "UPDATE osserp.system_properties SET pvalue = '" + id + "' WHERE name = '"
                        + getDatabaseScriptPropertyName() + "'";
                st.execute(sql);
            } catch (Exception e) {
                throw new DatabaseException("Connection error: " + e.getMessage(), e);
            } finally {
                close(null, st, con);
            }
        }
    }

    protected ResourcePatternResolver getPathResolver() {
        return pathResolver;
    }

}
