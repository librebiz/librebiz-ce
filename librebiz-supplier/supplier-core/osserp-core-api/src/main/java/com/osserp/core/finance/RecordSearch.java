/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 1, 2007 9:36:10 AM 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;
import java.util.List;

import com.osserp.common.Parameter;

import com.osserp.core.BusinessCase;
import com.osserp.core.contacts.ClassifiedContact;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordSearch {

    /**
     * Indicates that record is referenced by a record
     * @param productId
     * @return true if referenced
     */
    boolean isProductReferenced(Long productId);

    /**
     * Provides types for records (invoice, creditNote)
     * @return createableRecordTypes
     */
    List<RecordType> getSalesRecordTypes();

    /**
     * Provides all availabe record types
     * @return recordTypes
     */
    List<RecordType> getRecordTypes();
    
    /**
     * Provides the searchable columns supported by queries of this class.
     * @return list of available columns as label - value pairs
     */
    List<Parameter> getSearchRequestColumns();
    
    /**
     * Finds all records by provided search options
     * @param request
     * @return records or empty list if none found
     */
    List<RecordDisplay> find(RecordSearchRequest request);

    /**
     * Searchs for a sales record of given id
     * @param id
     * @return record or null if no record exists with given id
     */
    Record findSales(Long id);

    /**
     * Searchs for order or most current offer depending on 
     * business case context.
     * @param businessCase
     * @return order if business case is sales, offer if request or null
     */
    Record findSalesRecord(BusinessCase businessCase);

    /**
     * Searchs for a record of given id
     * @param id
     * @return record or null if no record exists with given id
     */
    Record findPurchase(Long id);

    /**
     * Provides distinct purchase records related to sales
     * @param salesId
     * @return purchase records referenced by sales excluding child records
     */
    List<RecordDisplay> findPurchases(Long salesId);

    /**
     * Provides all purchase items
     * @param salesId
     * @return purchase items
     */
    List<RecordDisplayItem> findPurchaseItems(Long salesId);

    /**
     * Finds a record by id and type
     * @param id
     * @param type
     * @return record or null if not exists
     */
    Record findRecord(Long id, Long type);
    
    /**
     * Finds all finance records from start til
     * @param type
     * @param from
     * @param til
     * @return records or empty list
     */
    List<FinanceRecord> findByDate(Long type, Date from, Date til);
    
    /**
     * Provides latest records
     * @param startDate
     * @return latest records
     */
    List<FinanceRecord> findLatest(Long type);

    /**
     * Indicates that a contact is involved in at least one purchase transaction
     * @param contact
     * @return involvedInPurchase
     */
    boolean isInvolvedInPurchase(ClassifiedContact contact);

    /**
     * Finds all delivery date changed infos by product
     * @param productId
     * @return delivery date history by product
     */
    List<DeliveryDateChangedInfo> findDeliveryDateHistoryByProduct(Long productId);

    /**
     * Finds all delivery date changed infos by record (e.g. purchase or sales order)
     * @param id
     * @return delivery date history by record
     */
    List<DeliveryDateChangedInfo> findDeliveryDateHistoryByRecord(Long id);

    /**
     * Finds all item changed infos by record (e.g. purchase or sales order)
     * @param id of the record
     * @return item history by record
     */
    List<ItemChangedInfo> findItemHistoryByRecord(Long id);
    
    /**
     * Provides the count of invoices related to a sales id
     * @param salesId
     * @return count of invoices
     */
    int getInvoiceCount(Long salesId);
    
    /**
     * Provides the count of invoices related to a sales 
     * including downpayement count
     * @param salesId
     * @return count of invoices including downpayment count
     */
    int getInvoiceCountAll(Long salesId);
    
    /**
     * Provides the lowest and highest id of existing records
     * @param type
     * @param company
     * @return array with array[0] = min, array[1] = max  
     */
    Long[] getCurrentNumberMinMax(Long type, Long company);
    
}
