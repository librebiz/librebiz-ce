/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.directory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.directory.FetchmailAccount

import com.osserp.groovy.web.MenuItem

/**
 *
 * @author tn <tn@osserp.com>
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class AbstractDirectoryUserView extends AbstractDirectoryView {
    private static Logger logger = LoggerFactory.getLogger(AbstractDirectoryUserView.class.getName())

    List userGroups = []
    List userPermissions = []
    boolean listModeAvailable = true
    FetchmailAccount fetchmailAccount
    String uid

    protected abstract List loadList()

    protected AbstractDirectoryUserView() {
        super()
        providesEditMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            uid = form.getString('uid')
            if (uid) {
                logger.debug("initRequest: forward request for dedicated user [uid=${uid}]")
                bean = domainDirectoryManager.findUser(uid)
                if (bean) {
                    loadGroups()
                    loadPermissions()
                }
                listModeAvailable = false
            } else {
                logger.debug('initRequest: forward request, loading list')
                list = loadList()
                list.sort{ it.values.uid }
            }
        }
    }

    @Override
    void save() {
        if (mailAccountEditMode && fetchmailAccount) {
            Map valueMap = [:]
            fetchmailAccount.editableAttributeNames.each {
                valueMap[it] = form.getString(it)
            }
            if (!valueMap.empty) {
                fetchmailAccount = domainMailboxManager.update(fetchmailAccount, valueMap)
                logger.debug("save: done [dn=${fetchmailAccount?.values?.dn}]")
            }
            disableMailAccountEditMode()
        } else if (passwordEditMode) {
            String password = form.getString('password')
            String confirmPassword = form.getString('confirmPassword')
            if (!password || !confirmPassword) {
                throw new ClientException(ErrorCode.PASSWORD_MISSING)
            }
            if (password != confirmPassword) {
                throw new ClientException(ErrorCode.PASSWORD_MATCH)
            }
            bean = domainDirectoryManager.updateUserPassword(bean, password)
            disablePasswordEditMode()
        }
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            fetchmailAccount = domainMailboxManager.findMailbox(bean)
            loadGroups()
            loadPermissions()
        } else {
            fetchmailAccount = null
            userGroups = []
            userPermissions = []
        }
    }

    void selectProperty() {
        env.propertyName = form.getString('name')
    }

    void updateProperty() {
        Map values = [:]
        String name = form.getString('name')
        if (name && bean) {
            values[name] = form.getString('value')
            logger.debug("updateProperty: invoked [name=${name}, value=${values[name]}]")
            bean = domainDirectoryManager.updateUser(bean, values)
            env.propertyName = null
            reloadList()
        }
    }

    void userReAssign() {
        bean = domainDirectoryManager.updateUser(bean, [ou:'Users'])
        reloadList()
    }

    void userResign() {
        bean = domainDirectoryManager.updateUser(bean, [ou:'Resign'])
        reloadList()
    }

    void disableMailAccount() {
        if (fetchmailAccount) {
            fetchmailAccount = domainMailboxManager.deactivateMailbox(fetchmailAccount)
        }
    }

    void enableMailAccount() {
        if (fetchmailAccount) {
            fetchmailAccount = domainMailboxManager.activateMailbox(fetchmailAccount)
        }
    }

    boolean isDisplayResignMode() {
        env.displayResignMode
    }

    void disableDisplayResignMode() {
        env.displayResignMode = false
        reloadList()
    }

    void enableDisplayResignMode() {
        env.displayResignMode = true
        reloadList()
        if (list.isEmpty()) {
            throw new ClientException(ErrorCode.NO_DATA_FOUND)
        }
    }

    boolean isMailAccountEditMode() {
        env.mailAccountEditMode
    }

    void disableMailAccountEditMode() {
        env.mailAccountEditMode = false
    }

    void enableMailAccountEditMode() {
        env.mailAccountEditMode = true
    }

    boolean isPasswordEditMode() {
        env.passwordEditMode
    }

    void disablePasswordEditMode() {
        env.passwordEditMode = false
    }

    void enablePasswordEditMode() {
        env.passwordEditMode = true
    }

    @Override
    protected void createCustomNavigation() {
        if (env.passwordEditMode) {
            nav.beanListNavigation = [passwordChangeLink, homeLink]
        } else {
            if (editMode) {
                nav.beanListNavigation = [disableEditLink, homeLink]
            } else if (listModeAvailable) {
                nav.beanListNavigation = [
                    selectExitLink,
                    editLink,
                    passwordChangeLink,
                    homeLink
                ]
            } else {
                nav.beanListNavigation = [exitLink, editLink, passwordChangeLink, homeLink]
            }
        }
        nav.listNavigation = [exitLink, displayResignModeLink, homeLink]
    }

    protected MenuItem getPasswordChangeLink() {
        if (env.passwordEditMode) {
            return navigationLink("/${context.name}/disablePasswordEditMode", 'exitIcon', 'disableChangePassword')
        }
        return navigationLink("/${context.name}/enablePasswordEditMode", 'passwordIcon', 'passwordChange')
    }

    protected MenuItem getDisplayResignModeLink() {
        if (env.displayResignMode) {
            return navigationLink("/${context.name}/disableDisplayResignMode", 'nosmileIcon', 'displayActive')
        }
        return navigationLink("/${context.name}/enableDisplayResignMode", 'smileIcon', 'displayResign')
    }

    protected final void reloadList() {
        if (listModeAvailable) {
            list = loadList()
            createCustomNavigation()
        }
    }

    protected final List filterList(List listToFilter) {
        List result = []
        if (listToFilter) {
            listToFilter.each {
                if (displayResignMode) {
                    if (it.resign) {
                        result << it
                    }
                } else if (!it.resign) {
                    result << it
                }
            }
            result.sort{it.values.uid}
        }
        result
    }

    private void loadGroups() {
        userGroups = domainDirectoryManager.findGroups(bean)
        userGroups.sort{ it.values.cn }
        logger.debug("loadGroups: done [uid=${bean.values.uid}, groupsCount=${userGroups.size()}]")
    }

    private void loadPermissions() {
        userPermissions = domainDirectoryManager.findPermissions(bean)
        userPermissions.sort{ it.values.cn }
        logger.debug("loadPermissions: done [uid=${bean.values.uid}, permissionsCount=${userPermissions.size()}]")
    }
}

