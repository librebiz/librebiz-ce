/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 6, 2004 
 * 
 */
package com.osserp.common.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DateFormatter {

    private static SimpleDateFormat fullDateTimeFormat =
            new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMANY);
    private static SimpleDateFormat dateTimeFormat =
            new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);
    private static SimpleDateFormat dateFormat =
            new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
    private static SimpleDateFormat hoursFormat =
            new SimpleDateFormat("HH", Locale.GERMANY);
    private static SimpleDateFormat minutesFormat =
            new SimpleDateFormat("mm", Locale.GERMANY);
    private static SimpleDateFormat timeFormat =
            new SimpleDateFormat("HH:mm", Locale.GERMANY);
    private static SimpleDateFormat totalTimeFormat =
            new SimpleDateFormat("HH:mm:ss", Locale.GERMANY);
    private static SimpleDateFormat yearFormat =
            new SimpleDateFormat("yyyy", Locale.GERMANY);
    private static SimpleDateFormat monthFormat =
            new SimpleDateFormat("MM", Locale.GERMANY);
    private static SimpleDateFormat dayFormat =
            new SimpleDateFormat("dd", Locale.GERMANY);
    private static SimpleDateFormat monthYearFormat =
            new SimpleDateFormat("MMMM yyyy", Locale.GERMANY);

    /**
     * Provides a date string of format 'yyyy-MM-dd' to use as comparison value in queries
     * @param date or null to get current date string
     * @return formatted date
     */
    public static String getComparisonDate(Date date) {
        Date result = (date == null ? new Date(System.currentTimeMillis()) : date);
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
        return dtf.format(result);
    }

    /**
     * Provides a date string formatted as 'yyyy-MM-dd'
     * @param date
     * @return formatted date or empty string if null
     */
    public static String getISO(Date date) {
        return date == null ? "" : getComparisonDate(date);
    }

    /**
     * Formats a date by default locale (de).
     * @param date the date to format
     * @return formatted date or empty string if date is null
     */
    public static String getDate(Date date) {
        if (date == null)
            return "";
        return dateFormat.format(date);
    }

    /**
     * Formats a date by default locale (de).
     * @param date the date to format
     * @param casenull the resulting string if date is null
     * @return formatted date, casenull or empty string if casenull is null
     */
    public static String getDate(Date date, String casenull) {
        if (date == null)
            return (casenull == null ? "" : casenull);
        return dateFormat.format(date);
    }

    /**
     * Formats a timestamp by default locale (de).
     * @param timestamp the timestamp to format
     * @return formatted timestamp or empty string if timestamp is null
     */
    public static String getDate(Timestamp timestamp) {
        if (timestamp == null)
            return "";
        return dateFormat.format(timestamp);
    }

    /**
     * Formats a timestamp by default locale (de).
     * @param timestamp the timestamp to format
     * @param casenull the resulting string if timestamp is null
     * @return formatted timestamp, casenull or empty string if casenull is null
     */
    public static String getDate(Timestamp timestamp, String casenull) {
        if (timestamp == null)
            return (casenull == null ? "" : casenull);
        return dateFormat.format(timestamp);
    }

    /**
     * Provides the time of the date (hours and minutes)
     * @param date
     * @return formatted time or empty string if date is null
     */
    public static String getTime(Date date) {
        if (date == null)
            return "";
        return timeFormat.format(date);
    }

    /**
     * Provides the time of the timestamp (hours and minutes)
     * @param timestamp
     * @return formatted time or empty string if timestamp is null
     */
    public static String getTime(Timestamp timestamp) {
        if (timestamp == null)
            return "";
        return timeFormat.format(timestamp);
    }

    /**
     * Provides the total time of the date (hours, minutes and seconds)
     * @param date
     * @return formatted time or empty string if date is null
     */
    public static String getTotalTime(Date date) {
        if (date == null)
            return "";
        return totalTimeFormat.format(date);
    }

    /**
     * Provides the total time of the timestamp (hours, minutes and seconds)
     * @param timestamp
     * @return formatted time or empty string if timestamp is null
     */
    public static String getTotalTime(Timestamp timestamp) {
        if (timestamp == null)
            return "";
        return totalTimeFormat.format(timestamp);
    }

    /**
     * Provides the hours of the time of the date
     * @param date
     * @return hours or empty string if date is null
     */
    public static String getHours(Date date) {
        if (date == null)
            return "";
        return hoursFormat.format(date);
    }

    /**
     * Provides the hours of the time of the timestamp
     * @param timestamp
     * @return hours or empty string if timestamp is null
     */
    public static String getHours(Timestamp timestamp) {
        if (timestamp == null)
            return "";
        return hoursFormat.format(timestamp);
    }

    /**
     * Provides the minutes of the time of the date
     * @param date
     * @return minutes or empty string if date is null
     */
    public static String getMinutes(Date date) {
        if (date == null)
            return "";
        return minutesFormat.format(date);
    }

    /**
     * Provides the minutes of the time of the timestamp
     * @param timestamp
     * @return minutes or empty string if timestamp is null
     */
    public static String getMinutes(Timestamp timestamp) {
        if (timestamp == null)
            return "";
        return minutesFormat.format(timestamp);
    }

    /**
     * Provides the formatted date with time
     * @param date the date to format
     * @return formatted date and time or empty string
     */
    public static String getDateAndTime(Date date) {
        if (date == null)
            return "";
        return dateTimeFormat.format(date);
    }

    /**
     * Provides the formatted date with time by default locale (de).
     * @param date the date to format
     * @param casenull the resulting string if date is null
     * @return formatted date and time, casenull or empty string if casenull is null
     */
    public static String getDateAndTime(Date date, String casenull) {
        if (date == null)
            return (casenull == null ? "" : casenull);
        return dateTimeFormat.format(date);
    }

    /**
     * Provides the formatted date with time
     * @param timestamp the timestamp to format
     * @return formatted date and time or empty string
     */
    public static String getDateAndTime(Timestamp timestamp) {
        if (timestamp == null)
            return "";
        return dateTimeFormat.format(timestamp);
    }

    /**
     * Provides the formatted date with time by default locale (de).
     * @param timestamp the date to format
     * @param casenull the resulting string if date is null
     * @return formatted date and time, casenull or empty string if casenull is null
     */
    public static String getDateAndTime(Timestamp timestamp, String casenull) {
        if (timestamp == null)
            return (casenull == null ? "" : casenull);
        return dateTimeFormat.format(timestamp);
    }

    /**
     * Provides the formatted date with time including seconds
     * @param date the date to format
     * @return formatted date and time or empty string
     */
    public static String getDateAndTimeWithSeconds(Date date) {
        if (date == null)
            return "";
        return fullDateTimeFormat.format(date);
    }

    /**
     * Provides the formatted date with time including seconds
     * @param timestamp the timestamp to format
     * @return formatted date and time or empty string
     */
    public static String getDateAndTimeWithSeconds(Timestamp timestamp) {
        if (timestamp == null)
            return "";
        return fullDateTimeFormat.format(timestamp);
    }

    /**
     * Provides the year of a date
     * @param date the date to extract year from
     * @return year or empty string
     */
    public static String getYear(Date date) {
        if (date == null)
            return "";
        return yearFormat.format(date);
    }

    /**
     * Provides the year of a date as integer
     * @param date the date to extract year from
     * @return year as integer or integer value 0
     */
    public static int getYearAsNumber(Date date) {
        if (date == null)
            return 0;
        return Integer.parseInt(yearFormat.format(date));
    }

    /**
     * Provides the month of a date
     * @param date the date to extract month from
     * @return month or empty string
     */
    public static String getMonth(Date date) {
        if (date == null)
            return "";
        return monthFormat.format(date);
    }

    /**
     * Provides the day of a date
     * @param date the date to extract day from
     * @return day or empty string
     */
    public static String getDay(Date date) {
        if (date == null)
            return "";
        return dayFormat.format(date);
    }

    /**
     * Provides the month and year of a date
     * @param date the date to extract values from
     * @return formatted month and year or empty string
     */
    public static String getMonthYear(Date date) {
        if (date == null)
            return "";
        return monthYearFormat.format(date);
    }

    /**
     * Provides current date as formatted string by default locale (de)
     * @return formatted current date string
     */
    public static String getCurrentDate() {
        return getDate(new Date(System.currentTimeMillis()));
    }

    /**
     * Provides current time as formatted string by default locale (de)
     * @return formatted current time string
     */
    public static String getCurrentTime() {
        return getTime(new Date(System.currentTimeMillis()));
    }

    /**
     * Provides a formatted date and time string with no seconds added
     * @param date
     * @return formatted date-time string
     */
    public static String getDateTimeNoSec(Date date) {
        if (date == null)
            return "";
        return dateTimeFormat.format(date);
    }

    /**
     * Provides a formatted date and time string with no seconds added
     * @param timestamp
     * @return formatted date-time string
     */
    public static String getDateTimeNoSec(Timestamp timestamp) {
        if (timestamp == null)
            return "";
        return dateTimeFormat.format(timestamp);
    }

    /**
     * Provides the week of the year of a date
     * @param date date to calculate week by
     * @return week of the year or empty string if date is null
     */
    public static String getWeekOfYear(Date date) {
        return (date == null) ? "" : String.valueOf(DateUtil.getWeekOfYear(date));
    }

    /**
     * Provides the week of the year of a date with a prefix added
     * @param date date to calculate week by
     * @param i18nPrefix
     * @return week of the year or empty string if date is null
     */
    public static String getWeekAndYear(Date date, String i18nPrefix) {
        if (date == null)
            return "";
        StringBuilder buff = new StringBuilder();
        if (i18nPrefix != null) {
            buff.append(i18nPrefix).append(" ");
        }
        buff.append(getWeekOfYear(date)).append(" ").append(getYear(date));
        return buff.toString();
    }

    /**
     * Provides a formatted date string with century removed
     * @param date date to format
     * @return short date or empty string if date is null
     */
    public static String getShortDate(Date date) {
        if (date == null)
            return "";
        String sd = getDate(date);
        StringBuilder b = new StringBuilder(
                sd.substring(0, 6)).append(sd.substring(8, 10));
        return b.toString();
    }

    /**
     * Provides the integer value as formatted time component (e.g. 2-digits representation)
     * @param value the integer to format
     * @return value as formatted digits (0 to 00, 2 to 02, 20 to 20)
     */
    public static String getFormatted(Integer value) {
        if (value == null) {
            return "00";
        }
        return (value < 10 ? ("0" + value) : value.toString());
    }

    /**
     * Creates a hours and minutes representation by minutes as integer
     * @param minutes
     * @return formatted hours and minutes
     */
    public static String getHoursAndMinutes(int minutes) {
        if (minutes == 0) {
            return "00:00";
        }
        int[] hoursAndMinutes = DateUtil.getHoursAndMinutes(minutes);
        return createHoursAndMinutes(hoursAndMinutes, (minutes < 0));
    }

    /**
     * Creates a formatted hours and minutes string by hours as double
     * @param hours
     * @return formatted hours and minutes
     */
    public static String getHoursAndMinutes(double hours) {
        int[] hoursAndMinutes = DateUtil.getHoursAndMinutes(hours);
        return createHoursAndMinutes(hoursAndMinutes, (hours < 0));
    }

    private static String createHoursAndMinutes(int[] hoursAndMinutes, boolean negative) {
        StringBuilder buff = new StringBuilder();
        if (negative) {
            buff.append("-");
        }
        buff
                .append(DateFormatter.getFormatted(hoursAndMinutes[0]))
                .append(":")
                .append(DateFormatter.getFormatted(hoursAndMinutes[1]));
        return buff.toString();
    }
}
