<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="column15l">
    <div class="subcolumnl">
        <div class="spacer"></div>
    </div>
</div>
<div class="column66l">
    <div class="subcolumnc">
        <div class="spacer"></div>
                
        <v:form name="commonPaymentForm" url="/accounting/taxReport/save">
            <div class="contentBox">
                <div class="contentBoxHeader">
                    <div class="contentBoxHeaderLeft">
                        <o:out value="${view.bean.name}"/>
                    </div>
                </div>
                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <div class="spacer"></div>
                                    
                            <table class="valueTable first33 inputs200">
                                <c:import url="/errors/_errors_table_label_value.jsp"/>

                                <tr>
                                    <td style="vertical-align:top;"><fmt:message key="company"/></td>
                                    <td><oc:options name="systemCompanies" value="${view.bean.reference}"/></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:top;"><fmt:message key="name"/></td>
                                    <td><o:out value="${view.bean.name}"/></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="fromDate"/></td>
                                    <td><o:date value="${view.bean.startDate}"/></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="til"/></td>
                                    <td><o:date value="${view.bean.stopDate}"/></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="records"/></td>
                                    <td>
                                        <v:link url="/accounting/taxReport/enableInflowSelectionMode">
                                            <fmt:message key="debitors"/>
                                        </v:link>
                                    </td>
                                </tr>
                                <tr>
                                    <td> </td>
                                    <td>
                                        <v:link url="/accounting/taxReport/enableOutflowSelectionMode">
                                            <fmt:message key="creditors"/>
                                        </v:link>
                                    </td>
                                </tr>
                                <%-- 
                                <tr>
                                    <td><fmt:message key="amount"/></td>
                                    <td>
                                        <o:number value="${view.bean.amount}" format="currency"/>
                                        <span style="margin-left:2px;">EUR</span>
                                    </td>
                                </tr>
                                --%>
                            </table>
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
            </div>
        </v:form>
        <div class="spacer"></div>
    </div>
</div>
<div class="column15r">
    <div class="subcolumnr">
        <div class="spacer"></div>
    </div>
</div>
