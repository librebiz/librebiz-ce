
# Schnellanleitung

Diese Anleitung beschreibt die Installation der Software auf einem Rechner
mit *Debian GNU/Linux* oder *Ubuntu Server Edition*.

Die Anleitung wurde mit folgenden Betriebssystemen getestet:

* Debian GNU/Linux 12 (Bookworm)
* Debian GNU/Linux 11 (Bullseye)
* Debian GNU/Linux 10 (Buster)
* Ubuntu Server Edition 24.04 LTS (Noble Numbat)
* Ubuntu Server Edition 22.04 LTS (Jammy Jellyfish)
* Ubuntu Server Edition 20.04 LTS (Focal Fossa)
* Ubuntu Server Edition 18.04 LTS (Bionic Beaver)

Weitere Anleitungen und Detailinformationen werden im Supportbereich unserer
[Webseite](https://librebiz.org/manuals) bereitgestellt.

## Download und Bau der Software

Installation der benötigten Pakete (als root):

```none
~$ sudo su - root
~# apt update && apt upgrade
~# apt install default-jdk-headless maven dpkg-dev git postfix
```

Anlegen eines Benutzers zum Bau der Software:

```none
~# adduser librebiz-dev
~# su - librebiz-dev
```

Quellcode herunterladen und Software kompilieren:

```none
~$ git clone https://codeberg.org/librebiz/librebiz-ce.git
~$ cd librebiz-ce
~$ git checkout v6.1.3
~$ ./build all
~$ exit
```

Die erstellten Pakete sowie Metadaten landen im Projektverzeichnis unter
`./dist/debian`.

## Installation

Paketverzeichnis konfigurieren und Software aktualisieren:

```none
~# echo "deb [ trusted=yes ] file:/home/librebiz-dev/librebiz-ce/dist/debian ./" > /etc/apt/sources.list.d/librebiz.list
~# apt update && apt upgrade
```

#### Pakete installieren - Debian 10, 11, 12 sowie Ubuntu 20.04, 22.04 und 24-04

```none
~# apt install librebiz
```

#### Pakete installieren - Ubuntu 18.04

Ubuntu 18.04 bietet sowohl Tomcat 8 als auch Tomcat 9 zur Auswahl an.
Das für den Einsatz mit Tomcat 9 vorgesehene Paket `librebiz-tomcat9` ist für
Debian 10 erstellt und mit Ubuntu 18.04 nicht kompatibel.

Die nachfolgende Paketauswahl führt zur Installation der Software zusammen mit
den kompatiblen Paketen `tomcat8-user` und `tomcat8-common`.
Die Option `--no-install-recommends` verhindert die gleichzeitige Installation
von Tomcat 9:

```none
~# apt install --no-install-recommends activemq postgresql librebiz librebiz-common librebiz-tomcat8 librebiz-webapp
```

## Konfiguration

Das Admin-Programm konfiguriert alle benötigten Dienste, richtet die Datenbank
ein und startet die Anwendung:

```none
~# librebiz-admin setup
```

Die Anwendung ist sodann unter `http://IP-ODER-NAME:9980/osdb/` erreichbar.
Zur fehlerfreien Anwendung muss die Ausführung von JavaScript für diese URL
erlaubt werden. Hierbei werden keine Scripte von externen Quellen geladen.

Hinweis: Auf Rechnern mit OpenJDK 11 treten Warnungen bei Ausführung von
`librebiz-admin` auf (https://issues.apache.org/jira/browse/GROOVY-9070).

Hierbei handelt es sich um rein technische Informationen für Entwickler
(Hinweis auf künftig entfallende Funktionen in Groovy).

#### Apache Webserver (optional)

Bei produktivem Einsatz sollte der Zugriff auf die Anwendung stets mittels
SSL abgesichert werden:

```none
~# apt install apache2
~# a2enmod ssl
~# a2ensite default-ssl
~# a2enmod proxy_http
~# cp /usr/share/doc/librebiz-common/examples/httpd-tomcat.conf /etc/apache2/conf-available/librebiz.conf
~# a2enconf librebiz
~# systemctl restart apache2
```

Die Anwendung ist im Anschluss unter `https://IP-ODER-NAME/osdb/` erreichbar.

#### Firma und Mitarbeiter einrichten

Informationen zur Konfiguration der Firma, Benutzerkonten und Berechtigungen
liefert der [Setup Guide](https://librebiz.org/manuals/de/setup/).

----

## Aktualisierung

```none
~# su - librebiz-dev
~$ cd librebiz-ce
~$ git pull
~$ git checkout v6.1.3
~$ ./build all
~$ exit
~# librebiz-admin reinstall
```

#### Datenbank Schema-Updates

Die Datenbank wird bei verfügbaren Updates nicht automatisch aktualisiert.

Verfügbare Schema-Updates werden bei Aufruf des Kommandos `librebiz-admin`
mit den Optionen `reinstall` sowie `update` angezeigt.

Mit dem Aufruf des Kommandos `librebiz-admin update $version` wird das
Update entsprechend der angegebenen Version installiert. Der Aufruf von
`librebiz-admin update all` hingegen installiert alle verfügbaren Updates.
