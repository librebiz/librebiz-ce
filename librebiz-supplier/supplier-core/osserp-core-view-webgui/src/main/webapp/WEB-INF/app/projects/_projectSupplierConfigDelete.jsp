<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="projectSupplierConfigView" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="deleteProjectSupplierRelation" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:if test="${!empty view.selectedSupplier}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="company"><fmt:message key="company" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:out value="${view.selectedSupplier.displayName}" />
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="voucher"><fmt:message key="voucher" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${empty view.bean.recordId}">
                                <fmt:message key="notAssignedLabel" />
                            </c:when>
                            <c:when test="${view.bean.purchaseInvoiceAssigned}">
                                <fmt:message key="deleteProjectSupplierRelation" />
                            </c:when>
                            <c:otherwise>
                                <div class="checkbox">
                                    <label for="includePurchaseOrder">
                                        <v:checkbox id="includePurchaseOrder"/>
                                        <fmt:message key="deleteIncludingPurchaseOrder" />
                                    </label>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/exit"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="delete" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
