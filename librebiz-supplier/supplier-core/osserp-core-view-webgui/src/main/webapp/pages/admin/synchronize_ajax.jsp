<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:if test="${empty sessionScope.synchronizeView}"><c:redirect url="/errors/error_context.jsp"/></c:if>
<c:set var="view" value="${sessionScope.synchronizeView}"/>

<o:ajaxForm name="synchronize" url="/synchronize.do?method=syncEmployee" targetElement="ajaxContent" preRequest="if (checkSearchException()) {" postRequest="}">
<table class="table" style="width: 300px; margin-left: auto; margin-right: auto;">
	<tr>
		<th><fmt:message key="synchronizeEmployee"/></th>
		<th><fmt:message key="action"/></th>
	</tr>
	<tr>
		<td>
			<select name="id" size="1" id="employeeId">
				<option value="0" selected="selected"><fmt:message key="makeSelection"/></option>
				<c:forEach var="employee" items="${view.employees}">
					<option value="${employee.id}"><o:out value="${employee.name}"/></option>
				</c:forEach>
			</select>
		</td>
		<td>
			<o:submit rawValue="ok"/>
		</td>
	</tr>
</table>
</o:ajaxForm>
<br/>
<table class="table table-striped" style="width: 300px; margin-left: auto; margin-right: auto;">
	<tr>
		<th><fmt:message key="synchronizeTheSystem"/></th>
	</tr>
	<tr>
		<td>
			<o:ajaxLink
				preRequest="activityMessage();"
				targetElement="ajaxContent"
				url="/synchronize.do?method=syncAll"
				linkId="synchronizeEverything">
				<fmt:message key="synchronizeEverything"/>
			</o:ajaxLink>
		</td>
	</tr>
	<tr class="altrow">
		<td>
			<o:ajaxLink
				preRequest="activityMessage();"
				targetElement="ajaxContent"
				url="/synchronize.do?method=syncBranches"
				linkId="synchronizeBranches">
				<fmt:message key="synchronizeBranches"/>
			</o:ajaxLink>
		</td>
	</tr>
	<tr>
		<td>
			<o:ajaxLink
				preRequest="activityMessage();"
				targetElement="ajaxContent"
				url="/synchronize.do?method=syncEmployeeGroups"
				linkId="synchronizeEmployeeGroups">
				<fmt:message key="synchronizeEmployeeGroups"/>
			</o:ajaxLink>
		</td>
	</tr>
	<tr class="altrow">
		<td>
			<o:ajaxLink
				preRequest="activityMessage();"
				targetElement="ajaxContent"
				url="/synchronize.do?method=syncEmployees"
				linkId="synchronizeEmployees">
				<fmt:message key="synchronizeAllEmployees"/>
			</o:ajaxLink>
		</td>
	</tr>
	<tr>
		<td>
			<o:ajaxLink
				preRequest="activityMessage();"
				targetElement="ajaxContent"
				url="/synchronize.do?method=syncPrivateContacts"
				linkId="synchronizePrivateContacts">
				<fmt:message key="synchronizePrivateContacts"/>
			</o:ajaxLink>
		</td>
	</tr>
</table>
