<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestQueryIndexView" />
<c:set var="requestListingView" scope="request" value="${sessionScope.requestQueryIndexView}"/>
<c:set var="requestListingEssential" scope="request" value="${sessionScope.requestQueryIndexView.latestRequests}"/>
<c:set var="requestListName" scope="request" value="currentRequests"/>
<c:set var="requestSelectExitTarget" scope="request" value="requestQueryIndex"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>

    <tiles:put name="headline">
        <c:choose>
            <c:when test="${empty view.list}">
                <fmt:message key="queryRequests"/>
            </c:when>
            <c:otherwise>
                <o:listSize value="${view.list}"/> <fmt:message key="requestsFound"/>
                <c:choose>
                    <c:when test="${view.defaultListMode}">
                        <span> - <fmt:message key="currentRequests"/></span>
                    </c:when>
                    <c:when test="${!empty view.selectedUser}">
                        <span> - <o:out value="${view.selectedUser.employee.displayName}"/></span>
                    </c:when>
                    <c:when test="${!empty view.selectedBranchId}">
                        <span> - <o:out value="${view.selectedBranch.name}"/></span>
                    </c:when>
                    <c:when test="${!empty view.selectedOrigin}">
                        <span> - <oc:options name="campaigns" value="${view.selectedOrigin}"/></span>
                    </c:when>
                    <c:when test="${!empty view.selectedBusinessType}">
                        <span> - <fmt:message key="requestType"/>: <oc:options name="requestTypes" value="${view.selectedBusinessType}"/></span>
                    </c:when>
                </c:choose>
            </c:otherwise>
        </c:choose>
    </tiles:put>

    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="requestQueryIndexContent">
            <div class="row">
                <c:choose>
                    <c:when test="${!empty view.list}">
                        <c:import url="_requestQueryResultFull.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_requestQuerySelection.jsp"/>
                        <c:import url="_requestQueryResultEssential.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
