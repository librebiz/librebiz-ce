<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.purchaseInvoiceView}">
	<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.purchaseInvoiceView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records_edit.css"/>
</style>
</tiles:put>

<tiles:put name="headline">
<c:import url="/pages/records/record_headline_edit.jsp"/>
</tiles:put>

<tiles:put name="headline_right">
<a href="<c:url value="/purchaseInvoice.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>

        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">

                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>

                    <o:form name="recordForm" url="/purchaseInvoice.do">
                    <input type="hidden" name="method" value="update"/>
                    <table class="table">
                        <tr>
                            <td class="recordEditLeft" valign="top">
                                <table>
                                    <c:import url="/pages/records/record_branch_edit.jsp" />
                                    <c:import url="/pages/records/record_taxfree_edit.jsp"/>
                                    <c:import url="/pages/records/record_custom_header_edit.jsp"/>
                                    <c:import url="/pages/records/record_notes_edit.jsp"/>
                                </table>
                            </td>
                            <td class="recordEditRight" valign="top">
                                <table>
                                    <c:import url="/pages/records/record_created_date_edit.jsp"/>
                                    <c:import url="/pages/records/record_currency_edit.jsp"/>
                                    <c:import url="/pages/records/record_taxrate_edit.jsp"/>
                                    <c:if test="${!record.taxFree}">
                                    <c:import url="/pages/records/record_summary_edit.jsp"/>
                                    </c:if>
                                    <tr><td colspan="2"> </td></tr>
                                    <c:import url="/pages/records/record_save_changes.jsp"/>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </o:form>
                </div>
            </div>
        </div>
    </div>
</div>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
