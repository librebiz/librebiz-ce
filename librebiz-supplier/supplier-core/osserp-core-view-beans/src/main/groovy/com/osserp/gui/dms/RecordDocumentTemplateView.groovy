/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 8, 2015 
 * 
 */
package com.osserp.gui.dms

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException

import com.osserp.core.BusinessCase
import com.osserp.core.BusinessCaseSearch
import com.osserp.core.BusinessCaseTemplateManager
import com.osserp.core.BusinessTemplate
import com.osserp.core.finance.Record
import com.osserp.core.finance.RecordManager
import com.osserp.core.finance.RecordDocumentTemplateService

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class RecordDocumentTemplateView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(RecordDocumentTemplateView.class.getName())

    Boolean businessCaseAvailable = false
    BusinessCase businessCase
    String contextName
    Record record
    
    RecordDocumentTemplateView() {
        super()
        enablePopupView()
        dependencies = ['businessCaseView']
    }
    
    protected abstract RecordManager getRecordManager();

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        
        if (forwardRequest) {
            def businessObject = env.businessCaseView?.businessCase
            Long id = form.getLong('record')
            if (id) {
                record = recordManager.find(id)
                logger.debug("initRequest: recordSearch done [found=${record?.id}]")
            }
            if (!businessObject && record?.sale) {
                businessObject = businessCaseSearch.findBusinessCase(record.sale)
            }
            initBusinessCase(businessObject)
        }
    }
    
    protected void initBusinessCase(BusinessCase obj) {
        businessCase = obj
        if (businessCase) {
            businessCaseAvailable = true
            contextName = businessCase.contextName
            if (businessTemplatePreloadingEnabled) {
                loadBusinessTemplates()
            }
        } else {
            businessCaseAvailable = false
        }
    }
    
    byte[] createPdf() throws ClientException {
        Long id = form.getLong('id')
        if (!id) {
            throw new IllegalStateException('template id missing') 
        }
        BusinessTemplate template = list.find { it.id == id }
        if (!template) {
            throw new IllegalStateException('template not found')
        }
        byte[] pdf = recordDocumentTemplateService.createPdf(
            domainUser,
            template,
            record,
            businessCase)
        logger.debug("createPdf: done [record=${record?.id}, template=${template.id}]")
        return pdf
    }
    
    protected void disableBusinessTemplatePreloading() {
        env.businessTemplatePreloading = false
    }
    
    protected void enableBusinessTemplatePreloading() {
        env.businessTemplatePreloading = true
    }

    protected boolean getBusinessTemplatePreloadingEnabled() {
        env.businessTemplatePreloading == true
    }
    
    protected void loadBusinessTemplates() {
        
        if (businessCase) {
            Long branchId = record?.branchId
            if (!branchId) {
                branchId = businessCase.branch?.id
                logger.debug("loadBusinessTemplates: record.branchid missing, using default [record=${record?.id}, businessCase=${businessCase?.primaryKey}]")
            }
            list = businessCaseTemplateManager.findTemplates(
                businessCase.contextName, branchId)
            logger.debug("loadBusinessTemplates: done [contextName=${businessCase.contextName}, branchId=${branchId}, count=${list.size()}]")
        }
    }
    
    protected BusinessCaseSearch getBusinessCaseSearch() {
        getService(BusinessCaseSearch.class.getName())
    }
    
    protected BusinessCaseTemplateManager getBusinessCaseTemplateManager() {
        getService(BusinessCaseTemplateManager.class.getName())
    }
    
    protected RecordDocumentTemplateService getRecordDocumentTemplateService() {
        getService(RecordDocumentTemplateService.class.getName())
    }
}
