/**
 *
 * Copyright (C) 2002 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 03.05.2002 
 * 
 */
package com.osserp.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.service.Context;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PhoneUtil extends AbstractValidator {
    private static Logger log = LoggerFactory.getLogger(PhoneUtil.class.getName());
    
    private static final String BLANK = " ";
    private static final String SEPARATOR = " - ";
    private static Map<Long, PhonePrefix> prefixMap = new HashMap<>();
    private static Set<String> countrySet = new HashSet();

    /**
     * Checks if given string represents a valid phone number wich means that the string contains only digits, blanks and minus signs
     * @param number
     * @return true if phone number
     */
    public static boolean isPhoneNumber(String number) {
        if (number == null) {
            return false;
        }
        StringBuffer buffer = new StringBuffer(number);
        StringUtil.deleteBlanks(buffer);
        StringUtil.deleteMinus(buffer);
        try {
            Long.parseLong(buffer.toString());
            return true;
        } catch (NumberFormatException nfx) {
            return false;
        }
    }

    /**
     * Removes common phone formatting characters, blanks and leading zeros.
     * @param number
     * @param preserveZero
     * @return number only string
     */
    public static String clearPhoneNumber(String number, boolean preserveZero) {
        if (number == null) {
            return null;
        }
        StringBuffer n = new StringBuffer(number.trim());
        StringUtil.deleteBlanks(n);
        StringUtil.deleteMinus(n);
        StringUtil.remove(n, "+");
        StringUtil.remove(n, "/");
        StringUtil.remove(n, "(");
        StringUtil.remove(n, ")");
        if (!preserveZero) {
            while (n.toString().startsWith("0")) {
                n.deleteCharAt(0);
            }
        }
        return n.toString();
    }

    /**
     * Creates a phone number in the format 492772888999 from given values.
     * @param country country
     * @param prefix
     * @param number
     * @return formatted number or null if illegal characters found or any param is null
     */
    public static String createPhoneNumber(
            String country,
            String prefix,
            String number) {

        if (country == null || prefix == null || number == null) {
            return null;
        }
        StringBuffer result = new StringBuffer(16);
        StringBuffer p = new StringBuffer(prefix);
        StringBuffer n = new StringBuffer(number);
        StringUtil.deleteBlanks(p);
        StringUtil.deleteBlanks(n);
        StringUtil.deleteMinus(p);
        StringUtil.deleteMinus(n);
        try {
            return result
                    .append(Long.parseLong(country))
                    .append(Long.parseLong(p.toString()))
                    .append(Long.parseLong(n.toString())).toString();
        } catch (NumberFormatException nfx) {
            return null;
        }
    }

    /**
     * Creates a phone number in the format +49 (2772) 888999 from given values as expected by ms outlook.
     * @param country
     * @param prefix
     * @param number
     * @return formatted number or null if illegal characters found or any param is null
     */
    public static String createOutlook(String country, String prefix, String number) {

        if (country == null || prefix == null || number == null) {
            return null;
        }
        StringBuffer result = new StringBuffer(16);
        StringBuffer p = new StringBuffer(prefix);
        StringUtil.deleteBlanks(p);
        StringUtil.deleteMinus(p);
        try {
            return result
                    .append("+")
                    .append(Long.parseLong(country))
                    .append(BLANK)
                    .append(createOutlookPrefix(p.toString()))
                    .append(BLANK)
                    .append(number).toString();
        } catch (NumberFormatException nfx) {
            return null;
        }
    }

    private static String createOutlookPrefix(String prefix) {
        StringBuilder result = new StringBuilder("(");
        if (prefix.startsWith("0")) {
            result.append(prefix.substring(1));
        } else {
            result.append(prefix);
        }
        result.append(")");
        return result.toString();
    }

    /**
     * Creates a phone number in the format +49 (0)2772-888999 from given values.
     * @param country
     * @param prefix
     * @param number
     * @return formatted number or null if illegal characters found or any param is null
     */
    public static String createInternational(String country, String prefix, String number) {

        if (country == null || prefix == null || number == null) {
            return null;
        }
        StringBuffer result = new StringBuffer(16);
        StringBuffer p = new StringBuffer(prefix);
        StringUtil.deleteBlanks(p);
        StringUtil.deleteMinus(p);
        try {
            return result
                    .append("+")
                    .append(Long.parseLong(country))
                    .append(BLANK)
                    .append(createPrefix(p.toString()))
                    .append(SEPARATOR)
                    .append(number).toString();
        } catch (NumberFormatException nfx) {
            return null;
        }
    }

    private static String createPrefix(String prefix) {
        StringBuilder result = new StringBuilder("(0)");
        if (prefix.startsWith("0")) {
            result.append(prefix.substring(1));
        } else {
            result.append(prefix);
        }
        return result.toString();
    }

    /**
     * Validates a fax number.
     * @param prefix of the number
     * @param number
     * @param required if prefix and number must be set
     * @throws ClientException if prefix or number is invalid, prefix and number are not set if required is set or if only prefix or number is set
     */
    public static void validateFax(String prefix, String number, boolean required)
            throws ClientException {
        boolean prefixSet = isSet(prefix);
        boolean numberSet = isSet(number);
        if (required) {
            if (!prefixSet) {
                throw new ClientException(ErrorCode.FAX_PREFIX_MISSING);
            }
            if (!numberSet) {
                throw new ClientException(ErrorCode.FAX_NUMBER_MISSING);
            }
        }
        validatePrefix(prefix, ErrorCode.FAX_PREFIX_INVALID);
        if (numberSet && !PhoneUtil.isPhoneNumber(number)) {
            throw new ClientException(ErrorCode.FAX_NUMBER_INVALID);
        }
        if ((prefixSet && !numberSet) || (!prefixSet && numberSet)) {
            throw new ClientException(ErrorCode.FAX_INCOMPLETE);
        }
    }

    /**
     * Validates current mobile number.
     * @param prefix of the number
     * @param number
     * @param required if prefix and number must be set
     * @throws ClientException if prefix or number is invalid, prefix and number are not set if required is set or if only prefix or number is set
     */
    public static void validateMobile(String prefix, String number, boolean required)
            throws ClientException {
        boolean prefixSet = isSet(prefix);
        boolean numberSet = isSet(number);
        if (required) {
            if (!prefixSet) {
                throw new ClientException(ErrorCode.MOBILE_PREFIX_MISSING);
            }
            if (!numberSet) {
                throw new ClientException(ErrorCode.MOBILE_NUMBER_MISSING);
            }
        }
        validatePrefix(prefix, ErrorCode.MOBILE_PREFIX_INVALID);
        if (numberSet && !PhoneUtil.isPhoneNumber(number)) {
            throw new ClientException(ErrorCode.MOBILE_NUMBER_INVALID);
        }
        if ((prefixSet && !numberSet) || (!prefixSet && numberSet)) {
            throw new ClientException(ErrorCode.MOBILE_INCOMPLETE);
        }
    }

    /**
     * Validates current phone number.
     * @param prefix of the number
     * @param number
     * @param required if prefix and number must be set
     * @throws ClientException if prefix or number is invalid, prefix and number are not set if required is set or if only prefix or number is set
     */
    public static void validatePhone(String prefix, String number, boolean required)
            throws ClientException {
        boolean prefixSet = isSet(prefix);
        boolean numberSet = isSet(number);
        if (required) {
            if (!prefixSet) {
                throw new ClientException(ErrorCode.PHONE_PREFIX_MISSING);
            }
            if (!numberSet) {
                throw new ClientException(ErrorCode.PHONE_NUMBER_MISSING);
            }
        }
        validatePrefix(prefix, ErrorCode.PHONE_PREFIX_INVALID);
        if (numberSet && !PhoneUtil.isPhoneNumber(number)) {
            throw new ClientException(ErrorCode.PHONE_NUMBER_INVALID);
        }
        if ((prefixSet && !numberSet) || (!prefixSet && numberSet)) {
            throw new ClientException(ErrorCode.PHONE_INCOMPLETE);
        }
    }

    private static void validatePrefix(String prefix, String invalidPrefixError) throws ClientException {
        if (prefix != null && (!PhoneUtil.isPhoneNumber(prefix) || prefix.length() > 5)) {
            throw new ClientException(invalidPrefixError);
        }
    }

    public static PhonePrefix fetchPhonePrefix(String phoneNumber) {
        Map<Long, PhonePrefix> map = getPrefixMap();
        PhonePrefix prefix = null;
        String cleared = clearPhoneNumber(phoneNumber, false);
        String tmp = cleared;
        int size = tmp.length();
        while (size > 0) {
            Long i = NumberUtil.createLong(tmp);
            if (i != null) {
                prefix = map.get(i);
            }
            if (prefix != null) {
                return prefix;
            }
            --size;
            tmp = tmp.substring(0, size);
        }
        return null;
    }
    
    public static String getCountryPrefix(String number) {
        Set<String> cs = getCountrySet();
        String tmp = number;
        int size = tmp.length();
        while (size > 0) {
            if (cs.contains(tmp)) {
                return tmp;
            }
            --size;
            tmp = tmp.substring(0, size);
        }
        return null;
    }
    
    public static synchronized Set<String> getCountrySet() {
        if (countrySet.isEmpty()) {
            countrySet = createCountrySet();
            if (log.isDebugEnabled()) {
                log.debug("getCountrySet() set created [size=" + countrySet.size() + "]");
            }
        }
        return countrySet;
    }
    
    private static Set<String> createCountrySet() {
        //List<String> raw = FileUtil.readResourceFile("/com/osserp/common/util/phone_country_prefix.raw");
        List<String> raw = getPhoneConfigFile("phone_country_prefix.raw");
        Set<String> result = new HashSet();
        for (int i = 0, j = raw.size(); i < j; i++) {
            String iprefix = raw.get(i);
            if (iprefix != null) {
                // iprefix == +49
                String nprefix = iprefix.substring(1, iprefix.length());
                // nprefix == 49
                String zprefix = "00" + nprefix;
                // iprefix == 0049
                result.add(iprefix);
                result.add(nprefix);
                result.add(zprefix);
            }
        }
        return result;
    }
    
    public static synchronized Map<Long, PhonePrefix> getPrefixMap() {
        if (prefixMap.isEmpty()) {
            prefixMap = createPrefixMap();
            if (log.isDebugEnabled()) {
                log.debug("getPrefixMap() map created [size=" + prefixMap.size() + "]");
            }
        }
        return prefixMap;
    }
    
    private static Map<Long, PhonePrefix> createPrefixMap() {
        // List<String> raw = FileUtil.readResourceFile("/com/osserp/common/util/phone_locality_prefix_de.raw");
        List<String> raw = getPhoneConfigFile("phone_locality_prefix_de.raw");
        Map<Long, PhonePrefix> pm = new HashMap<>();
        PhonePrefix current = null;
        for (int i = 0, j = raw.size(); i < j; i++) {
            String next = raw.get(i);
            if (next != null) {
                Long nprefix = NumberUtil.createLong(next);
                if (nprefix != null) {
                    current = new PhonePrefix(nprefix);
                } else if (current != null) {
                    current.setLocality(next);
                    pm.put(current.getPrefixNum(), (PhonePrefix) current.clone());
                    current = null;
                }
            }
        }
        return pm;
    }
    
    private static List<String> getPhoneConfigFile(String filename) {
        assert filename != null;
        List<String> raw = new ArrayList<>();
        String configFile = Context.getConfigLocation() + "/" + filename;
        if (FileUtil.exists(configFile)) {
            try {
                raw = FileUtil.readFilesystemFile(configFile);
            } catch (Exception e) { }
        }
        if (raw == null || raw.isEmpty()) {
            try {
                raw = FileUtil.readResourceFile(filename);
            } catch (Exception e) { }
        }
        if (raw == null || raw.isEmpty()) {
            try {
                raw = FileUtil.readResourceFile("/com/osserp/common/util/" + filename);
            } catch (Exception e) { }
        }
        return raw;
    }
  
    // Formatted phone number creator methods
    
    /**
     * Attempts to split a phone number string into country, prefix and number components 
     * and tries to determine the device type by identified prefix (e.g. phone or mobile
     * @param number
     * @return string array with [country, prefix, number, device, validationStatus] 
     * or null if no or invalid number provided. 
     */
    public static String[] createPhoneNumber(String number) {
        String[] complete = null;
        // 1st attempt: format by preformatting and matches against country and prefix patterns and values
        String[] formatted = StringUtil.getTokenArray(number, " ");
        if (formatted.length == 2) {
            complete = createByPreformatted(formatted);
        } else {
            complete = createByRaw(number);
        }
        if (complete == null || complete.length == 5) {
            return complete;
        }
        // 2nd attempt: format by preformatting only
        if (formatted.length == 3) {
            return new String[] { formatted[0], formatted[1], formatted[2], "device" , "unchecked" };
        } else if (formatted.length == 2) {
            return new String[] { Constants.GERMAN_PREFIX, formatted[0], formatted[1], "device" , "unchecked" };
        }
        // 3rd attempt: clear number without previously kept leading zeros
        String cleared = PhoneUtil.clearPhoneNumber(number, false);
        if (cleared == null || cleared.length() < 1) {
            return null;
        }
        if (cleared.startsWith(Constants.GERMAN_PREFIX)) {
            cleared = cleared.substring(2, cleared.length());
        }
        complete = createByRaw(cleared);
        if (complete != null && complete.length == 5) {
            return complete;
        }
        if (cleared.length() < 7 || cleared.length() > 12) {
            return null;
        }
        if (cleared.startsWith("12345")) {
            return null;
        }
        if (cleared.length() >= 10) {
            String country = PhoneUtil.getCountryPrefix(cleared);
            if (country != null) {
                cleared = PhoneUtil.clearPhoneNumber(cleared.substring(country.length(), cleared.length()), false);
                String prefix = cleared.substring(0, 3);
                cleared = cleared.substring(3, cleared.length());
                country = getShortCountry(country);
                return new String[] { country, prefix, cleared, "device" , "foreignOrInvalid" };
            }
        }
        String prefix = cleared.substring(0, 4);
        cleared = cleared.substring(4, cleared.length());
        return new String[] { Constants.GERMAN_PREFIX, prefix, cleared, "device" , "invalid" };
    }

    private static String[] createByPreformatted(String[] formatted) {
        String[] complete = complete(formatted);
        PhonePrefix prefix = PhoneUtil.fetchPhonePrefix(complete[1]);
        if (prefix != null) {
            if (prefix.isMobileNumber()) {
                complete[3] = "mobile";
            } else {
                complete[3] = "phone";
            }
            complete[4] = "patternMatched";
            return complete;
        }
        String country = PhoneUtil.getCountryPrefix(complete[1]);
        if (country != null) {
            String crest = complete[1].substring(country.length(), complete[1].length());
            country = getShortCountry(country);
            if ((crest == null || crest.length() == 0) && !Constants.GERMAN_PREFIX.equals(country)) {
                complete[0] = country;
                if (complete[2] != null && complete[2].length() > 1) {
                    if (complete[2].length() > 3) {
                        complete[1] = complete[2].substring(0, 3);
                        complete[2] = complete[2].substring(3, complete[2].length());
                        complete[4] = "foreignPatternMatched";
                        return complete;
                    }
                    if (complete[2].length() > 2) {
                        complete[1] = complete[2].substring(0, 2);
                        complete[2] = complete[2].substring(2, complete[2].length());
                        complete[4] = "foreignPatternMatched";
                        return complete;
                    }
                    complete[1] = complete[2].substring(0, 1);
                    complete[2] = complete[2].substring(1, complete[2].length());
                    complete[4] = "foreignPatternMatched";
                    return complete;
                }
                return null;
                    
            } else if (Constants.GERMAN_PREFIX.equals(country)) {
                if (crest == null) {
                    crest = "";
                }
                String newn = crest + complete[2];
                prefix = PhoneUtil.fetchPhonePrefix(newn);
                if (prefix != null) {
                    String p = Long.valueOf(prefix.getPrefixNum()).toString();
                    complete[1] = p;
                    complete[2] = newn.substring(p.length(), newn.length());
                    if (prefix.isMobileNumber()) {
                        complete[3] = "mobile";
                    } else {
                        complete[3] = "phone";
                    }
                    complete[4] = "patternMatched";
                    return complete;
                }
                complete[1] = crest.length() < 1 ? "9999" : crest;
                complete[4] = "unknown";
                return complete;
                     
            } else {
                complete[0] = country;
                complete[1] = crest; 
                complete[4] = "foreignPatternMatched";
                return complete;
            }
        }
        return new String[] {"nextstep"};
    }

    private static String[] createByRaw(String plain) {
        String number = PhoneUtil.clearPhoneNumber(plain, true);
        String country = Constants.GERMAN_PREFIX;
        if (number.startsWith("00")) {
            country = PhoneUtil.getCountryPrefix(number);
            if (country != null) {
                number = PhoneUtil.clearPhoneNumber(number.substring(country.length(), number.length()), false);
                country = getShortCountry(country);
                if (!Constants.GERMAN_PREFIX.equals(country)) {
                    if (number.length() > 3) {
                        String prefix = number.substring(0, 3);
                        number = number.substring(3, number.length());
                        return new String[] { country, prefix, number, "phone" , "foreignPatternMatched" };
                    }
                    return null;
                } 
            } else {
                number = PhoneUtil.clearPhoneNumber(number, false);
                country = Constants.GERMAN_PREFIX;
            }
        }
        PhonePrefix prefix = PhoneUtil.fetchPhonePrefix(number);
        if (prefix != null) {
            String phonePrefix = Long.valueOf(prefix.getPrefixNum()).toString();
            String phoneNumber = number.substring(phonePrefix.length(), number.length());
            if (number.startsWith("0") && !phonePrefix.startsWith("0")) {
                phoneNumber = number.substring((phonePrefix.length() + 1), number.length());
            }
            String[] complete = new String[] { country, phonePrefix, phoneNumber, "phone" , "patternMatched" }; 
            if (prefix.isMobileNumber()) {
                complete[3] = "mobile";
            }
            return complete;
        }
        return new String[] {"nextstep"};
    }

    private static String[] complete(String[] formatted) {
        StringBuilder number = new StringBuilder(formatted[1]);
        if (formatted.length > 2) {
            for (int i = 2, j = formatted.length; i < j; i++) {
                number.append(formatted[i]);
            }
        }
        String prefix = formatted[0];
        if (prefix != null && prefix.startsWith("0")) {
            prefix = prefix.substring(1, prefix.length());
        }
        return new String[] { Constants.GERMAN_PREFIX, prefix, number.toString(), "device" , "unchecked" };
    }
    
    private static String getShortCountry(String country) {
        if (country.startsWith("00")) {
            country = country.substring(2, country.length());
        } else if (country.startsWith("+")) {
            country = country.substring(1, country.length());
        }
        return country;
    }
    
}
