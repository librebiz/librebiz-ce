/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 2, 2004 
 * 
 */
package com.osserp.core.projects;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Parameter;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsActionManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectFcsActionManager extends FcsActionManager {

    /**
     * Provides available startAction names
     * @return startAction names
     */
    List<Parameter> getStartActionNames();

    /**
     * Creates a new flow control action
     * @param typeId
     * @param name
     * @param description
     * @param groupId
     * @param displayEverytime
     * @return all actions of typeId including new created
     * @throws ClientException if name missing or already existing
     */
    List<FcsAction> createAction(Long typeId, String name, String description, Long groupId, boolean displayEverytime) throws ClientException;

    /**
     * Updates the order id with the new order values
     * @param typeId of the flow control actions to reaorder
     * @param id array of flow control action id's
     * @param order array of corresponding order id's
     * @throws ClientException if id.length != order.length
     */
    void updateOrder(Long typeId, Long[] id, Integer[] order)
            throws ClientException;

    /**
     * Changes flow control action's listing position
     * @param flowControlAction
     * @return ordered action list by type
     */
    List<FcsAction> changeEolFlag(FcsAction flowControlAction);

    /**
     * Changes flow control action's listing position
     * @param flowControlAction
     * @param moveUp
     * @return ordered action list by type
     */
    List<FcsAction> changeListPosition(FcsAction flowControlAction, boolean moveUp);
}
