<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productStockQueriesView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="headline">
        <fmt:message key="newQuery"/>
        <c:if test="${!empty view.bean && !view.editMode}"> - <o:out value="${view.bean.name}"/></c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="productStockQueryContent">
            <div class="row">
                <c:choose>
                    <c:when test="${empty view.bean}">
                        <c:import url="_productStockQuerySelection.jsp"/>
                    </c:when>
                    <c:when test="${view.editMode}">
                        <c:import url="_productStockQueryInput.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_productStockQueryList.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
