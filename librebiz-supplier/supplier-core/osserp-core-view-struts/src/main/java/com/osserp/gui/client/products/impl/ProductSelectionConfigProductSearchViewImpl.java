/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 26.04.2011 12:17:55 
 * 
 */
package com.osserp.gui.client.products.impl;

import java.util.Set;

import com.osserp.common.web.ViewName;

import com.osserp.gui.client.products.ProductSelectionConfigProductSearchView;

/**
 * 
 * @author eh <eh@osserp.com>
 */
@ViewName("productSelectionConfigProductSearchView")
public class ProductSelectionConfigProductSearchViewImpl extends AbstractProductSearchView implements ProductSelectionConfigProductSearchView {

    protected ProductSelectionConfigProductSearchViewImpl() {
        super();
    }

    protected ProductSelectionConfigProductSearchViewImpl(Set<String> methods, String defaultMethod) {
        super(methods, defaultMethod, false);
        setKeepViewWhenExistsOnCreate(false);
        setIncludeEolAvailable(false);
    }

}
