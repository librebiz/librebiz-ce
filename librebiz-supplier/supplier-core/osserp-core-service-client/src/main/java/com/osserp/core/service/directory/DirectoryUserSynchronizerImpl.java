/**
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 10, 2012 11:26:25 AM
 * 
 */
package com.osserp.core.service.directory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.User;
import com.osserp.common.directory.DirectoryGroup;
import com.osserp.common.directory.DirectoryManager;
import com.osserp.common.directory.DirectoryUser;
import com.osserp.common.service.Locator;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.PrivateContacts;
import com.osserp.core.dao.Users;
import com.osserp.core.directory.DirectoryUserSynchronizer;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeRole;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.NocSynchronizationSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DirectoryUserSynchronizerImpl extends AbstractDirectoryService implements DirectoryUserSynchronizer {
    private static Logger log = LoggerFactory.getLogger(DirectoryUserSynchronizerImpl.class.getName());

    private Contacts contacts;
    private PrivateContacts privateContacts;
    private Users users;
    private NocSynchronizationSender nocSynchronizationSender;

    public DirectoryUserSynchronizerImpl(
            SystemConfigManager systemConfigManager,
            DirectoryManager directoryManager,
            Locator locator,
            Contacts contacts,
            PrivateContacts privateContacts,
            Users users,
            NocSynchronizationSender nocSynchronizationSender) {
        super(systemConfigManager, directoryManager, locator);
        this.contacts = contacts;
        this.privateContacts = privateContacts;
        this.users = users;
        this.nocSynchronizationSender = nocSynchronizationSender;
    }

    public void synchronizeContact(Contact contact) {
        log.debug("synchronizeContact() invoked [contactId=" + contact.getContactId() + "]");
        List<PrivateContact> contactList = privateContacts.getPrivateContacts(contact.getContactId());
        if (contactList.isEmpty()) {
            // contacts outside addressLists will not be synchronized with directory  
            return;
        }
        if (contact.isContactPerson()) {
            if (contact.getReference() != null) {
                contact.setReferenceContact(contacts.loadContact(contact.getReference()));
            } else {
                log.warn("synchronizeContact() giving up: invalid contactPerson without reference found [contactId="
                        + contact.getContactId() + "]");
                // do not synchronize orphaned 
                // TODO remove from directory?
                return;
            }
        }
        for (int i = 0, j = contactList.size(); i < j; i++) {
            PrivateContact privateContact = contactList.get(i);
            Map mapped = null; // TODO map contact values
            this.synchronizeAddressListEntry(privateContact, mapped);
        }
        log.debug("synchronizeContact: done [contactId=" + contact.getContactId() + "]");
    }

    public void synchronizeEmployee(Employee employee) {
        log.debug("synchronizeEmployee() invoked [id=" + employee.getId()
                + ", contactId=" + employee.getContactId() + "]");

        User user = findUser(employee);
        DirectoryUser directoryUser = null;
        String clientName = null;
        String mapperName = null;

        if (user != null && user.getLdapUid() != null) {
            clientName = getProperty("ldapClientName");
            directoryUser = getDirectoryManager().findUser(user.getLdapUid(), clientName);
            mapperName = getProperty(DirectoryMapper.EMPLOYEE_MAPPER);
        }
        if (user != null && directoryUser != null && mapperName != null) {

            DirectoryMapper mapper = (DirectoryMapper) getService(mapperName);
            Map valueMap = mapper.map(null, employee, null);
            directoryUser = getDirectoryManager().update(directoryUser, valueMap);
            if (nocAvailable() && nocSynchronizationSender != null) {
                nocSynchronizationSender.updateUser(directoryUser.getValues());
            }
            synchronizeGroups(employee, directoryUser);
            log.debug("synchronizeEmployee() done [id=" + employee.getId()
                    + ", contactId=" + employee.getContactId() + "]");

        } else if (log.isDebugEnabled()) {
            log.debug("synchronizeEmployee() nothing to do [id=" + employee.getId()
                    + ", contactId=" + employee.getContactId() + "]");
        }
    }

    public void synchronizePrivateContact(PrivateContact privateContact) {
        log.debug("synchronizePrivateContact: invoked [id=" + privateContact.getId()
                + ", contactId=" + privateContact.getContact().getContactId() + "]");
    }

    protected User findUser(Employee employee) {
        User user = users.find(employee);
        return user;
    }

    private void synchronizeAddressListEntry(PrivateContact privateContact, Map values) {
        log.debug("synchronizeAddressListEntry() invoked [id=" + privateContact.getId()
                + ", contactId=" + privateContact.getContact().getContactId() + "]");
    }

    private void synchronizeGroups(Employee employee, DirectoryUser directoryUser) {
        List<DirectoryGroup> ldapGroups = getDirectoryManager().findGroups(directoryUser);
        DirectoryGroup defaultGroup = getDirectoryManager().getDefaultGroup(getDirectoryClientName());
        for (Iterator<DirectoryGroup> i = ldapGroups.iterator(); i.hasNext();) {
            DirectoryGroup ldapGroup = i.next();
            if (ldapGroup.isOrganizationalUnit()) {
                if (!employee.isRoleAssigned(ldapGroup.getEmployeeGroupId())) {
                    // role was removed so we also have to remove directoryGroup if it's not the default group
                    if (defaultGroup == null || !defaultGroup.getId().equals(ldapGroup.getId())) {
                        getDirectoryManager().removeGroupMember(ldapGroup, directoryUser);
                        i.remove();
                        if (log.isDebugEnabled()) {
                            log.debug("synchronizeGroups() employee removed [id=" + employee.getId()
                                    + ", group=" + ldapGroup.getEmployeeGroupId() + "]");
                        }
                        if (nocAvailable() && nocSynchronizationSender != null) {
                            nocSynchronizationSender.updateGroup(ldapGroup.getValues());
                        }
                    }
                }
            }
        }
        for (int i = 0, j = employee.getRoleConfigs().size(); i < j; i++) {
            EmployeeRoleConfig roleConfig = employee.getRoleConfigs().get(i);
            if (roleConfig.getBranch() != null && !roleConfig.getBranch().getGroups().isEmpty()) {
                for (int k = 0, l = roleConfig.getBranch().getGroups().size(); k < l; k++) {
                    EmployeeGroup group = roleConfig.getBranch().getGroups().get(k);
                    if (!isGroupAssigned(ldapGroups, group.getId())) {
                        DirectoryGroup ldapGroup = getGroup(group.getId());
                        if (ldapGroup != null) {
                            getDirectoryManager().addGroupMember(ldapGroup, directoryUser);
                            if (log.isDebugEnabled()) {
                                log.debug("synchronizeGroups() employee added [id=" + employee.getId()
                                        + ", group=" + ldapGroup.getEmployeeGroupId() + "]");
                            }
                            if (nocAvailable() && nocSynchronizationSender != null) {
                                nocSynchronizationSender.updateGroup(ldapGroup.getValues());
                            }
                        }
                    }
                }
            }
            for (int k = 0, l = roleConfig.getRoles().size(); k < l; k++) {
                EmployeeRole role = roleConfig.getRoles().get(k);
                if (!isGroupAssigned(ldapGroups, role.getGroup().getId())) {
                    DirectoryGroup ldapGroup = getGroup(role.getGroup().getId());
                    if (ldapGroup != null) {
                        getDirectoryManager().addGroupMember(ldapGroup, directoryUser);
                        if (log.isDebugEnabled()) {
                            log.debug("synchronizeGroups() employee added [id=" + employee.getId()
                                    + ", group=" + ldapGroup.getEmployeeGroupId() + "]");
                        }
                        if (nocAvailable() && nocSynchronizationSender != null) {
                            nocSynchronizationSender.updateGroup(ldapGroup.getValues());
                        }
                    }
                }
            }
        }
    }

    private boolean isGroupAssigned(List<DirectoryGroup> ldapGroups, Long groupId) {
        for (Iterator<DirectoryGroup> i = ldapGroups.iterator(); i.hasNext();) {
            DirectoryGroup ldapGroup = i.next();
            if (ldapGroup.isOrganizationalUnit() && ldapGroup.getEmployeeGroupId().equals(groupId)) {
                return true;
            }
        }
        return false;
    }

    private DirectoryGroup getGroup(Long groupId) {
        List<DirectoryGroup> ldapGroups = getDirectoryManager().findGroups(getDirectoryClientName());
        for (Iterator<DirectoryGroup> i = ldapGroups.iterator(); i.hasNext();) {
            DirectoryGroup ldapGroup = i.next();
            if (ldapGroup.isOrganizationalUnit() && ldapGroup.getEmployeeGroupId().equals(groupId)) {
                return ldapGroup;
            }
        }
        return null;
    }
}
