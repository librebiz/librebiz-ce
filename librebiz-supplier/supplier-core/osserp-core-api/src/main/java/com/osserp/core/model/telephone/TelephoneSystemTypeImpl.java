/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 11:02:00 AM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.common.beans.OptionImpl;
import com.osserp.core.telephone.TelephoneSystemType;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneSystemTypeImpl extends OptionImpl implements TelephoneSystemType {

    private String liveServiceUrl;

    protected TelephoneSystemTypeImpl() {
        super();
    }

    public String getLiveServiceUrl() {
        return liveServiceUrl;
    }

    protected void setLiveServiceUrl(String liveServiceUrl) {
        this.liveServiceUrl = liveServiceUrl;
    }
}
