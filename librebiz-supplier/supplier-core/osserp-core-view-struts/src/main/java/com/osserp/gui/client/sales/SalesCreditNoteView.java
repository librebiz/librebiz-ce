/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2007 5:39:57 AM 
 * 
 */
package com.osserp.gui.client.sales;

import java.util.List;

import com.osserp.common.web.ViewName;

import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.sales.SalesInvoice;

import com.osserp.gui.client.records.CreditNoteView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesCreditNoteView")
public interface SalesCreditNoteView extends CreditNoteView {

    /**
     * Provides the view of the related invoice if available in current view context
     * @return invoiceView
     */
    public SalesInvoiceView getInvoiceView();

    /**
     * Provides related invoice if exists either by invoiceView or reference id
     * @return invoice or null if none exists
     */
    public SalesInvoice getInvoice();

    /**
     * Creates a relation to an invoice
     * @param invoiceId
     */
    public void createInvoiceRelation(Long invoiceId);

    /**
     * Provides list of invoices with same customer if creditNote has no reference.
     * @return existing suggestions for relation or empty list if not found
     *  or reference already exists.
     */
    public List<RecordDisplay> getRelationSuggestions();

}
