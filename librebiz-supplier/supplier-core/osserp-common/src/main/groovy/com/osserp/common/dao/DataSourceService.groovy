/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.dao

import javax.sql.DataSource

import groovy.sql.Sql

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.StringUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class DataSourceService implements DataSourceProvider {
    private static Logger logger = LoggerFactory.getLogger(DataSourceService.class.getName())

    protected static final String REFRESH_MATERIALIZED = 'REFRESH MATERIALIZED VIEW' 
    private String url
    private Sql _sql
    DataSource dataSource


    final Sql getSql() {
        if (!_sql) {
            _sql = createSql()
        }
        _sql
    }
    
    protected DataSourceService() {
        super()
    }
    
    protected DataSourceService(DataSource dataSource) {
        this.dataSource = dataSource
    }
    
    protected DataSourceService(String host, String port, String dbname) {
        url = createUrl(host, createPort(port), dbname)
    }
    
    protected DataSourceService(Sql sql) {
        _sql = sql
    }

    protected Sql createSql() {
        Sql.newInstance(dataSource)
    }

    protected String createUrl(String host, Long port, String dbname) {
        if (port) {
            return "jdbc:${host}:${port}/${dbname}".toString()
        }
        return "jdbc:${host}/${dbname}".toString()
    }

    protected Long createPort(String port) {
        Long srvp = 0
        try {
            if (port) {
                srvp = Long.valueOf(port)
            }
        } catch (Exception e) {
        }
        return srvp
    }

    protected Boolean isTableLocked(String table, Long id) {
        def result = 0
        Sql sql = getSql()
        initLockTable(sql, table)
        try {
            sql.query("SELECT count(*) FROM ${table} WHERE id = ? AND locked".toString(), [id]) { resultSet ->
                if (resultSet.next()) {
                    result = resultSet.getInt(1)
                }
            }
        } finally {
            sql.close()
        }
        return result > 0
    }

    protected def lockTable(String table, Long id, Boolean enabled) {
        Sql sql = getSql()
        initLockTable(sql, table)
        boolean exists = false
        sql.query("SELECT count(*) FROM ${table} WHERE id = ?".toString(), [id]) { resultSet ->
            if (resultSet.next()) {
                def result = resultSet.getInt(1)
                exists = result > 0
            }
        }
        if (!exists) {
            sql.executeUpdate("INSERT INTO ${table} (id,locked) VALUES (?,?)".toString(), [id, enabled])
        } else {
            sql.executeUpdate("UPDATE ${table} SET locked = ? WHERE id = ?".toString(), [enabled, id])
        }
    }

    private def initLockTable(Sql sql, String table) {
        try {
            boolean exists = false
            sql.query("SELECT count(*) FROM ${table}".toString()) { resultSet ->
                // ok if this comment is reached
            }
        } catch (Throwable t) {
            logger.debug("initLockTable: trying to create lock table: ${t.message}")
            sql.executeUpdate("CREATE TABLE ${table} (id int, locked boolean)".toString())
            logger.debug('initLockTable: lock table created')
        }
    }

    protected final String getUrl() {
        url
    }

    protected Long getNextId(String table) {
        Long id
        Sql sql = getSql()
        sql.eachRow("SELECT max(id) FROM $table".toString()) { row ->
            id = row[0]
        }
        (id ? id + 1 : null)
    }

    protected Long getNextSequenceValue(String sequence) {
        Long result
        try {
            String query = "SELECT nextval('${sequence}')"
            getSql().eachRow(query) { obj ->
                logger.debug("getNextSequenceValue: found result [obj=${obj}]")
                result = obj.nextval
            }
        } catch (Exception e) {
            logger.error("getNextSequenceValue: failed [sequence=${sequence}, message=${e.message}]", e)
        }
        result
    }
    
    protected boolean restartSequence(String sequence, Long value) {
        boolean result = false
        String stmt = "ALTER SEQUENCE ${sequence} RESTART WITH ${value}"
        try {
            result = sql.executeUpdate(stmt.toString()) > 0
            logger.debug("restartSequence: done [sequence=${sequence}, value=${value}]")
        } catch (Exception e) {
            logger.error("restartSequence: failed [sequence=${sequence}, value=${value}, message=${e.message}]", e)
        }
        result
    }
    
    protected String fetchInsecureInput(String value) {
        try {
            return StringUtil.getSQLInput(value)
        } catch (Exception e) {
            return null
        }
    }

    protected boolean executeDDL(String ddlCommand) {
        boolean result = false
        try {
            result = sql.executeUpdate(ddlCommand) > 0
        } catch (Exception e) {
            logger.error("executeDDL: failed [command=${ddlCommand}, message=${e.message}]", e)
        }
        result
    }

    /**
     * Checks whether a row with given id exists for table
     * @param table
     * @param id
     */
    protected Boolean exists(String table, Long id) {
        Long cnt
        Sql sql = getSql()
        sql.eachRow("SELECT count(*) FROM $table WHERE id = $id".toString()) { row ->
            cnt = row[0]
        }
        (cnt > 0 ? true : false)
    }
    
    
    protected void refreshView(String viewName, Boolean concurrent) {
        String cmd
        try {
            boolean done = false
            if (concurrent) {
                cmd = "${REFRESH_MATERIALIZED} CONCURRENTLY ${viewName}"
                try {
                    getSql().executeUpdate(cmd)
                    done = true
                } catch (Exception e) {
                    // CONCURRENTLY may fail on first run if view 
                    // has not been initialized yet. 
                }
            }
            if (!done) {
                cmd = "${REFRESH_MATERIALIZED} ${viewName}"
                getSql().executeUpdate(cmd)
            }
        } catch (Exception e) {
            logger.error("refreshView: failed [statement=${cmd}, message=${e.message}]", e)
        }
    }
}
