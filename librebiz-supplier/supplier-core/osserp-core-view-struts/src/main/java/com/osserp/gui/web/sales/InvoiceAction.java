/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 7, 2006 4:18:43 PM 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.sales.SalesBillingView;
import com.osserp.gui.client.sales.SalesInvoiceView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class InvoiceAction extends AbstractCorrectionAwareSalesAction {
    private static Logger log = LoggerFactory.getLogger(InvoiceAction.class.getName());

    /**
     * Fetches the selected record id, loads record and forwards to 'display' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward displayDownpayment(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("displayDownpayment() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.fetchId(request);
        try {
            String exitId = RequestUtil.getExitId(request);
            String exitTarget = RequestUtil.getExit(request);
            boolean readonly = RequestUtil.getBoolean(request, "readonly");
            SalesInvoiceView view = (SalesInvoiceView) createView(request);
            view.loadDownpayment(id, exitTarget, exitId, readonly);
            return mapping.findForward(view.getActionTarget());
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("display() failed [id=" + id + ", message=" + getUser(session).getId() + "]");
            }
            return saveErrorAndReturn(request, ErrorCode.RECORD_NOT_AVAILABLE);
        }
    }

    /**
     * Returns from delete and forwards to 'display' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardDelete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardDelete() request from " + getUser(session).getId());
        }
        SalesInvoiceView view = getInvoiceView(session);
        try {
            view.checkDelete();
            return mapping.findForward("delete");
        } catch (PermissionException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
    }

    /**
     * Returns from delete and forwards to 'display' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitDelete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitDelete() request from " + getUser(session).getId());
        }
        SalesInvoiceView view = getInvoiceView(session);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Changes recipient of the invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeRecipient(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeRecipient() request from " + getUser(session).getId());
        }
        SalesInvoiceView view = getInvoiceView(session);
        Long id = RequestUtil.fetchId(request);
        if (id == null) {
            saveError(request, ErrorCode.SELECTION_MISSING);
        } else {
            try {
                view.changeCustomer(id);
            } catch (ClientException e) {
                saveError(request, e.getMessage());
            }
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Changes the id of the invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeRecordId(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeRecordId() request from " + getUser(session).getId());
        }
        SalesInvoiceView view = getInvoiceView(session);
        Form f = createForm(form, request);
        try {
            view.changeRecordNumber(f);
            view.setSetupMode(false);
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables/disables historical status
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward switchHistorical(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("switchHistorical() request from " + getUser(session).getId());
        }
        SalesInvoiceView view = getInvoiceView(session);
        try {
            view.switchHistorical();
        } catch (PermissionException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Returns from delete and forwards to 'display' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward payoutCanceled(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("payoutCanceled() request from " + getUser(session).getId());
        }
        SalesInvoiceView view = getInvoiceView(session);
        view.payoutCancelled();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Restores an accidentally canceled downpayment
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward restoreDownpayment(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("restoreDownpayment() request from " + getUser(session).getId());
        }
        SalesInvoiceView view = getInvoiceView(session);
        try {
            view.restoreDownpayment();
            return reload(mapping, form, request, response);
        } catch (ClientException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
    }

    /**
     * Exits from record display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        SalesInvoiceView view = getInvoiceView(session);
        SalesBillingView billingView = fechBillingView(session);
        BusinessCaseView bcv = fetchBusinessCaseView(session);
        if ((bcv != null && bcv.isFlowControlMode())
                || billingView != null) {
            if (billingView != null) {
                billingView.reload();
            }
            removeView(session);
            if (log.isDebugEnabled()) {
                log.debug("exit() billing found and reload [target=exit]");
            }
            return mapping.findForward(Actions.EXIT);

        } else if (bcv != null && bcv.isSalesContext() && bcv.getForwardTarget() != null) {
            
            removeView(session);
            if (log.isDebugEnabled()) {
                log.debug("exit() found businessCaseView in sales context with forward [target=" + bcv.getForwardTarget() + "]");
            }
            return mapping.findForward(bcv.getForwardTarget());

        } else {
            if (view.isVolumeView()) {
                if (log.isDebugEnabled()) {
                    log.debug("exit() view is volume view [target=volume]");
                }
                return mapping.findForward("volume");
            }
            if (view.getExitTarget() != null) {
                RequestUtil.saveExitId(request, view.getExitId());
                removeView(session);
                if (log.isDebugEnabled()) {
                    log.debug("exit() view provides exit [target=" + view.getExitTarget() + "]");
                }
                return mapping.findForward(view.getExitTarget());
            }
        }
        removeView(session);
        log.warn("exit() neither project nor billing nor exit target found!");
        return mapping.findForward(Actions.INDEX);
    }

    /**
     * Deletes an invoice and forwards to billing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward deleteInvoice(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("deleteInvoice() request from " + getUser(session).getId());
        }
        SalesInvoiceView view = getInvoiceView(session);
        view.delete();
        return exit(mapping, form, request, response);
    }

    protected SalesInvoiceView fetchInvoiceView(HttpSession session) {
        return (SalesInvoiceView) fetchView(session);
    }

    protected SalesInvoiceView getInvoiceView(HttpSession session) throws PermissionException {
        return (SalesInvoiceView) getRecordView(session);
    }

    @Override
    protected Class<SalesInvoiceView> getViewClass() {
        return SalesInvoiceView.class;
    }
}
