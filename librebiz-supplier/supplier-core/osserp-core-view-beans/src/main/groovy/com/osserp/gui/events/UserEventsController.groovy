/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Aug 16, 2010 
 * 
 */
package com.osserp.gui.events

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.PermissionException

/**
 * 
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/events/userEvents/*")
@Controller class UserEventsController extends AbstractEventController {
    private static Logger logger = LoggerFactory.getLogger(UserEventsController.class.getName())

    @Override
    @RequestMapping
    def forward(HttpServletRequest request) {
        logger.debug("forward: invoked [user=${getUserId(request)}]")
        UserEventsView view = getView(request)
        view?.load()
        return defaultPage
    }

    @RequestMapping
    def switchActivationMode(HttpServletRequest request) {
        logger.debug("switchActivationMode: invoked [user=${getUserId(request)}]")
        UserEventsView view = getView(request)
        view?.switchActivationMode()
        return defaultPage
    }

    @RequestMapping
    def filter(HttpServletRequest request) {
        logger.debug("filter: invoked [user=${getUserId(request)}]")
        UserEventsView view = getView(request)
        view?.filter()
        return defaultPage
    }

    @RequestMapping
    def enableCloseMode(HttpServletRequest request) {
        logger.debug("enableCloseMode: invoked [user=${getUserId(request)}]")
        UserEventsView view = getView(request)
        try {
            view.enableCloseMode()
        } catch (PermissionException t) {
            if (logger.debugEnabled) {
                logger.debug("enableCloseMode: caught exception [message=${t.message}]")
            }
            request.getSession().setAttribute('error', t.message)
        }
        return defaultPage
    }

    @RequestMapping
    def disableCloseMode(HttpServletRequest request) {
        logger.debug("disableCloseMode: invoked [user=${getUserId(request)}]")
        UserEventsView view = getView(request)
        view.disableCloseMode()
        return defaultPage
    }

    @RequestMapping
    def selectUser(HttpServletRequest request) {
        logger.debug("selectUser: invoked [user=${getUserId(request)}]")
        UserEventsView view = getView(request)
        view.selectUser()
        return defaultPage
    }
}
