/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 17, 2011 12:49:53 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.Parameter;
import com.osserp.common.beans.ParameterImpl;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.RecordQueries;
import com.osserp.core.finance.PaymentDisplay;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordSearchRequest;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordQueriesDao extends AbstractDao implements RecordQueries {
    private static Logger log = LoggerFactory.getLogger(RecordQueriesDao.class.getName());
    
    private static final int DEFAULT_FETCH_SIZE = 250;
    
    private List<Parameter> columns;
    protected OptionsCache options;

    public RecordQueriesDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory, OptionsCache options) {
        super(jdbcTemplate, tables, sessionFactory);
        this.options = options;
        columns = new ArrayList<>();
        columns.add(new ParameterImpl("recordNumber", "id")); 
        columns.add(new ParameterImpl("contact", "lastname")); 
        columns.add(new ParameterImpl("productNo", "product_id")); 
        columns.add(new ParameterImpl("productName", "product_name")); 
    }
    
    public List<Parameter> getColumns() {
        return columns;
    }

    public List<RecordDisplay> find(RecordSearchRequest request) {
        assert request != null;
        List<RecordDisplay> result = new ArrayList<>();
        String context = isEmpty(request.getContextName()) ? "record" : request.getContextName();
        String orderByClause = " ORDER BY id";
        StringBuilder sql = new StringBuilder("SELECT ");
        sql
                .append(RecordDisplayRowMapper.RECORD_VALUES)
                .append(" FROM v_")
                .append(context)
                .append("_query");
        
        String[] columnRestriction = createColumnRestriction(request);
        if (columnRestriction[0] != null) {
            sql.append(" WHERE ").append(columnRestriction[0]);
            if (columnRestriction[1] != null) {
                orderByClause = columnRestriction[1];
            }
        } else if (!isEmpty(request.getPattern())) {
            sql.append(" WHERE ").append(bindColumnToRegExPattern("id", request.getPattern(), false));
            sql.append(" OR ").append(bindColumnToRegExPattern("reference_id", request.getPattern(), false));
            sql.append(" OR ").append(bindColumnToRegExPattern("lastname", request.getPattern(), false));
            sql.append(" OR ").append(bindColumnToRegExPattern("product_id", request.getPattern(), false));
            sql.append(" OR ").append(bindColumnToRegExPattern("product_name", request.getPattern(), false));
            orderByClause = " ORDER BY lastname";
        }
        if (!isEmpty(request.getOrderByColumn())) {
            sql.append(" ORDER BY ").append(request.getOrderByColumn());
        } else {
            sql.append(orderByClause);
        }
        if (!request.isIgnoreFetchSize()) {
            int recordFetchSize = request.getFetchSize() > 0 ? request.getFetchSize() : getRecordSearchFetchSize();
            if (recordFetchSize > 0) {
                sql.append(" LIMIT ").append(recordFetchSize);
            } else {
                sql.append(" LIMIT ").append(DEFAULT_FETCH_SIZE);
            }
        }
        String createdQuery = sql.toString();
        try {
            result = RecordDisplayRowMapper.createDisplayList(options, 
                (List<Map<String, Object>>) jdbcTemplate.query(createdQuery, RecordDisplayRowMapper.getReader()));
            if (log.isDebugEnabled()) {
                log.debug("find() done [query=" + sql.toString() + ", found=" + result.size() + "]");
            }
        } catch (Exception e) {
            log.warn("find() failed to execute query [sql=" + createdQuery + ", message=" + e.getMessage() + "]");
        }
        return result;
    }

    public List<PaymentDisplay> findPayments(Long company) {
        StringBuilder query = new StringBuilder("SELECT ");
        query
                .append(PaymentDisplayRowMapper.RECORD_VALUES)
                .append(" FROM v_record_payments");
        if (!isEmpty(company)) {
            query.append(" WHERE company_id = ").append(company).append(" ORDER BY paid_on");
        }
        List<Map<String, Object>> result = (List<Map<String, Object>>) jdbcTemplate.query(
                query.toString(),
                PaymentDisplayRowMapper.getReader());
        List<PaymentDisplay> list = PaymentDisplayRowMapper.createDisplayList(RecordProcs.getBillingTypes(options), result);
        return CollectionUtil.reverse(list);
    }
    
    private String[] createColumnRestriction(RecordSearchRequest request) {
        String[] result = new String[2];
        StringBuilder sql = new StringBuilder();
        if ("id".equals(request.getColumnKey())) {
            sql.append(bindColumnToRegExPattern("id", request.getPattern(), request.isStartsWith()));
        } else if ("lastname".equals(request.getColumnKey())) {
            sql.append(bindColumnToRegExPattern("lastname", request.getPattern(), request.isStartsWith()));
            result[1] = " ORDER BY lastname";
        } else if ("product_id".equals(request.getColumnKey())) {
            sql.append(bindColumnToRegExPattern("product_id", request.getPattern(), request.isStartsWith()));
            result[1] = " ORDER BY product_id";
        } else if ("product_name".equals(request.getColumnKey())) {
            sql.append(bindColumnToRegExPattern("product_name", request.getPattern(), request.isStartsWith()));
            result[1] = " ORDER BY product_name";
        }
        if ("id".equals(request.getColumnKey())) {
            sql.append(" OR ").append(bindColumnToRegExPattern("reference_id", request.getPattern(), request.isStartsWith()));
        }
        String restriction = sql.toString(); 
        result[0] = restriction.length() == 0 ? null : restriction;
        return result;
    }

    private int getRecordSearchFetchSize() {
        Long val = getPropertyId("recordFetchSize");
        return isEmpty(val) ? 0 : val.intValue();
    }
}
