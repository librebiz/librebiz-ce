/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.junit.Test

import static org.junit.Assert.*

import com.osserp.TestConfig
import com.osserp.common.directory.ldap.impl.InternetdomainsImpl
import com.osserp.common.directory.ldap.LdapConfig

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class IInternetdomainsImpl {
    private static Logger logger = LoggerFactory.getLogger(IInternetdomainsImpl.class.getName())

    @Test
    void initInternetdomains() {
        InternetdomainsImpl impl = new InternetdomainsImpl(config)
        assertTrue(impl.config.domainAvailable)
    }

    @Test
    void createDelete() {
        InternetdomainsImpl impl = new InternetdomainsImpl(config)
        if (impl.config.domainAvailable) {
            Map obj = impl.create('osserp.de', 'mail.osserp.org', TestConfig.TESTCLIENT)
            assertTrue(obj != null)
            Map created = impl.find('osserp.de', TestConfig.TESTCLIENT)
            assertTrue(created != null)
            impl.delete(created)
            obj = impl.find('osserp.de', TestConfig.TESTCLIENT)
            assertTrue(!obj)
        }
    }

    @Test
    void updateExpiration() {
        InternetdomainsImpl impl = new InternetdomainsImpl(config)
        if (impl.config.domainAvailable) {
            Map obj = impl.create('osserp.de', 'smtp:mail.osserp.org', TestConfig.TESTCLIENT)
            assertTrue(obj != null)
            Map created = impl.find('osserp.de', TestConfig.TESTCLIENT)
            assertTrue(created != null)
            assertTrue(!created.domainexpires)
            Map updated = impl.updateExpiration(created, '2020-12-30')
            assertTrue(updated.domainexpires == '2020-12-30')
            impl.delete(created)
            obj = impl.find('osserp.de', TestConfig.TESTCLIENT)
            assertTrue(!obj)
        }
    }

    @Test
    void updateMailhosts() {
        InternetdomainsImpl impl = new InternetdomainsImpl(config)
        if (impl.config.domainAvailable) {
            Map obj = impl.create('osserp.de', 'smtp:mail.osserp.org', TestConfig.TESTCLIENT)
            assertTrue(obj != null)
            Map created = impl.find('osserp.de', TestConfig.TESTCLIENT)
            assertTrue(created != null)
            assertTrue(created.mailhost == 'smtp:mail.osserp.org')
            Map updated = impl.updateMailhosts(created, 'smtp:mx.osserp.org', 'smtp:relay.osserp.org')
            assertTrue(updated.mailhost == 'smtp:mx.osserp.org')
            assertTrue(updated.mailrelay == 'smtp:relay.osserp.org')
            impl.delete(created)
            obj = impl.find('osserp.de', TestConfig.TESTCLIENT)
            assertTrue(!obj)
        }
    }

    @Test
    void find() {
        InternetdomainsImpl impl = new InternetdomainsImpl(config)
        if (impl.config.domainAvailable) {
            Map obj = impl.find('osserp.org', TestConfig.TESTCLIENT)
            assertTrue(obj != null)
        }
    }

    @Test
    void findAll() {
        InternetdomainsImpl impl = new InternetdomainsImpl(config)
        if (impl.config.domainAvailable) {
            List<Map> list = impl.findAll(TestConfig.TESTCLIENT)
            assertTrue(!list.empty)
        }
    }


    private LdapConfig getConfig() {
        new LdapConfig(TestConfig.PROPERTY_FILE)
    }
}

