/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16.12.2012 
 * 
 */
package com.osserp.core.api.defaults

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.StringUtil
import com.osserp.api.WSBusinessCase
import com.osserp.core.BusinessCase
import com.osserp.core.api.CoreBusinessCaseService
import com.osserp.core.requests.Request

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CoreBusinessCaseServiceDefault implements CoreBusinessCaseService {
    private static Logger logger = LoggerFactory.getLogger(CoreBusinessCaseServiceDefault.class.getName())

    private CoreBusinessCaseServiceProcessor businessCaseServiceProcessor

    protected CoreBusinessCaseServiceDefault(CoreBusinessCaseServiceProcessor coreBusinessCaseServiceProcessor) {
        super()
        businessCaseServiceProcessor = coreBusinessCaseServiceProcessor
    }

    /* (non-Javadoc)
     * @see com.osserp.core.api.CoreBusinessCaseService#createOrUpdate(com.osserp.api.WSBusinessCase)
     */
    public BusinessCase createOrUpdate(WSBusinessCase wsBusinessCase) {
        if (!wsBusinessCase) {
            logger.warn('createOrUpdate: giving up: wsBusinessCase is null!')
            return null
        } 
        
        logBusinessCase('createOrUpdate', wsBusinessCase)
        
        if (!wsBusinessCase.contact) {
            logger.warn("createOrUpdate: giving up: wsBusinessCase.contact is null [id=${wsBusinessCase.primaryKey}]")
            return null
        }
        BusinessCase result = businessCaseServiceProcessor.createOrUpdate(wsBusinessCase, false)
        logger.debug("createOrUpdate: done [id=${result?.primaryKey}, customer=${result?.customer?.id}]")
        return result 
    }

    protected void logBusinessCase(String method, WSBusinessCase wsBusinessCase) {
        logger.debug("${method}: businessCase:\n${StringUtil.toString(wsBusinessCase)}\n")
        if (wsBusinessCase.contact) {
            logger.debug("${method}: embedded contact:\n${StringUtil.toString(wsBusinessCase.contact)}\n")
        }
        if (wsBusinessCase.items) {
            wsBusinessCase.items.each {
                logger.debug("${method}: embedded item: ${StringUtil.toString(it)}")
            }
        }
    }
}
