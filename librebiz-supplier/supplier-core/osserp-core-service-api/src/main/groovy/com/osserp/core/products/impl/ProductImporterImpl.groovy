/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 1, 2017 
 * 
 */
package com.osserp.core.products.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ds.ImportException
import com.osserp.common.ods.OdsImporterService

import com.osserp.core.api.ProductImportProcessor
import com.osserp.core.api.ProductImportVO
import com.osserp.core.products.Product
import com.osserp.core.products.ProductImporter

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ProductImporterImpl extends OdsImporterService implements ProductImporter {
    private static Logger logger = LoggerFactory.getLogger(ProductImporterImpl.class.getName())
    
    ProductImportProcessor productImportProcessor
    ProductImportMapper productMapper = new ProductImportMapper()
    
    ProductImporterImpl() throws ImportException {
        super(true)
    }

    @Override
    protected Map<String, String> initRow(Map<String, String> row) {
        if (ProductImportMapper.isMappable(row)) {
            return row
        }
        return null
    }
    
    /**
     * Inserts or updates a contact
     * @param row mapped contact data
     * @return true if import row was successful
     */
    @Override
    protected boolean importRow(Map<String, String> row) {
        boolean result = false // product insert or update performed
        ProductImportVO obj = productMapper.map(row)
        String name = obj.getName()
        logger.debug("importRow: trying to import mapped row [name=${name}, matchcode=${obj.getMatchcode()}, productId=${obj.getPrimaryKey()}]")
        try {
            Product product = productImportProcessor.createOrUpdate(obj, performActions)
            if (product) {
                logger.debug("importRow: done [productId=${product.productId}, name=${product.name}]")
                result = true
            }
        } catch (Exception e) {
            if (e.message == 'error.name.exists') {
                logger.error("importRow: failed on existing name ${name}", e)
            } else {
                logger.error("importRow: failed [message=${e.message}]", e)
            }
        }
        return result
    }
}
