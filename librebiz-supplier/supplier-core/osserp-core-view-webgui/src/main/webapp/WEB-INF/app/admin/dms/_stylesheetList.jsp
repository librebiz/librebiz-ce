<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.templateView}"/>
<c:set var="dmsUrl" value="${requestScope.templateDocumentUrl}"/>
<c:set var="deletePermissions" value="${requestScope.templateDeletePermissions}"/>
<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="docListNote"><fmt:message key="description"/></th>
                    <th class="docListName"><fmt:message key="printTemplate" /></th>
                    <th class="action"><fmt:message key="action" /></th>
                </tr>
            </thead>
            <tbody>
                <c:if test="${empty view.printTemplates}">
                    <tr><td colspan="3"><fmt:message key="thereIsNoDocumentYet" /></td></tr>
                    </c:if>
                    <c:forEach items="${view.printTemplates}" var="doc">
                    <tr>
                        <td class="docListNote"><o:out value="${doc.description}"/></td>
                        <td class="docListName"><o:doc obj="${doc}"><o:out value="${doc.fileName}"/></o:doc></td>
                            <td class="action">
                            <v:link url="${dmsUrl}/select?id=${doc.id}"><o:img name="writeIcon"/></v:link>
                            <c:if test="${view.documentDeleteSupported}">
                                <o:permission role="${deletePermissions}" info="permissionDocumentsDelete">
                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="${dmsUrl}/delete?id=${doc.id}" />');" title="<fmt:message key="deleteDocument"/>">
                                        <o:img name="deleteIcon"/>
                                    </a>
                                </o:permission>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                <c:if test="${!empty view.textTemplates}">
                    <tr>
                        <th class="docListNote"><fmt:message key="description"/></th>
                        <th class="docListName"><fmt:message key="textTemplate" /></th>
                        <th class="action"><fmt:message key="action" /></th>
                    </tr>
                    <c:forEach items="${view.textTemplates}" var="doc">
                        <c:if test="${!doc.emailText}">
                            <tr>
                                <td class="docListNote"><o:out value="${doc.description}"/></td>
                                <td class="docListName"><o:doc obj="${doc}"><o:out value="${doc.fileName}"/></o:doc></td>
                                    <td class="action">
                                    <v:link url="${dmsUrl}/select?id=${doc.id}"><o:img name="writeIcon"/></v:link>
                                    <c:if test="${view.documentDeleteSupported}">
                                        <o:permission role="${deletePermissions}" info="permissionDocumentsDelete">
                                            <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="${dmsUrl}/delete?id=${doc.id}" />');" title="<fmt:message key="deleteDocument"/>">
                                                <o:img name="deleteIcon"/>
                                            </a>
                                        </o:permission>
                                    </c:if>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </c:if>
            </tbody>
        </table>
    </div>
</div>
