/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 02-May-2010 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.products.Product;
import com.osserp.core.sales.SalesOrder;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class SalesOrderDeliveryDateChangedInfoImpl extends AbstractDeliveryDateChangedInfo {

    /**
     * Constructor required for object serialization
     */
    protected SalesOrderDeliveryDateChangedInfoImpl() {
        super();
    }

    /**
     * Default constructor to create new delivery date changed info
     * @param user
     * @param order
     * @param product
     * @param quantity
     * @param stockId
     * @param previousDate
     * @param newDate
     * @param note
     */
    public SalesOrderDeliveryDateChangedInfoImpl(
            Employee user,
            SalesOrder order,
            Product product,
            Double quantity,
            Long stockId,
            Date previousDate,
            Date newDate,
            String note) {
        super(user, order, product, quantity, stockId, previousDate, newDate, note);
    }

    public SalesOrderDeliveryDateChangedInfoImpl(DeliveryDateChangedInfo other) {
        super(other);
    }
}
