
/**
 *
 * Copyright (C) 2013, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 1, 2016 
 * 
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsReference

import com.osserp.core.system.BranchOfficeManager
import com.osserp.core.system.SystemCompany
import com.osserp.core.system.SystemCompanyManager

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class AbstractCompanyAdminView extends AbstractCompanyAwareAdminView {
    private static Logger logger = LoggerFactory.getLogger(AbstractCompanyAdminView.class.getName())

    protected loadLogo = false
    protected loadRegistration = false

    protected abstract void createStock();

    protected AbstractCompanyAdminView() {
        super()
    }

    protected void enableLogoPreloading() {
        loadLogo = true
    }

    protected void enableRegistrationPreloading() {
        loadLogo = true
    }

    protected void load(Long id) {
        SystemCompany company
        if (id) {
            company = systemCompanyManager.find(id)
        }
        if (!company) {
            company = selectedCompany
        }
        loadDependencies(company, true)
    }

    protected void loadDependencies(SystemCompany comp, boolean assignBean) {
        logger.debug("load: invoked [id=${comp?.id}, assignBean=${assignBean}]")
        if (comp) {
            branchOfficeList = branchOfficeManager.findByCompany(comp.id)
            logger.debug("load: branchs initialized [count=${branchOfficeList?.size()}]")
            if (branchOfficeList.size() == 1) {
                selectedBranch = branchOfficeList.get(0) 
            }
            if (loadLogo) {
                loadCompanyLogo(comp.id)
            }
            if (loadRegistration) {
                try {
                    List<DmsDocument> regs = dmsManager.findByReference(new DmsReference(SystemCompany.COMPANY_REGISTRATION, comp.id))
                    if (regs) {
                        registration = regs.get(0)
                    }
                } catch (Exception e) {
                    logger.warn('load: unable to locate dms on attempt to load registration [message=${e.message}]')
                }
            }
            if (assignBean) {
                bean = comp
            }
            if (branchOfficeList.size() > 1) {
                multipleBranchsAvailable = true
            } 
        }
    }

    boolean isStocktakingSupportEnabled() {
        return systemConfigManager.stocktakingSupportEnabled
    }

    void toggleStocktakingSupport() {
        systemConfigManager.toggleStocktakingSupport()
    }

    @Override
    void reset() {
        registration = null
        companyLogo = null
        bean = null
    }

}
