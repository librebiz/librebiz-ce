<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><o:out value="${view.product.productId}"/> - <o:out value="${view.product.name}"/></h4>
        </div>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productName"><fmt:message key="name" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text name="name" styleClass="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productDescription"><fmt:message key="description" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:textarea styleClass="form-control" name="description" rows="5" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="manufacturer"><fmt:message key="manufacturer" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <oc:select name="manufacturer" value="${view.product.manufacturer}" promptKey="setupOption" options="productManufacturers" styleClass="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productMatchcode"><fmt:message key="matchcode" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text name="matchcode" styleClass="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="descriptionShort"><fmt:message key="descriptionShort" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:textarea styleClass="form-control" name="descriptionShort" rows="2" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="marketingText"><fmt:message key="marketingText" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:textarea styleClass="form-control" name="marketingText" rows="4" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productPurchaseText"><fmt:message key="purchaseText" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:textarea styleClass="form-control" name="purchaseText" rows="1" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="gtin13"><fmt:message key="gtin13" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text styleClass="form-control" name="gtin13" />
                </div>
            </div>
        </div>
        
        <c:if test="${view.product.group.gtin14Available}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="gtin14"><fmt:message key="gtin14" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text styleClass="form-control" name="gtin14" />
                    </div>
                </div>
            </div>
        </c:if>
        
        <c:if test="${view.product.group.gtin8Available}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="gtin8"><fmt:message key="gtin8" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text styleClass="form-control" name="gtin8" />
                    </div>
                </div>
            </div>
        </c:if>

    </div>
</div>
