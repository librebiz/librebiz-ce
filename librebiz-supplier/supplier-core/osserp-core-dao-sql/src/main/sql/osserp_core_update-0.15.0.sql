SET search_path = osserp, pg_catalog;

CREATE OR REPLACE VIEW osserp.v_request_search AS
 SELECT rl.salutation,
    rl.firstname,
    rl.lastname,
    rl.contact_type,
    rl.plan_id,
    rl.type_id,
    rl.customer_id,
    rl.name,
    rl.sales_id,
    rl.manager_id,
    rl.branch_id,
    rl.sales_talk_date,
    rl.presentation_date,
    rl.status_id,
    rl.created,
    rl.created_by,
    rl.city,
    rl.origin_id,
    rl.order_probability,
    rl.order_probability_date,
    rl.plant_capacity,
    rl.last_note_date,
    rl.company_id,
    rl.stopped,
    rl.accounting_ref,
    rl.shipping_id,
    rl.payment_id,
    rl.delivery_date,
    ((rl.plan_id || ' '::text) || rl.name || ' ' || c.idx) AS idx
   FROM osserp.v_request_list rl,
    osserp.v_customer_search c
  WHERE (rl.customer_id = c.id)
  ORDER BY rl.name;

CREATE OR REPLACE VIEW osserp.v_sales_search AS
 SELECT sl.salutation,
    sl.firstname,
    sl.lastname,
    sl.contact_type,
    sl.id,
    sl.plan_id,
    sl.type_id,
    sl.customer_id,
    sl.status,
    sl.created,
    sl.created_by,
    sl.manager_id,
    sl.manager_sub_id,
    sl.sales_id,
    sl.branch_id,
    sl.name,
    sl.city,
    sl.plant_capacity,
    sl.stopped,
    sl.cancelled,
    sl.installer_id,
    sl.installation_days,
    sl.installation_date,
    sl.delivery_date,
    sl.confirmation_date,
    sl.origin_id,
    sl.origin_type_id,
    sl.company_id,
    sl.request_created,
    sl.street,
    sl.zipcode,
    sl.accounting_ref,
    sl.shipping_id,
    sl.payment_id,
    ((sl.id || ' '::text) || sl.name || ' ' || c.idx) AS idx
   FROM osserp.v_sales_list sl,
    osserp.v_customer_search c
  WHERE (sl.customer_id = c.id)
  ORDER BY sl.name;

INSERT INTO system_properties VALUES (215, 'mailMessageSizeLimit', '10240000', 'java.lang.Integer', true, false, null, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 215, true);

ALTER TABLE record_types ADD COLUMN add_product_datasheet boolean default false;
ALTER TABLE record_types ADD COLUMN add_right_to_cancel boolean default false;
UPDATE system_properties SET name = 'recordTermsAndConditionsAvailable' WHERE name = 'termsAndConditionsAvailable';
UPDATE system_properties SET displayable = true WHERE name = 'recordTermsAndConditionsAvailable';

ALTER TABLE letter_templates ADD COLUMN ignore_salutation boolean default false;
ALTER TABLE letter_types ADD COLUMN ignore_salutation boolean default false;
ALTER TABLE letters ADD COLUMN ignore_salutation boolean default false;

ALTER TABLE letter_templates ADD COLUMN content_keep_together text;
ALTER TABLE letter_types ADD COLUMN content_keep_together text;
ALTER TABLE letters ADD COLUMN content_keep_together text;

UPDATE system_properties SET order_id = order_id + 1 WHERE context_name = 'documentConfig' AND order_id >= 13 AND order_id < 17;
UPDATE system_properties SET order_id = 13, pvalue = '2.2cm' WHERE name = 'documentPrintHeaderAddressSpace';

INSERT INTO system_properties VALUES (216, 'documentPrintHeaderContentSpace', '0.1cm', 'java.lang.String', true, false, 'documentConfig', false, 19);
SELECT pg_catalog.setval('system_properties_id_seq', 216, true);
UPDATE system_properties SET order_id = 20 WHERE name = 'documentPrintHeaderContentSpaceLetter';
UPDATE system_properties SET pvalue = '3.0cm' WHERE name = 'documentPrintHeaderHeightTotal';
UPDATE system_properties SET order_id = order_id + 1 WHERE context_name = 'documentConfig' AND order_id >= 2 AND order_id < 30;
INSERT INTO system_properties VALUES (217, 'documentPrintFontSizeHeaderAddress', '10pt', 'java.lang.String', true, false, 'documentConfig', false, 2);
UPDATE system_properties SET order_id = order_id + 1 WHERE context_name = 'documentConfig' AND order_id >= 3 AND order_id < 30;
INSERT INTO system_properties VALUES (218, 'documentPrintFontSizeHeaderCompany', '8pt', 'java.lang.String', true, false, 'documentConfig', false, 3);
SELECT pg_catalog.setval('system_properties_id_seq', 218, true);

UPDATE documents SET file_size = 8243 WHERE file_name = 'xsl/documents/letter.vm';
UPDATE documents SET file_size = 13151 WHERE file_name = 'xsl/documents/project_tracking.vm';
UPDATE documents SET file_size = 1633 WHERE file_name = 'xsl/documents/shared/customer_address.vm';
UPDATE documents SET file_size = 35793 WHERE file_name = 'xsl/records/xsl_record_correction.vm';
UPDATE documents SET file_size = 8468 WHERE file_name = 'xsl/records/xsl_rtp_102.vm';
UPDATE documents SET file_size = 3301 WHERE file_name = 'xsl/records/xsl_rtp_105.vm';
UPDATE documents SET file_size = 4391 WHERE file_name = 'xsl/records/xsl_rtp_10.vm';
UPDATE documents SET file_size = 11640 WHERE file_name = 'xsl/records/xsl_rtp_1.vm';
UPDATE documents SET file_size = 8497 WHERE file_name = 'xsl/records/xsl_rtp_2.vm';
UPDATE documents SET file_size = 9381 WHERE file_name = 'xsl/records/xsl_rtp_3.vm';
UPDATE documents SET file_size = 35896 WHERE file_name = 'xsl/records/xsl_rtp_4.vm';
UPDATE documents SET file_size = 39609 WHERE file_name = 'xsl/records/xsl_rtp_5.vm';
UPDATE documents SET file_size = 6804 WHERE file_name = 'xsl/records/xsl_rtp_9.vm';
UPDATE documents SET file_size = 535 WHERE file_name = 'xsl/records/shared/cover_letter.vm';
UPDATE documents SET file_size = 2551 WHERE file_name = 'xsl/records/shared/item_listing_item.vm';
UPDATE documents SET file_size = 2065 WHERE file_name = 'xsl/records/shared/delivery_address.vm';
UPDATE documents SET file_size = 1713 WHERE file_name = 'xsl/records/shared/record_customer_address.vm';
UPDATE documents SET file_size = 1024 WHERE file_name = 'xsl/records/shared/supplier_address.vm';
UPDATE documents SET file_size = 6559 WHERE file_name = 'xsl/shared/footer.vm';
UPDATE documents SET file_size = 4746 WHERE file_name = 'xsl/shared/header.vm';
UPDATE documents SET file_size = 3429 WHERE file_name = 'xsl/shared/letter_content.vm';

ALTER TABLE customers ADD COLUMN invoice_by text;
ALTER TABLE customers ADD COLUMN invoice_email text;

CREATE TABLE sales_delivery_note_types (
    id bigint NOT NULL,
    name text,
    shortkey text,
    order_id integer DEFAULT 0,
    stock_affecting boolean DEFAULT false,
    custom_content boolean DEFAULT false
);

INSERT INTO sales_delivery_note_types VALUES (1, 'Standard', 'ST', 0, true, false);
INSERT INTO sales_delivery_note_types VALUES (2, 'Kleinteile', 'KT', 1, true, true);
ALTER TABLE sales_delivery_notes ADD COLUMN book_type_id bigint DEFAULT 1 REFERENCES sales_delivery_notes (id);
UPDATE sales_delivery_notes SET book_type_id = 1;

ALTER TABLE project_suppliers ADD COLUMN status_id bigint DEFAULT 0;

CREATE OR REPLACE FUNCTION osserp.get_reduced_tax() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
    i_result    NUMERIC := 0;
    n_tax       NUMERIC := 0;
BEGIN
    SELECT rate INTO n_tax FROM osserp.record_tax_rates WHERE is_default = 1 AND is_reduced = 1;
    IF FOUND THEN
        i_result := n_tax;
    END IF;
    RETURN i_result;
END;
$$;

CREATE OR REPLACE FUNCTION osserp.get_base_tax() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
    i_result    NUMERIC := 0;
    n_tax       NUMERIC := 0;
BEGIN
    SELECT rate INTO n_tax FROM osserp.record_tax_rates WHERE is_default = 1 AND is_reduced = 0;
    IF FOUND THEN
        i_result := n_tax;
    END IF;
    RETURN i_result;
END;
$$;

UPDATE system_properties SET pvalue = '0.15.0', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
