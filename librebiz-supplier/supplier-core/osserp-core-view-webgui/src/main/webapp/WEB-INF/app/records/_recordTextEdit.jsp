<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="letterView" />
<v:form name="letterForm" url="${view.baseLink}/save">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="settings" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="subject"><fmt:message key="subject" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="subject" value="${view.bean.subject}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="salutation"><fmt:message key="salutation" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="salutation" value="${view.bean.salutation}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="greetings"><fmt:message key="greetingsLabel" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="greetings" value="${view.bean.greetings}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="signatureLeft"><fmt:message key="signatureLeft" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:select styleClass="form-control" name="signatureLeft" options="employees" value="${view.bean.signatureLeft}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="signatureRight"><fmt:message key="signatureRight" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:select styleClass="form-control" name="signatureRight" options="employees" value="${view.bean.signatureRight}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="embeddedSpaceAfter"><fmt:message key="letterEmbeddedSpaceAfterLabel" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="embeddedSpaceAfter" value="${view.bean.embeddedSpaceAfter}" title="title.input.decimal.hint.cm" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="ignoreSubject"><fmt:message key="options" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreSubject"> <v:checkbox name="ignoreSubject" value="${view.bean.ignoreSubject}" /> <fmt:message key="ignoreSubjectLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreSalutation"> <v:checkbox name="ignoreSalutation" value="${view.bean.ignoreSalutation}" /> <fmt:message key="ignoreSalutationLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="ignoreGreetings"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreGreetings"> <v:checkbox name="ignoreGreetings" value="${view.bean.ignoreGreetings}" /> <fmt:message key="ignoreGreetingsLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="options"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="contentKeepTogether">
                                    <c:choose>
                                        <c:when test="${empty view.bean.contentKeepTogether or view.bean.contentKeepTogether == 'auto'}">
                                            <v:checkbox name="contentKeepTogetherAlways" />
                                            <span><fmt:message key="contentKeepTogetherAlways" /></span>
                                        </c:when>
                                        <c:otherwise>
                                            <v:checkbox name="contentKeepTogetherAlways" value="true" />
                                            <span><fmt:message key="contentKeepTogetherAlways" /></span>
                                        </c:otherwise>
                                    </c:choose>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <c:forEach var="info" items="${view.bean.infos}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="${info.name}"> </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="${info.name}"> <v:checkbox name="infos" value="${info.ignore}" printvalue="${info.name}" reverse="true" /> <fmt:message key="${info.name}" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>

                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/disableEditMode" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</v:form>
