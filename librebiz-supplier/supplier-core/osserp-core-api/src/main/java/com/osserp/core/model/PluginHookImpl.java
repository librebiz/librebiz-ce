/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2020 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.gui.MenuLink;
import com.osserp.common.gui.model.MenuLinkImpl;
import com.osserp.common.util.StringUtil;

import com.osserp.core.PluginHook;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PluginHookImpl extends AbstractOption implements PluginHook {

    private String value;
    private String type;
    private String hookType;
    private String iconName;
    private String paramNames;
    private String permissions;
    private boolean userRequired;
    private Long parentId;
    private int orderId = 0;

    protected PluginHookImpl() {
        super();
    }

    @Override
    public Object clone() {
        return new PluginHookImpl(this);
    }

    protected PluginHookImpl(PluginHook other) {
        super(other);
        this.hookType = other.getHookType();
        this.iconName = other.getIconName();
        this.paramNames = other.getParamNames();
        this.permissions = other.getPermissions();
        this.userRequired = other.isUserRequired();
        this.parentId = other.getParentId();
        this.orderId = other.getOrderId();
    }

    public String getHookType() {
        return hookType;
    }

    public void setHookType(String hookType) {
        this.hookType = hookType;
    }

    public String getIconName() {
        return iconName;
    }

    protected void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getParamNames() {
        return paramNames;
    }

    public void setParamNames(String paramNames) {
        this.paramNames = paramNames;
    }

    public String[] getParameters() {
        if (isNotSet(paramNames)) {
            return null;
        }
        return StringUtil.getTokenArray(paramNames);
    }

    public String getPermissions() {
        return permissions;
    }

    protected void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public boolean isUserRequired() {
        return userRequired;
    }

    protected void setUserRequired(boolean userRequired) {
        this.userRequired = userRequired;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    protected void setType(String type) {
        this.type = type;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public boolean isPage() {
        return ("jsp".equals(hookType)
                && value != null && value.length() > 0
                && getResourceKey() != null && getResourceKey().length() > 0);
    }

    public boolean isUrl() {
        return ("url".equals(hookType)
                && value != null && value.length() > 0
                && getResourceKey() != null && getResourceKey().length() > 0);
    }

    public MenuLink getLink() {
        MenuLink link = null;
        if (isUrl()) {
            link = new MenuLinkImpl(
                    getOrderId(),
                    getValue(),
                    getName(),
                    getPermissions());
        }
        return link;
    }
}
