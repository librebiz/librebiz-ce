/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 25, 2006 11:08:08 AM 
 * 
 */
package com.osserp.gui.client.records.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;

import com.osserp.core.Item;
import com.osserp.core.Options;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteManager;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Order;

import com.osserp.gui.client.records.DeliveryNoteView;
import com.osserp.gui.client.records.OrderView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDeliveryNoteView extends AbstractRecordView implements DeliveryNoteView {
    
    private static Logger log = LoggerFactory.getLogger(AbstractDeliveryNoteView.class.getName());
    private String orderViewKey = null;
    private OrderView orderView = null;
    private boolean sales = false;
    private boolean ignoreNotExistingSerialException = false;
    private String ignoreNotExistingSerialPermission = null;
    private boolean displayIgnoreNotExistingSerialDialog = false;
    private Long lastSerialInputItem = null;
    private String lastSerialInput = null;

    protected AbstractDeliveryNoteView() {
        super();
    }
    
    /**
     * Creates a new deliveryNoteView
     * @param orderViewKey
     * @param isSalesView
     * @param ignoreNotExistingSerialPermission
     */
    protected AbstractDeliveryNoteView(
        String orderViewKey,
        boolean isSalesView,
        String ignoreNotExistingSerialPermission) {
        super();
        this.sales = isSalesView;
        this.orderViewKey = orderViewKey;
        this.ignoreNotExistingSerialPermission = ignoreNotExistingSerialPermission;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        this.orderView = (OrderView) request.getSession().getAttribute(orderViewKey);
        if (orderView != null) {
            setList(orderView.getOrder().getDeliveryNotes());
        }
        Long id = RequestUtil.fetchId(request);
        if (id != null) {
            setRecord(getRecordManager().find(id));
            String exitTarget = RequestUtil.getExit(request);
            if (exitTarget != null) {
                setExitTarget(exitTarget);
            }
            if (log.isDebugEnabled()) {
                log.debug("init() done [id=" + id + ", exit=" + exitTarget + "]");
            }
        }
    }

    public void create() throws ClientException, PermissionException {
        DeliveryNoteType type = getDefaultType();
        if (log.isDebugEnabled()) {
            log.debug("create() invoked for default type [deliveryNoteType=" + type.getId() + "]");
        }
        if (orderView == null || orderView.getBean() == null) {
            log.warn("create() invoked in invalid context, no order bound!");
            throw new ActionException("no view bound!");

        }
        Order order = orderView.getOrder();
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [order=" + order.getId() + "]");
        }
        if (!order.isUnchangeable()) {
            if (log.isDebugEnabled()) {
                log.debug("create() failed, order is changeable [order=" + order.getId() + "]");
            }
            throw new ClientException(ErrorCode.ORDER_MISSING);
        }
        if (orderView.getOrder().isChangeableDeliveryAvailable()) {
            if (log.isDebugEnabled()) {
                log.debug("create() failed, changeable deliveries found [order=" + order.getId() + "]");
            }
            throw new ClientException(ErrorCode.UNRELEASED_RECORD);
        }
        DeliveryNoteManager manager = (DeliveryNoteManager) getRecordManager();

        try {
            DeliveryNote note = manager.create(
                    getDomainUser().getEmployee(),
                    orderView.getOrder(),
                    getDefaultType(),
                    null);

            if (log.isDebugEnabled()) {
                log.debug("create() done [record="
                        + (note == null ? "null" : note.getId())
                        + "]");
            }
            setRecord(note);
        } catch (ClientException e) {
            orderView.refresh();
            throw e;
        }
    }

    public void create(DeliveryNoteType type) throws ClientException, PermissionException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [deliveryNoteType=" + type.getId() + "]");
        }
        DeliveryNoteManager manager = (DeliveryNoteManager) getRecordManager();
        DeliveryNote note = manager.create(
                getDomainUser().getEmployee(),
                this.orderView.getOrder(),
                type,
                null);

        if (log.isDebugEnabled()) {
            log.debug("create() done [record="
                    + (note == null ? "null" : note.getId())
                    + "]");
        }
        setRecord(note);
    }

    private DeliveryNoteType getDefaultType() {
        List<Option> available = new ArrayList<>(getOptions(Options.DELIVERY_NOTE_TYPES));
        for (int i = 0, j = available.size(); i < j; i++) {
            DeliveryNoteType type = (DeliveryNoteType) available.get(i);
            if (type.isSales() == this.sales) {
                if (type.isDefaultType()) {
                    return type;
                }
            }
        }
        return null;
    }

    @Override
    public void save(Form form) throws ClientException {
        DeliveryNoteManager manager = (DeliveryNoteManager) getRecordManager();
        manager.update(
                (DeliveryNote) getRecord(),
                form.getString("note"),
                form.getString("language"),
                form.getDate("delivery"),
                form.getBoolean("printBusinessCaseId"),
                form.getBoolean("printBusinessCaseInfo"),
                form.getBoolean("printRecordDate"),
                form.getBoolean("printRecordDateByStatus"));
        refresh();
        savePrintOptionDefaults(form, getRecord());
    }

    public Order getOrder() {
        return (orderView == null || orderView.getRecord() == null)
                ? null : orderView.getOrder();
    }

    public DeliveryNote getNote() {
        return (DeliveryNote) getRecord();
    }

    public boolean isSerialsRequired() {
        boolean result = false;
        List<Item> items = ((DeliveryNote) getRecord()).getItems();
        for (int i = 0, j = items.size(); i < j; i++) {
            Item item = items.get(i);
            if (!item.isSerialComplete()
                    && item.getProduct().isSerialRequired()) {
                result = true;
                break;
            }
        }
        return result;
    }

    public boolean isDisplayIgnoreNotExistingSerialDialog() {
        return displayIgnoreNotExistingSerialDialog;
    }

    public boolean isIgnoreNotExistingSerialException() {
        return ignoreNotExistingSerialException;
    }

    public void enableIgnoreNotExistingSerialMode() throws PermissionException {
        this.displayIgnoreNotExistingSerialDialog = false;
        this.checkPermission(ignoreNotExistingSerialPermission);
        this.ignoreNotExistingSerialException = true;
    }

    public String getLastSerialInput() {
        return lastSerialInput;
    }

    public Long getLastSerialInputItem() {
        return lastSerialInputItem;
    }

    public void addSerial(Form form) throws ClientException, PermissionException {
        DeliveryNote note = (DeliveryNote) getRecord();
        this.lastSerialInputItem = form.getLong(Form.ITEM_ID);
        this.lastSerialInput = form.getString(Form.SERIAL_NUMBER);
        for (int i = 0, j = note.getItems().size(); i < j; i++) {
            Item item = note.getItems().get(i);
            if (item.getId().equals(lastSerialInputItem)) {
                DeliveryNoteManager manager = (DeliveryNoteManager) getRecordManager();
                try {
                    manager.addSerial(
                            getDomainUser().getEmployee(),
                            note,
                            item,
                            this.lastSerialInput,
                            this.ignoreNotExistingSerialException);
                    this.displayIgnoreNotExistingSerialDialog = false;
                    this.ignoreNotExistingSerialException = false;
                    this.lastSerialInput = null;
                    this.lastSerialInputItem = null;
                } catch (ClientException e) {
                    if (ErrorCode.SERIAL_NOT_EXISTING.equals(e.getMessage())) {
                        this.displayIgnoreNotExistingSerialDialog = true;
                    }
                }
                break;
            }
        }
    }

    public void deleteSerials(Long itemId) {
        DeliveryNote note = (DeliveryNote) getRecord();
        for (int i = 0, j = note.getItems().size(); i < j; i++) {
            Item item = note.getItems().get(i);
            if (item.getId().equals(itemId)) {
                item.getSerials().clear();
                getRecordManager().persist(note);
                break;
            }
        }
    }

    protected void setDisplayIgnoreNotExistingSerialDialog(
            boolean displayIgnoreNotExistingSerialDialog) {
        this.displayIgnoreNotExistingSerialDialog = displayIgnoreNotExistingSerialDialog;
    }

}
