<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<div class="table-responsive table-responsive-default">
<table class="table table-striped">
    <thead>
        <tr>
            <th><fmt:message key="number"/></th>
            <th><fmt:message key="commission"/></th>
            <th><fmt:message key="from"/></th>
            <th><span title="<fmt:message key="plannedDeliveryDateAccordingOrder"/>"><fmt:message key="plannedDeliveryDateShortcut"/></span></th>
            <th><span title="<fmt:message key="altogetherOrderedQuantity"/>"><fmt:message key="quantity"/></span></th>
            <th><span title="<fmt:message key="alreadyDelivered"/>"><fmt:message key="alreadyDeliveredShortcut"/></span></th>
            <th><span title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="open"/></span></th>
        </tr>
    </thead>
    <tbody>
        <c:choose>
            <c:when test="${empty records}">
                <tr><td colspan="8"><fmt:message key="noOrdersFound"/></td></tr>
            </c:when>
            <c:otherwise>
                <c:forEach var="record" items="${records}" varStatus="s">
                    <tr <c:if test="${record.vacant}">class="red"</c:if>>
                        <td><a href="<c:url value="/loadSales.do?exit=openOrders&id=${record.salesId}"/>"><o:out value="${record.salesId}"/></a></td>
                        <td><o:out value="${record.salesName}"/></td>
                        <td><o:date value="${record.created}"/></td>
                        <td>
                            <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${record.salesId}" linkId="businessCaseDisplayView">
                                <o:date value="${record.delivery}"/>
                            </v:ajaxLink>
                        </td>
                        <td><o:out value="${record.quantity}"/></td>
                        <td><o:out value="${record.delivered}"/></td>
                        <td><o:out value="${record.outstanding}"/></td>
                    </tr>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </tbody>
</table>
