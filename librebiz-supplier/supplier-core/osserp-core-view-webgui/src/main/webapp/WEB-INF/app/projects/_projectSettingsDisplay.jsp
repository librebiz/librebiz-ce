<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="project" value="${view.businessCase}" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="settings" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="agent"> <o:forbidden role="executive,executive_sales,executive_branch,sales_agent_config">
                                <fmt:message key="agent" />
                            </o:forbidden> <o:permission role="executive,executive_sales,executive_branch,sales_agent_config" info="permission">
                                <c:choose>
                                    <c:when test="${project.cancelled}">
                                        <fmt:message key="agent" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="title">
                                            <fmt:message key="agentConfigEditorTitle" />
                                        </c:set>
                                        <o:ajaxLink url="/sales.do?method=enableAgentConfig" linkId="agentConfig">
                                            <fmt:message key="agent" />
                                        </o:ajaxLink>
                                    </c:otherwise>
                                </c:choose>
                            </o:permission>
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:forbidden role="executive,executive_sales,executive_branch,sales_agent_config">
                            <c:choose>
                                <c:when test="${!empty project.request.agent}">
                                    <o:out value="${project.request.agent.displayName}" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="undefined" />
                                </c:otherwise>
                            </c:choose>
                        </o:forbidden>
                        <o:permission role="executive,executive_sales,executive_branch,sales_agent_config" info="permission">
                            <c:choose>
                                <c:when test="${!empty project.request.agent}">
                                    <o:out value="${project.request.agent.displayName}" /> /
                                        <o:number value="${project.request.agentCommission}" />
                                    <c:choose>
                                        <c:when test="${project.request.agentCommissionPercent}">
                                            <fmt:message key="percent" />
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="euro" />
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="undefined" />
                                </c:otherwise>
                            </c:choose>
                        </o:permission>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="agent"> <o:forbidden role="executive,executive_sales,executive_branch,sales_agent_config">
                                <fmt:message key="tipProvider" />
                            </o:forbidden> <o:permission role="executive,executive_sales,executive_branch,sales_agent_config" info="permission">
                                <c:choose>
                                    <c:when test="${project.cancelled}">
                                        <fmt:message key="tipProvider" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="title">
                                            <fmt:message key="tipConfigEditorTitle" />
                                        </c:set>
                                        <o:ajaxLink url="/sales.do?method=enableTipConfig" linkId="tipConfig">
                                            <fmt:message key="tipProvider" />
                                        </o:ajaxLink>
                                    </c:otherwise>
                                </c:choose>
                            </o:permission>
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:forbidden role="executive,executive_sales,executive_branch,sales_agent_config">
                            <c:choose>
                                <c:when test="${!empty project.request.tip}">
                                    <o:out value="${project.request.tip.displayName}" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="undefined" />
                                </c:otherwise>
                            </c:choose>
                        </o:forbidden>
                        <o:permission role="executive,executive_sales,executive_branch,sales_agent_config" info="permission">
                            <c:choose>
                                <c:when test="${!empty project.request.tip}">
                                    <o:out value="${project.request.tip.displayName}" /> /
                                        <o:number value="${project.request.tipCommission}" />
                                    <fmt:message key="euro" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="undefined" />
                                </c:otherwise>
                            </c:choose>
                        </o:permission>
                    </div>
                </div>
            </div>

            <o:permission role="executive,executive_sales,executive_branch,manager_change" info="permission">
                <c:set var="managerChange" value="true" />
            </o:permission>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="agent"> <c:choose>
                                <c:when test="${project.closed or project.cancelled or not managerChange}">
                                    <fmt:message key="projectController" />
                                </c:when>
                                <c:otherwise>
                                    <a href="<c:url value="/requests.do?method=enableEmployeeSelection&target=observer"/>"><fmt:message key="projectController" /></a>
                                </c:otherwise>
                            </c:choose>
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${!empty project.observerId}">
                                <o:ajaxLink linkId="observerId" url="${'/employeeInfo.do?id='}${project.observerId}">
                                    <oc:employee value="${project.observerId}" />
                                </o:ajaxLink>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="undefined" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="agent"> <c:choose>
                                <c:when test="${(!project.closed and !project.cancelled) and (view.userRelatedManager or managerChange)}">
                                    <a href="<c:url value="/requests.do?method=enableEmployeeSelection&target=substitute"/>"><fmt:message key="coResponsible" /></a>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="coResponsible" />
                                </c:otherwise>
                            </c:choose>
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${!empty project.managerSubstituteId}">
                                <o:ajaxLink linkId="managerSubstituteId" url="${'/employeeInfo.do?id='}${project.managerSubstituteId}">
                                    <oc:employee value="${project.managerSubstituteId}" />
                                </o:ajaxLink>
                                <c:if test="${!project.closed and !project.cancelled}">
                                    <a href="<c:url value="/sales.do?method=removeEmployeeSelection&target=substitute"/>" class="top"><o:img name="deleteIcon" styleClass="tab" /></a>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="undefined" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="actions"><fmt:message key="actions" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">

                        <oc:systemPropertyEnabled name="businessCaseReopenSupport">
                            <c:if test="${project.closed and !project.cancelled}">
                                <o:permission role="sales_reopen_closed" info="salesOrderChangeClosed">
                                    <p>
                                        <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="reopenClosedBusinessCase"/>','<c:url value="/sales.do?method=reopenClosed"/>');"> 
                                            <fmt:message key="reopenClosedBusinessCase" />
                                        </a>
                                    </p>
                                </o:permission>
                            </c:if>
                        </oc:systemPropertyEnabled>

                        <c:if test="${project.closed and (view.userRelatedManager or view.user.accounting or view.user.executive)}">
                            <p>
                                <a href="<c:url value="/sales.do?method=enableChangeValuesOnClosedSalesMode"/>"><fmt:message key="enableChangeValuesOnClosedSalesMode" /></a>
                            </p>
                        </c:if>

                        <c:if test="${!project.closed and !project.cancelled and !project.deliveryClosed and !project.type.recordByCalculation}">
                            <o:permission role="sales_order_change_closed" info="salesOrderChangeClosed">
                                <p>
                                    <a href="<c:url value="/salesOrder.do?method=reopen"/>"><fmt:message key="salesOrderChangeClosed" /></a>
                                </p>
                            </o:permission>
                        </c:if>
                        <c:if test="${!project.closed and !project.cancelled and !project.deliveryClosed}">
                            <o:permission role="project_billing_delete,executive_sales,sales_order_reset" info="salesOrderReset">
                                <p>
                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="salesOrderResetInfo1"/>\n<fmt:message key="salesOrderResetInfo2"/>','<c:url value="/sales.do?method=restoreRequestStatus"/>');"
                                        title="<fmt:message key="salesOrderDelete"/>"> <fmt:message key="salesOrderReset" />
                                    </a>
                                </p>
                            </o:permission>
                        </c:if>

                        <o:permission role="delivery_note_delete" info="permissionDeliveryNoteDeleteCreateRollinNote">
                            <c:if test="${!project.closed and !project.cancelled}">
                                <p>
                                    <a href="<c:url value="/salesDeliveryNote.do?method=forwardDelete&exit=salesOrderForward"/>"><fmt:message key="title.delivery.note.delete" /></a>
                                </p>
                                <p>
                                    <a href="javascript:onclick=confirmLink('<fmt:message key="promptCreateRollinNote"/>?','<c:url value="/salesDeliveryNote.do?method=createRollin&exit=salesOrderForward"/>');"><fmt:message key="createRollinNote" /></a>
                                </p>
                            </c:if>
                        </o:permission>

                        <p>
                            <o:forbidden role="executive,executive_sales,executive_branch,customer_service">
                                <fmt:message key="salesRelationCreatorHeadline" />
                            </o:forbidden>
                            <o:permission role="executive,executive_sales,executive_branch,customer_service" info="salesRelationCreatorHeadline">
                                <a href="<v:url value="/sales/salesRelationCreator/forward?exit=/projects/projectContract/reload"/>"><fmt:message key="salesRelationCreatorHeadline" /></a>
                            </o:permission>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
