/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16.10.2004 
 * 
 */
package com.osserp.common.web.struts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.extras.actions.ForwardAction;

import com.osserp.common.TimeoutException;
import com.osserp.common.User;
import com.osserp.common.web.Globals;
import com.osserp.common.web.RequestUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractForwardAction extends ForwardAction {

    /**
     * Returns the string from the message resource bundle associated with given key
     * @param request where the resource bundle is stored
     * @param key to lookup for associated value
     * @return message resource value
     */
    protected String getResourceString(HttpServletRequest request, String key) {
        return getResources(request).getMessage(getLocale(request), key);
    }

    /**
     * Gets the current logged in user
     * @param request
     * @return user
     * @throws TimeoutException if no user logged in
     */
    protected User getUser(HttpServletRequest request) throws TimeoutException {
        return getUser(request.getSession(true));
    }

    /**
     * Gets the current logged in user
     * @param session
     * @return user
     * @throws TimeoutException if no user logged in
     */
    protected User getUser(HttpSession session) throws TimeoutException {
        User u = null;
        if (session != null) {
            u = (User) session.getAttribute(Globals.USER);
        }
        if (u != null) {
            return u;
        }
        throw new TimeoutException();
    }

    /**
     * Saves the given error string under Globals.ERRORS key as ActionError
     * @param request
     * @param errorKey
     */
    protected void saveError(HttpServletRequest request, String errorKey) {
        RequestUtil.saveError(request, errorKey);
    }

    /**
     * Saves the given error string under Globals.ERRORS key as ActionError
     * @param request
     * @param errorMessage
     */
    protected void saveNativeError(HttpServletRequest request, String errorMessage) {
        RequestUtil.saveNativeError(request, errorMessage);
    }

}
