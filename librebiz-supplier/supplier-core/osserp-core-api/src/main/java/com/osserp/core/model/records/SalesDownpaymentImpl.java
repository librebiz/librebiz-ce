/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 18:31:12 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.RecordCorrection;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Product;
import com.osserp.core.sales.SalesDownpayment;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesDownpaymentImpl extends AbstractDownpayment implements SalesDownpayment {

    protected SalesDownpaymentImpl() {
        super();
    }

    /**
     * Creates a new downpayment
     * @param id
     * @param type
     * @param billingType
     * @param order
     * @param copyItems
     * @param createdBy
     */
    public SalesDownpaymentImpl(
            Long id,
            RecordType type,
            Long billingType,
            Order order,
            boolean copyItems,
            Employee createdBy) throws ClientException {

        super(
                id,
                type,
                order,
                createdBy);

        if (Downpayment.DELIVERY.equals(billingType)
                || Downpayment.DOWNPAYMENT.equals(billingType)
                || copyItems) {
            setItemsChangeable(false);
            createItems(order, billingType);
        } else {
            setItemsChangeable(true);
        }
    }

    @Override
    public void addItem(Long stockId, Product product, String customName,
            Double quantity, Double taxRate, BigDecimal price, Date priceDate,
            BigDecimal partnerPrice, boolean partnerPriceEditable,
            boolean partnerPriceOverridden, BigDecimal purchasePrice,
            String note, boolean includePrice, Long externalId) {
        super.addItem(stockId, product, customName, quantity, taxRate, price,
                priceDate, partnerPrice, partnerPriceEditable,
                partnerPriceOverridden, purchasePrice, note, includePrice,
                externalId);
        if (isItemsChangeable()) {
            updateProrateByItems();
        }
    }

    @Override
    public void deleteItem(Item toDelete) {
        super.deleteItem(toDelete);
        if (isItemsChangeable()) {
            updateProrateByItems();
        }
    }

    @Override
    public void replaceItem(Item item, Product product) {
        super.replaceItem(item, product);
        if (isItemsChangeable()) {
            updateProrateByItems();
        }
    }

    @Override
    public void updateItem(Item item, String customName, Double quantity,
            BigDecimal price, Double taxRate, Long stockId, String note)
        throws ClientException {
        super.updateItem(item, customName, quantity, price, taxRate, stockId, note);
        if (isItemsChangeable()) {
            updateProrateByItems();
        }
    }

    protected void updateProrateByItems() {
        setAmount(getAmounts().getAmount());
        setTaxAmount(getAmounts().getTaxAmount());
        setReducedTaxAmount(getAmounts().getReducedTaxAmount());
        setGrossAmount(getAmounts().getGrossAmount());
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new SalesDownpaymentItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }

    @Override
    protected RecordCorrection createCorrection(
            Employee user,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String note) {
        return new SalesDownpaymentCorrectionImpl(this, user, name, street, streetAddon, zipcode, city, country, note);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        return new SalesDownpaymentInfoImpl(this, info, user);
    }

    public boolean isLinkedWithSale() {
        return getReference() != null;
    }

    @Override
    public boolean isSales() {
        return true;
    }

    @Override
    public void updateStatus(Long status) {
        Long current = getStatus();
        if (current == null) {
            setStatus(status);
        } else if (SalesDownpayment.STAT_CLOSED.equals(current)
                && SalesDownpayment.STAT_BOOKED.equals(status)) {
            setStatus(status);
        } else {
            super.updateStatus(status);
        }
    }

    public void restoreCancelled() {
        setStatus(STAT_PRINT);
        setDateCanceled(null);
        setIdCanceled(null);
        setCanceled(false);
    }
}
