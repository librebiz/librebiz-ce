<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesRelationCreatorView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <o:out value="${view.selectedSales.id}" /> - <o:out value="${view.selectedSales.name}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="salesRelationCreatorContent">
            <div class="row">
                <v:form url="/sales/salesRelationCreator/save">

                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <fmt:message key="salesRelationCreatorHeadline" />
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="requestType"><fmt:message key="requestType" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <oc:select styleClass="form-control" name="typeId" value="${view.businessType.id}" options="${view.businessTypes}" prompt="false" />
                                    </div>
                                </div>
                            </div>

                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="salesBy"><fmt:message key="salesBy" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <oc:select styleClass="form-control" name="salesId" value="${view.salesPerson.id}" options="${view.salesPersons}" prompt="false" />
                                    </div>
                                </div>
                            </div>

                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="commission"><fmt:message key="commission" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <v:text name="salesName" styleClass="form-control" value="${view.salesName}" />
                                    </div>
                                </div>
                            </div>

                            <div class="row next">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <v:submitExit url="/sales/salesRelationCreator/exit" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <o:submit styleClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </v:form>
            </div>
        </div>
    </tiles:put>
</tiles:insert>