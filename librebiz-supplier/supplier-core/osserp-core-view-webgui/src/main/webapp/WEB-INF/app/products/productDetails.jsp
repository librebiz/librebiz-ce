<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productDetailsView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="headline">
		<o:out  value="${view.productName}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >

		<div class="subcolumns">

			<div class="column50l">
				<div class="subcolumnl">

					<div class="spacer"></div>
					<div class="contentBox">
						<div class="contentBoxHeader">
							<div class="contentBoxHeaderLeft">
								<fmt:message key="detail"/>
							</div>
						</div>
						<div class="contentBoxData">
							<div class="subcolumns">
								<div class="subcolumn">
									<div class="spacer"></div>
									<v:form name="productDetailsForm" url="/products/productDetails/saveMeasurements">
										<table class="valueTable first33">
											<tr>
												<td><fmt:message key="length"/></td>
												<td><v:text name="width" value="${view.product.details.width}"/></td>
											</tr>
											<tr>
												<td><fmt:message key="width"/></td>
												<td><v:text name="height" value="${view.product.details.height}"/></td>
											</tr>
											<tr>
												<td><fmt:message key="depth"/></td>
												<td><v:text name="depth" value="${view.product.details.depth}"/></td>
											</tr>
											<tr>
												<td><fmt:message key="unitOfLength"/></td>
												<td>
													<select name="measurementUnit" size="1">
														<option value=" "><fmt:message key="selection"/></option>
														<c:forEach var="obj" items="${view.measurementUnits}">
															<c:choose>
																<c:when test="${view.product.details.measurementUnit == obj}">
																	<option value="<o:out value="${obj}"/>" selected="selected"><o:out value="${obj}"/></option>
																</c:when>
																<c:otherwise>
																	<option value="<o:out value="${obj}"/>"><o:out value="${obj}"/></option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<td><fmt:message key="weight"/></td>
												<td>
													<v:text name="weight" value="${view.product.details.weight}"/>
												</td>
											</tr>
                                            <tr>
                                                <td><fmt:message key="unitOfWeight"/></td>
                                                <td>
                                                    <select name="weightUnit" size="1">
                                                        <option value=" "><fmt:message key="selection"/></option>
                                                        <c:forEach var="obj" items="${view.weightUnits}">
                                                            <c:choose>
                                                                <c:when test="${view.product.details.weightUnit == obj}">
                                                                    <option value="<o:out value="${obj}"/>" selected="selected"><o:out value="${obj}"/></option>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option value="<o:out value="${obj}"/>"><o:out value="${obj}"/></option>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><fmt:message key="color"/></td>
                                                <td><oc:select name="color" value="${view.product.details.color}" options="productColors"/></td>
                                            </tr>
											<tr>
												<td colspan="2"> </td>
											</tr>
											<tr>
												<td> </td>
												<td>
													<o:submit/>
												</td>
											</tr>
										</table>
									</v:form>
									<div class="spacer"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="spacer"></div>

				</div>
			</div>

            <oc:systemPropertyEnabled name="productPropertySupport">
			<div class="column50r">
				<div class="subcolumnr">

					<div class="spacer"></div>
					<div class="contentBox">
						<div class="contentBoxHeader">
							<div class="contentBoxHeaderLeft">
								<fmt:message key="properties"/>
							</div>
							<div class="contentBoxHeaderRight">
							</div>
						</div>
						<div class="contentBoxData">
							<div class="subcolumns">
								<div class="subcolumn">
									<div class="spacer"></div>
                                    
                                    <c:import url="${viewdir}/products/_productDetailsForm.jsp"/>

									<div class="spacer"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="spacer"></div>

				</div>
			</div>
            </oc:systemPropertyEnabled>
            
		</div>
	</tiles:put>
</tiles:insert>
