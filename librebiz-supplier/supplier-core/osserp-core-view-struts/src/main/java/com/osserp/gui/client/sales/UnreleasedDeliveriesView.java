/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Dec-2006 09:21:30 
 * 
 */
package com.osserp.gui.client.sales;

import java.util.List;

import com.osserp.common.PermissionException;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewName;

import com.osserp.core.planning.SalesPlanningSummary;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("unreleasedDeliveriesView")
public interface UnreleasedDeliveriesView extends View {

    /**
     * Indicates that view lists only open orders of an product
     * @return true if so
     */
    public boolean isProductMode();

    /**
     * Provides the product if view is in product mode
     * @return product
     */
    public Product getProduct();

    /**
     * Indicates that view is in summary mode
     * @return true if so
     */
    public boolean isSummaryMode();

    /**
     * Enables/disables summary mode, depending on current mode
     */
    public void changeSummaryMode();

    /**
     * Provides the product summary over all open orders
     * @return summary
     */
    public List<SalesPlanningSummary> getSummary();

    /**
     * Synchronizes current list with backend values
     */
    public void refresh() throws PermissionException;

    /**
     * Synchronizes current list with backend values
     * @param product
     * @throws PermissionException
     */
    public void refresh(Product product) throws PermissionException;

}
