/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 7, 2006 2:34:35 PM 
 * 
 */
package com.osserp.core.sales;

import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteManager;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.products.Product;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesDeliveryNoteManager extends DeliveryNoteManager {

    /**
     * Provides all delivery notes related to given order
     * @param orderId
     * @return deliveryNotes
     */
    List<DeliveryNote> getByOrder(Long orderId);

    /**
     * Provides all delivery notes related to given sales
     * @param sales
     * @return deliveryNotes
     */
    List<DeliveryNote> getBySales(Sales sales);

    /**
     * Creates a delivery note for direct booking invoices
     * @param user
     * @param invoice
     * @return deliveryNote created or null if nothing to book
     */
    DeliveryNote create(Employee user, Invoice invoice);

    /**
     * Creates a rollin delivery note for cancellation of direct booking invoice if booking exists
     * @param user
     * @param cancellation
     * @return deliveryNote created or null if nothing to book
     */
    DeliveryNote createRollin(Employee user, Cancellation cancellation);

    /**
     * Creates a rollin by credit note if stock affecting items available
     * @param user
     * @param note
     * @return deliveryNote or null if no stock affecting items available
     */
    DeliveryNote createRollin(Employee user, CreditNote note);

    /**
     * Creates a rollin delivery note for all delivered products of given sales
     * @param user
     * @param sales
     * @return deliveryNote
     * @throws ClientException if no items to rollin found
     */
    DeliveryNote createRollin(Employee user, Sales sales) throws ClientException;

    /**
     * Checks open deliveries for stock availability
     * @param order
     * @param selected
     * @throws ClientException if required count of products is not on stock
     */
    void checkOpenDeliveries(Order order, DeliveryNote selected) throws ClientException;

    /**
     * Checks if all quantities to deliver are available
     * @param record
     * @return true if so
     */
    boolean isAvailableOnStock(DeliveryNote record);

    /**
     * Provides all unreleased delivery notes
     * @param product or null if all should selected
     * @return unreleasedItems
     */
    List<OrderItemsDisplay> getUnreleasedItems(Product product);

    /**
     * Provides all unreleased items by manager
     * @param manager
     * @param stockId
     * @return unreleasedItems
     */
    List<OrderItemsDisplay> getUnreleasedItemsByManager(Employee manager, Long stockId);

    /**
     * Removes an existing creditNote based rollin if available
     * @param user
     * @param note
     */
    void removeRollin(Employee user, CreditNote note);
}
