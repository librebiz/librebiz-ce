/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2010 9:07:07 AM 
 * 
 */
package com.osserp.core.employees;

import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EmployeeGroupManager {

    /**
     * Provides groups by grant permission
     * @param permission
     * @return list of groups as id/name values
     */
    List<Option> findGroupDisplayByPermission(String permission);

    /**
     * Provides all available employee groups
     * @return groups
     */
    List<EmployeeGroup> getGroups();

    /**
     * Provides a group by id
     * @param id
     * @return group or null if not exists
     */
    EmployeeGroup getGroup(Long id);

    /**
     * Creates a new employee group
     * @param user
     * @param name
     * @param key
     * @return new created employee group
     * @throws ClientException if name or key already exist
     */
    EmployeeGroup createGroup(Employee user, String name, String key) throws ClientException;

    /**
     * Deletes an employee group
     * @param group
     */
    void delete(EmployeeGroup group);

    /**
     * Persists an employee group
     * @param group
     */
    void save(EmployeeGroup group);

    /**
     * Provides all employee groups mapped by ldap group id
     * @return ldapMap
     */
    Map<Long, EmployeeGroup> getLdapMap();

    /**
     * Synchronizes all employee groups with ldap groups
     */
    void synchronizeLdapGroups();

}
