<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productCreatorView" />

<div class="col-md-6 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th><fmt:message key="productType" /></th>
                    <th><fmt:message key="productGroup" /></th>
                    <th><fmt:message key="category" /></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="obj" items="${view.productClassificationConfigs}">
                    <tr>
                        <td><o:out value="${obj.type.name}" /></td>
                        <td>
                            <v:link url="${view.baseLink}/updateClassification?typeId=${obj.type.id}&groupId=${obj.group.id}&categoryId=${obj.category.id}">
                                <o:out value="${obj.group.name}" />
                            </v:link>
                        </td>
                        <td><o:out value="${obj.category.name}" /></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
