/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 03-Jun-2006 11:42:54 
 * 
 */
package com.osserp.gui.client.contacts;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.BusinessCase;
import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerSummary;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("contactView")
public interface CustomerView extends PaymentAwareContactView, RelatedContactView {

    
    /**
     * Initializes customerView by a referenced businessCase 
     * @param businessCase
     */
    public void load(BusinessCase businessCase);

    /**
     * Provides the summary which provides lists of requests, projects, etc.
     * @return summary
     */
    public CustomerSummary getSummary();

    /**
     * Refreshes customer summary
     */
    public void refreshSummary();

    /**
     * Indicates that this view is created for a request view source
     * @return true if request available in this view
     */
    public boolean isRequestMode();

    /**
     * Indicates that this view is created for a sale view source
     * @return true if sale available in this view
     */
    public boolean isSaleMode();

    /**
     * Indicates that customer view is in request listing mode
     * @return requestListingMode
     */
    public boolean isRequestListingMode();

    /**
     * Enables request listing mode. Disable via call disableAllModes
     * @param cancelled indicates that cancelled requests should be displayed
     */
    public void enableRequestListingMode(boolean cancelled);

    /**
     * Indicates that customer view is in sales listing mode
     * @return salesListingMode
     */
    public boolean isSalesListingMode();

    /**
     * Enables sales listing mode. Disable via call disableAllModes
     * @param cancelled indicates that cancelled sales should be displayed
     * @param closed indicates that closed sales should be displayed
     */
    public void enableSalesListingMode(boolean cancelled, boolean closed);

    /**
     * Indicates that only cancelled requests or sales should be displayed
     * @return true if so
     */
    public boolean isCancelledListingMode();

    /**
     * Indicates that all business cases should be displayed instead of displaying open only
     * @return true if all should be displayed
     */
    public boolean isIncludeAllBusinessCases();

    /**
     * Switches between all and open only business case display
     */
    public void toggleIncludeAllBusinessCasesMode();

    /**
     * Provides the customer
     * @return customer
     */
    public Customer getCustomer();

    public boolean isDiscountEditMode();

    public void setDiscountEditMode(boolean discountEditMode);

    public void updateDiscount(Form fh) throws ClientException, PermissionException;
}
