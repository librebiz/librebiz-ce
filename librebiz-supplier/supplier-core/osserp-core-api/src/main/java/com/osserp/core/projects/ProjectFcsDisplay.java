/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 1, 2008 9:01:31 AM 
 * 
 */
package com.osserp.core.projects;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectFcsDisplay extends AbstractOption {

    private boolean cancelsAction;
    private Long canceledBy;
    private Long actionId;
    private String note;
    private Integer orderId;
    private Long status;
    private Long group;
    private boolean cancels;
    private boolean stops;
    private boolean startsUp;
    private boolean managerSelecting;
    private boolean initializing;
    private boolean releaeseing;
    private boolean requests_dp;
    private boolean response_dp;
    private boolean requests_di;
    private boolean response_di;
    private boolean requests_fi;
    private boolean response_fi;
    private boolean closingDelivery;
    private boolean enablingDelivery;
    private boolean partialDelivery;
    private boolean confirming;
    private boolean verifying;
    private boolean partialPayment;

    protected ProjectFcsDisplay() {
        super();
    }

    public ProjectFcsDisplay(
            Long id,
            Date created,
            Long createdBy,
            Long reference,
            boolean cancelsAction,
            Long canceledBy,
            Long actionId,
            String note,
            String name,
            Integer orderId,
            Long status,
            Long group,
            boolean cancels,
            boolean stops,
            boolean startsUp,
            boolean managerSelecting,
            boolean initializing,
            boolean releaeseing,
            boolean requests_dp,
            boolean response_dp,
            boolean requests_di,
            boolean response_di,
            boolean requests_fi,
            boolean response_fi,
            boolean closingDelivery,
            boolean enablingDelivery,
            boolean partialDelivery,
            boolean confirming,
            boolean verifying,
            boolean partialPayment) {
        super(id, reference, name, created, createdBy);
        this.cancelsAction = cancelsAction;
        this.canceledBy = (canceledBy == null || canceledBy == 0 ? null : canceledBy);
        this.actionId = actionId;
        this.note = note;
        this.orderId = orderId;
        this.status = status;
        this.group = group;
        this.cancels = cancels;
        this.stops = stops;
        this.startsUp = startsUp;
        this.managerSelecting = managerSelecting;
        this.initializing = initializing;
        this.releaeseing = releaeseing;
        this.requests_dp = requests_dp;
        this.response_dp = response_dp;
        this.requests_di = requests_di;
        this.response_di = response_di;
        this.requests_fi = requests_fi;
        this.response_fi = response_fi;
        this.closingDelivery = closingDelivery;
        this.enablingDelivery = enablingDelivery;
        this.partialDelivery = partialDelivery;
        this.confirming = confirming;
        this.verifying = verifying;
        this.partialPayment = partialPayment;
    }

    public boolean isCancelsAction() {
        return cancelsAction;
    }

    public Long getCanceledBy() {
        return canceledBy;
    }

    public Long getActionId() {
        return actionId;
    }

    public String getNote() {
        return note;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public Long getStatus() {
        return status;
    }

    public Long getGroup() {
        return group;
    }

    public boolean isCancels() {
        return cancels;
    }

    public boolean isStops() {
        return stops;
    }

    public boolean isStartsUp() {
        return startsUp;
    }

    public boolean isManagerSelecting() {
        return managerSelecting;
    }

    public boolean isInitializing() {
        return initializing;
    }

    public boolean isReleaeseing() {
        return releaeseing;
    }

    public boolean isRequests_dp() {
        return requests_dp;
    }

    public boolean isResponse_dp() {
        return response_dp;
    }

    public boolean isRequests_di() {
        return requests_di;
    }

    public boolean isResponse_di() {
        return response_di;
    }

    public boolean isRequests_fi() {
        return requests_fi;
    }

    public boolean isResponse_fi() {
        return response_fi;
    }

    public boolean isClosingDelivery() {
        return closingDelivery;
    }

    public boolean isEnablingDelivery() {
        return enablingDelivery;
    }

    public boolean isPartialDelivery() {
        return partialDelivery;
    }

    public boolean isConfirming() {
        return confirming;
    }

    public boolean isVerifying() {
        return verifying;
    }

    public boolean isPartialPayment() {
        return partialPayment;
    }

    protected void setCancelsAction(boolean cancelsAction) {
        this.cancelsAction = cancelsAction;
    }

    protected void setCanceledBy(Long canceledBy) {
        this.canceledBy = canceledBy;
    }

    protected void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    protected void setNote(String note) {
        this.note = note;
    }

    protected void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    protected void setStatus(Long status) {
        this.status = status;
    }

    protected void setGroup(Long group) {
        this.group = group;
    }

    protected void setCancels(boolean cancels) {
        this.cancels = cancels;
    }

    protected void setStops(boolean stops) {
        this.stops = stops;
    }

    protected void setStartsUp(boolean startsUp) {
        this.startsUp = startsUp;
    }

    protected void setManagerSelecting(boolean managerSelecting) {
        this.managerSelecting = managerSelecting;
    }

    protected void setInitializing(boolean initializing) {
        this.initializing = initializing;
    }

    protected void setReleaeseing(boolean releaeseing) {
        this.releaeseing = releaeseing;
    }

    protected void setRequests_dp(boolean requests_dp) {
        this.requests_dp = requests_dp;
    }

    protected void setResponse_dp(boolean response_dp) {
        this.response_dp = response_dp;
    }

    protected void setRequests_di(boolean requests_di) {
        this.requests_di = requests_di;
    }

    protected void setResponse_di(boolean response_di) {
        this.response_di = response_di;
    }

    protected void setRequests_fi(boolean requests_fi) {
        this.requests_fi = requests_fi;
    }

    protected void setResponse_fi(boolean response_fi) {
        this.response_fi = response_fi;
    }

    protected void setClosingDelivery(boolean closingDelivery) {
        this.closingDelivery = closingDelivery;
    }

    protected void setEnablingDelivery(boolean enablingDelivery) {
        this.enablingDelivery = enablingDelivery;
    }

    protected void setPartialDelivery(boolean partialDelivery) {
        this.partialDelivery = partialDelivery;
    }

    protected void setConfirming(boolean confirming) {
        this.confirming = confirming;
    }

    protected void setVerifying(boolean verifying) {
        this.verifying = verifying;
    }

    protected void setPartialPayment(boolean partialPayment) {
        this.partialPayment = partialPayment;
    }

    @Override
    public Object clone() {
        throw new UnsupportedOperationException("missing implementation: method clone");
    }
}
