<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.businessCaseView}"/>
<c:set var="plan" scope="request" value="${sessionScope.businessCaseView.request}"/>
<c:if test="${view.salesContext}">
	<c:set var="project" value="${sessionScope.businessCaseView.sales}"/>
</c:if>
<c:set var="otherSales" value="${view.userForeignSales}"/>
<tr>
	<td>
		<c:choose>
			<c:when test="${otherSales or view.branchEditMode or plan.cancelled or (!empty project and ((project.closed or project.cancelled) and !view.changeValuesOnClosedMode))}">
				<fmt:message key="branchOffice"/>
			</c:when>
			<c:otherwise>
				<o:forbidden role="executive,sales_change"><fmt:message key="branchOffice"/></o:forbidden>
				<o:permission role="executive,sales_change" info="permissionChangeBranchOffice">
					<a href="<c:url value="/requests.do?method=enableBranchEditMode"/>" title="<fmt:message key="branchOfficeChange"/>"><fmt:message key="branchOffice"/></a>
				</o:permission>
			</c:otherwise>
		</c:choose>
	</td>
	<c:choose>
		<c:when test="${!view.branchEditMode}">
			<td>
				<c:choose><c:when test="${empty plan.branch}"><fmt:message key="undefined"/></c:when><c:otherwise><o:out value="${plan.branch.shortkey}"/> - <o:out value="${plan.branch.shortname}"/></c:otherwise></c:choose>
			</td>
		</c:when>
		<c:otherwise>
			<o:form name="requestForm" url="/requests.do">
				<input type="hidden" name="method" value="updateBranch" />
				<td>
					<select name="branch" style="width: 170px;" size="1">
						<c:forEach var="opt" items="${view.branchOffices}">
							<c:if test="${empty plan.branch or plan.branch.id != opt.id}">
								<option value="<o:out value="${opt.id}"/>"><o:out value="${opt.name}"/></option>						
							</c:if>
						</c:forEach>
					</select>
					<a href="<c:url value="/requests.do?method=disableBranchEditMode"/>"><img src="<c:url value="${applicationScope.backIcon}"/>" style="position: relative; left: 5px;" class="bigicon" /></a>
					<input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="updateBranch" onclick="setMethod('updateBranch');" style="position: relative; left: 15px" class="bigicon"/>
				</td>
			</o:form>
		</c:otherwise>
	</c:choose>
</tr>
