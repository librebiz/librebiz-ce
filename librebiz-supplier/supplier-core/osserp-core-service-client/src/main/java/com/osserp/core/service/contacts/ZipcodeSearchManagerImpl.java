/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 17, 2011 10:46:19 AM 
 * 
 */
package com.osserp.core.service.contacts;

import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.StringUtil;
import com.osserp.core.contacts.ZipcodeSearch;
import com.osserp.core.contacts.ZipcodeSearchManager;
import com.osserp.core.dao.ZipcodeSearches;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.ZipcodeSearchImpl;

/**
 * 
 * @author jg <jg@osserp.com>
 * 
 */
public class ZipcodeSearchManagerImpl implements ZipcodeSearchManager {
    private static final Logger log = LoggerFactory.getLogger(ZipcodeSearchManagerImpl.class.getName());

    private ZipcodeSearches zipcodeSearches = null;

    private static String SEARCH_URL = "http://www.deutschepost.de/plzsuche/PlzAjaxServlet";
    private static Integer MAXIMUM_SEARCHES = 195;

    public ZipcodeSearchManagerImpl(ZipcodeSearches zipcodeSearches) {
        this.zipcodeSearches = zipcodeSearches;
    }

    public void saveSearch(Employee employee, String callFrom, Long contactId, String searchFor, String zipcode, String city, String street, String district) {
        ZipcodeSearch search = new ZipcodeSearchImpl(employee != null ? employee.getId() : null, callFrom, contactId, searchFor, zipcode, city, street,
                district);
        zipcodeSearches.save(search);
    }

    private Integer countTodaySearches() {
        return zipcodeSearches.countTodaysSearches();
    }

    public String callZipcodeServlet(String data) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("callZipcodeServlet() invoked [data=" + data + "]");
        }
        if (countTodaySearches() >= MAXIMUM_SEARCHES) {
            throw new ClientException(ErrorCode.ZIPCODE_SEARCH_MAXIMUM_SEARCHES);
        }
        try {
            URL url = new URL(SEARCH_URL);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            String jsonText = StringUtil.createStringFromInputStream(conn.getInputStream());
            wr.close();
            String decodedJsonText = new String(jsonText.getBytes(), "UTF-8");
            return decodedJsonText;
        } catch (Exception e) {
            throw new ClientException(ErrorCode.ZIPCODE_SEARCH);
        }
    }
}
