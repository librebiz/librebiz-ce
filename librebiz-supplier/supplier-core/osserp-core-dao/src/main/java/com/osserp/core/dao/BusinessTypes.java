/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2009 12:39:53 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.BusinessType;
import com.osserp.core.BusinessTypeContext;
import com.osserp.core.BusinessTypeSelection;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessTypes {

    /**
     * Loads business type with given id
     * @return businessType
     */
    BusinessType load(Long id);

    /**
     * Indicates that a type with id exists
     * @param id
     * @return true if id is assigned
     */
    boolean exists(Long id);

    /**
     * Provides all available business types
     * @return businessTypes
     */
    List<BusinessType> findAll();

    /**
     * Provides type by value from system properties
     * @param propertyName
     * @return type or null if not exists
     */
    BusinessType findBySystemProperty(String propertyName);

    /**
     * Provides all available request type contexts
     * @return contexts
     */
    List<BusinessTypeContext> findContexts();

    /**
     * Tries to find a businessType selection by name
     * @param name
     * @return selection or null if not exists
     */
    BusinessTypeSelection findSelection(String name);

    /**
     * Provides the next id by current max + 1
     * @return nextId
     */
    Long findNextId();

    /**
     * Persists a business type
     * @param type
     */
    void save(BusinessType type);
}
