/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 20:31:31 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.Record;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesPaymentImpl extends AbstractPayment {

    /**
     * Default constructor
     */
    protected SalesPaymentImpl() {
        super();
    }

    /**
     * Creates a new salesPayment
     * @param toPay
     * @param billingType
     * @param paid
     * @param user
     * @param amount
     * @param bankAccountId
     */
    public SalesPaymentImpl(
            Record toPay,
            BillingType billingType,
            Date paid,
            Employee user,
            BigDecimal amount,
            Long bankAccountId) {
        super(toPay, billingType, paid, user, amount, bankAccountId);
    }

    /**
     * Creates a new custom payment
     * @param recordToPay
     * @param billingType
     * @param paid
     * @param customHeader
     * @param note
     * @param user
     * @param amount
     * @param bankAccountId
     */
    public SalesPaymentImpl(
            Record recordToPay,
            BillingType billingType,
            Date paid,
            String customHeader,
            String note,
            Employee user,
            BigDecimal amount,
            Long bankAccountId) {
        super(recordToPay, billingType, paid, customHeader, note, user, amount, bankAccountId);
    }
}
