/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 4, 2014 
 * 
 */
package com.osserp.gui.records

import com.osserp.core.finance.Record
import com.osserp.core.finance.RecordManager

import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractRecordView extends CoreView {

    private RecordManager recordManager

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            initRecordManager(form.getString('name'))
        }
    }
    
    protected void initRecordManager(String name) {
        if (name) {
            String managerName = getManagerName(name)
            if (managerName) {
                recordManager = getService(getSystemProperty(managerName))
            }
        }
    }
    
    protected String getManagerName(String managerName) {
        if (managerName) {
            def pos = managerName.indexOf('View')
            if (pos > -1) {
                managerName = "${managerName.substring(0, pos)}Manager"
            } else if (managerName.indexOf('Manager') < 0) {
                managerName = "${managerName}Manager"
            }
            return managerName
        }
        return null
    }

    /**
     * Tries to fetch a record by request id property and initialized recordManager
     * @return record or null if no id param found, recordManager not initialized or record not exists 
     */
    protected Record fetchRecordByRequestId() {
        def id = form.getLong('id')
        if (id && recordManager) {
            return recordManager.find(id)
        }
        return null
    }

    /**
     * Provides the recordManager or null if not assigned. 
     * @return recordManager
     */
    protected RecordManager getRecordManager() {
        recordManager
    }
    
    protected void setRecordManager(RecordManager recordManager) {
        this.recordManager = recordManager
    }
}
