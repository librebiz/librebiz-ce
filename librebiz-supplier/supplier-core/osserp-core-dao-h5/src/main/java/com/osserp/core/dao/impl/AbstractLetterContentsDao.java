/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 10:41:49 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.LetterContents;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterParagraph;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.letters.LetterTypeImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractLetterContentsDao extends AbstractDao implements LetterContents {
    private String lettersSequence = null;
    private String lettersTable = null;

    public AbstractLetterContentsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            String lettersTable,
            String lettersSequence) {
        super(jdbcTemplate, tables, sessionFactory);
        this.lettersSequence = tables.getName(lettersSequence);
        this.lettersTable = tables.getName(lettersTable);
    }

    protected abstract Class<? extends LetterContent> getPersistentClass();

    public LetterType findType(String contextName) {
        StringBuilder hql = new StringBuilder(256);
        hql
                .append("from ")
                .append(LetterTypeImpl.class.getName())
                .append(" o where o.clientContext = :contextName");
        List result = getCurrentSession().createQuery(hql.toString())
                .setParameter("contextName", contextName).list();
        return result.isEmpty() ? null : (LetterType) result.get(0);
    }

    public LetterType loadType(Long id) {
        return (LetterType) getCurrentSession().load(LetterTypeImpl.class.getName(), id);
    }

    public List<LetterType> loadTypes() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(LetterTypeImpl.class.getName()).append(" o order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public void save(LetterType type) {
        getCurrentSession().saveOrUpdate(LetterTypeImpl.class.getName(), type);
    }

    public LetterParagraph createParagraph(LetterContent letter, Employee user) {
        letter.addParagraph(user);
        save(letter);
        int count = letter.getParagraphs().size();
        if (count > 0) {
            return letter.getParagraphs().get(count - 1);
        }
        return null;
    }

    public LetterContent find(Long id) {
        try {
            return (LetterContent) getCurrentSession().load(getPersistentClass().getName(), id);
        } catch (Throwable t) {
            return null;
        }
    }

    public void delete(LetterContent letter) {
        getCurrentSession().delete(letter);
    }

    public void save(LetterContent letter) {
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), letter);
    }

    protected String getLettersSequence() {
        return lettersSequence;
    }

    protected String getLettersTable() {
        return lettersTable;
    }
}
