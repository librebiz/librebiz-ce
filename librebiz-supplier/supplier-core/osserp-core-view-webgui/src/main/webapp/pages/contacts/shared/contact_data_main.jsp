<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${contactView.bean}" />

<table class="valueTable first33">
    <tbody>
        <c:if test="${contact.business and !empty contact.firstName}">
            <tr>
                <td></td>
                <td>
                    <c:choose>
                        <c:when test="${contact.firstNamePrefix}">
                            <o:out value="${contact.lastName}" />
                        </c:when>
                        <c:otherwise>
                            <o:out value="${contact.firstName}" />
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:if>
        <c:if test="${contact.freelance and !empty contact.spouseLastName}">
            <tr>
                <td><fmt:message key="owner" /></td>
                <td>
                    <c:if test="${!empty contact.spouseSalutation}">
                        <o:out value="${contact.spouseSalutation.name}" />
                    </c:if>
                    <c:if test="${!empty contact.spouseTitle}">
                        <o:out value="${contact.spouseTitle.name}" />
                    </c:if>
                    <o:out value="${contact.spouseName}" />
                </td>
            </tr>
        </c:if>
        <tr>
            <td>
                <c:choose>
                    <c:when test="${contactView.contactAddressMapSupport}">
                        <o:mapsLink street="${contact.address.street}" zip="${contact.address.zipcode}" city="${contact.address.city}" title="showAddressOnMap">
                            <fmt:message key="address" />
                        </o:mapsLink>
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="address" />
                    </c:otherwise>
                </c:choose>
            </td>
            <td><o:out value="${contact.address.street}" /><br /> <o:out value="${contact.address.zipcode}" /> <o:out value="${contact.address.city}" /></td>
        </tr>
        <c:if test="${contactView.customFederalState and !empty contact.address.federalStateName}">
            <tr>
                <td></td>
                <td><o:out value="${contact.address.federalStateName}" /></td>
            </tr>
        </c:if>
        <tr>
            <td><fmt:message key="country" /></td>
            <td><oc:options name="countryNames" value="${contact.address.country}" /></td>
        </tr>
        <c:if test="${!contactView.customFederalState and contact.address.federalStateId != 0 and !empty contact.address.federalStateId}">
            <tr>
                <td><fmt:message key="federalState" /></td>
                <td><oc:options name="federalStates" value="${contact.address.federalStateId}" /></td>
            </tr>
        </c:if>
        <c:if test="${contact.privatePerson && !empty contact.spouseLastName}">
            <tr>
                <td><fmt:message key="partner" /></td>
                <td>
                    <c:if test="${!empty contact.spouseSalutation}">
                        <o:out value="${contact.spouseSalutation.name}" />
                    </c:if>
                    <c:if test="${!empty contact.spouseTitle}">
                        <o:out value="${contact.spouseTitle.name}" />
                    </c:if>
                    <o:out value="${contact.spouseDisplayName}" />
                </td>
            </tr>
        </c:if>
        <c:if test="${!empty contact.userNameAddressField}">
            <tr>
                <td><fmt:message key="addressField" /></td>
                <td><o:out value="${contact.userNameAddressField}" /></td>
            </tr>
        </c:if>
        <c:if test="${!empty contact.userSalutation}">
            <tr>
                <td><fmt:message key="salutation" /></td>
                <td><o:out value="${contact.userSalutation}" /></td>
            </tr>
        </c:if>
        <c:if test="${contact.business and !empty contact.website}">
            <tr>
                <td><fmt:message key="website" /></td>
                <td><a href="<o:out value="${'https://'}${contact.website}"/>" target="_blank"><o:out value="${contact.website}" /></a></td>
            </tr>
        </c:if>
        <c:if test="${!contact.client and !contact.branchOffice and !contactView.assignMode}">
            <tr>
                <td><fmt:message key="newsletter" /></td>
                <td>
                    <c:choose>
                        <c:when test="${contact.newsletterConfirmation}"><fmt:message key="bigYes" /></c:when>
                        <c:otherwise><fmt:message key="bigNo" /></c:otherwise>
                    </c:choose>
                    <c:if test="${contact.vip}">
                        <span style="margin-left: 20px;"><fmt:message key="vip" /></span>
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>
                    <c:choose>
                        <c:when test="${!contact.grantContactStatusAvailable or contact.grantContactNone}">
                            <fmt:message key="gdprStatus" />
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="communication" />
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <c:choose>
                        <c:when test="${!contact.grantContactStatusAvailable}">
                            <fmt:message key="unknownLabel" />
                        </c:when>
                        <c:when test="${contact.grantContactNone}">
                            <span class="errormessage"><fmt:message key="${contact.grantContactStatus}" /></span>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="${contact.grantContactStatus}" />
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td><fmt:message key="contactDisplay" /></td>
                <td>
                    <c:choose>
                        <c:when test="${empty contact.defaultGroupDisplay}">
                            <span title="<fmt:message key="contactDisplayDefaultDescription"/>"><fmt:message key="contactDisplayDefault" /></span>
                        </c:when>
                        <c:otherwise>
                            <span title="<fmt:message key="contactDisplayLoadDescription"/>"><fmt:message key="${contact.defaultGroupDisplay}" /></span>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:if>
    </tbody>
</table>
