/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 27, 2008 11:44:48 AM 
 * 
 */
package com.osserp.common.service.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;
import org.jdom2.Element;
import org.springframework.jdbc.core.JdbcTemplate;

import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.common.service.Cache;
import com.osserp.common.service.CacheJob;
import com.osserp.common.xml.JDOMParser;
import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDatabaseStateSavingCache extends AbstractTablesAwareSpringDao implements Cache {
    private static Logger log = LoggerFactory.getLogger(AbstractDatabaseStateSavingCache.class.getName());

    private transient boolean refreshing = false;
    private String cacheTasksTable = null;

    protected AbstractDatabaseStateSavingCache(JdbcTemplate jdbcTemplate, Tables tables, String cacheTasksTableKey) {
        super(jdbcTemplate, tables);
        cacheTasksTable = getTable(cacheTasksTableKey);
    }

    public final boolean isRefreshing() {
        return refreshing;
    }

    protected final void setRefreshing(boolean refreshing) {
        this.refreshing = refreshing;
    }

    public final CacheJob getCurrentTask() {
        try {
            StringBuilder select = new StringBuilder(96);
            select.append(CacheJobRowMapper.SELECT_VALUES).append(cacheTasksTable).append(" WHERE status = 0 ORDER BY id");
            List<CacheJob> jobs = (List<CacheJob>) jdbcTemplate.query(select.toString(), CacheJobRowMapper.getReader());
            int size = jobs.size();
            CacheJob current = (size == 0 ? null : jobs.get(size - 1));
            if (current != null) {
                StringBuilder update = new StringBuilder(64);
                update
                        .append("UPDATE ")
                        .append(cacheTasksTable)
                        .append(" SET status = 1 WHERE status = 0 AND id <= ")
                        .append(current.getId());
                jdbcTemplate.update(update.toString());
                Element initiators = new Element("initiators");
                for (int i = 0; i < size; i++) {
                    initiators.addContent(createInitiatorValue(jobs.get(i).getSourceEvent()));
                }
                current.setSourceEvent(JDOMUtil.getCompactString(initiators));
            }
            return current;
        } catch (Exception e) {
            log.warn("getCurrentTask() failed [message=" + e.getMessage() 
                    + ", class=" + e.getClass().getName() + "]");
            return null;
        }
    }

    private Element createInitiatorValue(String value) {
        Element initiator = new Element("initiator");
        if (value != null) {
            try {
                Document doc = JDOMParser.getDocument(value);
                initiator.addContent(doc.detachRootElement());
            } catch (Throwable t) {
                initiator.addContent(new Element("undefined"));
            }
        }
        return initiator;
    }

    public final void closeRunningTasks(CacheJob current) {
        if (current != null) {
            StringBuilder update = new StringBuilder(64);
            update
                    .append("DELETE FROM ")
                    .append(cacheTasksTable)
                    .append(" WHERE id <= ")
                    .append(current.getId());
            jdbcTemplate.update(update.toString());
        }
    }

    public final void setModified(String reference) {
        StringBuilder create = new StringBuilder(96);
        create
                .append("INSERT INTO ")
                .append(cacheTasksTable)
                .append(" (initiator) VALUES (?)");
        try {
            jdbcTemplate.update(create.toString(), new Object[] { reference }, new int[] { Types.VARCHAR });
        } catch (Throwable t) {
            create = new StringBuilder(96);
            create
                    .append("INSERT INTO ")
                    .append(cacheTasksTable)
                    .append(" (initiator) VALUES (null)");
            try {
                jdbcTemplate.update(create.toString());
            } catch (Throwable u) {
                log.error("setModified() failed [reference=" + reference
                        + ", message=" + u.toString() + "]");
            }
        }
    }
}
