SET search_path = osserp, pg_catalog;

ALTER TABLE record_payment_conditions ADD COLUMN is_donation boolean DEFAULT false;
INSERT INTO record_payment_conditions VALUES (nextval('record_payment_conditions_id_seq'), 'Spende', 0, false, 0, 0, 0, 0, true, 'de', false, true);
UPDATE documents SET file_size = 40059 WHERE file_name = 'xsl/records/xsl_rtp_5.vm';

INSERT INTO system_properties VALUES (222, 'contactAddressMapSupport', 'false', 'java.lang.Boolean', true, false, null, false, 0);
INSERT INTO system_properties VALUES (223, 'projectAddressMapSupport', 'true', 'java.lang.Boolean', true, false, null, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 223, true);

ALTER TABLE sales_delivery_notes DROP COLUMN book_type_id;
DROP TABLE sales_delivery_note_types;
ALTER TABLE record_delivery_note_types ADD COLUMN stock_affecting boolean DEFAULT true;
ALTER TABLE record_delivery_note_types ADD COLUMN custom_content boolean DEFAULT false;
UPDATE record_delivery_note_types SET order_id = order_id + 1 WHERE order_id > 1;
INSERT INTO record_delivery_note_types VALUES (10, 'Kleinteile', 'Nicht im Auftrag vorhandene Artikel', 2, true, false, false, true, true);

UPDATE system_properties SET pvalue = '0.15.5', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
