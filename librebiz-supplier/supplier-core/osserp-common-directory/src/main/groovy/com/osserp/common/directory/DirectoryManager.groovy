/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 10, 2012 12:33:52 AM
 * 
 */
package com.osserp.common.directory


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
interface DirectoryManager extends DirectoryService {
	
	/**
	 * Activates an user by uid. Activation is done by adding user to default group 'osserp'.
	 * @param uid
	 * @param client
	 */
	void activateUser(String uid, String client)
	
	/**
	 * Deactivates an user by uid. Deactivation is done by removing user from default group 'osserp'.
	 * @param uid
	 * @param client
	 */
	void deactivateUser(String uid, String client)

	/**
	 * Creates a new directory group
	 * @param name
	 * @param description
	 * @param client
	 * @param id optional
	 * @return group
	 */
	DirectoryGroup createGroup(String name, String description, String client, Long id)

	/**
	 * Deletes a directory group
	 * @param group 
	 */
	void deleteGroup(DirectoryGroup group)
	
	/**
	 * Finds all groups
	 * @param client
	 * @return groups
	 */
	List<DirectoryGroup> findGroups(String client)

	/**
	 * Finds users groups
	 * @param user 
	 * @return groups with user as member
	 */
	List<DirectoryGroup> findGroups(DirectoryUser user)

	/**
	 * Provides the default directory group any active user account belongs to	
	 * @param client
	 * @return group
	 */
	DirectoryGroup getDefaultGroup(String client)

	/**
	 * Creates a new directory permission
	 * @param name
	 * @param description
	 * @param client
	 * @param id optional
	 * @return permission
	 */
	DirectoryPermission createPermission(String name, String description, String client, Long id)

	/**
	 * Deletes a directory permission
	 * @param permission 
	 */
	void deletePermission(DirectoryPermission permission)
	
	/**
	 * Finds all permissions
	 * @param client
	 * @return permissions
	 */
	List<DirectoryPermission> findPermissions(String client)
	
	/**
	 * Finds users permissions
	 * @param user
	 * @return permissions with user as member
	 */
	List<DirectoryPermission> findPermissions(DirectoryUser user)
	
	/**
	 * Provides the default directory permission any active user account belongs to
	 * @param client
	 * @return permission
	 */
	DirectoryPermission getDefaultPermission(String client)
    
    /**
     * Creates a set of default values by business category 'Employee'
     * @param client
     * @return user without uid, not persistent
     */
    Map createEmployeeUserAttributes(String client)
    
    /**
     * Creates a set of default values by business category 'Subscriber'
     * @return user without uid, not persistent
     */
    Map createSubscriberAttributes(String client)

	/**
	 * Creates a set of default values by business category 'System'
	 * @return user without uid, not persistent
	 */
    Map createUserAttributes(String client)

	/**
	 * Creates a new directory user	
	 * @param values of the user
	 * @param client optional client context
	 * @return user
	 */
	DirectoryUser createUser(Map<String, Object> values, String client)
	
	/**
	 * Finds directory user by uid
	 * @param uid
	 * @param client optional client context
	 * @return user or null if not exists
	 */
	DirectoryUser findUser(String uid, String client)

	/**
	 * Finds all directory users
	 * @param client optional client name
	 * @return users or empty list
	 */
	List<DirectoryUser> findUsers(String client)

	/**
	 * Finds all system accounts
	 * @param client optional client name
	 * @return system accounts or empty list
	 */
	List<DirectoryUser> findSystemUsers(String client)
	
	/**
	 * Loads a user by dn
	 * @param dn
	 * @return user or null if not exists
	 */
	DirectoryUser loadUser(String dn)
	
	/**
	 * Loads a group by dn
	 * @param dn
	 * @return group or null if not exists
	 */
	DirectoryGroup loadGroup(String dn)

	/**
	 * Updates directory group
	 * @param group
	 * @param values
	 * @return updated directory group
	 */
	DirectoryGroup update(DirectoryGroup group, Map values)

	/**
	 * Updates directory permission
	 * @param permission
	 * @param values
	 * @return updated directory group
	 */
	DirectoryPermission update(DirectoryPermission permission, Map values)

	/**
	 * Updates directory user
	 * @param user
	 * @param values
	 * @return updated directory user
	 */
	DirectoryUser update(DirectoryUser user, Map values)

	/**
	 * Sets new password
	 * @param user
	 * @param password
	 * @return updated directory user
	 */
	DirectoryUser updatePassword(DirectoryUser user, String password)
	
	/**
	 * Adds a new member to a group
	 * @param group
	 * @param user
	 */
	void addGroupMember(DirectoryGroup group, DirectoryUser user)

	/**
	 * Removes a member by uid
	 * @param group
	 * @param user
	 */
	void removeGroupMember(DirectoryGroup group, DirectoryUser user)

	/**
	 * Adds a new member to a permission
	 * @param permission
	 * @param user
	 */
	void addPermissionMember(DirectoryPermission permission, DirectoryUser user)

	/**
	 * Removes a member by uid
	 * @param permission
	 * @param user
	 */
	void removePermissionMember(DirectoryPermission permission, DirectoryUser user)
}

