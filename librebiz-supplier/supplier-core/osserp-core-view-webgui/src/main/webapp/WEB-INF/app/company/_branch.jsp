<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="branchDisplay" scope="request" value="${view.bean}"/>
<c:set var="companyDisplay" scope="request" value="${view.bean.company}"/>
<c:set var="companyExit" scope="request" value="${view.baseLink}/reload"/>

<div class="row">
    <c:if test="${!empty requestScope.companyDisplay}">
        <c:import url="_companyDisplay.jsp"/>
    </c:if>
    
    <c:import url="_branchDisplay.jsp"/>
    
</div>
