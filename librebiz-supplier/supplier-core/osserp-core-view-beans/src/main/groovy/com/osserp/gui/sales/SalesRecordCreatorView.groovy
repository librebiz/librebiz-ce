/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 6, 2014 
 * 
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.customers.Customer
import com.osserp.core.customers.CustomerSearch
import com.osserp.core.finance.Record
import com.osserp.core.finance.RecordType
import com.osserp.core.finance.RecordManager
import com.osserp.core.finance.RecordSearch
import com.osserp.core.system.BranchOffice
import com.osserp.core.system.BranchOfficeManager
import com.osserp.core.users.Permissions
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class SalesRecordCreatorView extends CoreView {

    private static Logger logger = LoggerFactory.getLogger(SalesRecordCreatorView.class.getName())
    private static final String OVERRIDE_SEQUENCE_PERMISSION = 'record_sequence_override'
    
    Customer customer

    List<BranchOffice> availableBranchs = []

    List<RecordType> availableRecordTypes = []
    RecordType selectedRecordType

    Record availableRecord
    
    String recordExit
    String resultingTarget
	
	boolean customNumberEnabled
	boolean customDateEnabled
	boolean historicalRecordEnabled

    SalesRecordCreatorView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
			
            availableRecordTypes = recordSearch.salesRecordTypes
            
			customNumberEnabled = isPermissionGrant('record_manual_number_input')
			customDateEnabled = isPermissionGrant('record_manual_date_input')
			if (customNumberEnabled && customDateEnabled) {
				// creating historical records without custom id is not supported
				historicalRecordEnabled = isPermissionGrant(Permissions.HISTORICAL_RECORD_IMPORT)
			}
            Long id = form.getLong('id')
            recordExit = form.getString('recordExit')
            if (id) {
                customer = customerSearch.findById(id)
                if (customer) {
                    availableBranchs = branchOfficeManager.findByCustomer(customer.contactId)
                    if (availableBranchs?.size() == 1) {
                        selectedBranch = availableBranchs.get(0)
                    }
                }
            } else { 
                // check for copy mode
                Long recordId = form.getLong('record')
                Long recordType = form.getLong('type')
                if (recordId && recordType) {
                    selectedRecordType = availableRecordTypes.find { it.id == recordType }
                    if (selectedRecordType) {
                        availableRecord = recordManager?.find(recordId)
                        if (availableRecord) {
                            customer = availableRecord.contact
                            availableBranchs = branchOfficeManager.findByCustomer(customer.contactId)
                            if (availableBranchs && availableRecord.branchId) {
                                selectedBranch = availableBranchs.find { it.id == availableRecord.branchId }
                            }
                        }
                    }
                    logger.debug("initRequest: done by existing [id=${availableRecord?.id}, type=${selectedRecordType?.id}, branch=${selectedBranch?.id}, customNumberEnabled=${customNumberEnabled},customDateEnabled=${customDateEnabled}, historicalRecordEnabled=${historicalRecordEnabled}]")
                }
            }
        }
    }

    boolean isManualDateInputAvailable() {
        customDateEnabled && !selectedRecordType.numberCreatorBySequence && selectedRecordType.overrideCreatedDate
    }
    
    boolean isManualNumberInputAvailable() {
        customNumberEnabled && selectedRecordType.numberCreatorBySequence && isPermissionGrant(OVERRIDE_SEQUENCE_PERMISSION)
    }

    @Override
    void selectSelectedBranch(Long id) {
        if (!id) {
            selectedBranch = null
        } else {
            selectedBranch = availableBranchs.find { it.id == id }
        }
    }

    void selectRecordType() {
        Long id = form.getLong('id')
        if (!id) {
            selectedRecordType = null
        } else {
            selectedRecordType = availableRecordTypes.find { it.id == id }
            logger.debug("selectRecordType: done [id=${selectedRecordType.id}, name=${selectedRecordType.resourceKey}]")
        }
    }

    @Override
    void save() {
		// collect custom value input if available
		Long recordNumber = form.getLong('recordNumber')
		Date recordDate = form.getDate('recordDate')
		boolean historical = form.getBoolean('historical')
        resultingTarget = exitTargetSource
        
        Record record
        if (availableRecord) {
            // copy existing
            record = recordManager.create(
                domainEmployee, 
                availableRecord, 
                true, 
                recordNumber, 
                recordDate, 
                historical)
        } else {
            record = recordManager.create(
                domainEmployee, 
                selectedBranch, 
                customer, 
                recordNumber, 
                recordDate, 
                historical)
        }
        if (record) {
            resultingTarget = "/${selectedRecordType.resourceKey}.do?method=display&id=${record.id}&exit=${recordExit}"
        }
        logger.debug("save: done [target=${resultingTarget}]")
    }

    private void loadLastNumber() {
        if (selectedBranch && selectedRecordType) {
        }
    }

    protected BranchOfficeManager getBranchOfficeManager() {
        getService(BranchOfficeManager.class.getName())
    }

    protected CustomerSearch getCustomerSearch() {
        getService(CustomerSearch.class.getName())
    }

    protected RecordSearch getRecordSearch() {
        getService(RecordSearch.class.getName())
    }

    protected RecordManager getRecordManager() {
        if (selectedRecordType?.resourceKey) {
            String managerName = getSystemProperty("${selectedRecordType.resourceKey}Manager")
            if (managerName) {
                logger.debug("getRecordManager: recordManager name found [class=${managerName}]")
                return getService(managerName)
            }
        }
        return null
    }
}
