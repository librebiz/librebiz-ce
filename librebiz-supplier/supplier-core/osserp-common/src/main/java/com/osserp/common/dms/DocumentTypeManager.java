/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 11, 2007 7:48:09 AM 
 * 
 */
package com.osserp.common.dms;

import java.util.List;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DocumentTypeManager {

    /**
     * Provides all available document types
     * @return types
     */
    List<DocumentType> findAll();

    /**
     * Provides configurable document types
     * @return configurable types
     */
    List<DocumentType> findConfigurable();

    /**
     * Provides a document type by id
     * @param id
     * @return type or null if no such type exists
     */
    DocumentType findById(Long id);

    /**
     * Loads a document type by id. Like findById but throwing a runtime exception if no such document type exists
     * @param id
     * @return type
     */
    DocumentType load(Long id);

    /**
     * Counts documents for the given type and reference
     * @param id
     * @param reference
     * @return count
     */
    Long countByReference(Long id, Long reference);

    /**
     * Toggles filenameForDownload setting
     * @param type
     * @return updated type
     */
    DocumentType changeDownloadName(DocumentType type);

    /**
     * Updates document type to provided format and locks the document type.
     * You have to invoke unlock first to perform futher changes after calling
     * this method.
     * @param type
     * @param format
     * @return updated type
     */
    DocumentType changePersistenceFormat(DocumentType type, int format);

    /**
     * Updates document type to provided format and locks the document type.
     * You have to invoke unlock first to perform futher changes after calling
     * this method.
     * @param type
     * @return updated type
     */
    DocumentType unlock(DocumentType type);

}
