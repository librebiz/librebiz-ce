/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 7, 2005 12:27:19 PM 
 * 
 */
package com.osserp.gui.output;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.transform.Source;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ActionException;
import com.osserp.common.Constants;
import com.osserp.common.DependencyException;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.web.ContextUtil;
import com.osserp.common.web.DocumentUtil;
import com.osserp.common.web.Globals;

import com.osserp.core.dms.CoreStylesheetService;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public abstract class AbstractPdfServlet extends HttpServlet {
    private static final long serialVersionUID = 42L;

    private static Logger log = LoggerFactory.getLogger(AbstractPdfServlet.class.getName());

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        doGet(request, response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            if (log.isDebugEnabled()) {
                log.debug("doGet() invoked");
            }
            HttpSession session = request.getSession();
            Object obj = DocumentUtil.getPdfDocument(session);
            if (obj != null) {
                DocumentUtil.removePdfDocument(session);
                if (obj instanceof DocumentData) {
                    render(response, (DocumentData) obj);
                    
                } else if (obj instanceof byte[]) {
                    render(response, (byte[]) obj);
                }
            } else {
                
                Document doc = getDocument(session);
                Long branchId = getBranchId(doc.getRootElement());
                String lang = (String) session.getAttribute(Globals.STYLESHEET_LANGUAGE);
                Locale locale = new Locale(lang != null ? lang : "de");
                Source xsl = getXslSource(session, branchId, locale);
                DocumentUtil.removeXmlDocument(session);
                DocumentUtil.removeStyleSheet(session);
                byte[] output = getDomainStylesheetService(session).createPdf(doc, xsl);
                render(response, output);
                DocumentUtil.removePdfDocument(session);
                session.removeAttribute(Globals.STYLESHEET_LANGUAGE);
            }

        } catch (ActionException ax) {
            try {
                PrintWriter out = response.getWriter();
                out
                        .println("<html><head><title>Error</title></head>\n"
                                + "<body><h1>PdfServlet Error</h1><h3>"
                                + "<br>No document found in session context.<br><br>Multiple requests?</body></html>");
            } catch (Exception e) {
                throw new ServletException();
            }
        } catch (DependencyException dx) {
            try {
                PrintWriter out = response.getWriter();
                out.println("<html><head><title>Error</title></head>\n"
                        + "<body><h1>PdfServlet Error</h1><h3><br>"
                        + dx.getMessage() + "</body></html>");
            } catch (Exception e) {
                throw new ServletException();
            }
        } catch (Exception ex) {
            log.error("doGet() failed: " + ex.toString(), ex);
            try {
                PrintWriter out = response.getWriter();
                out
                        .println("<html><head><title>Error</title></head>\n"
                                + "<body><h1>PdfServlet Error</h1><h3>"
                                + "Please call your network administration.</body></html>");
            } catch (Exception e) {
                throw new ServletException();
            }
        }
    }

    private Long getBranchId(Element root) {
        List<Long> branchIds = getBranchIds(root);
        Long branchId = null;

        if (branchIds.isEmpty()) {
            branchId = Constants.HEADQUARTER;
        }

        for (Iterator<Long> i = branchIds.iterator(); i.hasNext();) {
            Long current = i.next();
            if (branchId == null) {
                if (current == null) {
                    branchId = Constants.HEADQUARTER;
                } else {
                    branchId = current;
                }
            } else if (branchId != current) {
                branchId = Constants.HEADQUARTER;
            }
        }
        return branchId;
    }

    private List<Long> getBranchIds(Element element) {
        if (!element.getChildren().isEmpty()) {
            List<Element> children = element.getChildren();
            List<Long> branchIds = new ArrayList<Long>();

            for (Element el : children) {
                branchIds.addAll(getBranchIds(el));
            }

            return branchIds;
        }
        List<Long> list = new ArrayList<Long>();

        if (element.getName() == "branchId") {
            list.add(Long.parseLong(element.getValue()));
        }
        return list;
    }

    private Source getXslSource(HttpSession session, Long branchId, Locale locale) throws DependencyException {
        if (log.isDebugEnabled()) {
            log.debug("getXslSource() invoked");
        }
        try {
            StringBuilder sheetName = new StringBuilder("xsl/documents/");
            sheetName.append(getStyleSheet(session));
            Source sheet = getDomainStylesheetService(session).getStylesheetSource(sheetName.toString(), branchId, locale);
            if (sheet == null) {
                throw new DependencyException();
            }
            if (log.isDebugEnabled()) {
                log.debug("getXslSource() retrieved source [name=" + sheetName + "]");
            }
            return sheet;

        } catch (Throwable t) {
            log.error("getXslSource() failed: " + t.toString(), t);
            throw new DependencyException("No stylesheet available!");
        }
        /*
         * Filesystem aware code Source xsl = new StreamSource( PdfServlet.class.getResourceAsStream(styleSheet));
         */
    }

    protected void render(HttpServletResponse response, byte[] content) throws ServletException {

        if (log.isDebugEnabled()) {
            log.debug("render() invoked");
        }
        try {
            response.setContentLength(content.length);
            response.setContentType(Constants.MIME_TYPE_PDF);
            StringBuilder header = new StringBuilder("attachment; filename=");
            header.append(System.currentTimeMillis()).append(".pdf");
            response.setHeader("Content-disposition", header.toString());
            response.getOutputStream().write(content);
            response.getOutputStream().flush();
        } catch (Exception ex) {
            log.error("render() failed: " + ex.toString(), ex);
            throw new ServletException(ex);
        }
    }

    protected void render(HttpServletResponse response, DocumentData document) throws ServletException {

        if (log.isDebugEnabled()) {
            log.debug("render() invoked");
        }
        try {
            response.setContentLength(document.getContentLength());
            response.setContentType(document.getContentType());
            response.setHeader("Content-disposition", document.getContentDisposition());
            response.getOutputStream().write(document.getBytes());
            response.getOutputStream().flush();
        } catch (Exception ex) {
            log.error("render() failed: " + ex.toString(), ex);
            throw new ServletException(ex);
        }
    }

    protected abstract Document getDocument(HttpSession session)
            throws Exception;

    /**
     * Provides the stylesheet name
     * @param session
     * @return name
     * @throws Exception
     */
    protected abstract String getStyleSheet(HttpSession session)
            throws Exception;

    private CoreStylesheetService getDomainStylesheetService(HttpSession session) {
        return (CoreStylesheetService) ContextUtil.getService(session, CoreStylesheetService.class.getName());
    }

}
