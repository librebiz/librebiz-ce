<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:set var="recordView" scope="session" value="${sessionScope.salesCreditNoteView}"/>
<c:set var="record" value="${recordView.record}" />

<div class="content-area" id="salesCreditNoteSettingsContent">
    <div class="row">

        <div class="col-md-6 panel-area panel-area-default">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><fmt:message key="createInvoiceRelation" /></h4>
                </div>
            </div>
            <div class="panel-body">

                <div class="table-responsive table-responsive-default">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><fmt:message key="number" /></th>
                                <th class="right"><fmt:message key="billFrom" /></th>
                                <th class="right"><fmt:message key="billAmount" /></th>
                                <th class="right"><fmt:message key="paid" /></th>
                                <th class="right"><fmt:message key="demand" /></th>
                                <th> </th>
                            </tr>
                        </thead>                        
                        <tbody>
                            <c:forEach var="record" items="${recordView.relationSuggestions}" varStatus="s">
                                <c:if test="${!record.downpayment and record.unchangeable}">
                                    <tr>
                                        <td>
                                            <a href="<c:url value="/salesInvoice.do?method=display&exit=salesCreditNoteView&readonly=true&id=${record.id}"/>">
                                                <o:out value="${record.number}" />
                                            </a>
                                        </td>
                                        <td class="right"><o:date value="${record.created}" /></td>
                                        <td class="right">
                                            <o:number value="${record.grossAmount}" format="currency" />
                                        </td>
                                        <td class="right"><o:number value="${record.paidAmount}" format="currency" /></td>
                                        <td class="right"><o:number value="${record.dueAmount}" format="currency" /></td>
                                        <td class="icon center">
                                            <o:permission role="accounting,accounting_documents,executive_accounting" info="createInvoiceRelation">
                                                <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="createInvoiceRelation"/>','<c:url value="/salesCreditNote.do?method=createInvoiceRelation&id=${record.id}"/>');" title="<fmt:message key="createInvoiceRelation"/>">
                                                    <o:img name="enabledIcon"/>
                                                </a>
                                            </o:permission>
                                            <o:forbidden role="accounting,accounting_documents,executive_accounting">
                                                <span title="<fmt:message key="permissionRequired"/>"><o:img name="importantIcon"/></span>
                                            </o:forbidden>
                                        </td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
