/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Sep 30, 2009 9:15:44 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.core.model.contacts.EmailAddressImpl;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class EmailRowMapper extends AbstractRowMapper {

    public EmailRowMapper() {
    }

    public static StringBuilder getQuery(String emailsTable) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT e.id, e.contact_id, e.type_id, e.email, " +
                        "e.isprimary, e.created, e.created_by, e.changed, " +
                        "e.changed_by, e.alias_of " +
                        "FROM ")
                .append(emailsTable).append(" e ");
        return sql;
    }

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new EmailAddressImpl(
                Long.valueOf(rs.getLong("created_by")),
                Long.valueOf(rs.getLong("contact_id")),
                Long.valueOf(rs.getLong("type_id")),
                rs.getString("email"),
                Boolean.valueOf(rs.getBoolean("isprimary")),
                Long.valueOf(rs.getLong("alias_of")));
    }
}
