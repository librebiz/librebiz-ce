/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api;

import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface WSContact extends WSObject {

    String getBranch();
    void setBranch(String branch);

    Long getBranchId();
    void setBranchId(Long branchId);

    String getStatus();
    void setStatus(String status);

    Long getContactTypeId();
    void setContactTypeId(Long contactTypeId);

    String getCompany();
    void setCompany(String company);

    String getSalutation();
    void setSalutation(String salutation);

    Long getSalutationId();
    void setSalutationId(Long salutationId);

    String getTitle();
    void setTitle(String title);

    Long getTitleId();
    void setTitleId(Long title);

    String getFirstName();
    void setFirstName(String firstName);

    String getLastName();
    void setLastName(String lastName);

    Date getDateOfBirth();
    void setDateOfBirth(Date dateOfBirth);

    String getGroupname();
    void setGroupname(String groupname);

    String getJobTitle();
    void setJobTitle(String jobTitle);


    String getStreet();
    void setStreet(String street);

    String getStreetAddon();
    void setStreetAddon(String streetAddon);

    String getCity();
    void setCity(String city);

    String getFederalState();
    void setFederalState(String federalState);

    String getZipcode();
    void setZipcode(String zipcode);

    String getCountry();
    void setCountry(String country);

    Long getCountryId();
    void setCountryId(Long countryId);

    String getBusinessPhone();
    void setBusinessPhone(String businessPhone);

    String getBusinessMobile();
    void setBusinessMobile(String businessMobile);

    String getBusinessFax();
    void setBusinessFax(String businessFax);

    String getBusinessEmail();
    void setBusinessEmail(String businessEmail);

    String getWebsite();
    void setWebsite(String website);

    boolean isPrivateContact();

    /**
     * Provides the password for contact representing a user account
     * @return password
     */
    String getPassword();

    /**
     * Sets the password
     * @param password
     */
    void setPassword(String password);

    /**
     * Indicates if contact object grants contact per email
     * @return true if contact per email is grant
     */
    boolean isGrantEmail();

    /**
     * Sets if contact object grants contact per email
     * @param grantEmail true if contact per email is grant
     */
    void setGrantEmail(boolean grantEmail);

    /**
     * Indicates if contact object grants contact per phone
     * @return true if contact per phone is grant
     */
    boolean isGrantPhone();

    /**
     * Sets if contact object grants contact per phone
     * @param grantPhone true if contact per phone is grant
     */
    void setGrantPhone(boolean grantPhone);

    /**
     * Indicates if contact object does not explicit grant contact
     * per email and phone. This forces contact per letter only 
     * according to current regulations (in DE). 
     * @return true if contact grants nothing and letter only is true
     */
    boolean isGrantNon();

    /**
     * Sets if contact object grants no contact per email and phone.
     * @param grantNon true if contact grants nothing
     */
    void setGrantNon(boolean grantNon);

    /**
     * Provides a contact related note. This property should not be
     * used if contact is embedded into a businessCase. The default
     * implementation ignores contactNote if contact is embedded.
     * @return contactNote or null if non available or provided by businessCase
     */
    String getContactNote();

    /**
     * Indicates if reference id is the id of a businessCase 
     * @return true if reference is businessCase
     */
    boolean isReferenceBusinessCase();

    /**
     * This is a transient switch used by applications internal when dealing 
     * with this object. There is no persistent relation.
     * @return true if object was just created (depending on application).
     */
    boolean isJustCreated();
}
