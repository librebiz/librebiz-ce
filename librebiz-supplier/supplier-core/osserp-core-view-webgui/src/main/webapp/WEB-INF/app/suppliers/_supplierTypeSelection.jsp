<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="selectionView" scope="request" value="${sessionScope.supplierTypeSelectionView}"/>
<c:import url="../shared/_selection.jsp" />