<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesDeliveryNoteView}">
<o:logger write="delivery_note_edit.jsp no view found" level="warn"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesDeliveryNoteView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records_edit.css"/>
</style>
</tiles:put>

<tiles:put name="headline">
<c:import url="/pages/records/record_headline_edit.jsp"/>
</tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/salesDeliveryNote.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<v:link url="/records/recordPrint/print?view=salesDeliveryNoteView" title="documentPrint"><o:img name="printIcon" /></v:link>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
 
                    <o:form name="recordForm" url="/salesDeliveryNote.do">
                    <input type="hidden" name="method" value="update"/>
                    <table class="table">
                        <tr>
                            <td class="recordEditLeft" valign="top">
                                <table>
                                    <c:import url="/pages/records/record_notes_edit.jsp"/>
                                </table>
                            </td>
                            <td class="recordEditRight" valign="top">
                                <table>
                                    <tr><td colspan="2"> </td></tr>
                                    <tr><td colspan="2"> </td></tr>
                                    <c:import url="/pages/records/record_language_edit.jsp"/>
                                    <tr><td colspan="2"> </td></tr>
                                    <tr>
                                        <td><fmt:message key="deliveryDate" /></td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><span class="datePickerField"><o:datepicker input="delivery"/></span></td>
                                                    <td style="text-align:right; padding-right:0px;">
                                                        <input type="text" id="delivery" name="delivery" value="<o:date value="${record.delivery}"/>" style="width:100px; text-align:right;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <c:if test="${!record.historical}">
                                        <tr>
                                            <td class="boldtext"><fmt:message key="printOptions" /></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="printOptionsGlobalDefaultsLabel" /></td>
                                            <td align="right"><input type="checkbox" name="printOptionTypeDefaults" title="printOptionsGlobalDefaultsLabel" /></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="businessCaseIdLabel" /></td>
                                            <td align="right"><v:checkbox name="printBusinessCaseId" value="${record.printBusinessCaseId}" title="businessCaseIdLabel" /></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="businessCaseInfoLabel" /></td>
                                            <td align="right"><v:checkbox name="printBusinessCaseInfo" value="${record.printBusinessCaseInfo}" title="businessCaseInfoLabel" /></td>
                                        </tr>
                                    </c:if>
                                    <c:import url="/pages/records/record_save_changes.jsp"/>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </o:form>
                </div>
            </div>
        </div>
    </div>
</div>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
