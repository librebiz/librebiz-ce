/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 5:07:57 PM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesDeliveryNotes extends DeliveryNotes, SalesRecords {

    /**
     * Creates a new delivery note
     * @param user
     * @param order
     * @param type
     * @param deliveryDate
     * @return note created
     * @throws ClientException if nothing to deliver found
     */
    DeliveryNote create(Employee user, Order order, DeliveryNoteType type, Date deliveryDate) throws ClientException;

    /**
     * Creates a new delivery note for a direct booking invoice
     * @param user
     * @param invoice
     * @return deliveryNote created or null if no bookable products found
     */
    DeliveryNote create(Employee user, Invoice invoice);

    /**
     * Creates a new rollin delivery note if a delivery note of direct booking invoice exists
     * @param user
     * @param cancellation
     * @return deliveryNote or null if no direct booking note exists
     */
    DeliveryNote createRollin(Employee user, Cancellation cancellation);

    /**
     * Creates a rollin delivery note for a credit note if required. Rollin will be created if <br/>
     * credit note contains stock aware items and <br/>
     * such delivery note does not already exist and <br/>
     * credit note is created outside order context or <br/>
     * credit note has delivery existing flag enabled (creation on demand in order context)
     * @param user
     * @param creditNote
     * @return deliveryNote or null if not created
     */
    DeliveryNote createRollin(Employee user, CreditNote creditNote);

    /**
     * Creates a rollin note
     * @param user
     * @param order
     * @param rollIns
     * @return new created rollin
     */
    DeliveryNote createRollin(Employee user, Order order, List<Item> rollIns);

    /**
     * Removes an associated rollin
     * @param user
     * @param creditNote
     */
    void removeRollin(Employee user, CreditNote creditNote);

    /**
     * Indicates that a delivery note already exists for a direct booking invoice
     * @param invoice
     * @return true if delivery note with invoice reference exists
     */
    boolean exists(Invoice invoice);

    /**
     * Provides all unreleased delivery notes
     * @param productId or null if all should selected
     * @return unreleasedItems
     */
    List<OrderItemsDisplay> getUnreleasedItems(Long productId);

    /**
     * Provides all unreleased items by manager
     * @param managerId
     * @param stockId
     * @return unreleasedItems
     */
    List<OrderItemsDisplay> getUnreleasedItemsByManager(Long managerId, Long stockId);
}
