/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2012 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;

import com.osserp.common.Option;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessContract;
import com.osserp.core.BusinessType;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessCaseDao extends BusinessDetails {

    /**
     * Checks whether a business case with specified id exists
     * @param businessId
     * @return true if business case exists
     */
    boolean exists(Long businessId);

    /**
     * Searches for a business case by business id
     * @param businessId
     * @return businessCase or null if not exists
     */
    BusinessCase findById(Long businessId);

    /**
     * Searches for a business case by external reference
     * @param externalReference
     * @return businessCase or null if not exists
     */
    BusinessCase findByExternalReference(String externalReference);

    /**
     * Creates a new business case by all required and some optional but
     * must-have values. This method is typically used to create a
     * business case with external input provided by an internetForm.   
     * @param businessId
     * @param externalReference
     * @param externalUrl
     * @param createdDate
     * @param businessType
     * @param branchOffice
     * @param customer
     * @param salesPersonId
     * @param projectManagerId
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param campaign
     * @param origin
     * @return new created business case
     */
    BusinessCase createBusinessCase(
            Long businessId,
            String externalReference,
            String externalUrl, 
            Date createdDate,
            BusinessType businessType,
            BranchOffice branchOffice,
            Customer customer,
            Long salesPersonId,
            Long projectManagerId, 
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Campaign campaign,
            Option origin);
    
    /**
     * Updates common values of a business case
     * @param businessCase
     * @param office
     * @param salesPersonId
     * @param projectManagerId
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param campaign
     * @param origin
     * @return businessCase with updated values
     */
    BusinessCase update(
            BusinessCase businessCase,
            BranchOffice office,
            Long salesPersonId,
            Long projectManagerId,
            String name, 
            String street,
            String streetAddon,
            String zipcode, 
            String city, 
            Long country, 
            Campaign campaign,
            Option origin);
    
    /**
     * Creates a new businessCase contract
     * @param user
     * @param businessCase contract reference
     * @return new created contract
     */
    BusinessContract createContract(Long user, BusinessCase businessCase);

    /**
     * Loads an existing contract by primary key
     * @param id
     * @return contract
     * @throws RuntimeException if no such contract exists
     */
    BusinessContract loadContract(Long id);
    
    /**
     * Provides the current contract referenced by reference id 
     * @param reference
     * @return contract or null if not exists
     */
    BusinessContract findContract(BusinessCase reference);
    
    /**
     * Perists contract object
     * @param user
     * @param contract
     */
    void update(BusinessContract contract);
}
