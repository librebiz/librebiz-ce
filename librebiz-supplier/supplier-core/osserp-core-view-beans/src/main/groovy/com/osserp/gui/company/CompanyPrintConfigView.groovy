/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 1, 2016 
 * 
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.common.util.CollectionUtil

import com.osserp.core.Comparators
import com.osserp.core.dms.CoreStylesheetConfigService

import com.osserp.groovy.web.ViewContextException

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CompanyPrintConfigView extends AbstractCompanyAdminView {
    private static Logger logger = LoggerFactory.getLogger(CompanyPrintConfigView.class.getName())

    Integer documentCount = 0

    CompanyPrintConfigView() {
        super()
        headerName  = 'companyPrintConfigView'
        propertyContext = 'documentConfig'
        enableCompanyPreselection()
        enableLogoPreloading()
        enableAutoreload()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!selectedCompany) {
                logger.warn("initRequest: user has no company assigned [id=${user?.id}]")
                throw new ViewContextException(ErrorCode.PERMISSION_DENIED)
            }
            reload()
        }
    }

    @Override
    void createCustomNavigation() {
        if (selectedPropertyName) {
            nav.defaultNavigation = [deselectPropertyLink, homeLink]
        } else {
            nav.defaultNavigation = [exitLink, homeLink]
        }
    }

    byte[] printTestRecord() {
        String documentName = getString('name')
        coreStylesheetConfigService.createRecordPdf(domainEmployee, documentName)
    }

    @Override
    void reload() {
        reset()
        loadDependencies(selectedCompany, false)
        loadProperties()
        CollectionUtil.sort(selectedProperties, Comparators.createPropertyByOrderIdComparator(false))
    }

    protected CoreStylesheetConfigService getCoreStylesheetConfigService() {
        getService(CoreStylesheetConfigService.class.getName())
    }

    void createStock() {
        // not utilized here
    }

    void loadStock() {
        // not utilized here
    }
}
