<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productPriceView"/>
<c:set scope="request" var="productPriceAwareView" value="${view}"/>
<c:if test="${empty view.product.summary}">
<o:logger write="did not find product summary" level="debug"/>
</c:if>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">

input[type=submit], 
input[type=button],
input[type=text].priceinput {
    float:right;
    width: 50%;
}

input[type=text].priceinput {
    text-align: right;
}

</style>
</tiles:put>

<c:if test="${view.editMode}">
<tiles:put name="javascript" type="string">
<script type="text/javascript">

function calculateMinimum(inputElement, minimumDisplayInput, priceInput) {
    var lpp = createDouble(document.getElementById("lastPurchasePrice").firstChild.nodeValue);
    if (lpp <= 0) {
        lpp = createDouble(document.getElementById("estimatedPurchasePrice").firstChild.nodeValue);
    }
    if (lpp > 0) {
        var margin = createDouble(inputElement.value) / 100;
        var price = Math.round((lpp/(1-margin)) * 100)/100;
        document.getElementById(minimumDisplayInput).value = price;
        var priceInputField = createDouble(document.getElementById(priceInput).value);
        if (priceInputField == 0) {
             document.getElementById(priceInput).value = price;
        }
    }
}
</script>
</tiles:put>
</c:if>
<c:choose>
<c:when test="${view.editMode}">
    <tiles:put name="headline"><fmt:message key="priceEditingProduct"/></tiles:put>
</c:when>
<c:otherwise>
    <tiles:put name="headline"><fmt:message key="productPrices"/></tiles:put>
</c:otherwise>
</c:choose>

<tiles:put name="headline_right">
    <v:navigation/>
</tiles:put>

<tiles:put name="content" type="string" >
    <div class="content-area" id="projectMonitoringContent">
        <div class="row">
            <div class="col-lg-12 panel-area">
                <c:choose>
                    <c:when test="${view.editMode}">
                        <c:import url="_salesPriceEditor.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_salesPriceDisplay.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</tiles:put>
</tiles:insert>
