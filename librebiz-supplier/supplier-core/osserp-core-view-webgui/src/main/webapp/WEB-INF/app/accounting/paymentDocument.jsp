<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>

<v:view viewName="paymentDocumentView"/>
<c:set var="dmsDocumentView" scope="request" value="${sessionScope.paymentDocumentView}"/>
<c:set var="dmsDocumentUrl" scope="request" value="/accounting/paymentDocument"/>
<c:set var="dmsDeletePermissions" scope="request" value="accounting,organisation_admin"/>
<c:set var="dmsReferencePermissions" scope="request" value="accounting,organisation_admin"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="headline">
        <fmt:message key="documentsTo"/> <o:out value="${view.payment.contact.displayName}"/>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <c:import url="${viewdir}/dms/_commonDocumentUpload.jsp"/>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
