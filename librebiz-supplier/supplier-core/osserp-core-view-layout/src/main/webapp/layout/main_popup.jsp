<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%-- <o:page /> --%>
<!--<o:base/>-->
<c:set var="isPopup" value="true" scope="request"/>
<c:set var="portalView" value="${sessionScope.portalView}" />
<!DOCTYPE html>
<html lang="de">
<head>
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_application_css.jsp" />
<title><tiles:getAsString name="title" ignore="true" /></title>
<style type="text/css">
body {
    font-size: 13px;
    padding-top: 0px;
}
.main {
    padding: 0px;
}
<tiles:getAsString name="styles" ignore="true" />
</style>
<tiles:insert attribute="javascript" ignore="true" />
</head>
<body <tiles:getAsString name="onload" ignore="true"/>>
  <div class="container-fluid">
    <div class="row">
      <div class="main">
        <%-- Navigation --%>
        <div class="page-header">
          <div id="content_header">
            <div id="content_header_left">
            </div>
            <div id="content_header_middle">
              <tiles:getAsString name="headline" ignore="true" />
            </div>
            <div id="content_header_right">
              <div class="contentnav">
                <tiles:getAsString name="headline_right" ignore="true" />
                <a href="javascript:window.close()"><o:img name="disabledIcon" /></a>
              </div>
            </div>
          </div>
        </div>
        <%-- Content --%>
        <tiles:insert attribute="content" ignore="true" />
        <div id="popup-area"></div>
        <div id="datePicker" style="position: absolute; visibility: hidden; background-color: white; z-index: 10;"></div>
      </div>
      <!-- main -->
    </div>
    <!-- row -->
  </div>
  <!-- container-fluid -->
  <c:import url="/layout/shared/bottom_legacy.jsp" />
</body>
</html>
<o:removeErrors/>
