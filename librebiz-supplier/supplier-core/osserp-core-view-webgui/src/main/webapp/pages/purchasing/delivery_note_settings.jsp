<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="recordView" scope="session" value="${sessionScope.purchaseDeliveryNoteView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="hadSomethingToDisplay" value="false"/>
<div class="subcolumns">
    <%-- Left column --%>
    <div class="column50l">
        <div class="subcolumnl">

            <div class="spacer"></div>
            
            <div class="contentBox">
                <div class="contentBoxHeader" style="width: 98%;">
                    <div class="contentBoxHeaderLeft">
                        <fmt:message key="actions"/>
                    </div>
                </div>

                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <div class="spacer"></div>
                            
                            <c:if test="${record.unchangeable}">
                            <o:permission role="purchase_delivery_note_change" info="permissionReopenClosedDeliveries">
                                <div class="recordLabelValue">
                                    <a href="<c:url value="/purchaseDeliveryNote.do?method=openClosed"/>"><fmt:message key="reopenClosedDelivery"/></a>
                                </div>
                                <c:set var="hadSomethingToDisplay" value="true"/>
                            </o:permission>
                            </c:if>
                            <c:if test="${!hadSomethingToDisplay}">
                                <div class="recordLabelValue">
                                    <fmt:message key="noActionsAvailableOnCurrentPermissionState"/>
                                </div>
                            </c:if>

                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="spacer"></div>
        </div>
    </div>

    <%-- Right column 
    <div class="column50r">
        <div class="subcolumnr">
            <div class="spacer"></div>
            <div class="contentBox">
                <div class="contentBoxHeader">
                    <div class="contentBoxHeaderLeft">
                                                
                    </div>
                </div>
                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <div class="spacer"></div>
                            
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer"></div>
        </div>
    </div>
    End right column --%>
</div>
