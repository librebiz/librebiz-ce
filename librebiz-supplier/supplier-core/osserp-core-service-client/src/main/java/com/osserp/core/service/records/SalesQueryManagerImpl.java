/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 28, 2008 7:10:36 PM 
 * 
 */
package com.osserp.core.service.records;

import java.util.List;

import com.osserp.core.customers.Customer;
import com.osserp.core.dao.records.SalesRecordQueries;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.sales.SalesQueryManager;
import com.osserp.core.sales.SalesRecordSummary;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesQueryManagerImpl extends AbstractService implements SalesQueryManager {

    private SalesRecordQueries salesQueries = null;

    protected SalesQueryManagerImpl(SalesRecordQueries salesQueries) {
        super();
        this.salesQueries = salesQueries;
    }

    public Double getAccountBalance(Customer customer) {
        return salesQueries.getAccountBalance(customer);
    }

    public List<SalesRecordSummary> getSummary(Customer customer) {
        return salesQueries.getSummary(customer);
    }

    public List<RecordDisplay> findUnreleased(Long recordType, Long company) {
        return salesQueries.getUnreleased(recordType, company);
    }

}
