/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Mar-2011 11:57:21 
 * 
 */
package com.osserp.common.beans;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.Property;

/**
 * Provides a property implementation
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PropertyImpl extends ParameterImpl implements Property {

    private String type = "java.lang.String";
    private String BOOLEAN = "java.lang.Boolean";
    private String DATE = "java.util.Date";
    private int orderId = 0;
    
    private String contextName;
    private boolean displayable;
    private boolean setupOnly;
    private boolean unchangeable;

    protected PropertyImpl() {
        super();
    }

    protected PropertyImpl(String name, String value, String type) {
        super(name, value);
        this.type = type;
    }

    protected PropertyImpl(Long id, Long reference, String name, String value, Long createdBy) {
        super(id, reference, name, value, createdBy);
    }

    /**
     * Creates a simple non-persistent property with required values set
     * @param name
     * @param value
     */
    public PropertyImpl(String name, String value) {
        super(name, value);
    }

    /**
     * Creates a new property with common values set
     * @param reference
     * @param name
     * @param value
     * @param createdBy
     */
    public PropertyImpl(Long reference, String name, String value, Long createdBy) {
        super((Long) null, reference, name, value, createdBy);
    }

    /**
     * Creates a new property with primary key and common values set
     * @param id
     * @param name
     * @param value
     * @param type
     * @param createdBy
     */
    public PropertyImpl(Long id, String name, String value, String type, Long createdBy) {
        super(id, (Long) null, name, value, createdBy);
        this.type = type;
    }

    /**
     * Creates a new property as copy of another property including primary key.
     * @param other
     */
    public PropertyImpl(Property other) {
        super(other);
        type = other.getType();
        orderId = other.getOrderId();
        contextName = other.getContextName();
        displayable = other.isDisplayable();
        setupOnly = other.isSetupOnly();
        unchangeable = other.isUnchangeable();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public boolean isDisplayable() {
        return displayable;
    }

    public void setDisplayable(boolean displayable) {
        this.displayable = displayable;
    }

    public boolean isSetupContext() {
        return "setup" == contextName;
    }

    public boolean isSetupOnly() {
        return setupOnly;
    }

    public void setSetupOnly(boolean setupOnly) {
        this.setupOnly = setupOnly;
    }

    public boolean isUnchangeable() {
        return unchangeable;
    }

    public void setUnchangeable(boolean unchangeable) {
        this.unchangeable = unchangeable;
    }

    public final boolean isBooleanType() {
        return BOOLEAN.equals(type);
    }

    public final boolean isEnabled() {
        if ("true".equalsIgnoreCase(getValue()) || "yes".equalsIgnoreCase(getValue())) {
            return true;
        }
        return false;
    }

    public void enable() {
        type = BOOLEAN;
        setValue("true");
    }

    public void disable() {
        type = BOOLEAN;
        setValue("false");
    }
    
    public final boolean isDateType() {
        return DATE.equals(type);
    }

    public final Date getAsDate() {
        return fetchDate(getValue());
    }

    public final BigDecimal getAsDecimal() {
        return parseDecimal(getValue());
    }

    public final Double getAsDouble() {
        return parseDouble(getValue());
    }

    public final Integer getAsInteger() {
        return parseInteger(getValue());
    }

    public final Long getAsLong() {
        return parseLong(getValue());
    }

    public String getAsString() {
        return isNotSet(getValue()) ? "" : getValue();
    }

    public void setBoolean(boolean booleanValue) {
        if (booleanValue) {
            enable();
        } else {
            disable();
        }
    }

    @Override
    public Object clone() {
        return new PropertyImpl(this);
    }
}
