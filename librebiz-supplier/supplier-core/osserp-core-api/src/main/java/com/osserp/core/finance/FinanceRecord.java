/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 12:32:24 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;

import com.osserp.common.EntityRelation;

import com.osserp.core.contacts.ClassifiedContact;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FinanceRecord extends EntityRelation {

    /**
     * Provides the id of the record as formatted number, e.g. <br/>
     * 1020601015 as 10206-01015 or S10206-01015 if internal
     * @return formatted number or id.toString if formatting is disabled
     */
    String getNumber();

    /**
     * Provides the id of the record reference as formatted number, e.g. <br/>
     * 1020601015 as 10206-01015 or S10206-01015 if internal
     * @return formatted reference number or id.toString if formatting is disabled
     */
    String getReferenceNumber();

    /**
     * Returns the related company
     * @return company
     */
    Long getCompany();

    /**
     * Provides the associated branch if implementing record supports this.
     * @return branchId optional id of the branch
     */
    Long getBranchId();

    /**
     * Provides the document id if record is closed and document available.
     * @return document id
     */
    Long getDocumentId();
    
    /**
     * Updates document reference
     * @param changedBy
     * @param documentId
     */
    void updateDocumentId(Long changedBy, Long documentId);

    /**
     * Sets the document id if available
     * @param documentId
     */
    void setDocumentId(Long documentId);
    
    /**
     * Returns the type of the record
     * @return type
     */
    RecordType getType();

    /**
     * Provides the i18n key of type name, e.g. type.resourceKey
     * @return i18n type name
     */
    String getTypeName();

    /**
     * Provides the name of the record context, e.g. typeName of record or 
     * typeName of related record if this record is a payment.
     * @return 18n type name
     */
    String getContextName();
    
    /**
     * Provides the contact record belongs to
     * @return contact
     */
    ClassifiedContact getContact();

    /**
     * Indicates that contact is customer of reseller type
     * @return true if contact is reseller
     */
    boolean isContactReseller();

    /**
     * Returns the currency
     * @return currency
     */
    Long getCurrency();

    /**
     * Sets the currency
     * @param currency
     */
    void setCurrency(Long currency);

    /**
     * Provides currency key
     * @return key
     */
    String getCurrencyKey();

    /**
     * Provides currency symbol
     * @return currency symbol
     */
    String getCurrencySymbol();

    /**
     * Provides a custom name for the header on printout
     * @return name of header for printout
     */
    String getCustomHeader();

    /**
     * Sets a custom header for record
     * @param customHeader
     */
    void setCustomHeader(String customHeader);

    /**
     * Provides a dedicated taxPoint for the goods. The date is used
     * if date supplying the goods differs from date created.
     * @return explicit date of taxPoint if differs from default
     */
    Date getTaxPoint();

    /**
     * Provides the final date for the taxPoint. The result depends on 
     * the record type and value of taxPoint. The methods looks up for taxPoint
     * and uses this value if not null. A type specific date is returned if the
     * value of taxPoint is null.
     * @return date of taxPoint, date created, paid or delivered. See type.
     */
    Date getTaxPointDate();

    /**
     * Sets a dedicated taxPoint if date differs from default date.
     * @param taxPoint date
     */
    void setTaxPoint(Date taxPoint);

    /**
     * Indicates that the record is tax free
     * @return taxFree
     */
    boolean isTaxFree();

    /**
     * Provides the id of the tax free reason
     * @return taxFreeId
     */
    Long getTaxFreeId();

    /**
     * Marks record as tax free and references tax free reason id
     * @param taxFreeId
     */
    void enableTaxFree(Long taxFreeId);

    /**
     * Provides the display name of the contact, e.g. calling this method is equal invoking {@code getContact().getDisplayName()}
     * @return name of the contact
     */
    String getName();

    /**
     * A free form note
     * @return note
     */
    String getNote();

    /**
     * Sets the note
     * @param note
     */
    void setNote(String note);

    /**
     * Provides a link to referenced businessCase if any
     * @return id or null if not assigned
     */
    Long getBusinessCaseId();

}
