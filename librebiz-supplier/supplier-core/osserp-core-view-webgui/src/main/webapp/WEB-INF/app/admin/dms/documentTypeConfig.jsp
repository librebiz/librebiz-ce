<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>
<o:adminContext/>
<v:view viewName="documentTypeConfigView"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
        .docListCount { text-align: right; }
    </style>
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}"/>
        <c:if test="${!empty view.bean}"> - <o:out value="${view.bean.name}"/></c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="templateContent">
            <c:choose>
                <c:when test="${!empty view.bean}">
                    <c:import url="_documentTypeConfig.jsp"/>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <c:import url="_documentTypeList.jsp"/>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </tiles:put>
</tiles:insert>
