<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productNumberRangeEditorView"/>

<c:if test="${!empty view}">
	<div class="modalBoxHeader">
		<div class="modalBoxHeaderLeft">
			<fmt:message key="${view.headerName}"/>
		</div>
		<div class="modalBoxHeaderRight">
			<div class="boxnav">
				<ul>
					<c:if test="${!view.createMode and !view.editMode}">
						<c:choose>
							<c:when test="${empty view.bean}">
								<li><v:ajaxLink url="/admin/products/productNumberRangeEditor/enableCreateMode" linkId="createLink" targetElement="product_number_range_editor_popup"><o:img name="newIcon"/></v:ajaxLink></li>
							</c:when>
							<c:otherwise>
								<li><v:ajaxLink url="/admin/products/productNumberRangeEditor/select" linkId="exitLink" targetElement="product_number_range_editor_popup"><o:img name="backIcon"/></v:ajaxLink></li>
								<li><v:ajaxLink url="/admin/products/productNumberRangeEditor/enableEditMode" linkId="editLink" targetElement="product_number_range_editor_popup"><o:img name="writeIcon"/></v:ajaxLink></li>
							</c:otherwise>
						</c:choose>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
	<div class="modalBoxData">
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<c:choose>
					<c:when test="${view.createMode or view.editMode}">
						<v:ajaxForm name="dynamicForm" targetElement="product_number_range_editor_popup" url="/admin/products/productNumberRangeEditor/save">
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">
											<c:choose>
												<c:when test="${view.createMode}"><fmt:message key="createNewNumberRange"/></c:when>
												<c:otherwise><fmt:message key="changeNumberRange"/></c:otherwise>
											</c:choose>
										</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${!empty sessionScope.errors}">
										<c:set var="errorsColspanCount" scope="request" value="2"/>
										<c:import url="/errors/_errors_table_entry.jsp"/>
									</c:if>
									<tr>
										<td><fmt:message key="name"/></td>
										<td><v:text name="name" value="${view.bean.name}"/></td>
									</tr>
									<tr>
										<td><fmt:message key="beginningNumberRange"/></td>
										<td><v:text name="rangeStart" value="${view.bean.rangeStart}"/></td>
									</tr>
									<tr>
										<td><fmt:message key="endNumberRange"/></td>
										<td><v:text name="rangeEnd" value="${view.bean.rangeEnd}"/></td>
									</tr>
									<tr>
										<td> </td>
										<td style="text-align: right;">
											<c:choose>
												<c:when test="${view.createMode}">
													<v:ajaxLink url="/admin/products/productNumberRangeEditor/disableCreateMode" linkId="exitLink" targetElement="product_number_range_editor_popup">
														<input type="button" class="cancel" value="<fmt:message key="exit"/>"/>
													</v:ajaxLink>
												</c:when>
												<c:otherwise>
													<v:ajaxLink url="/admin/products/productNumberRangeEditor/disableEditMode" linkId="exitLink" targetElement="product_number_range_editor_popup">
														<input type="button" class="cancel" value="<fmt:message key="exit"/>"/>
													</v:ajaxLink>
												</c:otherwise>
											</c:choose>
											<span style="margin-right: 5px;">
												<o:submit/>
											</span>
										</td>
									</tr>
								</tbody>
							</table>
						</v:ajaxForm>
					</c:when>
					<c:when test="${!empty view.bean}">
						<table class="table">
							<tr>
								<th colspan="2"><fmt:message key="details"/></th>
							</tr>
							<tr>
								<td><fmt:message key="name"/></td>
								<td><o:out value="${view.bean.name}"/></td>
							</tr>
							<tr>
								<td><fmt:message key="beginningNumberRange"/></td>
								<td><o:out value="${view.bean.rangeStart}"/></td>
							</tr>
							<tr>
								<td><fmt:message key="endNumberRange"/></td>
								<td><o:out value="${view.bean.rangeEnd}"/></td>
							</tr>
							<%-- TODO add list of associated groups and categories --%>
						</table>
					</c:when>
					<c:otherwise>
						<table class="table">
							<tr>
								<th><fmt:message key="name"/></th>
								<th><fmt:message key="numberOf"/></th>
								<th><fmt:message key="til"/></th>
								<th>&nbsp;</th>
							</tr>
							<c:if test="${!empty sessionScope.errors}">
								<c:set var="errorsColspanCount" scope="request" value="4"/>
								<c:import url="/errors/_errors_table_entry.jsp"/>
							</c:if>
							<c:forEach var="obj" items="${view.list}">
								<tr>
									<td>
										<v:ajaxLink url="/admin/products/productNumberRangeEditor/select?id=${obj.id}" linkId="selectLink" targetElement="product_number_range_editor_popup">
											<o:out value="${obj.name}"/>
										</v:ajaxLink>
									</td>
									<c:choose>
										<c:when test="${view.currentRange == obj.id}">
											<td><span class="boldtext"><o:out value="${obj.rangeStart}"/></span></td>
											<td><span class="boldtext"><o:out value="${obj.rangeEnd}"/></span></td>
											<td class="right"> </td>
										</c:when>
										<c:otherwise>
											<td><o:out value="${obj.rangeStart}"/></td>
											<td><o:out value="${obj.rangeEnd}"/></td>
											<td class="right"><v:link url="${view.targetUrl}&id=${obj.id}"><o:img name="enabledIcon" /></v:link></td>
										</c:otherwise>
									</c:choose>
									<c:set var="exitUrl"><v:url value="/admin/products/productNumberRangeEditor/exit"/></c:set>
								</tr>
							</c:forEach>
						</table>
					</c:otherwise>
				</c:choose>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</c:if>
