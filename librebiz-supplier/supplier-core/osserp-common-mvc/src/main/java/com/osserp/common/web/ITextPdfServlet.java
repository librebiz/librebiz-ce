/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 14-Oct-2005 22:11:59 
 * 
 */
package com.osserp.common.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ITextPdfServlet extends HttpServlet {
    private static final long serialVersionUID = 42L;

    /**
     * Returns a PDF document.
     * 
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        Document document = null;
        try {
            // step 1 fetch document from session contaxt 
            document = SessionUtil.getITextDocument(session);
            // step 2: we set the ContentType and create an instance of the corresponding Writer
            response.setContentType("application/pdf");
            PdfWriter.getInstance(document, response.getOutputStream());
            // step 3
            document.open();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("document: " + e.getMessage());
        }

        // step 5: we close the document (the outputstream is also closed internally)
        if (document != null) {
            document.close();
        }
    }

}
