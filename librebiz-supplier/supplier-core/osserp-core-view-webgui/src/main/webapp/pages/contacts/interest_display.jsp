<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.interestView}"/>
<c:set var="contact" value="${view.bean}"/>

<div class="subcolumns">
	<div class="spacer"></div>
	<div class="column50l">
		<div class="subcolumnl">
			<div class="contentBox">
				<div class="contentBoxHeader">
					<c:choose>
						<c:when test="${contact.type.business}">
							<table class="valueTable first33">
								<tr>
									<td>&nbsp;</td>
									<td><o:out value="${contact.company}"/></td>
								</tr>
							</table>
						</c:when>
						<c:otherwise>
							<table class="valueTable first33">
								<tr>
									<td>
										<span class="boldtext">
											<c:choose>
												<c:when test="${!empty contact.salutation.name}"><o:out value="${contact.salutation.name}"/></c:when>
												<c:otherwise>&nbsp;</c:otherwise>
											</c:choose>
											<c:if test="${!empty contact.title.name}">&nbsp;<o:out value="${contact.title.name}"/></c:if>
										</span>
									</td>
									<td><o:out value="${contact.displayName}"/></td>
								</tr>
							</table>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="contentBoxData">
					<div class="spacer"></div>
					<table class="valueTable first33">
						<c:if test="${contact.type.business}">
							<tr>
								<td>
									<span class="boldtext">
										<c:choose>
											<c:when test="${!empty contact.salutation.name}"><o:out value="${contact.salutation.name}"/></c:when>
											<c:otherwise>&nbsp;</c:otherwise>
										</c:choose>
										<c:if test="${!empty contact.title.name}">&nbsp;<o:out value="${contact.title.name}"/></c:if>
									</span>
								</td>
								<td><o:out value="${contact.displayName}"/></td>
							</tr>
						</c:if>
						<c:choose>
							<c:when test="${view.editMode}">
								<c:import url="/pages/contacts/interest_editor.jsp"/>
							</c:when>
							<c:otherwise>
								<tr>
									<td><fmt:message key="address"/></td>
									<td>
										<o:out value="${contact.address.street}"/><br />
										<o:out value="${contact.address.zipcode}"/> <o:out value="${contact.address.city}"/>
									</td>
								</tr>
								<tr>
									<td><fmt:message key="country"/></td>
									<td><oc:options name="countryNames" value="${contact.address.country}"/></td>
								</tr>
								<c:if test="${!empty contact.phone.number}">
									<tr>
										<td><fmt:message key="phone"/></td>
										<td><oc:phone value="${contact.phone}"/></td>
									</tr>
								</c:if>
								<c:if test="${!empty contact.fax.number}">
									<tr>
										<td><fmt:message key="fax"/></td>
										<td><oc:phone value="${contact.fax}"/></td>
									</tr>
								</c:if>
								<c:if test="${!empty contact.mobile.number}">
									<tr>
										<td><fmt:message key="mobile"/></td>
										<td><oc:phone value="${contact.mobile}"/></td>
									</tr>
								</c:if>
								<c:if test="${!empty contact.email}">
									<tr>
										<td><fmt:message key="email"/></td>
										<td><o:out value="${contact.email}"/></td>
									</tr>
								</c:if>
							</c:otherwise>
						</c:choose>
					</table>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>

	<c:if test="${!view.editMode}">
		<div class="column50r">
			<div class="subcolumnr">
				<div class="contentBox">
					<div class="contentBoxHeader">
						<table>
							<tr><td><p class="boldtext"><fmt:message key="info"/></p></td></tr>
						</table>
					</div>
					<div class="contentBoxData">
						<div class="subcolumns">
							<div class="subcolumn">
								<div class="spacer"></div>
								<div><o:textOut value="${contact.description}"/></div>
								<div class="spacer"></div>
								<c:if test="${!empty contact.comment}">
									<span class="boldtext"><fmt:message key="internalNoteLabel"/></span>
									<div><o:out value="${contact.comment}"/></div>
									<div class="spacer"></div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
</div>

<div class="subcolumns">
	<div class="spacer"></div>
	<div class="subcolumn">
		<table class="table table-striped">
			<c:if test="${!empty view.existingContacts}">
				<tr>
					<th id="name"><fmt:message key="alreadyExistingLabel"/></th>
					<th id="street"><fmt:message key="street"/></th>
					<th id="city"><fmt:message key="city"/></th>
					<th id="action"> </th>
				</tr>
				<c:forEach var="obj" items="${view.existingContacts}">
					<tr>
						<td valign="top">
							<a href="<c:url value="/contacts.do"><c:param name="method" value="load"/><c:param name="exit" value="interestView"/><c:param name="id" value="${obj.contactId}"/></c:url>"><o:out value="${obj.displayName}"/></a>
						</td>
						<td valign="top"><o:out value="${obj.address.street}"/></td>
						<td valign="top"><o:out value="${obj.address.zipcode}"/> <o:out value="${obj.address.city}"/></td>
						<td valign="top">
							<a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmInterestCloseByExistingContact"/>','<c:url value="/interests.do"><c:param name="method" value="closeByExistingContact"/><c:param name="id" value="${obj.contactId}"/></c:url>');" title="<fmt:message key="interestEqualsThisExistingContact"/>"><o:img name="enabledIcon" styleClass="bigicon"/></a>
						</td>
					</tr>
				</c:forEach>
			</c:if>
			<tr>
				<td valign="top" colspan="3"><fmt:message key="contactNotExistsCreateByRequestValues"/></td>
				<td valign="top">
					<a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmInterestCloseByCreateContact"/>','<c:url value="/interests.do?method=closeByCreateContact"/>');" title="<fmt:message key="createContactByContactFormValues"/>"><o:img name="copyIcon"/></a>
				</td>
			</tr>
			<tr>
				<td valign="top" colspan="3"><fmt:message key="manuallyContactLookupBeforeAnyOtherAction"/></td>
				<td valign="top">
					<v:link url="/contacts/contactSearch/forward?exit=/interestsReload.do"><o:img name="searchIcon"/></v:link>
				</td>
			</tr>
		</table>
	</div>
</div>