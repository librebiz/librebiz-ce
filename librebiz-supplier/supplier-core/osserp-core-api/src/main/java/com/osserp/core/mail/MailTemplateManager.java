/**
 *
 * Copyright (C) 2018 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 4, 2018 
 * 
 */
package com.osserp.core.mail;

import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface MailTemplateManager {

    /**
     * Provides a mail template by name
     * @param name
     * @return template or null if not exists
     */
    MailTemplate findTemplate(String name);

    /**
     * Provides available templates
     * @return templates
     */
    List<MailTemplate> getTemplates();

    /**
     * Provides template by primary key
     * @param id
     * @return template
     */
    MailTemplate getTemplate(Long id);

    /**
     * Persists existing template
     * @param mailTemplate to persist
     * @return mailTemplate
     * @throws ClientException if validation of template changes failed
     */
    MailTemplate update(MailTemplate mailTemplate) throws ClientException;

    /**
     * Creates an initial value map
     * @param user
     * @param mailTemplate
     * @return value map
     */
    Map<String, Object> createParameters(Employee user, MailTemplate mailTemplate);

    /**
     * Creates an example using all params provided by mailTemplates parameter map
     * @param user
     * @param mailTemplate
     * @return example text
     */
    String renderExample(Employee user, MailTemplate mailTemplate);
}
