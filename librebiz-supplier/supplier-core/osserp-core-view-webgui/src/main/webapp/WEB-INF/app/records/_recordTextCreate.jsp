<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="letterView" />
<v:form name="letterForm" url="${view.baseLink}/save">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="${view.headerCreateNew}" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <tbody>
                                <c:choose>
                                    <c:when test="${empty view.availableTemplates}">
                                        <tr>
                                            <td><fmt:message key="noLetterTemplatesAvailable" /></td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="opt" items="${view.availableTemplates}" varStatus="s">
                                            <tr>
                                                <td class="chkbx">
                                                    <input type="radio" name="templateId" value="<o:out value="${opt.id}"/>" <c:if test="${opt.defaultTemplate && opt.type.autoselect}">checked="checked"</c:if> />
                                                </td>
                                                <td class="letterName"><o:out value="${opt.name}" /></td>
                                                <td class="center">
                                                    <v:link url="/admin/letters/letterTemplateConfig/forward?templateId=${opt.id}&exit=/records/recordText/reload">
                                                        <o:img name="searchIcon" styleClass="bigIcon" />
                                                    </v:link>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        <tr>
                                            <td class="chkbx"><input type="radio" name="templateId" value="null" /></td>
                                            <td class="letterName"><fmt:message key="createWithoutTemplate" /></td>
                                            <td class="center"> </td>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/disableCreateMode" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" value="create" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</v:form>
