/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
interface LdapAdminManager extends LdapManager {

    /**
     * Looks up for an admin account by uid and password
     * @param uid
     * @param password
     * @param client if client based dn required
     * @return account or empty map
     * @throws LdapException if account not exists or password does not match
     */
    Map accountLogin(String uid, String password, String client) throws LdapException

    /**
     * Creates an admin account by uid and password
     * @param uid
     * @param password
     * @param client if client based dn required
     * @return created account
     */
    Map createAdminAccount(String uid, String password, String client)

    /**
     * Finds all admin accounts
     * @param client if client based dn required
     * @return accounts or empty list if no accounts exist
     */
    List<Map> lookupAdminAccounts(String client)

    /**
     * Finds an admin account by uid
     * @param uid
     * @param client if client based dn required
     * @return account or empty map if not exists
     */
    Map lookupAdminAccount(String uid, String client)

    /**
     * Changes all password tokens of a user
     * @param account
     * @param password
     * @return updated account
     */
    Map changeAdminAccountPassword(Map account, String password)

    /**
     * Provides all clients
     * @return all available clients
     */
    List<Map> lookupClients()

    /**
     * Finds client by name
     * @param name
     * @return client or null if not exists
     */
    Map lookupClient(String name)

    /**
     * Creates a new client
     * @param name
     * @param customerId
     * @param description
     * @param mailaddress
     * @param mailenabled
     * @return new created client or existing if client was already created
     */
    Map createClient(String name, Long customerId, String description, String mailaddress, boolean mailenabled)

    /**
     * Deletes a client
     * @param client
     */
    void deleteClient(Map client)

    /**
     * Updates client values
     * @param client
     * @param values
     * @return updated client
     */
    Map updateClient(Map client, Map values)

    /**
     * Changes the access password for the base dn
     * @param password
     * @return updated account
     */
    void changeOrganizationPassword(String password)

    /**
     * Checks organization password
     * @param password
     * @return organization entry
     */
    Map organizationLogin(String password) throws LdapException

    /**
     * Provides a list of all organizational units
     * @param client
     * @return units
     */
    List<Map> lookupOrganizationalUnits(String client)

    /**
     * Provides a list of all domains with enabled mail management
     * @return maildomains	
     */
    List<Map> getMaildomains()

    /**
     * Changes the current mailhost name of domains   
     * @param currentHost mailhost to change
     * @param newHost new mailhost to set
     * @param domain the optional domain name will restrict the update to the domain 
     * with the provided name. 
     */
    void changeMailhostname(String currentHost, String newHost, String domain)
}
