/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 * 
 */
package com.osserp.gui.projects

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.BusinessCase
import com.osserp.core.projects.ProjectSupplier
import com.osserp.core.projects.ProjectSupplierManager
import com.osserp.core.suppliers.Supplier
import com.osserp.core.suppliers.SupplierSearch
import com.osserp.groovy.web.MenuItem
import com.osserp.gui.common.BusinessCaseAwareView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ProjectSupplierConfigView extends BusinessCaseAwareView {
    private static Logger logger = LoggerFactory.getLogger(ProjectSupplierConfigView.getName())

    List<String> availableGroupNames = []
    Supplier selectedSupplier

    ProjectSupplierConfigView() {
        super()
        providesCreateMode()
        providesEditMode()
        providesCustomNavigation()
        businessCaseRequired = true
    }
    
    ProjectSupplier getProjectSupplier() {
        return bean
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {

            availableGroupNames = projectSupplierManager.groupNames

            Long supplierId = form.getLong('supplierId')
            if (supplierId) {
                selectedSupplier = supplierSearch.findById(supplierId)
            }
            Long id = form.getLong('id')
            if (id) {
                bean = projectSupplierManager.getById(id)
                selectedSupplier = projectSupplier.supplier
            }
            if (!bean) {
                enableCreateMode()
            } else {
                enableEditMode()
            }
            enableExternalInvocationMode()

        } else if (reloadRequest && bean) {
            List<ProjectSupplier> all = projectSupplierManager.getSuppliers(businessCase)
            all.each { ProjectSupplier ps ->
                if (ps.id == projectSupplier.id) {
                    bean = ps
                }
            }
        }
    }

    @Override
    void save() {
        String grpName = getString('customGroupName')
        if (!grpName) {
            grpName = getString('groupName')
        }
        if (createMode) {
            bean = projectSupplierManager.create(
                domainEmployee,
                businessCase,
                selectedSupplier.id,
                grpName,
                getDate('deliveryDate'))
        } else if (projectSupplier) {
            bean = projectSupplierManager.update(
                domainEmployee,
                projectSupplier,
                grpName,
                getDate('deliveryDate'),
                getDate('mountingDate'),
                getDouble('mountingDays'),
                projectSupplier.getStatus())
        }
        if (!externalInvocationMode) {
            disableCreateMode()
            disableEditMode()
        }
    }

    void delete() {
        if (bean) {
            projectSupplierManager.remove(
                domainEmployee,
                bean,
                getBoolean('includePurchaseOrder'))
        }
    }

    boolean isDeleteMode() {
        env.deleteMode
    }

    void disableDeleteMode() {
        env.deleteMode = false
    }

    void enableDeleteMode() {
        env.deleteMode = true
    }

    @Override
    void createCustomNavigation() {
        if (deleteMode) {
            nav.editNavigation = [ exitLink, homeLink ]
        } else if (editMode) {
            nav.editNavigation = [ exitLink, enableDeleteModeLink, homeLink ]
        }
    }

    MenuItem getEnableDeleteModeLink() {
        navigationLink("/${context.name}/enableDeleteMode", 'deleteIcon', 'delete')
    }

    MenuItem getDisableDeleteModeLink() {
        navigationLink("/${context.name}/disableDeleteMode", 'backIcon', 'exit')
    }

    protected ProjectSupplierManager getProjectSupplierManager() {
        getService(ProjectSupplierManager.class.getName())
    }

    protected SupplierSearch getSupplierSearch() {
        getService(SupplierSearch.class.getName())
    }
}
