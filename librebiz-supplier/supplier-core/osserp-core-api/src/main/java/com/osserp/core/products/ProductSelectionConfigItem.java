/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 6, 2009 4:52:57 PM 
 * 
 */
package com.osserp.core.products;

import java.util.Set;

import com.osserp.common.EntityRelation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductSelectionConfigItem extends EntityRelation {

    /**
     * Provides the owner of the selection if config supports dedicated associations.
     * Examples are an id of a supplier to provide a limited set of products as 
     * preselection for purchase record booking or an employee using the same bunch 
     * of products day by day.
     * @return the referenceOwner or null if not associated to a dedicated reference
     */
    Long getReferenceOwner();

    /**
     * Assigns a dedicated reference owner
     * @param referenceOwner the referenceOwner to set
     */
    void setReferenceOwner(Long referenceOwner);

    /**
     * Limits product selection to a dedicated product
     * @return product or null to select by classification
     */
    Product getProduct();

    /**
     * Limits product selection to members of type (required)
     * @return type
     */
    ProductType getType();

    /**
     * Limits product selection to members of group
     * @return group
     */
    ProductGroup getGroup();

    /**
     * Limits product selection to members of category
     * @return category
     */
    ProductCategory getCategory();

    /**
     * Indicates that selection is an exclusion
     * @return exclusion
     */
    boolean isExclusion();

    /**
     * Enables/disables exclusion flag
     * @param exlusion
     */
    void setExclusion(boolean exlusion);

    /**
     * Returns if partner price is editable
     * @return partnerPriceEditable
     */
    boolean isPartnerPriceEditable();

    /**
     * Sets that partner price is editable
     * @param partnerPriceEditable
     */
    void setPartnerPriceEditable(boolean partnerPriceEditable);

    /**
     * Indicates that product matches selection. An product matches selection if<br/>
     * - All selection properties are null <br/>
     * - Product matches configured properties <br/>
     * - Empty selection property evaluates true
     * @param product
     * @return true if product matches selection
     */
    boolean isMatching(Product product);

    /**
     * Indicates that product selection matches an product configuration
     * @param productId
     * @param types
     * @param group
     * @param category
     * @return true if product configuration matches selection
     */
    boolean isMatching(Long productId, Set<Long> types, Long group, Long category);

    /**
     * Indicates that selection item is same as other
     * @param other
     * @return true if item is equal to other (equal except id)
     */
    boolean isSame(ProductSelectionConfigItem other);
}
