/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 14.10.2004 
 * 
 */
package com.osserp.core.products;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductGroup extends ProductClassificationEntity {

    /**
     * Sets the name
     * @param name
     */
    void setName(String name);

    /**
     * The classification of this product group (A,B,C, etc.)
     * @return classification.
     */
    ProductClass getClassification();

    /**
     * Sets the classification of this product group (A,B,C, etc.)
     * @param classification to set.
     */
    void setClassification(ProductClass classification);

    /**
     * Indicates that related products are discount
     * @return discount
     */
    boolean isDiscount();

    /**
     * Indicates that related products have separate details
     * @return detailAvailable
     */
    boolean isDetailAvailable();

    /**
     * Sets that related products have separate details
     * @param detailAvailable
     */
    void setDetailAvailable(boolean detailAvailable);

    /**
     * Indicates that serial numbers are available for products of this group
     * @return serialAvailable
     */
    boolean isSerialAvailable();

    /**
     * Indicates that serial numbers are required for products of this group
     * @return serialRequired
     */
    boolean isSerialRequired();

    /**
     * Indicates availability of GTIN-13 product code
     * @return true if gtin13 supported
     */
    boolean isGtin13Available();

    /**
     * Indicates availability of GTIN-14 product code
     * @return true if gtin14 supported
     */
    boolean isGtin14Available();

    /**
     * Indicates availability of GTIN-8 product code
     * @return true if gtin8 supported
     */
    boolean isGtin8Available();

    /**
     * Provides the default power unit for an product related to this group
     * @return powerUnit
     */
    Long getPowerUnit();

    /**
     * Sets the default power unit for an product related to this group
     * @param powerUnit
     */
    void setPowerUnit(Long powerUnit);

    /**
     * Provides the default quantity unit for an product related to this group
     * @return quantityUnit
     */
    Long getQuantityUnit();

    /**
     * Sets the default quantity unit for an product related to this group
     * @param quantityUnit
     */
    void setQuantityUnit(Long quantityUnit);

    /**
     * Provides the default margin for consumers
     * @return consumer margin
     */
    Double getConsumerMargin();

    /**
     * Sets a new consumer margin
     * @param consumerMargin
     */
    void setConsumerMargin(Double consumerMargin);

    /**
     * Provides the default margin for resellers
     * @return reseller margin
     */
    Double getResellerMargin();

    /**
     * Sets a new reseller margin
     * @param resellerMargin
     */
    void setResellerMargin(Double resellerMargin);

    /**
     * Provides the default margin for partners
     * @return partner margin
     */
    Double getPartnerMargin();

    /**
     * Sets a new partner margin
     * @param partnerMargin
     */
    void setPartnerMargin(Double partnerMargin);

    /**
     * Indicates that sales prices should calculated automatically
     * @return true if so
     */
    boolean isCalculateSales();

    /**
     * Enables/disables calculateSales flag
     * @param calculateSales
     */
    void setCalculateSales(boolean calculateSales);

    /**
     * Indicates that this products end of life has reached
     * @return endOfLife
     */
    boolean isEndOfLife();

    /**
     * Sets that this products end of life has reached
     * @param endOfLife
     */
    void setEndOfLife(boolean endOfLife);

    /**
     * Indicates that product is class A product
     * @return classA
     */
    boolean isClassA();

    /**
     * Indicates that product is class B product
     * @return classB
     */
    boolean isClassB();

    /**
     * Indicates that product is class C product
     * @return classC
     */
    boolean isClassC();

    /**
     * @return true, if products of this group provide a datasheet
     */
    boolean isProvidingDatasheet();

    /**
     * Sets that products of this group provide a datasheet
     * @param providingDatasheet
     */
    void setProvidingDatasheet(boolean providingDatasheet);

    /**
     * @return true, if products of this group provides pictures
     */
    boolean isProvidingPicture();

    /**
     * Sets that products of this group provides pictures
     * @param providingPicture
     */
    void setProvidingPicture(boolean providingPicture);
}
