/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 3:12:16 PM 
 * 
 */
package com.osserp.core;

import java.util.Date;

import com.osserp.common.EntityRelation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FcsItem extends EntityRelation {

    /**
     * Returns the related employee id
     * @return employeeId
     */
    Long getEmployeeId();

    /**
     * Returns the flow control action
     * @return action
     */
    FcsAction getAction();

    /**
     * Changes the action
     * @param action
     */
    void changeAction(FcsAction action);

    /**
     * The date the action was performed
     * @return created
     */
    Date getCreated();

    /**
     * A note for this fcs
     * @return note
     */
    String getNote();

    /**
     * Sets the note for this fcs
     * @param note
     */
    void setNote(String note);

    /**
     * Indicates that this item represents an item which cancels another action done before. See notes for canceledBy for details. Take care!
     * @return cancels
     */
    boolean isCancels();

    /**
     * Provides the id of an action whose canceled this action NOTE: This is must be set to the related id of the cancelling action. This action itself must not
     * set to canceled itself. Canceled should be set for the action whose cancels. Take care!
     * @return canceledBy
     */
    Long getCanceledBy();

    /**
     * Sets the id of an action whose canceled this action NOTE: This is must be set to the related id of the cancelling action. This action itself must not set
     * to canceled itself. Canceled should be set for the action whose cancels. Take care!
     * @param canceledBy
     */
    void setCanceledBy(Long canceledBy);

    /**
     * Provides the headline if fcs should be displayed reverse
     * @return headline
     */
    String getHeadline();

    /**
     * Sets the headline
     * @param headline
     */
    void setHeadline(String headline);

    /**
     * Returns an optional closing status if available.
     * @return closing
     */
    FcsClosing getClosing();
}
