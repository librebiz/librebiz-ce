<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<o:logger write="page invoked..." level="debug"/>
<c:set var="view" value="${sessionScope.searchView}"/>
<c:set var="productExitTarget" value="${view.data['productExitTarget']}"/>
<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="productNumber">
                    <fmt:message key="product"/>
                </th>
                <th class="productName">
                    <fmt:message key="name"/>
                </th>
                <th class="productSalesOrders">
                    <fmt:message key="order"/>
                </th>
                <th class="productStock">
                    <fmt:message key="inventory"/>
                </th>
                <th class="productPurchaseReceipts">
                    <fmt:message key="stockReceiptShort"/>
                </th>
                <th class="productPurchaseOrders">
                    <fmt:message key="purchaseOrderShort"/>
                </th>
                <th class="productAction"> </th>
            </tr>
        </thead>
        <tbody>
            <c:choose>
                <c:when test="${empty view.list}">
                    <tr>
                        <td class="left" colspan="10"><fmt:message key="searchUnsuccessfulPleaseChangeYourInput" /></td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach var="product" items="${view.list}" varStatus="s">
                        <tr>
                            <td class="productNumber">
                                <a href="<c:url value="/products.do?method=load&id=${product.productId}&exit=${productExitTarget}"/>" title="<fmt:message key="loadProduct"/>">
                                    <o:out value="${product.productId}" />
                                </a>
                            </td>
                            <td class="productName">
                                <o:out value="${product.name}" limit="50"/><input type="hidden" name="customName" value=""/>
                            </td>
                            <td class="productSalesOrders"><span title="<fmt:message key="currentOrdersExclusiveThis"/>"><o:number value="${product.summary.ordered}" format="integer"/></span></td>
                            <td class="productStock"><span title="<fmt:message key="currentOnStock"/>"><o:number value="${product.summary.stock}" format="integer"/></span></td>
                            <td class="productPurchaseReceipts"><span title="<fmt:message key="currentPurchaseDelivery"/>"><o:number value="${product.summary.receipt}" format="integer"/></span></td>
                            <td class="productPurchaseOrders"><span title="<fmt:message key="currentPurchaseOrders"/>"><o:number value="${product.summary.expected}" format="integer"/></span></td>
                            <td class="productAction">
                                <a href="<c:url value="${view.data['searchUrl']}?method=selectItem&id=${product.productId}"/>" style="padding-right:33%;">
                                    <o:img name="enabledIcon" />
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </tbody>
    </table>
</div>
