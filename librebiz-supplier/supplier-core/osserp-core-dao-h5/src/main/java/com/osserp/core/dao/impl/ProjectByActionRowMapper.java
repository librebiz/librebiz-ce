/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 26-Sep-2006 00:34:03 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;

import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.model.projects.ProjectByActionDateVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectByActionRowMapper extends AbstractProjectRowMapper {

    public static RowMapperResultSetExtractor getReader(
            OptionsCache cache,
            FlowControlActions actions) {

        return new RowMapperResultSetExtractor(
                new ProjectByActionRowMapper(cache, actions));
    }

    protected ProjectByActionRowMapper(OptionsCache cache, FlowControlActions actions) {
        super(cache, actions);
    }

    /*
     * project_id bigint, plan_id bigint, contact_id bigint, contact_name text, street text, zipcode text, city text, project_name text, type_id bigint, status
     * integer, sales_id bigint, manager_id bigint, branch_id bigint plant_capacity numeric, action_id bigint, action_date timestamp without time zone,
     * last_action_name text, price_per_unit numeric
     */

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new ProjectByActionDateVO(
                Long.valueOf(rs.getLong(1)),
                Long.valueOf(rs.getLong(2)),
                Long.valueOf(rs.getLong(3)),
                rs.getString(4),
                rs.getString(5),
                rs.getString(6),
                rs.getString(7),
                rs.getString(8),
                fetchBusinessType(Long.valueOf(rs.getLong(9))),
                Integer.valueOf(rs.getInt(10)),
                Long.valueOf(rs.getLong(11)),
                Long.valueOf(rs.getLong(12)),
                Long.valueOf(rs.getLong(13)),
                Double.valueOf(rs.getDouble(14)),
                Long.valueOf(rs.getLong(15)),
                rs.getDate(16),
                rs.getString(17),
                Double.valueOf(rs.getDouble(18)));
    }

}
