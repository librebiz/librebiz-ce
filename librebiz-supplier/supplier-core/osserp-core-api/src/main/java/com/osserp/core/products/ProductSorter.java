/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 28, 2010 9:02:29 AM 
 * 
 */
package com.osserp.core.products;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public class ProductSorter {

    public static final String[] COMPARATORS = {
            "byAvailability",
            "byAvailabilityReverse",
            "byAvailableStock",
            "byAvailableStockReverse",
            "byConsumerPrice",
            "byConsumerPriceReverse",
            "byExpected",
            "byExpectedReverse",
            "byId",
            "byIdReverse",
            "byName",
            "byNameReverse",
            "byOrdered",
            "byOrderedReverse",
            "byOrderedAndVacant",
            "byOrderedAndVacantReverse",
            "byPartnerPrice",
            "byPartnerPriceReverse",
            "byReceipt",
            "byReceiptReverse",
            "byResellerPrice",
            "byResellerPriceReverse",
            "bySpecificPartnerPrice",
            "bySpecificPartnerPriceReverse",
            "byStock",
            "byStockReverse",
            "byVacantAvailability",
            "byVacantAvailabilityReverse"
    };

    private Map<String, Comparator> comparators = null;

    private static ProductSorter cinstance = null;

    @SuppressWarnings("unchecked")
    private ProductSorter() {
        super();
        comparators = Collections.synchronizedMap(new HashMap<>());

        comparators.put("byAvailability", ProductComparators.sortByAvailability(false));
        comparators.put("byAvailabilityReverse", ProductComparators.sortByAvailability(true));
        comparators.put("byAvailableStock", ProductComparators.sortByAvailableStock(false));
        comparators.put("byAvailableStockReverse", ProductComparators.sortByAvailableStock(true));
        comparators.put("byConsumerPrice", ProductComparators.sortByConsumerPrice(false));
        comparators.put("byConsumerPriceReverse", ProductComparators.sortByConsumerPrice(true));
        comparators.put("byExpected", ProductComparators.sortByExpected(false));
        comparators.put("byExpectedReverse", ProductComparators.sortByExpected(true));
        comparators.put("byId", ProductComparators.sortById(false));
        comparators.put("byIdReverse", ProductComparators.sortById(true));
        comparators.put("byName", ProductComparators.sortByName(false));
        comparators.put("byNameReverse", ProductComparators.sortByName(true));
        comparators.put("byOrdered", ProductComparators.sortByOrders(false));
        comparators.put("byOrderedReverse", ProductComparators.sortByOrders(true));
        comparators.put("byOrderedAndVacant", ProductComparators.sortByOrderedAndVacant(false));
        comparators.put("byOrderedAndVacantReverse", ProductComparators.sortByOrderedAndVacant(true));
        comparators.put("byPartnerPrice", ProductComparators.sortByPartnerPrice(false));
        comparators.put("byPartnerPriceReverse", ProductComparators.sortByPartnerPrice(true));
        comparators.put("byReceipt", ProductComparators.sortByReceipt(false));
        comparators.put("byReceiptReverse", ProductComparators.sortByReceipt(true));
        comparators.put("byResellerPrice", ProductComparators.sortByResellerPrice(false));
        comparators.put("byResellerPriceReverse", ProductComparators.sortByResellerPrice(true));
        comparators.put("bySpecificPartnerPrice", ProductComparators.sortBySpecificPartnerPrice(false));
        comparators.put("bySpecificPartnerPriceReverse", ProductComparators.sortBySpecificPartnerPrice(true));
        comparators.put("byStock", ProductComparators.sortByStock(false));
        comparators.put("byStockReverse", ProductComparators.sortByStock(true));
        comparators.put("byVacantAvailability", ProductComparators.sortByVacantAvailability(false));
        comparators.put("byVacantAvailabilityReverse", ProductComparators.sortByVacantAvailability(true));
    }

    public static ProductSorter instance() {
        if (cinstance == null) {
            cinstance = new ProductSorter();
        }
        return cinstance;
    }

    @SuppressWarnings("unchecked")
    public void sort(String name, List<Product> items) {
        Comparator c = comparators.get(name);
        if (c == null) {
            c = comparators.get("byName");
        }
        Collections.sort(items, c);
    }

}
