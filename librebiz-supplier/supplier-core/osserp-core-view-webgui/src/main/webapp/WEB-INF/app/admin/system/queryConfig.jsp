<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="queryConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}"/>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string" >
        <div class="content-area" id="queryConfigContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.editMode || view.createMode}">
                        <v:form name="queryConfigForm" url="${view.baseLink}/save">
                            <c:import url="_queryConfigEdit.jsp"/>
                        </v:form>
                    </c:when>
                    <c:when test="${!empty view.bean}">
                        <c:import url="_queryConfigDisplay.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_queryConfigList.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
