/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 11:28:01 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.SelectOption;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.PhoneUtil;

import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.OfficePhone;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Emails;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeDisciplinarian;
import com.osserp.core.employees.EmployeeDisplay;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.employees.EmployeeRoleDisplay;
import com.osserp.core.employees.EmployeeStatus;
import com.osserp.core.employees.EmployeeType;
import com.osserp.core.model.contacts.ContactImpl;
import com.osserp.core.model.contacts.OfficePhoneImpl;
import com.osserp.core.model.contacts.groups.EmployeeDisciplinarianImpl;
import com.osserp.core.model.contacts.groups.EmployeeDisciplinarianVO;
import com.osserp.core.model.contacts.groups.EmployeeDisplayVO;
import com.osserp.core.model.contacts.groups.EmployeeGroupImpl;
import com.osserp.core.model.contacts.groups.EmployeeImpl;
import com.osserp.core.model.contacts.groups.EmployeeStatusImpl;
import com.osserp.core.model.contacts.groups.EmployeeTypeImpl;
import com.osserp.core.model.system.BranchOfficeImpl;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.Permissions;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeesDao extends ClassifiedContactsDao implements Employees {
    private static Logger log = LoggerFactory.getLogger(EmployeesDao.class.getName());
    private static final String LIST =
            "employees.id,employees.contact_id," +
                    "contacts.salutation,contacts.firstname,contacts.lastname," +
                    "contacts.street,contacts.zipcode,contacts.city";
    private String employeesTable = null;
    private String employeeGroupsTable = null;
    private String employeeGroupPermissionsTable = null;
    private String employeeRolesTable = null;
    private String employeeRoleConfigsTable = null;
    private String employeeRoleDisplayView = null;
    private String usersTable = null;
    private String usersPermissionView = null;
    private SystemCompanies systemCompanies = null;
    private Map<Long, Employee> _employeesCache = Collections.synchronizedMap(new HashMap<Long, Employee>());

    /**
     * Creates a new employee dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param contacts
     * @param emails
     * @param namesCache
     * @param systemCompanies
     */
    public EmployeesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Contacts contacts,
            Emails emails,
            OptionsCache namesCache,
            SystemCompanies systemCompanies) {
        super(jdbcTemplate, tables, sessionFactory, contacts, emails, namesCache);
        this.employeesTable = getTable(TableKeys.EMPLOYEES);
        this.employeeGroupsTable = getTable(TableKeys.EMPLOYEE_GROUPS);
        this.employeeGroupPermissionsTable = getTable(TableKeys.EMPLOYEE_GROUP_PERMISSIONS);
        this.employeeRoleConfigsTable = getTable(TableKeys.EMPLOYEE_ROLE_CONFIGS);
        this.employeeRolesTable = getTable(TableKeys.EMPLOYEE_ROLES);
        this.employeeRoleDisplayView = getTable(TableKeys.EMPLOYEE_ROLE_DISPLAY);
        this.usersTable = getTable(TableKeys.USERS);
        this.usersPermissionView = getTable(TableKeys.USERS_PERMISSIONS_VIEW);
        this.systemCompanies = systemCompanies;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.impl.AbstractContactAwareDao#getEntityClass()
     */
    @Override
    protected Class<EmployeeImpl> getEntityClass() {
        return EmployeeImpl.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.impl.AbstractContactAwareDao#getEntityTable()
     */
    @Override
    protected String getEntityTable() {
        return employeesTable;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.ContactRelations#create(java.lang.Long, com.osserp.core.contacts.Contact, java.lang.Long)
     */
    public ClassifiedContact create(Long createdBy, Contact contact, Long customId) {
        if (customId != null && exists(customId)) {
            return loadEmployee(customId);
        }
        Long id = customId != null ? customId : getNextLongByTable(employeesTable);
        if (isEmpty(id)) {
            throw runtimeException(null, "Sequence not found: " + sequenceNameByTable(employeesTable));
        }
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [nextLongByTable=" + id + "]");
        }
        if (Constants.SYSTEM_NOBODY.equals(id)) {
            // prevent attempt to use the reserved id of nobody
            id = id + 1L;
        }
        EmployeeImpl obj = new EmployeeImpl(id, createdBy, contact, getDefaultType());
        if (obj.getRc() instanceof ContactImpl) {
            ContactImpl ci = (ContactImpl) obj.getRc();
            ci.setEmployee(true);
            ci.setOther(false);
            getCurrentSession().merge(ContactImpl.class.getName(), ci);
        }
        getCurrentSession().saveOrUpdate(EmployeeImpl.class.getName(), obj);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + obj.getId() + "]");
        }
        _employeesCache.put(obj.getId(), obj);
        return obj;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.ContactRelations#find(java.lang.Long)
     */
    public ClassifiedContact find(Long id) {
        try {
            return get(id);
        } catch (Throwable t) {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#get(java.lang.Long)
     */
    public Employee get(Long id) {
        return loadEmployee(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#findActiveEmployees()
     */
    public List<Employee> findActiveEmployees() {
        List<Employee> employees = new ArrayList<Employee>();
        List<Long> ids = findActiveIds();
        for (int i = 0, j = ids.size(); i < j; i++) {
            Employee next = loadEmployee(ids.get(i));
            if (!next.isClient() && !next.isBranchOffice()) {
                employees.add(next);
            }
        }
        return employees;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#findByGroup(java.lang.String)
     */
    public List<Employee> findByGroup(String groupKey) {
        List<Employee> employees = new ArrayList<Employee>();
        List<Long> ids = findIdsByGroup(groupKey);
        for (int i = 0, j = ids.size(); i < j; i++) {
            Employee next = loadEmployee(ids.get(i));
            if (!next.isClient() && !next.isBranchOffice()) {
                employees.add(next);
            }
        }
        return employees;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#findByPermission(java.lang.String)
     */
    public List<Employee> findByPermission(String permission) {
        StringBuilder query = new StringBuilder(64);
        query
                .append(" SELECT e.id FROM ")
                .append(usersTable).append(" u, ")
                .append(employeesTable)
                .append(" e WHERE u.contact_id = e.contact_id AND e.isactive AND ")
                .append(" u.id IN (SELECT id FROM ")
                .append(usersPermissionView)
                .append(" WHERE permission = ?)");
        final Object[] params = { permission };
        final int[] types = { Types.VARCHAR };
        List<Long> ids = (List) jdbcTemplate.query(query.toString(), params, types, getLongRowMapper());
        List<Employee> employees = new ArrayList<Employee>();
        for (int i = 0, j = ids.size(); i < j; i++) {
            Employee next = loadEmployee(ids.get(i));
            if (!next.isClient() && !next.isBranchOffice()) {
                employees.add(next);
            }
        }
        return employees;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#findActiveNames()
     */
    public List<Option> findActiveNames() {
        List<Employee> employees = findActiveEmployees();
        List<Option> result = new ArrayList<Option>();
        for (int i = 0, j = employees.size(); i < j; i++) {
            Employee next = employees.get(i);
            if (!next.isClient() && !next.isBranchOffice()) {
                result.add(new OptionImpl(next.getId(), next.getDisplayName()));
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#findUserIdsByGroup(java.lang.String)
     */
    public List<Long> findUserIdsByGroup(String groupKey) {
        StringBuilder query = new StringBuilder(64);
        query
                .append(" SELECT u.id FROM ")
                .append(usersTable).append(" u, ")
                .append(employeesTable)
                .append(" e WHERE u.contact_id = e.contact_id AND e.isactive = true");

        if (groupKey != null) {
            query
                    .append(" AND e.id IN ")
                    .append(createGroupQuerySelection(groupKey));
        }
        return (List<Long>) jdbcTemplate.query(query.toString(), getLongRowMapper());
    }

    public List<BusinessCaseRelationStat> filterBySupportedBranch(List<BusinessCaseRelationStat> list, DomainUser domainUser) {
        Employee user = domainUser.getEmployee();
        if (user.isIgnoringBranch() || domainUser.isPermissionGrant(Permissions.BRANCH_IGNORE_PERMISSIONS)) {
            return list;
        }
        Set<Long> added = new java.util.HashSet<Long>();
        List<BusinessCaseRelationStat> result = new ArrayList<BusinessCaseRelationStat>();
        boolean userIsCompanyExecutive = user.isCompanyExecutive();
        for (int i = 0, j = list.size(); i < j; i++) {
            BusinessCaseRelationStat capacityDisplay = list.get(i);
            Employee resultingEmployee = get(capacityDisplay.getId());
            for (int k = 0, l = resultingEmployee.getRoleConfigs().size(); k < l; k++) {
                EmployeeRoleConfig resultsRole = resultingEmployee.getRoleConfigs().get(k);

                for (int m = 0, n = user.getRoleConfigs().size(); m < n; m++) {
                    EmployeeRoleConfig usercfg = user.getRoleConfigs().get(m);
                    if (userIsCompanyExecutive) {
                        if (usercfg.getBranch().getCompany().getId().equals(resultsRole.getBranch().getCompany().getId())
                                && !added.contains(resultingEmployee.getId())) {
                            result.add(capacityDisplay);
                            added.add(capacityDisplay.getId());
                        }
                    } else {
                        if (usercfg.getBranch().getId().equals(resultsRole.getBranch().getId()) && !added.contains(resultingEmployee.getId())) {
                            result.add(capacityDisplay);
                            added.add(capacityDisplay.getId());
                        }
                    }
                }
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#findShortKeys()
     */
    public List<Option> findShortKeys() {
        // TODO read from cache
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT id,shortkey,isactive FROM ")
                .append(employeesTable);
        return (List<Option>) jdbcTemplate.query(query.toString(), new RowMapperResultSetExtractor(new ShortKeyRowMapper()));
    }

    private class ShortKeyRowMapper extends AbstractRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new OptionImpl(fetchLong(rs, "id"), fetchString(rs, "shortkey"), !this.fetchBoolean(rs, "isactive"));
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#findDisciplinarians(com.osserp.core.employees.Employee)
     */
    public List<EmployeeDisciplinarian> findDisciplinarians(Employee employee) {
        List<EmployeeDisciplinarian> result = new ArrayList<EmployeeDisciplinarian>();
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(EmployeeDisciplinarianImpl.class.getName())
                .append(" o where o.reference = :employeeId");
        List<EmployeeDisciplinarianImpl> ds = getCurrentSession().createQuery(hql.toString())
                .setParameter("employeeId", employee.getId()).list();
        for (int i = 0, j = ds.size(); i < j; i++) {
            EmployeeDisciplinarianImpl next = ds.get(i);
            result.add(new EmployeeDisciplinarianVO(next, loadEmployee(next.getDisciplinarianId())));
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#addDisciplinarian(com.osserp.core.employees.Employee, com.osserp.core.employees.Employee,
     * com.osserp.core.employees.Employee)
     */
    public void addDisciplinarian(Employee user, Employee employee, Employee disciplinarian) throws ClientException {
        boolean exists = false;
        boolean defaultExists = false;
        List<EmployeeDisciplinarian> existing = findDisciplinarians(employee);
        for (int i = 0, j = existing.size(); i < j; i++) {
            EmployeeDisciplinarian next = existing.get(i);
            if (next.getDisciplinarian().getId().equals(disciplinarian.getId())) {
                exists = true;
            }
            if (employee.getDefaultDisciplinarian() != null && employee.getDefaultDisciplinarian().equals(next.getId())) {
                defaultExists = true;
            }
        }
        if (!exists) {
            EmployeeDisciplinarianImpl obj = new EmployeeDisciplinarianImpl(user.getId(), employee.getId(), disciplinarian.getId());
            getCurrentSession().saveOrUpdate(EmployeeDisciplinarianImpl.class.getName(), obj);
            if (existing.isEmpty() || (employee.getDefaultDisciplinarian() != null && !defaultExists)) {
                employee.setDefaultDisciplinarian(disciplinarian.getId());
                save(employee);
            }
        } else if (employee.getDefaultDisciplinarian() == null) {
            employee.setDefaultDisciplinarian(disciplinarian.getId());
            save(employee);
        } else if (employee.getDefaultDisciplinarian() != null && !defaultExists) {
            employee.setDefaultDisciplinarian(null);
            save(employee);
        }
        _employeesCache.put(employee.getId(), employee);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#removeDisciplinarian(com.osserp.core.employees.Employee, com.osserp.core.employees.EmployeeDisciplinarian)
     */
    public void removeDisciplinarian(Employee user, EmployeeDisciplinarian disciplinarian) throws ClientException {
        try {
            getCurrentSession().delete(EmployeeDisciplinarianImpl.class.getName(), disciplinarian);
            Employee employee = load(disciplinarian.getReference());
            List<EmployeeDisciplinarian> existing = findDisciplinarians(employee);
            if (employee.getDefaultDisciplinarian() != null) {
                if (employee.getDefaultDisciplinarian().equals(disciplinarian.getId())) {
                    employee.setDefaultDisciplinarian(null);
                    if (!existing.isEmpty()) {
                        EmployeeDisciplinarian first = existing.get(0);
                        employee.setDefaultDisciplinarian(first.getId());
                    }
                    save(employee);
                } else {
                    boolean exists = false;
                    for (int i = 0, j = existing.size(); i < j; i++) {
                        EmployeeDisciplinarian next = existing.get(i);
                        if (next.getDisciplinarian().getId().equals(employee.getDefaultDisciplinarian())) {
                            exists = true;
                            break;
                        }
                    }
                    if (!exists) {
                        employee.setDefaultDisciplinarian(null);
                        if (!existing.isEmpty()) {
                            EmployeeDisciplinarian first = existing.get(0);
                            employee.setDefaultDisciplinarian(first.getId());
                        }
                        save(employee);
                    }
                }
            } else {
                if (!existing.isEmpty()) {
                    EmployeeDisciplinarian first = existing.get(0);
                    employee.setDefaultDisciplinarian(first.getId());
                    save(employee);
                }
            }
            _employeesCache.put(employee.getId(), employee);
        } catch (Exception e) {
            log.warn("removeDisciplinarian() ignored exception [user=" + user.getId()
                    + ", employee=" + (disciplinarian == null ? "null" : disciplinarian.getReference())
                    + ", disciplinarian=" + (disciplinarian == null ? "null" : disciplinarian.getId())
                    + ", message=" + e.getMessage()
                    + ", exception=" + e.getClass().getName()
                    + "]");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#getEmployeeByUser(java.lang.Long)
     */
    public Long getEmployeeByUser(Long userId) {
        if (userId == null) {
            return null;
        }
        final Object[] params = { userId };
        final int[] types = { Types.BIGINT };
        List<Long> result = (List<Long>) jdbcTemplate.query(
                Procs.EMPLOYEE_BY_USER,
                params,
                types,
                getLongRowMapper());
        return (empty(result) ? null : (Long) result.get(0));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#findActive()
     */
    public List<EmployeeDisplay> findActive() {
        return getCurrentSession().createQuery("from "
                + EmployeeDisplayVO.class.getName()
                + " e where e.active = true").list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#getEmail(java.lang.Long)
     */
    public String getEmail(Long employeeId) {
        if (employeeId == null) {
            return null;
        }
        Long id = employeeId;
        if (employeeId < Constants.SYSTEM_EMPLOYEE) {
            id = getEmployeeByUser(employeeId);
        }
        final Object[] params = { id };
        final int[] types = { Types.BIGINT };
        List<String> result = (List<String>) jdbcTemplate.query(
                Procs.EMPLOYEE_EMAIL,
                params,
                types,
                getStringRowMapper());
        return (empty(result) ? null : result.get(0));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#findEmailsAndNames(java.util.Set)
     */
    public List<SelectOption> findEmailsAndNames(Set<String> ignorable) {
        List<SelectOption> result = new ArrayList<SelectOption>();
        StringBuilder query = new StringBuilder(64);
        query
                .append("select c.firstname, c.lastname, em.email from ")
                .append(contactsTable)
                .append(" c left outer join ")
                .append(emailTable)
                .append(" em on em.contact_id = c.contact_id ")
                .append("where em.isprimary = true and c.status_id >= 0 and c.contact_id in ")
                .append("(select contact_id from ")
                .append(employeesTable)
                .append(" where isactive = true) order by lastname");
        List<SelectOption> qresult = (List<SelectOption>) jdbcTemplate.query(
                query.toString(),
                new RowMapperResultSetExtractor(new EmailAndNameRowMapper(ignorable)));
        for (int i = 0, j = qresult.size(); i < j; i++) {
            SelectOption option = qresult.get(i);
            if (option.getProperty() != null) {
                result.add(option);
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#getSalesCapacity(java.lang.Long)
     */
    public List<BusinessCaseRelationStat> getSalesCapacity(Long employee) {
        if (employee == null) {
            return (List<BusinessCaseRelationStat>) jdbcTemplate.query(
                    BusinessCaseRelationStatRowMapper.SALE_SQL,
                    BusinessCaseRelationStatRowMapper.getReader());
        }
        return (List<BusinessCaseRelationStat>) jdbcTemplate.query(
                BusinessCaseRelationStatRowMapper.SALE_SQL + " where id = " + employee,
                BusinessCaseRelationStatRowMapper.getReader());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#getTecCapacity(java.lang.Long)
     */
    public List<BusinessCaseRelationStat> getTecCapacity(Long employee) {
        if (employee == null) {
            return (List<BusinessCaseRelationStat>) jdbcTemplate.query(
                    BusinessCaseRelationStatRowMapper.TEC_SQL,
                    BusinessCaseRelationStatRowMapper.getReader());
        }
        return (List<BusinessCaseRelationStat>) jdbcTemplate.query(
                BusinessCaseRelationStatRowMapper.TEC_SQL + " where id = " + employee,
                BusinessCaseRelationStatRowMapper.getReader());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#getOfficePhones(java.lang.Long)
     */
    public List<OfficePhone> getOfficePhones(Long branchId) {
        if (branchId == null) {
            return getCurrentSession().createQuery("from " + OfficePhoneImpl.class.getName()
                    + " p order by p.number").list();
        }
        return getCurrentSession().createQuery("from " + OfficePhoneImpl.class.getName()
                + " p where p.branchId = :branchId order by p.number").setParameter("branchId", branchId).list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Employees#create(java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long, java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.Long)
     */
    public OfficePhone createOfficePhoneNumber(
            Long userId,
            Long contactId,
            Long device,
            Long type,
            String country,
            String prefix,
            String number,
            String directDial,
            Long branchId) throws ClientException {
        if (contactId == null) {
            throw new IllegalArgumentException("contactId must not be null");
        }
        OfficePhoneImpl obj = new OfficePhoneImpl(
                userId,
                contactId,
                device,
                type,
                country,
                prefix,
                number,
                directDial,
                branchId);
        if (PhoneType.FAX.equals(device)) {
            PhoneUtil.validateFax(obj.getPrefix(), obj.getNumber(), true);
        } else if (PhoneType.MOBILE.equals(device)) {
            PhoneUtil.validateMobile(obj.getPrefix(), obj.getNumber(), true);
        } else {
            PhoneUtil.validatePhone(obj.getPrefix(), obj.getNumber(), true);
        }
        String newNumber = PhoneUtil.createPhoneNumber(obj.getCountry(), obj.getPrefix(), obj.getNumber());
        List<OfficePhone> existing = getOfficePhones(branchId);
        for (int i = 0, j = existing.size(); i < j; i++) {
            OfficePhone next = existing.get(i);
            if (newNumber.equals(PhoneUtil.createPhoneNumber(next.getCountry(), next.getPrefix(), next.getNumber()))) {
                throw new ClientException(ErrorCode.PHONE_EXISTING);
            }
        }
        save(obj);
        return obj;
    }

    public void save(OfficePhone phone) {
        getCurrentSession().saveOrUpdate(OfficePhoneImpl.class.getName(), phone);
    }

    public List<EmployeeGroup> getEmployeeGroups() {
        return getCurrentSession().createQuery("from " + EmployeeGroupImpl.class.getName()
                + " o order by o.name").list();
    }

    public EmployeeGroup getEmployeeGroup(Long id) {
        try {
            return (EmployeeGroup) getCurrentSession().get(EmployeeGroupImpl.class.getName(), id);
        } catch (Throwable t) {
            if (id != null) {
                log.warn("getEmployeeGroup() not found [id=" + id + ", message=" + t.getMessage() + "]");
            }
            return null;
        }
    }

    public boolean exists(EmployeeGroup group) {
        EmployeeGroup existing = getEmployeeGroup(group.getId());
        return (existing != null);
    }

    public EmployeeGroup createGroup(Employee user, String name, String key) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("createGroup() invoked [user=" + user.getId()
                    + ", name=" + name + ", key=" + key + "]");
        }
        if (isEmpty(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (isEmpty(key)) {
            throw new ClientException(ErrorCode.SHORTKEY_MISSING);
        }
        List<EmployeeGroup> existing = getEmployeeGroups();
        for (int i = 0, j = existing.size(); i < j; i++) {
            EmployeeGroup next = existing.get(i);
            if (next.getName().equalsIgnoreCase(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
            if (!isEmpty(next.getKey()) && next.getKey().equalsIgnoreCase(key)) {
                throw new ClientException(ErrorCode.SHORTKEY_EXISTS);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("createGroup() validation ok, trying to create...");
        }
        EmployeeGroup created = new EmployeeGroupImpl(user, name, key);
        save(created);
        if (log.isDebugEnabled()) {
            log.debug("createGroup() done [id=" + created.getId() + "]");
        }
        return created;
    }

    public void delete(EmployeeGroup group) {
        StringBuilder deleteRoles = new StringBuilder("DELETE FROM ");
        deleteRoles
                .append(employeeRolesTable)
                .append(" WHERE group_id = ")
                .append(group.getId());
        int count = jdbcTemplate.update(deleteRoles.toString());
        if (log.isDebugEnabled()) {
            log.debug("delete() associated roles removed [count=" + count + "]");
        }
        this.systemCompanies.removeGroup(group.getId());
        this.removeGroupFromBranchs(group.getId());
        getCurrentSession().delete(group);
        if (log.isDebugEnabled()) {
            log.debug("delete() done [id=" + group.getId()
                    + ", name=" + group.getName() + "]");
        }
    }

    public void save(EmployeeGroup group) {
        getCurrentSession().saveOrUpdate(EmployeeGroupImpl.class.getName(), group);
    }

    protected void save(BranchOffice office) {
        getCurrentSession().saveOrUpdate(BranchOfficeImpl.class.getName(), office);
    }

    public List<EmployeeGroup> getFcsAwareEmployeeGroups() {
        return getCurrentSession().createQuery("from " + EmployeeGroupImpl.class.getName()
                + " o where o.flowControlAware = true order by o.id").list();
    }

    public List<Option> findGroupsByPermission(String permission) {
        List<Option> result = new ArrayList<Option>();
        StringBuilder sql = new StringBuilder("SELECT id,name FROM ");
        sql
                .append(employeeGroupsTable)
                .append(" WHERE id IN (SELECT reference_id FROM ")
                .append(employeeGroupPermissionsTable)
                .append(" WHERE name = '")
                .append(permission)
                .append("') ORDER BY name");
        try {
            result = (List<Option>) jdbcTemplate.query(sql.toString(), getOptionRowMapper());
        } catch (Throwable t) {
            log.error("findGroupsByPermission() failed [message=" + t.getMessage() + "]");
        }
        return result;
    }

    public Map<Long, EmployeeGroup> getLdapGroupMap() {
        Map<Long, EmployeeGroup> result = new HashMap<Long, EmployeeGroup>();
        List<EmployeeGroup> groups = getEmployeeGroups();
        for (int i = 0, j = groups.size(); i < j; i++) {
            EmployeeGroup next = groups.get(i);
            if (next.getLdapGroup() != null) {
                result.put(next.getLdapGroup(), next);
            } else if (log.isDebugEnabled()) {
                log.debug("getLdapGroupMap() ignoring not referenced [id="
                        + next.getId() + ", name=" + next.getName() + "]");
            }
        }
        return result;
    }

    public List<Option> getEmployeeGroupKeys() {
        List<Option> result = new ArrayList<Option>();
        List<EmployeeGroup> groups = getEmployeeGroups();
        for (int i = 0, j = groups.size(); i < j; i++) {
            EmployeeGroup next = groups.get(i);
            result.add(new OptionImpl(next.getId(), next.getKey()));
        }
        return result;
    }

    public List<BranchOffice> getBranchOffices() {
        try {
            List<BranchOffice> list = getCurrentSession().createQuery("from " + BranchOfficeImpl.class.getName()
                    + " o order by o.name").list();
            return list;
        } catch (Throwable t) {
            log.error("getBranchOffices() failed [message=" + t.getMessage() + "]", t);
            throw new BackendException(t);
        }
    }

    public List<EmployeeStatus> getEmployeeStatus() {
        return getCurrentSession().createQuery("from " + EmployeeStatusImpl.class.getName()
                + " o order by o.name").list();
    }

    public List<EmployeeType> getEmployeeTypes() {
        return getCurrentSession().createQuery("from " + EmployeeTypeImpl.class.getName()
                + " o order by o.name").list();
    }

    public EmployeeType getEmployeeType(Long id) {
        return (EmployeeType) getCurrentSession().load(EmployeeTypeImpl.class.getName(), id);
    }

    public EmployeeType getDefaultType() {
        List<EmployeeType> all = getCurrentSession().createQuery("from " + EmployeeTypeImpl.class.getName()
                + " o where o.defaultType = true").list();
        return all.isEmpty() ? null : all.get(0);
    }

    public List<EmployeeRoleDisplay> getEmployeeRoleDisplay() {
        StringBuilder sql = new StringBuilder(128);
        sql
                .append(EmployeeRoleDisplayRowMapper.SELECT_ALL_FROM)
                .append(employeeRoleDisplayView);
        if (log.isDebugEnabled()) {
            log.debug("getEmployeeRoleDisplay() executing [sql=" + sql.toString() + "]");
        }
        return (List<EmployeeRoleDisplay>) jdbcTemplate.query(sql.toString(), EmployeeRoleDisplayRowMapper.getReader());
    }

    protected void removeGroupFromBranchs(Long groupId) {
        List<BranchOffice> list = getBranchOffices();
        for (int i = 0, j = list.size(); i < j; i++) {
            BranchOffice office = list.get(i);
            if (office.removeGroup(groupId)) {
                save(office);
            }
        }
    }

    /**
     * Provides the basic statement required for queries
     * @return query
     */
    @Override
    protected StringBuilder getQuery() {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT ")
                .append(LIST)
                .append(" FROM ")
                .append(employeesTable)
                .append(",")
                .append(contactsTable)
                .append(" WHERE employees.contact_id = contacts.contact_id");
        return query;
    }

    /**
     * Provides the statement required for ordered queries
     * @return orderedQuery
     */
    @Override
    protected String getOrderedQuery() {
        StringBuilder query = getQuery();
        query.append(" ORDER BY lastname,firstname");
        return query.toString();
    }

    private String createGroupQuerySelection(String groupKey) {
        StringBuilder query = new StringBuilder(64);
        if (groupKey != null) {
            query
                    .append(" (SELECT reference_id FROM ")
                    .append(employeeRoleConfigsTable)
                    .append(" WHERE id IN (SELECT reference_id FROM ")
                    .append(employeeRolesTable)
                    .append(" WHERE group_id IN (SELECT id FROM ")
                    .append(employeeGroupsTable)
                    .append(" WHERE is_")
                    .append(groupKey)
                    .append(" = true)))");
        }
        return query.toString();
    }

    private class EmailAndNameRowMapper implements RowMapper {

        private Set<String> ignorable = null;

        public EmailAndNameRowMapper(Set<String> ignorable) {
            this.ignorable = ignorable;
        }

        public Object mapRow(ResultSet rs, int index) throws SQLException {
            String firstname = rs.getString(1);
            String lastname = rs.getString(2);
            String email = rs.getString(3);
            if (email != null) {
                if (ignorable != null && !ignorable.contains(email)) {
                    StringBuffer name = new StringBuffer(64);
                    name.append(lastname);
                    if (firstname != null) {
                        name.append(", ").append(firstname);
                    }
                    return new SelectOption(email, name.toString());
                }
            }
            return new SelectOption(null, null);
        }
    }

    public void reload(Long id) {
        this._employeesCache.put(id, load(id));
    }

    private Employee loadEmployee(Long id) {
        if (id == null || id == 0) {
            throw new IllegalArgumentException("load invoked with null or 0 arg");
        }
        Long empId = findId(id);
        Employee obj = null;
        if (!_employeesCache.containsKey(empId)) {
            obj = load(empId);
            if (obj != null) {
                _employeesCache.put(obj.getId(), obj);
            } else {
                log.info("loadEmployee() invoked for not existing entity [id=" + id + "]");
            }

        } else {
            obj = _employeesCache.get(empId);
        }
        return obj;
    }

    private Employee load(Long id) {
        return (id == null ? null : (Employee) getCurrentSession().load(EmployeeImpl.class, id));
    }

    private List<Long> findActiveIds() {
        StringBuilder sql = new StringBuilder(196);
        sql
                .append("SELECT e.id FROM ")
                .append(employeesTable)
                .append(" e,")
                .append(contactsTable)
                .append(" c WHERE c.status_id >= 0 AND e.contact_id = c.contact_id AND e.isactive = true")
                .append(" ORDER BY c.lastname");
        return jdbcTemplate.queryForList(sql.toString(), Long.class);
    }

    private List<Long> findIdsByGroup(String groupKey) {
        StringBuilder query = new StringBuilder(128);
        query
                .append("SELECT e.id FROM ")
                .append(employeesTable)
                .append(" e,")
                .append(contactsTable)
                .append(" c WHERE c.status_id >= 0 AND e.isactive = true AND e.contact_id = c.contact_id");

        if (groupKey != null) {
            query
                    .append(" AND e.id IN ")
                    .append(createGroupQuerySelection(groupKey));
        }
        return (List<Long>) jdbcTemplate.query(query.toString(), getLongRowMapper());
    }

    private Long findId(Long id) {
        StringBuilder sql = new StringBuilder(196);
        sql
                .append("SELECT e.id FROM ")
                .append(employeesTable)
                .append(" e,")
                .append(contactsTable)
                .append(" c WHERE e.contact_id = c.contact_id AND (e.id = ? OR e.contact_id = ?)");
        final Object[] params = { id, id };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        List<Long> result = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper());
        Long employeeId = null;
        if (empty(result)) {
            employeeId = getEmployeeByUser(id);
        } else {
            employeeId = result.get(0);
        }
        return employeeId;
    }
}
