/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 26, 2006 9:36:29 AM 
 * 
 */
package com.osserp.gui.web.contacts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.osserp.common.ClientException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;

import com.osserp.core.contacts.Contact;
import com.osserp.core.users.DomainUser;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.contacts.ContactSettingsView;
import com.osserp.gui.client.contacts.ContactView;
import com.osserp.gui.client.contacts.CustomerView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactAction extends AbstractContactAction {
    private static Logger log = LoggerFactory.getLogger(ContactAction.class.getName());

    /**
     * Loads a contact for the first time after a successfully contactCreator workflow.
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward loadCreated(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("loadCreated() request from " + getUser(session).getId());
        }
        String exit = RequestUtil.getExit(httpRequest);
        if (exit == null) {
            exit = "contactSearch";
        }
        String exitId = RequestUtil.getExitId(httpRequest);
        Long id = RequestUtil.getId(httpRequest);
        ContactView view = (ContactView) createView(httpRequest);
        view.load(id);
        if (((Contact) view.getBean()).isContactPerson()) {
            ActionRedirect redir = new ActionRedirect(mapping.findForwardConfig("contactPersons"));
            redir.addParameter("method", "load");
            redir.addParameter("id", id);
            redir.addParameter("exit", exit);
            if (isSet(exitId)) {
                redir.addParameter("exitId", exitId);
            }
            if (log.isDebugEnabled()) {
                log.debug("loadCreated() found contactPerson, redirecting to person action [contactId=" + id + "]");
            }
            return redir;
        }
        view.setExitTarget(exit);
        view.setAssignMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Loads contact by request param 'id'
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward load(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("load() request from " + getUser(session).getId());
        }
        String exit = RequestUtil.getExit(httpRequest);
        String exitId = RequestUtil.getExitId(httpRequest);
        Long id = RequestUtil.getId(httpRequest);
        ContactView view = (ContactView) createView(httpRequest);
        view.load(id);
        if (((Contact) view.getBean()).isContactPerson()) {
            ActionRedirect redir = new ActionRedirect(mapping.findForwardConfig("contactPersons"));
            redir.addParameter("method", "load");
            redir.addParameter("id", id);
            if (exit != null) {
                redir.addParameter("exit", exit);
            }
            if (isSet(exitId)) {
                redir.addParameter("exitId", exitId);
            }
            if (log.isDebugEnabled()) {
                log.debug("load() found contactPerson, redirecting to person action [contactId=" + id + "]");
            }
            return redir;
        }
        if (isSet(exit)) {
            view.setExitTarget(exit);
        }
        if (isSet(exitId)) {
            view.setExitId(exitId);
        }
        return mapping.findForward(findDisplayTarget(view));
    }

    private String findDisplayTarget(ContactView view) {

        if (view.getBean() instanceof Contact) {
            Contact contact = (Contact) view.getBean();
            if (!contact.isClient()) {
                if (contact.getDefaultGroupDisplay() != null) {
                    if (Contact.CUSTOMER_KEY.equals(contact.getDefaultGroupDisplay())) {
                        return Actions.CUSTOMER_DISPLAY;
                    }
                    if (Contact.EMPLOYEE_KEY.equals(contact.getDefaultGroupDisplay())) {
                        return Actions.EMPLOYEE_DISPLAY;
                    }
                    if (Contact.SUPPLIER_KEY.equals(contact.getDefaultGroupDisplay())) {
                        return Actions.SUPPLIER_DISPLAY;
                    }
                }
                if (contact.isEmployee()) {
                    return Actions.EMPLOYEE_DISPLAY;
                }
                if (contact.isCustomer()) {
                    if (contact.isSupplier()) {
                        if (view.getUser() instanceof DomainUser) {
                            DomainUser user = (DomainUser) view.getUser();
                            if (user.isPurchaser() || user.isLogistics()) {
                                return Actions.SUPPLIER_DISPLAY;
                            }
                        }
                    }
                    return Actions.CUSTOMER_DISPLAY;
                }
                if (contact.isSupplier()) {
                    return Actions.SUPPLIER_DISPLAY;
                }
            }
        }
        return view.getActionTarget();
    }

    /**
     * Enables assignment mode
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enableAssignment(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableAssignment() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        view.setAssignMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to settings display
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardSettings(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardSettings() request from " + getUser(session).getId());
        }
        ContactSettingsView view = (ContactSettingsView) createView(httpRequest, ContactSettingsView.class.getName());
        view.init(httpRequest);
        return mapping.findForward(Actions.SETTINGS);
    }

    /**
     * Returns from settings display
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward exitSettings(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitSettings() request from " + getUser(session).getId());
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Forwards to user salutations
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardSalutationConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardSalutationConfig() request from " + getUser(session).getId());
        }
        return mapping.findForward(Actions.SALUTATION_CONFIG);
    }

    /**
     * Updates the user salutations and reloads parent
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward updateUserSalutations(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardSalutationConfig() request from " + getUser(session).getId());
        }
        ContactSettingsView view = getContactSettingsView(session);
        view.updateUserSalutations(new StrutsForm(form));
        return mapping.findForward(Actions.PARENT_RELOAD_POPUP);
    }

    /**
     * Disables assignment mode
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward disableAssignment(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableAssignment() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        view.setAssignMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables type change mode
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enableTypeChange(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableTypeChange() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        view.setTypeChangeMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Changes type
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward changeType(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeChange() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        try {
            view.changeType(new StrutsForm(form));
        } catch (ClientException e) {
            saveError(httpRequest, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes a contact if possible
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("delete() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        try {
            view.delete();
            return mapping.findForward("contactSearch");
        } catch (ClientException e) {
            return saveErrorAndReturn(httpRequest, e.getMessage());
        }
    }

    /**
     * Enables current contact as private contact
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enablePrivateContact(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enablePrivateContact() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        view.enablePrivateContact();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables a previously enabled private contact
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward disablePrivateContact(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disablePrivateContact() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        view.disablePrivateContact();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Exits display
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward exitDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitDisplay() request from " + getUser(session).getId());
        }
        ContactView view = fetchContactView(session);
        if (view == null) {
            if (log.isDebugEnabled()) {
                log.debug("exitDisplay() no contact view found, returning to search");
            }
            return mapping.findForward(Actions.SEARCH);
        }
        if (view.isExitIdAvailable()) {
            RequestUtil.saveExitId(httpRequest, view.getExitId());
        }
        if (view.isExitTargetAvailable()) {
            String exitTarget = view.getExitTarget();
            removeView(session);
            if (log.isDebugEnabled()) {
                log.debug("exitDisplay() found exit target [target=" + exitTarget + "]");
            }
            return mapping.findForward(exitTarget);
        }
        if (view.isCustomerView()) {
            if (log.isDebugEnabled()) {
                log.debug("exitDisplay() contact view is customer view...");
            }
            if (((CustomerView) view).isRequestMode()) {
                if (log.isDebugEnabled()) {
                    log.debug("exitDisplay() customer view is in request mode, forward is plan");
                }
                removeView(session);
                return mapping.findForward(Actions.PLAN);
            } else if (((CustomerView) view).isSaleMode()) {
                if (log.isDebugEnabled()) {
                    log.debug("exitDisplay() customer view is in sale mode, forward is project");
                }
                removeView(session);
                return mapping.findForward(Actions.PROJECT);
            } else if (log.isDebugEnabled()) {
                log.debug("exitDisplay() no interesting mode on customer view...");
            }
        }
        removeView(session);
        if (log.isDebugEnabled()) {
            log.debug("exitDisplay() no target found using default [target=index]");
        }
        return mapping.findForward(Actions.INDEX);
    }

}
