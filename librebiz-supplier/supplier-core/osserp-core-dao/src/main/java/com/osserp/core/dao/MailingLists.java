/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2007 9:09:53 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.mail.MailingList;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface MailingLists {

    /**
     * Indicates if mail id exists in mailing list
     * @param mailId
     * @return true if so
     */
    boolean exists(Long mailId);

    /**
     * Finds all mailing lists ordered by name
     * @return lists
     */
    List<MailingList> findAll();

    /**
     * Finds activated mailing lists ordered by name
     * @return lists
     */
    List<MailingList> findActivated();

    /**
     * Creates a new mailing list
     * @param createdBy
     * @param owner id when list is not public, null for common access
     * @param name
     * @param description
     * @return new created mailingList
     * @throws ClientException if name is empty or already existing
     */
    MailingList create(Long createdBy, Long owner, String name, String description)
            throws ClientException;

    /**
     * Persists a mailing list
     * @param list
     */
    void save(MailingList list);

    /**
     * Removes an email address from all lists
     * @param emailAddressId
     */
    void removeAddress(Long emailAddressId);

}
