<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.purchaseInvoiceView or empty sessionScope.purchaseInvoiceView.bean}">
	<o:logger write="no view bound" level="warn"/>
	<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.purchaseInvoiceView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="baseUrl" scope="request" value="/purchaseInvoice.do"/>
<c:if test="${record.itemsChangeable or record.itemsEditable}">
	<c:set var="productUrl" scope="request" value="/purchaseInvoiceProducts.do"/>
</c:if>
<c:set var="productExitTarget" scope="request" value="purchaseInvoiceReload"/>
<c:set var="displayPrice" scope="request" value="false"/>
<o:permission role="purchasing,accounting,executive" info="permissionDisplayPrice">
	<c:set var="displayPrice" scope="request" value="true"/>
</o:permission>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records.css"/>
</style>
</tiles:put>

<tiles:put name="headline"><o:out value="${record.contact.id}"/> - <o:out value="${record.contact.displayName}"/></tiles:put>
<c:choose>
<c:when test="${recordView.setupMode}">
<tiles:put name="headline_right">
<c:if test="${!recordView.deleteClosedMode}">
    <a href="<c:url value="/purchaseInvoice.do?method=disableSetup"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
</c:if>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>
<tiles:put name="content" type="string">
<c:import url="/pages/purchasing/invoice_settings.jsp"/>
</tiles:put>
</c:when>
<c:otherwise>

<tiles:put name="headline_right">
	<a href="<c:url value="/purchaseInvoice.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
    <c:if test="${record.unchangeable}">
        <o:permission role="accounting,executive" info="permissionPaymentAdd">
            <c:if test="${record.bookingType.supportingPayments}">
                <v:link url="/purchasing/purchaseInvoicePayment/forward" title="paymentsEdit"><o:img name="moneyIcon" /></v:link>
            </c:if>
        </o:permission>
        <c:choose>
            <c:when test="${recordView.exitTargetAvailable}">
                <v:link url="/purchasing/purchaseInvoiceCreator/forward?copy=${record.id}&recordExit=${recordView.exitTarget}&exit=/purchaseInvoice.do?method=redisplay" title="createByCopy">
                    <o:img name="copyIcon"/>
                </v:link>
            </c:when>
            <c:otherwise>
                <v:link url="/purchasing/purchaseInvoiceCreator/forward?copy=${record.id}&exit=/purchaseInvoice.do?method=redisplay" title="createByCopy">
                    <o:img name="copyIcon"/>
                </v:link>
            </c:otherwise>
        </c:choose>
    </c:if>
    <v:link url="/purchasing/purchaseInvoiceDocuments/forward?exit=/purchaseInvoice.do?method=redisplay" title="documents"><o:img name="docIcon" /></v:link>
    <c:choose>
        <c:when test="${record.bookingType.printable}">
            <c:if test="${!record.unchangeable and !empty record.items}">
                <v:link url="/records/recordPrint/release?view=purchaseInvoiceView" confirm="true" message="confirmRecordRelease" title="recordRelease"><o:img name="enabledIcon" /></v:link>
            </c:if>
            <v:link url="/records/recordPrint/print?view=purchaseInvoiceView" title="documentPrint"><o:img name="printIcon" /></v:link>
        </c:when>
        <c:otherwise>
            <c:if test="${!empty recordView.document}">
                <v:link url="/records/recordPrint/print?view=purchaseInvoiceView" title="documentPrint"><o:img name="printIcon" /></v:link>
            </c:if>
        </c:otherwise>
    </c:choose>
	<c:if test="">
	</c:if>
	<c:if test="${!recordView.readOnlyMode and !record.unchangeable and (!recordView.recordReopened or record.creditNote)}">
	    <a href="javascript:onclick=confirmLink('<fmt:message key="confirmRecordDelete"/>','<c:url value="/purchaseInvoice.do?method=delete"/>');" title="<fmt:message key="recordDelete"/>"><o:img name="deleteIcon" /></a>
	</c:if>
	<c:if test="${!record.unchangeable}">
		<a href="<c:url value="/purchaseInvoice.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon" /></a>
	</c:if>
	<c:if test="${!record.unchangeable and !record.bookingType.printable}">
		<a href="javascript:onclick=confirmLink('<fmt:message key="confirmRecordRelease"/>','<c:url value="/purchaseInvoice.do?method=release"/>');" title="<fmt:message key="recordRelease"/>"><o:img name="enabledIcon" /></a>
	</c:if>
	<c:if test="${record.unchangeable}">
		<o:permission role="executive,purchase_invoice_change,purchase_invoice_delete" info="permissionEnableSetup">
			<a href="<c:url value="/purchaseInvoice.do?method=enableSetup"/>" title="<fmt:message key="enhancedFunctionality"/>"><o:img name="configureIcon" /></a>
		</o:permission>
	</c:if>
	<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                    
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
                    <div class="column66l">
                        <c:import url="/pages/records/record_branch_display.jsp" />
                        <c:import url="/pages/records/record_custom_header_display.jsp"/>
                        <c:choose>
                            <c:when test="${empty record.reference}">
                                <div class="recordLabelValue">
                                    <fmt:message key="stockReceiptWithoutOrder"/>
                                </div>
                            </c:when>
                            <c:when test="${record.bookingType.credit}">
                                <div class="recordLabelValue">
                                    <fmt:message key="invoice"/>:
                                    <a href="<c:url value="/purchaseInvoice.do?method=loadOther&id=${record.reference}"/>"><o:out value="${record.referenceNumber}"/></a>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="recordLabelValue">
                                    <fmt:message key="purchaseOrder"/>:
                                    <a href="<c:url value="/purchaseOrder.do?method=display&exit=purchaseInvoiceDisplay&readonly=true&id=${record.reference}"/>"><o:out value="${record.referenceNumber}"/></a>
                                </div>
                            </c:otherwise>
                        </c:choose>
                        <c:if test="${record.bookingType.supportingPayments}">
                            <c:import url="/pages/records/invoice_payment_display.jsp"/>
                            <c:set var="paymentInfo" value="${recordView.paymentInfoDisplay}" />
                            <c:if test="${!empty paymentInfo}">
                                <div class="recordLabelValue">
                                    <fmt:message key="paymentVia"/> <o:out value="${paymentInfo}"/>
                                </div>
                            </c:if>
                        </c:if>
                        <c:import url="/pages/purchasing/supplier_reference.jsp"/>
                        <c:if test="${!empty recordView.referencedSales}">
                            <div class="recordLabelValue">
                                <fmt:message key="customerOrderLabel"/>: 
                                <c:choose>
                                    <c:when test="${recordView.exitTarget == 'salesOrderDisplay'}">
                                        <span style="margin-left: 20px;"><o:out value="${record.businessCaseId}"/></span>
                                    </c:when>
                                    <c:otherwise>
                                        <span style="margin-left: 20px;">
                                            <a href="<c:url value="/loadSales.do?id=${record.businessCaseId}&exit=purchaseInvoiceDisplay"/>"><o:out value="${record.businessCaseId}"/></a>
                                        </span>
                                    </c:otherwise>
                                </c:choose>
                                <span> - </span>
                                <o:out value="${recordView.referencedSales.name}"/>
                            </div>
                        </c:if>
                        <c:if test="${!record.type.displayNotesBelowItems}">
                            <c:import url="/pages/records/record_note_display.jsp"/>
                        </c:if>
                        <br />
                    </div>
                    <div class="column33r">
                        <c:import url="/pages/records/record_status.jsp"/>
                        <c:if test="${!empty recordView.creditNoteList}">
                            <div class="recordLabelValue">
                                <fmt:message key="creditNote"/>: 
                                <c:forEach var="note" items="${recordView.creditNoteList}">
                                    <a href="<c:url value="/purchaseInvoice.do?method=loadOther&id=${note}"/>"><o:out value="${note}"/></a>
                                </c:forEach>
                            </div>
                        </c:if>
                    </div>
                    <c:import url="/pages/purchasing/invoice_summary.jsp"/>
                </div>
            </div>
        </div>
        <c:import url="/pages/records/record_items.jsp"/>
        <c:if test="${record.type.displayNotesBelowItems}">
            <c:import url="/pages/records/record_note_display.jsp"/>
        </c:if>
    </div>
</div>
</tiles:put>
</c:otherwise>
</c:choose>

</tiles:insert>
<c:remove var="recordView" scope="session"/>
