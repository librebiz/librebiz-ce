/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10.08.2002 
 * 
 */
package com.osserp.common.util;

import java.io.File;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.osserp.common.util.SecurityUtil;
import org.jasypt.util.password.rfc2307.RFC2307SHAPasswordEncryptor;
import org.jasypt.util.password.ConfigurablePasswordEncryptor;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class USecurityUtil {

    @Test
    public void testGetTokenList() {
        String password = "osserpPwd";
        String cleartext = "osserp.org";
        String encrypted = SecurityUtil.encrypt(password, cleartext);
        String decrypted = SecurityUtil.decrypt(password, encrypted);
        assertTrue(cleartext.equals(decrypted));
    }

    @Test
    public void testGetFileList() {
        String password = "osserpPwd";
        boolean ok = true;
        File src = new File("src/test/resources/log4j.properties");
        File enc = new File("src/test/resources/log4j.properties.enc");
        File plain = new File("src/test/resources/log4j.properties.plain");
        try {
            byte[] pf = FileUtil.getBytes(src);
            byte[] ef = SecurityUtil.encryptFile(password, pf);
            FileUtil.persist(ef, enc.getAbsolutePath());
            assertTrue(enc.exists());
        } catch (Exception e) {
            ok = false;
        }
        if (ok) {
            try {
                byte[] ef = FileUtil.getBytes(enc);
                byte[] pf = SecurityUtil.decryptFile(password, ef);
                FileUtil.persist(pf, plain.getAbsolutePath());
                assertTrue(plain.exists());
            } catch (Exception e) {
                ok = false;
            }
        }
        if (ok) {
            try {
                byte[] a = FileUtil.getBytes(src);
                byte[] b = FileUtil.getBytes(plain);
                assertTrue(a.length == b.length);
            } catch (Exception e) {
            }
        }
        try {
            enc.deleteOnExit();
            plain.deleteOnExit();
        } catch (Exception e) {
        }
    }

    @Test
    public void testSSHA512() {
        String password = "osserpPwd";
        ConfigurablePasswordEncryptor encryptor = new ConfigurablePasswordEncryptor();
        encryptor.setAlgorithm("SHA-512");
        encryptor.setPlainDigest(true);
        String encryptedPassword = encryptor.encryptPassword(password);
        System.out.println("Encrypted Password: " + encryptedPassword);
    }
}
