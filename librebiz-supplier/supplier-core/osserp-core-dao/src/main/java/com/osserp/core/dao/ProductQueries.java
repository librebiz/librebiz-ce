/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 6, 2009 7:05:46 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.finance.Record;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
public interface ProductQueries {

    /**
     * Finds all active products available by an optional selection
     * @param userSelection
     * @param includeEol
     * @param fetchSize 0 == system property, -1 == all
     * @return products
     */
    List<Product> find(ProductSelection userSelection, boolean includeEol, int fetchSize);

    /**
     * Finds products by selection config
     * @param config
     * @param includeEol
     * @param fetchSize 0 == system property, -1 == all
     * @return list with products matching filters created by config
     */
    List<Product> find(ProductSelectionConfig config, boolean includeEol, int fetchSize);

    /**
     * Finds products by selection item
     * @param item
     * @param includeEol
     * @param fetchSize 0 == system property, -1 == all
     * @return list with products matching filters created by item
     */
    List<Product> find(ProductSelectionConfigItem item, boolean includeEol, int fetchSize);

    /**
     * Finds products by selection config, columnKey, val, startsWith and includeEol
     * @param config
     * @param columnKey
     * @param val
     * @param startsWith
     * @param includeEol
     * @param fetchSize 0 == system property, -1 == all
     * @return list with products matching filters created by config
     */
    List<Product> find(
            ProductSelectionConfig config,
            String columnKey,
            String val,
            boolean startsWith,
            boolean includeEol,
            int fetchSize);

    /**
     * Provides available configurations
     * @return list
     */
    List<ProductClassificationConfig> findClassificationConfigs();

    /**
     * Adds last price paid by customer or supplier
     * @param productList
     * @param contactReference
     * @param contactReferenceCustomer
     * @return product list with added price
     */
    List<Product> addLastPricePaid(List<Product> productList, Long contactReference, boolean contactReferenceCustomer);

    /**
     * Adds last price paid by customer or supplier
     * @param record
     * @param contactReference
     * @param contactReferenceCustomer
     */
    void addLastPricePaid(Record record, Long contactReference, boolean contactReferenceCustomer);

    /**
     * Indicates if an product is matching a selection list
     * @param selectionConfigs
     * @param productId
     * @return true if at least one selection contains the product
     */
    boolean isMatching(List<ProductSelectionConfig> selectionConfigs, Long productId);

}
