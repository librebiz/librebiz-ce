/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Dec-2006 10:44:02 
 * 
 */
package com.osserp.gui.client.records.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewName;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.finance.Stocktaking;
import com.osserp.core.finance.StocktakingItem;
import com.osserp.core.finance.StocktakingManager;
import com.osserp.core.system.SystemCompany;

import com.osserp.gui.client.records.StocktakingView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("stocktakingView")
public class StocktakingViewImpl extends AbstractStockView implements StocktakingView {
    private static Logger log = LoggerFactory.getLogger(StocktakingViewImpl.class.getName());
    
    private static final int DEFAULT_PAGE_LIST_COUNT = 50;
    private static final String PAGE_LIST_COUNT_PROPERTY = "stocktakingProductListStep";
    
    private SystemCompany selectedCompany = null;
    private List<Option> types = null;
    private List<Option> employees = null;
    private boolean productEditMode = false;

    protected StocktakingViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        StocktakingManager manager = getStocktakingManager();
        types = manager.getTypes();
        setListTarget(getActionTarget());
        int pageListDisplayCount = 0;
        try { 
            pageListDisplayCount = getSystemPropertyId(PAGE_LIST_COUNT_PROPERTY).intValue();
        } catch (Exception e) {
            log.warn("<init> ignoring exception on attempt to init listStep [message=" + e.getMessage() + "]");
        }
        setListStep(pageListDisplayCount > 0 ? pageListDisplayCount : DEFAULT_PAGE_LIST_COUNT);

        selectedCompany = getSystemConfigManager().getCompany(RequestUtil.getId(request));
        setList(manager.getClosed(selectedCompany.getId()));
        if (log.isDebugEnabled()) {
            log.debug("init() found closed stocktakings [count="
                    + (getList() == null ? "0" : getList().size())
                    + "]");
        }
        Stocktaking open = manager.getOpen(selectedCompany.getId());
        if (open != null) {
            if (log.isDebugEnabled()) {
                log.debug("init() found open stocktaking");
            }
            setBean(open);
        }
    }

    public SystemCompany getSelectedCompany() {
        return selectedCompany;
    }

    public List<Option> getTypes() {
        return types;
    }

    public List<Option> getEmployees() {
        return employees;
    }

    @Override
    public void save(Form fh) throws ClientException, PermissionException {
        StocktakingManager manager = getStocktakingManager();
        if (isCreateMode()) {
            // create
            Option selectedType = getSelectedType(fh.getLong("type"));
            String name = fh.getString("name");
            Date recordDate = fh.getDate("recordDate");
            manager.validate(
                    getList(),
                    (Stocktaking) getBean(),
                    selectedType,
                    name);
            Stocktaking created = manager.create(
                    selectedCompany.getId(),
                    selectedType,
                    name,
                    recordDate,
                    getDomainUser().getEmployee().getId());
            setBean(created);
            setCreateMode(false);
        } else {
            Stocktaking st = getStocktaking();
            Long counter = fh.getLong("counter");
            if (isSet(counter)) {
                st.setCounter(counter);
            }
            Long writer = fh.getLong("writer");
            if (isSet(writer)) {
                st.setWriter(writer);
            }
            st.setName(fh.getString("name"));
            st.setNote(fh.getString("note"));
            manager.save(st);
            setEditMode(false);
        }
    }

    public boolean isProductEditMode() {
        return productEditMode;
    }

    public void setProductEditMode(boolean productEditMode) {
        this.productEditMode = productEditMode;
    }

    public void synchronizeQuantity() {
        Stocktaking st = getStocktaking();
        for (int i = getListStart(), j = getRunTo(st); i <= j; i++) {
            StocktakingItem next = st.getItems().get(i);
            next.setCountedQuantity(next.getExpectedQuantity());
        }
        getStocktakingManager().save(st);
    }

    private int getRunTo(Stocktaking st) {
        int runTo = getListEnd(); // 600
        int listSize = st.getItems().size(); // 585
        if (runTo >= listSize) {
            runTo = listSize - 1;
        }
        return runTo;
    }

    public void updateProducts(Form fh) throws ClientException {
        Long[] items = fh.getIndexedLong("items");
        String[] counters = fh.getIndexedStrings("counters");
        if (items == null || counters == null || items.length != counters.length) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        Map<Long, Double> map = new HashMap<Long, Double>();
        for (int i = 0, j = items.length; i < j; i++) {
            if (isNotSet(counters[i])) {
                throw new ClientException(ErrorCode.VALUES_MISSING);
            }
            map.put(items[i], NumberUtil.createDouble(counters[i]));
        }
        Stocktaking st = getStocktaking();
        for (int i = getListStart(), j = getRunTo(st); i <= j; i++) {
            StocktakingItem next = st.getItems().get(i);
            Double val = map.get(next.getId());
            if (log.isDebugEnabled()) {
                log.debug("updateCounters() index "
                        + i + ": count " + val + ", item " + next.getId());
            }
            st.getItems().get(i).setCountedQuantity(val);
        }
        getStocktakingManager().save(st);
    }

    public void close() throws ClientException, PermissionException {
        Stocktaking obj = getStocktaking();
        if (obj == null) {
            throw new ActionException("stocktaking expected, was null!");
        }
        if (!obj.isCloseable()) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        getStocktakingManager().close(getDomainUser().getEmployee(), obj);
    }

    public void delete() throws ClientException, PermissionException {
        Stocktaking obj = getStocktaking();
        if (obj == null) {
            throw new ActionException("stocktaking not bound");
        }
        if (obj.isClosed()) {
            throw new ClientException(ErrorCode.HISTORICAL_DATA);
        }
        if (!getDomainEmployee().getId().equals(obj.getCreatedBy())) {
            throw new PermissionException();
        }
        getStocktakingManager().delete(obj);
        setBean(null);
        disableEditMode();
    }

    public Document getXml(String stylesheetName) throws ClientException, PermissionException {
        if (stylesheetName == null) {
            throw new ActionException("stylesheet name expected, was null!");
        }
        Stocktaking obj = getStocktaking();
        if (obj == null) {
            throw new ActionException("stocktaking expected, was null!");
        }
        Document doc = getStocktakingManager().createDocument(
                getDomainUser(), 
                obj, 
                selectedCompany, 
                stylesheetName);
        if (log.isDebugEnabled()) {
            log.debug("getXml() done:\n" + JDOMUtil.getPrettyString(doc));
        }
        return doc;
    }

    @Override
    protected int getListCount() {
        if (getBean() == null) {
            return 0;
        }
        return ((Stocktaking) getBean()).getItems().size();
    }

    @Override
    public void setSelection(Long id) {
        setBean(id == null ? null : getStocktakingManager().find(id));
    }

    @Override
    public void setEditMode(boolean editMode) throws PermissionException {
        super.setEditMode(editMode);
        if (editMode) {
            employees = getEmployeeSearch().findActiveNames();
        }
    }

    private Stocktaking getStocktaking() {
        return (Stocktaking) getBean();
    }

    private Option getSelectedType(Long id) {
        return (Option) CollectionUtil.getByProperty(types, "id", id);
    }
}
