/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 12, 2008 10:49:39 AM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductClassificationEntity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductClassificationEntities {
    
    /**
     * Provides classification entity by name
     * @param name
     * @return entity by name or null if not exists
     */
    ProductClassificationEntity findByName(String name);

    /**
     * Provides all classifications (immutable)
     * @return list
     */
    List<? extends ProductClassificationEntity> getList();

    /**
     * Provides all classifications (mutable)
     * @return configs
     */
    List<? extends ProductClassificationEntity> getConfigs();

    /**
     * Provides classifications as id/value map
     * @return map
     */
    Map<Long, ? extends ProductClassificationEntity> getMap();

    /**
     * Creates a new product classification entity
     * @param user
     * @param name
     * @param description
     * @return new created entity
     * @throws ClientException if name already exists
     */
    ProductClassificationEntity create(Employee user, String name, String description) throws ClientException;

    /**
     * Persists an product classification instance
     * @param obj
     */
    void save(ProductClassificationEntity obj);
}
