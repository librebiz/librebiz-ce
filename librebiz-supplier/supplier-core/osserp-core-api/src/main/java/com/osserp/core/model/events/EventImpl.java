/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.events;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.Note;
import com.osserp.common.Parameter;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.events.Event;
import com.osserp.core.events.EventAction;

/**
 * Value object for Event.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventImpl extends AbstractEntity implements Event {
    private static Logger log = LoggerFactory.getLogger(EventImpl.class.getName());

    private Long recipientId;
    private Long referenceId;
    private EventAction action;
    private String link;
    private String description;
    private String message;
    private Date expires;
    private boolean alertSent = false;
    private boolean closed = false;
    private boolean alert = false;
    private Date activation;
    private Date appointmentDate;
    private Date closedOn;
    private Long closedBy;
    private String headline;
    private List<EventNoteImpl> notes = new ArrayList<>();

    /**
     * Proteceted constructor to implement serialization
     */
    protected EventImpl() {
        super();
    }

    /**
     * Default constructor for creating new events
     * @param action
     * @param created
     * @param createdBy
     * @param referenceId
     * @param recipientId
     * @param description
     * @param message
     * @param headline
     * @param parameters
     */
    public EventImpl(
            EventAction action,
            Date created,
            Long createdBy,
            Long referenceId,
            Long recipientId,
            String description,
            String message,
            String headline,
            List<Parameter> parameters) {

        super();
        if (action == null) {
            throw new IllegalArgumentException("fcs action must not be null");
        }
        setCreatedBy(createdBy);
        if (created != null) {
            setCreated(created);
        }
        this.recipientId = recipientId;
        this.referenceId = referenceId;
        this.action = action;
        this.description = description;
        this.message = message;
        this.headline = headline;
        
        expires = new Date(getCreated().getTime()
                + (action.getTerminationTime() == null ? 0
                        : action.getTerminationTime()));
        
        link = createLink(action.getActionLink(), parameters);
        activation = new Date(System.currentTimeMillis());
        if (action.getType().isAppointment()) {
            appointmentDate = activation;
        }
        if (log.isDebugEnabled()) {
            log.debug("<init> done [action="
                    + (action.getId() == null ? "null" : action.getId())
                    + ", type=" + (action.getType() == null ? "null" : action.getType().getId())
                    + ", name=" + action.getName()
                    + ", createdBy=" + createdBy
                    + ", reference=" + referenceId
                    + ", recipient=" + recipientId
                    + ", description=" + description
                    + ", message=" + message
                    + ", headline=" + headline
                    + ", expires=" + expires
                    + ", link=" + link
                    + ", appointmentDate=" + appointmentDate
                    + ", parameters=" + (parameters == null ? "0" : parameters.size()) + "]");
        }
    }

    /**
     * Default constructor for creating new appointment events
     * @param action
     * @param createdBy
     * @param referenceId
     * @param recipientId
     * @param description
     * @param message
     * @param parameters
     * @param activationDate
     * @param appointmentDate
     */
    public EventImpl(
            EventAction action,
            Long createdBy,
            Long referenceId,
            Long recipientId,
            String description,
            String message,
            List<Parameter> parameters,
            Date activationDate,
            Date appointmentDate) {

        this(
                action,
                null,
                createdBy,
                referenceId,
                recipientId,
                description,
                message,
                null,
                parameters);
        if (activationDate != null) {
            this.activation = activationDate;
        }
        if (appointmentDate == null && activationDate != null) {
            this.appointmentDate = activationDate;
        } else if (appointmentDate != null) {
            this.appointmentDate = appointmentDate;
        }
    }

    /**
     * Default constructor to create new alert events
     * @param alert
     * @param sourceCreated
     * @param referenceId
     * @param recipientId
     * @param description
     * @param message
     * @param link
     * @param notes
     */
    public EventImpl(
            EventAction alert,
            Date sourceCreated,
            Long referenceId,
            Long recipientId,
            String description,
            String message,
            String link,
            List<? extends Note> notes) {

        setCreatedBy(Constants.SYSTEM_EMPLOYEE);
        setCreated(sourceCreated);
        this.alert = true;
        this.recipientId = recipientId;
        this.referenceId = referenceId;
        this.action = alert;
        this.description = description;
        this.message = message;
        this.link = link;
        Date now = new Date(System.currentTimeMillis());
        this.expires = now;
        this.activation = now;
        if (notes != null) {
            for (int i = 0, j = notes.size(); i < j; i++) {
                Note note = notes.get(i);
                addNote(note.getCreatedBy(), note.getNote());
            }
        }
    }

    /**
     * Creates an info event about another event
     * @param infoAction
     * @param source
     * @param recipient
     */
    public EventImpl(
            EventAction infoAction,
            Event source,
            Long recipient) {

        action = infoAction;
        recipientId = recipient;
        setCreatedBy(source.getCreatedBy());
        setCreated(source.getCreated());
        expires = source.getExpires();
        referenceId = source.getReferenceId();
        description = source.getDescription();
        message = source.getMessage();
        link = source.getLink();
        activation = new Date(System.currentTimeMillis());
        if (source.getNotes() != null) {
            for (int i = 0, j = source.getNotes().size(); i < j; i++) {
                Note note = source.getNotes().get(i);
                addNote(note.getCreatedBy(), note.getNote());
            }
        }
    }

    public EventImpl(EventImpl otherValue) {
        super(otherValue);
        recipientId = otherValue.recipientId;
        referenceId = otherValue.referenceId;
        action = otherValue.action;
        description = otherValue.description;
        link = otherValue.link;
        expires = otherValue.expires;
        alertSent = otherValue.alertSent;
        closed = otherValue.closed;
        closedOn = otherValue.closedOn;
        closedBy = otherValue.closedBy;
        headline = otherValue.headline;
        activation = otherValue.activation;
        appointmentDate = otherValue.appointmentDate;
        if (otherValue.notes != null) {
            notes = otherValue.notes;
        }
    }

    public Long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Long recipientId) {
        this.recipientId = recipientId;
    }

    public Long getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Long referenceId) {
        this.referenceId = referenceId;
    }

    public EventAction getAction() {
        return action;
    }

    public void setAction(EventAction action) {
        this.action = action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMessage() {
        return message;
    }

    protected void setMessage(String message) {
        this.message = message;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public boolean isAlert() {
        return alert;
    }

    protected void setAlert(boolean alert) {
        this.alert = alert;
    }

    public boolean isAlertSent() {
        return alertSent;
    }

    public void setAlertSent(boolean alertSent) {
        this.alertSent = alertSent;
    }

    public Date getActivation() {
        return activation;
    }

    public void setActivation(Date activation) {
        this.activation = activation;
    }

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public final Date getEventDate() {
        return action.getType().isAppointment() ? appointmentDate : expires; 
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public Date getClosedOn() {
        return closedOn;
    }

    public void setClosedOn(Date closedOn) {
        this.closedOn = closedOn;
    }

    public Long getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(Long closedBy) {
        this.closedBy = closedBy;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public List<? extends Note> getNotes() {
        return notes;
    }

    protected void setNotes(List<EventNoteImpl> notes) {
        if (notes != null) {
            this.notes = notes;
        }
    }

    public void addNote(Long user, String text) {
        this.notes.add(new EventNoteImpl(user, text));
    }

    private String createLink(String baseLink, List<Parameter> parameters) {
        int length = (parameters == null) ? 0 : parameters.size();
        if ((baseLink != null && baseLink.length() > 0) && length == 0) {
            return baseLink;
        } else if (parameters != null && length > 0) {
            StringBuilder buffer = new StringBuilder(baseLink);
            for (int i = 0, j = length; i < j; i++) {
                Parameter param = parameters.get(i);
                if (buffer.indexOf("?") < 0) {
                    buffer.append("?");
                } else {
                    buffer.append("&");
                }
                buffer.append(param.getName()).append("=").append(param.getValue());
            }
            return buffer.toString();
        } else {
            return null;
        }
    }
}
