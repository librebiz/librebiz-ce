<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:if test="${empty sessionScope.user or empty sessionScope.portalView}">
<c:redirect url="/errors/error_timeout.jsp"/>
</c:if>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/error.jsp" flush="false">
  <tiles:put name="title">
    <o:displayTitle />
  </tiles:put>
  <tiles:put name="headline_right">
    <v:link url="/index" title="backToMenu"><o:img name="homeIcon" /></v:link>
  </tiles:put>
  <tiles:put name="content">
    <h4><fmt:message key="requiredDocumentNotCreatable" /></h4>
    <br />
    <p><fmt:message key="pleaseCheckCompletenessOfDocumentRelatedDataSets" /></p>
    <p><fmt:message key="documentCreatePermissionDeniedHint" /></p>
    <c:if test="${!empty sessionScope.errors}">
      <br />
      <fmt:message key="analyzedError" />:<br />
      <p><o:out value="${sessionScope.errors}" /></p>
      <o:removeErrors/>
    </c:if>
  </tiles:put>
</tiles:insert>
