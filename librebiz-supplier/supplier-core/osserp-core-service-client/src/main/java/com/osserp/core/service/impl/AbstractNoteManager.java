/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.mail.Mail;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessNote;
import com.osserp.core.NoteAware;
import com.osserp.core.NoteManager;
import com.osserp.core.NoteType;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.MailTemplates;
import com.osserp.core.employees.Employee;
import com.osserp.core.mail.MailMessageContext;
import com.osserp.core.mail.MailTemplate;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractNoteManager extends AbstractService implements NoteManager {
    private static Logger log = LoggerFactory.getLogger(AbstractNoteManager.class.getName());
    private BusinessNotes businessNotes = null;
    private Employees employees = null;
    private MailTemplates mailTemplates;

    protected AbstractNoteManager(BusinessNotes businessNotes, MailTemplates mailTemplates, Employees employees) {
        super();
        this.businessNotes = businessNotes;
        this.employees = employees;
        this.mailTemplates = mailTemplates;
    }

    public abstract NoteAware load(Long primaryKey);

    protected abstract String getTypeName();

    public final NoteType getType(String name) {
        return businessNotes.getType(name);
    }

    protected final void save(BusinessNote note) {
        businessNotes.save(note);
    }

    public final NoteAware addNote(Employee user, NoteAware noteAware, String headline, String note) throws ClientException {
        if (isNotSet(note)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        NoteType type = getType(getTypeName());
        BusinessNote businessNote = businessNotes.create(
                type,
                noteAware.getPrimaryKey(),
                (user == null ? null : user.getId()),
                note,
                headline);
        if (log.isDebugEnabled()) {
            log.debug("addNote() done [id=" + businessNote.getId() + ", reference=" + noteAware.getPrimaryKey() + "]");
        }
        return load(noteAware.getPrimaryKey());
    }

    public BusinessNote getSticky(Long referenceId) {
        return businessNotes.findSticky(getTypeName(), referenceId);
    }

    public NoteAware setSticky(NoteAware noteAware, Long noteId, boolean sticky) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("setSticky() invoked [id=" + noteId + ", reference=" + noteAware.getPrimaryKey() + ", sticky=" + sticky + "]");
        }
        for (int i = 0, j = noteAware.getNotes().size(); i < j; i++) {
            BusinessNote next = noteAware.getNotes().get(i);
            if (next.getId().equals(noteId) && sticky) {
                next.setSticky(true);
            } else {
                next.setSticky(false);
            }
            save(next);
        }
        return load(noteAware.getPrimaryKey());
    }

    public NoteAware updateNote(Employee user, NoteAware noteAware, Long noteId, String note) throws ClientException {
        if (isNotSet(note)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        for (int i = 0, j = noteAware.getNotes().size(); i < j; i++) {
            BusinessNote next = noteAware.getNotes().get(i);
            if (next.getId().equals(noteId)) {
                next.updateNote(user.getId(), note);
                save(next);
                break;
            }
        }
        return load(noteAware.getPrimaryKey());
    }

    public final NoteAware completeNote(NoteAware noteAware, Long noteId, String recipients, String subject, String message) {
        if (log.isDebugEnabled()) {
            log.debug("completeNote() invoked [businessCase=" + noteAware.getPrimaryKey() + "]");
        }
        for (int i = 0, j = noteAware.getNotes().size(); i < j; i++) {
            BusinessNote note = noteAware.getNotes().get(i);
            if (note.getId().equals(noteId)) {
                note.setRecipients(recipients);
                note.setSubject(subject);
                note.setText(message);
                note.setEmail(true);
                save(note);
                break;
            }
        }
        return load(noteAware.getPrimaryKey());
    }

    protected void addRecipient(Mail mail, Long employeeId) {
        String address = getMailAddress(employeeId);
        try {
            if (address != null) {
                mail.addRecipient(address);
            }
        } catch (ClientException e) {
            StringBuilder buffer = new StringBuilder(256);
            buffer.append("addRecipient() ignoring an exception [employeeId=")
                    .append(employeeId).append(", address=").append(address)
                    .append(", exception=").append(e.getClass().getName())
                    .append(", message=").append(e.getMessage()).append("]\n");
            log.warn(buffer.toString(), e);
        }
    }

    protected void addRecipient(Mail mail, Long employeeId, String mode) {
        String address = getMailAddress(employeeId);
        try {
            if (address != null) {
                if ("cc".equalsIgnoreCase(mode)) {
                    mail.addRecipientCC(address);
                } else if ("bcc".equalsIgnoreCase(mode)) {
                    mail.addRecipientBCC(address);
                } else {
                    mail.addRecipient(address);
                }
            }
        } catch (ClientException e) {
            StringBuilder buffer = new StringBuilder(256);
            buffer.append("addRecipient() ignoring an exception [employeeId=")
                    .append(employeeId).append(", address=").append(address)
                    .append(", exception=").append(e.getClass().getName())
                    .append(", message=").append(e.getMessage()).append("]\n");
            log.warn(buffer.toString(), e);
        }
    }

    protected String getMailAddress(Long employeeId) {
        String address = null;
        Employee employee = getEmployee(employeeId);
        if (employee != null) {
            address = employee.getEmail();
        }
        return address;
    }

    public MailMessageContext setupMail(Employee user, BusinessNote note, Map<String, String> opts) {
        MailTemplate mailTemplate = findMailTemplate();
        String originator = fetchOriginator(user, mailTemplate);
        if (originator == null) {
            originator = note.getOriginator();
        }
        Mail mail = new Mail(originator, note.getSubject(), note.getText());
        if (note.isEmail() && note.getRecipients() != null) {
            String[] recipients = StringUtil.getTokenArray(note.getRecipients());
            for (int i = 0, j = recipients.length; i < j; i++) {
                try {
                    mail.addRecipient(recipients[i]);
                } catch (ClientException e) {
                    log.warn("setupMail: ignoring invalid recipient [address=" + recipients[i] + "]");
                }
            }
        }
        return new MailMessageContext(mailTemplate, mail, opts);
    }

    protected String[] getRecipients(String recipients) {
        return recipients != null ? StringUtil.getTokenArray(recipients) : null;
    }

    protected Mail addRemainingRecipients(Mail mail, String[] recipients) {
        if (recipients != null && recipients.length > 1) {
            for (int i = 1; i < recipients.length; i++) {
                try {
                    mail.addRecipient(recipients[i]);
                } catch (ClientException e) {
                    log.warn("addRemainingRecipients: ignoring invalid recipient [address=" + recipients[i] + "]");
                }
            }
        }
        return mail;
    }

    protected Employee getEmployee(Long employeeId) {
        return (Employee) employees.find(employeeId);
    }

    protected String getOriginator(Employee user, MailTemplate mtpl) throws ClientException {
        String result = fetchOriginator(user, mtpl);
        if (result == null) {
            throw new ClientException(ErrorCode.ORIGINATOR_MISSING);
        }
        return result;
    }

    protected String fetchOriginator(Employee user, MailTemplate mtpl) {
        return (mtpl != null && mtpl.isSendAsUser() && user != null && user.getEmail() != null) ? user.getEmail()
                : ((mtpl != null && !mtpl.isSendAsUser() && mtpl.getOriginator() != null)
                        ? mtpl.getOriginator()
                        : null);
    }

    protected MailTemplate getMailTemplate() throws ClientException {
        MailTemplate mtpl = findMailTemplate();
        if (mtpl == null) {
            throw new ClientException(ErrorCode.MAIL_TEMPLATE_CONFIG);
        }
        return mtpl;
    }

    protected MailTemplate findMailTemplate() {
        return mailTemplates.findTemplate(getTypeName() + "EmailTemplate");
    }

    protected BusinessNotes getBusinessNotes() {
        return businessNotes;
    }

    protected Employees getEmployees() {
        return employees;
    }
}
