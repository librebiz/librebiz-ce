/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import com.osserp.common.ErrorCode
import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.LdapException
import com.osserp.common.directory.ldap.LdapManager
import com.osserp.common.util.NumberUtil
import com.osserp.common.util.SecurityUtil
import com.osserp.common.util.StringUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class LdapManagerImpl extends AbstractLdapManager implements LdapManager {

    LdapManagerImpl() {
        super()
    }

    LdapManagerImpl(String configFile) {
        super(configFile)
    }

    LdapManagerImpl(LdapConfig configInstance) {
        super(configInstance)
    }

    LdapManagerImpl(LdapManagerImpl otherManager) {
        super(otherManager)
    }

    Map changeClientPassword(Map client, String password) {
        clients.changePassword(client, password)
    }

    Map createGroup(String name, String description, String client, Long id) {
        List<Map> all = groups.findAll(client)
        for (group in all) {
            if (group.cn && group.cn.equalsIgnoreCase(name)) {
                return group
            }
        }
        if (!id) {
            throw new LdapException(ErrorCode.ID_REQUIRED)
        }
        groups.create(id.toString(), name, description, client)
    }

    void deleteGroup(Map group) {
        groups.delete(group)
    }

    String getDefaultGroupName(String client) {
        Map clientObject = lookupClient(client)
        clientObject?.clientgroupdefault
    }

    List<Map> lookupGroups(String client) {
        List<Map> all = groups.findAll(client)
        /* no known excludes by now
        List<Map> result = []
        for (user in all) {
            if (!user.gecos || !('Computer' == user.gecos)) {
                result << user
            }
        }
        return result
        */
        return all
    }

    Map lookupGroup(String name, String client) {
        groups.find(name, client)
    }

    Map addGroupMember(Map group, String uid) {
        groups.addMember(group, uid)
    }

    Map removeGroupMember(Map group, String uid) {
        groups.removeMember(group, uid)
    }

    Map updateGroup(Map group, Map values) throws LdapException {
        try {
            return groups.update(group, values)
        } catch (Throwable t) {
            throw new LdapException(t.message, t)
        }
    }


    Map createPermission(String name, String description, String client, Long id) {
        List<Map> all = permissions.findAll(client)
        for (group in all) {
            if (group.cn && group.cn.equalsIgnoreCase(name)) {
                return group
            }
        }
        if (!id) {
            id = permissions.createIdSuggestion(client)
        }
        permissions.create(id.toString(), name, description, client)
    }

    void deletePermission(Map permission) {
        permissions.delete(permission)
    }

    List<Map> lookupPermissions(String client) {
        List<Map> all = permissions.findAll(client)
        /* no known excludes by now
        List<Map> result = []
        for (user in all) {
            if (!user.gecos || !('Computer' == user.gecos)) {
                result << user
            }
        }
        return result
        */
        return all
    }

    Map lookupPermission(String name, String client) {
        permissions.find(name, client)
    }

    Map addPermissionMember(Map permission, String uid) {
        permissions.addMember(permission, uid)
    }

    Map removePermissionMember(Map permission, String uid) {
        permissions.removeMember(permission, uid)
    }

    Map updatePermission(Map permission, Map values) throws LdapException {
        try {
            return permissions.update(permission, values)
        } catch (Throwable t) {
            throw new LdapException(t.message, t)
        }
    }

    Map createUser(Map atts, String client) {
        // clear previous errors if exists
        if (atts.errors) {
            atts.errors = null
        }
        Map user = users.create(atts, client)
        return user
    }

    void deleteUser(Map user) {
        users.delete(user)
    }

    List<Map> lookupUserGroups(String uid, String client) {
        groups.findByUser(uid, client)
    }

    List<Map> lookupUserPermissions(String uid, String client) {
        permissions.findByUser(uid, client)
    }

    List<String> getUserDisplayAttributes() {
        users.getDisplayAttributes()
    }

    boolean isUserArrayAttribute(String attributeName) {
        Set<String> arrayAttributes = users.getArrayAttributes()
        arrayAttributes.contains(attributeName)
    }

    Map clientLogin(String client, String password) throws LdapException {
        //println "login: invoked [uid=$uid, pwd=${(password ? 'exists' : 'missing')}]"
        Map obj = clients.find(client)
        return checkPassword(obj, password)
    }

    Map login(String uid, String password, String client) throws LdapException {
        //println "login: invoked [uid=$uid, pwd=${(password ? 'exists' : 'missing')}]"
        Map user = users.lookup(uid, client)
        return checkPassword(user, password)
    }

    private Map checkPassword(Map user, String password) {
        if (!user) {
            throw new LdapException(ErrorCode.USERID_INVALID)
        }
        String currentPwd = user.userpassword ? new String(user.userpassword) : null
        if (!currentPwd) {
            throw new LdapException(ErrorCode.PERMISSION_DENIED)
        }
        if (!SecurityUtil.checkPassword(password, currentPwd)) {
            throw new LdapException(ErrorCode.USER_PASSWORD_INVALID)
        }
        return user
    }

    Map changePassword(Map user, String password) {
        users.changePassword(user, password)
    }

    String createUserIdSuggestion(String min, String max, String client) {
        Integer start = NumberUtil.createLong(min)
                ?: NumberUtil.createLong(config.userUidnumberMin)
                ?: NumberUtil.createLong(config.userIdStart) ?: 1100
        Integer stop = NumberUtil.createLong(max)
                ?: NumberUtil.createLong(config.userUidnumberMax) ?: 65530
        List<Map> existing = users.findAll(client)
        Integer current = start
        while (current < stop) {
            if (userIdExists(existing, current.toString())) {
                current = current + 1
            } else {
                return current
            }
        }
        return null
    }

    Map updateUser(Map user, Map values) throws LdapException {
        values.each { key, value ->
            if (isUserArrayAttribute(key)) {
                if (value instanceof String) {
                    StringBuilder val = new StringBuilder(value)
                    StringUtil.removeAll(val, '[')
                    StringUtil.removeAll(val, ']')
                    def vals = val.toString().split(',')
                    def list = []
                    vals.each {
                        if (it) {
                            list << it.trim()
                        }
                    }
                    values.put(key, list)
                }
            }
        }
        try {
            return users.update(user, values)
        } catch (Throwable t) {
            throw new LdapException(t.message, t)
        }
    }

    boolean userIdAssigned(String uidNumber, String client) {
        List<Map> existing = users.findAll(client)
        userIdExists(existing, uidNumber)
    }

    Map addMailAddress(Map user, String mailAddress) throws LdapException {
        users.addMailaddress(user, mailAddress)
    }

    Map addMailAlias(Map user, String alias) {
        users.addMailalias(user, alias)
    }

    Map removeMailAlias(Map user, String alias) {
        users.removeMailalias(user, alias)
    }

    private boolean userIdExists(List<Map> existing, String uidNumber) {
        if (!uidNumber) {
            // null declared as always assigned
            return true
        }
        if (!existing) {
            // empty database assumed
            return false
        }
        for (user in existing) {
            if (user.uidnumber && user.uidnumber == uidNumber) {
                return true
            }
        }
        return false
    }
}

