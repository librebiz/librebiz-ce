/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 4:38:45 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateUtil;

import com.osserp.core.customers.Customer;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesCancellations;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.FinanceRecord;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.ListItem;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.AbstractPayment;
import com.osserp.core.model.records.DeletedRecordImpl;
import com.osserp.core.model.records.SalesInvoiceImpl;
import com.osserp.core.model.records.SalesPaymentImpl;
import com.osserp.core.products.Product;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesCreditNoteType;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesInvoicesDao extends AbstractSalesInvoiceRecords implements SalesInvoices {
    private static Logger log = LoggerFactory.getLogger(SalesInvoicesDao.class.getName());

    private SalesCancellations cancellations = null;
    private SalesCreditNotes creditNotes = null;
    private SalesOrderDownpayments downpayments = null;
    private String salesInvoicesTable = null;
    private String salesInvoiceCorrectionsTable = null;
    private String salesInvoiceInfosTable = null;
    private String salesInvoiceItemsTable = null;
    private String salesOrdersTable = null;
    private String statusHistoryTable = null;

    public SalesInvoicesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            SalesPayments payments,
            SalesCancellations cancellations,
            SalesCreditNotes creditNotes,
            SalesOrderDownpayments downpayments) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                payments);
        this.cancellations = cancellations;
        this.creditNotes = creditNotes;
        this.downpayments = downpayments;
        salesInvoicesTable = getTable(TableKeys.SALES_INVOICES);
        salesInvoiceCorrectionsTable = getTable(TableKeys.SALES_INVOICE_CORRECTIONS);
        salesInvoiceInfosTable = getTable(TableKeys.SALES_INVOICE_INFOS);
        salesInvoiceItemsTable = getTable(TableKeys.SALES_INVOICE_ITEMS);
        salesOrdersTable = getTable(TableKeys.SALES_ORDERS);
        statusHistoryTable = getTable(TableKeys.RECORD_STATUS_INFOS);
        initializeDeletePermissions(DELETE_PERMISSION_DEFAULT);
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.SALES_INVOICE;
    }

    public SalesInvoice create(
            Employee user,
            Order order,
            Long invoiceType,
            Customer thirdParty,
            BigDecimal partialInvoiceAmount,
            BigDecimal partialInvoiceAmountGross,
            boolean copyNote,
            boolean copyTerms,
            boolean copyItems,
            Long userDefinedId,
            Date userDefinedDate) {

        boolean downpaymentsAvailable = false;
        if (!SalesInvoice.CUSTOM.equals(invoiceType)) {
            downpaymentsAvailable = ((downpayments.count(order.getId()) > 0)
                    && (count(order.getId()) == 0));
        }
        RecordType recordType = getRecordType();
        Long invoiceId = createInvoiceId(user, recordType, order.getCompany(), userDefinedId, userDefinedDate);
        SalesInvoiceImpl obj = new SalesInvoiceImpl(
                invoiceId,
                recordType,
                order,
                user,
                downpaymentsAvailable,
                invoiceType,
                copyItems,
                thirdParty,
                partialInvoiceAmount != null ? partialInvoiceAmount : new BigDecimal(0),
                partialInvoiceAmountGross != null ? partialInvoiceAmountGross : new BigDecimal(0));
        if (obj.getPaymentCondition() == null) {
            obj.setPaymentCondition(getDefaultCondition());
        }
        SalesInvoice result = (SalesInvoice) saveInitial(user, setUserDefinedDates(obj, userDefinedDate), order, copyNote, copyTerms);
        if (isSet(order.getPaymentAgreement().getNote())) {
            result.setPaymentNote(order.getPaymentAgreement().getNote());
            save(result);
        }
        return result;
    }

    public SalesInvoice create(
            Employee user,
            BranchOffice office,
            Customer customer,
            Long userDefinedId,
            Date userDefinedDate,
            boolean historical) {

        RecordType recordType = getRecordType();
        Long invoiceId = createInvoiceId(user, recordType, office.getCompany().getId(), userDefinedId, userDefinedDate);
        SalesInvoiceImpl obj = new SalesInvoiceImpl(
                invoiceId,
                recordType,
                office.getCompany().getId(),
                office.getId(),
                customer,
                user,
                null,
                taxRate,
                reducedTaxRate,
                true);
        if (historical) {
            obj.changeHistoricalStatus(true);
        }
        if (obj.getPaymentCondition() == null) {
            obj.setPaymentCondition(getDefaultCondition());
        }
        return (SalesInvoice) saveInitial(user, setUserDefinedDates(obj, userDefinedDate), null, false, false);
    }

    public SalesInvoice create(
            Employee user,
            Order order,
            boolean percentage,
            BigDecimal amount,
            Product product,
            String note,
            boolean printOrderItems,
            boolean copyNote,
            boolean copyTerms,
            Long userDefinedId,
            Date userDefinedDate) throws ClientException {

        if (order == null || user == null) {
            String err = "create() order or createdBy was null!";
            log.error(err);
            throw new BackendException(err);
        }
        if (log.isDebugEnabled()) {
            log.debug("create() invoked with partial invoice [printOrderItems=" + printOrderItems + "]");
        }
        // check that order is unchangeable, e.g. valid
        if (!order.isUnchangeable()) {
            throw new ClientException(ErrorCode.ORDER_MISSING);
        }
        if (amount == null || amount.doubleValue() == 0 || order.getAmounts() == null
                || order.getAmounts().getAmount() == null
                || order.getAmounts().getAmount().doubleValue() == 0) {
            throw new ClientException(ErrorCode.AMOUNT_MISSING);
        }
        BigDecimal price = null;
        if (percentage) {
            BigDecimal pc = amount.divide(new BigDecimal("100"));
            price = order.getAmounts().getAmount().multiply(pc);
        } else {
            price = amount;
        }
        RecordType recordType = getRecordType();
        Long invoiceId = createInvoiceId(user, recordType, order.getCompany(), userDefinedId, userDefinedDate);
        SalesInvoiceImpl obj = new SalesInvoiceImpl(
                invoiceId,
                recordType,
                order,
                user,
                percentage,
                price,
                product,
                null, // customName
                note,
                printOrderItems);
        if (obj.getPaymentCondition() == null) {
            obj.setPaymentCondition(getDefaultCondition());
        }
        return (SalesInvoice) saveInitial(user, setUserDefinedDates(obj, userDefinedDate), order, copyNote, copyTerms);
    }

    public SalesInvoice create(
            Employee user,
            SalesInvoice salesInvoice,
            boolean copyReferenceId,
            Long userDefinedId,
            Date userDefinedDate,
            boolean historical) {
        RecordType recordType = getRecordType();
        Long invoiceId = createInvoiceId(user, recordType, salesInvoice.getCompany(), userDefinedId, userDefinedDate);
        SalesInvoiceImpl obj = new SalesInvoiceImpl(
                invoiceId,
                salesInvoice,
                user,
                taxRate,
                reducedTaxRate,
                true,
                copyReferenceId);

        if (historical) {
            obj.updateStatus(Invoice.STAT_HISTORICAL);
        }
        if (salesInvoice.getPaymentCondition() != null) {
            //TODO create conditions
        } else if (obj.getPaymentCondition() == null) {
            obj.setPaymentCondition(getDefaultCondition());
        }
        return (SalesInvoice) saveInitial(user, setUserDefinedDates(obj, userDefinedDate), salesInvoice, copyReferenceId, copyReferenceId);
    }

    private SalesInvoiceImpl setUserDefinedDates(SalesInvoiceImpl obj, Date userDefinedDate) {
        boolean taxPointNow = systemPropertyEnabled("salesInvoiceTaxPointNow");
        if (userDefinedDate != null) {
            obj.setCreated(userDefinedDate);
            obj.setMaturity(DateUtil.addDays(
                    userDefinedDate,
                    obj.getPaymentCondition().getPaymentTarget().intValue()));
            obj.setTaxPoint(userDefinedDate);
        }
        if (!taxPointNow) {
            obj.setTaxPoint(null);
        }
        if (obj.getMaturity() == null) {
            obj.setMaturity(DateUtil.addDays(
                    obj.getCreated(),
                    obj.getPaymentCondition().getPaymentTarget().intValue()));
        }
        if (log.isDebugEnabled()) {
            log.debug("setUserDefinedDates() done [id=" + obj.getId() +
                    ", created=" + obj.getCreated() +
                    ", maturity=" + obj.getMaturity() +
                    ", taxPoint=" + obj.getTaxPoint() +
                    ", initialDate=" + obj.getInitialDate() +
                    ", statusDate=" + obj.getStatusDate() + "]");
        }
        return obj;
    }

    public SalesInvoice changeHistoricalStatus(SalesInvoice invoice, boolean enable) {
        invoice.changeHistoricalStatus(enable);
        save(invoice);
        return invoice;
    }

    public void changePaymentRecordsBranch(Sales sales) {
        if (log.isInfoEnabled()) {
            log.info("changePaymentRecordsBranch() invoked [sales="
                    + (sales == null ? "null"
                            : (sales.getId() + ", branch="
                                    + (sales.getBranch() == null ? "null" : sales.getBranch().getId())))
                    + "]");
        }
        changeBranch(sales);
        cancellations.changeBranch(sales);
        creditNotes.changeBranch(sales);
        downpayments.changeBranch(sales);
    }

    public void changeBranch(Sales sales) {
        if (sales != null && sales.getBranch() != null) {
            changeBranchBySales(
                    salesInvoicesTable,
                    sales.getId(),
                    sales.getBranch().getId(),
                    true);
        } else {
            if (log.isInfoEnabled()) {
                log.info("changePaymentRecordsBranch() invoked [sales="
                        + (sales == null ? "null" : sales.getId()) + "]");
            }
        }
    }

    public SalesInvoice changeNumber(Employee user, SalesInvoice salesInvoice, Long id) {
        if (exists(id)) {
            if (log.isInfoEnabled()) {
                log.info("changeNumber() requested number already exists, leaving record unchanged [requestedId="
                        + id + ", record=" + salesInvoice.getId()
                        + ", type=" + salesInvoice.getType().getId() + "]");
            }
            return salesInvoice;
        }
        SalesInvoiceImpl obj = (SalesInvoiceImpl) load(salesInvoice.getId());
        if (obj.isCanceled() || !obj.getCreditNotes().isEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("changeNumber() requested invoice is cancelled or refund exists, leaving record unchanged [requestedId="
                        + id + ", record=" + salesInvoice.getId()
                        + ", type=" + salesInvoice.getType().getId() + "]");
            }
            return salesInvoice;
        }
        Long tempId = createTempId();

        StringBuilder insertInvoiceTmp = new StringBuilder("INSERT INTO ");
        insertInvoiceTmp.append(salesInvoicesTable).append(" (id) values (?)");
        StringBuilder updateItemsTmp = new StringBuilder("UPDATE ");
        updateItemsTmp.append(salesInvoiceItemsTable).append(" SET reference_id = ? WHERE reference_id = ?");
        StringBuilder updateInfosTmp = new StringBuilder("UPDATE ");
        updateInfosTmp.append(salesInvoiceInfosTable).append(" SET reference_id = ? WHERE reference_id = ?");
        StringBuilder updateCorrectionsTmp = new StringBuilder("UPDATE ");
        updateCorrectionsTmp.append(salesInvoiceCorrectionsTable).append(" SET reference_id = ? WHERE reference_id = ?");
        StringBuilder updateStatusHistoryTmp = new StringBuilder("UPDATE ");
        updateStatusHistoryTmp.append(statusHistoryTable).append(" SET reference_id = ? WHERE type_id = " +
                obj.getType().getId() + " and reference_id = ?");
        StringBuilder updateInvoiceIdTmp = new StringBuilder("UPDATE ");
        updateInvoiceIdTmp.append(salesInvoicesTable).append(" SET id = ? WHERE id = ?");
        StringBuilder deleteTempInvoiceTmp = new StringBuilder("DELETE FROM ");
        deleteTempInvoiceTmp.append(salesInvoicesTable).append(" WHERE id = ?");

        String insertInvoice = insertInvoiceTmp.toString();
        String updateItems = updateItemsTmp.toString();
        String updateInfos = updateInfosTmp.toString();
        String updateCorrections = updateCorrectionsTmp.toString();
        String updateStatusHistory = updateStatusHistoryTmp.toString();
        String updateInvoiceId = updateInvoiceIdTmp.toString();
        String deleteTempInvoice = deleteTempInvoiceTmp.toString();

        Object[] invoiceParams = new Object[] { tempId };
        int[] invoiceTypes = new int[] { Types.BIGINT };
        Object[] updateParams = new Object[] { tempId, salesInvoice.getId() };
        int[] updateTypes = new int[] { Types.BIGINT, Types.BIGINT };

        int execCnt = jdbcTemplate.update(insertInvoice, invoiceParams, invoiceTypes);
        if (execCnt > 0) {
            // point references reference_id to the temporary created invoice
            execCnt = jdbcTemplate.update(updateItems, updateParams, updateTypes);
            execCnt = jdbcTemplate.update(updateInfos, updateParams, updateTypes);
            execCnt = jdbcTemplate.update(updateCorrections, updateParams, updateTypes);
            execCnt = jdbcTemplate.update(updateStatusHistory, updateParams, updateTypes);
            // update requested invoice id
            updateParams = new Object[] { id, salesInvoice.getId() };
            execCnt = jdbcTemplate.update(updateInvoiceId, updateParams, updateTypes);
            // restore references reference_id values
            updateParams = new Object[] { id, tempId };
            execCnt = jdbcTemplate.update(updateItems, updateParams, updateTypes);
            execCnt = jdbcTemplate.update(updateInfos, updateParams, updateTypes);
            execCnt = jdbcTemplate.update(updateCorrections, updateParams, updateTypes);
            execCnt = jdbcTemplate.update(updateStatusHistory, updateParams, updateTypes);
            // delete temporarily created invoice
            execCnt = jdbcTemplate.update(deleteTempInvoice, invoiceParams, invoiceTypes);
            if (execCnt == 0) {
                log.warn("changeNumber() unable to remove temp invoice [id=" + tempId + "]");
            }
            if (log.isDebugEnabled()) {
                log.debug("changeNumber() statement exec done [old=" + salesInvoice.getId()
                        + ", new=" + id + "]");
            }
        }
        return (SalesInvoice) load(id);
    }

    private Long createTempId() {
        Long result = -1L;
        while (exists(result)) {
            result = (result - 1L);
        }
        return result;
    }

    public CreditNote add(Employee user, Invoice invoice, SalesCreditNoteType bookingType, boolean copyItems) {
        CreditNote note = creditNotes.create(user, invoice, bookingType, copyItems);
        save(invoice);
        return note;
    }

    public SalesInvoice cancel(Employee user, SalesInvoice invoice, Date date, Long customId) throws ClientException {
        Cancellation result = cancellations.create(user, invoice, date, customId);
        invoice.cancel(user, result);
        save(invoice);
        return invoice;
    }

    public SalesInvoice restoreCanceled(Employee user, Cancellation cancellation) throws ClientException {
        SalesInvoiceImpl invoice = (SalesInvoiceImpl) load(cancellation.getReference());
        cancellations.delete(cancellation, user);
        invoice.updateStatus(Record.STAT_SENT);
        save(invoice);
        return invoice;
    }

    public boolean exists(Long recordId) {
        return exists(salesInvoicesTable, recordId);
    }

    public int count(Long referenceId) {
        return count(salesInvoicesTable, referenceId);
    }

    public int countBySales(Long salesId) {
        return countBySales(salesInvoicesTable, salesId);
    }

    public Date getCreated(Long id) {
        return getCreated(salesInvoicesTable, id);
    }

    public List<ListItem> getProductListing(Long productId) {
        return getArtcleListing(
                salesInvoicesTable,
                salesInvoiceItemsTable,
                productId);
    }

    @Override
    protected String getAdditionalProductListingAndClauses() {
        return " and r.is_canceled = false";
    }

    @Override
    public List<SalesInvoice> getByContact(Long contactId) {
        return (List<SalesInvoice>) super.getByContact(contactId);
    }

    public List<SalesInvoice> getByOrder(Order order) {
        List<SalesInvoice> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT id FROM ");
        sql.append(salesInvoicesTable).append(" WHERE reference_id = ").append(order.getId());
        List<Long> ids = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add((SalesInvoice) load(ids.get(i)));
        }
        return result;
    }

    public List<SalesInvoice> getByPayments(Date startDate, Date endDate) {
        assert (startDate != null && endDate != null);
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.id in (select p.recordId from ")
                .append(SalesPaymentImpl.class.getName())
                .append(" p where p.paid >= :startdate and p.paid < :enddate)");
        String hql = query.toString();
        List<SalesInvoice> result = getCurrentSession().createQuery(hql)
                .setParameter("startdate", startDate)
                .setParameter("enddate", endDate).list();
        if (log.isDebugEnabled()) {
            log.debug("findByPayment() done [from=" + startDate
                    + ", til=" + endDate
                    + ", count=" + result.size()
                    + ", query=" + hql
                    + "]");
        }
        loadAfterFind(result);
        return result;
    }

    public List<SalesInvoice> getBySales(Sales sales) {
        return find(sales, "reference_id");
    }

    public List<SalesInvoice> getPartialByInvoice(SalesInvoice invoice) {
        List<SalesInvoice> result = new ArrayList<>();
        if (invoice.getReference() != null) {
            List<? extends FinanceRecord> all = getByReference(invoice.getReference());
            for (int i = 0, j = all.size(); i < j; i++) {
                SalesInvoice salesInvoice = (SalesInvoice) all.get(i);
                if (!salesInvoice.getId().equals(invoice.getId()) && salesInvoice.isPartial()) {
                    result.add(salesInvoice);
                }
            }
        }
        return result;
    }

    public List<SalesInvoice> getPartialBySales(Sales sales) {
        List<SalesInvoice> result = new ArrayList<>();
        if (sales != null) {
            List<SalesInvoice> all = getBySales(sales);
            for (int i = 0, j = all.size(); i < j; i++) {
                SalesInvoice salesInvoice = all.get(i);
                if (salesInvoice.isPartial()) {
                    result.add(salesInvoice);
                }
            }
        }
        return result;
    }

    public List<SalesInvoice> getThirdPartyBySales(Sales sales) {
        return find(sales, "third_party_id");
    }

    private List<SalesInvoice> find(Sales sales, String column) {
        List<SalesInvoice> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT id FROM ");
        sql
                .append(salesInvoicesTable)
                .append(" WHERE ")
                .append(column)
                .append(" IN (SELECT id FROM ")
                .append(salesOrdersTable)
                .append(" WHERE sales_id = ")
                .append(sales.getId())
                .append(")");
        List<Long> ids = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add((SalesInvoice) load(ids.get(i)));
        }
        return result;
    }

    public Double getSummary(Sales sales) {
        StringBuilder sql = new StringBuilder("SELECT sum(net_amount) FROM ");
        sql
                .append(salesInvoicesTable)
                .append(" WHERE sales_id = ")
                .append(sales.getId());
        return jdbcTemplate.queryForObject(sql.toString(), Double.class);
    }

    @Override
    public Record load(Long id) {
        SalesInvoiceImpl invoice = (SalesInvoiceImpl) super.load(id);
        loadCreditNotes(invoice);
        loadCancellation(invoice);
        return invoice;
    }

    @Override
    protected void loadAfterFind(List<? extends FinanceRecord> resultingList) {
        loadPayments(resultingList);
        loadCreditNotes(resultingList);
        loadCancellation(resultingList);
    }

    @Override
    protected Class<SalesInvoiceImpl> getPersistentClass() {
        return SalesInvoiceImpl.class;
    }

    @Override
    protected void loadPayments(PaymentAwareRecord record) {
        if (isSupportingPayments()) {
            SalesInvoiceImpl invoice = (SalesInvoiceImpl) record;
            List<Payment> list = null;
            if (invoice.getReference() == null) {
                list = getPayments().getByRecord(invoice);
            } else {
                list = getPayments().getByReference(invoice.getReference());
            }
            for (int i = 0, j = list.size(); i < j; i++) {
                Payment next = list.get(i);
                // check if invoice is a project based invoice
                if (!invoice.isItemsChangeable() && invoice.getReference() != null) {
                    // check if payment is related to invoice or downpayment
                    // but not another invoice existing in same sales context
                    boolean dpExists = downpayments.exists(next.getRecordId());
                    if (next.getRecordId().equals(invoice.getId()) || dpExists) {
                        if (dpExists && next instanceof AbstractPayment) {
                            Downpayment dobj = (Downpayment) downpayments.load(next.getRecordId());
                            if (!dobj.isTaxFree()) {
                                ((AbstractPayment) next).setTaxRate(dobj.getTax());
                            }
                        }
                        invoice.getPayments().add(next);
                    }

                } else if (next.getRecordId().equals(invoice.getId())) {
                    // invoice is standalone
                    invoice.getPayments().add(next);
                }
            }
        }
    }

    @Override
    protected List<RecordDisplay> addPayments(List<RecordDisplay> invoices) {
        long startTime = System.currentTimeMillis();
        List<RecordDisplay> result = new ArrayList<>();
        // function: get_sales_invoice_paid_amount(record.id, record.reference_id)
        String query = "SELECT get_sales_invoice_paid_amount(?,?)";
        for (int i = 0, j = invoices.size(); i < j; i++) {
            RecordDisplay invoice = invoices.get(i);
            List<Double> paid = (List<Double>) jdbcTemplate.query(
                    query.toString(),
                    new Object[] { invoice.getId(), invoice.getReference() },
                    new int[] { Types.BIGINT, Types.BIGINT },
                    getDoubleRowMapper());
            invoice.setPaidAmount(paid.isEmpty() ? 0d : paid.get(0));
            result.add(invoice);
        }
        if (log.isDebugEnabled()) {
            log.debug("addPayments() done [duration=" + (System.currentTimeMillis() - startTime) + "ms]");
        }
        return result;
    }

    @Override
    public void delete(Record record, Employee user) {
        try {
            super.delete(record, user);
            DeletedRecordImpl obj = new DeletedRecordImpl(
                    record.getId(),
                    record.getType(),
                    record.getReference(),
                    record.getCreated(),
                    record.getCreatedBy(),
                    user == null ? null : user.getId(),
                    record.getBusinessCaseId());
            getCurrentSession().saveOrUpdate(DeletedRecordImpl.class.getName(), obj);
        } catch (Throwable e) {
            log.error("delete() failed [class=" + e.getClass().getName()
                    + ", message=" + e.getMessage()
                    + "]\nStackTrace:\n", e);
        }
    }

    private void loadCreditNotes(List<? extends FinanceRecord> records) {
        for (int i = 0, j = records.size(); i < j; i++) {
            loadCreditNotes((SalesInvoiceImpl) records.get(i));
        }
    }

    private void loadCancellation(List<? extends FinanceRecord> records) {
        for (int i = 0, j = records.size(); i < j; i++) {
            loadCancellation((SalesInvoiceImpl) records.get(i));
        }
    }

    private Long createInvoiceId(
            Employee user,
            RecordType recordType,
            Long company,
            Long userDefinedId,
            Date userDefinedDate) {

        return isSet(userDefinedId) ? userDefinedId : getNextRecordId(user, company, recordType, userDefinedDate);
    }

    @SuppressWarnings("unchecked")
    private void loadCreditNotes(SalesInvoiceImpl invoice) {
        List<CreditNote> creditNoteList = new ArrayList(creditNotes.getByReference(invoice.getId()));
        if (!creditNoteList.isEmpty()) {
            invoice.setCreditNotes(creditNoteList);
        }
    }

    private void loadCancellation(SalesInvoiceImpl invoice) {
        if (invoice.isCanceled()) {
            invoice.setCancellation(cancellations.loadCancellation(invoice));
        }
    }
}
