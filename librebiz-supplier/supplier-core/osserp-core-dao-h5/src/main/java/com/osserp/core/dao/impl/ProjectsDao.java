/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2006 11:57:13 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsItem;
import com.osserp.core.Options;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerSummary;
import com.osserp.core.customers.SalesSummary;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.dao.FlowControlWastebaskets;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.RequestFcsActions;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.DefaultSalesImpl;
import com.osserp.core.model.FlowControlItemImpl;
import com.osserp.core.model.SalesClosedStatusImpl;
import com.osserp.core.model.SalesImpl;
import com.osserp.core.model.projects.ProjectImpl;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesClosedStatus;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectsDao extends AbstractBusinessCaseDao implements Projects {
    private static Logger log = LoggerFactory.getLogger(ProjectsDao.class.getName());
    private FlowControlActions actionsDao = null;
    private FlowControlWastebaskets wastebaskets = null;
    private RequestFcsActions requestFcsActions = null;
    private String projectsTable = null;
    private String requestsTable = null;

    public ProjectsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory template,
            FlowControlActions actions,
            FlowControlWastebaskets wastebaskets,
            RequestFcsActions requestFcsActions,
            Contacts contacts,
            OptionsCache options) {
        super(jdbcTemplate, tables, template, contacts, options);
        this.actionsDao = actions;
        this.wastebaskets = wastebaskets;
        this.requestFcsActions = requestFcsActions;
        this.projectsTable = getTable(TableKeys.PROJECTS);
        this.requestsTable = getTable(TableKeys.PLANS);
    }
    
    /**
     * Provides the next sales id depending on requestType
     * and application configuration. Override this method
     * to provide your own number create method.
     * @param request
     * @return id
     */
    protected Long getNextSalesId(Request request) {
        if (salesIdByRequest()) {
            return request.getPrimaryKey();
        } 
        Long result = nextId(request.getType());
        while (exists(result)) {
            result = nextId(request.getType());
        }
        return result;
    }

    /**
     * Provides the next sales id by provided request
     * @param request
     * @return next sales id
     */
    private Long nextId(BusinessType type) {
        if (type == null || type.getWorkflow() == null) {
            throw new BackendException("type or workflow was null!");
        }
        return getNextLong(type.getWorkflow().getSequenceName());
    }
    
    protected final boolean salesIdByRequest() {
        return "request".equalsIgnoreCase(
                getPropertyString(SystemConfig.SALES_ID_GENERATOR));
    }

    public boolean exists(Long projectId) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT count(*) FROM ")
                .append(projectsTable)
                .append(" WHERE id = ?");
        final Object[] params = { projectId };
        final int[] types = { Types.BIGINT };
        int cnt = jdbcTemplate.queryForObject(query.toString(), params, types, Integer.class);
        return (cnt > 0);
    }

    public BusinessCase findById(Long businessId) {
        if (exists(businessId)) {
            try {
                return load(businessId);
            } catch (Exception e) {
                log.warn("findById() caught exception loading request by an id reported as existing"
                        + " [id=" + businessId + ", message=" + e.getMessage() + "]", e);
            }
        }
        return null;
    }

    public BusinessCase findByExternalReference(String externalReference) {
        BusinessCase object = null;
        if (!isEmpty(externalReference)) {
            final Object[] params = { externalReference };
            final int[] types = { Types.VARCHAR };
            StringBuilder query = new StringBuilder(64);
            query
                    .append("SELECT id FROM ")
                    .append(projectsTable)
                    .append(" WHERE plan_id IN (SELECT plan_id FROM ")
                    .append(requestsTable)
                    .append(" WHERE external_ref = ?)");
            List<Long> result = (List<Long>) jdbcTemplate.query(
                query.toString(), 
                params,
                types,
                getLongRowMapper());
            object = result.isEmpty() ? null : load(result.get(0));
            if (log.isDebugEnabled() && object != null) {
                log.debug("findByExternalReference() object retrieved [businessId="
                        + object.getPrimaryKey() + ", externalReference="
                        + externalReference + "]");
            }
        }
        return object;
    }

    public Sales create(Long employee, Request request) {
        return create(employee, request, null);
    }


    protected Sales create(Long employee, Request request, Long preselectedId) {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [user=" + employee + ", request=" + request.getRequestId() + "]");
        }
        Sales created = null;
        Long newId = isEmpty(preselectedId) ? getNextSalesId(request) : preselectedId;
        if (request.getType().isStandardProject()) {
            created = new ProjectImpl(newId, employee, request);
        } else {
            created = new DefaultSalesImpl(newId, employee, request);
        }
        if (log.isDebugEnabled()) {
            log.debug("create() object initialized [sales=" + created.getId()
                    + ", request=" + request.getRequestId()
                    + ", type=" + created.getType().getId()
                    + ", class=" + created.getClass().getName()
                    + "]");
        }
        List<FcsAction> throwables = actionsDao.getWastebasketStartUp(request.getType());
        if (throwables != null && !throwables.isEmpty()) {
            for (int i = 0, j = throwables.size(); i < j; i++) {
                FcsAction next = throwables.get(i);
                wastebaskets.add(
                    created.getPrimaryKey(),
                    created.getCreatedBy(),
                    next.getId());
            }
        }
        // close request
        FcsAction closing = getRequestClosingAction(request.getType().getId());
        if (closing != null) {
            try {
                request.addFlowControl(
                        closing,
                        employee,
                        null,
                        null,
                        null,
                        null);
            } catch (Exception e) {
                // should not happen on create
            }
        }
        request.setStatus(BusinessCase.CLOSED);

        FcsAction initialAction = getSalesStartingAction(request.getType().getId());
        // initialize sales
        if (initialAction != null) {
            try {
                created.addFlowControl(
                        initialAction,
                        created.getCreatedBy(),
                        created.getCreated(),
                        null,
                        null,
                        null);
            } catch (ClientException e) {
                // should not happen on create
            }
        }
        save(created);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + created.getId() 
                    + ", request=" + request.getRequestId() 
                    + ", type=" + request.getTypeId() 
                    + ", initialAction=" 
                    + (initialAction == null ? "null" : initialAction.getId()) 
                    + ", created=" + created.getCreated() 
                    + ", cancelled=" + created.isCancelled() + "]");
            
        }
        return created;
    }

    
    /* (non-Javadoc)
     * @see com.osserp.core.dao.BusinessCaseDao#createBusinessCase(java.lang.Long, java.lang.String, java.util.Date, com.osserp.core.BusinessType, com.osserp.core.system.BranchOffice, com.osserp.core.customers.Customer, java.lang.Long, java.lang.Long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Long, com.osserp.core.crm.Campaign, com.osserp.common.Option)
     */
    public BusinessCase createBusinessCase(
            Long businessId, 
            String externalReference,
            String externalUrl,
            Date createdDate, 
            BusinessType businessType, 
            BranchOffice branchOffice,
            Customer customer, 
            Long salesPersonId, 
            Long projectManagerId, 
            String name, 
            String street, 
            String streetAddon, 
            String zipcode, 
            String city,
            Long country, 
            Campaign campaign, 
            Option origin) {

        Sales sales = businessId == null ? null : (Sales) findById(businessId);
        Request request = sales != null ? sales.getRequest() : null;
        if (request == null) {
            
            request = createRequestImpl(
                    businessId, 
                    externalReference,
                    externalUrl,
                    createdDate, 
                    businessType, 
                    branchOffice, 
                    customer, 
                    salesPersonId, 
                    projectManagerId, 
                    name, 
                    street, 
                    streetAddon, 
                    zipcode, 
                    city, 
                    country, 
                    campaign, 
                    origin);
            request = (Request) initializeBranch(request, false);
            getCurrentSession().saveOrUpdate(request);
        }
        if (sales == null) {
            sales = create(request.getCreatedBy(), request, businessId);
        }
        save(sales);
        if (log.isDebugEnabled()) {
            log.debug("createBusinessCase() done [id=" 
                    + sales.getPrimaryKey()
                    + ", type=" + businessType.getId()
                    + ", class=" + sales.getClass().getName()
                    + "]");
        }
        return sales;
    }

    public BusinessCase update(
            BusinessCase businessCase, 
            BranchOffice office, 
            Long salesPersonId, 
            Long projectManagerId, 
            String name, 
            String street,
            String streetAddon, 
            String zipcode, 
            String city, 
            Long country, 
            Campaign campaign, 
            Option origin) {
        Sales sales = load(businessCase.getPrimaryKey());
        sales.getRequest().update(
                office, 
                salesPersonId, 
                projectManagerId, 
                name, 
                street, 
                streetAddon, 
                zipcode, 
                city, 
                country, 
                campaign, 
                origin);
        save(sales);
        return sales;
    }

    public Sales load(Long id) {
        try {
            return (Sales) getCurrentSession().load(SalesImpl.class.getName(), id);
        } catch (Throwable t) {
            log.error("load() failed [id=" + id + ", message=" + t.getMessage() + "]", t);
            throw new BackendException(t.getMessage(), t);
        }
    }

    public void save(Sales sales) {
        persist(sales);
    }

    public void save(FcsItem item) {
        getCurrentSession().saveOrUpdate(FlowControlItemImpl.class.getName(), item);
    }

    public List<Sales> findByCustomer(Long customerId) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT id FROM ")
                .append(projectsTable)
                .append(" WHERE customer_id = ?");
        final Object[] params = { customerId };
        final int[] types = { Types.BIGINT };
        List<Long> ids = (List<Long>) jdbcTemplate.query(
                query.toString(),
                params,
                types,
                getLongRowMapper());
        List<Sales> result = new java.util.ArrayList<Sales>();
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add(load(ids.get(i)));
        }
        return result;
    }

    public boolean isInstallerPresent(Long installerId) {
        return !(findIdsByInstaller(installerId).isEmpty());
    }

    public List<Sales> findByInstaller(Long installerId) {
        List<Long> ids = findIdsByInstaller(installerId);
        List<Sales> result = new java.util.ArrayList<Sales>();
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add(load(ids.get(i)));
        }
        return result;
    }

    public Sales findByRequest(Long requestId) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT id FROM ")
                .append(projectsTable)
                .append(" WHERE plan_id = ?");
        final Object[] params = { requestId };
        final int[] types = { Types.BIGINT };
        List<Long> list = (List<Long>) jdbcTemplate.query(
                query.toString(),
                params,
                types,
                getLongRowMapper());
        if (list.isEmpty()) {
            return null;
        }
        return load(list.get(0));
    }

    public String getName(Long projectId) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT name FROM ")
                .append(requestsTable)
                .append(" WHERE plan_id = (select plan_id from ")
                .append(projectsTable)
                .append(" WHERE id = ?)");
        final Object[] params = { projectId };
        final int[] types = { Types.BIGINT };
        List<String> list = (List<String>) jdbcTemplate.query(
                query.toString(),
                params,
                types,
                getStringRowMapper());
        return (list.isEmpty() ? "" : list.get(0));
    }

    public void addSummary(CustomerSummary summary) {
        if (summary.getId() == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        StringBuilder query = new StringBuilder(64);
        query
                .append(ProjectSummaryRowMapper.SELECT_FROM)
                .append(projectsTable)
                .append(" p, ")
                .append(requestsTable)
                .append(" pl WHERE p.plan_id = pl.plan_id AND p.customer_id = ? " +
                        "ORDER BY p.created DESC");
        final Object[] params = { summary.getId() };
        final int[] types = { Types.BIGINT };
        List<SalesSummary> list = (List<SalesSummary>) jdbcTemplate.query(
                query.toString(),
                params,
                types,
                new RowMapperResultSetExtractor(
                        new ProjectSummaryRowMapper(
                                getOptionsCache(), actionsDao)));
        for (int i = 0, j = list.size(); i < j; i++) {
            SalesSummary ps = list.get(i);
            summary.addSales(ps);
            if (log.isDebugEnabled()) {
                log.debug("addSummary() added sales [id=" + ps.getId()
                        + ", cancelled=" + ps.isCancelled() 
                        + ", closed=" + ps.isClosed()
                        + ", status=" + ps.getStatus()
                        + ", created=" + ps.getCreated()
                        + "]");
            }
        }
    }

    public SalesClosedStatus getClosedSatusDefault() {
        List<SalesClosedStatus> list = getCurrentSession().createQuery(
                "from " + SalesClosedStatusImpl.class.getName()
                + " o where o.defaultStatus = true").list();
        return (list.isEmpty() ? null : list.get(0));
    }

    public List<SalesClosedStatus> getClosedSatusList() {
        return getCurrentSession().createQuery(
                "from " + SalesClosedStatusImpl.class.getName()
                + " o order by o.name").list();
    }

    public List<Long> getStatusList() {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT DISTINCT status FROM ")
                .append(projectsTable)
                .append(" WHERE status > 0 ")
                .append("ORDER BY status");
        List<Long> list = (List<Long>) jdbcTemplate.query(
                query.toString(),
                getLongRowMapper());
        return list;
    }

    private List<Long> findIdsByInstaller(Long installerId) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT id FROM ")
                .append(projectsTable)
                .append(" WHERE installer_id = ")
                .append(installerId);
        List<Long> list = (List<Long>) jdbcTemplate.query(
                query.toString(),
                getLongRowMapper());
        return list;
    }

    @Override
    protected List<FcsAction> findActions(Long businessType) {
        return actionsDao.findByType(businessType);
    }

    @Override
    protected final void persist(BusinessCase businessCase) {
        getCurrentSession().saveOrUpdate(businessCase);
    }
    
    @Override
    protected BusinessCase initializeBranch(BusinessCase businessCase, boolean force) {
        Request request = (businessCase instanceof Request) ? 
                (Request) businessCase : ((Sales) businessCase).getRequest();
        BusinessType type = request.getType();
        if (type.getDefaultBranch() != null && (force || request.getBranch() == null)) {
            BranchOffice office = fetchBranchOffice(type.getDefaultBranch());
            if (office != null) {
                if (type.isForceDefaultBranch()) {
                    request.updateBranch(office);
                } else if (request.getBranch() == null) {
                    request.updateBranch(office);
                }
            }
        }
        return businessCase;
    }
    
    protected BranchOffice fetchBranchOffice(Long id) {
        return (BranchOffice) getOption(Options.BRANCH_OFFICES, id);
    }

    private FcsAction getRequestClosingAction(Long businessType) {
        List<FcsAction> actions = requestFcsActions.findByType(businessType);
        FcsAction closingAction = null;
        for (int i = 0, j = actions.size(); i < j; i++) {
            FcsAction fcsa = actions.get(i);
            if (fcsa.isClosing()) {
                closingAction = fcsa;
                break;
            }
        }
        return closingAction;
    }

    private FcsAction getSalesStartingAction(Long businessType) {
        List<FcsAction> all = actionsDao.findByType(businessType);
        if (!all.isEmpty()) {
            return all.get(0);
        }
        return null;
    }

}
