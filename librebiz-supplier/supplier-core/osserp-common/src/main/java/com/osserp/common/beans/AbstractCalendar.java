/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 30, 2008 10:56:00 AM 
 * 
 */
package com.osserp.common.beans;

import java.util.Date;

import com.osserp.common.Calendar;
import com.osserp.common.util.CalendarUtil;
import com.osserp.common.util.DateUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractCalendar extends AbstractMonth implements Calendar {

    /**
     * Creates a new instance of Calendar initialized with current date
     */
    protected AbstractCalendar() {
        super();
        Integer[] date = DateUtil.getTime(DateUtil.getCurrentDate());
        setDate(date[0], date[1], date[2]);
    }

    /**
     * Creates a new calendar with explicit values
     * @param day
     * @param month
     * @param year
     */
    protected AbstractCalendar(int day, int month, int year) {
        setDate(day, month, year);
    }

    /**
     * Creates a new calendar by another calendar
     * @param other
     */
    protected AbstractCalendar(Calendar other) {
        setDate(other.getDay(), other.getMonth(), other.getYear());
    }

    /**
     * Creates a new calendar by specific date ignoring time
     * @param date
     */
    protected AbstractCalendar(Date date) {
        super(date);
    }

    @Override
    public Integer getDay() {
        return super.getDay();
    }

    public String getDayDisplay() {
        int dow = getDayOfWeek(getDay());
        String result = "Sonntag";
        switch (dow) {
            case 1:
                result = "Montag";
                break;
            case 2:
                result = "Dienstag";
                break;
            case 3:
                result = "Mittwoch";
                break;
            case 4:
                result = "Donnerstag";
                break;
            case 5:
                result = "Freitag";
                break;
            case 6:
                result = "Samstag";
        }
        return result;
    }

    public String getDayDisplayKey() {
        int dow = getDayOfWeek(getDay());
        String result = "SO";
        switch (dow) {
            case 1:
                result = "MO";
                break;
            case 2:
                result = "DI";
                break;
            case 3:
                result = "MI";
                break;
            case 4:
                result = "DO";
                break;
            case 5:
                result = "FR";
                break;
            case 6:
                result = "SA";
        }
        return result;
    }

    /*
     * public String getDayDisplay() { int dow = getDayOfWeek(getDay()); String result = "day.sunday"; switch (dow) { case 1 : result = "day.monday";
     * break; case 2 : result = "day.tuesday"; break; case 3 : result = "day.wednesday"; break; case 4 : result = "day.thursday"; break; case 5 : result =
     * "day.friday"; break; case 6 : result = "day.saturday"; } return result; }
     * 
     * public String getDayDisplayKey() { return getDayDisplay() + ".short"; }
     */

    @Override
    public void setDate(Integer day, Integer month, Integer year) {
        super.setDate(day, month, year);
    }

    public int getDayOfWeek(int day) {
        return CalendarUtil.getDayOfWeek(createDay(day), getMonth(), getYear());
    }

    public int getWeekOfYear(int day) {
        return CalendarUtil.getWeekOfYear(createDay(day), getMonth(), getYear());
    }

    // helpers

    public boolean isWeekend() {
        int day = getDayOfWeek(getDay());
        return (day == 6 || day == 7);
    }

    public boolean isAfterDay(Date date) {
        Date local = getDate();
        return DateUtil.isLaterDay(local, date);
    }

    public boolean isFuture() {
        if (isToday()) {
            return false;
        }
        return DateUtil.isLaterDay(getDate(), DateUtil.getCurrentDate());
    }

    public boolean isPast() {
        if (isToday()) {
            return false;
        }
        return DateUtil.isLaterDay(DateUtil.getCurrentDate(), getDate());
    }

    public boolean isToday() {
        return DateUtil.isToday(getDate());
    }

    public boolean isTomorrow() {
        Date tomorrow = DateUtil.addDays(DateUtil.getCurrentDate(), 1);
        return DateUtil.isSameDay(getDate(), tomorrow);
    }

    public boolean isYesterday() {
        Date yesterday = DateUtil.subtractDays(DateUtil.getCurrentDate(), 1);
        return DateUtil.isSameDay(getDate(), yesterday);
    }

    private Integer createDay(int day) {
        if (day == 0) {
            return 1;
        }
        return day;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Calendar) {
            Calendar cal = (Calendar) obj;
            return (cal.getDay().equals(getDay())
                    && cal.getMonth().equals(getMonth())
                    && cal.getYear().equals(getYear()));
        }
        return super.equals(obj);
    }
}
