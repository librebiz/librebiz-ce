<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.id}"/> - <o:out value="${view.bean.name}"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="shortkey"><fmt:message key="shortkey" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${empty view.bean.key}">
                                <fmt:message key="undefined" />
                            </c:when>
                            <c:otherwise>
                                <o:out value="${view.bean.key}"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="companyName"><fmt:message key="company"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:options name="systemCompanies" value="${view.bean.company}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="requestContext"><fmt:message key="requestContextLabel"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.context}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="salesContext"><fmt:message key="orderContextLabel"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.salesContext}"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="requestContextExclusiveLabel"><fmt:message key="requestContextLabel"/><span class="tab smalltext">(<fmt:message key="exclusive"/>)</span></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.requestContextOnly}">
                                <fmt:message key="bigYes"/>
                                <span class="tab smalltext">(<fmt:message key="inThisContextValidOnly"/>)</span>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                                <span class="tab smalltext">(<fmt:message key="validInAllContexts"/>)</span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="interestOnly"><fmt:message key="interestOnly"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.interestContext}">
                                <fmt:message key="bigYes"/>
                                <span class="tab smalltext">(<fmt:message key="unspecifiedYet"/>)</span>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                                <span class="tab smalltext">(<fmt:message key="definedType"/>)</span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="directSales"><fmt:message key="directSales"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.directSales}">
                                <fmt:message key="bigYes"/>
                                <span class="tab smalltext">(<fmt:message key="requestJumpOver"/>)</span>
                            </c:when>
                            <c:otherwise>
                                 <fmt:message key="bigNo"/>
                                 <span class="tab smalltext">(<fmt:message key="requestRequired"/>)</span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="salesPersonAssignment"><fmt:message key="salesPersonAssignment"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.salesPersonByCollector}">
                                <fmt:message key="salesPersonByCollector"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="salesPersonByUser"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="serviceOrder"><fmt:message key="serviceOrder"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.service}">
                                <fmt:message key="bigYes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="subscription"><fmt:message key="subscription"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.subscription}">
                                <fmt:message key="bigYes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="packageRequired"><fmt:message key="packageRequired"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.packageRequired}">
                                <fmt:message key="bigYes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="trading"><fmt:message key="trading"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.trading}">
                                <fmt:message key="bigYes"/> <span class="tab smalltext">(<fmt:message key="noProject"/>)</span>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/> <span class="tab smalltext">(<fmt:message key="project"/>)</span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="wholesale"><fmt:message key="wholesale"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.wholeSale}">
                                <fmt:message key="bigYes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="offerCreation"><fmt:message key="offerCreation"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.supportingOffers}">
                                <fmt:message key="bigYes"/> <span class="tab smalltext">(<fmt:message key="possible"/>)</span>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/> <span class="tab smalltext">(<fmt:message key="notPossible"/>)</span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="downpayments"><fmt:message key="downpayments"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.supportingDownpayments}">
                                <fmt:message key="bigYes"/> <span class="tab smalltext">(<fmt:message key="possible"/>)</span>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/> <span class="tab smalltext">(<fmt:message key="notPossible"/>)</span>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="pictureUpload"><fmt:message key="pictureUpload"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.supportingPictures}">
                                <fmt:message key="bigYes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <oc:systemPropertyEnabled name="businessCaseInstallationSupport">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="installationDate"><fmt:message key="installationDate"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.installationDateSupported}">
                                <fmt:message key="bigYes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                            </c:otherwise>
                        </c:choose>
                        </div>
                    </div>
                </div>
            </oc:systemPropertyEnabled>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="calculationContextPriceDisplayStatus"><fmt:message key="calculationContext"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.includePriceByDefault}">
                                <fmt:message key="priceDisplayOn"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="priceDisplayOff"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recordByCalculationLabel"><fmt:message key="recordByCalculationLabel"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.recordByCalculation}">
                                <fmt:message key="bigYes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${view.bean.recordByCalculation}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="calculationConfig"><fmt:message key="calculationConfig"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.calculationConfig.name}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty view.bean.defaultContractType}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="contractType"><fmt:message key="contractType"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:options name="salesContractTypes" value="${view.bean.defaultContractType}" />
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="externalIdProvided"><fmt:message key="externalIdProvided"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.externalIdProvided}">
                                <fmt:message key="bigYes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="bigNo"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${!empty view.bean.defaultOriginType}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""><fmt:message key="requestType" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:options name="requestOriginTypes" value="${view.bean.defaultOriginType}" />
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty view.bean.defaultOrigin}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""><fmt:message key="origin" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:options name="campaigns" value="${view.bean.defaultOrigin}" />
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty view.lastId}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="latestOrderNumberAndLink"><fmt:message key="latestOrder"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <a href="<c:url value="/loadSales.do?id=${view.lastId}&exit=businessTypeConfigDisplay"/>"><o:out value="${view.lastId}"/></a>
                        </div>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
