/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 7, 2006 6:26:48 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.Option;
import com.osserp.common.Parameter;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.common.mail.EmailAddress;
import com.osserp.core.dao.Emails;
import com.osserp.core.dao.TableKeys;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
public class EmailsDao extends AbstractTablesAwareSpringDao implements Emails {

    private String emailTable = null;

    public EmailsDao(JdbcTemplate jdbcTemplate, Tables tables) {
        super(jdbcTemplate, tables);
        this.emailTable = getTable(TableKeys.EMAILS);
    }

    public List<Option> findAll() {
        StringBuffer sql = new StringBuffer(64);
        sql
                .append("SELECT DISTINCT contact_id,email FROM ")
                .append(emailTable)
                .append(" WHERE contact_id IS NOT NULL");
        return (List<Option>) jdbcTemplate.query(sql.toString(), getEmailListRowMapper());
    }

    public List<Option> findEqual(String email) {
        if (email == null) {
            return new ArrayList<Option>();
        }
        String searchFor = email.toLowerCase();
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT DISTINCT contact_id,email FROM ")
                .append(emailTable)
                .append(" WHERE contact_id IS NOT NULL AND email = ?");
        final Object[] params = { searchFor };
        final int[] types = { Types.VARCHAR };

        List<Option> list = (List<Option>) jdbcTemplate.query(
                sql.toString(), params, types, getEmailListRowMapper());
        return list;
    }

    public List<EmailAddress> findEmails(Long contactId) {
        if (contactId == null) {
            return new ArrayList<EmailAddress>();
        }
        StringBuilder sql = EmailRowMapper.getQuery(emailTable);
        sql.append(" WHERE contact_id = ?");
        final Object[] params = { contactId };
        final int[] types = { Types.BIGINT };

        List<EmailAddress> list = (List<EmailAddress>) jdbcTemplate.query(
                sql.toString(), params, types, getMailReader());
        return list;
    }

    public List<Parameter> findMatchingEmails(String pattern, boolean startsWith) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT email FROM ")
                .append(emailTable)
                .append(" WHERE contact_id IS NOT NULL AND email ~* '");
        if (startsWith) {
            sql.append("^");
        }
        sql
                .append(pattern)
                .append("') ORDER BY email");
        return (List<Parameter>) jdbcTemplate.query(sql.toString(), getParameterRowMapper());
    }

    protected RowMapperResultSetExtractor getMailReader() {
        return new RowMapperResultSetExtractor(new EmailRowMapper());
    }

    private RowMapperResultSetExtractor getEmailListRowMapper() {
        return new RowMapperResultSetExtractor(new EmailListRowMapper());
    }

    class EmailListRowMapper extends AbstractRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new OptionImpl(
                    this.fetchLong(rs, "contact_id"),
                    this.fetchString(rs, "email"));
        }
    }
}
