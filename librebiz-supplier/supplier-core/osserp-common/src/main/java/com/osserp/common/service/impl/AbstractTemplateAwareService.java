/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 24, 2008 1:37:15 PM 
 * 
 */
package com.osserp.common.service.impl;

import java.io.File;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.exception.VelocityException;

import org.springframework.ui.velocity.VelocityEngineUtils;

import com.osserp.common.BackendException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dms.DocumentException;
import com.osserp.common.service.TemplateManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractTemplateAwareService extends AbstractService {
    private static Logger log = LoggerFactory.getLogger(AbstractTemplateAwareService.class.getName());
    private VelocityEngine engine = null;

    protected AbstractTemplateAwareService(VelocityEngine engine) {
        this.engine = engine;
    }

    public final String createText(String templateName, Map model) {
        try {
            String templatePath = getTemplatePath();
            if (templatePath != null) {
                model.put("templatePath", templatePath);
            }
            String templateFile = TemplateManager.createFilename(templateName);
            String text = VelocityEngineUtils.mergeTemplateIntoString(
                    engine, templateFile, "UTF-8", model);
            /*
             * if (log.isDebugEnabled()) { log.debug("createText() done:\n" + text); }
             */
            return text;
        } catch (ResourceNotFoundException rnf) {
            log.error("createText() caught resource exception [templateFile="
                    + templateName + ", message=" + rnf.getMessage() + "]");
            throw new DocumentException(ErrorCode.STYLESHEET_MISSING, rnf);

        } catch (VelocityException e) {
            log.error("createText() caught exception [templateFile="
                    + templateName + ", message=" + e.getMessage() + "]", e);
            throw new BackendException(e);
        }
    }
    
    public String getTemplatePath() {
        Object obj = engine.getProperty("file.resource.loader.path");
        if (obj == null) {
            log.warn("getTemplatePath() did not find requested property "
                    + "file.resource.loader.path");
            return null;
        } 
        return obj.toString();
    }
    
    public boolean templateExists(String templateName) {
        boolean result = false;
        if (isSet(templateName)) {
            StringBuilder filename = new StringBuilder();
            String path = getTemplatePath();
            if (isSet(path)) {
                filename.append(path);
                if (!templateName.startsWith("/")) {
                    filename.append("/");
                }
            }
            String fn = filename.append(TemplateManager.createFilename(templateName)).toString();
            File file = new File(fn);
            result = file.exists() && file.canRead();
            if (!result && file.exists()) {
                log.warn("templateExists() returns true but file not readable");
            }
            if (log.isDebugEnabled()) {
                log.debug("templateExists() done [available=" + result
                        + ", template=" + fn + "]");
            }
        }
        return result;
    }
    
    protected VelocityEngine getEngine() {
        return engine;
    }

    protected void setEngine(VelocityEngine engine) {
        this.engine = engine;
    }
}
