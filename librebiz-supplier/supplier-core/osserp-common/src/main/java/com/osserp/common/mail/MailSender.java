/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 2, 2007 10:22:54 AM 
 * 
 */
package com.osserp.common.mail;

import java.util.List;

import com.osserp.common.ClientException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface MailSender {

    /**
     * Sends a debug message to a fixed recipient
     * @param message
     */
    void sendDebug(String message);

    /**
     * Sends an error message to a fixed recipient
     * @param message
     */
    void sendError(String message);

    /**
     * Sends an error message to a fixed recipient
     * @param message
     * @param throwable
     */
    void sendError(String message, Throwable throwable);

    /**
     * Sends an info mail with fixed originator and subject
     * @param recipients
     * @param message
     */
    void sendInfo(String[] recipients, String message);

    /**
     * Sends a noreply mail with fixed originator and subject
     * @param recipients
     * @param message
     */
    void sendNoreply(String[] recipients, String message);

    /**
     * Sends a simple mail to one recipient
     * @param recipient
     * @param subject
     * @param message
     */
    void send(String recipient, String subject, String message);

    /**
     * Sends a simple mail to several recipients
     * @param recipients
     * @param subject
     * @param message
     */
    void send(String[] recipients, String subject, String message);

    /**
     * Sends a simple mail to one recipient
     * @param originator
     * @param recipient
     * @param subject
     * @param message
     */
    void send(String originator, String recipient, String subject, String message);

    /**
     * Sends a simple mail to several recipients
     * @param originator
     * @param recipients
     * @param subject
     * @param message
     */
    void send(String originator, String[] recipients, String subject, String message);

    /**
     * Sends a mail message
     * @param mail
     * @throws ClientException if originator, recipient or attachment invalid
     */
    void send(Mail mail) throws ClientException;

    /**
     * Sends some mail messages
     * @param mails
     */
    void send(List<Mail> mails);
}
