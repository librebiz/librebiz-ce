/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 12, 2007 5:45:14 PM 
 * 
 */
package com.osserp.gui.client.records.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.Options;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.system.SystemCompany;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.records.RecordDisplayView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecordDisplayView extends AbstractView
        implements RecordDisplayView {

    private List<SystemCompany> allCompanies = new ArrayList<SystemCompany>();
    private List<SystemCompany> companies = new ArrayList<SystemCompany>();
    private Set<Long> selectedCompanies = new HashSet<Long>();
    private Double netAmount = null;
    private Double taxAmount = null;
    private Double reducedTaxAmount = null;
    private Double grossAmount = null;
    private Double dueAmount = null;
    private boolean includeInternal = false;

    protected AbstractRecordDisplayView() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        List<Option> list = new ArrayList<>(getOptions(Options.SYSTEM_COMPANIES));
        for (int i = 0, j = list.size(); i < j; i++) {
            SystemCompany next = (SystemCompany) list.get(i);
            allCompanies.add(next);
            companies.add(next);
            selectedCompanies.add(next.getId());
        }
    }

    public List<SystemCompany> getAvailableCompanies() {
        List<SystemCompany> result = new ArrayList<SystemCompany>();
        for (int i = 0, j = allCompanies.size(); i < j; i++) {
            SystemCompany next = allCompanies.get(i);
            if (!includeCompany(next.getId())) {
                result.add(next);
            }
        }
        return result;
    }

    public List<SystemCompany> getCompanies() {
        return companies;
    }

    public void addCompany(Long id) {
        SystemCompany selected = getSelectedCompany(id);
        this.companies.add(selected);
        this.selectedCompanies.add(selected.getId());
        refresh();
    }

    public void removeCompany(Long id) {
        for (Iterator<SystemCompany> i = companies.iterator(); i.hasNext();) {
            if (i.next().getId().equals(id)) {
                i.remove();
                break;
            }
        }
        this.selectedCompanies.remove(id);
        refresh();
    }

    public boolean isIncludeInternal() {
        return includeInternal;
    }

    public void toggleIncludeInternal() {
        this.includeInternal = !this.includeInternal;
        refresh();
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public Double getReducedTaxAmount() {
        return reducedTaxAmount;
    }

    public Double getGrossAmount() {
        return grossAmount;
    }

    public Double getDueAmount() {
        return dueAmount;
    }

    // unused setters

    protected void setCompanies(List<SystemCompany> companies) {
        this.companies = companies;
    }

    protected void setDueAmount(Double dueAmount) {
        this.dueAmount = dueAmount;
    }

    protected void setGrossAmount(Double grossAmount) {
        this.grossAmount = grossAmount;
    }

    protected void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    protected void setReducedTaxAmount(Double reducedTaxAmount) {
        this.reducedTaxAmount = reducedTaxAmount;
    }

    protected void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    protected void setIncludeInternal(boolean includeInternal) {
        this.includeInternal = includeInternal;
    }

    // protected helpers

    protected boolean includeCompany(Long id) {
        return selectedCompanies.contains(id);
    }

    protected void addAmounts(RecordDisplay record) {
        if (netAmount == null) {
            this.netAmount = 0d;
            this.taxAmount = 0d;
            this.reducedTaxAmount = 0d;
            this.grossAmount = 0d;
            this.dueAmount = 0d;
        }
        if (record.isDownpayment()) {
            this.netAmount = netAmount + create(record.getProrateNetAmount());
            this.taxAmount = taxAmount + create(record.getProrateTaxAmount());
            this.reducedTaxAmount = reducedTaxAmount + create(record.getProrateReducedTaxAmount());
            this.grossAmount = grossAmount + create(record.getProrateGrossAmount());
        } else {
            this.netAmount = netAmount + create(record.getNetAmount());
            this.taxAmount = taxAmount + create(record.getTaxAmount());
            this.reducedTaxAmount = reducedTaxAmount + create(record.getReducedTaxAmount());
            this.grossAmount = grossAmount + create(record.getGrossAmount());
        }
        this.dueAmount = dueAmount + create(record.getDueAmount());
    }

    protected void clearAmounts() {
        this.netAmount = 0d;
        this.taxAmount = 0d;
        this.reducedTaxAmount = 0d;
        this.grossAmount = 0d;
        this.dueAmount = 0d;
    }

    // private helpers
    private SystemCompany getSelectedCompany(Long id) {
        return (SystemCompany) CollectionUtil.getByProperty(allCompanies, "id", id);
    }

    private Double create(Double d) {
        return (d == null ? 0d : d);
    }
}
