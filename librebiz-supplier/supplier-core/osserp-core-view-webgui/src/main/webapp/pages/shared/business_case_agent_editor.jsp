<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:set var="view" value="${sessionScope.businessCaseView}" />
<c:set var="businessCase" value="${sessionScope.businessCaseView.request}" />
<c:choose>
    <c:when test="${view.salesContext}">
        <c:set var="url" value="/sales.do" />
    </c:when>
    <c:otherwise>
        <c:set var="url" value="/requests.do" />
    </c:otherwise>
</c:choose>

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="selection" />
        <fmt:message key="agent" />
    </div>
</div>
<div class="modalBoxData">
    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <o:form name="requestForm" url="${url}">
                <input type="hidden" name="method" value="updateAgentConfig" />
                <table class="valueTable">
                    <tbody>
                        <tr>
                            <td><fmt:message key="agent" /></td>
                            <td><o:ajaxSelect selectId="agentId" targetElement="agentConfig_popup">
                                    <option value="<c:url value="${url}"><c:param name="method" value="setAgentConfig"/><c:param name="id" value="-1"/></c:url>"><fmt:message key="selection" /></option>
                                    <c:forEach var="agent" items="${view.agents}">
                                        <option value="<c:url value="${url}"><c:param name="method" value="setAgentConfig"/><c:param name="id" value="${agent.id}"/></c:url>" <c:if test="${!empty view.selectedAgent and view.selectedAgent.id == agent.id}">selected="selected"</c:if>><o:out value="${agent.displayName}" /> /
                                            <o:out value="${agent.address.city}" /></option>
                                    </c:forEach>
                                    <c:if test="${!empty businessCase.agent}">
                                        <c:choose>
                                            <c:when test="${view.agentDeleteMode}">
                                                <option selected="selected" value="<c:url value="${url}"><c:param name="method" value="setAgentConfig"/><c:param name="id" value="0"/></c:url>"><fmt:message key="delete" /></option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="<c:url value="${url}"><c:param name="method" value="setAgentConfig"/><c:param name="id" value="0"/></c:url>"><fmt:message key="delete" /></option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </o:ajaxSelect></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="provision" /></td>
                            <td><c:choose>
                                    <c:when test="${!empty view.selectedAgent}">
                                        <input type="text" name="agentCommission" value="<o:number value="${view.selectedAgent.agentCommission}" format="currency"/>" style="width: 150px;" class="right" />
                                        <input type="hidden" name="agentId" value="<o:out value="${view.selectedAgent.id}"/>">
                                    </c:when>
                                    <c:otherwise>
                                        <input type="text" name="agentCommission" value="<o:number value="${0}" format="currency"/>" style="width: 150px;" class="right" />
                                    </c:otherwise>
                                </c:choose> <c:choose>
                                    <c:when test="${!empty view.selectedAgent and view.selectedAgent.agentCommissionPercent}">
                                        <input type="checkbox" name="agentCommissionPercent" checked="checked" class="tab" />
                                        <fmt:message key="percent" />
                                    </c:when>
                                    <c:otherwise>
                                        <input type="checkbox" name="agentCommissionPercent" class="tab" />
                                        <fmt:message key="percent" />
                                    </c:otherwise>
                                </c:choose></td>
                        </tr>
                        <tr>
                            <td class="row-submit"><input class="cancel" type="button" onclick="ojsCommon.setVisibility('agentConfig_popup', 'hidden');" value="<fmt:message key="exit"/>" /></td>
                            <td class="row-submit"><o:submit /></td>
                        </tr>
                    </tbody>
                </table>
            </o:form>
            <div class="spacer"></div>
        </div>
    </div>
</div>
