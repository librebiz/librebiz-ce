/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.contacts.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WsBusinessCase

import com.osserp.common.Constants
import com.osserp.common.ds.ImportMapper
import com.osserp.common.util.DateUtil

/**
 * 
 * @author rk <rk@osserp.com>
 *
 */
class BusinessCaseImportMapper extends ImportMapper {

    private static Logger logger = LoggerFactory.getLogger(BusinessCaseImportMapper.class.getName())

    /**
     * Checks if row exists and contains 'osserpkey' OR 'requeststatus' column
     * @param row
     * @return true if row contains required column
     */
    static boolean isMappable(Map<String, String> row) {
        boolean result = (row && (row['osserpkey'] || row[rowRequestStatus]))
        logger.debug("isMappable: done [result=${result}, key=${row['osserpkey']}, requestStatus=${row[rowRequestStatus]}]")
        result
    }

    @Override
    Object map(Map<String, String> row) {
        WsBusinessCase obj = new WsBusinessCase()

        String idString = fetchString(row[rowBusinessCaseId])
        Long businessCaseId = createLong(idString)
        if (businessCaseId) {
            obj.primaryKey = businessCaseId
            obj.externalKey = fetchString(row[rowAccountingId])
        } else {
            idString = fetchString(row[rowAccountingId])
            businessCaseId = createLong(idString)
        }
        if (businessCaseId) {
            obj.primaryKey = businessCaseId
        }

        String targetIdString = fetchString(row[rowTargetId])
        Long targetId = createLong(targetIdString)
        if (targetId) {
            obj.targetId = targetId
        }

        String typeString = fetchString(row[rowRequestType])
        if (typeString) {
            obj.businessType = createLong(typeString)
        } else {
            typeString = fetchString(row[rowRequestTypeName])
            if (typeString) {
                obj.businessTypeName = typeString
            }
        }

        obj.branchKey = fetchString(row[rowBranchKey])

        obj.campaignName = fetchString(row[rowOrigin])
        obj.campaignId = createLong(fetchString(row[rowOriginId]))

        obj.originName = fetchString(row[rowOriginType])
        obj.originId = createLong(fetchString(row[rowOriginTypeId]))

        obj.name = fetchString(row[rowRequestName])
        obj.note = fetchString(row[rowRequestNote])

        obj.street = fetchRequestStreet(row)
        //obj.requestStreetAddon
        obj.city = fetchString(row[rowRequestCity])
        //obj.requestFederalState
        obj.zipcode = fetchString(row[rowRequestZipcode])
        obj.countryId = Constants.GERMANY
        obj.currency = fetchString(row[rowCurrency])
        obj.statusId = createLong(fetchString(row[rowStatus]))
        obj.status = fetchString(row[rowRequestStatus])
        obj.requestCreated = fetchString(row[rowRequestCreated])
        if (obj.requestCreated) {
            obj.requestDate = DateUtil.createDate(obj.requestCreated, false)
        }
        return obj
    }

    String rowBusinessCaseId = 'osserpkey'
    String rowAccountingId = 'thirdpartykey'
    String rowTargetId = 'targetkey'
    String rowBranchKey = 'branch'
    String rowCurrency = 'currency'
    String rowSalesStatus = 'sales'
    String rowStatus = 'statusId'

    static String rowOrigin = 'origin'
    static String rowOriginId = 'originid'
    static String rowOriginType = 'origintype'
    static String rowOriginTypeId = 'origintypeid'

    static String rowRequestCreated = 'requestdate'
    static String rowRequestName = 'requestname'
    static String rowRequestNote = 'requestnote'
    static String rowRequestStatus = 'requeststatus'
    static String rowRequestStreet = 'requeststreet'
    static String rowRequestStreetAddon = 'requeststreetaddon'
    static String rowRequestStreetNumber = 'requeststreetno'
    static String rowRequestZipcode = 'requestzipcode'
    static String rowRequestCity = 'requestcity'
    static String rowRequestCountry = 'requestcountry'
    static String rowRequestType = 'requesttype'
    static String rowRequestTypeName = 'requesttypename'

    String[] knownColumns = [
        rowBusinessCaseId,
        rowAccountingId,
        rowTargetId,
        rowBranchKey,
        rowCurrency,
        rowSalesStatus,
        rowStatus,
        rowOrigin,
        rowOriginId,
        rowOriginType,
        rowOriginTypeId,
        rowRequestCreated,
        rowRequestName,
        rowRequestNote,
        rowRequestStatus,
        rowRequestStreet,
        rowRequestStreetAddon,
        rowRequestStreetNumber,
        rowRequestZipcode,
        rowRequestCity,
        rowRequestCountry,
        rowRequestType,
        rowRequestTypeName
    ]

    private String fetchRequestStreet(Map<String, String> row) {
        String street = fetchString(row[rowRequestStreet])
        String streetNumber = fetchString(row[rowRequestStreetNumber])
        if (street && streetNumber && !(street.indexOf(streetNumber) > -1)) {
            street = "${street.trim()} ${streetNumber.trim()}"
        }
        return street
    }
}
