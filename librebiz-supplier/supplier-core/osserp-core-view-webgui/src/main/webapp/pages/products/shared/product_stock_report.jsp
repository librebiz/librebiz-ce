<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<c:set var="view" value="${requestScope.view}" />

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table">
            <thead>
                <tr>
                    <th id="date"><fmt:message key="date" /></th>
                    <th id="number"><fmt:message key="number" /></th>
                    <th id="contact"><fmt:message key="supplierSlashCustomerLabel" /></th>
                    <th id="quantity" class="right"><fmt:message key="quantity" /></th>
                    <th id="stock" class="right"><fmt:message key="stock" /></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="record" items="${view.stockReport}" varStatus="s">
                    <c:choose>
                        <c:when test="${!record.input}">
                            <tr>
                                <td class="red"><o:date value="${record.created}" /></td>
                                <c:choose>
                                    <c:when test="${record.outputInvoice}">
                                        <td><a href="<c:url value="/salesInvoice.do"><c:param name="method" value="display"/><c:param name="exit" value="product"/><c:param name="readonly" value="true"/><c:param name="id" value="${record.id}"/></c:url>"><o:out value="${record.number}" /></a></td>
                                    </c:when>
                                    <c:when test="${record.outputCancellation}">
                                        <td><a href="<c:url value="/salesCancellation.do"><c:param name="method" value="display"/><c:param name="exit" value="product"/><c:param name="readonly" value="true"/><c:param name="id" value="${record.id}"/></c:url>"><o:out value="${record.number}" /></a></td>
                                    </c:when>
                                    <c:when test="${record.outputCreditNote}">
                                        <td><a href="<c:url value="/salesCreditNote.do"><c:param name="method" value="display"/><c:param name="exit" value="product"/><c:param name="readonly" value="true"/><c:param name="id" value="${record.id}"/></c:url>"><o:out value="${record.number}" /></a></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><a href="<c:url value="/loadSales.do"><c:param name="exit" value="product"/><c:param name="fetchBy" value="order"/><c:param name="id" value="${record.id}"/></c:url>"><o:out value="${record.id}" /></a></td>
                                    </c:otherwise>
                                </c:choose>
                                <td><o:out value="${record.contactName}" /></td>
                                <td class="right red"><o:number value="${record.quantity}" format="decimal" /></td>
                                <td class="right"><o:number value="${record.resultingStock}" format="decimal" /></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr class="row-alt">
                                <td><o:date value="${record.created}" /></td>
                                <td><a href="<c:url value="/purchaseInvoice.do?method=display&exit=product&id=${record.id}"/>"><o:out value="${record.id}" /></a></td>
                                <td><o:out value="${record.contactName}" /></td>
                                <td class="right"><o:number value="${record.quantity}" format="decimal" /></td>
                                <td class="right"><o:number value="${record.resultingStock}" format="decimal" /></td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
