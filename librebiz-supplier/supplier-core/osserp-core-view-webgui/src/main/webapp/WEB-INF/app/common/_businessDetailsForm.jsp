<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${requestScope.detailsView}"/>
<c:set var="url"><v:url value="${view.baseLink}/save"/></c:set>
<div class="form-body">
    <c:if test="${!empty sessionScope.errors}">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <span class="error"><o:out value="${sessionScope.errors}"/></span>
                </div>
            </div>
        </div>
    </c:if>
    <c:choose>
        <c:when test="${view.createMode}">
            <c:if test="${!empty view.existingProperties}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="existingLabel"><fmt:message key="existingProperty" /></label> 
                            <select name="name" class="form-control" size="1" onchange="enableUpdateOnly(); ojsAjax.ajaxForm('${url}', 'businessDetails_popup', true, $('dynamicForm'), null, null, null, null);">
                                <option value="undefined"><fmt:message key="or" /></option>
                                <c:forEach var="opt" items="${view.existingProperties}">
                                    <option value="<o:out value="${opt.name}"/>"<c:if test="${view.selectedName == opt.name}"> selected="selected"</c:if>><o:out value="${opt.value}" /></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!view.selectedNameAvailable}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="newProperty"><fmt:message key="newProperty" /></label>
                            <v:text name="label" styleClass="form-control" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!empty view.selectedName and !empty view.existingValues}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="existingValues"><fmt:message key="existingInputValues" /></label> 
                            <select name="preselectedValue" class="form-control" size="1">
                                <option value="undefined"><fmt:message key="or" /></option>
                                <c:forEach var="opt" items="${view.existingValues}">
                                    <option value="<o:out value="${opt.id}"/>"><o:out value="${opt.value}" /></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </c:if>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="newLabel">
                            <c:choose>
                                <c:when test="${!empty view.selectedName and !empty view.existingValues}"><fmt:message key="newEntry" /></c:when>
                                <c:otherwise><fmt:message key="propertyValue" /></c:otherwise>
                            </c:choose>
                        </label> 
                        <v:text name="value" styleClass="form-control" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <o:submit styleClass="btn btn-default" />
                    </div>
                </div>
            </div>
        </c:when>
        <c:when test="${view.editMode}">
            <c:if test="${!empty view.selectedName and !empty view.existingValues}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="existingValues"><fmt:message key="existingInputValues" /></label> 
                            <select name="preselectedValue" class="form-control" size="1" >
                                <option value="undefined"><fmt:message key="or" /></option>
                                <c:forEach var="opt" items="${view.existingValues}">
                                    <option value="<o:out value="${opt.id}"/>"<c:if test="${view.bean.value == opt.value}"> selected="selected"</c:if>><o:out value="${opt.value}" /></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </c:if>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="newLabel"><fmt:message key="${view.bean.name}" /></label> 
                        <v:text name="value" styleClass="form-control" value="${view.bean.value}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <o:submit styleClass="btn btn-default" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <v:ajaxLink url="${view.baseLink}/removeProperty" title="removeProperty" targetElement="businessDetails_popup">
                            <input type="button" class="btn" onclick="return false;" value="<fmt:message key="delete"/>"/>
                        </v:ajaxLink>
                    </div>
                </div>
            </div>

        </c:when>
        <c:otherwise>
            <c:forEach var="prop" items="${view.list}">
                <div class="row">
                    <div class="col-md-6">
                        <label for="existingLabel">
                            <v:ajaxLink url="${view.baseLink}/select?id=${prop.id}" title="change" targetElement="businessDetails_popup">
                                <fmt:message key="${prop.name}" />
                            </v:ajaxLink>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <span><o:out value="${prop.value}" /></span>
                    </div>
                </div>
            </c:forEach>
            <div class="row">
                <div class="col-md-6">
                    <label for="newLabel">
                        <v:ajaxLink url="${view.baseLink}/enableCreateMode" title="addNewProperty" targetElement="businessDetails_popup">
                            <fmt:message key="new" />
                        </v:ajaxLink>
                    </label>
                </div>
                <div class="col-md-6"><span>&nbsp;</span></div>
            </div>
        </c:otherwise>
    </c:choose>
</div>
<o:removeErrors/>
<!-- form-body -->
