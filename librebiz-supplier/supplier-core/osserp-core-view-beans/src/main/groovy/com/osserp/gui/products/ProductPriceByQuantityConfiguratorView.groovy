/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 14, 2011 4:54:44 AM 
 * 
 */
package com.osserp.gui.products

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.products.Product
import com.osserp.core.products.ProductPriceByQuantityMatrix

import com.osserp.groovy.web.ViewContextException

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ProductPriceByQuantityConfiguratorView extends ProductPriceAwareView {

    ProductPriceByQuantityMatrix matrix

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            Product product = env.productView.bean
            if (!product.isPriceByQuantity()) {
                throw new ViewContextException(ErrorCode.PRODUCT_CONFIG_INVALID)
            }
            matrix = productPriceManager.getMatrix(product)
            if (!matrix) {
                matrix = productPriceManager.createMatrix(domainEmployee, product)
            }
            list = matrix.ranges
        }
    }

    void delete() throws ClientException {
        productPriceManager.removeRange(domainEmployee, matrix)
    }

    @Override
    void save() {
        if (bean) {
            productPriceManager.updateMargin(
                    domainEmployee,
                    matrix,
                    bean,
                    form.getPercentage('consumerMargin'),
                    form.getPercentage('resellerMargin'),
                    form.getPercentage('partnerMargin'))
            bean = null
        } else {
            // add request
            Double range = form.getDouble('range')
            productPriceManager.addRange(domainEmployee, matrix, range)
        }
        // the configurator is always in edit mode,
        // so we don't have to disable this
    }
}
