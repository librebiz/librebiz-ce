/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 28, 2008 
 * 
 */
package com.osserp.core.employees;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.directory.DirectoryGroup;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class EmployeeUtil {

    /**
     * Fetches the default branch office of an employee
     * @param employee
     * @return branch id or null
     */
    public static Long fetchBranch(Employee employee) {
        BranchOffice office = employee.getBranch();
        return (office == null ? null : office.getId());
    }

    /**
     * Gets the default branch office id of an employee
     * @param employee
     * @return branch id
     * @throws ClientException if no branch configured
     */
    public static Long getBranch(Employee employee) throws ClientException {
        BranchOffice office = employee.getBranch();
        if (office == null) {
            throw new ClientException(ErrorCode.BRANCH_MISSING);
        }
        return office.getId();
    }

    /**
     * Indicates that an employee is a member of ldap group
     * @param employee
     * @param ldapGroup
     * @return true if employee is member of ldap group
     */
    public static boolean containsGroup(Employee employee, DirectoryGroup ldapGroup) {
        for (int i = 0, j = employee.getRoleConfigs().size(); i < j; i++) {
            EmployeeRoleConfig roleConfig = employee.getRoleConfigs().get(i);
            for (int k = 0, l = roleConfig.getBranch().getGroups().size(); k < l; k++) {
                EmployeeGroup employeeGroup = roleConfig.getBranch().getGroups().get(k);
                if (employeeGroup.getLdapGroup() != null
                        && employeeGroup.getLdapGroup().equals(ldapGroup.getId())) {
                    return true;
                }
            }
            for (int k = 0, l = roleConfig.getRoles().size(); k < l; k++) {
                EmployeeRole role = roleConfig.getRoles().get(k);
                if (role.getGroup().getLdapGroup() != null
                        && role.getGroup().getLdapGroup().equals(ldapGroup.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean containsGroup(List<DirectoryGroup> groups, EmployeeGroup group) {
        if (group.getLdapGroup() != null) {
            for (int i = 0, j = groups.size(); i < j; i++) {
                DirectoryGroup ldapGroup = groups.get(i);
                if (group.getLdapGroup().equals(ldapGroup.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Creates a filtered employee list by source list and employee branch.
     * @param sourceList with employees to filter
     * @param branch to filter by
     * @return employees by same branch or all if branch was null
     */
    public static List<Employee> createBySameBranch(List<Employee> sourceList, Employee employee) {
        List<Employee> result = new ArrayList<Employee>();
        for (Iterator<Employee> i = sourceList.iterator(); i.hasNext();) {
            Employee next = i.next();
            if (next.getBranch() != null && employee.isFromBranch(next.getBranch().getId())) {
                result.add(next);
            }
        }
        return result;
    }

    /**
     * Creates a filtered employee list by source list and company executive employee
     * @param sourceList with employees to filter
     * @param employee
     * @return employees by companies where given employee is company executive
     */
    public static List<Employee> createByCompanyExecutive(List<Employee> sourceList, Employee employee) {
        List<Employee> result = new ArrayList<Employee>();
        for (Iterator<Employee> i = sourceList.iterator(); i.hasNext();) {
            Employee next = i.next();
            if (next.getBranch() != null && next.getBranch().getCompany() != null && employee.isCompanyExecutive(next.getBranch().getCompany().getId())) {
                result.add(next);
            }
        }
        return result;
    }
}
