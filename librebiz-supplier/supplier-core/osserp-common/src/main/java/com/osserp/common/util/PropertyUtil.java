/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2005 
 * 
 */
package com.osserp.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Property;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PropertyUtil {
    private static Logger log = LoggerFactory.getLogger(PropertyUtil.class.getName());

    public static final Properties load(File file) {
        Properties result = new Properties();
        try {
            result.load(new FileInputStream(file));
        } catch (IOException e) {
            // do nothing we return empty props
            log.warn("load() exception loading properties [file="
                    + (file == null ? "null" : file.getAbsolutePath())
                    + "]", e);
        }
        return result;
    }

    /**
     * Reads a property file and creates a property object. <br>
     * Method never throws any throwable
     * @param fileName
     * @return properties file with values or empty properties if file not exists or any error occured while file reading
     */
    public static Properties getProperties(String fileName) {
        InputStream stream = fetchByFileSystemPath(fileName);
        if (stream == null) {
            stream = fetchByClassPath(fileName);
        }
        Properties props = new Properties();
        if (stream != null) {
            try {
                props.load(stream);
            } catch (IOException e) {
                // do nothing we return empty props
            } finally {
                try {
                    stream.close();
                } catch (IOException e) {
                    log.info("getProperties() ignoring exception while closing input stream after reading", e);
                }
            }
        }
        return props;
    }

    /**
     * Reads a property file and creates a map. Method never throws any throwable
     * @param fileName
     * @return map with values or empty map if file not exists or any error occured while file reading
     */
    public static Map<String, Object> getMap(String fileName) {
        return createMap(getProperties(fileName));
    }
    
    public static boolean fetchBoolean(Map<String, Property> map, String name) {
        Property prop = map.get(name);
        if (prop != null) {
            return prop.isEnabled();
        }
        return false;
    }
    
    public static Long fetchLong(Map<String, Property> map, String name) {
        Property prop = map.get(name);
        if (prop != null) {
            return prop.getAsLong();
        }
        return null;
    }
    
    public static String fetchString(Map<String, Property> map, String name) {
        Property prop = map.get(name);
        if (prop != null) {
            return prop.getAsString();
        }
        return null;
    }
    

    /**
     * Creates a map by property file
     * @param properties
     * @return map
     */
    public static Map<String, Object> createMap(Properties properties) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (properties != null) {
            Set<Object> keys = properties.keySet();
            for (Object key : keys) {
                Object value = properties.get(key);
                result.put(key.toString(), value);
            }
        }
        return result;
    }

    private static InputStream fetchByFileSystemPath(String fileName) {
        if (FileUtil.exists(fileName)) {
            try {
                return FileUtil.getStream(fileName);
            } catch (Throwable ignorable) {
                // caller should handle this
            }
        }
        return null;
    }

    private static InputStream fetchByClassPath(String fileName) {
        InputStream result = null;
        if (fileName != null) {
            try {
                result = PropertyUtil.class.getResourceAsStream(fileName);
            } catch (Throwable ignorable) {
                // we ignore this
            }
            // try with absolute path if given fileName is relative
            if (result == null && !fileName.startsWith("/")) {
                try {
                    result = PropertyUtil.class.getResourceAsStream("/" + fileName);
                } catch (Throwable ignorable) {
                    // caller should handle this
                }
            }
        }
        return result;
    }
    
    /**
     * Sorts a list of properties.
     * @param list the list to sort
     * @param name indicates sorting by name
     * @param reverse indicates reverse sorting
     * @return sorted list
     */
    public static List<Property> sort(List<Property> list, boolean name, boolean reverse) {
        if (name) {
            return CollectionUtil.sort(list, createPropertyNameComparator(reverse));
        }
        return CollectionUtil.sort(list, createPropertyValueComparator(reverse));
    }

    private static Comparator createPropertyNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Property) a).getName().compareTo(((Property) b).getName());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Property) a).getName().compareToIgnoreCase(((Property) b).getName());
            }
        };
    }

    private static Comparator createPropertyValueComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Property) a).getAsString().compareTo(((Property) b).getAsString());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Property) a).getAsString().compareToIgnoreCase(((Property) b).getAsString());
            }
        };
    }

}
