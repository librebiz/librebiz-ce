/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2006 4:22:56 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.Details;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.PermissionException;
import com.osserp.common.Property;
import com.osserp.common.User;
import com.osserp.common.dao.AbstractDao;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.StringUtil;

import com.osserp.core.Item;
import com.osserp.core.dao.ProductCategories;
import com.osserp.core.dao.ProductGroups;
import com.osserp.core.dao.ProductNumberRanges;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.dao.ProductTypes;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Stock;
import com.osserp.core.model.products.ProductClassificationConfigVO;
import com.osserp.core.model.products.ProductImpl;
import com.osserp.core.model.products.ProductManufacturerImpl;
import com.osserp.core.model.products.ProductPropertyImpl;
import com.osserp.core.products.Manufacturer;
import com.osserp.core.products.Package;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductNumberRange;
import com.osserp.core.products.ProductPlanningMode;
import com.osserp.core.products.ProductType;
import com.osserp.core.products.ProductTypeConfig;
import com.osserp.core.products.SerialDisplay;
import com.osserp.core.users.DomainUser;
import com.osserp.core.views.StockReportDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductsDao extends AbstractProductEntitiesDao implements Products {
    private static Logger log = LoggerFactory.getLogger(ProductsDao.class.getName());
    
    public final static Long DEFAULT_PRODUCT_TYPE = 101L;
    public final static Long DEFAULT_PRODUCT_GROUP = 202L;
    
    private String stockReport = null;
    private String serialDisplayView = null;
    private String[] permissionsPurchasePriceIgnoreFlag = null;
    private SystemCompanies companies = null;
    private ProductTypes productTypes = null;
    private ProductGroups productGroups = null;
    private ProductCategories productCategories = null;
    private ProductNumberRanges productNumberRanges = null;

    public ProductsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache,
            String purchasePriceIgnoreFlagPermssions,
            String serialDisplayView,
            ProductTypes productTypes,
            ProductGroups productGroups,
            ProductCategories productCategories,
            ProductNumberRanges productNumberRanges,
            ProductSummaryCache summaryCache,
            SystemCompanies companies) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache, summaryCache);
        this.stockReport = getTable(TableKeys.STOCK_REPORT);
        this.serialDisplayView = getTable(serialDisplayView);
        this.permissionsPurchasePriceIgnoreFlag = StringUtil.getTokenArray(purchasePriceIgnoreFlagPermssions, ",");
        this.companies = companies;
        this.productTypes = productTypes;
        this.productGroups = productGroups;
        this.productCategories =  productCategories;
        this.productNumberRanges = productNumberRanges;
    }
    
    public List<Long> getPublicIds() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT product_id FROM ").append(productTable).append(" WHERE is_public ORDER By name");
        return (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
    }

    public Long createIdSuggestion(ProductClassificationConfig config) {
        if (config != null) {
            ProductNumberRange range = null;
            if (config.getCategory() != null && config.getCategory().getNumberRangeId() != null
                    && config.getCategory().getNumberRangeId() != 0) {
                if (log.isDebugEnabled()) {
                    log.debug("createIdSuggestion() found range definition by category");
                }
                range = productNumberRanges.getNumberRange(config.getCategory().getNumberRangeId());
            } else if (log.isDebugEnabled()) {
                log.debug("createIdSuggestion() no category found...");
            }
            if (range == null && config.getGroup() != null && config.getGroup().getNumberRangeId() != null
                    && config.getGroup().getNumberRangeId() != 0) {
                if (log.isDebugEnabled()) {
                    log.debug("createIdSuggestion() found range definition by group");
                }
                range = productNumberRanges.getNumberRange(config.getGroup().getNumberRangeId());
            }
            if (range != null && isSet(range.getRangeStart()) && isSet(range.getRangeEnd())) {
                return getNextId(range);
                
            } else if (log.isDebugEnabled()) {
                if (range != null) {
                    log.debug("createIdSuggestion() invalid range found [range=" + range.getId()
                        + ", type=" + (config.getType() == null ? "null" : config.getType().getId())
                        + ", group=" + (config.getGroup() == null ? "null" : config.getGroup().getId()) 
                        + ", category=" + (config.getCategory() == null ? "null" : config.getCategory().getId()) + "]");
                    
                } else {
                    log.debug("createIdSuggestion() no range found by classification [type="
                        + (config.getType() == null ? "null" : config.getType().getId())
                        + ", group=" + (config.getGroup() == null ? "null" : config.getGroup().getId()) 
                        + ", category=" + (config.getCategory() == null ? "null" : config.getCategory().getId()) + "]");
                }
            }
        }
        return (getMaxId() + 1L);
    }

    public Product create(
            Employee user,
            Long productId,
            ProductClassificationConfig config,
            String matchcode,
            String name,
            String description,
            Long quantityUnit,
            Integer packagingUnit,
            Long manufacturer,
            ProductPlanningMode planningMode,
            Product other) throws ClientException {

        Product product = fetchExisting(productId, name);
        if (product == null) {
            checkNames(name, matchcode);
            Stock defaultStock = companies.getDefaultStock();

            product = new ProductImpl(
                    user,
                    productId,
                    config,
                    matchcode,
                    name,
                    description,
                    quantityUnit,
                    packagingUnit == null ? 1 : packagingUnit,
                    manufacturer,
                    (defaultStock == null ? null : defaultStock.getId()),
                    planningMode,
                    other);
            getCurrentSession().save(ProductImpl.class.getName(), product);
            if (log.isDebugEnabled()) {
                log.debug("create() done [productId=" + product.getProductId() +
                        ", created=" + product.getCreated() +
                        ", createdBy=" + product.getCreatedBy() +
                        ", manufacturer=" + product.getManufacturer() +
                        ", matchcode=" + product.getMatchcode() +
                        ", name=" + product.getName() +
                        "]");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("create() nothing to do, product exists [productId=" + productId + ", name=" + name + "]");
        }
        return product;
    }

    public void changePurchasePriceLimitFlag(DomainUser domainUser, Product product) throws PermissionException {
        product.changeIgnorePurchasePriceFlag(domainUser, permissionsPurchasePriceIgnoreFlag);
        getCurrentSession().saveOrUpdate(ProductImpl.class.getName(), product);
    }

    public Product removeType(Product product, ProductType type) {
        if (type == null || !product.isType(type)) {
            if (log.isDebugEnabled()) {
                log.debug("removeType() nothing to do, type null or not assigned");
            }
            return product;
        }
        ProductImpl obj = (ProductImpl) getCurrentSession().load(ProductImpl.class.getName(), product.getProductId());
        obj.removeType(type);
        save(obj);
        if (log.isDebugEnabled()) {
            log.debug("removeType() done [product=" + product.getProductId()
                    + ", type=" + type.getId() + "]");
        }
        return load(obj.getProductId());
    }

    public Product updateClassification(Product product, ProductType type, ProductGroup group, ProductCategory category)
            throws ClientException {

        if (group == null) {
            throw new ClientException(ErrorCode.GROUP_SELECTION);
        }
        if (type == null) {
            throw new ClientException(ErrorCode.PRODUCT_TYPE_MISSING);
        }
        ProductTypeConfig typeConfig = productTypes.load(type.getId());
        ProductGroupConfig groupConfig = productGroups.load(group.getId());
        ProductCategoryConfig categoryConfig = category != null ? productCategories.load(category.getId()) : null;
        return updateClassificationObjects(product, typeConfig, groupConfig, categoryConfig);
    }

    private Product updateClassificationObjects(
            Product product,
            ProductTypeConfig type,
            ProductGroupConfig group,
            ProductCategoryConfig category)
            throws ClientException {

        ProductImpl obj = (ProductImpl) getCurrentSession().load(ProductImpl.class, product.getProductId());
        if (obj.getTypes().isEmpty() || obj.isType(type)) {

            obj.updateClassification(type, group, category);
            if (log.isDebugEnabled()) {
                log.debug("updateClassification() context: type was not set or type was already supported");
            }
        } else if (obj.getGroup().getId().equals(group.getId())
                && ((obj.getCategory() == null && category == null)
                || (obj.getCategory() != null && category != null
                && obj.getCategory().getId().equals(category.getId())))) {
            obj.updateClassification(type, group, category);
            if (log.isDebugEnabled()) {
                log.debug("updateClassification() context: compatible group and category setup found");
            }
        } else {
            obj.getTypes().clear();
            obj.updateClassification(type, group, category);
            if (log.isDebugEnabled()) {
                log.debug("updateClassification() context: type reset and overriding previous");
            }
        }
        save(obj);
        return load(obj.getProductId());
    }

    public List<Product> getStockAware(boolean includeKanban) {
        StringBuilder hql = new StringBuilder(
                "from ProductImpl a where a.endOfLife = false " +
                        "and a.virtual = false " +
                        "and a.affectsStock = true");
        if (!includeKanban) {
            hql.append(" and a.kanban = false");
        }
        hql.append(" order by a.group, a.name");
        List<Product> result = getCurrentSession().createQuery(hql.toString()).list();
        loadSummary(result);
        return result;
    }

    public Item getPackage(Map<Long, Package> packages, List<Item> items) {
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (packages.containsKey(next.getProduct().getProductId())) {
                return next;
            }
        }
        return null;
    }

    public List<Product> getStockAware(ProductCategory category, boolean includeKanban) {
        if (log.isDebugEnabled()) {
            log.debug("getStockAware() invoked [category=" + (category == null ? "null]" : category.getId() + "]"));
        }
        if (category == null) {
            return getStockAware(includeKanban);
        }
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(ProductImpl.class.getName())
                .append(" a where a.category.id = :categoryId ")
                .append("and a.endOfLife = false ")
                .append("and a.virtual = false ")
                .append("and a.affectsStock = true");
        if (!includeKanban) {
            hql.append(" and a.kanban = false");
        }
        hql.append(" order by a.group, a.name");
        List<Product> result = getCurrentSession().createQuery(
                hql.toString()).setParameter("categoryId", category.getId()).list();
        loadSummary(result);
        return result;
    }

    public List<Product> getStockAware(ProductGroup group, boolean includeKanban) {
        if (log.isDebugEnabled()) {
            log.debug("getStockAware() invoked [group=" + (group == null ? "null]" : group.getId() + "]"));
        }
        if (group == null) {
            return getStockAware(includeKanban);
        }
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(ProductImpl.class.getName())
                .append(" a where a.group.id = :groupId ")
                .append("and a.endOfLife = false ")
                .append("and a.virtual = false ")
                .append("and a.affectsStock = true");
        if (!includeKanban) {
            hql.append(" and a.kanban = false");
        }
        hql.append(" order by a.group, a.name");
        List<Product> result = getCurrentSession().createQuery(
                hql.toString()).setParameter("groupId", group.getId()).list();
        loadSummary(result);
        return result;
    }

    public List<Product> getStockAware(ProductType type, boolean includeKanban) {
        if (type == null) {
            return getStockAware(includeKanban);
        }
        return load(findByType(type.getId(), includeKanban));
    }

    public List<StockReportDisplay> getStockReport(Long stockId, Long productId) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT * FROM ")
                .append(stockReport)
                .append(" WHERE stock_id = ? AND productid = ?");
        final Object[] params = { stockId, productId };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        List<StockReportDisplay> result = (List<StockReportDisplay>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                StockReportDisplayRowMapper.getReader());
        double previousStock = 0;
        for (int i = result.size() - 1; i >= 0; i--) {
            StockReportDisplay next = result.get(i);
            next.calculateResultingStock(previousStock);
            previousStock = next.getResultingStock();
        }
        if (log.isDebugEnabled()) {
            log.debug("getStockReport() done [resultingStock=" + previousStock + "]");
        }
        return result;
    }

    public List<SerialDisplay> getSerialDisplay() {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT * FROM ")
                .append(serialDisplayView);
        return (List<SerialDisplay>) jdbcTemplate.query(
                sql.toString(),
                SerialDisplayRowMapper.getReader());
    }

    public List<SerialDisplay> getSerialDisplay(Long productId) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT * FROM ")
                .append(serialDisplayView)
                .append(" WHERE product_id = ?");
        final Object[] params = { productId };
        final int[] types = { Types.BIGINT };
        return (List<SerialDisplay>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                SerialDisplayRowMapper.getReader());
    }

    public List<Product> findByGroup(Long groupId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT product_id FROM ")
                .append(productTable)
                .append(" WHERE end_of_life = false AND group_id = ? ORDER BY name");

        final Object[] params = { groupId };
        final int[] types = { Types.BIGINT };

        return load((List<Long>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper()));
    }

    public List<Product> findByMatchcode(String matchcode) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT product_id FROM ")
                .append(productTable)
                .append(" WHERE end_of_life = false AND matchcode = ? ORDER BY name");

        final Object[] params = { matchcode };
        final int[] types = { Types.VARCHAR };

        return load((List<Long>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper()));
    }

    public List<Product> findTrackable() {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT product_id FROM ")
                .append(productTable)
                .append(" WHERE end_of_life = false AND is_time = true ORDER BY name");

        return load((List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper()));
    }

    public String getName(Long productId) {
        StringBuilder sql = new StringBuilder(64);
        sql.append("SELECT name FROM ").append(productTable).append(" WHERE product_id = ?");

        final Object[] params = { productId };
        final int[] types = { Types.BIGINT };

        return jdbcTemplate.queryForObject(
                sql.toString(), params, types, String.class);
    }

    public void checkCalculationValues(Product product) throws ClientException {
        // default implementation does nothing
    }

    public Manufacturer findManufacturer(Product product) {
        Manufacturer m = null;
        if (product.getManufacturer() != null) {
            List<Manufacturer> list = getCurrentSession().createQuery(
                    "from " + ProductManufacturerImpl.class.getName()
                            + " o where o.id = " + product.getManufacturer().toString()
                            + " order by o.name").list();
            m = list.isEmpty() ? null : list.get(0);
        }
        return m;
    }

    public List<Manufacturer> getManufacturersByGroup(Long groupId) {
        return getCurrentSession().createQuery(
                "from " + ProductManufacturerImpl.class.getName() 
                + " o where o.groupId = :groupId order by o.name")
                .setParameter("groupId", groupId).list();
    }

    public List<Manufacturer> getManufacturers() {
        return getCurrentSession().createQuery("from " + ProductManufacturerImpl.class.getName()
                + " o order by o.name").list();
    }

    public Manufacturer addManufacturer(Long groupId, String name) throws ClientException {
        if (isEmpty(name)) {
            return null;
        }
        List<Manufacturer> existing = new ArrayList<>();
        if (isEmpty(groupId)) {
            existing = getCurrentSession().createQuery(
                    "from " + ProductManufacturerImpl.class.getName()
                    + " o where o.name = :name").setParameter("name", name).list();
        } else {
            existing = getCurrentSession().createQuery(
                "from " + ProductManufacturerImpl.class.getName()
                + " o where o.groupId = :groupId and o.name = :name")
                    .setParameter("groupId", groupId)
                    .setParameter("name", name)
                    .list();
        }
        if (!existing.isEmpty()) {
            throw new ClientException(ErrorCode.NAME_EXISTS);
        }
        ProductManufacturerImpl m = new ProductManufacturerImpl(groupId, name);
        getCurrentSession().saveOrUpdate(ProductManufacturerImpl.class.getName(), m);
        return m;
    }

    public void updateManufacturer(Long id, String name) throws ClientException {
        Manufacturer toChange = (Manufacturer) getCurrentSession().load(ProductManufacturerImpl.class, id);
        List<Manufacturer> existing = getCurrentSession().createQuery(
                "from " + ProductManufacturerImpl.class.getName()
                        + " o where o.name = :name").setParameter("name", name).list();
        if (!existing.isEmpty()) {
            for (int i = 0, j = existing.size(); i < j; i++) {
                Manufacturer next = existing.get(i);
                if (next.getName().equals(name)
                        && next.getGroupId().equals(toChange.getGroupId())
                        && !next.getId().equals(id)) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }
        toChange.setName(name);
        getCurrentSession().saveOrUpdate(ProductManufacturerImpl.class.getName(), toChange);
    }

    public Product addProperty(User user, Details details, String name, String value) {
        Product product = (Product) details;
        ProductPropertyImpl property = new ProductPropertyImpl(
                (user == null ? null : user.getId()), product.getProductId(), name, value);
        getCurrentSession().saveOrUpdate(ProductPropertyImpl.class.getName(), property);
        return load(product.getProductId());
    }

    public Product updateProperty(User user, Details details, Property property, String value) {
        Product product = (Product) details;
        if (property != null) {
            property.setValue(value);
            property.updateChanged(user == null ? null : user.getId());
            getCurrentSession().saveOrUpdate(ProductPropertyImpl.class.getName(), property);
        }
        return load(product.getProductId());
    }

    public Details removeProperty(User user, Details details, Property property) {
        Product product = (Product) details;
        product.removeProperty(user == null ? null : user.getId(), property);
        save(product);
        return product;
    }
    
    public ProductClassificationConfig fetchOrCreate(
            Long typeId, 
            String typeName, 
            Long groupId, 
            String groupName, 
            Long categoryId, 
            String categoryName, 
            Long rangeStart, 
            Long rangeEnd) {
        
        ProductType type = null;
        ProductGroup group = null;
        ProductCategory category = null;
        
        if (log.isDebugEnabled()) {
            log.debug("fetchOrCreate() invoked [type=" + typeName
                    + ", group=" + groupName
                    + ", category=" + categoryName
                    + ", rangeStart=" + rangeStart
                    + ", rangeEnd=" + rangeEnd + "]");
        }
        
        if (isSet(typeId)) {
            type = productTypes.load(typeId);
        } else if (isSet(typeName)) {
            type = (ProductType) productTypes.findByName(typeName);
            if (type == null) {
                try {
                    type = (ProductType) productTypes.create(null, typeName, null);
                } catch (Exception e) {
                    log.warn("fetchOrCreate() failed on attempt to create type [name=" + typeName + "]", e);
                }
            }
        }
        if (type == null) {
            type = productTypes.load(DEFAULT_PRODUCT_TYPE);
        }
        
        if (isSet(groupId)) {
            group = productGroups.load(groupId);
        } else if (isSet(groupName)) {
            group = (ProductGroup) productGroups.findByName(groupName);
            if (group == null) {
                try {
                    group = (ProductGroup) productGroups.create(null, groupName, null);
                    if (isSet(rangeStart) && isSet(rangeEnd)) {
                        ProductNumberRange range = productNumberRanges.fetchNumberRange(rangeStart, rangeEnd);
                        if (range == null) {
                            try {
                                range = productNumberRanges.createNumberRange(null, group.getName(), rangeStart, rangeEnd);
                            } catch (Exception e) {
                                log.warn("fetchOrCreate() failed on attempt to create number range [start=" + rangeStart 
                                        + ", rangeEnd=" + rangeEnd + "]", e);
                            }
                        }
                        if (range != null) {
                            group.setNumberRangeId(range.getId());
                            productGroups.save(group);
                        }
                    }
                } catch (Exception e) {
                    log.warn("fetchOrCreate() failed on attempt to create group [name=" + groupName + "]", e);
                }
            }
        }
        if (group == null) {
            group = productGroups.load(DEFAULT_PRODUCT_GROUP);
        }
        
        if (isSet(categoryId)) {
            category = productCategories.load(categoryId);
        } else if (isSet(categoryName)) {
            category = (ProductCategory) productCategories.findByName(categoryName);
            if (category == null) {
                try {
                    category = (ProductCategory) productCategories.create(null, categoryName, null);
                } catch (Exception e) {
                    log.warn("fetchOrCreate() failed on attempt to create category [name=" + categoryName + "]", e);
                }
            }
        }
        if (type != null && type.getId() != null && group != null && group.getId() != null) {
            ProductTypeConfig typeConfig = productTypes.load(type.getId());
            ProductGroupConfig groupConfig = productGroups.load(group.getId());
            typeConfig.addGroup(groupConfig);
            productTypes.save(typeConfig);
            if (category != null && category.getId() != null) {
                ProductCategoryConfig categoryConfig = productCategories.load(category.getId());
                groupConfig.addCategory(categoryConfig);
                productGroups.save(groupConfig);
            }
        }
        return new ProductClassificationConfigVO(type, group, category);
    }

    @Override
    protected String getColumn(String columnKey) {
        if (columnKey == null) {
            throw new BackendException("error.configuration");
        }
        if (columnKey.equals(Constants.SEARCH_BY_ID)) {
            return "product_id";
        } else if (columnKey.equals(Constants.SEARCH_BY_MATCHCODE)) {
            return "matchcode";
        } else if (columnKey.equals(Constants.SEARCH_BY_NAME)) {
            return "name";
        } else if (columnKey.equals(Constants.SEARCH_BY_DESCRIPTION)) {
            return "description";
        } else {
            throw new BackendException("error.configuration");
        }
    }

    private Long getMaxId() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT max(product_id) FROM ").append(productTable);
        return jdbcTemplate.queryForObject(sql.toString(), Long.class);
    }

    private Long getNextId(ProductNumberRange range) {
        if (log.isDebugEnabled()) {
            log.debug("getNextId() invoked [rangeStart=" + range.getRangeStart()
                    + ", rangeEnd=" + range.getRangeEnd() + "]");
        }
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT product_id FROM ")
                .append(productTable)
                .append(" WHERE product_id >= ? AND product_id <= ?");
        final Object[] params = { range.getRangeStart(), range.getRangeEnd() };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        List<Long> list = (List<Long>) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
        Set<Long> existing = new HashSet<Long>(list);
        for (long i = range.getRangeStart(), j = range.getRangeEnd(); i < j; i++) {
            Long next = i;
            if (!existing.contains(next)) {
                if (log.isDebugEnabled()) {
                    log.debug("getNextId() done [id=" + next + "]");
                }
                return next;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getNextId() done, no suggestion found");
        }
        return null;
    }

    public Long getExistingName(String name) {
        if (name == null) {
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT product_id FROM ")
                .append(productTable)
                .append(" WHERE name = ?");
        final Object[] params = { name };
        final int[] types = { Types.VARCHAR };
        List<Long> result = (List<Long>) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
        return result.isEmpty() ? null : result.get(0);
    }

    private Product fetchExisting(Long productId, String name) {
        Product p = find(productId);
        if (p != null) {
            return p;
        }
        return findExisting(name);
    }

    protected List<Long> findByType(Long typeId, boolean includeKanban) {
        List<Long> result = new ArrayList<>();
        if (typeId == null || typeId == 0) {
            return result;
        }
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT product_id FROM ")
                .append(productTable)
                .append(" WHERE NOT end_of_life AND NOT is_virtual AND has_stock");
        if (!includeKanban) {
            sql.append(" AND NOT kanban");
        }
        sql
            .append(" AND product_id IN (SELECT product_id FROM ")
            .append(productTypeRelationsTable)
            .append(" WHERE type_id = ")
            .append(typeId)
            .append(")")
            .append(" ORDER BY name");
        return (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
    }

    public Product findExisting(String name) {
        assert name != null;
        Long productId = getExistingName(name);
        return productId != null ? load(productId) : null;
    }

    public void checkUpdate(Long productId, String name, String matchcode) throws ClientException {
        checkNames(name, matchcode);
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT product_id,name,matchcode FROM ")
                .append(productTable)
                .append(" WHERE name = ? AND product_id <> ?");
        final Object[] params = { name, productId };
        final int[] types = { Types.VARCHAR, Types.BIGINT };
        List<Product> list = (List<Product>) jdbcTemplate.query(sql.toString(), params, types, getCheckReader());
        if (list.size() > 0) {
            if (log.isDebugEnabled()) {
                log.debug("checkUpdate() name already exists: " + name);
            }
            throw new ClientException(ErrorCode.NAME_EXISTS);
        }
    }

    protected void checkNames(String name, String matchcode) throws ClientException {
        if (isEmpty(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (name.length() > 250) {
            throw new ClientException(ErrorCode.NAME_MAXLENGTH);
        }
        if (matchcode != null && matchcode.length() > 120) {
            throw new ClientException(ErrorCode.MATCHCODE_MAXLENGTH);
        }
    }

    private RowMapperResultSetExtractor getCheckReader() {
        return new RowMapperResultSetExtractor(new CheckRowMapper());
    }

    private class CheckRowMapper extends AbstractDao implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new ProductImpl(
                    rs.getLong(1),
                    rs.getString(3),
                    rs.getString(2));
        }
    }
}
