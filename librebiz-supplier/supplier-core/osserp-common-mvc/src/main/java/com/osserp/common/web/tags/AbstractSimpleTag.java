/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31 Mar 2007 12:20:38 
 * 
 */
package com.osserp.common.web.tags;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.osserp.common.User;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.SessionUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractSimpleTag extends SimpleTagSupport {

    private String id = null;
    private String style = null;
    private String styleClass = null;
    private String rawTitle = null;
    private String title = null;
    private String onblur = null;
    private String onchange = null;
    private String onclick = null;
    private String maxlength = null;
    private String tabindex = null;
    private String placeholder = null;
    private String rawPlaceholder = null;

    /**
     * Fetches object from session if exists
     * @param key
     * @return object or null if not exists
     */
    protected final Object fetchRequestObject(String key) {
        PageContext ctx = (PageContext) getJspContext();
        return ((HttpServletRequest) ctx.getRequest()).getAttribute(key);
    }

    /**
     * Fetches object from session if exists
     * @param key
     * @return object or null if not exists
     */
    protected final Object fetchSessionObject(String key) {
        PageContext ctx = (PageContext) getJspContext();
        return ((HttpServletRequest) ctx.getRequest()).getSession(true).getAttribute(key);
    }

    /**
     * Provides the servlet context
     * @return servlet context
     */
    protected final ServletContext getServletContext() {
        PageContext ctx = (PageContext) getJspContext();
        return ctx.getServletContext();
    }

    /**
     * Provides current request
     * @return request
     */
    protected final HttpServletRequest getRequest() {
        PageContext ctx = (PageContext) getJspContext();
        return (HttpServletRequest) ctx.getRequest();
    }

    /**
     * Provides current session without initialization. Throws exception if session is invalidated.
     * @return session
     */
    protected final HttpSession getSession() {
        return getRequest().getSession();
    }

    /**
     * Provides the resource string as provided by portal views resource bundle. Locale depends on current user or application default if no user logged in
     * during view activity.
     * @param key
     * @return resource or key depending on whether a string could be found or not.
     */
    protected final String getResourceString(String key) {
        return RequestUtil.getResourceString(getRequest(), key);
    }

    /**
     * Provides the request contextPath
     * @return contextPath
     */
    protected final String getContextPath() {
        PageContext ctx = (PageContext) getJspContext();
        return ((HttpServletRequest) ctx.getRequest()).getContextPath();
    }

    /**
     * Provides the page context
     * @return page context
     */
    protected final PageContext getPageContext() {
        return (PageContext) getJspContext();
    }

    /**
     * Provides an image url by imageDeployer's logical name
     * @param name as provided by imageDeployer.
     * @return imageUrl
     */
    protected final String createImageUrl(String name) {
        ServletContext ctx = getServletContext();
        StringBuilder buffer = new StringBuilder();
        buffer.append(getContextPath()).append(ctx.getAttribute(name));
        return buffer.toString();
    }

    /**
     * Creates a link by url (adds context path if required)
     * @param url
     * @return url with added context path
     */
    protected String createUrl(String url) {
        if (url != null) {
            String ctxPath = getContextPath();
            if (url.startsWith("http") || url.startsWith(ctxPath)) {
                return url;
            }
            return ctxPath + url;
        }
        return null;
    }
    
    /**
     * Provides current user
     * @return user or null if not logged in
     */
    protected final User getUser() {
        try {
            return SessionUtil.getUser(getSession());
        } catch (Exception e) {
            return null;
        }
    }
    
    // attribute getters, setters and helpers  
    
    
    public final String getId() {
        return id;
    }

    public final void setId(String id) {
        this.id = id;
    }

    public final String getStyle() {
        return style;
    }

    public final void setStyle(String style) {
        this.style = style;
    }

    public final String getStyleClass() {
        return styleClass;
    }

    public final void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getRawTitle() {
        return rawTitle;
    }

    public void setRawTitle(String rawTitle) {
        this.rawTitle = rawTitle;
    }

    public final String getTitle() {
        return title;
    }

    public final void setTitle(String title) {
        this.title = title;
    }

    public final String getOnblur() {
        return onblur;
    }

    public final void setOnblur(String onblur) {
        this.onblur = onblur;
    }

    public final String getOnchange() {
        return onchange;
    }

    public final void setOnchange(String onchange) {
        this.onchange = onchange;
    }

    public final String getOnclick() {
        return onclick;
    }

    public final void setOnclick(String onclick) {
        this.onclick = onclick;
    }

    public final String getMaxlength() {
        return maxlength;
    }

    public final void setMaxlength(String maxlength) {
        this.maxlength = maxlength;
    }

    public final String getPlaceholder() {
        return placeholder;
    }

    public final void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public final String getRawPlaceholder() {
        return rawPlaceholder;
    }

    public final void setRawPlaceholder(String rawPlaceholder) {
        this.rawPlaceholder = rawPlaceholder;
    }

    public final String getTabindex() {
        return tabindex;
    }

    public final void setTabindex(String tabindex) {
        this.tabindex = tabindex;
    }

    public StringBuilder addId(StringBuilder buffer) {
        return appendAttribute(buffer, "id", id);
    }

    public StringBuilder addStyle(StringBuilder buffer) {
        return appendAttribute(buffer, "style", style);
    }

    public StringBuilder addStyleClass(StringBuilder buffer) {
        return appendAttribute(buffer, "class", styleClass);
    }

    public StringBuilder addTitle(StringBuilder buffer) {
        if (rawTitle != null && rawTitle.length() > 0) {
            return appendAttribute(buffer, "title", rawTitle);
        } else if (title != null) {
            return appendAttribute(buffer, "title", getResourceString(title));
        }
        return buffer;
    }

    public StringBuilder addPlaceholder(StringBuilder buffer) {
        if (rawPlaceholder != null && rawPlaceholder.length() > 0) {
            return appendAttribute(buffer, "placeholder", rawPlaceholder);
        } else if (placeholder != null && placeholder.length() > 0) {
            return appendAttribute(buffer, "placeholder", getResourceString(placeholder));
        }
        return buffer;
    }

    public StringBuilder addOnblur(StringBuilder buffer) {
        return appendAttribute(buffer, "onblur", onblur);
    }

    public StringBuilder addOnchange(StringBuilder buffer) {
        return appendAttribute(buffer, "onchange", onchange);
    }

    public StringBuilder addOnclick(StringBuilder buffer) {
        return appendAttribute(buffer, "onclick", onclick);
    }

    public StringBuilder addMaxlength(StringBuilder buffer) {
        return appendAttribute(buffer, "maxlength", maxlength);
    }

    public StringBuilder addTabindex(StringBuilder buffer) {
        return appendAttribute(buffer, "tabindex", tabindex);
    }

    protected StringBuilder appendAttribute(StringBuilder buffer, String name, String value) {
        if (value != null) {
            buffer.append(" ").append(name).append("=\"").append(value).append("\" ");
        }
        return buffer;
    }

    protected String renderId() {
        return addId(new StringBuilder()).toString();
    }

    protected String renderOnBlur() {
        return addOnblur(new StringBuilder()).toString();
    }

    protected String renderOnChange() {
        return addOnchange(new StringBuilder()).toString();
    }

    protected String renderStyle() {
        return addStyle(new StringBuilder()).toString();
    }
    
    protected String renderStyleClass() {
        return addStyleClass(new StringBuilder()).toString();
    }

    protected String renderMaxlength() {
        return addMaxlength(new StringBuilder()).toString();
    }

    protected String renderTabindex() {
        return addTabindex(new StringBuilder()).toString();
    }

    protected String renderTitle() {
        return addTitle(new StringBuilder()).toString();
    }

    protected String renderPlaceholder() {
        return addPlaceholder(new StringBuilder()).toString();
    }

    protected String encodeForAttribute(String untrusted) {
        return TagUtil.encodeForAttribute(untrusted);
    }

    protected String encodeForElement(String untrusted) {
        return TagUtil.encodeForAttribute(untrusted);
    }
}
