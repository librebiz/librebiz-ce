/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Nov 21, 2005 8:20:40 AM 
 * 
 */
package com.osserp.gui.client.calc.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.service.Locator;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationManager;
import com.osserp.core.calc.CalculationTemplate;
import com.osserp.core.calc.CalculationTemplateManager;
import com.osserp.core.calc.Calculator;
import com.osserp.core.calc.CalculatorService;
import com.osserp.core.calc.PartlistManager;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductManager;
import com.osserp.core.sales.SalesRevenue;
import com.osserp.core.sales.SalesRevenueManager;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
public abstract class AbstractCalculator implements Calculator {
    private static Logger log = LoggerFactory.getLogger(AbstractCalculator.class.getName());

    private DomainUser employee = null;
    private BusinessCase businessCase = null;
    private Calculation calculation = null;
    private SalesRevenue revenue = null;

    private boolean changed = false;
    private boolean optionMode = false;
    private boolean includePrice = false;
    private boolean replace = false;
    private Long replaceable = null;
    private Item selectedItem = null;

    private boolean includeLazyProducts = true;
    private String[] discountPermissions = null;
    private String[] overridePartnerPricePermissions = null;

    private transient Locator locator;

    /**
     * Default constructor
     */
    protected AbstractCalculator() {
        super();
    }

    /**
     * Common constructor
     * @param employee
     * @param calculation
     * @param businessCase
     * @param discountPermissions
     */
    protected AbstractCalculator(Locator locator, DomainUser employee, Calculation calculation, BusinessCase businessCase, String[] discountPermissions,
            String[] overridePartnerPricePermissions) {
        this.locator = locator;
        this.employee = employee;
        this.discountPermissions = discountPermissions;
        this.overridePartnerPricePermissions = overridePartnerPricePermissions;
        this.businessCase = businessCase;
        if (businessCase != null) {
            includePrice = businessCase.getType().isIncludePriceByDefault();
            if (!businessCase.isSalesContext()) {
                calculation.refreshPartnerPrices();
            }
        }
        updateCalculation(calculation);
    }

    public void updateCalculation(Calculation calc) {
        loadRevenue(calc);
        this.calculation = calc;
    }

    public BusinessCase getBusinessCase() {
        return businessCase;
    }

    public SalesRevenue getRevenue() {
        return revenue;
    }

    public Calculation getCalculation() {
        return calculation;
    }

    public boolean isChanged() {
        return changed;
    }

    public List<Long> addProducts(
            ItemPosition selectedPosition,
            Item item,
            Long[] productIds,
            String[] customNames,
            String[] quantities,
            String[] prices,
            String[] notes)
            throws ClientException {

        CalculationManager manager = getCalculationManager();
        if (calculation != null && calculation.getAllItems().isEmpty()) {
            manager.updateCreatedBy(employee, calculation);
        }
        List<Long> added = new ArrayList<>();
        if (productIds == null || quantities == null || prices == null || notes == null) {
            log.warn("addProducts() nothing to do "
                    + "[quantities=" + ((quantities == null) ? "NULL" : quantities.length)
                    + ",prices=" + ((prices == null) ? "NULL" : prices.length)
                    + ",notes=" + ((notes == null) ? "NULL" : notes.length)
                    + "]");
            return added;
        }
        int length = productIds.length;
        if (length == quantities.length && length == prices.length && length == notes.length) {
            if (replace || item != null) {
                int selectedIndex = getSelectedReplaceable(length, productIds, quantities);
                if (selectedIndex < 0) {
                    throw new ClientException(ErrorCode.SELECTION_MISSING);
                }
                Product selected = getSelectedProduct(productIds[selectedIndex]);
                preventUnsupportedDiscount(selected, selectedPosition.isDiscounts());
                if (optionMode) {
                    calculation = manager.replaceOptionalItem(
                            employee,
                            calculation,
                            (item != null ? item.getId() : replaceable),
                            selected.getProductId(),
                            NumberUtil.createDouble(quantities[selectedIndex]),
                            NumberFormatter.createDecimal(prices[selectedIndex], false),
                            notes[selectedIndex]);
                } else {
                    if (item != null && item.isPartlistAvailable()) {
                        PartlistManager partlistManager = (PartlistManager) getService(PartlistManager.class.getName());
                        partlistManager.deletePartlist(getCalculation().getId(), item.getProduct().getProductId());
                    }
                    calculation = (Calculation) manager.replaceItem(
                            employee,
                            calculation,
                            (item != null ? item.getId() : replaceable),
                            selected.getProductId(),
                            NumberUtil.createDouble(quantities[selectedIndex]),
                            NumberFormatter.createDecimal(prices[selectedIndex], false),
                            notes[selectedIndex]);
                    CalculationTemplateManager ctm = getCalculationTemplateManager();
                    if (getBusinessCase() != null && getBusinessCase().getType() != null
                            && item != null
                            && ctm.isTemplateAvailable(item.getProduct().getProductId(), getBusinessCase().getType().getId())) {
                        CalculationTemplate template = ctm.findByProductAndType(
                                calculation, item.getProduct().getProductId(), getBusinessCase().getType().getId());
                        calculation = manager.removeTemplate(employee, calculation, template);
                    }
                    if (getBusinessCase() != null && getBusinessCase().getType() != null
                            && ctm.isTemplateAvailable(selected.getProductId(), getBusinessCase().getType().getId())) {
                        CalculationTemplate template = ctm.findByProductAndType(calculation, selected.getProductId(), getBusinessCase().getType()
                                .getId());
                        calculation = manager.applyTemplate(employee, calculation, template,
                                (getBusinessCase().getType().isWholeSale() || getBusinessCase().getCustomer().isReseller()));
                    } else if (log.isDebugEnabled()) {
                        log.debug("addProducts() product does not provide template [product="
                                + selected.getProductId()
                                + ", businessCase="
                                + (getBusinessCase() == null ? "null" : getBusinessCase().getPrimaryKey())
                                + ", businessType="
                                + (getBusinessCase() == null || getBusinessCase().getType() == null ? "null" : getBusinessCase().getType()
                                        .getId())
                                + "]");
                    }
                }
                added.add(selected.getProductId());
                replace = false;
                changed = true;
            } else {
                for (int i = 0; i < length; i++) {
                    Double quantity = NumberUtil.createDouble(quantities[i]);
                    if (quantity > 0
                            || (businessCase.getType().isService() && quantity < 0)) {

                        Product selected = getSelectedProduct(productIds[i]);
                        if (selected != null && selected.getGroup() != null) {
                            preventUnsupportedDiscount(selected, selectedPosition.isDiscounts());

                            if (optionMode) {
                                calculation = manager.addOptionalProduct(
                                        employee,
                                        calculation,
                                        selectedPosition.getId(),
                                        selected, 
                                        customNames[i],
                                        quantity,
                                        NumberFormatter.createDecimal(prices[i], false), 
                                        notes[i]);
                            } else {
                                calculation = (Calculation) manager.addItem(
                                        employee,
                                        calculation,
                                        selectedPosition.getId(),
                                        selected,
                                        customNames[i],
                                        quantity,
                                        NumberFormatter.createDecimal(prices[i], false),
                                        includePrice,
                                        notes[i],
                                        null); // externalId
                                changed = true;
                                CalculationTemplateManager ctm = getCalculationTemplateManager();
                                if (getBusinessCase() != null && getBusinessCase().getType() != null
                                        && ctm.isTemplateAvailable(selected.getProductId(), getBusinessCase().getType().getId())) {
                                    CalculationTemplate template = ctm.findByProductAndType(calculation, selected.getProductId(), getBusinessCase()
                                            .getType().getId());
                                    calculation = manager.applyTemplate(employee, calculation, template,
                                            (getBusinessCase().getType().isWholeSale() || getBusinessCase().getCustomer().isReseller()));

                                } else if (log.isDebugEnabled()) {
                                    log.debug("addProducts() product does not provide template [product="
                                            + selected.getProductId()
                                            + ", businessCase="
                                            + (getBusinessCase() == null ? "null" : getBusinessCase().getPrimaryKey())
                                            + ", businessType="
                                            + (getBusinessCase() == null || getBusinessCase().getType() == null ? "null" : getBusinessCase()
                                                    .getType().getId())
                                            + "]");
                                }
                                added.add(selected.getProductId());
                            }
                            if (log.isDebugEnabled()) {
                                log.debug("addProducts() added "
                                        + selected.getProductId());
                            }
                        } else if (log.isDebugEnabled()) {
                            log.debug("addProducts() unable to find selected product [id=" + productIds[i] + "]");
                        }
                    }
                }
            }
        }
        refresh();
        return added;
    }

    private void preventUnsupportedDiscount(Product product, boolean discountPosition) throws ClientException {
        if (product.getGroup() == null) {
            throw new ClientException(ErrorCode.PRODUCT_CONFIG_INVALID);
        }
        if (product.getGroup().isDiscount()) {
            try {
                this.employee.checkPermission(discountPermissions);
            } catch (PermissionException p) {
                throw new ClientException(p.getMessage());
            }
        } else if (discountPosition) {
            throw new ClientException(ErrorCode.PRODUCT_SELECTION_INVALID);
        }
    }

    public void addDiscount(Long itemToDiscount) throws ClientException {
        try {
            calculation = getCalculationManager().addDiscount(employee, calculation, itemToDiscount);
            refresh();
        } catch (Throwable t) {
            if (t instanceof ClientException) {
                throw new ClientException(t.getMessage());
            }
            // TODO check why null pointer exception happens here for item with
            // 0.00 set as price
            throw new ClientException(ErrorCode.DISCOUNT_DISABLED);
        }
    }

    public void deleteProduct(Long itemId) {
        if (optionMode) {
            if (log.isDebugEnabled()) {
                log.debug("deleteProduct() called in options context");
            }
            calculation = getCalculationManager().removeOptionalProduct(employee, calculation, itemId);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("deleteProduct() called in standard context");
            }
            Item itemToDelete = null;
            for (Item item : getCalculation().getAllItems()) {
                if (itemId != null && itemId.equals(item.getId())) {
                    itemToDelete = item;
                    break;
                }
            }
            if (itemToDelete != null && itemToDelete.isPartlistAvailable()) {
                PartlistManager partlistManager = (PartlistManager) getService(PartlistManager.class.getName());
                partlistManager.deletePartlist(getCalculation().getId(), itemToDelete.getProduct().getProductId());
            }
            CalculationTemplateManager ctm = getCalculationTemplateManager();
            if (getBusinessCase() != null && getBusinessCase().getType() != null
                    && itemToDelete != null
                    && ctm.isTemplateAvailable(itemToDelete.getProduct().getProductId(), getBusinessCase().getType().getId())) {
                CalculationTemplate template = ctm.findByProductAndType(calculation, itemToDelete.getProduct().getProductId(), getBusinessCase()
                        .getType().getId());
                calculation = getCalculationManager().removeTemplate(employee, calculation, template);
            }
            calculation = getCalculationManager().removeProduct(employee, calculation, itemId);
            changed = true;
        }
        refresh();
    }

    public Calculation changeCalculator(String name) {
        CalculationManager manager = getCalculationManager();
        calculation = manager.changeCalculator(employee, calculation, name); 
        changed = true;
        refresh();
        return calculation;
    }

    public void changePriceDisplayStatus(Long item) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("changePriceDisplayStatus() invoked [id=" + item + "]");
        }
        calculation = getCalculationManager().changePriceDisplayStatus(employee, calculation, item);
        changed = true;
        refresh();
    }

    public void enablePriceDisplayStatus() throws ClientException {
        changePriceDisplayStatus(true);
    }

    public void disablePriceDisplayStatus() throws ClientException {
        changePriceDisplayStatus(false);
    }

    private void changePriceDisplayStatus(boolean enable) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("changePriceDisplayStatus() invoked [enable=" + enable + "]");
        }
        calculation = getCalculationManager().changePriceDisplayStatus(employee, calculation, enable);
        changed = true;
        refresh();
    }

    public Item getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Long id) {
        List<ItemPosition> positions = null;
        if (optionMode) {
            if (log.isDebugEnabled()) {
                log.debug("setSelectedItem() options fetched");
            }
            positions = getCalculation().getOptions();
        } else {
            if (log.isDebugEnabled()) {
                log.debug("setSelectedItem() positions fetched");
            }
            positions = getCalculation().getPositions();
        }
        boolean newAssigned = false;
        for (int i = 0, j = positions.size(); i < j; i++) {
            if (newAssigned) {
                break;
            }
            ItemPosition pos = positions.get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.getId().equals(id)) {
                    if (log.isDebugEnabled()) {
                        log.debug("setSelectedItem() done [item=" + id + "]");
                    }
                    selectedItem = next;
                    newAssigned = true;
                    break;
                }
            }
        }
    }

    public void updateItem(
            String customName,
            Double quantity, 
            BigDecimal partnerPrice, 
            boolean partnerPriceOverridden, 
            BigDecimal price, 
            String note)
            throws ClientException {
        if (optionMode) {
            calculation = getCalculationManager().updateOptionalItem(
                    employee,
                    calculation,
                    selectedItem.getId(),
                    customName,
                    quantity,
                    price,
                    note);
        } else if (selectedItem != null) {
            calculation = (Calculation) getCalculationManager().updateItem(
                    employee,
                    calculation,
                    selectedItem.getId(),
                    customName,
                    quantity,
                    partnerPrice,
                    partnerPriceOverridden,
                    employee.isPermissionGrant(overridePartnerPricePermissions),
                    price,
                    note);
            changed = true;
        }
        selectedItem = null;
        refresh();
    }

    public void moveUpItem(Long itemId) {
        CalculationManager manager = getCalculationManager();
        if (optionMode) {
            calculation = manager.moveUpOptionItem(employee, calculation, itemId);
        } else {
            calculation = (Calculation) manager.moveUpItem(employee, calculation, itemId);
        }
        refresh();
    }

    public void moveDownItem(Long itemId) {
        CalculationManager manager = getCalculationManager();
        if (optionMode) {
            calculation = manager.moveDownOptionItem(employee, calculation, itemId);
        } else {
            calculation = (Calculation) manager.moveDownItem(employee, calculation, itemId);
        }
        refresh();
    }

    public void createOptions() throws ClientException {
        calculation = getCalculationManager().createOptions(employee, calculation);
    }

    public boolean isOptionMode() {
        return optionMode;
    }

    public void setOptionMode(boolean optionMode) {
        this.optionMode = optionMode;
    }

    public boolean isIncludeLazyProducts() {
        return includeLazyProducts;
    }

    public void setIncludeLazyProducts(boolean includeLazyProducts) {
        this.includeLazyProducts = includeLazyProducts;
    }

    public void disableLock() {
        getCalculationManager().unlock(calculation, employee);
        if (log.isDebugEnabled()) {
            log.debug("diableLock() done");
        }
    }

    public void calculate() throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("calculate() invoked for " + calculation.getId());
        }
        if (businessCase.isSalesContext()
                && businessCase.getFlowControlSheet().size() > 1
                && !calculation.isInitial()) {
            for (int i = 0, j = calculation.getPositions().size(); i < j; i++) {
                ItemPosition next = calculation.getPositions().get(i);
                for (int k = 0, l = next.getItems().size(); k < l; k++) {
                    Item nextItem = next.getItems().get(k);
                    if (nextItem.getProduct().isOfferUsageOnly()) {
                        if (log.isDebugEnabled()) {
                            log.debug("calculate() found invalid product "
                                    + nextItem.getProduct().getProductId()
                                    + " in sales " + businessCase.getPrimaryKey());
                        }
                        throw new ClientException(
                                ErrorCode.PRODUCT_SELECTION_INVALID,
                                nextItem.getProduct().getProductId());
                    }
                }
            }
        }
        CalculatorService calculatorService = (CalculatorService) getService(calculation.getCalculatorClass());
        calculatorService.calculate(calculation, employee, false);
        if (businessCase.isSalesContext()) {
            checkSales();
            CalculationManager manager = getCalculationManager();
            manager.unlock(calculation, employee);
        }
        changed = false;
        refresh();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.calc.Calculator#markChanged()
     */
    public void markChanged() {
        changed = true;
    }

    private void checkSales() throws ClientException {
        if (businessCase.getFlowControlSheet().size() > 1) {
            for (int i = 0, j = calculation.getPositions().size(); i < j; i++) {
                ItemPosition next = calculation.getPositions().get(i);
                for (int k = 0, l = next.getItems().size(); k < l; k++) {
                    Item nextItem = next.getItems().get(k);
                    if (nextItem.getProduct().isOfferUsageOnly()) {
                        throw new ClientException(ErrorCode.PRODUCT_SELECTION_INVALID);
                    }
                }
            }
        }
    }

    protected abstract CalculationManager getCalculationManager();

    public void refresh() {
        if (changed) {
            CalculatorService calculatorService = (CalculatorService) getService(calculation.getCalculatorClass());
            try {
                calculatorService.calculate(calculation, employee, true);
            } catch (Exception e) {
                if (log.isDebugEnabled()) {
                    log.debug("refresh() calculate will run into trouble with current setup [message="
                            + e.getMessage() + ", class=" + e.getClass().getName() + "]", e);
                }
            }
        }
        calculation = (Calculation) getCalculationManager().getList(calculation.getId());
        loadRevenue(calculation);
        if (log.isDebugEnabled()) {
            log.debug("refresh() done, values: "
                    + (calculation == null ? "null!" : ("\n" + calculation.getLogValues())));
        }
    }

    private void loadRevenue(Calculation calc) {
        if (calc != null) {
            try {
                SalesRevenueManager salesRevenueManager = (SalesRevenueManager) getService(SalesRevenueManager.class.getName());
                revenue = salesRevenueManager.getRevenue(calc.getId());
            } catch (Exception e) {
                log.warn("loadRevenue() failed [message=" + e.getMessage() + ", class=" + e.getClass().getName() + "]", e);
            }
        }
    }

    private Product getSelectedProduct(Long id) {
        ProductManager manager = getProductManager();
        return manager.get(id);
    }

    private int getSelectedReplaceable(int count, Long[] productIds, String[] quantities)
            throws ClientException {
        int result = -1;
        int selections = 0;
        for (int i = 0; i < count; i++) {
            if (selections > 1) {
                throw new ClientException(ErrorCode.SELECTION_MULTIPLE_DISABLED);
            }
            Double quantity = NumberUtil.createDouble(quantities[i]);
            if (quantity.doubleValue() != 0) {
                result = i;
                selections++;
            }
        }
        return result;
    }

    protected ProductManager getProductManager() {
        return (ProductManager) getService(ProductManager.class.getName());
    }

    private CalculationTemplateManager getCalculationTemplateManager() {
        return (CalculationTemplateManager) getService(CalculationTemplateManager.class.getName());
    }

    protected Object getService(String name) {
        if (locator == null) {
            throw new com.osserp.common.BackendException();
        }
        return locator.lookupService(name);
    }
}
