/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 7, 2007 9:00:50 AM 
 * 
 */
package com.osserp.gui.client;

import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.web.SessionUtil;

import com.osserp.groovy.web.ResourceLocatorInitListener;

/**
 * Initializes a web application dealing with com.osserp.core package.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CoreContextListener extends ResourceLocatorInitListener {
    private static Logger log = LoggerFactory.getLogger(CoreContextListener.class.getName());

    @Override
    protected void initializeApplication(ServletContext ctx) {
        super.initializeApplication(ctx);
        // create the app ctx of the client view components
        ContextManager.createApplicationContext(ctx);
        ContextManager.createConfig(ctx);
        ContextManager.loadPageDefaults(ctx);
        ContextManager.loadPlugins(ctx);
        // app shares menu with thirdparty apps / plugins
        SessionUtil.enableShareMenu(ctx);
        // init params info
        if (ContextManager.isPropertyEnabled(ctx, "systemPrintInitialContext")) {
            contextOut(ctx);
        }
        // os environment info
        if (ContextManager.isPropertyEnabled(ctx, "systemPrintEnvironment")) {
            envOut();
        }
        if (log.isInfoEnabled()) {
            log.info("initializeApplication: done");
        }
    }

    protected void contextOut(ServletContext ctx) {
        if (log.isDebugEnabled()) {
            Enumeration<String> initNames = ctx.getInitParameterNames();
            int cnt = 0;
            while (initNames.hasMoreElements()) {
                String n = initNames.nextElement();
                String v = ctx.getInitParameter(n);
                if (cnt == 0) {
                    log.debug("Init Params:");
                    cnt = 1;
                }
                log.debug("[name=" + n + ", value=" + v + "]");
            }
            Enumeration<String> attributes = ctx.getAttributeNames();
            StringBuilder classAttrs = new StringBuilder();
            StringBuilder stringAttrs = new StringBuilder();
            while (attributes.hasMoreElements()) {

                String n = attributes.nextElement();
                Object v = ctx.getAttribute(n);

                if (n.indexOf("Icon") < 0 && n.indexOf("jsp_classpath") < 0) {

                    if ((v instanceof String)
                            || (v instanceof Long)
                            || (v instanceof Boolean)
                            || (v instanceof Integer)) {

                        stringAttrs.append("[name=").append(n).append(", value=").append(v).append("]\n");

                    } else if (!(v instanceof java.util.ArrayList)) {

                        classAttrs.append("[name=").append(n).append(", class=").append((v == null ? "null" : v.getClass().getName())).append("]\n");
                    }
                }
            }
            log.debug("\nObjects:\n" + classAttrs.toString() + "\nAttributes:\n" + stringAttrs.toString() + "contextOut: done");
        }
    }

    private void envOut() {
        Map<String, String> env = new TreeMap<>(System.getenv());
        log.debug("Environment:");
        for (String name : env.keySet()) {
            log.debug("[name=" + name + ", value=" + env.get(name) + "]");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        ServletContext ctx = event.getServletContext();
        ContextManager.destroyApplicationContext(ctx);
        super.contextDestroyed(event);
    }
}
