/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 3, 2011 2:41:01 PM 
 * 
 */
package com.osserp.core.hrm;

import java.util.Date;
import java.util.List;

import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordingQueryManager {

    /**
     * Indicates that time recording exists
     * @param employee
     * @return true if employee is time recording
     */
    boolean exists(Employee employee);

    /**
     * Starts the time recording query task with given year and mail and query type
     * @param year
     * @param mail
     * @param type
     */
    void query(String date, String mail, Long typeId);

    /**
     * Returns the name and the leave and timebank at the given month for the given employee
     * @param employee
     * @param year
     * @result array of string with surname, firstname, cost center, leave, timebank
     */
    String[] getTimebankQuery(Employee employee, Date date);

    /**
     * Returns the name and the date of incorrect bookings at the given month for the given employee
     * @param employee
     * @param year
     * @result list of string arrays with surname, firstname, cost center, date, reason
     */
    List<String[]> getBookingQuery(Employee employee, Date date);

}
