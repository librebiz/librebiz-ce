/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 02.10.2004 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.contacts.Contact;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.suppliers.SupplierType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Suppliers extends ClassifiedContacts {
    
    /**
     * Provides a list of all supplier types
     * @return supplier types
     */
    List<SupplierType> getSupplierTypes();

    /**
     * Provides list of id / name objects of all available shipping companies
     * @return shipping company list
     */
    List<Option> getShippingCompanies();

    /**
     * Provides list of id / shortkey objects of all available shipping companies
     * @return shipping company shortkey list
     */
    List<Option> getShippingCompanyKeys();

    /**
     * Provides the supplierType configured as salary account.
     * @return salaryType
     * @throws BackendException if no type configured and default
     * type does not exist. The default type is provided by initial
     * database setup script. 
     */
    SupplierType getSalaryType();

    /**
     * Finds the all suppliers dealing with given product group
     * @param groupId
     * @return suppliers
     */
    List<Supplier> findByProductGroup(Long groupId);

    /**
     * Provides the associated supplier of a system company
     * @param company
     * @return supplier
     * @throws ClientException if no such supplier exists
     */
    Supplier findByCompany(Long company) throws ClientException;

    /**
     * Finds all suppliers delivering specified product
     * @param productId
     * @return suppliers
     */
    List<Supplier> findByDeliveries(Long productId);

    /**
     * Finds all suppliers whose direct invoice booking flag is enabled
     * @return suppliers or empty list
     */
    List<Supplier> findByDirectInvoiceBooking();

    /**
     * Finds all suppliers whose simple billing flag is enabled
     * @return suppliers or empty list
     */
    List<Supplier> findBySimpleBilling();

    /**
     * Adds an product group to provided supplier
     * @param supplier
     * @param group
     * @return supplier
     */
    Supplier addProductGroup(Supplier supplier, ProductGroup productGroup);

    /**
     * Creates a supplier for an employee required to create salary payments. 
     * This results in a virtual supplier with simple billing enabled.    
     * @param createdBy
     * @param supplierId
     * @param employee
     */
    void createSalaryAccount(Long createdBy, Long supplierId, Contact employee);
    
    /**
     * Indicates if salary account exists
     * @param supplierId
     * @return true if supplier with id exists
     */
    boolean isSalaryAccountExisting(Long supplierId);
}
