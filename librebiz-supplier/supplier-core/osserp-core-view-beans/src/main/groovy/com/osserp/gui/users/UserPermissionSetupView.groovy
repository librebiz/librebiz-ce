/**
 *
 * Copyright (C) 2006, 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 8, 2006 6:32:32 AM 
 * Created on Dec 9, 2013 10:11:15 AM (Groovy implementation) 
 * 
 */
package com.osserp.gui.users

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.Permission
import com.osserp.common.UserPermission
import com.osserp.common.User
import com.osserp.common.UserManager
import com.osserp.common.util.CollectionUtil

import com.osserp.core.Comparators
import com.osserp.core.employees.Employee
import com.osserp.core.model.PermissionImpl

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class UserPermissionSetupView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(UserPermissionSetupView.class.getName())

    Map<String, Permission> perms = [:]

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
			perms = [:]
            List<Permission> all = userManager.getPermissions()
            all.each {
                perms[it.permission] = it
            }
            Long id = form.getLong('id')
            bean = userManager.findById(id)
            if (!bean) {
                throw new ViewContextException(this)
            }
        }
    }

    List<Permission> getCurrentPermissions() {
        List<Permission> result = []
        if (bean) {
            Collection<UserPermission> current = bean.getPermissions().values()
            for (Iterator<UserPermission> i = current.iterator(); i.hasNext();) {
                UserPermission next = i.next()
                Permission p = perms.get(next.permission)
                if (p) {
                    result.add(new PermissionImpl(next.getPermission(), p.description, p.displayable))
                } else {
				    logger.debug("getCurrentPermissions: ignoring administrative [permission=${next.permission}]")
                }
            }
            CollectionUtil.sort(result, Comparators.createPermissionComparator())
        }
        return result
    }

    List<Permission> getAvailablePermissions() {
        List<Permission> result = []
        List<Permission> all = userManager.getPermissions()
        if (bean) {
            all.each { Permission next ->
                if (next.permission && !bean.getPermissions().containsKey(next.permission)) {
                    result.add(next)
                }
            }
        }
        return result
    }

    void grantPermission() throws ClientException {
        String permission = form.getString('name')
        logger.debug("grantPermission: invoked [name=${permission}]")
        bean = userManager.grantPermission(user, bean?.employee, permission)
        logger.debug("grantPermission: done [name=${permission}]")
    }


    void revokePermission() throws ClientException {
        String permission = form.getString('name')
        logger.debug("revokePermission: invoked [name=${permission}]")
        bean = userManager.revokePermission(user, bean?.employee, permission)
        logger.debug("revokePermission: done [name=${permission}]")
    }

    void toggleActivation() {
        if (bean?.isActive()) {
            bean = userManager.disable(bean)
        } else {
            bean = userManager.enable(bean)
        }
        logger.debug("toggleActivation: done [id=${bean?.getLdapUid()}, activated=${bean?.isActive()}]")
        createCustomNavigation()
    }

    @Override
    protected void createCustomNavigation() {
        nav.beanNavigation = [exitLink, toggleActivationLink, homeLink]
    }

    private MenuItem getToggleActivationLink() {
        if (bean?.isActive()) {
            new MenuItem(link: "${context.fullPath}/${context.name}/toggleActivation", icon: 'smileIcon', title: 'deactivate')
        } else {
            new MenuItem(link: "${context.fullPath}/${context.name}/toggleActivation", icon: 'nosmileIcon', title: 'activate')
        }
    }
    
    private UserManager getUserManager() {
        getService(UserManager.class.getName())
    }
}
