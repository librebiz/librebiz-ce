/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 19:10:34 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.Option;
import com.osserp.common.util.DateUtil;

import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.finance.RecordDiscount;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Product;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOfferImpl extends AbstractOffer implements SalesOffer {

    private String calculationInfo = null;
    private String calculatorName = null;
    private Double minimalMargin = SalesUtil.getDefaultMinimalMargin();
    private Double targetMargin = SalesUtil.getDefaultTargetMargin();

    protected SalesOfferImpl() {
        super();
    }

    /**
     * Creates a new SalesOrder
     * @param id
     * @param type
     * @param request
     * @param defaultPaymentAgreement
     * @param user
     * @param taxRate
     * @param reducedTaxRate
     */
    public SalesOfferImpl(
            Long id,
            RecordType type,
            Request request,
            PaymentAgreement defaultPaymentAgreement,
            Employee user,
            Double taxRate,
            Double reducedTaxRate) {
        super(
                id,
                type,//RecordType.OFFER,
                null, //BookingType currently not supported by salesOffers / purchaseOffers only
                request.getType().getCompany(),
                request.getBranch().getId(),
                request.getRequestId(),
                request.getCustomer(),
                user,
                request.getRequestId(),
                taxRate,
                reducedTaxRate);
        minimalMargin = request.getType().getMinimalMargin();
        targetMargin = request.getType().getTargetMargin();
        if (getPaymentAgreements().isEmpty()) {
            RecordPaymentAgreement obj = createPaymentAgreement();
            if (defaultPaymentAgreement != null) {
                obj.update(defaultPaymentAgreement);
            }
            getPaymentAgreements().add(obj);
        }
    }

    /**
     * Creates a new SalesOrder
     * @param id
     * @param type
     * @param offer
     * @param user
     * @param taxRate
     * @param reducedTaxRate
     */
    public SalesOfferImpl(
            Long id,
            RecordType type,
            Offer offer,
            Employee user,
            Double taxRate,
            Double reducedTaxRate) {
        super(
                id,
                type,//RecordType.OFFER,
                null, //BookingType currently not supported by salesOffers / purchaseOffers only
                offer.getCompany(),
                offer.getBranchId(),
                offer.getReference(),
                offer.getContact(),
                user,
                offer.getBusinessCaseId(),
                taxRate,
                reducedTaxRate);
        SalesOffer salesOffer = (SalesOffer) offer;
        minimalMargin = salesOffer.getMinimalMargin();
        targetMargin = salesOffer.getTargetMargin();
        if (getPaymentAgreements().isEmpty()) {
            RecordPaymentAgreement obj = createPaymentAgreement();
            if (offer.getPaymentAgreement() != null) {
                obj.update(offer.getPaymentAgreement());
            }
            getPaymentAgreements().add(obj);
        }
        updateItems(offer);
    }

    /**
     * Creates new SalesOrder by calculation
     * @param id
     * @param type
     * @param request
     * @param calculation
     * @param defaultPaymentAgreement
     * @param user
     * @param taxRate
     * @param reducedTaxRate
     */
    public SalesOfferImpl(
            Long id,
            RecordType type,
            Request request,
            Calculation calculation,
            PaymentAgreement defaultPaymentAgreement,
            Employee user,
            Double taxRate,
            Double reducedTaxRate) {
        super(
                id,
                request.getType().getCompany(),
                request.getBranch().getId(),
                type,//RecordType.OFFER, 
                request.getRequestId(),
                request.getCustomer(),
                user,
                taxRate,
                reducedTaxRate,
                DateUtil.addDays(DateUtil.getCurrentDate(), 30),
                calculation.getReference(),
                calculation.getPositions(),
                calculation.getOptions());
        calculationInfo = calculation.getName();
        calculatorName = calculation.getCalculatorClass();
        if (calculation.getMinimalMargin() > 0 && calculation.getTargetMargin() > 0) {
            minimalMargin = calculation.getMinimalMargin();
            targetMargin = calculation.getTargetMargin();
        } else {
            minimalMargin = request.getType().getMinimalMargin();
            targetMargin = request.getType().getTargetMargin();
        }
        if (getPaymentAgreements().isEmpty()) {
            RecordPaymentAgreement obj = createPaymentAgreement();
            if (defaultPaymentAgreement != null) {
                obj.update(defaultPaymentAgreement);
            }
            getPaymentAgreements().add(obj);
        }
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new SalesOfferItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                (getItems() == null || getItems().isEmpty() ? 0 : getItems().size()),
                externalId);
    }

    @Override
    protected Item createOptionItem(
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice) {

        return new SalesOfferOptionItemImpl(
                this,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                false,
                false,
                purchasePrice,
                note,
                includePrice,
                null);
    }

    @Override
    protected RecordDiscount createDiscount(
            Employee employee,
            Double discount,
            String note) {
        return new SalesOfferDiscountImpl(this, employee, discount, note);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        return new SalesOfferInfoImpl(this, info, user);
    }

    @Override
    protected RecordPaymentAgreement createPaymentAgreement() {
        return new SalesOfferPaymentAgreementImpl(this);
    }

    @Override
    public boolean isSales() {
        return true;
    }

    public final void updateByCalculation(Calculation calculation) {
        this.updateItems(calculation.getPositions(), calculation.getOptions());
        this.calculatorName = calculation.getCalculatorClass();
    }

    public String getCalculationInfo() {
        return calculationInfo;
    }

    public void setCalculationInfo(String calculationInfo) {
        this.calculationInfo = calculationInfo;
    }

    public String getCalculatorName() {
        return calculatorName;
    }

    public void setCalculatorName(String calculatorName) {
        this.calculatorName = calculatorName;
    }

    public Double getMinimalMargin() {
        return minimalMargin;
    }

    public void setMinimalMargin(Double minimalMargin) {
        this.minimalMargin = minimalMargin;
    }

    public Double getTargetMargin() {
        return targetMargin;
    }

    public void setTargetMargin(Double targetMargin) {
        this.targetMargin = targetMargin;
    }

    public void resetOrderStatus() {
        if (getStatus() > SalesOffer.STAT_SENT) {
            setStatus(SalesOffer.STAT_SENT);
        }
    }
}
