/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.mail

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dms.DocumentData

import com.osserp.common.mail.Attachment
import com.osserp.common.mail.ReceivedMail
import com.osserp.common.mail.ReceivedMailInfo
import com.osserp.common.util.FileUtil
import com.osserp.common.web.tags.TagUtil
import com.osserp.core.BusinessCaseSearch
import com.osserp.core.contacts.Contact
import com.osserp.core.customers.CustomerSummary
import com.osserp.core.mail.FetchmailInboxManager

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractFetchmailInboxView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractFetchmailInboxView.class.getName())

    Boolean adminMode = false
    Long selectedUser
    CustomerSummary customerSummary
    ReceivedMail receivedMail

    public AbstractFetchmailInboxView() {
        super()
        headerName = 'inboxLabel'
    }

    protected abstract void loadList();

    @Override
    void reload() {
        loadList()
    }

    @Override
    void select() {
        super.select();
        if (receivedMailInfo) {
            receivedMail = fetchmailInboxManager.getMessage(receivedMailInfo.messageId)
            logger.debug("select: done [id=${receivedMail?.id}, attachments=${!receivedMail.attachments.empty}, headers=${!receivedMail.headers.empty}]")
            if (receivedMail?.reference
                    && Contact.CUSTOMER == receivedMail?.referenceType) {
                customerSummary = businessCaseSearch.getCustomerSummary(receivedMail.reference)
            } else {
                customerSummary = null
            }
        } else {
            receivedMail = null
            customerSummary = null
        }
    }

    void delete() {
        if (receivedMail) {
            fetchmailInboxManager.delete(receivedMail)
            receivedMail = bean = null
            loadList()
        }
    }

    void resetBusinessCase() {
        if (receivedMail) {
            receivedMail = fetchmailInboxManager.resetReferences(receivedMail)
            receivedMail = bean = null
            disableEditMode()
            loadList()
        }
    }

    void resetContact() {
        if (receivedMail) {
            receivedMail = fetchmailInboxManager.resetReferences(receivedMail)
            receivedMail = bean = null
            disableEditMode()
            loadList()
        }
    }

    void takeover() {
        if (receivedMail) {
            receivedMail = fetchmailInboxManager.setUser(receivedMail, domainUser.id)
            receivedMail = bean = null
            disableEditMode()
            loadList()
        }
    }

    void deleteAttachment() {
        Attachment obj = fetchAttachment()
        if (obj) {
            receivedMail = fetchmailInboxManager.deleteAttachment(receivedMail, obj)
        }
    }

    DocumentData downloadAttachment() {
        Attachment obj = fetchAttachment()
        if (obj) {
            byte[] bytes = FileUtil.getBytes(new File(obj.file))
            def data = new DocumentData(
                bytes,
                TagUtil.encodeForUri(obj.fileName),
                obj.contentType)
            return data
        }
        return null
    }

    void ignoreAttachment() {
        Attachment obj = fetchAttachment()
        if (obj) {
            receivedMail = fetchmailInboxManager.ignoreAttachment(receivedMail, obj)
        }
    }

    /**
     * Fetches attachment from received message using attachmentId request param
     * @return attachment or null if attachment or receivedMail not exists
     */
    protected Attachment fetchAttachment() {
        Long attachmentId = form.getLong('attachmentId');
        if (receivedMail && attachmentId) {
            Attachment obj = receivedMail.attachments.find { it.id == attachmentId }
            return obj
        }
        return null
    }

    protected void assignBusinessCase(Long reference, Long referenceType, Long businessId) {
        if (receivedMail) {
            receivedMail = bean = fetchmailInboxManager.setBusinessCase(
                receivedMail,
                reference,
                referenceType,
                businessId)
        }
    }

    boolean isBusinessCaseContext() {
        return false
    }

    protected ReceivedMailInfo getReceivedMailInfo() {
        return bean
    }

    protected BusinessCaseSearch getBusinessCaseSearch() {
        return getService(BusinessCaseSearch.class.getName())
    }

    protected FetchmailInboxManager getFetchmailInboxManager() {
        return getService(FetchmailInboxManager.class.getName())
    }
}
