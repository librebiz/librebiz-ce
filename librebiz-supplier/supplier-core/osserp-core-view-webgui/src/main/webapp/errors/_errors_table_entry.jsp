<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="sessionErrorsFound" value="false"/>
<c:if test="${!empty sessionScope.errors}">
<c:set var="sessionErrorsFound" value="true"/>
<c:set var="colspanValue" value="${requestScope.errorsColspanCount}"/>
<o:logger write="current page has errors" level="debug"/>
<c:choose>
<c:when test="${!empty sessionScope.errorId}">
<tr><td<c:if test="${!empty colspanValue}"> colspan="<o:out value="${colspanValue}"/>"</c:if>><span class="errormessage"><o:out value="${sessionScope.errors}"/> / <o:out value="${sessionScope.errorId}"/></span></td></tr>
<c:remove var="errorId" scope="session"/>
</c:when>
<c:when test="${!empty sessionScope.errorDetails}">
<o:logger write="current page has errorDetails" level="debug"/>
<tr><td<c:if test="${!empty colspanValue}"> colspan="<o:out value="${colspanValue}"/>"</c:if>><span class="errormessage"><o:out value="${sessionScope.errors}"/> : <o:out value="${sessionScope.errorDetails}"/></span></td></tr>
<c:remove var="errorDetails" scope="session"/>
</c:when>
<c:otherwise>
<tr><td<c:if test="${!empty colspanValue}"> colspan="<o:out value="${colspanValue}"/>"</c:if>><span class="errormessage"><o:out value="${sessionScope.errors}"/></span></td></tr>
</c:otherwise>
</c:choose>
</c:if>
<c:if test="${!sessionErrorsFound && requestScope.error}">
<tr><td<c:if test="${!empty colspanValue}"> colspan="<o:out value="${colspanValue}"/>"</c:if>><span class="errormessage"><o:out value="${requestScope.error}"/></span></td></tr>
</c:if>
<o:removeErrors/>
