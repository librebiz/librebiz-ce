/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 26-Apr-2005 17:07:14 
 * 
 */
package com.osserp.core.finance;

import com.osserp.common.EntityRelation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PaymentAgreement extends EntityRelation {

    /**
     * The percent of the price to be paid as downpayment
     * @return downpaymentPercent
     */
    Double getDownpaymentPercent();

    /**
     * Sets the percent of the price to be paid as downpayment
     * @param downpaymentPercent
     */
    void setDownpaymentPercent(Double downpaymentPercent);

    /**
     * The percent of the price to be paid after delivery
     * @return deliveryInvoicePercent
     */
    Double getDeliveryInvoicePercent();

    /**
     * Sets the percent of the price to be paid after delivery
     * @param deliveryInvoicePercent
     */
    void setDeliveryInvoicePercent(Double deliveryInvoicePercent);

    /**
     * The percent of the price to be paid at final invoice
     * @return finalInvoicePercent
     */
    Double getFinalInvoicePercent();

    /**
     * Sets the percent of the price to be paid at final invoice
     * @param finalInvoicePercent
     */
    void setFinalInvoicePercent(Double finalInvoicePercent);

    /**
     * Provides the id of the downpayment payment target
     * @return downpaymentTargetId
     */
    Long getDownpaymentTargetId();

    /**
     * Sets the downpaymentTargetId
     * @param downpaymentTargetId
     */
    void setDownpaymentTargetId(Long downpaymentTargetId);

    /**
     * Provides the id of the delivery invoice payment target
     * @return deliveryInvoiceTargetId
     */
    Long getDeliveryInvoiceTargetId();

    /**
     * Sets the deliveryInvoiceTargetId
     * @param deliveryInvoiceTargetId
     */
    void setDeliveryInvoiceTargetId(Long deliveryInvoiceTargetId);

    /**
     * Provides the id of the final invoice payment target
     * @return finalInvoiceTargetId
     */
    Long getFinalInvoiceTargetId();

    /**
     * Sets the finalInvoiceTargetId
     * @param finalInvoiceTargetId
     */
    void setFinalInvoiceTargetId(Long finalInvoiceTargetId);

    /**
     * Provides the payment condition
     * @return paymentCondition
     */
    PaymentCondition getPaymentCondition();

    /**
     * Sets the payment condition
     * @param paymentCondition
     */
    void setPaymentCondition(PaymentCondition paymentCondition);

    /**
     * A note to the payment agreement
     * @return note
     */
    String getNote();

    /**
     * Sets the note
     * @param note
     */
    void setNote(String note);

    /**
     * Default payment method id
     * @return paymentMethodId
     */
    Long getPaymentMethodId();

    /**
     * Sets default payment method id
     * @param paymentMethodId
     */
    void setPaymentMethodId(Long paymentMethodId);

    /**
     * Default billing account id
     * @return billingAccountId
     */
    Long getBillingAccountId();

    /**
     * Sets default billing account id
     * @param billingAccountId
     */
    void setBillingAccountId(Long billingAccountId);

    /**
     * Indicates that payment steps should not be displayed (e.g. customized payments used)
     * @return hidePayments
     */
    boolean isHidePayments();

    /**
     * Updates values with another payment agreement. This method is useful to initialize a payment agreement after create with default values.
     * @param other
     */
    void update(PaymentAgreement other);

    /**
     * Updates payment conditions.
     * @param hidePayments
     * @param downpaymentPercent
     * @param downpaymentTargetId
     * @param deliveryInvoicePercent
     * @param deliveryInvoiceTargetId
     * @param finalInvoiceTargetId
     * @param paymentCondition
     */
    void updatePaymentConditions(
            boolean hidePayments,
            Double downpaymentPercent,
            Long downpaymentTargetId,
            Double deliveryInvoicePercent,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            PaymentCondition paymentCondition);
    
    /**
     * Checks summary of downpayment, delivery and final invoice percent 
     * @return true if summary of percentage is 1
     */
    boolean isPaymentStepsValid();
}
