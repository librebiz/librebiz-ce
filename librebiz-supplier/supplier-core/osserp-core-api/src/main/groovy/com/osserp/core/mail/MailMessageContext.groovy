/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 23, 2013 8:10:26 PM 
 * 
 */
package com.osserp.core.mail

import com.osserp.common.mail.Mail

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class MailMessageContext implements Serializable {

    public static final String NAME = 'mailMessageContext'

    Mail mail
    MailTemplate mailTemplate
    List<MailMessageRecipient> recipientSuggestions = []
    List<MailMessageRecipient> addedSuggestions = []
    List<MailMessageRecipient> addedSuggestionsCC = []

    Boolean attachmentsEnabled = true
    Boolean recipientsDisabled
    Boolean recipientsCCDisabled
    Boolean recipientsBCCDisabled

    String viewHeader
    String exitTarget
    String successTarget

    String sendStatus
    String errorMessage
    String errorSource

    protected MailMessageContext() {
        super()
    }

    MailMessageContext(MailTemplate mailTemplate, Mail mail, Map<String, String> opts) {
        super()
        this.mail = mail
        this.mailTemplate = mailTemplate
        this.successTarget = opts.get('successTarget')
        this.exitTarget = opts.get('exitTarget')
        this.viewHeader = opts.get('viewHeader')
        if (mailTemplate != null) {
            attachmentsEnabled = mailTemplate.addAttachments
            recipientsDisabled = mailTemplate.fixedRecipients
            recipientsCCDisabled = mailTemplate.fixedRecipientsCC
            recipientsBCCDisabled = mailTemplate.fixedRecipientsBCC
        }
    }

    MailMessageContext(
        MailTemplate mailTemplate,
        Mail mail,
        Map<String, String> opts,
        List<MailMessageRecipient> recipientSuggestions) {
        this(mailTemplate, mail, opts)
        this.recipientSuggestions = recipientSuggestions
    }

    /**
     * Provides all mail recipients as comma separated list
     * @return all recipient list
     */
    String getAllRecipients() {
        StringBuilder buffer = new StringBuilder()
        mail.recipients.each {
            if (buffer.length() > 0) {
                buffer.append(', ').append(it)
            } else {
                buffer.append(it)
            }
        }
        mail.recipientsCC.each {
            if (buffer.length() > 0) {
                buffer.append(', ').append(it)
            } else {
                buffer.append(it)
            }
        }
        mail.recipientsBCC.each {
            if (buffer.length() > 0) {
                buffer.append(', ').append(it)
            } else {
                buffer.append(it)
            }
        }
        buffer.toString()
    }

    boolean addSuggestedRecipient(String emailAddress) {
        boolean added = false
        for (Iterator<MailMessageRecipient> i = recipientSuggestions.iterator(); i.hasNext();) {
            MailMessageRecipient rcpt = i.next()
            if (rcpt.email.equalsIgnoreCase(emailAddress)) {
                added = mail.addRecipient(emailAddress)
                addedSuggestions.add(rcpt)
                i.remove()
            }
        }
        return added
    }

    boolean addSuggestedRecipientCC(String emailAddress) {
        boolean added = false
        for (Iterator<MailMessageRecipient> i = recipientSuggestions.iterator(); i.hasNext();) {
            MailMessageRecipient rcpt = i.next()
            if (rcpt.email.equalsIgnoreCase(emailAddress)) {
                added = mail.addRecipientCC(emailAddress)
                addedSuggestionsCC.add(rcpt)
                i.remove()
            }
        }
        return added
    }
}
