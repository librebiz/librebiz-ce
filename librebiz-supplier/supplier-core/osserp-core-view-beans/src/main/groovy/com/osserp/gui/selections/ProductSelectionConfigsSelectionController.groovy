/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 15.04.2011 14:18:35 
 * 
 */
package com.osserp.gui.selections

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.AdminViewController

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
@RequestMapping("/selections/productSelectionConfigsSelection/*")
@Controller class ProductSelectionConfigsSelectionController extends AdminViewController {
    private static Logger logger = LoggerFactory.getLogger(ProductSelectionConfigsSelectionController.class.getName())

    @Override    
    @RequestMapping
    def disableEditMode(HttpServletRequest request) {
        logger.debug("disableEditMode: invoked [user=${getUserId(request)}]")
        ProductSelectionConfigsSelectionView view = getView(request)
        view.disableEditMode()
        if (view.editOnlyMode) {
            return executeExit(request)
        }
        return defaultPage
    }

    @RequestMapping
    def addSelection(HttpServletRequest request) {
        logger.debug("addSelection: invoked [user=${getUserId(request)}]")
        ProductSelectionConfigsSelectionView view = getView(request)
        view.addSelection()
        return defaultPage
    }

    @RequestMapping
    def removeSelection(HttpServletRequest request) {
        logger.debug("removeSelection: invoked [user=${getUserId(request)}]")
        ProductSelectionConfigsSelectionView view = getView(request)
        view.removeSelection()
        return defaultPage
    }

    @RequestMapping
    def addProduct(HttpServletRequest request) {
        logger.debug("addProduct: invoked [user=${getUserId(request)}]")
        ProductSelectionConfigsSelectionView view = getView(request)
        view.addProduct()
        return defaultPage
    }

    @RequestMapping
    def toggleExclusion(HttpServletRequest request) {
        logger.debug("toggleExclusion: invoked [user=${getUserId(request)}]")
        ProductSelectionConfigsSelectionView view = getView(request)
        view.toggleExclusion()
        return defaultPage
    }
}
