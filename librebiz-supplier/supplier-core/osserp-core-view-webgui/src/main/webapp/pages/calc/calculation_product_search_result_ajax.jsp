<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="calculator" scope="request" value="${sessionScope.calculationView.calculator}"/>
<c:set var="view" value="${sessionScope.calculationProductSearchView}"/>

<c:if test="${empty sessionScope.businessCaseView}">
    <o:logger write="business case view not bound!" level="warn"/>
</c:if>
<c:set var="plan" value="${sessionScope.businessCaseView.request}"/>
<c:set var="includeLazy" value="${!view.ignoreLazy}"/>
<o:form name="multipleItemListForm" url="/calculationProductSearch.do">
    <input type="hidden" name="method" value="save" />
    <div class="table-responsive table-responsive-default">
		<table class="table table-striped">
			<thead>
				<tr>
					<th class="productNumber">
						<a href="<c:url value="/calculationProductSearch.do?method=sort&order=byId" />"><fmt:message key="product"/></a>
					</th>
					<th class="productName">
						<a href="<c:url value="/calculationProductSearch.do?method=sort&order=byName" />"><fmt:message key="name"/></a>
					</th>
					<th class="productSalesOrders">
						<a href="<c:url value="/calculationProductSearch.do?method=sort&order=byOrdered" />" title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="order"/></a>
					</th>
					<th class="productStock">
						<a href="<c:url value="/calculationProductSearch.do?method=sort&order=byStock" />" title="<fmt:message key="currentlyInInventory"/>"><fmt:message key="inventory"/></a>
					</th>
					<th class="productPurchaseReceipts">
						<a href="<c:url value="/calculationProductSearch.do?method=sort&order=byReceipt" />" title="<fmt:message key="locatedInStockReceipt"/>"><fmt:message key="stockReceiptShort"/></a>
					</th>
					<th class="productPurchaseOrders">
						<a href="<c:url value="/calculationProductSearch.do?method=sort&order=byExpected" />" title="<fmt:message key="orderedBySupplier"/>"><fmt:message key="purchaseOrderShort"/></a>
					</th>
					<th class="productBasePrice" style="text-align: right;">
						<a href="<c:url value="/calculationProductSearch.do?method=sort&order=bySpecificPartnerPrice" />" title="<fmt:message key="partnerPrice"/>"><span style="margin-right: 2px;"><fmt:message key="partnerPriceShort"/></span></a>
					</th>
					<th class="productQuantity" style="text-align: right;">
						<span style="margin-right: 2px;"><fmt:message key="quantity"/></span>
					</th>
					<th class="productPrice" style="text-align: right;">
						<span style="margin-right: 2px;"><fmt:message key="price"/></span>
					</th>
					<th class="productAction"> </th>
				</tr>
			</thead>
			<tbody style="<c:choose><c:when test="${view.productSelectionConfigsMode}">height:390px;</c:when><c:otherwise>height:350px;</c:otherwise></c:choose>">
				<c:forEach var="product" items="${view.list}" varStatus="s">
					<c:if test="${includeLazy or !product.lazy}">
						<tr id="product${product.productId}">
							<td class="productNumber">
								<a href="<c:url value="/products.do?method=load&id=${product.productId}&exit=calculationProductSearchRedisplay&exitId=product${product.productId}"/>">
									<o:out value="${product.productId}"/><input type="hidden" name="productId" value="<o:out value="${product.productId}"/>" />
								</a>
							</td>
							<td class="productName">
								<c:choose>
									<c:when test="${empty sessionScope.openSalesOrdersView}">
										<a href="<c:url value="/calculations.do?method=forwardProductPlanning&id=${product.productId}&exit=calculationProductSearchRedisplay&exitId=product${product.productId}"/>" title="<fmt:message key="deliveryStatus"/>">
											<o:out value="${product.name}" limit="50"/>
										</a>
									</c:when>
									<c:otherwise>
										<o:out value="${product.name}" limit="50"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td class="productSalesOrders"><span title="<fmt:message key="currentOrdersExclusiveThis"/>"><o:number value="${product.summary.ordered}" format="integer"/></span></td>
							<td class="productStock"><span title="<fmt:message key="currentOnStock"/>"><o:number value="${product.summary.stock}" format="integer"/></span></td>
							<td class="productPurchaseReceipts"><span title="<fmt:message key="currentPurchaseDelivery"/>"><o:number value="${product.summary.receipt}" format="integer"/></span></td>
							<td class="productPurchaseOrders"><span title="<fmt:message key="currentPurchaseOrders"/>"><o:number value="${product.summary.expected}" format="integer"/></span></td>
							<td class="productBasePrice">
								<c:choose>
									<c:when test="${product.specificPartnerPriceAvailable}">
										<span title="<fmt:formatNumber value="${product.partnerPrice}" maxFractionDigits="2" minFractionDigits="2"/>/<fmt:message key="piece"/>">
											<fmt:formatNumber value="${product.specificPartnerPrice}" maxFractionDigits="2" minFractionDigits="2"/>/W
										</span>
									</c:when>
									<c:otherwise>
										<fmt:formatNumber value="${product.partnerPrice}" maxFractionDigits="2" minFractionDigits="2"/>/<fmt:message key="piece"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td class="productQuantity">
								<input type="text" class="form-control productQuantityInput" name="quantity" value="0" />
							</td>
							<td class="productPrice">
								<input type="text" class="form-control productPriceInput" name="price" value="<oc:defaultPrice product="${product}" contact="${plan.customer}" method="${view.productPriceMethod}" missing="${view.productPriceDefault}" />"/>
							</td>
							<td class="productAction">
								<input type="image" src="<c:url value="${applicationScope.saveIcon}"/>" />
							</td>
						</tr>
						<tr>
							<td class="productNumber">&nbsp;</td>
							<td colspan="8" class="productNote">
								<input type="text" class="form-control note" name="note" value=""/>
                                <input type="hidden" name="customName" value=""/>
							</td>
							<td class="productAction">&nbsp;</td>
						</tr>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
    </div>
</o:form>
