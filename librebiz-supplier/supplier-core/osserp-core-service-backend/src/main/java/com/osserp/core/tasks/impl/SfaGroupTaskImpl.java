/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 4, 2010 3:27:09 PM 
 * 
 */
package com.osserp.core.tasks.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.impl.AbstractObjectMessageHandler;
import com.osserp.common.util.NumberUtil;
import com.osserp.core.dao.telephone.SfaTelephoneGroups;
import com.osserp.core.telephone.TelephoneConfiguration;
import com.osserp.core.telephone.TelephoneConfigurationManager;
import com.osserp.core.telephone.TelephoneGroup;
import com.osserp.core.telephone.TelephoneGroupManager;
import com.osserp.core.tasks.Task;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class SfaGroupTaskImpl extends AbstractObjectMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(SfaGroupTaskImpl.class.getName());
    private SfaTelephoneGroups sfaTelephoneGroups = null;
    private TelephoneGroupManager telephoneGroupManager = null;
    private TelephoneConfigurationManager telephoneConfigurationManager = null;

    protected SfaGroupTaskImpl(
            SfaTelephoneGroups sfaTelephoneGroups,
            TelephoneConfigurationManager telephoneConfigurationManager,
            TelephoneGroupManager telephoneGroupManager) {
        super();
        this.sfaTelephoneGroups = sfaTelephoneGroups;
        this.telephoneConfigurationManager = telephoneConfigurationManager;
        this.telephoneGroupManager = telephoneGroupManager;
    }

    @Override
    protected void execute(Object object, byte[] file) {
        if (object instanceof String) {
            long start = System.currentTimeMillis();
            Long id = NumberUtil.createLong((String) object);
            if (id != null) {
                TelephoneGroup group = telephoneGroupManager.find(id);
                if (group != null) {
                    if (group.isEndOfLife()) {
                        List<TelephoneConfiguration> list = telephoneConfigurationManager.findByTelephoneGroup(group.getId());
                        for (Iterator<TelephoneConfiguration> iterator = list.iterator(); iterator.hasNext();) {
                            TelephoneConfiguration tc = iterator.next();
                            this.telephoneConfigurationManager.updateTelephoneGroup(group.getChangedBy(), tc.getId(), group.getId());
                        }
                    }
                    this.sfaTelephoneGroups.synchronize(group);
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("execute() done by common telephone group [id=" + id
                        + ", duration=" + (System.currentTimeMillis() - start) + "ms]");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("execute() object not allocated");
            }
        }
    }
}
