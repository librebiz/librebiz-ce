/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.ods

import groovy.sql.Sql

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dao.DataSourceProvider
import com.osserp.common.ds.ImportException
import com.osserp.common.ds.ImportFile

/**
 * Provides an importer instance using a data source to import a row.  
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractDatabaseImporter extends OdsImporter {
    private static Logger logger = LoggerFactory.getLogger(AbstractDatabaseImporter.class.getName())

    private static final int MAX_EMPTY_ROWS = 3
    DataSourceProvider dataSource
    String filename
    ImportFile file
    int maxEmpty = MAX_EMPTY_ROWS
    int lastEmpty = 0

    /**
     * Creates a new importer implementation
     * @param dataSourceProvider of target database
     * @param directory of import files, may be null if file consists directory path
     * @param file as name or path + name if directory is null
     * @param sheetProvidesHeader
     */
    AbstractDatabaseImporter(DataSourceProvider dataSourceProvider, String directory, String file, Boolean sheetProvidesHeader)
        throws ImportException {
        super(sheetProvidesHeader, null, directory, false, (directory != null))
        dataSource = dataSourceProvider
        filename = file
    }

    /**
     * Analyzes imported row and creates the final value map to run importRow with.
     * This method may create or initialize dependent objects, remove invalid row, etc.
     * The default implementation does nothing and returns the row as provided.
     * Override to implement required tasks needed by your implementation.
     * @param sql as provided by dataSourceProvider
     * @param row
     * @return final map of row to invoke importRow with
     */
    protected Map<String, String> initRow(Sql sql, Map<String, String> row) {
        row
    }

    /**
     * Imports a mapped row to the database, Override to implement your logic.
     * @param sql as provided by dataSourceProvider
     * @param row
     * @return true if import row was successful
     */
    abstract protected boolean importRow(Sql sql, Map<String, String> row);

    /**
     * Imports a sheet from file and invokes importRow on each row map.
     * @return count of rows for whose importRow returns true
     */
    Integer run() throws ImportException {
        Sql sql = createSql()
        Integer result = 0
        int ires = 0
        while (ires >= 0) {
            ires = importNext(sql)
            if (ires > 0) {
                result++
            }
            if (lastEmpty > maxEmpty) {
                logger.debug("run: interrupt of loop caused by a sequence of ${lastEmpty} empty rows")
                ires = -1
            }
        }
        result
    }

    protected int importNext(Sql sql) {
        if (!file) {
            long start = System.currentTimeMillis()
            file = createImportFile(createFile(filename))
            logger.debug("importNext: import file created [duration=${(System.currentTimeMillis() - start)}ms]")
        }
        Map<String, String> row = file.getNextMap()
        if (!row) {
            // eof
            return -1
        }
        row = initRow(sql, row)
        if (row && importRow(sql, row)) {
            return 1
        }
        return 0
    }

    protected Sql createSql() {
        dataSource.getSql()
    }
}

