<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.productPriceAwareView}"/>
<c:set var="product" value="${view.product}"/>

<style type="text/css">

	.col1 {
		width: 187px;
	}

	.col2,
	.col3,
	.col4 {
		width: 186px;
	}

</style>

<o:permission role="purchase_price_view,purchase_price_change" info="permissionPurchasePriceView">
	<table class="table table-striped">
		<thead>
			<tr>

				<th style="width: 778px;" colspan="4"><fmt:message key="purchasingPrices"/></th>
			</tr>
		</thead>
		<tbody>

			<tr>
				<td class="col1"><fmt:message key="purchasePriceBase"/></td>
				<td class="right col2"><o:number value="${product.estimatedPurchasePrice}" format="currency" /></td>
				<c:choose>
					<c:when test="${view.valuationMode}">
						<td class="right col3">
							<fmt:message key="newProductValuation"/>
						</td>
						<td class="right col4">
							<v:form name="productPriceForm" url="${view.baseLink}/updateValuation">
								<input type="text" class="number" id="valuationInput" name="value" value="<o:number value="${product.summary.lastPurchasePrice}" format="currency"/>" style="text-align: right;"/>
								<input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="updateValuation" onclick="setMethod('updateValuation');" style="margin-left: 5px; margin-right: 5px;" class="bigicon" title="<fmt:message key="saveInput"/>"/>
							</v:form>
						</td>
					</c:when>
					<c:otherwise>
						<td class="right col3">
							<o:roles>
								<o:when role="product_valuation_change" info="permissionProductValuationChange">
									<a href="<v:url value="${view.baseLink}/enableValuationMode"/>" title="<fmt:message key="permissionProductValuationChange"/>">
										<fmt:message key="purchasePriceLast"/>
									</a>
								</o:when>
								<o:otherwise>
									<fmt:message key="purchasePriceLast"/>
								</o:otherwise>
							</o:roles>
						</td>
						<td class="right col4"><o:number value="${product.summary.lastPurchasePrice}" format="currency" /></td>
					</c:otherwise>
				</c:choose>
			</tr>
			<tr>
				<td colspan="2">
					<o:permission role="product_pp_ignore_change" info="permissionPriceUnderPurchasePriceInput">
						<a href="<v:url value="${view.baseLink}/changePurchasePriceLimitFlag"/>">
							<c:choose>
								<c:when test="${product.ignorePurchasePriceLimit}">
									<fmt:message key="salesPriceUnderPurchasePriceInputEnabled"/>
								</c:when>
								<c:otherwise>
									<fmt:message key="salesPriceUnderPurchasePriceInputDisabled"/>
								</c:otherwise>
							</c:choose>
						</a>
					</o:permission>
					<o:forbidden role="product_pp_ignore_change">
						<c:choose>
							<c:when test="${product.ignorePurchasePriceLimit}">
								<fmt:message key="salesPriceUnderPurchasePriceInputEnabled"/>
							</c:when>
							<c:otherwise>
								<fmt:message key="salesPriceUnderPurchasePriceInputDisabled"/>
							</c:otherwise>
						</c:choose>
					</o:forbidden>
				</td>
				<td class="right col3"><fmt:message key="purchasePriceAverage"/></td>
				<td class="right col4"><o:number value="${product.summary.averagePurchasePrice}" format="currency" /></td>
			</tr>
		</tbody>
	</table>
</o:permission>
