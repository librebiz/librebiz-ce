/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Dec-2006 09:19:41 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.planning.SalesPlanningSummary;
import com.osserp.core.products.Product;
import com.osserp.core.sales.SalesDeliveryNoteManager;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;
import com.osserp.core.views.OrderItemsDisplay;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.sales.UnreleasedDeliveriesView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("unreleasedDeliveriesView")
public class UnreleasedDeliveriesViewImpl extends AbstractView
        implements UnreleasedDeliveriesView {

    private Product product = null;
    private List<SalesPlanningSummary> summary = null;
    private boolean summaryMode = false;

    protected UnreleasedDeliveriesViewImpl() {
        super();
    }

    public boolean isProductMode() {
        return (product != null);
    }

    public Product getProduct() {
        return product;
    }

    public boolean isSummaryMode() {
        return summaryMode;
    }

    public void changeSummaryMode() {
        this.summaryMode = !this.summaryMode;
    }

    public List<SalesPlanningSummary> getSummary() {
        return summary;
    }

    public void refresh() throws PermissionException {
        List<OrderItemsDisplay> list = null;
        if (product == null) {
            DomainUser user = getDomainUser();
            Long stockId = getSystemConfigManager().getDefaultStock().getId();
            if (user.isProjectManager()
                    && !user.isExecutive()) {
                list = getDeliveryNoteManager().getUnreleasedItemsByManager(user.getEmployee(), stockId);
            } else {
                list = getDeliveryNoteManager().getUnreleasedItemsByManager(null, stockId);
            }
        } else {
            list = getDeliveryNoteManager().getUnreleasedItems(product);
        }
        DomainUser domainUser = getDomainUser();
        for (Iterator<OrderItemsDisplay> i = list.iterator(); i.hasNext();) {
            OrderItemsDisplay next = i.next();
            if (isSet(next.getBranchId())) {
                BranchOffice office = fetchBranchOffice(next.getBranchId());
                if (!domainUser.isBranchAccessible(office)) {
                    i.remove();
                }
            }
        }
        
        setList(list);
        summary = new ArrayList<SalesPlanningSummary>();
        Set<Long> summaryContent = new HashSet<Long>();
        for (int i = 0, j = list.size(); i < j; i++) {
            OrderItemsDisplay next = list.get(i);
            if (summaryContent.contains(next.getProductId())) {
                updateExistingSummary(next);
            } else {
                summary.add(new SalesPlanningSummary(
                        next.getStockId(),
                        next.getProductId(),
                        next.getProductGroup(),
                        next.getProductName(),
                        next.getOutstanding(),
                        next.getVacantOrdered(),
                        next.getStock(),
                        next.getReceipt(),
                        next.getExpected(),
                        next.getSalesReceipt()));
                summaryContent.add(next.getProductId());
            }
        }
    }

    public void refresh(Product product) throws PermissionException {
        this.product = product;
        refresh();
    }

    private void updateExistingSummary(OrderItemsDisplay next) {
        for (int i = 0, j = summary.size(); i < j; i++) {
            SalesPlanningSummary existing = summary.get(i);
            if (existing.getProductId().equals(next.getProductId())) {
                existing.addQuantity(next.getOutstanding());
                break;
            }
        }
    }

    private SalesDeliveryNoteManager getDeliveryNoteManager() {
        return (SalesDeliveryNoteManager) getService(SalesDeliveryNoteManager.class.getName());
    }

    @Override
    public void save(Form fh) throws ClientException, PermissionException {
        // Nothing to do here, bean is readonly
    }
}
