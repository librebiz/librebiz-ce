<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.stocktakingView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="view" value="${sessionScope.stocktakingView}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title">
    <o:displayTitle />
</tiles:put>
<tiles:put name="javascript" type="string">
<script type="text/javascript">
function forwardList(linkUrl) {
  var cnt = document.getElementById('count').value;
  gotoUrl(linkUrl + cnt);
}
</script>
</tiles:put>
<c:if test="${view.productEditMode}">
<tiles:put name="styles" type="string">
<style type="text/css">
input .count {
  text-align: right;
  width: 75px;
}
</style>
</tiles:put>
</c:if>
<tiles:put name="headline">
    <fmt:message key="stockTaking" />
    <o:out value="${view.selectedCompany.contact.displayName}" />
</tiles:put>

<tiles:put name="headline_right">
    <ul>
        <c:choose>
            <c:when test="${view.editMode}">
                <li><a href="<c:url value="/stocktakings.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                <li><a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteEntry"/>','<c:url value="/stocktakings.do?method=delete"/>');" title="<fmt:message key="delete"/>"><o:img name="deleteIcon"/></a></li>
            </c:when>
            <c:when test="${view.createMode}">
                <li><a href="<c:url value="/stocktakings.do?method=disableCreate"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
            </c:when>
            <c:when test="${view.productEditMode}">
                <li><a href="<c:url value="/stocktakings.do?method=lastList"/>"><o:img name="leftIcon" /></a></li>
                <li><a href="<c:url value="/stocktakings.do?method=nextList"/>"><o:img name="rightIcon" /></a></li>
                <c:if test="${!view.bean.closed}">
                    <li><a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmInitializeExpectedAsCountedForThisList"/>','<c:url value="/stocktakings.do?method=synchronizeQuantity"/>');" title="<fmt:message key="initializeCountedWithExpected"/>"><o:img name="moveIcon" /></a></li>
                </c:if>
                <li><a href="<c:url value="/stocktakings.do?method=exitProducts"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
            </c:when>
            <c:otherwise>
                <c:choose>
                    <c:when test="${!empty view.bean}">
                        <li><a href="<c:url value="/stocktakings.do?method=select"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="<c:url value="/stocktakings.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                    </c:otherwise>
                </c:choose>
                <c:if test="${!empty view.bean and !view.bean.closed}">
                    <c:if test="${view.bean.closeable}">
                        <li><a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmStocktakingClose"/>','<c:url value="/stocktakings.do?method=close"/>');" title="<fmt:message key="stockTakingClose"/>"><o:img name="enabledIcon" /></a></li>
                    </c:if>
                    <li>
                        <v:link url="/records/stocktakingPdf/print?name=stocktaking_list" title="printCountingList">
                            <o:img name="printIcon" />
                        </v:link>
                    </li>
                </c:if>
                <c:if test="${!empty view.bean and view.bean.closed}">
                    <li>
                        <v:link url="/records/stocktakingPdf/print?name=stocktaking_protocol" title="printProtocol">
                            <o:img name="printIcon" />
                        </v:link>
                    </li>
                </c:if>
                <c:if test="${empty view.bean and !empty view.list}">
                    <li><a href="<c:url value="/stocktakings.do?method=enableCreate"/>" title="<fmt:message key="createNew"/>"><o:img name="newdataIcon" /></a></li>
                </c:if>
            </c:otherwise>
        </c:choose>
        <li>
            <v:link url="/index" title="backToMenu">
                <o:img name="homeIcon" />
            </v:link>
        </li>
    </ul>
</tiles:put>

<tiles:put name="content" type="string">
    <div class="content-area" id="stocktakingContent">
        <div class="row">
            <c:choose>
                <c:when test="${view.createMode}">
                    <c:import url="_stocktakingCreate.jsp"/>
                </c:when>
                <c:when test="${view.editMode}">
                    <c:import url="_stocktakingEdit.jsp"/>
                </c:when>
                <c:when test="${view.productEditMode}">
                    <c:import url="_stocktakingProductList.jsp"/>
                </c:when>
                <c:otherwise>
                    <c:import url="_stocktakingDisplay.jsp"/>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</tiles:put>
</tiles:insert>
