/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 15-Oct-2006 11:37:00 
 * 
 */
package com.osserp.core.employees;

import java.util.List;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EmployeeGroup extends Option {

    static final String ACCOUNTING = "accounting"; 
    static final String CUSTOMER_SERVICE = "cs";
    static final String EXECUTIVE = "executive";
    static final String HRM = "hrm";
    static final String IT = "it";
    static final String LOGISTICS = "logistics"; 
    static final String OM = "om";
    static final String PURCHASING = "purchasing";
    static final String SALES = "sales";
    static final String TECHNICIAN = "technician";
    static final String WHOLESALE = "wholesale";


    /**
     * Provides the id of corresponding ldap group
     * @return ldapId
     */
    Long getLdapGroup();

    /**
     * Sets the ldap group id
     * @param ldapGroup
     */
    void setLdapGroup(Long ldapGroup);

    /**
     * Represents an internal employee group that is available in local database but should not be exported to any directory services (ex.: group 0, undefined)
     * @return ldapGroupIgnoreGid true if group should be ignored
     */
    boolean isLdapGroupIgnoreGid();

    /**
     * Enables/disables directory export
     * @param ldapGroupIgnoreGid
     */
    void setLdapGroupIgnoreGid(boolean ldapGroupIgnoreGid);

    /**
     * Indicates if group should be synchronized with groupware (requires enabled ldap sync too)
     * @return ldapGroupwareGroup true if group should be ignored
     */
    boolean isLdapGroupwareGroup();

    /**
     * Enables/disables groupware export
     * @param ldapGroupwareGroup
     */
    void setLdapGroupwareGroup(boolean ldapGroupwareGroup);

    /**
     * Provides the short key of the group
     * @return key
     */
    String getKey();

    /**
     * Sets the short key
     * @param key
     */
    void setKey(String key);

    /**
     * Indicates that group is flow control aware
     * @return flowControlAware
     */
    boolean isFlowControlAware();

    /**
     * Enables/disables flow control aware
     * @param flowControlAware
     */
    void setFlowControlAware(boolean flowControlAware);

    /**
     * Indicates that group is branch only (e.g. not directly assigned)
     * @return branch only
     */
    boolean isBranchOnly();

    /**
     * Sets that group is branch related only
     * @param branchOnly
     */
    void setBranchOnly(boolean branchOnly);

    /**
     * Indicates that members of that group may ignore branch settings
     * @return ignoringBranch
     */
    boolean isIgnoringBranch();

    /**
     * Enables/disables ignoring branch flag
     * @param ignoringBranch
     */
    void setIgnoringBranch(boolean ignoringBranch);

    /**
     * Treat members of this group as application admin 
     * @return true if user is an application admin 
     */
    boolean isApplicationAdmin();

    /**
     * Indicates that role is executive of all companies
     * @return executive
     */
    boolean isExecutive();

    /**
     * Enables/disables executive flag
     * @param executive
     */
    void setExecutive(boolean executive);

    /**
     * Indicates that role is executive for a company
     * @return executiveCompany
     */
    boolean isExecutiveCompany();

    /**
     * Enables/disables executiveCompany flag
     * @param executiveCompany
     */
    void setExecutiveCompany(boolean executiveCompany);

    /**
     * Indicates that role is logistics
     * @return logistics
     */
    boolean isLogistics();

    /**
     * Enables/disables logistics flag
     * @param logistics
     */
    void setLogistics(boolean logistics);

    /**
     * Indicates that role is sales
     * @return sales
     */
    boolean isSales();

    /**
     * Enables/disables sales flag
     * @param sales
     */
    void setSales(boolean sales);

    /**
     * Indicates that role is technician
     * @return technician
     */
    boolean isTechnician();

    /**
     * Enables/disables technician flag
     * @param technician
     */
    void setTechnician(boolean technician);

    /**
     * Indicates that role is it
     * @return it
     */
    boolean isIt();

    /**
     * Enables/disables it flag
     * @param it
     */
    void setIt(boolean it);

    /**
     * Indicates that role is accounting
     * @return accounting
     */
    boolean isAccounting();

    /**
     * Enables/disables accounting flag
     * @param accounting
     */
    void setAccounting(boolean accounting);

    /**
     * Indicates that role is customer service
     * @return cs
     */
    boolean isCs();

    /**
     * Enables/disables customer service flag
     * @param cs
     */
    void setCs(boolean cs);

    /**
     * Indicates that role is hrm
     * @return hrm
     */
    boolean isHrm();

    /**
     * Enables/disables hrm flag
     * @param hrm
     */
    void setHrm(boolean hrm);

    /**
     * Indicates that role installation
     * @return installation
     */
    boolean isInstallation();

    /**
     * Enables/disables installation flag
     * @param installation
     */
    void setInstallation(boolean installation);

    /**
     * Indicates that role is purchasing
     * @return purchasing
     */
    boolean isPurchasing();

    /**
     * Enables/disables purchasing flag
     * @param purchasing
     */
    void setPurchasing(boolean purchasing);

    /**
     * Indicates that role is wholesale
     * @return wholesale
     */
    boolean isWholesale();

    /**
     * Enables/disables wholesale flag
     * @param wholesale
     */
    void setWholesale(boolean wholesale);

    /**
     * Indicates that role is office managment
     * @return om
     */
    boolean isOm();

    /**
     * Enables/disables office management flag
     * @param om
     */
    void setOm(boolean om);

    /**
     * Provides the default permission for members of group
     * @return permissions
     */
    List<Option> getPermissions();

    /**
     * Adds a permission
     * @param user adding permission
     * @param name of permission
     */
    void addPermission(Employee user, String name);

    /**
     * Removes a permission
     * @param permission
     */
    void removePermission(String permission);

    /**
     * Indicates that permission is grant
     * @param permission
     * @return true if permission grant
     */
    boolean isPermissionGrant(String permission);

}
