/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.common.PermissionException
import com.osserp.groovy.web.ViewController

/**
 * Display all telephone calls
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/telephones/telephoneCall/*")
@Controller class TelephoneCallController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(TelephoneCallController.class.getName())

    @RequestMapping
    @Override
    def forward(HttpServletRequest request) {
        logger.debug("forward: invoked [user=${getUserId(request)}]")
        TelephoneCallView view = getView(request)
        try {
            view?.reload()
        } catch (ClientException t) {
            if (logger.debugEnabled) {
                logger.debug("search: caught exception [message=${t.message}]")
            }
            request.getSession().setAttribute('error', t.message)
        }
        return defaultPage
    }

    @RequestMapping
    def search(HttpServletRequest request) {
        logger.debug('search: invoked')
        TelephoneCallView view = getView(request)
        try {
            view?.search()
        } catch (PermissionException t) {
            if (logger.debugEnabled) {
                logger.debug("search: caught exception [message=${t.message}]")
            }
            request.getSession().setAttribute('error', t.message)
        } catch (ClientException t) {
            if (logger.debugEnabled) {
                logger.debug("search: caught exception [message=${t.message}]")
            }
            request.getSession().setAttribute('error', t.message)
        }

        return pageArea
    }

    @RequestMapping
    @Override
    def sort(HttpServletRequest request) {
        logger.debug("sort: invoked [user=${getUserId(request)}]")
        TelephoneCallView view = getView(request)
        view?.sort()
        return pageArea
    }
}
