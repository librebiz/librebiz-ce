/**
 *
 * Copyright (C) 2012 The original author or authors.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.core.api

import com.osserp.common.OptionsCache
import com.osserp.api.WsContact
import com.osserp.api.WSContact
import com.osserp.api.WSContactService
import com.osserp.api.WSObjectException
import com.osserp.core.contacts.Contact
import com.osserp.core.dao.Contacts
import com.osserp.core.dao.SystemConfigs

/**
 * 
 * @author rk <rk@osserp.com>
 * 
 */
class WsContactServiceImpl implements WSContactService {

    private SystemConfigs systemConfigs
    private Contacts contacts
    private WsContactMapper wsContactMapper
    private CoreContactService coreContactService 

    /**
     * Creates a new contact webservice    
     * @param systemConfigs
     * @param optionsCache
     * @param contacts
     * @param wsContactMapper
     * @param coreContactService
     */
    protected WsContactServiceImpl(
            SystemConfigs systemConfigs, 
            Contacts contacts, 
            WsContactMapper wsContactMapper, 
            CoreContactService coreContactService) {
        super()
        this.systemConfigs = systemConfigs
        this.contacts = contacts
        this.wsContactMapper = wsContactMapper
        this.coreContactService = coreContactService
    }

    WSContact fetchContactByEmail(String email) {
        WsContact result
        List<Contact> found = contacts.findByEmail(email)
        for (int i = 0; i < found.size(); i++) {
            Contact next = found.get(i)
            if (next.hasEmail(email)) {
                result = wsContactMapper.map(next)
                break 
            }
        }
        result
    }

    WSContact fetchContactById(Long contactId) {
        Contact found = contacts.fetchContact(contactId)
        !found ? null : wsContactMapper.map(found)
    }

    WSContact getContactById(Long contactId) throws WSObjectException {
        Contact found = contacts.fetchContact(contactId)
        if (!found) {
            throw new WSObjectException("contact.id ${contactId} not exists")
        }
        wsContactMapper.map(found)
    }

    WSContact createOrUpdate(WSContact wsContact) {
        WsContact result = wsContact
        if (systemConfigs.isSystemPropertyEnabled('wsContactReceiptEnabled') && coreContactService) {
            Contact createdOrUpdated = coreContactService.createOrUpdate(wsContact)
            if (createdOrUpdated) {
                result = wsContactMapper.map(createdOrUpdated)
            }
        }
        result
    }

}
