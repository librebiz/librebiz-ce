/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Jan-2007 12:57:21 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.Records;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.PaymentAgreementAwareRecord;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.finance.RecordStatusHistory;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Stock;
import com.osserp.core.model.records.ItemChangedInfoVO;
import com.osserp.core.products.Product;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecordManager extends AbstractFinanceRecordManager implements RecordManager {
    private static Logger log = LoggerFactory.getLogger(AbstractRecordManager.class.getName());

    private ProductPlanningCacheSender productPlanningTask = null;
    private Products products = null;
    private Records records = null;
    private SystemConfigManager systemConfigManager = null;

    protected AbstractRecordManager(
            Records records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask) {
        super(records);
        this.records = records;
        this.products = products;
        this.productPlanningTask = productPlanningTask;
        this.systemConfigManager = systemConfigManager;
    }

    public RecordType getRecordType() {
        return records.getRecordType();
    }

    public final boolean exists(Long id) {
        return records.exists(id);
    }

    public final Record find(Long recordId) {
        try {
            return load(recordId);
        } catch (Exception e) {
            log.warn("find() failed [id=" + recordId + ", message=" + e.getMessage() + "]");
        }
        return null;
    }

    public final List<Record> getUnreleased() {
        return records.getUnreleased();
    }

    public final List<RecordDisplay> findBySales(Long salesId) {
        return records.getBySalesId(salesId);
    }

    public final List<Record> getOpen(boolean descending) {
        return records.getOpen(descending);
    }

    public final Record changeContact(Employee user, Record record, ClassifiedContact contact) throws ClientException {
        if (record.isUnchangeable()) {
            throw new ClientException(ErrorCode.RECORD_UNCHANGEABLE);
        }
        return records.changeContact(user, record, contact);
    }

    public void changeCreatedDate(Record record, Date bookingDate) {
        record.updateCreatedDate(bookingDate);
        records.save(record);
        if (log.isDebugEnabled()) {
            log.debug("changeCreatedDate() done [record=" + record.getId()
                    + ", created=" + record.getCreated()
                    + ", initial=" + record.getInitialDate()
                    + "]");
        }
    }

    public boolean deletePermissionGrant(DomainUser user, Record record) {
        return records.deletePermissionGrant(user, record);
    }

    public void delete(Record record, Employee employee) throws ClientException {
        try {
            records.delete(record, employee);
        } catch (Throwable t) {
            log.warn("delete() caught exception [class=" + t.getClass().getName() + ", message=" + t.getMessage() + "]");
            throw new ClientException(ErrorCode.RECORD_DELETE_FAILED_UNKNOWN);
        }
    }

    public Record unlockItems(Employee user, Record record) throws ClientException {
        Record obj = records.load(record.getId());
        obj.unlockItems(user);
        records.save(obj);
        return obj;
    }

    public final void update(
            Record record,
            Long signatureLeft,
            Long signatureRight,
            Long personId,
            String note,
            boolean taxFree,
            Long taxFreeId,
            Double taxRate,
            Double reducedTaxRate,
            Long currency,
            String language,
            Long branchId,
            Long shippingId,
            String customHeader,
            boolean printComplimentaryClose,
            boolean printProjectmanager,
            boolean printSalesperson,
            boolean printBusinessId,
            boolean printBusinessInfo,
            boolean printRecordDate,
            boolean printRecordDateByStatus,
            boolean printPaymentTarget,
            boolean printConfirmationPlaceholder,
            boolean printCoverLetter) throws ClientException {

        boolean branchChanged = (record.getBranchId() != null
                && !record.getBranchId().equals(branchId));

        record.update(
                signatureLeft,
                signatureRight,
                personId,
                note,
                taxFree,
                taxFreeId,
                taxRate,
                reducedTaxRate,
                currency,
                language,
                branchId,
                shippingId,
                customHeader,
                printComplimentaryClose,
                printProjectmanager,
                printSalesperson,
                printBusinessId,
                printBusinessInfo,
                printRecordDate,
                printRecordDateByStatus,
                printPaymentTarget,
                printConfirmationPlaceholder,
                printCoverLetter);
        if (branchChanged) {
            updateStock(record);
        }
        records.save(record);
    }

    /**
     * Updates the payment agreement
     * @param record
     * @param hidePayments
     * @param amountsFixed
     * @param downpayment percent or fixed value
     * @param deliveryInvoice percent or fixed value
     * @param finalInvoice percent or fixed value
     * @param downpaymentTargetId
     * @param deliveryInvoiceTargetId
     * @param finalInvoiceTargetId
     * @param paymentConditionId
     * @param note
     * @param user
     * @throws ClientException if validation failed
     */
    protected void update(
            PaymentAgreementAwareRecord record,
            boolean hidePayments,
            boolean amountsFixed,
            Double downpayment,
            Double deliveryInvoice,
            Double finalInvoice,
            Long downpaymentTargetId,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            Long paymentConditionId,
            String note,
            Employee user)
            throws ClientException {

        if (log.isDebugEnabled()) {
            log.debug("update() invoked [record=" + record.getId()
                    + ", hidePayments=" + hidePayments
                    + ", amountsFixed=" + amountsFixed
                    + ", downpayment=" + downpayment
                    + ", deliveryInvoice=" + deliveryInvoice
                    + ", finalInvoice=" + finalInvoice
                    + ", downpaymentTargetId=" + downpaymentTargetId
                    + ", deliveryInvoiceTargetId=" + deliveryInvoiceTargetId
                    + ", finalInvoiceTargetId=" + finalInvoiceTargetId
                    + ", paymentConditionId=" + paymentConditionId
                    + ", note=" + note
                    + ", user=" + (user != null ? user.getId() : null)
                    + "]");
        }
        record.updatePaymentAgreement(
                hidePayments,
                amountsFixed,
                downpayment,
                deliveryInvoice,
                finalInvoice,
                downpaymentTargetId,
                deliveryInvoiceTargetId,
                finalInvoiceTargetId,
                getPaymentCondition(record, paymentConditionId),
                note,
                (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
        records.save(record);
    }

    public final PaymentCondition getPaymentCondition(PaymentAgreementAwareRecord record, Long id) {
        if (id == null
                && record.getPaymentAgreement() != null
                && record.getPaymentAgreement().getPaymentCondition() != null) {
            return record.getPaymentAgreement().getPaymentCondition();
        }
        if (id == null) {
            return null;
        }
        return records.loadPaymentCondition(id);
    }

    /**
     * Indicates that given record is just released, e.g. <br/>
     * record.status == Record.STAT_SENT && oldStatus < Record.STAT_SENT
     * @param record
     * @param oldStatus
     * @return true if just released
     */
    protected final boolean justReleased(Record record, Long oldStatus) {
        long oldStat = (oldStatus == null ? 0 : oldStatus.longValue());
        return (oldStat < Record.STAT_SENT && Record.STAT_SENT.equals(record.getStatus()));
    }

    public void resetStatus(Record record, Employee employee) throws ClientException {
        if (record.isUnchangeable()) {
            record.markChanged();
            records.save(record);
            records.createStatusHistory(record, Record.STAT_CHANGED, employee.getId());
            createProductPlanningRefreshTask(record, null);
        }
    }

    public void updateStatus(Record record, Employee employee, Long newStatus)
            throws ClientException {
        Long oldStatus = record.getStatus();
        record.updateStatus(newStatus);
        records.save(record);
        records.createStatusHistory(record, newStatus, employee.getId());
        doAfterStatusUpdate(record, employee, oldStatus);
        createProductPlanningRefreshTask(record, null);
    }

    protected void updateStock(Record record) {
        if (record.getType().isStockAware()) {
            Stock stock = getStockByRecord(record);
            if (stock != null) {
                for (Iterator<Item> items = record.getItems().iterator(); items.hasNext();) {
                    Item next = items.next();
                    if (next.getDelivered() == null || next.getDelivered() == 0) {
                        next.setStockId(stock.getId());
                    }
                }
            }
        }
    }

    public void updatePrintOptionDefaults(Employee user, Record record) {
    	if (log.isDebugEnabled()) {
    		log.debug("updatePrintOptionDefaults() invoked [user=" + user.getId() 
    				+ ", recordType=" + record.getType().getId() + "]");
    	}
    	records.updatePrintOptionDefaults(record);
	}

    public Record updateDeliveryAddress(Record record, BusinessCase businessCase)
        throws ClientException {
        assert (record != null && businessCase != null);
        if (businessCase.getAddress() != null) {
            record.updateDeliveryAddress(
                    (businessCase.getAddress().getName() != null) ?
                            businessCase.getAddress().getName() :
                                businessCase.getCustomer().getDisplayName(),
                    businessCase.getAddress().getStreet(),
                    businessCase.getAddress().getStreetAddon(),
                    businessCase.getAddress().getZipcode(),
                    businessCase.getAddress().getCity(),
                    businessCase.getAddress().getCountry());
            records.save(record);
        }
        return record;
    }

    public Record resetDeliveryAddress(Record record) {
        assert (record != null);
        record.resetDeliveryAddress();
        records.save(record);
        return record;
    }

	public final List<RecordStatusHistory> getStatusHistory(Record record) {
        return records.getStatusHistory(record.getId(), record.getType().getId());
    }

    public final List<BookingType> getBookingTypes() {
        return records.getBookingTypes();
    }

    public final BookingType getBookingType(Long id) {
        return records.getBookingType(id);
    }

    public final BookingType getDefaultBookingType() {
        return records.getDefaultBookingType();
    }

    public Stock getStockByRecord(Record record) {
        return records.getStockByRecord(record);
    }

    public ItemChangedInfo addItem(
            Employee user,
            Record record,
            Product product,
            Long stockId,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            String note) throws ClientException {
        return addItem(user, record, stockId, product, customName,
                quantity, taxRate, price, note);
    }

    protected final ItemChangedInfo addItem(
            Employee user, 
            Record record, 
            Long stockId, 
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price, 
            String note) {
        if (record == null) {
            throw new IllegalArgumentException("record must not be null");
        }
        if (product == null) {
            throw new IllegalArgumentException("product must not be null");
        }
        if (taxRate == null) {
            if (record.getAmounts() == null) {
                taxRate = 0d;
            } else if (product.isReducedTax()) {
                taxRate = record.getAmounts().getReducedTaxRate();
            } else {
                taxRate = record.getAmounts().getTaxRate();
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("addItem() invoked [user="
                    + (user == null ? "null" : user.getId())
                    + ", record=" + record.getId()
                    + ", product=" + product.getProductId()
                    + ", quantity=" + quantity
                    + ", price=" + price
                    + ", note=" + note
                    + ", partnerPrice=" + product.getPartnerPrice()
                    + ", lastPurchasePrice=" + (product.getSummary() == null ? "null" 
                            : product.getSummary().getLastPurchasePrice())
                    + ", taxRate=" + taxRate
                    + (isSet(customName) ? (", customName=" + customName + "]") : "]"));
        }
        
        record.addItem(
                stockId,
                product,
                customName,
                quantity,
                taxRate,
                price,
                record.getCreated(),
                new BigDecimal(product.getPartnerPrice()),
                false,
                false,
                new BigDecimal(product.getSummary().getLastPurchasePrice()),
                note,
                true,
                null); // externalId
        ItemChangedInfo info = new ItemChangedInfoVO(user, record.getItems().get(record.getItems().size() - 1), false);
        records.save(record);
        if (log.isDebugEnabled()) {
            log.debug("addItem() done [record=" + record.getId()
                    + ", stockId=" + stockId
                    + ", product=" + product.getProductId()
                    + ", quantity=" + quantity
                    + ", price=" + price
                    + "]");
        }
        return info;
    }

    public ItemChangedInfo deleteItem(Employee user, Record record, Item item) {
        if (log.isDebugEnabled()) {
            log.debug("deleteItem() invoked for record "
                    + record.getId() + ", item " + item.getId());
        }
        ItemChangedInfo info = new ItemChangedInfoVO(user, item, true);
        record.deleteItem(item);
        records.save(record);
        if (log.isDebugEnabled()) {
            log.debug("deleteItem() done");
        }
        return info;
    }

    public final Record deleteItems(Employee user, Record record) {
        int deleted = 0;
        for (Iterator<Item> i = record.getItems().iterator(); i.hasNext();) {
            Item current = i.next();
            if (deleteItemValidation(user, record, current)) {
                i.remove();
                deleted++;
            }
        }
        record.setChangedBy(user.getId());
        record.setChanged(new Date(System.currentTimeMillis()));
        records.save(record);
        if (log.isDebugEnabled()) {
            log.debug("deleteItems() done [record=" + record.getId()
                + ", count=" + deleted + "]");
        }
        return record;
    }

    protected boolean deleteItemValidation(Employee user, Record record, Item item) {
        return true;
    }

    public Record pasteItem(Employee user, Record record, Item item, Long itemId) {
        record.pasteItem(item, itemId);
        records.save(record);
        return load(record.getId());
    }

    public Record replaceItem(Employee user, Record record, Item item, Product product) {
        record.replaceItem(item, product);
        records.save(record);
        return load(record.getId());
    }

    public ItemChangedInfo updateItem(
            Employee user,
            Record record,
            Item item,
            String customName,
            Double quantity,
            BigDecimal price,
            Double taxRate,
            Long stockId,
            String note)
            throws ClientException {
        ItemChangedInfo info = new ItemChangedInfoVO(
                user,
                record.getId(),
                item.getProduct(),
                item.getStockId(),
                stockId,
                item.getQuantity(),
                quantity,
                item.getPrice(),
                price);
        record.updateItem(item, customName, quantity, price, taxRate, stockId, note);
        records.save(record);
        doAfterItemUpdate(record, info);
        createProductPlanningRefreshTask(record, null);
        return info;
    }

    public final List<ItemChangedInfo> getItemChangedInfos(Long recordId) {
        return records.getItemChangedInfos(recordId);
    }

    public void moveDownItem(Employee user, Record record, Long itemId) {
        record.moveDownItem(itemId);
        record.setChanged(new Date(System.currentTimeMillis()));
        record.setChangedBy(user.getId());
        getDao().save(record);
	}

	public void moveUpItem(Employee user, Record record, Long itemId) {
        record.moveUpItem(itemId);
        record.setChanged(new Date(System.currentTimeMillis()));
        record.setChangedBy(user.getId());
        getDao().save(record);
	}

    /**
     * Override method if additional action is required
     * @param record
     * @param result
     */
    protected void doAfterItemUpdate(Record record, ItemChangedInfo result) {
        // does nothing by default
    }

	public final void addInfo(Employee user, Record record, Option info) {
        record.addInfo(user, info);
        getDao().save(record);
    }

    public final void clearInfos(Record record) {
        record.clearInfos();
        records.save(record);
    }

    public final void setDefaultInfos(Employee user, Record record) {
        records.setDefaultInfos(user, record);
    }

    public final void moveInfoDown(Record record, RecordInfo info) {
    	record.moveInfoDown(info);
    	records.save(record);
    }

    public final void moveInfoUp(Record record, RecordInfo info) {
    	record.moveInfoUp(info);
    	records.save(record);
    }
    
    public final void removeInfo(Record record, RecordInfo info) {
        if (log.isDebugEnabled()) {
            log.debug("removeInfo() record " + record.getId()
                    + " supports infos");
        }
        record.removeInfo(info);
        records.save(record);
    }

    public final void addSerial(Employee user, Record record, Item item, String serialNumber, boolean ignoreAdditionalChecks)
            throws ClientException {
        if (!(record instanceof DeliveryNote)) {
            throw new BackendException("unsupported type "
                    + record.getClass().getName()
                    + " expected an instanceof DeliveryNote");
        }
        if (isNotSet(serialNumber)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        } else if (serialNumber.indexOf(",") > -1
                || serialNumber.indexOf(";") > -1
                || serialNumber.indexOf("/") > -1) {
            throw new ClientException(ErrorCode.VALUES_INVALID);
        }
        if (!ignoreAdditionalChecks) {
            performAdditionalSerialChecks(item.getProduct(), serialNumber);
        }
        DeliveryNote obj = (DeliveryNote) record;
        obj.addSerial(item, serialNumber.trim(), user);
        getDao().save(obj);
    }

    /**
     * Performs additional checks on serial numbers if required. Method is invoked by {@link #addSerial(Employee, Record, Item, String, boolean)}. <br/>
     * Override if implementing manager requires additional checks
     * @param product
     * @param serialNumber to check
     * @throws ClientException if additional steps failed
     */
    protected void performAdditionalSerialChecks(Product product, String serialNumber) throws ClientException {
        // default implementation doesn't provide additional checks 
    }
    
    public void validatePrint(Record record, Long nextStatus) throws ClientException {
        if (record != null) {
            if (log.isDebugEnabled()) {
                log.debug("validatePrint() invoked [record="
                        + record.getId() + "]");
            }
            if (record.getItems().isEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("validatePrint() missing items [record="
                            + record.getId() + "]");
                }
                throw new ClientException(ErrorCode.QUANTITY_MISSING);
            }
            for (int i = 0, j = record.getItems().size(); i < j; i++) {
                Item current = record.getItems().get(i);
                if (current.getQuantity() == null || current.getQuantity() == 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("validatePrint() missing quantity [item="
                                + current.getId() + ", product="
                                + current.getProduct().getProductId()
                                + "]");
                    }
                    throw new ClientException(ErrorCode.QUANTITY_MISSING);
                }
            }
            if (record.getBranchId() == null) {
                throw new ClientException(ErrorCode.BRANCH_MISSING);
            }
            List<BranchOffice> list = systemConfigManager.getBranchs();
            for (int i = 0, j = list.size(); i < j; i++) {
                BranchOffice next = list.get(i);
                if (next.getId().equals(record.getBranchId())) {
                    if (!record.getCompany().equals(next.getCompany().getId())) {
                        throw new ClientException(ErrorCode.BRANCH_INVALID);
                    }
                }
            }
            if (record instanceof PaymentAgreementAwareRecord && 
                    !(record instanceof CreditNote)) {
                PaymentAgreementAwareRecord par = (PaymentAgreementAwareRecord) record;
                
                if (log.isDebugEnabled()) {
                    log.debug("validatePrint() paymentAgreementAware record found [id=" + record.getId()
                            + ", agreementAvailable=" + (par.getPaymentAgreement() != null) 
                            + ", conditionAvailable=" 
                            + (par.getPaymentAgreement() != null && par.getPaymentAgreement().getPaymentCondition() != null) 
                            + ", class=" + par.getClass().getName() 
                            + "]");
                }
                if (par.getPaymentAgreement() == null) {
                    
                    throw new ClientException(ErrorCode.PAYMENT_AGREEMENT_MISSING);
                }
                if (par.getPaymentAgreement().getPaymentCondition() == null) {
                    throw new ClientException(ErrorCode.PAYMENT_CONDITION_MISSING);
                }
            }
            doAfterPrintValidation(record, nextStatus);
            if (log.isDebugEnabled()) {
                log.debug("validatePrint() done [record=" + record.getId() + "]");
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordManager#load(java.lang.Long)
     */
    public Record load(Long recordId) {
        return records.load(recordId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.RecordManager#persist(com.osserp.core.finance.Record)
     */
    public final void persist(Record record) {
        records.save(record);
    }

    /**
     * Override this to perform tasks after a status update
     * @param record
     * @param employee
     * @param oldStatus
     */
    protected void doAfterStatusUpdate(Record record, Employee employee, Long oldStatus) {
        // we do nothing here
    }

    /**
     * Override to perform type specific validations
     * @param record
     * @param nextStatus
     * @throws ClientException
     */
    protected void doAfterPrintValidation(Record record, Long nextStatus) throws ClientException {
        // default does nothing
    }

    /**
     * Provides products dao
     * @return products
     */
    protected final Products getProducts() {
        return products;
    }

    /**
     * Provides the implementing data access object
     * @return dao
     */
    protected final Records getDao() {
        return records;
    }

    protected void reloadSummary(Record record) {
        if (products != null) {
            record.getItems().forEach(item -> {
                products.loadSummary(item.getProduct());
            });
        }
    }

    /**
     * Creates a new product planning refresh task. Task is then performed asynchronously.
     * @param company
     * @param items
     */
    protected final void createProductPlanningRefreshTask(Record record, List<Item> items) {
        if (record != null) {
            List<Item> ritems = items != null && !items.isEmpty() ? items : record.getItems();
            if (products != null) {
                products.updateSummary(ritems);
            }
            if (productPlanningTask != null) {
                productPlanningTask.send(ritems);
            }
        }
    }

    protected final boolean isSystemPropertyEnabled(String propertyName) {
        return systemConfigManager == null ? false : 
            systemConfigManager.isSystemPropertyEnabled(propertyName); 
    }
}
