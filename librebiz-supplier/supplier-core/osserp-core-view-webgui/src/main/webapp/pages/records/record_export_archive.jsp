<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.recordExportView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="styles" type="string">
        <style type="text/css">
        <c:import url="/pages/records/record_export.css"/>
        </style>
    </tiles:put>
    
    <tiles:put name="headline"><fmt:message key="recordJournal"/> - <o:out value="${view.exportType.name}"/></tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/recordExports.do?method=exitArchive"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="voucherExportArchiveContent">
            <div class="row">
                <div class="col-md-6 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="recordExport"><fmt:message key="period"/></th>
                                    <th class="recordType"><fmt:message key="type"/></th>
                                    <th class="recordNumber"><fmt:message key="numberShort"/></th>
                                    <th class="recordDate"><fmt:message key="fromDate"/></th>
                                    <th class="recordDate"><fmt:message key="til"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="export" items="${view.availableExports}" varStatus="s">
                                    <tr>
                                        <td class="recordExport">
                                            <span title="<oc:employee value="${export.createdBy}"/>">
                                                <o:out value="${export.exportInterval}"/>
                                            </span>
                                        </td>
                                        <td class="recordType">
                                            <a href="<c:url value="/recordExports.do?method=selectAvailable&id=${export.id}"/>" title="<fmt:message key="displayExport"/>">
                                                <oc:options name="recordTypes" value="${export.type}"/>
                                            </a>
                                        </td>
                                        <td class="recordNumber"><o:out value="${export.exportCount}"/></td>
                                        <td class="recordDate">
                                            <o:date value="${export.startDate}"/>
                                        </td>
                                        <td class="recordDate">
                                            <o:date value="${export.endDate}"/>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
