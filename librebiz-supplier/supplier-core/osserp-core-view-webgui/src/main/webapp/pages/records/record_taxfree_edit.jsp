<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<tr>
	<td>
		<c:choose>	
		<c:when test="${record.taxFree}">
		<fmt:message key="exemptOfTax"/>: <input type="checkbox" style="margin-left: 10px;" name="taxFree" checked="checked"/>
		</c:when>
		<c:otherwise>	
		<fmt:message key="exemptOfTax"/>: <input type="checkbox" style="margin-left: 10px;" name="taxFree"/>
		</c:otherwise>	
		</c:choose>	
	</td>
	<td>
		<oc:select name="taxFreeId" value="${record.taxFreeId}" options="taxFreeDefinitions" styleClass="input"/>
	</td>
</tr>
