/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 12, 2016 
 * 
 */
package com.osserp.gui.employees

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.StringUtil

import com.osserp.core.employees.Employee
import com.osserp.core.users.DomainUser;

import com.osserp.gui.CoreView


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class EmployeeSelectionView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(EmployeeSelectionView.class.getName())

    String groupName
    String searchPattern
    List<Employee> allEmployees = []
    
    EmployeeSelectionView() {
        super()
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            String target = form.getString('target')
            if (target) {
                selectionTarget = createTarget(target)
            }
            String group = form.getString('group')
            if (group) {
                groupName = group
                list = employeeSearch.findByGroup(domainUser, groupName)
            }
            if (!list) {
                list = activatedEmployeeList
            }
        }
    }
    
    def disableSearchMode() {
        env.searchMode = false
        list = salesPersonList
    }

    def enableSearchMode() {
        env.searchMode = true
        list = []
    }

    def search() {
        def pattern = form.getString('value')
        if (pattern) {
            searchPattern = pattern
            list = findEmployees()
        } else {
            searchPattern = null
            list = []
        }
    }

    boolean isSearchMode() {
        env.searchMode
    }

    protected List<Employee> findEmployees() {
        List result = []
        if (!allEmployees) {
            allEmployees = activatedEmployeeList
        }
        allEmployees.each {
            if (StringUtil.contains(it.displayName, searchPattern)) {
                result << it
            }
        }
        return result
    }

}
