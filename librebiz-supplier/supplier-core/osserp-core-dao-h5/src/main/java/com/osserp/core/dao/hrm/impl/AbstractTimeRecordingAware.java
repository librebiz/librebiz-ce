/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 4, 2011 3:26:15 PM 
 * 
 */
package com.osserp.core.dao.hrm.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.Options;
import com.osserp.core.dao.impl.AbstractRepository;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordStatus;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.model.hrm.TimeRecordCorrectionImpl;
import com.osserp.core.model.hrm.TimeRecordImpl;
import com.osserp.core.model.hrm.TimeRecordingImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public abstract class AbstractTimeRecordingAware extends AbstractRepository {

    private String recordTableName = null;
    private String recordCorrectionsTableName = null;
    private String recordingApprovalsTableName = null;
    private String recordingTableName = null;
    private String recordingPeriodTableName = null;
    private OptionsCache options = null;
    private TimeRecordStatus _defaultStatus = null;
    private TimeRecordType _stopType = null;

    protected AbstractTimeRecordingAware(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache options,
            String timeRecordingTableKey,
            String timeRecordingPeriodTableKey,
            String timeRecordTableKey,
            String timeRecordCorrectionsTableKey,
            String timeRecordingApprovalsTableKey) {
        super(jdbcTemplate, tables, sessionFactory);
        this.options = options;
        recordingApprovalsTableName = tables.getName(timeRecordingApprovalsTableKey);
        recordingTableName = tables.getName(timeRecordingTableKey);
        recordingPeriodTableName = tables.getName(timeRecordingPeriodTableKey);
        recordTableName = tables.getName(timeRecordTableKey);
        recordCorrectionsTableName = tables.getName(timeRecordCorrectionsTableKey);
    }

    protected final void save(TimeRecording recording) {
        getCurrentSession().saveOrUpdate(TimeRecordingImpl.class.getName(), recording);
    }

    protected final void save(TimeRecord record) {
        getCurrentSession().saveOrUpdate(TimeRecordImpl.class.getName(), record);
    }

    protected List<TimeRecordCorrection> fetchCorrections(TimeRecord record) {
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(TimeRecordCorrectionImpl.class.getName())
                .append(" o where o.reference = ")
                .append(record.getId())
                .append(" order by o.id desc");
        List<TimeRecordCorrection> result = getCurrentSession().createQuery(hql.toString()).list();
        return result;
    }

    public TimeRecordType findType(Long id) {
        return (TimeRecordType) options.getMapped(Options.TIME_RECORD_TYPES, id);
    }

    protected TimeRecordStatus getStatus(Long id) {
        return (TimeRecordStatus) options.getMapped(Options.TIME_RECORD_STATUS, id);
    }

    protected TimeRecordingConfig getDefaultConfig() {
        @SuppressWarnings("unchecked")
        List<TimeRecordingConfig> configs = new ArrayList(options.getList(Options.TIME_RECORDING_CONFIGS));
        for (int i = 0, j = configs.size(); i < j; i++) {
            TimeRecordingConfig next = configs.get(i);
            if (next.isDefaultConfig()) {
                return next;
            }
        }
        return null;
    }

    protected TimeRecordStatus getDefaultStatus() {
        if (_defaultStatus == null) {
            _defaultStatus = (TimeRecordStatus) options.getMapped(Options.TIME_RECORD_STATUS, 0L);
        }
        if (_defaultStatus == null) {
            throw new IllegalStateException("default time record status missing");
        }
        return _defaultStatus;
    }

    protected TimeRecordType getStopType() {
        if (_stopType == null) {
            @SuppressWarnings("unchecked")
            List<TimeRecordType> all = new ArrayList(options.getList(Options.TIME_RECORD_TYPES));
            for (int i = 0, j = all.size(); i < j; i++) {
                TimeRecordType next = all.get(i);
                if (!next.isStarting()) {
                    _stopType = next;
                    break;
                }
            }
        }
        if (_stopType == null) {
            throw new IllegalStateException("time record stop type missing");
        }
        return _stopType;
    }

    protected String getRecordTableName() {
        return recordTableName;
    }

    protected String getRecordCorrectionsTableName() {
        return recordCorrectionsTableName;
    }

    protected String getRecordingApprovalsTableName() {
        return recordingApprovalsTableName;
    }

    protected String getRecordingTableName() {
        return recordingTableName;
    }

    protected String getRecordingPeriodTableName() {
        return recordingPeriodTableName;
    }

    protected OptionsCache getOptions() {
        return options;
    }
}
