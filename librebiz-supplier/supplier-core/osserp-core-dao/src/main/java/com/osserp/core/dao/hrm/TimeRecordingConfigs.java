/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 3, 2011 11:57:50 AM 
 * 
 */
package com.osserp.core.dao.hrm;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordStatus;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecordingConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public interface TimeRecordingConfigs {

    /**
     * Creates a new time recording config without any checks
     * @param user
     * @param name
     * @param description
     * @return new created config
     */
    TimeRecordingConfig createConfig(Employee user, String name, String description);

    /**
     * Provides available time recording configs
     * @return list with configs
     */
    List<TimeRecordingConfig> getConfigs();

    /**
     * Change employees time recording
     * @param user
     * @param config
     */
    void save(Employee user, TimeRecordingConfig config);

    /**
     * Provides a time record type
     * @return type
     */
    TimeRecordType getType(Long id);

    /**
     * Provides all time record types
     * @return types
     */
    List<TimeRecordType> getTypes();

    /**
     * Provides all time record marker types
     * @return types
     */
    List<TimeRecordMarkerType> getMarkerTypes();

    /**
     * Creates a new type
     * @param user
     * @param id
     * @param name
     * @param description
     * @return new created stop type with associated start type
     * @throws ClientException
     */
    TimeRecordType createType(Employee user, Long id, String name, String description)
            throws ClientException;

    /**
     * Creates a new marker type
     * @param user
     * @param id
     * @param name
     * @param description
     * @return new created marker type
     * @throws ClientException
     */
    TimeRecordMarkerType createMarkerType(Employee user, Long id, String name, String description)
            throws ClientException;

    /**
     * Persists type values
     * @param user
     * @param type
     */
    void save(Employee user, TimeRecordType type);

    /**
     * Persists marker type values
     * @param user
     * @param marker type
     */
    void save(Employee user, TimeRecordMarkerType type);

    /**
     * Provides the time record status values
     * @return statusList
     */
    List<TimeRecordStatus> getStatusList();

}
