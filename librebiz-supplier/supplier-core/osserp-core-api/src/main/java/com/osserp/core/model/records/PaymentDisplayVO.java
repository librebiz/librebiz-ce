/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 28, 2015 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;
import java.util.Map;

import com.osserp.common.util.DateUtil;

import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.PaymentDisplay;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PaymentDisplayVO extends FinanceRecordDisplayVO implements PaymentDisplay {
    
    private Date paidOn;
    private String note;
    private Long invoiceId;
    private Long bankAccountId;
    private Long recordTypeId;
    private RecordType invoiceType;
    private boolean outflowOfCash;
    private boolean taxReduced;
    
    private boolean accountStatusOnly = false;
    private String accountStatusName;
    private Double accountStatusSummary = 0d;

    protected PaymentDisplayVO() {
        super();
    }
    
    public PaymentDisplayVO(Date statusDate, Double amount) {
        super(amount);
        paidOn = statusDate;
        accountStatusOnly = true;
        accountStatusName = "accountBalance";
    }

    public PaymentDisplayVO(Map<String, Object> map) {
        super(map);
        paidOn = (Date) map.get("paidOn");
        note = (String) map.get("note");
        invoiceId = (Long) map.get("invoiceId");
        bankAccountId = (Long) map.get("bankAccountId");
        recordTypeId = (Long) map.get("recordTypeId");
        outflowOfCash = (Boolean) map.get("outflowOfCash");
        taxReduced = (Boolean) map.get("taxReduced");
        invoiceType = (RecordType) map.get("invoiceType");
    }

    @Override
    public String getContextName() {
        return invoiceType != null ? invoiceType.getResourceKey() : 
            super.getContextName();
    }

    public Date getPaidOn() {
        return paidOn;
    }

    public void setPaidOn(Date paidOn) {
        this.paidOn = paidOn;
    }

    public int getPaidInYear() {
        return paidOn == null ? 0 : DateUtil.getYear(paidOn);
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public RecordType getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(RecordType invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceNumber() {
        if (isSet(invoiceId)) {
            if (invoiceType != null && isSet(invoiceType.getNumberPrefix())) {
                return invoiceType.getNumberPrefix() + invoiceId.toString();
            }
            return invoiceId.toString();
        }
        return null;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Long getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(Long recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    public boolean isOutflowOfCash() {
        return outflowOfCash;
    }

    public void setOutflowOfCash(boolean outflowOfCash) {
        this.outflowOfCash = outflowOfCash;
    }

    public boolean isTaxReduced() {
        return taxReduced;
    }

    public void setTaxReduced(boolean taxReduced) {
        this.taxReduced = taxReduced;
    }

    public Date getTaxPoint() {
        return paidOn;
    }

    public boolean isCommonPayment() {
        return !isPurchaseInvoicePayment() 
                && !isSalesDownpayment() 
                && !isSalesInvoicePayment();
    }

    public boolean isPurchaseInvoicePayment() {
        return RecordType.PURCHASE_INVOICE.equals(recordTypeId);
    }

    public boolean isSalesDownpayment() {
        return RecordType.SALES_DOWNPAYMENT.equals(recordTypeId);
    }

    public boolean isSalesInvoicePayment() {
        return RecordType.SALES_INVOICE.equals(recordTypeId);
    }
    
    public boolean isBooksOnly() {
        if (getRecordType() instanceof BillingType) {
            return (BillingType.BOOKS_OUTPAYMENT.equals(getRecordType().getResourceKey())
                    || BillingType.BOOKS_PAYMENT.equals(getRecordType().getResourceKey()));
        }
        return false;
    }

    public boolean isCompleted() {
        return true;
    }

    public boolean isUnchangeable() {
        return true;
    }

    public boolean isAccountStatusOnly() {
        return accountStatusOnly;
    }

    public void setAccountStatusOnly(boolean accountStatusOnly) {
        this.accountStatusOnly = accountStatusOnly;
    }

    public String getAccountStatusName() {
        return accountStatusName;
    }

    public void setAccountStatusName(String accountStatusName) {
        this.accountStatusName = accountStatusName;
    }

    public Double getAccountStatusSummary() {
        return accountStatusSummary;
    }

    public void setAccountStatusSummary(Double accountStatusSummary) {
        this.accountStatusSummary = accountStatusSummary;
    }
}
