/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 2, 2012 9:52:37 AM 
 * 
 */
package com.osserp.gui.projects

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException

import com.osserp.groovy.web.ViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/projects/projectTracking/*")
@Controller class ProjectTrackingController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(ProjectTrackingController.class.getName())
    
    @RequestMapping
    def enableTextEditMode(HttpServletRequest request) {
        logger.debug("enableTextEditMode: invoked [user=${getUserId(request)}]")
        ProjectTrackingView view = getView(request)
        view.enableTextEditMode()
        defaultPage
    }
    
    @RequestMapping
    def disableTextEditMode(HttpServletRequest request) {
        logger.debug("disableTextEditMode: invoked [user=${getUserId(request)}]")
        ProjectTrackingView view = getView(request)
        view.disableTextEditMode()
        defaultPage
    }

    @RequestMapping
    def deleteText(HttpServletRequest request) {
        logger.debug("deleteText: invoked [user=${getUserId(request)}]")
        ProjectTrackingView view = getView(request)
        view.deleteText()
        defaultPage
    }
    
    @RequestMapping
    def saveText(HttpServletRequest request) {
        logger.debug("saveText: invoked [user=${getUserId(request)}]")
        ProjectTrackingView view = getView(request)
        try {
            view.saveText()
        } catch (ClientException e) {
            saveError(request, e.message)
        }
        defaultPage
    }

    @RequestMapping
    def toggleIgnore(HttpServletRequest request) {
        logger.debug("toggleIgnore: invoked [user=${getUserId(request)}]")
        ProjectTrackingView view = getView(request)
        try {
            view.toggleIgnore()
        } catch (ClientException e) {
            saveError(request, e.message)
        }
        defaultPage
    }
    
    @RequestMapping
    def release(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("release: invoked [user=${getUserId(request)}]")
        ProjectTrackingView view = getView(request)
        try {
            view.release()
        } catch (ClientException e) {
            saveError(request, e.message)
        }
        defaultPage
    }

    @RequestMapping
    def print(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("print: invoked [user=${getUserId(request)}]")
        ProjectTrackingView view = getView(request)
        savePdf(request, view.getPdf())
        renderPdf(request, response)
    }

}
