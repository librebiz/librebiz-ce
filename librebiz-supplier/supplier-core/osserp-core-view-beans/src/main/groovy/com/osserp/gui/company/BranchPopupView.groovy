/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 15, 2011 
 * 
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.system.BranchOfficeManager
import com.osserp.gui.CoreView

/**
 * 
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class BranchPopupView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(BranchPopupView.class.getName())

    BranchPopupView() {
        dependencies = ['clientCompanyView']
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        Long id = form.getLongVal('id')
        if (id && id >= 0) {
            logger.debug("initRequest: invoked [id=${id}]")
            bean = branchOfficeManager.find(id)
        }
    }

    private BranchOfficeManager getBranchOfficeManager() {
        getService(BranchOfficeManager.class.getName())
    }
}
