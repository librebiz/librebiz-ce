<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource />

<c:set var="view" value="${requestScope.dmsDocumentView}" />
<c:set var="deletePermissions" value="${requestScope.dmsDeletePermissions}" />
<c:set var="referencePermissions" value="${requestScope.dmsReferencePermissions}" />


<c:choose>
    <c:when test="${!empty view.selectedImage}">
        <div class="col-md-12">
            <div class="text-xs-center text-lg-center">
                <v:link url="${view.baseLink}/selectImage">
                    <img src="<oc:img value="${view.selectedImage}"/>" class="center-block" style="height:${view.thumbnailHeightDisplay};width:auto;border:0px;" />
                </v:link>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <c:forEach items="${view.list}" var="doc">
            <c:if test="${doc.thumbnailAvailable}">
                <div class="${view.thumbnailCols}">
                    <div class="thumbnail">
                        <v:link url="${view.baseLink}/selectImage?id=${doc.id}&type=${doc.type.id}">
                            <oc:thumbnail value="${doc}" url="${view.baseLink}/thumbnail" height="${view.thumbnailHeightGallery}" width="auto" />
                        </v:link>
                    </div>
                </div>
            </c:if>
        </c:forEach>
    </c:otherwise>
</c:choose>
