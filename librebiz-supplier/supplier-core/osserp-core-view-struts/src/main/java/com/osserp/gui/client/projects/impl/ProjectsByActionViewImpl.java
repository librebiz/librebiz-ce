/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13 Apr 2007 14:25:37 
 * 
 */
package com.osserp.gui.client.projects.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.BusinessType;
import com.osserp.core.BusinessTypeManager;
import com.osserp.core.BusinessTypeSelection;
import com.osserp.core.FcsAction;
import com.osserp.core.Options;
import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerManager;
import com.osserp.core.projects.ProjectFcsActionManager;
import com.osserp.core.projects.results.ProjectByActionDate;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.projects.ProjectsByActionView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("projectsByActionView")
public class ProjectsByActionViewImpl extends AbstractProjectQueryView implements ProjectsByActionView {

    private Long selectedType = null;
    private Long action = null;
    private Long withAction = null;
    private Date from = null;
    private Date until = null;
    private String zipcode = null;
    private boolean withClosed = false;
    private boolean withCanceled = false;
    private boolean byAction = false;
    private boolean byMissingAction = false;

    private List<FcsAction> actions = new ArrayList<>();
    private List<BusinessType> requestTypes = new ArrayList<>();
    private Map<Long, FcsAction> actionMap = new HashMap<>();

    protected ProjectsByActionViewImpl() {
        super();
        setActionTarget(Actions.QUERY);
        setListTarget(Actions.LIST);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        init();
    }

    @Override
    public void save(Form fh) throws ClientException, PermissionException {
        action = fh.getLong("id");
        withAction = fh.getLong("withAction");
        from = fh.getDate("from");
        until = fh.getDate("until");
        zipcode = fh.getString("zipcode");
        withCanceled = fh.getBoolean("withCanceled");
        withClosed = fh.getBoolean("withClosed");
    }

    public List<BusinessType> getRequestTypes() {
        return requestTypes;
    }

    protected void setRequestTypes(List<BusinessType> requestTypes) {
        this.requestTypes = requestTypes;
    }

    public Long getSelectedType() {
        return selectedType;
    }

    public void selectType(Long type) {
        selectedType = type;
        if (selectedType != null) {
            actions = getFlowControlActionManager().findByType(selectedType);
        }
    }

    public void executeByAction() {
        List<ProjectByActionDate> result = getProjectSearch().findByActionAndDate(
                selectedType,
                action,
                from,
                until,
                withCanceled,
                withClosed,
                zipcode);
        setList(result);
        byAction = true;
        byMissingAction = false;
    }

    public boolean isByAction() {
        return byAction;
    }

    public void executeByMissingAction() {
        List<ProjectByActionDate> result = getProjectSearch().findByMissingAction(
                selectedType,
                action,
                withAction,
                withCanceled,
                withClosed);
        setList(result);
        byAction = false;
        byMissingAction = true;
    }

    public boolean isByMissingAction() {
        return byMissingAction;
    }

    public void refresh() {
        if (byAction) {
            executeByAction();
        } else {
            executeByMissingAction();
        }
    }

    public Long getAction() {
        return action;
    }

    public Date getFrom() {
        return from;
    }

    public Date getUntil() {
        return until;
    }

    public Long getWithAction() {
        return withAction;
    }

    public boolean isWithCanceled() {
        return withCanceled;
    }

    public boolean isWithClosed() {
        return withClosed;
    }

    public String getZipcode() {
        return zipcode;
    }

    public List<FcsAction> getActions() {
        return actions;
    }

    public Document createCustomerXml() {
        int size = getList().size();
        Long[] customerIds = new Long[size];
        for (int i = 0, j = size; i < j; i++) {
            ProjectByActionDate item = (ProjectByActionDate) getList().get(i);
            customerIds[i] = item.getCustomerId();
        }
        CustomerManager manager = (CustomerManager) getService(CustomerManager.class.getName());
        List<Customer> customers = manager.getCustomers(customerIds);
        Document doc = new Document();
        Element root = new Element("customers");
        size = customers.size();
        for (int i = 0, j = size; i < j; i++) {
            Customer customer = customers.get(i);
            root.addContent(customer.getXML());
        }
        doc.setRootElement(root);
        return doc;
    }

    public Document createActionXml() {
        Document doc = new Document();
        doc.setRootElement(new Element("projects"));
        Element root = doc.getRootElement();
        root.setAttribute(new Attribute("action", getActionName()));
        root.addContent(new Element("currentDate").setText(
                DateFormatter.getDate(new Date(System.currentTimeMillis()))));
        Map employeeMap = getOptionMap(Options.EMPLOYEES);
        for (Iterator i = getList().iterator(); i.hasNext();) {
            ProjectByActionDate item = (ProjectByActionDate) i.next();
            root.addContent(item.getValues(employeeMap));
        }
        return doc;
    }

    /**
     * Interceptor removing search results not related to branch of current user if user is not from headquarter
     */
    @Override
    protected void setList(List list) {
        DomainUser domainUser = getDomainUser();
        for (Iterator<ProjectByActionDate> i = list.iterator(); i.hasNext();) {
            ProjectByActionDate next = i.next();
            if (isSet(next.getBranchId())) {
                BranchOffice office = fetchBranchOffice(next.getBranchId());
                if (!domainUser.isBranchAccessible(office)) {
                    i.remove();
                }
            }
        }
        super.setList(list);
    }

    // setters are protected, use save instead

    protected void setSelectedType(Long selectedType) {
        this.selectedType = selectedType;
    }

    protected void setAction(Long action) {
        this.action = action;
    }

    protected void setFrom(Date from) {
        this.from = from;
    }

    protected void setUntil(Date until) {
        this.until = until;
    }

    protected void setWithAction(Long withAction) {
        this.withAction = withAction;
    }

    protected void setWithCanceled(boolean withCanceled) {
        this.withCanceled = withCanceled;
    }

    protected void setWithClosed(boolean withClosed) {
        this.withClosed = withClosed;
    }

    protected void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    protected void setActions(List<FcsAction> actions) {
        this.actions = actions;
    }

    protected void setByAction(boolean byAction) {
        this.byAction = byAction;
    }

    protected void setByMissingAction(boolean byMissingAction) {
        this.byMissingAction = byMissingAction;
    }

    private void init() {
        BusinessTypeSelection selection = getBusinessTypeManager().findTypeSelection("salesMonitoringProjects");
        if (selection != null) {
            requestTypes = selection.getTypes();
        }
        if (requestTypes.isEmpty()) {
            requestTypes = getBusinessTypeManager().findActive();
        }
        BusinessType type = getBusinessTypeManager().findDefaultType("salesByActionDefaultType");
        selectedType = type != null ? type.getId() : 2L;
        ProjectFcsActionManager manager = getFlowControlActionManager();
        actions = manager.findByType(selectedType);
        actionMap = manager.createMap();
    }

    private BusinessTypeManager getBusinessTypeManager() {
        return (BusinessTypeManager) getService(BusinessTypeManager.class.getName());
    }

    private ProjectFcsActionManager getFlowControlActionManager() {
        return (ProjectFcsActionManager) getService(ProjectFcsActionManager.class.getName());
    }

    private String getActionName() {
        if (!actionMap.containsKey(action)) {
            return "";
        }
        return ((Option) actionMap.get(action)).getName();
    }

}
