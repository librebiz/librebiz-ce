<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.businessCaseView}"/>
<c:if test="${!empty view}">

<table class="table">
	<thead>
		<tr>
		<th><fmt:message key="appointment"/></th>
		<th><fmt:message key="description"/></th>
		<th><fmt:message key="appointmentOf"/></th>
		<th class="action"><fmt:message key="action"/></th>
	</tr>
	</thead>
	<tbody>
		<c:forEach var="app" items="${view.appointments}" varStatus="s">
			<tr class="altrow">
				<td style="width: 100px;">
					<span><o:date value="${app.appointmentDate}" addcentury="false"/></span>
					<span><o:time value="${app.appointmentDate}"/></span>
				</td>
				<td><o:out value="${app.action.name}"/></td>
				<td>
					<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${app.recipientId}">
						<oc:employee value="${app.recipientId}"/>
					</v:ajaxLink>
				</td>
				<td class="center">
					<v:ajaxLink url="/events/eventEditor/forward?id=${app.id}&view=businessCaseView" linkId="eventEditor"><o:img name="texteditIcon" /></v:ajaxLink>
					<c:if test="${view.user.domainUser and view.user.userEmployee and view.user.employee.id == app.recipientId}">
						<a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmCloseAppointment"/>','<c:url value="/sales.do"><c:param name="method" value="closeAppointmentNote"/><c:param name="exit" value="display"/><c:param name="id" value="${app.id}"/></c:url>');" title="<fmt:message key="removeAppointment"/>"><o:img name="deleteIcon"/></a>
					</c:if>
				</td>
			</tr>
			<c:if test="${!empty app.message or !empty app.notes}">
				<tr>
					<td>&nbsp;</td>
					<td colspan="3">
						<c:forEach var="note" items="${app.notes}">
							<span class="boldtext"><o:date value="${note.created}" addtime="true"/> <oc:employee value="${note.createdBy}"/>:</span><br/>
							<o:out value="${note.note}"/><c:if test="${!s.last or !empty app.message}"><br/></c:if>
						</c:forEach>
						<c:if test="${!empty app.message}">
							<c:if test="${!empty app.notes}">---<br/></c:if>
							<c:if test="${app.createdBy != app.recipientId}"><span class="boldtext"><oc:employee value="${app.createdBy}"/>: </span></c:if><o:out value="${app.message}"/>
						</c:if>
					</td>
				</tr>
			</c:if>
		</c:forEach>
	</tbody>
</table>
</c:if>