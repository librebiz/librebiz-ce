/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 17, 2006 7:41:06 PM 
 * 
 */
package com.osserp.gui.client.records;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Order;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DeliveryNoteView extends CreateableRecordView {

    /**
     * Creates a new delivery note of given type
     * @param type
     * @throws ClientException if any requirements not met
     * @throws PermissionException if user is no employee
     */
    public void create(DeliveryNoteType type) throws ClientException, PermissionException;

    /**
     * Provides the order where our delivery note is related to
     * @return order
     */
    public Order getOrder();

    /**
     * Provides current record as delivery note
     * @return note
     */
    public DeliveryNote getNote();

    /**
     * Indicates that serials for any item position are required and not booked
     * @return serialsRequired s
     */
    public boolean isSerialsRequired();

    /**
     * Indicates that ignore not existing serial dialog should be displayed
     * @return displayIgnoreNotExistingSerialDialog
     */
    public boolean isDisplayIgnoreNotExistingSerialDialog();

    /**
     * Enables ignore not existing serial mode if user has required permissions
     * @throws PermissionException if user has not required permissions
     */
    public void enableIgnoreNotExistingSerialMode() throws PermissionException;

    /**
     * Indicates that ignore not existing serial exception is activated
     * @return ignoreNotExistingSerialException
     */
    public boolean isIgnoreNotExistingSerialException();

    /**
     * Provides last serial input after a failed attempt to add a serial
     * @return lastSerialInput
     */
    public String getLastSerialInput();

    /**
     * Provides the id of the last serial input item
     * @return lastSerialInputItem
     */
    public Long getLastSerialInputItem();

    /**
     * Adds a serial number to an item specified in given form
     * @param form
     * @throws ClientException if validation failed
     * @throws PermissionException if user is no employee
     */
    public void addSerial(Form fh) throws ClientException, PermissionException;

    /**
     * Deletes all serials of an item
     * @param itemId
     */
    public void deleteSerials(Long itemId);
}
