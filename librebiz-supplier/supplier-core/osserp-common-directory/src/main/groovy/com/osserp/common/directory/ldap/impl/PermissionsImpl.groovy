/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 19, 2013 11:50:20 AM 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.dao.Permissions


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class PermissionsImpl extends AbstractLdapGroups implements Permissions {
    private static Logger logger = LoggerFactory.getLogger(PermissionsImpl.class.getName())

    protected PermissionsImpl() {
        super()
    }

    PermissionsImpl(LdapConfig configInstance) {
        super(configInstance)
        schema = new PermissionsSchema(configInstance)
    }


    Long createIdSuggestion(String client) {
        Long currentMax = 0
        List<Map> existing = findAll(client)
        for (group in existing) {
            Long next = fetchLong(group.gidnumber)
            if (next && next > currentMax) {
                currentMax = next
            }
        }
        return (currentMax + 1)
    }

    @Override
    protected String createDn(String name, String client) {
        //cn=GroupName,ou=Groups,dc=osserp,dc=org
        "cn=${name},ou=Permissions,${createBaseDn(client)}"
    }
}
