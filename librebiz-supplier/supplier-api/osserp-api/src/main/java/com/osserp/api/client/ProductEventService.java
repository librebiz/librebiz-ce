/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api.client;

import com.osserp.api.WsProduct;
import com.osserp.api.WsMedia;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductEventService {

    /**
     * This method will be invoked whenever an product information has changed.
     * @param wsProduct
     * @return product object
     */
    WsProduct productChanged(WsProduct wsProduct);

    /**
     * This method will be invoked whenever an product realted media has changed. 
     * @param productMedia
     * @return media object
     */
    WsMedia productMediaChanged(WsMedia productMedia);
  
}
