<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="requestSearchView"/>
<o:ajaxLookupForm name="searchForm" url="${view.appLink}/save" targetElement="ajaxContent" minChars="0" events="onkeyup" activityElement="value">
    <div class="form-body">

        <div class="row">
        
            <div class="col-md-3">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <input class="form-control" type="text" name="value" id="value" value="<o:out value="${view.searchRequest.pattern}"/>" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <a href="javascript: $('searchForm').onkeyup();"> 
                                <o:img name="replaceIcon" styleClass="bigicon" style="padding-top: 5px;" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <select name="businessType" size="1" class="form-control" onchange="javascript: $('searchForm').onkeyup();">
                        <option value="0"><fmt:message key="allOrderTypes" /></option>
                        <c:forEach var="obj" items="${view.requestTypes}">
                            <option value="<c:url value="${obj.id}"/>" <c:if test="${obj.id == view.searchRequest.businessTypeId}">selected="selected"</c:if>>
                                <o:out value="${obj.name}" />
                            </option>
                        </c:forEach>
                    </select> 
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <select name="branch" size="1" class="form-control" onchange="javascript: $('searchForm').onkeyup();">
                        <option value="0"><fmt:message key="allBranch" /></option>
                        <c:forEach var="obj" items="${view.branchOfficeList}">
                            <option value="<c:url value="${obj.id}"/>" <c:if test="${obj.id == view.searchRequest.branchId}">selected="selected"</c:if>>
                                <o:out value="${obj.name}" />
                            </option>
                        </c:forEach>
                    </select> 
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <div class="checkbox">
                        <label> 
                            <v:checkbox name="includeCanceled" value="${view.searchRequest.includeCanceled}" onchange="javascript: $('searchForm').onkeyup();" /> 
                            <fmt:message key="includeCanceled" />
                        </label>
                    </div>
                </div>
            </div>
        </div>

    </div>
</o:ajaxLookupForm>
<div id="ajaxDiv">
    <div id="ajaxContent">
        <c:if test="${!empty view.list}">
            <c:import url="_requestSearch.jsp"/>
        </c:if>
    </div>
</div>
