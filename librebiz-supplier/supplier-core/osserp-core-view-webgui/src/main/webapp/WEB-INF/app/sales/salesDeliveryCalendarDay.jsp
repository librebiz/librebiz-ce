<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource />
<o:userContext />
<v:view viewName="salesDeliveryCalendarDayView" />
<c:set var="cal" value="${view.calendar}"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
        .table > thead > tr > th.quantity, .table > tbody > tr > td.quantity { text-align: right; padding-right: 15px; }
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="salesDeliveryCalendar"/> <o:out value="${cal.day}"/>.<o:out value="${cal.month + 1}"/>.<o:out value="${cal.year}"/>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table">
                            <c:choose>
                                <c:when test="${view.itemMode}">
                                    <thead>
                                        <tr>
                                            <th><fmt:message key="order" /></th>
                                            <th><fmt:message key="product" /></th>
                                            <th class="quantity"><fmt:message key="quantity"/></th>
                                            <th><fmt:message key="description" /></th>
                                            <th><fmt:message key="shippingLabel" /></th>
                                            <th><fmt:message key="consignment" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="obj" items="${view.list}">
                                            <tr>
                                                <td><a href="<c:url value="/loadSales.do?id=${obj.id}&exit=salesDeliveryCalendarDay"/>"><o:out value="${obj.id}"/></a></td>
                                                <td><a href="<c:url value="/products.do?method=load&id=${obj.productId}&exit=salesDeliveryCalendarDay"/>"><o:out value="${obj.productId}" /></a></td>
                                                <td class="quantity"><o:out value="${obj.outstanding}" /></td>
                                                <td><o:out value="${obj.productName}" /></td>
                                                <td><oc:options name="shippingCompanies" value="${obj.shippingId}"/></td>
                                                <td><o:out value="${obj.name}" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </c:when>
                                <c:otherwise>
                                    <thead>
                                        <tr>
                                            <th><fmt:message key="order" /></th>
                                            <th><fmt:message key="consignment" /></th>
                                            <th><fmt:message key="shippingLabel" /></th>
                                            <th><fmt:message key="street" /></th>
                                            <th><fmt:message key="zipcode" /></th>
                                            <th><fmt:message key="city" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="obj" items="${view.list}">
                                            <tr>
                                                <td><a href="<c:url value="/loadSales.do?id=${obj.id}&exit=salesDeliveryCalendarDay"/>"><o:out value="${obj.id}"/></a></td>
                                                <td><o:out value="${obj.name}" /></td>
                                                <td><oc:options name="shippingCompanies" value="${obj.shippingId}"/></td>
                                                <td><o:out value="${obj.street}" /></td>
                                                <td><o:out value="${obj.zipcode}" /></td>
                                                <td><o:out value="${obj.city}" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </c:otherwise>
                            </c:choose>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
