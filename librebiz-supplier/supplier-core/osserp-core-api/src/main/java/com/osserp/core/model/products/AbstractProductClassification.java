/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 12, 2008 8:45:05 PM 
 * 
 */
package com.osserp.core.model.products;

import java.util.Date;

import org.apache.commons.beanutils.BeanUtils;

import org.jdom2.Element;

import com.osserp.common.beans.AbstractClassification;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.products.ProductClassificationEntity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractProductClassification extends AbstractClassification
        implements ProductClassificationEntity {

    private boolean customNameSupport;
    private Long numberRangeId = null;
    

    /**
     * Object serialization constructor
     */
    protected AbstractProductClassification() {
        super();
    }

    /**
     * Initializes classification by xml
     * @param root element
     * @throws Exception
     */
    public AbstractProductClassification(Element root) throws Exception {
        super(root);
        for (Object child : root.getChildren()) {
            Element el = (Element) child;
            try {
                if (BeanUtils.getProperty(this, el.getName()) == null) {
                    BeanUtils.setProperty(this, el.getName(), JDOMUtil.getObjectFromElement(el));
                }
            } catch (Exception ex) {
            }
        }
    }

    /**
     * Default constructor for new classifications
     * @param name
     * @param description
     * @param createdBy
     */
    protected AbstractProductClassification(
            String name,
            String description,
            Long createdBy) {
        super(name, description, createdBy);
    }

    /**
     * All value constructor
     * @param id
     * @param name
     * @param description
     * @param created
     * @param createdBy
     * @param changed
     * @param changedBy
     * @param resourceKey
     * @param endOfLife
     * @param numberRangeId
     */
    protected AbstractProductClassification(
            Long id,
            String name,
            String description,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            String resourceKey,
            boolean endOfLife,
            boolean customNameSupport,
            Long numberRangeId) {
        super(id, (Long) null, name, description, resourceKey, created, createdBy, changed, changedBy, endOfLife);
        this.numberRangeId = numberRangeId;
    }

    /**
     * All values by other constructor
     * @param other
     */
    protected AbstractProductClassification(ProductClassificationEntity other) {
        this(
                other.getId(),
                other.getName(),
                other.getDescription(),
                other.getCreated(),
                other.getCreatedBy(),
                other.getChanged(),
                other.getChangedBy(),
                other.getResourceKey(),
                other.isEndOfLife(),
                other.isCustomNameSupport(),
                other.getNumberRangeId());
    }

    public boolean isCustomNameSupport() {
        return customNameSupport;
    }

    public void setCustomNameSupport(boolean customNameSupport) {
        this.customNameSupport = customNameSupport;
    }

    public Long getNumberRangeId() {
        return numberRangeId;
    }

    public void setNumberRangeId(Long numberRangeId) {
        this.numberRangeId = numberRangeId;
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("customNameSupport", customNameSupport));
        root.addContent(JDOMUtil.createElement("numberRangeId", numberRangeId));
        return root;
    }

    public boolean isSelectionLabel() {
        return false;
    }
}
