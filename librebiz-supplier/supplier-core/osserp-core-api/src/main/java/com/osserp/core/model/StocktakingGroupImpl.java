/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 27-Dec-2006 13:15:35 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.beans.OptionImpl;
import com.osserp.core.finance.StocktakingGroup;
import com.osserp.core.finance.StocktakingItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StocktakingGroupImpl extends OptionImpl implements StocktakingGroup {

    private List<StocktakingItem> items = new ArrayList<StocktakingItem>();

    protected StocktakingGroupImpl() {
        super();
    }

    protected StocktakingGroupImpl(Long id, String name) {
        super(id, name);
    }

    public List<StocktakingItem> getItems() {
        return items;
    }

    public void addItem(StocktakingItem item) {
        this.items.add(item);
    }

    protected void setItems(List<StocktakingItem> items) {
        this.items = items;
    }

    public int compareTo(Object o) {
        StocktakingGroup other = (StocktakingGroup) o;
        if (other.getId().equals(getId())) {
            return 0;
        }
        if (getId() < other.getId()) {
            return -1;
        }
        return 1;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof StocktakingGroup)) {
            return false;
        }
        return getId().equals(((StocktakingGroup) other).getId());
    }

}
