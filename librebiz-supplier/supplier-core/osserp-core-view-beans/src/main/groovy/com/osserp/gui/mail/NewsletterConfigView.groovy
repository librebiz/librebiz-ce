/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Jun 22, 2014
 *
 */
package com.osserp.gui.mail

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactManager

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class NewsletterConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(NewsletterConfigView.class.getName())

    boolean listDeactivated = false

    NewsletterConfigView() {
        super()
        enableAutoreload()
        providesCustomNavigation()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            loadRecipients()
        }
    }
    
    @Override
    void reload() {
        loadRecipients()
    }
    
    @Override
    void save() {
        Long[] ids = form.getIndexedLong('ids')
        if (ids) {
            contactManager.updateNewsletterStatus(ids, listDeactivated ?: false)
            loadRecipients()
        }
    }
    
    void toggleSelection() {
        listDeactivated = !listDeactivated
        loadRecipients()
    }
    
    private void loadRecipients() {
        if (listDeactivated) {
            list = contactManager.findNewsletterRecipients(false)
            logger.debug("loadRecipients: done [deactivatedRecipients=${list.size()}]")
        } else {
            list = contactManager.findNewsletterRecipients(true)
            logger.debug("loadRecipients: done [activatedRecipients=${list.size()}]")
        }
    }
    
    @Override
    void createCustomNavigation() {
        if (listDeactivated) {
            nav.listNavigation = nav.defaultNavigation = [exitLink, deactivatedLink, homeLink]
        } else {
            if (list) {
                nav.listNavigation = nav.defaultNavigation = [exitLink, mailtoLink, activatedLink, homeLink]
            } else {
                nav.listNavigation = nav.defaultNavigation = [exitLink, activatedLink, homeLink]
            }
        }
    }
    
    MenuItem getActivatedLink() {
        navigationLink("/${context.name}/toggleSelection", 'smileIcon', 'newsletterDeactivatedContacts')
    }
    
    MenuItem getDeactivatedLink() {
        navigationLink("/${context.name}/toggleSelection", 'nosmileIcon', 'newsletterActivatedContacts')
    }

    MenuItem getMailtoLink() {
        mailtoLink(recipientList, 'mailToThisMailingList')
    }
    
    private String getRecipientList() {
        StringBuilder result = new StringBuilder()
        boolean oneAdded = false
        list.each {
            if (oneAdded) {
                result.append(',')
            }
            result.append(it.address)
            oneAdded = true
        }
        result.toString()
    }
    
    private ContactManager getContactManager() {
        getService(ContactManager.class.getName())
    }
}
