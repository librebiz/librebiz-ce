/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 9, 2006 7:40:23 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.osserp.common.dao.Tables;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class TaxRatesDao extends AbstractDao implements TaxRates {

    //TODO remove, see #2887
    @Deprecated
    private static final Double DEFAULT_RATE = Double.valueOf("1.19");
    @Deprecated
    private static final Double DEFAULT_REDUCED_RATE = Double.valueOf("1.07");

    private String taxRatesTable = null;

    public TaxRatesDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        taxRatesTable = getTable(TableKeys.TAX_RATES);
    }

    public Double getCurrentRate() {
        return getRate(false);
    }

    public Double getCurrentReducedRate() {
        return getRate(true);
    }

    public Double getRate(boolean reduced) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT rate FROM ")
                .append(taxRatesTable)
                .append(" WHERE ");
        if (reduced) {
            sql.append("is_reduced = 1 AND is_default = 1");
        } else {
            sql.append("is_reduced = 0 AND is_default = 1");
        }
        List list = (List) jdbcTemplate.query(sql.toString(), getDoubleRowMapper());
        return empty(list) ? (reduced ? DEFAULT_REDUCED_RATE : DEFAULT_RATE) : (Double) list.get(0);
    }

    public List<Double> getRates() {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT rate FROM ")
                .append(taxRatesTable)
                .append(" WHERE is_reduced = 0");
        if (!systemPropertyEnabled("taxRateZeroSupport")) {
            sql.append(" AND rate > 1");
        }
        return (List) jdbcTemplate.query(sql.toString(), getDoubleRowMapper());
    }

    public List<Double> getReducedRates() {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT rate FROM ")
                .append(taxRatesTable)
                .append(" WHERE is_reduced = 1");
        return (List) jdbcTemplate.query(sql.toString(), getDoubleRowMapper());
    }

}
