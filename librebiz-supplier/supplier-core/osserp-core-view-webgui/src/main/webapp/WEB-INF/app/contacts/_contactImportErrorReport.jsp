<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="contactImportErrorReportView" />

<style type="text/css">
div.standardForm .label {
    min-width: 150px;
    text-align: right;
    margin-right: 10px;
    font-size: 100%;
    font-weight: normal;
}
</style>
<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <o:out value="${view.bean.filename}" />
    </div>
    <div class="modalBoxHeaderRight">
        <div class="boxnav"></div>
    </div>
</div>
<div class="modalBoxData">
    <div class="spacer"></div>
        <div class="standardForm">
        <div class="formContent">
            <div class="data">
                <div class="label">
                    <p><fmt:message key="errorReport" /></p>
                </div>
                <div class="value" style="width:1024px;">
                    <p>
                        <textarea class="form-control" name="note" rows="16" style="width:1000px;" >
                            <o:out value="${view.bean.errorReport}"/>
                        </textarea> 
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="spacer"></div>
</div>
