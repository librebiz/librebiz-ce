/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 28, 2007 2:14:32 PM 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsConfig;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestFcsConfigImpl extends AbstractEntity
        implements FcsConfig {

    private List<FcsAction> actions = new ArrayList<FcsAction>();

    protected RequestFcsConfigImpl() {
        super();
    }

    public RequestFcsConfigImpl(BusinessType type, Long createdBy) {
        super(null, type.getId(), createdBy);
    }

    public List<FcsAction> getActions() {
        return actions;
    }

    public void add(Employee user, FcsAction action) {
        boolean exists = false;
        for (int i = 0, j = actions.size(); i < j; i++) {
            FcsAction next = actions.get(i);
            if (next.getId().equals(action.getId())) {
                exists = true;
                break;
            }
        }
        if (!exists) {
            if (actions.isEmpty()) {
                actions.add(action);
            } else {
                // add the new action as second to allow quickly move down
                actions.add(1, action);
            }
            setChanged(DateUtil.getCurrentDate());
            setChangedBy(user.getId());
        }
    }

    public void remove(Employee user, FcsAction action) {
        for (Iterator<FcsAction> i = actions.iterator(); i.hasNext();) {
            FcsAction next = i.next();
            if (next.getId().equals(action.getId())) {
                i.remove();
                setChanged(DateUtil.getCurrentDate());
                setChangedBy(user.getId());
                break;
            }
        }
    }

    public boolean contains(FcsAction action) {
        for (int i = 0, j = actions.size(); i < j; i++) {
            FcsAction next = actions.get(i);
            if (next.getId().equals(action.getId())) {
                return true;
            }
        }
        return false;
    }

    public void moveDown(Long id) {
        CollectionUtil.moveNext(actions, id);
    }

    public void moveUp(Long id) {
        CollectionUtil.movePrevious(actions, id);
    }

    protected void setActions(List<FcsAction> actions) {
        this.actions = actions;
    }
}
