/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 1, 2008 4:22:39 PM 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.Option;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Product;
import com.osserp.core.purchasing.PurchaseOffer;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseOfferImpl extends AbstractOffer implements PurchaseOffer {

    /**
     * Default constructor required by Serializable
     */
    protected PurchaseOfferImpl() {
        super();
    }

    /**
     * Creates a new PurchaseOffer
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param supplier
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     */
    public PurchaseOfferImpl(
            Long id,
            RecordType type,
            BookingType bookingType,
            Long company,
            Long branchId,
            Long reference,
            Supplier supplier,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate) {
        super(
                id,
                type,
                bookingType,
                company,
                branchId,
                reference,
                supplier,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate);
    }

    private String supplierReferenceNumber = null;
    private Date supplierReferenceDate = null;

    public String getSupplierReferenceNumber() {
        return supplierReferenceNumber;
    }

    public void setSupplierReferenceNumber(String supplierReferenceNumber) {
        this.supplierReferenceNumber = supplierReferenceNumber;
    }

    public Date getSupplierReferenceDate() {
        return supplierReferenceDate;
    }

    public void setSupplierReferenceDate(Date supplierReferenceDate) {
        this.supplierReferenceDate = supplierReferenceDate;
    }

    @Override
    protected Item createOptionItem(
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice) {
        // purchase offer currently doesn't support options 
        return null;
    }

    @Override
    protected RecordPaymentAgreement createPaymentAgreement() {
        return new PurchaseOfferPaymentAgreementImpl(this);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        return new PurchaseOfferInfoImpl(
                this,
                info,
                user);
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {
        return new PurchaseOfferItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }

    @Override
    public boolean isSales() {
        return false;
    }
}
