<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="${sessionScope.contactInputViewName}" />

<div class="row">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <c:choose>
                        <c:when test="${!empty view.selectedType.description}">
                            <o:out value="${view.selectedType.description}"/>
                        </c:when>
                        <c:otherwise>
                            <o:out value="${view.selectedType.name}"/>
                        </c:otherwise>
                    </c:choose>
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
            
                <c:choose>
                    <c:when test="${view.freelanceSelected}">
                        <input type="hidden" name="privateContact" value="false" />
                        <c:import url="_contactCreateFormFreelance.jsp" />
                    </c:when>
                    <c:when test="${view.contactPersonSelected}">
                        <input type="hidden" name="privateContact" value="true" />
                        <c:import url="_contactCreateFormPerson.jsp" />
                    </c:when>
                    <c:when test="${view.privateContactSelected}">
                        <input type="hidden" name="privateContact" value="true" />
                        <c:import url="_contactCreateFormPrivate.jsp" />
                    </c:when>
                    <c:otherwise>
                        <input type="hidden" name="privateContact" value="false" />
                        <c:import url="_contactCreateFormCompany.jsp" />
                    </c:otherwise>
                </c:choose>
                
            </div>
        </div>
    </div>

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="communicationData"/></h4>
            </div>
        </div>
        <div class="panel-body">
                        
            <div class="form-body">
            
                <c:import url="_contactCreateFormCommunication.jsp" />
                
                <div class="row next">
                    <div class="col-md-4"> </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="/contacts/contactCreator/disableCreateMode"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit value="create" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
        
</div>
