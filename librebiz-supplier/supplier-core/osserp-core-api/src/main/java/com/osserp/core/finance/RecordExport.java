/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Aug-2006 17:41:36 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;
import java.util.List;

import com.osserp.common.CalendarMonth;
import com.osserp.common.Entity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordExport extends Entity {

    /**
     * Provides the record type of records in records
     * @return type
     */
    Long getType();

    /**
     * Provides the date created of the first record associated with this export. 
     * @return startDate
     */
    Date getStartDate();

    /**
     * Provides the start month
     * @return startMonth
     */
    CalendarMonth getStartMonth();
    
    /**
     * Provides the date created of the last record associated with this export. 
     * @return endDate
     */
    Date getEndDate();

    /**
     * Provides the end month
     * @return endMonth
     */
    CalendarMonth getEndMonth();

    /**
     * Provides the export interval as string of startDate and endDate year and month values.
     * E.g. 'yy-mm' if start and end is same or 'yy-mm - yy-mm' if start and end differs.
     * The exportInterval may be used as a key to group exports of different types matching the same interval.    
     * @return exportInterval
     */
    String getExportInterval();
   
    /**
     * Provides the exported records
     * @return records
     */
    List<RecordDisplay> getRecords();

    /**
     * Provides the count of exported records
     * @return exportCount
     */
    int getExportCount();

    /**
     * Provides the summarized net amount over all records
     * @return netAmount
     */
    Double getNetAmount();
    
    /**
     * Provides the summarized tax amount over all records
     * @return taxAmount
     */
    Double getTaxAmount();
    
    /**
     * Provides the summarized reduced tax amount over all records
     * @return reducedTaxAmount
     */
    Double getReducedTaxAmount();
    
    /**
     * Provides the summarized gross amount over all records
     * @return grossAmount
     */
    Double getGrossAmount();
}
