<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="projectQueriesView" />

<div class="col-md-6 panel-area panel-area-default">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="monthlySummary" />
                <span style="float: right;"> <o:out value="${view.report.monthDisplay}" /> / <o:out value="${view.report.yearDisplay}" />
                </span>
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <table class="table table-striped">
            <tr>
                <th class="summaryLegend"><c:choose>
                        <c:when test="${view.report.current}">
                            <v:link url="${view.baseLink}/forwardList" parameters="type=monthCreated">
                                <fmt:message key="incoming" />
                                <fmt:message key="currentMonth" />
                            </v:link>
                        </c:when>
                        <c:otherwise>
                            <v:link url="${view.baseLink}/forwardList" parameters="type=monthCreated">
                                <fmt:message key="incoming" />
                            </v:link>
                        </c:otherwise>
                    </c:choose></th>
                <th class="summaryValue right"><o:number value="${view.report.monthCreatedCapacity}" format="currency" /> EUR</th>
            </tr>
            <c:forEach var="obj" items="${view.report.monthCreatedReport}">
                <c:if test="${!empty obj.capacity and obj.capacity > 0}">
                    <tr>
                        <td class="summaryLegend"><v:link url="${view.baseLink}/forwardList" parameters="type=monthCreated&businessType=${obj.type.id}">
                                <o:out value="${obj.type.name}" />
                            </v:link></td>
                        <td class="summaryValue right"><o:number value="${obj.capacity}" format="currency" /> EUR</td>
                    </tr>
                </c:if>
            </c:forEach>
            <tr>
                <td class="summaryLegend"><v:link url="${view.baseLink}/forwardList" parameters="type=monthCreated&confirmed=false">
                        <fmt:message key="thereofFormalUnconfirmed" />
                    </v:link></td>
                <td class="summaryValue right error"><o:number value="${view.report.monthCreatedUnconfirmedCapacity}" format="currency" /> EUR</td>
            </tr>
        </table>

        <table class="table table-striped">
            <tr>
                <th class="summaryLegend"><c:choose>
                        <c:when test="${view.report.current}">
                            <v:link url="${view.baseLink}/forwardList" parameters="type=month">
                                <fmt:message key="overturn" />
                                <fmt:message key="currentMonth" />
                            </v:link>
                        </c:when>
                        <c:otherwise>
                            <v:link url="${view.baseLink}/forwardList" parameters="type=month">
                                <fmt:message key="overturn" />
                            </v:link>
                        </c:otherwise>
                    </c:choose></th>
                <th class="summaryValue right"><o:number value="${view.report.monthCapacity}" format="currency" /> EUR</th>
            </tr>
            <c:forEach var="obj" items="${view.report.monthReport}">
                <c:if test="${!empty obj.capacity and obj.capacity > 0}">
                    <tr>
                        <td class="summaryLegend"><v:link url="${view.baseLink}/forwardList" parameters="type=month&businessType=${obj.type.id}">
                                <o:out value="${obj.type.name}" />
                            </v:link></td>
                        <td class="summaryValue right"><o:number value="${obj.capacity}" format="currency" /> EUR</td>
                    </tr>
                </c:if>
            </c:forEach>
        </table>

        <table class="table table-striped">
            <tr>
                <th class="summaryLegend"><c:choose>
                        <c:when test="${view.report.current}">
                            <v:link url="${view.baseLink}/forwardListUnreferenced?type=monthUnreferenced">
                                <fmt:message key="others" />
                                <fmt:message key="currentMonth" />
                            </v:link>
                        </c:when>
                        <c:otherwise>
                            <v:link url="${view.baseLink}/forwardListUnreferenced?type=monthUnreferenced">
                                <fmt:message key="others" />
                            </v:link>
                        </c:otherwise>
                    </c:choose></th>
                <th class="summaryValue right"><o:number value="${view.report.monthUnreferencedCapacity}" format="currency" /> EUR</th>
            </tr>
            <c:if test="${view.report.monthUnreferencedInvoiceCapacity > 0}">
                <tr>
                    <td class="summaryLegend"><v:link url="${view.baseLink}/forwardListUnreferenced?type=monthUnreferencedInvoice">
                            <fmt:message key="invoices" />
                        </v:link></td>
                    <td class="summaryValue right"><o:number value="${view.report.monthUnreferencedInvoiceCapacity}" format="currency" /> EUR</td>
                </tr>
            </c:if>
            <c:if test="${view.report.monthUnreferencedCreditCapacity > 0}">
                <tr>
                    <td class="summaryLegend"><v:link url="${view.baseLink}/forwardListUnreferenced?type=monthUnreferencedCredit">
                            <fmt:message key="creditNotes" />
                        </v:link></td>
                    <td class="summaryValue right"><o:number value="${view.report.monthUnreferencedCreditCapacity}" format="currency" /> EUR</td>
                </tr>
            </c:if>
        </table>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="annualSummary" />
                <span style="float: right;"><o:out value="${view.report.yearDisplay}" /></span>
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <table class="table table-striped">
            <tr>
                <th class="summaryLegend"><c:choose>
                        <c:when test="${view.report.current}">
                            <v:link url="${view.baseLink}/forwardList" parameters="type=yearCreated">
                                <fmt:message key="incoming" />
                                <fmt:message key="currentYear" />
                            </v:link>
                        </c:when>
                        <c:otherwise>
                            <v:link url="${view.baseLink}/forwardList" parameters="type=yearCreated">
                                <fmt:message key="incoming" />
                            </v:link>
                        </c:otherwise>
                    </c:choose></th>
                <th class="summaryValue right"><o:number value="${view.report.yearCreatedCapacity}" format="currency" /> EUR</th>
            </tr>
            <c:forEach var="obj" items="${view.report.yearCreatedReport}">
                <c:if test="${!empty obj.capacity and obj.capacity > 0}">
                    <tr>
                        <td class="summaryLegend"><v:link url="${view.baseLink}/forwardList" parameters="type=yearCreated&businessType=${obj.type.id}">
                                <o:out value="${obj.type.name}" />
                            </v:link></td>
                        <td class="summaryValue right"><o:number value="${obj.capacity}" format="currency" /> EUR</td>
                    </tr>
                </c:if>
            </c:forEach>
            <tr>
                <td class="summaryLegend"><v:link url="${view.baseLink}/forwardList" parameters="type=yearCreated&confirmed=false">
                        <fmt:message key="thereofFormalUnconfirmed" />
                    </v:link></td>
                <td class="summaryValue right error"><o:number value="${view.report.yearCreatedUnconfirmedCapacity}" format="currency" /> EUR</td>
            </tr>
        </table>

        <table class="table table-striped">
            <tr>
                <th class="summaryLegend"><c:choose>
                        <c:when test="${view.report.current}">
                            <v:link url="${view.baseLink}/forwardList" parameters="type=year">
                                <fmt:message key="overturn" />
                                <fmt:message key="currentYear" />
                            </v:link>
                        </c:when>
                        <c:otherwise>
                            <v:link url="${view.baseLink}/forwardList" parameters="type=year">
                                <fmt:message key="overturn" />
                            </v:link>
                        </c:otherwise>
                    </c:choose></th>
                <th class="summaryValue right"><o:number value="${view.report.yearCapacity}" format="currency" /> EUR</th>
            </tr>
            <c:forEach var="obj" items="${view.report.yearReport}">
                <c:if test="${!empty obj.capacity and obj.capacity > 0}">
                    <tr>
                        <td class="summaryLegend"><v:link url="${view.baseLink}/forwardList" parameters="type=year&businessType=${obj.type.id}">
                                <o:out value="${obj.type.name}" />
                            </v:link></td>
                        <td class="summaryValue right"><o:number value="${obj.capacity}" format="currency" /> EUR</td>
                    </tr>
                </c:if>
            </c:forEach>
        </table>

        <table class="table table-striped">
            <tr>
                <th class="summaryLegend"><c:choose>
                        <c:when test="${view.report.current}">
                            <v:link url="${view.baseLink}/forwardListUnreferenced?type=yearUnreferenced">
                                <fmt:message key="others" />
                                <fmt:message key="currentYear" />
                            </v:link>
                        </c:when>
                        <c:otherwise>
                            <v:link url="${view.baseLink}/forwardListUnreferenced?type=yearUnreferenced">
                                <fmt:message key="others" />
                            </v:link>
                        </c:otherwise>
                    </c:choose></th>
                <th class="summaryValue right"><o:number value="${view.report.yearUnreferencedCapacity}" format="currency" /> EUR</th>
            </tr>
            <c:if test="${view.report.yearUnreferencedInvoiceCapacity > 0}">
                <tr>
                    <td class="summaryLegend"><v:link url="${view.baseLink}/forwardListUnreferenced?type=yearUnreferencedInvoice">
                            <fmt:message key="invoices" />
                        </v:link></td>
                    <td class="summaryValue right"><o:number value="${view.report.yearUnreferencedInvoiceCapacity}" format="currency" /> EUR</td>
                </tr>
            </c:if>
            <c:if test="${view.report.yearUnreferencedCreditCapacity > 0}">
                <tr>
                    <td class="summaryLegend"><v:link url="${view.baseLink}/forwardListUnreferenced?type=yearUnreferencedCredit">
                            <fmt:message key="creditNotes" />
                        </v:link></td>
                    <td class="summaryValue right"><o:number value="${view.report.yearUnreferencedCreditCapacity}" format="currency" /> EUR</td>
                </tr>
            </c:if>
        </table>

        <table class="table table-striped">
            <tr>
                <th class="summaryLegend"><fmt:message key="cancellationAndStopYear" /></th>
                <th class="summaryValue right"><o:number value="${view.report.cancelledCapacity + view.report.stoppedCapacity}" format="currency" /> EUR</th>
            </tr>
            <tr>
                <td class="summaryLegend"><v:link url="${view.baseLink}/forwardList" parameters="type=cancelled">
                        <fmt:message key="cancelled" />
                    </v:link></td>
                <td class="summaryValue right"><o:number value="${view.report.cancelledCapacity}" format="currency" /> EUR</td>
            </tr>
            <tr>
                <td class="summaryLegend"><v:link url="${view.baseLink}/forwardList" parameters="type=stopped">
                        <fmt:message key="stopped" />
                    </v:link></td>
                <td class="summaryValue right"><o:number value="${view.report.stoppedCapacity}" format="currency" /> EUR</td>
            </tr>
        </table>

    </div>

</div>
