/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 15, 2014 
 * 
 */
package com.osserp.core.model.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.osserp.common.Property;
import com.osserp.common.beans.AbstractOption;

import com.osserp.core.employees.Employee;
import com.osserp.core.system.SystemSetup;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SystemSetupImpl extends AbstractOption implements SystemSetup {

    private String currentStatus = "initial";
    private List<Property> setupPropertyList = new ArrayList<Property>();
    private Map<String, Property> _propertyMap = new HashMap<String, Property>();

    private volatile Employee admin = null;

    protected SystemSetupImpl() {
        super();
    }

    public Employee getAdmin() {
        return admin;
    }

    public void setAdmin(Employee newCreatedAdmin) {
        admin = newCreatedAdmin;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Map<String, Property> getPropertyMap() {
        if (_propertyMap.isEmpty() && !setupPropertyList.isEmpty()) {
            for (int i = 0, j = setupPropertyList.size(); i < j; i++) {
                Property next = setupPropertyList.get(i);
                _propertyMap.put(next.getName(), next);
            }
        }
        return _propertyMap;
    }

    public String getProperty(String name) {
        Property property = getPropertyMap().get(name);
        return property == null ? null : property.getValue();
    }

    public void updateProperty(Long user, String name, String value) {
        if (!updatePropertyValue(name, value)) {
            setupPropertyList.add(new SystemSetupPropertyImpl(this, name, value, user));
        }
    }
    
    public void removeProperty(Long user, String name) {
        for (Iterator<Property> i = setupPropertyList.iterator(); i.hasNext();) {
            Property next = i.next();
            if (next.getName().equals(name)) {
                i.remove();
                break;
            }
        }
    }

    public void resetInput() {
        for (Iterator<Property> i = setupPropertyList.iterator(); i.hasNext();) {
            Property next = i.next();
            if (!next.isSetupOnly() && !next.isUnchangeable()) {
                i.remove();
            }
        }
    }

    private boolean updatePropertyValue(String name, String value) {
        for (int i = 0, j = setupPropertyList.size(); i < j; i++) {
            Property next = setupPropertyList.get(i);
            if (next.getName().equals(name)) {
                next.setValue(value);
                return true;
            }
        }
        return false;
    }

    protected List<Property> getSetupPropertyList() {
        return setupPropertyList;
    }

    protected void setSetupPropertyList(List<Property> setupPropertyList) {
        this.setupPropertyList = setupPropertyList;
    }
}
