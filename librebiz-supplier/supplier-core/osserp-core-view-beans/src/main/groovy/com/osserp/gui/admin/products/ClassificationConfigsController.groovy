/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 25, 2011 4:27:02 PM 
 * 
 */
package com.osserp.gui.admin.products

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException

import com.osserp.groovy.web.AdminViewController

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/admin/products/classificationConfigs/*")
@Controller class ClassificationConfigsController extends AdminViewController {
    private static Logger logger = LoggerFactory.getLogger(ClassificationConfigsController.class.getName())

    @RequestMapping
    def removeType(HttpServletRequest request) {
        logger.debug("removeType: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        try {
            view.removeType()
        } catch (ClientException c) {
            String target = redirect(request, '/admin/products/productListClassificationConfigurator/forward?view=classificationConfigsView&exit=/admin/products/classificationConfigs/reload')
            logger.debug("removeType: caught exception [target=${target}]")
            return target
        }
        return defaultPage
    }

    @RequestMapping
    def selectGroup(HttpServletRequest request) {
        logger.debug("selectGroup: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.selectGroup()
        return defaultPage
    }

    @RequestMapping
    def selectCategory(HttpServletRequest request) {
        logger.debug("selectCategory: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.selectCategory()
        return defaultPage
    }

    @RequestMapping
    def moveGroupUp(HttpServletRequest request) {
        logger.debug("moveGroupUp: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.moveGroupUp()
        return defaultPage
    }

    @RequestMapping
    def moveGroupDown(HttpServletRequest request) {
        logger.debug("moveGroupDown: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.moveGroupDown()
        return defaultPage
    }

    @RequestMapping
    def addGroup(HttpServletRequest request) {
        logger.debug("addGroup: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.addGroup()
        return defaultPage
    }

    @RequestMapping
    def removeGroup(HttpServletRequest request) {
        logger.debug("removeGroup: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        try {
            view.removeGroup()
        } catch (ClientException c) {
            String target = redirect(request, '/admin/products/productListClassificationConfigurator/forward?view=classificationConfigsView&exit=/admin/products/classificationConfigs/reload')
            logger.debug("removeGroup: caught exception [target=${target}]")
            return target
        }

        return defaultPage
    }

    @RequestMapping
    def moveCategoryUp(HttpServletRequest request) {
        logger.debug("moveCategoryUp: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.moveCategoryUp()
        return defaultPage
    }

    @RequestMapping
    def moveCategoryDown(HttpServletRequest request) {
        logger.debug("moveCategoryDown: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.moveCategoryDown()
        return defaultPage
    }

    @RequestMapping
    def addCategory(HttpServletRequest request) {
        logger.debug("addCategory: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.addCategory()
        return defaultPage
    }

    @RequestMapping
    def removeCategory(HttpServletRequest request) {
        logger.debug("removeCategory: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        try {
            view.removeCategory()
        } catch (ClientException c) {
            String target = redirect(request, '/admin/products/productListClassificationConfigurator/forward?view=classificationConfigsView&exit=/admin/products/classificationConfigs/reload')
            logger.debug("removeCategory: caught exception [target=${target}]")
            return target
        }
        return defaultPage
    }

    @RequestMapping
    def assignCategoryNumberRange(HttpServletRequest request) {
        logger.debug("assignCategoryNumberRange: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.assignCategoryNumberRange()
        return defaultPage
    }

    @RequestMapping
    def assignGroupNumberRange(HttpServletRequest request) {
        logger.debug("assignGroupNumberRange: invoked [user=${getUserId(request)}]")
        ClassificationConfigsView view = getView(request)
        view.assignGroupNumberRange()
        return defaultPage
    }
}

