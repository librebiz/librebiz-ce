/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Aug-2006 15:29:50 
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;
import com.osserp.core.sales.SalesOrderVolumeExportDelivery;
import com.osserp.core.sales.SalesOrderVolumeExportItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderVolumeExportItemImpl extends AbstractEntity
        implements SalesOrderVolumeExportItem {
    private Long orderId = null;
    private boolean completed = false;
    private List<SalesOrderVolumeExportDelivery> deliveries = new ArrayList<SalesOrderVolumeExportDelivery>();

    protected SalesOrderVolumeExportItemImpl() {
        super();
    }

    public SalesOrderVolumeExportItemImpl(Long referenceId, Long orderId) {
        super(null, referenceId);
        this.orderId = orderId;
    }

    public void addDeliveries(Order order, List<Long> deliveryList) {
        for (int i = 0, j = deliveryList.size(); i < j; i++) {
            Long next = deliveryList.get(i);
            deliveries.add(new SalesOrderVolumeExportDeliveryImpl(this, next));
        }
    }

    public List<DeliveryNote> getOpen(Order order) {
        List<DeliveryNote> result = new ArrayList<DeliveryNote>();
        for (int i = 0, j = order.getDeliveryNotes().size(); i < j; i++) {
            DeliveryNote next = order.getDeliveryNotes().get(i);
            if (!added(next)) {
                result.add(next);
            }
        }
        return result;
    }

    public boolean added(DeliveryNote note) {
        for (int i = 0, j = deliveries.size(); i < j; i++) {
            SalesOrderVolumeExportDelivery next = deliveries.get(i);
            if (note.getId().equals(next.getId())) {
                return true;
            }
        }
        return false;
    }

    public Long getOrderId() {
        return orderId;
    }

    protected void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public boolean isCompleted() {
        return completed;
    }

    protected void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public List<SalesOrderVolumeExportDelivery> getDeliveries() {
        return deliveries;
    }

    protected void setDeliveries(List<SalesOrderVolumeExportDelivery> deliveries) {
        this.deliveries = deliveries;
    }
}
