<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="purchaseOrderSearchView" />
<c:set var="displayPrice" value="false" />
<c:if test="${view.displayPricePermission}">
    <c:set var="displayPrice" value="true" />
</c:if>
<style type="text/css">
    .table .unreleased {
        font-style: italic;
    }
</style>
<div class="table-responsive table-responsive-default">
    <table class="table">
        <thead>
            <tr>
                <th><fmt:message key="number" /></th>
                <th><fmt:message key="company" /></th>
                <th class="right"><v:sortLink key="created"><fmt:message key="ordered" /></v:sortLink></th>
                <th class="right"><fmt:message key="sent" /></th>
                <th class="right"><v:sortLink key="delivery"><fmt:message key="delivery" /></v:sortLink></th>
                <th class="center"><fmt:message key="shortEmployeeLabel" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="record" items="${view.list}" varStatus="s">
                <tr id="record${record.id}" class="altrow<c:if test="${!record.unchangeable}"> unreleased</c:if>">
                    <td>
                        <v:link url="${view.baseLink}/select?id=${record.id}&exitId=record${record.id}">
                            <c:choose>
                                <c:when test="${empty record.reference}">
                                    <o:out value="${record.number}" />
                                </c:when>
                                <c:otherwise>
                                    <o:out value="${record.referenceNumber}" />
                                </c:otherwise>
                            </c:choose>
                        </v:link>
                    </td>
                    <td><o:out value="${record.contactName}" /></td>
                    <td class="right"><o:date value="${record.created}" /></td>
                    <td class="right"><o:date value="${record.dateSent}" /></td>
                    <td class="right"><o:date value="${record.delivery}" /></td>
                    <td class="center">
                        <o:ajaxLink linkId="${'createdBy'}${record.id}" url="${'/employeeInfo.do?id='}${record.createdBy}">
                            <oc:employee initials="true" value="${record.createdBy}" />
                        </o:ajaxLink>
                    </td>
                </tr>
                <c:forEach var="item" items="${record.items}">
                    <tr>
                        <td><a href="<c:url value="/products.do?method=load&id=${item.productId}&exit=purchaseOrderSearchDisplay&exitId=record${record.id}"/>"><o:out value="${item.productId}" /></a></td>
                        <td><o:out value="${item.name}" /></td>
                        <td class="right"><o:number value="${item.quantity}" format="currency" /></td>
                        <td>&nbsp;</td>
                        <td class="right"><o:date value="${item.delivery}" /></td>
                        <td>&nbsp;</td>
                    </tr>
                </c:forEach>
            </c:forEach>
        </tbody>
    </table>
</div>
