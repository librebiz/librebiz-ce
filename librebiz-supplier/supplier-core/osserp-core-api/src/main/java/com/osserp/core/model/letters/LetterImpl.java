/**
 *
 * Copyright (C) 2008, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 9, 2008 10:25:37 PM 
 * 
 */
package com.osserp.core.model.letters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Status;
import com.osserp.common.util.StringUtil;

import com.osserp.core.Address;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterInfo;
import com.osserp.core.dms.LetterParagraph;
import com.osserp.core.dms.LetterParameter;
import com.osserp.core.dms.LetterTemplate;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LetterImpl extends AbstractLetterContent implements Letter {
    private static Logger log = LoggerFactory.getLogger(LetterImpl.class.getName());

    private Long businessId = null;
    private Status status = null;
    private Date printDate = null;

    private List<LetterInfo> infos = new ArrayList<>();

    protected LetterImpl() {
        super();
    }

    protected LetterImpl(Letter other) {
        super(other);
        this.status = other.getStatus();
        this.infos = other.getInfos();
        this.businessId = other.getBusinessId();
    }

    /**
     * Creates a new letter
     * @param id for the new letter
     * @param initialStatus
     * @param user creating the letter
     * @param type of the letter
     * @param name of the letter
     * @param contact as recipient
     * @param businessId if associated (optional)
     * @param branchId
     * @param template
     */
    public LetterImpl(
            Long id,
            Status initialStatus,
            Employee user,
            LetterType type,
            String name,
            ClassifiedContact contact,
            Long businessId,
            Long branchId,
            LetterTemplate template) {

        super(id, contact.getId(), user, type, name, branchId);
        this.businessId = businessId;
        this.status = initialStatus;
        this.printDate = getCreated();

        String salutationDisplay = contact.getSalutationDisplay();
        if (salutationDisplay != null) {
            setSalutation(salutationDisplay + ",");
        }
        if (contact.getSalutation() != null
                && contact.getSalutation().getId() != null
                && contact.getSalutation().getId() != 0) {
            getAddress().setHeader(contact.getSalutation().getName());
        }
        getAddress().setName(contact.getName());
        Address ca = contact.getAddress();
        if (ca != null) {
            getAddress().setStreet(ca.getStreet());
            getAddress().setZipcode(ca.getZipcode());
            getAddress().setCity(ca.getCity());
            getAddress().setCountry(ca.getCountry());
        }
        if (type.getInfoKeys() != null) {
            List<String> labels = StringUtil.getTokenList(type.getInfoKeys(), StringUtil.COMMA);
            for (int i = 0, j = labels.size(); i < j; i++) {
                String label = labels.get(i);
                LetterInfo info = new LetterInfoImpl(this, label);
                infos.add(info);
            }
        }
        if (type.getParameterKeys() != null) {
            List<String> labels = StringUtil.getTokenList(type.getParameterKeys(), StringUtil.COMMA);
            for (int i = 0, j = labels.size(); i < j; i++) {
                String label = labels.get(i);
                LetterParameter param = new LetterParameterImpl(this, label);
                getParameters().add(param);
            }
        }
        if (template != null) {
            if (isSet(template.getSubject())) {
                setSubject(template.getSubject());
            }
            for (int i = 0, j = template.getParagraphs().size(); i < j; i++) {
                LetterParagraph templateParagraph = template.getParagraphs().get(i);
                LetterParagraph values = new LetterParagraphImpl(this, user, templateParagraph);
                getParagraphs().add(values);
            }
            if (branchId == null && template.getBranchId() != null) {
                setBranchId(template.getBranchId());
            }
            if (template.getGreetings() != null) {
                setGreetings(template.getGreetings());
            }
            if (template.getLanguage() != null) {
                setLanguage(template.getLanguage());
            }
            setIgnoreSalutation(template.isIgnoreSalutation());
            setIgnoreGreetings(template.isIgnoreGreetings());
            setIgnoreHeader(template.isIgnoreHeader());
            setIgnoreSubject(template.isIgnoreSubject());
            setEmbedded(template.isEmbedded());
            setEmbeddedAbove(template.isEmbeddedAbove());
            setEmbeddedSpaceAfter(template.getEmbeddedSpaceAfter());
            setContentKeepTogether(template.getContentKeepTogether());
        }
        if (log.isDebugEnabled()) {
            StringBuilder bf = new StringBuilder(128);
            bf.append("<init> done [user=").append(user.getId())
                    .append(", type=").append(type.getId())
                    .append(", reference=").append(contact.getId());
            if (businessId != null) {
                bf.append(", businessId=").append(businessId);
            }
            if (template != null) {
                bf.append(", template=").append(template.getId());
            }
            bf.append("]");
            log.debug(bf.toString());
        }
    }

    public Long getBusinessId() {
        return businessId;
    }

    protected void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getPrintDate() {
        return printDate;
    }

    public void setPrintDate(Date printDate) {
        this.printDate = printDate;
    }

    public void update(
            Employee user,
            String header,
            String name,
            String street,
            String zipcode,
            String city,
            Long country,
            String subject,
            String salutation,
            String greetings,
            String language,
            Long signatureLeft,
            Long signatureRight,
            Date printDate,
            boolean ignoreSalutation,
            boolean ignoreGreetings,
            boolean ignoreHeader,
            boolean ignoreSubject,
            boolean embedded,
            boolean embeddedAbove,
            String embeddedSpaceAfter,
            String contentKeepTogether)
            throws ClientException {

        super.update(
                user,
                subject,
                salutation,
                greetings,
                language,
                signatureLeft,
                signatureRight,
                ignoreSalutation,
                ignoreGreetings,
                ignoreHeader,
                ignoreSubject,
                embedded,
                embeddedAbove,
                embeddedSpaceAfter,
                contentKeepTogether);
        super.updateAdress(
                header,
                name,
                street,
                zipcode,
                city,
                country);
        this.printDate = printDate;
    }

    public List<LetterInfo> getInfos() {
        return infos;
    }

    protected void setInfos(List<LetterInfo> infos) {
        this.infos = infos;
    }

    @Override
    public void addParagraph(Employee user) {
        LetterParagraph p = new LetterParagraphImpl(this, user);
        getParagraphs().add(p);
    }

    public boolean isClosed() {
        return (status == null ? false : status.isClosing());
    }

}
