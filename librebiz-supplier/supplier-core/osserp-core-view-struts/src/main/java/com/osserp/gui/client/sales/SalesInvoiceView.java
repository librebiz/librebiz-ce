/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 28, 2006 9:44:25 AM 
 * 
 */
package com.osserp.gui.client.sales;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.BookingType;

import com.osserp.gui.client.records.RebateAwareRecordView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesInvoiceView")
public interface SalesInvoiceView extends RebateAwareRecordView {

    /**
     * Loads record for display
     * @param id
     * @param exitTarget
     * @param exitId
     * @param readonly
     * @throws ClientException if record no longer exists
     */
    public void loadDownpayment(Long id, String exitTarget, String exitId, boolean readonly) throws ClientException;

    /**
     * Checks if sales invoice is cancellable
     * @throws ClientException if record is not cancellable
     */
    public void checkCancellation() throws ClientException;

    /**
     * Checks if record is deletable by user
     * @throws PermissionException if user has no permission
     */
    public void checkDelete() throws PermissionException;

    /**
     * Provides customer credit amount if invoice was canceled after payments were done
     * @return canceledCustomerCredit
     */
    public Double getCanceledCustomerCredit();

    /**
     * Adds an outpayment to invoice if cancelled
     * @throws ClientException
     */
    public void payoutCancelled() throws ClientException;

    /**
     * Restores an accidentally canceled downpayment
     * @throws ClientException
     */
    public void restoreDownpayment() throws ClientException;

    /**
     * Indicates if the number of the record is changeable.
     * @return true if user has permission and record status meets all requirements
     */
    public boolean isRecordNumberChangeable();

    /**
     * Changes the record number
     * @param form with recordId
     * @throws ClientException if number change failed
     */
    public void changeRecordNumber(Form form) throws ClientException;

    /**
     * Indicates that view is in volume mode
     * @return true if so
     */
    public boolean isVolumeView();

    /**
     * Changes customer
     * @param id of the customer
     * @throws ClientException
     */
    public void changeCustomer(Long id) throws ClientException;

    /**
     * Enables/disables historical mode
     */
    public void switchHistorical() throws PermissionException;

    /**
     * Provides available credit note booking types
     * @return the creditNoteBookingTypes
     */
    public List<BookingType> getCreditNoteBookingTypes();
}
