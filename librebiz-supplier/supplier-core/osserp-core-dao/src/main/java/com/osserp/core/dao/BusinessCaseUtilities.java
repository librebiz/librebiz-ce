/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2012 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.Option;
import com.osserp.common.Status;

import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.contacts.ContactCountry;
import com.osserp.core.crm.Campaign;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessCaseUtilities {
    
    /**
     * Indicates if a system property is enabled
     * @param property
     * @return true if systemProperty available and 'true'
     */
    boolean isPropertyEnabled(String property);
    
    /**
     * Provides the value of a system property defined as long
     * @param property
     * @return long value or null if not exists
     */
    Long getPropertyId(String property);

    /**
     * Provides the default business status entity (e.g. Status.CREATED)
     * @return status
     */
    Status getBusinessStatusDefault();

    /**
     * Provides available business status entities
     * @return status list
     */
    List<Status> getBusinessStatusList();
    
    /**
     * Tries to fetch a business type either by id, name or key
     * @param id
     * @param name
     * @return businessType or null if not exists
     */
    BusinessType fetchRequestType(Long id, String name);

    /**
     * Tries to fetch a business type by external status.
     * @param externalStatus
     * @return businessType or not found or no default exists
     */
    BusinessType fetchRequestType(String externalStatus);
    
    /**
     * Tries to fetch a branch office either by id, name, shortname or shortkey
     * @param id
     * @param name
     * @param required
     * @return branchOffice or null if not exists and required is true.
     * Provides default branch if required is false and no branch was found.
     */
    BranchOffice fetchBranch(Long id, String name, boolean required);
    
    /**
     * Tries to fetch a campaign either by id or name. A new campaign will be
     * created if name was provided but not found and a systemProperty with 
     * provided name is enabled. The implementation class must also support
     * on the fly campaign creation. 
     * @param id
     * @param name
     * @param createNonExistingPropertyName the name of the systemProperty
     * indicating if non-existing campaign should be created if enabled
     * @param defaultCampaignPropertyName  the name of the systemProperty
     * providing a default campaign id if non provided
     * @return campaign or null if not found and not created
     */
    Campaign fetchCampaign(Long id, String name, String createNonExistingPropertyName, String defaultCampaignPropertyName);
    
    /**
     * Tries to find the country by id, name and country code. The method 
     * ignores case on countryCode so 'en' and 'EN' will match
     * @param id
     * @param name
     * @return country or null if not exists
     */
    ContactCountry fetchCountry(Long id, String name);
    
    /**
     * Tries to fetch a campaign either by id or name. A new origin will be
     * created if name was provided but not found and a systemProperty with 
     * provided name is enabled. Creating new origins on the fly should be
     * supported by every implementation.  
     * @param id
     * @param name
     * @param createNonExistingPropertyName the name of the systemProperty
     * indicating if non-existing origin should be created if enabled
     * @return origin or null if not found and not created
     */
    Option fetchOrigin(Long id, String name, String createNonExistingPropertyName);

    /**
     * Tries to fetch a campaign either by id or name.
     * @param context 'sales' or 'request'
     * @param businessType
     * @param id
     * @param name
     * @return action or null if not exists
     */
    FcsAction fetchFcsAction(String context, BusinessType businessType, Long id, String name);

}
