/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 10, 2006 12:59:01 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.dao.AbstractDao;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.PhoneUtil;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.dao.Phones;
import com.osserp.core.dao.TableKeys;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PhonesDao extends AbstractTablesAwareSpringDao implements Phones {

    public PhonesDao(JdbcTemplate jdbcTemplate, Tables tables) {
        super(jdbcTemplate, tables);
    }

    /**
     * Provides the primary phone of given contact
     * @param contactId
     * @return number or null if none exists
     */
    public String getPrimaryPhone(Long contactId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT country,prefix,number,isprimary FROM ")
                .append(getTable(TableKeys.PHONES))
                .append(" WHERE contact_id = ? AND device_id = ?");
        final Object[] params = { contactId, PhoneType.PHONE };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        List result = (List) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getValueReader());
        int size = ((result == null) ? 0 : result.size());
        if (result == null || size == 0) {
            return null;
        }
        if (size == 1) {
            PhoneValues p = (PhoneValues) result.get(0);
            return PhoneUtil.createInternational(p.country, p.prefix, p.number);
        }
        for (Iterator i = result.iterator(); i.hasNext();) {
            PhoneValues p = (PhoneValues) i.next();
            if (p.primary) {
                return PhoneUtil.createInternational(p.country, p.prefix, p.number);
            }
        }
        PhoneValues p = (PhoneValues) result.get(0);
        return PhoneUtil.createInternational(p.country, p.prefix, p.number);
    }

    /**
     * Provides the primary numebr of given type as formatted string
     * @param contactId
     * @param type of the device to lookup for
     * @return formatted number
     */
    public String getPrimaryNumber(Long contactId, Long type) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT country,prefix,number,isprimary FROM ")
                .append(getTable(TableKeys.PHONES))
                .append(" WHERE contact_id = ? AND device_id = ?");
        final Object[] params = { contactId, type };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        List result = (List) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getValueReader());
        int size = ((result == null) ? 0 : result.size());
        if (result == null || size == 0) {
            return null;
        }
        if (size == 1) {
            PhoneValues p = (PhoneValues) result.get(0);
            return PhoneUtil.createInternational(p.country, p.prefix, p.number);
        }
        for (Iterator i = result.iterator(); i.hasNext();) {
            PhoneValues p = (PhoneValues) i.next();
            if (p.primary) {
                return PhoneUtil.createInternational(p.country, p.prefix, p.number);
            }
        }
        PhoneValues p = (PhoneValues) result.get(0);
        return PhoneUtil.createInternational(p.country, p.prefix, p.number);
    }

    protected RowMapperResultSetExtractor getValueReader() {
        return new RowMapperResultSetExtractor(new ValueRowMapper());
    }

    private class ValueRowMapper extends AbstractDao implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new PhoneValues(
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getBoolean(4));
        }
    }

    protected class PhoneValues {
        String country = null;
        String prefix = null;
        String number = null;
        boolean primary = false;

        public PhoneValues(
                String country,
                String prefix,
                String number,
                boolean primary) {

            this.country = country;
            this.prefix = prefix;
            this.number = number;
            this.primary = primary;
        }
    }

}
