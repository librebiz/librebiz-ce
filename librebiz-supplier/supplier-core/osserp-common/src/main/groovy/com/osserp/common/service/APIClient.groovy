/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.osserp.common.service

import java.net.http.HttpClient
import java.net.http.HttpHeaders
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.Duration

import groovy.json.JsonOutput

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.apache.commons.codec.binary.Base64

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class APIClient {

    private static Logger logger = LoggerFactory.getLogger(APIClient.class)

    protected static final long CONNECTION_TIMEOUT = 10
    protected static final String EMPTY_JSON = '{}'
    protected static final String USER_AGENT = 'Java-http-client/11'

    APIConfig apiConfig
    HttpClient httpClient
    String authHeader

    APIClient(APIConfig config) {
        super();
        apiConfig = config
        httpClient = createHttpClient()
        authHeader = createAuthHeader()
    }

    protected APIResponse execute(HttpRequest request) {
        APIResponse result = new APIResponse()
        try {
            HttpResponse<String> response = httpClient.send(
                request, HttpResponse.BodyHandlers.ofString())
            String apiVersion = getVersion(response)
            result.headers = response.headers().map()
            result.status = response.statusCode()
            if (ok(response)) {
                String jsonString = response.body()
                if (jsonString) {
                    result.apiVersion = apiVersion
                    result.json = jsonString
                    if (apiConfig.logResponse && logger.infoEnabled) {
                        logger.info(JsonOutput.prettyPrint(jsonString))
                    }
                }
            }
        } catch (Exception e) {
            result.errorMessage = e.message
            logger.error(e.message, e)
        }
        return result
    }    

    protected HttpRequest createGet(String url) {
        HttpRequest request = HttpRequest.newBuilder()
            .GET()
            .uri(URI.create("${apiConfig.serviceUri}${url}"))
            .setHeader('User-Agent', USER_AGENT)
            .setHeader('Authorization', authHeader)
            .build()
        return request 
    }

    protected HttpRequest createPatch(String url, String json) {
        HttpRequest request = HttpRequest.newBuilder()
            .method('PATCH', HttpRequest.BodyPublishers.ofString(json))
            .uri(URI.create("${apiConfig.serviceUri}${url}"))
            .setHeader('User-Agent', USER_AGENT)
            .setHeader('Authorization', authHeader)
            .header('Content-Type', 'application/json')
            .build()
        return request
    }

    protected HttpRequest createPost(String url, String json) {
        HttpRequest request = HttpRequest.newBuilder()
            .POST(HttpRequest.BodyPublishers.ofString(json))
            .uri(URI.create("${apiConfig.serviceUri}${url}"))
            .setHeader('User-Agent', USER_AGENT)
            .setHeader('Authorization', authHeader)
            .header('Content-Type', 'application/json')
            .build()
        return request
    }

    protected HttpRequest createPut(String url, String json) {
        HttpRequest request = HttpRequest.newBuilder()
            .PUT(HttpRequest.BodyPublishers.ofString(json))
            .uri(URI.create("${apiConfig.serviceUri}${url}"))
            .setHeader('User-Agent', USER_AGENT)
            .setHeader('Authorization', authHeader)
            .header('Content-Type', 'application/json')
            .build()
        return request 
    }

    protected HttpClient createHttpClient() {
        HttpClient httpClient = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(CONNECTION_TIMEOUT))
            .version(HttpClient.Version.HTTP_1_1)
            .followRedirects(HttpClient.Redirect.NEVER)
            .build()
        return httpClient
    }

    protected String createAuthHeader() {
        assert (apiConfig.username && apiConfig.password)
        String plainCredentials = "${apiConfig.username}:${apiConfig.password}"
        String base64Credentials = new String(
            Base64.encodeBase64(plainCredentials.getBytes()))
        return "Basic ${base64Credentials}"
    }

    protected String getVersion(HttpResponse<String> response) {
        if (apiConfig.apiVersionHeader) {
            HttpHeaders headers = response.headers()
            return headers.firstValue(apiConfig.apiVersionHeader)
        }
        return 'unknown'
    }

    protected boolean ok(HttpResponse<String> response) {
        return !response ? false :
            (response.statusCode() >= 200
                && response.statusCode() < 400) 
    }

}
