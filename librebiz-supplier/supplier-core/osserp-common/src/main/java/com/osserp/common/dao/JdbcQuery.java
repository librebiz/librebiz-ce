/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 25, 2007 7:04:51 PM 
 * 
 */
package com.osserp.common.dao;

import java.util.List;

import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface JdbcQuery extends Option {

    static final String BOOLEAN = "boolean";
    static final String DOUBLE = "double";
    static final String INTEGER = "int";
    static final String LONG = "long";
    static final String STRING = "string";
    static final String TIMESTAMP = "timestamp";

    static final String OUTPUT_ROOT = "root";
    static final String OUTPUT_ERROR = "error";
    static final String OUTPUT_HEADERS = "headers";
    static final String OUTPUT_DATA = "data";
    static final String OUTPUT_ITEM = "item";
    static final String OUTPUT_ATTRIBUTE = "attribute";
    static final String OUTPUT_HEAD = "head";

    /**
     * Provides available type names for parameters
     * @return availableTypes
     */
    List<String> getAvailableTypes();

    /**
     * Indicates that query is parameterized
     * @return true if so
     */
    boolean isParameterized();

    /**
     * Provides the query
     * @return query
     */
    String getQuery();

    /**
     * Updates query values
     * @param name
     * @param title
     * @param query
     * @throws ClientException if name, title or query not set or query is not valid
     */
    void update(String name, String title, String query) throws ClientException;

    /**
     * Provides the parameters
     * @return parameters
     */
    List<JdbcParameter> getParameters();

    /**
     * Fetches a well known paramter from parameters
     * @param id
     * @return fetched jdbc parameter
     */
    JdbcParameter fetchParameter(Long id);

    /**
     * Adds a new paramter
     * @param label
     * @param name
     * @param type
     * @throws ClientException if param already exists
     */
    void addParameter(String label, String name, String type) throws ClientException;

    /**
     * Updates a parameter
     * @param id of the param
     * @param label
     * @param name
     * @param type
     * @throws ClientException if parameter already exists
     */
    void updateParameter(Long id, String label, String name, String type) throws ClientException;

    /**
     * Removes all parameters
     */
    void clearParameters();

    /**
     * Sets the parameter value to exucute a query
     * @param id of the paramter
     * @param value to convert to specified object
     * @throws ClientException if value is invalid
     */
    void setParameterValue(Long id, String value) throws ClientException;

    /**
     * Provides all parameters values as object array
     * @return parameterValues
     */
    Object[] getParameterValues();

    /**
     * Provides the jdbc types of the parameters
     * @return parameterTypes
     */
    public int[] getParameterTypes();

    /**
     * Provides the parameter xml
     * @param cache
     * @return doc
     */
    Document getParameterXML(OptionsCache cache);

    /**
     * Provides the output title
     * @return outputTitle
     */
    String getOutputTitle();

    /**
     * Provides the output headers and types
     * @return outputs
     */
    List<JdbcOutput> getOutputs();

    /**
     * Fetches a well known output from outputs
     * @param id
     * @return fetched jdbc output
     */
    JdbcOutput fetchOutput(Long id);

    /**
     * Adds a new ouput column
     * @param name
     * @param type
     * @param width
     * @param alignment
     * @param display
     */
    void addOutput(
            String name,
            String type,
            Integer width,
            String alignment,
            boolean display);

    /**
     * Updates an output column
     * @param id to update
     * @param name
     * @param type
     * @param width
     * @param alignment
     * @param display
     */
    void updateOutput(
            Long id,
            String name,
            String type,
            Integer width,
            String alignment,
            boolean display);

    /**
     * Removes all output columns
     */
    void clearOutput();

    /**
     * Adds the query execution result
     */
    void addResult(List<String[]> result) throws QueryException;

    /**
     * Provides the result of the query
     * @return document
     */
    Document getResult();

    /**
     * Provides the result as HTML table content
     * @return htmlTableContent
     */
    String getResultTableContent();

    /**
     * Provides the result in format compatible with spreadsheet apps (tab separated strings with new line on each row)
     * @return spreadsheet
     */
    String getSpreadSheet();

    /**
     * Indicates if output should be displayed on full screen
     * @return true if so
     */
    boolean isFullscreen();

    /**
     * Toggle between fullscreen on/off
     */
    void switchFullscreen();

    /**
     * Indicates that query is marked as deleted
     * @return true if so
     */
    boolean isDeleted();

    /**
     * Sets as deleted
     * @param deleted
     */
    void setDeleted(boolean deleted);

    /**
     * Provides rquired permissions to perform query
     * @return permissions
     */
    String[] getPermissions();
}
