/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 29, 2008 3:44:27 PM 
 * 
 */
package com.osserp.gui.client.records.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.util.AddressValidator;
import com.osserp.common.web.Form;

import com.osserp.core.Address;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.PaymentAwareRecordManager;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordCorrection;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.finance.RecordPrintManager;
import com.osserp.core.model.AddressImpl;

import com.osserp.gui.client.records.CorrectionAwareRecordView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractCorrectionAwareRecordView extends AbstractRecordView
        implements CorrectionAwareRecordView {
    private static Logger log = LoggerFactory.getLogger(AbstractCorrectionAwareRecordView.class.getName());

    private Address address = null;
    private boolean correctionCreateMode = false;
    private boolean correctionEditMode = false;

    protected AbstractCorrectionAwareRecordView() {
        super();
    }

    @Override
    public boolean isProvidingCorrections() {
        return true;
    }

    public Address getAddress() {
        return address;
    }

    protected void setAddress(Address address) {
        this.address = address;
    }

    public boolean isCorrectionCreateMode() {
        return correctionCreateMode;
    }

    public void setCorrectionCreateMode(boolean correctionCreateMode) {
        this.correctionCreateMode = correctionCreateMode;
    }

    public boolean isCorrectionEditMode() {
        return correctionEditMode;
    }

    public void setCorrectionEditMode(boolean correctionEditMode) {
        this.correctionEditMode = correctionEditMode;
    }

    public DocumentData printCorrection(Long id) throws ClientException {
        PaymentAwareRecord record = getPaymentAwareRecord();
        RecordCorrection correction = record.getCorrection();
        if (correction == null) {
            throw new ClientException(ErrorCode.CORRECTION_MISSING);
        }
        RecordPrintManager printManager = getPrintManager();
        DocumentData pdf;
        try {
            pdf = printManager.getCorrectionPdf(record.getCorrection());
        } catch (ClientException c) {
            pdf = printManager.createCorrectionPdf(
                    getDomainEmployee(), record, Record.STAT_NEW);
        }
        return pdf;
    }

    public void releaseCorrection() throws ClientException {
        PaymentAwareRecord record = getPaymentAwareRecord();
        RecordCorrection correction = record.getCorrection();
        if (correction == null) {
            throw new ClientException(ErrorCode.CORRECTION_MISSING);
        }
        RecordPrintManager printManager = getPrintManager();
        DocumentData pdf = printManager.createCorrectionPdf(
                getDomainEmployee(), record, Record.STAT_SENT);
        record.releaseCorrection();
        getRecordManager().persist(record);
        if (log.isDebugEnabled()) {
            log.debug("releaseCorrection() done [record=" + record.getId()
                    + ", size=" + (pdf.getBytes() == null ? "null" 
                            : pdf.getBytes().length) + "]");
        }
        refresh();
    }

    public void saveCorrection(Form form) throws ClientException {
        PaymentAwareRecord record = getPaymentAwareRecord();
        Long status = Record.STAT_CHANGED;
        if (correctionEditMode && record.getCorrectionCount() > 0) {
            updateCorrection(form, record);
        } else if (correctionCreateMode) {
            addCorrection(form, record);
            status = Record.STAT_NEW;
        }
        RecordPrintManager printManager = getPrintManager();
        printManager.createCorrectionPdf(getDomainEmployee(), record, status);
        correctionCreateMode = false;
        correctionEditMode = false;
    }

    private void addCorrection(Form form, PaymentAwareRecord record) throws ClientException {
        this.address = populateAddress(form);
        this.validateAddress(address);
        PaymentAwareRecordManager manager = getCorrectionAwareRecordManager();
        manager.addCorrection(
                getDomainEmployee(),
                record,
                form.getString("name"),
                this.address.getStreet(),
                this.address.getStreetAddon(),
                this.address.getZipcode(),
                this.address.getCity(),
                this.address.getCountry(),
                this.address.getNote());
        refresh();
        if (log.isDebugEnabled()) {
            log.debug("addCorrection() done [record=" + record.getId() + "]");
        }
    }

    private void updateCorrection(Form form, PaymentAwareRecord record) throws ClientException {
        RecordCorrection current = record.getCorrection();
        if (current != null) {
            this.address = populateAddress(form);
            this.validateAddress(address);
            record.updateCorrection(
                    getDomainEmployee(),
                    current.getId(),
                    this.address.getName(),
                    this.address.getStreet(),
                    this.address.getStreetAddon(),
                    this.address.getZipcode(),
                    this.address.getCity(),
                    this.address.getCountry(),
                    this.address.getNote());
            getRecordManager().persist(record);

        }
        refresh();
        if (log.isDebugEnabled()) {
            log.debug("updateCorrection() done [record=" + record.getId() + "]");
        }
    }

    private Address populateAddress(Form form) {
        Address addr = new AddressImpl();
        addr.setName(form.getString(Form.NAME));
        addr.setStreet(form.getString(Form.STREET));
        addr.setStreetAddon(form.getString(Form.STREET_ADDON));
        addr.setZipcode(form.getString(Form.ZIPCODE));
        addr.setCity(form.getString(Form.CITY));
        addr.setCountry(form.getLong(Form.COUNTRY_ID));
        addr.setNote(form.getString(Form.NOTE));
        return addr;
    }

    private void validateAddress(Address addr) throws ClientException {
        AddressValidator.validateAddress(
                addr.getStreet(),
                addr.getZipcode(),
                addr.getCity(),
                addr.getCountry(),
                true);
    }

    // protected helpers

    protected PaymentAwareRecord getPaymentAwareRecord() {
        Record current = getRecord();
        if (current instanceof PaymentAwareRecord) {
            PaymentAwareRecord record = (PaymentAwareRecord) current;
            return record;
        }
        throw new IllegalStateException("current record not instance of required PaymentAwareRecord");
    }

    protected PaymentAwareRecordManager getCorrectionAwareRecordManager() {
        RecordManager manager = getRecordManager();
        if (manager != null && manager instanceof PaymentAwareRecordManager) {
            return (PaymentAwareRecordManager) getRecordManager();
        }
        log.error("getCorrectionAwareRecordManager() failed [managerClass="
                + (manager == null ? "null" : manager.getClass().getName())
                + ", message=Current view doesn't provide payment aware record manager]");
        throw new BackendException();
    }
}
