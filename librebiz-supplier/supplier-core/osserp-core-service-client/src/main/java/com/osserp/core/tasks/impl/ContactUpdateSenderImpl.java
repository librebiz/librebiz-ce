/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 3, 2010 12:39:03 PM 
 * 
 */
package com.osserp.core.tasks.impl;

import com.osserp.common.service.MessageSender;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.service.contacts.ContactSynchronisationService;
import com.osserp.core.tasks.ContactUpdateSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactUpdateSenderImpl extends AbstractTaskInitiator implements ContactUpdateSender {

    protected ContactUpdateSenderImpl(MessageSender sender, String destinationQueue) {
        super(sender, destinationQueue, "contactUpdateTask");
    }

    public void send(Contact contact) {
        if (contact != null) {
            send("<root><type>" + ContactSynchronisationService.CONTACT + "</type><id>" + contact.getContactId() + "</id></root>");
        }
    }

    public void send(PrivateContact contact) {
        if (contact != null) {
            send("<root><type>" + ContactSynchronisationService.PRIVATE_CONTACT + "</type><id>" + contact.getId() + "</id></root>");
        }
    }
}
