/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 19, 2009 00:27:03 PM 
 * 
 */
package com.osserp.gui.client.contacts;

import com.osserp.common.web.Form;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewName;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
@ViewName("contactSettingsView")
public interface ContactSettingsView extends View {

    /**
     * Updates user salutations
     * @param form
     */
    public void updateUserSalutations(Form form);

    /**
     * Indicates if current user has permission to delete the contact
     * @return true if permission grant
     */
    public boolean isContactDeletePermissionGrant();

}
