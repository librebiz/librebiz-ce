package com.osserp.common.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

// 

/**
 * 
 * Tracing beanPostProcessor, see
 * https://docs.spring.io/spring-framework/docs/4.2.9.RELEASE/spring-framework-reference/html/beans.html#beans-factory-extension-bpp-examples-hw
 *
 */
public class BeanTracingPostProcessor implements BeanPostProcessor {
    private static Logger log = LoggerFactory.getLogger(BeanTracingPostProcessor.class.getName());

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        log.debug("Before " + beanName + ", class=" + bean.getClass().getName());
        return bean; // we could potentially return any object reference here...
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        log.debug("After " + beanName + ", class=" + bean.getClass().getName());
        return bean;
    }

}