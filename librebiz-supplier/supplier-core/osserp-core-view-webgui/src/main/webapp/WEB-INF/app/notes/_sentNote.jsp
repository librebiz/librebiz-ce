<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="sentNoteView"/>

<div class="row row-modal">
    <div class="col-md-6 panel-area panel-area-modal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="emailDisplayHeader" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <td><fmt:message key="originator" /></td>
                            <td><oc:employee value="${view.bean.createdBy}" /></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="receiver" /></td>
                            <td><o:out value="${view.bean.recipients}" /></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="subject" /></td>
                            <td><o:out value="${view.bean.subject}" /></td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;"><fmt:message key="message" /></td>
                            <td><o:textOut value="${view.bean.text}" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
