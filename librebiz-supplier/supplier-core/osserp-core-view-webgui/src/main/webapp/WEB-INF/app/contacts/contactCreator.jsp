<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="contactCreatorView" />
<c:set var="contactInputViewName" scope="session" value="contactCreatorView" />
<c:if test="${! view.requiredValidationOnly}">
    <c:choose>
        <c:when test="${view.contactPersonSelected}">
            <c:set var="contactValidation" scope="page" value="return ojsForms.validateContactPersonForm(this);" />
        </c:when>
        <c:otherwise>
            <c:set var="contactValidation" scope="page" value="return ojsForms.validateContactForm(this);" />
        </c:otherwise>
    </c:choose>
</c:if>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="javascript" type="string">
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
        <c:if test="${!empty view.selectedType.name}">
            - <o:out value="${view.selectedType.name}"/>
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="contactCreatorContent">
            <c:choose>
                <c:when test="${!empty view.existingContacts}">
                    <c:import url="_contactCreateListExisting.jsp" />
                </c:when>
                <c:when test="${empty view.selectedType}">
                    <c:import url="_contactCreateTypeSelection.jsp" />
                </c:when>
                <c:otherwise>
                    <v:form url="/contacts/contactCreator/save" id="contactForm" name="contactForm" onsubmit="${contactValidation}">
                        <c:import url="_contactCreateForm.jsp" />
                    </v:form>
                </c:otherwise>
            </c:choose>
        </div>
    </tiles:put>
</tiles:insert>
