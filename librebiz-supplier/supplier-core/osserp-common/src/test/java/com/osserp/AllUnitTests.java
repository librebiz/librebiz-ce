/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.osserp.common.dao.UPGDataSourceFactory;
import com.osserp.common.gui.service.UJsonMenuProvider;
import com.osserp.common.util.UCollectionUtil;
import com.osserp.common.util.UDateUtil;
//import com.osserp.common.util.UEmailValidator;
import com.osserp.common.util.UFileUtil;
import com.osserp.common.util.UJsonUtil;
import com.osserp.common.util.UNumberFormatter;
import com.osserp.common.util.UNumberUtil;
import com.osserp.common.util.UPhoneTestData;
import com.osserp.common.util.UPhoneUtil;
import com.osserp.common.util.USecurityUtil;
import com.osserp.common.util.UStringUtil;
import com.osserp.common.xml.UJDOMUtil;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        UPGDataSourceFactory.class,
        UJsonMenuProvider.class,
        UDateUtil.class,
        UCollectionUtil.class,
        //UEmailValidator.class,
        UFileUtil.class,
        UJsonUtil.class,
        UNumberFormatter.class,
        UNumberUtil.class,
        UPhoneUtil.class,
        UPhoneTestData.class,
        USecurityUtil.class,
        UStringUtil.class,
        UJDOMUtil.class
})
public class AllUnitTests {
}
