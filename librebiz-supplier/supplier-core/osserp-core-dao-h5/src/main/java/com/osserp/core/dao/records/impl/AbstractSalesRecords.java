/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Jan-2007 13:51:45 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessType;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.dao.records.SalesRecords;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.model.RequestTypeImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractSalesRecords extends AbstractRecords implements SalesRecords {
    private String projectPlansTable;
    private String projectsTable;

    public AbstractSalesRecords(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                cache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs);
        projectPlansTable = getTable(TableKeys.PLANS);
        projectsTable = getTable(TableKeys.PROJECTS);
    }

    public AbstractSalesRecords(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            SalesPayments salesPayments) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                cache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                salesPayments);
        projectPlansTable = getTable(TableKeys.PLANS);
        projectsTable = getTable(TableKeys.PROJECTS);
    }

    public List<Record> getByCustomer(Customer customer, Long company) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.contact.id = :customerId and o.company = :companyId " +
                        "order by o.id desc");
        List<Record> result = getCurrentSession().createQuery(query.toString())
                .setParameter("customerId", customer.getId())
                .setParameter("companyId", company)
                .list();
        return result;
    }

    protected final BusinessType getRequestType(Record record) {
        if (record.getBusinessCaseId() != null) {
            Long typeId = null;
            StringBuilder sql = new StringBuilder(64);
            sql
                .append("SELECT type_id FROM ")
                .append(projectsTable)
                .append(" WHERE id = ?");
            final Object[] params = { record.getBusinessCaseId() };
            final int[] types = { Types.BIGINT };
            List<Long> result = (List<Long>) jdbcTemplate.query(
                    sql.toString(),
                    params,
                    types,
                    getLongRowMapper());
            if (result.isEmpty()) {
                sql = new StringBuilder(64);
                sql
                    .append("SELECT type_id FROM ")
                    .append(projectPlansTable)
                    .append(" WHERE plan_id = ?");
                result = (List<Long>) jdbcTemplate.query(
                        sql.toString(),
                        params,
                        types,
                        getLongRowMapper());
                if (result.isEmpty()) {
                    return null;
                }
            }
            typeId = result.get(0);
            return isEmpty(typeId) ? null :
                (BusinessType) getCurrentSession().load(RequestTypeImpl.class, typeId);
        }
        return null;
    }

    protected final int countBySales(String recordTableName, Long salesId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT count(*) FROM ")
                .append(recordTableName)
                .append(" WHERE sales_id = ?");
        if (!isIgnoreCanceledStatusOnSearch()) {
            sql.append(" AND status <> ").append(Record.STAT_CANCELED);
        }
        final Object[] params = { salesId };
        final int[] types = { Types.BIGINT };
        return jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
    }

    protected final void changeBranchBySales(String recordTableName, Long salesId, Long branchId, boolean openOnly) {
        StringBuilder sql = new StringBuilder(128);
        sql
                .append("UPDATE ")
                .append(recordTableName)
                .append(" SET branch_id = ? WHERE sales_id = ? AND status <> ")
                .append(Record.STAT_CANCELED);
        if (openOnly) {
            sql.append(" AND status < ").append(Record.STAT_SENT);
        }
        final Object[] params = { branchId, salesId };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }

    @Override
    protected PaymentCondition getDefaultCondition() {
        return getPaymentConditionDefault(true);
    }

    @Override
    protected Long getOpenStatus() {
        return Record.STAT_SENT;
    }
}
