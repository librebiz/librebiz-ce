/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25 Mar 2007 09:05:43 
 * 
 */
package com.osserp.common.dms.dao.impl;

import java.io.File;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ActionException;
import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.FileObject;
import com.osserp.common.Option;
import com.osserp.common.User;
import com.osserp.common.dao.AbstractHibernateAndSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsConfig;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.DocumentReference;
import com.osserp.common.dms.dao.DocumentStorage;
import com.osserp.common.dms.dao.Documents;
import com.osserp.common.dms.model.DmsConfigImpl;
import com.osserp.common.dms.model.DocumentTypeImpl;
import com.osserp.common.service.SecurityService;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.FileUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDocumentsDao extends AbstractHibernateAndSpringDao implements Documents {
    private static Logger log = LoggerFactory.getLogger(AbstractDocumentsDao.class.getName());

    private static final String FIXED_NAMING = "fixed";

    private String categoryTable = null;
    private String documentTable = null;
    private String documentSequence = null;
    private Long documentTypeId = null;
    private DocumentStorage storage = null;
    private DocumentEncoder encoder = null;
    private SecurityService authenticator = null;

    public AbstractDocumentsDao() {
        super();
    }

    /**
     * Constructor for document daos
     * @param jdbcTemplate for document objects
     * @param tables
     * @param sessionFactory
     * @param documentTypeId (optional, should be provided by dmsReference.typeId)
     * @param documentTableKey for document table
     * @param categoryTableKey
     */
    public AbstractDocumentsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Long documentTypeId,
            String documentTableKey,
            String categoryTableKey,
            DocumentStorage documentStorage,
            DocumentEncoder documentEncoder,
            SecurityService authenticationProvider) {
        super(jdbcTemplate, tables, sessionFactory);
        authenticator = authenticationProvider;
        storage = documentStorage;
        encoder = documentEncoder;
        documentTable = getTable(documentTableKey);
        documentSequence = getDefaultSequenceName(documentTable);
        if (documentTypeId != null) {
            this.documentTypeId = documentTypeId;
        }
        if (categoryTableKey != null) {
            categoryTable = getTable(categoryTableKey);
        }
    }

    /**
     * Provides the document reference required to perform binary persistence tasks.
     * @param document
     * @return document reference 
     */
    protected abstract DocumentReference getDocumentReference(DmsDocument document);

    /**
     * Provides the thumbnail reference required to perform binary persistence tasks.
     * @param document
     * @return thumbnail reference
     */
    protected abstract DocumentReference getThumbnailReference(DmsDocument document);

    /**
     * Provides a new, not persistent document value object.
     * @param user
     * @param documentType
     * @param reference
     * @param fileName
     * @param fileSize
     * @param note
     * @param sourceDate
     * @param categoryId
     * @param validFrom
     * @param validTil
     * @param messageId
     * @return document value object
     */
    protected abstract DmsDocument createObject(
            User user,
            DocumentType documentType,
            DmsReference reference,
            String fileName,
            long fileSize,
            String note,
            Date sourceDate,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId);

    /**
     * Provides the class required to persist the document metadata. 
     * @return class of implementing document metadata object
     */
    protected abstract Class<? extends DmsDocument> getPersistentClass();

    /**
     * Creates a thumbnail for an image. Default does nothing.
     * @param image
     * @return image with enabled thumbnail property or unchanged if not supported by type
     */
    public DmsDocument createThumbnail(DmsDocument image) {
        return image;
    }

    public DmsConfig getDmsConfig(boolean templates) {
        Long id = templates ? DmsConfig.TEMPLATES : DmsConfig.DOCUMENTS;
        return (DmsConfig) getCurrentSession().load(DmsConfigImpl.class.getName(), id);
    }

    public final DmsDocument getById(Long id) {
        return (DmsDocument) getCurrentSession().load(getPersistentClass().getName(), id);
    }

    public final DmsDocument findById(Long id) {
        try {
            return getById(id);
        } catch (Exception e) {
            log.debug("findById() ignoring not existing [id=" + id + "]");
            return null;
        }
    }

    public final List<DmsDocument> findByMessageId(String messageId) {
        List<DmsDocument> result = new ArrayList<>();
        if (messageId != null) {
            StringBuilder query = new StringBuilder("from ");
            query.append(getPersistentClass().getName())
                .append(" o where o.messageId = :messageid");
            result = getCurrentSession().createQuery(
                query.toString()).setParameter("messageid", messageId).list();
        }
        return result;
    }

    public final List<DmsDocument> findByReference(DmsReference reference) {
        StringBuilder query = new StringBuilder("from ");
        query.append(getPersistentClass().getName()).append(" o");
        if (reference.getTypeId() != null) {
            query.append(" where o.type.id = :typeId and o.reference = :referenceId");

            if (isSet(reference.getReferenceType())) {
                query.append(" and o.referenceType = :referenceTypeId order by o.id desc");
                return getCurrentSession().createQuery(query.toString())
                        .setParameter("typeId", reference.getTypeId())
                        .setParameter("referenceId", reference.getReferenceId())
                        .setParameter("referenceTypeId", reference.getReferenceType()).list();
            }
            query.append(" order by o.id desc");
            return getCurrentSession().createQuery(query.toString())
                    .setParameter("typeId", reference.getTypeId())
                    .setParameter("referenceId", reference.getReferenceId()).list();
        }
        if (isSet(reference.getReferenceType())) {
            query
                .append(" where o.reference = :referenceId and ")
                .append("o.referenceType = :referenceTypeId order by o.id desc");
            return getCurrentSession().createQuery(query.toString())
                    .setParameter("referenceId", reference.getReferenceId())
                    .setParameter("referenceTypeId", reference.getReferenceType()).list();
        }
        query.append(" where o.reference = :referenceId order by o.id desc");
        return getCurrentSession().createQuery(query.toString())
                .setParameter("referenceId", reference.getReferenceId()).list();
    }

    public final List<DmsDocument> findByType(DocumentType type) {
        StringBuilder query = new StringBuilder("from ");
        query.append(getPersistentClass().getName()).append(" o");
        DocumentType docType = type;
        if (docType == null && documentTypeId != null) {
            docType = getDocumentType();
        }
        if (docType == null) {
            throw new IllegalArgumentException("documentType required");
        }
        query.append(" where o.type.id = :typeId order by o.fileName");
        return getCurrentSession().createQuery(query.toString())
                .setParameter("typeId", docType.getId()).list();
    }

    protected final DmsDocument findByFilename(String filename) throws ClientException {
        if (isEmpty(filename)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        StringBuilder query = new StringBuilder("from ");
        query.append(getPersistentClass().getName()).append(" o where o.fileName = :filename");

        List<DmsDocument> list = getCurrentSession().createQuery(
                query.toString()).setParameter("filename", filename).list();
        DmsDocument result = list.isEmpty() ? null : list.get(0);
        if (log.isDebugEnabled()) {
            log.debug("findByFilename() done [name=" + filename 
                    + (result == null ? ", found=false" : (", id=" + result.getId())) + "]");
        }
        return result;
    }

    public final int countByReference(DmsReference reference) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql
                .append(getDocumentTable())
                .append(" WHERE reference_id = ")
                .append(reference.getReferenceId());
        if (reference.getTypeId() != null) {
            sql.append(" AND type_id = ").append(reference.getTypeId());
        }
        if (reference.getReferenceType() != null) {
            sql.append(" AND reference_type_id = ").append(reference.getTypeId());
        }
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class);
    }
    
    public int countByType(DocumentType documentType) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql
                .append(getDocumentTable())
                .append(" WHERE type_id = ")
                .append(documentType.getId());
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class);
    }

    /**
     * Creates a common document without reference, e.g. templates, etc.
     * @param user
     * @param docType
     * @param filename
     * @param fileObject
     * @param note
     * @param categoryId
     * @param validFrom
     * @param validTil
     * @param messageId
     * @return new created document
     * @throws ClientException
     */
    public DmsDocument create(
            User user,
            DocumentType docType,
            String filename,
            FileObject fileObject,
            String note,
            Long categoryId, 
            Date validFrom, 
            Date validTil,
            String messageId) throws ClientException {

        if (docType == null) {
            throw new IllegalArgumentException("no documentType provided");
        }
        if (fileObject == null || fileObject.getFileData() == null ||
                fileObject.getFileData().length < 1) {
            throw new ClientException(ErrorCode.FILE_EMPTY);
        }
        if (docType.isOverwrite()) {
            if (log.isDebugEnabled()) {
                log.debug("create() invoked with 'overwriteExisting' enabled...");
            }
            DmsDocument existing = findByFilename(filename);
            if (existing != null) {
                if (log.isDebugEnabled()) {
                    log.debug("create() existing found, trying to update...");
                }
                update(user, existing, fileObject, note, categoryId);
                return existing;
            }
            if (log.isDebugEnabled()) {
                log.debug("create() no existing found, trying to create...");
            }
        }
        if (docType.isNoteRequired() && (note == null || note.length() < 1)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        docType.validateSuffix(fileObject.getFileType());
        DmsDocument doc = createObject(
                user,
                docType,
                null,
                filename,
                fileObject.getFileData().length,
                note,
                null,
                categoryId, 
                validFrom, 
                validTil,
                messageId);
        return persistNew(doc, fileObject);
    }

    public final DmsDocument create(
            User user,
            DmsReference reference,
            FileObject fileObject,
            String note,
            Date sourceDate,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId) throws ClientException {

        DocumentType docType = getDocumentType(reference.getTypeId() != null ?
                reference.getTypeId() : documentTypeId);
        if (docType == null) {
            throw new IllegalArgumentException("no documentType provided");
        }
        if (fileObject == null || fileObject.getFileData() == null ||
                fileObject.getFileData().length < 1) {
            throw new ClientException(ErrorCode.FILE_EMPTY);
        }
        if (docType.isOverwrite()) {
            if (log.isDebugEnabled()) {
                log.debug("create() invoked with 'overwriteExisting' enabled...");
            }
            List<DmsDocument> existing = findByReference(reference);
            if (!empty(existing)) {
                if (log.isDebugEnabled()) {
                    log.debug("create() existing found, trying to update...");
                }
                DmsDocument doc = existing.get(0);
                update(user, doc, fileObject, note, categoryId);
                return doc;
            }
            if (log.isDebugEnabled()) {
                log.debug("create() no existing found, trying to create...");
            }
        }
        if (docType.isNoteRequired() && (note == null || note.length() < 1)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        docType.validateSuffix(fileObject.getFileType());
        DmsDocument doc = createObject(
                user,
                docType,
                reference,
                fileObject.getFileName(),
                fileObject.getFileData().length,
                note,
                sourceDate,
                categoryId, 
                validFrom, 
                validTil,
                messageId);
        return persistNew(doc, fileObject);
    }

    public DmsDocument create(
            User user,
            DmsReference reference,
            String fileName,
            String absoluteFile,
            String note,
            Date sourceDate,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId) throws ClientException {
        File file = FileUtil.getFile(absoluteFile);
        return create(
                user,
                reference,
                new FileObject(fileName, FileUtil.getBytes(file)),
                note,
                sourceDate,
                categoryId,
                validFrom,
                validTil,
                messageId);
    }

    protected final DmsDocument persistNew(DmsDocument doc, FileObject fileObject) throws ClientException {
        DmsDocument result;
        if (doc.getId() == null) {
            getCurrentSession().save(getPersistentClass().getName(), doc);
            result = doc;
        } else {
            result = (DmsDocument) getCurrentSession().merge(getPersistentClass().getName(), doc);
        }
        if (result.getId() != null) {
            if (!persist(result, fileObject.getFileData(), result.getType().getPersistenceFormat())) {
                deleteDocument(result);
                throw new ClientException(ErrorCode.FILE_ACCESS);
            } else if (fileObject.isReferenceOnly()) {
                fileObject.cleanupReference();
                if (log.isDebugEnabled()) {
                    log.debug("persistNew() cleanup reference done [document=" 
                            + fileObject.getReferenceFilename() + "]");
                }
            }
            result.setPersistenceFormat(result.getType().getPersistenceFormat());
            result = (DmsDocument) getCurrentSession().merge(getPersistentClass().getName(), result);
            if (doc.getType().isCreateThumbmail()) {
                result = createThumbnail(result);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("persistNew() done [document=" + result.getId() + "]");
        }
        return result;
    }

    public final void update(User user, DmsDocument doc, FileObject file, String note, Long category) throws ClientException {
        byte[] fileData = (file == null ? null : file.getFileData());
        if (log.isDebugEnabled()) {
            log.debug("update() invoked [user=" + (user == null ? "null" : user.getId())
                    + ", document=" + (doc == null ? "null" : doc.getId()) 
                    + ", filename=" + (file == null ? "null" : file.getFileName())
                    + ", filesize=" + (fileData == null ? "null" : fileData.length)
                    + ", fileExists=" + (file == null ? "false" : "true")
                    + ", note=" + note + "]");
        }
        if (user == null) {
            throw new ClientException(ErrorCode.PERMISSION_DENIED);
        }
        if (file == null || file.getFileData() == null ||
                file.getFileData().length < 1) {
            throw new ClientException(ErrorCode.FILE_EMPTY);
        }
        if (doc == null) {
            throw new ClientException(ErrorCode.DOCUMENT_NOT_FOUND);
        }
        if (doc.getType().isNoteRequired() 
                && ((note == null || note.length() < 1) 
                        && (doc.getNote() == null || doc.getNote().length() < 1))) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        if (!doc.getType().isSuffixByFile()) {
            String fileType = file.getFileType();
            if (!fileType.equalsIgnoreCase(doc.getType().getSuffix())) {
                throw new ClientException(ErrorCode.DOCUMENT_TYPE_INVALID);
            }
        }
        boolean result = persist(doc, file.getFileData(), doc.getPersistenceFormat());
        if (result) {
            doc.setFileSize(fileData.length);
            if (!FIXED_NAMING.equalsIgnoreCase(doc.getType().getNamingStyle())) {
                doc.setFileName(file.getFileName());
            }
            doc.setChanged(DateUtil.getCurrentDate());
            doc.setChangedBy(user.getId());
            doc.setNote(note);
            doc.setCategoryId(category);
            save(doc);
            if (log.isDebugEnabled()) {
                log.debug("update() done [user=" + user.getId()
                        + ", document=" + doc.getId()
                        + ", filename=" + file.getFileName()
                        + ", filesize=" + fileData.length
                        + ", note=" + note + "]");
            }
        } else {
            // user did not provide a new file; update metadata only
            doc.setChanged(DateUtil.getCurrentDate());
            doc.setChangedBy(user.getId());
            doc.setNote(note);
            doc.setCategoryId(category);
            save(doc);
            if (log.isDebugEnabled()) {
                log.debug("update() done metadata only [user=" + user.getId()
                        + ", document=" + doc.getId()
                        + ", note=" + note + "]");
            }
        }
    }

    public final void updateMetadata(
            User user, 
            DmsDocument document, 
            String note,
            Date validFrom,
            Date validTil) throws ClientException {
        assert (user != null && document != null);
        document.setNote(note);
        if (document.getType().isValidityPeriodSupported()) {
            document.setValidFrom(validFrom);
            document.setValidTil(validTil);
        }
        document.updateChanged(user.getId());
        save(document);
    }

    public List<Option> getCategories() {
        List<Option> result = new ArrayList<>();
        if (categoryTable != null) {
            StringBuilder sql = new StringBuilder("SELECT id, name, end_of_life FROM ");
            sql
                .append(categoryTable)
                .append(" ORDER BY name");
            try {
                result = (List<Option>) jdbcTemplate.query(sql.toString(), getOptionRowMapper());
            } catch (Exception e) {
                // fetching the default category never breaks the app
                log.warn("getCategories() ignoring exception [message=" + e.getMessage() + "]");
            }
        }
        return result;
    }

    public Long getDefaultCategoryId(Long documentType) {
        Long result = null;
        if (categoryTable != null && documentType != null) {
            StringBuilder sql = new StringBuilder("SELECT id FROM ");
            sql
                .append(categoryTable)
                .append(" WHERE end_of_life = false AND is_default = true AND type_id = ")
                .append(documentType);
            try {
                List<Long> defaults = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
                result = defaults.isEmpty() ? null : defaults.get(0);
            } catch (Exception e) {
                // fetching the default category never breaks the app
                log.warn("getDefaultCategoryId() ignoring exception [message=" + e.getMessage() + "]");
            }
        }
        return result;
    }

    public void updateCategory(User user, Long id, Long categoryId) {
        DmsDocument doc = findById(id);
        if (doc != null) {
            doc.setCategoryId(categoryId);
            if (user == null) {
                doc.setChangedBy(Constants.SYSTEM_EMPLOYEE);
            } else {
                doc.setChangedBy(user.getId());
            }
            doc.setChanged(new Date(System.currentTimeMillis()));
            save(doc);
        }
    }

    public void setAsReference(Long id) {
        DmsDocument doc = findById(id);
        if (doc.isReferenceDocument()) {
            doc.setReferenceDocument(false);
            save(doc);
        } else {
            StringBuilder sql = new StringBuilder(64);
            sql
                    .append("UPDATE ")
                    .append(getDocumentTable())
                    .append(" SET is_reference = false WHERE reference_id = ")
                    .append(doc.getReference());
            if (doc.getType() != null && doc.getType().isFilesystemStore()) {
                sql.append(" AND type_id = ").append(doc.getType().getId());
            }
            jdbcTemplate.update(sql.toString());
            sql = new StringBuilder(64);
            sql
                    .append("UPDATE ")
                    .append(getDocumentTable())
                    .append(" SET is_reference = true WHERE id = ")
                    .append(doc.getId());
            jdbcTemplate.update(sql.toString());
        }
    }

    public DmsDocument getReferenceDocument(DmsReference reference) throws ClientException {
        List<DmsDocument> list;
        StringBuilder query = new StringBuilder("from ");
        if (reference.getTypeId() != null) {
            query
                    .append(getPersistentClass().getName())
                    .append(" o where o.type.id = :typeId and o.reference = :referenceId and o.referenceDocument = true");
            list = getCurrentSession().createQuery(query.toString())
                    .setParameter("typeId", reference.getTypeId())
                    .setParameter("referenceId", reference.getReferenceId()).list();
        } else {
            query.append(getPersistentClass().getName()).append(
                    " o where o.reference = :referenceId and o.referenceDocument = true");
            list = getCurrentSession().createQuery(query.toString())
                    .setParameter("referenceId", reference.getReferenceId()).list();
        }
        if (empty(list)) {
            throw new ClientException(ErrorCode.DOCUMENT_NOT_FOUND);
        }
        return list.get(0);
    }

    public void deleteDocument(DmsDocument doc) {
        drop(doc);
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("DELETE FROM ")
                .append(getDocumentTable())
                .append(" WHERE id = ?");
        final Object[] params = { doc.getId() };
        final int[] types = { Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }

    protected void save(DmsDocument document) {
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), document);
    }

    protected String getDocumentTable() {
        return documentTable;
    }

    protected String getDocumentSequence() {
        return documentSequence;
    }

    public final DocumentType getDocumentType() {
        return getDocumentType(documentTypeId);
    }

    public DocumentType getDocumentType(Long id) {
        return (DocumentType) getCurrentSession().load(DocumentTypeImpl.class.getName(), id);
    }

    public final byte[] getDocumentData(DmsDocument document) {
        if (log.isDebugEnabled()) {
            log.debug("getDocumentData() invoked [id="
                    + (document == null ? "null" : (document.getId()
                    + ", type=" + document.getType().getId())
                    + ", reference=" + document.getReference())
                    + "]");
        }
        if (document == null) {
            return null;
        }
        try {
            byte[] bin = storage.getBinary(getDocumentReference(document));
            return encoder.decode(bin, 
                document.getPersistenceFormat(), 
                getSecret(document));
        } catch (ClientException c) {
            throw new BackendException(c.getMessage(), c);
        }
    }

    public byte[] getThumbnailData(DmsDocument document) {
        if (document == null || !document.isThumbnailAvailable()) {
            return null;
        }
        try {
            DocumentReference tref = getThumbnailReference(document);
            return tref != null ? storage.getBinary(tref) : null;
        } catch (Exception e) {
            log.warn("getThumbnailData() failed [document=" + document.getId() +
                    ", message=" + e.getMessage() + "]");
            return null;
        }
    }

    public void export(DmsDocument document, String exportFile) throws ClientException {
        try {
            byte[] doc = getDocumentData(document);
            FileUtil.persist(doc, exportFile);
            if (log.isDebugEnabled()) {
                log.debug("export() done [id=" + document.getId() 
                        + ", type=" + document.getType().getId() 
                        + ", target=" + exportFile + "]");
            }
        } catch (Exception e) {
            log.error("export() failed [id=" + (document == null ? "null" 
                    : document.getId()) + ", message=" + e.getMessage() + "]");
        }
    }

    public void deleteExport(String exportFile) throws ClientException {
        assert exportFile != null;
        File file = new File(exportFile);
        if (!file.canWrite()) {
            throw new ClientException(ErrorCode.FILE_ACCESS);
        }
        try {
            FileUtil.delete(file);
            if (log.isDebugEnabled()) {
                log.debug("deleteExport() done [file=" + exportFile + "]");
            }
        } catch (Exception e) {
            log.error("deleteExport() failed [file=" + exportFile 
                    + ", message=" + e.getMessage() + "]");
        }
    }

    protected boolean store(DocumentReference reference, byte[] file) {
        return storage.storeBinary(reference, file);
    }

    private boolean persist(DmsDocument doc, byte[] file, int format) {
        if (log.isDebugEnabled()) {
            log.debug("persist() invoked [document=" + (doc == null ? "null" : doc.getId()) + "]");
        }
        if (doc == null || file == null) {
            return false;
        }
        return store(getDocumentReference(doc), 
                encoder.encode(file, format, getSecret(doc)));
    }

    public void changeFormat(DmsDocument document, int format) {
        if (document != null && document.getPersistenceFormat() != format) {
            byte[] bin = getDocumentData(document);
            if (persist(document, bin, format)) {
                document.setPersistenceFormat(format);
                save(document);
            }
        }
    }

    private boolean drop(DmsDocument doc) {
        if (doc.isThumbnailAvailable()) {
            storage.removeBinary(getThumbnailReference(doc));
        }
        return storage.removeBinary(getDocumentReference(doc));
    }

    protected String getSecret(DmsDocument document) {
        
        String result = null;
        try {
            result = authenticator.readText(document.getType(), "documentTypes");
        } catch (Exception e) {
            log.warn("getSecret() ignoring failed attempt to get secret " 
                    + "[message=" + e.getMessage() + "]", e);
        }
        if (result == null && document != null && document.getType() != null 
                && (document.getType().getPersistenceFormat() == DocumentType.FORMAT_CRYPT
                    || document.getPersistenceFormat() == DocumentType.FORMAT_CRYPT)) {
            throw new ActionException(ErrorCode.SETUP_REQUIRED);
        }
        return result;
    }
}
