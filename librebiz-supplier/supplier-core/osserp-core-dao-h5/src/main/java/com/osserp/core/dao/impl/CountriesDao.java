/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2006 9:27:55 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.hibernate.SessionFactory;

import com.osserp.common.Option;
import com.osserp.common.SelectOption;
import com.osserp.common.dao.CountryCodeRowMapper;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.Countries;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.contacts.ContactCountryImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CountriesDao extends SelectOptionsDao implements Countries {
    private static Logger log = LoggerFactory.getLogger(CountriesDao.class.getName());

    public CountriesDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public List<Option> getList() {
        try {
            StringBuilder hql = new StringBuilder(128);
            hql
                    .append("from ")
                    .append(ContactCountryImpl.class.getName())
                    .append(" o order by o.name");
            return getCurrentSession().createQuery(hql.toString()).list();
        } catch (Exception e) {
            log.error("getList() ignoring exception [message=" + e.getMessage() + "]", e);
            return new java.util.ArrayList<Option>();
        }
    }

    public List<Option> getCodeList() {
        StringBuffer query = new StringBuffer(64);
        query
                .append("SELECT id,country_code,end_of_life FROM ")
                .append(getTable(TableKeys.COUNTRIES))
                .append(" ORDER BY country_code");
        return (List) jdbcTemplate.query(query.toString(),
                new RowMapperResultSetExtractor(new CountryCodeRowMapper()));
    }

    public List<SelectOption> getPrefixList() {
        StringBuffer query = new StringBuffer(64);
        query
                .append("SELECT DISTINCT prefix FROM ")
                .append(getTable(TableKeys.COUNTRIES))
                .append(" ORDER BY prefix");
        return (List) jdbcTemplate.query(query.toString(), getSelectOptionRowMapper());
    }

}
