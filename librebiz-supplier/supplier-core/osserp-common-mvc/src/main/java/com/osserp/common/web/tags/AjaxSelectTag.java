/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2007 10:45:32 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Tag renders an HTML-Select executing an ajax request on change. The function invoked depends on given params.
 * 
 * function ajaxSelect(targetElement, targetIsPopup, activityElement, activityMessage, successFunction, select)
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public class AjaxSelectTag extends AbstractAjaxElementTag {
    private static Logger log = LoggerFactory.getLogger(AjaxSelectTag.class.getName());

    private String selectId = null;

    /**
     * Provides the optional id for the select
     * @return selectId
     */
    public String getSelectId() {
        return selectId;
    }

    /**
     * Sets an optional select id
     * @param selectId
     */
    public void setSelectId(String selectId) {
        this.selectId = selectId;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            if (getActivityElement() == null) {
                setActivityElement("");
            }
            if (getPostRequest() == null) {
                setPostRequest("");
            }

            StringBuilder buffer = new StringBuilder();
            buffer.append("<select");
            if (getSelectId() != null) {
                buffer.append(" id=\"").append(getSelectId()).append("\"");
            }
            buffer
                    .append(" onchange=\"");
            if (getPreRequest() != null) {
                buffer.append(getPreRequest());
            }
            buffer
                    .append(" ojsAjax.ajaxSelect('")
                    .append(getTargetElement())
                    .append("', ");
            if (isPopup()) {
                buffer.append("true");
            } else {
                buffer.append("false");
            }
            buffer
                    .append(", '")
                    .append(getActivityElement())
                    .append("'")
                    .append(", '")
                    .append(getResourceString("waitPleaseWithDots"))
                    .append("'")
                    .append(", function() { ")
                    .append(getOnSuccess())
                    .append(" }, ")
                    .append("this")
                    .append("); ")
                    .append(getPostRequest())
                    .append("\"");

            if (getStyle() != null) {
                buffer.append(" style=\"").append(getStyle()).append("\"");
            }

            buffer.append(">");
            JspWriter out = pageContext.getOut();
            out.print(buffer.toString());
            setTargetElement(null);
            setPreRequest(null);
            setPostRequest(null);
            setOnSuccess(null);
        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("</select>");
        } catch (IOException ioe) {
            log.error("doEndTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    @Override
    public String getTargetElement() {
        String te = super.getTargetElement();
        return (te != null && te != "") ? te : getSelectId() + "_popup";
    }
}
