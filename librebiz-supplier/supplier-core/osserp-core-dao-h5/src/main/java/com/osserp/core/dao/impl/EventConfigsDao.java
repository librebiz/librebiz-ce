/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 4, 2007 10:06:03 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.EventConfigs;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventConfigType;
import com.osserp.core.events.EventTask;
import com.osserp.core.events.EventTermination;
import com.osserp.core.events.RecipientPool;
import com.osserp.core.model.events.EventActionImpl;
import com.osserp.core.model.events.EventConfigTypeImpl;
import com.osserp.core.model.events.EventTaskImpl;
import com.osserp.core.model.events.EventTerminationImpl;
import com.osserp.core.model.events.RecipientPoolImpl;
import com.osserp.core.views.EventDisplayView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventConfigsDao extends AbstractDao implements EventConfigs {
    private static Logger log = LoggerFactory.getLogger(EventConfigsDao.class.getName());

    private String eventsTable = null;
    private String eventsHistoryTable = null;
    private String eventActionsTable = null;
    private String eventTerminationsTable = null;

    public EventConfigsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {

        super(jdbcTemplate, tables, sessionFactory);
        eventsTable = getTable(TableKeys.EVENTS);
        eventsHistoryTable = getTable(TableKeys.EVENTS_HISTORY);
        eventActionsTable = getTable(TableKeys.EVENT_ACTIONS);
        eventTerminationsTable = getTable(TableKeys.EVENT_TERMINATIONS);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#createAction(com.osserp.core.events.EventConfigType, java.lang.String)
     */
    public EventAction createAction(EventConfigType type, String name) {
        EventActionImpl obj = new EventActionImpl(
            createActionId(type),
            type,
            null,
            name);
        save(obj);
        return obj;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#createAction(java.lang.Long, com.osserp.core.events.EventConfigType, java.lang.String, java.lang.Long)
     */
    public EventAction createAction(
            Long requestType,
            EventConfigType type,
            String name,
            Long caller) {

        EventActionImpl obj = new EventActionImpl(
            createActionId(type),
            type,
            caller,
            name);
        save(obj);
        return obj;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#setAlert(com.osserp.core.events.EventAction, com.osserp.core.events.RecipientPool)
     */
    public void setAlert(EventAction action, RecipientPool pool) {

        if (action.getAlertEvent() != null) {
            action.updateAlertEvent(pool);
        } else {
            EventActionImpl obj = new EventActionImpl(
                createActionId(action.getType()),
                action,
                pool);
            save(obj);
            action.setAlertEvent(obj);
        }
        save(action);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#createPool(java.lang.String)
     */
    public RecipientPool createPool(String name) {
        RecipientPool pool = new RecipientPoolImpl(name);
        save(pool);
        return pool;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#findActionsByCaller(java.lang.Long, java.lang.Long)
     */
    public List<EventAction> findActionsByCaller(Long eventType, Long callerId) {
        StringBuilder hql = new StringBuilder(255);
        hql
                .append("from ")
                .append(EventActionImpl.class.getName())
                .append(" ec where ec.type.id = ")
                .append(eventType)
                .append(" and ec.callerId = ")
                .append(callerId);
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#findActionsByEventType(java.lang.Long)
     */
    public List<EventAction> findActionsByEventType(Long eventType) {
        return getCurrentSession().createQuery(
                "from EventActionImpl ec where ec.type.id = :eventTypeId order by ec.id")
                .setParameter("eventTypeId", eventType).list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#findActionsByRequestType(java.lang.Long)
     */
    public List<EventAction> findFlowControlActions(EventConfigType configType, Long requestType) {
        List<Long> list = new ArrayList<Long>();
        StringBuilder sql = new StringBuilder(128);
        sql
                .append("SELECT id FROM ")
                .append(eventActionsTable)
                .append(" WHERE caller_id IN (SELECT id FROM ")
                .append(configType.getFlowControlActionTable());
        if (requestType != null) {
            sql.append(" WHERE type_id = ?)");
            list = (List<Long>) jdbcTemplate.query(
                    sql.toString(),
                    new Object[] { requestType },
                    new int[] { Types.BIGINT },
                    getLongRowMapper());
        } else {
            sql.append(")");
            list = (List<Long>) jdbcTemplate.query(
                    sql.toString(),
                    getLongRowMapper());
        }
        List<EventAction> actions = new ArrayList<EventAction>();
        for (int i = 0, j = list.size(); i < j; i++) {
            actions.add(loadAction(list.get(i)));
        }
        return actions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#findTerminationsByCaller(java.lang.Long, java.lang.Long)
     */
    public List<EventTermination> findTerminationsByCaller(Long eventType, Long callerId) {
        if (callerId == null) {
            return new ArrayList<EventTermination>();
        }
        StringBuilder hql = new StringBuilder(255);
        hql
                .append("from ")
                .append(EventTerminationImpl.class.getName())
                .append(" ec where ec.type.id = ")
                .append(eventType)
                .append(" and ec.callerId = ")
                .append(callerId);
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#loadAction(java.lang.Long)
     */
    public EventAction loadAction(Long id) {
        return (EventAction) getCurrentSession().load(EventActionImpl.class, id);
    }
    
    /* (non-Javadoc)
     * @see com.osserp.core.dao.EventConfigs#loadActions()
     */
    public List<EventAction> loadActions() {
        List<EventAction> list = getCurrentSession().createQuery(
                "from " + EventActionImpl.class.getName() + " o order by o.name").list();
        return list;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#loadTermination(java.lang.Long)
     */
    public EventTermination loadTermination(Long id) {
        return (EventTermination) getCurrentSession().load(EventTerminationImpl.class, id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#loadPool(java.lang.Long)
     */
    public RecipientPool loadPool(Long id) {
        return (RecipientPool) getCurrentSession().load(RecipientPoolImpl.class, id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#findPools()
     */
    public List<RecipientPool> findPools() {
        return getCurrentSession().createQuery(
                "from " + RecipientPoolImpl.class.getName()
                + " pool order by pool.name").list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#loadType(java.lang.Long)
     */
    public EventConfigType loadType(Long id) {
        return (EventConfigType) getCurrentSession().load(EventConfigTypeImpl.class, id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#findTypes()
     */
    public List<EventConfigType> findTypes() {
        return getCurrentSession().createQuery("from "
                + EventConfigTypeImpl.class.getName() + " type order by type.id").list();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#save(com.osserp.core.events.EventAction)
     */
    public void save(EventAction action) {
        getCurrentSession().saveOrUpdate(action);
        if (log.isDebugEnabled()) {
            log.debug("save() done for " + action.getId());
        }
        try {
            if (action.getType().isFlowControl()) {
                synchronizeFcsActions(action.getType());
            }
        } catch (Exception e) {
            log.warn("save() ignoring exception on attempt to synchronize fcs [message"
                   + e.getMessage() + "]", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#delete(com.osserp.core.events.EventAction)
     */
    public void delete(EventAction action) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("delete() invoked for action " + action.getId());
        }
        StringBuilder sql = new StringBuilder();
        sql
                .append("DELETE FROM ")
                .append(eventsTable)
                .append(" WHERE event_config_id = ?");
        final Object[] params = { action.getId() };
        final int[] types = { Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
        sql = new StringBuilder();
        sql
                .append("DELETE FROM ")
                .append(eventsHistoryTable)
                .append(" WHERE event_config_id = ?");
        jdbcTemplate.update(sql.toString(), params, types);
        getCurrentSession().delete(action);
        if (log.isDebugEnabled()) {
            log.debug("delete() done, removed action " + action.getId());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#save(com.osserp.core.events.EventTermination)
     */
    public void save(EventTermination termination) {
        getCurrentSession().saveOrUpdate(termination);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#delete(com.osserp.core.events.EventTermination)
     */
    public void delete(EventTermination termination) {
        getCurrentSession().delete(termination);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#save(com.osserp.core.events.RecipientPool)
     */
    public void save(RecipientPool pool) {
        getCurrentSession().saveOrUpdate(pool);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#getTask(java.lang.String)
     */
    public EventTask getTask(String name) {
        List<EventTask> tasks = getCurrentSession().createQuery(
                "from " + EventTaskImpl.class.getName()
                + " task where task.name = :name")
                .setParameter("name", name).list();
        return tasks.isEmpty() ? null : tasks.get(0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#findStartActionView(com.osserp.core.events.EventConfigType, java.lang.Long)
     */
    public List<EventDisplayView> findStartActionView(EventConfigType configType, Long requestType) {
        if (configType == null || configType.getFlowControlStartActionView() == null) {
            log.warn("findStartActionView() invoked with invalid eventConfigType [id=" +
                    (configType == null ? "null" : configType.getId()) + "]");
            return new ArrayList<EventDisplayView>();
        }
        StringBuilder query = new StringBuilder(EventDisplayViewRowMapper.SELECT_FROM);
        query.append(configType.getFlowControlStartActionView());

        if (requestType == null) {
            query
                    .append(" WHERE configtype = ")
                    .append(configType.getId());

        } else {
            query
                    .append(" WHERE type = ")
                    .append(requestType)
                    .append(" AND configtype = ")
                    .append(configType.getId());
        }
        return (List<EventDisplayView>) jdbcTemplate.query(
                query.toString(),
                EventDisplayViewRowMapper.getReader());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#synchronizeFcsActions()
     */
    public void synchronizeFcsActions() {
        List<EventConfigType> types = findTypes();
        for (Iterator<EventConfigType> i = types.iterator(); i.hasNext();) {
            EventConfigType type = i.next();
            if (type.isFlowControl() && type.getFlowControlActionTable() != null) {
                synchronizeFcsActions(type);
            }
        }
    }

    private void synchronizeFcsActions(EventConfigType type) {
        StringBuilder sqlSetTrue = new StringBuilder();
        sqlSetTrue
                .append("UPDATE ")
                .append(type.getFlowControlActionTable())
                .append(" SET perform_events = true WHERE " +
                        "id IN (SELECT caller_id FROM ")
                .append(eventTerminationsTable)
                .append(" WHERE type_id = ")
                .append(type.getId())
                .append(") OR id IN (SELECT caller_id FROM ")
                .append(eventActionsTable)
                .append(" WHERE type_id = ")
                .append(type.getId())
                .append(")");
        int updated = jdbcTemplate.update(sqlSetTrue.toString());
        if (log.isDebugEnabled()) {
            log.debug("synchronizeFcsActions() updated enabled status [type=" + type.getId()
                    + ", count=" + updated + "]");
        }
        StringBuilder sqlSetFalse = new StringBuilder();
        sqlSetFalse
                .append("UPDATE ")
                .append(type.getFlowControlActionTable())
                .append(" SET perform_events = false WHERE " +
                        "id NOT IN (SELECT caller_id FROM ")
                .append(eventTerminationsTable)
                .append(" WHERE type_id = ")
                .append(type.getId())
                .append(") AND id NOT IN (SELECT caller_id FROM ")
                .append(eventActionsTable)
                .append(" WHERE type_id = ")
                .append(type.getId())
                .append(")");
        updated = jdbcTemplate.update(sqlSetFalse.toString());
        if (log.isDebugEnabled()) {
            log.debug("synchronizeFcsActions() updated disabled status [type=" + type.getId()
                    + ", count=" + updated + "]");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#closeEvents(com.osserp.core.events.EventAction)
     */
    public void closeEvents(EventAction action) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(eventsTable).append(
                " SET is_closed = true, closed_on = ?, closed_by = ? "
                        + "WHERE event_config_id = ?");
        String update = sql.toString();
        final Object[] params = {
                new Timestamp(System.currentTimeMillis()),
                Constants.SYSTEM_USER,
                action.getId()
        };
        final int[] types = {
                Types.TIMESTAMP,
                Types.BIGINT,
                Types.BIGINT
        };
        jdbcTemplate.update(update, params, types);
    }

    private Long createActionId(EventConfigType type) {
        return getNextLong(type.getSequenceName());
    }
}
