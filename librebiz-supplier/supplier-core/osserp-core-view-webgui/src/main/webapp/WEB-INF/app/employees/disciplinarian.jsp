<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="disciplinarianView"/>
<c:set var="employeeDisciplinarians" value="${view.list}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>

	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
	</tiles:put>
	<tiles:put name="headline_right">
        <v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string">

		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th style="width:50px;"><fmt:message key="employee"/></th>
							<th><fmt:message key="name"/></th>
							<th><fmt:message key="branch"/></th>
							<th><fmt:message key="role"/></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${empty employeeDisciplinarians}">
							<tr>
								<td colspan="5">
									<fmt:message key="noDisciplinariansAssigned"/>
								</td>
							</tr>
						</c:if>
						<c:forEach var="employeeDisciplinarian" items="${employeeDisciplinarians}">
							<tr>
								<td class="right"><o:out value="${employeeDisciplinarian.disciplinarian.id}"/></td>
								<td><o:out value="${employeeDisciplinarian.disciplinarian.name}"/></td>
								<td><o:out value="${employeeDisciplinarian.disciplinarian.branch.shortname}"/></td>
								<td><o:out value="${employeeDisciplinarian.disciplinarian.role}"/></td>
								<td class="center"><a href="<v:url value="/employees/disciplinarian/remove?id=${employeeDisciplinarian.disciplinarian.id}"/>"><o:img name="deleteIcon"/></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="spacer"></div>
			</div>
		</div>
	</tiles:put>
</tiles:insert>