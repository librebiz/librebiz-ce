/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 3, 2005 
 * 
 */
package com.osserp.common.web;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;

/**
 * 
 * Helper class for extracting retrieved values from an dyna action form. Usage of this class simplifies property access only
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public interface Form {

    static final String METHOD = "method";
    static final String VALUE = "value";
    static final String ID = "id";

    static final String ACTIVE = "active";
    static final String AFFECTS_STOCK = "affectsStock";
    static final String AGENT = "agent";
    static final String AGENT_COMMISSION = "agentCommission";
    static final String AGENT_COMMISSION_PERCENT = "agentCommissionPercent";
    static final String ALIAS_OF = "aliasOf";
    static final String AMOUNT = "amount";
    static final String BACKGROUND_COLOR = "backgroundColor";
    static final String BEGIN_DATE = "beginDate";
    static final String BILLING_NOTE_REQUIRED = "billingNoteRequired";
    static final String BIRTHDATE = "birthDate";
    static final String BOOKING_TYPE = "bookingType";
    static final String BRANCH = "branch";
    static final String BUNDLE = "bundle";
    static final String CALCULATE_SALES = "calculateSales";
    static final String CALCULATION_CLASS = "calculationClass";
    static final String CATEGORY_ID = "categoryId";
    static final String CERTIFICATION = "certification";
    static final String CITY = "city";
    static final String CITY_CONTACT_PERSON = "cityContactPerson";
    static final String CLASSIFICATION = "classification";
    static final String CLOSED = "closed";
    static final String COMPANY = "company";
    static final String COMPANY_AFFIX = "companyAffix";
    static final String COMPONENT = "component";
    static final String CONFIRM_PASSWORD = "confirmPassword";
    static final String CONSUMER_MARGIN = "consumerMargin";
    static final String CONTENT = "content";
    static final String COUNTRY = "country";
    static final String COUNTRY_ID = "countryId";
    static final String COUNTRY_ID_CONTACT_PERSON = "countryIdContactPerson";
    static final String CREATED = "created";
    static final String CREATED_DATE = "createdDate";
    static final String CUSTOM_NAME = "customName";
    static final String DATASHEET_TEXT = "datasheetText";
    static final String DATE = "date";
    static final String DELIVERY = "delivery";
    static final String DEPTH = "depth";
    static final String DESCRIPTION = "description";
    static final String DESCRIPTION_SHORT = "descriptionShort";
    static final String DURATION = "duration";
    static final String EMAIL = "email";
    static final String END_DATE = "endDate";
    static final String END_OF_LIFE = "endOfLife";
    static final String ESTIMATED_PURCHASE_PRICE = "estimatedPurchasePrice";
    static final String FAX_COUNTRY = "fcountry";
    static final String FAX_NUMBER = "fnumber";
    static final String FAX_PREFIX = "fprefix";
    static final String FEDERAL_STATE_ID = "federalStateId";
    static final String FEDERAL_STATE_ID_CONTACT_PERSON = "federalStateIdContactPerson";
    static final String FEDERAL_STATE_NAME = "federalStateName";
    static final String FIRSTNAME = "firstName";
    static final String FIRSTNAME_PREFIX = "firstNamePrefix";
    static final String FRAME = "frame";
    static final String FROM = "from";
    static final String GREATER_THAN = "greaterThan";
    static final String GREETINGS = "greetings";
    static final String GROUP_ID = "groupId";
    static final String HEADER = "header";
    static final String HEIGHT = "height";
    static final String INCLUDE_EOL = "includeEol";
    static final String INDEX = "index";
    static final String INDEXED = "indexed";
    static final String INFO = "info";
    static final String INFOS = "infos";
    static final String IGNORE_HEADER = "ignoreHeader";
    static final String INITIALS = "initials";
    static final String ITEM_ID = "itemId";
    static final String JOB = "job";
    static final String KANBAN = "kanban";
    static final String KEY = "key";
    static final String LANGUAGE = "language";
    static final String LASTNAME = "lastName";
    static final String LOGINNAME = "loginName";
    static final String MANUFACTURER = "manufacturer";
    static final String MARKETING_TEXT = "marketingText";
    static final String MATCHCODE = "matchcode";
    static final String MOBILE_COUNTRY = "mcountry";
    static final String MOBILE_NUMBER = "mnumber";
    static final String MOBILE_PREFIX = "mprefix";
    static final String NAME = "name";
    static final String NEW_PASSWORD = "newPassword";
    static final String NO_CLASSIFICATION = "noClassification";
    static final String NOTE = "note";
    static final String NULLABLE = "nullable";
    static final String NUMBER = "number";
    static final String OFFER_USAGE_ONLY = "offerUsageOnly";
    static final String OFFICE = "office";
    static final String OPEN_ONLY = "openOnly";
    static final String PACKAGING_UNIT = "packagingUnit";
    static final String PARTNER_MARGIN = "partnerMargin";
    static final String PARTNER_PRICE = "partnerPrice";
    static final String PARTNER_PRICE_SOURCE = "partnerPriceSource";
    static final String PASSWORD = "password";
    static final String PAID = "paid";
    static final String PERSON = "person";
    static final String PHONE_COUNTRY = "pcountry";
    static final String PHONE_NUMBER = "pnumber";
    static final String PHONE_PREFIX = "pprefix";
    static final String PLANT = "plant";
    static final String POSITION = "position";
    static final String POWER = "power";
    static final String POWER_UNIT = "powerUnit";
    static final String PREFIX = "prefix";
    static final String PRICE = "price";
    static final String PRIMARY = "primary";
    static final String PRINT_DATE = "printDate";
    static final String PRINT_GREETINGS = "printGreetings";
    static final String PRINT_HEADER = "printHeader";
    static final String PRODUCT_ID = "productId";
    static final String PRODUCT_SHEET = "productSheet";
    static final String PURCHASE_TEXT = "purchaseText";
    static final String PUBLIC_AVAILABLE = "publicAvailable";
    static final String QUANTITY = "quantity";
    static final String QUANTITY_BY_PLANT = "quantityByPlant";
    static final String QUANTITY_UNIT = "quantityUnit";
    static final String RECIPIENT_LIST = "recipientList";
    static final String RECIPIENTS = "recipients";
    static final String RECIPIENTS_BCC = "recipientsBCC";
    static final String RECIPIENTS_CC = "recipientsCC";
    static final String REDUCED_TAX = "reducedTax";
    static final String REFERENCE = "reference";
    static final String REMOTE_ACCESS = "remoteAccess";
    static final String RESELLER_MARGIN = "resellerMargin";
    static final String SALES_ID = "salesId";
    static final String SALUTATION = "salutation";
    static final String SECTION = "section";
    static final String SEND_EMAIL = "sendEmail";
    static final String SERIAL_NUMBER = "serialNumber";
    static final String SERVICE = "service";
    static final String SHOW_ALL = "showAll";
    static final String SIGNATURE_LEFT = "signatureLeft";
    static final String SIGNATURE_RIGHT = "signatureRight";
    static final String SPOUSE_FIRSTNAME = "spouseFirstName";
    static final String SPOUSE_LASTNAME = "spouseLastName";
    static final String SPOUSE_SALUTATION = "spouseSalutation";
    static final String SPOUSE_TITLE = "spouseTitle";
    static final String STARTS_WITH = "startsWith";
    static final String STATUS_ID = "statusId";
    static final String STOCK_ID = "stockId";
    static final String STREET = "street";
    static final String STREET_ADDON = "streetAddon";
    static final String STREET_ADDON_CONTACT_PERSON = "streetAddonContactPerson";
    static final String STREET_CONTACT_PERSON = "streetContactPerson";
    static final String STRING_TYPE = "stringType";
    static final String SUBJECT = "subject";
    static final String TECH_NOTE = "techNote";
    static final String TEMPLATE_ID = "templateId";
    static final String TEMPLATE_NAME = "templateName";
    static final String TERM = "term";
    static final String TEXT = "text";
    static final String TIP_COMMISSION = "tipCommission";
    static final String TIP_PROVIDER = "tipProvider";
    static final String TITLE = "title";
    static final String TOPOLOGY = "topology";
    static final String TYPE = "type";
    static final String TYPE_ID = "typeId";
    static final String UNTIL = "until";
    static final String USER_NAME_ADDRESS_FIELD = "userNameAddressField";
    static final String USER_SALUTATION = "userSalutation";
    static final String VALID_FROM = "validFrom";
    static final String VAT_ID = "vatId";
    static final String VIRTUAL = "virtual";
    static final String WARRANTY = "warranty";
    static final String WEBSITE = "website";
    static final String WEIGHT = "weight";
    static final String WIDTH = "width";
    static final String ZIPCODE = "zipcode";
    static final String ZIPCODE_CONTACT_PERSON = "zipcodeContactPerson";

    /**
     * Tries to extract an object from the form variable key.
     * @param key the name of the variable holding the value
     * @return value or null if variable not exists
     */
    Object getValue(String key);

    /**
     * Tries to extract a String from the form variable key.
     * @param key / name of the variable holding the value
     * @return value or null if variable not exists
     */
    String getString(String key);

    /**
     * Tries to extract a String from the form variable key.
     * @param key the name of the variable holding the value
     * @param required indicates if an exception should be thrown if no value under key found
     * @return value or null if variable not exists AND required is false
     * @throws ClientException if required is true and no string set
     */
    String getString(String key, boolean required) throws ClientException;

    /**
     * Tries to extract an Integer from the form variable key.
     * @param key the name of the variable holding the value
     * @return value or null if variable not exists
     */
    Integer getInteger(String key);

    /**
     * Tries to get an int from a form property defined as Integer
     * @param key
     * @return value
     * @throws ClientException if no int found
     */
    int getIntVal(String key) throws ClientException;

    /**
     * Tries to get an object specified as Long in form definition
     * @param key / name of the form field
     * @return value or null if not found
     */
    Long getLong(String key);

    /**
     * Tries to get the long value of an object specified as Long in form definition
     * @param key / name of the form field
     * @return value or -1 if not found
     */
    long getLongVal(String key);
    
    /**
     * Same as getLong but returns null if value is 0
     * @param key
     * @return value if &lt;&gt; 0 or null 
     */
    Long getSelection(String key);

    /**
     * Tries to get a boolean value of an object specified as Boolean in form definition
     * @param key / name of the form field
     * @return true if set, false if not set or found
     */
    boolean getBoolean(String key);

    /**
     * Tries to retrieve an array of String objects specified as String[] in the form definition.
     * @param key / name of the form field
     * @return array of string or null if not set or found
     */
    String[] getIndexedStrings(String key);

    /**
     * Tries to retrieve an array of Integer objects specified as Integer[] in the form definition.
     * @param key / name of the form field
     * @return array of integer or null if not set or found
     */
    Integer[] getIndexedIntegers(String key);

    /**
     * Tries to retrieve an array of Long objects specified as Long[] in the form definition.
     * @param key / name of the form field
     * @return array of long or null if not set or found
     */
    Long[] getIndexedLong(String key);

    /**
     * Retrieves indexed double value
     * @param key of form field
     * @return array of double values or null if none exist
     * @throws ClientException if any value has an invalid format
     */
    Double[] getIndexedDouble(String key) throws ClientException;

    /**
     * Retrieves a request parameter string as date
     * @param key request param value of key
     * @return date or null if not set
     * @throws ClientException if value was set but illegal format
     */
    Date getDate(String key) throws ClientException;

    /**
     * Tries to extract a Double value from the form variable key.
     * @param key the name of the variable holding the value
     * @return big decimal object or null if variable not exists
     * @throws ClientException if value exists and is no number
     */
    BigDecimal getDecimal(String key) throws ClientException;

    /**
     * Tries to extract a Double value from the form variable key.
     * @param key the name of the variable holding the value
     * @return object or null if variable not exists
     * @throws ClientException if value exists and is no number
     */
    Double getDouble(String key) throws ClientException;

    /**
     * Tries to extract a Double value from the form variable key and returns value as string to provide a convenient method for later construction of a
     * BigDecimal instance.
     * @param key the name of the variable holding the value
     * @return string representation of double or null if variable not exists
     * @throws ClientException if value exists and is no number
     */
    String getDoubleAsString(String key) throws ClientException;
    
    /**
     * Tries to extract an hours value
     * @param varName
     * @return hours or 0
     * @throws ClientException if hours exist but value or format is invalid
     */
    Integer getHours(String varName) throws ClientException;

    /**
     * Tries to extract a minute value
     * @param varName
     * @return minutes or 0
     * @throws ClientException if minutes exist but value or format is invalid
     */
    Integer getMinutes(String varName) throws ClientException;

    /**
     * Provides a decimal percentage representation of a percentage value expected as string under given key
     * @param key
     * @return percentage representation or 0
     */
    Double getPercentage(String key);

    /**
     * Provides a decimal percentage representation of a percentage value expected as string under given key
     * @param key
     * @param required
     * @return percentage representation or null if required is false and no value found
     * @throws ClientException if string found under key represents no number
     */
    Double getPercentage(String key, boolean required) throws ClientException;

    /**
     * Resets a local value if implementing view supports this
     * @param key
     */
    void resetValue(String key);

    /**
     * Sets a local value. The method may also implement an
     * object type string conversion based on provided type.
     * @param key
     * @param value
     */
    void setValue(String key, Object value);

    /**
     * Provides the internal implementation of the form (request param referencing object)
     * @return form
     */
    Object getForm();

    /**
     * Provides the uploaded file on multipart request
     * @return upload
     * @throws ClientException if upload is null or io exception occurs
     */
    FileObject getUpload() throws ClientException;

    /**
     * Provides all uploaded files of a multipart request if page supports
     * multiple files per upload and user selected more than one file.
     * @return uploads or empty list if no uploaded file exists
     */
    List<FileObject> getUploads();

    /**
     * Provides the supplied values as key/values
     * @return mapped form values
     */
    Map getMapped();

    /**
     * Checks if given object is null or zero
     * @param o
     * @throws ClientException if object is null or zero
     */
    void validateSet(Object o) throws ClientException;
}
