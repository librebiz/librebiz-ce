<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.stocktakingView}" />
<c:if test="${!empty view.bean}">
    <c:set var="obj" value="${view.bean}" />
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="actualStockTaking" /></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="stockTakingType" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.bean.closed}">
                                    <o:out value="${obj.type.name}" />
                                </c:when>
                                <c:otherwise>
                                    <a href="<c:url value="/stocktakings.do?method=enableEdit"/>" title="<fmt:message key="settings" />">
                                        <o:out value="${obj.type.name}" />
                                    </a>                                
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="stockTakingName" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${obj.name}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="deadline" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:date value="${obj.recordDate}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="createdBy" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:ajaxLink linkId="createdBy${obj.createdBy}" url="${'/employees/employeePopup/forward?id='}${obj.createdBy}">
                                <oc:employee value="${obj.createdBy}" />
                            </v:ajaxLink>
                        </div>
                    </div>
                </div>

                <c:if test="${!empty obj.counter}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="liable" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:ajaxLink linkId="counter${obj.counter}" url="${'/employees/employeePopup/forward?id='}${obj.counter}">
                                    <oc:employee value="${obj.counter}" />
                                </v:ajaxLink>
                            </div>
                        </div>
                    </div>
                </c:if>

                <c:if test="${!empty obj.writer}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="input" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:ajaxLink linkId="writer${obj.writer}" url="${'/employees/employeePopup/forward?id='}${obj.writer}">
                                    <oc:employee value="${obj.writer}" />
                                </v:ajaxLink>
                            </div>
                        </div>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="status" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:if test="${view.bean.closed}">
                                <v:link url="/records/stocktakingPdf/print?name=stocktaking_report" title="printStocktakingReport">
                                    <o:img name="printIcon" />
                                </v:link>
                            </c:if>
                            <span> <o:out value="${obj.status.name}" /></span>
                        </div>
                    </div>
                </div>
                <c:if test="${!empty obj.note}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="comments" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${obj.note}" />
                            </div>
                        </div>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="productList" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:listSize value="${obj.items}" /> <fmt:message key="productWithStockFound" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="with" />
                            <input type="text" id="count" value="<o:out value="${view.listStep}"/>" style="width: 45px; text-align: center;" />
                            <a href="javascript:forwardList('<c:url value="/stocktakings.do?method=forwardProducts&count="/>');">
                                <fmt:message key="displayProductPerPage" />
                            </a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

</c:if>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="stockTakingHistory" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table">
                <tbody>
                    <c:choose>
                        <c:when test="${empty view.list}">
                            <tr>
                                <td><fmt:message key="noneAvailable" /></td>
                                <td>
                                    <c:if test="${empty view.bean}">
                                        <a href="<c:url value="/stocktakings.do?method=enableCreate"/>"><fmt:message key="createNew" /></a>
                                    </c:if>
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="obj" items="${view.list}" varStatus="s">
                                <tr>
                                    <td><o:out value="${obj.description}" /></td>
                                    <td><a href="<c:url value="/stocktakings.do?method=select&id=${obj.id}"/>"><o:out value="${obj.name}" /></a></td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </tbody>
            </table>
        </div>
    </div>
</div>
