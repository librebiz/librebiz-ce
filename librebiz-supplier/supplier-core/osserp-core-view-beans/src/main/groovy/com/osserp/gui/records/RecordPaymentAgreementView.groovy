/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 4, 2014 
 * 
 */
package com.osserp.gui.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Constants;

import com.osserp.core.finance.PaymentAgreementAwareRecord
import com.osserp.core.finance.PaymentAgreementAwareRecordManager


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RecordPaymentAgreementView extends AbstractRecordView {

    private static Logger logger = LoggerFactory.getLogger(RecordPaymentAgreementView.class.getName())

    RecordPaymentAgreementView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest && recordManager) {
            bean = fetchRecordByRequestId()
            logger.debug("initRequest: done [record=${bean?.id}]")
        }
    }

    @Override
    void save() {
        if (recordManager instanceof PaymentAgreementAwareRecordManager
            && bean instanceof PaymentAgreementAwareRecord) {
            
            def fixedAmounts = form.getBoolean('fixedAmounts')
            
            if (updateOnly) {
                
                ((PaymentAgreementAwareRecord) bean).updatePaymentAgreement(
                    form.getBoolean('hidePayments'),
                    fixedAmounts,
                    fetchValue(fixedAmounts, 'downpayment'),
                    fetchValue(fixedAmounts, 'deliveryInvoice'),
                    fetchValue(fixedAmounts, 'finalInvoice'),
                    form.getLong('downpaymentTargetId'),
                    form.getLong('deliveryInvoiceTargetId'),
                    form.getLong('finalInvoiceTargetId'),
                    getOption('recordPaymentConditions', 
                        form.getLong('paymentConditionId')),
                    form.getString('note'),
                    domainEmployee.id)
    
            } else {

                recordManager.update(
                    bean,
                    form.getBoolean('hidePayments'),
                    fixedAmounts,
                    fetchValue(fixedAmounts, 'downpayment'),
                    fetchValue(fixedAmounts, 'deliveryInvoice'),
                    fetchValue(fixedAmounts, 'finalInvoice'),
                    form.getLong('downpaymentTargetId'),
                    form.getLong('deliveryInvoiceTargetId'),
                    form.getLong('finalInvoiceTargetId'),
                    form.getLong('paymentConditionId'),
                    form.getString('note'),
                    domainEmployee)
            }
        }
    }

    void switchFixedAmounts() {
        recordManager?.switchPaymentFixedAmountsFlag(bean)
        reload()
    }

    @Override
    void reload() {
        if (bean) {
            bean = recordManager?.find(bean.id)
        }
    }

    private Double fetchValue(boolean fixedAmounts, String name) {
        fixedAmounts ? form.getDouble("${name}Amount") :
                form.getDouble("${name}Percent")
    }
}
