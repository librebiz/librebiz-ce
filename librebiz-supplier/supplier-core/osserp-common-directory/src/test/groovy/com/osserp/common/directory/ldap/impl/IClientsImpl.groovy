/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.junit.Test

import static org.junit.Assert.*

import com.osserp.TestConfig
import com.osserp.common.directory.ldap.impl.ClientsImpl
import com.osserp.common.directory.ldap.LdapConfig

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class IClientsImpl {
    private static Logger logger = LoggerFactory.getLogger(IClientsImpl.class.getName())

    @Test
    void initClients() {
        ClientsImpl impl = new ClientsImpl(config)
        assertTrue(impl.config.domainAvailable)
    }

    @Test
    void createFindDelete() {
        ClientsImpl impl = new ClientsImpl(config)
        if (impl.config.domainAvailable) {
            Map obj = impl.create('osserptest', 1110111L, 'testclient', 'test@example.com', true)
            assertTrue(obj != null)
            Map created = impl.find('osserptest')
            assertTrue(created != null)
            impl.delete(created)
            obj = impl.find('osserptest')
            assertTrue(!obj)
        }
    }

    @Test
    void findAll() {
        ClientsImpl impl = new ClientsImpl(config)
        if (impl.config.domainAvailable) {
            List<Map> list = impl.findAll()
            assertTrue(!list.empty)
        }
    }


    private LdapConfig getConfig() {
        new LdapConfig(TestConfig.PROPERTY_FILE)
    }
}
