/**
 *
 * Copyright (C) 2019 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 10, 2019
 *
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.BackendException

import com.osserp.core.BusinessCase
import com.osserp.core.BusinessCaseSearch
import com.osserp.core.requests.Request
import com.osserp.core.sales.Sales

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.requests.RequestCreatorView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class SalesByPurchaseView extends RequestCreatorView {
    private static Logger logger = LoggerFactory.getLogger(SalesByPurchaseView.class.getName())

    String successTarget

    SalesByPurchaseView() {
        super()
        headerName = 'createNewOrder'
        dependencies = [ 'orderByPurchaseView' ]
        businessTypeSelectionName = 'salesByPurchase'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            successTarget = form.getString('success')
            if (!successTarget) {
                throw new ViewContextException('success param missing')
            }
        }
    }

    @Override
    protected void initCustomer() {
        setSelectedCustomer(env.orderByPurchaseView?.customer)
        logger.debug("initCustomer: done [id=${customer.id}]")
    }

    @Override
    protected void initBusinessTypeSelection(String name) {
        super.initBusinessTypeSelection(name)
        initializeBean()
    }

    @Override
    void selectType() {
        super.selectType()
        initializeBean()
    }

    protected void initializeBean() {
        if (bean) {
            BusinessCase latest = businessCaseSearch.findLatest(customer.id)
            if (latest) {
                Request vo = bean
                Request latestRequest = latest.isSalesContext() ? latest.request : latest
                if (latestRequest?.origin) {
                    vo.setOrigin(latestRequest?.origin)
                }
                if (latestRequest?.originType) {
                    vo.updateOriginType(vo.createdBy, latestRequest?.originType)
                }
            } else {
                logger.debug('initializeBean: no existing businessCase found')
            }
        }
    }

    @Override
    void save() {
        super.save()
        if (!updateOnly) {
            if (!(bean instanceof Sales) && !(bean instanceof Request)) {
                throw new BackendException('save failed for unknown reason')
            } else if (bean instanceof Sales) {
                selectedSales = bean
            } else {
                selectedRequest = bean
            }
        }
    }

    protected BusinessCaseSearch getBusinessCaseSearch() {
        getService(BusinessCaseSearch.class.getName())
    }
}
