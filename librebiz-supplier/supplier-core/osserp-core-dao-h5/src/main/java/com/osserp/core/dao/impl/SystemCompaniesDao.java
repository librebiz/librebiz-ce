/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Jul-2006 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dao.Tables;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.OfficeContact;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.finance.Stock;
import com.osserp.core.model.StockImpl;
import com.osserp.core.model.contacts.ContactImpl;
import com.osserp.core.model.system.SystemCompanyBankAccountImpl;
import com.osserp.core.model.system.SystemCompanyImpl;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SystemCompaniesDao extends AbstractRepository implements SystemCompanies {
    private static Logger log = LoggerFactory.getLogger(SystemCompaniesDao.class.getName());

    private String companiesTable = null;
    private String customersTable = null;

    protected SystemCompaniesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        this.companiesTable = getTable(TableKeys.SYSTEM_COMPANIES);
        this.customersTable = getTable(TableKeys.CUSTOMERS);
    }

    public List<SystemCompany> getActive() {
        return getCurrentSession().createQuery(
                "from " + SystemCompanyImpl.class.getName() 
                + " sc where sc.endOfLife = false order by sc.name").list();
    }

    public List<SystemCompany> getAll() {
        return getCurrentSession().createQuery(
                "from " + SystemCompanyImpl.class.getName() 
                + " sc order by sc.name").list();
    }

    public List<Option> getShortnames() {
        List<Option> list = new ArrayList<>();
        List<SystemCompany> companies = getAll();
        for (int i = 0, j = companies.size(); i < j; i++) {
            SystemCompany company = companies.get(i);
            list.add(new OptionImpl(company.getId(), company.getShortname()));
        }
        return list;
    }

    public SystemCompany find(Long id) {
        List<SystemCompany> list = getCurrentSession().createQuery(
                "from " + SystemCompanyImpl.class.getName() 
                + " sc where sc.id = :pk or sc.contact.contactId = :contactId")
                .setParameter("pk", id)
                .setParameter("contactId", id)
                .list();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return getCompanyByCustomer(id);
    }

    public void removeGroup(Long id) {
        List<SystemCompany> companies = getAll();
        for (int i = 0, j = companies.size(); i < j; i++) {
            SystemCompany company = companies.get(i);
            if (company.removeGroup(id)) {
                save(company);
            }
        }
    }

    public List<OfficeContact> getOfficeContacts() {
        return getCurrentSession().createQuery(
                "from OfficeContactImpl o order by o.id").list();
    }

    public void save(SystemCompany company) {
        getCurrentSession().merge(SystemCompanyImpl.class.getName(), company);
    }

    public Stock createStock(SystemCompany company, BranchOffice office) {
        assert (company != null);
        Stock result = null;
        List<Stock> available = getStocks();
        Long stockId = office != null ? (company.getId() + office.getId()) : company.getId();
        for (int i = 0, j = available.size(); i < j; i++) {
            Stock next = available.get(i);
            if (next.getId().equals(stockId)) {
                result = next;
                break;
            }
        }
        if (result != null) {
            return result;
        }
        if (office == null) {
            result = new StockImpl(
                    stockId,
                    company.getId(),
                    null,
                    company.getContact().getLastName(),
                    company.getContact().getAddress().getCity(),
                    company.getShortname());
        } else {
            result = new StockImpl(
                    stockId,
                    company.getId(),
                    office.getId(),
                    office.getName(),
                    office.getContact().getAddress().getCity(),
                    office.getShortkey());
        }
        getCurrentSession().save(StockImpl.class.getName(), result);
        return result;
    }

    public Stock getStockByBranch(Long company, Long office) {
        Stock result = null;
        try {
            List<Stock> list = getStocks();
            for (Iterator<Stock> i = list.iterator(); i.hasNext();) {
                Stock stock = i.next();
                if (office != null && stock.getBranchId() != null
                        && office.equals(stock.getBranchId())
                        && stock.isActive() && !stock.isEndOfLife()) {
                    result = stock;
                } else if (office == null && stock.getReference() != null
                        && stock.getReference().equals(company)
                        && stock.isActive() && !stock.isEndOfLife()
                        && stock.isDefaultStock()) {
                    result = stock;
                }
            }
            if (result == null) {
                result = getDefaultStock();
            }
        } catch (Exception e) {
            log.warn("Failed to get stock by record: " + e.getMessage(), e);
            throw new BackendException(e);
        }
        return result;
    }

    public Stock getStockById(Long id) {
        Stock result = null;
        try {
            result = (Stock) getCurrentSession().load(StockImpl.class.getName(), id);
        } catch (Exception e) {
            log.warn("Failed to load stock by id: " + e.getMessage(), e);
            throw new BackendException(e);
        }
        return result;
    }

    public List<Stock> getStocks() {
        return getCurrentSession().createQuery(
                "from " + StockImpl.class.getName() +
                " stock order by stock.id").list();
    }

    public Stock updateStock(
            Stock stock,
            String shortKey,
            String name,
            String description,
            boolean active,
            Date activationDate,
            boolean planningAware)
        throws ClientException {

        List<Stock> available = getStocks();
        for (int i = 0, j = available.size(); i < j; i++) {
            Stock next = available.get(i);
            if (!next.getId().equals(stock.getId())) {
                if (next.getName().equalsIgnoreCase(name)) {
                    throw new ClientException(ErrorCode.SHORTNAME_EXISTS);
                }
                if (next.getDescription().equalsIgnoreCase(description)) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
                if (next.getShortKey().equalsIgnoreCase(shortKey)) {
                    throw new ClientException(ErrorCode.SHORTKEY_EXISTS);
                }
            }
        }
        stock.setDescription(description);
        stock.setName(name);
        stock.setShortKey(shortKey);
        stock.setActivationDate(activationDate);
        stock.setActive(active);
        stock.setPlanningAware(planningAware);
        getCurrentSession().merge(StockImpl.class.getName(), stock);
        return stock;
    }

    public List<BankAccount> getBankAccounts() {
        return getCurrentSession().createQuery(
                "from " + SystemCompanyBankAccountImpl.class.getName() +
                " ba order by ba.name").list();
    }

    public BankAccount getBankAccount(Long id) {
        return (BankAccount) getCurrentSession().load(SystemCompanyBankAccountImpl.class, id);
    }
    
    public void assignBankAccount(Long bankAccountId, Long supplierId) {
        try {
            SystemCompanyBankAccountImpl obj = (SystemCompanyBankAccountImpl)
                    getCurrentSession().load(SystemCompanyBankAccountImpl.class, bankAccountId);
            obj.setSupplierId(supplierId);
            getCurrentSession().saveOrUpdate(SystemCompanyBankAccountImpl.class.getName(), obj);
        } catch (Exception e) {
            log.warn("assignBankAccount() ignoring exception [bankAccountId="
                    + bankAccountId + ", supplierId=" + supplierId
                    + ", message=" + e.getMessage() + "]", e);
        }
    }

    public Stock getDefaultStock() {
        List<Stock> stocks = getStocks();
        for (int i = 0, j = stocks.size(); i < j; i++) {
            Stock next = stocks.get(i);
            if (next.isDefaultStock()) {
                return next;
            }
        }
        return null;
    }

    public SystemCompany getCompany(Long id) {
        SystemCompany systemCompany = (SystemCompany)
                getCurrentSession().load(SystemCompanyImpl.class, id);
        return systemCompany;
    }

    public SystemCompany getCompanyByCustomer(Long customerId) {
        StringBuilder sql = new StringBuilder(128);
        sql
                .append("SELECT id FROM ")
                .append(companiesTable)
                .append(" WHERE contact_id IN (SELECT contact_id FROM ")
                .append(customersTable)
                .append(" WHERE id = ?)");
        List<Long> ids = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                new Object[] { customerId },
                new int[] { Types.BIGINT },
                getLongRowMapper());
        return (ids.isEmpty() ? null : getCompany(ids.get(0)));
    }

    public SystemCompany createCompany(SystemCompany companyProposal) throws ClientException {
        if (!companyProposal.getContact().isCustomer()) {
            throw new ClientException(ErrorCode.CUSTOMER_TYPE_MISSING);
        }
        if (companyProposal.getId() == null) {
            throw new ClientException(ErrorCode.ID_REQUIRED);
        }
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(companiesTable).append(" WHERE id = ").append(companyProposal.getId());
        if (jdbcTemplate.queryForObject(sql.toString(), Integer.class) > 0) {
            throw new ClientException(ErrorCode.ID_EXISTS);
        }
        getCurrentSession().save(SystemCompanyImpl.class.getName(), companyProposal);
        return getCompany(companyProposal.getId());
    }

    public SystemCompany createCompanySuggestion(Contact contact) throws ClientException {
        if (!contact.isCustomer()) {
            throw new ClientException(ErrorCode.CUSTOMER_TYPE_MISSING);
        }
        ContactImpl obj = (ContactImpl) getCurrentSession().load(
                ContactImpl.class.getName(), contact.getContactId());
        Long id = getNextCompanyId();
        return new SystemCompanyImpl(id, obj.getDisplayName(), obj);
    }

    private Long getNextCompanyId() {
        StringBuilder sql = new StringBuilder("SELECT max(id) FROM ");
        sql.append(companiesTable);
        try {
            return jdbcTemplate.queryForObject(sql.toString(), Long.class) + 100;
        } catch (Exception e) {
            return 100L;
        }
    }
}
