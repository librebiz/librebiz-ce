<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="purchaseOrderSearchView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="onload" type="string">onload="$('value').focus();"</tiles:put>
    <tiles:put name="headline"><fmt:message key="purchaseOrderSearch"/></tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="purchaseOrderSearchContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <div class="panel-body">
                        <c:import url="${viewdir}/purchasing/_purchaseOrderSearchForm.jsp"/>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
