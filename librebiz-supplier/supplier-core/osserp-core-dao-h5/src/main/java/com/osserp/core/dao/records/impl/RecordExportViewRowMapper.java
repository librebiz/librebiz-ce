/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 15, 2014 3:23:13 PM
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.finance.RecordComparators;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordExport;
import com.osserp.core.model.records.RecordExportImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordExportViewRowMapper extends RecordDisplayRowMapper {

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new RecordExportViewRowMapper());
    }

    protected static final String EXPORT_VIEW_VALUES = "ign,export,exported," + RECORD_VALUES;

    @Override
    protected void populate(ResultSet rs, Map<String, Object> map) {
        super.populate(rs, map);
        addLong(map, "exportId", rs, "export");
        addDate(map, "exportDate", rs, "exported");
        addBoolean(map, "exportIgnorable", rs, "ign");
    }

    public static List<RecordExport> createViewList(OptionsCache optionsCache, List<Map<String, Object>> values) {
        List<RecordDisplay> displayList = createDisplayList(optionsCache, values);
        return createViewList(displayList);
    }

    public static List<RecordExport> createViewList(List<RecordDisplay> displayList) {
        Map<Long, RecordExport> result = new HashMap<Long, RecordExport>();
        int size = displayList.size();
        for (int i = 0; i < size; i++) {
            RecordDisplay recordObj = displayList.get(i);
            if (result.containsKey(recordObj.getExportId())) {
                RecordExport export = result.get(recordObj.getExportId());
                export.getRecords().add(recordObj);
            } else {
                RecordExport export = new RecordExportImpl(recordObj);
                result.put(export.getId(), export);
            }
        }
        List<RecordExport> finalResult = new ArrayList<RecordExport>(result.values());
        return CollectionUtil.sort(finalResult, RecordComparators.createExportViewByCreatedComparator(true));
    }

}
