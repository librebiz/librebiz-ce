/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Feb-2007 09:27:20 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.PurchaseDeliveryNotes;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.products.Product;
import com.osserp.core.purchasing.PurchaseDeliveryNote;
import com.osserp.core.purchasing.PurchaseDeliveryNoteManager;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseDeliveryNoteManagerImpl extends AbstractDeliveryNoteManager
        implements PurchaseDeliveryNoteManager {
    private static Logger log = LoggerFactory.getLogger(PurchaseDeliveryNoteManagerImpl.class.getName());

    public PurchaseDeliveryNoteManagerImpl(
            PurchaseDeliveryNotes notes,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            PurchaseOrders orders) {
        super(notes, systemConfigManager, products, productPlanningTask, orders);
    }

    public DeliveryNote create(Employee user, Order order, DeliveryNoteType type, Date deliveryDate) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [order=" + order.getId() + "]");
        }
        List<Record> referenced = new ArrayList(getDao().getByReference(order.getId()));
        for (int i = 0, j = referenced.size(); i < j; i++) {
            Record next = referenced.get(i);
            if (!next.isUnchangeable()) {
                throw new ClientException(ErrorCode.UNRELEASED_RECORD);
            }
        }
        order = (Order) getOrders().load(order.getId());
        return getPurchaseDeliveryNotesDao().create(user, order, type);
    }

    public List<DeliveryNote> findOpen(Long stockId, Long productId) {
        return getPurchaseDeliveryNotesDao().getOpen(stockId, productId);
    }

    public void reopenClosed(Employee user, PurchaseDeliveryNote note) {
        getPurchaseDeliveryNotesDao().reopenClosed(user, note);
        reloadSummary(note);
        createProductPlanningRefreshTask(note, null);
    }

    @Override
    public ItemChangedInfo addItem(
            Employee user,
            Record record,
            Product product,
            Long stockId,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            String note) throws ClientException {
        return addItem(user, record, stockId, product, customName, quantity, taxRate, price, note);
    }

    @Override
    public void delete(Record record, Employee employee) throws ClientException {
        super.delete(record, employee);
        if (record.isUnchangeable()) {
            reloadSummary((DeliveryNote) record);
        }
        refreshDeliveries((DeliveryNote) record);
    }

    private PurchaseDeliveryNotes getPurchaseDeliveryNotesDao() {
        return (PurchaseDeliveryNotes) getDao();
    }
}
