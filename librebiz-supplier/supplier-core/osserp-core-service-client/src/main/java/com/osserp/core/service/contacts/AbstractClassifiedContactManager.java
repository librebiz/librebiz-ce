/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2008 3:14:06 PM 
 * 
 */
package com.osserp.core.service.contacts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.PersonManager;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.ClassifiedContacts;
import com.osserp.core.dao.Contacts;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.contacts.AbstractContactAware;
import com.osserp.core.tasks.ContactUpdateSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractClassifiedContactManager extends AbstractContactManager implements PersonManager {
    private static Logger log = LoggerFactory.getLogger(AbstractClassifiedContactManager.class.getName());

    private ClassifiedContacts contactRelationsDao = null;

    /**
     * Creates a new contact relation managers super class
     * @param contactsDao
     * @param contactRelationsDao
     * @param contactUpdateSender
     */
    protected AbstractClassifiedContactManager(Contacts contactsDao, ClassifiedContacts contactRelationsDao, ContactUpdateSender contactUpdateSender) {
        super(contactsDao, contactUpdateSender);
        this.contactRelationsDao = contactRelationsDao;
    }

    @Override
    public void changeType(
            Employee changingUser,
            Person personToChange,
            String company) throws ClientException {

        getContactsDao().changeType(changingUser.getId(), fetchContact(personToChange), company);
        if (log.isDebugEnabled()) {
            log.debug("changeType() done [id=" + personToChange.getPrimaryKey() + "]");
        }
    }

    @Override
    public Person find(Long id) {
        if (getContactsDao().exists(id)) {
            if (log.isDebugEnabled()) {
                log.debug("find() by contact id invoked [contactId=" + id
                        + ", manager=" + getClass().getName() + "]");
            }
            return contactRelationsDao.findByContact(id);
        }
        if (log.isDebugEnabled()) {
            log.debug("find() by id invoked [id=" + id + "]");
        }
        return contactRelationsDao.find(id);
    }

    @Override
    protected final void persist(Contact contact) {
        boolean saved = false;
        if (contact instanceof ClassifiedContact) {
            contactRelationsDao.save((ClassifiedContact) contact);
            saved = true;
        }
        Contact obj = fetchContact(contact);
        if (obj != null) {
            if (!saved) {
                getContactsDao().save(obj);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("persist() done [id=" + contact.getPrimaryKey() + "]");
        }
    }

    @Override
    public void addEmail(
            Employee user,
            Contact contact,
            Long type,
            String email,
            boolean primary,
            Long aliasOf) throws ClientException {
        super.addEmail(user, fetchContact(contact), type, email, primary, aliasOf);
    }

    protected ClassifiedContacts getContactRelationsDao() {
        return contactRelationsDao;
    }

    protected final Contact fetchContact(Person person) {
        Contact contact = null;
        if (person instanceof AbstractContactAware) {
            if (log.isDebugEnabled()) {
                log.debug("fetchContact() invoked with abstract contact aware, extracting contact...");
            }
            AbstractContactAware aca = (AbstractContactAware) person;
            contact = aca.getRc();
        } else if (person instanceof Contact) {
            if (log.isDebugEnabled()) {
                log.debug("fetchContact() invoked [class="
                        + person.getClass().getName()
                        + "]");
            }
            contact = (Contact) person;
        }
        if (contact == null) {
            log.warn("fetchContact() unexpected entity [class="
                    + (person == null ? "null" : person.getClass().getName())
                    + "]");
        }
        return contact;
    }

    public final void save(Person person) {
        if (person instanceof Contact) {
            Contact c = (Contact) person;
            persist(c);
            if (c instanceof ClassifiedContact) {
                ClassifiedContact rc = (ClassifiedContact) c;
                this.doAfterSave(rc);
            }
            sendChangedEvent(c);
        } else {
            throw new IllegalArgumentException("save expected instance of " + Contact.class.getName()
                    + ", found " + (person == null ? "null" : person.getClass().getName()));
        }
    }

    protected void doAfterSave(ClassifiedContact contact) {
        // default implementation does nothing
    }
}
