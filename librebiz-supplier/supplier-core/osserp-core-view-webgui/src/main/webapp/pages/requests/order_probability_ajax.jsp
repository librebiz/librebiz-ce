<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<c:set var="view" value="${sessionScope.orderProbabilityView}" />
<c:set var="businessCase" value="${view.request}" />

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="orderProbabilityChange" />
    </div>
</div>
<div class="modalBoxData">

    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <o:ajaxForm name="dynamicForm" onSuccess="" preRequest="" postRequest="" targetElement="order_probability_popup" url="/orderProbability.do">
                <input type="hidden" name="method" value="save" />
                <table class="valueTable inputs100">
                    <tr>
                        <td><fmt:message key="toOrderReceipt" /></td>
                        <td><select name="orderProbability" size="1" style="">
                                <c:forEach var="next" items="${view.percentageList}">
                                    <c:choose>
                                        <c:when test="${next == businessCase.orderProbability}">
                                            <option value="<o:out value="${next}"/>" selected="selected"><o:out value="${next}" /></option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="<o:out value="${next}"/>"><o:out value="${next}" /></option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                        </select> %</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="date" /></td>
                        <td><input type="text" id="orderProbabilityDate" name="orderProbabilityDate" value="<o:date value="${businessCase.orderProbabilityDate}"/>" />&nbsp;<o:datepicker input="orderProbabilityDate" /></td>
                    </tr>
                    <tr>
                        <td class="row-submit"><o:ajaxLink url="/orderProbability.do?method=exit" linkId="exitLink" targetElement="order_probability_popup">
                                <input class="cancel" type="button" value="<fmt:message key="exit"/>" />
                            </o:ajaxLink></td>
                        <td class="row-submit"><o:submit /></td>
                    </tr>
                </table>
            </o:ajaxForm>
            <div class="spacer"></div>
        </div>
    </div>
</div>
