/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 28-Jul-2006 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.RowMapper;

import com.osserp.common.BackendException;
import com.osserp.common.OptionsCache;

import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.model.SalesSummaryImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectSummaryRowMapper extends AbstractProjectRowMapper implements RowMapper {
    private static Logger log = LoggerFactory.getLogger(ProjectSummaryRowMapper.class.getName());
    public static final String SELECT_FROM =
            "SELECT p.id,p.type_id,p.manager_id,p.status,p.created," +
                    "p.cancelled,pl.sales_id, pl.name, p.stopped FROM ";

    public ProjectSummaryRowMapper(OptionsCache optionsCache, FlowControlActions actionsDao) {
        super(optionsCache, actionsDao);
    }

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        try {
            Long projectId = Long.valueOf(rs.getLong(1));
            SalesSummaryImpl vo = new SalesSummaryImpl(
                    projectId,
                    fetchBusinessType(Long.valueOf(rs.getLong(2))),
                    Long.valueOf(rs.getLong(7)),
                    Long.valueOf(rs.getLong(3)),
                    rs.getString(8),
                    Integer.valueOf(rs.getInt(4)),
                    actionsDao.getLastAction(projectId),
                    rs.getDate(5),
                    rs.getBoolean(6),
                    rs.getBoolean(9));
            /*
             * if (log.isDebugEnabled()) { log.debug("mapRow() done, mapped sale " + projectId); }
             */
            return vo;
        } catch (Throwable t) {
            log.error("mapRow() failed: " + t.toString(), t);
            throw new BackendException(t.getMessage(), t);
        }
    }

}
