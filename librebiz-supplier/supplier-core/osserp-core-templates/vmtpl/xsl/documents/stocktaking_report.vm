<?xml version="1.0" encoding="UTF-8"?>
<!-- Inventur-Report / stocktaking_report -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.1">
    <xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes" />
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="ersteSeite" page-height="21cm" page-width="29.7cm" margin-left="1.5cm" margin-right="1.5cm">
                    <fo:region-body margin-top="3cm" margin-bottom="1.6cm" />
                    <fo:region-before extent="3cm" />
                    <fo:region-after extent="1.6cm" />
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="ersteSeite">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block font-family="$documentPrintFontFamily" font-size="10pt" text-align="left" space-before="30pt">
                        <xsl:value-of select="/root/stocktaking/company/contact/address/header" />
                        ,
                        <xsl:value-of select="/root/stocktaking/company/contact/address/street" />
                        ,
                        <xsl:value-of select="/root/stocktaking/company/contact/address/city" />
                    </fo:block>
                    <fo:block font-family="$documentPrintFontFamily" font-size="16pt" font-weight="bold" text-align="center" space-before="20pt" space-after="20pt">Inventurbewertung</fo:block>
                </fo:static-content>
                <fo:static-content flow-name="xsl-region-after">
                    <fo:table font-family="$documentPrintFontFamily" table-layout="fixed">
                        <fo:table-column column-width="8.9cm" column-number="1" />
                        <fo:table-column column-width="8.9cm" column-number="2" />
                        <fo:table-column column-width="8.9cm" column-number="3" />
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" space-before="14pt" text-align="left">
                                        Ausdruck vom:
                                        <xsl:value-of select="/root/currentDate" />
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell />
                                <fo:table-cell>
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" space-before="14pt" text-align="right">
                                        Seite
                                        <fo:page-number />
                                        von
                                        <fo:page-number-citation ref-id="lastBlock" />
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left">
                        Lagerstand:
                        <xsl:value-of select="/root/stocktaking/recordDate" />
                    </fo:block>
                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left">Die Inventur-Bewertung erfolgt auf der Grundlage des durchschnittlichen Einkaufspreises der vergangenen 12 Monate</fo:block>
                    <fo:table font-family="$documentPrintFontFamily" table-layout="fixed" keep-together.within-column="always">
                        <fo:table-column column-width="2cm" column-number="1" />
                        <fo:table-column column-width="10.1cm" column-number="2" />
                        <fo:table-column column-width="2cm" column-number="3" />
                        <fo:table-column column-width="2.6cm" column-number="4" />
                        <fo:table-column column-width="3cm" column-number="5" />
                        <fo:table-column column-width="3.5cm" column-number="6" />
                        <fo:table-column column-width="3.5cm" column-number="7" />
                        <fo:table-body>
                            <xsl:for-each select="/root/stocktaking/group">
                                <fo:table-row>
                                    <fo:table-cell padding-top="2pt" padding-bottom="1pt" number-columns-spanned="7">
                                        <fo:block font-family="$documentPrintFontFamily" font-size="12pt" font-weight="bold" space-before="10pt">
                                            Warengruppe
                                            <xsl:value-of select="@name" />
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell border-style="solid" border-color="black" border-width="1.25pt" padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                        <fo:block font-family="$documentPrintFontFamily" font-size="8pt" font-weight="bold">Artikel Nr.</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border-style="solid" border-color="black" border-width="1.25pt" padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                        <fo:block font-family="$documentPrintFontFamily" font-size="8pt" font-weight="bold">Bezeichnung</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border-style="solid" border-color="black" border-width="1.25pt" padding-top="2pt" padding-bottom="1pt">
                                        <fo:block font-family="$documentPrintFontFamily" font-size="8pt" font-weight="bold" text-align="center">Einheit</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border-style="solid" border-color="black" border-width="1.25pt" padding-top="2pt" padding-bottom="1pt">
                                        <fo:block font-family="$documentPrintFontFamily" font-size="8pt" font-weight="bold" text-align="center">Preis pro Anzahl</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border-style="solid" border-color="black" border-width="1.25pt" padding-top="2pt" padding-bottom="1pt">
                                        <fo:block font-family="$documentPrintFontFamily" font-size="8pt" font-weight="bold" text-align="center">Lagerbestand</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border-style="solid" border-color="black" border-width="1.25pt" padding-top="2pt" padding-bottom="1pt">
                                        <fo:block font-family="$documentPrintFontFamily" font-size="8pt" font-weight="bold" text-align="center">Einkaufskosten</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border-style="solid" border-color="black" border-width="1.25pt" padding-top="2pt" padding-bottom="1pt">
                                        <fo:block font-family="$documentPrintFontFamily" font-size="8pt" font-weight="bold" text-align="right">Einkaufswert</fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                                <xsl:for-each select="product">
                                    <fo:table-row>
                                        <fo:table-cell border-style="solid" border-color="black" border-width="0.5pt" padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                            <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left">
                                                <xsl:value-of select="id" />
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-style="solid" border-color="black" border-width="0.5pt" padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                            <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left">
                                                <xsl:value-of select="name" />
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-style="solid" border-color="black" border-width="0.5pt" padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                            <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left">
                                                <xsl:value-of select="quantityUnit" />
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-style="solid" border-color="black" border-width="0.5pt" padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                            <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right">
                                                <xsl:value-of select="packagingUnit" />
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-style="solid" border-color="black" border-width="0.5pt" padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                            <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right">
                                                <xsl:value-of select="currentStock" />
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-style="solid" border-color="black" border-width="0.5pt" padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                            <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right">
                                                <xsl:value-of select="averagePurchasePrice" />
                                                EUR
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell border-style="solid" border-color="black" border-width="0.5pt" padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                            <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right">
                                                <xsl:value-of select="averageStockValue" />
                                                EUR
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </xsl:for-each>
                            </xsl:for-each>
                            <fo:table-row space-before="8pt" height="12pt">
                                <fo:table-cell padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left" />
                                </fo:table-cell>
                                <fo:table-cell padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left" />
                                </fo:table-cell>
                                <fo:table-cell padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left" />
                                </fo:table-cell>
                                <fo:table-cell padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right" />
                                </fo:table-cell>
                                <fo:table-cell padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right" />
                                </fo:table-cell>
                                <fo:table-cell padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right" />
                                </fo:table-cell>
                                <fo:table-cell padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right" />
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row space-before="8pt">
                                <fo:table-cell padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left" />
                                </fo:table-cell>
                                <fo:table-cell padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left" font-weight="bold">Gesamt aus allen Warengruppen</fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-left="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="left" />
                                </fo:table-cell>
                                <fo:table-cell padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right" />
                                </fo:table-cell>
                                <fo:table-cell padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right" font-weight="bold">
                                        <xsl:value-of select="/root/stocktaking/totalStock" />
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right" />
                                </fo:table-cell>
                                <fo:table-cell padding-right="2pt" padding-top="2pt" padding-bottom="1pt">
                                    <fo:block font-family="$documentPrintFontFamily" font-size="8pt" text-align="right" font-weight="bold">
                                        <xsl:value-of select="/root/stocktaking/totalAmount" />
                                        EUR
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                    <fo:block font-family="$documentPrintFontFamily" font-size="1pt" text-align="center" id="lastBlock" />
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>

