/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2005 8:18:57 PM 
 * 
 */
package com.osserp.gui.web.calc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.SessionUtil;
import com.osserp.common.web.struts.TargetActionMapping;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.calc.CalculationView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class CalculationEditAction extends AbstractCalculationAction {
    private static Logger log = LoggerFactory.getLogger(CalculationEditAction.class.getName());

    /**
     * Removes item by id and position and forwards to 'refresh' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward remove(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("remove() request from " + getUser(session).getId());
        }
        Long itemId = RequestUtil.getLong(request, "itemId");
        Long positionId = RequestUtil.getLong(request, "positionId");
        CalculationView view = getCalculationView(session);
        view.getCalculator().deleteProduct(itemId);
        view.setJumpTo(positionId);
        return mapping.findForward(Actions.REFRESH);
    }

    /**
     * Moves item up
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward moveUp(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("moveUp() request from " + getUser(session).getId());
        }
        Long itemId = RequestUtil.getLong(request, "itemId");
        CalculationView view = getCalculationView(session);
        view.getCalculator().moveUpItem(itemId);
        try {
            Long positionId = RequestUtil.getLong(request, "positionId");
            view.setJumpTo(positionId);
        } catch (Exception ignorable) {
            // we don't set jumpto if no position exists
        }
        return mapping.findForward(Actions.REFRESH);
    }

    /**
     * Moves item down
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward moveDown(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("moveDown() request from " + getUser(session).getId());
        }
        Long itemId = RequestUtil.getLong(request, "itemId");
        CalculationView view = getCalculationView(session);
        view.getCalculator().moveDownItem(itemId);
        try {
            Long positionId = RequestUtil.getLong(request, "positionId");
            view.setJumpTo(positionId);
        } catch (Exception ignorable) {
            // we don't set jumpto if no position exists
        }
        return mapping.findForward(Actions.REFRESH);
    }

    /**
     * Changes the calculator class
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeCalculator(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeCalculator() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        String calculatorClassName = RequestUtil.getString(request, "name");
        if (isSet(calculatorClassName)) {
            view.changeCalculator(calculatorClassName);
        }
        return mapping.findForward(Actions.REFRESH);
    }

    /**
     * Changes price display status of an item
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changePriceDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changePriceDisplay() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        Long positionId = RequestUtil.getLong(request, "positionId");
        CalculationView view = getCalculationView(session);
        try {
            view.getCalculator().changePriceDisplayStatus(id);
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        view.setJumpTo(positionId);
        return mapping.findForward(Actions.REFRESH);
    }

    /**
     * Enables price display status of all items
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enablePriceDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enablePriceDisplay() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        try {
            view.getCalculator().enablePriceDisplayStatus();
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(Actions.REFRESH);
    }

    /**
     * Disables price display status of all items
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disablePriceDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disablePriceDisplay() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        try {
            view.getCalculator().disablePriceDisplayStatus();
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(Actions.REFRESH);
    }

    /**
     * Adds a discount for a position
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward addDiscount(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("addDiscount() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        CalculationView view = getCalculationView(session);
        try {
            view.getCalculator().addDiscount(id);
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(Actions.REFRESH);
    }

    /**
     * Calculates the calculation.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward calculate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("calculate() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        try {
            view.calculate();
            if (log.isDebugEnabled()) {
                log.debug("calculate() done");
            }
        } catch (ClientException cx) {
            if (log.isDebugEnabled()) {
                log.debug("calculate() aborted with validation errors: " + cx.toString());
            }
            saveError(request, cx.getMessage());
        }
        return exit(mapping, form, request, response);
    }

    /**
     * Closes the calculation
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward close(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("close() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        ActionForward forward = null;
        try {
            view.calculate();
            BusinessCaseView bcv = fetchBusinessCaseView(session);
            if (bcv != null && bcv.isSalesContext() && bcv.isFlowControlMode()) {
                if (view.getBusinessCase().getPrimaryKey().equals(
                        bcv.getBusinessCase().getPrimaryKey())) {
                    bcv.reload();
                }
                this.removeCalculationView(session);
                forward = mapping.findForward(Actions.ORDER);
            } else {
                forward = exit(mapping, form, request, response);
            }
        } catch (ClientException cx) {
            view.setJumpTo(null);
            saveError(request, cx.getMessage());
            if (cx.getErrorId() != null) {
                SessionUtil.saveErrorId(session, cx.getErrorId());
            }
            forward = mapping.findForward(Actions.EDIT);
        }
        if (log.isDebugEnabled()) {
            log.debug("close() done [forward=" + forward.getPath() + "]");
        }
        this.redirectAjax(request, response, forward);
        return null;
    }

    /**
     * Toggles calculation sales price lock
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleSalesPriceLock(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleSalesPriceLock() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        view.toggleSalesPriceLock();
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Forwards to option edit
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardOptions(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardOptions() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        view.getCalculator().setOptionMode(true);
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Initially creates empty options if no options exist
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createOptions(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createOptions() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        try {
            view.getCalculator().createOptions();
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Exists option editing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitOptions(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitOptions() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        view.getCalculator().setOptionMode(false);
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Exits calculation edit actions
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        if (view.isCalculatorAvailable() && view.getCalculator().isChanged()) {
            saveError(request, ErrorCode.CALCULATION_CHANGED);
            view.setJumpTo(null);
            return mapping.findForward(Actions.EDIT);
        }
        if (view.isCalculatorAvailable()
                && view.getCalculator().getCalculation().isLocked()) {
            view.getCalculator().disableLock();
            if (log.isDebugEnabled()) {
                log.debug("exit() lock disabled");
            }
        }
        view.refresh();
        try {
            BusinessCaseView bv = getBusinessCaseView(session);
            bv.reload();
        } catch (Throwable t) {
            // we ignore this
        }
        return mapping.findForward(Actions.EXIT);
    }

    /**
     * Reloads editor display with jump to last requested position id.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward reload(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("reload() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        TargetActionMapping m = (TargetActionMapping) mapping;
        if (view.jumpToExists()) {
            return new ActionForward(view.createJumpTo(m.getTarget()), true);
        }
        return new ActionForward(m.getTarget(), true);
    }

    /**
     * Destroys the calculation view and forwards to index if current calculation is not blocked
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitHome(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("execute() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        if (view.getCalculator().isChanged()) {
            saveError(request, ErrorCode.CALCULATION_CHANGED);
            return mapping.findForward(Actions.EDIT);
        }
        this.removeCalculationView(session);
        return mapping.findForward(Actions.INDEX);
    }
}
