/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 02-Feb-2007 01:48:25 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.records.Payments;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.PaymentManager;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPaymentManager extends AbstractService implements PaymentManager {
    private static Logger log = LoggerFactory.getLogger(AbstractPaymentManager.class.getName());

    private Payments payments = null;

    protected AbstractPaymentManager(Payments payments) {
        super();
        this.payments = payments;
    }

    public List<Payment> getByBankAccount(BankAccount bankAccount) {
        List<Payment> result = ((bankAccount == null || bankAccount.getId() == null) ? 
                new ArrayList<>() :
                    payments.getByBankAccount(bankAccount.getId()));
        if (log.isDebugEnabled()) {
            log.debug("getByBankAccount() done [bankAccount=" 
                    + (bankAccount == null ? "null" : bankAccount.getId()) 
                    + ", count=" + result.size() + "]");
        }
        return result;
    }

    public List<Payment> getByContact(ClassifiedContact contact) {
        return payments.getByContact(contact.getId());
    }

    public List<Payment> getByOrder(Order order) {
        return payments.getByOrder(order);
    }

    public List<Payment> getByRecord(PaymentAwareRecord record) {
        return payments.getByRecord(record);
    }

    public void delete(Payment payment) {
        payments.delete(payment);
    }
    
    public void subtract(Payment payment, BigDecimal cashDiscount) {
        payments.subtract(payment, cashDiscount);
    }

    public Payment getPayment(Long id) {
        return payments.getPayment(id);
    }
    
    protected Payments getPayments() {
        return payments;
    }

    protected void setPayments(Payments payments) {
        this.payments = payments;
    }
}
