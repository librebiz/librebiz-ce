<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="sleepingProjectsView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="headline"><o:listSize value="${view.list}"/> <fmt:message key="sleepingOrders"/></tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area" id="sleepingProjectsContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <div class="panel-body">
                        <div class="panel-data center">
                            <v:form name="daysForm" url="${view.baseLink}/reload" style="display: inline;">
                                <fmt:message key="longerThan"/>
                                <v:text name="daysCount" value="${view.daysCount}" onchange="document.daysForm.submit();" style="width: 40px; text-align: right;" />
                                <fmt:message key="sleepingOrdersDaysCountHint"/>
                            </v:form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 panel-area">

                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><v:sortLink key="id"><fmt:message key="number"/></v:sortLink></th>
                                    <th><v:sortLink key="name"><fmt:message key="consignment"/></v:sortLink></th>
                                    <th class="right"><v:sortLink key="capacity"><fmt:message key="value"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="branchKey"><fmt:message key="branchOfficeShortcut"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="salesKey"><fmt:message key="salesShortcut"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="managerKey"><fmt:message key="projectorShort"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="lastActionDate">FCS</v:sortLink></th>
                                    <th class="center"><v:sortLink key="status"><fmt:message key="status"/></v:sortLink></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:if test="${empty view.list}">
                                    <td colspan="8"><fmt:message key="noOrdersFound"/></td>
                                </c:if>
                                <c:forEach var="project" items="${view.list}" varStatus="s">
                                    <tr id="sales${project.id}">
                                        <td>
                                            <a href="<c:url value="/loadSales.do?exit=sleepingProjects&id=${project.id}&exitId=sales${project.id}"/>"><o:out value="${project.id}"/></a>
                                        </td>
                                        <td>
                                            <a href="<c:url value="/loadSales.do?exit=sleepingProjects&id=${project.id}&exitId=sales${project.id}"/>"><o:out value="${project.name}" limit="50"/></a>
                                        </td>
                                        <td class="right">
                                            <o:number value="${project.capacity}" format="currency"/>
                                        </td>
                                        <td class="center"><o:out value="${project.branchKey}"/></td>
                                        <td class="center">
                                            <c:choose>
                                                <c:when test="${!empty project.salesId && project.salesId > 0}">
                                                    <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${project.salesId}"><oc:employee initials="true" value="${project.salesId}" /></v:ajaxLink>
                                                </c:when>
                                                <c:otherwise>
                                                    &nbsp;
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td class="center">
                                            <c:choose>
                                                <c:when test="${!empty project.managerId and project.managerId > 0}">
                                                    <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${project.managerId}"><oc:employee initials="true" value="${project.managerId}" /></v:ajaxLink>
                                                </c:when>
                                                <c:otherwise>
                                                    &nbsp;
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td class="center"><o:date value="${project.lastActionDate}" addcentury="false"/></td>
                                        <td class="right">
                                            <v:ajaxLink url="/sales/flowControlDisplay/forward?id=${project.id}" styleClass="boldtext" linkId="flowControlDisplayView">
                                                <o:out value="${project.status}"/>%
                                            </v:ajaxLink>
                                            <o:permission role="notes_sales_deny" info="permissionNotes">
                                                <v:ajaxLink url="/sales/businessCaseDisplay/forward?notes=true&id=${project.id}" linkId="businessCaseDisplayView">
                                                    <o:img name="texteditIcon"/>
                                                </v:ajaxLink>
                                            </o:permission>
                                            <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${project.id}" linkId="businessCaseDisplayView">
                                                <o:img name="openFolderIcon"/>
                                            </v:ajaxLink>
                                            <c:choose>
                                                <c:when test="${!project.released}">
                                                    <span title="<fmt:message key="orderNotReleased"/>"><o:img name="importantDisabledIcon"/></span>
                                                </c:when>
                                                <c:when test="${project.deliveryClosed}">
                                                    <span title="<fmt:message key="materialPlanningFinished"/>"><o:img name="importantDisabledIcon"/></span>
                                                </c:when>
                                                <c:otherwise>
                                                    <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${project.id}" linkId="deliveryStatus" title="materialPlanning">
                                                        <o:img name="importantIcon"/>
                                                    </v:ajaxLink>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
