/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 9, 2009 1:40:50 PM 
 * 
 */
package com.osserp.gui.client.calc.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigManager;
import com.osserp.core.calc.CalculationType;
import com.osserp.gui.client.calc.CalculationTypeView;
import com.osserp.gui.client.impl.AbstractView;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
@ViewName("calculationTypeView")
public class CalculationTypeViewImpl extends AbstractView implements CalculationTypeView {

    private List<CalculationConfig> configs = new ArrayList<CalculationConfig>();

    /**
     * Initializes view name, actionUrl and selectionUrl as annotated and initializes action target as 'success'
     */
    protected CalculationTypeViewImpl() {
        super();
    }

    public List<CalculationConfig> getConfigs() {
        return configs;
    }

    /**
     * Loads calculation types and assigns local server url property.
     * @param request
     * @throws ClientException if a validation of request or session params/attributes failed
     * @throws PermissionException if current user has no permission to initialize the view
     */
    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        CalculationConfigManager configManager = (CalculationConfigManager) getService(CalculationConfigManager.class.getName());
        setList(configManager.getTypes());
        String selectionTarget = request.getParameter("selectionTarget");
        if (selectionTarget != null) {
            setSelectionTarget(selectionTarget);
        }
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        CalculationConfigManager configManager = (CalculationConfigManager) getService(CalculationConfigManager.class.getName());
        String name = form.getString(Form.NAME);
        String description = form.getString(Form.DESCRIPTION);

        if (isCreateMode()) {
            CalculationType type = configManager.createType(name, description);
            setBean(type);
            if (getList() != null) {
                getList().add(type);
            }
            disableCreateMode();
        } else {
            CalculationType type = (CalculationType) getBean();
            if (type != null) {
                setBean(configManager.updateType(type, name, description));
                disableEditMode();
            }
        }
    }

    @Override
    public void setSelection(Long id) {
        super.setSelection(id);
        CalculationType type = (CalculationType) getBean();
        if (type == null) {
            this.configs.clear();
        } else {
            CalculationConfigManager configManager = (CalculationConfigManager) getService(CalculationConfigManager.class.getName());
            this.configs = configManager.getConfigs(type.getId());
        }
    }
}
