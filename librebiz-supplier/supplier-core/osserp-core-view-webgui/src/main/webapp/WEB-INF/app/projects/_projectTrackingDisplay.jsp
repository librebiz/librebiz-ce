<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="settings" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="description"><fmt:message key="description" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.name}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="type"><fmt:message key="type" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.type.name}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="startDate"><fmt:message key="periodStart" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:date value="${view.bean.startDate}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="endDate"><fmt:message key="periodEnd" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:date value="${view.bean.endDate}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="accomplishedHours"><fmt:message key="summaryAccomplishedLabel" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <span><o:number value="${view.bean.totalTime}" format="currency" /> <fmt:message key="hours"/></span>
                    </div>
                </div>
            </div>

            <c:if test="${!view.bean.closed}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="endDate"><fmt:message key="booking" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:link url="/projects/projectTracking/enableTextEditMode">
                                <fmt:message key="new" />
                            </v:link>
                        </div>
                    </div>
                </div>
            </c:if>

        </div>
    </div>
</div>
