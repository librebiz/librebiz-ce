<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="recordTypeConfigView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="styles" type="string">
        <style type="text/css">
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}"/>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="recordTypeConfigContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.numberConfigMode}">
                        <v:form id="recordTypeForm" url="${view.baseLink}/changeNumberConfig">
                            <c:import url="${viewdir}/admin/records/_recordTypeNumberConfig.jsp" />
                        </v:form>
                    </c:when>
                    <c:when test="${view.editMode}">
                        <v:form id="recordTypeForm" url="${view.baseLink}/save">
                            <c:import url="${viewdir}/admin/records/_recordTypeEdit.jsp" />
                        </v:form>
                    </c:when>
                    <c:when test="${!empty view.bean}">
                        <c:import url="${viewdir}/admin/records/_recordTypeDisplay.jsp" />
                    </c:when>
                    <c:otherwise>
                        <c:import url="${viewdir}/admin/records/_recordTypeList.jsp" />
                    </c:otherwise>
                </c:choose>
            
            </div>
        </div>
    </tiles:put>
</tiles:insert>
