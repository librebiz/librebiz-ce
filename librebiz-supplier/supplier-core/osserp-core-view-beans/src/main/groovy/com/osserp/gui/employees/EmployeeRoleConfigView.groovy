/**
 *
 * Copyright (C) 2008, 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 29, 2008 
 * 
 */
package com.osserp.gui.employees

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.util.CollectionUtil
import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeGroup
import com.osserp.core.employees.EmployeeGroupManager
import com.osserp.core.employees.EmployeeManager
import com.osserp.core.employees.EmployeeRoleConfig
import com.osserp.core.employees.EmployeeStatus
import com.osserp.core.system.BranchOffice
import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class EmployeeRoleConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(EmployeeRoleConfigView.class.getName())
    
    Employee employee
    EmployeeRoleConfig selectedConfig
    List<EmployeeGroup> availableGroups = []
    List<EmployeeStatus> availableStatus = []

    EmployeeRoleConfigView() {
        super()
        dependencies = ['contactView']
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        
        if (forwardRequest) {
            if (env.contactView) {
                employee = env.contactView.employee
            } else {
                Long id = form.getLong('id')
                if (id) {
                    employee = employeeManager.find(id)
                }
            }
            availableStatus = employeeManager.status
            availableGroups = employeeGroupManager.groups
            logger.debug("initRequest: done [employee=${employee?.id}, availableStatus=${availableStatus.size()}, availableGroups=${availableGroups.size()}]")
        }
    }
    
    
    List<BranchOffice> getAvailableBranchs() {
        List<BranchOffice> result = []
        List<BranchOffice> available = allBranchs
        available.each {
            if (!employee.isFromBranch(it.id)) {
                result << it
            }
        }
        logger.debug("getAvailableBranchs: done [count=${result.size()}]")
        return result
    }
    
    void createConfig() throws ClientException {
        Employee currentUser = domainEmployee
        List<BranchOffice> available = allBranchs
        BranchOffice selected
        Long branchId = form.getLong('branch')
        if (branchId) {
            selected = CollectionUtil.getById(available, branchId)
        }
        if (selected) {
            selectedConfig = null
            employee = employeeManager.addRoleConfig(
                currentUser, 
                employee, 
                selected, 
                form.getString('description'))
            reload(employee, false)
            disableCreateMode()
        }
    }

    void selectConfig() {
        Long id = form.getLong('id')
        setSelectedConfig(id)
    }

    void removeConfig() {
        Long id = form.getLong('id')
        if (id) {
            selectedConfig = null
            employeeManager.removeRoleConfig(domainUser, employee, id)
            reload(employee, false)
            boolean defaultConfigPresent = false
            for (Iterator<EmployeeRoleConfig> i = employee.getRoleConfigs().iterator(); i.hasNext();) {
                EmployeeRoleConfig next = i.next()
                if (next.isDefaultRole()) {
                    defaultConfigPresent = true
                }
            }
            if (employee.getRoleConfigs().size() > 0 && !defaultConfigPresent) {
                EmployeeRoleConfig cfg = employee.getRoleConfigs().get(0)
                setAsDefaultConfig(cfg.getId())
            }
        }
    }

    void setDefaultConfig() {
        Long id = form.getLong('id')
        setAsDefaultConfig(id)
    }

    void createRole() {
        employeeManager.addRole(
                domainUser,
                employee,
                selectedConfig,
                CollectionUtil.getById(availableGroups, form.getLong('group')),
                CollectionUtil.getById(availableStatus, form.getLong('status')))
        reload(employee, false)
        disableCreateMode()
    }

    void setDefaultRole() {
        Long id = form.getLong('id')
        if (id && selectedConfig) {
            employee.setDefaultRole(selectedConfig, domainEmployee, id)
            reload(employee, true)
        }
    }

    void removeRole() {
        Long id = form.getLong('id')
        if (id && selectedConfig) {
            employee = employeeManager.removeRole(domainUser, employee, selectedConfig, id)
            reload(employee, false)
        }
    }

    void synchronizeLdap() {
        employeeManager.synchronizeLdapUser(employee)
    }

    private void reload(Employee employee, boolean persist) {
        if (persist) {
            employeeManager.save(employee)
        }
        Employee updated = employeeManager.find(employee.getId())
        employee = updated
        if (selectedConfig) {
            setSelectedConfig(selectedConfig.getId())
        }
        if (env.contactView) {
            env.contactView.refresh()
        }
        if (portalView) {
            portalView.reload()
        }
    }

    private void setSelectedConfig(Long id) {
        logger.debug("setSelectedConfig: invoked [id=${id}]")
        if (!id) {
            selectedConfig = null
            logger.debug('setSelectedConfig: reset selection done')
            
        } else {
            selectedConfig = CollectionUtil.getById(employee.roleConfigs, id)
            logger.debug("setSelectedConfig: done [id=${selectedConfig?.id}, roleCount=${selectedConfig?.roles.size()}]")
        }
    }
    
    void setAsDefaultConfig(Long id) {
        for (Iterator<EmployeeRoleConfig> i = employee.getRoleConfigs().iterator(); i.hasNext();) {
            EmployeeRoleConfig next = i.next()
            if (next.getId().equals(id)) {
                next.setDefaultRole(true)
            } else {
                next.setDefaultRole(false)
            }
        }
        reload(employee, true)
    }

    private EmployeeManager getEmployeeManager() {
        getService(EmployeeManager.class.getName())
    }

    private EmployeeGroupManager getEmployeeGroupManager() {
        getService(EmployeeGroupManager.class.getName())
    }

    private List<BranchOffice> getAllBranchs() {
        systemConfigManager.branchs
    }
}
