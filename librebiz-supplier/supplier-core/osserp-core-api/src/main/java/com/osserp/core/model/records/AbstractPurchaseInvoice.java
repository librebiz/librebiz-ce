/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2009 1:39:18 PM 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.Option;

import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordType;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceType;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPurchaseInvoice extends AbstractInvoice implements PurchaseInvoice {

    private boolean stockAffecting = false;
    private boolean initialBooking = false;
    private String supplierReferenceNumber = null;
    private Date supplierReferenceDate = null;

    /**
     * Serializable constructor
     */
    protected AbstractPurchaseInvoice() {
        super();
    }

    /**
     * Creates a new purchase invoice by order
     * @param id
     * @param type
     * @param bookingType
     * @param order
     * @param createdBy
     */
    protected AbstractPurchaseInvoice(
            Long id,
            RecordType type,
            PurchaseInvoiceType bookingType,
            Order order,
            Employee createdBy) {
        super(id, type, order, createdBy);
        setBookingType(bookingType);
        stockAffecting = bookingType.isStockAffecting();
    }

    /**
     * Default constructor for order independent purchase invoices
     * @param id
     * @param type
     * @param bookingType
     * @param company
     * @param branchId
     * @param reference
     * @param contact
     * @param createdBy
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected AbstractPurchaseInvoice(
            Long id,
            RecordType type,
            PurchaseInvoiceType bookingType,
            Long company,
            Long branchId,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                itemsChangeable,
                itemsEditable);
        setBookingType(bookingType);
        stockAffecting = bookingType.isStockAffecting();
    }
    
    /**
     * Creates a new invoice by existing invoice
     * @param id
     * @param existing
     * @param createdBy
     * @param recordDate
     * @param invoiceDate
     * @param invoiceNumber
     */
    public AbstractPurchaseInvoice(
            Long id,
            PurchaseInvoice existing,
            Employee createdBy,
            Date recordDate,
            Date invoiceDate,
            String invoiceNumber) {
        this(
                id,
                existing.getType(),
                (PurchaseInvoiceType) existing.getBookingType(),
                existing.getCompany(),
                existing.getBranchId(),
                existing.getReference(),
                existing.getContact(),
                createdBy,
                existing.getBusinessCaseId(),
                existing.getAmounts().getTaxRate(),
                existing.getAmounts().getReducedTaxRate(),
                true,
                true);
        
        if (recordDate != null) {
            updateCreatedDate(recordDate);
        }
        
        setSupplierReferenceDate(invoiceDate);
        setSupplierReferenceNumber(invoiceNumber);
        if (invoiceDate != null) {
            setTaxPoint(invoiceDate);
        } else {
            setTaxPoint(getCreated());
        }
        createItems(existing.getItems(), true);
        
        if (((Supplier) getContact()).getPaymentAgreement() != null) {
            setTaxFree(((Supplier) getContact()).getPaymentAgreement().isTaxFree());
            if (isTaxFree()) {
                setTaxFreeId(((Supplier) getContact()).getPaymentAgreement().getTaxFreeId());
                calculateSummary();
            }
        }
    }
    
    /**
     * Creates a new purchase creditNote by invoice
     * @param id
     * @param bookingType
     * @param invoice 
     * @param createdBy
     */
    protected AbstractPurchaseInvoice(
            Long id,
            PurchaseInvoiceType bookingType,
            PurchaseInvoice invoice,
            Employee createdBy) {
        super(
                id,
                invoice.getCompany(),
                invoice.getBranchId(),
                invoice.getType(),
                invoice.getId(),
                invoice.getContact(),
                createdBy,
                invoice.getBusinessCaseId(),
                invoice.getAmounts().getTaxRate(),
                invoice.getAmounts().getReducedTaxRate(),
                true,
                true);
        setTaxPoint(new Date(System.currentTimeMillis()));
        setTaxFree(invoice.isTaxFree());
        setCurrency(invoice.getCurrency());
        setBookingType(bookingType);
        stockAffecting = bookingType.isStockAffecting();
        for (int i = 0, j = invoice.getItems().size(); i < j; i++) {
            Item next = invoice.getItems().get(i);
            addItem(
                    next.getStockId(),
                    next.getProduct(),
                    next.getCustomName(),
                    (next.getQuantity().doubleValue() * (-1)),
                    next.getTaxRate(),
                    next.getPrice(),
                    next.getPriceDate(),
                    next.getPartnerPrice(),
                    next.isPartnerPriceEditable(),
                    next.isPartnerPriceOverridden(),
                    next.getPurchasePrice(),
                    next.getNote(),
                    next.isIncludePrice(),
                    next.getExternalId());
        }
        calculateSummary();
    }

    public Date getSupplierReferenceDate() {
        return supplierReferenceDate;
    }

    public void setSupplierReferenceDate(Date supplierReferenceDate) {
        this.supplierReferenceDate = supplierReferenceDate;
    }

    public String getSupplierReferenceNumber() {
        return supplierReferenceNumber;
    }

    public void setSupplierReferenceNumber(String supplierReferenceNumber) {
        this.supplierReferenceNumber = supplierReferenceNumber;
    }

    public boolean isInitialBooking() {
        return initialBooking;
    }

    protected void setInitialBooking(boolean initialBooking) {
        this.initialBooking = initialBooking;
    }

    public boolean isStockAffecting() {
        return stockAffecting;
    }

    public void setStockAffecting(boolean stockAffecting) {
        this.stockAffecting = stockAffecting;
    }

    public boolean isService() {
        return getBookingType() == null ? false : PurchaseInvoiceType.SERVICE.equals(getBookingType().getId());
    }

    public boolean isCreditNote() {
        return getBookingType() == null ? false : PurchaseInvoiceType.CREDIT_NOTE.equals(getBookingType().getId());
    }

    @Override
    public void addItems(List<Item> items) {
        super.addItems(items);
    }

    @Override
    public void setItemsChangeable(boolean itemsChangeable) {
        super.setItemsChangeable(itemsChangeable);
    }

    @Override
    public void setItemsEditable(boolean itemsEditable) {
        super.setItemsEditable(itemsEditable);
    }

    @Override
    public void setStatus(Long status) {
        super.setStatus(status);
    }

    @Override
    public boolean isSales() {
        return false;
    }

    @Override
    public boolean isItemsDeletable() {
        if (initialBooking) {
            return false;
        }
        return (getStatus() == null
                ? super.isItemsDeletable()
                : PurchaseInvoice.STAT_CHANGED.equals(getStatus()));
    }

    @Override
    protected Payment createCustomPayment(Employee user, BillingType type, BigDecimal amount, Date paid, String customHeader, String note, Long bankAccountId) {
        throw new IllegalStateException();
    }

    @Override
    protected Payment createPayment(
            Employee user,
            BillingType type,
            BigDecimal amount,
            Date paid,
            Long bankAccountId) {
        return new PurchasePaymentImpl(
                this,
                type,
                paid,
                user,
                amount,
                bankAccountId);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        throw new UnsupportedOperationException("purchase invoice supports no infos");
    }

}
