<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.id}" />
                -
                <o:out value="${view.bean.name}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <c:if test="${!empty view.selectedCompany && view.multipleClientsAvailable}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="company" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.selectedCompany.name}"/>
                    </div>
                </div>
            </div>
        </c:if>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name"><fmt:message key="name" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text styleClass="form-control" name="name" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="description"><fmt:message key="description" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text styleClass="form-control" name="description" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="group">
                        <v:link url="/admin/system/optionsAdmin/forward?name=campaignGroups&exit=/admin/crm/campaignConfig/reload">
                            <fmt:message key="group" />
                        </v:link>
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <select name="group" class="form-control">
                        <option value="0"><fmt:message key="noSelection" /></option>
                        <c:forEach var="obj" items="${view.groups}">
                            <option value="${obj.id}" <c:if test="${view.bean.group.id == obj.id}">selected="selected"</c:if>><o:out value="${obj.name}" /></option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="type">
                        <v:link url="/admin/system/optionsAdmin/forward?name=campaignTypes&exit=/admin/crm/campaignConfig/reload">
                            <fmt:message key="type" />
                        </v:link>
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <select name="type" class="form-control">
                        <option value="0"><fmt:message key="noSelection" /></option>
                        <c:forEach var="obj" items="${view.types}">
                            <option value="${obj.id}" <c:if test="${view.bean.type.id == obj.id}">selected="selected"</c:if>><o:out value="${obj.name}" /></option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>

        <c:if test="${view.reachListAvailable}">
            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="reach"><fmt:message key="reachLabel" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <select name="reach" class="form-control">
                            <c:forEach var="obj" items="${view.reachList}">
                                <option value="${obj}" <c:if test="${view.bean.reach == obj}">selected="selected"</c:if>><fmt:message key="${obj}" /></option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        
            <c:if test="${!empty view.branchOfficeList}">
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="branch"><fmt:message key="branch" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="branch" class="form-control">
                                <option value="-1"><fmt:message key="noSelection" /></option>
                                <c:forEach var="obj" items="${view.branchOfficeList}">
                                    <option value="${obj.id}" <c:if test="${view.bean.branch != null && view.bean.branch == obj.id}">selected="selected"</c:if>><o:out value="${obj.name}" /></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </c:if>
        </c:if>

        <v:date name="campaignStarts" styleClass="form-control" label="campaignStart" picker="true" />

        <v:date name="campaignEnds" styleClass="form-control" label="campaignEnd" picker="true" />

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="eol"><fmt:message key="status" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="checkbox">
                        <label for="headquarter"> <v:checkbox id="endOfLife" /> <fmt:message key="deactivate" />
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4"> </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <v:submitExit url="/admin/crm/campaignConfig/disableEditMode" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <o:submit styleClass="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
