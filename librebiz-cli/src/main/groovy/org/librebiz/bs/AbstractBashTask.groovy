/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 5, 2017 
 * 
 */
package org.librebiz.bs

import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.attribute.GroupPrincipal
import java.nio.file.attribute.PosixFileAttributeView
import java.nio.file.attribute.PosixFileAttributes
import java.nio.file.attribute.PosixFilePermission
import java.nio.file.attribute.PosixFilePermissions
import java.nio.file.attribute.UserPrincipal

import com.osserp.common.service.UnixShell

/**
 * 
 * Base class for script tasks using bash
 * 
 * 
 * @author Rainer Kirchner <rk@librebiz.org>
 * 
 */
class AbstractBashTask {
    
    boolean verbose = false

    AbstractBashTask(boolean verbose) {
        this.verbose = verbose
    } 
    
    boolean runCommand(String command) {
        int exitval = exec(command)
        if (exitval == 0) {
            return true
        }
        return false
    }

    int exec(String command) {
        UnixShell.execute(command, verbose)
    }

    /**
     * Provides the owner and group of a file or directory
     * @param file
     * @return owner as array of [ UserPrincipal, GroupPrincipal ]
     *  or null if file not exists or any exception occured.
     */
    UserPrincipal getOwner(String file) {
        try {
            Path path = Paths.get(file)
            PosixFileAttributes posix = Files.readAttributes(
                    path, PosixFileAttributes.class)
            return posix.owner()
        } catch (Exception e) {
            return null
        }
    }
    
    /**
     * Provides the owner and group of a file or directory
     * @param file
     * @return owner as array of [ UserPrincipal, GroupPrincipal ]
     *  or null if file not exists or any exception occured.
     */
    GroupPrincipal getGroup(String file) {
        try {
            Path path = Paths.get(file)
            PosixFileAttributes posix = Files.readAttributes(
                    path, PosixFileAttributes.class)
            return posix.group()
        } catch (Exception e) {
            return null
        }
    }

    /**
     * Sets the owner of a file or directory    
     * @param file
     * @param owner
     * @return true if not failed
     */
    boolean setOwner(String file, UserPrincipal owner) {
        try {
            Files.setOwner(Paths.get(file), owner);
            return true
        } catch (Exception e) {
            return false
        }
    }
    
    /**
     * Sets the group of a file or directory    
     * @param file
     * @param owner
     * @return true if not failed
     */
    boolean setGroup(String file, GroupPrincipal group) {
        try {
            Files.getFileAttributeView(
                    new File(file).toPath(),
                    PosixFileAttributeView.class,
                    LinkOption.NOFOLLOW_LINKS)
                .setGroup(group)
            return true
        } catch (Exception e) {
            return false
        }
    }

    /**
     * Sets posix permission for a file or directory
     * @param file
     * @param posixPerms
     * @return true if not failed
     */
    boolean setPermissions(String file, String posixPerms) {
        try {
            Path p = Paths.get(file)
            Set<PosixFilePermission> perms =
                    PosixFilePermissions.fromString(posixPerms)
            Files.setPosixFilePermissions(p, perms)
            return true
        } catch (Exception e) {
            return false
        }
    }
    
    /**
     * Checks file or directory exists and has expected type 
     * @param name file or directory
     * @param dir true if directory expected, file expected if false
     * @param write true if file or directory must writable
     * @return true if all ok
     */
    boolean checkFile(String name, boolean dir, boolean write) {
        assert name != null
        File file = new File(name)
        if (file.exists() && file.canRead()) {
            if ((dir && file.isDirectory()) || (!dir && !file.isDirectory())) {
                if (!write || file.canWrite()) {
                    return true
                }
            }
        }
        return false
    }

}
