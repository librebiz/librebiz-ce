/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 17.09.2004 
 * 
 */
package com.osserp.common;

import java.util.List;
import java.util.Map;

/**
 * Caches available option lists
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface OptionsCache {

    /**
     * Provides an options list by key
     * @param optionKey
     * @return list
     */
    List<Option> getList(String optionKey);

    /**
     * Refreshs the options specified by key
     * @param optionKey
     */
    void refresh(String optionKey);

    /**
     * Refreshs all current activated caches. Use with care.
     */
    void refreshAll();

    /**
     * Provides an options map by key
     * @param optionKey
     * @return map
     */
    Map<Long, Option> getMap(String optionKey);

    /**
     * Provides a cached object
     * @param optionKey of the option map
     * @param id of the mapped option
     * @return option or null if not exist
     */
    Option getMapped(String optionKey, Long id);

    /**
     * Provides the value of a cache object
     * @param optionKey of the option map
     * @param id of the mapped option
     * @return value or empty string if not exist
     */
    String getMappedValue(String optionKey, Long id);

    /**
     * Adds the given value to the option type list specified by given key. Note: This method automatically refreshs the options cache after successfull
     * execution so an explicit call to refresh is not required.
     * @param optionKey where we add the new value
     * @param value to add
     * @return new created option
     * @throws ClientException if value is null or already existing
     */
    Option add(String optionKey, String value) throws ClientException;

    /**
     * Renames the value under the given id of the option type specified by given option key
     * @param optionKey where we rename the value
     * @param id of the value to rename
     * @param newValue to add
     * @throws ClientException if value is null or already existing under another id
     */
    void rename(String optionKey, Long id, String newValue) throws ClientException;

    /**
     * Toggles eol flag of an option
     * @param optionKey of options list
     * @param id of the option to toggle
     */
    void toogleEol(String optionKey, Long id);
}
