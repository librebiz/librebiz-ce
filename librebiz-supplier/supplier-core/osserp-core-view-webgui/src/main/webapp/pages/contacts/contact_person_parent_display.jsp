<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="personView" value="${sessionScope.contactPersonView}" />
<c:set var="contact" value="${sessionScope.contactPersonView.related}" />

<c:if test="${!empty personView.related}">
    <div class="subcolumns">
        <div class="column50l">
            <div class="subcolumnl">
                <div class="spacer"></div>
                <div class="contentBox">
                    <div class="contentBoxHeader">
                        <div class="contentBoxHeaderLeft">
                            <div class="subHeaderLeft"> </div>
                            <div class="subHeaderRight">
                                <span style="margin-right: 8px;"><fmt:message key="contactOf" /></span>
                               	<a href="<c:url value="/contacts.do?method=load&id=${contact.contactId}&exit=${personView.exitTarget}"/>">
                               		<o:out value="${contact.displayName}" />
                               	</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>

                <div class="contentBox">
                    <div class="contentBoxData">
                        <div class="subcolumns">
                            <div class="subcolumn">
                                <div class="smallSpacer"></div>
                                <table class="valueTable first33">
                                    <tbody>
                                        <tr>
                                            <td><fmt:message key="address" /></td>
                                            <td><o:out value="${contact.address.street}" /><br /> <o:out value="${contact.address.zipcode}" /> <o:out value="${contact.address.city}" /></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="country" /></td>
                                            <td><oc:options name="countryNames" value="${contact.address.country}" /></td>
                                        </tr>
                                        <%-- 
                                        <c:if test="${!contactView.customFederalState and contact.address.federalStateId != 0 and !empty contact.address.federalStateId}">
                                            <tr>
                                                <td><fmt:message key="federalState" /></td>
                                                <td><oc:options name="federalStates" value="${contact.address.federalStateId}" /></td>
                                            </tr>
                                        </c:if>
                                        --%>
                                        <c:if test="${!empty contact.website}">
                                            <tr>
                                                <td><fmt:message key="website" /></td>
                                                <td><a href="<o:out value="${'http://'}${contact.website}"/>" target="_blank"><o:out value="${contact.website}" /></a></td>
                                            </tr>
                                        </c:if>
                                        <tr>
                                            <td><fmt:message key="newsletter" /></td>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${contact.newsletterConfirmation}">
                                                        <fmt:message key="bigYes" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:message key="bigNo" />
                                                    </c:otherwise>
                                                </c:choose> 
                                                <span style="margin-left: 20px;"><fmt:message key="vip" /></span> 
                                                <c:choose>
                                                    <c:when test="${contact.vip}">
                                                        <span style="margin-left: 20px;"><fmt:message key="bigYes" /></span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span style="margin-left: 20px;"><fmt:message key="bigNo" /></span>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="smallSpacer"></div>
                    </div>
                </div>
                <div class="spacer"></div>
            </div>
        </div>

        <div class="column50r">
            <div class="subcolumnr">
                <div class="spacer"></div>
                <c:if test="${!contactView.assignMode and !contactView.editMode}">
                    <c:set var="communicationView" scope="request" value="${contactView}" />
                    <c:set var="communicationViewContact" scope="request" value="${contact}" />
                    <c:set var="communicationViewReadonly" scope="request" value="true" />
                    <c:import url="/pages/contacts/shared/contact_phones.jsp" />
                    <div class="spacer"></div>
                    <c:import url="/pages/contacts/shared/contact_fax.jsp" />
                    <div class="spacer"></div>
                    <c:import url="/pages/contacts/shared/contact_mobiles.jsp" />
                    <div class="spacer"></div>
                    <c:import url="/pages/contacts/shared/contact_emails.jsp" />
                </c:if>
            </div>
        </div>
    </div>
    <div class="spacer"></div>
</c:if>
