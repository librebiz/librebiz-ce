/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 25, 2005 
 * 
 */
package com.osserp.common.web;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.owasp.csrfguard.CsrfGuard;

import com.osserp.common.ActionException;
import com.osserp.common.Constants;
import com.osserp.common.Month;
import com.osserp.common.User;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestUtil {
    private static Logger log = LoggerFactory.getLogger(RequestUtil.class.getName());

    /**
     * Tries to get a request parameter specified as '&amp;id=' and converts it to Long
     * @param request where to get the id param from
     * @return the id param as Long or null
     */
    public static Long fetchId(HttpServletRequest request) {
        return getLong(request, Actions.ID, false);
    }

    /**
     * Tries to get a request parameter specified as '&amp;id=' and converts it to Long
     * @param request where to get the id param from
     * @return the id param as Long
     */
    public static Long getId(HttpServletRequest request) {
        return getLong(request, Actions.ID);
    }

    /**
     * Tries to get a request parameter specified by key as Double
     * @param request where to get the id param from
     * @param key, the name of the param
     * @return the key param as Date
     */
    public static Double getDouble(HttpServletRequest request, String key) {
        try {
            return NumberUtil.createDouble(getParameter(request, key));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Tries to get a request parameter specified by key as Double
     * @param request where to get the id param from
     * @param key, the name of the param
     * @return the key param as Date
     */
    public static BigDecimal getDecimal(HttpServletRequest request, String key) {
        try {
            return NumberUtil.createDecimal(getParameter(request, key));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Tries to get a request parameter specified by key as Date
     * @param request where to get the id param from
     * @param key, the name of the param
     * @return the key param as Date
     */
    public static Date getDate(HttpServletRequest request, String key) {
        try {
            return DateUtil.createDate(getParameter(request, key), false);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Tries to get a request parameter specified by key as Long
     * @param request where to get the id param from
     * @param key, the name of the param
     * @return key param as Long
     */
    public static Long getLong(HttpServletRequest request, String key) {
        try {
            return Long.valueOf(getParameter(request, key));
        } catch (Exception e) {
            throw new ActionException(key);
        }
    }

    /**
     * Tries to get a request parameter specified by key as Long
     * @param request where to get the id param from
     * @param key, the name of the param
     * @return key param as Long or null if none exists
     */
    public static Long fetchLong(HttpServletRequest request, String key) {
        try {
            return getLong(request, key);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Tries to get a request attribute of type Long
     * @param request 
     * @param key the name of the attribute
     * @return attribute value or null if not exists
     */
    public static Long fetchLongAttribute(HttpServletRequest request, String key) {
        try {
            return (Long) request.getAttribute(key);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Tries to get a request parameter specified by key as Long
     * @param request where to get the id param from
     * @param key, the name of the param
     * @param required, if false, a runtime exception is thrown if no param found
     * @return key param as Long or null if not exists and required is false
     */
    public static Long getLong(HttpServletRequest request, String key, boolean required) {
        try {
            return Long.valueOf(getParameter(request, key));
        } catch (Exception e) {
            if (required) {
                throw new ActionException(key);
            }
            return null;
        }
    }

    /**
     * Tries to create a month by request params month and (optional) year <br>
     * current year is used if year not exists
     * @param request
     * @return month or null if month param null
     */
    public static Month fetchMonth(HttpServletRequest request) {
        Integer year = getInteger(request, "year");
        Integer month = getInteger(request, "month");
        if (month == null) {
            return null;
        }
        if (year == null) {
            year = DateUtil.getCurrentYear();
        }
        return DateUtil.createMonth(month, year);
    }

    /**
     * Provides a cleaned up request param. Useful if javascript adds additional params to an already parameterized request
     * @param request
     * @param name to lookup for
     * @return value
     */
    public static String getString(HttpServletRequest request, String name) {
        return getParameter(request, name);
    }

    /**
     * Tries to get a request parameter specified by key as Integer
     * @param request where to get the param from
     * @param key name to lookup for
     * @return param as Integer or null if not exists
     */
    public static Integer getInteger(HttpServletRequest request, String key) {
        try {
            return Integer.valueOf(getParameter(request, key));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Tries to get a request parameter specified by key as Integer
     * @param request where to get the param from
     * @param key, the name of the param
     * @return param as int, throws runtime exception if not exists
     */
    public static int getInt(HttpServletRequest request, String key) {
        try {
            return (Integer.valueOf(getParameter(request, key))).intValue();
        } catch (Exception e) {
            throw new ActionException(key);
        }
    }

    /**
     * Privides a request param value as boolean
     * @param request
     * @param key
     * @return true if value matches true, false in any other case including null
     */
    public static boolean getBoolean(HttpServletRequest request, String key) {
        boolean result = false;
        try {
            String value = getParameter(request, key);
            if (value != null && value.equalsIgnoreCase("on")) {
                result = true;
            } else if (value != null) {
                result = (Boolean.valueOf(value)).booleanValue();
            }
        } catch (Exception e) {
        }
        return result;
    }

    /**
     * Indicates if parameter is existing
     * @param request
     * @param key
     * @return true if so
     */
    public static boolean isAvailable(HttpServletRequest request, String key) {
        return (request.getParameter(key) != null);
    }

    /**
     * Tries to get a request parameter specified as 'target'
     * @param request where to lookup for target param
     * @return the target param or null if not exists
     */
    public static String getTarget(HttpServletRequest request) {
        return request.getParameter(Actions.TARGET);
    }

    /**
     * Provides the CSRFGuard token
     * @param request
     * @param uri
     * @return String array with name and value
     */
    public static String[] getCSRFToken(HttpServletRequest request, String uri) {
        try {
            CsrfGuard csrfGuard = CsrfGuard.getInstance();
            String tokenValue = csrfGuard.getTokenValue(request, uri);
            String tokenName = csrfGuard.getTokenName();
            return new String[] { tokenName, tokenValue };
        } catch (Exception e) {
            log.warn("Failed to fetch CSRF token [message=" + e.getMessage() + "]");
            return new String[2];
        }
    }
    
    public static String appendCSRFToken(HttpServletRequest request, String uri) {
        String[] csrfToken = getCSRFToken(request, uri);
        if (csrfToken[0] == null || csrfToken[1] == null) {
            log.warn("Did not find token [link=" + uri + "]");
            return uri;
        }
        StringBuilder buffer = (uri == null) ? new StringBuilder() : new StringBuilder(uri);
        if (uri != null && uri.indexOf("?") > -1) {
            buffer.append("&");
        } else {
            buffer.append("?");
        }
        buffer.append(csrfToken[0]).append("=").append(csrfToken[1]);
        return buffer.toString();
    }
    
    /**
     * Tries to get a request parameter specified as 'target'
     * @param request where to lookup for target param
     * @return the target param or null if not exists
     */
    public static String getTarget(HttpServletRequest request, boolean required) {
        String target = getTarget(request);
        if (required && target == null) {
            throw new ActionException("target required");
        }
        return target;
    }

    /**
     * Tries to get a request parameter specified as 'target' and stores the target in session scope. An existing session.target attribute will be removed if a
     * target could be found in request.
     * @param request to lookup for target param
     */
    public static void persistTarget(HttpServletRequest request) {
        String target = getTarget(request);
        request.getSession().setAttribute(Actions.TARGET, target);
    }

    /**
     * Saves exitId as request attribute if exitId provided.
     * @param request
     * @param exitId
     */
    public static void saveExitId(HttpServletRequest request, String exitId) {
        if (exitId != null) {
            request.setAttribute("exitId", exitId);
        }
    }

    /**
     * Tries to get a request parameter or attribute specified as 'exit'
     * @param request where to lookup for exit param
     * @return the exit param or null if not exists
     */
    public static String getExit(HttpServletRequest request) {
        String target = request.getParameter(Actions.EXIT);
        if (target == null) {
            Object tobj = request.getAttribute(Actions.EXIT);
            if (tobj != null && tobj instanceof String) {
                target = (String) tobj;
            }
        }
        return target;
    }

    /**
     * Tries to get a request parameter specified as 'exitId'
     * @param request
     * @return exitId param or null if not exists
     */
    public static String getExitId(HttpServletRequest request) {
        return request.getParameter("exitId");
    }

    /**
     * Creates a page name by request
     * @param request
     * @return pageName and relative path
     */
    public static String createPageName(HttpServletRequest request) {
        return createPageName(getServerURI(request), request.getContextPath());
    }

    /**
     * Creates a page name by server uri and context
     * @param serverUri
     * @param context
     * @return pageName and relative path
     */
    public static String createPageName(String serverUri, String context) {
        return serverUri.substring(serverUri.indexOf(context) + context.length());
    }

    /**
     * Tries to get a request parameter specified as 'url'
     * @param request where to lookup for target param
     * @return the target param or null if not exists
     */
    public static String getUrlParam(HttpServletRequest request) {
        return request.getParameter("url");
    }

    /**
     * Provides the server url with port and context path, <br>
     * e.g. 'http://servername:8080/appctx'
     * @param request
     * @return serverUrl
     */
    public static String getServerURL(HttpServletRequest request) {
        return createServerStringBuffer(request).toString();
    }

    /**
     * Render the complete server uri
     * @param request
     * @return uri
     */
    public static String getServerURI(HttpServletRequest request) {
        return createServerUriStringBuffer(request, request.getRequestURI()).toString();
    }

    /**
     * Return <code>StringBuffer</code> representing the scheme, server, and port number of the current request.
     * </p>
     * @author from jakarta struts
     * 
     * @param scheme The scheme name to use
     * @param server The server name to use
     * @param port The port value to use
     * @param uri The uri value to use
     * 
     * @return in the form scheme: server: port
     */
    private static StringBuffer createServerUriStringBuffer(HttpServletRequest request, String uri) {
        StringBuffer serverUri = createServerStringBuffer(request);
        serverUri.append(uri);
        return serverUri;
    }

    /**
     * <p>
     * Return <code>StringBuffer</code> representing the scheme, server, and port number of the current request.
     * </p>
     * @param request
     * @return in the form scheme: server: port
     */
    private static StringBuffer createServerStringBuffer(HttpServletRequest request) {
        String scheme = request.getScheme();
        String server = request.getServerName();
        int port = request.getServerPort();
        if ("https".equals(request.getHeader("X-Forwarded-Proto"))
                && "on".equals(request.getHeader("X-Forwarded-Ssl"))) {
            scheme = "https";
            port = 443;
        }
        // author from jakarta struts:
        StringBuffer url = new StringBuffer();
        if (port < 0) {
            port = 80; // Work around java.net.URL bug
        }
        url.append(scheme);
        url.append("://");
        url.append(server);
        if ((scheme.equals("http") && (port != 80)) || (scheme.equals("https") && (port != 443))) {
            url.append(':');
            url.append(port);
        }
        return url;
    }

    /**
     * Stores an error message under Globals.ERRORS. Value will be retrieved from localization context
     * @param request
     * @param errorMessageKey
     */
    public static void saveError(HttpServletRequest request, String errorMessageKey) {
        String message = getResourceString(request, errorMessageKey);
        if (message != null && message.startsWith("?")) {
            // seems that message has been already resolved
            SessionUtil.saveError(request.getSession(), errorMessageKey);
        } else {
            SessionUtil.saveError(request.getSession(), message);
        }
        request.getSession().setAttribute("error", errorMessageKey);
    }

    /**
     * Stores an error message as is under Globals.ERRORS
     * @param request
     * @param errorMessage
     */
    public static void saveNativeError(HttpServletRequest request, String errorMessage) {
        SessionUtil.saveError(request.getSession(), errorMessage);
    }

    /**
     * Returns the source remote address provided by X-Forwarded-For header
     * or request.remoteAddr if header not present
     * @param request
     * @return remote address
     */
    public static String getRemoteAddress(HttpServletRequest request) {
        String result = null;
        String[] forwardIPs = StringUtil.getTokenArray(request.getHeader("X-Forwarded-For"));
        if (forwardIPs != null) {
            InetAddressValidator validator = InetAddressValidator.getInstance();
            for (int i = 0; i < forwardIPs.length; i++) {
                String next = forwardIPs[i].trim();
                if (validator.isValid(next)) {
                    result = next;
                    break;
                }
            }
        }
        return result != null ? result : request.getRemoteAddr();
    }

    /**
     * Returns the string from the message resource bundle associated with given key
     * @param request where the resource bundle is stored
     * @param key to lookup for associated value
     * @return message resource value
     */
    public static String getResourceString(HttpServletRequest request, String key) {
        ResourceBundle bundle = getResourceBundle(request);
        String result = null;
        if (bundle != null) {
            try {
                result = bundle.getString(key);
            } catch (Throwable t) {
                if (key != null) {
                    log.warn("getResourceString() failed [key=" + key + ", message=" + t.getMessage() + "]");
                }
            }
        }
        return result == null ? key : result;
    }

    /**
     * Provides the resource bundle for current users locale or default locale of the app if view does not depend on a user. You may use
     * {@link #getResourceString(HttpServletRequest, String)} to pick up just a particular string
     * @return resource bundle or null if none exists
     */
    public static ResourceBundle getResourceBundle(HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();
            PortalView view = (PortalView) session.getAttribute(Globals.PORTAL_VIEW);
            User user = (User) session.getAttribute(Globals.USER);
            Locale locale = (user == null ? null : user.getLocale());
            if (locale == null) {
                locale = Constants.DEFAULT_LOCALE_OBJECT;
            }
            if (view != null) {
                return view.getResourceBundle(locale);
            }
            ResourceLocator resLocator = (ResourceLocator) ContextUtil.getService(
                    request, ResourceLocator.class.getName());
            if (resLocator != null) {
                return resLocator.getResourceBundle(locale);
            }
        } catch (Throwable t) {
            // Caller should handle this. It's not guaranteed that portal view provides resources 
        }
        return null;
    }
    
    /**
     * Provides the servletContext for the request
     * @param request
     * @return servletContext
     */
    public static ServletContext getServletContext(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        return session.getServletContext();
    }

    private static String getParameter(HttpServletRequest request, String key) {
        String s = request.getParameter(key);
        if (s == null)
            return null;
        int i = s.indexOf("?");
        return (i < 0) ? s : s.substring(0, i);
    }

    public static void log(HttpServletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("REQUEST DUMP - " + request.getRequestURI() +
                    " - Server '" + request.getServerName() +
                    "', port '" + request.getServerPort() + "'");
            Enumeration headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String name = (String) headerNames.nextElement();
                String value = request.getHeader(name);
                log.debug("Header [name=" + name + ", value=" + value + "]");
            }
            Enumeration paramNames = request.getParameterNames();
            while (paramNames.hasMoreElements()) {
                String name = (String) paramNames.nextElement();
                String value = request.getParameter(name);
                if (name.indexOf("assword") > -1) {
                    log.debug("Param [name=" + name + ", exists=" + (value != null) + "]");
                } else {
                    log.debug("Param [name=" + name + ", value=" + value + "]");
                }
            }
            Enumeration attributes = request.getAttributeNames();
            while (attributes.hasMoreElements()) {
                String name = (String) attributes.nextElement();
                Object value = request.getAttribute(name);
                log.debug("Attribute [name=" + name + ", value=" + value + "]");
            }

            /*
             * Cookie[] cookies = request.getCookies(); if (cookies == null) { log.debug("login() cookies were null..."); } else if (cookies.length == 0) {
             * log.debug("login() cookies were empty..."); } else { log.debug("login() cookies:\n"); for (int i = 0, j = cookies.length; i < j; i++) { Cookie
             * next = cookies[i]; StringBuffer buffer = new StringBuffer(); buffer .append(i) .append("; domain: ").append(next.getDomain())
             * .append(", path: ").append(next.getPath()) .append(", name: ").append(next.getName()) .append(", value: ").append(next.getValue())
             * .append(", maxage: ").append(next.getMaxAge()); log.debug(buffer.toString()); } }
             */
        }
    }

    public static String createSessionDump(HttpSession session) {
        StringBuilder dump = new StringBuilder(512);
        Enumeration e = session.getAttributeNames();
        while (e.hasMoreElements()) {
            String name = (String) e.nextElement();
            Object value = session.getAttribute(name);
            dump.append("\n").append("name=").append(name).append(", class=" + value.getClass().getName() + "]");
        }
        return dump.toString();
    }

    /**
     * Creates a textual dump of the request uri, params and user agent
     * @param request
     * @return requestDump
     */
    public static String createRequestDump(HttpServletRequest request) {
        StringBuilder buffer = new StringBuilder();
        buffer
                .append("\nreferer=").append(request.getHeader("referer"))
                .append("\nrequestURL=").append(request.getRequestURL())
                .append("\nrequestURI=").append(request.getRequestURI())
                .append("\n").append(createParamsDump(request))
                .append("\nremoteAddr=").append(request.getRemoteAddr());
        /*
         * buffer .append("\ncontext: ").append(request.getContextPath()) .append("\nserver name: ").append(request.getServerName())
         * .append("\nserver port: ").append(request.getServerPort()) .append("\nservlet path: ").append(request.getServletPath());
         */
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            if (key != null && key.indexOf("gent") > 0) {
                buffer.append("\nuser-agent: " + request.getHeader(key));
                break;
            }
        }
        /*
         * Cookie[] cookies = request.getCookies(); if (cookies == null) { log.debug("login() cookies were null..."); } else if (cookies.length == 0) {
         * log.debug("login() cookies were empty..."); } else { log.debug("login() cookies:\n"); for (int i = 0, j = cookies.length; i < j; i++) { Cookie next =
         * cookies[i]; StringBuffer buffer = new StringBuffer(); buffer .append(i) .append("; domain: ").append(next.getDomain())
         * .append(", path: ").append(next.getPath()) .append(", name: ").append(next.getName()) .append(", value: ").append(next.getValue())
         * .append(", maxage: ").append(next.getMaxAge()); log.debug(buffer.toString()); } }
         */
        return buffer.toString();
    }

    /**
     * Creates a dump of the request in the form 'uri=/requestURI, params=[paramA=abc, paramsB=def]'
     * @param request
     * @return requestURIDump
     */
    public static String createRequestURIDump(HttpServletRequest request) {
        StringBuilder buffer = new StringBuilder(64);
        buffer
                .append("uri=")
                .append(request.getRequestURI())
                .append(", ")
                .append(createParamsDump(request));
        return buffer.toString();
    }

    /**
     * Creates a parameter dump in the form 'params=[paramA=abc, paramsB=def]'
     * @param request
     * @return parameter dump
     */
    private static String createParamsDump(HttpServletRequest request) {
        StringBuilder buffer = new StringBuilder(64);
        buffer.append("params=[");
        boolean added = false;
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            if (added) {
                buffer.append(", ");
            }
            String paramName = paramNames.nextElement();
            String paramValue = request.getParameter(paramName);
            if (paramName.indexOf("assword") > -1) {
                paramValue = "***";
            }
            buffer.append(paramName).append("=").append(paramValue);
            added = true;
        }
        buffer.append("]");
        return buffer.toString();
    }
}
