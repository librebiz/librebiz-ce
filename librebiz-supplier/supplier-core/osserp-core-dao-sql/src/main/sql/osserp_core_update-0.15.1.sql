SET search_path = osserp, pg_catalog;

CREATE OR REPLACE VIEW osserp.v_purchase_invoices_query AS
 SELECT r.id,
    r.type_id,
    r.company_id,
    r.reference_id,
    r.contact_id,
    r.contact_type,
    r.status,
    r.created,
    r.created_by,
    NULL::bigint AS signature_left,
    NULL::bigint AS signature_right,
    r.base_tax,
    r.reduced_tax,
    r.tax_free,
    r.tax_free_id,
    r.net_amount,
    r.tax_amount,
    r.reduced_tax_amount,
    r.gross_amount,
    r.currency_id,
    r.internal,
    now() AS maturity,
    (0)::numeric AS prorate_tax,
    (0)::numeric AS net_prorated_amount,
    (0)::numeric AS tax_prorated_amount,
    (0)::numeric AS reduced_prorated_tax_amount,
    (0)::numeric AS gross_prorated_amount,
    c.lastname,
    c.firstname,
    c.street,
    c.zipcode,
    c.city,
    a.product_id,
    a.name AS product_name,
    i.name AS custom_name,
    (0)::bigint AS type_count,
    at.type_id AS product_type,
    a.group_id AS product_group,
    a.category_id AS product_category,
    a.has_stock,
    a.is_virtual,
    a.is_plant,
    a.is_bundle,
    a.is_service,
    i.id AS item_id,
    i.quantity,
    i.price,
    i.include_price,
    i.stock_id,
    i.quantity AS delivered,
    r.created::timestamp with time zone AS delivery,
    true AS delivery_ack,
    r.sales_id,
    r.book_type_id AS sub_type,
    NULL::timestamp without time zone AS sent,
    NULL::bigint AS external_item_id,
    ( SELECT sum(purchase_payments.amount) AS sum
           FROM osserp.purchase_payments
          WHERE (purchase_payments.invoice_id = r.id)) AS paid_amount
   FROM ((((osserp.purchase_invoices r
     LEFT JOIN osserp.purchase_invoice_items i ON ((r.id = i.reference_id)))
     LEFT JOIN osserp.products a ON ((i.product_id = a.product_id)))
     LEFT JOIN osserp.product_types_relations at ON ((i.product_id = at.product_id)))
     LEFT JOIN osserp.contacts c ON ((c.contact_id = ( SELECT suppliers.contact_id
           FROM osserp.suppliers
          WHERE (suppliers.id = r.contact_id)))));

UPDATE events SET action_link = (SELECT substring(action_link, 0, position('&' IN action_link))) WHERE action_link like '%&%';
UPDATE system_menu_links SET link = '/users/userCalendar/forward' WHERE link = '/userCalendar.do?method=forward';
UPDATE user_startup_targets SET target_url = '/users/userCalendar/forward' WHERE target_url = '/userCalendar.do?method=forward';
UPDATE user_startup_targets SET target_url = '/sales/salesDeliveryCalendar/forward' WHERE target_url = '/salesDeliveryCalendar.do?method=forward';

CREATE OR REPLACE VIEW osserp.v_suppliers AS
 SELECT su.id,
    su.contact_id,
    su.type_id,
    co.created,
    co.created_by,
    co.lastname,
    co.firstname,
    co.street,
    co.zipcode,
    co.city
   FROM osserp.suppliers su,
    osserp.contacts co
  WHERE (su.contact_id = co.contact_id)
  ORDER BY co.lastname;

CREATE OR REPLACE VIEW osserp.v_project_mountings AS
 SELECT sl.salutation,
    sl.firstname,
    sl.lastname,
    sl.contact_type,
    sl.id,
    sl.plan_id,
    sl.type_id,
    sl.customer_id,
    sl.status,
    sl.created,
    sl.created_by,
    sl.manager_id,
    sl.manager_sub_id,
    sl.sales_id,
    sl.branch_id,
    sl.name,
    sl.city,
    sl.plant_capacity,
    sl.stopped,
    sl.cancelled,
    sl.installer_id,
    ps.mounting_days as installation_days,
    ps.mounting_date as installation_date,
    ps.delivery_date,
    sl.confirmation_date,
    sl.origin_id,
    sl.origin_type_id,
    sl.company_id,
    sl.request_created,
    sl.street,
    sl.zipcode,
    sl.accounting_ref,
    sl.shipping_id,
    sl.payment_id,
    s.id as supplier_id,
    s.lastname as supplier_name,
    ps.group_name as supplier_group
   FROM osserp.v_sales_list sl
     LEFT JOIN osserp.project_suppliers ps ON (sl.id = ps.reference_id)
     LEFT JOIN osserp.v_suppliers s ON (s.id = ps.supplier_id)
     WHERE (sl.cancelled = false) AND (sl.stopped = false) AND (sl.status < 100)
        AND (ps.mounting_date IS NOT null) AND (ps.mounting_days > 0)
  ORDER BY sl.type_id, sl.id;

INSERT INTO system_properties VALUES (219, 'projectSupplierRelationSupport', 'false', 'java.lang.Boolean', true, false, NULL, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 219, true);

UPDATE system_properties SET pvalue = '0.15.1', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
