<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="projectContractView" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="javascript" type="string">
    </tiles:put>
    <tiles:put name="headline">
        <o:out value="${view.businessCase.type.name}"/> <o:out value="${view.businessCase.id}"/><c:if test="${!empty view.businessCase.created}"> / <o:date value="${view.businessCase.created}"/></c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="contractContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.editMode}">
                        <v:form url="/projects/projectContract/save" id="contractForm" name="contractForm">
                            <c:import url="_projectContractEdit.jsp" />
                        </v:form>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_projectSettingsDisplay.jsp" />
                        <c:if test="${view.businessCase.type.createSla or view.businessCase.type.subscription}">
                        <c:import url="_projectContractDisplay.jsp" />
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </div> <!-- row -->
        </div> <!-- content-area -->
    </tiles:put>
</tiles:insert>
