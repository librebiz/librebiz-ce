/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 17, 2010 3:27:09 PM 
 * 
 */
package com.osserp.core.tasks.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.service.impl.AbstractXmlMessageHandler;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.dao.telephone.SfaTelephoneActions;
import com.osserp.core.tasks.Task;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class SfaTelephoneActionTaskImpl extends AbstractXmlMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(SfaTelephoneActionTaskImpl.class.getName());
    private SfaTelephoneActions sfaTelephoneActions = null;

    protected SfaTelephoneActionTaskImpl(SfaTelephoneActions sfaTelephoneActions) {
        super();
        this.sfaTelephoneActions = sfaTelephoneActions;
    }

    @Override
    protected void execute(Document doc, byte[] file) {
        Element root = doc.getRootElement();
        String action = root.getChildText("action");
        String macaddress = root.getChildText("macaddress");
        Long telephoneSystemId = NumberUtil.createLong(root.getChildText("telephoneSystemId"));
        if (action == null || macaddress == null || telephoneSystemId == null) {
            log.warn("execute() invoked with missing element [xml=" + JDOMUtil.getPrettyString(doc)
                    + ", root=" + JDOMUtil.getPrettyString(root) + ", text=" + root.getText() + "]");
        } else {
            this.sfaTelephoneActions.createAction(action, macaddress, telephoneSystemId);
            if (log.isDebugEnabled()) {
                log.debug("execute() done");
            }
        }
    }
}
