<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="phoneNumberSearchView"/>

<c:set var="selectUrl" scope="request" value="/contacts.do"/>
<c:set var="selectMethod" scope="request" value="load"/>
<c:set var="selectExit" scope="request" value="phoneNumberSearchResult"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="javascript" type="string">
		<script type="text/javascript">
			<c:import url="phoneNumberSearch.js"/>
		</script>
	</tiles:put>
	<tiles:put name="headline"><fmt:message key="phoneNumberSearch"/></tiles:put>
	<tiles:put name="headline_right">
        <v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string"> 
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="contentBox">
					<div class="contentBoxData">
						<div class="subcolumns">
							<div class="subcolumn">
								<o:ajaxLookupForm name="contactSearchForm" url="/app/contacts/phoneNumberSearch/save" targetElement="ajaxContent" preRequest="if (checkSearchException()) {" postRequest="}" minChars="0" events="onkeyup" activityElement="value">
									<div style="width:100%; height:25px; margin-bottom:15px; margin-top:10px; padding-left:0px;">
                                        <div style="width:32%; float:left; margin-left:0px;">
                                            <input style="background: none; margin-right: 0px;" type="text" name="value" id="value" value="<o:out value="${view.selectedValue}"/>" />
                                        </div>
                                        <div style="width:10%; float:left; margin-left:0px; text-align: center;">
                                            <a href="javascript:if(checkSearchException()) {$('contactSearchForm').onkeyup();}">
                                                <o:img name="replaceIcon" styleClass="bigicon" style="margin-top: 8px;" />
                                            </a>
                                        </div>
										<div style="width:32%; float:left; margin-left:0px;">
											<select name="mode" size="1" id="method" style="width: 210px;" onchange="javascript: $('contactSearchForm').onkeyup();">
												<c:forEach var="mode" items="${view.modes}">
													<option value="<o:out value="${mode}"/>"<c:if test="${view.selectedMode == mode}"> selected="selected"</c:if>><fmt:message key="${mode}"/></option>
												</c:forEach>
											</select>
										</div>
										<div style="width:32%; float:left; margin-left:0px;"></div>
									</div>
								</o:ajaxLookupForm>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div id="ajaxDiv">
					<div id="ajaxContent">
						<c:if test="${!empty view.list}"><c:import url="_phoneNumberSearch.jsp"/></c:if>
					</div>
				</div>
			</div>
		</div>
	</tiles:put> 
</tiles:insert>
