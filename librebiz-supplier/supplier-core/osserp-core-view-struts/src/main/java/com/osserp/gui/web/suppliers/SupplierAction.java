/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 08-Jun-2006 10:47:27 
 * 
 */
package com.osserp.gui.web.suppliers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;

import com.osserp.gui.client.contacts.ContactView;
import com.osserp.gui.client.contacts.SupplierView;
import com.osserp.gui.web.contacts.AbstractContactViewAwareAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SupplierAction extends AbstractContactViewAwareAction {
    private static Logger log = LoggerFactory.getLogger(SupplierAction.class.getName());

    /**
     * Enables installer details in existing contact view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward display(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("display() request from " + getUser(session).getId());
        }
        ContactView contactView = getContactView(session);
        SupplierView view = (SupplierView) createView(request, SupplierView.class.getName());
        view.load(contactView);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables installer details in existing contact view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward load(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("load() request from " + getUser(session).getId());
        }
        SupplierView view = (SupplierView) createView(request, SupplierView.class.getName());
        if (view.getBean() == null) {
            return saveErrorAndReturn(request, ErrorCode.ID_INVALID);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Return from contact display to contact list.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeStatus(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeStatus() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        view.changeStatus();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Return from contact display to contact list.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableGroupSelectionMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableGroupSelectionMode() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        view.setGroupSelectionMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Return from contact display to contact list.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableGroupSelectionMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableGroupSelectionMode() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        view.setGroupSelectionMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Return from contact display to contact list.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward addGroup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("addGroup() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        view.addGroup(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Return from contact display to contact list.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward removeGroup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("removeGroup() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        view.removeGroup(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables payment agreement edit mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enablePaymentAgreementEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enablePaymentAgreementEdit() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        view.setPaymentAgreementEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables payment agreement edit mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward disablePaymentAgreementEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disablePaymentAgreementEdit() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        view.setPaymentAgreementEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates payment agreement
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward updatePaymentAgreement(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updatePaymentAgreement() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        try {
            view.updatePaymentAgreement(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(httpRequest, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates payment agreement
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward selectSupplierType(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectSupplierType() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        try {
            view.selectSupplierType(RequestUtil.getId(httpRequest));
        } catch (Exception c) {
            saveError(httpRequest, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggels direct invoice booking status
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward changeDirectInvoiceBooking(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeDirectInvoiceBooking() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        view.changeDirectInvoiceBooking();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Assignes a bank account if selected supplierType supports this.
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward assignBankAccount(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("assignBankAccount() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        try {
            view.assignBankAccount(RequestUtil.fetchId(httpRequest));
        } catch (Exception c) {
            saveError(httpRequest, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables bank account selection if selected supplierType supports this.
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enableBankAccountSelection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableBankAccountSelection() request from " + getUser(session).getId());
        }
        SupplierView view = getSupplierView(session);
        try {
            view.enableBankAccountSelection();
        } catch (Exception c) {
            saveError(httpRequest, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    private SupplierView getSupplierView(HttpSession session) throws PermissionException {
        SupplierView view = (SupplierView) fetchView(session, SupplierView.class);
        if (view == null) {
            throw new com.osserp.common.ActionException("view not bound!");
        }
        return view;
    }

}
