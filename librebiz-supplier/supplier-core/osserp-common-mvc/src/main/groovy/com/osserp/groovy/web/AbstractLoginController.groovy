/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on 02.03.2013 00:37:27
 *
 */
package com.osserp.groovy.web

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.servlet.http.HttpServletRequest

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.BackendException
import com.osserp.common.ClientException
import com.osserp.common.Constants
import com.osserp.common.web.Globals
import com.osserp.common.web.PortalView
import com.osserp.common.web.RequestUtil
import com.osserp.common.web.ResourceUtil
import com.osserp.common.web.SessionUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractLoginController extends AbstractPortalController {
    private static Logger logger = LoggerFactory.getLogger(AbstractLoginController.class.getName())

    @RequestMapping
    def forward(HttpServletRequest request) {
        String remoteAddress = RequestUtil.getRemoteAddress(request)
        logger.debug("forward: invoked [host=${remoteAddress}]")
        String loginName = request.getRemoteUser()
        if (loginName) {
            PortalView view = getPortalView(request)
            if (view && (!view.user || view.user.ldapUid != loginName)) {
                logger.debug("forward: authenticated user found [host=${remoteAddress}, uid=${loginName}]")
                def user = view.login(loginName, remoteAddress)
                SessionUtil.setUser(request.session, user)
            }
        }
        if (request.session?.servletContext.getAttribute(Globals.ERRORS_BACKEND)) {
            saveLoginError(request, request.session.servletContext.getAttribute(Globals.ERRORS_BACKEND))
        }
        defaultPage
    }

    @RequestMapping
    def authenticate(HttpServletRequest request) {
        String remoteAddress = RequestUtil.getRemoteAddress(request)
        logger.debug("authenticate: invoked [host=${remoteAddress}]")
        PortalView view = getPortalView(request)
        String loginName = request.getParameter('loginName')
        String password = request.getParameter('password')
        try {
            def user = view.login(loginName, password, RequestUtil.getRemoteAddress(request))
            SessionUtil.setUser(request.session, user)
            ResourceUtil.updateLocale(request)
            logger.debug("authenticate: done [host=${remoteAddress}, id=${user.id}, uid=${user.ldapUid}]")
            if (view.systemSetupRequired) {
                view.selectMenu('setup')
                return redirect(request, "/admin/setup/${view.systemSetupTargetForward}/forward")
            }
        } catch (BackendException b) {
            logger.warn("authenticate: caused backend exception [host=${remoteAddress}]")
            saveLoginError(request, 'Resource not available')
            try {
                request.session.invalidate()
            } catch (Exception b2) {
                logger.debug("authenticate: failed to invalidate session [host=${remoteAddress}, message=${b2.message}]")
            }
            return defaultPage
        } catch (Exception e) {
            logger.warn("authenticate: failed [host=${remoteAddress}, message=${e.message}]")
            saveLoginError(request, 'Invalid access tokens provided')
            try {
                request.session.invalidate()
            } catch (Exception e2) {
                logger.debug("authenticate: failed to invalidate session [host=${remoteAddress}, message=${e2.message}]")
            }
            return defaultPage
        }
        if (view?.systemSetupMode) {
            return 'setupEnabled'
        }
        defaultPage
    }

    @RequestMapping
    def loginChange(HttpServletRequest request) {
        String remoteAddress = RequestUtil.getRemoteAddress(request)
        PortalView view = getPortalView(request)
        logger.debug("loginChange: invoked [host=${remoteAddress}, user=${view.user?.id}]")
        if (!view.user) {
            logger.debug("loginChange: no user logged in [host=${remoteAddress}]")
            return forward(request)
        }
        'loginChange'
    }

    @RequestMapping
    def authenticationUpdate(HttpServletRequest request) {
        logger.debug("authenticationUpdate: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        PortalView view = getPortalView(request)
        try {
            String password = request.getParameter('newPassword')
            String confirmation = request.getParameter('confirmPassword')
            view.updatePassword(password, confirmation)
            logger.debug('authenticationUpdate: done')
        } catch (ClientException lx) {
            logger.debug("authenticationUpdate: failed [message=${lx.message}]")
            saveError(request, lx.message)
            return 'loginChange'
        }
        defaultPage
    }

    @RequestMapping
    def logout(HttpServletRequest request) {
        PortalView view = getPortalView(request)
        logger.debug("logout: invoked [host=${RequestUtil.getRemoteAddress(request)}, id=${view.user?.id}, uid=${view.user?.ldapUid}, target=${view.logoutTarget}]")
        String targetPage = view.logoutTarget
        request.session.invalidate()
        targetPage
    }
}
