/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 5, 2004 
 * 
 */
package com.osserp.core.contacts;

import org.jdom2.Element;

import com.osserp.common.Entity;
import com.osserp.common.Mappable;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Phone extends Cloneable, Entity, Mappable {

    static final Long PRIVATE = 1L;
    static final Long BUSINESS = 2L;
    
    static final String CHECKED = "checked";
    static final String FOREIGN_PATTERN_MATCHED = "foreignPatternMatched";
    static final String FOREIGN_OR_INVALID = "foreignOrInvalid";
    static final String INVALID = "invalid";
    static final String PATTERN_MATCHED = "patternMatched";
    static final String UNCHECKED = "unchecked";
    static final String UNCHECKED_EXTERNAL = "uncheckedExternal";
    static final String UNKNOWN = "unknown";

    /**
     * Returns the related contact
     * @return contactId
     */
    Long getContactId();

    /**
     * Returns the device id of the telephone number (PhoneType.PHONE | PhoneType.MOBILE | PhoneType.FAX | PhoneType.OFFICE)
     * @return type
     */
    Long getDevice();

    /**
     * Returns the type of the telephone number (Phone.PRIVATE | Phone.BUSINESS)
     * @return type
     */
    Long getType();

    /**
     * Sets the type
     * @param type
     */
    void setType(Long type);

    /**
     * The country prefix
     * @return country
     */
    String getCountry();

    /**
     * Sets the country prefix
     * @param country
     */
    void setCountry(String country);

    /**
     * The dial prefix
     * @return prefix
     */
    String getPrefix();

    /**
     * Sets the dial prefix
     * @param prefix
     */
    void setPrefix(String prefix);

    /**
     * The number
     * @return number
     */
    String getNumber();

    /**
     * Sets the dial number
     * @param number
     */
    void setNumber(String number);

    /**
     * A note for this phone
     * @return note
     */
    String getNote();

    /**
     * Sets the note
     * @param note
     */
    void setNote(String note);

    /**
     * Is this the primary phone
     * @return primary
     */
    boolean isPrimary();

    /**
     * Sets this phone as primary phone
     * @param primary
     */
    void setPrimary(boolean primary);

    /**
     * Indicates that number is an external business number
     * @return true if business
     */
    boolean isBusiness();

    /**
     * Indicates that number is an internal business number
     * @return internal
     */
    boolean isInternal();

    /**
     * Indicates that number is an external private number
     * @return private
     */
    boolean isPrivate();

    /**
     * Provides the phone key, e.g. 4969889900
     * @return phoneKey
     */
    String getPhoneKey();

    /**
     * Provides the listed number for dial operations.
     * @return listedNumber
     */
    String getListedNumber();

    /**
     * Indicates that number is published (e.g. for pbx)
     * @return published
     */
    boolean isPublished();

    /**
     * Sets/unsets number as published
     * @param published
     */
    void setPublished(boolean published);

    /**
     * Provides the number in the form +xx (0)xxx - XXXXXX where XXXXXX will be served as given
     * @return formatted number
     */
    String getFormattedNumber();

    /**
     * Provides the number in the form +xx (xxx) XXXXXX as expected by ms outlook where XXXXXX will be served as given
     * @return formatted number
     */
    String getFormattedOutlookNumber();

    /**
     * Provides information about the validation status of a phone number
     * @return validationStatus
     */
    String getValidationStatus();
    
    /**
     * Updates the validation status with a new value
     * @param validator id of the employee setting new status
     * @param status new status to set; no update if null.
     */
    void updateValidationStatus(Long validator, String status);

    /**
     * Provides phone values as xml. Root element will be named by type (phone,fax,mobile).
     * @return xml
     */
    Element getXml();

    /**
     * Provides phone values as xml
     * @param name of the resulting root element
     * @return xml
     */
    Element getXml(String name);

    /**
     * Clones the phone object
     * @return clone
     */
    Object clone();
}
