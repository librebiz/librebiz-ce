/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06.12.2004 
 * 
 */
package com.osserp.common.mail;

import com.osserp.common.Entity;

/**
 * 
 * Provides an email address domain entity
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EmailAddress extends Entity {

    static final Long PRIVATE = 1L;
    static final Long BUSINESS = 2L;
    static final Long INTERNAL = 3L;
    static final Long ALIAS = 4L;
    static final Long INVOICE = 5L;

    static final String VALIDATED_NOTHING = "validatedNothing";
    static final String VALIDATED_PATTERN = "validatedPattern";
    static final String VALIDATED_SMTP = "validatedSMTP";

    /**
     * @return returns the email.
     */
    String getEmail();

    /**
     * @param email The email to set.
     */
    void setEmail(String email);

    /**
     * @return returns the primary.
     */
    boolean isPrimary();

    /**
     * @param primary The primary to set.
     */
    void setPrimary(boolean primary);

    /**
     * @return returns the type.
     */
    Long getType();

    /**
     * @param type The type to set.
     */
    void setType(Long type);

    /**
     * Indicates that email address is alias of another -internal- address
     * @return true if address is internal alias
     */
    boolean isAlias();

    /**
     * Provides the id of the internal address if this address is an alias
     * @return aliasOf
     */
    Long getAliasOf();

    /**
     * Sets alias of id
     * @param aliasOf
     */
    void setAliasOf(Long aliasOf);

    /**
     * Indicates that address is external business address
     * @return true if business
     */
    boolean isBusiness();

    /**
     * Indicates that address is an internal address (but not an alias)
     * @return internal
     */
    boolean isInternal();

    /**
     * Indicates that address is external private address
     * @return private
     */
    boolean isPrivate();

    /**
     * Provides information about the validation status of an email address
     * @return validationStatus
     */
    String getValidationStatus();
}
