/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2004 
 * 
 */
package com.osserp.gui.web.services;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.service.Locator;
import com.osserp.common.service.SharedObjects;
import com.osserp.common.service.UserScope;

import com.osserp.core.service.impl.AbstractWebService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SharedObjectsService extends AbstractWebService implements SharedObjects {
    private static Logger log = LoggerFactory.getLogger(SharedObjectsService.class.getName());

    private boolean verbose = false;

    public SharedObjectsService(Locator locator) {
        super(locator);
    }

    public SharedObjectsService(Locator locator, boolean verbose) {
        super(locator);
        this.verbose = verbose;
    }

    public void add(Long id, String name, Serializable object) {
        if (log.isDebugEnabled()) {
            log.debug("add: invoked [id=" + id
                    + ", name=" + name
                    + ", object=" + (object == null ? "null" : object.getClass().getName())
                    + "]");
        }
        try {
            getUserScope().addObject(id, name, object);
        } catch (Exception e) {
            runFailed("add", e);
        }
    }

    public Serializable fetch(Long id, String name) {
        if (log.isDebugEnabled()) {
            log.debug("fetch: invoked [id=" + id + ", name=" + name + "]");
        }
        try {
            return getUserScope().getObject(id, name);
        } catch (Exception e) {
            return runFailed("fetch", e);
        }
    }

    public void remove(Long id, String name) {
        if (log.isDebugEnabled()) {
            log.debug("remove: invoked [id=" + id + ", name=" + name + "]");
        }
        try {
            getUserScope().removeObject(id, name);
        } catch (Exception e) {
            runFailed("remove", e);
        }
    }

    private Serializable runFailed(String method, Exception e) {
        if (verbose) {
            log.error("runFailed: invoked [method=" + method + ", message=" + e.getMessage() + "]", e);
            throw new BackendException("Resource not available [name=userScope]");
        }
        log.error("runFailed: invoked [method=" + method + ", message=" + e.getMessage() + "]");
        return null;
    }

    private UserScope getUserScope() {
        return (UserScope) getService(UserScope.class.getName());
    }

}
