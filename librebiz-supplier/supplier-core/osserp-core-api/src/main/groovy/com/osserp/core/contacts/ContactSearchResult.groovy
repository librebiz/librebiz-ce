/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.contacts

import com.osserp.common.PersistentEntity
import com.osserp.common.beans.AbstractEntity
import com.osserp.common.util.PhoneUtil

import com.osserp.core.contacts.ContactUtil
import com.osserp.core.contacts.Phone
import com.osserp.core.contacts.PhoneType
import com.osserp.core.contacts.Salutation

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ContactSearchResult extends AbstractEntity implements PersistentEntity {

    Long contactId
    Long parentContact
    ContactType type
    Salutation salutation
    String lastname
    String firstname
    String street
    String zipcode
    String city
    String parent
    String parentStreet
    String parentZipcode
    String parentCity
    String email
    Boolean primaryEmail
    Boolean employee
    Boolean customer
    Boolean supplier
    Boolean installer
    Boolean client
    Boolean branch
    Boolean other

    String idx
    List emails = []

    Long customerType
    Long customerStatus

    Long phoneId
    Long phoneDeviceId
    Long phoneTypeId
    String phoneCountry
    String phonePrefix
    String phoneNumber
    Boolean phonePrimary
    String phoneKey
    String phoneDisplay


    String getDisplayName() {
        ContactUtil.createDisplayName(type, lastname, firstname)
    }

    String getAddressDisplay() {
        def zip = zipcode ?: parentZipcode
        if (zip) {
            return "${zip} ${city ?: parentCity}"
        }
        return city ?: parentCity
    }

    String getStreetDisplay() {
        street ?: parentStreet
    }

    String getPhoneDisplay() {
        PhoneUtil.createOutlook(phoneCountry, phonePrefix, phoneNumber)
    }

    String getPhoneTypeDisplay() {
        def result = 'TEL'
        switch(phoneDeviceId) {
            case PhoneType.MOBILE:
                result = 'GSM'
                break
            case PhoneType.FAX:
                result = 'FAX'
        }
        result
    }

    Boolean getFax() {
        PhoneType.FAX == phoneDeviceId
    }

    Long getPrimaryKey() {
        contactId
    }
}

