/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 3, 2005 
 * 
 */
package com.osserp.core.contacts;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ContactListEntry extends Option {

    /**
     * Provides the salutation id
     * @return salutation
     */
    Long getSalutation();

    /**
     * Provides the firstname
     * @return firstName or null for companies
     */
    String getFirstName();

    /**
     * Provides the lastname of the contact or company for companies
     * @return lastName or company name
     */
    String getLastName();

    /**
     * Returns the relatedId
     * @return relatedId.
     */
    Long getRelatedId();

    /**
     * Sets the relatedId
     * @param relatedId
     */
    void setRelatedId(Long relatedId);

    /**
     * Returns the street
     * @return street.
     */
    String getStreet();

    /**
     * Sets the street
     * @param street.
     */
    void setStreet(String street);

    /**
     * Returns the city
     * @return city.
     */
    String getCity();

    /**
     * Sets the city
     * @param city
     */
    void setCity(String city);

    /**
     * Returns the phone
     * @return phone.
     */
    String getPhone();

    /**
     * Sets the phone
     * @param phone
     */
    void setPhone(String phone);

    /**
     * Returns the office
     * @return office.
     */
    String getOffice();

    /**
     * Sets the office
     * @param office
     */
    void setOffice(String office);

    /**
     * Returns the position
     * @return position.
     */
    String getPosition();

    /**
     * Sets the position
     * @param position
     */
    void setPosition(String position);

    /**
     * Returns the section
     * @return section.
     */
    String getSection();

    /**
     * Sets the section
     * @param section
     */
    void setSection(String section);

    /**
     * Indicates if list entry is currently selected. This is a non-persistent 
     * property and is used mainly by gui workflows. 
     * @return true if selected in any context.
     */
    boolean isSelected();
    
    /**
     * Enables/disables selection property
     * @param selected
     */
    void setSelected(boolean selected);
}
