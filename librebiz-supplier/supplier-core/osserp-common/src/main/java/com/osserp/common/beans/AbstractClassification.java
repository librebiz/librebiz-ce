/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 12, 2008 9:21:58 AM 
 * 
 */
package com.osserp.common.beans;

import java.util.Date;

import org.apache.commons.beanutils.BeanUtils;
import org.jdom2.Element;

import com.osserp.common.Classification;
import com.osserp.common.XmlAwareEntity;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractClassification extends AbstractEntity implements Classification, XmlAwareEntity {

    private String name = null;
    private String description = null;
    private String resourceKey = null;

    protected AbstractClassification() {
        super();
    }

    protected AbstractClassification(String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }

    protected AbstractClassification(String name, String description, String resourceKey) {
        super();
        this.name = name;
        this.description = description;
        this.resourceKey = resourceKey;
    }

    protected AbstractClassification(String name, String description, Long createdBy) {
        this(name, description);
        setCreatedBy(createdBy);
    }

    protected AbstractClassification(
            Long id,
            Long reference,
            String name,
            String description,
            String resourceKey,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            boolean endOfLife) {
        super(id, reference, created, createdBy, changed, changedBy, endOfLife);
        this.name = name;
        this.description = description;
        this.resourceKey = resourceKey;
    }

    public AbstractClassification(Element root) throws Exception {
        super(NumberUtil.createLong(root.getChildText("id")));
        for (Object child : root.getChildren()) {
            Element el = (Element) child;
            try {
                if (BeanUtils.getProperty(this, el.getName()) == null) {
                    BeanUtils.setProperty(this, el.getName(), JDOMUtil.getObjectFromElement(el));
                }
            } catch (Exception ex) {
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResourceKey() {
        return resourceKey;
    }

    public void setResourceKey(String resourceKey) {
        this.resourceKey = resourceKey;
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("description", description));
        root.addContent(JDOMUtil.createElement("name", name));
        root.addContent(JDOMUtil.createElement("resourceKey", resourceKey));
        return root;
    }

    @Override
    public abstract Object clone();
}
