<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output encoding="UTF-8" method="xml" />
    <xsl:template match="/">
        <fo:root>
            <!-- ********************************** START Seitenlayout ********************************** -->
            <fo:layout-master-set>
                <!-- Side -->
                <fo:simple-page-master master-name="A4" page-height="29.7cm" page-width="21cm" margin-top="1.8cm" margin-bottom="1cm" margin-left="2cm" margin-right="2cm">
                    <!-- Footer -->
                    <fo:region-after extent="3.5cm" region-name="xsl-region-after" />
                    <!-- Content -->
                    <fo:region-body region-name="xsl-region-body" margin-bottom="4cm">
                        <xsl:apply-templates />
                    </fo:region-body>
                </fo:simple-page-master>
                <!-- Seitenverlaufsvorlage -->
                <fo:page-sequence-master master-name="A4Side">
                    <fo:repeatable-page-master-reference master-reference="A4" />
                </fo:page-sequence-master>
            </fo:layout-master-set>
            <!-- ********************************** END Seitenlayout ********************************** -->

            <!-- Seitenverlauf -->
            <fo:page-sequence master-reference="A4Side">

                <!-- footer -->
                <fo:static-content flow-name="xsl-region-after" font-family="$documentPrintFontFamily">
                    <fo:block font-size="7pt" text-align="center" space-before="0.1cm" space-after="0.5cm">
                        Seite:
                        <fo:page-number />
                    </fo:block>
                    #parse( "xsl/shared/footer.vm" )
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">

                    <fo:table width="17.2cm" table-layout="fixed" height="2cm" space-before="$documentPrintHeaderContentSpace" space-after="0cm">
                        <fo:table-column column-width="11.2cm" column-number="1" />
                        <fo:table-column column-width="6cm" column-number="2" />
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell width="9.7cm" margin-right="0.5cm">

                                    <fo:block-container top="0cm" left="0cm" width="8cm" height="2cm" font-size="8pt">
                                        <fo:block>
                                            #if( $systemCompany.letterWindowName)
                                            $systemCompany.letterWindowName
                                            #else
                                            $systemCompany.name
                                            #end
                                        </fo:block>
                                        <fo:block space-before="0.05cm">
                                            <xsl:value-of select="/root/projectTracking/name" />
                                        </fo:block>
                                    </fo:block-container>

                                </fo:table-cell>
                                <fo:table-cell margin-left="0.4cm">

                                    <fo:block-container space-before="0cm" margin-left="1cm" top="0.5cm" width="5.8cm" height="2cm" font-size="8pt" text-align="right">
                                        <xsl:if test="/root/businessCase/id!=''">
                                            <fo:block space-before="0.03cm">
                                                Auftrag / Projekt -
                                                <xsl:value-of select="/root/businessCase/id" />
                                            </fo:block>
                                        </xsl:if>

                                        <fo:block text-align="right">
                                            <xsl:choose>
                                                <xsl:when test="string-length(/root/businessCase/name)&gt;'44'">
                                                    <xsl:value-of select="substring(/root/businessCase/name, 1, 42)" />
                                                    ...
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="/root/businessCase/name" />
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </fo:block>

                                        <xsl:if test="/root/projectTracking/printWorker='true'">
                                            <fo:block space-before="0.03cm">
                                                Bearbeiter:
                                                <xsl:value-of select="person" />
                                            </fo:block>
                                        </xsl:if>

                                        <xsl:if test="/root/projectTracking/invoiceId!=''">
                                            <fo:block space-before="0.03cm">
                                                Anlage zu Rechnungsnummer:
                                                <xsl:value-of select="/root/projectTracking/invoiceId" />
                                            </fo:block>
                                        </xsl:if>

                                    </fo:block-container>

                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>

                    <fo:table width="100%" table-layout="fixed" font-size="10pt" space-before="0cm">
                        <fo:table-column column-width="2cm" />
                        <fo:table-column column-width="4cm" />
                        <fo:table-column column-width="9.5cm" />
                        <fo:table-column column-width="1.5cm" />
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell font-weight="bold">
                                    <fo:block>Datum</fo:block>
                                </fo:table-cell>
                                <fo:table-cell font-weight="bold">
                                    <fo:block>Kontext</fo:block>
                                </fo:table-cell>
                                <fo:table-cell font-weight="bold">
                                    <fo:block>Tätigkeit</fo:block>
                                </fo:table-cell>
                                <fo:table-cell font-weight="bold">
                                    <fo:block>Stunden</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                            </fo:table-row>

                            <xsl:for-each select="/root/projectTracking/items/item">
                                <fo:table-row>
                                    <fo:table-cell>
                                        <fo:block space-before="0.2cm">
                                            <xsl:value-of select="startDate" />
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block space-before="0.2cm">
                                            <xsl:value-of select="name" />
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block space-before="0.2cm">
                                            <xsl:value-of select="description" />
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block space-before="0.2cm" text-align="right">
                                            <xsl:value-of select="duration" />
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </xsl:for-each>

                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                            </fo:table-row>

                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block font-weight="bold">Gesamtstunden</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>&#160;</fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="/root/projectTracking/totalTime" />
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>
