/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 19, 2008 7:03:04 PM 
 * 
 */
package com.osserp.gui.client.contacts;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.Option;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewName;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("privateContactsView")
public interface PrivateContactsView extends View {

    /**
     * Selects private contact by contactId if existing
     * @param contactId
     */
    public void selectByContact(Long contactId);

    /**
     * Removes a private contact
     * @param id
     */
    public void removeContact(Long id);

    /**
     * Refreshs contact list
     */
    public void refresh();

    /**
     * get the option list of current active employees
     */
    public List<Option> getEmployees();

    /**
     * get the current employee
     */
    public Employee getCurrentEmployee();

    /**
     * Sets the current employee
     */
    public void setCurrentEmployee();

    /**
     * Sets the employee
     * @param employeeId
     */
    public void setCurrentEmployee(Long employeeId);

    /**
     * adds a private contact for current employee and given contact
     * @param request
     */
    public void addContact(HttpServletRequest request);
}
