<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesOfferView}">
<o:logger write="offer_edit.jsp no view found" level="warn"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesOfferView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="conditionCommand" scope="request" value="/salesOfferInfos.do"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records_edit.css"/>
</style>
</tiles:put>

<tiles:put name="headline">
<c:import url="/pages/records/record_headline_edit.jsp"/>
</tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/salesOffer.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<v:link url="/records/recordPrint/print?view=salesOfferView" title="documentPrint"><o:img name="printIcon" /></v:link>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
 
                    <o:form name="recordForm" url="/salesOffer.do">
                    <input type="hidden" name="method" value="update"/>
                    <table class="table">
                        <tr>
                            <td class="recordEditLeft" valign="top">
                                <table>
                                    <tr>
                                        <td><fmt:message key="internalNameLabel"/></td>
                                        <td><input type="text" name="name" class="recordInput" value="<o:out value="${record.calculationInfo}"/>"/></td>
                                    </tr>
                                    <c:import url="/pages/records/record_sigs_edit.jsp"/>
                                    <c:import url="/pages/records/record_shipping_edit.jsp" />
                                    <c:import url="/pages/records/record_tax_edit.jsp"/>
                                    <c:import url="/pages/records/record_notes_edit.jsp"/>
                                </table>
                            </td>
                            <td class="recordEditRight" valign="top">
                                <table>
                                    <c:import url="/pages/records/record_language_edit.jsp"/>
                                    <c:import url="/pages/records/record_currency_edit.jsp"/>
                                    <tr>
                                        <td><fmt:message key="validUntil"/></td>
                                        <td style="text-align: right;"><input type="text" id="validUntil" name="validUntil" value="<o:date value="${record.validUntil}" />" style="width: 100px;" /></td>
                                    </tr>
                                    <c:import url="/pages/records/record_print_options.jsp"/>
                                    <tr>
                                        <td><fmt:message key="orderConfirmationPlaceholderPrintlabel"/></td>
                                        <td align="right">
                                            <v:checkbox name="printConfirmationPlaceholder" value="${record.printConfirmationPlaceholder}"/>
                                        </td>
                                    </tr>
                                    <c:import url="/pages/records/record_save_changes.jsp"/>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </o:form>
                </div>
            </div>
        </div>
    </div>
</div>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
