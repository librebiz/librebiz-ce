/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 10, 2008 7:09:26 AM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.Status;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterTemplate;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Letters extends LetterContents {

    /**
     * Creates a new letter
     * @param initialStatus
     * @param user creating new letter
     * @param type of the letter to create
     * @param recipient of the letter
     * @param name
     * @param businessId (optional)
     * @param branchId
     * @param template
     * @return letter new created
     * @throws ClientException if name already exists
     */
    Letter create(
            Status initialStatus,
            Employee user,
            LetterType type,
            ClassifiedContact recipient,
            String name,
            Long businessId,
            Long branchId,
            LetterTemplate template);

    /**
     * Finds existing letters of specified recipient
     * @param typeId
     * @param recipientId
     * @param businessId
     * @return letters
     */
    List<Letter> find(Long typeId, Long recipientId, Long businessId);

    /**
     * Counts all letters of given type and references
     * @param typeId
     * @param recipientId
     * @param businessId
     * @return count
     */
    int count(Long typeId, Long recipientId, Long businessId);

    /**
     * Updates letter references
     * @param recipient
     * @param currentType
     * @param currentBusinessId
     * @param type
     * @param businessId
     */
    void updateReferences(Long recipient, Long currentType, Long currentBusinessId, Long type, Long businessId);
}
