<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:set var="deliveryNoteUrl" value="/salesDeliveryNote.do"/>
<div class="recordLabelValue">
<c:choose>
<c:when test="${empty record.deliveryNotes}">
    <fmt:message key="deliveries"/>:
    <fmt:message key="none"/>
    <c:if test="${record.unchangeable and !record.canceled and !record.closed}">
        <c:if test="${!record.changeableDeliveryAvailable}">
            <o:permission role="delivery_note_create,purchase_delivery_create" info="permissionDeliveryNoteCreate">
                <c:choose>
                    <c:when test="${view.partialDeliveryAvailable}">
                        <v:link style="color:red;" url="/sales/deliveryNoteCreator/forward?typeId=1&exit=/salesOrder.do?method=reload" title="createPartialDeliveryNote">
                            [<fmt:message key="new"/>]
                        </v:link>
                    </c:when>
                    <c:when test="${view.deliveryAvailable}">
                        <v:link url="/sales/deliveryNoteCreator/forward?typeId=1&exit=/salesOrder.do?method=reload" title="title.delivery.note.create">
                            [<fmt:message key="new"/>]
                        </v:link>
                    </c:when>
                </c:choose>
            </o:permission>
        </c:if>
        <c:if test="${!empty sessionScope.recordView.openDeliveries}">
            <c:choose>
                <c:when test="${view.openDeliveryDisplay}">
                    <a href="<c:url value="${baseUrl}"><c:param name="method" value="disableOpenDeliveryDisplay"/></c:url>" title="<fmt:message key="disableDisplayOpenDeliveries"/>"<c:if test="${!view.deliveryAvailable or view.partialDeliveryAvailable}"> style="color:red;"</c:if>>[<fmt:message key="off"/>]</a>
                </c:when>
                <c:otherwise>
                    <a href="<c:url value="${baseUrl}"><c:param name="method" value="enableOpenDeliveryDisplay"/></c:url>" title="<fmt:message key="displayOpenDeliveries"/>"<c:if test="${!view.deliveryAvailable or view.partialDeliveryAvailable}"> style="color:red;"</c:if>>[<fmt:message key="open"/>]</a>
                </c:otherwise>
            </c:choose>
        </c:if>
        <c:if test="${!record.changeableDeliveryAvailable and view.supportingCustomDelivery and !empty view.customDeliveryType}">
            <span style="padding-left:10px;"><fmt:message key="customPartsLabelShort"/>: </span>
            <v:link url="/sales/deliveryNoteCreator/forward?typeId=${view.customDeliveryType}&exit=/salesOrder.do?method=reload" title="title.delivery.note.create">
                [<fmt:message key="new"/>]
            </v:link>
        </c:if>
    </c:if>
</c:when>
<c:otherwise>
    <fmt:message key="deliveries"/>:
    <c:if test="${record.unchangeable and !record.canceled and !record.closed and !empty view.openDeliveries}">
        <c:if test="${!record.changeableDeliveryAvailable}">
            <o:permission role="delivery_note_create,purchase_delivery_create" info="permissionDeliveryNoteCreate">
                <c:choose>
                    <c:when test="${view.partialDeliveryAvailable}">
                        <v:link style="color:red;" url="/sales/deliveryNoteCreator/forward?typeId=1&exit=/salesOrder.do?method=reload" title="createPartialDeliveryNote">
                            [<fmt:message key="new"/>]
                        </v:link>
                    </c:when>
                    <c:when test="${view.deliveryAvailable}">
                        <v:link url="/sales/deliveryNoteCreator/forward?typeId=1&exit=/salesOrder.do?method=reload" title="title.delivery.note.create">
                            [<fmt:message key="new"/>]
                        </v:link>
                    </c:when>
                </c:choose>
            </o:permission>
        </c:if>
        <c:choose>
            <c:when test="${view.openDeliveryDisplay}">
                <a href="<c:url value="${baseUrl}"><c:param name="method" value="disableOpenDeliveryDisplay"/></c:url>" title="<fmt:message key="disableDisplayOpenDeliveries"/>"<c:if test="${!view.deliveryAvailable or view.partialDeliveryAvailable}"> style="color:red;"</c:if>>[<fmt:message key="off"/>]</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="${baseUrl}"><c:param name="method" value="enableOpenDeliveryDisplay"/></c:url>" title="<fmt:message key="displayOpenDeliveries"/>"<c:if test="${!view.deliveryAvailable or view.partialDeliveryAvailable}"> style="color:red;"</c:if>>[<fmt:message key="open"/>]</a>
            </c:otherwise>
        </c:choose>
    </c:if>
    <c:forEach var="note" items="${record.deliveryNotes}">
        <c:if test="${note.bookingType.parentContent}">
            <c:choose>
                <c:when test="${note.unchangeable}">
                <br/><a href="<c:url value="${deliveryNoteUrl}"><c:param name="method" value="select"/><c:param name="id" value="${note.id}"/></c:url>"><o:out value="${note.number}"/></a> <fmt:message key="from"/> <fmt:formatDate value="${note.created}"/>
                </c:when>
                <c:otherwise>
                <br/><a style="color:red;" href="<c:url value="${deliveryNoteUrl}"><c:param name="method" value="select"/><c:param name="id" value="${note.id}"/></c:url>"><o:out value="${note.number}"/></a> <fmt:message key="from"/> <fmt:formatDate value="${note.created}"/>
                </c:otherwise>
            </c:choose>
        </c:if>
    </c:forEach>
    <c:if test="${view.supportingCustomDelivery and !empty view.customDeliveryType}">
        <br/><fmt:message key="customPartsLabel"/>:
        <c:if test="${!record.changeableDeliveryAvailable}">
            <v:link url="/sales/deliveryNoteCreator/forward?typeId=${view.customDeliveryType}&exit=/salesOrder.do?method=reload" title="title.delivery.note.create">
                [<fmt:message key="new"/>]
            </v:link>
        </c:if>
        <c:forEach var="note" items="${record.deliveryNotes}">
            <c:if test="${!note.bookingType.parentContent}">
                <c:choose>
                    <c:when test="${note.unchangeable}">
                    <br/><a href="<c:url value="${deliveryNoteUrl}"><c:param name="method" value="select"/><c:param name="id" value="${note.id}"/></c:url>"><o:out value="${note.number}"/></a> <fmt:message key="from"/> <fmt:formatDate value="${note.created}"/>
                    </c:when>
                    <c:otherwise>
                    <br/><a style="color:red;" href="<c:url value="${deliveryNoteUrl}"><c:param name="method" value="select"/><c:param name="id" value="${note.id}"/></c:url>"><o:out value="${note.number}"/></a> <fmt:message key="from"/> <fmt:formatDate value="${note.created}"/>
                    </c:otherwise>
                </c:choose>
            </c:if>
        </c:forEach>
    </c:if>
</c:otherwise>
</c:choose>
</div>
