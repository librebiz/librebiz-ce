<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.deliveryMonitoringView}"/>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="settings"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="stock"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <select name="selectAction" size="1" onChange="gotoUrl(this.value);">
                            <c:forEach var="obj" items="${view.availableStocks}">
                                <c:choose>
                                    <c:when test="${obj.id == view.selectedStock.id}">
                                        <option value="<c:url value="/salesDeliveryMonitoring.do?method=selectStock&id=${obj.id}"/>" selected="selected"><oc:options name="systemCompanies" value="${obj.id}"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<c:url value="/salesDeliveryMonitoring.do?method=selectStock&id=${obj.id}"/>"><oc:options name="systemCompanies" value="${obj.id}"/></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="searchFor"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.listVacantMode}">
                                <a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleListVacantMode"/>" title="<fmt:message key="currentLabel"/>: <fmt:message key="displayAllOpenOrders"/>">
                                    <fmt:message key="displayReleasedOnly"/>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleListVacantMode"/>" title="<fmt:message key="currentLabel"/>: <fmt:message key="displayReleasedOnly"/>">
                                    <fmt:message key="displayAllOpenOrders"/>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
