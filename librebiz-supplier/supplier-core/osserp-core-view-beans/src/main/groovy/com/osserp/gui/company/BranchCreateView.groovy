/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 11-Feb-2011 
 * 
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.contacts.ContactManager
import com.osserp.core.customers.Customer
import com.osserp.core.model.system.BranchOfficeImpl
import com.osserp.core.system.BranchOfficeManager
import com.osserp.core.system.SystemCompany
import com.osserp.core.system.SystemCompanyManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class BranchCreateView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(BranchCreateView.class.getName())
    
    Integer shortkeyMaxlength

    BranchCreateView() {
        dependencies = ['contactView']
        headerName = 'branchCreate'
        enableCompanyPreselection()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            shortkeyMaxlength = branchOfficeManager.shortkeyMaxlength
            if (!env.selectedContact) {
                logger.debug('initRequest: no contact selected...')
                if (!env.contactView || !env.contactView.bean) {
                    throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
                }
                def selectedContact = env.contactView.bean
                if (selectedContact instanceof Customer) {
                    selectedContact = ((Customer) selectedContact).rc
                }
                if (selectedContact.branchOffice) {
                    logger.debug("initRequest: error: selected contact is already branch [contactId=${selectedContact.contactId}]")
                    throw new ViewContextException(ErrorCode.CONTACT_ALREADY_BRANCH, url('/company/branch/forward'))
                }
                env.selectedContact = selectedContact
            }
            if (!bean && env.selectedContact) {
                // initial call
                logger.debug('initRequest: initial call, bean not initialized...')
                if (multipleClientsAvailable || !selectedCompany) {
                    bean = new BranchOfficeImpl(null, null, env.selectedContact, env.selectedContact.getDisplayName(), null, null)
                } else {
                    bean = new BranchOfficeImpl(null, selectedCompany, env.selectedContact, env.selectedContact.getDisplayName(), null, null)
                }
            }
            enableCreateMode()
            companies = systemConfigManager.companies
        }
    }

    @Override
    void save() {
        if (!bean) {
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
        }
        bean.name = form.getString('name')
        Long ref = getLong('reference')
        if (ref) {
            bean.reference = ref
        }
        if (!bean.reference) {
            throw new ClientException(ErrorCode.COMPANY_MISSING)
        }
        bean.company = systemConfigManager.getCompany(bean.reference)
        bean.shortkey = form.getString('shortkey')
        bean.shortname = form.getString('shortname')
        bean.email = form.getString('email')
        bean.phoneCountry = form.getString('phoneCountry')
        bean.phonePrefix = form.getString('phonePrefix')
        bean.phoneNumber = form.getString('phoneNumber')
        bean.headquarter = form.getBoolean('headquarter')
        bean.costCenter = form.getLong('costCenter')

        if (bean.name == null || bean.name.length() < 1 || bean.company == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }

        bean = branchOfficeManager.create(bean)
        contactManager.updateBranchFlag(bean.contact.contactId, true)
        env.contactView?.refresh()
    }

    protected BranchOfficeManager getBranchOfficeManager() {
        getService(BranchOfficeManager.class.getName())
    }

    protected ContactManager getContactManager() {
        getService(ContactManager.class.getName())
    }
}
