/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 26, 2009 6:34:59 AM 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.sales.SalesMonitoringItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessCaseRelationStatVO extends AbstractOption implements BusinessCaseRelationStat {

    private String lastName = null;
    private String firstName = null;
    private Long branchId = null;
    private boolean active = false;
    private double capUnstopped;
    private double capOpen;
    private double capClosed;
    private double capLost;
    private int countOpen;
    private int countUnstopped;
    private int countClosed;
    private int countLost;

    protected BusinessCaseRelationStatVO() {
        super();
    }

    public BusinessCaseRelationStatVO(
            Long id,
            String lastName,
            String firstName,
            Long branchId,
            boolean active,
            double capUnstopped,
            double capOpen,
            double capClosed,
            double capLost,
            int countOpen,
            int countUnstopped,
            int countClosed,
            int countLost) {

        super(id);
        this.lastName = lastName;
        this.firstName = firstName;
        this.branchId = branchId;
        this.active = active;
        this.capUnstopped = capUnstopped;
        this.capOpen = capOpen;
        this.capClosed = capClosed;
        this.capLost = capLost;
        this.countOpen = countOpen;
        this.countUnstopped = countUnstopped;
        this.countClosed = countClosed;
        this.countLost = countLost;
    }

    /**
     * Creates a new stat object by sales and sales relation
     * @param item
     * @param name
     */
    public BusinessCaseRelationStatVO(SalesMonitoringItem item, String name) {
        super(item.getInstallerId(), name);
        if (item.getBranch() != null) {
            branchId = item.getBranch().getId();
        }
        addItem(item);
    }
    
    public void addItem(SalesMonitoringItem item) {
        if (item.isCancelled()) {
            countLost++;
            capLost = capLost + item.getCapacity();
        } else if (item.isClosed()) {
            countClosed++;
            capClosed = capClosed + item.getCapacity();
        } else {
            countOpen++;
            capOpen = capOpen + item.getCapacity();
            if (!item.isStopped()) {
                countUnstopped++;
                capUnstopped = capUnstopped + item.getCapacity();
            }
        }
    }

    @Override
    public String getName() {
        return firstName == null && lastName == null ? super.getName() : 
            firstName == null ? lastName : lastName + ", " + firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public boolean isActive() {
        return active;
    }

    public double getCapUnstopped() {
        return capUnstopped;
    }

    public double getCapStopped() {
        return (capOpen - capUnstopped);
    }

    public double getCapOpen() {
        return capOpen;
    }

    public double getCapClosed() {
        return capClosed;
    }

    public double getCapLost() {
        return capLost;
    }

    public int getCountUnstopped() {
        return countUnstopped;
    }

    public int getCountStopped() {
        return (countOpen - countUnstopped);
    }

    public int getCountOpen() {
        return countOpen;
    }

    public int getCountClosed() {
        return countClosed;
    }

    public int getCountLost() {
        return countLost;
    }

    public Long getBranchId() {
        return branchId;
    }

    public int getCountTotal() {
        return countOpen + countClosed + countLost;
    }

    // bean is readonly
    protected void setLastName(String lastName) {
        this.lastName = lastName;
    }

    protected void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    protected void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    protected void setActive(boolean active) {
        this.active = active;
    }

    protected void setCapUnstopped(double capUnstopped) {
        this.capUnstopped = capUnstopped;
    }

    protected void setCapOpen(double capOpen) {
        this.capOpen = capOpen;
    }

    protected void setCapClosed(double capClosed) {
        this.capClosed = capClosed;
    }

    protected void setCapLost(double capLost) {
        this.capLost = capLost;
    }

    protected void setCountOpen(int countOpen) {
        this.countOpen = countOpen;
    }

    protected void setCountUnstopped(int countUnstopped) {
        this.countUnstopped = countUnstopped;
    }

    protected void setCountClosed(int countClosed) {
        this.countClosed = countClosed;
    }

    protected void setCountLost(int countLost) {
        this.countLost = countLost;
    }

    @Override
    public Object clone() {
        return new BusinessCaseRelationStatVO(
                getId(),
                lastName,
                firstName,
                branchId,
                active,
                capUnstopped,
                capOpen,
                capClosed,
                capLost,
                countOpen,
                countUnstopped,
                countClosed,
                countLost);
    }

}
