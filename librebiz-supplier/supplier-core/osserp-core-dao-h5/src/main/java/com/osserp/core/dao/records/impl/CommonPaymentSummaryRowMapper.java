/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 18, 2016 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;

import com.osserp.core.dao.impl.BusinessObjectRowMapper;
import com.osserp.core.finance.CommonPaymentSummary;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CommonPaymentSummaryRowMapper extends BusinessObjectRowMapper {
    
    public static final String SELECT_FROM =
            "SELECT id, type_id, reference, supplier, name, status, created, created_by, taxfree, " +
                    "amount, tax, reducedtax, gross, sales, canceled, internal, company, branch, " +
                    "reference_type, paid_amount, tax_rate, note, bank_account_id, document_id FROM ";

    public CommonPaymentSummaryRowMapper(OptionsCache options) {
        super(options);
    }

    public static RowMapperResultSetExtractor getReader(OptionsCache options) {
        return new RowMapperResultSetExtractor(new CommonPaymentSummaryRowMapper(options));
    }

    public Object mapRow(ResultSet rs, int arg1) throws SQLException {
        long reference = rs.getLong(3);
        long bankAccountId = rs.getLong(23);
        long documentId = rs.getLong(24);
        CommonPaymentSummary record = new CommonPaymentSummary(
            Long.valueOf(rs.getLong(1)),     // id
            fetchRecordType(Long.valueOf(rs.getLong(2))),
            (reference == 0 ? null : reference), // reference to sales or other entity
            Long.valueOf(rs.getLong(4)),     // id of supplier
            rs.getString(5),             // name of supplier
            Long.valueOf(rs.getLong(6)),     // status
            rs.getTimestamp(7),          // created
            Long.valueOf(rs.getLong(8)),     // created_by
            rs.getBoolean(9),            // taxFree 
            rs.getDouble(10),            // amount of the record (net) 
            rs.getDouble(11),            // tax
            rs.getDouble(12),            // reducedTax
            rs.getDouble(13),            // gross  == amount_open of source table
            Long.valueOf(rs.getLong(14)),    // sales id if referenced to business case
            rs.getBoolean(15),           // canceled 
            rs.getBoolean(16),           // internal
            fetchCompany(Long.valueOf(rs.getLong(17))),    // company
            fetchBranch(Long.valueOf(rs.getLong(18))),   // branch
            rs.getDouble(20),           // paid_amount
            rs.getDouble(21),           // tax_rate
            rs.getString(22),           // note
            (bankAccountId == 0 ? null : Long.valueOf(bankAccountId)),
            (documentId == 0 ? null : Long.valueOf(documentId)));
        return record;
    }

}
