<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${!empty record.shippingId}">
    <div class="recordLabelValue">
        <span><fmt:message key="shippingLabel"/>: <oc:options name="shippingCompanies" value="${record.shippingId}"/></span>
    </div>
</c:if>
