<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeGroupPermissionView"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<o:out value="${view.groupsByPermissionName}"/>
	</div>
	<div class="modalBoxHeaderRight">
		<div class="boxnav">
		</div>
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<table class="table" style="width: 380px;">
				<tr>
					<th class="center">GID</th>
					<th class="small"><fmt:message key="name"/></th>
				</tr>
				<c:choose>
					<c:when test="${empty view.groupsByPermission}">
						<tr>
							<td colspan="2" class="left"><fmt:message key="noGroupsAssigned"/></td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="obj" items="${view.groupsByPermission}">
							<tr>
								<td class="center"><o:out value="${obj.id}"/></td>
								<td class="left"><o:out value="${obj.name}"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
			<div class="spacer"></div>
		</div>
	</div>
</div>
