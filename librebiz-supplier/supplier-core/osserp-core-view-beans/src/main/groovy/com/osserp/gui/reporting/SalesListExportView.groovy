/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jul 7, 2011 3:34:24 PM 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ContentType
import com.osserp.common.ErrorCode
import com.osserp.common.OptionsCache
import com.osserp.core.Options
import com.osserp.core.sales.SalesUtil

import com.osserp.groovy.web.AbstractOutputView
import com.osserp.groovy.web.ViewContextException

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
public class SalesListExportView extends AbstractOutputView {
    private static Logger logger = LoggerFactory.getLogger(SalesListExportView.class.getName())

    List list = []

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        String dependentViewName = form.getString('view')
        if (dependentViewName && !env[dependentViewName]) {
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
        }

        list = env[dependentViewName].list
    }

    byte[] getData() {
        logger.debug("getData: invoked")
        String spreadSheet = SalesUtil.createSpreadSheet(
            list, 
            optionsCache.getMap(Options.EMPLOYEES), 
            optionsCache.getMap(Options.SUPPLIERS))
        return spreadSheet.getBytes()
    }

    String getContentType() {
        ContentType.EXCEL
    }

    private OptionsCache getOptionsCache() {
        getService(OptionsCache.class.getName())
    }
}
