<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource />
<v:view viewName="bankAccountDocumentView" />
<c:set var="dmsDocumentView" scope="request" value="${sessionScope.bankAccountDocumentView}"/>
<c:set var="dmsDocumentUrl" scope="request" value="/accounting/bankAccountDocument"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="documentsTo" />
        <o:out value="${view.bankAccount.name}" />
        <c:if test="${empty view.bankAccount.nameAddon}">
            <o:out value="${view.bankAccount.nameAddon}" />
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="annualReportContent">
            <div class="row">
                <c:choose>
                <c:when test="${!empty view.bean}">
                    <c:import url="${viewdir}/dms/_documentUpdateMetadata.jsp" />
                </c:when>
                <c:otherwise>
                <div class="col-md-12 panel-area">
                    <v:form url="/accounting/bankAccountDocument/save" multipart="true">
                        <div class="table-responsive table-responsive-default">
                            <table class="table">
                                <tbody>
                                    <tr class="row-alt">
                                        <td><v:text name="note" styleClass="form-control" placeholder="name" /></td>
                                        <td><v:date name="validFrom" picker="true" placeholder="dateStartLabel" styleClass="form-control date inline" /></td>
                                        <td><v:date name="validTil" picker="true" placeholder="til" styleClass="form-control date inline" /></td>
                                        <td colspan="3"><o:fileUpload /></td>
                                        <td colspan="2"><o:submitFile /></td>
                                    </tr>
                                    <c:import url="${viewdir}/dms/_commonDocumentImports.jsp" />
                                    <c:forEach items="${view.list}" var="doc">
                                        <tr>
                                            <td><o:doc obj="${doc}">
                                                    <o:out value="${doc.displayName}" limit="70" />
                                                </o:doc></td>
                                            <td><o:date value="${doc.validFrom}" /></td>
                                            <td><o:date value="${doc.validTil}" /></td>
                                            <td class="doc"><span class="docName"><o:out value="${doc.fileName}" /></span></td>
                                            <td><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${doc.createdBy}">
                                                    <oc:employee initials="true" value="${doc.createdBy}" />
                                                </v:ajaxLink></td>
                                            <td class="docDate"><o:date value="${doc.created}" /></td>
                                            <td class="action icon"><o:permission role="accounting,accounting_bank_documents" info="permissionDocumentsDelete">
                                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="/accounting/bankAccountDocument/delete?id=${doc.id}" />');" title="<fmt:message key="deleteDocument"/>"><o:img name="deleteIcon" /></a>
                                                </o:permission></td>
                                            <td class="action icon">
                                                <v:link url="${view.baseLink}/select?id=${doc.id}" title="documentEdit">
                                                    <o:img name="writeIcon" />
                                                </v:link>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </v:form>
                </div>
                </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
