/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 15, 2010 10:24:20 AM 
 * 
 */
package com.osserp.gui.tags;

import javax.servlet.jsp.JspException;

import com.osserp.common.util.PhoneUtil;
import com.osserp.common.web.Globals;
import com.osserp.common.web.PortalView;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.common.web.tags.AbstractImageTag;

import com.osserp.core.contacts.OfficePhone;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class PhoneTag extends AbstractImageTag {

    private Phone phone;
    private String phoneNumber;
    private boolean provideIcon = false;

    public boolean getProvideIcon() {
        return provideIcon;
    }

    public void setProvideIcon(boolean provideIcon) {
        this.provideIcon = provideIcon;
    }

    @Override
    public String getOutput() throws JspException {
        StringBuilder buffer = new StringBuilder(1024);
        PortalView view = getPortalView();
        setName("telephoneGoIcon");
        this.fetchValue();
        if (phone != null || phoneNumber != null) {
            if (view != null && view.isClickToCallEnabled()
                    && ((phone != null && !this.phone.getDevice().equals(PhoneType.FAX)) || (phoneNumber != null && !this.phoneNumber.equals("-1")))) {

                StringBuffer result = new StringBuffer(17);
                result.append((phone != null) ? phone.getPhoneKey() : PhoneUtil.clearPhoneNumber(phoneNumber, true)).toString();

                if (provideIcon) {
                    buffer.append((phone != null) ? phone.getFormattedOutlookNumber() : phoneNumber).append(" ");
                }
                String exitLink = RequestUtil.appendCSRFToken(getRequest(), "/app/users/clickToCall/exit");
                buffer
                        .append("<a href=\"javascript:;\" ")
                        .append("onclick=\"ojsAjax.ajaxLink('")
                        .append(getContextPath())
                        .append("/app/users/clickToCall/clickToCall?pnumber=")
                        .append(result)
                        .append("', 'click_to_call_popup', true, '', '', function() {  $('click_to_call_popup_close').onclick = function() { ojsAjax.loadAndExecute('")
                        .append(getContextPath())
                        .append(exitLink).append("'); } }, this);\" ")
                        .append("title=\"")
                        .append(getResourceString("callPhoneNumber"))
                        .append("\">");
                if (provideIcon) {
                    buffer.append("<img src=\"")
                            .append(getImageUrl())
                            .append("\" class=\"icon\"/>");
                } else {
                    buffer.append((phone != null) ? phone.getFormattedOutlookNumber() : phoneNumber);
                }
                buffer.append("</a>");
            } else {
                buffer.append((phone != null) ? phone.getFormattedOutlookNumber() : phoneNumber);
            }
        } else {
            buffer.append((String) value);
        }
        //TODO "null" shouldn't be written into buffer at all (this is just a quick fix)
        return buffer.toString().equals("null") ? "" : buffer.toString();
    }

    protected void fetchValue() {
        if (value != null) {
            if (value instanceof Phone) {
                this.phone = (Phone) value;
                this.phoneNumber = null;
            } else if (value instanceof OfficePhone) {
                this.phone = (Phone) value;
                this.phoneNumber = null;
            } else if (value instanceof Long) {
                this.phone = null;
                this.phoneNumber = Long.toString((Long) value);
            } else {
                this.phone = null;
                this.phoneNumber = (String) value;
            }
        } else {
            this.phone = null;
            this.phoneNumber = null;
        }
    }

    /**
     * Tries to fetch portal view by name as defined under Globals.PORTAL_VIEW. This method makes no attempt to create a portal view if none exists. Subclass
     * this class and override this method if you need a page tag creating a new portal view when no view exists.
     * @return portalView or null if not exists
     */
    protected PortalView getPortalView() {
        View view = (View) getSession().getAttribute(Globals.PORTAL_VIEW);
        if (view != null && view instanceof PortalView) {
            return (PortalView) view;
        }
        return null;
    }

}
