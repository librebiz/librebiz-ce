/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 09-Feb-2007 16:33:33 
 * 
 */
package com.osserp.core.service.requests;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.User;
import com.osserp.common.UserManager;

import com.osserp.core.Address;
import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.contacts.PrivateContactManager;
import com.osserp.core.crm.CampaignAssignmentHistory;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.BusinessProperties;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.ResourceProvider;
import com.osserp.core.dao.records.SalesOffers;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;
import com.osserp.core.requests.RequestFcsAction;
import com.osserp.core.requests.RequestFcsManager;
import com.osserp.core.requests.RequestManager;
import com.osserp.core.sales.SalesPersonLocator;
import com.osserp.core.sales.SalesPersonLocatorResult;
import com.osserp.core.service.impl.AbstractBusinessCaseManager;
import com.osserp.core.tasks.RequestChangedSender;
import com.osserp.core.tasks.RequestListCacheSender;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestManagerImpl extends AbstractBusinessCaseManager implements RequestManager {
    private static Logger log = LoggerFactory.getLogger(RequestManagerImpl.class.getName());
    private static final String REQUEST_CREATED_EVENT = "requestCreatedEventTask";
    private static final String REQUEST_ORIGIN_CHANGED_EVENT = "requestOriginChangedEventTask";

    private SalesRequestChangedProcessor requestChangedService = null;
    private RequestFcsManager flowControlManager = null;
    private SalesOffers offers = null;
    private RequestListCacheSender requestList = null;
    private RequestChangedSender requestChangedSender = null;
    private UserManager userManager = null;
    private PrivateContactManager privateContactManager = null;
    private SalesPersonLocator salesPersonLocator = null;

    public RequestManagerImpl(
            ResourceProvider resourceProvider, 
            BusinessProperties businessProperties,
            Requests requests,
            Employees employees, 
            RequestFcsManager flowControlManager,
            SalesRequestChangedProcessor requestChangedService,
            SalesOffers offers,
            RequestChangedSender requestChangedSender,
            RequestListCacheSender requestList,
            UserManager userManager,
            PrivateContactManager privateContactManager,
            SalesPersonLocator salesPersonLocator) {
        super(resourceProvider, businessProperties, requests, employees);
        this.flowControlManager = flowControlManager;
        this.requestChangedService = requestChangedService;
        this.offers = offers;
        this.requestChangedSender = requestChangedSender;
        this.requestList = requestList;
        this.userManager = userManager;
        this.privateContactManager = privateContactManager;
        this.salesPersonLocator = salesPersonLocator;
    }

    public boolean exists(Long id) {
        return getRequests().exists(id);
    }

    public BusinessCase load(Long id) {
        return getRequests().load(id);
    }

    public List<BusinessType> getRequestTypes() {
        return getRequests().getRequestTypes();
    }

    public Request createEmpty(User user, Customer customer, BusinessType type) {
        if (!(user instanceof DomainUser) || customer == null || type == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        Request request = getRequests().createTransient(user, customer, type);
        SalesPersonLocatorResult locatorResult = salesPersonLocator.find((DomainUser) user, request);
        request.updateSales(locatorResult.getSales(), locatorResult.getSales().getBranch());
        request.updateSalesAssignment(locatorResult.getResolution(), locatorResult.isStrictAssignment());
        if (log.isDebugEnabled()) {
            log.debug("createEmpty() done [user=" + ((DomainUser) user).getEmployee().getId()
                    + ", branch=" + (request.getBranch() == null ? "null" : request.getBranch().getId() + "]")
                    + ", salesPerson=" + request.getSalesId()
                    + ", salesAssignment=" + request.getSalesAssignment()
                    + ", salesAssignmentStrict=" + request.isSalesAssignmentStrict()
                    + "]");
        }
        return request;
    }

    public Long createRequest(Request request) {
        if (request.getBranch() == null && request.getType() != null && request.getType().getCompany() != null) {

            Employee user = null;

            if (request.getSalesId() != null) {
                user = (Employee) getEmployees().find(request.getSalesId());
                if (user != null && user.getBranch() != null
                        && request.getType().getCompany().equals(user.getBranch().getCompany().getId())) {
                    request.updateBranch(user.getBranch());
                }
            }
            if (request.getBranch() == null && request.getCreatedBy() != null) {

                user = (Employee) getEmployees().find(request.getCreatedBy());
                if (user != null && user.getBranch() != null
                        && request.getType().getCompany().equals(user.getBranch().getCompany().getId())) {
                    request.updateBranch(user.getBranch());
                }
            }
        }
        getRequests().save(request);
        flowControlManager.initializeFlowControl(request);
        if (log.isDebugEnabled()) {
            log.debug("create() created new [request="
                    + request.getRequestId()
                    + "]");
        }
        Map<String, Object> values = new java.util.HashMap<String, Object>();
        values.put(RequestChangedSender.EVENT, REQUEST_CREATED_EVENT);
        values.put(RequestChangedSender.REQUEST_ID, request.getRequestId());
        requestChangedSender.send(values);
        return request.getRequestId();
    }

    public Request transferRequest(User user, Request request, BusinessType type) {
        if (log.isDebugEnabled()) {
            log.debug("transferRequest() invoked [srcType=" + request.getType().getId()
                    + ", targetType=" + type.getId() + "]");
        }
        Request transfered = getRequests().changeType(
                (user.getId() == null ? Constants.SYSTEM_USER : user.getId()), 
                request, 
                type);
        updateRequestList(transfered, "transfered");
        return transfered;
    }

    public void update(Request request) throws ClientException {
        validateAddress(request.getAddress());
        getRequests().save(request);
        updateRequestList(request, "update");
    }

    public void updateOrigin(Request request, Employee user, Long origin) {
        CampaignAssignmentHistory hist = getRequests().updateOrigin(request, user, origin);
        if (hist != null && requestChangedSender != null) {
            Map<String, Object> values = new java.util.HashMap<String, Object>();
            values.put(RequestChangedSender.EVENT, REQUEST_ORIGIN_CHANGED_EVENT);
            values.put(RequestChangedSender.REQUEST_ID, request.getRequestId());
            values.put("historyId", hist.getId());
            requestChangedSender.send(values);
        } else {
            log.warn("updateOrigin() not creating change event [request=" + request.getRequestId()
                    + ", oldOrigin=" + request.getOrigin()
                    + ", newOrigin=" + origin
                    + ", senderAvailable=" + (requestChangedSender != null)
                    + ", user=" + (user == null ? "null" : user.getId())
                    + "]");
        }
    }

    public void updateOriginType(Request request, Employee user, Long originType) {
        if (request == null) {
            log.error("updateOriginType() invoked with null as request, check caller");
            throw new BackendException();
        }
        request.updateOriginType(user == null ? null : user.getId(), originType);
        getRequests().save(request);
        if (log.isDebugEnabled()) {
            log.debug("updateOriginType() done [request=" + request.getRequestId()
                    + ", originType=" + request.getOriginType()
                    + ", user=" + (user == null ? "null" : user.getId())
                    + "]");
        }
    }

    public void validateAddress(Address address) throws ClientException {
        // we do nothing here now
    }

    public void validateCommons(Request request) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("validateCommons() called");
        }
        if (request == null) {
            throw new BackendException("request is null");
        }
        if (request.getType() == null) {
            throw new BackendException("request type is null");
        }
        if (isNotSet(request.getSalesId())) {
            throw new ClientException(ErrorCode.SALESPERSON_MISSING);
        }
        Date now = new Date(System.currentTimeMillis());
        if (request.getSalesTalkDate() == null) {
            throw new ClientException(ErrorCode.SALESTALK_DATE_MISSING);
        } else if (request.getSalesTalkDate().after(now)) {
            throw new ClientException(ErrorCode.SALESTALK_DATE_FUTURE);
        }
        if (!request.getType().isDirectSales()) {
            if (request.getPresentationDate() == null) {
                throw new ClientException(ErrorCode.TARGET_DATE_MISSING);
            } else if (request.getPresentationDate().before(now)) {
                throw new ClientException(ErrorCode.TARGET_DATE_PAST);
            }
        }
        if (request.getOrigin() == null || request.getOrigin() == 0
                || request.getOriginType() == null || request.getOriginType() == 0) {
            throw new ClientException(ErrorCode.ORIGIN_MISSING);
        }
        if (log.isDebugEnabled()) {
            log.debug("validateCommons() done; no errors");
        }
    }

    public List<CampaignAssignmentHistory> getOriginHistory(Request request) {
        return getRequests().getCampaignAssignmentHistory(request);
    }

    public boolean isOriginHistoryAvailable(Request request) {
        return getRequests().isCampaignAssignmentHistoryAvailable(request);
    }

    public BusinessCase updateBranch(BusinessCase businessCase, Long id) {
        if (businessCase.isClosed() || !(businessCase instanceof Request)) {
            if (businessCase.isClosed()) {
                throw new IllegalStateException("business case must not be closed: " + businessCase.getPrimaryKey());
            }
            throw new IllegalStateException("business case must be instance of Request: " + businessCase.getPrimaryKey());
        }
        Request request = (Request) businessCase;
        getRequests().changeBranch(request, id);
        offers.changeBranch(request);
        updateRequestList(request, "updateBranch");
        return load(request.getPrimaryKey());
    }

    public void updateSalesCo(Request request, Long salesCoId) {
        request.setSalesCoId(salesCoId);
        getRequests().save(request);
        updateRequestList(request, "updateSalesCo");
    }

    public void updateSalesCoPercent(Request request, Double salesCoPercent) {
        request.setSalesCoPercent(salesCoPercent);
        getRequests().save(request);
    }

    public BusinessCase setManager(User user, BusinessCase businessCase, Long employeeId) {
        if (isNotSet(employeeId) || !(businessCase instanceof Request)) {
            throw new BackendException(ErrorCode.NULL_PARAM);
        }
        Request request = (Request) businessCase;
        if (isNotSet(employeeId) || (request.getManagerId() != null && request.getManagerId().equals(employeeId))) {
            request.setManagerId(null);
        } else {
            request.setManagerId(employeeId);
        }
        request.setChanged(new Date(System.currentTimeMillis()));
        request.setChangedBy(user.getId());
        getRequests().save(request);
        return load(request.getPrimaryKey());
    }

    public synchronized BusinessCase setSales(User user, BusinessCase businessCase, Long employeeId) {
        if (isNotSet(employeeId) || !(businessCase instanceof Request)) {
            throw new BackendException(ErrorCode.NULL_PARAM);
        }
        Request request = getRequests().load(businessCase.getPrimaryKey());
        Employee oldSales = fetchSales(request);
        if (oldSales == null || !oldSales.getId().equals(employeeId)) {
            Employee sales = getEmployees().get(employeeId);
            DomainUser domainUser = (DomainUser) userManager.findById(sales.getId());
            if (domainUser != null && request.getCustomer() != null) {
                privateContactManager.createPrivate(domainUser, request.getCustomer(), null);
            }
            request.updateSales(sales, sales.getBranch());
            request.setChanged(new Date(System.currentTimeMillis()));
            request.setChangedBy(user.getId());
            getRequests().save(request);
            if (request.getType().isUpdateBranchOnSalesPersonChange()) {
                offers.changeBranch(request);
            }
            try {
                requestChangedService.execute(oldSales, sales, request);
            } catch (Exception e) {
                log.error("setSales: failed to send requestChanged event [request="
                    + request.getRequestId() + ", message=" + e.getMessage() + "]", e);
            }
        } else if (log.isDebugEnabled()) {
            log.debug("setSales: Nothing to do [sales=" + oldSales.getId() + ", new=" + employeeId);
        }
        request = getRequests().load(request.getRequestId());
        List<FcsAction> actions = flowControlManager.createActionList(request);
        for (int i = 0, j = actions.size(); i < j; i++) {
            RequestFcsAction action = (RequestFcsAction) actions.get(i);
            if (action.isSalesSelecting()) {
                try {
                    Employee userEmployee = (user instanceof DomainUser) ? ((DomainUser) user).getEmployee() : null;
                    if (userEmployee == null) {
                        userEmployee = (Employee) getEmployees().find(getEmployees().getEmployeeByUser(user.getId()));
                    }
                    flowControlManager.addFlowControl(userEmployee, request, action, new Date(), null, null, null, true);
                    request = getRequests().load(request.getRequestId());
                } catch (Throwable t) {
                    log.error("setSales: Failed on attempt to add corresponding fcs action: " + t.toString(), t);
                }
            }
        }
        updateRequestList(request, "updateSales");
        return load(request.getPrimaryKey());
    }

    private Employee fetchSales(Request request) {
        if (request == null || request.getSalesId() == null) {
            return null;
        }
        return getEmployees().get(request.getSalesId());
    }

    private void updateRequestList(Request request, String cause) {
        if (requestList != null) {
            try {
                requestList.send(request.getRequestId(), cause);
            } catch (Throwable t) {
                log.warn("updateRequestList() failed [message=" + t.getMessage() + "]");
            }
        }
    }

}
