/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 28, 2010 9:02:29 AM 
 * 
 */
package com.osserp.core.sales;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.osserp.core.Comparators;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesListItemSorter {
    public static final String[] COMPARATORS = {
            "byBranch",
            "byBranchReverse",
            "byCapacity",
            "byCapacityReverse",
            "byCreated",
            "byCreatedReverse",
            "byId",
            "byIdReverse",
            "byManager",
            "byManagerReverse",
            "byName",
            "byNameReverse",
            "bySales",
            "bySalesReverse",
            "byStatus",
            "byStatusReverse",
            "byVerifyDate",
            "byVerifyDateReverse",
            "byConfirmationDate",
            "byConfirmationDateReverse",
            "byReleaseDate",
            "byReleaseDateReverse",
            "byDeliveryDate",
            "byDeliveryDateReverse",
            "byInstallationDate",
            "byInstallationDateReverse"
    };

    private Map<String, Comparator> comparators = null;

    private static SalesListItemSorter cinstance = null;

    @SuppressWarnings("unchecked")
    private SalesListItemSorter() {
        super();
        comparators = Collections.synchronizedMap(new HashMap<>());
        comparators.put("byBranch", createByBranchComparator(false));
        comparators.put("byBranchReverse", createByBranchComparator(true));
        comparators.put("byCapacity", createByCapacityComparator(false));
        comparators.put("byCapacityReverse", createByCapacityComparator(true));
        comparators.put("byCreated", createByCreatedComparator(false));
        comparators.put("byCreatedReverse", createByCreatedComparator(true));
        comparators.put("byId", createByIdComparator(false));
        comparators.put("byIdReverse", createByIdComparator(true));
        comparators.put("byManager", createByManagerComparator(false));
        comparators.put("byManagerReverse", createByManagerComparator(true));
        comparators.put("byName", createByNameComparator(false));
        comparators.put("byNameReverse", createByNameComparator(true));
        comparators.put("bySales", createBySalesComparator(false));
        comparators.put("bySalesReverse", createBySalesComparator(true));
        comparators.put("byStatus", createByStatusComparator(false));
        comparators.put("byStatusReverse", createByStatusComparator(true));
        comparators.put("byVerifyDate", createByVerifyDateComparator(false));
        comparators.put("byVerifyDateReverse", createByVerifyDateComparator(true));
        comparators.put("byConfirmationDate", createByConfirmationDateComparator(false));
        comparators.put("byConfirmationDateReverse", createByConfirmationDateComparator(true));
        comparators.put("byReleaseDate", createByReleaseDateComparator(false));
        comparators.put("byReleaseDateReverse", createByReleaseDateComparator(true));
        comparators.put("byDeliveryDate", createByDeliveryDateComparator(false));
        comparators.put("byDeliveryDateReverse", createByDeliveryDateComparator(true));
        comparators.put("byInstallationDate", createByInstallationDateComparator(false));
        comparators.put("byInstallationDateReverse", createByInstallationDateComparator(true));
    }

    public static SalesListItemSorter instance() {
        if (cinstance == null) {
            cinstance = new SalesListItemSorter();
        }
        return cinstance;
    }

    @SuppressWarnings("unchecked")
    public void sort(String name, List<SalesListItem> items) {
        Comparator c = comparators.get(name);
        if (c == null) {
            c = comparators.get("byName");
        }
        Collections.sort(items, c);
    }

    private Comparator createByBranchComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesListItem) a).getBranchKey(), ((SalesListItem) b).getBranchKey(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesListItem) a).getBranchKey(), ((SalesListItem) b).getBranchKey(), false);
            }
        };
    }

    private Comparator createByCapacityComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((SalesListItem) a).getCapacity().compareTo(((SalesListItem) b).getCapacity());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((SalesListItem) a).getCapacity().compareTo(((SalesListItem) b).getCapacity());
            }
        };
    }

    private Comparator createByConfirmationDateComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    try {
                        return Comparators.compare(((SalesMonitoringItem) a).getConfirmationDate(), ((SalesMonitoringItem) b).getConfirmationDate(), true);
                    } catch (Exception e) {
                        return 0;
                    }
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                try {
                    return Comparators.compare(((SalesMonitoringItem) a).getConfirmationDate(), ((SalesMonitoringItem) b).getConfirmationDate(), false);
                } catch (Exception e) {
                    return 0;
                }
            }
        };
    }

    private Comparator createByCreatedComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesListItem) a).getCreated(), ((SalesListItem) b).getCreated(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesListItem) a).getCreated(), ((SalesListItem) b).getCreated(), false);
            }
        };
    }

    private Comparator createByDeliveryDateComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    try {
                        return Comparators.compare(((SalesMonitoringItem) a).getDeliveryDate(), ((SalesMonitoringItem) b).getDeliveryDate(), true);
                    } catch (Exception e) {
                        return 0;
                    }
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                try {
                    return Comparators.compare(((SalesMonitoringItem) a).getDeliveryDate(), ((SalesMonitoringItem) b).getDeliveryDate(), false);
                } catch (Exception e) {
                    return 0;
                }
            }
        };
    }

    private Comparator createByIdComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((SalesListItem) a).getId().compareTo(((SalesListItem) b).getId());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((SalesListItem) a).getId().compareTo(((SalesListItem) b).getId());
            }
        };
    }

    private Comparator createByInstallationDateComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    try {
                        return Comparators.compare(((SalesMonitoringItem) a).getInstallationDate(), ((SalesMonitoringItem) b).getInstallationDate(), true);
                    } catch (Exception e) {
                        return 0;
                    }
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                try {
                    return Comparators.compare(((SalesMonitoringItem) a).getInstallationDate(), ((SalesMonitoringItem) b).getInstallationDate(), false);
                } catch (Exception e) {
                    return 0;
                }
            }
        };
    }

    private Comparator createByManagerComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesListItem) a).getManagerKey(), ((SalesListItem) b).getManagerKey(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesListItem) a).getManagerKey(), ((SalesListItem) b).getManagerKey(), false);
            }
        };
    }

    private Comparator createByNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesListItem) a).getName(), ((SalesListItem) b).getName(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesListItem) a).getName(), ((SalesListItem) b).getName(), false);
            }
        };
    }

    private Comparator createByReleaseDateComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    try {
                        return Comparators.compare(((SalesMonitoringItem) a).getReleaseDate(), ((SalesMonitoringItem) b).getReleaseDate(), true);
                    } catch (Exception e) {
                        return 0;
                    }
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                try {
                    return Comparators.compare(((SalesMonitoringItem) a).getReleaseDate(), ((SalesMonitoringItem) b).getReleaseDate(), false);
                } catch (Exception e) {
                    return 0;
                }
            }
        };
    }

    private Comparator createBySalesComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesListItem) a).getSalesKey(), ((SalesListItem) b).getSalesKey(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesListItem) a).getSalesKey(), ((SalesListItem) b).getSalesKey(), false);
            }
        };
    }

    private Comparator createByStatusComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((SalesListItem) a).getStatus(), ((SalesListItem) b).getStatus(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((SalesListItem) a).getStatus(), ((SalesListItem) b).getStatus(), false);
            }
        };
    }

    private Comparator createByVerifyDateComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    try {
                        return Comparators.compare(((SalesMonitoringItem) a).getVerifyDate(), ((SalesMonitoringItem) b).getVerifyDate(), true);
                    } catch (Exception e) {
                        return 0;
                    }
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                try {
                    return Comparators.compare(((SalesMonitoringItem) a).getVerifyDate(), ((SalesMonitoringItem) b).getVerifyDate(), false);
                } catch (Exception e) {
                    return 0;
                }
            }
        };
    }

}
