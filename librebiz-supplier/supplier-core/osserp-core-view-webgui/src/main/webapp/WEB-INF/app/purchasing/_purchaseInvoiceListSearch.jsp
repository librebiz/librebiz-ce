<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.purchaseInvoiceListSearchView}" />

<v:form url="${view.baseLink}/save" name="purchaseInvoiceListSearchForm">

<div class="row row-modal">
    <div class="col-md-6 panel-area panel-area-modal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="limitStockReceipt" />
                </h4>
            </div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name"><fmt:message key="name" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="listSearchValue" styleClass="form-control" title="searchFor" />
                    </div>
                </div>
            </div>
            
            <v:date name="listStartDate" label="of" picker="true" styleClass="form-control" dateCol="4" />
            
            <v:date name="listStopDate" label="til" picker="true" styleClass="form-control" dateCol="4" />

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/resetSearchParams" value="reset" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" value="search" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</v:form>
