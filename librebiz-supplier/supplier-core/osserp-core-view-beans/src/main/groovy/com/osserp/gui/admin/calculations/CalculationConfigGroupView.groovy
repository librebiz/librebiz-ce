/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 29.04.2011 09:32:11 
 * 
 */
package com.osserp.gui.admin.calculations

import com.osserp.common.ErrorCode

import com.osserp.core.calc.CalculationConfig
import com.osserp.core.calc.CalculationConfigManager

import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
public class CalculationConfigGroupView extends CoreView {

    List configs = []
    CalculationConfig config = null

    CalculationConfigGroupView() {
        enablePopupView()
        dependencies = ['calculationConfigView']
        permissions = 'executive,calc_config'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!env.calculationConfigView || !env.calculationConfigView?.bean) {
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
        }
        configs = env.calculationConfigView.list
        config = env.calculationConfigView.bean

        Long groupId = form.getLong('id')
        if (groupId) {
            bean = config.getGroup(groupId)
            headerName = 'updateGroupLabel'
        } else {
            headerName = 'createNewGroup'
        }
    }

    void enableAssignMode() {
        env.assingMode = true
        headerName = 'assignPartlistConfig'
    }

    boolean isAssignMode() {
        env.assingMode
    }

    List getConfigs() {
        List result = []
        if (assignMode) {
            configs.each {
                if (!it.type?.supportsPartlists) {
                    result << it
                }
            }
        } else {
            result = configs
        }
        result
    }

    @Override
    void save() {
        if (assignMode) {
            Long id = form.getLong('configId')
            CalculationConfig partList = configs.find { it.id == id }
            calculationConfigManager.setPartList(config, bean.id, partList)
        } else {
            if (bean) {
                calculationConfigManager.updateGroup(config, bean, form.getString('name'), form.getBoolean('discounts'), form.getBoolean('option'))
            } else {
                calculationConfigManager.addGroup(config, form.getString('name'), form.getBoolean('discounts'), form.getBoolean('option'))
            }
        }
        env.calculationConfigView.reload()
    }

    private CalculationConfigManager getCalculationConfigManager() {
        getService(CalculationConfigManager.class.getName())
    }
}
