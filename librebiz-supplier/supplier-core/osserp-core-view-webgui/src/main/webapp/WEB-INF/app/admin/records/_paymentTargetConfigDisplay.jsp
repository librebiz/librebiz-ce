<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.name}"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="paymentTarget"><fmt:message key="paymentTarget" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.paid}"><fmt:message key="paid"/> <fmt:message key="pointOfAccounting"/></c:when>
                            <c:otherwise><o:out value="${view.bean.paymentTarget}"/> <fmt:message key="daysLabel"/></c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${view.bean.donation}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="donation"><fmt:message key="donation" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="donation">
                                    <fmt:message key="bigYes" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="discount"><fmt:message key="discount" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.withDiscount}"><fmt:message key="activated"/></c:when>
                            <c:otherwise><fmt:message key="deactivated"/></c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="discountOne"><fmt:message key="discountOne" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${view.bean.discount1}" format="currency"/> &percnt;
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="paymentTargetDiscountOne"><fmt:message key="paymentTargetDiscountOne" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.discount1Target}"/> <fmt:message key="daysLabel"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="discountTwo"><fmt:message key="discountTwo" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:number value="${view.bean.discount2}" format="currency"/> &percnt;
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="paymentTargetDiscountTwo"><fmt:message key="paymentTargetDiscountTwo" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.discount2Target}"/> <fmt:message key="daysLabel"/>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
