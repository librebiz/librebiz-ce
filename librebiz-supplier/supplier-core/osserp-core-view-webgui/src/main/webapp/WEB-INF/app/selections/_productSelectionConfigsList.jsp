<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="content-area" id="productSelectionConfigsListContent">
    <div class="row">
        <div class="col-lg-12 panel-area">
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><v:sortLink key="id"><fmt:message key="id" /></v:sortLink></th>
                            <th><v:sortLink key="name"><fmt:message key="name" /></v:sortLink></th>
                            <th><fmt:message key="description" /></th>
                            <o:permission role="product_selections_admin,calc_config,organisation_admin" info="permissionConfigureProductSelectionConfigs">
                                <th class="center"><fmt:message key="action" /></th>
                            </o:permission>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="selection" items="${view.list}">
                            <tr>
                                <td><o:out value="${selection.id}" /></td>
                                <td<c:if test="${selection.disabled}"> class="unused"</c:if>>
                                    <c:choose>
                                        <c:when test="${!empty view.selectionTarget}">
                                            <a href="<c:url value="${view.url}${selection.id}"/>">
                                                <o:out value="${selection.name}" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${selection.name}" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td><o:out value="${selection.description}" /></td>
                                <o:permission role="product_selections_admin,calc_config,organisation_admin" info="permissionConfigureProductSelectionConfigs">
                                    <td class="icon center">
                                        <v:link url="/selections/productSelectionConfigsSelection/enableEditMode" parameters="id=${selection.id}" title="editFilter">
                                            <o:img name="writeIcon" />
                                        </v:link>
                                    </td>
                                </o:permission>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
