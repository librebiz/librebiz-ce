<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="listdate"><fmt:message key="from" /></th>
                    <th class="listdate"><fmt:message key="til" /></th>
                    <th class="listname"><fmt:message key="description" /></th>
                    <th class="listsummary"><fmt:message key="time" /></th>
                    <th class="action"><fmt:message key="status" /></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty view.list}">
                        <tr>
                            <td colspan="5">
                                <span><fmt:message key="noDataAvailable" /></span> 
                                <span style="margin-left: 20px;"> 
                                    <v:link url="/projects/projectTracking/enableCreateMode">[<fmt:message key="create" />]</v:link>
                                </span>
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="obj" items="${view.list}">
                            <tr>
                                <td class="listdate"><o:date value="${obj.startDate}" /></td>
                                <td class="listdate"><o:date value="${obj.endDate}" /></td>
                                <td class="listname">
                                    <v:link url="/projects/projectTracking/select?id=${obj.id}">
                                        <o:out value="${obj.name}" />
                                    </v:link>
                                </td>
                                <td class="listsummary"><o:number value="${obj.totalTime}" format="currency" /></td>
                                <td class="action">
                                    <c:if test="${obj.closed}">
                                        <v:link url="/projects/projectTracking/print?id=${obj.id}">
                                            <o:img name="printIcon" styleClass="bigIcon" />
                                        </v:link>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
