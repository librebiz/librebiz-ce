/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 26, 2009 12:31:11 PM 
 * 
 */
package com.osserp.core.model.products;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.products.Product;
import com.osserp.core.products.ProductDescription;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class ProductDescriptionImpl extends AbstractOption implements ProductDescription {

    private String language = null;
    private String descriptionShort;
    private String marketingText;
    private String datasheetText = null;

    protected ProductDescriptionImpl() {
        super();
    }

    /**
     * Creates a new product description
     * @param user
     * @param product
     * @param language
     * @param name
     * @param description
     */
    public ProductDescriptionImpl(
            Long user,
            Product product,
            String language,
            String name,
            String description) {
        super((Long) null, product.getProductId(), name, description, (Date) null, user);
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDatasheetText() {
        return datasheetText;
    }

    public void setDatasheetText(String datasheetText) {
        this.datasheetText = datasheetText;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getMarketingText() {
        return marketingText;
    }

    public void setMarketingText(String marketingText) {
        this.marketingText = marketingText;
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElementFromString("<datasheetText>" + datasheetText + "</datasheetText>"));
        root.addContent(JDOMUtil.createElementFromString("<descriptionShort>" + descriptionShort + "</descriptionShort>"));
        root.addContent(JDOMUtil.createElementFromString("<marketingText>" + marketingText + "</marketingText>"));
        root.addContent(JDOMUtil.createElement("language", language));
        return root;
    }
}
