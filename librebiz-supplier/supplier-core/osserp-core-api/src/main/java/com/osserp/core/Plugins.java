/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class Plugins implements Serializable {

    private static final long serialVersionUID = 4L;

    private Map<String, Plugin> map = new HashMap<>();
    private Map<String, PluginHook> links = new HashMap<>();

    protected Plugins() {
        super();
    } 

    public Plugins(List<Plugin> activatedPlugins) {
        super();
        configure(activatedPlugins);
    } 

    protected void configure(List<Plugin> activatedPlugins) {
        map = new HashMap(activatedPlugins.size());
        links = new HashMap<>();
        activatedPlugins.forEach(plugin -> {
            map.put(plugin.getName(), plugin);
            plugin.getHooks().forEach((key, hook) -> {
                if (hook.isUrl() || hook.isPage()) {
                    links.put(key, hook);
                }
            });
        });
    }

    public Map<String, Plugin> getMap() {
        return map;
    }

    protected void setMap(Map<String, Plugin> pluginMap) {
        map = pluginMap;
    }

    public Plugin getPlugin(String name) {
        return map.get(name);
    }

    public Plugin getPlugin(PluginHook hook) {
        Plugin result = null;
        for (Iterator i = map.keySet().iterator(); i.hasNext();) {
            Plugin next = map.get((String) i.next());
            if (next.getId().equals(hook.getReference())) {
                result = next;
                break;
            }
        }
        return result;
    }

    public List<Plugin> getPluginsByProperty(String propertyName) {
        List<Plugin> result = new ArrayList<>();
        for (Iterator i = map.keySet().iterator(); i.hasNext();) {
            Plugin next = map.get((String) i.next());
            if (next.getProperties().containsKey(propertyName)) {
                result.add(next);
            }
        }
        return result;
    }

    public PluginHook getLink(String name) {
        if (links.containsKey(name)) {
            return links.get(name);
        }
        return null;
    }

    public Map<String, PluginHook> getLinks() {
        return links;
    }

    protected void setLinks(Map<String, PluginHook> links) {
        this.links = links;
    }

}
