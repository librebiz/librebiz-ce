--
-- Librebiz - Initial data import (required)
--
-- Copyright (C) 2017 Rainer Kirchner <rk@librebiz.org>  
--
-- This file is part of Librebiz ERP & CRM for Debian GNU/Linux
-- and most other operating systems.
--  
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU Affero General Public License (AGPL) 
-- version 3 as published by the Free Software Foundation. In accordance 
-- with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
-- the effect that the original authors expressly exclude the warranty of
-- non-infringement of any third-party rights. 
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
-- at http://www.gnu.org/licenses/agpl-3.0.html for more details.
--
-- The interactive user interfaces in modified source and object code 
-- versions must display Appropriate Legal Notices, as required under 
-- Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
-- the GNU AGPL you must include a clickable link "Powered by osserp.com"
-- that leads directly to the URL http://osserp.com in the footer area 
-- or, if not reasonably feasible for technical reason, as a top-level 
-- link of the primary navigation of the graphical user interface in
-- every copy of the program you distribute. 
--
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = osserp, pg_catalog;

UPDATE document_configs SET dms_root = '/var/lib/librebiz/dms' WHERE id = 1;
UPDATE document_configs SET dms_root = '/var/lib/librebiz/vmtpl' WHERE id = 2;

--UPDATE system_properties SET pvalue = 'Librebiz' WHERE name = 'appLabel';
UPDATE system_properties SET pvalue = 'http://librebiz.org' WHERE name = 'internetSite';
UPDATE system_properties SET pvalue = '/var/lib/librebiz/dms/attachments' WHERE name = 'attachmentPath';
UPDATE system_properties SET pvalue = '/var/lib/librebiz/dms/imports' WHERE name = 'contactImportPath';
UPDATE system_properties SET pvalue = '/var/lib/librebiz/dms/imports' WHERE name = 'productImportPath';
UPDATE system_properties SET displayable = false, is_admin = true, pvalue = 'true' WHERE name = 'documentFormatQuantityByDbms';
