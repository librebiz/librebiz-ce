<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-app" prefix="oa" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="recordDate"><fmt:message key="date"/></th>
                <th class="recordId"><fmt:message key="number"/></th>
                <th class="recordContact"><fmt:message key="customer"/></th>
                <th class="recordAmount"><fmt:message key="amount"/></th>
                <th class="recordTaxAmount"><fmt:message key="turnoverTax"/></th>
                <th class="recordTaxAmount reducedTax"><fmt:message key="reducedTurnoverTax"/></th>
                <th class="recordAmount"><fmt:message key="gross"/></th>
                <th class="action"> </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="record" items="${view.selectedRecords}" varStatus="s">
                <c:choose>
                    <c:when test="${record.type.payment}">
                        <tr>
                            <td class="recordDate"><o:date value="${record.taxPointDate}"/></td>
                            <td class="recordId">
                                <oa:recordLink record="${record}" exit="taxReport" viewExit="/accounting/taxReport/reload">
                                    <o:out value="${record.referenceNumber}"/>
                                </oa:recordLink>
                            </td>
                            <td class="recordContact" title="<o:out value="${record.contact.contactId}"/>"><o:out value="${record.contact.displayName}"/></td>
                            <td class="recordAmount"> </td>
                            <td class="recordTaxAmount"> </td>
                            <td class="recordTaxAmount reducedTax"> </td>
                            <td class="recordAmount"><o:number value="${record.amount}" format="currency"/></td>
                            <td class="action">
                                <%-- add document link --%>
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td class="recordDate"><o:date value="${record.taxPointDate}"/></td>
                            <td class="recordId">
                                <oa:recordLink record="${record}" exit="taxReport">
                                    <o:out value="${record.number}"/>
                                </oa:recordLink>
                            </td>
                            <td class="recordContact" title="<o:out value="${record.contact.contactId}"/>"><o:out value="${record.contact.displayName}"/></td>
                            <td class="recordAmount"><o:number value="${record.amounts.amount}" format="currency"/></td>
                            <td class="recordTaxAmount"><o:number value="${record.amounts.taxAmount}" format="currency"/></td>
                            <td class="recordTaxAmount reducedTax"><o:number value="${record.amounts.reducedTaxAmount}" format="currency"/></td>
                            <td class="recordAmount"><o:number value="${record.amounts.grossAmount}" format="currency"/></td>
                            <td class="action">
                                <%-- add document link --%>
                            </td>
                        </tr>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </tbody>
    </table>
</div>
