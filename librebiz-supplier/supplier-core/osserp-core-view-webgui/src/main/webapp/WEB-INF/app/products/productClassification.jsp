<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productClassificationView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <c:choose>
            <c:when test="${empty view.bean}">
                <fmt:message key="productClassification" />
            </c:when>
            <c:otherwise>
                <o:out value="${view.bean.productId}" /> - <o:out value="${view.bean.name}" />
            </c:otherwise>
        </c:choose>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">

        <div class="content-area" id="productClassificationContent">
            <div class="row">

                <div class="col-md-4 panel-area panel-area-default">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">

                                <div class="col-md-10">
                                    <h4>
                                        <fmt:message key="productType" />
                                    </h4>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-1"></div>
                            </div>

                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tbody>
                                    <c:if test="${!empty view.selectedType}">
                                        <tr class="altrow">
                                            <td class="boldtext"><o:out value="${view.selectedType.name}" /></td>
                                            <td class="action"></td>
                                        </tr>
                                    </c:if>
                                    <c:forEach var="type" items="${view.typeSelections}">
                                        <c:if test="${type.id != view.selectedType.id}">
                                            <tr>
                                                <td><v:link url="/products/productClassification/selectType?id=${type.id}">
                                                        <o:out value="${type.name}" />
                                                    </v:link></td>
                                                <td class="action"><o:contains list="${view.bean.types}" value="${type.id}">
                                                        <v:link url="/products/productClassification/removeType?id=${type.id}">
                                                            <img src="<c:url value="${applicationScope.smallCancelIcon}"/>" class="bigicon" />
                                                        </v:link>
                                                    </o:contains></td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="col-md-4 panel-area panel-area-default">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">

                                <div class="col-md-10">
                                    <h4>
                                        <fmt:message key="productGroup" />
                                    </h4>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-1"></div>
                            </div>

                        </div>
                    </div>
                    <div class="panel-body">
                        <c:if test="${!empty view.selectedType}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tbody>
                                        <c:choose>
                                            <c:when test="${empty view.selectedType.groups}">
                                                <tr>
                                                    <td colspan="2"><fmt:message key="noGroupsDefined" /></td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <c:if test="${!empty view.selectedGroup}">
                                                    <tr class="altrow">
                                                        <td class="boldtext"><o:out value="${view.selectedGroup.name}" /></td>
                                                        <td class="action"></td>
                                                    </tr>
                                                </c:if>
                                                <c:forEach var="group" items="${view.selectedType.groups}">
                                                    <c:if test="${group.id != view.selectedGroup.id}">
                                                        <tr>
                                                            <td><v:link url="/products/productClassification/selectGroup?id=${group.id}">
                                                                    <o:out value="${group.name}" />
                                                                </v:link></td>
                                                            <td class="action"></td>
                                                        </tr>
                                                    </c:if>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </c:if>

                    </div>
                </div>

                <div class="col-md-4 panel-area panel-area-default">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">

                                <div class="col-md-10">
                                    <h4>
                                        <fmt:message key="category" />
                                    </h4>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-1"></div>
                            </div>

                        </div>
                    </div>
                    <div class="panel-body">

                        <c:if test="${!empty view.selectedGroup}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tbody>
                                        <c:choose>
                                            <c:when test="${empty view.selectedGroup.categories}">
                                                <tr class="altrow">
                                                    <td><fmt:message key="noCategoriesDefined" /></td>
                                                    <td class="action"><v:link url="/products/productClassification/save">
                                                            <o:img name="saveIcon" styleClass="bigicon" />
                                                        </v:link></td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <c:if test="${!empty view.selectedCategory}">
                                                    <tr class="altrow">
                                                        <td class="boldtext"><o:out value="${view.selectedCategory.name}" /></td>
                                                        <td class="action"></td>
                                                    </tr>
                                                </c:if>
                                                <c:forEach var="category" items="${view.selectedGroup.categories}">
                                                    <c:if test="${category.id != view.selectedCategory.id}">
                                                        <tr>
                                                            <td><v:link url="/products/productClassification/selectCategory?id=${category.id}">
                                                                    <o:out value="${category.name}" />
                                                                </v:link></td>
                                                            <td></td>
                                                        </tr>
                                                    </c:if>
                                                </c:forEach>
                                                <tr class="altrow">
                                                    <td><fmt:message key="selectSelected" /></td>
                                                    <td class="action"><v:link url="/products/productClassification/save">
                                                            <o:img name="saveIcon" styleClass="bigicon" />
                                                        </v:link></td>
                                                </tr>
                                            </c:otherwise>
                                        </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </c:if>

                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
<o:removeErrors />
