/**
 *
 * Copyright (C) 2018 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 30, 2018
 * 
 */
package com.osserp.gui.admin.system

import com.osserp.common.Option

import com.osserp.core.system.TextResourceManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreAdminView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class TextResourceEditorView extends CoreAdminView {

    Locale locale

    TextResourceEditorView() {
        super()
        headerName = 'labelAndTextConfig'
        permissions = 'client_admin,i18n_admin,organisation_admin,runtime_config,executive_saas'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            locale = user?.getLocale()
            if (!locale || !user) {
                throw new ViewContextException()
            }
            loadResources()
        }
    }

    @Override
    void save() {
        String resourceKey = getString('resourceKey')
        String value = getString('name')
        list = textResourceManager.update(locale, resourceKey, value)
        portalView?.reloadMenu()
        disableEditMode()
    }

    @Override
    void disableEditMode() {
        super.disableEditMode()
        bean = null
    }

    @Override
    void select() {
        String resourceKey = getString('resourceKey')
        if (!resourceKey) {
            disableEditMode()
        } else {
            for (int i = 0; i < list.size(); i++) {
                Option opt = list.get(i)
                if (opt.resourceKey == resourceKey) {
                    bean = opt
                    enableEditMode()
                    break
                }
            }
        }
    }

    protected void loadResources() {
        list = textResourceManager.getResources(locale)
    }

    protected TextResourceManager getTextResourceManager() {
        getService(TextResourceManager.class.getName())
    }
}
