/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 29, 2007 10:14:59 AM 
 * 
 */
package com.osserp.core.service.fcs.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.BusinessCase;
import com.osserp.core.FcsAction;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.EventConfigs;
import com.osserp.core.dao.Events;
import com.osserp.core.dao.FcsActions;
import com.osserp.core.dao.FcsWastebaskets;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.employees.Employee;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventTermination;
import com.osserp.core.events.EventTicket;
import com.osserp.core.service.events.AbstractEventTicketCreator;
import com.osserp.core.service.fcs.FcsEventCreator;
import com.osserp.core.tasks.EventTicketSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFcsEventCreator extends AbstractEventTicketCreator
        implements FcsEventCreator {

    private static Logger log = LoggerFactory.getLogger(AbstractFcsEventCreator.class.getName());
    private static final String IGNORE_EVENTS_ON_FCS_CANCEL = "eventsIgnoreOnCanceledFCS"; 
    private SystemConfigs systemConfigs = null;
    private FcsActions actions = null;
    private FcsWastebaskets baskets = null;
    private Long eventConfigTypeId;

    protected AbstractFcsEventCreator(
            SystemConfigs systemConfigs,
            Events events,
            EventConfigs eventConfigs,
            FcsActions actions,
            FcsWastebaskets baskets,
            Employees employees,
            EventTicketSender eventTicketSender,
            Long eventConfigTypeId) {

        super(events, eventConfigs, employees, eventTicketSender);
        this.systemConfigs = systemConfigs;
        this.actions = actions;
        this.baskets = baskets;
        this.eventConfigTypeId = eventConfigTypeId;
    }

    public void closeAllEvents(Employee employee, BusinessCase bc) {
        closeAllEvents(employee.getId(), bc.getPrimaryKey());
    }

    public final void createEventTickets(
            BusinessCase bc,
            FcsAction action,
            Date actionDate,
            Long createdBy,
            Long saleId,
            String message,
            String headline,
            boolean canceled) {

        
        boolean ignore = systemConfigs.isSystemPropertyEnabled(IGNORE_EVENTS_ON_FCS_CANCEL);
        if ((canceled && !ignore) || action.isStopping()) {
            closeAllEvents(createdBy, bc.getPrimaryKey());
        }
        List<FcsAction> todos = actions.createActionList(bc);
        Set<Long> availableTodos = new HashSet<Long>();
        for (int i = 0, j = todos.size(); i < j; i++) {
            FcsAction fca = todos.get(i);
            availableTodos.add(fca.getId());
            //if (log.isDebugEnabled()) {
            //    log.debug("executeActions() adding action [" + fca.getId()
            //            + "] to available todos");
            //}
        }
        if (log.isDebugEnabled()) {
            log.debug("executeActions() fetched total of [" + availableTodos.size()
                    + "] available todos...");
        }
        List<EventAction> events = findActionsByCaller(action.getEventConfigId(), action.getId());
        if (log.isDebugEnabled() && !events.isEmpty()) {
            log.debug("executeActions() found events [count=" + events.size() + "]");
        }
        for (int i = 0, j = events.size(); i < j; i++) {
            EventAction event = events.get(i);
            if ((isTerminateableByFcs(event, availableTodos)
                    || isTerminateableByWastebasket(event, bc)
                    || event.isCloseableByTarget())
                    && !event.isPaused()) {
                createTicket(
                        event,
                        bc.getPrimaryKey(),
                        bc.getName(),
                        bc.getBranch() == null ? null : bc.getBranch().getId(),
                        bc.getManagerId(),
                        bc.getSalesId(),
                        createdBy,
                        message,
                        headline,
                        canceled);
            } else if (log.isDebugEnabled()) {
                log.debug("executeActions() ignoring not terminateable [action="
                        + action.getId() + ", name=" + action.getName() 
                        + ", paused=" + event.isPaused() + "]");
            }
        }
        if (events.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("executeActions() no events found, checking terminations...");
            }
            List<EventTermination> terminations = findTerminationsByCaller(action.getEventConfigId(), action.getId());
            if (!terminations.isEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("executeActions() found terminations [count=" + terminations.size() + "]");
                }
                createTerminationOnlyTicket(action, bc, createdBy, canceled);
            }
        }
    }

    private void createTerminationOnlyTicket(
            FcsAction action,
            BusinessCase bc,
            Long createdBy,
            boolean canceled) {

        Events events = getEvents();
        EventTicket eticket = events.createTicket(
                action.getId(),
                eventConfigTypeId,
                createdBy,
                bc.getPrimaryKey(),
                canceled);
        create(eticket);
        if (log.isDebugEnabled()) {
            log.debug("createTerminationOnlyTicket() done [caller=" + action.getId()
                    + ", reference=" + bc.getPrimaryKey() + "]");
        }
    }

    /**
     * Checks that given action event is terminatable by an fcs todo
     * @param action
     * @param availableTodos
     * @return true if so
     */
    private boolean isTerminateableByFcs(EventAction action, Set<Long> availableTodos) {
        int closingCount = (action.getClosings() == null ? 0 : action.getClosings().size());
        if (log.isDebugEnabled()) {
            StringBuffer buffer = new StringBuffer("isTerminateableByFcs() action ");
            buffer
                    .append(action.getId())
                    .append(" has ")
                    .append(closingCount)
                    .append(" closings: ");
            if (closingCount == 0) {
                if (action.getClosings() == null) {
                    buffer.append("closing list was null!");
                }
            } else {
                for (int i = 0, j = closingCount; i < j; i++) {
                    EventTermination next = action.getClosings().get(i);
                    buffer
                            .append("\nfound closing ")
                            .append(next.getId())
                            .append(", caller id ")
                            .append(next.getCallerId())
                            .append(", config id ")
                            .append(next.getConfigId());
                }
                buffer.append("\n");
            }
            log.debug(buffer.toString());
        }
        for (int i = 0, j = action.getClosings().size(); i < j; i++) {
            EventTermination termination = action.getClosings().get(i);
            if (availableTodos.contains(termination.getCallerId())) {
                if (log.isDebugEnabled()) {
                    log.debug("isTerminateableByFcs() reports true, found "
                            + termination.getCallerId());
                }
                return true;
            } else if (log.isDebugEnabled()) {
                log.debug("isTerminateableByFcs() ignoring termination "
                        + termination.getCallerId());
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("isTerminateableByFcs() reports false");
        }
        return false;
    }

    /**
     * Checks that given action event is terminateable by an fcs todo
     * @param action
     * @param bc
     * @return true if so
     */
    private boolean isTerminateableByWastebasket(EventAction action, BusinessCase bc) {
        Set<Long> wasted = baskets.getWaste(bc.getPrimaryKey());
        boolean result = false;
        for (int i = 0, j = action.getClosings().size(); i < j; i++) {
            EventTermination termination = action.getClosings().get(i);
            if (wasted.contains(termination.getCallerId())) {
                result = true;
                break;
            }
        }
        return result;
    }
}
