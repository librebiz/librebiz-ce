/**
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2012 11:45:06 AM
 * 
 */
package com.osserp.core.directory;

import java.util.List;
import java.util.Map;

import com.osserp.common.directory.DirectoryGroup;
import com.osserp.common.directory.DirectoryPermission;
import com.osserp.common.directory.DirectoryUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DomainDirectoryManager {

    /**
     * Finds all directory users
     * @return users or empty list
     */
    List<DirectoryUser> findUsers();

    /**
     * Finds a user by uid
     * @param uid
     * @return user or null if not exists
     */
    DirectoryUser findUser(String uid);

    /**
     * Updates directory user
     * @param user
     * @param values
     * @return updated directory user
     */
    DirectoryUser updateUser(DirectoryUser user, Map values);

    /**
     * Sets new password
     * @param user
     * @param password
     * @return updated directory user
     */
    DirectoryUser updateUserPassword(DirectoryUser user, String password);

    /**
     * Finds all available groups
     * @return groups
     */
    List<DirectoryGroup> findGroups();

    /**
     * Finds users groups
     * @param user
     * @return groups
     */
    List<DirectoryGroup> findGroups(DirectoryUser user);

    /**
     * Updates directory group
     * @param group
     * @param values
     * @return updated directory group
     */
    DirectoryGroup updateGroup(DirectoryGroup group, Map values);

    /**
     * Finds all available permissions
     * @return permissions
     */
    List<DirectoryPermission> findPermissions();

    /**
     * Finds users permissions
     * @param user
     * @return permissions
     */
    List<DirectoryPermission> findPermissions(DirectoryUser user);

    /**
     * Updates directory permission
     * @param permission
     * @param values
     * @return updated directory permission
     */
    DirectoryPermission updatePermission(DirectoryPermission permission, Map values);

}
