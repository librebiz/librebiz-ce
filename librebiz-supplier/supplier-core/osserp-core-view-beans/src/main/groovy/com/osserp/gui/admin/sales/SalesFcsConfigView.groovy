/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 02.05.2011 11:29:08 
 * 
 */
package com.osserp.gui.admin.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.BusinessType
import com.osserp.core.FcsAction
import com.osserp.core.FcsActionManager
import com.osserp.core.projects.ProjectFcsActionManager

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.admin.fcs.FlowControlConfigView

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SalesFcsConfigView extends FlowControlConfigView {
    private static Logger logger = LoggerFactory.getLogger(SalesFcsConfigView.class.getName())
    
    SalesFcsConfigView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
    }
    
    protected List<BusinessType> fetchBusinessTypes() {
        businessTypes = businessTypeManager.findActive().findAll { BusinessType bt ->
            !bt.interestContext
        }
    }

    @Override
    void select() {
        super.select()
        employeeGroup = null
        if (bean) {
            employeeGroup = employeeGroups.find { it.id == bean.groupId }
        }
    }

    void reload() {
        if (selectedType) {
            list = projectFcsActionManager.findByType(selectedType.id)
            actualSortKey = 'orderId'
        }
        employeeGroup = null
        if (bean) {
            bean = list.find { it.id == bean.id }
            employeeGroup = employeeGroups.find { it.id == bean.groupId }
        }
    }
    
    void selectBusinessType() {
        super.selectBusinessType()
        if (selectedType) {
            list = projectFcsActionManager.findByType(selectedType.id)
            actualSortKey = 'orderId'
        } else {
            list = []
        }
        disableComparisonMode()
    }

    void compareBusinessType() {
        Long id = getLong('id') 
        if (id) {
            otherType = businessTypeManager.load(id)
            otherActions = !otherType ? [] : projectFcsActionManager.findByType(id)
            if (!otherActions) {
                throw new ClientException(ErrorCode.FLOW_CONTROL_CONFIG_EMPTY)
            }
            actualSortKey = 'orderId'
            comparisonMode = true
            createCustomNavigation()
        } else {
            disableComparisonMode()
        }
    }
    
    void disableComparisonMode() {
        otherType = null
        otherActions = []
        comparisonMode = false
        createCustomNavigation()
    }

    boolean isEolDisplay() {
        env.eolDisplay
    }

    void enableEolDisplay() {
        env.eolDisplay = true
        createCustomNavigation()
    }

    void disableEolDisplay() {
        env.eolDisplay = false
        createCustomNavigation()
    }

    void changeEolFlag() {
        Long id = form.getLong('id')
        def action = list.find { it.id == id }
        if (action) {
            list = projectFcsActionManager.changeEolFlag(action)
        }
    }

    void save() {

        String name = form.getString('name')
        String description = form.getString('description')
        Long groupId = form.getLong('groupId')
        boolean displayEverytime = form.getBoolean('displayEverytime')

        if (isCreateMode()) {

            list = projectFcsActionManager.createAction(
                selectedType.id, name, description, groupId, displayEverytime)
            disableCreateMode()
        } else {
            FcsAction action = bean
            if (!name) {
                throw new ClientException(ErrorCode.NAME_MISSING)
            }
            if (action?.status && displayEverytime) {
                throw new ClientException(ErrorCode.FLOW_CONTROL_DISPLAY_EVERYTIME)
            }
            action.status = getLong('status')
            action.name = name
            action.description = description
            action.groupId = groupId
            action.displayEverytime = displayEverytime
            action.enablingDelivery = form.getBoolean('enablingDelivery')
            action.closingDelivery = form.getBoolean('closingDelivery')
            projectFcsActionManager.update(action)

            disableEditMode()
        }
        reload()
    }

    void saveOrder() {
        Long[] ids = form.getIndexedLong('id')
        Integer[] orderIds = form.getIndexedIntegers('orderId')
        projectFcsActionManager.updateOrder(selectedType.id, ids, orderIds)
        disableOrderEditMode()
        reload()
    }

    void enableOrderEditMode() {
        env.orderEditMode = true
        createCustomNavigation()
    }

    void disableOrderEditMode() {
        env.orderEditMode = false
    }

    boolean isOrderEditMode() {
        env.orderEditMode
    }

    void moveUp() {
        move(true)
    }

    void moveDown() {
        move(false)
    }

    private void move(boolean up) {
        Long id = form.getLong('id')
        FcsAction action = list.find { FcsAction action -> action.id == id }
        if (!action?.endOfLife) {
            list = projectFcsActionManager.changeListPosition(action, up)
        }
    }
    
    @Override
    void setList(List list) {
        super.setList(list)
        eolCount = 0
        activatedCount = 0
        list.each { FcsAction action ->
            activatedCount++ 
            if (action.endOfLife) {
                eolCount++
            }
        }
        logger.debug("setList: done [count=${activatedCount}, eol=${eolCount}]")
    } 
    
    @Override
    void createCustomNavigation() {
        if (orderEditMode) {
            nav.currentNavigation = [exitOrderEditLink, homeLink]
        } else {
            nav.listNavigation = [
                comparisonMode ? exitComparisonModeLink : exitSelectedBusinessTypeLink,
                businessTypeSelectionLink,
                createLink,
                businessTypeCompareLink,
                eolDisplayFlagLink,
                homeLink
                ]
            nav.defaultNavigation = [exitLink, homeLink]
        }
    }

    MenuItem getExitOrderEditLink() {
        navigationLink("/${context.name}/disableOrderEditMode", 'backIcon', 'cancelEditing')
    }
    
    MenuItem getExitComparisonModeLink() {
        navigationLink("/${context.name}/disableComparisonMode", 'backIcon', 'backToLast')
    }

    MenuItem getExitSelectedBusinessTypeLink() {
        navigationLink("/${context.name}/selectBusinessType", 'backIcon', 'backToLast')
    }

    private MenuItem getEolDisplayFlagLink() {
        if (isEolDisplay()) {
            return navigationLink("/${context.name}/disableEolDisplay", 'showDetailsIcon', 'disableEolDisplay')
        }
        navigationLink("/${context.name}/enableEolDisplay", 'hideDetailsIcon', 'enableEolDisplay')
    }
    
    MenuItem getBusinessTypeSelectionLink() {
        new MenuItem(
                link: "${context.fullPath}/selections/businessTypeSelection/forward?selectionTarget=/admin/sales/salesFcsConfig/selectBusinessType&context=sales&exclude=${selectedType?.id}",
                icon: 'searchIcon',
                title: 'businessTypeSelection',
                ajaxPopup: true,
                ajaxPopupName: 'businessTypeSelectionView')
    }

    MenuItem getBusinessTypeCompareLink() {
        new MenuItem(
                link: "${context.fullPath}/selections/businessTypeSelection/forward?selectionTarget=/admin/sales/salesFcsConfig/compareBusinessType&context=sales&exclude=${selectedType?.id}",
                icon: 'tableIcon',
                title: 'compareWithOtherType',
                ajaxPopup: true,
                ajaxPopupName: 'businessTypeSelectionView')
    }
    
    @Override
    protected FcsActionManager getFcsActionManager() {
        projectFcsActionManager
    }
    
    private ProjectFcsActionManager getProjectFcsActionManager() {
        getService(ProjectFcsActionManager.class.getName())
    }
}
