/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 9, 2009 8:37:27 AM 
 * 
 */
package com.osserp.common.beans;

import java.util.HashMap;
import java.util.Map;

import com.osserp.common.ChartConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ChartConfigImpl implements ChartConfig {

    private static float DEFAULT_QUALITY = 1.0f;
    private static int DEFAULT_WIDTH = 800;
    private static int DEFAULT_HEIGHT = 600;
    private float quality = DEFAULT_QUALITY;
    private int width = DEFAULT_WIDTH;
    private int height = DEFAULT_HEIGHT;

    private Map<String, Boolean> additionalOptions = new HashMap<String, Boolean>();
    private Object data = null;
    private String rendererServiceName = null;

    protected ChartConfigImpl() {
        super();
    }

    public ChartConfigImpl(Object data, String rendererName) {
        super();
        this.data = data;
        this.rendererServiceName = rendererName;
    }

    public ChartConfigImpl(Object data, String rendererName, float quality, int width, int height) {
        super();
        this.data = data;
        this.rendererServiceName = rendererName;
        this.quality = quality;
        this.width = width;
        this.height = height;
    }

    public ChartConfigImpl(Object data, String rendererName, float quality, int width, int height, boolean showTitle) {
        super();
        this.data = data;
        this.rendererServiceName = rendererName;
        this.quality = quality;
        this.width = width;
        this.height = height;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public float getQuality() {
        return quality;
    }

    public void setQuality(float quality) {
        this.quality = quality;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getRendererServiceName() {
        return rendererServiceName;
    }

    public void setRendererServiceName(String rendererServiceName) {
        this.rendererServiceName = rendererServiceName;
    }

    public boolean getAdditionalOption(String name) {
        return additionalOptions.get(name) == null ? false : additionalOptions.get(name).booleanValue();
    }

    public void addAdditionalOption(String name, boolean value) {
        additionalOptions.put(name, Boolean.valueOf(value));
    }
}
