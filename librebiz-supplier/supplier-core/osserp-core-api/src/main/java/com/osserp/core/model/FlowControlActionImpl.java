/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 28-Oct-2006 08:48:16 
 * 
 */
package com.osserp.core.model;

import org.jdom2.Element;

import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.Dependency;
import com.osserp.core.events.EventConfigType;
import com.osserp.core.projects.FlowControlAction;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class FlowControlActionImpl extends AbstractFcsAction implements FlowControlAction {

    private Long typeId = null;
    private int orderId;

    private boolean startingUp = false;
    private boolean managerSelecting = false;
    private boolean initializing = false;

    private boolean confirming = false;
    private boolean releaseing = false;
    private boolean verifying = false;
    private boolean downpaymentRequest = false;
    private boolean downpaymentResponse = false;
    private boolean deliveryInvoiceRequest = false;
    private boolean deliveryInvoiceResponse = false;
    private boolean invoiceRequest = false;
    private boolean invoiceResponse = false;
    private boolean enablingDelivery = false;
    private boolean partialDelivery = false;
    private boolean closingDelivery = false;
    private boolean closingService = false;
    private boolean partialPayment = false;

    /**
     * Default constructor to implement serializable interface
     */
    protected FlowControlActionImpl() {
        super();
        setEventConfigId(EventConfigType.SALES_FCS_EVENT);
    }

    /**
     * Creates a new flow control action
     * @param typeId
     * @param name
     * @param description
     * @param groupId
     * @param displayEverytime
     * @param orderId
     */
    public FlowControlActionImpl(
            Long typeId,
            String name,
            String description,
            Long groupId,
            boolean displayEverytime,
            int orderId) {
        super(
                name, 
                groupId, 
                description, 
                EventConfigType.SALES_FCS_EVENT, 
                displayEverytime);
        this.typeId = typeId;
        this.orderId = orderId;
    }

    /**
     * Creates a new flow control action by another action
     * @param type
     * @param source
     */
    public FlowControlActionImpl(BusinessType type, FlowControlAction source) {
        super(
                source.getName(),
                source.getGroupId(),
                source.getDescription(),
                source.getStatus(),
                source.getEventConfigId(),
                source.isDisplayEverytime(),
                source.isNoteRequired(),
                source.isPerformAction(),
                source.isPerformOnStart(),
                source.getStartActionName(),
                source.isPerformAtEnd(),
                source.getEndActionName(),
                source.isThrowable(),
                source.isCancelling(),
                source.isStopping(),
                source.isDisplayReverse(),
                source.isEndOfLife(),
                source.isTriggered());

        this.typeId = type.getId();
        this.orderId = source.getOrderId();

        this.startingUp = source.isStartingUp();
        this.managerSelecting = source.isManagerSelecting();
        this.initializing = source.isInitializing();

        this.confirming = source.isConfirming();
        this.releaseing = source.isReleaseing();
        this.verifying = source.isVerifying();
        this.downpaymentRequest = source.isDownpaymentRequest();
        this.downpaymentResponse = source.isDownpaymentResponse();
        this.deliveryInvoiceRequest = source.isDeliveryInvoiceRequest();
        this.deliveryInvoiceResponse = source.isDeliveryInvoiceResponse();
        this.invoiceRequest = source.isInvoiceRequest();
        this.invoiceResponse = source.isInvoiceResponse();
        this.enablingDelivery = source.isEnablingDelivery();
        this.partialDelivery = source.isPartialDelivery();
        this.closingDelivery = source.isClosingDelivery();
        this.partialPayment = source.isPartialPayment();
        this.closingService = source.isClosingService();
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public boolean isStartingUp() {
        return startingUp;
    }

    public void setStartingUp(boolean startingUp) {
        this.startingUp = startingUp;
    }

    public boolean isManagerSelecting() {
        return managerSelecting;
    }

    public void setManagerSelecting(boolean managerSelecting) {
        this.managerSelecting = managerSelecting;
    }

    public boolean isInitializing() {
        return initializing;
    }

    protected void setInitializing(boolean initializing) {
        this.initializing = initializing;
    }

    public boolean isConfirming() {
        return confirming;
    }

    protected void setConfirming(boolean confirming) {
        this.confirming = confirming;
    }

    public boolean isVerifying() {
        return verifying;
    }

    protected void setVerifying(boolean verifying) {
        this.verifying = verifying;
    }

    public boolean isReleaseing() {
        return releaseing;
    }

    protected void setReleaseing(boolean releaseing) {
        this.releaseing = releaseing;
    }

    public boolean isDownpaymentRequest() {
        return downpaymentRequest;
    }

    protected void setDownpaymentRequest(boolean downpaymentRequest) {
        this.downpaymentRequest = downpaymentRequest;
    }

    public boolean isDownpaymentResponse() {
        return downpaymentResponse;
    }

    protected void setDownpaymentResponse(boolean downpaymentResponse) {
        this.downpaymentResponse = downpaymentResponse;
    }

    public boolean isDeliveryInvoiceRequest() {
        return deliveryInvoiceRequest;
    }

    protected void setDeliveryInvoiceRequest(boolean deliveryInvoiceRequest) {
        this.deliveryInvoiceRequest = deliveryInvoiceRequest;
    }

    public boolean isDeliveryInvoiceResponse() {
        return deliveryInvoiceResponse;
    }

    protected void setDeliveryInvoiceResponse(boolean deliveryInvoiceResponse) {
        this.deliveryInvoiceResponse = deliveryInvoiceResponse;
    }

    public boolean isInvoiceRequest() {
        return invoiceRequest;
    }

    protected void setInvoiceRequest(boolean invoiceRequest) {
        this.invoiceRequest = invoiceRequest;
    }

    public boolean isInvoiceResponse() {
        return invoiceResponse;
    }

    protected void setInvoiceResponse(boolean invoiceResponse) {
        this.invoiceResponse = invoiceResponse;
    }

    public boolean isPartialDelivery() {
        return partialDelivery;
    }

    protected void setPartialDelivery(boolean partialDelivery) {
        this.partialDelivery = partialDelivery;
    }

    public boolean isClosingDelivery() {
        return closingDelivery;
    }

    protected void setClosingDelivery(boolean closingDelivery) {
        this.closingDelivery = closingDelivery;
    }

    public boolean isEnablingDelivery() {
        return enablingDelivery;
    }

    protected void setEnablingDelivery(boolean enablingDelivery) {
        this.enablingDelivery = enablingDelivery;
    }

    public boolean isClosingService() {
        return closingService;
    }

    protected void setClosingService(boolean closingService) {
        this.closingService = closingService;
    }

    public boolean isPartialPayment() {
        return partialPayment;
    }

    protected void setPartialPayment(boolean partialPayment) {
        this.partialPayment = partialPayment;
    }

    @Override
    protected Dependency createDependency(Long dependency) {
        return new FlowControlDependencyImpl(getId(), dependency);
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("typeId", typeId));
        root.addContent(JDOMUtil.createElement("orderId", orderId));
        root.addContent(JDOMUtil.createElement("startingUp", startingUp));
        root.addContent(JDOMUtil.createElement("managerSelecting", managerSelecting));
        root.addContent(JDOMUtil.createElement("initializing", initializing));
        root.addContent(JDOMUtil.createElement("confirming", confirming));
        root.addContent(JDOMUtil.createElement("releaseing", releaseing));
        root.addContent(JDOMUtil.createElement("verifying", verifying));
        root.addContent(JDOMUtil.createElement("downpaymentRequest", downpaymentRequest));
        root.addContent(JDOMUtil.createElement("downpaymentResponse", downpaymentResponse));
        root.addContent(JDOMUtil.createElement("deliveryInvoiceRequest", deliveryInvoiceRequest));
        root.addContent(JDOMUtil.createElement("deliveryInvoiceResponse", deliveryInvoiceResponse));
        root.addContent(JDOMUtil.createElement("invoiceRequest", invoiceRequest));
        root.addContent(JDOMUtil.createElement("invoiceResponse", invoiceResponse));
        root.addContent(JDOMUtil.createElement("enablingDelivery", enablingDelivery));
        root.addContent(JDOMUtil.createElement("partialDelivery", partialDelivery));
        root.addContent(JDOMUtil.createElement("closingDelivery", closingDelivery));
        root.addContent(JDOMUtil.createElement("closingService", closingService));
        root.addContent(JDOMUtil.createElement("partialPayment", partialPayment));
        return root;
    }
}
