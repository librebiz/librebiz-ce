/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.dao

import com.osserp.common.directory.ldap.LdapException

/**
 *
 * LDAP Users data access interface
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
interface Users extends LdapDao {

    /**
     * Creates a new user
     * @param atts
     * @param client if client based dn required
     */
    Map create(Map atts, String client)

    /**
     * Deletes a user
     * @param user	
     */
    void delete(Map user)

    /**
     * Finds all entries with uid (includes computers)
     * @param client if search should be limited to client based dn
     * @return users with uid
     */
    List<Map> findAll(String client)

    /**
     * Looks up for a user by uid
     * @param uid
     * @param client if search should be limited to client based dn
     * @return user
     */
    Map lookup(String uid, String client)

    /**
     * Looks up for a user by complete dn
     * @param dn
     * @return user or empty map if not exists
     */
    Map lookupByDn(String dn)

    /**
     * Changes all password tokens of a user
     * @param user
     * @param password
     * @return user with updated values
     */
    Map changePassword(Map user, String password)

    /**
     * Provides all attribute names
     * @return allAttributes
     */
    List<String> getAllAttributes()

    /**
     * Provides the displayable attribute names 
     * @return displayAttributes
     */
    List<String> getDisplayAttributes()

    /**
     * Provides the attribute names whose values are stored as array
     * @return arrayAttributes
     */
    Set<String> getArrayAttributes()

    /**
     * Adds a mail and optional fetchmail account to an existing user
     * @param user
     * @param mailAddress
     * @param fetchmailAccount
     * @param fetchmailPassword
     * @return user with updated values
     */
    Map addMailaccount(
            Map user,
            String mailAddress,
            String fetchmailAccount,
            String fetchmailPassword) throws LdapException

    /**
     * Adds a mail and optional fetchmail account to an existing user
     * @param user
     * @param mailAddress
     * @return user with updated values
     */
    Map addMailaddress(Map user, String mailAddress) throws LdapException

    /**
     * Adds a new mailalias if not already added
     * @param user
     * @param alias
     * @return updated user
     */
    Map addMailalias(Map user, String alias)

    /**
     * Removes an email alias
     * @param user
     * @param alias
     * @return updated user
     */
    Map removeMailalias(Map user, String alias)

    /**
     * Enables the primary mail account of an existing user
     * @param uid
     * @param client
     */
    void enableFetchmailAccount(String uid, String client)

    /**
     * Enables the primary mail account of an existing user
     * @param uid
     * @param client
     */
    void disableFetchmailAccount(String uid, String client)
}

