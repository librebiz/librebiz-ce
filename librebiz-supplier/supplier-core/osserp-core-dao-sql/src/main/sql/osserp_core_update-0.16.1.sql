SET search_path = osserp, pg_catalog;

ALTER TABLE system_query_parameters ADD COLUMN default_value character varying(255);

INSERT INTO user_permissions VALUES ('fetchmail_admin', 'Inbox-Administration', true, false, false, NULL, false);

UPDATE system_menu_links SET order_id = order_id + 1 WHERE reference_id = 7 AND order_id > 0;
INSERT INTO system_menu_links VALUES (82, 7, 1, 'emailImport', 'fetchmail_admin', false, '/mail/fetchmailInbox/forward', true, false, NULL, false, true);
SELECT pg_catalog.setval('system_menu_links_id_seq', 82, true);

UPDATE system_properties SET pvalue = '0.16.1', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
