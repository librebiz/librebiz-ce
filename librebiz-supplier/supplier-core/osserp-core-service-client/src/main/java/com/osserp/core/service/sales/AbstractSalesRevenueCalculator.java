/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 30, 2011 11:05:08 AM 
 * 
 */
package com.osserp.core.service.sales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionConfig;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesRevenueCostCollector;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.CalculationAwareRecord;
import com.osserp.core.finance.DiscountAwareRecord;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.model.SalesRevenueByItems;
import com.osserp.core.products.Package;
import com.osserp.core.products.ProductDetails;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.sales.SalesRevenue;
import com.osserp.core.sales.SalesRevenueConfig;
import com.osserp.core.sales.SalesRevenueConfigProvider;
import com.osserp.core.users.DomainUser;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public abstract class AbstractSalesRevenueCalculator implements SalesRevenueCalculator {
    private static Logger log = LoggerFactory.getLogger(AbstractSalesRevenueCalculator.class.getName());

    private static final String DEFAULT_COST_COLLECTOR = "costCollectorByPurchase";
    private Employees employees;
    private Products products;
    private SystemConfigs systemConfigs;
    private SalesCreditNotes salesCreditNotesDao;
    private SalesRevenueConfig _config;
    private SalesRevenueConfigProvider configProvider;
    private Map<String, SalesRevenueCostCollector> costCollectors = new HashMap<>();

    protected AbstractSalesRevenueCalculator(
            Employees employees,
            Products products,
            SystemConfigs systemConfigs,
            SalesCreditNotes salesCreditNotesDao,
            SalesRevenueConfigProvider salesRevenueConfigProvider,
            Map<String, SalesRevenueCostCollector> costCollectors) {
        super();
        this.employees = employees;
        this.products = products;
        this.systemConfigs = systemConfigs;
        this.salesCreditNotesDao = salesCreditNotesDao;
        this.configProvider = salesRevenueConfigProvider;
        this.costCollectors = costCollectors;
    }

    protected SalesRevenueConfig getConfig() {
        if (_config == null) {
            reloadConfig();
        }
        return _config;
    }

    @Override
    public synchronized void reloadConfig() {
        _config = (configProvider != null) ? configProvider.getConfig() : null;
    }

    protected boolean postCalculationRequired(BusinessCase businessCase, Record record) {
        return (businessCase != null && businessCase.isSalesContext()
                && businessCase.getStatus() >= Sales.STATUS_BILLED && record instanceof Order);
    }

    protected SalesRevenueCostCollector getCostCollector(String collectorName) {
        return costCollectors == null || costCollectors.isEmpty() ? null : costCollectors.get(collectorName);
    }

    protected String getDefaultCostCollector() {
        String result = (systemConfigs == null ? null : systemConfigs.getSystemProperty("salesRevenueCostCollector"));
        if (result == null) {
            result = DEFAULT_COST_COLLECTOR;
            if (log.isDebugEnabled()) {
                log.debug("getDefaultCostCollector: Using default collector [name=" + result + "]");
            }
        }
        return result;
    }

    protected List<Item> fetchItems(ProductSelectionConfig config, List<Item> items) {
        List<Item> list = new ArrayList<>();
        if (!config.isDisabled()) {
            for (int i = 0, j = items.size(); i < j; i++) {
                Item item = items.get(i);
                if (config.isExclusion()) {
                    if (!config.isMatching(item.getProduct())) {
                        list.add((Item) item.clone());
                    }
                } else if (config.isMatching(item.getProduct())) {
                    list.add((Item) item.clone());
                }
            }
        }
        return list;
    }

    public SalesRevenue create(BusinessCase businessCase, Calculation calculation) {
        if (log.isDebugEnabled()) {
            log.debug("create: invocation by calculation [sales=" + businessCase.getPrimaryKey()
                    + ", calculation=" + calculation.getId()
                    + "]");
        }
        SalesRevenueConfig cfg = getConfig();
        List<Item> items = calculation.getAllItems();
        SalesRevenue salesRevenue = new SalesRevenueByItems(
                businessCase,
                getSalesPerson(businessCase),
                calculation,
                cfg.getHardwareCostsAFilterDescription(),
                cfg.getHardwareCostsBFilterDescription(),
                cfg.getHardwareCostsCFilterDescription(),
                cfg.getOtherCostsFilterDescription(),
                cfg.getForeignCostsFilterDescription(),
                fetchItems(cfg.getHardwareCostsAFilter(), items),
                fetchItems(cfg.getHardwareCostsBFilter(), items),
                fetchItems(cfg.getHardwareCostsCFilter(), items),
                fetchItems(cfg.getOtherCostsFilter(), items),
                fetchItems(cfg.getForeignCostsFilter(), items),
                fetchItems(cfg.getDevelopmentCostsFilter(), items),
                fetchItems(cfg.getUnknownCostsFilter(), items));
        return salesRevenue;
    }

    protected SalesRevenueByItems createByRecord(Employee salesPerson, SalesRevenueConfig cfg, BusinessCase businessCase, Record record) {
        if (record == null) {
            throw new IllegalArgumentException("record must not be null");
        }
        if (log.isDebugEnabled()) {
            log.debug("createByRecord: invocation [businessCase=" + businessCase.getPrimaryKey()
                    + ", record=" + record.getId()
                    + ", type=" + record.getClass().getName()
                    + "]");
        }
        SalesRevenueByItems salesRevenue = new SalesRevenueByItems(
                businessCase,
                salesPerson,
                record,
                new ArrayList<Record>(), // ignore creditNotes in pre-calculation
                cfg.getHardwareCostsAFilterDescription(),
                cfg.getHardwareCostsBFilterDescription(),
                cfg.getHardwareCostsCFilterDescription(),
                cfg.getOtherCostsFilterDescription(),
                cfg.getForeignCostsFilterDescription(),
                fetchItems(cfg.getHardwareCostsAFilter(), record.getItems()),
                fetchItems(cfg.getHardwareCostsBFilter(), record.getItems()),
                fetchItems(cfg.getHardwareCostsCFilter(), record.getItems()),
                fetchItems(cfg.getOtherCostsFilter(), record.getItems()),
                fetchItems(cfg.getForeignCostsFilter(), record.getItems()),
                fetchItems(cfg.getDevelopmentCostsFilter(), record.getItems()),
                fetchItems(cfg.getUnknownCostsFilter(), record.getItems()));
        return salesRevenue;
    }

    @Override
    public void checkDiscount(DomainUser user, CalculationAwareRecord record, BusinessCase businessCase) throws ClientException {
        if (businessCase == null) {
            log.warn("checkDiscount: ignoring invocation outside business case context");
            return;
        }
        if (record.getAmounts() == null
                || record.getAmounts().getAmount() == null
                || record.getAmounts().getAmount().doubleValue() == 0) {
            if (log.isDebugEnabled()) {
                log.debug("checkDiscount: amount not set, checking for service context...");
            }
            if (!businessCase.getType().isService()) {
                if (log.isDebugEnabled()) {
                    log.debug("checkDiscount: failed, no service request!");
                }
                throw new ClientException(ErrorCode.AMOUNT_MISSING);
            }
            boolean freeService = false;
            for (int i = 0, j = record.getItems().size(); i < j; i++) {
                Item next = record.getItems().get(i);
                if (next.getProduct().isPlant()) {
                    ProductDetails details = next.getProduct().getDetails();
                    if (details == null) {
                        details = products.find(next.getProduct().getProductId()).getDetails();
                    }
                    if (details != null && details instanceof Package) {
                        if (log.isDebugEnabled()) {
                            log.debug("checkDiscount: package found [product="
                                    + details.getReference() + "]");
                        }
                        Package pkg = (Package) details;
                        freeService = pkg.isFreeService();
                    }
                }
            }
            if (!freeService) {
                if (log.isDebugEnabled()) {
                    log.debug("checkDiscount: failed, no free service!");
                }
                throw new ClientException(ErrorCode.AMOUNT_MISSING);
            } else if (log.isDebugEnabled()) {
                log.debug("checkDiscount: ok, free service found");
            }
        }
        if (!isDiscountCheckEnabled()) {
            return;
        }
        Double requiredDiscount = 0d;
        if (businessCase.getType().isRecordByCalculation()) {
            requiredDiscount = getDiscountByItems(businessCase, record);
        }
        if (requiredDiscount > 0 && record instanceof DiscountAwareRecord) {
            Double allowedDiscount = ((DiscountAwareRecord) record).getDiscount();
            if (log.isDebugEnabled() && allowedDiscount > 0) {
                log.debug("checkDiscount: found already confirmed discount [value="
                        + allowedDiscount + "]");
            }
            if (allowedDiscount < requiredDiscount) {
                if (user.isPermissionGrant(getConfirmDiscountPermissions())) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkDiscount: current user could confirm discount [user="
                                + user.getId()
                                + ", requiredPermissions="
                                + StringUtil.getArrayAsString(getConfirmDiscountPermissions())
                                + "]");
                    }
                    return;
                }
                if (log.isDebugEnabled()) {
                    log.debug("checkDiscount: current user couldn't confirm discount [user="
                            + user.getId()
                            + ", requiredPermissions="
                            + StringUtil.getArrayAsString(getConfirmDiscountPermissions())
                            + "]");
                }
                throw new ClientException(ErrorCode.PRICE_LIMIT, requiredDiscount);
            }
        }
    }

    private Double getDiscountByItems(BusinessCase request, CalculationAwareRecord record) {
        SalesRevenue revenue = create(request, record);
        if (revenue.isProfitLowerMinimum()) {
            return revenue.getMissingMarginPercent();
        }
        return 0d;
    }

    protected boolean isDiscountCheckEnabled() {
        return systemConfigs.isSystemPropertyEnabled("discountCheckEnabled");
    }

    public String[] getConfirmDiscountPermissions() {
        PermissionConfig cfg = systemConfigs.getPermissionConfig("confirmDiscountPermissions");
        return (cfg == null) ? null : cfg.getPermissionValues();
    }

    protected List<Record> getCreditNotes(Record order) {
        List<Record> creditNotes = new ArrayList<>();
        if (order instanceof SalesOrder && salesCreditNotesDao != null) {
            creditNotes = salesCreditNotesDao.getByOrder(order);
        }
        return creditNotes;
    }

    protected Employee getSalesPerson(BusinessCase businessCase) {
        return employees.get((businessCase.getSalesId() == null || businessCase.getSalesId() == 0) ? Constants.SYSTEM_EMPLOYEE : businessCase.getSalesId());
    }
}
