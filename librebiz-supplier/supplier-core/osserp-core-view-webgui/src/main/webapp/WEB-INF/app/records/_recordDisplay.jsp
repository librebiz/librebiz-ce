<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" scope="page" value="${sessionScope.recordDisplayView}"/>
<c:set var="record" scope="page" value="${view.bean}"/>

<div>
	<span style="padding-left:15px;"><strong><o:out value="${record.type.name}"/></strong></span>
	<table class="table">
		<thead>
			<tr>
				<th><o:out value="${record.id}"/></th>
				<th colspan="3"><o:out value="${record.contact.displayName}"/></th>
			</tr>
			<tr>
				<th><fmt:message key="product"/></th>
				<th><fmt:message key="productName"/></th>
				<th style="text-align: right;"><fmt:message key="quantity"/></th>
				<th style="text-align: right;"><fmt:message key="price"/></th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty record.items}">
					<tr>
						<td colspan="4"><fmt:message key="noProductsFound"/></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="item" items="${record.items}">
						<tr>
							<td><o:out value="${item.product.productId}"/></td>
							<td><o:out value="${item.product.name}"/></td>
							<td style="text-align: right;"><o:number value="${item.quantity}" format="decimal"/></td>
							<td style="text-align: right;"><o:number value="${item.price}" format="currency"/></td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>
