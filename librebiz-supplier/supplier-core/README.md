# The open, smart and simple ERP

This project provides a bunch of libraries to create a free, libre
and open source ERP, CRM and DMS solution for GNU/Linux and most
other operating systems.

### Features

* Customer Relationship Management
* Order Management
* Inventory Management
* Sales & Marketing Management
* Project Management
* Service Management
* Simple Accounting Management
* Workflow Engine
* HRM & Time Recording

### License

The osserp-core package is a free and open source software (FOSS) project, 
ensuring the most freedom for the users of the software.

It lets you read and modify the source to better assist you in creating 
a solution that best fits your requirements. 
You may also create derivate work and give it to your users.

You have the right to do this as long as you follow the rules of the 
GNU Affero General Public License (AGPL) version 3, that comes with the 
following, not exhaustive, but possibly most important restrictions:

* You may not deploy the software on a network without disclosing the full 
 source code of your own applications under the same license.
* You must distribute all source code, including your own product and web-based
 applications.
* You must disclose any modifications made to the software.
* You must include a clickable link "Powered by osserp.com" that leads directly 
 to the URL http://osserp.com in the footer area or, if not reasonably feasible 
 for technical reason, as a top-level link of the primary navigation of the 
 graphical user interface in every copy of the program you distribute.

See included COPYING file for complete license text.
