<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="contactPhoneView"/>

<c:if test="${!empty view}">
    <style type="text/css">
    label {
        font-weight: normal;
    }
    .modalBoxData input[type="button"],
    .modalBoxData input[type="submit"] {
        width: 110px;
    }
    </style>
	<div class="modalBoxHeader">
		<div class="modalBoxHeaderLeft">
			<fmt:message key="${view.headerName}"/>
		</div>
		<div class="modalBoxHeaderRight">
			<div class="boxnav">
				<ul>
                    <c:if test="${!view.createMode and !view.editMode and empty view.bean}">
                        <li><v:ajaxLink url="/contacts/contactPhone/enableCreateMode" linkId="createLink" targetElement="contact_phones_popup"><o:img name="newIcon"/></v:ajaxLink></li>
                    </c:if>
				</ul>
			</div>
		</div>
	</div>
	<div class="modalBoxData">
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<c:choose>
					<c:when test="${view.createMode}">
						<v:ajaxForm name="dynamicForm" targetElement="contact_phones_popup" url="/contacts/contactPhone/save">
							<table class="table">
								<thead>
									<tr>
										<th colspan="3">
                                            <fmt:message key="create${view.headerName}"/>
										</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${!empty sessionScope.errors}">
										<c:set var="errorsColspanCount" scope="request" value="3"/>
										<c:import url="/errors/_errors_table_entry.jsp"/>
									</c:if>
                                    <tr>
                                        <td>
                                            <oc:select name="type" value="${view.bean.type}" options="phoneTypes" styleClass="type"/>
                                        </td>
                                        <td colspan="2">
                                            <v:text name="phoneNumber" value="${view.form.mapped['phoneNumber']}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="note"/></td>
                                        <td colspan="2"><v:text name="note" value="${view.bean.note}" styleClass="pnote"/></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="numberIs"/></td>
                                        <td>
                                            <v:checkbox name="primary" value="${view.bean.primary}"/>
                                            <label for="primary"><fmt:message key="mainNumber"/></label>
                                        </td>
										<td style="text-align: right;">
                                            <span style="margin-right: 15px;">
                                                <v:ajaxLink url="/contacts/contactPhone/disableCreateMode" linkId="exitLink" targetElement="contact_phones_popup">
                                                    <input class="cancel" type="button" value="<fmt:message key="exit"/>"/>
                                                </v:ajaxLink>
                                            </span>
                                            <span style="margin-right: 5px;">
                                                <o:submit styleClass="form-control" />
                                            </span>
										</td>
									</tr>
								</tbody>
							</table>
						</v:ajaxForm>
					</c:when>
                    <c:when test="${view.editMode}">
                        <v:ajaxForm name="dynamicForm" targetElement="contact_phones_popup" url="/contacts/contactPhone/save">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="3">
                                            <fmt:message key="change${view.headerName}"/>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${!empty sessionScope.errors}">
                                        <c:set var="errorsColspanCount" scope="request" value="3"/>
                                        <c:import url="/errors/_errors_table_entry.jsp"/>
                                    </c:if>
                                    <tr>
                                        <td>
                                            <oc:select name="type" value="${view.bean.type}" options="phoneTypes" styleClass="type"/>
                                        </td>
                                        <td colspan="2">
                                            <oc:selectCountry styleClass="phoneCountry" name="country" value="${view.bean}" type="prefixes" prompt="false"/>
                                            <v:text name="prefix" styleClass="phonePrefix" value="${view.bean.prefix}"/>
                                            <v:text name="number" styleClass="phoneNumber" value="${view.bean.number}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="note"/></td>
                                        <td colspan="2"><v:text name="note" value="${view.bean.note}" styleClass="pnote"/></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="numberIs"/></td>
                                        <td>
                                            <v:checkbox name="primary" value="${view.bean.primary}"/>
                                            <label for="primary"><fmt:message key="mainNumber"/></label>
                                        </td>
                                        <td style="text-align: right;">
                                            <span style="margin-right: 15px;">
                                                <v:ajaxLink url="/contacts/contactPhone/disableEditMode" linkId="exitLink" targetElement="contact_phones_popup">
                                                    <input class="cancel" type="button" value="<fmt:message key="exit"/>"/>
                                                </v:ajaxLink>
                                            </span>
                                            <span style="margin-right: 5px;">
                                                <o:submit styleClass="form-control" />
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </v:ajaxForm>
                    </c:when>
					<c:otherwise>
						<%-- list numbers --%>
						<o:logger write="invoked, list mode selected..." level="debug"/>
						<table class="table">
							<tr>
								<th><fmt:message key="type"/></th>
								<th><fmt:message key="number"/></th>
								<th>&nbsp;</th>
							</tr>
							<c:if test="${!empty sessionScope.errors}">
								<c:set var="errorsColspanCount" scope="request" value="3"/>
								<c:import url="/errors/_errors_table_entry.jsp"/>
							</c:if>
                            <c:forEach var="phone" items="${view.list}">
                                <tr>
                                    <td>
                                        <oc:options name="phoneTypes" value="${phone.type}"/> <c:if test="${phone.primary}"><fmt:message key="mainNumberShort"/></c:if>
                                    </td>
                                    <td>
                                        <v:ajaxLink url="/contacts/contactPhone/select?id=${phone.id}" linkId="selectLink" targetElement="contact_phones_popup">
                                            <c:choose>
                                                <c:when test="${!empty phone.note}"><span title="<o:out value="${phone.note}"/>"><oc:phone value="${phone}"/></span></c:when>
                                                <c:otherwise><oc:phone value="${phone}"/></c:otherwise>
                                            </c:choose>
                                        </v:ajaxLink>
                                    </td>
                                    <td class="right">
                                        <v:ajaxLink url="/contacts/contactPhone/delete?id=${phone.id}" linkId="deleteLink" targetElement="contact_phones_popup"><o:img name="deleteIcon" /></v:ajaxLink>
                                    </td>
                                </tr>
                            </c:forEach>
						</table>
					</c:otherwise>
				</c:choose>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</c:if>