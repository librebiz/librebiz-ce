/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web.tag

import javax.servlet.ServletContext
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession
import javax.servlet.jsp.JspException
import javax.servlet.jsp.JspWriter
import javax.servlet.jsp.PageContext

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.web.RequestUtil
import com.osserp.common.web.tags.AbstractSimpleTag
import com.osserp.groovy.web.WebUtil
import com.osserp.groovy.web.View

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractGroovyTag extends AbstractSimpleTag {
    private static Logger logger = LoggerFactory.getLogger(AbstractGroovyTag.class.getName())

    void doTag() throws JspException {
        def response = execute()
        if (response) {
            writeResponse(response)
        }
    }

    protected String execute() {
        return null
    }

    /**
     * Provides current view if available
     * @return current view
     */
    protected final View getCurrentView() {
        View view = WebUtil.fetchCurrentView(request)
        if (!view) {
            view = WebUtil.fetchCurrentView(request.session)
            if (view) {
                // check if last requested is current (e,g. external view init)
                View pageScoped = jspContext.getAttribute('view')
                if (view.name == pageScoped?.name) {
                    view = pageScoped
                }
            }
        }
        return view
    }
    
    /**
     * Provides an object by name searching in all available scopes.
     * @param name
     * @return object found object or null if nothing found
     */
    protected final Object fetchObject(String name) {
        Object result
        if (name) {
            result = pageContext.getAttribute(name)
            if (!result) {
                result = request.getAttribute(name)
                if (!result) {
                    result = session.getAttribute(name)
                }
            }
        }
        result
    }

    /**
     * Writes tags response to servlet output stream.
     */
    protected final void writeResponse(String response) {
        try {
            JspWriter out = getJspContext().getOut()
            out.print(response)
        } catch (Exception e) {
            logger.error("writeResponse: failed [message=${e.message}]")
        }
    }

    @Override
    protected final String createUrl(String action) {
        if (WebUtil.isStrutsRequest(action)) {
            //url.contains('.do')) {
            return "${contextPath}${action}"
        } else if (action.startsWith(WebUtil.getViewPath(servletContext, request))) {
            return action
        } else {
            return "${viewContextPath}${action}"
        }
    }
    
    protected String createAjaxPopupLinkStart(String link, String linkid, String target) {
        createAjaxLinkStart(true, link, linkid, target, null, null, null, null, null)
    }

    protected String createAjaxLinkStart(
        boolean aspopup,
        String link,
        String linkid, 
        String target,
        String styles,
        String prerequest,
        String postrequest,
        String onsuccess,
        String activityelement) {
        
        StringBuilder buffer = new StringBuilder()
        
        buffer.append('<a')
        if (linkid) {
            buffer.append(' id="').append(linkid).append('"')
        }
        buffer.append(' href="javascript:;"').append(' onclick="')
        if (prerequest) {
            buffer.append(prerequest)
        }
        buffer
                .append(' ojsAjax.ajaxLink(\'')
                .append(createUrl(link))
                .append('\', \'')
                .append(target ?: '')
                .append('\', ')
        if (aspopup) {
            buffer.append('true')
        } else {
            buffer.append('false')
        }
        buffer
                .append(", '")
                .append(activityelement ?: '')
                .append("', '")
                .append(getResourceString('waitPleaseWithDots'))
                .append('\', function() { ')
                .append(onsuccess ?: '')
                .append(' }, this); ')
                .append(postrequest ?: '')
                .append('" ')
        if (styles) {
            buffer.append(styles)
        }
        buffer.append('>')
        return buffer.toString()
    }

    protected final String getViewContextPath() {
        "${contextPath}${WebUtil.getViewServletPath(servletContext)}"
    }

    protected final String renderCommonAttributes() {
        StringBuilder buffer = new StringBuilder()
        buffer
                .append(renderId())
                .append(renderOnChange())
                .append(renderOnBlur())
                .append(renderStyle())
                .append(renderStyleClass())
                .append(renderMaxlength())
                .append(renderTabindex())
                .append(renderTitle())
                .append(renderPlaceholder())
        return buffer.toString()
    }
}
