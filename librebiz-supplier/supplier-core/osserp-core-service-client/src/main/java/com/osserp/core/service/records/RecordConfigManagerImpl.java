/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 30, 2016 
 * 
 */
package com.osserp.core.service.records;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.Parameter;
import com.osserp.common.beans.ParameterImpl;

import com.osserp.core.Options;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.finance.RecordConfigManager;
import com.osserp.core.finance.RecordNumberConfig;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.finance.RecordType;
import com.osserp.core.service.impl.AbstractCoreService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordConfigManagerImpl extends AbstractCoreService implements RecordConfigManager {
    private static Logger log = LoggerFactory.getLogger(RecordConfigManagerImpl.class.getName());

    private RecordSearch recordSearch;
    private RecordTypes recordTypes;
    
    public RecordConfigManagerImpl(OptionsCache optionsCache, RecordTypes recordTypes, RecordSearch recordSearch) {
        super(optionsCache);
        this.recordTypes = recordTypes;
        this.recordSearch = recordSearch;
    }

    public List<RecordType> getRecordTypes() {
        return recordTypes.getRecordTypes();
    }

    public List<Parameter> getRecordNumberMethods() {
        List<Parameter> result = new ArrayList<>();
        ParameterImpl byDate = new ParameterImpl(RecordNumberCreator.DATE_SEQUENCE, RecordNumberCreator.DATE_SEQUENCE + "Hint");
        ParameterImpl bySequence = new ParameterImpl(RecordNumberCreator.SEQUENCE, RecordNumberCreator.SEQUENCE + "Hint");
        ParameterImpl byYearlySequence = new ParameterImpl(RecordNumberCreator.YEARLY_SEQUENCE, RecordNumberCreator.YEARLY_SEQUENCE + "Hint");
        result.add(byDate);
        result.add(bySequence);
        result.add(byYearlySequence);
        return result;
    }

    public RecordType update(DomainUser user, RecordType recordType) throws ClientException {
        recordType.updateChanged(user == null ? null : user.getId());
        recordTypes.save(recordType);
        reloadOptions(Options.RECORD_TYPES);
        return recordTypes.getRecordType(recordType.getId());
    }
    
    public RecordType updateNumberConfig(
            DomainUser user, 
            RecordType recordType,
            String numberConfigName,
            Long value) throws ClientException {
        
        if (RecordNumberCreator.DATE_SEQUENCE.equals(numberConfigName)) {
            recordType.updateNumberCreator(
                    user.getId(), 
                    numberConfigName, 
                    (value == null ? 0 : value.intValue()));
            recordTypes.save(recordType);
            
            if (log.isDebugEnabled()) {
                log.debug("updateNumberConfig() done [name=" + recordType.getNumberCreatorName()
                        + ", digits=" + recordType.getUniqueNumberDigitCount() + "]");
            }
            
        } else {
            Long company = ((user.getEmployee().getBranch() != null 
                    && user.getEmployee().getBranch().getCompany() != null) ? 
                            user.getEmployee().getBranch().getCompany().getId() : null);
            if (isSet(company)) {
                
                if (log.isDebugEnabled()) {
                    log.debug("updateNumberConfig() invoked [name=" + numberConfigName
                            + ", company=" + company + "]");
                }
                            
                RecordNumberConfig config = getRecordNumberConfig(recordType, company);
                if (RecordNumberCreator.YEARLY_SEQUENCE.equals(numberConfigName)) {
                    
                    if (config == null) {
                        config = recordTypes.createRecordNumberConfig(
                                recordType, company, true, 
                                (value == null  ? 4 : value.intValue()), null,
                                getDefaultSequenceStart(recordType.getId()));
                    } else {
                        boolean changed = false;
                        if (!config.isYearly()) {
                            config.setYearly(true);
                            changed = true;
                        }
                        int digits = 0;
                        if (value != null && value >= 3L && value <= 10L) {
                            // max id length == 14
                            digits = value.intValue();
                            if (digits != config.getDigits()) {
                                config.setDigits(digits);
                                changed = true;
                            }
                        }
                        if (changed) {
                            recordTypes.save(config);
                        }
                    }
                
                } else if (RecordNumberCreator.SEQUENCE.equals(numberConfigName)) {
                    if (config == null) {
                        config = recordTypes.createRecordNumberConfig(
                                recordType, company, false, 
                                (value == null  ? 4 : value.intValue()), 
                                value, getDefaultSequenceStart(recordType.getId()));
                        
                        if (log.isDebugEnabled()) {
                            log.debug("updateNumberConfig() config and sequence created [config="
                                    + config.getId() + ", name=" + config.getName() + "]");
                        }
                    } else {
                        if (config.isYearly()) {
                            config.setYearly(false);
                            recordTypes.save(config);
                        }
                        recordTypes.createOrRestartSequence(config, value);
                        
                        if (log.isDebugEnabled()) {
                            log.debug("updateNumberConfig() sequence restarted [config="
                                    + config.getId() + ", name=" + config.getName() + "]");
                        }
                    }
            
                } else {
                    log.error("updateNumberConfig() invalid configType provided=" + numberConfigName + "]");
                    throw new IllegalArgumentException("expected: dateSequence|yearlySequence|sequence");
                }
                recordType.updateNumberCreator(user.getId(), numberConfigName, 0);
                recordTypes.save(recordType);
            } else {
                log.warn("updateNumberConfig() ignoring, user does not provide company [id=" 
                        + user.getId() + "]");
            }
        }
        reloadOptions(Options.RECORD_TYPES);
        return recordTypes.getRecordType(recordType.getId());
    }
    
    private Long getDefaultSequenceStart(Long recordType) {
        Long result = 0L;
        List<RecordNumberConfig> configs = getRecordNumberConfigs(null);
        for (int i = 0, j = configs.size(); i < j; i++) {
            RecordNumberConfig next = configs.get(i);
            if (next.getReference().equals(recordType)) {
                if (next.getDefaultNumber() != null && next.getDefaultNumber() > result) {
                    result = next.getDefaultNumber();
                }
            }
        }
        return (result < 1L ? 1L : result);
    }

    public List<RecordNumberConfig> getRecordNumberConfigs(Long company) {
        return recordTypes.getRecordNumberConfigs(company);
    }

    public RecordNumberConfig createRecordNumberConfig(RecordType type, Long company) {
        return addRecordNumberConfigStats(recordTypes.getRecordNumberConfig(type, company), type, company);
    }

    public RecordNumberConfig getRecordNumberConfig(RecordType type, Long company) {
        return addRecordNumberConfigStats(recordTypes.getRecordNumberConfig(type, company), type, company);
    }
    
    private RecordNumberConfig addRecordNumberConfigStats(RecordNumberConfig config, RecordType type, Long company) {
        if (config == null) {
            return null;
        }
        Long[] currentMinMax = recordSearch.getCurrentNumberMinMax(type.getId(), company);
        if (currentMinMax != null) {
            config.setLowestNumber(currentMinMax[0]);
            config.setHighestNumber(currentMinMax[1]);
            if (log.isDebugEnabled()) {
                log.debug("setRecordNumberConfigStats() done [type=" + type.getId()
                        + ", company=" + company + ", min=" + config.getLowestNumber() 
                        + ", max=" + config.getHighestNumber() + "]");
            }
        }
        return config;
    } 

    public void save(RecordNumberConfig config) {
        recordTypes.save(config);
    }
}
