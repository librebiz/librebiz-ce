/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 5, 2016 
 * 
 */
package com.osserp.gui.dms

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsReference
import com.osserp.common.dms.DocumentData
import com.osserp.common.dms.DocumentDataView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class DocumentView extends DocumentAwareView implements DocumentDataView {
    private static Logger logger = LoggerFactory.getLogger(DocumentView.class.getName())

    DmsDocument dmsDocument
    
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        dmsDocument = null
        Long id = form.getLong('id')
        Long type = form.getLong('type')
        if (id && type) {
            dmsDocument = getDocument(type, id)
            
        } else if (id) {
            dmsDocument = fetchDocument(id)
            
        } else {
            id = form.getLong('reference')
            if (id && type) {
                dmsDocument = fetchDocumentByReference(type, id)
            }
        }
        logger.debug("initRequest: done [document=${dmsDocument?.id}]")
    }

    DocumentData getDocumentData() {
        if (dmsDocument) {
            return dmsSearch.getDocumentData(dmsDocument)
        }
        return null
    }
    
    protected DmsDocument fetchDocument(Long id) {
        dmsManager.findDocument(id)
    }
    
    protected DmsDocument fetchDocumentByReference(Long type, Long reference) {
        dmsManager.findDocument(new DmsReference(type, reference))
    }

    protected DmsManager getDmsManager() {
        getService(DmsManager.class.getName())
    }
}


