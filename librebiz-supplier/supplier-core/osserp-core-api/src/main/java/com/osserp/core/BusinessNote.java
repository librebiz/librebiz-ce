/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Oct-2006 09:05:55 
 * 
 */
package com.osserp.core;

import java.util.Date;

import com.osserp.common.Note;
import com.osserp.common.mail.SimpleMail;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessNote extends Note, SimpleMail {

    /**
     * Provides the type of implementing note
     * @return type
     */
    NoteType getType();

    /**
     * @return headline
     */
    String getHeadline();

    /**
     * @param headline
     */
    void setHeadline(String headline);

    /**
     * @return email
     */
    boolean isEmail();

    /**
     * @param email
     */
    void setEmail(boolean email);

    /**
     * A comma separated list of all recipients
     * @return recipients
     */
    String getRecipients();

    /**
     * Sets the recipients
     * @param recipients
     */
    void setRecipients(String recipients);

    /**
     * Indicates if confirmation of note is expected
     * @return true if confirmation expected
     */
    boolean isConfirmationExpected();

    /**
     * Enables/disables confirmation expection
     * @param confirmationExpected
     */
    void setConfirmationExpected(boolean confirmationExpected);

    /**
     * Indicates who has to confirm
     * @return confirmationExpectedBy
     */
    Long getConfirmationExpectedBy();

    /**
     * Sets who has to confirm
     * @param confirmationExpectedBy
     */
    void setConfirmationExpectedBy(Long confirmationExpectedBy);

    /**
     * Provides the date when note was confirmed
     * @return confirmationDate
     */
    Date getConfirmationDate();

    /**
     * Sets the date when note was confirmed
     * @param confirmationDate
     */
    void setConfirmationDate(Date confirmationDate);

    /**
     * Indicates sticky note
     * @return true if sticky
     */
    boolean isSticky();

    /**
     * Declares note as sticky
     * @param sticky
     */
    void setSticky(boolean sticky);

    /**
     * @return true if note is received mail
     */
    boolean isReceived();

}
