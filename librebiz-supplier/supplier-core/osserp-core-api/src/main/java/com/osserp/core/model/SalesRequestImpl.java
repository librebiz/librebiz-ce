/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Sep-2005 18:04:43 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.Option;
import com.osserp.common.User;

import com.osserp.core.Address;
import com.osserp.core.BusinessType;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.SalesRequest;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRequestImpl extends RequestImpl implements SalesRequest {
    private static final long serialVersionUID = 42L;

    protected SalesRequestImpl() {
        super();
    }

    /**
     * Constructor for request transfer
     * @param user creating new request
     * @param request other
     * @param type
     */
    public SalesRequestImpl(Long user, Request request, BusinessType type) {
        super(user, request, type);
    }

    /**
     * Constructor for create actions
     * @param user creating new request
     * @param type
     * @param customer
     */
    public SalesRequestImpl(User user, BusinessType type, Customer customer) {
        super(user, type, customer);
    }

    /**
     * Constructor for create actions
     * @param user creating new request
     * @param type
     * @param customer
     * @param address
     */
    protected SalesRequestImpl(User user, BusinessType type, Customer customer, Address address) {
        super(user, type, customer, new BusinessAddressImpl(address));
    }

    /**
     * @param requestId
     * @param type
     * @param salesId
     * @param salesCoId
     * @param salesTalkDate
     * @param presentationDate
     * @param status
     * @param created
     * @param createdBy
     * @param name
     * @param origin
     * @param branchId
     * @param deliveryDate
     */
    public SalesRequestImpl(
            Long requestId,
            BusinessType type,
            Long salesId,
            Long salesCoId,
            Date salesTalkDate,
            Date presentationDate,
            Long status,
            Date created,
            Long createdBy,
            String name,
            Long origin,
            BranchOffice branch,
            Date deliveryDate) {

        super(
                requestId,
                type,
                salesId,
                salesCoId,
                salesTalkDate,
                presentationDate,
                status,
                created,
                createdBy,
                name,
                origin,
                branch,
                deliveryDate);
    }

    public SalesRequestImpl(
            Long businessId, 
            String externalReference,
            String externalUrl,
            Date createdDate, 
            BusinessType type, 
            BranchOffice office, 
            Customer customer,
            Long salesPersonId, 
            Long projectManagerId, 
            String name, 
            String street, 
            String streetAddon, 
            String zipcode, 
            String city, 
            Long country,
            Campaign campaign, 
            Option origin) {
        super(businessId, externalReference, externalUrl, createdDate, 
                type, office, customer, salesPersonId, projectManagerId, 
                name, street, streetAddon, zipcode, city, country, 
                campaign, origin);
    }

    public SalesRequestImpl(Request otherValue) {
        super(otherValue);
    }

    public SalesRequestImpl(SalesRequest otherValue) {
        super(otherValue);
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        return root;
    }
}
