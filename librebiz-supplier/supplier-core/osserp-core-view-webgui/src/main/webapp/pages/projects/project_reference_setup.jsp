<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="column25l">
    <div class="subcolumnl">
        <div class="spacer"></div>
    </div>
</div>
<div class="column50l">
    <div class="subcolumnc">
        <div class="spacer"></div>
        <div class="contentBox">
            <div class="contentBoxHeader">
                <fmt:message key="changeReferenceSettings" />
            </div>
            <div class="contentBoxData">
                <div class="subcolumns">
                    <div class="subcolumn">
                        <div class="spacer"></div>
                        <o:form name="requestForm" url="/sales.do">
                            <input type="hidden" name="method" value="updateReferenceStatus" />
                            <table class="valueTable first33 inputsFull">
                                <tr>
                                    <td><fmt:message key="reference" /></td>
                                    <td><c:choose>
                                            <c:when test="${project.reference}">
                                                <input type="checkbox" name="reference" checked="checked" />
                                            </c:when>
                                            <c:otherwise>
                                                <input type="checkbox" name="reference" />
                                            </c:otherwise>
                                        </c:choose></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="remark" /></td>
                                    <td><textarea class="form-control" rows="4" name="note"><o:out value="${project.referenceNote}" /></textarea></td>
                                </tr>

                                <c:if test="${internetConfigPermission and project.status >= 95}">
                                    <input type="hidden" name="internetExportPermission" value="true" />
                                    <tr>
                                        <td><fmt:message key="internetReference" /></td>
                                        <td><c:choose>
                                                <c:when test="${project.internetExportEnabled}">
                                                    <input type="checkbox" name="internetExportEnabled" checked="checked" />
                                                </c:when>
                                                <c:otherwise>
                                                    <input type="checkbox" name="internetExportEnabled" />
                                                </c:otherwise>
                                            </c:choose> <span>(<fmt:message key="internetDisplayEnabled" />)
                                        </span></td>
                                    </tr>
                                </c:if>
                                <tr>
                                    <td class="row-submit"><input type="button" class="cancel" onclick="gotoUrl('<c:url value="/sales.do?method=disableReferenceSetup"/>');" value="<fmt:message key="exit"/>" /></td>
                                    <td class="row-submit"><o:submit onclick="setMethod('updateReferenceStatus');" /></td>
                                </tr>
                            </table>
                        </o:form>
                        <div class="spacer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="spacer"></div>
    </div>
</div>
<div class="column25r">
    <div class="subcolumnr">
        <div class="spacer"></div>
    </div>
</div>
