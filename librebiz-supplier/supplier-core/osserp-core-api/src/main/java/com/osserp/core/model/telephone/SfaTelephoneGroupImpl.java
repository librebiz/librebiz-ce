/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 4, 2010 2:49:17 PM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.SfaTelephoneGroup;
import com.osserp.core.telephone.TelephoneGroup;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class SfaTelephoneGroupImpl extends AbstractEntity implements SfaTelephoneGroup {
    private boolean synced = false;
    private boolean unused = false;
    private Long telephoneSystemId = null;
    private TelephoneGroup telephoneGroup = null;
    private String name = null;

    public SfaTelephoneGroupImpl() {
        super();
    }

    public SfaTelephoneGroupImpl(Long telephoneSystemId, TelephoneGroup telephoneGroup, String name, boolean unused) {
        super();
        this.telephoneSystemId = telephoneSystemId;
        this.telephoneGroup = telephoneGroup;
        this.name = name;
        this.unused = unused;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isUnused() {
        return unused;
    }

    public void setUnused(boolean unused) {
        this.unused = unused;
    }

    public Long getTelephoneSystemId() {
        return telephoneSystemId;
    }

    public void setTelephoneSystemId(Long telephoneSystemId) {
        this.telephoneSystemId = telephoneSystemId;
    }

    public TelephoneGroup getTelephoneGroup() {
        return telephoneGroup;
    }

    public void setTelephoneGroup(TelephoneGroup telephoneGroup) {
        this.telephoneGroup = telephoneGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
