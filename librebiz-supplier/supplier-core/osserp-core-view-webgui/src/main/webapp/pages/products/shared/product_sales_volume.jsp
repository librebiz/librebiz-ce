<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${requestScope.view}"/>
<div class="content-area" id="productSalesListContent">
    <div class="row">
        <div class="col-md-12 panel-area">
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th id="number"><fmt:message key="number"/></th>
                            <th id="date"><fmt:message key="date"/></th>
                            <th id="customerId"><fmt:message key="customerId"/></th>
                            <th id="customer"><fmt:message key="customer"/></th>
                            <th id="quantity" class="right"><fmt:message key="quantity"/></th>
                            <th id="amount" class="right"><fmt:message key="price"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="record" items="${view.salesVolume}" varStatus="s">
                            <tr>
                                <td><a href="<c:url value="/salesInvoice.do?method=display&exit=product&readonly=true&id=${record.id}"/>"><o:out value="${record.id}"/></a></td>
                                <td><o:date value="${record.created}"/></td>
                                <td><o:out value="${record.contactId}"/></td>
                                <td><o:out value="${record.contactName}"/></td>
                                <td class="right"><o:number value="${record.quantity}" format="decimal"/></td>
                                <td class="right"><o:number value="${record.price}" format="currency"/> <o:out value="${record.currencyKey}"/></td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <th id="summary" colspan="4"><fmt:message key="totalQuantity"/> / <fmt:message key="averagePrice"/></th>
                            <th class="right"><o:number value="${view.salesVolumeQuantity}" format="decimal"/></th>
                            <th class="right"><o:number value="${view.salesVolumePriceAverage}" format="currency"/> IGN</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
