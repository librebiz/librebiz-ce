/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 30, 2016 
 * 
 */
package com.osserp.gui.employees

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.common.util.NameUtil

import com.osserp.core.Options
import com.osserp.core.contacts.Contact
import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeGroup
import com.osserp.core.employees.EmployeeManager
import com.osserp.core.employees.EmployeeRole
import com.osserp.core.employees.EmployeeRoleConfig
import com.osserp.core.employees.EmployeeStatus
import com.osserp.core.employees.EmployeeType
import com.osserp.core.users.DomainUser

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class EmployeeSetupView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(EmployeeSetupView.class.getName())
    
    Contact contact
    Employee selectedEmployee
    DomainUser employeeAccount
    List<EmployeeGroup> employeeGroups = []
    EmployeeGroup selectedEmployeeGroup
    EmployeeType selectedEmployeeType
    EmployeeStatus selectedEmployeeStatus
    String initialSuggestion
    String rolename
    Date beginDate
    boolean createAccount 

    EmployeeSetupView() {
        super()
        dependencies = ['contactView']
        headerName = 'createEmployeeHeader'
        enableCompanyPreselection()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!env.contactView.bean) {
            logger.warn("initRequest: not bound [user=${user?.id}, name=contactView]")
            throw new ViewContextException(this)
        }
        if (forwardRequest) {
            contact = env.contactView.bean
            selectedEmployee = employeeSearch.find(contact.contactId)
            if (selectedEmployee) {
                employeeAccount = domainUserManager.find(selectedEmployee)
                if (employeeAccount?.ldapUid) {
                    // create already done
                    throw new ViewContextException(ErrorCode.USER_ALREADY_EXISTS)
                }
                EmployeeRoleConfig employeeRoleConfig = selectedEmployee.defaultRoleConfig 
                if (employeeRoleConfig) {
                    selectedBranch = employeeRoleConfig.branch
                    EmployeeRole employeeRole = selectedEmployee.defaultRole
                    selectedEmployeeGroup = employeeRole?.group
                    selectedEmployeeStatus = employeeRole.status
                } else {
                    branchOfficeList = systemConfigManager.getActivatedBranchs()
                }
                if (selectedEmployee.employeeType) {
                    selectedEmployeeType = selectedEmployee.employeeType
                }
                
            } else if (!multipleClientsAvailable && branchOfficeList.size() == 1) {
                selectedBranch = branchOfficeList.get(0)
            } else {
                branchOfficeList = systemConfigManager.getActivatedBranchs()
            }
            if (!selectedEmployee?.initials) {
                initialSuggestion = NameUtil.createInitials(contact.firstName, contact.lastName)
            }
        }
    }

    @Override
    void save() {
        if (!selectedBranch) {
            Long branchId = form.getLong('branchId')
            if (branchId) {
                selectedBranch = systemConfigManager.getBranch(branchId)
            }
        }
        if (!selectedEmployeeGroup) {
            Long groupId = getLong('groupId')
            if (groupId) {
                selectedEmployeeGroup = getOption(Options.EMPLOYEE_GROUPS, groupId)
            }
        }
        if (!selectedEmployeeType) {
            Long typeId = getLong('typeId')
            if (typeId) {
                selectedEmployeeType = getOption(Options.EMPLOYEE_TYPES, typeId)
            }
        }
        if (!selectedEmployeeStatus) {
            Long statusId = getLong('statusId')
            if (statusId) {
                selectedEmployeeStatus = getOption(Options.EMPLOYEE_STATUS, statusId)
            }
        }
        beginDate = getDate('beginDate')
        rolename = getString('role') 
        initialSuggestion = getString('initials')
        createAccount = getBoolean('createAccount')
        
        Long employeeId = employeeManager.create(
            domainUser, 
            contact, 
            selectedEmployeeType, 
            selectedBranch, 
            selectedEmployeeGroup, 
            selectedEmployeeStatus, 
            beginDate, 
            initialSuggestion, 
            rolename)
        selectedEmployee = employeeManager.find(employeeId)
        env.contactView.setAssignMode(false)
        logger.debug("save: done [employee=${selectedEmployee?.id}]")
    }

    protected EmployeeManager getEmployeeManager() {
        getService(EmployeeManager.class.getName())
    }
}
