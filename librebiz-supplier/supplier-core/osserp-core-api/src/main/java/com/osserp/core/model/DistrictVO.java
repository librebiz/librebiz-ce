/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 7, 2004 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.beans.OptionImpl;

import com.osserp.core.District;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DistrictVO extends OptionImpl implements District {
    private static final long serialVersionUID = 42L;

    private String carTemplate = null;
    private Long stateId = null;

    /**
     * Default constructor to implement serializable
     */
    public DistrictVO() {
        super();
    }

    /**
     * Constructor for option interface
     * @param id
     * @param name
     */
    public DistrictVO(Long id, String name) {
        super(id, name);
    }

    /**
     * Constructor to initialize all values
     * @param id
     * @param name
     * @param carTemplate
     * @param stateId
     */
    public DistrictVO(Long id, String name, String carTemplate, Long stateId) {
        super(id, name);
        this.carTemplate = carTemplate;
        this.stateId = stateId;
    }

    /**
     * Returns the carTemplate
     * @return carTemplate.
     */
    public String getCarTemplate() {
        return carTemplate;
    }

    /**
     * Returns the stateId
     * @return stateId.
     */
    public Long getStateId() {
        return stateId;
    }
}
