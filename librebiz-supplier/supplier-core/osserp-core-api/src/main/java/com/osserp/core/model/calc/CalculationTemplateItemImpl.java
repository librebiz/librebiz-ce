/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 31, 2011 9:22:43 AM 
 * 
 */
package com.osserp.core.model.calc;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.calc.CalculationTemplateItem;
import com.osserp.core.products.Product;

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 */
public class CalculationTemplateItemImpl extends AbstractEntity implements CalculationTemplateItem {

    private Product product;
    private String note;
    private String customName;
    private Long calculationConfigGroupId;
    private Date priceDate;
    private boolean partnerPriceEditable;
    private BigDecimal partnerPrice;
    private BigDecimal purchasePrice;
    private boolean optional;

    /**
     * Creates an empty calculation template.
     */
    protected CalculationTemplateItemImpl() {
        super();
    }

    /**
     * Creates a new Item
     * @param reference of the item
     * @param product
     * @param note
     * @param calculationConfigGroupId
     */
    public CalculationTemplateItemImpl(
            Long reference,
            Product product,
            String note,
            Long calculationConfigGroupId) {

        super(null, reference);
        this.product = product;
        this.note = note;
        this.calculationConfigGroupId = calculationConfigGroupId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getCalculationConfigGroupId() {
        return calculationConfigGroupId;
    }

    public void setCalculationConfigGroupId(Long calculationConfigGroupId) {
        this.calculationConfigGroupId = calculationConfigGroupId;
    }

    public Date getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(Date priceDate) {
        this.priceDate = priceDate;
    }

    public BigDecimal getPartnerPrice() {
        return partnerPrice;
    }

    public void setPartnerPrice(BigDecimal partnerPrice) {
        this.partnerPrice = partnerPrice;
    }

    public boolean isPartnerPriceEditable() {
        return partnerPriceEditable;
    }

    public void setPartnerPriceEditable(boolean partnerPriceEditable) {
        this.partnerPriceEditable = partnerPriceEditable;
    }

    public BigDecimal getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public void update(Date priceDate, Double partnerPrice, boolean partnerPriceEditable, Double purchasePrice) {
        this.priceDate = priceDate;
        this.partnerPrice = new BigDecimal(partnerPrice);
        this.partnerPriceEditable = partnerPriceEditable;
        this.purchasePrice = new BigDecimal(purchasePrice);
    }
}
