/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 3, 2011 
 * 
 */
package com.osserp.gui.purchasing

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.Options
import com.osserp.core.contacts.RelatedContactSearch
import com.osserp.core.finance.RecordType
import com.osserp.core.purchasing.PurchaseInvoice
import com.osserp.core.purchasing.PurchaseInvoiceManager
import com.osserp.core.purchasing.PurchaseInvoiceType
import com.osserp.core.suppliers.Supplier
import com.osserp.core.suppliers.SupplierSearch

import com.osserp.gui.records.AbstractRecordCreatorView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class PurchaseRecordCreatorView extends AbstractRecordCreatorView {
    private static Logger logger = LoggerFactory.getLogger(PurchaseRecordCreatorView.class.getName())

    private static final String OVERRIDE_SEQUENCE_PERMISSION = 'record_sequence_override'
    
    RecordType recordType
    
    List<PurchaseInvoiceType> invoiceTypes = []
    PurchaseInvoiceType selectedType

    List<Supplier> supplierPreselection = []
    Supplier supplier
    Boolean supplierRequired = true
    
    PurchaseInvoice existing
    String recordExitTarget

    PurchaseRecordCreatorView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            recordType = fetchRecordType()
            List<PurchaseInvoiceType> typeList = getOptions(Options.PURCHASE_INVOICE_TYPES)
            typeList.each {
                if (!it.autoCreated && (it.supplierSelectionEnabled || it.directInvoiceBookingEnabled || !it.createByOrder)) {
                    invoiceTypes << it
                }
            }
            recordExitTarget = form.getString('recordExit')
            Long id = form.getLong('copy')
            if (id) {
                existing = purchaseInvoiceManager.find(id)
                if (existing) {
                    supplier = existing.contact
                    supplierRequired = false
                    selectedType = existing.bookingType
                    logger.debug("init: done in copy mode... [id=${existing.id}, selectedType=${selectedType.id}, supplier=${supplier.id}, recordExitTarget=${recordExitTarget}]")
                }
            } else {
                if (selectedContact) {
                    supplier = selectedContact
                    supplierRequired = false
                }
                Long type = form.getLong('bookingType')
                if (type) {
                    execSelectType(type)
                }
                logger.debug("init: done [supplier=${supplier?.id}, selectedType=${type}, recordExitTarget=${recordExitTarget}]")
            }
        }
    }

    boolean isManualDateInputAvailable() {
        !recordType.numberCreatorBySequence && recordType.overrideCreatedDate
    }
    
    boolean isManualNumberInputAvailable() {
        recordType.numberCreatorBySequence && isPermissionGrant(OVERRIDE_SEQUENCE_PERMISSION)
    }

    def selectSupplier() {
        selectContact('id')
        if (selectedContact) {
            supplier = selectedContact
        } else {
            supplier = null
        }
    }

    def selectType() {
        Long id = form.getLong('id')
        execSelectType(id)
    }
    
    protected void execSelectType(Long id) {
        if (!id) {
            selectedType = null
            supplierPreselection = []
            if (!supplier) {
                supplierRequired = true
            }
        } else {
            selectedType = invoiceTypes.find { id.equals(it.id) }
            if (!supplier) {
                if (selectedType?.directInvoiceBookingEnabled) {
                    supplierPreselection = supplierSearch.findByDirectInvoiceBooking()
                } else if (selectedType?.supplierSelectionEnabled) {
                    supplierRequired = true
                }
            }
        }
        logger.debug("execSelectType: done [id=${selectedType?.id ?: 'null'}, supplierRequired=${supplierRequired}, preselection=${supplierPreselection.size() > 0}]")
    }

    String getSupplierSearchForward() {
        "${context.appPath}/contacts/contactSearch/forward?exit=/purchasing/purchaseInvoiceCreator/reload&selectionExit=/purchasing/purchaseInvoiceReceipt/forward&selectionTarget=/purchasing/purchaseInvoiceCreator/selectSupplier&context=supplier"
    }

    String getInvoiceCreatedForward() {
        if (bean) {
            if (recordExitTarget) {
                return "/purchaseInvoice.do?method=display&id=${bean.id}&exit=${recordExitTarget}"
            }
            return "/purchaseInvoice.do?method=display&id=${bean.id}&exit=purchaseInvoiceReceipt"
        }
        return null
    }
    
    protected RecordType fetchRecordType() {
        purchaseInvoiceManager.getRecordType()
    }
    
    protected PurchaseInvoiceManager getPurchaseInvoiceManager() {
        getService(PurchaseInvoiceManager.class.getName())
    }

    protected SupplierSearch getSupplierSearch() {
        getService(SupplierSearch.class.getName())
    }
    
    @Override
    protected RelatedContactSearch getRelatedContactSearch() {
        supplierSearch
    }

}
