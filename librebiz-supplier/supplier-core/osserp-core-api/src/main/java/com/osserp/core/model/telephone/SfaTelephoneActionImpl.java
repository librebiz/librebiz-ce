/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 17, 2010 2:49:17 PM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.SfaTelephoneAction;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class SfaTelephoneActionImpl extends AbstractEntity implements SfaTelephoneAction {
    private String action = null;
    private String macaddress = null;
    private Long telephoneSystemId = null;

    protected SfaTelephoneActionImpl() {
        super();
    }

    public SfaTelephoneActionImpl(String action, String macaddress, Long telephoneSystemId) {
        super();
        this.action = action;
        this.macaddress = macaddress;
        this.telephoneSystemId = telephoneSystemId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMacaddress() {
        return macaddress;
    }

    public void setMacaddress(String macaddress) {
        this.macaddress = macaddress;
    }

    public Long getTelephoneSystemId() {
        return telephoneSystemId;
    }

    public void setTelephoneSystemId(Long telephoneSystemId) {
        this.telephoneSystemId = telephoneSystemId;
    }
}
