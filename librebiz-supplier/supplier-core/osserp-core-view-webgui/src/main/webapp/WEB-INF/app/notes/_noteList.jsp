<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="listdate">
                        <v:sortLink key="created">
                            <fmt:message key="dateTime" />
                        </v:sortLink>
                    </th>
                    <th class="listname" colspan="4">
                        <v:sortLink key="name">
                            <fmt:message key="noteFrom" />
                        </v:sortLink>
                    </th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty view.list}">
                        <tr>
                            <td class="emptyList" colspan="5">
                                <span><fmt:message key="noNotesAvailable" /></span>
                                <span style="margin-left: 2px;">
                                    <v:link url="/notes/note/enableCreateMode">
                                        [<fmt:message key="create" />]
                                    </v:link>
                                </span>
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${!empty view.stickyNote}">
                            <tr>
                                <td class="listdate">
                                    <c:choose>
                                        <c:when test="${!empty view.stickyNote.changed}">
                                            <span title="<fmt:message key="createdOn" /> <o:date value="${view.stickyNote.created}" addtime="true" />"><o:date value="${view.stickyNote.changed}" addtime="true" /></span>
                                        </c:when>
                                        <c:otherwise>
                                            <o:date value="${view.stickyNote.created}" addtime="true" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="listname">
                                    <o:ajaxLink linkId="createdBy" url="${'/employeeInfo.do?id='}${view.stickyNote.createdBy}">
                                        <span style="float: left;"><oc:employee value="${view.stickyNote.createdBy}" /></span>
                                    </o:ajaxLink>
                                    <c:if test="${!empty view.stickyNote.changedBy}">
                                        <o:ajaxLink linkId="chnagedBy" url="${'/employeeInfo.do?id='}${view.stickyNote.changedBy}">
                                            <span style="float: left; padding-left: 5px;"> / <fmt:message key="update" />: <oc:employee value="${view.stickyNote.changedBy}" /></span>
                                        </o:ajaxLink>
                                    </c:if>
                                    <c:if test="${view.stickyNote.email}">
                                        <v:ajaxLink url="/notes/sentNote/forward?id=${view.stickyNote.id}" linkId="note_${view.stickyNote.id}">
                                            <span style="float: left; padding-left: 5px;" title="<o:out value="${view.stickyNote.recipients}" />">
                                            <span> / </span><fmt:message key="noteWasSent" /></span>
                                        </v:ajaxLink>
                                    </c:if>
                                </td>
                                <td class="icon center">
                                    <c:if test="${!empty obj.messageId}">
                                        <v:link url="/mail/businessCaseMail/forward?messageId=${obj.messageId}&exit=/notes/note/reload" title="imported">
                                            <span style="float: right; margin-right: 10px;"><o:img name="mailSentIcon" /></span>
                                        </v:link>
                                    </c:if>
                                </td>
                                <c:choose>
                                    <c:when test="${view.stickyNote.email}">
                                        <td class="icon center">
                                            <v:link url="/notes/note/forwardMail?id=${view.stickyNote.id}" title="sendNoteAgain">
                                                <span style="float: right; margin-right: 10px;"><o:img name="mailIcon" /></span>
                                            </v:link>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="icon center">
                                            <v:link url="/notes/note/forwardMail?id=${view.stickyNote.id}" title="sendNoteAsEmail">
                                                <span style="float: right; margin-right: 10px;"><o:img name="mailIcon" /></span>
                                            </v:link>
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td class="icon center">
                                    <v:link url="/notes/note/setSticky?noteId=${view.stickyNote.id}" title="stickyNoteDisable">
                                        <o:img name="enabledIcon" />
                                    </v:link>
                                </td>
                            </tr>
                            <tr>
                                <td class="listdate"></td>
                                <td class="listname">
                                    <c:if test="${!empty view.stickyNote.headline}">
                                        <o:out value="${view.stickyNote.headline}" />
                                        <c:if test="${!empty view.stickyNote.note}">: </c:if>
                                    </c:if>
                                    <v:link url="/notes/note/enableEditMode?noteId=${view.stickyNote.id}" title="changeText">
                                        <o:textOut value="${view.stickyNote.note}" />
                                    </v:link>
                                </td>
                                <td colspan="3"> </td>
                            </tr>
                        </c:if>
                        <c:forEach var="obj" items="${view.list}">
                            <c:if test="${!obj.sticky}">
                                <tr>
                                    <td class="listdate"><o:date value="${obj.created}" addtime="true" /></td>
                                    <td class="listname">
                                        <o:ajaxLink linkId="createdBy" url="${'/employeeInfo.do?id='}${obj.createdBy}">
                                            <span style="float: left;"><oc:employee value="${obj.createdBy}" /></span>
                                        </o:ajaxLink>
                                        <c:if test="${obj.email}">
                                            <v:ajaxLink url="/notes/sentNote/forward?id=${obj.id}" linkId="note_${obj.id}">
                                                <span style="float: left; padding-left: 5px;" title="<o:out value="${obj.recipients}" />">
                                                <span> / </span><fmt:message key="noteWasSent" /></span>
                                            </v:ajaxLink>
                                        </c:if>
                                    </td>
                                    <td class="icon center">
                                        <c:if test="${!empty obj.messageId}">
                                            <v:link url="/mail/businessCaseMail/forward?messageId=${obj.messageId}&exit=/notes/note/reload" title="imported">
                                                <span style="float: right; margin-right: 10px;"><o:img name="mailSentIcon" /></span>
                                            </v:link>
                                        </c:if>
                                    </td>
                                    <c:choose>
                                        <c:when test="${obj.email}">
                                            <td class="icon center">
                                                <v:link url="/notes/note/forwardMail?id=${obj.id}" title="sendNoteAgain">
                                                    <span style="float: right; margin-right: 10px;"><o:img name="mailIcon" /></span>
                                                </v:link>
                                            </td>
                                        </c:when>
                                        <c:otherwise>
                                            <td class="icon center">
                                                <v:link url="/notes/note/forwardMail?id=${obj.id}" title="sendNoteAsEmail">
                                                    <span style="float: right; margin-right: 10px;"><o:img name="mailIcon" /></span>
                                                </v:link>
                                            </td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td class="icon center">
                                        <v:link url="/notes/note/setSticky?noteId=${obj.id}" title="stickyNoteEnable">
                                            <o:img name="toggleStoppedFalseIcon" />
                                        </v:link>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="listdate"></td>
                                    <td class="listname">
                                        <c:if test="${!empty obj.headline}">
                                            <o:out value="${obj.headline}" />
                                            <c:if test="${!empty obj.note}">: </c:if>
                                        </c:if>
                                        <o:textOut value="${obj.note}" />
                                    </td>
                                    <td colspan="3"> </td>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
