/**
 *
 * Copyright (C) 2007, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 16, 2016 
 * 
 */
package com.osserp.common.gui.model;

import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.gui.MenuItem;
import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractMenuItem extends AbstractOption implements MenuItem {

    private Integer orderId = null;
    private String icon = null;
    private String permissions = null;
    private boolean disabled = false;
    private boolean hiddenForAdmin = false;
    private boolean hiddenForGuest = false;
    private boolean hiddenForPublic = false;
    
    protected AbstractMenuItem() {
        super();
    }
    
    protected AbstractMenuItem(String name, int orderId, String permissions) {
        super(name);
        this.orderId = orderId;
        this.permissions = permissions;
    }

    protected AbstractMenuItem(MenuItem vo) {
        super(vo);
        permissions = vo.getPermissions();
        icon = vo.getIcon();
        hiddenForAdmin = vo.isHiddenForAdmin();
        hiddenForGuest = vo.isHiddenForGuest();
        hiddenForPublic = vo.isHiddenForPublic();
        disabled = vo.isDisabled();
        if (vo instanceof AbstractMenuItem) {
            orderId = ((AbstractMenuItem) vo).orderId;
        }
    }

    /**
     * Mapped constructor, copying all values found including id! 
     * You have to reset or reassign the id manually for value-only copying.
     * @param map
     */
    protected AbstractMenuItem(Map<String, Object> map) {
        super(map);
        permissions = fetchString(map, "permissions");
        icon = fetchString(map, "icon");
        hiddenForGuest = fetchBoolean(map, "hiddenForGuest");
        hiddenForAdmin = fetchBoolean(map, "hiddenForAdmin");
        hiddenForPublic = fetchBoolean(map, "hiddenForPublic");
        disabled = fetchBoolean(map, "disabled");
        orderId = fetchInteger(map, "orderId");
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "permissions", permissions);
        putIfExists(map, "icon", icon);
        putIfExists(map, "hiddenForGuest", hiddenForGuest);
        putIfExists(map, "hiddenForAdmin", hiddenForAdmin);
        putIfExists(map, "hiddenForPublic", hiddenForPublic);
        putIfExists(map, "disabled", disabled);
        putIfExists(map, "orderId", orderId);
        return map;
    }
    
    public boolean isHiddenForAdmin() {
        return hiddenForAdmin;
    }

    public void setHiddenForAdmin(boolean hiddenForAdmin) {
        this.hiddenForAdmin = hiddenForAdmin;
    }

    public boolean isHiddenForGuest() {
        return hiddenForGuest;
    }

    public void setHiddenForGuest(boolean hiddenForGuest) {
        this.hiddenForGuest = hiddenForGuest;
    }

    public boolean isHiddenForPublic() {
        return hiddenForPublic;
    }

    public void setHiddenForPublic(boolean hiddenForPublic) {
        this.hiddenForPublic = hiddenForPublic;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public Integer getOrderId() {
        return orderId;
    }

    protected void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public Element getXML(String name) {
        Element root = super.getXML(name);
        root.addContent(JDOMUtil.createElement("permissions", permissions));
        root.addContent(JDOMUtil.createElement("icon", icon));
        root.addContent(JDOMUtil.createElement("hiddenForAdmin", hiddenForAdmin));
        root.addContent(JDOMUtil.createElement("hiddenForGuest", hiddenForGuest));
        root.addContent(JDOMUtil.createElement("hiddenForPublic", hiddenForPublic));
        root.addContent(JDOMUtil.createElement("orderId", orderId));
        root.addContent(JDOMUtil.createElement("disabled", disabled));
        return root;
    }
}
