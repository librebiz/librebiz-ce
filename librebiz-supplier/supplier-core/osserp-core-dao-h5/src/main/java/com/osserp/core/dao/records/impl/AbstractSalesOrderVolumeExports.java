/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 28, 2009 7:39:09 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.dao.Tables;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderVolumeExports;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.model.records.SalesOrderVolumeExportImpl;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesOrderVolumeExport;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractSalesOrderVolumeExports extends AbstractDao
        implements SalesOrderVolumeExports {
    private static Logger log = LoggerFactory.getLogger(AbstractSalesOrderVolumeExports.class.getName());
    protected SalesInvoices salesInvoices = null;
    protected SalesOrders salesOrders = null;
    protected String salesInvoicesTable = null;
    protected String salesOrdersTable = null;
    protected String salesOrderItemsTable = null;
    protected String exportsTable = null;
    protected String exportItemsTable = null;
    protected String exportDeliveriesTable = null;

    /**
     * Creates a new volume export dao instance
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param salesOrders
     * @param salesInvoices
     */
    protected AbstractSalesOrderVolumeExports(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            SalesOrders salesOrders,
            SalesInvoices salesInvoices) {
        super(jdbcTemplate, tables, sessionFactory);

        this.salesInvoices = salesInvoices;
        this.salesOrders = salesOrders;
        this.salesOrdersTable = getTable(TableKeys.SALES_ORDERS);
        this.salesInvoicesTable = getTable(TableKeys.SALES_INVOICES);
        this.salesOrderItemsTable = getTable(TableKeys.SALES_ORDER_ITEMS);
        this.exportItemsTable = getTable(TableKeys.SALES_ORDER_VOLUME_EXPORT_ITEMS);
        this.exportsTable = getTable(TableKeys.SALES_ORDER_VOLUME_EXPORTS);
        this.exportDeliveriesTable = getTable(TableKeys.SALES_ORDER_VOLUME_EXPORT_DELIVERIES);
    }

    /**
     * Creates the statement required for executing {@link #getAvailableOrderCount(SalesOrderVolumeExportConfig)} and
     * {@link #getAvailableOrders(SalesOrderVolumeExportConfig)} queries.
     * @param config with values required for query construction
     * @return query string including any required parameters
     * @throws ClientException if config is invalid and query could not be created
     */
    protected abstract String createAvailabilityQuery(SalesOrderVolumeExportConfig config) throws ClientException;

    @SuppressWarnings("unchecked")
    public List<Order> getAvailableOrders(SalesOrderVolumeExportConfig config) throws ClientException {
        long duration = System.currentTimeMillis();
        List<Long> list = (List<Long>) jdbcTemplate.query(
                createAvailabilityQuery(config),
                getLongRowMapper());
        long queryDuration = System.currentTimeMillis() - duration;
        List result = filter(config, new ArrayList(salesOrders.load(list)));
        duration = System.currentTimeMillis() - duration;
        if (log.isDebugEnabled()) {
            log.debug("getAvailableOrders() done [count=" + result.size()
                    + ", query=" + queryDuration + "ms"
                    + ", fetch=" + (duration - queryDuration) + "ms"
                    + ", total=" + duration + "ms]");
        }
        return result;
    }

    protected List<Order> filter(SalesOrderVolumeExportConfig config, List<Order> result) {
        return result;
    }

    public int getUnreleasedOrderCount(SalesOrderVolumeExportConfig config) {
        return getUnreleasedOrders(config).size();
    }

    public List<RecordDisplay> getUnreleasedOrders(SalesOrderVolumeExportConfig config) {
        List<RecordDisplay> result = new ArrayList<RecordDisplay>();
        List<RecordDisplay> all = salesOrders.findUnreleased(config.getExportCompany().getId());
        boolean filterByCustomer = (config.getExportCustomer() != null);
        for (int i = 0, j = all.size(); i < j; i++) {
            RecordDisplay next = all.get(i);
            if (filterByCustomer) {
                if (next.getContactId().equals(config.getExportCustomer().getId())) {
                    result.add(next);
                }
            } else {
                result.add(next);
            }
        }
        return result;
    }

    public int getInvoiceArchiveCount(SalesOrderVolumeExportConfig config) {
        return jdbcTemplate.queryForObject(
                createInvoiceArchiveSelection(true),
                new Object[] { config.getCompany().getId(), config.getCustomer().getId() },
                new int[] { Types.BIGINT, Types.BIGINT }, Integer.class);
    }

    @SuppressWarnings("unchecked")
    public List<Invoice> getInvoiceArchive(SalesOrderVolumeExportConfig config) {
        List<Long> ids = (List) jdbcTemplate.query(
                createInvoiceArchiveSelection(false),
                new Object[] { config.getCompany().getId(), config.getCustomer().getId() },
                new int[] { Types.BIGINT, Types.BIGINT },
                getLongRowMapper());
        return new ArrayList(salesInvoices.load(ids));
    }

    private String createInvoiceArchiveSelection(boolean count) {
        StringBuilder sql = createSelectFrom(count);
        sql
                .append(salesInvoicesTable)
                .append(" WHERE company_id = ? AND contact_id = ? AND status >= ")
                .append(Invoice.STAT_SENT)
                .append(" AND id IN (SELECT reference_id FROM ")
                .append(exportsTable)
                .append(")");
        return sql.toString();
    }

    public int getOpenInvoiceCount(SalesOrderVolumeExportConfig config) {
        return jdbcTemplate.queryForObject(
                this.createOpenInvoiceSelection(true),
                new Object[] { config.getCompany().getId(), config.getCustomer().getId() },
                new int[] { Types.BIGINT, Types.BIGINT }, Integer.class);
    }

    @SuppressWarnings("unchecked")
    public List<Invoice> getOpenInvoices(SalesOrderVolumeExportConfig config) {
        List<Long> ids = (List) jdbcTemplate.query(
                createOpenInvoiceSelection(false),
                new Object[] { config.getCompany().getId(), config.getCustomer().getId() },
                new int[] { Types.BIGINT, Types.BIGINT },
                getLongRowMapper());
        return new ArrayList(salesInvoices.load(ids));
    }

    @SuppressWarnings("unchecked")
    public Set<Long> getExportedDeliveries() {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT delivery_id FROM ")
                .append(exportDeliveriesTable);
        return new HashSet((List) jdbcTemplate.query(sql.toString(), getLongRowMapper()));
    }

    private String createOpenInvoiceSelection(boolean count) {
        StringBuilder sql = createSelectFrom(count);
        sql
                .append(salesInvoicesTable)
                .append(" WHERE company_id = ? AND contact_id = ? AND status < ")
                .append(Invoice.STAT_SENT)
                .append(" AND id IN (SELECT reference_id FROM ")
                .append(exportsTable)
                .append(")");
        return sql.toString();
    }

    public SalesOrderVolumeExport create(SalesOrderVolumeExportConfig config, SalesInvoice invoice) {
        SalesOrderVolumeExportImpl vo = new SalesOrderVolumeExportImpl(config, invoice);
        save(vo);
        return vo;
    }

    public final void addItem(SalesOrderVolumeExport export, Order order) {
        export.addItem(order.getId());
        save(export);
    }

    public final void addDeliveries(SalesOrderVolumeExport export, Order order, List<Long> deliveries) {
        export.addDeliveries(order, deliveries);
        save(export);
    }

    public void save(SalesOrderVolumeExport volumeExport) {
        getCurrentSession().saveOrUpdate(volumeExport);
    }

    protected final StringBuilder createSelectFrom(boolean count) {
        StringBuilder query = new StringBuilder(256);
        if (count) {
            query.append("SELECT count(*) FROM ");
        } else {
            query.append("SELECT id FROM ");
        }
        return query;
    }

}
