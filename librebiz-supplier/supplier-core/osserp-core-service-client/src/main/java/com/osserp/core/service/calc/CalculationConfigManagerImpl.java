/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 09-Jan-2007 20:26:36 
 * 
 */
package com.osserp.core.service.calc;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.core.BusinessType;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.CalculationConfigManager;
import com.osserp.core.calc.CalculationType;
import com.osserp.core.dao.CalculationConfigs;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationConfigManagerImpl extends AbstractService implements CalculationConfigManager {
    private static Logger log = LoggerFactory.getLogger(CalculationConfigManagerImpl.class.getName());
    private CalculationConfigs configs = null;
    private SystemCompanies companies = null;

    /**
     * Creates a new CalculationConfigManager
     */
    public CalculationConfigManagerImpl(CalculationConfigs configs, SystemCompanies companies) {
        this.configs = configs;
        this.companies = companies;
    }

    public CalculationConfig findConfig(BusinessType requestType) {
        if (requestType == null || requestType.getCalculationConfig() == null) {
            throw new BackendException("missing config for request type "
                    + (requestType == null ? "null" : requestType.getId())
                    + "]");
        }
        return configs.load(requestType.getCalculationConfig());
    }

    public List<SystemCompany> getCompanies() {
        return companies.getAll();
    }

    public CalculationConfig createConfig(CalculationType type, String name) throws ClientException {
        if (type == null) {
            throw new ClientException(ErrorCode.TYPE_REQUIRED);
        }
        CalculationConfig cfg = configs.createConfig(type, name);
        if (log.isDebugEnabled()) {
            log.debug("createConfig() done [id=" + cfg.getId() + ", name=" + cfg.getName() + "]");
        }
        return cfg;
    }

    public CalculationConfig getConfig(Long id) {
        return configs.load(id);
    }

    public List<CalculationConfig> getConfigs() {
        return configs.findAll();
    }

    public List<CalculationConfig> getConfigs(Long typeId) {
        return configs.findByType(typeId);
    }

    public CalculationConfig updateConfig(CalculationConfig config, String name) throws ClientException {
        config.setName(name);
        configs.save(config);
        return config;
    }

    public CalculationConfigGroup findGroup(ItemPosition position) {
        return configs.findGroup(position.getGroupId());
    }

    public CalculationConfigGroup findGroup(Long id) {
        return configs.findGroup(id);
    }

    public CalculationConfigGroup addGroup(CalculationConfig config, String name, boolean discounts, boolean option) throws ClientException {
        return configs.addGroup(config, name, discounts, option);
    }

    public CalculationConfigGroup updateGroup(
            CalculationConfig config,
            CalculationConfigGroup group,
            String name,
            boolean discounts,
            boolean option) throws ClientException {

        CalculationConfigGroup updated = config.updateGroup(group.getId(), name, discounts, option);
        configs.save(config);
        return updated;
    }

    public void removeGroup(CalculationConfig config, Long groupId) {
        config.removeGroup(groupId);
        configs.save(config);
        config = configs.load(config.getId());
    }

    public CalculationConfig addProductSelection(CalculationConfig config, Long groupId, Long configId) {
        return configs.addProductSelection(config, groupId, configId);
    }

    public CalculationConfig removeProductSelection(CalculationConfig config, Long groupId, Long configId) {
        return configs.removeProductSelection(config, groupId, configId);
    }

    public CalculationConfig setPartList(CalculationConfig config, Long groupId, CalculationConfig partListConfig) {
        return configs.configurePartList(config, groupId, partListConfig);
    }

    // type related methods

    public CalculationType createType(String name, String description) throws ClientException {
        return configs.createType(name, description);
    }

    public void updateType(Long id, String name, String description) throws ClientException {
        CalculationType type = configs.loadType(id);
        configs.updateType(type, name, description);
    }

    public CalculationType updateType(CalculationType type, String name, String description) throws ClientException {
        return configs.updateType(type, name, description);
    }

    public CalculationType getType(Long id) {
        return configs.loadType(id);
    }

    public List<CalculationType> getTypes() {
        return configs.getTypes();
    }
}
