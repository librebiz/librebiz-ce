/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13-Oct-2006 13:33:06 
 * 
 */
package com.osserp.core.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.Option;
import com.osserp.common.Property;
import com.osserp.common.User;
import com.osserp.common.util.DateUtil;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BusinessAddress;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.FcsItem;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestImpl extends AbstractBusinessCase implements Request {
    private static Logger log = LoggerFactory.getLogger(RequestImpl.class.getName());
    private static final int PRESENTATION_DAYS = 3;
    private Long requestId;
    private String name;
    private Customer customer;
    private Long managerId;
    private Long salesId;
    private String salesAssignment = "manualAssignment";
    private boolean salesAssignmentStrict = false;
    private Long salesCoId;
    private Double salesCoPercent = 0d;
    private Customer agent = null;
    private Double agentCommission = 0d;
    private boolean agentCommissionPercent = false;
    private Customer tip = null;
    private Double tipCommission = 0d;
    private Date salesTalkDate;
    private Date presentationDate;
    private Date deliveryDate;
    private Long origin = null;
    private Long originType = null;
    private BusinessAddress address;
    private BranchOffice branch = null;
    private boolean stopped = false;
    private Integer orderProbability = 0;
    private Date orderProbabilityDate = null;
    private Double capacity = 0.0;
    private Double salesCommission = 0d;
    private Double salesCommissionGoodwill = 0d;
    private boolean salesCommissionPaid = false;
    private Long paymentId;
    private Long shippingId;
    private String accountingReference;
    private String externalReference;
    private String externalUrl;

    private BusinessCaseDetailsImpl detailsObj = new BusinessCaseDetailsImpl();

    protected RequestImpl() {
        super();
        setClassDisc(RequestTypeImpl.CLASS_SALES);
    }

    protected RequestImpl(Long user, Request request, BusinessType type) {
        super();
        requestId = request.getRequestId();
        update(user, request);
        setBusinessType(type);
    }

    /**
     * Default constructor for inherited classes to create a new request
     * @param user
     * @param type
     * @param customer
     */
    protected RequestImpl(
            User user,
            BusinessType type,
            Customer customer) {
        super(user, type);

        this.customer = customer;
        address = new BusinessAddressImpl(customer);
        salesTalkDate = new Date(System.currentTimeMillis());
        if (type.isDirectSales()) {
            presentationDate = getCreated();
        } else {
            presentationDate = DateUtil.addDays(salesTalkDate, PRESENTATION_DAYS);
        }
        if (getName() == null) {
            StringBuffer requestName = new StringBuffer(128);
            requestName.append(customer.getLastName());
            if (isSet(customer.getAddress().getCity())) {
                requestName.append(", ").append(customer.getAddress().getCity());
            }
            setName(requestName.toString());
        }
        if (user instanceof DomainUser) {
            DomainUser du = (DomainUser) user;
            if (du.getEmployee() != null
                    && du.getEmployee().getBranch() != null
                    && du.getEmployee().getBranch().getId() != null) {
                branch = du.getEmployee().getBranch();
            }
        }
        if (isSet(type.getDefaultOrigin())) {
            origin = type.getDefaultOrigin();
        }
        if (isSet(type.getDefaultOriginType())) {
            originType = type.getDefaultOriginType();
        }
    }

    protected RequestImpl(
            User user,
            BusinessType type,
            Customer customer,
            BusinessAddress address) {
        this(user, type, customer);
        this.address = address;
    }

    protected RequestImpl(
            Long businessId,
            String externalReference,
            String externalUrl,
            Date createdDate,
            BusinessType type,
            BranchOffice office,
            Customer customer,
            Long salesPersonId,
            Long projectManagerId,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Campaign campaign,
            Option origin) {
        this(null, type, customer);
        this.externalReference = externalReference;
        this.externalUrl = externalUrl;

        if (createdDate != null) {
            setCreated(createdDate);
        }
        if (isSet(businessId)) {
            setRequestId(businessId);
        }
        if (isSet(salesPersonId)) {
            setCreatedBy(salesPersonId);
        }
        update(
                office,
                salesPersonId,
                projectManagerId,
                name,
                street,
                streetAddon,
                zipcode,
                city,
                country,
                campaign,
                origin);
    }

    //  constructor for plan list action; values from database: 	
    //	"plan_id,type_id,sales_id,sales_talk_date,presentation_date," +
    //	"installation_date,status_id,created,created_by";

    public RequestImpl(
            Long requestId,
            BusinessType type,
            Long salesId,
            Long salesCoId,
            Date salesTalkDate,
            Date presentationDate,
            Long status,
            Date created,
            Long createdBy,
            String name,
            Long origin,
            BranchOffice branch,
            Date deliveryDate) {

        super(createdBy, type);
        this.requestId = requestId;
        this.salesId = salesId;
        this.salesCoId = salesCoId;
        this.salesTalkDate = salesTalkDate;
        this.presentationDate = presentationDate;
        this.name = name;
        this.origin = origin;
        this.branch = branch;
        this.deliveryDate = deliveryDate;
        setStatus(status);
    }

    public RequestImpl(Request otherValue) {
        super(otherValue);
        accountingReference = otherValue.getAccountingReference();
        externalReference = otherValue.getExternalReference();
        externalUrl = otherValue.getExternalUrl();
        requestId = otherValue.getRequestId();
        customer = otherValue.getCustomer();
        managerId = otherValue.getManagerId();
        salesId = otherValue.getSalesId();
        salesCoId = otherValue.getSalesCoId();
        salesTalkDate = otherValue.getSalesTalkDate();
        presentationDate = otherValue.getPresentationDate();
        address = otherValue.getAddress();
        name = otherValue.getName();
        branch = otherValue.getBranch();
        deliveryDate = otherValue.getDeliveryDate();
        origin = otherValue.getOrigin();
        originType = otherValue.getOriginType();
        orderProbability = otherValue.getOrderProbability();
        orderProbabilityDate = otherValue.getOrderProbabilityDate();
        capacity = otherValue.getCapacity();
    }

    private void update(Long user, Request otherValue) {
        super.update(user, otherValue);
        accountingReference = otherValue.getAccountingReference();
        externalReference = otherValue.getExternalReference();
        externalUrl = otherValue.getExternalUrl();
        agent = otherValue.getAgent();
        agentCommission = otherValue.getAgentCommission();
        agentCommissionPercent = otherValue.isAgentCommissionPercent();
        customer = otherValue.getCustomer();
        managerId = otherValue.getManagerId();
        salesId = otherValue.getSalesId();
        salesCoId = otherValue.getSalesCoId();
        salesCoPercent = otherValue.getSalesCoPercent();
        salesTalkDate = otherValue.getSalesTalkDate();
        presentationDate = otherValue.getPresentationDate();
        name = otherValue.getName();
        branch = otherValue.getBranch();
        deliveryDate = otherValue.getDeliveryDate();
        origin = otherValue.getOrigin();
        originType = otherValue.getOriginType();
        orderProbability = otherValue.getOrderProbability();
        orderProbabilityDate = otherValue.getOrderProbabilityDate();
        salesAssignment = otherValue.getSalesAssignment();
        salesAssignmentStrict = otherValue.isSalesAssignmentStrict();
        tip = otherValue.getTip();
        tipCommission = otherValue.getTipCommission();
        if (otherValue.getAddress().getStreet() == null
                && otherValue.getAddress().getZipcode() == null
                && otherValue.getAddress().getCity() == null) {
            if (log.isDebugEnabled()) {
                log.debug("update() creating new address object");
            }
            address = new BusinessAddressImpl(otherValue.getCustomer());
            if (log.isDebugEnabled()) {
                log.debug("update() creating new address object:\n"
                        + address.toString());
            }
        } else {
            address = otherValue.getAddress();
        }
        //this.propertyList = other.getPropertyList();
    }

    public void update(
            BranchOffice office,
            Long salesPersonId,
            Long projectManagerId,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Campaign campaign,
            Option origin) {

        if (office != null) {
            setBranch(office);
        }
        if (isSet(salesPersonId)) {
            salesId = salesPersonId;
        }
        if (isSet(projectManagerId)) {
            managerId = projectManagerId;
        }
        if (isSet(name)) {
            setName(name);
        }
        if (campaign != null) {
            setOrigin(campaign.getId());
        }
        if (origin != null) {
            setOriginType(origin.getId());
        }
        updateAddress(street, streetAddon, zipcode, city, country);
    }

    private void updateAddress(String street, String streetAddon, String zipcode, String city, Long country) {
        if (isSet(street) || isSet(zipcode) || isSet(city)) {
            // reset all existing values to avoid mixed address values
            address.setStreet(null);
            address.setStreetAddon(null);
            address.setZipcode(null);
            address.setCity(null);
            address.setCountry(null);
            if (isSet(street)) {
                address.setStreet(street);
            }
            if (isSet(streetAddon)) {
                address.setStreetAddon(streetAddon);
            }
            if (isSet(zipcode)) {
                address.setZipcode(zipcode);
            }
            if (isSet(city)) {
                address.setCity(city);
            }
            if (isSet(country)) {
                address.setCountry(country);
            }
        }
    }

    public void updateOrderProbability(Integer orderProbability, Date orderProbabilityDate) {
        this.orderProbability = (orderProbability == null) ? 0 : orderProbability;
        this.orderProbabilityDate = orderProbabilityDate;
    }

    @Override
    public String getAccountingReference() {
        return accountingReference;
    }

    @Override
    public void setAccountingReference(String accountingReference) {
        this.accountingReference = accountingReference;
    }

    public void setExternalId(Long externalBusinessCaseId) {
        if (getType() != null && getType().isExternalIdProvided()) {
            setRequestId(externalBusinessCaseId);
        }
    }

    public String getExternalReference() {
        return externalReference;
    }

    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public Long getPrimaryKey() {
        return requestId;
    }

    public Long getRequestId() {
        return requestId;
    }

    protected void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public void setId(Long requestId) {
        this.requestId = requestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public String getSalesAssignment() {
        return salesAssignment;
    }

    public void setSalesAssignment(String salesAssignment) {
        this.salesAssignment = salesAssignment;
    }

    public boolean isSalesAssignmentStrict() {
        return salesAssignmentStrict;
    }

    public void setSalesAssignmentStrict(boolean salesAssignmentStrict) {
        this.salesAssignmentStrict = salesAssignmentStrict;
    }

    public void updateSalesAssignment(String assignment, boolean assignmentStrict) {
        salesAssignment = assignment;
        salesAssignmentStrict = assignmentStrict;
    }

    public Long getSalesId() {
        return salesId;
    }

    public void setSalesId(Long salesId) {
        this.salesId = salesId;
    }

    public void updateSales(Employee sales, BranchOffice branch) {
        salesId = sales.getId();
        if (getType().isUpdateBranchOnSalesPersonChange() && branch != null) {
            if (!getType().isForceDefaultBranch() ||
                    (getType().isForceDefaultBranch() && getBranch() == null)) {
                this.branch = branch;
            }
        }
    }

    public void updateBranch(BranchOffice branch) {
        this.branch = branch;
    }

    public Double getSalesPercent() {
        return (salesCoPercent == null ? 1d : (1 - salesCoPercent));
    }

    public Long getSalesCoId() {
        return salesCoId;
    }

    public void setSalesCoId(Long salesCoId) {
        this.salesCoId = salesCoId;
    }

    public Double getSalesCoPercent() {
        return salesCoPercent;
    }

    public void setSalesCoPercent(Double salesCoPercent) {
        this.salesCoPercent = salesCoPercent;
    }

    public Customer getAgent() {
        return agent;
    }

    protected void setAgent(Customer agent) {
        this.agent = agent;
    }

    public void configureAgent(
            Customer selectedAgent,
            Double commission,
            boolean commissionPercent) {
        agent = selectedAgent;
        if (agent != null) {
            agentCommission = commission;
            agentCommissionPercent = commissionPercent;
        } else {
            agentCommission = 0d;
            agentCommissionPercent = false;
        }
    }

    public Double getAgentCommission() {
        return agentCommission;
    }

    public void setAgentCommission(Double agentCommission) {
        this.agentCommission = agentCommission;
    }

    public boolean isAgentCommissionPercent() {
        return agentCommissionPercent;
    }

    public void setAgentCommissionPercent(boolean agentCommissionPercent) {
        this.agentCommissionPercent = agentCommissionPercent;
    }

    public Double getSalesCommission() {
        return salesCommission;
    }

    public void setSalesCommission(Double salesCommission) {
        this.salesCommission = salesCommission;
    }

    public Double getSalesCommissionGoodwill() {
        return salesCommissionGoodwill;
    }

    public void setSalesCommissionGoodwill(Double salesCommissionGoodwill) {
        this.salesCommissionGoodwill = salesCommissionGoodwill;
    }

    public boolean isSalesCommissionPaid() {
        return salesCommissionPaid;
    }

    public void setSalesCommissionPaid(boolean salesCommissionPaid) {
        this.salesCommissionPaid = salesCommissionPaid;
    }

    public Customer getTip() {
        return tip;
    }

    protected void setTip(Customer tip) {
        this.tip = tip;
    }

    public void configureTip(Customer customerTip, Double commission) {
        tip = customerTip;
        if (tip != null) {
            tipCommission = commission;
        } else {
            tipCommission = 0d;
        }
    }

    public Double getTipCommission() {
        return tipCommission;
    }

    protected void setTipCommission(Double tipCommission) {
        this.tipCommission = tipCommission;
    }

    public Date getSalesTalkDate() {
        return salesTalkDate;
    }

    public void setSalesTalkDate(Date salesTalkDate) {
        this.salesTalkDate = salesTalkDate;
    }

    public Date getPresentationDate() {
        return presentationDate;
    }

    public void setPresentationDate(Date presentationDate) {
        this.presentationDate = presentationDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Long getShippingId() {
        return shippingId;
    }

    public void setShippingId(Long shippingId) {
        this.shippingId = shippingId;
    }

    public Long getOrigin() {
        return origin;
    }

    public void setOrigin(Long origin) {
        this.origin = origin;
    }

    public Long getOriginType() {
        return originType;
    }

    public void setOriginType(Long originType) {
        this.originType = originType;
    }

    public void updateOriginType(Long employee, Long originType) {
        setOriginType(originType);
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(employee == null ? Constants.SYSTEM_EMPLOYEE : employee);
    }

    public BranchOffice getBranch() {
        return branch;
    }

    protected void setBranch(BranchOffice branch) {
        this.branch = branch;
    }

    public boolean isStopped() {
        return stopped;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    public Integer getOrderProbability() {
        return orderProbability;
    }

    public void setOrderProbability(Integer orderProbability) {
        this.orderProbability = orderProbability;
    }

    public Date getOrderProbabilityDate() {
        return orderProbabilityDate;
    }

    public void setOrderProbabilityDate(Date orderProbabilityDate) {
        this.orderProbabilityDate = orderProbabilityDate;
    }

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public BusinessAddress getAddress() {
        return address;
    }

    protected void setAddress(BusinessAddress address) {
        this.address = address;
    }

    public void updateAddress(
            Long updateBy,
            String addressName,
            String person,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String federalStateName,
            Long district) {

        if (address == null) {
            address = new BusinessAddressImpl(
                    addressName,
                    person,
                    street,
                    streetAddon,
                    zipcode,
                    city,
                    country,
                    federalState,
                    federalStateName,
                    district);
        } else {
            address.setCity(city);
            address.setCountry(country);
            address.setDistrictId(district);
            address.setFederalStateId(federalState);
            address.setFederalStateName(federalStateName);
            address.setName(addressName);
            address.setPerson(person);
            address.setStreet(street);
            address.setZipcode(zipcode);
        }
    }

    public void updateAddress(BusinessAddress values) {
        if (address == null) {
            address = new BusinessAddressImpl(
                    values.getName(),
                    values.getPerson(),
                    values.getStreet(),
                    values.getStreetAddon(),
                    values.getZipcode(),
                    values.getCity(),
                    values.getCountry(),
                    values.getFederalStateId(),
                    values.getFederalStateName(),
                    values.getDistrictId());
        } else {
            address.setCity(values.getCity());
            address.setCountry(values.getCountry());
            address.setDistrictId(values.getDistrictId());
            address.setFederalStateId(values.getFederalStateId());
            address.setFederalStateName(values.getFederalStateName());
            address.setName(values.getName());
            address.setPerson(values.getPerson());
            address.setStreet(values.getStreet());
            address.setZipcode(values.getZipcode());
        }
        address.setEmail(values.getEmail());
        address.setFax(values.getFax());
        address.setMobile(values.getMobile());
        address.setPhone(values.getPhone());
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(values.getChangedBy());
    }

    public boolean isCancelled() {
        // a request is cancelled if status < -1 wich stands for waiting 
        return (getStatus() == null ? false : getStatus() < -1);
    }

    public boolean isInitial() {
        return (getType() == null ? false : getType().getId() >= 100);
    }

    public boolean addFlowControl(
            FcsAction action,
            Long employeeId,
            Date created,
            String note,
            String headline,
            FcsClosing closing) throws ClientException {

        if (isFlowControlActionCreateable(action)) {
            checkFlowControlActionNote(action, note);
            FcsItem item = new RequestFcsItemImpl(
                    this,
                    action,
                    created,
                    employeeId,
                    note,
                    headline,
                    false,
                    closing);
            addFlowControl(item);
            if (!action.isCancelling()) {
                increaseStatus(action.getStatus());
            } else {
                setStatus(Long.valueOf(action.getStatus()));
            }
            setChanged(new Date(System.currentTimeMillis()));
            setChangedBy(employeeId);
            return true;
        }
        return false;
    }

    public final boolean isSalesContext() {
        return false;
    }

    // implementation of details interface

    public List<Property> getPropertyList() {
        return detailsObj.getPropertyList();
    }

    public Map<String, Property> getPropertyMap() {
        return detailsObj.getPropertyMap();
    }

    public void addProperty(Long user, String name, String value) {
        detailsObj.addProperty(user, name, value);
    }

    public void removeProperty(Long user, Property property) {
        detailsObj.removeProperty(user, property);
    }

    public boolean isPropertyAvailable(String propertyName) {
        return detailsObj.getPropertyMap().containsKey(propertyName);
    }

    public Property getProperty(String propertyName) {
        return detailsObj.getPropertyMap().get(propertyName);
    }

    protected BusinessCaseDetailsImpl getDetailsObj() {
        return detailsObj;
    }

    protected void setDetailsObj(BusinessCaseDetailsImpl detailsObj) {
        this.detailsObj = detailsObj;
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("accountingReference", accountingReference));
        root.addContent(JDOMUtil.createElement("externalReference", externalReference));
        root.addContent(JDOMUtil.createElement("requestId", requestId));
        root.addContent(JDOMUtil.createElement("name", name));
        root.addContent(JDOMUtil.createElement("customer", customer));
        root.addContent(JDOMUtil.createElement("managerId", managerId));
        root.addContent(JDOMUtil.createElement("salesId", salesId));
        root.addContent(JDOMUtil.createElement("salesCoId", salesCoId));
        root.addContent(JDOMUtil.createElement("salesCoPercent", salesCoPercent));
        root.addContent(JDOMUtil.createElement("agent", agent));
        root.addContent(JDOMUtil.createElement("agentCommission", agentCommission));
        root.addContent(JDOMUtil.createElement("agentCommissionPercent", agentCommissionPercent));
        root.addContent(JDOMUtil.createElement("tip", tip));
        root.addContent(JDOMUtil.createElement("tipCommission", tipCommission));
        root.addContent(JDOMUtil.createElement("salesTalkDate", salesTalkDate));
        root.addContent(JDOMUtil.createElement("presentationDate", presentationDate));
        root.addContent(JDOMUtil.createElement("presentationDate", presentationDate));
        root.addContent(JDOMUtil.createElement("origin", origin));
        root.addContent(JDOMUtil.createElement("originType", originType));
        root.addContent(JDOMUtil.createElement("address", address));
        root.addContent(JDOMUtil.createElement("branch", branch));
        root.addContent(JDOMUtil.createElement("stopped", stopped));
        root.addContent(JDOMUtil.createElement("orderProbability", orderProbability));
        root.addContent(JDOMUtil.createElement("orderProbabilityDate", orderProbabilityDate));
        root.addContent(detailsObj.getXML("details"));
        return root;
    }
}
