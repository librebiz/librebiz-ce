/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 03, 2009 12:02:15 PM 
 * 
 */
package com.osserp.core.hrm;

import java.util.Date;

import com.osserp.common.Calendar;
import com.osserp.common.EntityRelation;

/**
 * 
 * @author jg <jg@osserp.com>
 * 
 */
public interface TimeRecordMarker extends EntityRelation {

    /**
     * Provides the type of the marker
     * @return type
     */
    TimeRecordMarkerType getType();

    /**
     * Provides the marker date
     * @return markerDate
     */
    Date getMarkerDate();

    /**
     * Provides the marker value
     * @return markerValue
     */
    Double getMarkerValue();

    /**
     * Provides a note for the marker
     * @return note
     */
    String getNote();

    /**
     * Indicates that marker is same day
     * @param day
     * @return true if marker is same day
     */
    boolean isDay(Calendar day);

    /**
     * Indicates that marker is from current day
     * @return currentDay
     */
    boolean isCurrentDay();

    /**
     * Indicates that marker is member of a year
     * @param year
     * @return true if marker values year matches
     */
    boolean isMemberOf(int year);

    /**
     * Indicates that marker is member of a month in year
     * @param month
     * @param year
     * @return true if marker values month and year matches
     */
    boolean isMemberOf(int month, int year);

}
