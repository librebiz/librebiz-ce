<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="${sessionScope.contactInputViewName}" />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="companyName"><fmt:message key="company" /> / <fmt:message key="institution" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="company" styleClass="form-control" title="companyNameLabelHint" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="companyAffix"><fmt:message key="companyNameAddonLabel" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="companyAffix" styleClass="form-control" title="companyNameAddonLabelHint" />
        </div>
    </div>
</div>

<c:import url="${viewdir}/contacts/_contactAddressInput.jsp" />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="website" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="website" styleClass="form-control" />
        </div>
    </div>
</div>
