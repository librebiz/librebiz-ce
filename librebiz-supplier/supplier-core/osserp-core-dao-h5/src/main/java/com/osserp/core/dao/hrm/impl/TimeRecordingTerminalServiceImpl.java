/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 4, 2011 1:14:20 PM 
 * 
 */
package com.osserp.core.dao.hrm.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberFormatter;

import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingTerminalService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingTerminalServiceImpl extends AbstractTimeRecordings implements TimeRecordingTerminalService {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingTerminalServiceImpl.class.getName());

    protected TimeRecordingTerminalServiceImpl(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache options,
            String timeRecordingTableKey,
            String timeRecordingPeriodTableKey,
            String timeRecordTableKey,
            String timeRecordCorrectionsTableKey,
            String timeRecordingApprovalsTableKey) {
        super(jdbcTemplate, tables, sessionFactory, options, timeRecordingTableKey,
                timeRecordingPeriodTableKey, timeRecordTableKey, timeRecordCorrectionsTableKey,
                timeRecordingApprovalsTableKey);
    }

    public String getCarryoverHours(String chipID) throws ClientException {
        TimeRecording recording = find(chipID);
        return DateFormatter.getHoursAndMinutes(recording.getCarryoverMinutes());
    }

    public String getRemainingLeave(String chipID) throws ClientException {
        TimeRecording recording = find(chipID);
        return NumberFormatter.getValue(recording.getRemainingLeave(), NumberFormatter.DECIMAL, 2);
    }

    public Date addBooking(Long typeId, String stringStatus, String stringChipId, String timestamp, String stringDeviceId)
            throws ClientException {

        if (log.isDebugEnabled()) {
            log.debug("addBooking() invoked [type=" + typeId
                    + ", stringDeviceId=" + stringDeviceId
                    + ", stringStatus=" + stringStatus
                    + ", timestamp=" + timestamp
                    + ", stringChipId=" + stringChipId
                    + "]");
        }
        Long status = parseStatus(stringStatus);
        Long deviceId = parseTerminalId(stringDeviceId);
        Date date = parseDate(timestamp);
        if (status == null) {
            date = DateUtil.getCurrentDate();
        }

        TimeRecordType type = findType(typeId);
        if (type == null) {
            log.error("addBooking() no valid time recording type found [type=" + typeId + "]");
            throw new ClientException("hrm.terminal.error");
        }

        TimeRecording recording = find(stringChipId);
        if (recording == null) {
            throw new ClientException("hrm.terminal.invalid.chip");
        }
        recording.selectCurrentPeriod();
        recording.checkRecordDate(date, type.isStarting());

        if (log.isDebugEnabled()) {
            log.debug("addRecord() add booking [id=" + recording.getId()
                    + ", date=" + DateFormatter.getDateAndTime(date)
                    + ", type=" + type.getId()
                    + "]");
        }
        recording.addRecord(type, getDefaultStatus(), date, deviceId);
        save(recording);
        return date;
    }

    private TimeRecording find(String chipId) throws ClientException {
        long start = System.currentTimeMillis();
        Long id;
        try {
            String tmpId = chipId.substring(chipId.length() - 5);
            id = Long.parseLong(tmpId);
        } catch (Exception e) {
            log.error("parseChipId() can't parse chipId [stringChipId=$stringChipId]");
            throw new ClientException("hrm.terminal.error");
        }
        List<TimeRecording> list = getCurrentSession().createQuery(
                "from TimeRecordingImpl where terminalChipNumber = :chipId")
                .setParameter("chipId", id).list();
        TimeRecording tr = (list.isEmpty() ? null : list.get(0));
        if (log.isDebugEnabled()) {
            log.debug("find() done [chipNumber=" + id
                    + ", id=" + (tr == null ? "null" : tr.getId())
                    + ", duration=" + (System.currentTimeMillis() - start)
                    + "]");
        }
        return createClosingsIfRequired(tr);
    }

    private Date parseDate(String timestamp) throws ClientException {
        Date date;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmm");
            date = sdf.parse(timestamp);
        } catch (Exception e) {
            log.error(" parseDate() can't parse date [timestamp=$timestamp]");
            throw new ClientException("hrm.terminal.error");
        }
        return date;
    }

    private Long parseTerminalId(String stringDeviceId) throws ClientException {
        Long deviceId;
        try {
            deviceId = Long.parseLong(stringDeviceId);
        } catch (Exception e) {
            log.error("parseTerminalId() can't parse deviceId [stringDeviceId=$stringDeviceId]");
            throw new ClientException("hrm.terminal.error");
        }
        return deviceId;
    }

    private Long parseStatus(String stringStatus) throws ClientException {
        Long status = null;
        try {
            if (!stringStatus.equals(" ")) {
                status = Long.parseLong(stringStatus);
            }
        } catch (Exception e) {
            log.error("parseStatus() can't parse status [stringStatus=$stringStatus]");
            throw new ClientException("hrm.terminal.error");
        }
        return status;
    }
}
