/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 1 Apr 2007 15:38:53 
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.dms.DocumentType;

import com.osserp.core.Item;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordVersionDocument;
import com.osserp.core.finance.RecordVersionItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordDocumentVersionImpl extends AbstractRecordDocument implements RecordVersionDocument {

    private List<RecordVersionItem> items = new ArrayList<RecordVersionItem>();

    protected RecordDocumentVersionImpl() {
        super();
    }

    public RecordDocumentVersionImpl(
            Long id,
            Record record,
            DocumentType type,
            String fileName,
            long fileSize,
            Long createdBy,
            String note) {
        super(record.getId(), type, fileName, fileSize, createdBy, note);
        setId(id);
        setReferenceType(record.getType().getId());
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            Item next = record.getItems().get(i);
            RecordVersionItem item = new RecordVersionItemImpl(id, record, next);
            this.items.add(item);
        }
    }

    public List<RecordVersionItem> getItems() {
        return items;
    }

    protected void setItems(List<RecordVersionItem> items) {
        this.items = items;
    }
}
