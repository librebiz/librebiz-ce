/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 3, 2011 1:33:31 PM 
 * 
 */
package com.osserp.core.service.hrm;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.core.dao.hrm.TimeRecordingConfigs;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingConfigManager;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingConfigManagerImpl extends AbstractService implements TimeRecordingConfigManager {

    private TimeRecordingConfigs timeRecordingConfigs;

    protected TimeRecordingConfigManagerImpl(TimeRecordingConfigs timeRecordingConfigs) {
        super();
        this.timeRecordingConfigs = timeRecordingConfigs;
    }

    public TimeRecordingConfig createConfig(Employee user, String name, String description) throws ClientException {
        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (isNotSet(description)) {
            throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
        }
        List<TimeRecordingConfig> configs = findConfigs();
        for (int i = 0, j = configs.size(); i < j; i++) {
            TimeRecordingConfig next = configs.get(i);
            if (next.getName().equalsIgnoreCase(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        return timeRecordingConfigs.createConfig(user, name, description);
    }

    public TimeRecordMarkerType createMarkerType(Employee user, Long id, String name, String description) throws ClientException {
        return timeRecordingConfigs.createMarkerType(user, id, name, description);
    }

    public TimeRecordType createType(Employee user, Long id, String name, String description) throws ClientException {
        return timeRecordingConfigs.createType(user, id, name, description);
    }

    public List<TimeRecordingConfig> findConfigs() {
        return timeRecordingConfigs.getConfigs();
    }

    public List<TimeRecordMarkerType> findMarkerTypes() {
        return timeRecordingConfigs.getMarkerTypes();
    }

    public List<TimeRecordType> findTypes() {
        return timeRecordingConfigs.getTypes();
    }

    public void save(Employee user, TimeRecordingConfig config) {
        timeRecordingConfigs.save(user, config);
    }

    public void save(Employee user, TimeRecordType type) {
        timeRecordingConfigs.save(user, type);
    }

    public void save(Employee user, TimeRecordMarkerType type) {
        timeRecordingConfigs.save(user, type);
    }
}
