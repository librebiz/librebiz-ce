/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 21, 2008 
 * 
 */
package com.osserp.core.model.contacts.groups;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.beans.AbstractRole;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeRole;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.employees.EmployeeStatus;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeRoleConfigImpl extends AbstractRole implements EmployeeRoleConfig {
    private static Logger log = LoggerFactory.getLogger(EmployeeRoleConfigImpl.class.getName());

    private BranchOffice branch = null;
    private List<EmployeeRole> roles = new ArrayList<EmployeeRole>();

    protected EmployeeRoleConfigImpl() {
        super();
    }

    public EmployeeRoleConfigImpl(
            Employee employee,
            BranchOffice branch,
            Employee user,
            boolean defaultConfig) {
        super(user.getId(), employee.getId(), defaultConfig);
        this.branch = branch;
    }

    protected EmployeeRoleConfigImpl(EmployeeRoleConfig o) {
        super(o);
        this.branch = (BranchOffice) o.getBranch().clone();
        if (o.getRoles() != null && !o.getRoles().isEmpty()) {
            roles = new ArrayList<EmployeeRole>();
            for (int i = 0, j = o.getRoles().size(); i < j; i++) {
                EmployeeRole next = o.getRoles().get(i);
                roles.add((EmployeeRole) next.clone());
            }
        }
    }

    public BranchOffice getBranch() {
        return branch;
    }

    protected void setBranch(BranchOffice branch) {
        this.branch = branch;
    }

    public List<EmployeeRole> getRoles() {
        return roles;
    }

    protected void setRoles(List<EmployeeRole> roles) {
        this.roles = roles;
    }

    public EmployeeRole addRole(Employee user, EmployeeGroup group, EmployeeStatus status) {
        boolean alreadyAdded = false;
        for (int i = 0, j = roles.size(); i < j; i++) {
            EmployeeRole next = roles.get(i);
            if (next.getGroup().getId().equals(group.getId())
                    && next.getStatus().getId().equals(status.getId())) {
                alreadyAdded = true;
            }
        }
        if (!alreadyAdded) {
            EmployeeRole role = new EmployeeRoleImpl(
                    user,
                    this,
                    group,
                    status,
                    (roles.size() == 0));
            this.roles.add(role);
            setLastChanged(user);
            return role;
        }
        return null;
    }

    public void removeRole(Employee user, Long id) {
        for (Iterator<EmployeeRole> i = roles.iterator(); i.hasNext();) {
            EmployeeRole next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                setLastChanged(user);
                if (log.isDebugEnabled()) {
                    log.debug("removeRole() done [config="
                            + getId()
                            + ", id=" + id + "]");
                }
                break;
            } else if (log.isDebugEnabled()) {
                log.debug("removeRole() ignoring role [config=" +
                        getId()
                        + ", id=" + next.getId() + "]");
            }
        }
    }

    public void setDefaultRole(Employee user, Long id) {
        for (Iterator<EmployeeRole> i = roles.iterator(); i.hasNext();) {
            EmployeeRole next = i.next();
            if (next.getId().equals(id)) {
                next.setDefaultRole(true);
                setLastChanged(user);
            } else {
                next.setDefaultRole(false);
            }
        }
    }

    protected void setLastChanged(Employee user) {
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(user.getId());
    }

    @Override
    public Object clone() {
        return new EmployeeRoleConfigImpl(this);
    }
}
