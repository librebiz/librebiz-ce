/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 9, 2006 3:50:50 PM 
 * 
 */
package com.osserp.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.osserp.common.ClientException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class NumberUtil {

    public static final int DIGITS_PERCENT = 7;

    /**
     * Checks whether given value is a number
     * @param value
     * @return true if so
     */
    public static boolean isNumber(String value) {
        try {
            NumberFormatter.createDouble(value, true);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * Checks whether given value is an integer
     * @param value
     * @return true if so
     */
    public static boolean isInteger(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * Provides the value of given Double or 0 if object is null
     * @param object
     * @return value
     */
    public static double getDouble(Double object) {
        return ((object == null) ? 0 : object.doubleValue());
    }

    /**
     * Provides the value of given Double or 0 if object is null
     * @param object
     * @return value
     */
    public static BigDecimal getDecimal(BigDecimal object) {
        return ((object == null) ? new BigDecimal(0) : object);
    }

    public static BigDecimal getPercent(BigDecimal total, BigDecimal amount, int scale) {
        return total == null || amount == null ? new BigDecimal(0) :
            amount.divide(
                total.divide(new BigDecimal(100),
                    scale, RoundingMode.HALF_UP), scale, RoundingMode.HALF_UP)
                    .divide(new BigDecimal(100), scale, RoundingMode.HALF_UP);
    }

    /**
     * Tries to create a double from given string.
     * @param value
     * @return created or Double initialized with 0
     */
    public static Double createDouble(String value) {
        Double result = null;
        try {
            result = NumberFormatter.createDouble(value, false);
        } catch (Throwable e) {
        }
        return ((result == null) ? 0d : result);
    }

    /**
     * Tries to create a double by given object.
     * @param object
     * @return created or Double initialized with 0
     */
    public static Double createDouble(Object object) {
        Double result = null;
        if (object instanceof Double) {
            result = (Double) object;
        } else if (object instanceof BigDecimal) {
            result = Double.valueOf(((BigDecimal) object).doubleValue());
        } else {
            try {
                result = NumberFormatter.createDouble(object.toString(), false);
            } catch (Throwable e) {
            }
        }
        return ((result == null) ? 0d : result);
    }

    /**
     * Tries to create an integer from given string.
     * @param value
     * @return created or null
     */
    public static Integer createInteger(String value) {
        try {
            return Integer.valueOf(value);
        } catch (Throwable e) {
            return null;
        }
    }

    /**
     * Tries to create a long from given string.
     * @param value
     * @return created value or null
     */
    public static Long createLong(String value) {
        try {
            return value == null ? null : Long.valueOf(value);
        } catch (Throwable e) {
            // sheets may provide integers as floats
            if (value != null && value.indexOf(".") > -1) {
                Double d = createDouble(value);
                if (d != null && d != 0d) {
                    return d.longValue();
                }
            }
            return null;
        }
    }

    /**
     * Tries to create a long by given object.
     * @param value
     * @return created or null
     */
    public static Long createLong(Object value) {
        if (value != null) {
            try {
                if (value instanceof String) {
                    return Long.valueOf((String) value);
                }
                if (value instanceof Long) {
                    return (Long) value;
                }
                if (value instanceof Integer) {
                    return Long.valueOf(((Integer) value).longValue());
                }
            } catch (Throwable e) {
            }
        }
        return null;
    }

    /**
     * Tries to create a BigDecimal by given string.
     * @param value
     * @return created or 0 if not createable by value (e.g. ignores throwables)
     */
    public static BigDecimal createDecimal(String value) {
        BigDecimal result = null;
        try {
            result = NumberFormatter.createDecimal(value, false);
        } catch (Throwable e) {
        }
        return ((result == null) ? new BigDecimal(0) : result);
    }

    /**
     * Tries to create a BigDecimal by given string.
     * @param value
     * @return created or 0 if not createable by value (e.g. ignores throwables)
     */
    public static BigDecimal fetchDecimal(String value) {
        BigDecimal result = null;
        try {
            result = NumberFormatter.createDecimal(value, false);
        } catch (Throwable e) {
        }
        return ((result == null) ? null : result);
    }

    /**
     * Creates a BigDecimal by given Double.
     * @param value
     * @return created decimal or decimal 0 if value is null
     */
    public static BigDecimal createDecimal(Double value) {
        return (value == null) ? new BigDecimal(0) : new BigDecimal(value);
    }

    /**
     * Creates a BigDecimal by given Double if value exists
     * @param value
     * @return created decimal or null if value is null
     */
    public static BigDecimal fetchDecimal(Double value) {
        return (value == null) ? null : BigDecimal.valueOf(value.doubleValue());
    }

    /**
     * Rounds a double value and cuts of digits as given
     * @param d
     * @return rounded and scaled value
     */
    public static Double round(Double d, int digits) {
        BigDecimal bd = null;
        try {
            bd = round(new BigDecimal(d), digits);
            return bd == null ? 0d : bd.doubleValue();
        } catch (Throwable e) {
            return 0d;
        }
    }

    /**
     * Rounds a double value and cuts of digits as given
     * @param d
     * @return rounded and scaled value
     */
    public static BigDecimal round(BigDecimal d, int digits) {
        if (d == null) {
            return new BigDecimal(0);
        }
        return NumberFormatter.formatPlain(d.toPlainString(), 2);
    }

    /**
     * Rounds a double value and cuts of digits as given
     * @param d
     * @return rounded and scaled value
     */
    public static Double round(double d, int digits) {
        return round(Double.valueOf(d), digits);
    }

    /**
     * Rounds a double value
     * @param d
     * @return rounded
     */
    public static double round(double d) {
        return round(d, 2);
    }

    /**
     * Rounds a double
     * @param d
     * @return rounded
     */
    public static double round(Double d) {
        return round(d, 2);
    }

    public static Double roundDue(Double d) {
        if (d == null) {
            return 0d;
        }
        return roundDue(d.doubleValue());
    }

    public static Double roundDue(double val) {
        if ((val > 0 && val < 0.01) || (val < 0 && val > -0.01)) {
            return 0d;
        }
        return round(Double.valueOf(val), 2);
    }

    public static boolean isZero(BigDecimal bd) {
        if (bd == null) {
            return true;
        }
        return bd.compareTo(BigDecimal.ZERO) == 0 ? true : false;
    }

    public static boolean isZeroTax(Double taxRate) {
        if (taxRate == null || taxRate == 0d || taxRate == 1d) {
            return true;
        }
        return false;
    }

    public static void validateNotNull(BigDecimal value, String name, boolean valueNullAllowed) throws ClientException {
        if (value == null) {
            throw new ClientException("error." + name + ".null");
        }
        if (!valueNullAllowed && value.doubleValue() == 0) {
            throw new ClientException("error." + name + ".0");
        }
    }

    public static void validateNotNull(Double value, String name, boolean valueNullAllowed) throws ClientException {
        if (value == null) {
            throw new ClientException("error." + name + ".null");
        }
        if (!valueNullAllowed && value.doubleValue() == 0) {
            throw new ClientException("error." + name + ".0");
        }
    }

    public static void validateNotNull(Long value, String name, boolean valueNullAllowed) throws ClientException {
        if (value == null) {
            throw new ClientException("error." + name + ".null");
        }
        if (!valueNullAllowed && value.longValue() == 0) {
            throw new ClientException("error." + name + ".0");
        }
    }

    public static void validateNotNull(Integer value, String name, boolean valueNullAllowed) throws ClientException {
        if (value == null) {
            throw new ClientException("error." + name + ".null");
        }
        if (!valueNullAllowed && value.intValue() == 0) {
            throw new ClientException("error." + name + ".0");
        }
    }

    public static void validateLessThan(BigDecimal value, BigDecimal as, String name) throws ClientException {
        if (value != null && as != null && value.doubleValue() < as.doubleValue()) {
            throw new ClientException("error." + name + ".less.than." + as);
        }
    }

    public static void validateLessThan(Double value, Double as, String name) throws ClientException {
        if (value != null && as != null && value.doubleValue() < as.doubleValue()) {
            throw new ClientException("error." + name + ".less.than." + as);
        }
    }

    public static void validateLessThan(Long value, Long as, String name) throws ClientException {
        if (value != null && as != null && value.longValue() < as.longValue()) {
            throw new ClientException("error." + name + ".less.than." + as);
        }
    }

    public static void validateLessThan(Integer value, Integer as, String name) throws ClientException {
        if (value != null && as != null && value.intValue() < as.intValue()) {
            throw new ClientException("error." + name + ".less.than." + as);
        }
    }

    public static void validateGreaterThan(BigDecimal value, BigDecimal as, String name) throws ClientException {
        if (value != null && as != null && value.doubleValue() > as.doubleValue()) {
            throw new ClientException("error." + name + ".greater.than." + as);
        }
    }

    public static void validateGreaterThan(Double value, Double as, String name) throws ClientException {
        if (value != null && as != null && value.doubleValue() > as.doubleValue()) {
            throw new ClientException("error." + name + ".greater.than." + as);
        }
    }

    public static void validateGreaterThan(Long value, Long as, String name) throws ClientException {
        if (value != null && as != null && value.longValue() > as.longValue()) {
            throw new ClientException("error." + name + ".greater.than." + as);
        }
    }

    public static void validateGreaterThan(Integer value, Integer as, String name) throws ClientException {
        if (value != null && as != null && value.intValue() > as.intValue()) {
            throw new ClientException("error." + name + ".greater.than." + as);
        }
    }

}
