<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestFcsView"/>

<c:set var="salesRequest" value="${view.businessCaseView.request}"/>
<c:set var="salesContext" scope="request" value="false"/>
<c:set var="popup" scope="request" value="false"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="${viewdir}/fcs/flowControl.css"/>
</style>
</tiles:put>
<tiles:put name="headline">
    <c:if test="${view.wastebasketMode}">
        FCS-<fmt:message key="trash"/> - 
    </c:if>
    <c:if test="${salesRequest.cancelled}">
        <span class="bolderror"><fmt:message key="cancelled"/>: </span>
    </c:if>
    <o:out value="${salesRequest.status}"/> % 
    - <o:out value="${salesRequest.requestId}"/> - <o:out value="${salesRequest.name}" limit="45"/>
</tiles:put>
<tiles:put name="headline_right">
    <v:navigation/>
</tiles:put>

<tiles:put name="content" type="string">
    <div class="content-area" id="requestFcsContent">
        <div class="row">
            <c:choose>
                <c:when test="${view.editMode}">
                    <c:import url="${viewdir}/fcs/_flowControlEditor.jsp"/>
                </c:when>
                <c:when test="${view.wastebasketMode}">
                    <div class="col-lg-12 panel-area">
                        <c:import url="${viewdir}/fcs/_flowControlWastebasket.jsp"/>
                    </div>
                </c:when>
                <c:when test="${!salesRequest.closed and !salesRequest.cancelled}">
                    <c:import url="${viewdir}/fcs/_flowControlDisplay.jsp"/>
                </c:when>
                <c:otherwise>
                    <div class="col-lg-12 panel-area">
                        <c:import url="${viewdir}/fcs/_flowControlTableClosed.jsp"/>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</tiles:put>
</tiles:insert>
