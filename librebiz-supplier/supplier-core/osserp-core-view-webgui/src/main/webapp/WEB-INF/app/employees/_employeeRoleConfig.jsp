<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeRoleConfigView"/>
<c:set var="availableBranchs" value="${view.availableBranchs}"/>
<style type="text/css">
.inputSelect {
    width: 200px;
}       
</style>
<c:if test="${!empty view}">
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <fmt:message key="branchAndRoleConfig"/>
        </div>
        <div class="modalBoxHeaderRight">
            <div class="boxnav">
                <ul>
                    <c:choose>
                        <c:when test="${view.createMode}">
                            <li><v:ajaxLink url="/employees/employeeRoleConfig/disableCreateMode" linkId="exitLink" targetElement="employeeRoleConfig_popup"><o:img name="backIcon"/></v:ajaxLink></li>
                        </c:when>
                        <c:when test="${empty view.selectedConfig and !view.createMode}">
                            <c:if test="${!empty availableBranchs}">
                                <o:permission role="hrm,client_edit,executive" info="permissionEmployeeAddBranches">
                                    <li><v:ajaxLink url="/employees/employeeRoleConfig/enableCreateMode" linkId="createLink" targetElement="employeeRoleConfig_popup" title="addBranch"><o:img name="newdataIcon"/></v:ajaxLink></li>
                                </o:permission>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <li><v:ajaxLink url="/employees/employeeRoleConfig/selectConfig" linkId="exitLink" targetElement="employeeRoleConfig_popup" title="backToLast"><o:img name="backIcon"/></v:ajaxLink></li>
                            <li><v:ajaxLink url="/employees/employeeRoleConfig/enableCreateMode" linkId="createLink" targetElement="employeeRoleConfig_popup" title="addRole"><o:img name="newdataIcon"/></v:ajaxLink></li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
        </div>
    </div>
    <div class="modalBoxData">
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<c:choose>
                    <c:when test="${empty view.selectedConfig and view.createMode}">
                        <%-- CREATE ROLE CONFIG --%>
                        <o:logger write="create or edit mode enabled..." level="debug"/>
                        <v:ajaxForm name="dynamicForm" targetElement="employeeRoleConfig_popup" url="/employees/employeeRoleConfig/createConfig">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2"><fmt:message key="addBranch"/></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${!empty sessionScope.errors}">
                                        <c:set var="errorsColspanCount" scope="request" value="2"/>
                                        <c:import url="/errors/_errors_table_entry.jsp"/>
                                    </c:if>
                                    <tr>
                                        <td><fmt:message key="branchOffice"/></td>
                                        <td>
                                            <select name="branch" class="inputSelect" size="1">
                                                <c:forEach var="branch" items="${availableBranchs}">
                                                    <option value="<o:out value="${branch.id}"/>"><o:out value="${branch.name}"/> / <o:out value="${branch.company.name}"/></option>
                                                </c:forEach>
                                            </select> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="comment"/> (<fmt:message key="optionally"/>)</td>
                                        <td><textarea class="form-control" name="description" rows="4"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td> </td>
                                        <td style="text-align: right;"><input type="submit" class="submit" value="<fmt:message key="save"/>" /></td>
                                    </tr>
                                </tbody>  
                            </table>
                        </v:ajaxForm>  
                    </c:when>
                    <c:when test="${view.createMode}">
                        <%-- CREATE ROLE ON SELECTED CONFIG --%>
                        <v:ajaxForm name="dynamicForm" targetElement="employeeRoleConfig_popup" url="/employees/employeeRoleConfig/createRole">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2"><fmt:message key="createRole"/></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${!empty sessionScope.errors}">
                                        <c:set var="errorsColspanCount" scope="request" value="2"/>
                                        <c:import url="/errors/_errors_table_entry.jsp"/>
                                    </c:if>
                                    <tr>
                                        <td><fmt:message key="group"/></td>
                                        <td>
                                            <select name="group" class="inputSelect" size="1">
                                                <c:forEach var="obj" items="${view.availableGroups}">
                                                    <option value="<o:out value="${obj.id}"/>"><o:out value="${obj.name}"/></option>
                                                </c:forEach>
                                            </select> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="status"/></td>
                                        <td>
                                            <select name="status" class="inputSelect" size="1">
                                                <c:forEach var="obj" items="${view.availableStatus}">
                                                    <option value="<o:out value="${obj.id}"/>"><o:out value="${obj.name}"/></option>
                                                </c:forEach>
                                            </select> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> </td>
                                        <td style="text-align: right;"><input type="submit" class="submit" value="<fmt:message key="save"/>" /></td>
                                    </tr>
                                </tbody>  
                            </table>
                        </v:ajaxForm>  
                    </c:when>
                    <c:when test="${empty view.selectedConfig}">
                        <table class="table">
                            <c:choose>
                                <c:when test="${empty view.employee.roleConfigs}">
                                    <tr>
                                        <td colspan="3">
                                            <fmt:message key="noBranchConfigAvailable"/>
                                        </td>
                                    </tr>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach var="config" items="${view.employee.roleConfigs}">
                                        <tr class="altrow">
                                            <td class="shortkey">
                                                <o:permission role="hrm,client_edit,executive" info="permissionEmployeeEditBranches">
                                                    <v:ajaxLink url="/employees/employeeRoleConfig/selectConfig?id=${config.id}" linkId="selectLink" targetElement="employeeRoleConfig_popup">
                                                        <o:out value="${config.branch.shortkey}"/>
                                                    </v:ajaxLink>
                                                </o:permission>
                                                <o:forbidden role="hrm,client_edit,executive">
                                                    <o:out value="${config.branch.shortkey}"/>
                                                </o:forbidden>
                                            </td>
                                            <c:choose>
                                                <c:when test="${config.defaultRole}">
                                                    <td class="name boldtext" style=""><o:out value="${config.branch.shortname}"/> - <o:out value="${config.branch.name}"/></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="name"><o:out value="${config.branch.shortname}"/> - <o:out value="${config.branch.name}"/></td>
                                                </c:otherwise>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${config.branch.headquarter}">
                                                    <c:choose>
                                                        <c:when test="${config.defaultRole}">
                                                            <td class="hqFlag boldtext">HQ</td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td class="hqFlag">
                                                                <v:ajaxLink url="/employees/employeeRoleConfig/setDefaultConfig?id=${config.id}" linkId="selectLink" targetElement="employeeRoleConfig_popup" title="setDefaultRoleConfig">HQ</v:ajaxLink>
                                                            </td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:choose>
                                                        <c:when test="${config.defaultRole}">
                                                            <td class="hqFlag boldtext">NL</td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td class="hqFlag">
                                                                <v:ajaxLink url="/employees/employeeRoleConfig/setDefaultConfig?id=${config.id}" linkId="selectLink" targetElement="employeeRoleConfig_popup" title="setDefaultRoleConfig">NL</v:ajaxLink>
                                                            </td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:otherwise>
                                            </c:choose>
                                        </tr>
                                        <c:forEach var="role" items="${config.roles}">
                                            <tr>
                                                <td class="shortkey"><o:out value="${role.group.key}"/></td>
                                                <td class="name"><o:out value="${role.group.name}"/> / <o:out value="${role.status.name}"/></td>
                                                <c:choose>
                                                    <c:when test="${role.defaultRole}">
                                                        <td class="hqFlag">D</td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <td class="hqFlag">S</td>
                                                    </c:otherwise>
                                                </c:choose>
                                            </tr>
                                        </c:forEach>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <table class="table">
                            <tr class="altrow">
                                <td class="shortkey"><o:out value="${view.selectedConfig.branch.shortkey}"/></td>
                                <td class="name"><o:out value="${view.selectedConfig.branch.shortname}"/> - <o:out value="${view.selectedConfig.branch.name}"/></td>
                                <c:choose>
                                    <c:when test="${view.selectedConfig.branch.headquarter}">
                                        <td class="hqFlag">HQ</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="hqFlag">NL</td>
                                    </c:otherwise>
                                </c:choose>
                                <td class="action">
                                    <v:ajaxLink url="/employees/employeeRoleConfig/removeConfig?id=${view.selectedConfig.id}" linkId="selectLink" targetElement="employeeRoleConfig_popup" title="deleteRoleConfig">
                                        <o:img name="deleteIcon" styleClass="bigicon"/>
                                    </v:ajaxLink>
                                </td>
                            </tr>
                            <c:forEach var="role" items="${view.selectedConfig.roles}">
                                <tr>
                                    <td class="shortkey"><o:out value="${role.group.key}"/></td>
                                    <td class="name"><o:out value="${role.group.name}"/> / <o:out value="${role.status.name}"/></td>
                                    <c:choose>
                                        <c:when test="${role.defaultRole}">
                                            <td class="hqFlag">D</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td class="hqFlag">
                                                <v:ajaxLink url="/employees/employeeRoleConfig/setDefaultRole?id=${role.id}" linkId="selectLink" targetElement="employeeRoleConfig_popup" title="markAsMainRole">S</v:ajaxLink>
                                            </td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td class="action">
                                        <v:ajaxLink url="/employees/employeeRoleConfig/removeRole?id=${role.id}" linkId="selectLink" targetElement="employeeRoleConfig_popup" title="deleteRole">
                                            <o:img name="deleteIcon" styleClass="bigicon"/>
                                        </v:ajaxLink>
                                    </td>   
                                </tr>
                            </c:forEach>
                        </table>
                    </c:otherwise>
				</c:choose>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</c:if>