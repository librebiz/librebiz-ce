/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 2, 2006 4:13:17 PM 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jdom2.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.calc.CalculatorService;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.sales.SalesRevenue;
import com.osserp.core.sales.SalesRevenueManager;
import com.osserp.core.sales.SalesUtil;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.records.impl.AbstractOrderView;
import com.osserp.gui.client.sales.SalesOrderView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesOrderView")
public class SalesOrderViewImpl extends AbstractOrderView implements SalesOrderView {
    private static Logger log = LoggerFactory.getLogger(SalesOrderViewImpl.class.getName());
    private List<RecordDisplay> purchaseRecords = new ArrayList<>();
    private Long purchasingProductSelectionId = null;
    private BusinessCaseView businessCaseView = null;
    private String packageListStylesheetName = null;
    private boolean logisticsDisplayMode = false;

    protected SalesOrderViewImpl() {
        super();
    }

    protected SalesOrderViewImpl(String packageListStylesheetName, String confirmDiscountPermissions) {
        super(confirmDiscountPermissions);
        this.packageListStylesheetName = packageListStylesheetName;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        purchasingProductSelectionId = NumberUtil.createLong(
                getSystemConfigManager().getSystemProperty("salesOrderPurchasingProductSelectionConfig"));
    }

    public Sales getRelatedSales() {
        return businessCaseView == null ? null : businessCaseView.getSales();
    }

    public Order getOrder() {
        Record record = getRecord();
        if (!(record instanceof Order)) {
            throw new ActionException(ErrorCode.ORDER_MISSING);
        }
        return (Order) record;
    }

    public byte[] createPackagelist() throws ClientException {
        CoreStylesheetService xlsManager = (CoreStylesheetService) getService(CoreStylesheetService.class.getName());
        Document doc = getRecordDocument(false);
        return xlsManager.createPdf(doc, packageListStylesheetName);
    }

    @Override
    public boolean isSupportingCustomDelivery() {
        return getRelatedSales() != null &&
                getRelatedSales().getType().isSupportingCustomDelivery() &&
                getRelatedSales().getType().getCustomDeliveryNoteType() != null;
    }

    @Override
    public Long getCustomDeliveryType() {
        return (getRelatedSales() != null && getRelatedSales().getType().getCustomDeliveryNoteType() != null)
                ? getRelatedSales().getType().getCustomDeliveryNoteType() : null;
    }

    public void load(BusinessCaseView businessCaseView) throws ClientException {
        setBusinessCaseView(businessCaseView);
        setExitTarget(businessCaseView.getForwardTarget());
        SalesOrderManager manager = getOrderManager();
        Order order = manager.getBySales(businessCaseView.getSales());
        if (order != null) {
            setRecord(order);
            if (log.isDebugEnabled()) {
                log.debug("load() done for business case ["
                    + businessCaseView.getId()
                    + "], order ["
                    + order.getId()
                    + "], open deliveries ["
                    + (getOpenDeliveries() == null ? "null]" : this
                            .getOpenDeliveries().size()
                            + "]"));
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("load() did not find related order, trying to create empty [sales=" + 
                    + businessCaseView.getId() + "]");
            }
            order = manager.createMissing(getDomainEmployee(), businessCaseView.getSales());
            setRecord(order);
        }
        loadBusinessTemplates(businessCaseView.getSales());
    }


    @Override
    public void save(Form form) throws ClientException, PermissionException {
        Order record = (Order) getRecord();
        SalesOrderManager manager = getOrderManager();
        manager.update(
                record,
                form.getLong("signatureLeft"),
                form.getLong("signatureRight"),
                form.getLong("personId"),
                form.getString("note"),
                form.getBoolean("taxFree"),
                form.getLong("taxFreeId"),
                form.getDouble("taxRate"),
                form.getDouble("reducedTaxRate"),
                form.getLong("currency"),
                form.getString("language"),
                form.getLong("branch"),
                form.getLong("shippingId"),
                form.getString("customHeader"),
                form.getBoolean("printComplimentaryClose"),
                form.getBoolean("printProjectmanager"),
                form.getBoolean("printSalesperson"),
                form.getBoolean("printBusinessCaseId"),
                form.getBoolean("printBusinessCaseInfo"),
                form.getBoolean("printRecordDate"),
                form.getBoolean("printRecordDateByStatus"),
                form.getBoolean("printPaymentTarget"),
                form.getBoolean("printConfirmationPlaceholder"),
                form.getBoolean("printCoverLetter"));
        boolean includeKanban = form.getBoolean("kanbanDelivery");
        if (record.isKanbanDeliveryEnabled() != includeKanban) {
            record.setKanbanDeliveryEnabled(includeKanban);
            manager.persist(record);
        }
        refresh();
        savePrintOptionDefaults(form, getRecord());
    }

    @Override
    public void refresh() {
        SalesOrder order = (SalesOrder) getRecordManager().find(getOrder().getId());
        if (log.isDebugEnabled()) {
            log.debug("refresh() invoked [order=" + order.getId() + "]");
        }
        if (!order.isCanceled() && !order.isClosed()) {
            order.refreshDeliveries();
            getRecordManager().persist(order);
        }
        setRecord(order);
        if (log.isDebugEnabled()) {
            log.debug("refresh() done [record=" + order.getId() + "]");
        }
    }

    public boolean isDiscountPercent() {
        SalesOrder order = (SalesOrder) getOrder();
        if (order == null) {
            log.warn("isDiscountPercent() invoked when even no order selected!");
            return false;
        }
        return CalculatorService.CALCULATOR_BY_ITEMS.equals(order.getCalculatorName());
    }

    @Override
    protected void checkDiscount() throws ClientException {
        SalesOrder order = (SalesOrder) getOrder();
        Sales sales = getRelatedSales();
        if (sales == null || sales.getType().isWholeSale()) {
            setRequiredDiscount(0d);
        } else {
            SalesRevenueManager salesRevenueManager = getSalesRevenueManager();
            try {
                salesRevenueManager.checkDiscount(getDomainUser(), order, sales);
            } catch (Exception e) {
                SalesRevenue revenue = salesRevenueManager.getRevenue(order.getId());
                if (revenue.isProfitLowerMinimum()) {
                    setRequiredDiscount(revenue.getMissingMarginPercent());
                } else {
                    setRequiredDiscount(0d);
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("setDiscountByItems() done [sales=" + sales.getId()
                        + ", order=" + order.getId()
                        + ", requiredDiscount=" + getRequiredDiscount()
                        + "]");
            }
        }
    }

    public boolean isSupportingRevenueOnly() {
        Sales sales = getRelatedSales();
        return (sales != null && SalesUtil.isSupportingRevenueOnly(sales));
    }

    public List<RecordDisplay> getPurchaseRecords() {
        return purchaseRecords;
    }

    public Long getPurchasingProductSelectionId() {
        return purchasingProductSelectionId;
    }

    public void reopen() throws ClientException {
        Sales sales = getRelatedSales();
        SalesOrderManager manager = getOrderManager();
        manager.reopen(getDomainUser().getEmployee(), sales);
        setRecord(manager.getBySales(sales));
    }

    @Override
    public void createNewVersion() throws PermissionException {
        SalesOrderManager manager = getOrderManager();
        Sales sales = getRelatedSales();
        manager.createNewVersion(getDomainUser().getEmployee(), sales, false, null);
        setRecord(manager.getBySales(sales));
    }

    @Override
    public void setRecord(Record record) {
        super.setRecord(record);
        if (isSet(record.getBusinessCaseId())) {
            RecordSearch recordSearch = (RecordSearch) getService(RecordSearch.class.getName());
            purchaseRecords = recordSearch.findPurchases(record.getBusinessCaseId());
            if (log.isDebugEnabled()) {
                log.debug("setRecord() done [relatedPurchaseOrderCount=" + purchaseRecords.size() + "]");
            }
        }
    }

    public boolean isLogisticsDisplayMode() {
        return logisticsDisplayMode;
    }

    public void setLogisticsDisplayMode(boolean logisticsDisplayMode) {
        this.logisticsDisplayMode = logisticsDisplayMode;
    }

    public BusinessCaseView getBusinessCaseView() {
        return businessCaseView;
    }

    protected void setBusinessCaseView(BusinessCaseView businessCaseView) {
        this.businessCaseView = businessCaseView;
    }

    protected CoreStylesheetService getCoreStylesheetService() {
        return (CoreStylesheetService) getService(CoreStylesheetService.class.getName());
    }

    protected SalesRevenueManager getSalesRevenueManager() {
        return (SalesRevenueManager) getService(SalesRevenueManager.class.getName());
    }

    @Override
    public RecordManager getRecordManager() {
        return getOrderManager();
    }

    @Override
    protected SalesOrderManager getOrderManager() {
        return (SalesOrderManager) getService(SalesOrderManager.class.getName());
    }
}
