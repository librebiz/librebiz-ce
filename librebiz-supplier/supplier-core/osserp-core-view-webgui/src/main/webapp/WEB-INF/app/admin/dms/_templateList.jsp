<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.templateView}"/>
<c:set var="dmsUrl" value="${requestScope.templateDocumentUrl}"/>
<c:set var="deletePermissions" value="${requestScope.templateDeletePermissions}"/>
<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="docListName"><fmt:message key="filename" /></th>
                    <th class="docListNote"><fmt:message key="comment"/></th>
                    <th class="action center" colspan="2"><fmt:message key="action" /></th>
                </tr>
            </thead>
            <tbody>
                <c:if test="${empty view.list}">
                    <tr><td colspan="3"><fmt:message key="thereIsNoDocumentYet" /></td></tr>
                </c:if>
                <c:forEach items="${view.list}" var="doc">
                    <tr>
                        <td class="docListName"><o:doc obj="${doc}"><o:out value="${doc.fileName}"/></o:doc></td>
                        <td class="docListNote"><o:out value="${doc.note}"/></td>
                        <td class="icon center">
                            <v:link url="${dmsUrl}/select?id=${doc.id}"><o:img name="writeIcon"/></v:link>
                        </td>
                        <td class="icon center">
                            <c:if test="${view.documentDeleteSupported}">
                                <o:permission role="${deletePermissions}" info="permissionDocumentsDelete">
                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="${dmsUrl}/delete?id=${doc.id}" />');" title="<fmt:message key="deleteDocument"/>">
                                        <o:img name="deleteIcon"/>
                                    </a>
                                </o:permission>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
