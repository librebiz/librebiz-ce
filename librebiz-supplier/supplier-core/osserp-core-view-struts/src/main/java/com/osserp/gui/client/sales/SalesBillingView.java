/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 15, 2007 2:01:41 PM 
 * 
 */
package com.osserp.gui.client.sales;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.LockException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.customers.Customer;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.products.Product;
import com.osserp.core.sales.Sales;
import com.osserp.gui.client.BusinessCaseDependingView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesBillingView")
public interface SalesBillingView extends BusinessCaseDependingView {

    /**
     * Indicates that view was created in flow control context
     * @return flowControlContext
     */
    public boolean isFlowControlContext();

    /**
     * Provides the order billing belongs to
     * @return order
     */
    public Order getOrder();

    /**
     * Provides the sales current order belongs to
     * @return sale
     */
    public Sales getSale();

    /**
     * Indicates that the current order is not valid so recalculation required
     * @return true if so
     */
    public boolean isRecalculationRequired();

    /**
     * Indicates availability of unpaid downpayments
     * @return true if unpaid downpayments found
     */
    public boolean isUnpaidDownpaymentsAvailable();

    /**
     * Indicates if unpaid downpayments should be canceled by default
     * @return true to cancel by default
     */
    public boolean isCancelUnpaidDownpaymentsDefault();

    /**
     * Provides all available invoice types
     * @return invoiceTypes
     */
    public List<BillingType> getInvoiceTypes();

    /**
     * The selected billing type
     * @return billingType
     */
    public Long getBillingType();

    /**
     * Provides billing type by current selected fcs action
     * @return billing type
     */
    public Long getBillingTypeByCurrentFcs();

    /**
     * Sets the selected billing type
     * @param billingType
     */
    public void setBillingType(Long billingType) throws ClientException;

    /**
     * The action target when used in foreign context
     * @return target
     */
    public String getTarget();

    /**
     * Sets the action target when used in foreign context
     * @param target
     */
    public void setTarget(String target);

    /**
     * The invoice of the related project
     * @return invoices
     */
    public List<Invoice> getInvoices();

    /**
     * Provides the invoice related to given credit note id
     * @param creditNoteId
     * @return invoice
     */
    public Invoice getInvoiceByCreditNote(Long creditNoteId);

    /**
     * The downpayments of the related project
     * @return downpayments
     */
    public List<Invoice> getDownpayments();

    /**
     * Provides canceled downpayments
     * @return list of canceled downpayments or empty if none exists.
     */
    public List<Downpayment> getCanceledDownpayments();

    /**
     * Indicates that billing contains invoices whose are not unchangeable
     * @return true if so
     */
    public boolean hasChangeableInvoices();

    /**
     * Indicates that current sale has open invoices wich is true if account ballance greater than 0.
     * @return true if so
     */
    public boolean hasOpenInvoices();

    /**
     * Indicates that billing has an invoice while account ballance is 0
     * @return true if so
     */
    public boolean hasPrepaidInvoice();

    /**
     * Provides all invoices of current project including final invoice if exists
     * @return all invoices
     */
    public List<Invoice> getAllInvoices();

    /**
     * Provides third party invoices if available
     * @return thirdPartyInvoices
     */
    public List<Invoice> getThirdPartyInvoices();

    /**
     * Tries to fetch invoice by id
     * @param id
     * @param recordType
     * @return invoice
     */
    public Invoice fetchInvoice(Long id, Long recordType);

    /**
     * Provides the payment agreement for current project
     * @return paymentAgreement
     */
    public RecordPaymentAgreement getPaymentAgreement();

    /**
     * Privides the current account balance
     * @return accountBalance
     */
    public Double getAccountBalance();

    /**
     * Adds an invoice to project
     * @param type of the invoice
     * @param form optional request params
     * @return new created invoice
     * @throws ClientException if validation failed
     */
    public Invoice addInvoice(Long type, Form form) throws ClientException;

    /**
     * Adds an invoice to project
     * @param form
     * @return new created invoice
     * @throws ClientException if invoice already exists or created is null
     */
    public Invoice addPartialInvoice(Form form) throws ClientException;

    /**
     * Deletes an invoice and related payments
     * @param invoice
     * @throws ClientException if invoice is unchangeable
     */
    public void deleteInvoice(Invoice invoice) throws ClientException;

    /**
     * Deletes a payment
     * @param id
     */
    public void deletePayment(Long id);

    /**
     * Provides all available business cases for creating partial invoices
     * @return business case products
     */
    public List<Product> getBusinessCases();


    /**
     * Indicates if customized record number input is enabled
     * @return customNumberEnabled
     */
    public boolean isCustomNumberEnabled();

    /**
     * Indicates if customized date input is enabled
     * @return the customDateEnabled
     */
    public boolean isCustomDateEnabled();

    /**
     * Adds a third party recipient for next record
     * @param id
     */
    public void addThirdPartyRecipient(Long id);

    /**
     * Provides third party recipient if added for next record
     * @return third party customer
     */
    public Customer getThirdParty();

    /**
     * Reloads values from backend
     */
    public void reload();

    public void checkFinalOrderState() throws ClientException;

    public void checkLock() throws LockException;
}
