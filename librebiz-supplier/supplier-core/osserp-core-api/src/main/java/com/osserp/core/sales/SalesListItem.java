/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 13, 2006 3:48:08 PM 
 * 
 */
package com.osserp.core.sales;

import java.util.Date;

import com.osserp.core.requests.RequestListItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesListItem extends RequestListItem {

    /**
     * The id of the request project's based on
     * @return requestId
     */
    Long getRequestId();

    /**
     * Provides the capacity of the sales
     * @return capacity
     */
    Double getCapacity();

    /**
     * Indicates that sales is currently stopped
     * @return stopped
     */
    boolean isStopped();

    /**
     * Indicates that sales is released
     * @return released
     */
    boolean isReleased();

    /**
     * Indicates that sales is delivered
     * @return billed
     */
    boolean isDeliveryClosed();

    /**
     * Provides manager substitute id
     * @return managerSubId
     */
    Long getManagerSubId();

    /**
     * Provides the related installer if available
     * @return installerId or null if not exists
     */
    Long getInstallerId();

    /**
     * Provides the installation date
     * @return installation date
     */
    Date getInstallationDate();

    /**
     * Provides the count of days for installation
     * @return installationDays
     */
    int getInstallationDays();

    /**
     * Provides the last action date
     * @return lastActionDate
     */
    Date getLastActionDate();

    /**
     * Indicates that sales is vacant, e.g. not deliverable when planned
     * @return vacant
     */
    boolean isVacant();

    /**
     * Enables/disables vacancy status
     * @param vacant
     */
    void setVacant(boolean vacant);

    /**
     * Confirmation date
     * @return date
     */
    Date getConfirmationDate();

    Long getSupplierId();

    String getSupplierName();

    String getSupplierGroup();
}
