/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web

import com.osserp.common.ClientException
import com.osserp.common.PermissionException
import com.osserp.common.User
import com.osserp.common.web.Form


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public interface View {

    static final String CURRENT = 'currentView'
    static final String LAST_REQUESTED = 'lastRequestedView'

    /**
     * Initializes a new request. Method is invoked by view context listener.
     * @param atts of the view
     * @param headers of the request
     * @param params of the request 
     */
    void initRequest(Map atts, Map headers, Map params)

    /**
     * Indicates availability of a customized view navigation. 
     * Forces invocation of generateNavigation in viewTag if enabled.
     * @return true if custom navigation is available
     */
    boolean isCustomNavigationAvailable()

    /**
     * Generates the view navigation. 
     */
    void generateNavigation()

    /**
     * Provides the values deployed by latest invocation of initRequest method.
     * @return form with latest deployed request param values
     */
    Form getForm()

    /**
     * Provides dependency informations
     * @return dependencies
     */
    List<String> getDependencies()

    /**
     * Provides current navigation items
     * @return navigation
     */
    List<MenuItem> getNavigation()

    /**
     * Provides current page header as i18n key
     * @return headerName
     */
    String getHeaderName()

    /**
     * Provides the context name of the view (name in associated scope)
     * @return name
     */
    String getName()

    /**
     * Provides the domain object bean this view handles  
     * @return bean
     */
    Object getBean()

    /**
     * Provides the environment of the view  
     * @return env
     */
    Map<String, Object> getEnv()

    /**
     * Provides a list
     * @return list
     */
    List<? extends Object> getList()

    /**
     * Provides the count of objects provided by current list    
     * @return list.size
     */
    Integer getListCount()

    /**
     * Provides the list starting position if view supports paging 
     * @return list pager start
     */
    Integer getListStart()

    /**
     * Provides the list end position if view supports paging
     * @return list pager start
     */
    Integer getListEnd()

    /**
     * Number of items in paging mod
     * @return paging list step
     */
    Integer getListStep()

    /**
     * Indicates if current list snippet is first page
     * @return true if first page in paging mode 
     */
    boolean isFirstList()

    /**
     * Indicates if current list snippet is last page
     * @return true if last page in paging mode 
     */
    boolean isLastList()

    /**
     * Initializes the listStep property    
     * @param initialListStep
     */
    void initListStep(int initialListStep)

    /**
     * Moves pager to next 
     */
    void listNext()

    /**
     * Moves pager to previous
     */
    void listPrevious()

    /**
     * The resulting list of objects created by collect method invocation
     * @return collected
     */
    List<? extends Object> getCollected()

    /**
     * Indicates wether view has create mode enabled
     * @return true if create mode enabled   
     */
    boolean isCreateMode()

    /**
     * Provides local link to enable create mode
     * @return createLink
     */
    MenuItem getCreateLink()

    /**
     * Provides local link to disable create mode
     * @return disableCreateLink
     */
    MenuItem getDisableCreateLink()

    /**
     * Disables create mode
     */
    void disableCreateMode()

    /**
     * Enables create mode
     */
    void enableCreateMode()

    /**
     * Indicates wether view has setup mode enabled
     * @return true if setup mode enabled
     */
    boolean isSetupMode()

    /**
     * Provides local link to enable setup mode
     * @return setupLink
     */
    MenuItem getSetupLink()

    /**
     * Provides local link to disable setup mode
     * @return disableSetupLink
     */
    MenuItem getDisableSetupLink()

    /**
     * Disables setup mode
     */
    void disableSetupMode()

    /**
     * Enables setup mode
     */
    void enableSetupMode()

    /**
     * Indicates wether view has edit mode enabled
     * @return true if edit mode enabled   
     */
    boolean isEditMode()

    /**
     * Provides local link to enable edit mode
     * @return editLink
     */
    MenuItem getEditLink()

    /**
     * Provides local link to disable edit mode
     * @return disableEditLink
     */
    MenuItem getDisableEditLink()

    /**
     * Disables edit mode
     */
    void disableEditMode()

    /**
     * Enables edit mode
     */
    void enableEditMode()

    /**
     * Reloads current bean and/or list if view provides this.
     */
    void reload()

    /**
     * Indicates that view should be reloaded by controller on reload
     * @return true if reload should be invoked
     */
    boolean isAutoreloadEnabled()

    /**
     * Indicates external invocation. This property is mostly used to limit
     * view workflow to a dedicated bean, e.g. a forward with an id. This 
     * disables the selection on list based views. 
     * @return true if external invocation mode enabled.
     */
    boolean isExternalInvocationMode()

    /**
     * Disables external invocation mode
     */
    void disableExternalInvocationMode()

    /**
     * Enables external invocation mode
     */
    void enableExternalInvocationMode()

    /**
     * Resets all view environment except context
     */
    void reset()

    /**
     * Performs save action. Normally persists selected bean.
     * @throws ClientException if validation of input values and state failed
     * @throws PermissionException if current user lacks required permission
     */
    void save() throws ClientException, PermissionException

    /**
     * Provides local link to enable edit mode
     * @return editLink
     */
    MenuItem getSaveLink()

    /**
     * Provides select exit link (e.g. empty select)
     * @return selectExitLink
     */
    MenuItem getSelectExitLink()

    /**
     * Provides select link
     * @return selectLink
     */
    MenuItem getSelectLink()

    /**
     * Selects an object from list and adds the object to list 'collected'
     */
    void collect()

    /**
     * Deselects an object from collected and adds the object to list
     */
    void removeCollected()

    /**
     * Selects object by selected id from local list 
     */
    void select()

    /**
     * Provides the selection exit target
     * @return selection exit
     */
    String getSelectionExit()

    /**
     * Provides the selection target
     * @return selection target
     */
    String getSelectionTarget()

    /**
     * Provides the selection target id param name
     * @return selection target id param name
     */
    String getSelectionTargetIdParam()

    /**
     * Provides the exit id
     * @return exitId
     */
    String getExitId()

    /**
     * Provides the exit target of the view
     * @return exitTarget
     */
    String getExitTarget()

    /**
     * Provides the home target of the view
     * @return homeTarget
     */
    String getHomeTarget()

    /**
     * Provides the actual scroll position
     * @return actual scroll position
     */
    Long getScrollPosition()

    /**
     * Sets the actual scroll position
     * @param scroll position
     */
    void setScrollPosition(Long position)

    /**
     * Sorts current list
     */
    void sort()

    /**
     * Indicates if current user has required permissions
     * @param permissions
     * @return true if permission grant
     */
    Boolean isPermissionGrant(String[] permissions)

    /**
     * Indicates if current user has required permissions
     * @param permissions
     * @return true if permission grant
     */
    Boolean isPermissionGrant(String permissions)

    /**
     * Indicates that view should be removed when controller invokes exit target.
     * This is true by default. Set 'env.destroyViewOnExit = false' to change.
     * @return true if view should be removed.
     */
    boolean isDestroyViewOnExit()

    /**
     * Indicates that view should be removed when controller invokes home target.
     * This is true by default. Set 'env.destroyViewOnHome = false' to change.
     * @return true if view should be removed.
     */
    boolean isDestroyViewOnHome()

    /**
     * Indicates whether the view is a popup or not
     */
    boolean isPopupView()

    /**
     * Indicates if successful invocation of save method should return with calling exit
     * @return true if exit should be invoked after save
     */
    boolean isPopupSaveExit()

    /**
     * Indicates if successful invocation of save method should return with reloadParent command
     * @return true if reloadParent command should be invoked after save
     */
    boolean isPopupSaveReloadParent()

    /**
     * Indicates if logged in user is required to perform actions on this view
     * @return true if user is required
     */
    boolean isUserRequired()

    /**
     * Provides logged in user
     * @return user
     */
    User getUser()

    /**
     * Provides a view reference object assigned by 'view' parameter (mostly another view)
     * @return view reference object or null if not assigned
     */
    Object getViewReference()

    /**
     * Provides the app link (TODO replace when tag-implementations completed)
     * @return appLink
     */
    String getAppLink()

    /**
     * Provides the base link
     * @return baseLink
     */
    String getBaseLink()

    /**
     * Gets updateOnlyTarget if available
     * @return updateOnlyTarget
     */
    String getUpdateOnlyTarget()

    /**
     * Provides an optional directory for fileuploads. 
     * @return uploadDir directory or null if not provided by implementing view 
     */
    String getUploadDir()

    List<String> getScriptsList();

    List<String> getScriptsListBottom();

    List<String> getStylesList();
}
