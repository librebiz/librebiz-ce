/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 22, 2008 9:42:32 PM 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.core.finance.DiscountAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDiscount;
import com.osserp.gui.client.records.DiscountAwareRecordView;
import com.osserp.gui.client.records.RecordView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDiscountConfirmationDisplayForwarder extends AbstractRecordForwarder {
    private static Logger log = LoggerFactory.getLogger(AbstractDiscountConfirmationDisplayForwarder.class.getName());

    @Override
    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        try {
            HttpSession session = request.getSession();
            RecordView recordView = getRecordView(session);
            Record record = recordView.getRecord();
            if (record instanceof DiscountAwareRecord) {
                session.removeAttribute("discountPercent");
                DiscountAwareRecord discountAware = (DiscountAwareRecord) record;
                if (!discountAware.getDiscountHistory().isEmpty()) {
                    RecordDiscount discount = discountAware.getDiscountHistory().get(0);
                    session.setAttribute("discountAware", discount);
                    if (recordView instanceof DiscountAwareRecordView) {
                        DiscountAwareRecordView darv = (DiscountAwareRecordView) recordView;
                        if (darv.isDiscountPercent()) {
                            session.setAttribute("discountPercent", true);
                        }
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("execute() bound discount aware [scope=session, percent="
                                + (session.getAttribute("discountPercent") == null ? "false" : "true") + "]");
                    }
                }
            } else {
                log.warn("execute() expected instanceof DiscountAwareRecordView, " +
                        "found [class=" + recordView.getClass().getName() + "]");
            }
        } catch (Throwable t) {
            log.warn("execute() failed [message=" + t.getMessage() + "]");
        }
        return super.execute(mapping, form, request, response);
    }

}
