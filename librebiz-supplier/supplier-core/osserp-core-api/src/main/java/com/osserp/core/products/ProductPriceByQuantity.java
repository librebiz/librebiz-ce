/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 13, 2011 7:02:55 PM 
 * 
 */
package com.osserp.core.products;

import com.osserp.common.EntityRelation;
import com.osserp.core.employees.Employee;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public interface ProductPriceByQuantity extends PriceAware, EntityRelation {

    /**
     * Provides referenced product id
     * @return productId
     */
    Long getProductId();

    /**
     * Provides quantity range start
     * @return rangeStart
     */
    Double getRangeStart();

    /**
     * Provides quantity range end
     * @return rangeEnd
     */
    Double getRangeEnd();

    /**
     * Resets range end (rangeEnd == 0)
     * @param user
     */
    void resetRangeEnd(Employee user);

    /**
     * Updates range
     * @param rangeEnd
     */
    void update(Double rangeEnd);

    /**
     * Updates prices
     * @param consumerPrice
     * @param resellerPrice
     * @param partnerPrice
     */
    void update(Double consumerPrice, Double resellerPrice, Double partnerPrice);

    /**
     * Updates prices
     * @param user
     * @param consumerMinumumMargin
     * @param resellerMinumumMargin
     * @param partnerMinumumMargin
     */
    void updateMinimumMargin(Employee user, Double consumerMinumumMargin, Double resellerMinumumMargin, Double partnerMinumumMargin);

    /**
     * Indicates that price is eol
     * @return endOfLife
     */
    boolean isEndOfLife();

    /**
     * Sets eol property
     * @param endOfLife
     */
    void setEndOfLife(boolean endOfLife);

    /**
     * Indicates that price range is is eol
     * @return endOfLife
     */
    boolean isInitial();

    /**
     * Indicates that price range is is eol
     * @return endOfLife
     */
    boolean isLast();

}
