/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 9, 2012 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.users.DomainUser;

/**
 * Employees private contacts.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PrivateContacts {

    /**
     * Creates a private contact entry (e.g. links an existing contact to a domain user)
     * @param user
     * @param contact
     * @param company or null if contact is not contact person
     */
    PrivateContact createPrivateContact(DomainUser user, Contact contact, Contact company);

    /**
     * Provides a private contact by id
     * @param id
     * @return private contact
     */
    PrivateContact getPrivateContact(Long id);

    /**
     * Provides a private contact
     * @param user
     * @param contact
     * @return contact or null if not exists
     */
    PrivateContact getPrivateContact(DomainUser user, Contact contact);

    /**
     * Indicates that contact is private contact of domain user
     * @param user
     * @param contact
     * @return true if contact is associated with domain user
     */
    boolean isPrivateContact(DomainUser user, Contact contact);

    /**
     * Indicates that an existing contact is currently marked as unused
     * @param user
     * @param contact
     * @return true if no private contact exists or existing has unused flag enabled
     */
    boolean isPrivateContactUnused(DomainUser user, Contact contact);

    /**
     * Indicates that at least one private contact is available
     * @param user
     * @return true if any private contact found
     */
    boolean isPrivateContactAvailable(DomainUser user);

    /**
     * Provides all private contacts of a user
     * @param user
     * @return privateContacts
     */
    List<PrivateContact> getPrivateContacts(DomainUser user);

    /**
     * Provides all private contacts which inherits the given contactId
     * @param contactId
     * @return privateContacts
     */
    List<PrivateContact> getPrivateContacts(Long contactId);

    /**
     * Persists a private contact
     * @param contact
     */
    void save(PrivateContact contact);

}
