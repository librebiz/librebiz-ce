/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 15, 2010 9:20:54 AM 
 * 
 */
package com.osserp.core.tasks.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.mail.MailSender;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.impl.AbstractXmlMessageHandler;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.StringUtil;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeManager;
import com.osserp.core.hrm.TimeRecordingQueryManager;
import com.osserp.core.tasks.Task;

/**
 * 
 * @author jg <jg@osserp.com>
 * 
 */
public class TimeRecordingQueryTaskImpl extends AbstractXmlMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingQueryTaskImpl.class.getName());

    private TimeRecordingQueryManager timeRecordingQueryManager = null;
    private EmployeeManager employeeManager = null;
    private MailSender mailSender = null;
    private ResourceLocator resourceLocator = null;

    protected TimeRecordingQueryTaskImpl(
            TimeRecordingQueryManager timeRecordingQueryManager,
            EmployeeManager employeeManager,
            MailSender mailSender,
            ResourceLocator resourceLocator) {
        super();
        this.timeRecordingQueryManager = timeRecordingQueryManager;
        this.employeeManager = employeeManager;
        this.mailSender = mailSender;
        this.resourceLocator = resourceLocator;
    }

    @Override
    protected void execute(Document doc, byte[] file) {
        if (log.isDebugEnabled()) {
            log.debug("execute() invoked with xml:\n" + JDOMUtil.getPrettyString(doc));
        }
        Element root = doc.getRootElement();
        Element dateElement = root.getChild("date");
        Element mailElement = root.getChild("mail");
        Element typeElement = root.getChild("typeId");

        Date date = (Date) JDOMUtil.getObjectFromElement(dateElement);
        String mail = (String) JDOMUtil.getObjectFromElement(mailElement);
        Long typeId = (Long) JDOMUtil.getObjectFromElement(typeElement);

        if (log.isDebugEnabled()) {
            log.debug("execute() generate list [date=" + date + ",mail=" + mail + ",typeId=" + typeId + "]");
        }
        if (date != null && mail != null && typeId != null) {
            int month = DateUtil.getMonth(date) + 1;
            String subject = "";
            List<String[]> list = new ArrayList<String[]>();
            if (typeId == 0) {
                list = getTimeBankQuery(date);
                subject = getLocalizedString("hrm.query.mail.subject.timebank") + " " + month + "." + DateUtil.getYear(date);
            } else if (typeId == 1) {
                list = getBookingsQuery(date);
                subject = getLocalizedString("hrm.query.mail.subject.booking") + " " + month + "." + DateUtil.getYear(date);
            }
            mailSender.send(mail, subject, StringUtil.createSheet(list));
        } else {
            log.warn("execute() no date or mail adress, aborting");
        }
    }

    private List<String[]> getTimeBankQuery(Date date) {
        List<Employee> activeEmployees = employeeManager.findActiveEmployees();
        List<String[]> list = new ArrayList<String[]>();
        String[] result = new String[8];
        result[0] = getLocalizedString("number");
        result[1] = getLocalizedString("lastName");
        result[2] = getLocalizedString("firstName");
        result[3] = getLocalizedString("costCenter");
        result[4] = getLocalizedString("remainingLeave");
        result[5] = getLocalizedString("timeBank");
        result[6] = getLocalizedString("timeBankOverMax");
        result[7] = getLocalizedString("hoursOutsideLimit");
        list.add(result);
        for (int i = 0, j = activeEmployees.size(); i < j; i++) {
            if (activeEmployees.get(i).isTimeRecordingEnabled() && timeRecordingQueryManager.exists(activeEmployees.get(i))) {
                result = timeRecordingQueryManager.getTimebankQuery(activeEmployees.get(i), date);
                if (result != null) {
                    list.add(result);
                }
            }
        }
        return list;
    }

    private List<String[]> getBookingsQuery(Date date) {
        List<Employee> activeEmployees = employeeManager.findActiveEmployees();
        List<String[]> list = new ArrayList<String[]>();
        String[] result = new String[5];
        result[0] = getLocalizedString("number");
        result[1] = getLocalizedString("name");
        result[2] = getLocalizedString("costCenter");
        result[3] = getLocalizedString("date");
        result[4] = getLocalizedString("reason");
        list.add(result);
        for (int i = 0, j = activeEmployees.size(); i < j; i++) {
            if (activeEmployees.get(i).isTimeRecordingEnabled() && timeRecordingQueryManager.exists(activeEmployees.get(i))) {
                List<String[]> tmp = timeRecordingQueryManager.getBookingQuery(activeEmployees.get(i), date);
                list.addAll(tmp);
            }
        }
        return list;
    }

    private String getLocalizedString(String key) {
        ResourceBundle bundle = getResourceBundle();
        String value = bundle != null && key != null ? bundle.getString(key) : null;
        return (value == null) ? key : value;
    }

    private ResourceBundle getResourceBundle() {
        if (resourceLocator != null) {
            try {
                return resourceLocator.getResourceBundle(Constants.DEFAULT_LOCALE_OBJECT);
            } catch (Exception e) {
                log.error("constructor caught exception while initializing resourceBundle [message=" + e.getMessage() + "]", e);
            }
        }
        log.warn("ResourceLocator not mapped");
        return null;
    }

}
