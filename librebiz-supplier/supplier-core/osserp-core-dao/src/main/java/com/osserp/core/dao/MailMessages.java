/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.dao;

import java.util.List;
import java.util.Set;

import com.osserp.common.mail.ReceivedMail;
import com.osserp.common.mail.ReceivedMailInfo;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public interface MailMessages {

    /**
     * Indicates if a message exists in storage
     * @param messageId
     * @return true if exists
     */
    boolean exists(String messageId);

    /**
     * Provides all received mail by business case
     * @param userId
     * @param businessCaseId
     * @param customerId
     * @return list of email envelopes
     */
    List<ReceivedMailInfo> findByBusinessCase(Long userId, Long businessCaseId, Long customerId);

    /**
     * Provides count of all received mail by business case or corresponding customer
     * @param userId
     * @param businessCaseId
     * @param customerId
     * @return count of related business cases
     */
    int getCountByBusinessCase(Long userId, Long businessCaseId, Long customerId);

    /**
     * Provides all received mail by assigned user
     * @param userId
     * @return list of email envelopes
     */
    List<ReceivedMailInfo> findByUser(Long userId);

    /**
     * Provides count of all received new mail by user
     * @param userId
     * @return count of related mail messages
     */
    int getCountByUser(Long userId);

    /**
     * Provides all received mail for a 
     * @param classifiedContactId
     * @param contactGroupId
     * @return list of email envelopes
     */
    List<ReceivedMailInfo> findByContact(Long classifiedContactId, Long contactGroupId);

    /**
     * Provides envelopes of all received emails with provided status
     * @param statusId
     * @return list of email envelopes
     */
    List<ReceivedMailInfo> findByStatus(Integer statusId);

    /**
     * Provides all received mail with provided status
     * @param statusId
     * @return list
     */
    List<ReceivedMail> getByStatus(Integer statusId);

    /**
     * Provides a message from storage
     * @param messageId
     * @return message or null if not exists
     */
    ReceivedMail getReceivedMail(String messageId);

    /**
     * Persists a rececived message
     * @param mail
     */
    void save(ReceivedMail mail);

    /**
     * Removes all content from the message and sets status as 'removed'.
     * @param messageId
     */
    void remove(String messageId);

    /**
     * Indicates an existing but empty message with status 'removed'.
     * @param messageId
     * @return true if empty message with status 'removed' exists 
     */
    boolean removed(String messageId);

    /**
     * Deletes businessNote and attachments created by fetched message.
     * @param messageId
     */
    void resetMoved(String messageId);

    /**
     * Restores an existing email message after reset
     * @param mail
     */
    void restore(ReceivedMail mail);

    /**
     * Sets a message as no longer available on corresponding server
     * @param messageId
     */
    void setUnavailable(String messageId);

    /**
     * Sets the assigned businessId property
     * @param mail
     * @param reference
     * @param referenceType
     * @param businessId
     * @return mail with removed businessId
     */
    ReceivedMail setBusinessReference(
            ReceivedMail mail,
            Long reference,
            Long referenceType,
            Long businessId);

    /**
     * Sets the user property
     * @param mail
     * @param userId
     * @return mail
     */
    ReceivedMail setUserReference(ReceivedMail mail, Long userId);

    /**
     * Resets the assigned businessId property
     * @param mail
     * @return mail with removed businessId
     */
    ReceivedMail resetBusinessReference(ReceivedMail mail);

    /**
     * Deletes an attachment from message
     * @param mail
     * @param attachmentId
     * @return mail with removed attachment
     */
    ReceivedMail deleteAttachment(ReceivedMail mail, Long attachmentId);

    /**
     * Flags attachment as shared and adds file to list of ignorable files
     * @param mail
     * @param attachmentId
     * @return mail with removed attachment
     */
    ReceivedMail ignoreAttachment(ReceivedMail mail, Long attachmentId);

    /**
     * Provides set of all attachments
     * @return files as absolute paths
     */
    Set<String> getExistingAttachments();

}
