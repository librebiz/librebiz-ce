<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:message var="saveIcon" key="img.icon.save"/>
<tr>
	<td><fmt:message key="save.changes"/></td>
	<td style="text-align: right;">
		<input type="image" name="method" src="<c:url value="${saveIcon}"/>" value="update" onclick="setMethod('update');" style="width: 16px; height: 16px; margin-right: 0px;" title="<fmt:message key="title.data.save"/>"/>
	</td>
</tr>
