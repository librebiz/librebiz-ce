SET search_path = osserp, pg_catalog;

DROP FUNCTION backup_purchase_order_history(recordid bigint);

UPDATE system_properties SET pvalue = '25' WHERE name = 'productSearchFetchSize';
UPDATE system_properties SET pvalue = '100' WHERE name = 'purchaseOrderSearchFetchSize';
UPDATE system_properties SET pvalue = '100' WHERE name = 'requestSearchFetchSize';
UPDATE system_properties SET pvalue = '100' WHERE name = 'salesSearchFetchSize';

INSERT INTO system_properties VALUES (212, 'mailSenderAlwaysBccFromAddress', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 212, true);

UPDATE system_properties SET pvalue = '0.14.2', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
