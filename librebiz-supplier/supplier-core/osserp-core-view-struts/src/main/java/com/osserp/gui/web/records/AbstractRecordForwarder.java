/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 13, 2007 7:32:49 PM 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.PermissionException;
import com.osserp.gui.client.records.RecordView;
import com.osserp.gui.web.struts.AbstractViewForwarder;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecordForwarder extends AbstractViewForwarder {
    private static Logger log = LoggerFactory.getLogger(AbstractRecordForwarder.class.getName());

    protected RecordView getRecordView(HttpSession session) throws PermissionException {
        RecordView view = (RecordView) getView(session);
        if (view == null) {
            log.warn("getRecordView() no view bound!");
            throw new ActionException();
        }
        return view;
    }
}
