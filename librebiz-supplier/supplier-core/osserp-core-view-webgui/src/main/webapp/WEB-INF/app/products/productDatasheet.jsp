<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>
<v:view viewName="productDatasheetView"/>
<c:set var="dmsDocumentView" scope="request" value="${sessionScope.productDatasheetView}"/>
<c:set var="actionsCount" scope="page" value="2"/>
<c:if test="${view.syncClientEnabled}">
<c:set var="actionsCount" scope="page" value="3"/>
</c:if>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="styles" type="string">
		<style type="text/css">
			#noteEdit {
				background-color: white;
				padding-left: 90px;
				border: 1px solid #999999;
			}
		</style>
	</tiles:put>
	<tiles:put name="headline">
		<fmt:message key="datasheets"/> <o:out value="${view.product.productId}"/> - <o:out value="${view.product.name}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <v:form url="/products/productDatasheet/save" multipart="true">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <c:if test="${!empty view.list}">
                                    <thead>
                                        <tr>
                                            <th><fmt:message key="document" /></th>
                                            <th><fmt:message key="date" /></th>
                                            <th><fmt:message key="uploadBy" /></th>
                                            <th class="action" colspan="${actionsCount}"><fmt:message key="action" /></th>
                                        </tr>
                                    </thead>
                                </c:if>
                                <tbody>
                                    <c:import url="${viewdir}/products/_productDocumentUploadColumn.jsp" />
                                    <c:forEach items="${view.list}" var="doc">
                                        <tr>
                                            <td><o:doc obj="${doc}">
                                                    <o:out value="${doc.displayName}" />
                                                </o:doc>
                                            </td>
                                            <td><o:date value="${doc.created}" /></td>
                                            <td><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${doc.createdBy}">
                                                    <oc:employee value="${doc.createdBy}" />
                                                </v:ajaxLink>
                                            </td>
                                            <td class="action icon">
                                                <c:choose>
                                                    <c:when test="${doc.referenceDocument}">
                                                        <o:img name="enabledIcon" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <v:link url="${view.baseLink}/setReference?id=${doc.id}" title="setAsReference">
                                                            <o:img name="toggleStoppedFalseIcon" />
                                                        </v:link>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <td class="action icon">
                                                <o:permission role="executive,product_datasheet_upload_admin" info="permissionDocumentsDelete">
                                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="/products/productDatasheet/delete?id=${doc.id}" />');" title="<fmt:message key="delete"/>">
                                                        <o:img name="deleteIcon" />
                                                    </a>
                                                </o:permission>
                                                <o:forbidden role="executive,product_datasheet_upload_admin" info="permissionDocumentsDelete">
                                                    <o:img name="disabledIcon" />
                                                </o:forbidden>
                                            </td>
                                            <c:if test="${view.syncClientEnabled}">
                                                <td class="action icon">
                                                    <span style="align-left: 6px;">
                                                        <v:link url="/products/productDatasheet/syncWithClient?id=${doc.id}" title="syncClientActionHint">
                                                            <o:img name="syncDocumentIcon" />
                                                        </v:link>
                                                    </span>
                                                </td>
                                            </c:if>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </v:form>
                </div>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
