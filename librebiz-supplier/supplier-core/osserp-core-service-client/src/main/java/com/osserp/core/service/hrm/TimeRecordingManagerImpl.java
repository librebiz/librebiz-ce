/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 22, 2007 3:51:35 PM 
 * 
 */
package com.osserp.core.service.hrm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.PermissionException;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.core.Options;
import com.osserp.core.dao.hrm.TimeRecordings;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingManager;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.tasks.TimeRecordingEvent;
import com.osserp.core.tasks.TimeRecordingEventSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingManagerImpl extends AbstractService implements TimeRecordingManager {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingManagerImpl.class.getName());

    private OptionsCache optionsCache = null;
    private TimeRecordings timeRecordings = null;
    private TimeRecordingEventSender timeRecordingEventSender = null;

    protected TimeRecordingManagerImpl(
            TimeRecordings timeRecordings,
            OptionsCache optionsCache,
            TimeRecordingEventSender timeRecordingEventSender) {
        super();
        this.timeRecordings = timeRecordings;
        this.optionsCache = optionsCache;
        this.timeRecordingEventSender = timeRecordingEventSender;
    }

    public List<TimeRecordingConfig> findConfigs() {
        return new ArrayList(optionsCache.getList(Options.TIME_RECORDING_CONFIGS));
    }

    public TimeRecordingConfig findConfig(Long id) {
        return (TimeRecordingConfig) optionsCache.getMapped(Options.TIME_RECORDING_CONFIGS, id);
    }

    public List<TimeRecordType> findTypes() {
        return new ArrayList(optionsCache.getList(Options.TIME_RECORD_TYPES));
    }

    public TimeRecordType findType(Long id) {
        return (TimeRecordType) optionsCache.getMapped(Options.TIME_RECORD_TYPES, id);
    }

    public List<TimeRecordMarkerType> findMarkerTypes() {
        return new ArrayList(optionsCache.getList(Options.TIME_RECORD_MARKER_TYPES));
    }

    public TimeRecordMarkerType findMarkerType(Long id) {
        return (TimeRecordMarkerType) optionsCache.getMapped(Options.TIME_RECORD_MARKER_TYPES, id);
    }

    public boolean exists(Employee timeRecording) {
        return timeRecordings.exists(timeRecording);
    }

    public TimeRecording find(Employee timeRecording) {
        return timeRecordings.find(timeRecording);
    }

    public TimeRecording getRecording(Long primaryKey) {
        return timeRecordings.load(primaryKey);
    }

    public TimeRecording create(Employee user, Employee timeRecording) {
        return timeRecordings.create(user, timeRecording);
    }

    public void createPeriod(Employee user, TimeRecording timeRecording) {
        timeRecordings.createPeriod(user, timeRecording);
    }

    public TimeRecording sychronizePublicHoliday(TimeRecording recording) {
        return timeRecordings.sychronizePublicHoliday(recording);
    }

    public void save(Employee user, TimeRecording recording) {
        timeRecordings.save(user, recording);
    }

    public TimeRecording createClosingsIfRequired(TimeRecording recording) {
        return timeRecordings.createClosingsIfRequired(recording);
    }

    public void addRecord(Employee user, TimeRecording recording, TimeRecordType type) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("addRecord() - system time booking [typeId=" + type.getId() + "]");
        }
        timeRecordings.addRecord(user, recording, type, null, null);
    }

    public void addRecord(Employee user, TimeRecording recording, TimeRecordType type, Date date, String note) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("addRecord() - manual booking [typeId=" + type.getId()
                    + ", date=" + DateFormatter.getDateAndTime(date) + "]");
        }
        timeRecordings.addRecord(user, recording, type, date, note);
    }

    public void addRecord(Employee user, TimeRecording recording, TimeRecordType type, Date startDate, Date stopDate, String note) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("addRecord() - interval booking [recording=" + recording.getId()
                    + ", typeId=" + type.getId()
                    + ", startDate=" + DateFormatter.getDateAndTime(startDate)
                    + ", stopDate=" + DateFormatter.getDateAndTime(stopDate) + "]");
        }
        timeRecordings.addInterval(user, recording, type, startDate, stopDate, note);
        if (type.isNeedApproval()) {
            TimeRecordingEvent event = new TimeRecordingEvent(
                    user.getId(),
                    recording.getId(),
                    type.getId(),
                    startDate,
                    stopDate,
                    note);
            timeRecordingEventSender.send(event);
        }
    }

    public void addMarker(
            Employee user,
            TimeRecording recording,
            TimeRecordMarkerType type,
            Date date,
            Double value,
            String note) throws ClientException {
        if (type.isExpireLeave() && DateUtil.getMonth(date) == 0) {
            Date tmpDateF = DateUtil.createDate("01.02." + DateUtil.getYear(date), true);
            Date tmpDateM = DateUtil.createDate("01.03." + DateUtil.getYear(date), true);
            timeRecordings.addMarker(user, recording, type, date, value, note);
            timeRecordings.addMarker(user, recording, type, tmpDateF, value, note);
            timeRecordings.addMarker(user, recording, type, tmpDateM, value, note);
        } else {
            timeRecordings.addMarker(user, recording, type, date, value, note);
        }
    }

    public void updateRecord(
            Employee user,
            TimeRecording recording,
            TimeRecord selected,
            Date startDate,
            Date stopDate,
            String correctionNote) throws ClientException {
        if (isNotSet(correctionNote)) {
            throw new ClientException(ErrorCode.CORRECTION_NOTE_MISSING);
        }
        if (selected.isWaitForApproval()) {
            throw new ClientException(ErrorCode.RECORD_WAITFORAPPROVAL);
        }
        if (stopDate != null && stopDate.before(startDate)) {
            throw new ClientException(ErrorCode.BOOKING_STOP_INVALID);
        }
        timeRecordings.updateRecord(user, recording, selected, startDate, stopDate, correctionNote);
        TimeRecordingEvent event = new TimeRecordingEvent(
                user.getId(),
                recording.getId(),
                selected.getId(),
                selected.getType().getId(),
                startDate,
                stopDate,
                correctionNote);
        timeRecordingEventSender.send(event);
    }

    public void updatePeriod(
            Employee user,
            TimeRecording recording,
            Long terminalChipNumber,
            Date validFrom,
            Date validTil,
            Double leave,
            Double hours,
            Double extraLeave,
            Double firstYearLeave,
            Long state) throws ClientException {

        if (terminalChipNumber != null && recording.getTerminalChipNumber() != null
                && !recording.getTerminalChipNumber().equals(terminalChipNumber)) {
            if (timeRecordings.exists(terminalChipNumber)) {
                throw new ClientException(ErrorCode.CHIP_NUMBER_USED);
            }
        }
        recording.updatePeriod(validFrom, validTil, leave, hours, extraLeave, firstYearLeave, state, terminalChipNumber);
        timeRecordings.save(user, recording);
        sychronizePublicHoliday(recording);
    }

    public void removeRecord(Employee user, TimeRecording recording, Long id) throws PermissionException {
        timeRecordings.removeRecord(user, recording, id);
    }

    public void removeMarker(Employee user, TimeRecording recording, Long id) throws PermissionException {
        timeRecordings.removeMarker(user, recording, id);
    }

    public List<TimeRecordCorrection> findCorrections(TimeRecord record) {
        return timeRecordings.fetchCorrections(record);
    }

    public void changeConfig(Employee user, TimeRecording recording, Long id) throws ClientException {
        TimeRecordingPeriod period = recording.getSelectedPeriod();
        if (period == null) {
            throw new ClientException(ErrorCode.PERIOD_SELECTION_MISSING);
        }
        List<TimeRecordingConfig> configs = findConfigs();
        for (int i = 0, j = configs.size(); i < j; i++) {
            TimeRecordingConfig next = configs.get(i);
            if (next.getId().equals(id)) {
                period.changeConfig(next);
                recording.setLastClosingActionTime(null);
                timeRecordings.save(user, recording);
                createClosingsIfRequired(recording);
                break;
            }
        }
    }
}
