<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="mailEditorView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="javascript" type="string">
<script type="text/javascript">
    function enableSendMail() {
        var form = document.forms[0];
        form.sendMail.value = 'true';
        return true;
    }
</script>
</tiles:put>
<tiles:put name="headline">
    <c:if test="${view.recipientSelectionMode}"><fmt:message key="selectionEmailRecipient"/> - </c:if>
    <o:out value="${view.mailMessageContext.viewHeader}" />
</tiles:put>
<tiles:put name="headline_right">
    <v:navigation />
</tiles:put>

<tiles:put name="content" type="string">
    <div class="content-area" id="mailEditorContent">
        <div class="row">
            <c:choose>
                <c:when test="${view.recipientSelectionMode}">
                    <c:import url="${viewdir}/mail/_mailEditRecipientSelection.jsp" />
                </c:when>
                <c:otherwise>
                    <c:import url="${viewdir}/mail/_mailEdit.jsp" />
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</tiles:put>
</tiles:insert>
