/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 5, 2016 
 * 
 */
package com.osserp.core.service.records;

import java.util.Date;

import org.apache.velocity.app.VelocityEngine;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.service.impl.AbstractTemplateAwareService;

import com.osserp.core.BusinessCase;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordCorrection;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.finance.RecordPrintManager;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Records;
import com.osserp.core.purchasing.PurchaseDeliveryNoteManager;
import com.osserp.core.purchasing.PurchaseInvoiceManager;
import com.osserp.core.purchasing.PurchaseOrderManager;
import com.osserp.core.sales.SalesCancellationManager;
import com.osserp.core.sales.SalesCreditNoteManager;
import com.osserp.core.sales.SalesDeliveryNoteManager;
import com.osserp.core.sales.SalesDownpaymentManager;
import com.osserp.core.sales.SalesInvoiceManager;
import com.osserp.core.sales.SalesOfferManager;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.sales.SalesSearch;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordPrintManagerImpl extends AbstractTemplateAwareService implements RecordPrintManager {
    
    private RecordPrintService recordPrintService = null;
    private SalesSearch salesSearch = null;

    private PurchaseDeliveryNoteManager purchaseDeliveryNoteManager;
    private PurchaseInvoiceManager purchaseInvoiceManager;
    private PurchaseOrderManager purchaseOrderManager;
    private SalesCancellationManager salesCancellationManager;
    private SalesCreditNoteManager salesCreditNoteManager;
    private SalesDeliveryNoteManager salesDeliveryNoteManager;
    private SalesDownpaymentManager salesDownpaymentManager;
    private SalesInvoiceManager salesInvoiceManager;
    private SalesOfferManager salesOfferManager;
    private SalesOrderManager salesOrderManager;
    
    protected RecordPrintManagerImpl(
            VelocityEngine engine, 
            RecordPrintService recordPrintService, 
            SalesSearch salesSearch,
            PurchaseDeliveryNoteManager purchaseDeliveryNoteManager, 
            PurchaseInvoiceManager purchaseInvoiceManager, 
            PurchaseOrderManager purchaseOrderManager,
            SalesCancellationManager salesCancellationManager, 
            SalesCreditNoteManager salesCreditNoteManager,
            SalesDeliveryNoteManager salesDeliveryNoteManager,
            SalesDownpaymentManager salesDownpaymentManager,
            SalesInvoiceManager salesInvoiceManager, 
            SalesOfferManager salesOfferManager,
            SalesOrderManager salesOrderManager) {
        super(engine);
        this.recordPrintService = recordPrintService;
        this.salesSearch = salesSearch;
        this.purchaseDeliveryNoteManager = purchaseDeliveryNoteManager;
        this.purchaseInvoiceManager = purchaseInvoiceManager;
        this.purchaseOrderManager = purchaseOrderManager;
        this.salesCancellationManager = salesCancellationManager;
        this.salesCreditNoteManager = salesCreditNoteManager;
        this.salesDownpaymentManager = salesDownpaymentManager;
        this.salesDeliveryNoteManager = salesDeliveryNoteManager;
        this.salesInvoiceManager = salesInvoiceManager;
        this.salesOfferManager = salesOfferManager;
        this.salesOrderManager = salesOrderManager;
    }

    public final DocumentData createPdf(Employee employee, Record record, Long status) throws ClientException {
        RecordManager manager = getRecordManager(record);
        // load latest version from backend to avoid concurrent modification pitfalls
        Record obj = manager.load(record.getId());
        manager.validatePrint(obj, status);
        
        byte[] pdf = null;
        BusinessCase businessCase = null;
        if (obj.isSales()) {
            try {
                businessCase = salesSearch.findBusinessCase(obj);
            } catch (Throwable t) {
                // sales record without referenced businessCase
            }
        }
        boolean preview = false;
        if (status == null || status < Record.STAT_SENT) {
            preview = true;
        }
        if (businessCase != null) {
            pdf = recordPrintService.createPdf(employee, obj, businessCase, preview);
        } else {
            pdf = recordPrintService.createPdf(employee, obj, preview);
        }
        DmsDocument doc = recordPrintService.archive(employee, obj, pdf);
        if (status != null) {
            addStatus(employee, obj, status);
        }
        return new DocumentData(pdf, doc.getRealFileName(), Constants.MIME_TYPE_PDF);
    }

    public final DocumentData createCustomPdf(
            Employee employee,
            Record record,
            boolean preview,
            String headerName,
            Date date) throws ClientException {
        if (isSet(headerName)) {
            record.setCustomHeader(headerName);
        }
        if (date != null) {
            record.updateCreatedDate(date);
        }
        byte[] pdf = null;
        BusinessCase businessCase = null;
        if (record.isSales()) {
            try {
                businessCase = salesSearch.findBusinessCase(record);
            } catch (Throwable t) {
                // sales record without referenced businessCase
            }
        }
        if (businessCase != null) {
            pdf = recordPrintService.createPdf(employee, record, businessCase, preview);
        } else {
            pdf = recordPrintService.createPdf(employee, record, preview);
        }
        String filename = Records.createNumber(record.getType(), record.getId(), false);
        if (!preview) {
            recordPrintService.archiveVersion(employee, record, pdf, filename);
        }
        return new DocumentData(pdf, filename, Constants.MIME_TYPE_PDF);
    }

    private void addStatus(Employee user, Record record, Long status) throws ClientException {
        long currentStatus = record.getStatus().longValue();
        boolean isSent = (currentStatus >= Record.STAT_SENT.longValue());
        if (!isSent || (status.longValue() > currentStatus)) {
            RecordManager manager = getRecordManager(record);
            manager.updateStatus(record, user, status);
        }
    }
    
    private RecordManager getRecordManager(Record record) {
        if (RecordType.PURCHASE_INVOICE.equals(record.getType().getId())) {
            return purchaseInvoiceManager;
        } else if (RecordType.PURCHASE_RECEIPT.equals(record.getType().getId())) {
            return purchaseDeliveryNoteManager;
        } else if (RecordType.PURCHASE_ORDER.equals(record.getType().getId())) {
            return purchaseOrderManager;
        } else if (RecordType.SALES_CANCELLATION.equals(record.getType().getId())) {
            return salesCancellationManager;
        } else if (RecordType.SALES_CREDIT_NOTE.equals(record.getType().getId())) {
            return salesCreditNoteManager;
        } else if (RecordType.SALES_DELIVERY_NOTE.equals(record.getType().getId())) {
            return salesDeliveryNoteManager;
        } else if (RecordType.SALES_DOWNPAYMENT.equals(record.getType().getId())) {
            return salesDownpaymentManager;
        } else if (RecordType.SALES_INVOICE.equals(record.getType().getId())) {
            return salesInvoiceManager;
        } else if (RecordType.SALES_OFFER.equals(record.getType().getId())) {
            return salesOfferManager;
        } else if (RecordType.SALES_ORDER.equals(record.getType().getId())) {
            return salesOrderManager;
        }
        return null;
    }

    public DocumentData createCorrectionPdf(Employee employee, PaymentAwareRecord record, Long status) throws ClientException {
        boolean preview = status == null || status < Record.STAT_SENT;
        byte[] data = recordPrintService.createCorrectionPdf(employee, record, preview);
        DmsDocument doc = recordPrintService.archiveCorrection(employee, record, data);
        return new DocumentData(data, doc.getRealFileName(), Constants.MIME_TYPE_PDF);
    }

    public DocumentData getPdf(Record record) {
        return recordPrintService.getPdf(record);
    }

    public DocumentData getCorrectionPdf(RecordCorrection correction) throws ClientException {
        return recordPrintService.getCorrectionPdf(correction);
    }

    public void archiveCleanup(Record record) {
        recordPrintService.archiveCleanup(record);
    }

}
