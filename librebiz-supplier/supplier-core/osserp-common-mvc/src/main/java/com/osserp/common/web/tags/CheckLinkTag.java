/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2007 10:45:32 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Tag renders an HTML-Link executing an ajax request on click. The function invoked depends on given params.
 * 
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public class CheckLinkTag extends AbstractClassicValueTag {
    private static Logger log = LoggerFactory.getLogger(CheckLinkTag.class.getName());
    private String checkUrl = null;
    private String linkUrl = null;

    @Override
    public int doStartTag() throws JspException {
        try {
            StringBuilder buffer = new StringBuilder();

            buffer
                    .append("<a href=\"javascript:;\" onclick=\"ojsAjax.checkLink('")
                    .append(getContextPath()).append(checkUrl)
                    .append("','")
                    .append(getContextPath()).append(linkUrl)
                    .append("');\">");

            JspWriter out = pageContext.getOut();
            out.print(buffer.toString());
        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("</a>");
        } catch (IOException ioe) {
            log.error("doEndTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    public String getCheckUrl() {
        return checkUrl;
    }

    public void setCheckUrl(String checkUrl) {
        this.checkUrl = checkUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }
}
