<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="mailAttachmentView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="styles" type="string">
  		<style type="text/css">
        </style>
	</tiles:put>
	<tiles:put name="headline">
        <o:out value="${view.headerName}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="mailAttachmentContent">
            <div class="row">
                <v:form name="mailForm" url="/mail/mailAttachment/save" multipart="true">

                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <span><fmt:message key="writeEmailLabel"/>: <fmt:message key="editAttachments"/></span>
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">

                            <c:choose>
                                <c:when test="${empty view.attachments}">
                                    <div class="row next">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="attachments"><fmt:message key="attachments" /></label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <fmt:message key="noFilesAttached"/>
                                            </div>
                                        </div>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach var="obj" items="${view.attachments}" varStatus="status">
                                        <div class="row<c:if test="${status.first}"> next</c:if>">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="attachments">
                                                        <c:if test="${status.first}">
                                                            <fmt:message key="attachments" />
                                                        </c:if>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">

                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                                <v:link url="/mail/mailAttachment/remove?name=${obj.fileName}">
                                                                    <o:img name="deleteIcon"/>
                                                                </v:link>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-11">
                                                            <div class="form-group">
                                                                <o:out value="${obj.fileName}"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>

                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="addAttachment"><fmt:message key="addLabel"/></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <o:fileUpload />
                                    </div>
                                </div>
                            </div>

                            <div class="row next">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <v:submitExit url="/mail/mailAttachment/exit" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <o:submit styleClass="form-control" value="loadSelection" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </v:form>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
