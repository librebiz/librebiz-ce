/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 27, 2016 
 * 
 */
package com.osserp.gui.purchasing

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.finance.BillingType
import com.osserp.core.finance.BookingType
import com.osserp.core.finance.Cancellation
import com.osserp.core.finance.Payment
import com.osserp.core.finance.Record
import com.osserp.core.purchasing.PurchaseInvoiceManager
import com.osserp.core.purchasing.PurchasePaymentManager

import com.osserp.gui.records.AbstractRecordPaymentView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class PurchaseInvoicePaymentView extends AbstractRecordPaymentView {

    private static Logger logger = LoggerFactory.getLogger(PurchaseInvoicePaymentView.class.getName())
    String customHeaderSuggestion

    PurchaseInvoicePaymentView() {
        super()
        dependencies = ['purchaseInvoiceView']
    }

    protected String getDefaultExitLink() {
        "/purchaseInvoiceRedisplay"
    }

    protected String getPaymentAwareRecordViewName() {
        'purchaseInvoiceView'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest && record) {
            paymentTypes = purchasePaymentManager.getPaymentTypes(record)
        }
    }

    @Override
    void save() {
        if (paymentDeleteMode) {
            Long id = getLong('id')
            if (id) {
                Payment payment = record.payments.find { it.id == id }
                if (payment) {
                    purchasePaymentManager.delete(payment)
                    record = purchaseInvoiceManager.find(record.id)
                }
            }
            paymentDeleteMode = false
        } else {
            Date datePaid = getDate('datePaid')
            BigDecimal amount = getDecimal('amount')
            boolean paid = getBoolean('paid')
            Long bankAccountId = getLong('bankAccountId')
            
            if (!amount) {
                throw new ClientException(ErrorCode.AMOUNT_MISSING)
            }
            if (datePaid == null) {
                datePaid = new Date(System.currentTimeMillis())
            }
            purchaseInvoiceManager.addPayment(
                domainEmployee, record, selectedPaymentType.id, amount, datePaid, bankAccountId)
            if (paid) {
                purchaseInvoiceManager.updateStatus(record, domainEmployee, Record.STAT_CLOSED)
            }
            record = purchaseInvoiceManager.find(record.id)
        }
    }

    void selectPaymentType() {
        cancellationMode = false
        creditNoteMode = false
        recordCloseMode = false
        paymentDeleteMode = false
        partialPayment = false
        
        Long id = getLong('id')
        if (!id) {
            selectedPaymentType = null
            
        } else if (100L == id) {
            recordCloseMode = true
            
        } else if (-1L == id) {
            paymentDeleteMode = true
            
        } else if (BillingType.TYPE_CREDIT_NOTE == id) {
            creditNoteMode = true
            customHeaderSuggestion = getSystemProperty('purchaseInvoicePrintHeaderDefault')
        } else {
            selectedPaymentType = paymentTypes.find { it.id == id }
            if (selectedPaymentType && (BillingType.TYPE_PURCHASE_PARTIAL_PAYMENT == id)) {
                partialPayment = true
            }
        }
    }

    String createCreditNote() {
        resultingRecord = purchaseInvoiceManager.createCreditNote(
            domainEmployee, record, form.getString('customHeader'))
        creditNoteMode = false
        return "/purchaseInvoice.do?method=display&id=${resultingRecord.id}"
    }

    String createCancellation() {
        throw new IllegalStateException('creating cancellation not supported')
    }

    String setPaid() {
        purchaseInvoiceManager.updateStatus(record, domainEmployee, Record.STAT_CLOSED)
        record = purchaseInvoiceManager.find(record.id)
        recordCloseMode = false
        return exitTarget
    }

    protected PurchaseInvoiceManager getPurchaseInvoiceManager() {
        getService(PurchaseInvoiceManager.class.getName())
    }

    protected PurchasePaymentManager getPurchasePaymentManager() {
        getService(PurchasePaymentManager.class.getName())
    }

    protected boolean billingViewExists() {
        // billingView is sales context only
        false
    }

}
