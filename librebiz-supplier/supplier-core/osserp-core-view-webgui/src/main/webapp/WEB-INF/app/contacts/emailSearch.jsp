<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="emailSearchView"/>

<c:set var="selectUrl" scope="request" value="/contacts.do"/>
<c:set var="selectMethod" scope="request" value="load"/>
<c:set var="selectExit" scope="request" value="contactSearchDisplay"/>
<c:set var="resultPage" scope="request" value="_emailSearch.jsp"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="javascript" type="string">
        <script type="text/javascript">
            <c:import url="contactSearch.js"/>
        </script>
    </tiles:put>
    <tiles:put name="headline"><fmt:message key="emailSearch"/></tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string"> 
        <div class="subcolumns">
            <div class="subcolumn">
                <c:import url="_contactSearchForm.jsp"/>
            </div>
        </div>
    </tiles:put> 
</tiles:insert>
