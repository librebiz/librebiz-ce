<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactPersonView}"/>
<c:set var="contact" value="${contactView.bean}"/>

<div class="contentBox">
	<div class="contentBoxHeader">
		<div class="contentBoxHeaderLeft">
			<table class="valueTable first33">
				<tbody>
                    <tr>
                        <td style="padding: 0 5px;">
                            <c:if test="${!empty contact.salutation.name and !contact.salutation.selectionLabel}"><span style="margin-right: 8px;"><o:out value="${contact.salutation.name}"/></span></c:if>
                            <c:if test="${!empty contact.title.name}"><span style="margin-right: 8px;"><o:out value="${contact.title.name}"/></span></c:if>
                            <a href="javascript:ojsCommon.toggleDisplay('created');" title="<fmt:message key="editStatus"/>"><o:out value="${contact.displayName}"/></a>
                        </td>
                    </tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="contentBoxData">
		<div id="created" style="display: none;">
			<div class="smallSpacer"></div>
			<table class="valueTable first33" style="margin-top: 5px;">
				<tbody>
					<tr>
						<td><fmt:message key="databaseId"/></td>
						<td><o:out value="${contact.contactId}"/></td>
					</tr>
					<tr>
						<td><fmt:message key="createdBy"/></td>
						<td>
							<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${contact.personCreatedBy}">
								<oc:employee value="${contact.personCreatedBy}"/>
							</v:ajaxLink>
							<c:if test="${!empty contact.personCreated}"> / <fmt:formatDate value="${contact.personCreated}"/></c:if>
						</td>
					</tr>
					<c:if test="${!empty contact.personChangedBy}">
						<tr>
							<td><fmt:message key="changedBy"/></td>
							<td>
								<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${contact.personChangedBy}">
									<oc:employee value="${contact.personChangedBy}"/>
								</v:ajaxLink>
								<c:if test="${!empty contact.personChanged}"> / <fmt:formatDate value="${contact.personChanged}"/></c:if>
							</td>
						</tr>
					</c:if>
                    <o:permission role="contact_person_delete" info="permissionContactPersonDelete">
                        <tr>
                            <td><fmt:message key="contact" /></td>
                            <td>
                                <a href="javascript:onclick=confirmLink('<fmt:message key="confirmation"/>:\n<fmt:message key="confirmContactDelete"/>','<c:url value="/contactPersons.do?method=delete"/>');"
                                    title="<fmt:message key="contactDelete"/>"><fmt:message key="delete" /></a>
                            </td>
                        </tr>
                    </o:permission>
				</tbody>
			</table>
    		<div class="smallSpacer"></div>
		</div>
	</div>
</div>
