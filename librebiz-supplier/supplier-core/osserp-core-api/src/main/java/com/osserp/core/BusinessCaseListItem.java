/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 13, 2006 2:13:08 PM 
 * 
 */
package com.osserp.core;

import java.util.Date;

import com.osserp.common.AppointmentAware;
import com.osserp.common.Option;

import com.osserp.core.events.Event;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * Provides a business case list item for query results.
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessCaseListItem extends AppointmentAware, Option {

    /**
     * The business type
     * @return type
     */
    BusinessType getType();

    /**
     * Provides the shortkey of the type - used by sort  
     * @return type.key
     */
    String getTypeKey();

    /**
     * The customer id
     * @return customerId
     */
    Long getCustomerId();

    /**
     * Provides the customer name
     * @return name
     */
    String getCustomerName();

    /**
     * Provides the short name of the customer
     * @return short name
     */
    String getShortName();

    /**
     * Indicates if customer is a private, business or public contact
     * @return contactType
     */
    Long getContactType();

    /**
     * Provides the street of the delivery address
     * @return street
     */
    String getStreet();

    /**
     * Provides the zipcode of the delivery address
     * @return zipcode
     */
    String getZipcode();

    /**
     * Provides the city of the delivery address
     * @return city
     */
    String getCity();

    /**
     * The current status of the item
     * @return status
     */
    Long getStatus();

    /**
     * The date the item was created
     * @return created
     */
    Date getCreated();

    /**
     * User or employee who created the item
     * @return createdBy
     */
    Long getCreatedBy();

    /**
     * The related sales person id
     * @return salesId
     */
    Long getSalesId();

    /**
     * The related sales person id
     * @return salesKey
     */
    String getSalesKey();

    /**
     * The related technical manager id
     * @return managerId
     */
    Long getManagerId();

    /**
     * The related technical manager key
     * @return managerKey
     */
    String getManagerKey();

    /**
     * Provides the company id (e.g. client)
     * Deprecated, use company.id instead
     * @return companyId
     */
    @Deprecated
    Long getCompanyId();

    /**
     * Provides the company
     * @return company
     */
    SystemCompany getCompany();

    /**
     * Provides the branch request is related to
     * Deprecated, use branch.id instead
     * @return branchId
     */
    @Deprecated
    Long getBranchId();

    /**
     * Provides the branch key request is related to
     * Deprecated, use branch.shortkey instead
     * @return branchKey
     */
    @Deprecated
    String getBranchKey();

    /**
     * Provides the branch office
     * @return branch
     */
    BranchOffice getBranch();

    /**
     * Provides the origin id of the request (campaign)
     * @return originId
     */
    Long getOriginId();

    /**
     * Provides the origin type id of the request (request per)
     * @return originId
     */
    Long getOriginTypeId();

    /**
     * Provides the average estimated capacity of the request
     * @return capacity
     */
    Double getCapacity();

    /**
     * Provides the capacity number format
     * @return capacityFormat
     */
    String getCapacityFormat();

    /**
     * Provides the date of the last note
     * @return lastNoteDate
     */
    Date getLastNoteDate();

    /**
     * Indicates if businessCase is cancelled.
     * @return true if cancelled
     */
    boolean isCancelled();

    /**
     * Indicates if businessCase is closed.
     * @return true if status == 100
     */
    boolean isClosed();

    /**
     * Indicates that request is currently stopped
     * @return stopped
     */
    boolean isStopped();

    /**
     * Provides an accounting reference
     * @return the accountingReference
     */
    public String getAccountingReference();

    /**
     * Provides the shipping company id
     * @return shipping id
     */
    Long getShippingId();

    /**
     * Provides the payment id
     * @return payment id
     */
    Long getPaymentId();

    /**
     * The delivery date
     * @return deliveryDate
     */
    Date getDeliveryDate();

    /**
     * Indicates that an appointment is available
     * @return appointmentAvailable
     */
    boolean isAppointmentAvailable();

    /**
     * Provides an appointment if available
     * @return appointment
     */
    Event getAppointment();

    /**
     * Sets an appointment if available
     * @param appointment
     */
    void setAppointment(Event appointment);

    /**
     * The appointment date
     * @return of appointment
     */
    Date getAppointmentDate();

}
