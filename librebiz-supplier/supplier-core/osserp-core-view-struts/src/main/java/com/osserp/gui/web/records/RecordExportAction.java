/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Aug-2006 21:36:12 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.EmptyListException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.web.RequestForm;
import com.osserp.common.web.RequestUtil;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.records.RecordExportView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordExportAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(RecordExportAction.class.getName());

    @Override
    protected Class<RecordExportView> getViewClass() {
        return RecordExportView.class;
    }

    /**
     * Creates and 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward display(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("display() request from " + getUser(session).getId());
        }
        RecordExportView view = (RecordExportView) createView(request);
        view.load(RequestUtil.fetchLong(request, "type"), RequestUtil.fetchId(request));
        if (view.getBean() != null) {
            // type initialized and export with id found
            return mapping.findForward(Actions.DISPLAY);
        }
        if (view.isArchiveAvailable()) {
            // type initialized, archive exists but export not exists or id not provided
            return mapping.findForward(Actions.ARCHIVE);
        }
        // neither type nor id provided or corresponding objects found
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitSelectType(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitSelectType() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        view.setType(null);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectType(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectType() request from " + getUser(session).getId());
        }
        RecordExportView view = fetchOrCreateView(request);
        view.setType(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward displayArchive(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("displayArchive() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        try {
            view.initArchive();
            return mapping.findForward(Actions.ARCHIVE);
        } catch (EmptyListException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitArchive(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitArchive() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        view.removeArchive();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectAvailable(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectAvailable() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        view.selectAvailable(RequestUtil.getId(request));
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createExport(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createExport() request from " + getUser(session).getId());
        }

        RecordExportView view = getExportView(session);
        try {
            view.checkPermission(new String[] { "record_export" });
        } catch (PermissionException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
        view.createExport();
        if (view.isExportConfirmationMode()) {
            if (log.isDebugEnabled()) {
                log.debug("createExport() done [target=" + view.getActionTarget() + "]");
            }
            return mapping.findForward(view.getActionTarget());
        }
        if (log.isDebugEnabled()) {
            log.debug("createExport() done [target=display]");
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitExportDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitExportDisplay() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        if (view.isArchiveMode()) {
            if (log.isDebugEnabled()) {
                log.debug("exitExportDisplay() view is in archive mode");
            }
            view.selectAvailable(null);
            return mapping.findForward(Actions.ARCHIVE);
        }
        if (log.isDebugEnabled()) {
            log.debug("exitExportDisplay() exit from previously created export");
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward listUnexported(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("listUnexported() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        view.setUnexportedDisplayMode(true);
        view.setSelectedDisplayMode(false);
        return mapping.findForward(Actions.LIST);
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitUnexported(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitUnexported() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        view.setUnexportedDisplayMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectUnexported(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectUnexported() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        view.selectUnexported(new RequestForm(request));
        return mapping.findForward(Actions.LIST);
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward listSelected(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("listSelected() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        view.setSelectedDisplayMode(true);
        view.setUnexportedDisplayMode(false);
        return mapping.findForward(Actions.LIST);
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitSelected(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitSelected() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        view.setSelectedDisplayMode(false);
        return mapping.findForward(view.getActionTarget());
    }
    

    /**
     * Exits from project open amounts list
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward printXml(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("printXml() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        return forwardXmlOutput(mapping, request, view.getXml());
    }

    /**
     * Exits from project open amounts list
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward printPdf(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("printPdf() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        try {
            return forwardPdfOutput(mapping, request, view.getPdf());
        } catch (ClientException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(Actions.DISPLAY);
        }
    }

    /**
     * Forwards to xls output
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward xls(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("xls() request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        return forwardXlsOutput(mapping, request, view.getXls());
    }

    /**
     * Downloads export as zip
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward download(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("download: Request from " + getUser(session).getId());
        }
        RecordExportView view = getExportView(session);
        try {
            DocumentData document = view.getZip();

            String header = document.getContentDisposition();
            byte[] data = document.getBytes();
            String contentType = document.getContentType();
            int length = document.getContentLength();

            response.setContentLength(length);
            response.setContentType(contentType);
            response.setHeader("Content-disposition", header);
            response.getOutputStream().write(data);
            response.getOutputStream().flush();
            return null;

        } catch (ClientException c) {
            return saveErrorAndReturn(request, c.getMessage());
        } catch (Exception e) {
            log.error("download: Failed [message=" + e.getMessage() + "]", e);
            return saveErrorAndReturn(request, ErrorCode.FILE_ACCESS);
        }
    }

    private RecordExportView getExportView(HttpSession session) {
        RecordExportView view = fetchExportView(session);
        if (view == null) {
            throw new com.osserp.common.ActionException("view not bound!");
        }
        return view;
    }

    private RecordExportView fetchExportView(HttpSession session) {
        return (RecordExportView) fetchView(session);
    }

    private RecordExportView fetchOrCreateView(HttpServletRequest request) throws ClientException, PermissionException {
        RecordExportView view = fetchExportView(request.getSession());
        if (view == null) {
            view = (RecordExportView) createView(request);
        }
        return view;
    }

}
