/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2008 4:09:03 PM 
 * 
 */
package com.osserp.core.service.contacts;

import com.osserp.common.ClientException;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Search;
import com.osserp.core.dao.ClassifiedContacts;
import com.osserp.core.dao.Contacts;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractContactRelationSearch extends AbstractService implements Search {

    private Contacts contactsDao = null;
    private ClassifiedContacts contactRelationsDao = null;

    public AbstractContactRelationSearch(Contacts contactsDao, ClassifiedContacts contactRelationsDao) {
        this.contactsDao = contactsDao;
        this.contactRelationsDao = contactRelationsDao;
    }

    public Contact find(Long contactId) throws ClientException {
        if (contactsDao.exists(contactId)) {
            return contactRelationsDao.findByContact(contactId);
        }
        return contactRelationsDao.find(contactId);
    }

    protected Contacts getContactsDao() {
        return contactsDao;
    }

    protected ClassifiedContacts getContactRelationsDao() {
        return contactRelationsDao;
    }
}
