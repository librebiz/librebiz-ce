/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 18, 2008 12:38:44 PM 
 * 
 */
package com.osserp.core.contacts;

import com.osserp.common.EntityRelation;

/**
 * 
 * A contact reference repesents a subset of contact values in addition with values from a related contact, for example a customer
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ContactReference extends Person, EntityRelation {

    /**
     * Provides the company name if interest comes from an organisation
     * @return company
     */
    String getCompany();

    /**
     * Sets the company name
     * @param company
     */
    void setCompany(String company);

    /**
     * Indicates that this contact has accepted receiving newsletters
     * @return newsletterAccepted
     */
    boolean isNewsletterAccepted();

    /**
     * Sets that this contact should receive newsletters
     * @param newsletterAccepted
     */
    void setNewsletterAccepted(boolean newsletterAccepted);

    /**
     * Sets the email address
     * @param email
     */
    void setEmail(String email);

    /**
     * Sets the reference relation
     * @param reference
     */
    void setReference(Long reference);

    void addFax(String country, String prefix, String number);

    void addMobile(String country, String prefix, String number);

    void addPhone(String country, String prefix, String number);
}
