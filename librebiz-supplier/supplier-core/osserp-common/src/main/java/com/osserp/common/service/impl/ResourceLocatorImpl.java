/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 13, 2008 5:57:22 PM 
 * 
 */
package com.osserp.common.service.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.io.IOUtils;

import com.osserp.common.BackendException;
import com.osserp.common.ErrorCode;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.FileUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ResourceLocatorImpl extends AbstractResourceLocator implements ResourceLocator {
    private static Logger log = LoggerFactory.getLogger(ResourceLocatorImpl.class.getName());

    protected ResourceLocatorImpl() {
        super();
    }

    protected ResourceLocatorImpl(String localePath) {
        super(localePath);
    }

    protected Reader getResourceReader(Locale locale) {
        InputStream stream = getResourceStream(locale);
        Reader reader = new StringReader(stream.toString());
        return reader;
    }

    @Override
    protected InputStream getResourceStream(Locale locale) {
        String allProperties = getResourcesByLocalPath(locale);
        try {
            return IOUtils.toInputStream(allProperties, "UTF-8");
        } catch (Exception e) {
            log.warn("getResourceStream() failed streaming resources as text [message="
                    + e.getMessage() + "]\n" + allProperties + "\n");
            throw new BackendException(ErrorCode.INVALID_CONFIG);
        }
    }
    
    protected String getResourcesByLocalPath(Locale locale) {
        locale = (locale == null ? getDefaultLocale() : locale);
        String[] files = null;
        
        String localePath = (getLocalePath() != null ? getLocalePath() : DEFAULT_APP_RES_PATH);
        files = StringUtil.getTokenArray(localePath);
        StringBuffer buffer = new StringBuffer();
        for (int i = 0, j = files.length; i < j; i++) {
            String fileName = toResourceName(files[i].trim(), locale, "properties");
            try {
                InputStream ips = fetchByPath(fileName);
                if (ips == null) {
                    // look up for default file without i18n extension
                    ips = fetchByPath(toResourceName(files[i].trim(), null, "properties"));
                }
                if (ips != null) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(ips));
                    try {
                        String nextLine;
                        while ((nextLine = in.readLine()) != null) {
                            buffer.append(nextLine).append("\n");
                        }
                    } finally {
                        IOUtils.close(in);
                    }
                } else {
                    log.debug("getResourcesByLocalPath() ignoring unavailable [file=" + fileName + "]");
                }
            } catch (Exception e) {
                log.warn("getResourcesByLocalPath() ignoring exception [message=" + e.getMessage() 
                            + ", file=" + fileName + "]");
            }
        }
        return buffer.toString();
    }
    
    protected static InputStream fetchByPath(String fileName) {
        InputStream s = fetchByFileSystemPath(fileName);
        if (s == null) {
            s = fetchByClassPath(fileName);
        }
        return s;
    }
    
    protected static InputStream fetchByFileSystemPath(String fileName) {
        if (FileUtil.exists(fileName)) {
            try {
                return FileUtil.getStream(fileName);
            } catch (Throwable ignorable) {
                // caller should handle this
            }
        }
        return null;
    }

    protected static InputStream fetchByClassPath(String fileName) {
        try {
            return ResourceLocatorImpl.class.getResourceAsStream(fileName);
        } catch (Throwable ignorable) {
            // caller should handle this
        }
        return null;
    }

    public final String toResourceName(String baseName, Locale locale, String suffix) {
        if (!baseName.startsWith("/")) {
            baseName = "/" + baseName;
        }
        String bundleName = baseName;
        if (locale != null) {
            String language = locale.getLanguage();
            bundleName = bundleName + "_" + language;
        }
        String resourceName = bundleName.replace('.', '/') + '.' + suffix;
        return resourceName;
    }

}
