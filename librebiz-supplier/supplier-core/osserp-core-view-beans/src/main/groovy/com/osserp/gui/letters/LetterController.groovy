/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2008 12:18:42 AM 
 * 
 */
package com.osserp.gui.letters

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@RequestMapping("/letters/letter/*")
@Controller class LetterController extends AbstractLetterController {
    private static Logger logger = LoggerFactory.getLogger(LetterController.class.getName())

    @RequestMapping
    def createTemplate(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("createTemplate: invoked [user=${getUserId(request)}]")
        LetterView view = getView(request)
        Long tplId = view.createTemplate()
        if (!tplId) {
            saveError(request, 'createTemplateFailedUnknown')
            return defaultPage
        }
        String exitTarget = view.exitTargetSource ? "&exit=${view.exitTargetSource}" : ''
        String target = "/admin/letters/letterTemplateConfig/forward?templateId=${tplId}${exitTarget}"
        logger.debug("createTemplate: done [id=${tplId}, target=${target}]")
        removeView(request, view)
        redirect(request, target)
    }

    @RequestMapping
    def delete(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("delete: invoked [user=${getUserId(request)}]")
        LetterView view = getView(request)
        view.delete()
        defaultPage
    }

    @RequestMapping
    def release(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("release: invoked [user=${getUserId(request)}]")
        LetterView view = getView(request)
        view.release()
        defaultPage
    }

    @RequestMapping
    def print(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("print: invoked [user=${getUserId(request)}]")
        LetterView view = getView(request)
        savePdf(request, view.getPdf())
        renderPdf(request, response)
    }

    @RequestMapping
    def printTemplate(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("printTemplate: invoked [user=${getUserId(request)}]")
        LetterView view = getView(request)
        savePdf(request, view.getTemplatePdf())
        renderPdf(request, response)
    }
}
