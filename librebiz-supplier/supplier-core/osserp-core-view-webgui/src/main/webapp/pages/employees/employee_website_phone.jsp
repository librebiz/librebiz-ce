<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${contactView.bean}" />
<oc:systemPropertyEnabled name="websiteSyncSupport">
	<c:set var="internetPhone" value="${contact.internetPhone}" />
	<c:choose>
		<c:when test="${!contactView.internetPhoneSelectionMode}">
			<tr>
				<td>
					<a href="<c:url value="/employees.do?method=enableInternetPhoneSelectionMode"/>">
						<fmt:message key="internet" />
					</a>
				</td>
				<td>
					<c:choose>
						<c:when test="${!empty internetPhone}">
							<oc:phone value="${internetPhone}" />
						</c:when>
						<c:otherwise>
							<fmt:message key="notAssigned" />
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</c:when>
		<c:otherwise>
			<c:set var="availablePhones" value="${contact.phoneNumbers}" />
			<tr>
				<td>
					<a href="<c:url value="/employees.do?method=disableInternetPhoneSelectionMode"/>">
						<fmt:message key="internet" />
					</a>
				</td>
				<td>
					<c:forEach var="phone" items="${availablePhones}">
						<a href="<c:url value="/employees.do?method=selectInternetPhone&id=${phone.id}"/>">
							<o:out value="${phone.country}" /> - <o:out value="${phone.prefix}" /> - <o:out value="${phone.number}"	limit="25" />
						</a>
						<br />
					</c:forEach> 
					<a href="<c:url value="/employees.do?method=selectInternetPhone"/>">
						<fmt:message key="delete" />
					</a>
				</td>
			</tr>
		</c:otherwise>
	</c:choose>
</oc:systemPropertyEnabled>
