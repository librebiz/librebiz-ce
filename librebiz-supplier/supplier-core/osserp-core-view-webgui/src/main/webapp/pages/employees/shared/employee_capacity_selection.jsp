<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="list" value="${requestScope.selectList}" />
<c:set var="url" value="${requestScope.selectUrl}" />
<c:choose>
    <c:when test="${!empty requestScope.selectMethod}">
        <c:set var="method" value="${requestScope.selectMethod}" />
    </c:when>
    <c:otherwise>
        <c:set var="method" value="select" />
    </c:otherwise>
</c:choose>

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="left"><fmt:message key="id" /></th>
                <th class="left"><fmt:message key="name" /></th>
                <th class="right"><fmt:message key="open" /></th>
                <th class="right">EUR</th>
                <th class="right"><fmt:message key="stop" /></th>
                <th class="right">EUR</th>
                <th class="right"><fmt:message key="completion" /></th>
                <th class="right">EUR</th>
                <th class="right"><fmt:message key="cancellation" /></th>
                <th class="center" colspan="2"> </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="empl" items="${list}" varStatus="s">
                <tr>
                    <td class="left"><o:out value="${empl.id}" /></td>
                    <td class="left"><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${empl.id}"><o:out value="${empl.name}" /></v:ajaxLink></td>
                    <td class="right"><o:out value="${empl.countOpen}" /></td>
                    <td class="right"><o:number value="${empl.capOpen}" format="decimal" /></td>
                    <td class="right"><o:out value="${empl.countStopped}" /></td>
                    <td class="right"><o:number value="${empl.capStopped}" format="decimal" /></td>
                    <td class="right"><o:out value="${empl.countClosed}" /></td>
                    <td class="right"><o:number value="${empl.capClosed}" format="decimal" /></td>
                    <td class="right"><o:out value="${empl.countLost}" /></td>
                    <td class="icon center">
                        <o:ajaxExecute url="${url}?method=${method}&id=${empl.id}" linkId="foobar"><o:img name="enabledIcon"/></o:ajaxExecute>
                    </td>
                    <td class="icon center">  </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
