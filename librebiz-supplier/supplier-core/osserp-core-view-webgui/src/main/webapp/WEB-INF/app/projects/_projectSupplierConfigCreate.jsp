<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="projectSupplierConfigView" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="createNewDataSet" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:if test="${!empty view.selectedSupplier}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="company"><fmt:message key="company" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:out value="${view.selectedSupplier.displayName}" />
                        </div>
                    </div>
                </div>
            </c:if>

            <c:choose>
                <c:when test="${!empty view.availableGroupNames}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="label"><fmt:message key="label" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="groupName" class="form-control" >
                                    <option value=""><fmt:message key="selection" /></option>
                                    <c:forEach var="grpName" items="${view.availableGroupNames}">
                                        <option value="${grpName}"><o:out value="${grpName}" /></option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="otherLabel"><fmt:message key="otherLabel" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:text name="customGroupName" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="label"><fmt:message key="label" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:text name="groupName" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>

            <v:date name="deliveryDate" styleClass="form-control" label="deliveryDate" picker="true" />

            <div class="row next">
                <div class="col-md-4"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/exit"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="save" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
