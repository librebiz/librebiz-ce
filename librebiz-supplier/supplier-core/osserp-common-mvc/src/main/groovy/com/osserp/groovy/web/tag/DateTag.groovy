/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web.tag


import com.osserp.common.util.DateFormatter
import com.osserp.common.web.tags.TagUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class DateTag extends AbstractInputTag {

    boolean current = false
    boolean picker = false
    String palign = 'bottom'
    String label
    int dateCol = 3
    int labelCol = 4
    String rowClass

    @Override
    protected String getObjectValue() {
        if (value) {
            try {
                return DateFormatter.getDate(value)
            } catch (Exception e) {
            }
        }
        if (current) {
            return DateFormatter.currentDate
        }
        return ''
    }
    
    @Override
    protected String createResult() {
        if ('form-control' == styleClass && label) {
            return createFormControledResult()
        }
        StringBuilder inputTag = new StringBuilder(super.createResult())
        if (picker) {
            inputTag.append(' ').append(TagUtil.createDatepicker(getRequest().getContextPath(), name, autoupdate))
        }
        inputTag.toString()
    }
    
    protected String createFormControledResult() {
        StringBuilder buffer = new StringBuilder('<div class="')
        if (rowClass) {
            buffer.append(rowClass).append('">')
        } else {
            buffer.append('row next">')
        }
        buffer.append('<div class="col-md-').append(labelCol).append('">')
        buffer.append('<div class="form-group"><label for="">')
        buffer.append(getResourceString(label)).append('</label></div></div>')
        buffer.append('<div class="col-md-').append(dateCol).append('"><div class="form-group">')
        buffer.append(super.createResult()).append('</div></div>')
        buffer.append('<div class="col-md-').append(12 - labelCol - dateCol).append('">')
        if (picker) {
            buffer.append(TagUtil.createDatepicker(getRequest().getContextPath(), name, autoupdate))
        }
        buffer.append('</div></div>')
    }
    
    @Override
    protected String renderStyle() {
        if (style) {
            return " style=\"${style}\""
        }
        if ('form-control' != styleClass) {
            return " style=\"width:105px;\""
        }
        return ''
    }
}
