/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 23-Dec-2006 12:54:21 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.finance.StocktakingItem;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StocktakingItemImpl extends AbstractEntity
        implements StocktakingItem {

    private Product product = null;
    private Double averagePurchasePrice = Constants.DOUBLE_NULL;
    private Double lastPurchasePrice = Constants.DOUBLE_NULL;
    private Double expectedQuantity = Constants.DOUBLE_NULL;
    private Double countedQuantity = null;
    private String note = null;

    protected StocktakingItemImpl() {
        super();
    }

    protected StocktakingItemImpl(Long reference, Product product) {
        super(null, reference);
        this.product = product;
        this.lastPurchasePrice = product.getSummary().getLastPurchasePrice();
        this.averagePurchasePrice = product.getSummary().getAveragePurchasePrice();
        this.expectedQuantity = product.getSummary().getStock();
    }

    public Product getProduct() {
        return product;
    }

    protected void setProduct(Product product) {
        this.product = product;
    }

    public Double getLastPurchasePrice() {
        return lastPurchasePrice;
    }

    protected void setLastPurchasePrice(Double lastPurchasePrice) {
        this.lastPurchasePrice = lastPurchasePrice;
    }

    public Double getAveragePurchasePrice() {
        return averagePurchasePrice;
    }

    protected void setAveragePurchasePrice(Double averagePurchasePrice) {
        this.averagePurchasePrice = averagePurchasePrice;
    }

    public Double getExpectedQuantity() {
        return expectedQuantity;
    }

    protected void setExpectedQuantity(Double expectedQuantity) {
        this.expectedQuantity = expectedQuantity;
    }

    public Double getCountedQuantity() {
        return countedQuantity;
    }

    public void setCountedQuantity(Double countedQuantity) {
        this.countedQuantity = countedQuantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
