/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 6, 2009 7:53:04 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.Products;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductSelectionItemAware;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
public class AbstractProductSelectionAwares extends AbstractProductAwareDao {
    private static Logger log = LoggerFactory.getLogger(AbstractProductSelectionAwares.class.getName());
    private Products products = null;

    /**
     * Creates a new product selection aware dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param optionsCache
     * @param products
     */
    protected AbstractProductSelectionAwares(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache,
            Products products) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache);
        this.products = products;
    }

    /**
     * Loads all product provided by primary key list
     * @param primaryKeys
     * @return products
     */
    protected final List<Product> load(List<Long> primaryKeys) {
        return products.load(primaryKeys);
    }

    /**
     * Creates an product id select statement by product selection config
     * @param selection
     * @return select statement with all filters created by config item
     */
    protected final StringBuilder createSelect(ProductSelectionItemAware selection, boolean includeEol) {
        StringBuilder sql = new StringBuilder(96);
        sql.append("SELECT product_id FROM ").append(productTable);
        if (selection != null && !selection.getItems().isEmpty()) {
            sql.append(" WHERE ");
            if (!includeEol) {
                sql.append(" end_of_life = false AND (");
            }
            boolean addedSomething = false;
            for (int i = 0, j = selection.getItems().size(); i < j; i++) {
                ProductSelectionConfigItem item = selection.getItems().get(i);
                if (addedSomething) {
                    sql.append(" OR ");
                }
                sql.append(createClause(item));
                addedSomething = true;
            }
            if (!includeEol) {
                sql.append(")");
            }
        } else if (selection == null) {
            if (!includeEol) {
                sql.append(" WHERE end_of_life = false");
            }
        }
        return sql;
    }

    // SELECT product_id FROM osserp.products WHERE 
    //	(product_id IN (SELECT product_id FROM osserp.product_types_relations WHERE type_id = 1) AND group_id = 3) AND end_of_life = false ORDER BY name	
    /**
     * Creates an product id select statement by product selection item
     * @param item
     * @return select statement with filter created by item
     */
    protected final StringBuilder createSelect(ProductSelectionConfigItem item, boolean includeEol) {
        StringBuilder sql = new StringBuilder(96);
        sql.append("SELECT product_id FROM ").append(productTable);
        if (item != null) {
            String clause = createClause(item);
            if (isSet(clause)) {
                sql.append(" WHERE ");
                if (!includeEol) {
                    sql.append(" end_of_life = false AND (").append(clause).append(")");
                } else {
                    sql.append(clause);
                }

            }
        } else {
            if (!includeEol) {
                sql.append(" WHERE ").append(" end_of_life = false");
            }
        }
        return sql;
    }

    /**
     * Creates a filter clause by product selection item
     * @param item
     * @return sql clause like '(type_id in x and group_id = y and category_id = z)' including brackets
     */
    protected final String createClause(ProductSelectionConfigItem item) {
        StringBuilder result = new StringBuilder();
        if (item != null) {
            if (item.getProduct() != null && item.getProduct().getProductId() != null) {
                result
                        .append("(product_id = ")
                        .append(item.getProduct().getProductId())
                        .append(")");
                return result.toString();
            }
            StringBuilder sql = new StringBuilder(128);
            sql.append("(");
            boolean addedSomething = false;
            if (item.getType() != null) {
                sql
                        .append("product_id IN (SELECT product_id FROM ")
                        .append(productTypeRelationsTable)
                        .append(" WHERE type_id = ")
                        .append(item.getType().getId())
                        .append(")");
                addedSomething = true;
            }
            if (item.getGroup() != null) {
                if (addedSomething) {
                    sql.append(" AND group_id = ");
                } else {
                    sql.append("group_id = ");
                }
                sql.append(item.getGroup().getId());
                addedSomething = true;
            }
            if (item.getCategory() != null) {
                if (addedSomething) {
                    sql.append(" AND category_id = ");
                } else {
                    sql.append(" category_id = ");
                }
                sql.append(item.getCategory().getId());
                addedSomething = true;
            }
            if (addedSomething) {
                sql.append(")");
                result.append(sql);
            } else {
                log.warn("createClause() invoked with misconfigured item [id=" + item.getId() + "]");
            }
        }
        return result.toString();
    }
}
