/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30.05.2012 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.osserp.common.Parameter;
import com.osserp.common.beans.OptionImpl;

import com.osserp.core.BankAccount;
import com.osserp.core.ContractStatus;
import com.osserp.core.ContractTermination;
import com.osserp.core.ContractTerms;
import com.osserp.core.ContractTermsProperty;
import com.osserp.core.ContractType;
import com.osserp.core.sales.SalesContract;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesContractImpl extends OptionImpl implements SalesContract {

    private Long contactId;
    private ContractStatus status;
    private ContractTermination termination;
    private ContractType type;
    private BankAccount bankAccount;
    private Date contractStart;
    private int monthsRunning = 6;
    private int billingDays = 30;
    private String billingDayFixed = "01";
    private boolean trackingRequired = false;
    private Long trackingTypeDefault;
    private Double timeIncluded = 0d;
    private boolean allTimeIncluded = false;
    private Double transportIncluded = 0d;
    private boolean allTransportIncluded = false;
    private Double materialIncluded = 0d;
    private boolean allMaterialIncluded = false;
    private boolean allServiceIncluded = false;
    private List<SalesContractTermsValueImpl> values = new ArrayList<SalesContractTermsValueImpl>();
    private List<ContractTerms> _terms = new ArrayList<ContractTerms>();

    protected SalesContractImpl() {
        super();
    }

    /**
     * Creates a new sales contract
     * @param type
     * @param reference
     * @param contactId
     * @param createdBy
     * @param name
     */
    public SalesContractImpl(ContractType type, Long reference, Long contactId, Long createdBy, String name) {
        super((Long) null, reference, name, createdBy);
        this.type = type;
        this.contactId = contactId;
    }

    public Long getContactId() {
        return contactId;
    }

    protected void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public ContractStatus getStatus() {
        return status;
    }

    public void setStatus(ContractStatus status) {
        this.status = status;
    }

    public ContractTermination getTermination() {
        return termination;
    }

    public void setTermination(ContractTermination termination) {
        this.termination = termination;
    }

    public ContractType getType() {
        return type;
    }

    protected void setType(ContractType type) {
        this.type = type;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Date getContractStart() {
        return contractStart;
    }

    public void setContractStart(Date contractStart) {
        this.contractStart = contractStart;
    }

    public int getMonthsRunning() {
        return monthsRunning;
    }

    public void setMonthsRunning(int monthsRunning) {
        this.monthsRunning = monthsRunning;
    }

    public int getBillingDays() {
        return billingDays;
    }

    public void setBillingDays(int billingDays) {
        this.billingDays = billingDays;
    }

    public String getBillingDayFixed() {
        return billingDayFixed;
    }

    public void setBillingDayFixed(String billingDayFixed) {
        this.billingDayFixed = billingDayFixed;
    }

    public boolean isTrackingRequired() {
        return trackingRequired;
    }

    public void setTrackingRequired(boolean trackingRequired) {
        this.trackingRequired = trackingRequired;
    }

    public Long getTrackingTypeDefault() {
        return trackingTypeDefault;
    }

    public void setTrackingTypeDefault(Long trackingTypeDefault) {
        this.trackingTypeDefault = trackingTypeDefault;
    }

    public Double getTimeIncluded() {
        return timeIncluded;
    }

    public void setTimeIncluded(Double timeIncluded) {
        this.timeIncluded = timeIncluded;
    }

    public boolean isAllTimeIncluded() {
        return allTimeIncluded;
    }

    public void setAllTimeIncluded(boolean allTimeIncluded) {
        this.allTimeIncluded = allTimeIncluded;
    }

    public Double getTransportIncluded() {
        return transportIncluded;
    }

    public void setTransportIncluded(Double transportIncluded) {
        this.transportIncluded = transportIncluded;
    }

    public boolean isAllTransportIncluded() {
        return allTransportIncluded;
    }

    public void setAllTransportIncluded(boolean allTransportIncluded) {
        this.allTransportIncluded = allTransportIncluded;
    }

    public Double getMaterialIncluded() {
        return materialIncluded;
    }

    public void setMaterialIncluded(Double materialIncluded) {
        this.materialIncluded = materialIncluded;
    }

    public boolean isAllMaterialIncluded() {
        return allMaterialIncluded;
    }

    public void setAllMaterialIncluded(boolean allMaterialIncluded) {
        this.allMaterialIncluded = allMaterialIncluded;
    }

    public boolean isAllServiceIncluded() {
        return allServiceIncluded;
    }

    public void setAllServiceIncluded(boolean allServiceIncluded) {
        this.allServiceIncluded = allServiceIncluded;
    }

    protected List<SalesContractTermsValueImpl> getValues() {
        return values;
    }

    protected void setValues(List<SalesContractTermsValueImpl> values) {
        this.values = values;
    }

    public List<ContractTerms> getTerms() {
        if (_terms.isEmpty()) {
            loadTerms();
        }
        return _terms;
    }

    public void reloadTerms() {
        loadTerms();
    }

    public void setTermsValue(String name, String value) {
        boolean exists = false;
        for (int i = 0, j = values.size(); i < j; i++) {
            SalesContractTermsValueImpl next = values.get(i);
            if (next.getName().equals(name)) {
                next.setValue(value);
                exists = true;
                break;
            }
        }
        if (!exists) {
            this.values.add(new SalesContractTermsValueImpl(this, name, value));
        }
    }

    private void loadTerms() {
        _terms = new ArrayList<ContractTerms>();
        if (type != null && !type.getTerms().isEmpty()) {
            Map<String, String> mapped = new HashMap<String, String>();
            for (int i = 0, j = values.size(); i < j; i++) {
                Parameter next = values.get(i);
                mapped.put(next.getName(), next.getValue());
            }
            for (int k = 0, l = type.getTerms().size(); k < l; k++) {
                ContractTerms terms = type.getTerms().get(k);
                for (int m = 0, n = terms.getProperties().size(); m < n; m++) {
                    ContractTermsProperty property = terms.getProperties().get(m);
                    property.setObjectValue(mapped.get(property.getResourceKey()));
                }
                _terms.add(terms);
            }
        }
    }
}
