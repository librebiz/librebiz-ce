/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 16, 2013 4:56:07 PM 
 * 
 */
package com.osserp.common.directory.ldap

import org.junit.Test

import com.osserp.common.directory.ldap.LdapUtils

import static org.junit.Assert.*

/**
 * Unit tests for LdapUtils
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
class ULdapUtils {

    @Test
    void fetchClientName() {
        String dn = 'cn=Purchasing,ou=Groups,cn=osserp,ou=clients,dc=osserp,dc=org'
        String client = LdapUtils.fetchClientName(dn)
        assertTrue('osserp' == client)
    }

    @Test
    void fetchClientNameWhenClientNameDoesNotExist() {
        String dn = 'cn=Purchasing,ou=Groups,dc=osserp,dc=org'
        String client = LdapUtils.fetchClientName(dn)
        assertTrue(!client)
    }

    @Test
    void createClientNocDn() {
        String dn = 'uid=username,ou=Users,cn=osserp,ou=clients,dc=osserp,dc=org'
        String expected = 'uid=username,ou=Users,cn=osserp,ou=clients,dc=noc,dc=osserp,dc=org'
        String nocdn = LdapUtils.createClientNocDn(dn)
        println nocdn
        assertTrue(nocdn == expected)
    }

    @Test
    void createClientNocDnWithExistingNocDn() {
        String dn = 'uid=username,ou=Users,cn=osserp,ou=clients,dc=noc,dc=osserp,dc=org'
        String nocdn = LdapUtils.createClientNocDn(dn)
        println nocdn
        assertTrue(nocdn == dn)
    }

    @Test
    void createClientNocDnWithNullDn() {
        String dn = null
        String nocdn = LdapUtils.createClientNocDn(dn)
        assertTrue(nocdn == null)
    }
}
