<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${contactView.bean}" />

<tr>
	<td>
		<o:permission role="employee_assign,employee_activation,permission_grant,client_edit,ldap_admin" info="permissionEditLdapAccount">
			<c:choose>
				<c:when test="${empty contactView.userAccount.ldapUid}">
					<v:link url="/users/accountManagement/forward?id=${contact.id}&exit=/employees.do?method=reload" title="createAccount">
						<fmt:message key="account" />
					</v:link>
				</c:when>
				<c:otherwise>
                    <o:permission role="employee_activation,permission_grant,client_edit,ldap_admin" info="permissionChangePassword">
					<v:link url="/users/accountManagement/forward?uid=${contactView.userAccount.ldapUid}&exit=/employees.do?method=reload" title="editAccount">
						<fmt:message key="account" />
					</v:link>
                    </o:permission> 
                    <o:forbidden role="employee_activation,permission_grant,client_edit,ldap_admin">
                        <fmt:message key="account" />
                    </o:forbidden>
				</c:otherwise>
			</c:choose>
		</o:permission> 
		<o:forbidden role="employee_assign,employee_activation,permission_grant,client_edit,ldap_admin">
			<fmt:message key="account" />
		</o:forbidden>
	</td>
	<td>
		<c:choose>
			<c:when test="${empty contactView.userAccount.ldapUid}">
				<fmt:message key="notCreatedText" />
			</c:when>
			<c:otherwise>
				<o:out value="${contactView.userAccount.ldapUid}" />
			</c:otherwise>
		</c:choose>
	</td>
</tr>
<tr>
	<td>
    	<o:permission role="employee_activation,permission_grant,client_edit,ldap_admin" info="permissionEditPermissions">
			<v:link url="/users/userPermissionSetup/forward?id=${contactView.userAccount.id}&exit=/employees.do?method=reload" title="dbStatusTitle">
				<fmt:message key="dbStatus"/>
			</v:link>
		</o:permission> 
		<o:forbidden role="employee_activation,permission_grant,client_edit,ldap_admin">
			<span title="<fmt:message key="dbStatusTitle"/>"><fmt:message key="dbStatus"/></span>
		</o:forbidden>
	</td>
	<td>
		<c:choose>
			<c:when test="${empty contactView.userAccount}">
				<fmt:message key="notAvailable"/>
			</c:when>
			<c:when test="${contactView.userAccount.active}">
				<fmt:message key="activated"/>
			</c:when>
			<c:otherwise>
				<span class="errortext"> <fmt:message key="deactivated"/>
				</span>
			</c:otherwise>
		</c:choose>
	</td>
</tr>
