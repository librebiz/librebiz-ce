/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22.12.2013 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.core.system.SyncTask;
import com.osserp.core.system.SyncTaskConfig;
import com.osserp.core.dao.SyncTasks;
import com.osserp.core.model.system.SyncTaskConfigImpl;
import com.osserp.core.model.system.SyncTaskImpl;

/**
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SyncTasksDao extends AbstractRepository implements SyncTasks {
    private static Logger log = LoggerFactory.getLogger(SyncTasksDao.class.getName());

    protected SyncTasksDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public SyncTaskConfig getConfig(Long id) {
        try {
            return (SyncTaskConfig) getCurrentSession().load(SyncTaskConfigImpl.class.getName(), id);
        } catch (Throwable t) {
            log.warn("getConfig() failed [id=" + id + ", message=" + t.getMessage() + "]");
        }
        return null;
    }

    public void create(SyncTaskConfig config, String typeName, String actionName, String objectId, String objectData) {
        boolean syncTaskCreated = false;
        if (config != null && typeName != null && actionName != null && (objectId != null || objectData != null)) {
            if ((objectId != null && !syncTaskExists(config, typeName, actionName, objectId)) || objectData != null) {
                SyncTask task = new SyncTaskImpl(config, typeName, actionName, objectId, objectData);
                getCurrentSession().save(SyncTaskImpl.class.getName(), task);
                syncTaskCreated = true;
                if (log.isDebugEnabled()) {
                    log.debug("create() done [id=" + task.getId()
                        + ", config=" + task.getConfig().getId()
                        + ", type=" + task.getTypeName()
                        + ", action=" + task.getActionName()
                        + ", object=" + task.getObjectId()
                        + ", data=" + task.getObjectData()
                        + "]");
                }
            }
        }
        if (!syncTaskCreated) {
            log.debug("create() failed: Task already exists or required input missing [config="
                    + (config == null ? "null" : config.getId())
                    + ", type=" + typeName
                    + ", action=" + actionName
                    + ", object=" + objectId
                    + ", objectData=" + objectData
                    + "]");
        }
    }

    public List<SyncTask> find(SyncTaskConfig config) {
        StringBuilder hql = new StringBuilder(128);
        hql
            .append("from ")
            .append(SyncTaskImpl.class.getName())
            .append(" o where o.config.id = :configId order by o.created desc");
        return getCurrentSession().createQuery(hql.toString())
                .setParameter("configId", config.getId()).list();
    }

    public List<SyncTask> findOpen() {
        StringBuilder hql = new StringBuilder(128);
        hql
            .append("from ")
            .append(SyncTaskImpl.class.getName())
            .append(" o where o.status = 0 order by o.created");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public boolean runningTaskExists(SyncTaskConfig config) {
        StringBuilder hql = new StringBuilder(128);
        hql
                .append("from ").append(SyncTaskImpl.class.getName())
                .append(" o where o.config.id = :configId and o.status = 1");
        return !getCurrentSession().createQuery(hql.toString())
                .setParameter("configId", config.getId()).list().isEmpty();
    }

    public void close(SyncTask syncTask) {
        syncTask.setClosed();
        save(syncTask);
        //getCurrentSession().delete(SyncTaskImpl.class.getName(), syncTask);
        if (log.isDebugEnabled()) {
            log.debug("close() done [id=" + syncTask.getId()
                    + ", config=" + syncTask.getConfig().getId()
                    + ", type=" + syncTask.getTypeName()
                    + ", action=" + syncTask.getActionName()
                    + ", object=" + syncTask.getObjectId()
                    + "]");
        }
    }

    public void lock(SyncTask syncTask) {
        syncTask.setRunning();
        save(syncTask);
    }

    public void stop(SyncTask syncTask, String errorMessage) {
        syncTask.stop(errorMessage);
        save(syncTask);
    }

    public void update(SyncTask syncTask, String objectData) {
        syncTask.setObjectData(objectData);
        save(syncTask);
    }

    private void save(SyncTask syncTask) {
        getCurrentSession().saveOrUpdate(SyncTaskImpl.class.getName(), syncTask);
    }

    private boolean syncTaskExists(SyncTaskConfig config, String typeName, String actionName, String objectId) {
        StringBuilder hql = new StringBuilder(128);
        hql
                .append("from ").append(SyncTaskImpl.class.getName())
                .append(" o where o.config.id = :configid and o.status = 0 and")
                .append(" o.typeName = :typename and o.actionName = :actionname and o.objectId = :objectid");
        return !getCurrentSession().createQuery(hql.toString())
                .setParameter("configId", config.getId())
                .setParameter("typename", typeName)
                .setParameter("actionname", actionName)
                .setParameter("objectId", objectId)
                .list().isEmpty();
    }
}
