/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 16:51:26 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.finance.Amounts;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class AmountsImpl implements Amounts {
    private BigDecimal amount = null;
    private BigDecimal amountWithBaseTax = null;
    private BigDecimal amountWithReducedTax = null;
    private BigDecimal amountWithZeroTax = null;
    private BigDecimal taxAmount = null;
    private BigDecimal reducedTaxAmount = null;
    private BigDecimal grossAmount = null;
    private Double taxRate = null;
    private Double reducedTaxRate = null;
    private boolean fixedTaxCalculation = false;

    protected AmountsImpl() {
        super();
    }

    protected AmountsImpl(Double taxRate, Double reducedTaxRate) {
        super();
        this.taxRate = taxRate;
        this.reducedTaxRate = reducedTaxRate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public BigDecimal getReducedTaxAmount() {
        return reducedTaxAmount;
    }

    public BigDecimal getGrossAmount() {
        return grossAmount;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public Double getReducedTaxRate() {
        return reducedTaxRate;
    }

    public BigDecimal getAmountWithBaseTax() {
        return amountWithBaseTax;
    }

    public BigDecimal getAmountWithReducedTax() {
        return amountWithReducedTax;
    }

    public BigDecimal getAmountWithZeroTax() {
	    return amountWithZeroTax;
    }

    public boolean isFixedTaxCalculation() {
        return fixedTaxCalculation;
    }

    protected void fixedTaxCalculation(
            String tax,
            String reducedTax,
            String gross) throws ClientException {

        BigDecimal tTaxAmount = NumberUtil.createDecimal(tax);
        BigDecimal tReducedTaxAmount = NumberUtil.createDecimal(reducedTax);
        BigDecimal grossResult = amount.add(tTaxAmount).add(tReducedTaxAmount);
        double diffResult = NumberUtil.round(grossAmount.doubleValue() - grossResult.doubleValue(), 5);
        if (diffResult < 0) {
            diffResult = diffResult * (-1);
        }
        BigDecimal tGrossAmount = NumberUtil.createDecimal(gross);
        double diffInput = NumberUtil.round(grossAmount.doubleValue() - tGrossAmount.doubleValue(), 5);
        if (diffInput < 0) {
            diffInput = diffInput * (-1);
        }
        if (diffResult > 0.01d || diffInput > 0.01d) {
            throw new ClientException(ErrorCode.VALUES_INVALID);
        }
        taxAmount = tTaxAmount;
        reducedTaxAmount = tReducedTaxAmount;
        grossAmount = tGrossAmount;
        fixedTaxCalculation = true;
    }

    // protected setters

    protected void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    protected void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    protected void setReducedTaxAmount(BigDecimal reducedTaxAmount) {
        this.reducedTaxAmount = reducedTaxAmount;
    }

    protected void setGrossAmount(BigDecimal grossAmount) {
        this.grossAmount = grossAmount;
    }

    protected void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    protected void setReducedTaxRate(Double reducedTaxRate) {
        this.reducedTaxRate = reducedTaxRate;
    }

    protected void setAmountWithBaseTax(BigDecimal amountWithBaseTax) {
        this.amountWithBaseTax = amountWithBaseTax;
    }

    protected void setAmountWithReducedTax(BigDecimal amountWithReducedTax) {
        this.amountWithReducedTax = amountWithReducedTax;
    }

    protected void setAmountWithZeroTax(BigDecimal amountWithZeroTax) {
        this.amountWithZeroTax = amountWithZeroTax;
    }
}
