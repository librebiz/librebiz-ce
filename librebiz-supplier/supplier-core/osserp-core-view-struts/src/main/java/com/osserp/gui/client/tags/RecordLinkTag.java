/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 15, 2015 
 * 
 */
package com.osserp.gui.client.tags;

import javax.servlet.jsp.JspException;

import com.osserp.common.web.tags.AbstractLinkTag;
import com.osserp.groovy.web.WebUtil;

import com.osserp.core.finance.CommonPayment;
import com.osserp.core.finance.FinanceRecord;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.RecordType;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordLinkTag extends AbstractLinkTag {

    private FinanceRecord record = null;
    private String exit = null;
    private String viewExit = null;
    private boolean readonly = true;

    public void setRecord(FinanceRecord record) {
        this.record = record;
    }

    public void setExit(String exit) {
        this.exit = exit;
    }

    public void setViewExit(String viewExit) {
        this.viewExit = viewExit;
    }

    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    @Override
    public String getStartTag() throws JspException {
        if (record != null && record.getType() != null) {
            StringBuilder buffer = new StringBuilder(LINK_START);
            buffer.append(createLink());
            if (readonly) {
                buffer.append("&readonly=true\"");
            }
            appendEnd(buffer);
            return buffer.toString();
        }
        return "";
    }
    
    protected String createLink() {
        Long recordId = record.getId();
        StringBuilder buffer = new StringBuilder();
        if (record instanceof CommonPayment) {
            buffer
                .append(WebUtil.getViewPath(getServletContext(), getRequest()))
                .append("/accounting/commonPayment/forward?id=")
                .append(record.getId());
            if (viewExit != null && viewExit.length() > 0) {
                buffer.append("&exit=").append(viewExit);
            }
            
        } else {
            buffer.append(getContextPath());
            
            if (record instanceof Payment) {
                Payment payment = (Payment) record;
                if (RecordType.PURCHASE_INVOICE.equals(payment.getRecordTypeId())) {
                    buffer.append("/purchaseInvoice.do?method=display&id=");
                    recordId = payment.getRecordId();
                } else if (RecordType.SALES_INVOICE.equals(payment.getRecordTypeId())) {
                    buffer.append("/salesInvoice.do?method=display&id=");
                    recordId = payment.getRecordId();
                }
            
            } else if (RecordType.PURCHASE_INVOICE.equals(record.getType().getId())) {
                buffer.append("/purchaseInvoice.do?method=display&id=");
            } else if (RecordType.PURCHASE_OFFER.equals(record.getType().getId())) {
                buffer.append("/purchaseOffer.do?method=display&id=");
            } else if (RecordType.PURCHASE_ORDER.equals(record.getType().getId())) {
                buffer.append("/purchaseOrder.do?method=display&id=");
            } else if (RecordType.PURCHASE_RECEIPT.equals(record.getType().getId())) {
                buffer.append("/purchaseDeliveryNote.do?method=display&id=");
            } else if (RecordType.SALES_CREDIT_NOTE.equals(record.getType().getId())) {
                buffer.append("/salesCreditNote.do?method=display&id=");
            } else if (RecordType.SALES_CANCELLATION.equals(record.getType().getId())) {
                buffer.append("/salesCancellation.do?method=display&id=");
            } else if (RecordType.SALES_DELIVERY_NOTE.equals(record.getType().getId())) {
                buffer.append("/salesDeliveryNote.do?method=display&id=");
            } else if (RecordType.SALES_OFFER.equals(record.getType().getId())) {
                buffer.append("/salesOffer.do?method=display&id=");
            } else if (RecordType.SALES_ORDER.equals(record.getType().getId())) {
                buffer.append("/salesOrder.do?method=display&id=");
            } else if (RecordType.SALES_DOWNPAYMENT.equals(record.getType().getId())) {
                buffer.append("/salesInvoice.do?method=displayDownpayment&id=");
            } else if (RecordType.SALES_INVOICE.equals(record.getType().getId())) {
                buffer.append("/salesInvoice.do?method=display&id=");
            }
            buffer.append(recordId);
            if (exit != null && exit.length() > 0) {
                buffer.append("&exit=").append(exit);
            }
        }
        return buffer.toString();
    }
}
