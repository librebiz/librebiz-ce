/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 29, 2009 10:29:46 AM 
 * 
 */
package com.osserp.core.service.requests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Requests;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.RequestObservationManager;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.tickets.RequestResetWarningTicket;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestObservationManagerImpl extends AbstractService implements RequestObservationManager {
    private Employees employees = null;
    private Requests requests = null;
    private RequestResetWarningProcessor requestResetWarningService = null;

    public RequestObservationManagerImpl(
            Requests requests,
            RequestResetWarningProcessor requestResetWarningService,
            Employees employees) {

        this.requests = requests;
        this.requestResetWarningService = requestResetWarningService;
        this.employees = employees;
    }

    public void checkAndResetInvalidRequests() {
        requests.executeResetWarningTasks();
        Map<Long, List<RequestResetWarningTicket>> warnings = getWarnings();
        Set<Long> sales = warnings.keySet();
        for (java.util.Iterator<Long> i = sales.iterator(); i.hasNext();) {
            Employee employee = employees.get(i.next());
            if (employee.getEmail() != null) {
                List<RequestResetWarningTicket> list = warnings.get(employee.getId());
                Map<String, Object> values = new HashMap<String, Object>();
                values.put("salesName", employee.getSalutationDisplay());
                values.put("requests", list);
                requestResetWarningService.execute(new String[] { employee.getEmail() }, values);
                closeResetWarnings(list);
            }
        }
    }

    private Map<Long, List<RequestResetWarningTicket>> getWarnings() {
        Map<Long, List<RequestResetWarningTicket>> warnings = new HashMap<Long, List<RequestResetWarningTicket>>();
        List<RequestResetWarningTicket> tickets = requests.getOpenRequestResetWarnings();
        for (int i = 0, j = tickets.size(); i < j; i++) {
            RequestResetWarningTicket next = tickets.get(i);
            if (warnings.containsKey(next.getSalesId())) {
                warnings.get(next.getSalesId()).add(next);
            } else {
                List<RequestResetWarningTicket> list = new ArrayList<RequestResetWarningTicket>();
                list.add(next);
                warnings.put(next.getSalesId(), list);
            }
        }
        return warnings;
    }

    private void closeResetWarnings(List<RequestResetWarningTicket> tickets) {
        for (int i = 0, j = tickets.size(); i < j; i++) {
            RequestResetWarningTicket next = tickets.get(i);
            requests.closeOpenRequestResetWarning(next);
        }
    }

}
