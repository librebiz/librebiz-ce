<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesRecordCreatorView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="createManuallyRecord" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="salesRecordCreatorContent">
            <div class="row">
                <v:form url="/sales/salesRecordCreator/save" name="salesRecordCreatorForm">

                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <o:out value="${view.customer.displayName}" />
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="issuer"><fmt:message key="issuer" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <c:choose>
                                            <c:when test="${!empty view.selectedBranch}">
                                                <o:out value="${view.selectedBranch.company.name}" /> / <o:out value="${view.selectedBranch.name}" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="branch" items="${view.availableBranchs}" varStatus="s">
                                                    <c:if test="${s.index > 0}">
                                                        <br />
                                                        <br />
                                                    </c:if>
                                                    <v:link url="/sales/salesRecordCreator/selectBranch?id=${branch.id}">
                                                        <o:out value="${branch.company.name}" /> / <o:out value="${branch.name}" />
                                                    </v:link>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="recordType"><fmt:message key="record" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <c:choose>
                                            <c:when test="${!empty view.selectedRecordType}">
                                                <o:out value="${view.selectedRecordType.name}" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="recordType" items="${view.availableRecordTypes}" varStatus="s">
                                                    <c:if test="${s.index > 0}">
                                                        <br />
                                                        <br />
                                                    </c:if>
                                                    <v:link url="/sales/salesRecordCreator/selectRecordType?id=${recordType.id}">
                                                        <o:out value="${recordType.name}" />
                                                    </v:link>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>

                            <c:if test="${!empty view.availableRecord}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="copyOf"><fmt:message key="copyOf" /></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <o:out value="${view.availableRecord.number}" />
                                            <fmt:message key="dated" />
                                            <o:date value="${view.availableRecord.created}" />
                                        </div>
                                    </div>
                                </div>
                            </c:if>

                            <c:if test="${!empty view.selectedBranch and !empty view.selectedRecordType}">
                                <c:if test="${view.manualNumberInputAvailable or view.manualDateInputAvailable or view.historicalRecordEnabled}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="options"> </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <fmt:message key="options" />
                                                :
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test="${view.manualNumberInputAvailable}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="customRecordNumber"><fmt:message key="recordNumber" /></label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <v:text name="recordNumber" styleClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test="${view.manualDateInputAvailable}">
                                    <v:date name="recordDate" label="recordDate" picker="true" styleClass="form-control" />
                                </c:if>
                                <c:if test="${view.historicalRecordEnabled}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="recordExternal"><fmt:message key="document" /></label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label for="headquarter"> <v:checkbox id="historical" /> <fmt:message key="recordExternal" />
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>

                                <div class="row next">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <v:submitExit url="/sales/salesRecordCreator/exit" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <o:submit styleClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>

                        </div>
                    </div>

                </v:form>
            </div>
        </div>

    </tiles:put>
</tiles:insert>
