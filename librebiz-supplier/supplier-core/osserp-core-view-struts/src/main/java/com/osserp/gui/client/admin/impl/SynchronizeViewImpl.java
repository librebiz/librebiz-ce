/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Sep 16, 2009 
 * 
 */
package com.osserp.gui.client.admin.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Option;
import com.osserp.common.web.ViewName;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.contacts.PrivateContactManager;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroupManager;
import com.osserp.core.employees.EmployeeManager;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.DomainUserManager;
import com.osserp.gui.client.admin.SynchronizeView;
import com.osserp.gui.client.impl.AbstractView;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
@ViewName("synchronizeView")
public class SynchronizeViewImpl extends AbstractView implements SynchronizeView {
    private static Logger log = LoggerFactory.getLogger(SynchronizeViewImpl.class.getName());

    private List<Option> employees = getEmployeeManager().findAllActiveEmployees();

    protected SynchronizeViewImpl() {
        super();
    }

    public void syncBranches() {
        SystemConfigManager systemConfigManager = getSystemConfigManager();
        systemConfigManager.synchronizeBranchs();
    }

    public void syncEmployeeGroups() {
        EmployeeGroupManager groupManager = (EmployeeGroupManager) getService(EmployeeGroupManager.class.getName());
        groupManager.synchronizeLdapGroups();
    }

    public void syncEmployees() {
        EmployeeManager employeeManager = getEmployeeManager();
        for (Iterator<Option> iterator = employees.iterator(); iterator.hasNext();) {
            Option option = iterator.next();
            Employee employee = (Employee) employeeManager.find(option.getId());
            this.syncEmployee(employee);
        }
        if (log.isDebugEnabled()) {
            log.debug("syncEmployees() done [employees=" + employees.size() + "]");
        }
    }

    public void syncEmployee(Long employeeId) {
        EmployeeManager employeeManager = getEmployeeManager();
        Employee employee = (Employee) employeeManager.find(employeeId);
        if (employee != null) {
            this.syncEmployee(employee);
        }
    }

    private void syncEmployee(Employee employee) {
        DomainUserManager domainUserManager = getDomainUserManager();
        EmployeeManager employeeManager = getEmployeeManager();
        employeeManager.synchronizeLdapUser(employee);

        DomainUser domainUser = (DomainUser) domainUserManager.find(employee);
        PrivateContactManager privateContactManager = (PrivateContactManager) getService(PrivateContactManager.class.getName());
        List<PrivateContact> privateContacts = privateContactManager.findPrivate(domainUser);
        for (Iterator<PrivateContact> it = privateContacts.iterator(); it.hasNext();) {
            PrivateContact privateContact = it.next();
            privateContactManager.sync(privateContact);
        }
        if (log.isDebugEnabled()) {
            log.debug("syncEmployee() done [id=" + employee.getId() + ", privateContacts=" + privateContacts.size() + "]");
        }
    }

    public void syncPrivateContacts() {
        // TODO LDAP add new synchronization
        /*
         * DomainUserManager domainUserManager = getDomainUserManager(); DomainLdapManager domainLdapManager = (DomainLdapManager)
         * getService(DomainLdapManager.class.getName()); PrivateContactManager privateContactManager = (PrivateContactManager)
         * getService(PrivateContactManager.class.getName());
         * 
         * List<Option> employees = getEmployeeManager().findAllActiveEmployees(); for (Iterator<Option> iterator = employees.iterator();
         * iterator.hasNext();) { Option option = iterator.next(); Employee employee = (Employee) getEmployeeManager().find(option.getId()); DomainUser
         * domainUser = (DomainUser) domainUserManager.find(employee); String ldapUid = domainUser.getLdapUid(); LdapUser ldapUser =
         * domainLdapManager.lookupUser(ldapUid); if (ldapUser != null && ldapUser.isGroupwareEnabled()) {
         * 
         * List<PrivateContact> privateContacts = privateContactManager.findPrivate(domainUser); for (Iterator<PrivateContact> it = privateContacts.iterator();
         * it.hasNext();) { PrivateContact privateContact = it.next(); privateContactManager.sync(privateContact); } if (log.isDebugEnabled()) {
         * log.debug("syncPrivateContacts() [employee=" + option.getId() + ", privateContacts=" + privateContacts.size() + "]"); }
         * 
         * } else { if (log.isDebugEnabled()) { log.debug("syncPrivateContacts() no LdapUser found with ldapUid = " + ldapUid); } } } if (log.isDebugEnabled())
         * { log.debug("syncPrivateContacts() done [employees=" + employees.size() + "]"); }
         */
    }

    public void syncAll() {
        this.syncBranches();
        this.syncEmployeeGroups();
        this.syncEmployees();
        this.syncPrivateContacts();
        if (log.isDebugEnabled()) {
            log.debug("syncAll() done");
        }
    }

    public List<Option> getEmployees() {
        return employees;
    }
}
