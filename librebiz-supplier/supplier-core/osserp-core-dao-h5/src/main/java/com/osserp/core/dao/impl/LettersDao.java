/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 10, 2008 7:09:43 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.osserp.common.Status;
import com.osserp.common.dao.Tables;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.Letters;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterTemplate;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.letters.LetterImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LettersDao extends AbstractLetterContentsDao implements Letters {
    private static Logger log = LoggerFactory.getLogger(LettersDao.class.getName());

    protected LettersDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            String lettersTable,
            String lettersSequence) {
        super(jdbcTemplate, tables, sessionFactory, lettersTable, lettersSequence);
    }

    public Letter create(
            Status initialStatus,
            Employee user,
            LetterType type,
            ClassifiedContact recipient,
            String name,
            Long businessId,
            Long branchId,
            LetterTemplate template) {
        Letter letter = new LetterImpl(
                getNextLong(getLettersSequence()),
                initialStatus,
                user,
                type,
                name,
                recipient,
                businessId,
                branchId,
                template);
        save(letter);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + letter.getId()
                    + ", type=" + type.getId()
                    + ", recipient=" + recipient.getId() + "]");
        }
        return letter;
    }

    public List<Letter> find(Long typeId, Long recipientId, Long businessId) {
        if (typeId == null || recipientId == null) {
            throw new IllegalStateException("typeId and recipientId must not be empty");
        }
        StringBuilder hql = new StringBuilder(256);
        hql
                .append("from ")
                .append(LetterImpl.class.getName())
                .append(" o where o.type.id = :typeId and o.reference = :recipientId");

        if (businessId != null) {
            hql.append(" and o.businessId = :businessId");
        }
        hql.append(" order by o.id desc");

        Query query = getCurrentSession().createQuery(hql.toString())
                .setParameter("typeId", typeId)
                .setParameter("recipientId", recipientId);
        if (businessId != null) {
            query = query.setParameter("businessId", businessId);
        }
        return query.list();
    }

    public int count(Long typeId, Long recipientId, Long businessId) {
        if (typeId == null || recipientId == null) {
            throw new IllegalStateException("typeId and contactId must not be empty");
        }
        StringBuilder sql = new StringBuilder(256);
        sql
                .append("SELECT count(*) FROM ")
                .append(getLettersTable())
                .append(" WHERE type_id = ? AND reference_id = ?");

        if (businessId != null) {
            sql.append(" AND business_id = ?");
        }

        Object[] params = (businessId == null
                ? new Object[] { typeId, recipientId }
                : new Object[] { typeId, recipientId, businessId });
        int[] types = (businessId == null
                ? new int[] { Types.BIGINT, Types.BIGINT }
                : new int[] { Types.BIGINT, Types.BIGINT, Types.BIGINT });
        return jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
    }

    public void updateReferences(Long recipient, Long currentType, Long currentBusinessId, Long type, Long businessId) {
        StringBuilder sql = new StringBuilder(256);
        sql
                .append("UPDATE ")
                .append(getLettersTable())
                .append(" SET type_id = ?, business_id = ? WHERE type_id = ? AND reference_id = ? AND business_id = ?");

        Object[] params = new Object[] { type, businessId, currentType, recipient, currentBusinessId };
        int[] types = new int[] { Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT };
        int count = jdbcTemplate.update(sql.toString(), params, types);
        if (log.isDebugEnabled()) {
            log.debug("updateReferences() done [count=" + count + "]");
        }
    }

    @Override
    protected Class<? extends LetterContent> getPersistentClass() {
        return LetterImpl.class;
    }
}
