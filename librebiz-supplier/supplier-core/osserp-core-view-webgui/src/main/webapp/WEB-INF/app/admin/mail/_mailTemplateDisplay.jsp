<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.name}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name"><fmt:message key="name" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.name}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="originator"><fmt:message key="originator" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <span <c:if test="${view.bean.fixedOriginator}"> class="unchangeable" title="<fmt:message key="unchangeable"/>"</c:if>>
                            <c:choose>
                                <c:when test="${view.bean.sendAsUser}">
                                    <fmt:message key="sendAsUserLabel" />
                                </c:when>
                                <c:when test="${empty view.bean.originator}">
                                    <span class="error"> <fmt:message key="notDefined" />
                                    </span>
                                </c:when>
                                <c:otherwise>
                                    <o:out value="${view.bean.originator}" />
                                </c:otherwise>
                            </c:choose>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recipientsBCC"><fmt:message key="archiveAddress" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${!empty view.bean.recipientsBCC}">
                                <span <c:if test="${view.bean.fixedRecipientsBCC}"> class="unchangeable" title="<fmt:message key="unchangeable"/>"</c:if>>
                                    <o:out value="${view.bean.recipientsBCC}" />
                                </span>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="notDefined" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
            <c:if test="${view.bean.headless && !view.bean.recipientsByAction}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipients"><fmt:message key="recipient" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${!empty view.bean.recipients}">
                                    <span <c:if test="${view.bean.fixedRecipients}"> class="unchangeable" title="<fmt:message key="unchangeable"/>"</c:if>>
                                        <o:out value="${view.bean.recipients}" />
                                    </span>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="notDefined" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <c:if test="${!empty view.bean.recipientsCC}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="recipientsCC"><fmt:message key="recipientCC" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <span <c:if test="${view.bean.fixedRecipientsCC}"> class="unchangeable" title="<fmt:message key="unchangeable"/>"</c:if>>
                                    <o:out value="${view.bean.recipientsCC}" />
                                </span>
                            </div>
                        </div>
                    </div>
                </c:if>
            </c:if>
            <c:if test="${!view.bean.headless && view.bean.recipientsByUser && !empty view.bean.recipientsCC}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipientsCC"><fmt:message key="recipientCC" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <span <c:if test="${view.bean.fixedRecipientsCC}"> class="unchangeable" title="<fmt:message key="unchangeable"/>"</c:if>>
                                <o:out value="${view.bean.recipientsCC}" />
                            </span>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="subject"><fmt:message key="subject" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <span <c:if test="${view.bean.fixedText}"> class="unchangeable" title="<fmt:message key="unchangeable"/>"</c:if>>
                            <o:out value="${view.bean.subject}" />
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="personalizedHeader"><fmt:message key="personalizedHeader" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.personalizedHeader}" mode="yesnocap" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="personalizedFooter"><fmt:message key="personalizedFooter" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${!view.bean.personalizedFooter}">
                                <fmt:message key="bigNo" />
                            </c:when>
                            <c:when test="${view.bean.addUser || view.bean.addGroup}">
                                <c:choose>
                                    <c:when test="${view.bean.addUser && view.bean.addGroup &&
                                                    !empty view.bean.username && !empty view.bean.groupname}">
                                        <o:out value="${view.bean.username}" /> / <o:out value="${view.bean.groupname}" />
                                    </c:when>
                                    <c:when test="${view.bean.addUser && view.bean.addGroup && !empty view.bean.username}">
                                        <o:out value="${view.bean.username}" /> / <fmt:message key="department" />
                                    </c:when>
                                    <c:when test="${view.bean.addUser && view.bean.addGroup && !empty view.bean.groupname}">
                                        <fmt:message key="employeeName" /> / <o:out value="${view.bean.groupname}" />
                                    </c:when>
                                    <c:when test="${view.bean.addUser && view.bean.addGroup}">
                                        <fmt:message key="employeeAndDepartmentLabel" />
                                    </c:when>
                                    <c:when test="${view.bean.addUser && !empty view.bean.username}">
                                        <o:out value="${view.bean.username}" />
                                    </c:when>
                                    <c:when test="${view.bean.addUser}">
                                        <fmt:message key="employeeName" />
                                    </c:when>
                                    <c:when test="${view.bean.addGroup && !empty view.bean.groupname}">
                                        <o:out value="${view.bean.groupname}" />
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="department" />
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="templateFooterEnabledDefault" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${!view.bean.customText && !empty view.templateText}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="templateTextDownload"><fmt:message key="templateText" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:doc obj="${view.templateText}">
                                <fmt:message key="downloadLabel" />
                            </o:doc>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="templateTextUpdate"></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:link url="/admin/dms/stylesheetDms/forward?template=${view.bean.template}&exit=/admin/mail/mailTemplateConfig/reload">
                                <fmt:message key="update" />
                            </v:link>
                        </div>
                    </div>
                </div>
            </c:if>
            <c:choose>
                <c:when test="${!view.bean.customText && empty view.templateText}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="templateTextWarning"><fmt:message key="templateText" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <span class="error"> <fmt:message key="templateNotInstalledHint" />
                                </span>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <c:if test="${!empty view.availableRecipients}">
                        <c:if test="${!empty view.previewSentTo}">
                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="testMailSentLabel"><fmt:message key="testMailSent" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <span class="boldtext"><o:out value="${view.previewSentTo}" /></span>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <div class="row next">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="testMailToLabel"><fmt:message key="testMailToLabel" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="selectAction" size="1" onChange="gotoUrl(this.value);" class="form-control" >
                                        <option value="#" selected="selected"><fmt:message key="selection" /></option>
                                        <c:forEach var="obj" items="${view.availableRecipients}">
                                            <option value="<v:url value="${view.baseLink}/forwardTestmail?recipient=${obj.id}"/>">
                                                <o:out value="${obj.email}" />
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
