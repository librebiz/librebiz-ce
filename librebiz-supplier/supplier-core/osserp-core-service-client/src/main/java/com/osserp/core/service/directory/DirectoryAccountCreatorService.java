/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2014 
 * 
 */
package com.osserp.core.service.directory;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.User;
import com.osserp.common.directory.DirectoryGroup;
import com.osserp.common.directory.DirectoryManager;
import com.osserp.common.directory.DirectoryUser;
import com.osserp.common.service.Locator;
import com.osserp.common.util.NameUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.dao.Users;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeRole;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.users.AccountCreatorService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DirectoryAccountCreatorService extends AbstractDirectoryService implements AccountCreatorService {
    private static Logger log = LoggerFactory.getLogger(DomainDirectoryManagerImpl.class.getName());
    private Users users;

    /**
     * Creates a new DirectoryAccountCreatorService
     * @param systemConfigManager
     * @param directoryManager
     * @param locator
     * @param users
     */
    public DirectoryAccountCreatorService(
            SystemConfigManager systemConfigManager,
            DirectoryManager directoryManager,
            Locator locator,
            Users users) {
        super(systemConfigManager, directoryManager, locator);
        this.users = users;
    }
    
    /* (non-Javadoc)
     * @see com.osserp.core.users.AccountCreatorService#createRequired(com.osserp.core.employees.Employee)
     */
    public boolean createRequired(Employee employee) {
        User dbUser = users.find(employee);
        if (dbUser == null) {
            if (log.isDebugEnabled()) {
                log.debug("createRequired() did not find database entry [employee=" + employee.getId() + ", result=true]");
            }
            return true;
            
        } else if (log.isDebugEnabled()) {
            log.debug("createRequired() found database user [employee=" + employee.getId() + "]");
        }
        DirectoryUser existing = null;
        if (isSet(dbUser.getLdapUid())) {
            existing = getDirectoryManager().findUser(dbUser.getLdapUid(), getDirectoryClientName());
            if (existing != null) {
                if (log.isDebugEnabled()) {
                    log.debug("createRequired() directory entry exists [employee=" + employee.getId() 
                        + ", uid=" + dbUser.getLdapUid() + ", result=false]");
                }
                return false;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("createRequired() directory entry not exists [employee=" + employee.getId() 
                + ", uid=" + dbUser.getLdapUid() + ", result=true]");
        }
        return true;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.users.AccountCreatorService#createWillFail(com.osserp.core.employees.Employee)
     */
    public String createWillFail(Employee employee) {
        if (employee.getEmail() == null) {
            return ErrorCode.EMAIL_MISSING;
        }
        if (employee.getRoleConfigs().isEmpty()) {
            return ErrorCode.BRANCH_ASSIGNMENT;
        }
        if (employee.getEmployeeType() == null) {
            return ErrorCode.EMPLOYEE_TYPE_MISSING;
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.users.AccountCreatorService#createUidSuggestion(com.osserp.core.employees.Employee)
     */
    public String createUidSuggestion(Employee employee) {
        String uid = null;
        int fnameLength = 1;
        do {
            uid = StringUtil.createLoginUid(employee.getFirstName(), fnameLength, employee.getLastName(), employee.getLastName().length());
            fnameLength++;
        } while (uidAlreadyExists(uid, employee));
        return uid;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.users.AccountCreatorService#create(com.osserp.core.employees.Employee, com.osserp.core.employees.Employee, java.lang.String, java.lang.String)
     */
    public User create(Employee creator, Employee employee, String uid, String password) throws ClientException {
        assert creator != null;
        String clientName = getDirectoryClientName();
        log.debug("create() invoked [user=" + creator.getId()
                + ", employee=" + employee.getId()
                + ", uid=" + uid
                + ", client=" + clientName
                // do not write passwords to log!
                + ", passwordProvided=" + (password != null) + "]");
        if (getDirectoryManager().findUser(uid, clientName) != null) {
            throw new ClientException(ErrorCode.USER_LOGINNAME_EXISTS);
        }
        if (employee.getEmail() == null) {
            throw new ClientException(ErrorCode.EMAIL_MISSING);
        }
        if (employee.getRoleConfigs().isEmpty()) {
            throw new ClientException(ErrorCode.BRANCH_ASSIGNMENT);
        }
        DirectoryMapper employeeMapper = getMapper(DirectoryMapper.EMPLOYEE_MAPPER);
        Map<String, Object> values = employeeMapper.map(
                getDirectoryManager().createEmployeeUserAttributes(clientName), employee, null);
        values.put("uid", uid);
        values.put("uidnumber", employee.getId().toString());
        values.put("password", password);
        values.put("gecos", NameUtil.createAccountName("em",
                employee.getFirstName(), employee.getLastName(), employee.getId()));
        DirectoryUser dUser = getDirectoryManager().createUser(values, clientName);
        User dbUser = users.find(employee);
        dbUser.setLdapUid(uid);
        if (isSet(dUser.getId()) && !dUser.getId().equals(dbUser.getId())) {
            dbUser.setLdapNumber(dUser.getId());
        }
        dbUser.setActive(true);
        users.save(dbUser);
        addUsersGroups(dUser, employee);
        try {
            getDirectoryManager().activateUser(uid, clientName);
        } catch (Exception e) {
            log.warn("create() failed on attempt to activate directoryUser" 
                    + " account after successful setting up user [id="
                    + dbUser.getId() + ", message=" + e.getMessage() + "]", e);
        }
        return dbUser;
    }

    private boolean uidAlreadyExists(String uid, Employee employee) {
        User user = users.find(uid);
        if (user != null && !user.getId().equals(employee.getId())) {
            return true;
        }
        return false;
    }

    private void addUsersGroups(DirectoryUser user, Employee employee) {
        List<DirectoryGroup> dGroups = getDirectoryManager().findGroups(getDirectoryClientName());
        for (int i = 0, j = employee.getRoleConfigs().size(); i < j; i++) {
            EmployeeRoleConfig cfg = employee.getRoleConfigs().get(i);
            for (int k = 0, l = cfg.getRoles().size(); k < l; k++) {
                EmployeeRole role = cfg.getRoles().get(k);
                DirectoryGroup group = fetchGroup(dGroups, role.getGroup().getId());
                if (group != null) {
                    getDirectoryManager().addGroupMember(group, user);
                    if (log.isDebugEnabled()) {
                        log.debug("addUsersGroups() added user to group [id=" + group.getEmployeeGroupId() + "]");
                    }
                } else if (log.isDebugEnabled()) {
                    log.debug("addUsersGroups() group to add does not exist [id=" + role.getGroup().getId() + "]");
                }
            }
        }
    }

    private DirectoryGroup fetchGroup(List<DirectoryGroup> directoryGroups, Long employeeGroupId) {
        for (int i = 0, j = directoryGroups.size(); i < j; i++) {
            DirectoryGroup group = directoryGroups.get(i);
            if (group.getEmployeeGroupId() != null && group.getEmployeeGroupId().equals(employeeGroupId)) {
                return group;
            }
        }
        return null;
    }
}
