/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2005 
 * 
 */
package com.osserp.core.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.util.Quantities;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.Item;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.Record;
import com.osserp.core.products.Product;
import com.osserp.core.products.SerialNumber;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ItemImpl extends AbstractEntity implements Item {
    private static Logger log = LoggerFactory.getLogger(ItemImpl.class.getName());

    private Product product = null;
    private Double quantity = Constants.DOUBLE_NULL;
    private BigDecimal price = new BigDecimal(Constants.DOUBLE_NULL);
    private Date priceDate = new Date(System.currentTimeMillis());
    private boolean priceOverridden = false;
    private BigDecimal partnerPrice = new BigDecimal(Constants.DOUBLE_NULL);
    private boolean partnerPriceEditable = false;
    private boolean partnerPriceOverridden = false;
    private BigDecimal purchasePrice = new BigDecimal(Constants.DOUBLE_NULL);
    private Double taxRate = Constants.DOUBLE_NULL;
    private String customName = null;
    private String note = null;
    private boolean includePrice = true;
    private List<SerialNumber> serials = new ArrayList<SerialNumber>();
    private Double delivered = Constants.DOUBLE_NULL;
    private Long stockId = null;
    private boolean unchecked = false;
    private boolean partlistAvailable = false;
    private Long accountingReferenceId = null;
    private Date accountingReferenceDate = null;
    private int orderId = 0;
    private Long externalId = null;
    private Date taxPoint;
    private String timeOfSupply;
    private boolean ignoreInDocument = false;

    // OrderItem properties
    private Date delivery = null;
    private boolean deliveryConfirmed = false;
    private Date finalDelivery = null;
    String deliveryNote = null;
    private Long deliveryDateBy = null;

    private List<String> filterWords = new ArrayList<String>();

    protected ItemImpl() {
        super();
        filterWords.add("[EOL]");
        filterWords.add("(EOL)");
    }

    /**
     * Creates a new Item . Use this constructor, if referenced Object is a record (e.g. Invoice).
     * @param reference of the item
     * @param stockId of stock item is taken from
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param priceDate
     * @param partnerPrice
     * @param partnerPriceEditable
     * @param partnerPriceOverridden
     * @param purchasePrice
     * @param taxRate
     * @param note
     * @param includePrice
     * @param orderId
     * @param externalId
     */
    public ItemImpl(
            Record reference,
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            boolean includePrice,
            int orderId,
            Long externalId) {

        this(
                null,
                reference.getId(),
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate != null ? taxRate : reference.getAmounts() == null ? 0d :
                    (product != null && product.isReducedTax() ?
                            reference.getAmounts().getReducedTaxRate() :
                                reference.getAmounts().getTaxRate()),
                note,
                includePrice,
                orderId,
                externalId);
    }

    /**
     * Creates a new Item Use this constructor, if referenced Object is NOT a record (e.g. Calculation).
     * @param reference id of the item
     * @param stockId of stock item is taken from
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param priceDate
     * @param partnerPrice
     * @param partnerPriceEditable
     * @param partnerPriceOverridden
     * @param purchasePrice
     * @param taxRate
     * @param note
     * @param includePrice
     * @param orderId
     * @param externalId
     */
    public ItemImpl(
            Long reference,
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            boolean includePrice,
            int orderId,
            Long externalId) {

        this(
                null,
                reference,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                orderId,
                externalId);
    }

    /**
     * Creates a simple item. This constructor is used to create value objects.
     * @param product
     * @param quantity
     * @param price
     */
    public ItemImpl(Product product, Double quantity, Double price) {
        this(product, quantity, (price != null ? new BigDecimal(price.doubleValue()) : new BigDecimal(0)));
    }

    /**
     * Creates a simple item. This constructor is used to create value objects.
     * @param product
     * @param quantity
     * @param price
     */
    public ItemImpl(Product product, Double quantity, BigDecimal price) {
        super();
        if (product == null) {
            throw new IllegalArgumentException("product must not be null");
        }
        this.product = product;
        this.quantity = quantity != null ? quantity : 0d;
        this.price = price != null ? price : new BigDecimal(0);
    }

    /**
     * Creates a new Item
     * @param id of the item
     * @param reference of the item
     * @param stockId of stock item is taken from
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param priceDate
     * @param partnerPrice
     * @param purchasePrice
     * @param partnerPriceEditable
     * @param partnerPriceOverridden
     * @param taxRate
     * @param note
     * @param includePrice
     * @param orderId
     * @param externalId
     */
    protected ItemImpl(//
            Long id,
            Long reference,
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            boolean includePrice,
            int orderId,
            Long externalId) {

        super(id, reference);
        if (product == null) {
            throw new IllegalArgumentException("product must not be null");
        }
        this.product = product;
        this.customName = customName;
        this.quantity = quantity != null ? quantity : 0d;
        this.price = price != null ? price : new BigDecimal(0);
        this.taxRate = taxRate != null ? taxRate : 0d;
        this.note = note;
        this.includePrice = includePrice;
        if (stockId != null) {
            this.stockId = stockId;
            this.product.enableStock(stockId);
        } else {
            this.stockId = product.getCurrentStock() != null ? product.getCurrentStock() : product.getDefaultStock();
            this.product.enableStock(stockId);
        }
        this.partnerPriceEditable = partnerPriceEditable;
        this.partnerPriceOverridden = partnerPriceOverridden;
        this.orderId = orderId;
        this.externalId = externalId;
        setPartnerAndPurchasePrice(priceDate, partnerPrice, purchasePrice);
    }

    public void setPartlistAvailable(boolean partlistAvailable) {
        this.partlistAvailable = partlistAvailable;
    }

    public boolean isPartlistAvailable() {
        return partlistAvailable;
    }

    public Product getProduct() {
        return product;
    }

    protected void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return isSet(customName) ? customName : (product != null ? product.getName() : "unknown");
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(Date priceDate) {
        this.priceDate = priceDate;
    }

    public boolean isPriceOverridden() {
        return priceOverridden;
    }

    public void setPriceOverridden(boolean priceOverridden) {
        this.priceOverridden = priceOverridden;
    }

    public final BigDecimal getPartnerPrice() {
        return partnerPrice;
    }

    public void setPartnerPrice(BigDecimal partnerPrice) {
        this.partnerPrice = partnerPrice;
    }

    public boolean isPartnerPriceEditable() {
        return partnerPriceEditable;
    }

    public void setPartnerPriceEditable(boolean partnerPriceEditable) {
        this.partnerPriceEditable = partnerPriceEditable;
    }

    public boolean isPartnerPriceOverridden() {
        return partnerPriceOverridden;
    }

    public void setPartnerPriceOverridden(boolean partnerPriceOverridden) {
        this.partnerPriceOverridden = partnerPriceOverridden;
    }

    public void setCalculatedPartnerPrice(BigDecimal calculatedPartnerPrice) {
        this.partnerPrice = (calculatedPartnerPrice == null ? new BigDecimal(0) : calculatedPartnerPrice);
    }

    public void setManualPartnerPrice(BigDecimal manualPartnerPrice) {
        this.partnerPrice = (manualPartnerPrice == null ? new BigDecimal(0) : manualPartnerPrice);
        this.partnerPriceOverridden = true;
    }

    public BigDecimal getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getAmount() {
        if (price != null && quantity != null) {
            BigDecimal result = price.multiply(new BigDecimal(quantity.toString()));
            /*
             * if (log.isDebugEnabled()) { log.debug("getAmount() item " + getId() + " reports price " + price.toPlainString() + " quantity " +
             * this.quantity.toString() + " amount " + result.toPlainString()); }
             */
            return result;
        } else if (price != null) {
            return price;
        } else {
            return null;
        }
    }

    public BigDecimal getPartnerAmount() {
        if (partnerPrice != null && quantity != null) {
            BigDecimal result = partnerPrice.multiply(new BigDecimal(quantity.toString()));
            /*
             * if (log.isDebugEnabled()) { log.debug("getPartnerAmount() item " + getId() + " reports price " + price.toPlainString() + " quantity " +
             * this.quantity.toString() + " amount " + result.toPlainString()); }
             */
            return result;
        } else if (price != null) {
            return price;
        } else {
            return null;
        }
    }

    public void replaceProduct(
            Product product,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            BigDecimal purchasePrice,
            String note) {
        this.product = product;
        this.quantity = quantity;
        this.price = price;
        this.note = note;
        this.partnerPriceEditable = partnerPriceEditable;
        setPartnerAndPurchasePrice(priceDate, partnerPrice, purchasePrice);
    }

    public boolean isIgnoreInDocument() {
        return ignoreInDocument;
    }

    public void setIgnoreInDocument(boolean ignoreInDocument) {
        this.ignoreInDocument = ignoreInDocument;
    }

    public boolean isIncludePrice() {
        return includePrice;
    }

    public void setIncludePrice(boolean includePrice) {
        this.includePrice = includePrice;
    }

    public boolean isSerialComplete() {
        if (serials.size() < quantity
                || (quantity < 0 && serials.size() < quantity * (-1))) {
            return false;
        }
        return true;
    }

    public String getSerialsDisplay() {
        if (serials.isEmpty()) {
            return "";
        }
        StringBuilder sbf = new StringBuilder();
        for (int i = 0, j = serials.size(); i < j; i++) {
            sbf.append(serials.get(i).getSerial());
            if (i < j - 1) {
                sbf.append(", ");
            }
        }
        return sbf.toString();
    }

    public List<SerialNumber> getSerials() {
        return serials;
    }

    protected void setSerials(List<SerialNumber> serials) {
        if (serials != null) {
            this.serials = serials;
        }
    }

    public Double getDelivered() {
        return delivered;
    }

    public void setDelivered(Double delivered) {
        this.delivered = delivered;
    }

    public final Double getOutstanding() {
        return (Math.abs(quantity == null ? 0 : quantity)
                - Math.abs(delivered == null ? 0 : delivered));
    }

    public final Double getOutOfStockCount() {
        Double outstanding = getOutstanding();
        if (getProduct() == null || !getProduct().isDeliveryNoteAffecting() ||
                getProduct().getSummary() == null ||
                getProduct().getSummary().getAvailableStock() == null ||
                outstanding == 0) {
            return 0d;
        }
        Double currentStock = getProduct().getSummary().getAvailableStock();
        if (currentStock < outstanding) {
            return Math.abs(outstanding) - Math.abs(currentStock);
        }
        return 0d;
    }

    public boolean isProvidingDeliveryDate() {
        return false;
    }

    public final boolean isDeliveryNoteAffecting() {
        if (getProduct() == null) {
            log.warn("isDeliveryNoteAffecting() possible incorrect result: "
                    + " Referenced product is null [item=" + getId() + "]");
            return false;
        }
        return getProduct().isDeliveryNoteAffecting();
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public boolean isUnchecked() {
        return unchecked;
    }

    public void setUnchecked(boolean unchecked) {
        this.unchecked = unchecked;
    }

    public Long getAccountingReferenceId() {
        return accountingReferenceId;
    }

    protected void setAccountingReferenceId(Long accountingReferenceId) {
        this.accountingReferenceId = accountingReferenceId;
    }

    public Date getAccountingReferenceDate() {
        return accountingReferenceDate;
    }

    protected void setAccountingReferenceDate(Date accountingReferenceDate) {
        this.accountingReferenceDate = accountingReferenceDate;
    }

    public void updateAccountingReference(Long id, Date date) {
        accountingReferenceId = id;
        accountingReferenceDate = date;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    public Date getTaxPoint() {
        return taxPoint;
    }

    public void setTaxPoint(Date taxPoint) {
        this.taxPoint = taxPoint;
    }

    public String getTimeOfSupply() {
        return timeOfSupply;
    }

    public void setTimeOfSupply(String timeOfSupply) {
        this.timeOfSupply = timeOfSupply;
    }

    public Date getDelivery() {
        return delivery;
    }

    public void setDelivery(Date delivery) {
        this.delivery = delivery;
    }

    public Long getDeliveryDateBy() {
        return deliveryDateBy;
    }

    public void setDeliveryDateBy(Long deliveryDateBy) {
        this.deliveryDateBy = deliveryDateBy;
    }

    public Date getFinalDelivery() {
        return finalDelivery;
    }

    public void setFinalDelivery(Date finalDelivery) {
        this.finalDelivery = finalDelivery;
    }

    public String getDeliveryNote() {
        return deliveryNote;
    }

    public void setDeliveryNote(String deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    public boolean isDeliveryConfirmed() {
        return deliveryConfirmed;
    }

    public void setDeliveryConfirmed(boolean deliveryConfirmed) {
        this.deliveryConfirmed = deliveryConfirmed;
    }

    public Element getXML(boolean formatValues) {
        if (formatValues) {
            return getFormattedXML();
        }
        return getXML();
    }

    public Element getFormattedXML() {
        Element it = new Element("item");
        Long qtyUnit = isNotSet(product.getQuantityUnit()) ? Quantities.PCS : product.getQuantityUnit();
        it.addContent(new Element("quantity").setText(Quantities.format(quantity, qtyUnit)));
        it.addContent(new Element("quantityUnit").setText(qtyUnit.toString()));
        it.addContent(new Element("productId").setText(product.getProductId().toString()));
        it.addContent(new Element("ignoreInDocument").setText(Boolean.toString(ignoreInDocument)));

        String name = getProductName();
        for (Iterator<String> i = filterWords.iterator(); i.hasNext();) {
            String word = i.next();
            if (name.indexOf(word) > -1) {
                name = name.substring(0, name.indexOf(word));
            }
        }

        it.addContent(new Element("name").setText(name));
        it.addContent(createDescription());

        if (product.getPurchaseText() != null) {
            it.addContent(new Element("purchaseText").setText(product.getPurchaseText()));
        } else {
            it.addContent(new Element("purchaseText"));
        }
        if (!includePrice) {
            it.addContent(new Element("price"));
            it.addContent(new Element("amount"));
        } else {
            it.addContent(new Element("price").setText(
                    NumberFormatter.getValue(price, NumberFormatter.CURRENCY)));
            it.addContent(new Element("amount").setText(
                    NumberFormatter.getValue(getAmount(), NumberFormatter.CURRENCY)));
        }
        if (note != null) {
            it.addContent(new Element("note").setText(note));
        } else {
            it.addContent(new Element("note"));
        }
        if (stockId != null && stockId != 0) {
            it.addContent(new Element("stockId").setText(stockId.toString()));
        } else {
            it.addContent(new Element("stockId"));
        }
        if (externalId != null && externalId != 0) {
            it.addContent(new Element("externalId").setText(externalId.toString()));
        } else {
            it.addContent(new Element("externalId"));
        }
        addSerialsXML(it);
        return it;
    }

    @Override
    public Element getXML() {
        Element it = new Element("item");
        it.addContent(new Element("quantity").setText(
                Quantities.format(quantity, product.getQuantityUnit())));
        if (product.getQuantityUnit() == null) {
            it.addContent(new Element("quantityUnit").setText(
                    Quantities.PCS.toString()));
        } else {
            it.addContent(new Element("quantityUnit").setText(
                    product.getQuantityUnit().toString()));
        }
        it.addContent(new Element("productId").setText(
                product.getProductId().toString()));
        it.addContent(new Element("ignoreInDocument").setText(
                Boolean.toString(ignoreInDocument)));
        it.addContent(new Element("name").setText(getProductName()));
        it.addContent(new Element("plant").setText(
                Boolean.toString(product.isPlant())));
        it.addContent(createDescription());
        if (product.getPurchaseText() != null) {
            it.addContent(new Element("purchaseText").setText(
                    product.getPurchaseText()));
        } else {
            it.addContent(new Element("purchaseText"));
        }
        if (product.getGroup() != null) {
            it.addContent(new Element("group").setText(
                    product.getGroup().getId().toString()));
        } else {
            it.addContent(new Element("group"));
        }
        if (product.getCategory() != null) {
            it.addContent(new Element("category").setText(
                    product.getCategory().getId().toString()));
        } else {
            it.addContent(new Element("category"));
        }
        if (!includePrice) {
            it.addContent(new Element("price"));
            it.addContent(new Element("amount"));
            it.addContent(new Element("taxRate"));
        } else {
            it.addContent(new Element("price").setText(
                    NumberFormatter.getValue(price, NumberFormatter.CURRENCY)));
            it.addContent(new Element("amount").setText(
                    NumberFormatter.getValue(getAmount(), NumberFormatter.CURRENCY)));
            it.addContent(new Element("taxRate").setText(
                    NumberFormatter.getValue(getTaxRate(), NumberFormatter.TAX_RATE)));
        }
        if (note != null) {
            it.addContent(createNote());
        } else {
            it.addContent(new Element("note"));
        }
        if (stockId != null && stockId != 0) {
            it.addContent(new Element("stockId").setText(stockId.toString()));
        } else {
            it.addContent(new Element("stockId"));
        }
        if (externalId != null && externalId != 0) {
            it.addContent(new Element("externalId").setText(externalId.toString()));
        } else {
            it.addContent(new Element("externalId"));
        }
        addSerialsXML(it);
        return it;
    }

    private void addSerialsXML(Element item) {
        if (!product.isSerialAvailable() || serials.isEmpty()) {
            item.addContent(new Element("serialAvailable").setText("false"));
            item.addContent(new Element("serials"));
        } else {
            item.addContent(new Element("serialAvailable").setText("true"));
            item.addContent(new Element("serials").setText(getSerialsDisplay()));
        }
    }

    private Element createDescription() {
        return JDOMUtil.createListByText("description", "line", product.getDescription());
    }

    private Element createNote() {
        return JDOMUtil.createListByText("note", "line", note);
    }

    @Override
    public Object clone() {
        return new ItemImpl(this);
    }

    protected ItemImpl(Item item) {
        this();
        setId(item.getId());
        setOrderId(item.getOrderId());
        setReference(item.getReference());
        setStockId(item.getStockId());
        setProduct(item.getProduct());
        setQuantity(item.getQuantity());
        setTaxRate(item.getTaxRate());
        setPrice(item.getPrice());
        setPriceDate(item.getPriceDate());
        setPriceOverridden(item.isPartnerPriceOverridden());
        setPartnerPrice(item.getPartnerPrice());
        setPartnerPriceEditable(item.isPartnerPriceEditable());
        setPartnerPriceOverridden(item.isPartnerPriceOverridden());
        setPurchasePrice(item.getPurchasePrice());
        setTaxRate(item.getTaxRate());
        setCustomName(item.getCustomName());
        setNote(item.getNote());
        setIncludePrice(item.isIncludePrice());
        setAccountingReferenceId(item.getAccountingReferenceId());
        setDelivered(item.getDelivered());
        setAccountingReferenceDate(item.getAccountingReferenceDate());
        setExternalId(item.getExternalId());
        if (item instanceof OrderItem) {
            OrderItem aoi = (OrderItem) item;
            setDelivery(aoi.getDelivery());
            setDeliveryConfirmed(aoi.isDeliveryConfirmed());
            setFinalDelivery(aoi.getFinalDelivery());
            setDeliveryDateBy(aoi.getDeliveryDateBy());
            setDeliveryNote(aoi.getDeliveryNote());
        }

        for (int i = 0, j = item.getSerials().size(); i < j; i++) {
            SerialNumber serialnumber = item.getSerials().get(i);
            SerialNumber clone = (SerialNumber) serialnumber.clone();
            serials.add(clone);
        }
    }

    protected final void setPartnerAndPurchasePrice(Date priceDate, BigDecimal partnerPrice, BigDecimal purchasePrice) {
        if (isSet(partnerPrice)) {
            this.partnerPrice = NumberUtil.round(partnerPrice, 2);
        } else {
            if (partnerPriceOverridden) {
                this.partnerPrice = new BigDecimal(0);
            } else {
                this.partnerPrice = NumberUtil.round(createPartnerPrice(), 2);
            }
        }
        if (isSet(purchasePrice)) {
            this.purchasePrice = NumberUtil.round(purchasePrice, 2);
        } else {
            this.purchasePrice = NumberUtil.round(createLastPurchasePrice(), 2);
        }
        if (priceDate != null) {
            this.priceDate = priceDate;
        } else {
            this.priceDate = new Date(System.currentTimeMillis());
        }
    }

    private BigDecimal createPartnerPrice() {
        if (product == null || product.getPartnerPrice() == null) {
            return new BigDecimal(0);
        }
        return NumberUtil.round(new BigDecimal(product.getPartnerPrice()), 5);
    }

    private BigDecimal createLastPurchasePrice() {
        if (product == null || product.getSummary() == null || product.getSummary().getLastPurchasePrice() == null) {
            return new BigDecimal(0);
        }
        return NumberUtil.round(new BigDecimal(product.getSummary().getLastPurchasePrice()), 5);
    }
}
