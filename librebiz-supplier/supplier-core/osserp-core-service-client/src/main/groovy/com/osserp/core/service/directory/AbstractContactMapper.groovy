/**
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2013 08:30:24 AM
 * 
 */
package com.osserp.core.service.directory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.OptionsCache
import com.osserp.common.mail.EmailAddress
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactUtil
import com.osserp.core.contacts.Phone
import com.osserp.core.system.SystemConfigManager

/**
 * 
 * Maps common contact values to a directory map.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractContactMapper extends AbstractDirectoryMapper {
	private static Logger logger = LoggerFactory.getLogger(AbstractContactMapper.class.getName())
	private OptionsCache optionsCache
	
	protected AbstractContactMapper(SystemConfigManager systemConfigManager, OptionsCache optionsCache) {
		super(systemConfigManager)
		this.optionsCache = optionsCache
	}

	protected Map deployContactValues(Map result, Contact contact) {
		logger.debug("deployContactValues: invoked [contactId=${contact?.contactId}]")
		
		result.sn = contact.lastName
		result.givenname = contact.firstName
		result.cn = contact.name
		result.displayname = contact.displayName 
		result.title = contact.salutationDisplayShort
		result.userwww = contact.website
		
	
		if (contact.type == Contact.TYPE_PERSON) {
			result.postalcode = contact.referenceContact.address.zipcode
			result.street = contact.referenceContact.address.street
			result.l = contact.referenceContact.address.city
			result.st = fetchFederalState(contact.referenceContact)
		
		} else {
			result.postalcode = contact.address.zipcode
			result.street = contact.address.street
			result.l = contact.address.city
			result.st = fetchFederalState(contact)
		}
		def mailAssigned = false
		contact.emails.each {
			EmailAddress p = it
			if (!mailAssigned && p.isBusiness()) {
				result.mail = p.email
				mailAssigned = true
			}
		}
		if (!mailAssigned) {
			contact.emails.each {
				EmailAddress p = it
				if (!mailAssigned && p.isInternal()) {
					result.mail = p.email
					mailAssigned = true
				}
			}
		}
		if (!mailAssigned && contact.type == Contact.TYPE_PERSON) {
			result.mail = contact.referenceContact.email
		}
		if (!mailAssigned) {
			result.mail = contact.email
		}

		result.telephonenumber = ContactUtil.getBusinessPhone(contact)
		
		result.homephone = ContactUtil.getPrivatePhone(contact)

		result.mobile = contact.mobile?.formattedOutlookNumber
		if (!result.mobile && contact.type == Contact.TYPE_PERSON) {
			result.mobile = contact.referenceContact.mobile?.formattedOutlookNumber
		}
		
		result.facsimiletelephonenumber = contact.fax?.formattedOutlookNumber
		if (!result.facsimiletelephonenumber && contact.type == Contact.TYPE_PERSON) {
			result.facsimiletelephonenumber = contact.referenceContact.fax?.formattedOutlookNumber
		}
		//logger.debug("deployContactValues: done [contactId=${contact?.contactId}]\nContact values:\n${result}")
		return result
	}
	
	protected String fetchFederalState(Contact contact) {
		if (contact.address?.federalStateName) {
			return contact.address?.federalStateName
		}
		if (contact.address?.federalStateId) {
			return optionsCache.getMappedValue('federalStates', contact.address.federalStateId)
		}
		return null
	}
}
