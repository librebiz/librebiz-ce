/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 31, 2008 10:02:49 AM 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceCancellation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseInvoiceCancellationImpl extends PurchaseInvoiceImpl implements PurchaseInvoiceCancellation {

    /**
     * Default constructor required by Serializable
     */
    protected PurchaseInvoiceCancellationImpl() {
        super();
    }

    /**
     * Creates a new cancellation by cancelled invoice
     * @param user
     * @param invoice
     * @param note
     */
    public PurchaseInvoiceCancellationImpl(
            Employee user,
            PurchaseInvoice invoice,
            String note) {

        super();
        setBookingType(invoice.getBookingType());
        setCanceled(invoice.isCanceled());
        setCompany(invoice.getCompany());
        setContact(invoice.getContact());
        setCreated(new Date(System.currentTimeMillis()));
        setCreatedBy(user.getId());
        setCurrency(invoice.getCurrency());
        setId(invoice.getId());
        setInternal(invoice.isInternal());
        setItemsChangeable(invoice.isItemsChangeable());
        setItemsEditable(invoice.isItemsEditable());
        setLanguage(invoice.getLanguage());
        setMaturity(invoice.getMaturity());
        if (isSet(getNote()) && isSet(note)) {
            StringBuilder buffer = new StringBuilder(note);
            buffer.append("\n---\n").append(getNote());
        } else {
            setNote(note);
        }
        setPaymentCondition(invoice.getPaymentCondition());
        setPersonId(invoice.getPersonId());
        setReference(invoice.getReference());
        setBusinessCaseId(invoice.getBusinessCaseId());
        setSignatureLeft(invoice.getSignatureLeft());
        setSignatureRight(invoice.getSignatureRight());
        setStockAffecting(invoice.isStockAffecting());
        setSupplierReferenceNumber(invoice.getSupplierReferenceNumber());
        setTaxFree(invoice.isTaxFree());
        setTaxFreeId(invoice.getTaxFreeId());
        setType(invoice.getType());
        setAmounts(new AmountsImpl(
                invoice.getAmounts().getTaxRate(),
                invoice.getAmounts().getReducedTaxRate()));
        addItems(invoice.getItems());
        setStatus(PurchaseInvoiceCancellation.STAT_CANCELED);
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new PurchaseInvoiceCancellationItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }
}
