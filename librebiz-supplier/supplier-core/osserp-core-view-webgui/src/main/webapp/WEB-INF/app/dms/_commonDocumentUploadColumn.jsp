<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:choose>
    <c:when test="${dmsDocumentView.multipleFileUploadSupport}">
        <tr>
            <td colspan="2"><o:fileUpload multiple="${dmsDocumentView.multipleFileUploadSupport}"/></td>
            <td><v:date name="sourceDate" picker="true" title="optionalDateLabel" styleClass="form-control date inline" /></td>
            <c:choose>
                <c:when test="${dmsDocumentView.referenceSupport and (!empty dmsDocumentView.list or
                            (view.assignImportsSupport and !empty view.documentsImported))}">
                    <td colspan="3"> </td>
                </c:when>
                <c:otherwise>
                    <td colspan="2"> </td>
                </c:otherwise>
            </c:choose>
            <td><o:submitFile /></td>
        </tr>
    </c:when>
    <c:otherwise>
        <tr>
            <td colspan="2"><o:fileUpload multiple="${dmsDocumentView.multipleFileUploadSupport}"/></td>
            <td><v:date name="sourceDate" picker="true" title="optionalDateLabel" styleClass="form-control date inline" /></td>
            <td<c:if test="${dmsDocumentView.referenceSupport and (!empty dmsDocumentView.list or 
                    (view.assignImportsSupport and !empty view.documentsImported))}"> colspan="2"</c:if>>
                <v:text name="note" styleClass="form-control" placeholder="${dmsDocumentView.documentNotePlaceholder}" />
            </td>
            <td colspan="2"><o:submitFile /></td>
        </tr>
    </c:otherwise>
</c:choose>
