/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2009 5:40:15 PM 
 * 
 */
package com.osserp.common.dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.PersistentObject;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractHibernateAndSpringEntityDao extends AbstractHibernateAndSpringDao {

    /**
     * Creates a new hibernate entity data access object
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     */
    protected AbstractHibernateAndSpringEntityDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    /**
     * Loads an existing entity by primary key. Throws runtime exception if entity not exists
     * @param primaryKey
     * @return entity
     */
    protected PersistentObject loadEntity(Long primaryKey) {
        return (PersistentObject) getCurrentSession().load(getEntityClass().getName(), primaryKey);
    }

    /**
     * Loads an existing entity by primary key. Throws runtime exception if entity not exists
     * @param primaryKeys
     * @return entity
     */
    protected List<? extends PersistentObject> loadEntities(List<Long> primaryKeys) {
        StringBuilder hql = new StringBuilder(512);
        hql
                .append(createFindAllQuery(false))
                .append(" where ec.id in (")
                .append(StringUtil.createCommaSeparated(primaryKeys))
                .append(")");
        return getCurrentSession().createQuery(appendFindAllSuffix(hql)).list();
    }

    /**
     * Tries to find an entity by primary key.
     * @param primaryKey
     * @return entity or null if no such object exists
     */
    protected PersistentObject findEntity(Long primaryKey) {
        return (PersistentObject) getCurrentSession().get(getEntityClass().getName(), primaryKey);
    }

    /**
     * Finds all entities in unsorted order. Override 'appendFindAllSuffix' to append an hql order clause before executing the hql query.
     * @return all entities
     */
    protected List<? extends PersistentObject> findAll() {
        return getCurrentSession().createQuery(createFindAllQuery(true)).list();
    }

    private final String createFindAllQuery(boolean appendSuffix) {
        StringBuilder hql = new StringBuilder();
        hql.append("from ").append(getEntityClass().getName()).append(" ec");
        if (!appendSuffix) {
            return hql.toString();
        }
        return appendFindAllSuffix(hql);
    }

    /**
     * Appends a suffix on an 'from EntityClassName ec' query. Default does nothing and returns an unsorted list.
     * Override to append entity specific statements like ' order by ec.property' as appropriate.
     * @param buffer initialized with 'from EntityClassName ec'
     * @return find all query with append suffix, e.g. ' order by ec.name' for example
     */
    protected String appendFindAllSuffix(StringBuilder buffer) {
        return buffer.toString();
    }

    /**
     * Persits an entity by create or update
     * @param entity
     */
    protected void save(PersistentObject entity) {
        getCurrentSession().saveOrUpdate(getEntityClass().getName(), entity);
    }

    /**
     * Provides the entity class
     * @return entity class
     */
    protected abstract Class<? extends PersistentObject> getEntityClass();
}
