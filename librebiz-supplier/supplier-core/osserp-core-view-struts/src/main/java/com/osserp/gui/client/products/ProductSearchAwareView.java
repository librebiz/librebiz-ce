/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2009 8:58:11 PM 
 * 
 */
package com.osserp.gui.client.products;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.web.Form;
import com.osserp.common.web.SearchByMethodView;

import com.osserp.core.products.Product;
import com.osserp.core.products.ProductClassificationEntity;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;

import com.osserp.gui.client.LazyIgnoringView;

/**
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 */
public interface ProductSearchAwareView extends LazyIgnoringView, SearchByMethodView, ProductClassificationAwareView {

    /**
     * Provides employees product filter.
     * @return employeeProductFilter
     */
    public ProductSelection getEmployeeProductFilter();

    /**
     * Sets employees product filter.
     * @param productFilter
     */
    public void setEmployeeProductFilter(ProductSelection productFilter);

    public boolean isProvidingSerialNumberSearch();

    public boolean isNoClassification();

    /**
     * Indicates if view should be kept active after adding products
     * (e.g. leave view via exit only)
     * @return true to keep
     */
    public boolean isKeepViewAfterAdd();

    /**
     * Change status of keepViewAfterAdd property
     */
    public void toggleKeepViewAfterAdd();

    /**
     * Indicates that search should include all products ignoring classification
     * @return showAllProducts
     */
    public boolean isShowAllProducts();

    /**
     * Stores the last selected search string
     * @return value
     */
    public String getValue();

    /**
     * Provides available product groups
     * @return productGroups
     */
    public List<ProductClassificationEntity> getProductGroups();

    /**
     * Provides available product categories
     * @return productCategorys
     */
    public List<ProductClassificationEntity> getProductCategorys();

    /**
     * Indicates that view should provide product selection configs
     * @return productSelectionConfigsAvailable
     */
    public boolean isProductSelectionConfigsAvailable();

    /**
     * Indicates that view is in product selection config selection mode
     * @return productSelectionConfigsMode
     */
    public boolean isProductSelectionConfigsMode();

    /**
     * Provides available product selection configs
     * @return productSelectionConfigs
     */
    public List<ProductSelectionConfig> getProductSelectionConfigs();

    /**
     * Provides current selected product selection config if noClassification-flag is not enabled
     * @return selectedProductSelectionConfig
     */
    public ProductSelectionConfig getSelectedProductSelectionConfig();

    /**
     * Selects an product selection config
     * @param id
     */
    public void selectProductSelectionConfig(Long id);

    /**
     * Provides current selected product selection config item if noClassification-flag is not enabled
     * @return selectedProductSelectionConfigItem
     */
    public ProductSelectionConfigItem getSelectedProductSelectionConfigItem();

    /**
     * Selects an product selection config item
     * @param id
     */
    public void selectProductSelectionConfigItem(Long id);

    /**
     * Updates classification values (e.g. type, group, category)
     * @param form
     */
    public void updateClassification(Form form);

    /**
     * Performs an product search
     * @param form with search values
     * @throws ClientException if no product(s) found
     */
    public void search(Form form) throws ClientException;

    /**
     * Provides current selected product if view supports direct product selection
     * @return product or null if none selected
     */
    public Product getProduct();

    /**
     * Method to select the input form default price
     * @return priceMethod
     */
    public String getProductPriceMethod();

    /**
     * Default strategy if the value retrieved using productPriceMethod
     * results in a missing price.
     * @return priceDefault
     */
    public String getProductPriceDefault();
}
