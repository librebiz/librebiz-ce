<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="paymentData"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <v:link url="/accounting/accountTransaction/selectContact">
                                <fmt:message key="issuer" />
                            </v:link>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.selectedContact.displayName}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <v:link url="/accounting/accountTransaction/selectType">
                                <fmt:message key="type" />
                            </v:link>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.selectedType.name}" />
                        </div>
                    </div>
                </div>
                
                <br />

                <c:if test="${!view.selectedType.taxPayment}">
                    <v:date name="recordDate" styleClass="form-control" label="recordDate" picker="true" />
                </c:if>
                
                <v:date name="paid" styleClass="form-control" label="paidOnLabel" picker="true" />
                
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="amount"><fmt:message key="amount" /></label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <v:number name="amount" format="currency" styleClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <span style="margin-left: 0px;">EUR</span>
                        </div>
                    </div>
                </div>
                
                <c:if test="${!view.selectedType.taxPayment}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="vat"><fmt:message key="vatName" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="taxFree"> <v:checkbox id="taxFree" /> <fmt:message key="exemptOfTax" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="taxFreeSubject" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <oc:select name="taxFreeId" value="${record.taxFreeId}" options="taxFreeDefinitions" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group"></div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="reducedTax"> <v:checkbox id="reducedTax" /> <fmt:message key="reducedTurnoverTax" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
                
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="bookingText"><fmt:message key="bookingText" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:textarea styleClass="form-control" name="note" rows="3" />
                        </div>
                    </div>
                </div>
                
                <c:choose>
                    <c:when test="${empty view.selectedTemplate}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="saveAsTemplate"><fmt:message key="templateLabel" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="partner"> <v:checkbox id="template" /> <fmt:message key="saveAsTemplate" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="templateName"><fmt:message key="templateName" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <v:text name="templateName" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="saveAsTemplate"><fmt:message key="templateLabel" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="partner"> <v:checkbox id="template" /> <fmt:message key="updateTemplate" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" value="createBooking" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
        
</div>
