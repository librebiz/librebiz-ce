<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.serialNumberQueryView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
    
.recordType {
    width: 48px;
}
    
.productNumber {
    width: 85px;    
}
    
.productName {
    width: 285px;   
}
    
.recordNumber {
    width: 85px;    
}
    
.serialNumber {
    width: 170px;   
}
    
.created {
    width: 65px;    
}

</style>
</tiles:put>
<tiles:put name="headline"><fmt:message key="serialNumbers"/></tiles:put>
<tiles:put name="headline_right">
<c:choose>
<c:when test="${view.editMode}">
<a href="<c:url value="/serialNumbers.do?method=select"/>" ><o:img name="backIcon"/></a>
</c:when>
<c:otherwise>
<a href="<c:url value="/serialNumbers.do?method=exit"/>" ><o:img name="backIcon"/></a>
</c:otherwise>
</c:choose>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string"> 
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
        <c:choose>
			<c:when test="${view.editMode}">
				<c:set var="obj" value="${view.bean}"/>
				<o:form name="searchForm" url="/serialNumbers.do">
					<input type="hidden" name="method" value="save" />
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="recordType"><fmt:message key="type"/></th>
								<th class="recordNumber"><fmt:message key="recordNumber"/></th>
								<th class="serialNumber"><fmt:message key="serialNumber"/></th>
							</tr>
						</thead>
						<tbody style="overflow:auto; height:255px;">
							<tr>
								<td class="recordType">
									<c:choose>
										<c:when test="${obj.sales}">
											<fmt:message key="recordLabelOutgoing"/>
										</c:when>
										<c:otherwise>
											<fmt:message key="recordLabelIncoming"/>
										</c:otherwise>
									</c:choose>
								</td>
								<td class="recordNumber">
									<o:out value="${obj.recordNumber}"/>
								</td>
								<td class="serialNumber">
									<input type="text" name="value" id="value" value="<o:out value="${obj.serialNumber}"/>" style="width:300px;" />
									<a href="<c:url value="/serialNumbers.do"><c:param name="method" value="select"/></c:url>" title="<fmt:message key="exitEdit"/>"><o:img name="backIcon" styleClass="bigicon"/></a>
									<input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="save" onclick="setMethod('save');" style="margin-left: 5px; margin-right: 5px;" class="bigicon" title="<fmt:message key="saveInput"/>"/>
								</td>
							</tr>
						</tbody>
					</table>
				</o:form>
			</c:when>
            <c:otherwise>
                <div class="contentBox">
                    <div class="contentBoxData">
                        <div class="subcolumns">
                            <div class="subcolumn">
                                <o:ajaxLookupForm name="serialNumberSearchForm" url="/serialNumbers.do" targetElement="ajaxContent" activityElement="value">
                                    <input type="hidden" name="method" value="find" />
                                    <table class="table" style="border: 0px;">
                                        <tr>
                                            <td style="width:130px;"><fmt:message key="serialNumber"/></td>
                                            <td style="width:220px;"><input type="text" name="value" value="" id="value" /></td>
                                            <td style="width:20px; text-align:right;"><input type="checkbox" id="startsWith" name="startsWith" /></td>
                                            <td><fmt:message key="beginningWith"/></td>
                                            <td style="width:30%;"> </td>
                                        </tr>
                                    </table>
                                </o:ajaxLookupForm>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div id="ajaxDiv">
                    <div id="ajaxContent">
                        <c:if test="${!empty view.list}"><c:import url="serial_number_list_ajax.jsp"/></c:if>
                    </div>
				</div>
			</c:otherwise>
		</c:choose>
    </div>
</div>
</tiles:put> 
</tiles:insert>
