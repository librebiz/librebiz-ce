/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 28.04.2011 11:41:35 
 * 
 */
package com.osserp.gui.admin.calculations

import com.osserp.core.calc.CalculationConfigManager
import com.osserp.groovy.web.MenuItem
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
public class CalculationConfigView extends CoreView {
    Map includedSelections = [:]

    CalculationConfigView() {
        headerName = 'partsListsConfiguration'
        enableAutoreload()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            list = calculationConfigManager.getConfigs()
            actualSortKey = 'name'
        }
    }

    @Override
    void createCustomNavigation() {
        nav.beanListNavigation= [selectExitLink, createLink, homeLink]
    }

    @Override
    MenuItem getCreateLink() {
        MenuItem link = navigationLink("/admin/calculations/calculationConfigGroup/forward", 'newdataIcon', 'addNewGroup')
        link.ajaxPopup = true
        link.ajaxPopupName = 'calculationConfigGroupView'
        link.permissions = 'executive,calc_config'
        link.permissionInfo = 'permissionEditGroups'
        return link
    }

    @Override
    void select() {
        super.select()
        updateIncludedSelections()
    }

    @Override
    void reload() {
        list = calculationConfigManager.getConfigs()
        actualSortKey = 'name'
        if (bean) {
            bean = list.find { it.id == bean.id }
            updateIncludedSelections()
        }
    }

    void deleteGroup() {
        Long id = form.getLong('id')
        if (id) {
            calculationConfigManager.removeGroup(bean, id)
            reload()
        }
    }

    void addSelection() {
        Long groupId = form.getLong('groupId')
        Long id = form.getLong('id')
        if (groupId && id) {
            bean = calculationConfigManager.addProductSelection(bean, groupId, id)
            reload()
        }
    }

    void removeSelection() {
        Long groupId = form.getLong('groupId')
        Long id = form.getLong('id')
        if (groupId && id) {
            bean = calculationConfigManager.removeProductSelection(bean, groupId, id)
            reload()
        }
    }

    protected updateIncludedSelections() {
        includedSelections = [:]
        if (bean) {
            bean.groups?.each {
                List tmp = []
                it.configs.each {
                    tmp << it.productSelectionConfig.id
                }
                includedSelections."group${it.id}" = tmp
            }
        }
    }

    private CalculationConfigManager getCalculationConfigManager() {
        getService(CalculationConfigManager.class.getName())
    }
}
