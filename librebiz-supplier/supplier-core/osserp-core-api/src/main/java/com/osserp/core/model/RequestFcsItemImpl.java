/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 5:54:28 PM 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.FcsItem;
import com.osserp.core.requests.Request;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestFcsItemImpl extends AbstractFcsItem implements FcsItem {

    protected RequestFcsItemImpl() {
        super();
    }

    /**
     * Default constructor to add a new item to a request
     * @param reference
     * @param action
     * @param created
     * @param createdBy
     * @param note
     * @param headline
     * @param cancels
     * @param closing
     */
    public RequestFcsItemImpl(
            Request reference,
            FcsAction action,
            Date created,
            Long createdBy,
            String note,
            String headline,
            boolean cancels,
            FcsClosing fcsClosing) {
        super(reference.getRequestId(), action, created, createdBy, note, headline, cancels, fcsClosing);
    }

    /**
     * Used for wastebasket init
     * @param id
     * @param referenceId
     * @param employeeId
     * @param action
     * @param created
     */
    public RequestFcsItemImpl(Long id, Long referenceId, Long employeeId, FcsAction action, Date created) {
        super(id, referenceId, employeeId, action, created);
    }

    public RequestFcsItemImpl(FcsItem otherValue) {
        super(otherValue);
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        return root;
    }
}
