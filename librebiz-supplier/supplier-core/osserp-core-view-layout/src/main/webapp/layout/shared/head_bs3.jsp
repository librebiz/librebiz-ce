<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<o:charset/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%-- The above 3 meta tags *must* come first in the head; 
     any other head content must come *after* these tags --%>
<o:base/>
<meta name="description" content="Open, smart and simple ERP">
<meta name="author" content="developers@osserp.com">
<link rel="icon" href="<c:url value="/img/favicon.png"/>" type="image/png" />
<link rel="apple-touch-icon-precomposed" href="<c:url value="/img/favicon.png"/>" />
<meta name="msapplication-TileImage" content="<c:url value="/img/favicon.png"/>" />
<c:import url="/layout/shared/head_bootstrap3_css.jsp" />
