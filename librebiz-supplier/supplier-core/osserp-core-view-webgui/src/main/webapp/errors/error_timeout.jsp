<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/error_public.jsp" flush="false">
    <tiles:put name="title">Authentication Required</tiles:put>
    <tiles:put name="error">Authentication Required</tiles:put>
    <tiles:put name="message">The requested resource requires authentication.</tiles:put>
</tiles:insert>
<o:invalidateSession/>
