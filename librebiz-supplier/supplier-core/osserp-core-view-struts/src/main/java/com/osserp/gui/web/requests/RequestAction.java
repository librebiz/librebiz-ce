/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Oct-2006 12:46:51 
 * 
 */
package com.osserp.gui.web.requests;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.sales.SalesUtil;
import com.osserp.gui.web.business.AbstractBusinessCaseBaseAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestAction extends AbstractBusinessCaseBaseAction {
    private static Logger log = LoggerFactory.getLogger(RequestAction.class.getName());

    /**
     * Displays a sales request
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward display(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("display() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        if (view.isSalesContext()) {
            view.setReadOnlyMode(true);
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Change customer for this request
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeCustomer(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeCustomer() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            Long id = RequestUtil.fetchId(request);
            view.updateCustomer(id);
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Selects a sales request Exits from request create action
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward select(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("select() request from " + getUser(session).getId());
        }
        BusinessCaseView view = null;
        try {
            view = SalesUtil.createBusinessCaseView(
                servlet.getServletContext(), request,
                SalesUtil.loadRequest(request, fetchId(request)));
        } catch (Exception e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
        String exit = RequestUtil.getExit(request);
        if (exit != null) {
            view.setExitTarget(exit);
        }
        String target = RequestUtil.getTarget(request);
        if (target != null) {
            view.setActionTarget(target);
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    private Long fetchId(HttpServletRequest request) {
        if (request.getAttribute("id") != null) {
            return (Long) request.getAttribute("id");
        }
        return RequestUtil.getId(request);
    }

    /**
     * Enables name edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableNameEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableNameEditMode() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        view.setNameEditMode(true);
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Disables name edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableNameEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableNameEditMode() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        view.setNameEditMode(false);
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Updates the name of current sales request
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateName(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateName() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            view.updateName(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Enables address edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableAddressEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableAddressEditMode() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        view.setAddressEditMode(true);
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Disables address edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableAddressEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableAddressEditMode() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        view.setAddressEditMode(false);
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Updates the address of current sales request
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward updateAddress(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateAddress() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            view.updateAddress(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Enables branch edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableBranchEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableBranchEditMode() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        view.setBranchEditMode(true);
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Disables branch edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableBranchEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableBranchEditMode() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        view.setBranchEditMode(false);
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Updates the branch of current sales request
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateBranch(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateBranch() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            view.updateBranch(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(view.getForwardTarget());
    }

    public ActionForward enableCoSalesPercentMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableCoSalesPercentMode() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        view.setCoSalesPercentMode(true);
        return mapping.findForward("salesSettings");
    }

    public ActionForward disableCoSalesPercentMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableCoSalesPercentMode() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        view.setCoSalesPercentMode(false);
        return mapping.findForward("salesSettings");
    }

    public ActionForward updateCoSalesPercent(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateCoSalesPercent() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            view.updateCoSalesPercent(new StrutsForm(form));
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward("salesSettings");
    }

    /**
     * Creates a document by predefined template. 
     * See businessCaseView.templates property.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createDocument(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createDocument() request from " + getUser(session).getId());
        }
        Long templateId = RequestUtil.getId(request);
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            return forwardPdfOutput(mapping, request, view.createDocument(templateId));
        } catch (ClientException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(view.getForwardTarget());
        }
    }
}
