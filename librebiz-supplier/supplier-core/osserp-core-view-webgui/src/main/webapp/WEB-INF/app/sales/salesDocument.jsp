<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource />
<v:view viewName="salesDocumentView" />
<c:set var="dmsDocumentView" scope="request" value="${sessionScope.salesDocumentView}" />
<c:set var="dmsDocumentUrl" scope="request" value="/sales/salesDocument"/>
<c:set var="dmsDeletePermissions" scope="request" value="delete_sales_documents,delete_sales_pictures,organisation_admin,executive_sales"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="documentsTo" />
        <o:out value="${view.businessCase.primaryKey}" /> - <o:out value="${view.businessCase.name}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="subcolumns">
            <div class="column50l">
                <div class="subcolumnl">
                    <div class="spacer"></div>
                    <div class="contentBox">
                        <div class="contentBoxHeader">
                            <div class="contentBoxHeaderLeft">
                                <fmt:message key="picturesAndCorrespondence" />
                            </div>
                        </div>
                        <div class="contentBoxData">
                            <div class="subcolumns">
                                <div class="subcolumn">
                                    <div class="spacer"></div>
                                    <table class="valueTable first33">
                                        <tr>
                                            <td>[<o:out value="${view.pictureCount}" />] <v:link url="/sales/salesPicture/forward?exit=/sales/salesDocument/reload">
                                                    <fmt:message key="pictures" />
                                                </v:link></td>
                                            <td>[<o:out value="${view.letterCount}" />] <v:link url="/sales/salesLetter/forward?exit=/sales/salesDocument/reload">
                                                    <fmt:message key="letters" />
                                                </v:link></td>
                                        </tr>
                                    </table>
                                    <div class="spacer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="spacer"></div>
                </div>
            </div>
            <div class="column50r">
                <div class="subcolumnr">
                    <div class="spacer"></div>
                    <div class="contentBox">
                        <div class="contentBoxHeader">
                            <div class="contentBoxHeaderLeft">
                                <fmt:message key="sales" />
                            </div>
                        </div>
                        <div class="contentBoxData">
                            <div class="subcolumns">
                                <div class="subcolumn">
                                    <div class="spacer"></div>
                                    <table class="valueTable first33">
                                        <tr>
                                            <td colspan="2"><a href="<c:url value="/salesOrder.do?method=forward&exit=salesDocuments"/>"><fmt:message key="orderConfirmation" /></a></td>
                                        </tr>
                                    </table>
                                    <div class="spacer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="spacer"></div>
                </div>
            </div>
        </div>
        <c:import url="_salesDocumentList.jsp" />
    </tiles:put>
</tiles:insert>
