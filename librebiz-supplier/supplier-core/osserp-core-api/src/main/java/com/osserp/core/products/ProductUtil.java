/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 4, 2011 2:21:59 PM 
 * 
 */
package com.osserp.core.products;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.customers.Customer;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class ProductUtil {

    private static Logger log = LoggerFactory.getLogger(ProductUtil.class.getName());

    public static final String PRODUCT_PRICE_PURCHASING_PROPERTY_DEFAULT = "productPriceDefaultPurchasing";
    public static final String PRODUCT_PRICE_PURCHASING_PROPERTY_METHOD = "productPriceMethodPurchasing";
    public static final String PRODUCT_PRICE_SALES_PROPERTY_DEFAULT = "productPriceDefaultSales";
    public static final String PRODUCT_PRICE_SALES_PROPERTY_METHOD = "productPriceMethodSales";
    public static final String METHOD_LAST_PRICE_PAID = "lastPrice";
    public static final String METHOD_ESTIMATED_PURCHASE_PRICE = "estimatedPurchasePrice";

    public static boolean isPartnerPriceEditable(List<ProductSelectionConfigItem> items, Product product) {
        if (items != null && !items.isEmpty() && product != null) {
            for (int i = 0, j = items.size(); i < j; i++) {
                ProductSelectionConfigItem item = items.get(i);
                if (item.isMatching(product)) {
                    if (log.isInfoEnabled()) {
                        log.info("isPartnerPriceEditable() found matching [product=" + product.getProductId() + "]");
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public static Double getDefaultPrice(Product product, ClassifiedContact contact, String method, String missing) {
        Double result = 0.0d;
        if (product != null) {
            if (METHOD_LAST_PRICE_PAID.equals(method)) {
                if (product.getSummary().getLastPricePaid() != null
                        && product.getSummary().getLastPricePaid().doubleValue() > 0) {
                    result = product.getSummary().getLastPricePaid();
                } else if ("null".equals(missing)) {
                    result = 0d;
                } else {
                    result = getDefaultPrice(product, contact);
                }
            } else if (METHOD_ESTIMATED_PURCHASE_PRICE.equals(method)) {
                result = product.getEstimatedPurchasePrice() != null
                        ? product.getEstimatedPurchasePrice() : 0d;
            } else {
                result = getDefaultPrice(product, contact);
            }
        }
        return result;
    }

    private static Double getDefaultPrice(Product product, ClassifiedContact contact) {
        Double result = product.getConsumerPrice();
        if (contact instanceof Customer) {
            Customer customer = (Customer) contact;
            if (customer.isHolding() || customer.isClient()) {
                result = product.getPartnerPrice();
            } else if (customer.isReseller()) {
                result = product.getResellerPrice();
            }
        } else if (contact instanceof Supplier) {
            result = product.getSummary().getLastPurchasePrice();
        }
        return result;
    }
}
