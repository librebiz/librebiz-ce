/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 7, 2006 2:31:35 PM 
 * 
 */
package com.osserp.core.finance;

import com.osserp.common.ClientException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordPaymentAgreement extends PaymentAgreement {

    /**
     * Updates the payment agreement
     * @param amounts
     * @param hidePayments
     * @param amountsFixed
     * @param downpayment
     * @param deliveryInvoice
     * @param finalInvoice
     * @param downpaymentTargetId
     * @param deliveryInvoiceTargetId
     * @param finalInvoiceTargetId
     * @param paymentCondition
     * @param note
     * @param changedBy
     * @throws ClientException if validation failes
     */
    void update(
            Amounts amounts,
            boolean hidePayments,
            boolean amountsFixed,
            Double downpayment,
            Double deliveryInvoice,
            Double finalInvoice,
            Long downpaymentTargetId,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            PaymentCondition paymentCondition,
            String note,
            Long changedBy)
            throws ClientException;

    /**
     * The price per unit
     * @return pricePerUnit
     */
    Double getPricePerUnit();

    /**
     * Sets the price per unit
     * @param pricePerUnit
     */
    void setPricePerUnit(Double pricePerUnit);

    /**
     * The amount of the price to be paid as downpayment
     * @return downpaymentAmount
     */
    Double getDownpaymentAmount();

    /**
     * Sets the amount of the price to be paid as downpayment
     * @param downpaymentAmount
     */
    void setDownpaymentAmount(Double downpaymentAmount);

    /**
     * The amount of the price to be paid after delivery
     * @return deliveryInvoiceAmount
     */
    Double getDeliveryInvoiceAmount();

    /**
     * Sets the amount of the price to be paid after delivery
     * @param deliveryInvoiceAmount
     */
    void setDeliveryInvoiceAmount(Double deliveryInvoiceAmount);

    /**
     * The amount of the price to be paid at final invoice
     * @return finalInvoiceAmount
     */
    Double getFinalInvoiceAmount();

    /**
     * Sets the amount of the price to be paid at final invoice
     * @param finalInvoiceAmount
     */
    void setFinalInvoiceAmount(Double finalInvoiceAmount);

    /**
     * Indicates whether the amounts are fixed
     * @return true if so
     */
    boolean isAmountsFixed();

    /**
     * Sets amountsFixed status
     * @param amountsFixed
     */
    void setAmountsFixed(boolean amountsFixed);

    /**
     * Toggles hidePayments flag
     */
    void toggleHidePayments();
}
