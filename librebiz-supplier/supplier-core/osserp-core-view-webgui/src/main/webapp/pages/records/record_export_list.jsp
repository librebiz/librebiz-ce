<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.recordExportView}" />
<c:set var="list" value="${requestScope.recordExportList}" />

<div class="row">
    <div class="col-md-12 panel-area">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="recordId"><fmt:message key="number"/></th>
                        <th class="recordContactId"><fmt:message key="customerId"/></th>
                        <th class="recordContact"><fmt:message key="customer"/></th>
                        <th class="recordDate"><fmt:message key="dated"/></th>
                        <th class="amount"><fmt:message key="amount"/></th>
                        <th class="amount"><fmt:message key="turnoverTax"/></th>
                        <th class="amount reducedTax"><fmt:message key="reducedTurnoverTax"/></th>
                        <th class="amount"><fmt:message key="gross"/></th>
                        <c:if test="${view.selectedDisplayMode or view.unexportedDisplayMode}">
                        <th class="action"> </th>
                        </c:if>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="record" items="${list}" varStatus="s">
                        <tr>
                            <c:choose>
                                <c:when test="${view.cancellationSelected}">
                                    <td class="recordId"><a href="<c:url value="/salesCancellation.do?method=display&exit=unexported&readonly=true&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                    <td class="recordContactId"><o:out value="${record.contactId}"/></td>
                                    <td class="recordContact"><o:out value="${record.contactName}"/></td>
                                </c:when>
                                <c:when test="${view.creditNoteSelected}">
                                    <td class="recordId"><a href="<c:url value="/salesCreditNote.do?method=display&exit=unexported&readonly=true&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                    <td class="recordContactId"><o:out value="${record.contactId}"/></td>
                                    <td class="recordContact">
                                        <c:choose>
                                            <c:when test="${record.unchangeable and !empty record.typeName}">
                                                <v:link url="/records/recordPrint/print?name=${record.typeName}&id=${record.printId}" title="documentPrint"><o:out value="${record.contactName}"/></v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${record.contactName}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </c:when>
                                <c:when test="${view.downpaymentSelected}">
                                    <td class="recordId"><a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=unexported&readonly=true&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                    <td class="recordContactId"><o:out value="${record.contactId}"/></td>
                                    <td class="recordContact">
                                        <c:choose>
                                            <c:when test="${record.unchangeable and !empty record.typeName}">
                                                <v:link url="/records/recordPrint/print?name=${record.typeName}&id=${record.printId}" title="documentPrint"><o:out value="${record.contactName}"/></v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${record.contactName}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </c:when>
                                <c:when test="${view.purchaseInvoiceSelected}">
                                    <td class="recordId"><a href="<c:url value="/purchaseInvoice.do?method=display&exit=unexported&readonly=true&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                    <td class="recordContactId"><o:out value="${record.contactId}"/></td>
                                    <td class="recordContact">
                                        <c:choose>
                                            <c:when test="${record.unchangeable and !empty record.typeName}">
                                                <v:link url="/records/recordPrint/print?name=${record.typeName}&id=${record.printId}" title="documentPrint"><o:out value="${record.contactName}"/></v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${record.contactName}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </c:when>
                                <c:when test="${view.purchasePaymentSelected}">
                                    <td class="recordId"><a href="<c:url value="/purchaseInvoice.do?method=display&exit=unexported&readonly=true&id=${record.reference}"/>"><o:out value="${record.referenceNumber}"/></a></td>
                                    <td class="recordContactId"><o:out value="${record.contactId}"/></td>
                                    <td class="recordContact">
                                        <o:out value="${record.contactName}"/>
                                    </td>
                                </c:when>
                                <c:when test="${view.salesPaymentSelected}">
                                    <td class="recordId"><a href="<c:url value="/salesInvoice.do?method=display&exit=unexported&readonly=true&id=${record.reference}"/>"><o:out value="${record.referenceNumber}"/></a></td>
                                    <td class="recordContactId"><o:out value="${record.contactId}"/></td>
                                    <td class="recordContact">
                                        <o:out value="${record.contactName}"/>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td class="recordId"><a href="<c:url value="/salesInvoice.do?method=display&exit=unexported&readonly=true&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                    <td class="recordContactId"><o:out value="${record.contactId}"/></td>
                                    <td class="recordContact">
                                        <c:choose>
                                            <c:when test="${record.unchangeable and !empty record.typeName}">
                                                <v:link url="/records/recordPrint/print?name=${record.typeName}&id=${record.printId}" title="documentPrint"><o:out value="${record.contactName}"/></v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${record.contactName}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </c:otherwise>
                            </c:choose>
                            <td class="recordDate"><o:date value="${record.created}"/></td>
                            <c:choose>
                                <c:when test="${view.downpaymentSelected}">
                                    <td class="amount"><o:number value="${record.prorateNetAmount}" format="currency"/></td>
                                    <td class="amount"><o:number value="${record.prorateTaxAmount}" format="currency"/></td>
                                    <td class="amount reducedTax">0,00</td>
                                    <td class="amount"><o:number value="${record.prorateGrossAmount}" format="currency"/></td>
                                </c:when>
                                <c:otherwise>
                                    <td class="amount"><o:number value="${record.netAmount}" format="currency"/></td>
                                    <td class="amount"><o:number value="${record.taxAmount}" format="currency"/></td>
                                    <td class="amount reducedTax"><o:number value="${record.reducedTaxAmount}" format="currency"/></td>
                                    <td class="amount"><o:number value="${record.grossAmount}" format="currency"/></td>
                                </c:otherwise>
                            </c:choose>
                            <c:if test="${view.selectedDisplayMode or view.unexportedDisplayMode}">
                                <td class="action">
                                    <input type="checkbox" name="recordId" onclick="gotoUrl('<c:url value="${recordExportSelection}&recordId=${record.id}"/>');"  value="${record.id}"/>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    <c:if test="${!empty list and (view.selectedDisplayMode or view.unexportedDisplayMode)}">
                        <tr>
                            <th colspan="8" class="submitHint"> </th>
                            <th class="action"><input type="image" src="<c:url value="${applicationScope.saveIcon}"/>" title="<fmt:message key="selectAllSelected"/>" /></th>
                        </tr>
                    </c:if>
                </tbody>
            </table>
        </div>
    </div>
</div>
