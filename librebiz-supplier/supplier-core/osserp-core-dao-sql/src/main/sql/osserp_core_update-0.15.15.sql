SET search_path = osserp, pg_catalog;

INSERT INTO system_properties VALUES (232, 'salesDownpaymentHeaderByType', 'false', 'java.lang.Boolean', false, false, NULL, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 232, true);

UPDATE system_properties SET pvalue = '0.15.15', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
