/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 10, 2009 8:06:55 AM 
 * 
 */
package com.osserp.core.model.products;

import java.util.Set;

import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractProductSelectionConfigItem extends AbstractEntity implements ProductSelectionConfigItem {

    private Product product = null;
    private ProductCategory category = null;
    private ProductGroup group = null;
    private ProductType type = null;
    private boolean exclusion = false;
    private boolean partnerPriceEditable = false;
    private Long referenceOwner = null;

    /**
     * Default constructor required by {@link java.io.Serializable}
     */
    protected AbstractProductSelectionConfigItem() {
        super();
    }

    /**
     * Creates a new product selection item
     * @param createdBy
     * @param config
     * @param type
     * @param group
     * @param category
     */
    protected AbstractProductSelectionConfigItem(
            Long createdBy,
            AbstractProductSelectionConfig config,
            ProductType type,
            ProductGroup group,
            ProductCategory category) {

        super((Long) null, config.getId(), createdBy);
        this.category = category;
        this.group = group;
        this.type = type;
    }

    /**
     * Creates a new product selection item
     * @param type
     * @param group
     * @param category
     */
    protected AbstractProductSelectionConfigItem(
            ProductType type,
            ProductGroup group,
            ProductCategory category) {
        super();
        this.category = category;
        this.group = group;
        this.type = type;
    }

    protected AbstractProductSelectionConfigItem(ProductSelectionConfig config, ProductSelectionConfigItem item) {
        super(item.getId(), config.getId(), config.getCreatedBy());
        this.category = item.getCategory();
        this.group = item.getGroup();
        this.type = item.getType();
    }

    public final boolean isMatching(Product other) {
        Set<Long> types = new java.util.HashSet<Long>();
        for (int i = 0, j = other.getTypes().size(); i < j; i++) {
            ProductType next = other.getTypes().get(i);
            types.add(next.getId());
        }
        return isMatching(
                other.getProductId(),
                types,
                (other.getGroup() == null ? null : other.getGroup().getId()),
                (other.getCategory() == null ? null : other.getCategory().getId()));
    }

    public final boolean isMatching(Long productId, Set<Long> types, Long groupId, Long categoryId) {
        if (product != null && productId != null) {
            return product.getProductId().equals(productId);
        }
        if (type != null) {
            boolean typeMatching = false;
            for (java.util.Iterator<Long> i = types.iterator(); i.hasNext();) {
                Long next = i.next();
                if (next.equals(type.getId())) {
                    typeMatching = true;
                }
            }
            if (!typeMatching) {
                return false;
            }
        }
        if (group != null && groupId != null) {
            if (!group.getId().equals(groupId)) {
                return false;
            }
        } else if (group != null && groupId == null) {
            return false;
        }
        if (category != null && categoryId != null) {
            if (!category.getId().equals(categoryId)) {
                return false;
            }
        } else if (category != null && categoryId == null) {
            return false;
        }
        return true;
    }

    public final boolean isSame(ProductSelectionConfigItem other) {
        if (product != null) {
            if (other.getProduct() != null
                    && product.getProductId().equals(other.getProduct().getProductId())) {
                return true;
            }
            return false;
        }
        if (isSame(category, other.getCategory())
                && isSame(group, other.getGroup())
                && isSame(type, other.getType())) {
            return true;
        }
        return false;
    }

    public Long getReferenceOwner() {
        return referenceOwner;
    }

    public void setReferenceOwner(Long referenceOwner) {
        this.referenceOwner = referenceOwner;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public ProductGroup getGroup() {
        return group;
    }

    public void setGroup(ProductGroup group) {
        this.group = group;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public boolean isExclusion() {
        return exclusion;
    }

    public void setExclusion(boolean exclusion) {
        this.exclusion = exclusion;
    }

    public boolean isPartnerPriceEditable() {
        return partnerPriceEditable;
    }

    public void setPartnerPriceEditable(boolean partnerPriceEditable) {
        this.partnerPriceEditable = partnerPriceEditable;
    }
}
