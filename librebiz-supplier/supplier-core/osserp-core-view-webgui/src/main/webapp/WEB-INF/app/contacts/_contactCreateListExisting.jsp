<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="${sessionScope.contactInputViewName}" />
<div class="row">
    <div class="col-md-12 panel-area">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="name"><fmt:message key="justEntered" /></th>
                        <th class="street"><fmt:message key="street" /></th>
                        <th class="city"><fmt:message key="city" /></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="name">
                            <a href="javascript:onclick=confirmLink('<fmt:message key="confirmation"/>: <fmt:message key="confirmContactCreateAnyway"/>?', '<v:url value="/contacts/contactCreator/confirm"/>');" title="<fmt:message key="contactNotExistsCreateByRequestValues"/>">
                                <c:choose>
                                    <c:when test="${view.privateContactSelected}">
                                        <v:formOut name="lastName" />, <v:formOut name="firstName" />
                                    </c:when>
                                    <c:otherwise>
                                        <v:formOut name="company" />
                                    </c:otherwise>
                                </c:choose>
                            </a>
                        </td>
                        <td class="street"><v:formOut name="street" /></td>
                        <td class="city"><v:formOut name="zipcode" /> <v:formOut name="city" /></td>
                    </tr>
                    <tr>
                        <th class="name"><fmt:message key="alreadyAvailable" /></th>
                        <th class="street"><fmt:message key="street" /></th>
                        <th class="city"><fmt:message key="city" /></th>
                    </tr>
                    <c:forEach var="existing" items="${view.existingContacts}" varStatus="s">
                        <tr>
                            <td class="name">
                                <a href="javascript:onclick=confirmLink('<fmt:message key="confirmation"/>: <fmt:message key="contactExistsDiscardInput"/>?', '<v:url value="/contacts/contactCreator/discard?id=${existing.contactId}"/>');" title="<fmt:message key="contactExistsDiscardInput"/>">
                                    <o:out value="${existing.displayName}" />
                                </a>
                            </td>
                            <td class="street"><o:out value="${existing.address.street}" /></td>
                            <td class="city"><o:out value="${existing.address.zipcode}" /> <o:out value="${existing.address.city}" /></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
