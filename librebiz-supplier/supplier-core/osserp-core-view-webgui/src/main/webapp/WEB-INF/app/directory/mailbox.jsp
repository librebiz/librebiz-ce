<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="mailboxView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <fmt:message key="directoryMailboxView" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="mailboxContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.createMode}">
                        <c:import url="_mailboxCreate.jsp" />
                    </c:when>
                    <c:when test="${!empty view.bean}">
                        <c:import url="_userBean.jsp" />
                    </c:when>
                    <c:otherwise>
                        <c:import url="_userList.jsp" />
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>