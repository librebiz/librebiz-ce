/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 10, 2011 12:27:22 PM 
 * 
 */
package com.osserp.core.service.calc;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.util.NumberUtil;
import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationUtil;
import com.osserp.core.calc.CalculatorService;
import com.osserp.core.dao.Calculations;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.sales.SalesOfferManager;
import com.osserp.core.sales.SalesOrderManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculatorByItems extends AbstractCalculatorService {
    private static Logger log = LoggerFactory.getLogger(CalculatorByItems.class.getName());

    protected CalculatorByItems(
            Calculations calculations,
            Projects projects,
            Requests requests,
            SalesOfferManager salesOfferManager,
            SalesOrderManager salesOrderManager) {
        super(calculations, projects, requests, salesOfferManager, salesOrderManager);
    }

    public String getName() {
        return CalculatorService.CALCULATOR_BY_ITEMS;
    }

    @Override
    protected void calculate(Calculation calculation, boolean calculateOnly) throws ClientException {
        if (!calculateOnly) {
            CalculationUtil.validateProductPriceInput(calculation, true);
        }
        List<Item> items = calculation.getAllItems();
        if (calculation.isPackageAvailable()) {
            if (log.isDebugEnabled()) {
                log.debug("calculate() invoked for calculation with package [calculation=" + calculation.getId() + "]");
            }
            BigDecimal partnerPriceSummary = new BigDecimal(0);
            BigDecimal salesPriceSummary = new BigDecimal(0);
            for (Iterator<Item> i = items.iterator(); i.hasNext();) {
                Item next = i.next();
                if (!next.getProduct().isPlant() && !next.isIncludePrice()) {
                    if (log.isDebugEnabled()) {
                        log.debug("calculate() adding values for included product [calculation="
                                + calculation.getId()
                                + ", product=" + ((next.getProduct() == null) ? "null" : next.getProduct().getProductId().toString())
                                + ", partnerPrice=" + next.getPartnerPrice()
                                + ", price=" + next.getPrice()
                                + "]");
                    }
                    if (next.getPartnerPrice() != null) {
                        partnerPriceSummary = partnerPriceSummary.add(next.getPartnerPrice().multiply(new BigDecimal(next.getQuantity())));
                    }
                    if (next.getPrice() != null) {
                        salesPriceSummary = salesPriceSummary.add(next.getPrice().multiply(new BigDecimal(next.getQuantity())));
                    }
                }
            }
            calculation.updatePackagePrice(NumberUtil.round(partnerPriceSummary, 5), NumberUtil.round(salesPriceSummary, 5));
            try {
                getCalculations().save(calculation);
            } catch (RuntimeException e) {
                // Catch and ignore -but log- rarely thrown hibernate state exception
                // when saving calculations in this context.
                // Ignoring does not harm here since save is called later if required.
                log.warn("calculate() Ignoring runtime exception, message: " + e.getMessage(), e);
            }
        } else if (log.isDebugEnabled()) {
            log.debug("calculate() no package found, nothing to do");
        }
    }
}
