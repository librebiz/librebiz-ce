/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 4, 2010 3:57:18 PM 
 * 
 */
package com.osserp.core.tasks.impl;

import com.osserp.common.service.MessageSender;
import com.osserp.common.service.impl.AbstractMessageInitiator;
import com.osserp.core.tasks.SfaSubscriberSender;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class SfaSubscriberSenderImpl extends AbstractMessageInitiator implements SfaSubscriberSender {

    protected SfaSubscriberSenderImpl(MessageSender sender, String destinationQueue) {
        super(sender, destinationQueue, "sfaSubscriberTask");
    }

    public void send(Long id) {
        send(id.toString());
    }
}
