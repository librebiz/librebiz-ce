/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Apr-2005 18:33:15 
 * 
 */
package com.osserp.common.dms;

import java.io.File;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.FileUtil;
import com.osserp.common.service.Locator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DmsUtil {

    private static Logger log = LoggerFactory.getLogger(DmsUtil.class.getName());

    public static final int DEFAULT_THUMBNAIL_HEIGHT = 240;
    public static final int DEFAULT_THUMBNAIL_WIDTH = 240;

    public static String getRealFileName(
            Long documentId,
            Long reference,
            boolean suffixByFile,
            String defaultSuffix,
            String prefix,
            String postfix,
            String suffix,
            String sourceFileName) {
        StringBuilder buffer = new StringBuilder(22);
        buffer
                .append(Long.valueOf(prefix) + reference)
                .append(Long.valueOf(postfix) + documentId);
        if (suffixByFile) {
            if (suffix != null && suffix.length() > 0) {
                buffer.append(".").append(suffix);
            } else {
                String suffixByFileName = FileUtil.getFileType(sourceFileName);
                if (suffixByFileName != null && suffixByFileName.length() > 0) {
                    buffer.append(".").append(suffixByFileName);
                }
            }
        } else {
            buffer.append(".").append(defaultSuffix);
        }
        return buffer.toString();
    }
    
    public static String createAbsolutePath(String path, String filename) {
        if (filename == null || filename.length() < 1) {
            throw new IllegalArgumentException("filename must not be null");
        }
        StringBuilder buffer = new StringBuilder();
        boolean filenameWithSeparator = filename.startsWith("/"); 
        if (path != null) {
            if ((path.endsWith("/") && !filenameWithSeparator)
                    || (!path.endsWith("/") && filenameWithSeparator)) {
                buffer.append(path).append(filename);
            } else if (path.endsWith("/") && filenameWithSeparator) {
                buffer.append(path.substring(0, path.length() - 1)).append(filename);
            } else {
                buffer.append(path).append("/").append(filename);
            }
            return buffer.toString();
        }
        return filename;
    }
    
    public static String checkAndPrepareDmsDirectory(String root, String prefix, String dir) {
        StringBuilder file = new StringBuilder(root);
        if (prefix != null && prefix.length() > 0) {
            file.append("/").append(prefix);
        }
        String customDir = file.toString();
        File fobject = new File(customDir);
        try {
            if (fobject.exists() && !fobject.isDirectory()) {
                
            }
        } catch (SecurityException e) {
            
        }
        if (prefix != null && prefix.length() > 0) {
            file.append("/").append(prefix);
        }
        file.append("/").append(dir);
        return file.toString();
    }
    
    public static List<? extends DmsDocument> sortByDate(List<? extends DmsDocument> list, boolean descend) {
        return CollectionUtil.sort(list, comparatorByDate(descend));
    }
    
    public static List<? extends DmsDocument> sortByDescription(List<? extends DmsDocument> list) {
        return CollectionUtil.sort(list, comparatorByDescription());
    }
    
    public static List<? extends DmsDocument> sortByFilename(List<? extends DmsDocument> list) {
        return CollectionUtil.sort(list, comparatorByFilename());
    }
    
    public static List<? extends DmsDocument> sortByOrder(List<? extends DmsDocument> list, boolean descend) {
        return CollectionUtil.sort(list, comparatorByOrderId(descend));
    }

    public static Comparator comparatorByDate(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    DmsDocument docA = (DmsDocument) a;
                    DmsDocument docB = (DmsDocument) b;
                    Date dateA = docA.getCreated();
                    Date dateB = docB.getCreated();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                DmsDocument docA = (DmsDocument) a;
                DmsDocument docB = (DmsDocument) b;
                Date dateA = docA.getCreated();
                Date dateB = docB.getCreated();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static Comparator comparatorByDescription() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                String sA = ((DmsDocument) a).getDescription();
                String sB = ((DmsDocument) b).getDescription();
                if (sA == null && sB == null) {
                    return 0;
                }
                if (sA == null) {
                    return 1;
                }
                if (sB == null) {
                    return -1;
                }
                return sA.compareTo(sB);
            }
        };
    }

    public static Comparator comparatorByFilename() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                String sA = ((DmsDocument) a).getFileName();
                String sB = ((DmsDocument) b).getFileName();
                if (sA == null && sB == null) {
                    return 0;
                }
                if (sA == null) {
                    return 1;
                }
                if (sB == null) {
                    return -1;
                }
                return sA.compareTo(sB);
            }
        };
    }

    public static Comparator comparatorByOrderId(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    DmsDocument docA = (DmsDocument) a;
                    DmsDocument docB = (DmsDocument) b;
                    int orderIdA = docA.getOrderId();
                    int orderIdB = docB.getOrderId();
                    
                    if (orderIdA == 0 && orderIdB == 0) {
                        return 0;
                    }
                    if (orderIdA == 0) {
                        return -1;
                    }
                    if (orderIdB == 0) {
                        return 1;
                    }
                    return Integer.valueOf(orderIdB).compareTo(Integer.valueOf(orderIdA));
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                DmsDocument docA = (DmsDocument) a;
                DmsDocument docB = (DmsDocument) b;
                int orderIdA = docA.getOrderId();
                int orderIdB = docB.getOrderId();
                
                if (orderIdA == 0 && orderIdB == 0) {
                    return 0;
                }
                if (orderIdA == 0) {
                    return 1;
                }
                if (orderIdB == 0) {
                    return -1;
                }
                return Integer.valueOf(orderIdA).compareTo(Integer.valueOf(orderIdB));
            }
        };
    }

    public static Comparator comparatorByValidFrom(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    DmsDocument docA = (DmsDocument) a;
                    DmsDocument docB = (DmsDocument) b;
                    Date dateA = docA.getValidFrom();
                    Date dateB = docB.getValidFrom();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                DmsDocument docA = (DmsDocument) a;
                DmsDocument docB = (DmsDocument) b;
                Date dateA = docA.getValidFrom();
                Date dateB = docB.getValidFrom();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static DmsManager getDmsManager(Locator locator, DocumentType documentType) {
        try {
            if (locator != null) {
                DmsManager manager = (DmsManager) locator.lookupService(documentType.getServiceName());
                return manager;
            }
            log.warn("getDmsManager() invocation without locator!");
        } catch (Exception e) {
            log.error("getDmsManager() failed [type=" 
                    + (documentType == null ? "null" : documentType.getId()) 
                    + ", class=" 
                    + (documentType == null || documentType.getServiceName() == null 
                    ? "null" : documentType.getServiceName()) + "]");
        }
        return null;
    }

    public static int getThumbnailHeight(DocumentType documentType) {
        return documentType != null && documentType.getThumbmailHeight() > 0 ?
                documentType.getThumbmailHeight() : DmsUtil.DEFAULT_THUMBNAIL_HEIGHT;
    }

    public static int getThumbnailWidth(DocumentType documentType) {
        return documentType != null && documentType.getThumbmailWidth() > 0 ?
                documentType.getThumbmailWidth() : DmsUtil.DEFAULT_THUMBNAIL_WIDTH;
    }

}
