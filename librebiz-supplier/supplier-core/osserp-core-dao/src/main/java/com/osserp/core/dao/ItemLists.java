/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 7, 2009 3:16:31 PM 
 * 
 */
package com.osserp.core.dao;

import com.osserp.core.ItemList;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ItemLists {

    /**
     * Checks calculation exists
     * @param id
     * @return true if calculation exists
     */
    boolean exists(Long id);

    /**
     * Loads an item list from persistent storage. Throws a runtime exception if no such list exists
     * @param id
     * @return itemList
     */
    ItemList load(Long id);

    /**
     * Persists an item list
     * @param itemList
     */
    void save(ItemList itemList);

    /**
     * Finds the id of the item list whose historical flag is false. This indicates that it's the active item list
     * @param referenceId
     * @return itemList or null if none exists
     */
    ItemList getActive(Long referenceId);

    /**
     * Sets a new item list as active, marks old as historical
     * @param newListId
     * @param reference id new
     * @return update status
     */
    int setOldHistorical(Long newListId, Long reference);

    /**
     * Deletes an item list. Resets previous list's historical flag.
     * @param list
     */
    void delete(ItemList list);

    /**
     * Provides the last item list related to given reference
     * @param referenceId
     * @param contextName
     * @return last list or null if not exists
     */
    ItemList getLast(Long referenceId, String contextName);

}
