<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="projectTrackingView" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="bookingEdit" />
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <c:choose>
            <c:when test="${view.bean.type.timeExactly}">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="startDate"><fmt:message key="dateDatetime" /></label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <v:date name="startTime" styleClass="form-control" value="${view.selectedRecord.startTime}"/>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <o:datepicker input="startTime" />
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <v:text name="startHours" styleClass="form-control" title="dateTimeHours" value="${view.selectedRecord.startHours}" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">:</div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <v:text name="startMinutes" styleClass="form-control" title="dateTimeMinutes" value="${view.selectedRecord.startMinutesDisplay}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="endDate"><fmt:message key="until" /></label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <v:date name="endTime" styleClass="form-control" value="${view.selectedRecord.endTime}" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <o:datepicker input="endTime" />
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <v:text name="endHours" styleClass="form-control" title="dateTimeHours" value="${view.selectedRecord.endHours}" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">:</div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <v:text name="endMinutes" styleClass="form-control" title="dateTimeMinutes" value="${view.selectedRecord.endMinutesDisplay}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </c:when>
            <c:when test="${view.bean.type.timeIgnorable}">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="startDate"><fmt:message key="dateHoursLabel" /></label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <v:date name="startTime" styleClass="form-control" value="${view.selectedRecord.startTime}" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <o:datepicker input="startTime" />
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <v:text name="duration" styleClass="form-control input-hours" title="inputDecimalHint" value="${view.selectedRecord.duration}" />
                        </div>
                    </div>
                </div>

            </c:when>
            <c:otherwise>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="startDate"><fmt:message key="dateHoursMinutesLabel" /></label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <v:date name="startTime" styleClass="form-control" value="${view.selectedRecord.startTime}" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <o:datepicker input="startTime" />
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <v:text name="hours" styleClass="form-control" title="dateTimeHours" value="${view.selectedRecord.startHours}" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <v:text name="minutes" styleClass="form-control input-time" title="minutes" value="${view.selectedRecord.startMinutes}" />
                        </div>
                    </div>
                </div>

            </c:otherwise>
        </c:choose>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="description"><fmt:message key="incident" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text name="name" styleClass="form-control" value="${view.selectedRecord.name}" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="description"><fmt:message key="description" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:textarea styleClass="form-control" name="description" rows="5" value="${view.selectedRecord.description}" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="internalNote"><fmt:message key="internalNoteLabel" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:textarea styleClass="form-control" name="internalNote" value="${view.selectedRecord.internalNote}" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4"> </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <v:submitExit url="/projects/projectTracking/disableTextEditMode"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <o:submit value="save" styleClass="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
