/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 30, 2008 9:51:04 AM 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.common.Calendar;
import com.osserp.common.PublicHoliday;
import com.osserp.common.beans.AbstractCalendar;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordMarker;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingDay;
import com.osserp.core.hrm.TimeRecordingPeriod;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingDayVO extends AbstractCalendar implements TimeRecordingDay {
    //private static Logger log = LoggerFactory.getLogger(TimeRecordingDayVO.class.getName());

    private static final int MAX_MINUTES = 10 * 60;

    private int firstBreakAfter = 360;
    private int firstBreakAfterMinutes = 30;
    private int secondBreakAfter = 540;
    private int secondBreakAfterMinutes = 45;

    private int minutesBreak = 0;
    private int minutesTotal;
    private int minutesNet = 0;
    private int minutesIllness = 0;
    private int minutesLeave = 0;
    private int minutesLost = 0;
    private int minutesDifference = 0;
    private int minutesRequired;
    private int minutesSubtraction = 0;
    private int minutesAddition = 0;

    private Date incorrectRecord = null;

    private int minutesCarryoverInMonth;

    private boolean ignoreWorkingSummary = false;
    private boolean noBooking = true;

    private String name = null;
    private Date startTime = null;
    private Date stopTime = null;
    private boolean waitForApproval = false;

    private PublicHoliday holiday = null;

    private TimeRecordingPeriod period = null;
    private TimeRecordingConfig config = null;
    private List<TimeRecord> records = new ArrayList<TimeRecord>();
    private double markerValue = 0;
    private List<TimeRecordMarker> markers = new ArrayList<TimeRecordMarker>();

    protected TimeRecordingDayVO() {
        super();
    }

    public TimeRecordingDayVO(TimeRecordingYearVO recordingYear, Calendar day, List<TimeRecord> records, List<TimeRecordMarker> markers, PublicHoliday holiday) {
        super(day.getDay(), day.getMonth(), day.getYear());

        for (int i = 0, j = records.size(); i < j; i++) {
            TimeRecord next = records.get(i);
            if (next.getType().isStarting() && next.isDay(day)) {
                this.records.add(next);
            }
        }

        for (int i = 0, j = markers.size(); i < j; i++) {
            TimeRecordMarker next = markers.get(i);
            if (next.getType().isExpireDay() && next.isDay(day)) {
                markerValue += next.getMarkerValue();
            }
        }

        this.holiday = holiday;
        /*
         * if (holiday != null && log.isDebugEnabled()) { log.debug("<init> found public holiday [day=" + day.getDay() + ", month=" + day.getMonth() +
         * ", year=" + day.getYear() + ", name=" + holiday.getName() + "]"); }
         */
        this.period = recordingYear.getPeriod();
        this.config = period.getConfig();

        if (!isBeyondRange()) {
            this.calculate();
        }
    }

    public double getMarkerValue() {
        return MAX_MINUTES / 60 + markerValue;
    }

    public boolean isMarker() {
        if (markerValue == 0) {
            return false;
        }
        return true;
    }

    public boolean isPublicHoliday() {
        return (holiday != null);
    }

    public String getPublicHolidayName() {
        return (holiday == null ? null : holiday.getName());
    }

    public int getMinutesTotal() {
        return minutesTotal;
    }

    public int getMinutesSubtraction() {
        return minutesSubtraction;
    }

    public int getMinutesAddition() {
        return minutesAddition;
    }

    public int getMinutesNet() {
        return minutesNet;
    }

    public int getMinutesBreak() {
        return minutesBreak;
    }

    public int getMinutesLost() {
        return minutesLost;
    }

    public boolean isNoBooking() {
        return noBooking;
    }

    public int getMinutesLeave() {
        return minutesLeave;
    }

    public int getMinutesIllness() {
        return minutesIllness;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public int getMinutesRequired() {
        return minutesRequired;
    }

    public String getDifferenceHoursDisplay() {
        return DateFormatter.getHoursAndMinutes(minutesDifference);
    }

    public int getMinutesDifference() {
        return minutesDifference;

    }

    public Date getIncorrectRecord() {
        return incorrectRecord;
    }

    public String getLostHoursDisplay() {
        return DateFormatter.getHoursAndMinutes(minutesLost);
    }

    public String getHoursDisplay() {
        return DateFormatter.getHoursAndMinutes(minutesNet);
    }

    public String getWorkingHoursDisplay() {
        return DateFormatter.getHoursAndMinutes(minutesRequired);
    }

    public boolean isOpen() {
        for (int i = 0, j = records.size(); i < j; i++) {
            TimeRecord next = records.get(i);
            if (next.getClosedBy() == null) {
                return true;
            }
        }
        return false;
    }

    public List<TimeRecord> getRecords() {
        return records;
    }

    public List<TimeRecordMarker> getDayMarkers() {
        return markers;
    }

    protected void setRecords(List<TimeRecord> records) {
        this.records = records;
    }

    public String getName() {
        return name;
    }

    public int getMinutesCarryoverInMonth() {
        return minutesCarryoverInMonth;
    }

    public void setMinutesCarryoverInMonth(int minutesCarryoverInMonth) {
        this.minutesCarryoverInMonth = minutesCarryoverInMonth;
    }

    public String getCarryoverHoursInMonthDisplay() {
        return DateFormatter.getHoursAndMinutes(minutesCarryoverInMonth);
    }

    public boolean isIgnoreWorkingSummary() {
        return ignoreWorkingSummary;
    }

    public boolean isLostSubtractionBooking() {
        for (int i = 0; i < this.records.size(); i++) {
            TimeRecord next = records.get(i);
            if (next.getClosedBy() != null) {
                int duration = next.getDuration().intValue();
                if (next.getType().isDaySubtraction()) {
                    if (duration == this.minutesLost) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isBooking(Date dateAndTime, boolean startBooking) {
        TimeRecord empty = null;
        if (dateAndTime != null) {
            for (int i = 0; i < this.records.size(); i++) {
                TimeRecord next = records.get(i);
                if (next.getClosedBy() != null) {
                    if (dateAndTime.after(next.getValue()) && dateAndTime.before(next.getClosedBy().getValue())) {
                        return true;
                    }
                } else {
                    empty = next;
                }
            }
            if (!startBooking && empty != null) {
                if (dateAndTime.before(empty.getValue())) {
                    return true;
                }
                return isBooking(empty.getValue(), dateAndTime);
            }
        }
        return false;
    }

    public boolean isBooking(Date start, Date stop) {
        if (start != null && stop != null) {
            for (int i = 0; i < this.records.size(); i++) {
                TimeRecord next = records.get(i);
                if (next.getClosedBy() != null) {
                    if (next.getValue().after(start) && next.getClosedBy().getValue().before(stop)) {
                        return true;
                    }
                    if (start.after(next.getValue()) && start.before(next.getClosedBy().getValue())) {
                        return true;
                    }
                    if (stop.after(next.getValue()) && stop.before(next.getClosedBy().getValue())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isBeyondRange() {
        return !DateUtil.isDayInInterval(getDate(), period.getValidFrom(), period.getValidTil());
    }

    /**
     * Not necessary yet. When needed some changes are required in this methods
     */

    /**
     * public Element getXml(int completedSummary, int periodSummary) { Element root = new Element("day"); root.addContent(new
     * Element("dateKey").setText(getDayDisplayKey())); root.addContent(new Element("publicHoliday").setText(new
     * Boolean(isPublicHoliday()).toString())); if (startTime != null) { root.addContent(new
     * Element("startTime").setText(DateFormatter.getTime(startTime))); } else { root.addContent(new Element("startTime")); } if (stopTime != null) {
     * root.addContent(new Element("stopTime").setText(DateFormatter.getTime(stopTime))); } else { root.addContent(new Element("stopTime")); }
     * root.addContent(new Element("workingTime").setText(getWorkingHoursDisplay())); root.addContent(new
     * Element("missingTime").setText(getMissingHoursDisplay())); root.addContent(new Element("completedTime").setText(getHoursDisplay()));
     * root.addContent(new Element("completedSummary").setText(DateFormatter.getHoursAndMinutes(completedSummary))); root.addContent(new
     * Element("periodSummary").setText(DateFormatter.getHoursAndMinutes(periodSummary))); if (!records.isEmpty()) { if (records.size() == 1) {
     * TimeRecord obj = records.get(0); Element record = createRecord(obj); root.addContent(record); } else { Element items = new Element("records");
     * for (int i = 0, j = records.size(); i < j; i++) { TimeRecord next = records.get(i); items.addContent(createRecord(next)); }
     * root.addContent(items); } } else if (isPublicHoliday()) { root.addContent(createPublicHoliday()); } return root; }
     * 
     * private Element createPublicHoliday() { Element record = new Element("record"); record.addContent(new Element("startTime").setText(EMPTY_TIME));
     * record.addContent(new Element("stopTime").setText(EMPTY_TIME)); record.addContent(new Element("type").setText(TimeRecordType.PUBLIC_HOLIDAY.toString()));
     * record.addContent(new Element("name").setText(holiday.getName())); return record; }
     * 
     * private Element createRecord(TimeRecord obj) { Element record = new Element("record"); record.addContent(new
     * Element("startTime").setText(DateFormatter.getTime(obj.getValue()))); if (obj.getClosedBy() != null) { record.addContent(new
     * Element("stopTime").setText(DateFormatter.getTime(obj.getClosedBy().getValue()))); } else { record.addContent(new Element("stopTime")); }
     * record.addContent(new Element("type").setText(obj.getType().getId().toString())); if (obj.getType().isIgnoreNameOnPrint()) { record.addContent(new
     * Element("name")); } else { record.addContent(new Element("name").setText(obj.getType().getName())); } return record; }
     **/

    protected int getFirstBreakAfter() {
        return firstBreakAfter;
    }

    protected void setFirstBreakAfter(int firstBreakAfter) {
        this.firstBreakAfter = firstBreakAfter;
    }

    protected int getFirstBreakAfterMinutes() {
        return firstBreakAfterMinutes;
    }

    protected void setFirstBreakAfterMinutes(int firstBreakAfterMinutes) {
        this.firstBreakAfterMinutes = firstBreakAfterMinutes;
    }

    protected int getSecondBreakAfter() {
        return secondBreakAfter;
    }

    protected void setSecondBreakAfter(int secondBreakAfter) {
        this.secondBreakAfter = secondBreakAfter;
    }

    protected int getSecondBreakAfterMinutes() {
        return secondBreakAfterMinutes;
    }

    protected void setSecondBreakAfterMinutes(int secondBreakAfterMinutes) {
        this.secondBreakAfterMinutes = secondBreakAfterMinutes;
    }

    private void calculate() {
        this.minutesRequired = config.getDailyMinutes();
        if (getMonth() == 11 && (getDay() == 24 || getDay() == 31)) {
            Double minRequired = Double.valueOf(config.getDailyMinutes() / 2);
            this.minutesRequired = (int) Math.round(minRequired);
        }

        boolean openAvailable = false;

        for (int i = 0; i < this.records.size(); i++) {
            TimeRecord next = records.get(i);
            this.noBooking = false;
            if (next.getClosedBy() != null && !next.isWaitForApproval()) {
                if (next.getValue().equals(next.getClosedBy().getValue())) {
                    this.incorrectRecord = next.getValue();
                }

                int duration = next.getDuration().intValue();

                if (next.getType().isLeave()) {
                    this.minutesLeave = minutesLeave + duration;
                    this.minutesRequired = minutesRequired - duration;
                    if (next.getType().isVisible()) {
                        this.name = next.getType().getName();
                    }
                } else if (next.getType().isSubtraction()) {
                    this.minutesSubtraction = minutesSubtraction + duration;
                } else if (next.getType().isAddition()) {
                    this.minutesAddition = minutesAddition + duration;
                } else if (next.getType().isIllness()) {
                    this.minutesIllness = minutesIllness + duration;
                    this.minutesRequired = minutesRequired - duration;
                    if (next.getType().isVisible()) {
                        this.name = next.getType().getName();
                    }
                } else if (next.getType().isUnpaidLeave()) {
                    if (next.getType().isVisible()) {
                        this.name = next.getType().getName();
                    }
                } else if (next.getType().isSpecialLeave()) {
                    this.minutesRequired = minutesRequired - duration;
                    if (next.getType().isVisible()) {
                        this.name = next.getType().getName();
                    }
                } else {
                    this.minutesTotal = minutesTotal + duration;
                    if (next.getType().isVisible()) {
                        if (startTime == null || next.getValue().before(startTime)) {
                            this.startTime = next.getValue();
                        }
                        if (stopTime == null || next.getClosedBy().getValue().after(stopTime)) {
                            this.stopTime = next.getClosedBy().getValue();
                        }
                    }
                }
            } else if (next.isWaitForApproval()) {
                this.waitForApproval = true;
            } else {
                openAvailable = true;
                if (startTime == null || next.getValue().before(startTime)) {
                    if (next.getType().isVisible()) {
                        this.startTime = next.getValue();
                    }
                }
            }
        }

        if (!openAvailable) {
            this.calculateBreaks();
        } else {
            this.minutesNet = minutesTotal;
        }

        int markerMinutes = (int) Math.round(markerValue * 60);

        if (minutesNet > MAX_MINUTES + markerMinutes) {
            this.minutesLost = minutesNet - MAX_MINUTES - markerMinutes;
        }

        if ((holiday != null || isWeekend())) {
            this.noBooking = false;
            this.ignoreWorkingSummary = true;
            this.minutesRequired = 0;
        }

        this.minutesDifference = minutesNet - this.minutesRequired - this.minutesLost;
        if (!period.getConfig().isRecordingHours() && minutesNet == 0) {
            this.minutesDifference = 0;
        }
        /*
         * if (!isFuture() && log.isDebugEnabled()) { StringBuilder buffer = new StringBuilder("calculate() done [date="); buffer .append(getDate())
         * .append(", minutesRequired=").append(minutesRequired) .append(", minutesTotal=").append(minutesTotal)
         * .append(", minutesNet=").append(minutesNet) .append(", minutesBreak=").append(minutesBreak)
         * .append(", minutesLeave=").append(minutesLeave) .append(", minutesSubtraction=").append(minutesSubtraction)
         * .append(", minutesAddition=").append(minutesAddition) .append(", minutesIllness=").append(minutesIllness)
         * .append(", minutesDifference=").append(minutesDifference) .append(", minutesLost=").append(minutesLost)
         * .append(", ignoreSummary=").append(ignoreWorkingSummary) .append("]"); log.debug(buffer.toString()); }
         */
    }

    private void calculateBreaks() {

        int minutesNotIgnoreBreaks = 0;
        int minutesIgnoreBreaks = 0;

        Date workStart = null;
        Date workStop = null;
        for (int i = 0; i < this.records.size(); i++) {
            TimeRecord next = records.get(i);
            if (!next.getType().isAddition()
                    && !next.getType().isSubtraction()
                    && next.getClosedBy() != null) {
                if (next.getType().isIgnoreBreakRules()) {
                    minutesIgnoreBreaks += next.getDuration().intValue();
                } else {
                    minutesNotIgnoreBreaks += next.getDuration().intValue();
                }
                if (workStart == null || next.getValue().before(workStart)) {
                    workStart = next.getValue();
                }
                if (workStop == null || next.getClosedBy().getValue().after(workStop)) {
                    workStop = next.getClosedBy().getValue();
                }
            }
        }

        if (workStart != null && workStop != null) {
            int durationTotal = (int) DateUtil.getMinutes(Long.valueOf(workStop.getTime() - workStart.getTime()));

            this.minutesBreak = durationTotal - minutesIgnoreBreaks - minutesNotIgnoreBreaks;
            if (minutesNotIgnoreBreaks > this.firstBreakAfter) {
                int minRequiredBreakMinutes = firstBreakAfterMinutes;
                int requiredBreakMinutes = firstBreakAfterMinutes;
                int requiredBreakAfter = firstBreakAfter;
                if (minutesNotIgnoreBreaks > this.secondBreakAfter) {
                    requiredBreakAfter = secondBreakAfter;
                    requiredBreakMinutes = secondBreakAfterMinutes;
                }
                if (minutesBreak < requiredBreakMinutes) {
                    int missingBreaks = requiredBreakMinutes - this.minutesBreak;
                    if ((minutesNotIgnoreBreaks - missingBreaks) < requiredBreakAfter) {
                        this.minutesNet = requiredBreakAfter + minutesIgnoreBreaks;
                        this.minutesBreak = minutesNotIgnoreBreaks - requiredBreakAfter + minutesBreak;
                        if (minutesBreak < minRequiredBreakMinutes && minRequiredBreakMinutes < requiredBreakMinutes) {
                            missingBreaks = minRequiredBreakMinutes - this.minutesBreak;
                            this.minutesNet = minutesNet - missingBreaks;
                            this.minutesBreak = minRequiredBreakMinutes;
                        }
                    } else {
                        this.minutesNet = minutesTotal - missingBreaks;
                        this.minutesBreak = requiredBreakMinutes;
                    }
                } else {
                    this.minutesNet = minutesTotal;
                }
            } else {
                this.minutesNet = minutesTotal;
            }
        } else {
            this.minutesNet = minutesTotal;
        }
    }

    public boolean isWaitForApproval() {
        return waitForApproval;
    }
}
