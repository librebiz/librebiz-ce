/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.ds

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.NumberUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class ImportMapper {
    
    private static Logger logger = LoggerFactory.getLogger(ImportMapper.class.getName())
    
    abstract Object map(Map<String, String> values)

    /**
     * Provides a trimmed string by a string ignoring empty
     * @param value the string to trim
     * @return trimmed string or null if value is an empty string
     */
    protected String fetchString(String value) {
        value ? value.trim() : null
    }

    /**
     * Creates a long by a string from cell
     * @param cellValue
     * @return long value or null if none provided
     */
    protected Long createLong(String cellValue) {
        if (!cellValue) {
            return null
        }
        return NumberUtil.createLong(cellValue)
    }
    
    protected Long createLong(Long value) {
        // This method exists to provide a matching method instead of performing explicit type analysis.
        // (Values provided by map may be a string assigned by sheet or a long assigned by importer itself)
        value
    }
    
    /**
     * Creates an integer by a string from cell
     * @param cellValue
     * @return integer value or null if none provided
     */
    protected Integer createInteger(String cellValue) {
        if (!cellValue) {
            return null
        }
        return NumberUtil.createInteger(cellValue)
    }

    /**
     * Creates a double by a string from cell
     * @param cellValue
     * @return double value or null if none provided
     */
    protected Double createDouble(String cellValue) {
        if (!cellValue) {
            return null
        }
        return NumberUtil.createDouble(cellValue)
    }
    
    /**
     * Creates a double value by a string from cell
     * @param cellValue
     * @return double value or 0.0 if none provided
     */
    protected Double createDoubleValue(String cellValue) {
        Double result = createDouble(cellValue)
        if (result == null) {
            return 0.0
        }
        return result
    }

    protected boolean fetchBoolean(String value) {
        't' == value || 'true' == value || 'True' == value ||
            'yes' == value || 'Yes' == value || 'ja' == value || 'Ja' == value
    }

}
