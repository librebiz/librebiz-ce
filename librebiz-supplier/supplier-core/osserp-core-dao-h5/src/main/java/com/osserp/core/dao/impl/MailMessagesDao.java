/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.SessionFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.dao.Tables;
import com.osserp.common.mail.Attachment;
import com.osserp.common.mail.AttachmentImpl;
import com.osserp.common.mail.ReceivedMail;
import com.osserp.common.mail.ReceivedMailInfo;
import com.osserp.common.util.FileUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.dao.MailMessages;
import com.osserp.core.mail.IgnorableAttachment;
import com.osserp.core.mail.IgnorableHeader;
import com.osserp.core.mail.MailMessage;
import com.osserp.core.model.MailAttachment;
import com.osserp.core.model.MailMessageHeader;
import com.osserp.core.model.MailMessageImpl;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class MailMessagesDao extends AbstractDao implements MailMessages {
    private static Logger log = LoggerFactory.getLogger(MailMessagesDao.class.getName());

    private String mailMessagesTable;
    private String mailAttachmentIgnorableTable;
    private String mailAttachmentsTable;
    private String mailHeaderIgnorableTable;

    protected MailMessagesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        mailMessagesTable = getTable("mailMessages");
        mailAttachmentIgnorableTable = getTable("mailAttachmentIgnorable");
        mailAttachmentsTable = getTable("mailAttachments");
        mailHeaderIgnorableTable = getTable("mailHeaderIgnorable");
    }

    public boolean exists(String messageId) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(mailMessagesTable).append(" WHERE message_id = ?");
        final Object[] params = { messageId };
        final int[] types = { Types.VARCHAR };
        return jdbcTemplate.queryForObject(
                sql.toString(), params, types, Integer.class) > 0;
    }

    public List<ReceivedMailInfo> findByBusinessCase(Long userId, Long businessCaseId, Long customerId) {
        assert businessCaseId != null && customerId != null;
        StringBuilder sql = new StringBuilder(
                selectReceivedMessageInfoColumnsFromTable())
                    .append(getByBusinessCaseQuery())
                    .append(" ORDER BY date_received DESC");
        final Object[] params = { MailMessage.STATUS_IMPORTED,
                userId, businessCaseId, Contact.CUSTOMER, customerId };
        final int[] types = { Types.BIGINT,
                Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT };
        return (List<ReceivedMailInfo>) jdbcTemplate.query(
                sql.toString(), params, types, getReceivedMailInfoValuesReader());
    }

    public int getCountByBusinessCase(Long userId, Long businessCaseId, Long customerId) {
        assert businessCaseId != null && customerId != null;
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ")
                .append(mailMessagesTable)
                .append(getByBusinessCaseQuery());
        final Object[] params = { MailMessage.STATUS_IMPORTED,
                userId, businessCaseId, Contact.CUSTOMER, customerId };
        final int[] types = { Types.BIGINT,
                Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT };
        return jdbcTemplate.queryForObject(
                sql.toString(), params, types, Integer.class);
    }

    private String getByBusinessCaseQuery() {
        StringBuilder sql = new StringBuilder(" WHERE status >= ")
            .append(MailMessage.STATUS_NEW)
            .append(" AND status NOT IN (?) AND ")
            .append("(reference_id IS NULL AND business_id IS NULL AND")
            .append(" (user_id IS NULL OR user_id = ?) OR (business_id = ? OR")
            .append(" (business_id IS NULL AND reference_type = ? AND reference_id = ?)))");
        return sql.toString();
    }

    public List<ReceivedMailInfo> findByUser(Long userId) {
        assert userId != null;
        StringBuilder sql = new StringBuilder(
                selectReceivedMessageInfoColumnsFromTable())
                        .append(" WHERE user_id = ? AND status >= ")
                        .append(MailMessage.STATUS_NEW)
                        .append(" AND status NOT IN (?) ORDER BY date_received DESC");
        final Object[] params = { userId, MailMessage.STATUS_IMPORTED };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        return (List<ReceivedMailInfo>) jdbcTemplate.query(
                sql.toString(), params, types, getReceivedMailInfoValuesReader());
    }

    public int getCountByUser(Long userId) {
        assert userId != null;
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ")
                .append(mailMessagesTable)
                .append(" WHERE user_id = ? AND status >= ")
                .append(MailMessage.STATUS_NEW)
                .append(" AND status NOT IN (?)");
        final Object[] params = { userId, MailMessage.STATUS_IMPORTED };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        return jdbcTemplate.queryForObject(
                sql.toString(), params, types, Integer.class);
    }

    public List<ReceivedMailInfo> findByContact(Long classifiedContactId, Long contactGroupId) {
        assert classifiedContactId != null && contactGroupId != null;
        StringBuilder sql = new StringBuilder(
                selectReceivedMessageInfoColumnsFromTable())
                        .append(" WHERE reference_id = ? AND reference_type = ? AND status >= ")
                        .append(MailMessage.STATUS_NEW)
                        .append(" AND status NOT IN (?) ORDER BY date_received DESC");
        final Object[] params = { classifiedContactId, contactGroupId,
                MailMessage.STATUS_IMPORTED };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT };
        return (List<ReceivedMailInfo>) jdbcTemplate.query(
                sql.toString(), params, types, getReceivedMailInfoValuesReader());
    }

    public List<ReceivedMailInfo> findByStatus(Integer statusId) {
        assert statusId != null;
        StringBuilder sql = new StringBuilder(
                selectReceivedMessageInfoColumnsFromTable())
                        .append(" WHERE status = ")
                        .append(statusId)
                        .append(" ORDER BY date_received DESC");
        return (List<ReceivedMailInfo>) jdbcTemplate.query(
                sql.toString(), getReceivedMailInfoValuesReader());
    }

    public List<ReceivedMail> getByStatus(Integer statusId) {
        assert statusId != null;
        List<ReceivedMail> result = new ArrayList<>();
        StringBuilder hql = new StringBuilder(256);
        hql
                .append("from ")
                .append(MailMessageImpl.class.getName())
                .append(" o where o.status = :statusId order by o.dateReceived");
        List<MailMessageImpl> list = getCurrentSession().createQuery(hql.toString())
                .setParameter("statusId", statusId).list();
        if (!list.isEmpty()) {
            list.forEach( obj -> {
                result.add(loadReceivedMail(obj));
            });
        }
        return result;
    }

    public ReceivedMail getReceivedMail(String messageId) {
        return loadReceivedMail(load(messageId));
    }

    protected ReceivedMail loadReceivedMail(MailMessageImpl obj) {
        ReceivedMail result = null;
        if (obj != null) {
            result = new ReceivedMail(
                    obj.getId(),
                    obj.getReference(),
                    obj.getReferenceType(),
                    obj.getBusinessId(),
                    obj.getStatus(),
                    obj.getUserId(),
                    obj.getOriginator(),
                    obj.getRecipient(),
                    obj.getMessageId(),
                    obj.getDateSent(),
                    obj.getDateReceived(),
                    obj.getSubject(),
                    obj.getText(),
                    obj.getHtml());
            if (obj.getRecipients() != null && obj.getRecipients().length() > 0) {
                try {
                    result.addRecipientsCC(obj.getRecipients());
                } catch (Exception ignorable) {
                    // address received by mail should not be invalid
                }
            }
            Map<String, String> headers = new HashMap<>();
            if (!obj.getHeaders().isEmpty()) {
                for (MailMessageHeader header : obj.getHeaders()) {
                    headers.put(header.getName(), header.getValue());
                }
            }
            if (!obj.getAttachments().isEmpty()) {
                for (MailAttachment ma : obj.getAttachments()) {
                    if (!ma.isIgnorable()) {
                        result.addAttachment(
                                new AttachmentImpl(
                                        ma.getId(),
                                        ma.getReference(),
                                        ma.getContentID(),
                                        ma.getContentType(),
                                        ma.getFileName(),
                                        ma.getFileType(),
                                        ma.getFileStored(),
                                        ma.getFileSize()));
                    }
                }
            }
        }
        return result;
    }

    protected MailMessageImpl load(String messageId) {
        StringBuilder hql = new StringBuilder(256);
        hql
                .append("from ")
                .append(MailMessageImpl.class.getName())
                .append(" o where o.messageId = :messageId");
        List result = getCurrentSession().createQuery(hql.toString())
                .setParameter("messageId", messageId).list();
        MailMessageImpl obj = result.isEmpty() ? null : (MailMessageImpl) result.get(0);
        return obj;
    }

    public void save(ReceivedMail mail) {
        assert mail != null;
        if (!isEmpty(mail.getMessageId()) && !exists(mail.getMessageId())) {
            String recipientsCC = mail.getRecipientsCCList();
            MailMessageImpl obj = new MailMessageImpl(
                    true, // reveived
                    mail.getUserId(),
                    mail.getReference(),
                    mail.getReferenceType(),
                    mail.getBusinessId(),
                    mail.getOriginator(),
                    mail.getRecipient(),
                    (recipientsCC != null && recipientsCC.length() < 1) ? null : recipientsCC,
                    mail.getMessageId(),
                    mail.getSentDate(),
                    mail.getReceivedDate(),
                    mail.getSubject(),
                    mail.getText(),
                    mail.getHtml());
            getCurrentSession().save(MailMessageImpl.class.getName(), obj);
            saveHeaders(obj, mail);
            saveAttachments(obj, mail);
        }
    }

    public void remove(String messageId) {
        MailMessageImpl obj = load(messageId);
        if (!obj.getHeaders().isEmpty()) {
            for (Iterator<MailMessageHeader> i = obj.getHeaders().iterator(); i.hasNext();) {
                i.next();
                i.remove();
            }
            getCurrentSession().save(MailMessageImpl.class.getName(), obj);
        }
        if (!obj.getAttachments().isEmpty()) {
            for (Iterator<MailAttachment> i = obj.getAttachments().iterator(); i.hasNext();) {
                MailAttachment aobj = i.next();
                deleteStoredAttachment(aobj);
                i.remove();
            }
            getCurrentSession().save(MailMessageImpl.class.getName(), obj);
        }
        obj.setText(null);
        obj.setHtml(null);
        obj.setStatus(MailMessage.STATUS_REMOVED);
        getCurrentSession().save(MailMessageImpl.class.getName(), obj);
    }

    public boolean removed(String messageId) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql
                .append(mailMessagesTable)
                .append(" WHERE message_id = ? AND status = ?");
        final Object[] params = { messageId, MailMessage.STATUS_REMOVED };
        final int[] types = { Types.VARCHAR, Types.INTEGER };
        return jdbcTemplate.queryForObject(
                sql.toString(), params, types, Integer.class) > 0;
    }

    public void resetMoved(String messageId) {
        MailMessageImpl obj = load(messageId);
        obj.setStatus(MailMessage.STATUS_RESET);
        obj.setBusinessId(null);
        getCurrentSession().saveOrUpdate(MailMessageImpl.class.getName(), obj);
    }

    public void restore(ReceivedMail mail) {
        MailMessageImpl obj = load(mail.getMessageId());
        obj.setSubject(mail.getSubject());
        obj.setText(mail.getText());
        obj.setHtml(mail.getHtml());
        obj.setStatus(MailMessage.STATUS_RESTORED);
        getCurrentSession().saveOrUpdate(MailMessageImpl.class.getName(), obj);
    }

    public void setUnavailable(String messageId) {
        MailMessageImpl obj = load(messageId);
        if (obj != null) {
            obj.setStatus(MailMessage.STATUS_UNAVAILABLE);
            obj.setChanged(new Date(System.currentTimeMillis()));
            getCurrentSession().merge(MailMessageImpl.class.getName(), obj);
        }
    }

    private void saveAttachments(MailMessageImpl obj, ReceivedMail mail) {
        if (mail.isAttachmentSet()) {
            Map<String, IgnorableAttachment> ignorables = getIgnorableAttachmentFilenames();
            int orderId = 0;
            for (Attachment a : mail.getAttachments()) {
                long fileSize = a.getFileSize();
                if (fileSize <= 0) {
                    if (a.getFile() != null) {
                        fileSize = FileUtil.size(a.getFile());
                    }
                }
                boolean added = false;
                if (ignorables.containsKey(a.getFileName())) {
                    IgnorableAttachment ign = ignorables.get(a.getFileName());
                    if (ign.getFileSize() == fileSize) {
                        MailAttachment aobj = new MailAttachment(
                                obj.getId(),
                                a.getContentID(),
                                a.getContentType(),
                                a.getFileName(),
                                ign.getFileStore(),
                                ign.getFileType(),
                                ign.getFileSize(),
                                true,
                                orderId);
                        obj.getAttachments().add(aobj);
                        orderId++;
                        added = true;
                        if (FileUtil.exists(a.getFile())) {
                            try {
                                FileUtil.delete(a.getFile());
                            } catch (Exception e) {
                                log.warn("saveAttachments: Failed on attemt to delete ignorable attachment "
                                        + "[name=" + a.getFileName()
                                        + ", message=" + e.getMessage() + "]");
                            }
                        }
                    } else {
                        log.debug("saveAttachments: Attachment size does not match ignorable [name="
                                + a.getFileName() + ", size=" + a.getFileSize()
                                + ", ignorableSize=" + ign.getFileSize() + "]");
                    }
                }
                if (!added) {
                    MailAttachment aobj = new MailAttachment(
                            obj.getId(),
                            a.getContentID(),
                            a.getContentType(),
                            a.getFileName(),
                            a.getFile(),
                            a.getMimeType(),
                            fileSize,
                            false,
                            orderId);
                    obj.getAttachments().add(aobj);
                    orderId++;
                }
            }
            getCurrentSession().save(MailMessageImpl.class.getName(), obj);
        }
    }

    private void saveHeaders(MailMessageImpl obj, ReceivedMail mail) {
        if (!mail.getHeaders().isEmpty()) {
            Map<String, IgnorableHeader> ignorables = getIgnorableHeaderNames();
            int orderId = 0;
            for (Iterator<String> i = mail.getHeaders().keySet().iterator(); i.hasNext();) {
                String key = i.next();
                if (!ignorables.containsKey(key)) {
                    MailMessageHeader hobj = new MailMessageHeader(
                            obj.getId(),
                            key,
                            mail.getHeaders().get(key),
                            orderId);
                    obj.getHeaders().add(hobj);
                    getCurrentSession().save(MailMessageImpl.class.getName(), obj);
                    orderId++;
                }
            }
        }
    }

    public ReceivedMail setBusinessReference(ReceivedMail mail, Long reference, Long referenceType, Long businessId) {
        MailMessageImpl obj = load(mail.getMessageId());
        obj.setBusinessId(businessId);
        obj.setReference(reference);
        obj.setReferenceType(referenceType);
        getCurrentSession().save(MailMessageImpl.class.getName(), obj);
        return loadReceivedMail(load(mail.getMessageId()));
    }

    public ReceivedMail resetBusinessReference(ReceivedMail mail) {
        MailMessageImpl obj = load(mail.getMessageId());
        obj.setBusinessId(null);
        obj.setReference(null);
        obj.setReferenceType(null);
        getCurrentSession().save(MailMessageImpl.class.getName(), obj);
        return loadReceivedMail(load(mail.getMessageId()));
    }

    public ReceivedMail setUserReference(ReceivedMail mail, Long userId) {
        MailMessageImpl obj = load(mail.getMessageId());
        obj.setUserId(userId);
        getCurrentSession().save(MailMessageImpl.class.getName(), obj);
        return loadReceivedMail(load(mail.getMessageId()));
    }

    public ReceivedMail deleteAttachment(ReceivedMail mail, Long attachmentId) {
        MailMessageImpl obj = load(mail.getMessageId());
        return getReceivedMail(deleteAttachment(obj, attachmentId).getMessageId());
    }

    protected MailMessageImpl deleteAttachment(MailMessageImpl mail, Long attachmentId) {
        for (Iterator<MailAttachment> i = mail.getAttachments().iterator(); i.hasNext();) {
            MailAttachment aobj = i.next();
            if (aobj.getId().equals(attachmentId)) {
                deleteStoredAttachment(aobj);
                i.remove();
                getCurrentSession().save(MailMessageImpl.class.getName(), mail);
                break;
            }
        }
        return mail;
    }

    protected void deleteStoredAttachment(MailAttachment attachment) {
        if (attachment.getFileStored() != null && FileUtil.exists(attachment.getFileStored())) {
            try {
                FileUtil.delete(attachment.getFileStored());
            } catch (Exception e) {
                log.warn("Failed on attempt to delete attachment file: "
                        + attachment.getFileStored());
            }
        }
    }

    public Set<String> getExistingAttachments() {
        String sql = "SELECT file_stored FROM " + mailAttachmentsTable;
        Set<String> result = new HashSet<>(jdbcTemplate.queryForList(sql, String.class));
        sql = "SELECT file_stored FROM " + mailAttachmentIgnorableTable;
        result.addAll(new HashSet<>(jdbcTemplate.queryForList(sql, String.class)));
        return result;
    }

    public ReceivedMail ignoreAttachment(ReceivedMail mail, Long attachmentId) {
        MailMessageImpl obj = load(mail.getMessageId());
        for (Iterator<MailAttachment> i = obj.getAttachments().iterator(); i.hasNext();) {
            MailAttachment aobj = i.next();
            if (aobj.getId().equals(attachmentId) && createIgnorableAttachment(aobj)) {
                aobj.setIgnorable(true);
                getCurrentSession().save(MailMessageImpl.class.getName(), obj);
                break;
            }
        }
        return getReceivedMail(obj.getMessageId());
    }

    private boolean createIgnorableAttachment(MailAttachment aobj) {
        final Object[] params = new Object[] {
                aobj.getFileName(),
                aobj.getFileName(),
                aobj.getFileStored(),
                aobj.getFileType(),
                aobj.getFileSize() 
                };
        final int[] types = new int[] {
                Types.VARCHAR,
                Types.VARCHAR,
                Types.VARCHAR,
                Types.VARCHAR,
                Types.BIGINT
                };
        StringBuilder sql = new StringBuilder("INSERT INTO ")
                .append(mailAttachmentIgnorableTable)
                .append(" (name, file_name, file_stored, file_type, file_size)"
                        + " VALUES (?,?,?,?,?)");
        try {
            jdbcTemplate.update(sql.toString(), params, types);
            return true;
        } catch (Throwable t) {
            log.error("Failed attempt to insert ignorable attachment: "
                    + t.getMessage(), t);
            return false;
        }
    }

    public Map<String, IgnorableAttachment> getIgnorableAttachmentFilenames() {
        Map<String, IgnorableAttachment> result = new HashMap<>();
        List<IgnorableAttachment> ignorables = getIgnorableAttachments();
        for (IgnorableAttachment ignorable : ignorables) {
            result.put(ignorable.getFileName(), ignorable);
        }
        return result;
    }

    public List<IgnorableAttachment> getIgnorableAttachments() {
        return (List<IgnorableAttachment>) jdbcTemplate.query(
                selectIgnorableAttachmentColumnsFromTable(),
                getIgnorableAttachmentRowMapper());
    }

    protected RowMapperResultSetExtractor getIgnorableAttachmentRowMapper() {
        return new RowMapperResultSetExtractor(new IgnorableAttachmentRowMapper());
    }

    protected String selectIgnorableAttachmentColumnsFromTable() {
        return new StringBuilder("SELECT id, user_id, name, file_name, file_stored, file_type, file_size FROM ")
                .append(mailAttachmentIgnorableTable)
                .append(" ORDER BY file_name")
                .toString();
    }

    private class IgnorableAttachmentRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            IgnorableAttachment obj = new IgnorableAttachment(
                    Long.valueOf(rs.getLong("id")),
                    Long.valueOf(rs.getLong("user_id")),
                    rs.getString("name"),
                    rs.getString("file_name"),
                    rs.getString("file_stored"),
                    rs.getString("file_type"),
                    rs.getLong("file_size"));
            return obj;
        }
    }

    public Map<String, IgnorableHeader> getIgnorableHeaderNames() {
        Map<String, IgnorableHeader> result = new HashMap<>();
        List<IgnorableHeader> ignorables = getIgnorableHeaders();
        for (IgnorableHeader ignorable : ignorables) {
            result.put(ignorable.getName(), ignorable);
        }
        return result;
    }

    public List<IgnorableHeader> getIgnorableHeaders() {
        return (List<IgnorableHeader>) jdbcTemplate.query(
                selectIgnorableHeaderColumnsFromTable(),
                getIgnorableHeaderRowMapper());
    }

    protected RowMapperResultSetExtractor getIgnorableHeaderRowMapper() {
        return new RowMapperResultSetExtractor(new IgnorableHeaderRowMapper());
    }

    protected String selectIgnorableHeaderColumnsFromTable() {
        return new StringBuilder("SELECT id, user_id, name FROM ")
                .append(mailHeaderIgnorableTable)
                .append(" ORDER BY name")
                .toString();
    }

    private class IgnorableHeaderRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            IgnorableHeader obj = new IgnorableHeader(
                    Long.valueOf(rs.getLong("id")),
                    Long.valueOf(rs.getLong("user_id")),
                    rs.getString("name"));
            return obj;
        }
    }

    protected RowMapperResultSetExtractor getReceivedMailInfoValuesReader() {
        return new RowMapperResultSetExtractor(new ReceivedMailInfoRowMapper());
    }

    protected String selectReceivedMessageInfoColumnsFromTable() {
        return new StringBuilder("SELECT ")
                .append(receivedMessageInfoColumns())
                .append(" FROM ")
                .append(mailMessagesTable)
                .toString();
    }

    protected String receivedMessageInfoColumns() {
        return new StringBuilder("id,")
                .append(" reference_id,")
                .append(" reference_type,")
                .append(" business_id,")
                .append(" status,")
                .append(" user_id,")
                .append(" originator,")
                .append(" recipient,")
                .append(" recipients,")
                .append(" message_id,")
                .append(" date_sent,")
                .append(" date_received,")
                .append(" subject,")
                .append(" message")
                .toString();
    }

    private class ReceivedMailInfoRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            ReceivedMail obj = new ReceivedMail(
                    Long.valueOf(rs.getLong("id")),
                    Long.valueOf(rs.getLong("reference_id")),
                    Long.valueOf(rs.getLong("reference_type")),
                    Long.valueOf(rs.getLong("business_id")),
                    Integer.valueOf(rs.getInt("status")),
                    Long.valueOf(rs.getLong("user_id")),
                    rs.getString("originator"),
                    rs.getString("recipient"),
                    rs.getString("message_id"),
                    rs.getTimestamp("date_sent"),
                    rs.getTimestamp("date_received"),
                    rs.getString("subject"),
                    rs.getString("message"), // text message
                    (String) null); // html message
            String recipients = rs.getString("recipients");
            if (recipients != null && recipients.length() > 0) {
                try {
                    obj.addRecipientsCC(recipients);
                } catch (Exception ignorable) {
                }
            }
            return obj;
        }
    }
}
