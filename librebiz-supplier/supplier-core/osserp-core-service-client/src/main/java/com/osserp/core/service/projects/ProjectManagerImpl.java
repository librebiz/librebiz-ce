/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Feb-2007 14:33:31 
 * 
 */
package com.osserp.core.service.projects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.User;

import com.osserp.core.BusinessCase;
import com.osserp.core.FcsAction;
import com.osserp.core.dao.BusinessProperties;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.ResourceProvider;
import com.osserp.core.dao.SalesResetService;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.projects.Project;
import com.osserp.core.projects.ProjectFcsManager;
import com.osserp.core.projects.ProjectManager;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.service.impl.AbstractBusinessCaseManager;
import com.osserp.core.tasks.SalesMonitoringCacheSender;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectManagerImpl extends AbstractBusinessCaseManager implements ProjectManager {
    private static Logger log = LoggerFactory.getLogger(ProjectManagerImpl.class.getName());

    private Projects projects = null;
    private ProjectFcsManager projectFcsManager = null;
    private SalesInvoices invoices = null;
    private SalesOrders orders = null;
    private SalesPayments payments = null;
    private SalesResetService salesResetService = null;
    private SalesMonitoringCacheSender salesMonitoringCacheTask = null;

    /**
     * Creates a new projectManager
     * @param resourceProvider
     * @param requests
     * @param employees
     * @param projects
     * @param projectFcsManager
     * @param invoices
     * @param orders
     * @param payments
     * @param salesResetService
     * @param salesMonitoringCacheTask
     */
    public ProjectManagerImpl(
            ResourceProvider resourceProvider, 
            BusinessProperties businessProperties,
            Requests requests, 
            Employees employees,
            Projects projects,
            ProjectFcsManager projectFcsManager,
            SalesInvoices invoices,
            SalesOrders orders,
            SalesPayments payments,
            SalesResetService salesResetService,
            SalesMonitoringCacheSender salesMonitoringCacheTask) {

        super(resourceProvider, businessProperties, requests, employees);
        this.projects = projects;
        this.projectFcsManager = projectFcsManager;
        this.invoices = invoices;
        this.orders = orders;
        this.payments = payments;
        this.salesResetService = salesResetService;
        this.salesMonitoringCacheTask = salesMonitoringCacheTask;
    }

    public boolean exists(Long id) {
        return projects.exists(id);
    }

    public BusinessCase load(Long id) {
        return projects.load(id);
    }

    public void save(Sales sales) {
        projects.save(sales);
    }

    public Request resetRequestStatus(User user, Sales sales) throws ClientException {
        checkDelete(sales);
        Long requestId = salesResetService.reset(user, sales);
        return getRequests().load(requestId);
    }

    private void checkDelete(Sales sales) throws ClientException {
        Order order = orders.find(sales);
        if (order != null) {
            List<Payment> existingPayments = payments.getByOrder(order);
            if (!existingPayments.isEmpty()) {
                throw new ClientException(ErrorCode.PAYMENTS_FOUND_DELETE_DENIED);
            }
            List<Record> invoiceList = new ArrayList(invoices.getByReference(order.getId()));
            if (!invoiceList.isEmpty()) {
                throw new ClientException(ErrorCode.INVOICES_FOUND_DELETE_DENIED);
            }
            if (!order.getDeliveryNotes().isEmpty()) {
                throw new ClientException(ErrorCode.DELIVERIES_FOUND);
            }
        }
    }

    public Double getAccountBalance(Sales sales) {
        return projects.getAccountBalance(sales.getId());
    }

    public BusinessCase setManager(User user, BusinessCase businessCase, Long managerId) {
        if (user == null || !(businessCase instanceof Sales)) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        Sales sales = (Sales) businessCase;
        if (managerId == null || managerId == 0 || (sales.getManagerId() != null && sales.getManagerId().equals(managerId))) {
            sales.setManagerId(null);
        } else {
            sales.setManagerId(managerId);
        }
        sales.setChanged(new Date(System.currentTimeMillis()));
        sales.setChangedBy(user.getId());
        projects.save(sales);
        List<FcsAction> actions = projectFcsManager.createActionList(sales);
        for (int i = 0, j = actions.size(); i < j; i++) {
            FlowControlAction action = (FlowControlAction) actions.get(i);
            if (action.isManagerSelecting()) {
                try {
                    Employee userEmployee = (user instanceof DomainUser) ? ((DomainUser) user).getEmployee() : null;
                    if (userEmployee == null) {
                        userEmployee = (Employee) getEmployees().find(getEmployees().getEmployeeByUser(user.getId()));
                    }
                    projectFcsManager.addFlowControl(userEmployee, sales, action, new Date(), null, null, null, true);
                    sales = projects.load(sales.getId());
                } catch (Throwable t) {
                    log.error("setManager: Failed on attempt to add corresponding fcs action: " + t.toString(), t);
                }
            }
        }
        salesMonitoringCacheTask.send(sales.getType().getCompany());
        return load(sales.getId());
    }

    public void setManagerSubstitute(User user, Sales sales, Long managerId) {
        if (user == null || sales == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        sales.setManagerSubstituteId(managerId);
        sales.setChanged(new Date(System.currentTimeMillis()));
        sales.setChangedBy(user.getId());
        projects.save(sales);
    }

    public void setObserver(User user, Sales sales, Long observerId) {
        if (user == null || sales == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        sales.setObserverId(observerId);
        sales.setChanged(new Date(System.currentTimeMillis()));
        sales.setChangedBy(user.getId());
        projects.save(sales);
    }

    public void setCoSales(User user, Sales sales, Long salesId) {
        if (user == null || sales == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        sales.getRequest().setSalesCoId(salesId);
        sales.getRequest().setChanged(new Date(System.currentTimeMillis()));
        sales.getRequest().setChangedBy(user.getId());
        getRequests().save(sales.getRequest());
        salesMonitoringCacheTask.send(sales.getType().getCompany());
    }

    public BusinessCase setSales(User user, BusinessCase businessCase, Long salesId) {
        if (user == null || !(businessCase instanceof Sales)) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        Sales sales = (Sales) businessCase;
        Employee salesEmployee = getEmployees().get(salesId);
        sales.getRequest().updateSales(salesEmployee, salesEmployee.getBranch());
        sales.getRequest().setChanged(new Date(System.currentTimeMillis()));
        sales.getRequest().setChangedBy(user.getId());
        getRequests().save(sales.getRequest());
        if (sales.getType().isUpdateBranchOnSalesPersonChange()) {
            orders.changeBranch(sales);
        }
        salesMonitoringCacheTask.send(sales.getType().getCompany());
        return load(sales.getId());
    }

    public void setName(User user, Sales sales, String name)
            throws ClientException {
        if (user == null || sales == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);

        }
        sales.getRequest().setName(name);
        sales.getRequest().setChanged(new Date(System.currentTimeMillis()));
        sales.getRequest().setChangedBy(user.getId());
        getRequests().save(sales.getRequest());
        salesMonitoringCacheTask.send(sales.getType().getCompany());
    }

    public void changeWebsiteReferenceStatus(Sales sales) {
        sales.changeInternetExportStatus();
        save(sales);
    }

    public BusinessCase updateBranch(BusinessCase bc, Long id) {
        if (!(bc instanceof Sales)) {
            throw new IllegalStateException("business case must be instance of Sales: " + bc.getPrimaryKey());
        }
        Sales sales = (Sales) bc;
        Request request = sales.getRequest();
        getRequests().changeBranch(request, id);
        orders.changeBranch(sales);
        invoices.changePaymentRecordsBranch(sales);
        salesMonitoringCacheTask.send(sales.getType().getCompany());
        return load(bc.getPrimaryKey());
    }

    public void updateInstallationDate(Project project, Date installationDate, Integer installationDays) {
        project.updateInstallationDate(installationDate, installationDays);
        save(project);
        salesMonitoringCacheTask.send(project.getType().getCompany());
    }

    public List<Long> getStatusList() {
        return projects.getStatusList();
    }
}
