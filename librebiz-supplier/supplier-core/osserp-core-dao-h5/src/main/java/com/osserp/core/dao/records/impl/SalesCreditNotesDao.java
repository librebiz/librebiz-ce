/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 29, 2006 9:52:34 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.customers.Customer;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesCancellations;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.SalesCreditNoteImpl;
import com.osserp.core.model.records.SalesCreditNoteTypeImpl;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesCreditNote;
import com.osserp.core.sales.SalesCreditNoteType;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesCreditNotesDao extends AbstractSalesRecords implements SalesCreditNotes {

    private static final String DELETE_PERMISSION_DEFAULT = "executive_accounting,accounting,sales_credit_note_delete";

    private SalesCancellations cancellations = null;
    private String salesCreditNotesTable = null;

    public SalesCreditNotesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            SalesPayments payments,
            SalesCancellations cancellations) {
        super(jdbcTemplate, tables, sessionFactory, namesCache, taxRates,
                companies, infoConfigs, paymentConditions, recordNumberCreator,
                recordPrintOptionDefaults, recordMailLogs, payments);
        this.salesCreditNotesTable = getTable(TableKeys.SALES_CREDIT_NOTES);
        this.cancellations = cancellations;
        initializeDeletePermissions(DELETE_PERMISSION_DEFAULT);
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.SALES_CREDIT_NOTE;
    }

    public CreditNote create(Employee user, Invoice invoice,
            SalesCreditNoteType bookingType, boolean copyItems) {
        if (!(invoice instanceof SalesInvoice)) {
            throw new BackendException("expected SalesInvoice, found "
                    + (invoice == null ? "null" : invoice.getClass().getName()));
        }
        SalesInvoice si = (SalesInvoice) invoice;
        RecordType recordType = getRecordType();
        CreditNote result = si.addCreditNote(
                user,
                getNextRecordId(user, invoice.getCompany(), recordType, null),
                recordType,
                bookingType,
                copyItems);
        saveInitial(result);
        return result;
    }

    public CreditNote create(Employee user, BranchOffice office,
            Customer customer, Long userDefinedId, Date userDefinedDate,
            boolean historical) {
        RecordType recordType = getRecordType();
        Long creditId = userDefinedId != null ? userDefinedId : getNextRecordId(user, office.getCompany().getId(), recordType, userDefinedDate);
        SalesCreditNoteImpl obj = new SalesCreditNoteImpl(
                creditId,
                recordType,
                (SalesCreditNoteType) getDefaultBookingType(),
                office.getCompany().getId(),
                office.getId(),
                customer,
                user,
                null,
                taxRate,
                reducedTaxRate,
                Constants.DEFAULT_CURRENCY);
        if (userDefinedDate != null) {
            obj.setCreated(userDefinedDate);
        }
        if (historical) {
            obj.setItemsChangeable(true);
            obj.setItemsEditable(true);
            obj.updateStatus(Invoice.STAT_HISTORICAL);
        }
        saveInitial(obj);
        return obj;
    }

    public CreditNote create(Employee user, CreditNote creditNote, boolean copyReferenceId, Long userDefinedId, Date userDefinedDate, boolean historical) {
        assert (creditNote instanceof SalesCreditNote);

        RecordType recordType = getRecordType();
        Long creditId = userDefinedId != null ? userDefinedId : getNextRecordId(user, creditNote.getCompany(), recordType, null);
        SalesCreditNoteImpl obj = new SalesCreditNoteImpl(
                creditId,
                (SalesCreditNote) creditNote,
                user,
                copyReferenceId);

        if (userDefinedDate != null) {
            obj.setCreated(userDefinedDate);
        }
        if (historical) {
            obj.updateStatus(Invoice.STAT_HISTORICAL);
        }
        saveInitial(obj);
        return obj;
    }

    public Cancellation cancel(Employee user, CreditNote note, Date date, Long customId) throws ClientException {
        Cancellation result = cancellations.create(user, note, date, customId);
        note.cancel(user, result);
        save(note);
        return result;
    }

    public CreditNote resetCanceled(Employee user, Cancellation cancellation) {
        cancellations.delete(cancellation, user);
        SalesCreditNoteImpl creditNote = (SalesCreditNoteImpl) load(cancellation.getReference());
        creditNote.setCancellation(null);
        creditNote.updateStatus(Record.STAT_SENT);
        save(creditNote);
        return creditNote;
    }

    public void changeBranch(Sales sales) {
        if (sales != null && sales.getBranch() != null) {
            changeBranchBySales(
                    salesCreditNotesTable,
                    sales.getId(),
                    sales.getRequest().getBranch().getId(),
                    true);
        }
    }

    public List<Record> getByOrder(Record order) {
        List<Record> result = getCurrentSession().createQuery(
                "from " + SalesCreditNoteImpl.class.getName()
                        + " cn where cn.businessCaseId = :salesid")
                .setParameter("salesid", order.getBusinessCaseId()).list();
        return result;
    }

    public boolean exists(Long recordId) {
        return exists(salesCreditNotesTable, recordId);
    }

    public int count(Long referenceId) {
        return count(salesCreditNotesTable, referenceId);
    }

    public int countBySales(Long salesId) {
        return countBySales(salesCreditNotesTable, salesId);
    }

    public List<Long> getPrinted() {
        return getPrinted(salesCreditNotesTable);
    }

    public Date getCreated(Long id) {
        return getCreated(salesCreditNotesTable, id);
    }

    @Override
    protected void loadAfterAll(Record record) {
        SalesCreditNoteImpl obj = (SalesCreditNoteImpl) record;
        if (obj.isCanceled()) {
            Cancellation c = cancellations.loadCancellation(obj);
            obj.setCancellation(c);
        }
    }

    @Override
    protected Class<SalesCreditNoteImpl> getPersistentClass() {
        return SalesCreditNoteImpl.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ()
     */
    @Override
    protected Long getDefaultBookingTypeId() {
        return SalesCreditNoteType.CUSTOMER_CREDIT;
    }

    /*
     * (non-Javadoc)
     * 
     * @seecom.osserp.core.dao.records.impl.AbstractRecords# getPersistentSubTypeClass()
     */
    @Override
    protected Class<SalesCreditNoteTypeImpl> getPersistentBookingTypeClass() {
        return SalesCreditNoteTypeImpl.class;
    }

    @Override
    protected void loadPayments(PaymentAwareRecord record) {
        if (isSupportingPayments()) {
            SalesCreditNoteImpl note = (SalesCreditNoteImpl) record;
            List<Payment> list = getPayments().getByRecord(note);
            for (int i = 0, j = list.size(); i < j; i++) {
                Payment next = list.get(i);
                note.getPayments().add(next);
            }
        }
    }
}
