/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 7, 2008 10:11:38 PM 
 * 
 */
package com.osserp.common.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.osserp.common.CalendarMonth;
import com.osserp.common.Year;
import com.osserp.common.util.DateUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractYear extends AbstractClass implements Year, Serializable {
    private Integer day;
    private Integer month;
    private Integer year;

    /**
     * Creates a new calendar year initialized with current year values
     */
    protected AbstractYear() {
        super();
        Integer[] date = DateUtil.getTime(DateUtil.getCurrentDate());
        setDate(1, date[1], date[2]);
    }

    /**
     * Creates a new year, internal date is set as 01.01 of year
     * @param year
     */
    protected AbstractYear(int year) {
        setDate(1, 0, year);
    }

    /**
     * Creates a new year by date
     * @param date
     */
    protected AbstractYear(Date date) {
        super();
        Integer[] time = DateUtil.getTime(date);
        setDate(1, time[1], time[2]);
    }
    
    public final String getYearDisplay() {
        return (year == null) ? null : year.toString();
    }
    
    public final String getShortYearDisplay() {
        String ys = getYearDisplay();
        return (ys == null) ? null : ys.substring(2, 4);
    }

    public Integer getYear() {
        return year;
    }

    public Long getYearAsLong() {
        return year == null ? 1970L : Long.valueOf(year.intValue());
    }

    public boolean isAfter(Year obj) {
        if (isSame(obj) || year < obj.getYear()) {
            return false;
        }
        return true;
    }

    public boolean isBefore(Year obj) {
        if (isSame(obj) || year > obj.getYear()) {
            return false;
        }
        return true;
    }

    public boolean isSame(Year obj) {
        return year.equals(obj.getYear());
    }

    public Date getDate() {
        Calendar calendar = new GregorianCalendar(getYear(), getMonth(), getDay());
        return calendar.getTime();
    }

    protected List<CalendarMonth> createMonths() {
        List<CalendarMonth> result = new ArrayList<CalendarMonth>();
        for (int i = 0; i < 12; i++) {
            CalendarMonth mon = new CalendarMonthImpl(i, year);
            result.add(mon);
        }
        return result;
    }

    // setters are protected

    protected Integer getDay() {
        return day;
    }

    protected void setDay(int day) {
        this.day = day;
    }

    protected Integer getMonth() {
        return month;
    }

    protected void setMonth(int month) {
        this.month = month;
    }

    protected Long getMonthAsLong() {
        return month == null ? 0L : Long.valueOf(month.intValue());
    }

    protected void setYear(int year) {
        this.year = year;
    }

    protected void setDate(Integer day, Integer month, Integer year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
}
