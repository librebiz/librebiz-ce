/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 13, 2008 4:48:45 PM 
 * 
 */
package com.osserp.common.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionTarget;
import com.osserp.common.Constants;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.UserPermission;
import com.osserp.common.util.SecurityUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractUser extends AbstractPropertyAware implements User {
    private static Logger log = LoggerFactory.getLogger(AbstractUser.class.getName());
    private Long contactId = null;
    private boolean userEmployee = false;
    private String loginName = null;
    private String ldapUid = null;
    private Long ldapNumber = null;
    private String password = null;
    private boolean active = false;

    private String remoteIp = null;
    private String sessionId = null;
    private Date loginTime;
    private Date lastLoginTime;
    private String lastLoginIp;

    private String localeLanguage = null;
    private String localeCountry = null;
    private ActionTarget startup = null;
    private boolean guestFlagInitialized = false;
    private boolean guest = false;
    private boolean passwordResetRequired = false;

    private transient Map<String, UserPermission> permissions;
    private List<UserPermission> permissionList;

    private Long companyId = null;
    private Long customerId = null;

    /**
     * Default constructor. Needed to implement the serializable interface
     */
    protected AbstractUser() {
        super();
        permissions = new HashMap<>();
        permissionList = new ArrayList<>();
    }

    /**
     * Creates a new user and initializes the password with a 10-digit random string
     * to prevent passwordless logins. 
     * The password is clear text and should be replaced asap.  
     * @param id
     * @param createdBy
     * @param contactId
     * @param loginName
     * @param startup
     */
    protected AbstractUser(
            Long id,
            Long createdBy,
            Long contactId,
            String loginName,
            ActionTarget startup) {
        super(id, createdBy);
        permissions = new HashMap<>();
        permissionList = new ArrayList<>();
        this.contactId = contactId;
        this.loginName = loginName;
        this.password = SecurityUtil.createString(10, true);
        this.startup = startup;
    }

    /**
     * Constructor for derived classes
     * @param user
     */
    protected AbstractUser(AbstractUser user) {
        super(user);
        permissions = new HashMap<>();
        contactId = user.getContactId();
        userEmployee = user.isUserEmployee();
        loginName = user.getLoginName();
        password = user.getPassword();
        active = user.isActive();
        localeLanguage = (user.getLocaleLanguage() != null) ? user.getLocaleLanguage() : Constants.DEFAULT_LANGUAGE;
        localeCountry = (user.getLocaleCountry() != null) ? user.getLocaleCountry() : Constants.DEFAULT_COUNTRY;
        remoteIp = user.getRemoteIp();

        if (user.getLoginTime() == null) {
            loginTime = new Date(System.currentTimeMillis());
        } else {
            loginTime = user.getLoginTime();
        }

        if (user.getLastLoginTime() == null) {
            lastLoginTime = new Date(System.currentTimeMillis());
        } else {
            lastLoginTime = user.getLastLoginTime();
        }

        if (user.getLastLoginIp() != null) {
            lastLoginIp = user.getLastLoginIp();
        }

        List<UserPermission> permlist = user.permissionList;
        permissionList = new ArrayList<>();
        for (int i = 0, j = permlist.size(); i < j; i++) {
            permissionList.add((UserPermission) permlist.get(i).clone());
        }
        ldapUid = user.getLdapUid();
        ldapNumber = user.getLdapNumber();
        startup = user.getStartup();
    }

    public boolean isAdmin() {
        return false;
    }

    public boolean isSetupAdmin() {
        return false;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    protected void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    protected void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getLdapUid() {
        return ldapUid;
    }

    public void setLdapUid(String ldapUid) {
        this.ldapUid = ldapUid;
    }

    public Long getLdapNumber() {
        return ldapNumber;
    }

    public void setLdapNumber(Long ldapNumber) {
        this.ldapNumber = ldapNumber;
    }

    public String getEmail() {
        return null;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, UserPermission> getPermissions() {
        if (permissions == null) {
            permissions = new HashMap<>();
        }
        if (permissionList == null) {
            permissionList = new ArrayList<>();
        }
        if (permissions.isEmpty() && !permissionList.isEmpty()) {
            for (int i = 0, j = permissionList.size(); i < j; i++) {
                UserPermission next = permissionList.get(i);
                if (next.getPermission() != null) {
                    permissions.put(next.getPermission(), next);
                }
            }
        }
        return permissions;
    }

    public void setPermissions(Map<String, UserPermission> permissions) {
        this.permissions = permissions;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean isDomainEditor() {
        return false;
    }

    public boolean isDomainUser() {
        return false;
    }

    public boolean isGuest() {
        if (!guestFlagInitialized) {
            guest = getPermissions().containsKey("guest");
            guestFlagInitialized = true;
        }
        return guest;
    }

    protected void setGuest(boolean guest) {
        this.guest = guest;
        this.guestFlagInitialized = true;
    }

    public boolean isUserEmployee() {
        return userEmployee;
    }

    public void setUserEmployee(boolean userEmployee) {
        this.userEmployee = userEmployee;
    }

    public boolean isFromHeadquarter() {
        return false;
    }

    public void changeLocale(String localeString) {
        if (localeString != null && localeString.length() == 5) {
            setLocaleCountry(localeString.substring(3));
            setLocaleLanguage(localeString.substring(0, 2));
        }
    }

    public Locale getLocale() {
        if (localeLanguage != null && localeCountry != null) {
            return new Locale(localeLanguage, localeCountry);
        }
        return Constants.DEFAULT_LOCALE_OBJECT;
    }

    public String getLocaleDisplay() {
        Locale locale = getLocale();
        return locale.toString();
    }

    public ActionTarget getStartup() {
        return startup;
    }

    public void setStartup(ActionTarget startup) {
        this.startup = startup;
    }

    public void addPermission(Long user, String permission) {
        Map<String, UserPermission> perms = getPermissions();
        if (!perms.containsKey(permission)) {
            UserPermission upi = createPermission(user, permission);
            permissionList.add(upi);
            permissions.put(upi.getPermission(), upi);
        }
    }

    protected abstract UserPermission createPermission(Long grantBy, String permission);

    public void removePermission(String permission) {
        if (log.isDebugEnabled()) {
            log.debug("removePermission() invoked"
                    + " to revoke permission " + permission
                    + " on user " + getId());
        }
        for (Iterator<UserPermission> i = permissionList.iterator(); i.hasNext();) {
            UserPermission next = i.next();
            if (next.getPermission().equals(permission)) {
                i.remove();
                getPermissions().remove(permission);
            }
        }
    }

    public void checkPermission(String[] required) throws PermissionException {
        if (!isPermissionGrant(required)) {
            throw new PermissionException();
        }
    }

    public boolean isPermissionGrant(String[] required) {
        Map<String, UserPermission> perms = getPermissions();
        if (required != null && required.length > 0) {
            List<String> list = new ArrayList<>();
            // check if denied by dedicated permission
            for (int i = 0, j = required.length; i < j; i++) {
                String perm = required[i];
                if (perm.startsWith("deny_") || perm.endsWith("_deny")) { 
                    if (perms.containsKey(perm)) {
                        return false;
                    }
                } else {
                    list.add(perm);
                }
            }
            if (list.isEmpty()) {
                // dedicated deny permissions only
                return true;
            }
            // nothing denied, check 
            for (int i = 0, j = list.size(); i < j; i++) {
                if (perms.containsKey(list.get(i))) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public boolean isPermissionAssigned(String permission) {
        Map<String, UserPermission> perms = getPermissions();
        if (perms.containsKey(permission)) {
            return true;
        }
        return false;
    }

    protected List<UserPermission> getPermissionList() {
        return permissionList;
    }

    protected void setPermissionList(List<UserPermission> permissionList) {
        this.permissionList = permissionList;
    }

    public String getLocaleLanguage() {
        return localeLanguage;
    }

    public void setLocaleLanguage(String localeLanguage) {
        this.localeLanguage = localeLanguage;
    }

    public String getLocaleCountry() {
        return localeCountry;
    }

    public void setLocaleCountry(String localeCountry) {
        this.localeCountry = localeCountry;
    }

    public boolean isPasswordResetRequired() {
        return passwordResetRequired;
    }

    public void setPasswordResetRequired(boolean passwordResetRequired) {
        this.passwordResetRequired = passwordResetRequired;
    }

    public boolean isHidden() {
        return false;
    }

    @Override
    public abstract Object clone();
}
