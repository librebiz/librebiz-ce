<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area panel-area-default">
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><fmt:message key="receivedLabel" /></th>
                        <th><fmt:message key="incident" /></th>
                        <th><fmt:message key="mailSubject" /></th>
                        <th><fmt:message key="originator" /></th>
                        <th><fmt:message key="recipient" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="obj" items="${view.list}" varStatus="s">
                        <c:if test="${empty obj.businessId or obj.businessId == view.businessCase.primaryKey}">
                            <tr>
                                <td>
                                    <v:link url="${view.baseLink}/select?id=${obj.id}" title="displayEmail">
                                        <o:date value="${obj.receivedDate}" addcentury="false" addtime="true" />
                                    </v:link>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${!empty obj.businessId}">
                                            <o:out value="${obj.businessId}" />
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="unknown" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td><o:out value="${obj.subject}" /></td>
                                <td><o:out value="${obj.originator}" /></td>
                                <td><o:out value="${obj.recipient}" /></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
