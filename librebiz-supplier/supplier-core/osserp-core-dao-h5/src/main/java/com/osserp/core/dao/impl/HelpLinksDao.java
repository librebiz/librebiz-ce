/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 12, 2007 11:32:26 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.gui.HelpLink;

import com.osserp.core.dao.HelpLinks;
import com.osserp.core.model.HelpLinkImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class HelpLinksDao extends AbstractDao implements HelpLinks {
    private static Logger log = LoggerFactory.getLogger(HelpLinksDao.class.getName());
    private String linkTable = null;

    public HelpLinksDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            String linkTableKey) {
        super(jdbcTemplate, tables, sessionFactory);
        this.linkTable = getTable(linkTableKey);
    }

    public List<HelpLink> getLinks() {
        return getCurrentSession().createQuery(
                "from " + HelpLinkImpl.class.getName() 
                + " o order by o.description").list();
    }

    public HelpLink getLinkByName(String name) {
        List<HelpLink> links = getCurrentSession().createQuery(
                "from " + HelpLinkImpl.class.getName() + " o where o.name = :linkName")
                .setParameter("linkName", name).list();
        if (links.isEmpty()) {
            return null;
        }
        return links.get(0);
    }

    public HelpLink addLink(String name) {
        HelpLinkImpl link = new HelpLinkImpl(name);
        save(link);
        if (log.isDebugEnabled()) {
            log.debug("addLink() link [" + link.getId()
                    + "] for page [" + link.getName() + "] added");
        }
        return link;
    }

    public void save(HelpLink link) {
        if (link != null) {
            getCurrentSession().saveOrUpdate("HelpLinkImpl", link);
        }
    }

    public boolean existsUnderAnotherId(Long existingId, String name) {
        StringBuilder query = new StringBuilder("SELECT count(*) FROM ");
        query
                .append(linkTable)
                .append(" WHERE name = ? AND id <> ?");
        int i = jdbcTemplate.queryForObject(
                query.toString(),
                new Object[] { name, existingId },
                new int[] { Types.VARCHAR, Types.BIGINT }, 
                Integer.class);
        return (i > 0);
    }

    protected String getLinkTable() {
        return linkTable;
    }
}
