/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.common.FileObject
import com.osserp.common.ClientException
import com.osserp.common.util.DateFormatter
import com.osserp.common.util.DateUtil
import com.osserp.common.util.NumberFormatter
import com.osserp.common.util.NumberUtil
import com.osserp.common.web.AbstractForm
import com.osserp.common.web.Form

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class WebForm extends AbstractForm implements Form {
    private static Logger logger = LoggerFactory.getLogger(WebForm.class.getName())

    Map params = [:]
    List uploads = []

    WebForm() {
        super()
    }

    WebForm(Map params) {
        super()
        this.params = params
        if (params.uploads) {
            uploads = params.uploads
            params.remove('uploads')
        }
        if (logger.debugEnabled) {
            params.each { key, value ->
                if (key != 'x' && key != 'y') {
                    if (key.indexOf('assword') < 0) {
                        logger.debug("<init>: added param [name=${key}, value=${value}]")
                    } else {
                        logger.debug("<init>: added param [name=${key}, value=hidden]")
                    }
                }
            }
        }
    }

    private String logParams(Map pmap) {
        StringBuilder buffer = new StringBuilder()
        pmap.each { key, value ->
            if (buffer.length() > 0) {
                buffer.append(', ')
            }
            if (key.indexOf('assword') < 0) {
                buffer.append(key).append('=').append(value)
            } else {
                buffer.append(key).append('=').append('hidden')
            }
        }
        buffer.toString()
    }

    Object getForm() {
        params
    }

    FileObject getUpload() {
        (!uploads) ? null : uploads.get(0)
    }

    boolean getBoolean(String key) {
        getBoolean(key, false)
    }

    boolean getBoolean(String key, Boolean defaultValue) {
        try {
            String bool = getString(key)
            if (!bool) {
                return defaultValue
            }
            if (bool.equalsIgnoreCase("on") || bool.equalsIgnoreCase("checked")) {
                return true
            }
            return Boolean.valueOf(bool)
        } catch (Exception ignore) {
            if (logger.debugEnabled) logger.debug("getBoolean: invalid value [key=$key]")
            return false
        }
    }

    Date getDate(String key) throws ClientException {
        DateUtil.createDate(getString(key), false)
    }

    BigDecimal getDecimal(String key) {
        try {
            return NumberFormatter.createDecimal(getString(key), false)
        } catch (ClientException e) {
            return new BigDecimal(0)
        }
    }

    Double getDouble(String key) {
        try {
            return NumberFormatter.createDouble(getString(key), false)
        } catch (ClientException e) {
            if (logger.debugEnabled) logger.debug("getDouble: invalid value [key=$key]")
            return 0d
        }
    }

    String getDoubleAsString(String key) throws ClientException {
        Double value = getDouble(key)
        if (value == null) {
            return null
        }
        value.toString()
    }

    Double[] getIndexedDouble(String key) throws ClientException {
        String[] sarray = getIndexedStrings(key)
        if (!sarray) {
            return null
        }
        int l = sarray.length
        Double[] res = new Double[l]
        for (int i = 0; i < l; i++) {
            res[i] = NumberFormatter.createDouble(sarray[i], true)
        }
        return res
    }

    Integer[] getIndexedIntegers(String key) {
        try {
            Integer[] result
            String[] values = getIndexedStrings(key)
            int ln = (!values ? 0 : values.length)
            if (ln > 0) {
                result = new Integer[ln]
                for (int i = 0; i < ln; i++) {
                    result[i] = NumberUtil.createInteger(values[i])
                }
            }
            return result
        } catch (Exception e) {
            if (logger.debugEnabled) logger.debug("getIndexedIntegers: invalid value [key=$key]")
            return null
        }
    }

    Long[] getIndexedLong(String key) {
        try {
            Long[] result
            String[] values = getIndexedStrings(key)
            int ln = (!values ? 0 : values.length)
            if (ln > 0) {
                result = new Long[ln]
                for (int i = 0; i < ln; i++) {
                    result[i] = NumberUtil.createLong(values[i])
                }
            }
            return result
        } catch (Exception e) {
            if (logger.debugEnabled) logger.debug("getIndexedLong: invalid value [key=$key]")
            return null
        }
    }

    String[] getIndexedStrings(String key) {
        try {
            Object obj = params[key]
            if (obj instanceof String) {
                return (String[]) [(String) obj]
            }
            return (String[]) params[key]
        } catch (Throwable e) {
            if (logger.debugEnabled) logger.debug("getIndexedStrings: invalid value [key=$key]")
            return null
        }
    }

    int getIntVal(String key) throws ClientException {
        try {
            return getInteger(key).intValue()
        } catch (Throwable ex) {
            if (logger.debugEnabled) logger.debug("getIntVal: invalid value [key=$key]")
            throw new ClientException("int.expected")
        }
    }

    Integer getInteger(String key) {
        try {
            return NumberUtil.createInteger(getString(key))
        } catch (Throwable ignore) {
            if (logger.debugEnabled) logger.debug("getInteger: invalid value [key=$key]")
            return null
        }
    }

    Long getLong(String key) {
        try {
            return NumberUtil.createLong(getString(key))
        } catch (Throwable ignore) {
            if (logger.debugEnabled) logger.debug("getLong: invalid value [key=$key, message=${ignore.toString()}]")
            return null
        }
    }

    long getLongVal(String key) {
        Long l = getLong(key)
        return (l == null) ? -1 : l.longValue()
    }
    
    Long getSelection(String key) {
        Long l = getLong(key)
        return l == null || l == 0 ? null : l
    }

    Double getPercentage(String key) {
        getPercentage(key, false)
    }

    Double getPercentage(String key, boolean required) throws ClientException {
        try {
            Double result
            String s = getString(key, required)
            if (s) {
                BigDecimal bd = new BigDecimal(s).divide(new BigDecimal("100"))
                result = Double.valueOf(bd.toPlainString())
            }
            return result
        } catch (NumberFormatException nfe) {
            if (required) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
            }
            return 0
        }
    }

    String getString(String key) {
        String s = params[key]
        if (!s || s.length() < 1) {
            return null
        }
        return s
    }

    String getString(String key, boolean required) throws ClientException {
        String s = params[key]
        if (!s || s.length() < 1) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }
        return s
    }

    Map getMapped() {
        return params
    }

    Object getValue(String key) {
        params[key]
    }
    
    void setValue(String key, Object value) {
        if (value instanceof Double || value instanceof BigDecimal) {
            params[key] = NumberFormatter.getValue(value, NumberFormatter.CURRENCY)
        } else if (value instanceof Date) {
            params[key] = DateFormatter.getDate(value)
        } else {
            params[key] = value
        }
    }
    
    void resetValue(String key) {
        params.remove(key)
    }
}
