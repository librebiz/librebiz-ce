/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 6, 2008 9:12:19 AM 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.finance.Records;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.SerialNumberDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SerialNumberDisplayVO extends AbstractOption
        implements SerialNumberDisplay {

    private boolean sales = false;
    private Long productId = null;
    private String serialNumber = null;
    private RecordType recordType = null;

    protected SerialNumberDisplayVO() {
        super();
    }

    public SerialNumberDisplayVO(
            Long id,
            Long reference,
            Date created,
            Long createdBy,
            boolean sales,
            Long productId,
            String productName,
            String serialNumber,
            RecordType recordType) {
        super(id, reference, productName, created, createdBy);
        this.sales = sales;
        this.productId = productId;
        this.serialNumber = serialNumber;
        this.recordType = recordType;
    }

    public String getRecordNumber() {
        return Records.createNumber(recordType, getReference(), false);
    }

    public RecordType getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    public boolean isSales() {
        return sales;
    }

    public void setSales(boolean sales) {
        this.sales = sales;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
