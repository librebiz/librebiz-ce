<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<script type="text/javascript">
var x = 100;
var y = 100;
var vis = "false";
var ignoreTime;

/**
 * (de)activates permanent visibility of AjaxPopupDiv (when clicking on a term)
 */
function chVis() {
    if(vis == "false") { vis = "true"; }
    else {
        vis = "false";
        hideTerm();
    }
}

/**
 * Makes div visible
 * @param day
 * @param time
 * @param title
 * @param headline
 * @param text
 */
function displayTerm(day, time, title, headline, text) {
    if(vis == "false") {
        var tmp = day;
        if(ignoreTime == "false") {
            tmp += "., " + time + " Uhr: " + title;
        } else {
            tmp += ".:" + title;
        }

        document.getElementById("ajaxPopupHeader").innerHTML = tmp;
        var txt = "<div style='padding-left:5px;'>" + headline + "</div>";
        if (text) {
            txt += "<br/><div style='padding-left:5px;'>" + text + "</div>"
        }
        document.getElementById("ajaxPopupContent").innerHTML = txt;
        document.getElementById("ajaxPopupDiv").style.display = "block";

        document.getElementById("ajaxPopupDiv").style.left = x + "px";
        document.getElementById("ajaxPopupDiv").style.top = y + "px";
    }
}

//makes div invisible
function hideTerm() {
    if(vis == "false") {
        document.getElementById("ajaxPopupHeader").innerHTML = "";
        document.getElementById("ajaxPopupContent").innerHTML = "";
        document.getElementById("ajaxPopupDiv").style.display = "none";
    }
}

//makes AjaxPopupDiv follow the mouse pointer
function moveDiv (Ereignis) {
    x = Ereignis.pageX + 160;
    y = Ereignis.pageY + 10;

    if((x+160) > window.innerWidth) { x -= 330; }

    if((y+150) > window.innerHeight) {
        y -= 180;
    }
}

document.onmousemove = moveDiv;

</script>
