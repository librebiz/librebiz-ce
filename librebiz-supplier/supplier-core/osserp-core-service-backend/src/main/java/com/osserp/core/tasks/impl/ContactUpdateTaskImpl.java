/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 3, 2010 12:39:20 PM 
 * 
 */
package com.osserp.core.tasks.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.OptionsCache;
import com.osserp.common.service.impl.AbstractXmlMessageHandler;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.PrivateContacts;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.service.contacts.ContactSynchronisationService;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.tasks.Task;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactUpdateTaskImpl extends AbstractXmlMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(ContactUpdateTaskImpl.class.getName());
    private ContactSynchronisationService contactSynchronisationService = null;
    private Contacts contacts = null;
    private PrivateContacts privateContacts = null;
    private Suppliers suppliers = null;
    private OptionsCache optionsCache = null;

    // TODO LDAP revise contact synchronization
    protected ContactUpdateTaskImpl(
            ContactSynchronisationService contactSynchronisationService,
            Contacts contacts,
            PrivateContacts privateContacts,
            Suppliers suppliers,
            OptionsCache optionsCache) {
        super();
        this.contactSynchronisationService = contactSynchronisationService;
        this.contacts = contacts;
        this.privateContacts = privateContacts;
        this.suppliers = suppliers;
        this.optionsCache = optionsCache;
    }

    @Override
    protected void execute(Document doc, byte[] file) {
        Element root = doc.getRootElement();
        String type = root.getChildText("type");
        Long id = NumberUtil.createLong(root.getChildText("id"));

        if (id == null) {
            log.warn("execute() invoked with missing contact id [xml=" + JDOMUtil.getPrettyString(doc)
                + ", root=" + JDOMUtil.getPrettyString(root) + ", text=" + root.getText() + "]");
            return;
        }
        if (ContactSynchronisationService.PRIVATE_CONTACT.equals(type)) {
            PrivateContact contact = privateContacts.getPrivateContact(id);
            if (contact == null) {
                log.warn("execute() invoked with invalid id [privateContact=" + id + "]");
            } else {
                contactSynchronisationService.sync(contact);
            }

        } else if (ContactSynchronisationService.CONTACT.equalsIgnoreCase(type)) {
            Contact contact = contacts.loadContact(id);
            if (contact == null) {
                log.warn("execute() invoked with invalid id [contact=" + id + "]");
                return;
            }
            if (contact.isSupplier()) {
                Supplier supplier = (Supplier) suppliers.findByContact(contact.getContactId());
                if (supplier == null) {
                    log.warn("execute() contact declared as supplier but supplier not exists [contact=" + id + "]");
                } else if (supplier.getSupplierType().isShipping()) {
                    optionsCache.refresh(Options.SHIPPING_COMPANIES);
                    optionsCache.refresh(Options.SHIPPING_COMPANY_KEYS);
                }
            }
            contactSynchronisationService.sync(contact);
        }
    }
}
