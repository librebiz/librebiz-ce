SET search_path = osserp, pg_catalog;

UPDATE documents SET file_size = 35946 WHERE file_name = 'xsl/records/xsl_rtp_4.vm';
UPDATE documents SET file_size = 39659 WHERE file_name = 'xsl/records/xsl_rtp_5.vm';
INSERT INTO documents VALUES (nextval('osserp.documents_id_seq'), NULL, NULL, 20, 'xsl/records/shared/time_of_supply.vm', NULL, 1253, '1970-01-01 00:00:00', 6000, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, 0, NULL, NULL, 'Import - Beleg - Leistungsdatum /-ort', 'xslRecordTimeOfSupply', false, 0);

CREATE OR REPLACE FUNCTION osserp.get_contact_email(contactid bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    tmp_id  BIGINT;
    result  TEXT;
BEGIN
    SELECT contact_id INTO tmp_id FROM osserp.contacts WHERE contact_id = contactid;
    IF FOUND THEN
        SELECT email INTO result FROM osserp.contact_emails WHERE contact_id = tmp_id ORDER BY isprimary DESC;
        IF FOUND THEN
            RETURN trim(result);
        END IF;
    END IF;
    RETURN '';
END;
$$;

CREATE OR REPLACE FUNCTION osserp.get_contact_fax_display(contactid bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    tmp_id  BIGINT;
    result  TEXT;
BEGIN
    SELECT contact_id INTO tmp_id FROM osserp.contacts WHERE contact_id = contactid;
    IF FOUND THEN
        SELECT ('0' || prefix || '-' || number) INTO result FROM osserp.contact_phones WHERE contact_id = tmp_id AND device_id = 3 ORDER BY isprimary DESC;
        IF FOUND THEN
            RETURN result;
        END IF;
    END IF;
    RETURN '';
END;
$$;

CREATE OR REPLACE FUNCTION osserp.get_contact_mobile_display(contactid bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    tmp_id  BIGINT;
    result  TEXT;
BEGIN
    SELECT contact_id INTO tmp_id FROM osserp.contacts WHERE contact_id = contactid;
    IF FOUND THEN
        SELECT ('0' || prefix || '-' || number) INTO result FROM osserp.contact_phones WHERE contact_id = tmp_id AND device_id = 2 ORDER BY isprimary DESC;
        IF FOUND THEN
            RETURN result;
        END IF;
    END IF;
    RETURN '';
END;
$$;

CREATE OR REPLACE FUNCTION osserp.get_contact_phone_display(contactid bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    tmp_id  BIGINT;
    result  TEXT;
BEGIN
    SELECT contact_id INTO tmp_id FROM osserp.contacts WHERE contact_id = contactid;
    IF FOUND THEN
        SELECT ('0' || prefix || '-' || number) INTO result FROM osserp.contact_phones WHERE contact_id = tmp_id AND device_id = 1 ORDER BY isprimary DESC;
        IF FOUND THEN
            RETURN result;
        END IF;
    END IF;
    RETURN '';
END;
$$;

-- DROP VIEW v_customer_export_de;
CREATE VIEW v_customer_export_de AS
 SELECT id AS "Debitor", lastname AS "Nachname", firstname AS "Firstname", street AS "Strasse", zipcode AS "PLZ", city AS "Ort",
  get_contact_email(c.contact_id) AS "E-Mail", get_contact_phone_display(c.contact_id) AS "Telefon",
  get_contact_mobile_display(c.contact_id) AS "Mobiltelefon", get_contact_fax_display(c.contact_id) AS "Fax"
 FROM v_customers c WHERE type_id <> 7 ORDER BY lastname;

-- DROP VIEW v_supplier_export_de;
CREATE VIEW v_supplier_export_de AS
 SELECT id AS "Kreditor", lastname AS "Nachname", firstname AS "Firstname", street AS "Strasse", zipcode AS "PLZ", city AS "Ort",
  get_contact_email(c.contact_id) AS "E-Mail", get_contact_phone_display(c.contact_id) AS "Telefon",
  get_contact_mobile_display(c.contact_id) AS "Mobiltelefon", get_contact_fax_display(c.contact_id) AS "Fax"
 FROM v_suppliers c WHERE type_id <> 7 ORDER BY lastname;

INSERT INTO system_properties VALUES (211, 'salesRevenueCostCollector', 'costCollectorByPurchase', 'java.lang.String', false, false, NULL, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 211, true);

UPDATE system_properties SET pvalue = '0.14.1', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
