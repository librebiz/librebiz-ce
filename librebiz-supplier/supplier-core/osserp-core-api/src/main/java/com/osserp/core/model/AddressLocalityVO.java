/**
 *
 * Copyright (C) 2002 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 17, 2002 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.AddressLocality;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AddressLocalityVO extends AbstractOption implements AddressLocality {

    private String zipcode;
    private String city;
    private String county;
    
    protected AddressLocalityVO() {
        super();
    }

    /**
     * Creates a new locality
     * @param id
     * @param description
     * @param zipcode
     * @param city
     * @param county
     */
    public AddressLocalityVO(Long id, String description, String zipcode, String city, String county) {
        super(id, (zipcode != null && city != null ? (zipcode + " " + city) : null), description, (String) null);
        this.zipcode = zipcode;
        this.city = city;
        this.county = county;
    }

    public String getZipcode() {
        return zipcode;
    }
    
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
    
    public String getCity() {
        return city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCounty() {
        return county;
    }
    
    public void setCounty(String county) {
        this.county = county;
    }
}
