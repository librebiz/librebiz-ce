<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="message" /> - <fmt:message key="detail" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:import url="${viewdir}/mail/_receivedMailAddresses.jsp"/>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="status"><fmt:message key="status" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${!empty view.businessNote}">
                                <fmt:message key="imported" />
                            </c:when>
                            <c:when test="${!empty view.documents}">
                                <fmt:message key="documents" /> <fmt:message key="imported" />
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="assignedLabel" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${!empty view.documents}">
                <c:forEach var="attachment" items="${view.documents}" varStatus="s">
                    <div class="row<c:if test="${s.index == 0}"> next</c:if>">
                        <div class="col-md-4">
                            <div class="form-group">
                                <c:if test="${s.index == 0}">
                                    <fmt:message key="documents" />
                                 </c:if>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="recipients">
                                    <o:out value="${attachment.displayName}" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <o:doc obj="${attachment}"><o:img name="printIcon" styleClass="bigIcon"/></o:doc>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>

            <c:if test="${!empty view.businessNote}">
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/delete" styleClass="form-control error" value="deleteMailImport" title="deleteMailImportInfo" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

        </div>
    </div>
</div>
