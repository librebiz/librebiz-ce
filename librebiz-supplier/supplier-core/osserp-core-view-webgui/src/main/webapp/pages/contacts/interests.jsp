<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.interestView}"><c:redirect url="/errors/error_context.jsp"/></c:if> 
<c:set var="view" value="${sessionScope.interestView}"/>
<c:set var="interestView" scope="request" value="${view}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<c:choose>

		<c:when test="${!empty view.bean}">
			<tiles:put name="styles" type="string">
				<style type="text/css">
					<c:import url="/css/contact_display.css"/>
				</style>
			</tiles:put>
			<c:choose>
				<c:when test="${view.editMode}">
					<tiles:put name="headline"><span class="bolderror"><fmt:message key="changeLabel"/>:</span> <fmt:message key="contactData"/></tiles:put>
					<tiles:put name="headline_right">
						<ul>
							<li><a href="<c:url value="/interests.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
							<li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
						</ul>
					</tiles:put>
					<tiles:put name="content" type="string">
						<c:import url="/pages/contacts/interest_editor.jsp"/>
					</tiles:put>
				</c:when>
				<c:otherwise>
					<tiles:put name="headline"><fmt:message key="contactData"/></tiles:put>
					<tiles:put name="headline_right">
						<ul>
							<li><a href="<c:url value="/interests.do?method=select"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
							<li><a href="<c:url value="/interests.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon"/></a></li>
							<li><a href="javascript:;" onclick="confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmInterestRemove"/>','<c:url value="/interests.do?method=delete"/>');" title="<fmt:message key="interestRemove"/>"><o:img name="deleteIcon"/></a></li>
							<li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
						</ul>
					</tiles:put>
					<tiles:put name="content" type="string">
						<c:import url="/pages/contacts/interest_display.jsp"/>
					</tiles:put>
				</c:otherwise>
			</c:choose>
		</c:when>

		<c:otherwise>
			<tiles:put name="styles" type="string">
				<style type="text/css">
					.table {
						margin-left: 17px;
					}

					.recordheader {
						margin-left: 17px;
					}
				</style>
			</tiles:put>
			<tiles:put name="headline"><o:listSize value="${view.list}"/> <fmt:message key="contactsFound"/></tiles:put>
			<tiles:put name="headline_right">
				<a href="<c:url value="/interests.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
				<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
			</tiles:put>
			<tiles:put name="content" type="string">
				<c:import url="/pages/contacts/interest_list.jsp"/>
			</tiles:put>
		</c:otherwise>
	</c:choose>

</tiles:insert>

