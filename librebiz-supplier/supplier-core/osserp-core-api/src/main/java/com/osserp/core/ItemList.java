/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2007 8:48:08 AM 
 * 
 */
package com.osserp.core;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ItemList extends ItemListDisplay {

    /**
     * Provides all positions with products
     * @return positions
     */
    List<ItemPosition> getPositions();

    /**
     * Fetches a position
     * @param positionId
     * @return position
     * @throws RuntimeException if position no longer exists
     */
    ItemPosition getPosition(Long positionId);

    /**
     * Resets a position by removing all items of the position with given config group id
     * @param configGroupId
     */
    void resetPosition(Long configGroupId);

    /**
     * Provides all items of all positions as list
     * @return allItems
     */
    List<Item> getAllItems();

    /**
     * Indicates that product is already added
     * @param product
     * @return true if already added to any position
     */
    boolean isAlreadyAdded(Product product);

    /**
     * Adds a new item
     * @param position
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param priceDate
     * @param priceOverridden,
     * @param partnerPrice
     * @param partnerPriceEditable
     * @param partnerPriceOverridden
     * @param purchasePrice
     * @param taxRate
     * @param note
     * @param includePrice
     * @param externalId
     * @throws ClientException if any required value missing or invalid
     */
    void addItem(
            Long position,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            boolean priceOverridden,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            boolean includePrice,
            Long externalId)
            throws ClientException;

    /**
     * Updates an existing item
     * @param itemId
     * @param customName
     * @param quantity
     * @param partnerPrice
     * @param partnerPriceOverridden
     * @param partnerPriceOverriddenByPermission
     * @param price
     * @param note
     * @throws ClientException if quantity is not set
     */
    void updateItem(
            Long itemId,
            String customName,
            Double quantity,
            BigDecimal partnerPrice,
            boolean partnerPriceOverridden,
            boolean partnerPriceOverriddenByPermission,
            BigDecimal price,
            String note) throws ClientException;

    /**
     * Replaces an product on an existing item
     * @param itemId
     * @param product
     * @param quantity
     * @param price
     * @param priceDate
     * @param partnerPice
     * @param partnerPriceEditable
     * @param purchasePice
     * @param note
     * @throws ClientException if validation failed
     */
    void replaceItem(
            Long itemId,
            Product product,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            BigDecimal purchasePrice,
            String note) throws ClientException;

    /**
     * Moves an item up
     * @param itemId
     */
    void moveUpItem(Long itemId);

    /**
     * Moves an item down
     * @param itemId to move
     */
    void moveDownItem(Long itemId);

    /**
     * Removes an item from related position
     * @param itemId
     */
    void removeItem(Long itemId);

    /**
     * Indicates that item listitem list is no longer active
     * @return historical
     */
    boolean isHistorical();

    /**
     * Sets that item list is no longer active
     * @param historical
     */
    void setHistorical(boolean historical);

    /**
     * Provides the initial created date (initialCreated == sales.created)
     * @return initialCreated
     */
    Date getInitialCreated();

    /**
     * Sets the initial created date
     * @param initialCreated
     */
    void setInitialCreated(Date initialCreated);

    /**
     * Indicates that itemList is related to wholesale order
     * @return wholesale
     */
    boolean isWholesale();

    /**
     * Indicates that this item list is currently locked
     * @return locked
     */
    boolean isLocked();

    /**
     * Provides the employee whose locking the item list
     * @return lockedBy
     */
    Long getLockedBy();

    /**
     * Provides the timestamp when item list was locked
     * @return lockTime
     */
    Date getLockTime();

    /**
     * Enables/disables lock mode
     * @param lockedBy
     * @param lock
     */
    void lock(Long lockedBy, boolean lock);

    /**
     * Provides item position for given group
     * @param group
     * @return
     */
    ItemPosition getPositionByGroup(Long group);
}
