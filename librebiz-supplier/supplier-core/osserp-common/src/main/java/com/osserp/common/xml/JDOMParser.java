/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 09.09.2004 
 * 
 */
package com.osserp.common.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class JDOMParser {

    public static final boolean NOVALIDATION = false;
    public static final boolean VALIDATE = true;

    /**
     * Creates an org.jdom2.Document by parsing the given input stream without any validation.
     * @param inputStream
     * @return document
     */
    public static Document getDocument(InputStream inputStream) {
        return getDocument(inputStream, NOVALIDATION);
    }

    /**
     * Creates an org.jdom2.Document by parsing the given xml string without any validation.
     * @param xml string
     * @return document
     */
    public static Document getDocument(String xml) {
        return getDocument(xml, NOVALIDATION);
    }

    /**
     * Creates an org.jdom2.Document by parsing the given input stream.
     * @param inputStream
     * @param validation turned on or off
     * @return document
     */
    public static Document getDocument(InputStream inputStream, boolean validation) {
        try {
            SAXBuilder builder = !validation ? new SAXBuilder() :
                new SAXBuilder(XMLReaders.DTDVALIDATING);
            builder.setExpandEntities(false);
            return builder.build(inputStream);
        } catch (IOException jex) {
            jex.printStackTrace();
            System.out.println("getDocument() error reading file...");
            return createError(XMLException.FILE_ACCESS);
        } catch (JDOMException jex) {
            jex.printStackTrace();
            System.out.println("getDocument() error parsing file...");
            return createError(XMLException.FILE_PARSE);
        } catch (Throwable t) {
            t.printStackTrace();
            System.out.println("getDocument() unknown exception");
            return createError(XMLException.UNKNOWN);
        }
    }

    /**
     * Creates an org.jdom2.Document by parsing the given xml string.
     * @param xml format
     * @param validation turned on or off
     * @return document (contains error tag and code on any exception
     */
    public static Document getDocument(String xml, boolean validation) {
        try {
            SAXBuilder builder = !validation ? new SAXBuilder() :
                new SAXBuilder(XMLReaders.DTDVALIDATING);
            builder.setExpandEntities(false);
            return builder.build(new StringReader(xml));
        } catch (IOException jex) {
            jex.printStackTrace();
            System.out.println("getDocument() error reading file...");
            return createError(XMLException.FILE_ACCESS);
        } catch (JDOMException jex) {
            jex.printStackTrace();
            System.out.println("getDocument() error parsing file...");
            return createError(XMLException.FILE_PARSE);
        } catch (Throwable t) {
            t.printStackTrace();
            System.out.println("getDocument() unknown exception");
            return createError(XMLException.UNKNOWN);
        }
    }

    private static Document createError(String errorMessage) {
        Document doc = new Document();
        Element root = new Element("error");
        if (errorMessage != null) {
            root.setText(errorMessage);
        }
        doc.setRootElement(root);
        return doc;
    }
}
