/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 29, 2007 10:49:54 AM 
 * 
 */
package com.osserp.common.beans;

import java.util.Date;

import com.osserp.common.Appointment;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DefaultAppointment extends AbstractAppointment {

    protected DefaultAppointment() {
        super();
    }

    public DefaultAppointment(
            Long id,
            String name,
            String headline,
            String message,
            String url,
            String target,
            Date date) {
        super(id, name, headline, message, url, target, date);
    }

    public DefaultAppointment(
            Long id,
            Long reference,
            String name,
            String headline,
            String message,
            String url,
            Date date) {
        super(id, reference, name, headline, message, url, date);
    }

    @Override
    public Object clone() {
        return new DefaultAppointment(this);
    }

    private DefaultAppointment(Appointment appointment) {
        super(appointment);
    }
}
