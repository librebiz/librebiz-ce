<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="businessTypeSelection" value="${requestScope.businessTypeSelection}"/>
<c:set var="businessTypeSelectionUrl" value="${requestScope.businessTypeSelectionUrl}"/>

<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
					<th class="businessTypeShortkey"><fmt:message key="shortkey"/></th>
                    <th class="businessTypeName"><fmt:message key="name"/></th>
                    <th class="businessTypeName"><fmt:message key="workflow"/></th>
					<th class="businessTypeContext"><fmt:message key="requestContextLabel"/></th>
					<th class="businessTypeContext"><fmt:message key="orderContextLabel"/></th>
                    <th class="businessTypeCompany"><fmt:message key="company"/></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty businessTypeSelection}">
                        <tr><td colspan="6"><fmt:message key="noOrderTypesAvailable"/></td></tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="obj" items="${businessTypeSelection}">
                            <tr>
                                <td class="businessTypeShortkey"><o:out value="${obj.key}"/></td>
                                <td class="businessTypeName"><v:link url="${businessTypeSelectionUrl}?id=${obj.id}"><o:out value="${obj.name}"/></v:link></td>
                                <td class="businessTypeContext"><o:out value="${obj.workflow.name}"/></td>
                                <td class="businessTypeContext"><o:out value="${obj.context}"/></td>
						        <td class="businessTypeContext"><o:out value="${obj.salesContext}"/></td>
                                <td class="businessTypeCompany"><oc:options name="systemCompanies" value="${obj.company}"/></td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
