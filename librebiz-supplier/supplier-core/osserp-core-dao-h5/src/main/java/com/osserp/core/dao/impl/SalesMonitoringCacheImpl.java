/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 31, 2008 1:08:35 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;
import org.jdom2.Element;
import org.springframework.jdbc.core.JdbcTemplate;

import com.osserp.common.dao.Tables;
import com.osserp.common.service.impl.AbstractDatabaseStateSavingCache;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.xml.JDOMParser;
import com.osserp.core.dao.ProjectQueries;
import com.osserp.core.dao.SalesMonitoringCache;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.sales.SalesMonitoringItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesMonitoringCacheImpl extends AbstractDatabaseStateSavingCache implements SalesMonitoringCache {
    private static Logger log = LoggerFactory.getLogger(SalesMonitoringCacheImpl.class.getName());
    private ProjectQueries queries = null;
    private Map<Long, List<SalesMonitoringItem>> cacheByCompany = Collections.synchronizedMap(new HashMap<Long, List<SalesMonitoringItem>>());
    private Map<String, List<SalesMonitoringItem>> cacheByRequestTypeSelection = Collections.synchronizedMap(new HashMap<String, List<SalesMonitoringItem>>());

    /**
     * Creates a new sales monitoring cache
     * @param jdbcTemplate
     * @param tables
     * @param queries
     */
    public SalesMonitoringCacheImpl(JdbcTemplate jdbcTemplate, Tables tables, ProjectQueries queries) {
        super(jdbcTemplate, tables, TableKeys.SALES_MONITORING_CACHE_TASKS);
        this.queries = queries;
    }

    public List<SalesMonitoringItem> getSalesMonitoring(String requestTypeSelection) {
        List<SalesMonitoringItem> result = cacheByRequestTypeSelection.get(requestTypeSelection);
        if (result == null || result.isEmpty()) {
            result = queries.getSalesMonitoring(requestTypeSelection);
            cacheByRequestTypeSelection.put(requestTypeSelection, result);
        }
        if (log.isDebugEnabled()) {
            log.debug("getSalesMonitoring() done [requestTypeSelection=" + requestTypeSelection 
                + ", count=" + result.size() + "]");
        }
        return new ArrayList<SalesMonitoringItem>(result);
    }

    public List<SalesMonitoringItem> getSalesMonitoring(Long company) {
        List<SalesMonitoringItem> result = cacheByCompany.get(company);
        if (result == null || result.isEmpty()) {
            result = queries.getSalesMonitoring(company);
            cacheByCompany.put(company, result);
        }
        if (log.isDebugEnabled()) {
            log.debug("getSalesMonitoring() done [company=" + company 
                + ", count=" + result.size() + "]");
        }
        return new ArrayList<SalesMonitoringItem>(result);
    }

    public void refresh(String eventXml) {
        long start = System.currentTimeMillis();
        if (log.isInfoEnabled()) {
            log.info("refresh() invoked");
        }
        Set<String> typeSelections = cacheByRequestTypeSelection.keySet();
        for (Iterator<String> i = typeSelections.iterator(); i.hasNext();) {
            String nextSelection = i.next();
            reloadSalesMonitoring(nextSelection);
        }
        Set<Long> companies = fetchCompanyIds(eventXml);
        if (companies.isEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("refresh() no company selection found, reloading all...");
            }
            Set<Long> companyIds = cacheByCompany.keySet();
            for (Iterator<Long> i = companyIds.iterator(); i.hasNext();) {
                Long company = i.next();
                reloadSalesMonitoring(company);
            }
        } else {
            if (log.isInfoEnabled()) {
                log.info("refresh() company selection found, reloading...");
            }
            for (Iterator<Long> i = companies.iterator(); i.hasNext();) {
                Long company = i.next();
                reloadSalesMonitoring(company);
            }
        }
        if (log.isInfoEnabled()) {
            log.info("refresh() done [duration=" + (System.currentTimeMillis() - start) + "ms]");
        }
    }

    private Set<Long> fetchCompanyIds(String eventXml) {
        Set<Long> list = new java.util.HashSet<Long>();
        try {
            Document doc = JDOMParser.getDocument(eventXml);
            Element root = doc.getRootElement();
            List<Element> initiators = root.getChildren("initiator");
            for (int i = 0, j = initiators.size(); i < j; i++) {
                Element nextInitiator = initiators.get(i);
                Long company = NumberUtil.createLong(nextInitiator.getChildText("company"));
                if (company != null) {
                    list.add(company);
                }
            }

        } catch (Throwable t) {
        }
        return list;
    }

    private void reloadSalesMonitoring(String requestTypeSelection) {
        long start = System.currentTimeMillis();
        List<SalesMonitoringItem> prj = queries.getSalesMonitoring(requestTypeSelection);
        cacheByRequestTypeSelection.put(requestTypeSelection, prj);
        if (log.isDebugEnabled()) {
            log.debug("reloadSalesMonitoring() done [requestTypeSelection=" + requestTypeSelection
                    + ", duration=" + (System.currentTimeMillis() - start) + "ms]");
        }
    }

    private void reloadSalesMonitoring(Long company) {
        long start = System.currentTimeMillis();
        List<SalesMonitoringItem> list = queries.getSalesMonitoring(company);
        cacheByCompany.put(company, list);
        if (log.isDebugEnabled()) {
            log.debug("reloadSalesMonitoring() done [company=" + company
                    + ", duration=" + (System.currentTimeMillis() - start) + "ms]");
        }
    }
}
