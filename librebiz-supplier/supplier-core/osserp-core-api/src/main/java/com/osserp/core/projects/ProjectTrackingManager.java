/**
 *
 * Copyright (C) 2009, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.projects;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.BusinessCase;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectTrackingManager {

    /**
     * Creates a new project tracking
     * @param user
     * @param businessCase
     * @param type
     * @param name
     * @param startDate
     * @param endDate
     * @return new created tracking
     */
    ProjectTracking create(DomainUser user, BusinessCase businessCase, ProjectTrackingType type, String name, Date startDate, Date endDate);

    /**
     * Close a tracking by freezing content and creating a persistent pdf.
     * @param user
     * @param tracking
     * @throws ClientException if tracking data not valid or completed
     */
    void close(DomainUser user, ProjectTracking tracking) throws ClientException;

    /**
     * Updates project tracking meta data
     * @param user
     * @param tracking
     * @param type
     * @param name
     * @param startDate
     * @param endDate
     * @return tracking
     * @throws ClientException if validation failed
     */
    ProjectTracking update(DomainUser user, ProjectTracking tracking, ProjectTrackingType type, String name, Date startDate, Date endDate) throws ClientException;

    /**
     * Updates project tracking by existing or new record 
     * @param user
     * @param tracking 
     * @param record existing record or null to add new
     * @param recordType
     * @param name
     * @param text
     * @param internalNote
     * @param startDate
     * @param endDate
     * @param time use time instead of start and end date only
     * @return updated tracking data
     * @throws ClientException if validation failed 
     */
    ProjectTracking updateTrackingData(
            DomainUser user, 
            ProjectTracking tracking, 
            ProjectTrackingRecord record, 
            ProjectTrackingRecordType recordType, 
            String name, 
            String text,
            String internalNote,
            Date startDate, 
            Date endDate, 
            Double time) throws ClientException;

    /**
     * Deletes a tracking data row
     * @param user
     * @param tracking
     * @param recordId
     * @return tracking data
     */
    ProjectTracking deleteTrackingData(DomainUser user, ProjectTracking tracking, Long recordId);
    
    /**
     * Toggles ignore flag of a tracking record
     * @param user
     * @param tracking
     * @param id the primary key of the tracking record
     * @return tracking with changed record
     * @throws ClientException if user has no permission or tracking does 
     * not support changing ignorable. This is mostly a configuration issue.
     */
    ProjectTracking toggleIgnore(DomainUser user, ProjectTracking tracking, Long id) throws ClientException;
            
    /**
     * Finds latest tracking by businessCase
     * @param businessCase
     * @return tracking or null if not exists
     */
    ProjectTracking findLatest(BusinessCase businessCase);

    /**
     * Finds all trackings by businessCase
     * @param businessCase
     * @return trackings or empty list
     */
    List<ProjectTracking> findAll(BusinessCase businessCase);
    
    /**
     * Provides the count of available trackings
     * @param businessCase
     * @return count of trackings referenced by businessCase
     */
    int getTrackingCount(BusinessCase businessCase);

    /**
     * Provides the tracking data as pdf
     * @param user
     * @param tracking
     * @return tracking data document
     */
    DocumentData getPdf(DomainUser user, ProjectTracking tracking);
}
