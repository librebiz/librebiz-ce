<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.productPriceAwareView}"/>

<style type="text/css">

	.rangeStart,
	.rangeEnd {
		width: 48px;
	}

	.price,
	.priceMin,
	.priceMargin {
		width: 58px;
	}

	.actionIcon {
		width: 39px;
	}

</style>

<table class="table table-striped">
	<thead>

		<tr>
			<th class="rangeStart"><fmt:message key="salesPriceRangeStartLabel"/></th>
			<th class="right rangeEnd"><fmt:message key="til"/> <oc:options name="quantityUnits" value="${view.product.quantityUnit}"/></th>
			<th class="right price"><fmt:message key="salesPriceConsumerShortLabel"/></th>
			<th class="right priceMin"><fmt:message key="salesPriceConsumerMinShortLabel"/></th>
			<th class="right priceMargin"><fmt:message key="salesPriceConsumerMarginShortLabel"/></th>
			<th class="right price"><fmt:message key="salesPriceResellerShortLabel"/></th>
			<th class="right priceMin"><fmt:message key="salesPriceResellerMinShortLabel"/></th>
			<th class="right priceMargin"><fmt:message key="salesPriceResellerMarginShortLabel"/></th>
			<th class="right price"><fmt:message key="salesPricePartnerShortLabel"/></th>
			<th class="right priceMin"><fmt:message key="salesPricePartnerMinShortLabel"/></th>
			<th class="right priceMargin"><fmt:message key="salesPricePartnerMarginShortLabel"/></th>
			<th class="center actionIcon"><fmt:message key="action"/></th>
		</tr>
	</thead>
	<tbody>
		<%-- display ranges --%>
		<c:forEach var="bean" items="${view.list}">
			<tr>
				<td class="right rangeStart"><o:number value="${bean.rangeStart}" format="integer"/></td>
				<td class="right rangeEnd">
					<c:choose>
						<c:when test="${bean.rangeEnd == 0}">
							&infin;
						</c:when>
						<c:otherwise>
							<o:number value="${bean.rangeEnd}" format="integer"/>
						</c:otherwise>
					</c:choose>
				</td>
				<td class="right price boldtext"><o:number value="${bean.consumerPrice}" format="currency"/></td>
				<td class="right priceMin"><o:number value="${bean.consumerPriceMinimum}" format="currency"/></td>
				<td class="right priceMargin"><o:number value="${bean.consumerMinimumMargin}" format="percent"/></td>
				<td class="right price boldtext"><o:number value="${bean.resellerPrice}" format="currency"/></td>
				<td class="right priceMin"><o:number value="${bean.resellerPriceMinimum}" format="currency"/></td>
				<td class="right priceMargin"><o:number value="${bean.resellerMinimumMargin}" format="percent"/></td>
				<td class="right price boldtext"><o:number value="${bean.partnerPrice}" format="currency"/></td>
				<td class="right priceMin"><o:number value="${bean.partnerPriceMinimum}" format="currency"/></td>
				<td class="right priceMargin"><o:number value="${bean.partnerMinimumMargin}" format="percent"/></td>
				<td class="center actionIcon">
					<o:permission role="product_price_edit,purchase_price_change" info="permissionProductPriceEdit">
						<v:link url="/products/productPriceByQuantity/select?id=${bean.id}">
							<o:img name="writeIcon"/>
						</v:link>
					</o:permission>
				</td>
			</tr>

		</c:forEach>
	</tbody>
</table>
