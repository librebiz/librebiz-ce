/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22.12.2013 
 * 
 */
package com.osserp.core.model.system;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.system.SyncTask;
import com.osserp.core.system.SyncTaskConfig;

/**
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SyncTaskImpl extends AbstractEntity implements SyncTask {

    private SyncTaskConfig config;
    private Long status = SyncTask.STATUS_OPEN;
    private String typeName;
    private String actionName;
    private String objectId;
    private String objectData;
    private String errorMessage;

    protected SyncTaskImpl() {
        super();
    }

    public SyncTaskImpl(
            SyncTaskConfig config,
            String typeName,
            String actionName,
            String objectId,
            String objectData) {
        super();
        this.config = config;
        this.typeName = typeName;
        this.actionName = actionName;
        this.objectId = objectId;
        this.objectData = objectData;
    }

    public SyncTaskConfig getConfig() {
        return config;
    }

    public void setConfig(SyncTaskConfig config) {
        this.config = config;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectData() {
        return objectData;
    }

    public void setObjectData(String objectData) {
        this.objectData = objectData;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isClosed() {
        return SyncTask.STATUS_CLOSED.equals(status);
    }

    public boolean isFailed() {
        return SyncTask.STATUS_ERROR.equals(status);
    }

    public boolean isOpen() {
        return SyncTask.STATUS_OPEN.equals(status);
    }

    public boolean isRunning() {
        return SyncTask.STATUS_RUNNING.equals(status);
    }

    public void stop(String message) {
        status = SyncTask.STATUS_ERROR;
        errorMessage = message;
        setChanged(new Date(System.currentTimeMillis()));
    }

    public void setRunning() {
        status = SyncTask.STATUS_RUNNING;
        setChanged(new Date(System.currentTimeMillis()));
    }

    public void setClosed() {
        status = SyncTask.STATUS_CLOSED;
        setChanged(new Date(System.currentTimeMillis()));
    }

    public String getStatusDisplay() {
        if (isClosed()) {
            return "taskStatusClosed";
        } else if (isRunning()) {
            return "taskStatusRunning";
        } else if (isOpen()) {
            return "taskStatusWaiting";
        } else if (isFailed()) {
            return "taskStatusFailed";
        } 
        return "taskStatusUnknown";
    }

}
