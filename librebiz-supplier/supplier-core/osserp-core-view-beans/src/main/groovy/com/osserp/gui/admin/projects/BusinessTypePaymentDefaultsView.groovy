/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 11.04.2011 17:06:59 
 * 
 */
package com.osserp.gui.admin.projects

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.core.BusinessTypeManager
import com.osserp.groovy.web.ViewContextException
import com.osserp.core.Options
import com.osserp.core.finance.PaymentAgreement
import com.osserp.core.finance.PaymentCondition
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
public class BusinessTypePaymentDefaultsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(BusinessTypePaymentDefaultsView.class.getName())

    BusinessTypePaymentDefaultsView() {
        enablePopupView()
        providesEditMode()
        dependencies = ['businessTypeConfigView']
        headerName = 'defaultPaymentAgreement'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!env.businessTypeConfigView || !env.businessTypeConfigView.bean) {
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
        }

        if (!bean) {
            bean = businessTypeManager.getDefaultPaymentAgreement(env.businessTypeConfigView.bean)
        }
    }

    @Override
    void save() {
        updateBean()
        businessTypeManager.updateDefaultPaymentAgreement(getDomainEmployee(), bean)
        disableEditMode()
    }

    void selectPercentage() {
        updateBean()
    }

    private PaymentAgreement updateBean() {
        bean.updatePaymentConditions(
                form.getBoolean('hidePayments'),
                form.getDouble('downpaymentPercent'),
                form.getLong('downpaymentTargetId'),
                form.getDouble('deliveryInvoicePercent'),
                form.getLong('deliveryInvoiceTargetId'),
                form.getLong('finalInvoiceTargetId'),
                fetchPaymentCondition())
    }

    private PaymentCondition fetchPaymentCondition() {
        Long id = form.getLong('paymentConditionId')
        if (id) {
            List conditions = getOptions(Options.RECORD_PAYMENT_CONDITIONS)
            PaymentCondition pc = conditions.find { it.id == id }
            if (!pc) {
                logger.warn("fetchPaymentCondition: missing cached condition [id=" + id + "]")
            }
            return pc
        }
        return null
    }

    private BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }
}
