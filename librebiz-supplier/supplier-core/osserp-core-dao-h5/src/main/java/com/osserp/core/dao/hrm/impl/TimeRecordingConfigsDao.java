/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 3, 2011 12:03:15 PM 
 * 
 */
package com.osserp.core.dao.hrm.impl;

import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateUtil;

import com.osserp.core.Options;
import com.osserp.core.dao.hrm.TimeRecordingConfigs;
import com.osserp.core.dao.impl.AbstractRepository;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordStatus;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.model.hrm.TimeRecordMarkerTypeImpl;
import com.osserp.core.model.hrm.TimeRecordStatusImpl;
import com.osserp.core.model.hrm.TimeRecordTypeImpl;
import com.osserp.core.model.hrm.TimeRecordingConfigImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class TimeRecordingConfigsDao extends AbstractRepository implements TimeRecordingConfigs {
    private OptionsCache options;

    protected TimeRecordingConfigsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory, OptionsCache options) {
        super(jdbcTemplate, tables, sessionFactory);
        this.options = options;
    }

    public TimeRecordingConfig createConfig(Employee user, String name, String description) {
        TimeRecordingConfigImpl obj = new TimeRecordingConfigImpl(user, name, description);
        getCurrentSession().saveOrUpdate(TimeRecordingConfigImpl.class.getName(), obj);
        options.refresh(Options.TIME_RECORDING_CONFIGS);
        return obj;
    }

    public List<TimeRecordingConfig> getConfigs() {
        return getCurrentSession().createQuery(
                "from " + TimeRecordingConfigImpl.class.getName() + " o order by o.name").list();
    }

    public void save(Employee user, TimeRecordingConfig config) {
        config.setChanged(new Date(System.currentTimeMillis()));
        config.setChangedBy((user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
        getCurrentSession().saveOrUpdate(TimeRecordingConfigImpl.class.getName(), config);
        options.refresh(Options.TIME_RECORDING_CONFIGS);
    }

    public TimeRecordType createType(Employee user, Long id, String name, String description)
            throws ClientException {
        List<TimeRecordType> existing = getTypes();
        for (int i = 0, j = existing.size(); i < j; i++) {
            TimeRecordType next = existing.get(i);
            if (next.isStarting() && next.getName().equals(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
            if (next.getId().equals(id)) {
                throw new ClientException(ErrorCode.ID_EXISTS);
            }
        }
        TimeRecordTypeImpl start = new TimeRecordTypeImpl(user, id, name, description, existing.size());
        getCurrentSession().saveOrUpdate(TimeRecordTypeImpl.class.getName(), start);
        options.refresh(Options.TIME_RECORD_TYPES);
        return start;
    }

    public TimeRecordMarkerType createMarkerType(Employee user, Long id, String name, String description)
            throws ClientException {
        List<TimeRecordMarkerType> existing = getMarkerTypes();
        for (int i = 0, j = existing.size(); i < j; i++) {
            TimeRecordMarkerType next = existing.get(i);
            if (next.getName().equals(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
            if (next.getId().equals(id)) {
                throw new ClientException(ErrorCode.ID_EXISTS);
            }
        }
        TimeRecordMarkerTypeImpl start = new TimeRecordMarkerTypeImpl(user, id, name, description, existing.size());
        getCurrentSession().saveOrUpdate(TimeRecordMarkerTypeImpl.class.getName(), start);
        options.refresh(Options.TIME_RECORD_MARKER_TYPES);
        return start;
    }

    public void save(Employee user, TimeRecordType type) {
        type.setChanged(DateUtil.getCurrentDate());
        type.setChangedBy(user.getId());
        getCurrentSession().saveOrUpdate(TimeRecordTypeImpl.class.getName(), type);
        options.refresh(Options.TIME_RECORD_TYPES);
    }

    public void save(Employee user, TimeRecordMarkerType type) {
        type.setChanged(DateUtil.getCurrentDate());
        type.setChangedBy(user.getId());
        getCurrentSession().saveOrUpdate(TimeRecordMarkerTypeImpl.class.getName(), type);
        options.refresh(Options.TIME_RECORD_MARKER_TYPES);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.TimeRecordings#getType(java.lang.Long)
     */
    public TimeRecordType getType(Long id) {
        return (TimeRecordType) getCurrentSession().load(TimeRecordTypeImpl.class.getName(), id);
    }

    public List<TimeRecordType> getTypes() {
        return getCurrentSession().createQuery("from " +
                TimeRecordTypeImpl.class.getName() + " o order by o.id").list();
    }

    public List<TimeRecordMarkerType> getMarkerTypes() {
        return getCurrentSession().createQuery("from " +
                TimeRecordMarkerTypeImpl.class.getName() + " o").list();
    }

    public List<TimeRecordStatus> getStatusList() {
        return getCurrentSession().createQuery("from " +
                TimeRecordStatusImpl.class.getName() + " o order by o.id").list();
    }
}
