/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Feb-2007 10:11:33 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

import com.osserp.core.Item;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.ProjectSuppliers;
import com.osserp.core.dao.records.PurchaseInvoiceCancellations;
import com.osserp.core.dao.records.PurchaseInvoices;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.Stocktaking;
import com.osserp.core.model.records.ItemChangedInfoVO;
import com.osserp.core.products.Product;
import com.osserp.core.projects.ProjectSupplier;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceCancellation;
import com.osserp.core.purchasing.PurchaseInvoiceManager;
import com.osserp.core.purchasing.PurchaseInvoiceType;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseInvoiceManagerImpl extends AbstractPurchaseInvoiceManager
        implements PurchaseInvoiceManager {
    private static Logger log = LoggerFactory.getLogger(PurchaseInvoiceManagerImpl.class.getName());

    private PurchaseOrders purchaseOrders = null;
    private PurchaseInvoiceCancellations invoiceCancellations = null;
    private ProjectSuppliers projectSuppliers = null;

    public PurchaseInvoiceManagerImpl(
            PurchaseInvoices records,
            SystemConfigManager systemConfigManager,
            ProductPlanningCacheSender productPlanningTask,
            PaymentConditions paymentConditions,
            PurchaseOrders purchaseOrders,
            Products products,
            ProjectSuppliers projectSuppliers,
            PurchaseInvoiceCancellations invoiceCancellations) {

        super(records, systemConfigManager, products, productPlanningTask, paymentConditions);
        this.purchaseOrders = purchaseOrders;
        this.projectSuppliers = projectSuppliers;
        this.invoiceCancellations = invoiceCancellations;
    }

    public List<Record> getByOrder(Order order) {
        return new ArrayList(getByReference(order.getId()));
    }

    public Record getBySupplierReference(Supplier supplier, String supplierReference) {
        return getInvoicesDao().getBySupplierReference(supplier.getId(), supplierReference);
    }

    public PurchaseInvoice create(Employee user, PurchaseOrder order) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [user=" + user.getId() + ", order="
                    + order.getId() + "]");
        }
        if (!order.isUnchangeable() || order.isChangeableDeliveryAvailable()) {
            if (log.isDebugEnabled()) {
                log.debug("create() failed, record not released");
            }
            throw new ClientException(ErrorCode.RECORD_CHANGEABLE);
        }
        if (order.isClosed() || order.isCanceled()) {
            if (log.isDebugEnabled()) {
                log.debug("create() failed, record released or canceled");
            }
            throw new ClientException(ErrorCode.PURCHASE_ORDER_CLOSED);
        }
        PurchaseInvoice obj = getInvoicesDao().create(user, order);
        this.purchaseOrders.closeOrder(order.getId());
        return obj;
    }

    public PurchaseInvoice create(Employee user, Long company, Long branchId, Long bookingType) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [user=" + user.getId() + ", company="
                    + company + ", type=" + bookingType + "]");
        }
        return getInvoicesDao().create(user, company, branchId, bookingType);
    }

    public PurchaseInvoice create(Employee user, Long company, Long branchId, Long bookingType, Supplier supplier) {

        if (log.isDebugEnabled()) {
            log.debug("create() invoked [user="
                    + user.getId()
                    + ", company="
                    + company
                    + ", bookingType="
                    + bookingType
                    + ", supplier="
                    + ((supplier == null) ? "null" : supplier.getId()
                            .toString()) + "]");
        }
        return getInvoicesDao().create(user, company, branchId, bookingType, supplier, null);
    }
    
    public PurchaseInvoice create(
        Employee user, 
        Long company, 
        Long branchId, 
        BookingType bookingType, 
        Supplier supplier,
        Date recordDate,
        Long recordNumber,
        Date invoiceDate,
        String invoiceNumber,
        Product product,
        String customName,
        String productNote,
        Double productQuantity,
        BigDecimal productPrice) throws ClientException {
        
        return getInvoicesDao().create(
                user, 
                company, 
                branchId, 
                bookingType, 
                supplier, 
                recordDate, 
                recordNumber, 
                invoiceDate, 
                invoiceNumber, 
                (product == null ? null : product.getDefaultStock()), 
                product, 
                customName, 
                productNote, 
                productQuantity, 
                productPrice);
    }
    
    public PurchaseInvoice create(
        Employee user, 
        PurchaseInvoice existing,
        Date recordDate,
        Long recordNumber,
        Date invoiceDate,
        String invoiceNumber) throws ClientException {
        
        return getInvoicesDao().create(user, existing, recordDate, recordNumber, invoiceDate, invoiceNumber);
    }

    public PurchaseInvoice createCreditNote(Employee user, PurchaseInvoice invoice, String customHeader) {
        if (log.isDebugEnabled()) {
            log.debug("createCancellation() invoked [user=" + user.getId()
                    + ", invoice=" + invoice.getId() + "]");
        }
        return getInvoicesDao().createCreditNote(user, invoice, customHeader);
    }

    public PurchaseInvoice createStocktakingCorrection(Employee user, Stocktaking st) {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [user=" + user.getId()
                    + ", stocktaking=" + st.getId() + "]");
        }
        return getInvoicesDao().create(user, st);
    }

    public Product createValuation(Employee user, Product product,
            Long company, Long branchId, Double valuation)
            throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("createValuation() invoked [user=" + user.getId()
                    + ", product=" + product.getProductId() + ", company="
                    + company + ", branchId=" + branchId + ", valuation="
                    + valuation + "]");
        }
        return getInvoicesDao().createValuation(user, product, company,
                branchId, valuation);
    }

    public List<RecordDisplay> findOpen(Date from, Date til) {
        List<RecordDisplay> result = getInvoicesDao().findOpen(from, til);
        if (log.isDebugEnabled()) {
            log.debug("findOpen() invoked [from=" + from + ", til=" + til
                    + ", count=" + result.size() + "]");
        }
        return result;
    }

    public List<RecordDisplay> findClosed(Date from, Date til) {
        List<RecordDisplay> result = getInvoicesDao().findClosed(from, til);
        if (log.isDebugEnabled()) {
            log.debug("findClosed() invoked [from=" + from + ", til=" + til
                    + ", count=" + result.size() + "]");
        }
        return result;
    }

    public void cancel(Record record, Employee user, String note)
            throws ClientException {
        if (!(record instanceof PurchaseInvoice)) {
            throw new BackendException("cancel", "[expectedClass="
                    + PurchaseInvoice.class.getName() + ", foundClass="
                    + record.getClass().getName() + "]");
        }
        if (!record.isUnchangeable()) {
            throw new ClientException(ErrorCode.RECORD_OPERATION_NOT_SUPPORTED);
        }
        List<Item> items = new ArrayList<Item>(record.getItems());
        PurchaseInvoiceCancellation cancellation = invoiceCancellations
                .create(user, (PurchaseInvoice) record, note);
        if (log.isDebugEnabled()) {
            log.debug("cancel() created cancellation [id="
                    + cancellation.getId() + "]");
        }
        super.delete(record, user);
        deleteSupplierReference(record);
        if (record.getReference() != null) {
            Order order = purchaseOrders
                    .reopenOrder(record.getReference());
            items.addAll(new ArrayList<Item>(order.getItems()));
            items.addAll(new ArrayList<Item>(order.getDeliveries()));
        }
        createProductPlanningRefreshTask(record, items);
    }

    @Override
    public ItemChangedInfo addItem(
            Employee user,
            Record record,
            Product product,
            Long stockId,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            String note)
            throws ClientException {
        return super.addItem(user, record, stockId, product,
                customName, quantity, taxRate, price, note);
    }

    @Override
    public ItemChangedInfo deleteItem(Employee user, Record record, Item item) {
        ItemChangedInfo info = super.deleteItem(user, record, item);
        PurchaseInvoice invoice = (PurchaseInvoice) record;
        if (record.getReference() != null
                && PurchaseInvoiceType.GOODS.equals(invoice
                        .getBookingType().getId())) {
            List<PurchaseOrder> list = purchaseOrders
                    .getCarryover(invoice);
            if (!list.isEmpty()) {
                PurchaseOrder carryOver = list.get(0);
                carryOver.addItem(item.getStockId(), item.getProduct(),
                        item.getCustomName(), item.getQuantity(), item.getTaxRate(),
                        item.getPrice(), new Date(), item.getPrice(), false, false,
                        item.getPrice(), item.getNote(), true, item.getExternalId());
                purchaseOrders.save(carryOver);
                if (log.isDebugEnabled()) {
                    log.debug("deleteItem() added carry over from "
                            + record.getId() + " to existing "
                            + carryOver.getId() + " carry over order");
                }
            } else {
                try {
                    List<Item> carryOver = new ArrayList<Item>();
                    carryOver.add(item);

                    PurchaseOrder previous = (PurchaseOrder) purchaseOrders
                            .load(invoice.getReference());
                    PurchaseOrder newOrder = purchaseOrders.create(null, previous, carryOver);
                    if (log.isDebugEnabled()) {
                        log.debug("deleteItem() created new carry over order "
                                + newOrder.getId() + " for deleted "
                                + item.getProduct().getProductId());
                    }
                } catch (Throwable t) {
                    log.warn("deleteItem() ignoring failure while attempt to create"
                            + " carry over for "
                            + record.getId()
                            + " item "
                            + item.getId());
                }
            }
        }
        return info;
    }

    @Override
    public ItemChangedInfo updateItem(
            Employee user,
            Record record,
            Item item,
            String customName,
            Double quantity,
            BigDecimal price,
            Double taxRate,
            Long stockId,
            String note)
            throws ClientException {
        PurchaseInvoice obj = (PurchaseInvoice) record;
        ItemChangedInfo info = new ItemChangedInfoVO(user, record.getId(),
                item.getProduct(), item.getStockId(), stockId,
                item.getQuantity(), quantity, item.getPrice(), price);
        obj.updateItem(item, customName, quantity, price, taxRate, stockId, note);
        if (obj.getReference() != null
                && PurchaseInvoiceType.GOODS.equals(obj.getBookingType()
                        .getId())) {
            List<PurchaseOrder> list = purchaseOrders.getCarryover(obj);
            if (!list.isEmpty()) {
                PurchaseOrder carryOver = list.get(0);
                Item existing = getExisting(carryOver, item.getProduct()
                        .getProductId());
                if (existing == null) {
                    double coq = item.getQuantity() - quantity;
                    if (item.getQuantity() > quantity && coq > 0) {
                        carryOver.addItem(
                                item.getStockId(),
                                item.getProduct(),
                                customName,
                                coq,
                                taxRate,
                                item.getPrice(),
                                new Date(),
                                item.getPrice(),
                                false,
                                false,
                                item.getPrice(),
                                item.getNote(),
                                true,
                                item.getExternalId());
                        purchaseOrders.save(carryOver);
                        if (log.isDebugEnabled()) {
                            log.debug("updateItem() added carry over from "
                                    + obj.getId() + " to existing "
                                    + carryOver.getId() + " carry over order");
                        }
                    }
                } else {
                    double carryOverQuantity = existing.getQuantity()
                            + (item.getQuantity() - quantity);
                    if (carryOverQuantity > 0) {
                        carryOver.updateItem(
                                existing,
                                customName,
                                carryOverQuantity,
                                existing.getPrice(),
                                taxRate,
                                existing.getStockId(),
                                existing.getNote());
                        if (log.isDebugEnabled()) {
                            log.debug("updateItem() updated carry over item quantity of product "
                                    + item.getProduct().getProductId()
                                    + " to "
                                    + carryOverQuantity);
                        }
                    }
                }
            } else {
                if (item.getQuantity() > quantity) {
                    try {

                        List<Item> carryOver = new ArrayList<Item>();
                        Item diff = (Item) item.clone();
                        diff.setQuantity(item.getQuantity() - quantity);
                        carryOver.add(diff);
                        PurchaseOrder previous = (PurchaseOrder) purchaseOrders
                                .load(obj.getReference());
                        PurchaseOrder newOrder = purchaseOrders.create(null, previous, carryOver);
                        if (log.isDebugEnabled()) {
                            log.debug("updateItem() created new carry over order "
                                    + newOrder.getId()
                                    + " for deleted "
                                    + item.getProduct().getProductId());
                        }
                    } catch (Throwable t) {
                        log.warn("updateItem() ignoring failure while attempt to create"
                                + " carry over for "
                                + obj.getId()
                                + " item "
                                + item.getId());
                    }
                }
            }
        }
        persist(obj);
        doAfterItemUpdate(obj, info);
        return info;
    }

    @Override
    protected void doAfterItemUpdate(Record record, ItemChangedInfo result) {
        if (result != null) {
            ItemChangedInfo obj = getInvoicesDao().createItemChangedInfo(result);
            if (log.isDebugEnabled()) {
                log.debug("doAfterItemUpdate() item changed info created [id="
                        + (obj == null ? "null" : obj.getId()) + "]");
            }
        }
    }

    @Override
    public void delete(Record record, Employee user) throws ClientException {
        if (record.isUnchangeable()) {
            throw new ClientException(ErrorCode.RECORD_OPERATION_NOT_SUPPORTED);
        }
        List<Item> items = new ArrayList<Item>(record.getItems());
        super.delete(record, user);
        deleteSupplierReference(record);
        if (record.getReference() != null && purchaseOrders.exists(record.getReference())) {
            Order order = purchaseOrders.reopenOrder(record.getReference());
            items.addAll(new ArrayList<Item>(order.getItems()));
            items.addAll(new ArrayList<Item>(order.getDeliveries()));
        }
        createProductPlanningRefreshTask(record, items);
    }

    public void reopenClosed(Employee user, PurchaseInvoice invoice) {
        getInvoicesDao().reopenClosed(user, invoice);
        createProductPlanningRefreshTask(invoice, null);
    }

    protected void deleteSupplierReference(Record record) {
        ProjectSupplier ps = projectSuppliers.findByVoucher(
                record.getType().getId(), record.getId());
        if (ps != null) {
            projectSuppliers.remove(ps, false);
        }
    }

    @Override
    protected void doAfterStatusUpdate(Record record, Employee employee, Long oldStatus) {
        assert record instanceof PurchaseInvoice;
        PurchaseInvoice invoice = (PurchaseInvoice) record;
        if (invoice.getStatus() == Record.STAT_SENT &&
                (invoice.isCreditNote() || invoice.getReference() == null)) {
            invoice.updateStatus(Record.STAT_CLOSED);
            getInvoicesDao().save(invoice);
        } else if (invoice.getStatus() > Record.STAT_SENT) {
            if (!invoice.isCreditNote()) {
                if (log.isDebugEnabled()) {
                    log.debug("doAfterStatusUpdate() record was closed, "
                        + "performing actions [id=" + record.getId() + "]");
                }
                Map<Long, Product> productMap = new HashMap<Long, Product>();
                Map<Long, Item> items = new HashMap<Long, Item>();
                for (int i = 0, j = invoice.getItems().size(); i < j; i++) {
                    Item item = invoice.getItems().get(i);
                    Product product = getProducts().load(item.getProduct().getProductId());
                    productMap.put(product.getProductId(), product);
                    if (items.containsKey(product.getProductId())) {
                        Item existing = items.get(product.getProductId());
                        existing.setQuantity(existing.getQuantity() + item.getQuantity());
                    } else {
                        items.put(product.getProductId(), (Item) item.clone());
                    }
                }
                Order order = purchaseOrders.getOrder(invoice);
                if (order != null) {
                    for (int i = 0, j = order.getItems().size(); i < j; i++) {
                        Item next = order.getItems().get(i);
                        if (items.containsKey(next.getProduct().getProductId())) {
                            Item delivered = items.get(next.getProduct().getProductId());
                            next.setDelivered(delivered.getQuantity());
                        }
                    }
                    purchaseOrders.save(order);
                    if (log.isDebugEnabled()) {
                        log.debug("doAfterStatusUpdate() deliveries updated [order=" 
                                + order.getId() + "]");
                    }
                }
            } 
        }
    }

    private Item getExisting(PurchaseOrder order, Long productId) {
        for (int i = 0, j = order.getItems().size(); i < j; i++) {
            Item next = order.getItems().get(i);
            if (next.getProduct().getProductId().equals(productId)) {
                return next;
            }
        }
        return null;
    }

    private PurchaseInvoices getInvoicesDao() {
        return (PurchaseInvoices) getDao();
    }
}
