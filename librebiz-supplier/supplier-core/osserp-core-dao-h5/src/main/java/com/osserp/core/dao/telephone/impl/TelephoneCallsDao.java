/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 13, 2010 2:42:27 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.telephone.TelephoneCalls;
import com.osserp.core.model.telephone.TelephoneCallImpl;
import com.osserp.core.telephone.TelephoneCall;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneCallsDao extends AbstractDao implements TelephoneCalls {

    protected TelephoneCallsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public List<TelephoneCall> find(String uid) {
        if (uid != null) {
            return getCurrentSession().createQuery(
                    "from " + TelephoneCallImpl.class.getName()
                    + " tc where tc.uid = :uid order by tc.startedAt")
                    .setParameter("uid", uid).list();
        }
        return new ArrayList<TelephoneCall>();
    }

    public void save(TelephoneCall telephoneCall) {
        getCurrentSession().saveOrUpdate(TelephoneCallImpl.class.getName(), telephoneCall);
    }
}
