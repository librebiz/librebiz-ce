/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Feb-2013 07:12:04 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.records.CommonPayments;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.CommonPayment;
import com.osserp.core.finance.CommonPaymentManager;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CommonPaymentManagerImpl extends AbstractPaymentManager implements CommonPaymentManager {
    
    private RecordTypes recordTypes;

    protected CommonPaymentManagerImpl(CommonPayments payments, RecordTypes recordTypes) {
        super(payments);
        this.recordTypes = recordTypes;
    }

    public final List<BillingType> getSupportedTypes() {
        return recordTypes.getBillingTypes(BillingType.SIMPLE_BILLING);
    }

    public List<BillingType> findSupportedTypes(Supplier supplier) {
        List<BillingType> result = new ArrayList<>();
        List<BillingType> list = getSupportedTypes();
        for (int i = 0, j = list.size(); i < j; i++) {
            BillingType bt = list.get(i);
            if (supplier.getSupplierType().isSalaryAccount()
                    && isSet(bt.getResourceKey())
                    && bt.isSalary()) {
                result.add(bt);
            } else if (!supplier.getSupplierType().isSalaryAccount()
                    && isSet(bt.getResourceKey())
                    && !bt.isSalary()) {
                result.add(bt);
            }
        }
        return result;
    }

    public CommonPayment create(
            Employee user, 
            ClassifiedContact contact, 
            BranchOffice branch,
            BankAccount bankAccount, 
            Long paymentType, 
            Long currency, 
            BigDecimal amount,
            Date recordDate,
            Date paid, 
            String note,
            boolean reducedTax,
            boolean taxFree, 
            Long taxFreeId,
            boolean template,
            String templateName) throws ClientException {
        if (branch == null) {
            throw new IllegalStateException("branch must not be null");
        }
        if (bankAccount == null) {
            throw new IllegalStateException("bankAccount must not be null");
        }
        if (contact == null) {
            throw new IllegalStateException("contact must not be null");
        }
        if (isNotSet(amount)) {
            throw new ClientException(ErrorCode.AMOUNT_MISSING);
        }
        if (isNotSet(paymentType)) {
            throw new ClientException(ErrorCode.TYPE_MISSING);
        }
        return getCommonPayments().create(
                user, 
                contact, 
                branch.getCompany().getId(),
                branch.getId(),
                bankAccount.getId(), 
                paymentType, 
                currency, 
                amount, 
                recordDate,
                paid, 
                note,
                reducedTax,
                taxFree, 
                taxFreeId,
                template,
                templateName);
    }
    
    public CommonPayment update(
            Employee user, 
            CommonPayment payment, 
            Long paymentType, 
            Long currency, 
            BigDecimal amount, 
            Date recordDate, 
            Date paid,
            String note, 
            boolean reducedTax, 
            boolean taxFree, 
            Long taxFreeId, 
            boolean template, 
            String templateName) {
        return getCommonPayments().update(
                user, payment, paymentType, currency, amount, 
                recordDate, paid, note, reducedTax, 
                taxFree, taxFreeId, template, templateName);
    }

    public CommonPayment updateDocumentReference(Employee user, CommonPayment payment, Long id) {
        return (CommonPayment) getCommonPayments().updateDocumentReference((user == null ? null : user.getId()), payment, id);
    }

    public List<CommonPayment> getTemplates() {
        return getCommonPayments().getTemplates();
    }

    public void disableTemplate(CommonPayment template) {
        getCommonPayments().disableTemplate(template);
    }

    protected CommonPayments getCommonPayments() {
        return (CommonPayments) getPayments();
    }
}
