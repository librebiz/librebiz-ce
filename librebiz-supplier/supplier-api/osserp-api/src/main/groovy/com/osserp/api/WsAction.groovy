/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 27.10.2017
 * 
 */
package com.osserp.api

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@Entity
@Table(name='ws_action')
class WsAction extends WsObject {
    private static final long serialVersionUID = 4L

    WsAction() {
        super()
    }

    WsAction(String context, Long referenceId, String name, String value) {
        super()
        this.context = context
        this.referenceId = referenceId
        this.name = name
        this.value = value
        created = new Date(System.currentTimeMillis())
    }

    @Column(name='reference_id')
    Long referenceId

    @Column(name='context')
    String context

    @Column(name='name')
    String name
    
    @Column(name='val')
    String value

    @Column(name='created')
    Date created

    @Column(name='changed')
    Date changed

}
