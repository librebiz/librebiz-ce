/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 3, 2005 
 * 
 */
package com.osserp.gui.web.contacts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.contacts.ContactPersonView;
import com.osserp.gui.client.contacts.ContactView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactPersonAction extends ContactAction {
    private static Logger log = LoggerFactory.getLogger(ContactPersonAction.class.getName());

    /**
     * Creates a new contact person view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forward() request from " + getUser(session).getId());
        }
        ContactView view = (ContactView) fetchView(session, ContactView.class);
        if (view == null) {
            throw new ActionException("contactView not bound");
        }
        ContactPersonView personView = (ContactPersonView) createView(request);
        personView.load(view);
        return mapping.findForward(Actions.LIST);
    }

    /**
     * Creates a new contact person view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward load(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("load() request from " + getUser(session).getId());
        }
        ContactPersonView personView = (ContactPersonView) createView(request);
        personView.load(RequestUtil.fetchId(request));
        String exitTarget = RequestUtil.getExit(request);
        if (exitTarget != null) {
            personView.setExitTarget(exitTarget);
        }
        return mapping.findForward(personView.getActionTarget());
    }

    /**
     * Returns to list
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward exitDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitDisplay() request from " + getUser(session).getId());
        }
        ContactPersonView view = getPersonView(session);
        view.disableAllModes();
        if (view.getExitTarget() != null) {
            String exit = view.getExitTarget();
            if (view.isExitIdAvailable()) {
                RequestUtil.saveExitId(request, view.getExitId());
            }
            removeView(session);
            return mapping.findForward(exit);
        }
        removeView(session);
        return mapping.findForward(Actions.LIST);
    }

    @Override
    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("delete() request from " + getUser(session).getId());
        }
        ContactPersonView view = getPersonView(session);
        try {
            super.delete(mapping, form, request, response);
            view.refresh();
            return mapping.findForward(Actions.LIST);
        } catch (ClientException c) {
            saveError(request, c.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
    }

    @Override
    protected Class<ContactPersonView> getViewClass() {
        return ContactPersonView.class;
    }

    @Override
    protected ContactView fetchContactView(HttpSession session) {
        return (ContactView) fetchView(session, ContactPersonView.class);
    }

    @Override
    protected ContactView getContactView(HttpSession session) {
        ContactView view = fetchContactView(session);
        if (view == null) {
            log.warn("getContactView() failed, view not bound [class=" + ContactView.class.getName() + "]");
            throw new ActionException("contactView not bound");
        }
        return view;
    }

    protected ContactPersonView getPersonView(HttpSession session)
            throws PermissionException {
        ContactPersonView view = (ContactPersonView) fetchView(session);
        if (view == null) {
            log.warn("getPersonView() failed, view not bound [class=" + ContactPersonView.class.getName() + "]");
            throw new ActionException("contactPersonView not bound");
        }
        return view;
    }
}
