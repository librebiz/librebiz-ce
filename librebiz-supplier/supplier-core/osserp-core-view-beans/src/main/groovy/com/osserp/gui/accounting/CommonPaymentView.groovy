/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 28, 2015 
 * 
 */
package com.osserp.gui.accounting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsReference

import com.osserp.core.BankAccount
import com.osserp.core.finance.BillingType
import com.osserp.core.finance.CommonPaymentManager
import com.osserp.core.finance.CommonPayment
import com.osserp.core.finance.Record

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CommonPaymentView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(CommonPaymentView.class.getName())
    
    BankAccount bankAccount
    DmsDocument document
    List<BillingType> billingTypes = []
    
    CommonPaymentView() {
        super()
        providesEditMode()
        providesCustomNavigation()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            Long id = form.getLong('id')
            if (id) {
                CommonPayment payment = commonPaymentManager.getPayment(id)
                if (!payment) {
                    throw new ViewContextException('payment id not provided')
                }
                if (payment.bankAccountId) {
                    bankAccount = fetchBankAccount(payment.bankAccountId)
                }
                bean = payment
                billingTypes = commonPaymentManager.findSupportedTypes(payment.contact)
                logger.debug("initRequest: done [id=${payment.id}, supportedTypes=${billingTypes.size()}]")
            }
        }
        if (forwardRequest || reloadRequest) {
            loadReferencedDocument()
        }
    }
    
    @Override
    void save() {
        CommonPayment obj = bean
        if (obj) {
            boolean tpl = form.getBoolean('templateRecord')
            String templateName = null
            if (tpl) {
                templateName = form.getString('templateName')
                if (!templateName && tpl) {
                    tpl = false
                }
            }
            boolean taxFree = form.getBoolean('taxFree')
            Long taxFreeId = form.getLong('taxFreeId')
            if (taxFreeId && !taxFree) {
                throw new ClientException(ErrorCode.TAX_FREE_REASON_INVALID)
            }
            bean = commonPaymentManager.update(
                domainEmployee,
                obj,
                form.getLong('paymentType'),
                form.getLong('currency'),
                form.getDecimal('amount'),
                form.getDate('created'),
                form.getDate('paid'),
                form.getString('note'),
                form.getBoolean('reducedTax'),
                taxFree,
                taxFreeId,
                tpl,
                templateName)
        }
        disableEditMode()
    }
    
    @Override
    void createCustomNavigation() {
        if (document) {
            nav.beanNavigation = [exitLink, printLink, documentLink, editLink, homeLink]
        } else {
            nav.beanNavigation = [exitLink, documentLink, editLink, homeLink]
        }
    }
    
    protected MenuItem getDocumentLink() {
        new MenuItem(link: "${context.fullPath}/accounting/paymentDocument/forward?exit=/accounting/commonPayment/reload", icon: 'docIcon', title: 'commonPaymentDocuments')
    }
    
    @Override
    MenuItem getPrintLink() {
        new MenuItem(link: "${context.fullPath}/dms/document/print?id=${document.id}", icon: 'printIcon', title: 'displayDocument')
    }
    
    @Override
    MenuItem getEditLink() {
        MenuItem clnk = super.getEditLink()
        clnk.permissions = AbstractAccountingView.TRANSACTION_CREATE_PERMISSIONS
        return clnk
    }

    protected void loadReferencedDocument() {
        if (bean) {
            document = dmsManager.findReferenced(new DmsReference(CommonPayment.DOCUMENT_TYPE, bean.id))
            logger.debug("loadReferencedDocument() lookup done [found=${document != null}]")
        }
    }

    protected BankAccount fetchBankAccount(Long id) {
        try {
            return systemConfigManager.getBankAccount(id)
        } catch (Exception e) {
            logger.warn("fetchBankAccount: ignoring exception [id=${id}, message=${e.message}]")
        }
    }

    protected CommonPaymentManager getCommonPaymentManager() {
        getService(CommonPaymentManager.class.getName())
    }

    protected DmsManager getDmsManager() {
        getService(DmsManager.class.getName())
    }
}
