/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2016 
 * 
 */
package com.osserp.gui.admin.requests

import java.util.List;

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.Option

import com.osserp.core.FcsAction
import com.osserp.core.requests.RequestFcsActionManager

import com.osserp.gui.admin.fcs.FlowControlConfigAwareView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RequestFcsActionConfigView extends FlowControlConfigAwareView {

    String createModeContext
    List<Option> freeStatusList = []
    
    RequestFcsActionConfigView() {
        super()
        providesCreateMode()
        providesEditMode()
        headerName  = 'requestContextActionsLabel'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            list = requestFcsActionManager.findAll()
        }
    }
    
    void setCreateModeContext() {
        createModeContext = getString('contextName')
        if ('action' == createModeContext) {
            freeStatusList = requestFcsActionManager.freeActionStatus
        } else if ('cancellation' == createModeContext) {
            freeStatusList = requestFcsActionManager.freeCancellationStatus
        } else {
            freeStatusList = []
        }
    }
    
    @Override
    public void disableCreateMode() {
        super.disableCreateMode();
        freeStatusList = []
        createModeContext = null
    }

    void save() {

        String name = form.getString('name')
        String description = form.getString('description')
        Long groupId = form.getLong('groupId')
        Long status = getLong('status')
        String statusName = form.getString('statusName')
        
        if (isCreateMode()) {

            list = requestFcsActionManager.createAction(
                name, 
                description, 
                groupId, 
                status, 
                statusName)
            disableCreateMode()
            
        } else {
            FcsAction action = bean
            if (!name) {
                throw new ClientException(ErrorCode.NAME_MISSING)
            }
            list.each { FcsAction next ->
                if (next.name.equalsIgnoreCase(name) && next.id != action.id) {
                    throw new ClientException(ErrorCode.NAME_EXISTS)
                } 
            }
            action.setName(name)
            action.setDescription(description)
            action.setGroupId(groupId)
            requestFcsActionManager.update(action)

            disableEditMode()
            reload()
        }
    }

    protected RequestFcsActionManager getRequestFcsActionManager() {
        getService(RequestFcsActionManager.class.getName())
    }
}
