<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<c:set var="view" value="${sessionScope.businessCaseView}" />
<c:if test="${view.salesContext}">
    <c:set var="project" value="${sessionScope.businessCaseView.sales}" />
</c:if>
<c:set var="plan" value="${sessionScope.businessCaseView.request}" />
<c:set var="otherSales" value="${view.userForeignSales}" />

<c:choose>
    <c:when test="${view.nameEditMode}">
        <tr>
            <o:form name="requestForm" url="/requests.do">
                <input type="hidden" name="method" value="updateName" />
                <td style="padding-left: 15px;"><fmt:message key="consignment" /></td>
                <td><input type="text" name="value" value="<o:out value="${plan.name}"/>" style="width: 80%;" /> <a href="<c:url value="/requests.do?method=disableNameEditMode"/>"><img src="<c:url value="${applicationScope.backIcon}"/>" style="position: relative; left: 5px;" class="bigicon" /></a> <input type="image" name="method"
                    src="<c:url value="${applicationScope.saveIcon}"/>" value="updateName" onclick="setMethod('updateName');" style="position: relative; left: 10px" class="bigicon" /></td>
            </o:form>
        </tr>
    </c:when>
    <c:otherwise>
        <tr>
            <td style="vertical-align: top;"><c:choose>
                    <c:when test="${otherSales or plan.cancelled or (!empty project and (project.closed or project.cancelled))}">
                        <fmt:message key="consignment" />
                    </c:when>
                    <c:otherwise>
                        <a href="<c:url value="/requests.do?method=enableNameEditMode"/>" title="<fmt:message key="changeConsignmentName"/>"><fmt:message key="consignment" /></a>
                    </c:otherwise>
                </c:choose></td>
            <td><o:out value="${plan.name}" /></td>
        </tr>
    </c:otherwise>
</c:choose>
