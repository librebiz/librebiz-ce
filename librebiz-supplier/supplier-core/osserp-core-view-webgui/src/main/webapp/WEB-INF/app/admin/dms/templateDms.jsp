<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>

<v:view viewName="templateDmsView"/>
<c:set var="templateView" scope="request" value="${view}"/>
<c:set var="templateDocumentUrl" scope="request" value="/admin/dms/templateDms"/>
<c:set var="templateDeletePermissions" scope="request" value="executive,template_admin"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="headline">
		<fmt:message key="stylesheetsCustom"/>
        <c:if test="${!empty view.contextName}"> - <fmt:message key="${view.contextName}"/></c:if>
	</tiles:put>
	<tiles:put name="headline_right">
        <v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area" id="templateContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.createMode}">
                        <c:import url="_templateCreate.jsp"/>
                    </c:when>
                    <c:when test="${view.editMode}">
                        <c:import url="_templateEdit.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_templateList.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
