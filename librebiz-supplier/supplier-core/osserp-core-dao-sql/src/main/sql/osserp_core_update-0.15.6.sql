SET search_path = osserp, pg_catalog;

CREATE OR REPLACE VIEW osserp.v_sales_delivery_list AS
 SELECT sl.salutation,
    sl.firstname,
    sl.lastname,
    sl.contact_type,
    sl.id,
    sl.plan_id,
    sl.type_id,
    sl.customer_id,
    sl.status,
    sl.created,
    sl.created_by,
    sl.manager_id,
    sl.manager_sub_id,
    sl.sales_id,
    sl.branch_id,
    sl.name,
    sl.city,
    sl.plant_capacity,
    sl.stopped,
    sl.cancelled,
    sl.installer_id,
    sl.installation_days,
    sl.installation_date,
    sl.delivery_date,
    sl.confirmation_date,
    sl.origin_id,
    sl.origin_type_id,
    sl.company_id,
    sl.request_created,
    sl.street,
    sl.zipcode,
    sl.accounting_ref,
    sl.shipping_id,
    sl.payment_id,
    so.product AS product_id,
    so.name AS product_name,
    so.quantity,
    so.delivery,
    so.delivered,
    so.stock_id
   FROM osserp.v_sales_list sl,
    osserp.v_sales_order_products so
  WHERE ((sl.id = so.sales) AND (so.delivery IS NOT NULL) AND (sl.status < 100) AND (sl.cancelled = false) AND (sl.stopped = false)
   AND (sl.id IN
    (SELECT pf.project_id
           FROM osserp.project_fcs pf
          WHERE ((pf.cancels = false) AND (pf.canceled_by IS NULL) AND (pf.fcs_id IN ( SELECT pfa.id
                   FROM osserp.project_fcs_actions pfa
                  WHERE (pfa.enabling_delivery = true)))))) AND (NOT (sl.id IN ( SELECT pf.project_id
           FROM osserp.project_fcs pf
          WHERE ((pf.cancels = false) AND (pf.canceled_by IS NULL) AND (pf.fcs_id IN ( SELECT pfa.id
                   FROM osserp.project_fcs_actions pfa
                  WHERE (pfa.closing_delivery = true))))))))
  ORDER BY sl.type_id, sl.id;

INSERT INTO system_properties VALUES (224, 'calendarTextLength', '25', 'java.lang.Integer', true, false, null, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 224, true);

UPDATE calculation_configs SET name = 'Standard-Kalkulation' WHERE id = 1;
UPDATE calculation_configs SET name = 'Standard-Stückliste' WHERE id = 2;

ALTER TABLE record_delivery_note_types ADD COLUMN parent_content boolean DEFAULT false;
ALTER TABLE record_delivery_note_types ADD COLUMN content_strict boolean DEFAULT false;
ALTER TABLE record_delivery_note_types ADD COLUMN manual_selection boolean DEFAULT false;
ALTER TABLE record_delivery_note_types ADD COLUMN is_disabled boolean DEFAULT false;
ALTER TABLE record_delivery_note_types ADD COLUMN product_selection_id bigint REFERENCES product_selection_configs (id);
UPDATE record_delivery_note_types SET parent_content = true, content_strict = true;
UPDATE record_delivery_note_types SET parent_content = false WHERE id IN (10);
UPDATE record_delivery_note_types SET manual_selection = true WHERE id IN (1,10);

ALTER TABLE request_types ADD COLUMN custom_delivery_support boolean DEFAULT false;
ALTER TABLE request_types ADD COLUMN custom_delivery_type_id bigint REFERENCES record_delivery_note_types (id);
UPDATE request_types SET custom_delivery_type_id = 10 WHERE id = 4;
-- Enable for projects example:
-- UPDATE request_types SET custom_delivery_support = true, custom_delivery_type_id = 10 WHERE id = 4;

UPDATE system_properties SET pvalue = '0.15.6', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
