/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 15.10.2004 
 * 
 */
package com.osserp.gui.client;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import com.osserp.common.service.ContextCreator;
import com.osserp.common.web.ContextUtil;
import com.osserp.common.web.WebConfig;
import com.osserp.common.web.tags.BaseTag;

import com.osserp.core.Plugins;
import com.osserp.core.system.SystemConfig;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContextManager {
    private static Logger log = LoggerFactory.getLogger(ContextManager.class.getName());

    /**
     * Provides the application config
     * @return config
     */
    public static WebConfig getConfig(ServletContext ctx) {
        return ContextUtil.getConfig(ctx);
    }

    /**
     * Creates the client application context
     * @param ctx
     */
    public static void createApplicationContext(ServletContext ctx) {
        try {
            ApplicationContext appCtx = ContextCreator.createApplicationContext(
                    "/com/osserp/gui/resources/client-context.xml");
            ctx.setAttribute(CoreContext.APPLICATION_CONTEXT, appCtx);
        } catch (Throwable t) {
            String message = t.toString();
            log.error("createApplicationContext() failed: " + message, t);
            if (ctx != null) {
                ctx.setAttribute("globalApplicationErrorClient", "Application not initialized: " + message);
            }
        }
    }

    /**
     * Destroys the client application context
     * @param ctx
     */
    public static void destroyApplicationContext(ServletContext ctx) {
        try {
            ApplicationContext appCtx = (ApplicationContext) ctx.getAttribute(CoreContext.APPLICATION_CONTEXT);
            if (appCtx instanceof ConfigurableApplicationContext) {
                ((ConfigurableApplicationContext) appCtx).close();
            }
            ctx.removeAttribute(CoreContext.APPLICATION_CONTEXT);
        } catch (Throwable t) {
            log.error("destroyApplicationContext() failed: " + t.toString(), t);
        }
    }

    /**
     * Creates the web config
     * @param ctx
     */
    public static void createConfig(ServletContext ctx) {
        ApplicationContext appCtx = (ApplicationContext) ctx.getAttribute(CoreContext.APPLICATION_CONTEXT);
        WebConfig cfg = appCtx == null ? null : (WebConfig) appCtx.getBean(WebConfig.class.getName());
        if (cfg != null) {
            try {
                SystemConfigManager systemConfigManager = (SystemConfigManager) 
                        getService(ctx, SystemConfigManager.class.getName());
                String appLabel = systemConfigManager.getSystemProperty(SystemConfig.APP_LABEL_PROPERTY);
                if (appLabel != null && appLabel.length() > 0) {
                    cfg.setDisplayName(appLabel);
                }
            } catch (Exception e) {
                log.warn("createConfig() ignoring failed attempt to fetch systemProperty [message="
                        + e.getMessage() + "]", e);

            }
            ContextUtil.setConfig(ctx, cfg);
        } else {
            log.warn("createConfig() ignored - application context not available [attribute="
                    + CoreContext.APPLICATION_CONTEXT + "]");
        }
    }

    public static void loadPageDefaults(ServletContext ctx) {
        try {
            SystemConfigManager systemConfigManager = (SystemConfigManager)
                    getService(ctx, SystemConfigManager.class.getName());
            String popupPageSize = systemConfigManager.getSystemProperty(SystemConfig.POPUP_PAGE_SIZE);
            if (popupPageSize == null || popupPageSize.length() < 14) {
                popupPageSize = "width=1024,height=768";
            }
            ctx.setAttribute(SystemConfig.POPUP_PAGE_SIZE, popupPageSize);

            String baseTagEnabled = systemConfigManager.getSystemProperty(SystemConfig.BASE_TAG_PROPERTY);
            if (baseTagEnabled == null) {
                baseTagEnabled = "false";
            }
            ctx.setAttribute(BaseTag.BASE_TAG_ENABLED, baseTagEnabled);

        } catch (Exception e) {
            log.warn("loadPageDefaults() failed [message=" + e.getMessage() + "]", e);
        }
    }

    public static void loadPlugins(ServletContext ctx) {
        try {
            SystemConfigManager systemConfigManager = (SystemConfigManager)
                    getService(ctx, SystemConfigManager.class.getName());
            Plugins plugins = systemConfigManager.getActivePlugins();
            ctx.setAttribute(SystemConfig.APP_PLUGINS_PROPERTY, plugins);
            if (log.isDebugEnabled()) {
                log.debug("loadPlugins() done [count=" + plugins.getMap().size() + "]");
            }
        } catch (Exception e) {
            log.warn("loadPlugins() failed [message=" + e.getMessage() + "]", e);
        }
    }

    public static boolean isPropertyEnabled(ServletContext ctx, String propertyName) {
        try {
            SystemConfigManager systemConfigManager = (SystemConfigManager)
                    getService(ctx, SystemConfigManager.class.getName());
            return systemConfigManager.isSystemPropertyEnabled(propertyName);
        } catch (Exception e) {
            log.warn("isPropertyEnabled() failed [message=" + e.getMessage() + "]", e);
        }
        return false;
    }

    protected static final Object getService(ServletContext ctx, String name) {
        return ContextUtil.getService(ctx, name);
    }
}
