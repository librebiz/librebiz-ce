/**
 *
 * Copyright (C) 2010, 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 19, 2013
 *  
 */
package com.osserp.groovy.web

import javax.servlet.FilterConfig
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.web.context.WebApplicationContext

import com.osserp.common.TimeoutException
import com.osserp.common.service.Locator
import com.osserp.common.web.AbstractPortalView
import com.osserp.common.web.ContextUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class AbstractViewContextListener extends AbstractHttpFilter {
    private static Logger logger = LoggerFactory.getLogger(AbstractViewContextListener.class.getName())
    private static final String VIEW_SERVLET_NAME = 'org.osserp.mvc.view.servlet.NAME'
    private String resolvedViewServletName

    String timeoutExceptionTarget
    String popupTimeoutExceptionTarget

    @Override
    public void init(FilterConfig config) {
        super.init(config)
        timeoutExceptionTarget = config.getInitParameter('timeoutExceptionTarget')
        popupTimeoutExceptionTarget = config.getInitParameter('popupTimeoutExceptionTarget')
    }

    protected void checkUser(View view) {
        if (!view || (view.userRequired && !view.user)) {
            if (logger.isDebugEnabled()) {
                logger.debug('checkUser: User required but not available, throwing timeout...')
            }
            throw new TimeoutException()
        }
    }

    protected boolean authenticationRequired(View view) {
        if (view && view.userRequired && !view.user) {
            return true
        }
        return false
    }

    protected final Boolean isPotentialViewRequest(String uri) {
        (uri.indexOf("png") == -1
                && uri.indexOf("gif") == -1
                && uri.indexOf("bmp") == -1
                && uri.indexOf("ico") == -1
                && uri.indexOf("jpg") == -1
                && uri.indexOf("js") == -1
                && uri.indexOf("css") == -1
                && uri.indexOf("svg") == -1
                && uri.indexOf("swf") == -1
                && uri.indexOf("wof") == -1)
    }

    protected final View createView(HttpServletRequest request, Map viewparams, String servletContextName) {
        View view = null
        def servletContext = request.getSession().servletContext
        if (!resolvedViewServletName) {
            resolvedViewServletName = (servletContext.getInitParameter(VIEW_SERVLET_NAME) ?: servletContextName)
            if (logger.isDebugEnabled()) {
                logger.debug("createView: viewServlet lookup name created [name=${resolvedViewServletName}]")
            }
        }
        WebApplicationContext ctx = WebUtil.getWebApplicationContext(servletContext, resolvedViewServletName)
        if (ctx) {
            String ctlName = "${viewparams.viewName}Controller"
            String[] list = ctx.getBeanDefinitionNames()
            String viewName
            list.each {
                if (it == ctlName) {
                    def ctlClassName = ctx.getBean(ctlName).getClass().getName()
                    viewName = "${ctlClassName.substring(0, ctlClassName.indexOf('Controller'))}View"
                    if (logger.isDebugEnabled()) {
                        logger.debug("createView: view name created [name=$viewName]")
                    }
                    try {
                        Class c = Class.forName(viewName)
                        view = c.getDeclaredConstructor().newInstance()

                        view.context.homeTarget = WebUtil.getViewHome(servletContext)
                        view.context.appPath = WebUtil.getViewServletPath(servletContext)
                        def path = WebUtil.getViewPath(servletContext, request)
                        view.context.fullPath = path
                        view.context.contextPath = request.contextPath
                        view.context.name = viewparams.contextPath.substring(path.length()+1, viewparams.contextPath.length())
                        if (logger.isDebugEnabled()) {
                            logger.debug("createView: view initialized [name=${view.name}, context.name=${view.context.name}, context.contextPath=${view.context.contextPath}, context.appPath=${view.context.appPath}, context.fullPath=${view.context.fullPath}]")
                        }
                    } catch (IOException e) {
                        logger.warn("createView: caught exception [message=${e.message}, class=${e.getClass().getName()}]", e)
                    } catch (ClassNotFoundException e) {
                        logger.debug("createView: view does not exist [class=${viewName}]")
                    } catch (ClassCastException e) {
                        logger.warn("createView: caught exception [message=${e.message}, class=${e.getClass().getName()}]", e)
                    } catch (InstantiationException e) {
                        logger.warn("createView: caught exception [message=${e.message}, class=${e.getClass().getName()}]", e)
                    } catch (IllegalAccessException e) {
                        logger.warn("createView: caught exception [message=${e.message}, class=${e.getClass().getName()}]", e)
                    }
                }
            }
        }
        view?.requestContext = viewparams
        view?.user = request.getSession().getAttribute('user')
        return view
    }

    protected final void initRequest(View view, HttpServletRequest request) throws ViewContextException {
        if (view) {
            HttpSession session = request.session
            Locator locator = ContextUtil.getServiceLocator(session)
            injectLocator(view, locator)
            if (view.dependencies) {
                logger.debug("initRequest: view has dependencies [count=${view.dependencies.size()}]")
                view.dependencies.each {
                    def obj = session.getAttribute(it)
                    if (!obj) {
                        obj = request.getAttribute(it)
                    }
                    if (obj) {
                        if (locator) { 
                            injectLocator(obj, locator)
                        }
                        view.env[it] = obj
                        logger.debug("initRequest: dependency lookup done [name=${it}, found=true, class=${obj.getClass().getName()}]")
                    } else {
                        logger.debug("initRequest: dependency lookup done [name=${it}, found=false]")
                    }
                }
            }

            String viewReference = request.getParameter('view')
            if (viewReference) {
                logger.debug("initRequest: found view parameter [view=${viewReference}]")
                def obj = session.getAttribute(viewReference)
                logger.debug("initRequest: lookup for view [name=${viewReference}, found=${(obj != null)}]")
                if (obj) {
                    injectLocator(obj, locator)
                    view.env['viewReferenceName'] = viewReference
                    view.env[viewReference] = obj
                    logger.debug("initRequest: added view [name=${viewReference}, class=${obj.getClass().getName()}]")
                }
            }

            def portalView = getPortalView(request)
            if (portalView) {
                if (!portalView.webConfig) {
                    portalView.webConfig = WebUtil.fetchWebConfig(session)
                }
                if (portalView instanceof AbstractPortalView) {
                    ((AbstractPortalView) portalView).initRequest(request)
                }
                view.env.portalView = portalView
                view.env.lastPage = portalView.page?.name
                logger.debug("initRequest: last requested page added [name=${view.env.lastPage}]")
            }
        }
        try {
            view?.initRequest(
                getRequestAttributes(request),
                getRequestHeaders(request),
                getRequestParameters(request, view?.uploadDir))

        } catch (TimeoutException t) {
            logger.warn("view.initRequest failed [name=${view?.name}, message=${t.message}]")
            throw new ViewContextException(t.message)
        } catch (Exception e) {
            logger.warn("view.initRequest failed [name=${view?.name}, message=${e.message}]", e)
            throw new ViewContextException(e.message)
        }
    }

    protected void injectLocator(Object view, Locator locator) {
        if (locator && view instanceof AbstractView) {
            view.setLocator(locator)
        }
    }

    protected Object getPortalView(HttpServletRequest request) {
        request.session.getAttribute('portalView')
    }
}
