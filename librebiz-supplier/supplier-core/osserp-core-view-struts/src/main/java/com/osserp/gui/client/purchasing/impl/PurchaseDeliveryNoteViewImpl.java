/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 25, 2006 11:03:35 AM 
 * 
 */
package com.osserp.gui.client.purchasing.impl;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.purchasing.PurchaseDeliveryNote;
import com.osserp.core.purchasing.PurchaseDeliveryNoteManager;

import com.osserp.gui.client.purchasing.PurchaseDeliveryNoteView;
import com.osserp.gui.client.records.impl.AbstractDeliveryNoteView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("purchaseDeliveryNoteView")
public class PurchaseDeliveryNoteViewImpl extends AbstractDeliveryNoteView implements PurchaseDeliveryNoteView {

    private boolean provideOrderLink = false;

    protected PurchaseDeliveryNoteViewImpl() {
        super();
        setPriceInputEnabled(false);
    }

    protected PurchaseDeliveryNoteViewImpl(String orderViewKey) {
        super(orderViewKey, false, null);
        setPriceInputEnabled(false);
    }

    public void release() throws ClientException, PermissionException {
        RecordManager manager = getRecordManager();
        DeliveryNote record = (DeliveryNote) getRecord();
        if (record.getItems().isEmpty()) {
            throw new ClientException(ErrorCode.ITEM_MISSING);
        }
        if (!record.isSerialsCompleted()) {
            throw new ClientException(ErrorCode.SERIALS_MISSING);
        }
        manager.updateStatus(
                getRecord(),
                getDomainUser().getEmployee(),
                Invoice.STAT_SENT);
    }

    public void openClosed() throws PermissionException {
        PurchaseDeliveryNote record = (PurchaseDeliveryNote) getRecord();
        PurchaseDeliveryNoteManager manager = (PurchaseDeliveryNoteManager) getRecordManager();
        manager.reopenClosed(getDomainEmployee(), record);
        setSetupMode(false);
    }

    public boolean isProvideOrderLink() {
        return provideOrderLink;
    }

    protected void setProvideOrderLink(boolean provideOrderLink) {
        this.provideOrderLink = provideOrderLink;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        provideOrderLink = RequestUtil.getBoolean(request, "provideOrderLink");
    }

    @Override
    public boolean isProvidingStatusHistory() {
        return false;
    }

    @Override
    public boolean isSalesRecordView() {
        return false;
    }

    public boolean isSupplierReferenceNumberEditModeDisplayedWhenClosed() {
        return true;
    }

    @Override
    public RecordManager getRecordManager() {
        return (PurchaseDeliveryNoteManager) getService(PurchaseDeliveryNoteManager.class.getName());
    }
}
