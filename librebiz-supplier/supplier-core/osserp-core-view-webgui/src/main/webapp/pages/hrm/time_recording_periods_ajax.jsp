<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="recordingView" value="${sessionScope.timeRecordingView}"/>

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="timerecording.periodConfig"/>
    </div>
</div>
<div class="modalBoxData">
    <div class="col-md-12 panel-area">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><fmt:message key="id"/></th>
                        <th><fmt:message key="profile"/></th>
                        <th><fmt:message key="validFrom"/></th>
                        <th><fmt:message key="validUntil"/></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="period" items="${recordingView.availablePeriods}">
                        <tr>
                            <td><o:ajaxLink  url="/timeRecordings.do?method=selectPeriod&id=${period.id}"><o:out value="${period.id}"/></o:ajaxLink></td>
                            <td><o:out value="${period.config.name}"/></td>
                            <td><o:date value="${period.validFrom}"/></td>
                            <td>
                                <c:choose>
                                    <c:when test="${empty period.validTil}">
                                        <fmt:message key="unlimited"/>
                                    </c:when>
                                    <c:otherwise>
                                        <o:date value="${period.validTil}"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td> </td>
                        <td colspan="3">
                            <o:ajaxLink  url="/timeRecordings.do?method=createPeriod">
                                <fmt:message key="timerecording.createPeriod"/>
                            </o:ajaxLink>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
