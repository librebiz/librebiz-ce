/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2013 
 * 
 */
package com.osserp.core.model.system;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.system.NocClient;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class NocClientImpl extends AbstractOption implements NocClient {

    private boolean defaultClient = false;
    private boolean directorySupport = false;

    private boolean groupwareSupport = false;
    private String groupwareName;
    private boolean groupwareLocal = false;

    private boolean maildomainSupport = false;
    private boolean maildomainLocal = false;

    protected NocClientImpl() {
        super();
    }

    public NocClientImpl(String name) {
        super(name);
    }

    public boolean isDefaultClient() {
        return defaultClient;
    }

    public void setDefaultClient(boolean defaultClient) {
        this.defaultClient = defaultClient;
    }

    public boolean isDirectorySupport() {
        return directorySupport;
    }

    public void setDirectorySupport(boolean directorySupport) {
        this.directorySupport = directorySupport;
    }

    public boolean isGroupwareSupport() {
        return groupwareSupport;
    }

    public void setGroupwareSupport(boolean groupwareSupport) {
        this.groupwareSupport = groupwareSupport;
    }

    public String getGroupwareName() {
        return groupwareName;
    }

    public void setGroupwareName(String groupwareName) {
        this.groupwareName = groupwareName;
    }

    public boolean isGroupwareLocal() {
        return groupwareLocal;
    }

    public void setGroupwareLocal(boolean groupwareLocal) {
        this.groupwareLocal = groupwareLocal;
    }

    public boolean isMaildomainSupport() {
        return maildomainSupport;
    }

    public void setMaildomainSupport(boolean maildomainSupport) {
        this.maildomainSupport = maildomainSupport;
    }

    public boolean isMaildomainLocal() {
        return maildomainLocal;
    }

    public void setMaildomainLocal(boolean maildomainLocal) {
        this.maildomainLocal = maildomainLocal;
    }
}
