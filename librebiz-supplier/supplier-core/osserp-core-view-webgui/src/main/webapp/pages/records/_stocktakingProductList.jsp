<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.stocktakingView}" />
<div class="col-md-12 panel-area">

    <div class="table-responsive table-responsive-default">
        <table class="table">
            <thead>
                <tr>
                    <th class="productNumber"><fmt:message key="number" /></th>
                    <th class="productName"><fmt:message key="label" /></th>
                    <th class="productGroup" colspan="3"><fmt:message key="group" /></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${view.bean.closed}">
                        <c:forEach var="item" items="${view.bean.items}" begin="${view.listStart}" end="${view.listEnd}" varStatus="s">
                            <tr>
                                <td class="productNumber"><a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=stocktaking"/>" title="<fmt:message key="loadProduct"/>"> <o:out value="${item.product.productId}" /></a></td>
                                <td class="productName"><o:out value="${item.product.name}" /></td>
                                <td class="productGroup" colspan="3"><o:out value="${item.product.group.name}" limit="50" /></td>
                            </tr>
                            <tr>
                                <td class="bottom"></td>
                                <td class="bottom">
                                    <c:choose>
                                        <c:when test="${!empty item.product.description}">
                                            <o:out value="${item.product.description}" />
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${noDesc}" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="bottom center">
                                    <input type="hidden" name="items" value="<o:out value="${item.id}"/>" />
                                </td>
                                <td class="bottom center">
                                    <input type="text" name="dummy" readonly="readonly" value="<o:number value="${item.expectedQuantity}" format="integer"/>" class="input-count" />
                                </td>
                                <td class="bottom center">
                                    <input type="text" name="counters" readonly="readonly" value="<o:number value="${item.countedQuantity}" format="integer"/>" class="input-count" />
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <o:form name="stocktakingForm" url="/stocktakings.do">
                            <input type="hidden" name="method" value="updateProducts" />
                            <c:forEach var="item" items="${view.bean.items}" begin="${view.listStart}" end="${view.listEnd}" varStatus="s">
                                <tr>
                                    <td class="productNumber">
                                        <a href="<c:url value="/products.do"><c:param name="method" value="load"/><c:param name="id" value="${item.product.productId}"/><c:param name="exit" value="stocktaking"/></c:url>" title="<fmt:message key="loadProduct"/>">
                                            <o:out value="${item.product.productId}" />
                                        </a>
                                    </td>
                                    <td class="productName"><o:out value="${item.product.name}" /></td>
                                    <td class="productGroup"><o:out value="${item.product.group.name}" limit="50" /></td>
                                </tr>
                                <tr>
                                    <td class="bottom"></td>
                                    <td class="bottom">
                                        <c:choose>
                                            <c:when test="${!empty item.product.description}">
                                                <o:out value="${item.product.description}" />
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${noDesc}" />
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="bottom">
                                        <input type="hidden" name="items" value="<o:out value="${item.id}"/>" />
                                        <input type="text" name="dummy" readonly="readonly" value="<o:number value="${item.expectedQuantity}" format="integer"/>" class="input-count" />
                                    </td>
                                    <td class="bottom center">
                                        <input type="text" name="counters" value="<o:number value="${item.countedQuantity}" format="integer"/>" class="input-count" />
                                    </td>
                                    <td class="bottom center">
                                        <input type="image" name="submit" src="<c:url value="${applicationScope.saveIcon}"/>" class="bigicon save" />
                                    </td>
                                </tr>
                            </c:forEach>
                        </o:form>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
