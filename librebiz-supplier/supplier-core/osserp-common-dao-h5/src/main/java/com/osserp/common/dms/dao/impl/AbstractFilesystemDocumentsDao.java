/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25 Mar 2007 09:33:29 
 * 
 */
package com.osserp.common.dms.dao.impl;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsUtil;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.DocumentReference;
import com.osserp.common.service.SecurityService;
import com.osserp.common.util.FileUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFilesystemDocumentsDao extends AbstractDocumentsDao {

    private static Logger log = LoggerFactory.getLogger(AbstractFilesystemDocumentsDao.class.getName());

    /**
     * Provides a document dao using filesystem as storage.
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param documentTypeId
     * @param documentTableKey
     * @param categoryTableKey
     * @param documentEncoder
     * @param authenticationProvider
     */
    protected AbstractFilesystemDocumentsDao(
            JdbcTemplate jdbcTemplate, 
            Tables tables, 
            SessionFactory sessionFactory,
            Long documentTypeId,
            String documentTableKey, 
            String categoryTableKey, 
            DocumentEncoder documentEncoder,
            SecurityService authenticationProvider) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                documentTypeId,
                documentTableKey,
                categoryTableKey,
                new FilesystemDocumentStorage(),
                documentEncoder,
                authenticationProvider);
    }

    /**
     * Creates the absolute filesystem path and filename used to persist and
     * to retrieve a document.
     *
     * The default implementation creates the filename by a concatenation of
     * type.uploadPath + document.realFileName
     * Override this method to provide an alternate path and name.
     *
     * @param document
     * @return default filename by concatenation of type.uploadPath + document
     */
    protected String createFilesystemFilename(DmsDocument document) {
        return DmsUtil.createAbsolutePath(
                document.getType().getUploadPath(), document.getRealFileName());
    }

    /**
     * Creates the absolute filesystem path and filename used to persist and
     * to retrieve a thumbnail.
     *
     * The default implementation creates the filename by a concatenation of
     * type.uploadPath + 'thumbnail.' + document.realFileName
     * Override this method to provide an alternate path and name.
     *
     * @param document
     * @return default filename by concatenation of type.uploadPath + document
     */
    protected String createThumbnailFilesystemFilename(DmsDocument document) {
        return DmsUtil.createAbsolutePath(document.getType().getUploadPath(),
                "thumbnail." + document.getRealFileName());
    }

    public void createThumbnails(DocumentType documentType, boolean forceOverride) {
        if (documentType != null && documentType.isCreateThumbmail()) {
            List<DmsDocument> images = findByType(documentType);
            if (!images.isEmpty()) {
                int height = DmsUtil.getThumbnailHeight(documentType);
                int width = DmsUtil.getThumbnailWidth(documentType);
                images.forEach(image -> {
                    if (!image.isThumbnailAvailable() || forceOverride) {
                        createThumbnail(image, height, width);
                    }
                });
            } else {
                log.info("createThumbnails: No pics found for document type "
                        + documentType.getId());
            }
        }
    }

    @Override
    public DmsDocument createThumbnail(DmsDocument image) {
        return createThumbnail(image,
                DmsUtil.getThumbnailHeight(image.getType()),
                DmsUtil.getThumbnailWidth(image.getType()));
    }

    protected DmsDocument createThumbnail(DmsDocument image, int height, int width) {
        File source = new File(createFilesystemFilename(image));
        if (source.exists() && FileUtil.createThumbnail(source, height, width)) {
            File tb = new File(createThumbnailFilesystemFilename(image));
            if (tb.exists()) {
                image.setThumbnailAvailable(true);
                save(image);
            }
        }
        return image;
    }

    @Override
    protected DocumentReference getDocumentReference(DmsDocument document) {
        return new FilesystemDocumentReference(createFilesystemFilename(document));
    }

    @Override
    protected DocumentReference getThumbnailReference(DmsDocument document) {
        return new FilesystemDocumentReference(createThumbnailFilesystemFilename(document));
    }
}
