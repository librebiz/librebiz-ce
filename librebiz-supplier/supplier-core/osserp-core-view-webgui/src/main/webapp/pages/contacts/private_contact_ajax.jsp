<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.privateContactsView}"/>
<c:set var="obj" value="${view.bean}"/>

<div class="row row-modal">
    <div class="col-md-12 panel-area panel-area-modal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><o:out value="${obj.contact.displayName}"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <c:choose>
                <c:when test="${view.editMode}">
                    <o:ajaxForm name="dynamicForm" targetElement="privateContactDisplay_popup" url="/privateContacts.do">
                        <input type="hidden" name="method" value="save" />
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="category"><fmt:message key="type"/></label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" name="type" class="form-control" value="<o:out value="${obj.contactType}"/>"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="note"><fmt:message key="note"/></label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <textarea class="form-control" name="info" rows="3"><o:textOut value="${obj.contactInfo}"/></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row next">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <o:ajaxLink url="/privateContacts.do?method=disableEdit" linkId="editLink" targetElement="privateContactDisplay_popup">
                                        <input type="button" class="form-control cancel" value="<fmt:message key="exit"/>"/>
                                    </o:ajaxLink>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" value="save" />
                                </div>
                            </div>
                        </div>
                    </o:ajaxForm>    
                </c:when>
                <c:when test="${view.createMode}">
                    <o:ajaxForm name="dynamicForm" targetElement="privateContactDisplay_popup" url="/privateContacts.do">
                        <input type="hidden" name="method" value="addContact" />

                        <div class="table-responsive table-responsive-default">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th><fmt:message key="addToEmployee"/></th>
                                        <th><fmt:message key="action"/></th>
                                    </tr>
                                    <tr>
                                        <td><b><o:out value="${view.currentEmployee.displayName}"/></b></td>
                                        <td style="text-align:right;"><input type="image" name="save" src="<c:url value="${applicationScope.saveIcon}"/>" value="save" class="bigicon" title="<fmt:message key="save"/>"/></td>
                                    </tr>
                                    <c:forEach var="option" items="${view.employees}">
                                        <c:if test="${option.id != view.currentEmployee.id}">
                                            <tr>
                                                <td colspan="2">
                                                    <o:ajaxLink
                                                        url="/privateContacts.do?method=selectEmployee&id=${option.id}"
                                                        targetElement="privateContactDisplay_popup">
                                                        <o:out value="${option.name}"/>
                                                    </o:ajaxLink>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                            
                    </o:ajaxForm>
                </c:when>
                <c:otherwise>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <c:if test="${!empty obj.company}">
                                    <tr>
                                        <td><fmt:message key="company"/></td>
                                        <td><o:out value="${obj.company.displayName}"/></td>
                                    </tr>
                                </c:if>
                                <tr>
                                    <td><fmt:message key="assigned"/></td>
                                    <td><o:date value="${obj.created}"/></td>
                                </tr>
                                <c:if test="${!empty obj.changed}">
                                    <tr>
                                        <td><fmt:message key="changed"/></td>
                                        <td><o:out value="${obj.changed}"/></td>
                                    </tr>
                                </c:if>
                                <tr>
                                    <td><fmt:message key="type"/></td>
                                    <td><o:date value="${obj.contactType}"/></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="note"/></td>
                                    <td><o:date value="${obj.contactInfo}"/></td>
                                </tr>
                                <tr>
                                    <td><o:ajaxLink url="/privateContacts.do?method=enableEdit" linkId="editLink" targetElement="privateContactDisplay_popup"><fmt:message key="change"/></o:ajaxLink></td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${!empty sessionScope.contactPersonView}">
                                                <a href="<c:url value="/contactPersons.do?method=disablePrivateContact"/>"><fmt:message key="remove"/></a>
                                            </c:when>
                                            <c:when test="${!empty sessionScope.contactView}">
                                                <a href="<c:url value="/contacts.do?method=disablePrivateContact"/>"><fmt:message key="remove"/></a>
                                            </c:when>
                                        </c:choose>
                                    </td>
                                </tr>
                            </tbody>
                        </table>        
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
