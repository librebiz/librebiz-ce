<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="classificationConfigCreatorView" />

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="${view.headerName}" />
    </div>
</div>
<div class="modalBoxData">
    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <c:import url="/errors/_errors.jsp" />
            <v:ajaxForm name="classificationConfigForm" url="/admin/products/classificationConfigCreator/save">
                <table class="valueTable first33">
                    <tr>
                        <td><fmt:message key="name" /></td>
                        <td><input class="form-control" type="text" name="name" /></td>
                    </tr>
                    <tr>
                        <td><fmt:message key="description" /></td>
                        <td><textarea class="form-control" name="description"></textarea></td>
                    </tr>
                    <tr>
                        <td class="row-submit"><v:ajaxLink url="/admin/products/classificationConfigCreator/exit" targetElement="${view.name}_popup">
                                <input class="cancel" type="button" value="<fmt:message key="exit"/>" />
                            </v:ajaxLink></td>
                        <td class="row-submit"><o:submit /></td>
                    </tr>
                </table>
            </v:ajaxForm>
            <div class="spacer"></div>
        </div>
    </div>
</div>