/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.dao

import com.osserp.common.directory.ldap.LdapException

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
interface FetchmailAccounts extends LdapDao {

    static final String ACCOUNT_DISABLED = 'disabled'

    /**
     * Provides all available fetchmail accounts
     * @param client
     * @return accounts
     */
    List<Map> findAll(String client)

    /**
     * Looks up for a fetch mail account
     * @param user
     * @return fetchmail account
     */
    Map lookupFetchmailAccount(Map user)

    /**
     * Creates a new fetch mail account
     * @param user
     * @param accountName
     * @param accountPassord
     * @return user with new fetchmail account as mailAccount param
     * @throws LdapException if validation failed
     */
    Map createFetchmailAccount(
            Map user,
            String accountName,
            String accountPassword) throws LdapException

    /**
     * Enables an existing fetchmail account
     * @param account
     * @param serverName if differs from global default
     * @return account with updated values
     */
    Map enableAccount(Map account, String serverName)

    /**
     * Disables an existing fetchmail account without removing entry
     * @param account
     * @param serverName if differs from global default
     * @return account with updated values
     */
    Map disableAccount(Map account, String serverName)

    /**
     * Deletes the default fetchmail account if exists
     * @param account
     */
    void deleteAccount(Map account)

    /**
     * Updates mail account values
     * @param account
     * @param values
     * @return account with updated values
     */
    Map update(Map account, Map values)
}

