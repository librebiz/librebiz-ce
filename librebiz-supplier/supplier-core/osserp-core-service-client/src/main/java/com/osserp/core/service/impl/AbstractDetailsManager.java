/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 16, 2016 
 * 
 */
package com.osserp.core.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Details;
import com.osserp.common.DetailsManager;
import com.osserp.common.ErrorCode;
import com.osserp.common.Property;
import com.osserp.common.User;
import com.osserp.common.beans.PropertyImpl;
import com.osserp.common.util.PropertyUtil;

import com.osserp.core.dao.BusinessDetails;
import com.osserp.core.dao.BusinessProperties;
import com.osserp.core.dao.ResourceProvider;
import com.osserp.core.system.SystemI18n;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDetailsManager extends AbstractService implements DetailsManager {
    private static Logger log = LoggerFactory.getLogger(AbstractDetailsManager.class.getName());

    private ResourceProvider resourceProvider = null;
    private BusinessDetails businessDetails = null;
    private BusinessProperties businessProperties = null;
    
    public AbstractDetailsManager(
            ResourceProvider resourceProvider,
            BusinessProperties businessProperties,
            BusinessDetails businessDetails) {
        super();
        this.resourceProvider = resourceProvider;
        this.businessDetails = businessDetails;
        this.businessProperties = businessProperties;
    }
    
    protected abstract String getI18nContextName();
    
    /**
     * Override to implement specific actions to perform after create or update
     * @param details
     */
    protected void sendChangedEvent(Details details) {
        // default implementation does nothing. 
    }

    /* (non-Javadoc)
     * @see com.osserp.common.DetailsManager#findExistingProperties(com.osserp.common.Details)
     */
    public final List<Property> findExistingProperties(Details ignorable) {
        List<Property> result = new ArrayList<Property>();
        List<SystemI18n> allProps = resourceProvider.loadResources(getI18nContextName());
        for (Iterator<SystemI18n> iterator = allProps.iterator(); iterator.hasNext();) {
            SystemI18n i18n = iterator.next();
            //if (!containsProperty(ignorable.getPropertyList(), i18n)) {
            result.add(new PropertyImpl(i18n.getName(), i18n.getText()));
            //}
        }
        if (log.isDebugEnabled()) {
            log.debug("findExistingProperties() done [count=" + result.size() 
                    + ", context=" + getI18nContextName() + "]");
            
        }
        return PropertyUtil.sort(result, false, false);
    }

    public final List<Property> findExistingValues(String label, List<Property> ignorable) {
        List<Property> existing = businessProperties.findExistingValues(label);
        List<Property> result = new ArrayList<>();
        Set<String> values = new HashSet<String>();
        for (int i = 0, j = existing.size(); i < j; i++) {
            Property next = existing.get(i);
            boolean added = false;
            for (int k = 0, l = ignorable.size(); k < l; k++) {
                Property property = ignorable.get(k);
                if (property.getValue().equals(next.getValue())) {
                    added = true;
                    break;
                }
            }
            if (!added && !values.contains(next.getValue())) {
                values.add(next.getValue());
                result.add(next);
                log.debug("findExistingValues() added next [label="
                        + label + ", value=" + next.getValue() + "]");
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("findExistingProperties() done [count=" + result.size() 
                    + ", context=" + getI18nContextName()
                    + ", label=" + label
                    + "]");
            
        }
        return PropertyUtil.sort(result, false, false);
    }

    /* (non-Javadoc)
     * @see com.osserp.common.DetailsManager#addProperty(com.osserp.common.Details, java.lang.String, java.lang.String, java.lang.String)
     */
    public final Details addProperty(User user, Details details, String language, String label, String value) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("addProperty() invoked [user=" + (user == null ? "null" : user.getId()) 
                    + ", id=" + details.getPrimaryKey() + ", label=" + label 
                    + ", value=" + value + "]");
        }
        if (isNotSet(value)) {
            return details;
        }
        if (isNotSet(label)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (isNotSet(value)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        String name = label;
        if (!resourceProvider.isResourceExisting(getI18nContextName(), name)) {
            name = resourceProvider.createResourceName(getI18nContextName());
            resourceProvider.updateResource(language, name, label);
        }
        Details result = businessDetails.addProperty(user, details, name, value);
        sendChangedEvent(result);
        return result;
    }

    public final Details removeProperty(User user, Details details, Property property) {
        if (log.isDebugEnabled()) {
            log.debug("removeProperty() invoked [user=" + (user == null ? "null" : user.getId()) 
                    + ", id=" + details.getPrimaryKey() + ", property=" + property.getName() + "]");
        }
        Details updated = businessDetails.removeProperty(user, details, property);
        sendChangedEvent(updated);
        return updated;
    }

    public final Details updateProperty(User user, Details details, Property property, String value) {
        if (log.isDebugEnabled()) {
            log.debug("updateProperty() invoked [user=" + (user == null ? "null" : user.getId()) 
                    + ", id=" + details.getPrimaryKey() + ", property=" + property.getName() 
                    + ", value=" + value + "]");
        }
        if (isNotSet(value)) {
            return details;
        }
        Details result = businessDetails.updateProperty(user, details, property, value);
        sendChangedEvent(result);
        return result;
    }

    public final void updatePropertyLabel(User user, Property property, String language, String value) {
        if (log.isDebugEnabled()) {
            log.debug("updatePropertyLabel() invoked [user=" + (user == null ? "null" : user.getId()) 
                    + ", id=" + property.getId() + ", property=" + property.getName() 
                    + ", value=" + value + "]");
        }
        if (isSet(value)) {
            resourceProvider.updateResource(language, property.getName(), value);
        }
    }

    protected boolean containsProperty(List<Property> props, SystemI18n i18n) {
        if (props != null) {
            for (int i = 0, j = props.size(); i < j; i++) {
                Property property = props.get(i);
                if (property.getName().equals(i18n.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    protected ResourceProvider getResourceProvider() {
        return resourceProvider;
    }

    protected void setResourceProvider(ResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    protected BusinessDetails getBusinessDetails() {
        return businessDetails;
    }

    protected void setBusinessDetails(BusinessDetails businessDetails) {
        this.businessDetails = businessDetails;
    }
}
