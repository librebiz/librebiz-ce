#!/usr/bin/env bash
#
# Librebiz source packaging script
#
#---------------------------------------------------------------------
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as 
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Author: Rainer Kirchner <rk@librebiz.org>
#
#---------------------------------------------------------------------

TMPDIR=/tmp
SRC_TARGETNAME=librebiz-ce
SRC_TMPDIR=$TMPDIR/$SRC_TARGETNAME
SRC_TAR_NAME_LATEST=librebiz-src-latest
SRC_TARGETDIR=/dist/source

TMPD="$(dirname $0)"
echo ${TMPD} | grep '^/' >/dev/null 2>&1
if [ "$?" -eq 0 ]; then
    export SCRIPTDIR="${TMPD}"
else
    export SCRIPTDIR="$(pwd)"
fi

SUPPLIER_API="${SCRIPTDIR}/librebiz-supplier/supplier-api"
SUPPLIER_CORE="${SCRIPTDIR}/librebiz-supplier/supplier-core"

if [ ! -d "${SUPPLIER_API}" ] || [ ! -d "${SUPPLIER_CORE}" ]; then
    echo "Missing supplier source"
    exit 0
fi

LIBREBIZ_VERSION=`cat ${SCRIPTDIR}/build | grep LIBREBIZ_VERSION | cut -d "=" -f 2`

echo "Packaging source ${LIBREBIZ_VERSION}"

if [ -d "${SCRIPTDIR}${SRC_TARGETDIR}" ]; then
    rm -rf "${SCRIPTDIR}${SRC_TARGETDIR}"
fi
mkdir -p "${SCRIPTDIR}${SRC_TARGETDIR}"

cd $SUPPLIER_API
mvn clean > /dev/null

cd $SUPPLIER_CORE
mvn clean > /dev/null

cd $SCRIPTDIR
rm -rf $SCRIPTDIR/librebiz/target
rm -rf $SCRIPTDIR/librebiz-cli/target
rm -rf $SCRIPTDIR/librebiz-common/target
rm -rf $SCRIPTDIR/librebiz-webapp/target

rm -rf $SRC_TMPDIR
cp --recursive $SCRIPTDIR $SRC_TMPDIR

cd $SRC_TMPDIR
rm -rf ./dist
rm -rf ./.git
rm -rf ./.attach*
find . -name ".settings" -type d -prune -exec rm -r "{}" \;
find . -name ".classpath" -delete
find . -name ".gitignore" -delete

cd $TMPDIR
TARGET_FILENAME="librebiz-src-all-${LIBREBIZ_VERSION}.tar.gz"
TARGET_FILE="${SCRIPTDIR}${SRC_TARGETDIR}/${TARGET_FILENAME}"
tar cfz $TARGET_FILE ./$SRC_TARGETNAME

rm -rf ./$SRC_TARGETNAME

cd "${SCRIPTDIR}${SRC_TARGETDIR}"

ln -s ./${TARGET_FILENAME} ${SRC_TAR_NAME_LATEST}.tar.gz

if which sha256sum > /dev/null; then
    sha256sum ./$TARGET_FILENAME > $TARGET_FILE.sha256
elif which shasum > /dev/null; then
    shasum -a 256  ./$TARGET_FILENAME > $TARGET_FILE.sha256
else
    echo "Warning: Did not create SHA-256 hash"
fi

if which sha512sum > /dev/null; then
    sha512sum ./$TARGET_FILENAME > $TARGET_FILE.sha512
elif which shasum > /dev/null; then
    shasum -a 512  ./$TARGET_FILENAME > $TARGET_FILE.sha512
else
    echo "Warning: Did not create SHA-512 hash"
fi

chmod 664 $TARGET_FILE*

exit 0
