/**
 *
 * Copyright (C) 2019 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 26, 2019
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.RecordBackups;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordBackupItem;
import com.osserp.core.model.records.RecordBackupItemImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordBackupsDao extends AbstractDao implements RecordBackups {

    private String recordBackupItemsTable;

    protected RecordBackupsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        recordBackupItemsTable = tables.getName("recordBackupItems");
    }

    public void createItemBackup(Record record, String context) {
        int nextVersion = getCurrentVersion(record, context) + 1;
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            RecordBackupItem item = new RecordBackupItemImpl(record, record.getItems().get(i), nextVersion, context);
            getCurrentSession().saveOrUpdate(RecordBackupItemImpl.class.getName(), item);
        }
    }

    public void deleteItemBackup(Record record, String context) {
        int version = getCurrentVersion(record, context);
        if (version > 0) {
            StringBuilder update = new StringBuilder(64);
            update
                .append("DELETE FROM ")
                .append(recordBackupItemsTable)
                .append(" WHERE record_id = ")
                .append(record.getId())
                .append(" AND record_type_id = ")
                .append(record.getType().getId())
                .append(" AND context = '")
                .append(context)
                .append("' AND version_id = ")
                .append(version);
            jdbcTemplate.update(update.toString());
        }
    }

    public List<RecordBackupItem> getItemBackup(Record record, String context) {
        StringBuilder query = new StringBuilder("from ");
        query
                .append(RecordBackupItemImpl.class.getName())
                .append(" o where o.recordId = :recordId and o.recordTypeId = :recordTypeId and o.context = :context")
                .append(" order by o.version, o.orderId");
        return getCurrentSession().createQuery(query.toString())
                .setParameter("recordId", record.getId())
                .setParameter("recordTypeId", record.getType().getId())
                .setParameter("context", context)
                .list();
    }

    public List<RecordBackupItem> getBackupItems(Record record, String context) {
        if (!isItemBackupAvailable(record, context)) {
            return new ArrayList<>();
        }
        Integer versionId = getCurrentVersion(record, context);
        StringBuilder query = new StringBuilder("from ");
        query
                .append(RecordBackupItemImpl.class.getName())
                .append(" o where o.recordId = :recordId and o.recordTypeId= :recordTypeId")
                .append("  and o.context = :context and o.version = :versionId order by o.orderId");
        return getCurrentSession().createQuery(query.toString())
                .setParameter("recordId", record.getId())
                .setParameter("recordTypeId", record.getType().getId())
                .setParameter("context", context)
                .setParameter("versionId", versionId)
                .list();
    }

    public int getCurrentVersion(Record record, String context) {
        if (record == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT max(version_id) FROM ")
                .append(recordBackupItemsTable)
                .append(" WHERE record_id = ? and record_type_id = ? and context = ?");
        final Object[] params = { record.getId(), record.getType().getId(), context };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.VARCHAR };
        Integer maxVersion = jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
        return maxVersion == null ? 0 : maxVersion.intValue();
    }

    public boolean isItemBackupAvailable(Record record, String context) {
        if (record == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT count(*) FROM ")
                .append(recordBackupItemsTable)
                .append(" WHERE record_id = ? and record_type_id = ? and context = ?");
        final Object[] params = { record.getId(), record.getType().getId(), context };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.VARCHAR };
        return jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0;
    }
}
