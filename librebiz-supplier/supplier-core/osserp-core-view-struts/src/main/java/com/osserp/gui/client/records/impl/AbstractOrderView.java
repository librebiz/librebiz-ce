/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Aug-2006 
 * 
 */
package com.osserp.gui.client.records.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.Form;

import com.osserp.core.Item;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderManager;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDocument;
import com.osserp.core.finance.RecordManager;

import com.osserp.gui.client.records.OrderView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOrderView extends AbstractDiscountAwareRecordView
        implements OrderView {
    private static Logger log = LoggerFactory.getLogger(AbstractOrderView.class.getName());
    private List<RecordDocument> versions = new ArrayList<RecordDocument>();
    private List<Item> openDeliveries = null;
    private boolean openDeliveryDisplay = false;

    protected AbstractOrderView() {
        super();
    }

    /**
     * Sets view name as annotated, initializes action target as 'display'
     * @param productView
     * @param confirmDiscountPermissions
     */
    protected AbstractOrderView(String confirmDiscountPermissions) {
        super(confirmDiscountPermissions);
    }

    @Override
    public void setRecord(Record record) {
        setOpenDeliveries(record);
        super.setRecord(record);
    }

    public boolean isSupportingCustomDelivery() {
        return false;
    }

    public Long getCustomDeliveryType() {
        return null;
    }

    @Override
    public boolean isProvidingStatusHistory() {
        return true;
    }

    public List<Item> getOpenDeliveries() {
        return openDeliveries;
    }

    protected void setOpenDeliveries(List<Item> deliveries) {
        this.openDeliveries = deliveries;
    }

    protected void setOpenDeliveries(Record record) {
        if (record == null) {
            openDeliveries = new ArrayList<Item>();
        } else {
            openDeliveries = ((Order) record).getOpenDeliveries();
        }
    }

    public boolean isOpenDeliveryDisplay() {
        return openDeliveryDisplay;
    }

    public void setOpenDeliveryDisplay(boolean openDeliveryDisplay) {
        this.openDeliveryDisplay = openDeliveryDisplay;
    }

    public boolean isDeliveryAvailable() {
        if (!openDeliveries.isEmpty()) {
            for (int i = 0, j = openDeliveries.size(); i < j; i++) {
                Item item = openDeliveries.get(i);
                if (item.getProduct().isKanban() || (item.getQuantity() > 0 &&
                        item.getProduct().getSummary().getAvailableStock() > 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isPartialDeliveryAvailable() {
        if (isDeliveryAvailable()) {
            for (int i = 0, j = openDeliveries.size(); i < j; i++) {
                Item item = openDeliveries.get(i);
                if (!item.getProduct().isKanban() && item.getQuantity() > 0 &&
                        item.getProduct().getSummary().getAvailableStock() >= 0 &&
                        item.getProduct().getSummary().getAvailableStock() < item.getQuantity()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void checkDeliveryStatus() throws ClientException {
        if (getBean() != null && getBean() instanceof Order) {
            List<DeliveryNote> notes = ((Order) getBean()).getDeliveryNotes();
            for (int i = 0, j = notes.size(); i < j; i++) {
                DeliveryNote note = notes.get(i);
                if (!note.isUnchangeable()) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkDeliveryStatus() found unreleased record "
                                + note.getId());
                    }
                    throw new ClientException(ErrorCode.UNRELEASED_RECORD);
                }
                if (!note.isSerialsCompleted()) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkDeliveryStatus() found incomplete serials on note "
                                + note.getId());
                    }
                    throw new ClientException(ErrorCode.SERIALS_MISSING);
                }
            }
        }
    }

    public void changeDeliveryConfirmation(Long id) {
        Order record = getOrder();
        OrderManager manager = (OrderManager) getRecordManager();
        manager.changeDeliveryConfirmation(getDomainEmployee(), record, id);
    }

    // delivery date edit modes

    public void updateDeliveryDate(Form form) throws ClientException, PermissionException {
        Order order = getOrder();
        OrderManager manager = (OrderManager) getRecordManager();
        if (isItemDeliveryDateEditMode()) {
            manager.update(
                    getDomainEmployee(),
                    order,
                    form.getDate("delivery"),
                    form.getString("deliveryNote"),
                    getSelectedItem());
            setItemDeliveryDateEditMode(false);
        } else {
            manager.update(
                    getDomainEmployee(),
                    order,
                    form.getDate("delivery"),
                    form.getString("deliveryNote"),
                    form.getString("deliveryDateOnItems"),
                    form.getBoolean("deliveryConfirmed"));
            setDeliveryDateEditMode(false);
        }
        this.openDeliveries = order.getOpenDeliveries();
    }

    public void updateStock(Long productId, Long stockId) {
        if (log.isDebugEnabled()) {
            log.debug("updateStock() invoked [productId=" + productId
                    + ", stockId=" + stockId + "]");
        }
        Order order = getOrder();
        OrderManager manager = getOrderManager();
        manager.changeStock(order, productId, stockId);
        refresh();
    }

    // version handling

    public abstract void createNewVersion() throws ClientException, PermissionException;

    public List<RecordDocument> getVersions() {
        return versions;
    }

    protected void setVersions(List<RecordDocument> versions) {
        this.versions = versions;
    }

    public byte[] getVersion(Long id) {
        RecordDocument selected = (RecordDocument) CollectionUtil.getById(versions, id);
        OrderManager manager = getOrderManager();
        return manager.getVersion(selected);
    }

    @Override
    public void setSetupMode(boolean setupMode) {
        super.setSetupMode(setupMode);
        if (setupMode) {
            Order order = getOrder();
            OrderManager manager = getOrderManager();
            this.versions = manager.findVersions(order);
            if (log.isDebugEnabled() && !this.versions.isEmpty()) {
                log.debug("setSetupMode() found versions [id=" + order.getId()
                        + ", count=" + versions.size() + "]");
            }
        }
    }

    protected OrderManager getOrderManager() {
        RecordManager rm = getRecordManager();
        if (!(rm instanceof OrderManager)) {
            log.error("getOrderManager() implementing class provides no OrderManager [class="
                    + rm.getClass().getName() + "]");
            throw new com.osserp.common.ActionException("missing order manager instance");
        }
        return (OrderManager) rm;
    }
}
