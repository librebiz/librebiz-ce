/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 22, 2007 11:03:19 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.core.tickets.RequestResetWarningTicket;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestResetWarningTicketRowMapper extends AbstractRowMapper {
    public static final String SELECT_ALL =
            "SELECT w.id,w.reference_id,r.name,r.created,w.sales_id,w.created,w.warning_sent FROM ";

    public static String createOpenQuery(String requestsTable, String warningsTable) {
        StringBuilder sql = new StringBuilder(SELECT_ALL);
        sql
                .append(warningsTable)
                .append(" w, ")
                .append(requestsTable)
                .append(" r WHERE w.reference_id = r.plan_id " +
                        "AND w.warning_sent IS NULL ORDER BY r.created");
        return sql.toString();
    }

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new RequestResetWarningTicketRowMapper());
    }

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new RequestResetWarningTicket(
                Long.valueOf(rs.getLong(1)),
                Long.valueOf(rs.getLong(2)),
                rs.getString(3),
                rs.getDate(4),
                Long.valueOf(rs.getLong(5)),
                rs.getDate(6),
                rs.getDate(7));
    }
}
