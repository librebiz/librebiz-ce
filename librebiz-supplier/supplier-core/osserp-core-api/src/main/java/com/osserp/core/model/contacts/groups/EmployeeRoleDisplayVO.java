/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 26, 2009 6:33:09 AM 
 * 
 */
package com.osserp.core.model.contacts.groups;

import com.osserp.core.employees.EmployeeRoleDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeRoleDisplayVO implements EmployeeRoleDisplay {

    private Long employeeId = null;
    private String employee = null;
    private Long branchId = null;
    private String branch = null;
    private boolean defaultBranch = false;
    private Long groupId = null;
    private String group = null;
    private Long statusId = null;
    private String status = null;
    private boolean defaultRole = false;
    private Long companyId = null;
    private String company = null;

    protected EmployeeRoleDisplayVO() {
        super();
    }

    public EmployeeRoleDisplayVO(
            String branch,
            Long branchId,
            String company,
            Long companyId,
            boolean defaultBranch,
            boolean defaultRole,
            String employee,
            Long employeeId,
            String group,
            Long groupId,
            String status,
            Long statusId) {
        super();
        this.branch = branch;
        this.branchId = branchId;
        this.company = company;
        this.companyId = companyId;
        this.defaultBranch = defaultBranch;
        this.defaultRole = defaultRole;
        this.employee = employee;
        this.employeeId = employeeId;
        this.group = group;
        this.groupId = groupId;
        this.status = status;
        this.statusId = statusId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    protected void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployee() {
        return employee;
    }

    protected void setEmployee(String employee) {
        this.employee = employee;
    }

    public Long getBranchId() {
        return branchId;
    }

    protected void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getBranch() {
        return branch;
    }

    protected void setBranch(String branch) {
        this.branch = branch;
    }

    public boolean isDefaultBranch() {
        return defaultBranch;
    }

    protected void setDefaultBranch(boolean defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    public Long getGroupId() {
        return groupId;
    }

    protected void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getGroup() {
        return group;
    }

    protected void setGroup(String group) {
        this.group = group;
    }

    public Long getStatusId() {
        return statusId;
    }

    protected void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getStatus() {
        return status;
    }

    protected void setStatus(String status) {
        this.status = status;
    }

    public boolean isDefaultRole() {
        return defaultRole;
    }

    protected void setDefaultRole(boolean defaultRole) {
        this.defaultRole = defaultRole;
    }

    public Long getCompanyId() {
        return companyId;
    }

    protected void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompany() {
        return company;
    }

    protected void setCompany(String company) {
        this.company = company;
    }

}
