/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 5, 2011 10:34:01 AM 
 * 
 */
package com.osserp.common.directory.impl

import com.osserp.common.directory.DirectoryGroup
import com.osserp.common.directory.DirectoryManager
import com.osserp.common.directory.DirectoryPermission
import com.osserp.common.directory.DirectoryUser
import com.osserp.common.directory.ldap.LdapManager
import com.osserp.common.directory.ldap.LdapUtils

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class DirectoryManagerImpl extends AbstractDirectoryManager implements DirectoryManager {
	private static final String DEFAULT_PERMISSION = 'osserp'
	
	protected DirectoryManagerImpl() {
		super()
	}

	protected DirectoryManagerImpl(LdapManager ldapManager) {
		super(ldapManager)
	}

	void activateUser(String uid, String client) {
		ldapManager.addGroupMember(getDefaultGroupMap(client), uid)
		ldapManager.addPermissionMember(getDefaultPermissionMap(client), uid)
	}

	void deactivateUser(String uid, String client) {
		ldapManager.removeGroupMember(getDefaultGroupMap(client), uid)
		ldapManager.removePermissionMember(getDefaultPermissionMap(client), uid)
	}

    Map createUserAttributes(String client) {
		createDefaultUserAttributes(DirectoryUser.BUSINESS_CATEGORY_SYSTEM, client)
	}
	
    Map createEmployeeUserAttributes(String client) {
        createDefaultUserAttributes(DirectoryUser.BUSINESS_CATEGORY_EMPLOYEE, client)
    }

    Map createSubscriberAttributes(String client) {
        createDefaultUserAttributes(DirectoryUser.BUSINESS_CATEGORY_SUBSCRIBER, client)
    }

    DirectoryGroup createGroup(String name, String description, String client, Long id) {
		createGroupResult(ldapManager.createGroup(name, description, client, id))
	}

	void deleteGroup(DirectoryGroup group) {
		ldapManager.deleteGroup(group.values)
	}

	List<DirectoryGroup> findGroups(DirectoryUser user) {
		String client = LdapUtils.fetchClientName(user.values?.dn)
		createGroupsResult(ldapManager.lookupUserGroups(user.values.uid, client))
	}

	DirectoryPermission createPermission(String name, String description, String client, Long id) {
		Map permission = ldapManager.createPermission(name, description, client, id)
		new DirectoryPermission([values: permission])
	}

	void deletePermission(DirectoryPermission permission) {
		ldapManager.deletePermission(permission.values)
	}

	List<DirectoryPermission> findPermissions(DirectoryUser user) {
		String client = LdapUtils.fetchClientName(user.values?.dn)
		createPermissionsResult(ldapManager.lookupUserPermissions(user.values.uid, client))
	}
	
	DirectoryUser createUser(Map<String, Object> values, String client) {
        if (!values.mail && values.uid && ldapManager.config.dbmailDefaultDomain 
            && (DirectoryUser.BUSINESS_CATEGORY_SUBSCRIBER == values.osserpcategory
                || DirectoryUser.BUSINESS_CATEGORY_SYSTEM == values.osserpcategory)) {
            values.mail = "${values.uid}@${ldapManager.config.dbmailDefaultDomain}".toString()
        }
		createUserResult(ldapManager.createUser(values, client))
	}
	
	DirectoryUser findUser(String uid, String client) {
		createUserResult(ldapManager.lookupUser(uid, client))
	}
	
	DirectoryGroup update(DirectoryGroup group, Map values) {
		createGroupResult(ldapManager.updateGroup(group.values, values))
	}

	DirectoryPermission update(DirectoryPermission permission, Map values) {
		Map updatedPermission = ldapManager.updatePermission(permission.values, values)
		new DirectoryPermission(values:updatedPermission)
	}
	
	DirectoryUser update(DirectoryUser user, Map values) {
		createUserResult(ldapManager.updateUser(user.values, values))
	}

	DirectoryUser updatePassword(DirectoryUser user, String password) {
		createUserResult(ldapManager.changePassword(user.values, password))
	}
	
	void addGroupMember(DirectoryGroup group, DirectoryUser user) {
		ldapManager.addGroupMember(group.values, user.values.uid)
	}

	void removeGroupMember(DirectoryGroup group, DirectoryUser user) {
		ldapManager.removeGroupMember(group.values, user.values.uid)
	}

	void addPermissionMember(DirectoryPermission permission, DirectoryUser user) {
		ldapManager.addPermissionMember(permission.values, user.values.uid)
	}

	void removePermissionMember(DirectoryPermission permission, DirectoryUser user) {
		ldapManager.removePermissionMember(permission.values, user.values.uid)
	}
	
	DirectoryGroup getDefaultGroup(String client) {
		createGroupResult(getDefaultGroupMap(client))
	}

	protected Map getDefaultGroupMap(String client) {
		ldapManager.lookupGroup(ldapManager.getDefaultGroupName(client), client)
	}
	
	DirectoryPermission getDefaultPermission(String client) {
		new DirectoryPermission(values: getDefaultPermissionMap(client))
	}

	protected Map getDefaultPermissionMap(String client) {
		ldapManager.lookupPermission(DEFAULT_PERMISSION, client)
	}
}
