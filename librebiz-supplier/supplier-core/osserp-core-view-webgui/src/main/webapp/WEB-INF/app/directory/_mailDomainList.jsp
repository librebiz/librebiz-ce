<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="domainname"><fmt:message key="name"/></th>
                    <th class="date"><fmt:message key="registered"/></th>
                    <th class="date"><fmt:message key="til"/></th>
                    <th class="provider"><fmt:message key="provider"/></th>
                    <th class="action center"><fmt:message key="action"/></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="obj" items="${view.list}">
                    <tr>
                        <td class="domainname">
                            <a href="${view.selectLink.link}?id=${obj.id}"><o:out value="${obj.name}"/></a>
                        </td>
                        <td class="date"><o:date value="${obj.domainCreated}"/></td>
                        <td class="date"><o:date value="${obj.domainExpires}"/></td>
                        <td class="provider"><o:out value="${obj.provider}"/></td>
                        <td class="action center">
                            <a href="${view.selectLink.link}?id=${account.id}"><o:img name="enabledIcon"/></a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
