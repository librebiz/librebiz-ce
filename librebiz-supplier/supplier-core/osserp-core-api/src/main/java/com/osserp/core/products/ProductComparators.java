/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 24, 2008 3:19:06 PM 
 * 
 */
package com.osserp.core.products;

import java.util.Comparator;
import java.util.List;

import com.osserp.common.util.CollectionUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductComparators {

    public static Comparator sortByAvailability(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    ProductSummary sA = ((Product) a).getSummary();
                    ProductSummary sB = ((Product) b).getSummary();
                    Double valA = (sA == null || sA.getAvailability() == null ? 0d : sA.getAvailability());
                    Double valB = (sB == null || sB.getAvailability() == null ? 0d : sB.getAvailability());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                ProductSummary sA = ((Product) a).getSummary();
                ProductSummary sB = ((Product) b).getSummary();
                Double valA = (sA == null || sA.getAvailability() == null ? 0d : sA.getAvailability());
                Double valB = (sB == null || sB.getAvailability() == null ? 0d : sB.getAvailability());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortByAvailableStock(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    ProductSummary sA = ((Product) a).getSummary();
                    ProductSummary sB = ((Product) b).getSummary();
                    Double valA = (sA == null || sA.getAvailableStock() == null ? 0d : sA.getAvailableStock());
                    Double valB = (sB == null || sB.getAvailableStock() == null ? 0d : sB.getAvailableStock());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                ProductSummary sA = ((Product) a).getSummary();
                ProductSummary sB = ((Product) b).getSummary();
                Double valA = (sA == null || sA.getAvailableStock() == null ? 0d : sA.getAvailableStock());
                Double valB = (sB == null || sB.getAvailableStock() == null ? 0d : sB.getAvailableStock());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortByExpected(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    ProductSummary sA = ((Product) a).getSummary();
                    ProductSummary sB = ((Product) b).getSummary();
                    Double valA = (sA == null || sA.getExpected() == null ? 0d : sA.getExpected());
                    Double valB = (sB == null || sB.getExpected() == null ? 0d : sB.getExpected());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                ProductSummary sA = ((Product) a).getSummary();
                ProductSummary sB = ((Product) b).getSummary();
                Double valA = (sA == null || sA.getExpected() == null ? 0d : sA.getExpected());
                Double valB = (sB == null || sB.getExpected() == null ? 0d : sB.getExpected());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortById(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Product) a).getProductId().compareTo(((Product) b).getProductId());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Product) a).getProductId().compareTo(((Product) b).getProductId());
            }
        };
    }

    public static Comparator sortByName(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Product) a).getName().compareTo(((Product) b).getName());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Product) a).getName().compareToIgnoreCase(((Product) b).getName());
            }
        };
    }

    public static Comparator sortByOrders(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    ProductSummary sA = ((Product) a).getSummary();
                    ProductSummary sB = ((Product) b).getSummary();
                    Double valA = (sA == null || sA.getOrdered() == null ? 0d : sA.getOrdered());
                    Double valB = (sB == null || sB.getOrdered() == null ? 0d : sB.getOrdered());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                ProductSummary sA = ((Product) a).getSummary();
                ProductSummary sB = ((Product) b).getSummary();
                Double valA = (sA == null || sA.getOrdered() == null ? 0d : sA.getOrdered());
                Double valB = (sB == null || sB.getOrdered() == null ? 0d : sB.getOrdered());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortByOrderedAndVacant(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    ProductSummary sA = ((Product) a).getSummary();
                    ProductSummary sB = ((Product) b).getSummary();
                    Double valA = (sA == null || sA.getOrderedAndVacant() == null ? 0d : sA.getOrderedAndVacant());
                    Double valB = (sB == null || sB.getOrderedAndVacant() == null ? 0d : sB.getOrderedAndVacant());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                ProductSummary sA = ((Product) a).getSummary();
                ProductSummary sB = ((Product) b).getSummary();
                Double valA = (sA == null || sA.getOrderedAndVacant() == null ? 0d : sA.getOrderedAndVacant());
                Double valB = (sB == null || sB.getOrderedAndVacant() == null ? 0d : sB.getOrderedAndVacant());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortByConsumerPrice(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Product aA = (Product) a;
                    Product aB = (Product) b;
                    Double valA = (aA == null || aA.getConsumerPrice() == null ? 0d : aA.getConsumerPrice());
                    Double valB = (aB == null || aB.getConsumerPrice() == null ? 0d : aB.getConsumerPrice());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Product aA = (Product) a;
                Product aB = (Product) b;
                Double valA = (aA == null || aA.getConsumerPrice() == null ? 0d : aA.getConsumerPrice());
                Double valB = (aB == null || aB.getConsumerPrice() == null ? 0d : aB.getConsumerPrice());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortByResellerPrice(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Product aA = (Product) a;
                    Product aB = (Product) b;
                    Double valA = (aA == null || aA.getResellerPrice() == null ? 0d : aA.getResellerPrice());
                    Double valB = (aB == null || aB.getResellerPrice() == null ? 0d : aB.getResellerPrice());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Product aA = (Product) a;
                Product aB = (Product) b;
                Double valA = (aA == null || aA.getResellerPrice() == null ? 0d : aA.getResellerPrice());
                Double valB = (aB == null || aB.getResellerPrice() == null ? 0d : aB.getResellerPrice());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortByPartnerPrice(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Product aA = (Product) a;
                    Product aB = (Product) b;
                    Double valA = (aA == null || aA.getPartnerPrice() == null ? 0d : aA.getPartnerPrice());
                    Double valB = (aB == null || aB.getPartnerPrice() == null ? 0d : aB.getPartnerPrice());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Product aA = (Product) a;
                Product aB = (Product) b;
                Double valA = (aA == null || aA.getPartnerPrice() == null ? 0d : aA.getPartnerPrice());
                Double valB = (aB == null || aB.getPartnerPrice() == null ? 0d : aB.getPartnerPrice());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortBySpecificPartnerPrice(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Product aA = (Product) a;
                    Product aB = (Product) b;
                    Double valA = (aA == null || aA.getSpecificPartnerPrice() == null ? 0d : aA.getSpecificPartnerPrice());
                    Double valB = (aB == null || aB.getSpecificPartnerPrice() == null ? 0d : aB.getSpecificPartnerPrice());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Product aA = (Product) a;
                Product aB = (Product) b;
                Double valA = (aA == null || aA.getSpecificPartnerPrice() == null ? 0d : aA.getSpecificPartnerPrice());
                Double valB = (aB == null || aB.getSpecificPartnerPrice() == null ? 0d : aB.getSpecificPartnerPrice());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortByStock(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    ProductSummary sA = ((Product) a).getSummary();
                    ProductSummary sB = ((Product) b).getSummary();
                    Double valA = (sA == null || sA.getStock() == null ? 0d : sA.getStock());
                    Double valB = (sB == null || sB.getStock() == null ? 0d : sB.getStock());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                ProductSummary sA = ((Product) a).getSummary();
                ProductSummary sB = ((Product) b).getSummary();
                Double valA = (sA == null || sA.getStock() == null ? 0d : sA.getStock());
                Double valB = (sB == null || sB.getStock() == null ? 0d : sB.getStock());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortByReceipt(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    ProductSummary sA = ((Product) a).getSummary();
                    ProductSummary sB = ((Product) b).getSummary();
                    Double valA = (sA == null || sA.getReceipt() == null ? 0d : sA.getReceipt());
                    Double valB = (sB == null || sB.getReceipt() == null ? 0d : sB.getReceipt());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                ProductSummary sA = ((Product) a).getSummary();
                ProductSummary sB = ((Product) b).getSummary();
                Double valA = (sA == null || sA.getReceipt() == null ? 0d : sA.getReceipt());
                Double valB = (sB == null || sB.getReceipt() == null ? 0d : sB.getReceipt());
                return valA.compareTo(valB);
            }
        };
    }

    public static Comparator sortByVacantAvailability(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    ProductSummary sA = ((Product) a).getSummary();
                    ProductSummary sB = ((Product) b).getSummary();
                    Double valA = (sA == null || sA.getVacantAvailability() == null ? 0d : sA.getVacantAvailability());
                    Double valB = (sB == null || sB.getVacantAvailability() == null ? 0d : sB.getVacantAvailability());
                    int result = valA.compareTo(valB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                ProductSummary sA = ((Product) a).getSummary();
                ProductSummary sB = ((Product) b).getSummary();
                Double valA = (sA == null || sA.getVacantAvailability() == null ? 0d : sA.getVacantAvailability());
                Double valB = (sB == null || sB.getVacantAvailability() == null ? 0d : sB.getVacantAvailability());
                return valA.compareTo(valB);
            }
        };
    }
    
    public static List<Product> sort(List<Product> list) {
        return CollectionUtil.sort(list, sortByName(false));
    }
    
}
