/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 13, 2010 11:49:27 AM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.TelephoneConfigurationChangeset;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneConfigurationChangesetImpl extends AbstractEntity implements TelephoneConfigurationChangeset {

    private Long referenceId = null;
    private String property = null;
    private String oldValue = null;
    private String newValue = null;

    protected TelephoneConfigurationChangesetImpl() {
        super();
    }

    public TelephoneConfigurationChangesetImpl(Long createdBy, Long referenceId, String property, String oldValue, String newValue) {
        super();
        setCreatedBy(createdBy);
        this.referenceId = referenceId;
        this.property = property;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public Long getReferenceId() {
        return referenceId;
    }

    protected void setReferenceId(Long referenceId) {
        this.referenceId = referenceId;
    }

    public String getNewValue() {
        return newValue;
    }

    protected void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getOldValue() {
        return oldValue;
    }

    protected void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getProperty() {
        return property;
    }

    protected void setProperty(String property) {
        this.property = property;
    }

}
