<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="recordingView" value="${sessionScope.timeRecordingView}"/>
<c:set var="recording" value="${recordingView.timeRecording}"/>
<c:set var="period" value="${recording.selectedPeriod}"/>
<c:set var="markerTypes" value="${recordingView.markerTypes}"/>
<c:set var="selectedMonth" value="${period.selectedYear.selectedMonth}"/>

<o:ajaxForm name="dynamicForm" onSuccess="" preRequest="" postRequest=""
    targetElement="addMarker_popup" url="/timeRecordings.do">

    <input type="hidden" name="method" value="addMarker"/>
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <fmt:message key="addMarker"/>
        </div>
    </div>
    <div class="modalBoxData">

        <div class="col-md-12 panel-area panel-area-default">
            <c:if test="${!empty sessionScope.errors}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="errormessage">
                            <fmt:message key="error" />: <o:out value="${sessionScope.errors}" />
                        </h4>
                    </div>
                </div>
                <o:removeErrors />
            </c:if>

            <div class="panel-body">
                <div class="form-body">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="date"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="markerDate" value="<o:date value="${selectedMonth.firstDayDate}"/>" id="markerDateInput" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <o:datepicker input="markerDateInput"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="type"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="markerTyp" class="form-control" size="1">
                                    <c:forEach var="obj" items="${markerTypes}">
                                        <option value="<o:out value="${obj.id}"/>"><o:out value="${obj.name}"/></option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="value"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control" name="markerValue" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="comment"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea name="markerNote" class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</o:ajaxForm>
