/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.contacts.groups;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.Property;
import com.osserp.common.util.SecurityUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactPaymentAgreement;
import com.osserp.core.customers.Customer;
import com.osserp.core.model.contacts.AbstractContactAware;

/**
 * Value object for Customer implementations.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CustomerImpl extends AbstractContactAware implements Customer {
    private Long typeId;
    private String job = null;
    private String vatId = null;
    private boolean discountEnabled = false;
    private Double discountA = 0d;
    private Double discountB = 0d;
    private Double discountC = 0d;
    private boolean websiteAccountAvailable = false;
    private boolean websiteAccountEnabled = false;
    private String websiteAccountName = null;
    private String websiteAccountPassword = null;
    private Long websiteAccountContact = null;
    private List<ContactPaymentAgreement> paymentAgreements = new ArrayList<ContactPaymentAgreement>();
    private boolean agent = false;
    private Double agentCommission = 0d;
    private boolean agentCommissionPercent = false;
    private boolean tipProvider = false;
    private Double tipCommission = 0d;
    private boolean internal = false;
    private String invoiceBy;
    private String invoiceEmail;

    private CustomerDetailsImpl detailsObj = new CustomerDetailsImpl(getId());

    public CustomerImpl() {
        setGroupId(Customer.CUSTOMER);
        setTypeId(Customer.TYPE_CONSUMER);
    }

    /**
     * Constructor for new customers
     * @param primaryKey
     * @param createdBy
     * @param contact
     */
    public CustomerImpl(Long primaryKey, Contact contact, Long createdBy) {
        super(primaryKey, Customer.CUSTOMER, createdBy, contact, Customer.STATUS_INTEREST);
        setTypeId(Customer.TYPE_CONSUMER);
    }

    /**
     * Constructor for cloneable
     * @param other
     */
    private CustomerImpl(Customer other) {
        super(other);
        typeId = other.getTypeId();
        job = other.getJob();
        vatId = other.getVatId();
        discountEnabled = other.isDiscountEnabled();
        discountA = other.getDiscountA();
        discountB = other.getDiscountB();
        discountC = other.getDiscountC();
        websiteAccountAvailable = other.isWebsiteAccountAvailable();
        websiteAccountEnabled = other.isWebsiteAccountEnabled();
        agent = other.isAgent();
        agentCommission = other.getAgentCommission();
        agentCommissionPercent = other.isAgentCommissionPercent();
        tipProvider = other.isTipProvider();
        tipCommission = other.getTipCommission();
        invoiceBy = other.getInvoiceBy();
        invoiceEmail = other.getInvoiceEmail();

        paymentAgreements = new ArrayList<ContactPaymentAgreement>();
        ContactPaymentAgreement pa = other.getPaymentAgreement();
        if (pa != null) {
            paymentAgreements.add(new CustomerPaymentAgreementImpl(pa));
        }
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getInvoiceBy() {
        return invoiceBy;
    }

    public void setInvoiceBy(String invoiceBy) {
        this.invoiceBy = invoiceBy;
    }

    public String getInvoiceEmail() {
        return invoiceEmail;
    }

    public void setInvoiceEmail(String invoiceEmail) {
        this.invoiceEmail = invoiceEmail;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public boolean isDiscountEnabled() {
        return discountEnabled;
    }

    public void setDiscountEnabled(boolean discountEnabled) {
        this.discountEnabled = discountEnabled;
    }

    public Double getDiscountA() {
        return discountA;
    }

    public void setDiscountA(Double discountA) {
        this.discountA = discountA;
    }

    public Double getDiscountB() {
        return discountB;
    }

    public void setDiscountB(Double discountB) {
        this.discountB = discountB;
    }

    public Double getDiscountC() {
        return discountC;
    }

    public void setDiscountC(Double discountC) {
        this.discountC = discountC;
    }

    public boolean isWebsiteAccountAvailable() {
        return websiteAccountAvailable;
    }

    protected void setWebsiteAccountAvailable(boolean websiteAccountAvailable) {
        this.websiteAccountAvailable = websiteAccountAvailable;
    }

    public boolean isWebsiteAccountEnabled() {
        return websiteAccountEnabled;
    }

    public void setWebsiteAccountEnabled(boolean websiteAccountEnabled) {
        this.websiteAccountEnabled = websiteAccountEnabled;
    }

    public String getWebsiteAccountName() {
        return websiteAccountName;
    }

    public void setWebsiteAccountName(String websiteAccountName) {
        this.websiteAccountName = websiteAccountName;
    }

    public String getWebsiteAccountPassword() {
        return websiteAccountPassword;
    }

    public void setWebsiteAccountPassword(String websiteAccountPassword) {
        this.websiteAccountPassword = websiteAccountPassword;
    }

    public Long getWebsiteAccountContact() {
        return websiteAccountContact;
    }

    public void setWebsiteAccountContact(Long websiteAccountContact) {
        this.websiteAccountContact = websiteAccountContact;
    }

    public void setWebsiteAccount(String websiteAccountName) {
        this.websiteAccountName = websiteAccountName;
        this.websiteAccountAvailable = true;
    }

    public void updateWebsiteAccount(String accountName, String accountPassword) {
        if (isSet(accountName) && !accountName.equals(websiteAccountName)) {
            websiteAccountName = accountName;
        }
        if (isSet(accountPassword) && !accountPassword.equals(websiteAccountPassword)) {
            websiteAccountPassword = SecurityUtil.createPassword(accountPassword);
        }
        if (isSet(websiteAccountName)) {
            websiteAccountAvailable = true;
        }
    }

    public boolean checkWebsitePassword(String password) {
        if (isNotSet(password)) {
            return false;
        }
        return SecurityUtil.checkPassword(password, websiteAccountPassword);
    }

    public boolean isHolding() {
        return (typeId == null ? false : Customer.TYPE_HOLDING.equals(typeId));
    }

    @Override
    public boolean isClient() {
        return (typeId == null ? false : Customer.TYPE_CLIENT.equals(typeId));
    }

    public boolean isReseller() {
        return (typeId == null ? false : Customer.TYPE_RESELLER.equals(typeId));
    }

    public boolean isPos() {
        return (typeId == null ? false : Customer.TYPE_POSACCOUNT.equals(typeId));
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public boolean isAgent() {
        return agent;
    }

    public void setAgent(boolean agent) {
        this.agent = agent;
    }

    public Double getAgentCommission() {
        return agentCommission;
    }

    public void setAgentCommission(Double agentCommission) {
        this.agentCommission = agentCommission;
    }

    public boolean isAgentCommissionPercent() {
        return agentCommissionPercent;
    }

    public void setAgentCommissionPercent(boolean agentCommissionPercent) {
        this.agentCommissionPercent = agentCommissionPercent;
    }

    public boolean isTipProvider() {
        return tipProvider;
    }

    public void setTipProvider(boolean tipProvider) {
        this.tipProvider = tipProvider;
    }

    public Double getTipCommission() {
        return tipCommission;
    }

    public void setTipCommission(Double tipCommission) {
        this.tipCommission = tipCommission;
    }

    public ContactPaymentAgreement getPaymentAgreement() {
        if (paymentAgreements == null) {
            paymentAgreements = new ArrayList<ContactPaymentAgreement>();
        }
        if (paymentAgreements.isEmpty()) {
            paymentAgreements.add(new CustomerPaymentAgreementImpl(this));
        }
        return paymentAgreements.get(0);
    }
    
    // implementation of details interface
    
    public List<Property> getPropertyList() {
        return detailsObj.getPropertyList();
    }

    public Map<String, Property> getPropertyMap() {
        return detailsObj.getPropertyMap();
    }

    public void addProperty(Long user, String name, String value) {
        detailsObj.addProperty(user, name, value);
    }

    public void removeProperty(Long user, Property property) {
        detailsObj.removeProperty(user, property);
    }

    public boolean isPropertyAvailable(String propertyName) {
        return detailsObj.getPropertyMap().containsKey(propertyName);
    }

    public Property getProperty(String propertyName) {
        return detailsObj.getPropertyMap().get(propertyName);
    }
    
    protected CustomerDetailsImpl getDetailsObj() {
        return detailsObj;
    }

    protected void setDetailsObj(CustomerDetailsImpl detailsObj) {
        this.detailsObj = detailsObj;
    }

    @Override
    public Element getXML() {
        return getXML("customer");
    }

    @Override
    public Element getXML(String name) {
        Element xml = super.getXML(name);
        // TODO redundant, remove when stylesheets are updated from
        // referencing id from superclass instead of customerId
        if (getId() != null) {
            xml.addContent(new Element("customerId").setText(getId().toString()));
        }
        if (vatId == null) {
            xml.addContent(new Element("vatId"));
        } else {
            xml.addContent(new Element("vatId").setText(vatId));
        }
        xml.addContent(new Element("websiteAccountAvailable").setText(
                Boolean.valueOf(websiteAccountAvailable).toString()));
        xml.addContent(new Element("websiteAccountEnabled").setText(
                Boolean.valueOf(websiteAccountEnabled).toString()));
        return xml;
    }

    public String getXls() {
        return null;
    }

    protected List<ContactPaymentAgreement> getPaymentAgreements() {
        return paymentAgreements;
    }

    protected void setPaymentAgreements(
            List<ContactPaymentAgreement> paymentAgreements) {
        this.paymentAgreements = paymentAgreements;
    }

    @Override
    public Long getGroupId() {
        Long id = super.getGroupId();
        return (id != null ? id : Contact.CUSTOMER);
    }

    @Override
    public Object clone() {
        return new CustomerImpl(this);
    }

}
