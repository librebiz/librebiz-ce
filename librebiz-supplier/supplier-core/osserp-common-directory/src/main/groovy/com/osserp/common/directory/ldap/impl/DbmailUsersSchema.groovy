/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 29, 2013 
 * 
 */
package com.osserp.common.directory.ldap.impl


import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.SubSchema

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class DbmailUsersSchema extends AbstractLdapConfigAware implements SubSchema {

    private static final String DEFAULT_MBOX_UIDNR = '5000'
    private static final String DEFAULT_MBOX_GIDNR = '5000'

    private static final List OBJECT_CLASSES = ['dbmailUser']

    private static final List AVAILABLE_ATTRIBUTES = [
        'mailquota',
        'mailforwardingaddress',
        'mailhost',
        'dbmailuid',
        'dbmailgid',
        'mailalternateaddress',
        'deliverymode',
        'accountstatus'
    ]

    private static final List DISPLAY_ATTRIBUTES = [
        'accountstatus',
        'mailalternateaddress',
        'mailforwardingaddress',
        'mailhost',
        'mailquota'
    ]

    DbmailUsersSchema(LdapConfig configInstance) {
        super(configInstance)
    }

    Map create(Map parent, Map values) {
        if (values.dbmailuid) {
            parent.dbmailuid = values.dbmailuid
        } else {
            parent.dbmailuid = DEFAULT_MBOX_UIDNR
        }
        if (values.dbmailgid) {
            parent.dbmailgid = values.dbmailgid
        } else {
            parent.dbmailgid = DEFAULT_MBOX_GIDNR
        }
        addObjectClasses(this, parent)
        return parent
    }

    Map apply(Map values, Map source) {
        if (!values.dbmailuid && !source.dbmailuid) {
            values.dbmailuid = DEFAULT_MBOX_UIDNR
        }
        if (!values.dbmailgid && !source.dbmailgid) {
            values.dbmailgid = DEFAULT_MBOX_GIDNR
        }
        return updateObjectClasses(this, values, source)
    }

    List<String> getObjectClasses() {
        config?.dbmailObjectClasses ?: OBJECT_CLASSES
    }

    List<String> getAvailableAttributes() {
        AVAILABLE_ATTRIBUTES
    }

    Set<String> getAvailableArrays() {
        ['mailalternateaddress']
    }

    List<String> getDefaultDisplayAttributes() {
        DISPLAY_ATTRIBUTES
    }
}
