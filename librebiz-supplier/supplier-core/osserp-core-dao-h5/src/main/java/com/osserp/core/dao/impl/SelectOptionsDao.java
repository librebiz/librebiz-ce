/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 1:59:59 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.dao.Tables;
import com.osserp.core.dao.SelectOptions;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SelectOptionsDao extends AbstractRepository implements SelectOptions {

    public SelectOptionsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
		super(jdbcTemplate, tables, sessionFactory);
	}

    public List<Option> getList(String tableKey) {
        StringBuffer sql = new StringBuffer("SELECT * FROM ");
        sql.append(getTable(tableKey)).append(" ORDER BY name");
        return (List) jdbcTemplate.query(sql.toString(), getOptionRowMapper());
    }

    public int add(String tableKey, String value) throws ClientException {
        if (value == null) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        } else if (exists(tableKey, value)) {
            throw new ClientException(ErrorCode.NAME_EXISTS);
        }
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("INSERT INTO ")
                .append(getTable(tableKey))
                .append(" (name) VALUES (?)");
        final Object[] params = { value };
        final int[] types = { Types.VARCHAR };
        return jdbcTemplate.update(sql.toString(), params, types);
    }

    public int rename(String tableKey, Long id, String value) throws ClientException {
        if (value == null || id == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        } else if (exists(tableKey, id, value)) {
            throw new ClientException(ErrorCode.NAME_EXISTS);
        }
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("UPDATE ")
                .append(getTable(tableKey))
                .append(" SET name = ? WHERE id = ?");
        final Object[] params = { value, id };
        final int[] types = { Types.VARCHAR, Types.BIGINT };
        return jdbcTemplate.update(sql.toString(), params, types);
    }

    public int toggleEol(String tableKey, Long id) {
        if (id != null) {
            String tb = getTable(tableKey);
            StringBuilder sql = new StringBuilder(64);
            sql
                    .append("UPDATE ")
                    .append(tb)
                    .append(" SET end_of_life = NOT (SELECT end_of_life FROM ")
                    .append(tb)
                    .append(" WHERE id = ?) WHERE id = ?");

            final Object[] params = { id, id };
            final int[] types = { Types.BIGINT, Types.BIGINT };
            return jdbcTemplate.update(sql.toString(), params, types);
        }
        return 0;
    }

    /**
     * Checks if an entry with given value is already existing in given relation
     * @param tableKey of the table
     * @param value to lookup for
     * @return true if exists
     */
    private boolean exists(String tableKey, String value) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT count(*) FROM ")
                .append(getTable(tableKey))
                .append(" WHERE name = ?");
        final Object[] params = { value };
        final int[] types = { Types.VARCHAR };
        return (jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0);
    }

    /**
     * Checks if an entry with given value is already existing in given relation and has another id if exists.
     * @param tableKey of the table
     * @param id to check for if value already exists
     * @param value to lookup for
     * @return true if exists, false if not or under same id as given
     */
    private boolean exists(String tableKey, Long id, String value) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT count(*) FROM ")
                .append(getTable(tableKey))
                .append(" WHERE id = ? AND name = ?");
        final Object[] params = { id, value };
        final int[] types = { Types.BIGINT, Types.VARCHAR };
        return (jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0);
    }
}
