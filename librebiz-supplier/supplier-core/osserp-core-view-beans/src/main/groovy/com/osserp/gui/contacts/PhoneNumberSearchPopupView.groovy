/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.core.contacts.ContactSearch
import com.osserp.core.contacts.ContactSearchResult
import com.osserp.gui.CoreView

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class PhoneNumberSearchPopupView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(PhoneNumberSearchPopupView.class.getName())

    PhoneNumberSearchPopupView() {
        super()
        headerName = 'phoneNumberSearch'
        enablePopupView()
    }

    void search() {
        def selectedValue = form.getString('value')
        logger.debug("searchPhoneNumer: invoked [value=$selectedValue]")
        if (selectedValue) {
            list = getService(ContactSearch.class.getName()).findPhoneNumbers(selectedValue)
            logger.debug("searchPhoneNumer: search done [resultCount=${list.size()}]")
            for (Iterator i = list.iterator(); i.hasNext();) {
                ContactSearchResult next = i.next()
                if (next.fax) {
                    i.remove()
                }
            }
        } else {
            throw new ClientException(ErrorCode)
        }
    }
}
