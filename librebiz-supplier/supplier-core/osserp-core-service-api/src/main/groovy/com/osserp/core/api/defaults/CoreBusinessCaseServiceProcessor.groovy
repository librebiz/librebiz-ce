/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 17.12.2012 
 * 
 */
package com.osserp.core.api.defaults

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WSBusinessCase
import com.osserp.api.WSItem
import com.osserp.api.WSObjectException

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.Option
import com.osserp.common.OptionsCache
import com.osserp.common.util.DateUtil
import com.osserp.common.util.NumberUtil

import com.osserp.core.BusinessCase
import com.osserp.core.BusinessCaseCreator
import com.osserp.core.BusinessType
import com.osserp.core.FcsAction
import com.osserp.core.Item
import com.osserp.core.Options
import com.osserp.core.api.BusinessCaseServiceProcessor
import com.osserp.core.products.Product
import com.osserp.core.crm.Campaign
import com.osserp.core.customers.Customer
import com.osserp.core.dao.Products
import com.osserp.core.dao.BusinessCaseUtilities
import com.osserp.core.model.ItemImpl
import com.osserp.core.system.BranchOffice

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CoreBusinessCaseServiceProcessor extends AbstractBusinessCaseProcessor implements BusinessCaseServiceProcessor {
    private static Logger logger = LoggerFactory.getLogger(CoreBusinessCaseServiceProcessor.class.getName())

    private BusinessCaseCreator businessCaseCreator
    private CoreCustomerServiceProcessor customerService
    private OptionsCache optionsCache
    private Products products

    protected CoreBusinessCaseServiceProcessor(
            OptionsCache optionsCache,
            Products products,
            CoreCustomerServiceProcessor customerService,
            BusinessCaseUtilities businessCaseUtilities,
            BusinessCaseCreator businessCaseCreator) {
        super(businessCaseUtilities)
        this.products = products
        this.optionsCache = optionsCache
        this.customerService = customerService
        this.businessCaseCreator = businessCaseCreator
    }

    /**
     * Takes a businessCase provided by external app and creates
     * or updates the internal depending on provided values.
     * @param wsBusinessCase
     * @param performActions
     * @return businessCase internal businessCase representation
     * @throws WSObjectException if validation of input failed or create failed
     */
    BusinessCase createOrUpdate(WSBusinessCase wsBusinessCase, boolean performActions) throws WSObjectException {

        if (!wsBusinessCase.contact && !wsBusinessCase.customerId) {
            throw new WSObjectException('wsBusinessCase.contact or wsBusinessCase.customerId required')
        }
        // check for provided businessCase id
        boolean externalBusinessIdRequired = isEnabled('wsBusinessCaseBusinessIdRequired')
        boolean externalBusinessId = false
        
        Long businessId = wsBusinessCase.primaryKey
        if (!businessId && externalBusinessIdRequired) {
            logger.error('createOrUpdate: businessId required but not provided!')
            throw new WSObjectException(ErrorCode.ID_REQUIRED)
        } 
        if (businessId) {
            externalBusinessId = true
            logger.debug("createOrUpdate: businessId provided [id=${businessId}]")
        } else {
            logger.debug("createOrUpdate: no businessId provided but not configured as required, ok")
        }

        // check for businessType and context
        BusinessType businessType = businessCaseUtilities.fetchRequestType(
            wsBusinessCase.businessType, wsBusinessCase.businessTypeName)
        if (!businessType) {
            throw new WSObjectException(ErrorCode.REQUEST_TYPE_MISSING)
        }
        String businessContext = wsBusinessCase.context
        if (!businessContext) {
            logger.error('createOrUpdate: businessCase.context missing')
            throw new WSObjectException(ErrorCode.INVALID_CONTEXT)
        }
        if (businessContext != 'sales' && businessContext != 'request') {
            logger.error("createOrUpdate: businessCase.context invalid -> ${businessContext}")
            throw new WSObjectException(ErrorCode.INVALID_CONTEXT)
        }

        // check for branch
        boolean branchRequired = isEnabled('wsBusinessCaseBranchRequired')
        BranchOffice branchOffice = businessCaseUtilities.fetchBranch(
            wsBusinessCase.branchId, wsBusinessCase.branchKey, branchRequired)
        if (!branchOffice && branchRequired) {
            logger.error('createOrUpdate: branch expected and required but not provided!')
            throw new WSObjectException(ErrorCode.BRANCH_MISSING)
        }

        Customer customer = customerService.findOrCreate(wsBusinessCase)

        Campaign campaign = businessCaseUtilities.fetchCampaign(
            wsBusinessCase.campaignId, 
            wsBusinessCase.campaignName, 
            'wsBusinessCaseCreateMissingCampaign',
            'contactImportOrigin')

        Option origin = businessCaseUtilities.fetchOrigin(
            wsBusinessCase.originId, 
            wsBusinessCase.originName, 
            'wsBusinessCaseCreateMissingOrigin')

        FcsAction action = businessCaseUtilities.fetchFcsAction(businessContext, 
            businessType, wsBusinessCase.statusId, wsBusinessCase.status)

        List<Item> items = fetchItems(wsBusinessCase)

        Date createdDate = getDateCreated(wsBusinessCase) 

        String externalRef = wsBusinessCase.externalKey
        if (externalBusinessId) {
            externalRef = businessId.toString()
        } 
        BusinessCase createdBusinessCase = businessCaseCreator.createOrUpdate(
            businessId,
            externalRef,
            wsBusinessCase.externalUrl,
            createdDate,
            businessContext,
            businessType,
            branchOffice,
            customer,
            wsBusinessCase.salesPersonId,
            wsBusinessCase.projectManagerId,
            wsBusinessCase.name,
            wsBusinessCase.street,
            wsBusinessCase.streetAddon,
            wsBusinessCase.zipcode,
            wsBusinessCase.city,
            wsBusinessCase.countryId,
            campaign,
            origin,
            wsBusinessCase.note,
            action,
            items)
        return createdBusinessCase
    }

    protected List<Item> fetchItems(WSBusinessCase wsBusinessCase) {
        List<Item> result = []
        if (wsBusinessCase.items) {
            wsBusinessCase.items.each {
                Item created = createItem(it)
                if (created) {
                    result << created
                }
            }
        }
        return result
    }

    protected Item createItem(WSItem item) {
        if (item?.product?.primaryKey && item?.quantity) {
            Product product = products.find(item.product?.primaryKey)
            if (product) {
                Long externalId = item.externalId
                if (!externalId) {
                    logger.debug("createItem: external item did not provide externalId [productId=${product.productId}]")
                    externalId = NumberUtil.createLong(item.externalKey)
                }
                if (!externalId) {
                    logger.info("createItem: external item did not provide externalId or externalRef [productId=${product.productId}]")
                } else {
                    logger.debug("createItem: done [productId=${item.product?.primaryKey}, externalId=${externalId}]")
                }

                ItemImpl obj = new ItemImpl(product, item.quantity, item.price)
                obj.externalId = externalId
                return obj
            } else {
                logger.warn("createItem: invoked with non-existing product [productId=${item.product?.primaryKey}]")
                return null
            }
        }
    }

    BusinessCase override(WSBusinessCase wsBusinessCase) throws WSObjectException {

    }
}
