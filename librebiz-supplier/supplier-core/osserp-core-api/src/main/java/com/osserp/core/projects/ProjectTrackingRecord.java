/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.projects;

import java.util.Date;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectTrackingRecord extends Option {
    
    /**
     * Indicates if record data represents a single day 
     * @return true if day of startTime is same as day of endTime
     * or endTime not exists
     */
    boolean isSingleDay();

    /**
     * Provides the tracking record type
     * @return type
     */
    ProjectTrackingRecordType getType();

    /**
     * Sets the tracking record type
     * @param type
     */
    void setType(ProjectTrackingRecordType type);

    /**
     * Provides an internal note or description about the case.
     * @return internal note
     */
    String getInternalNote();

    /**
     * Sets an internal note 
     * @param internalNote
     */
    void setInternalNote(String internalNote);

    /**
     * Provides the startTime if summary is calculated by start and endtime or date of duration on manual input
     * @return startTime
     */
    Date getStartTime();

    /**
     * Sets the startTime if summary is calculated by start and endtime.
     * @return startTime
     */
    void setStartTime(Date startTime);
    
    /**
     * Provides the hours part of startTime
     * @return start hours or null
     */
    Integer getStartHours();
    
    /**
     * Provides the minutes part of startTime
     * @return start minutes or null
     */
    Integer getStartMinutes();
    
    /**
     * Provides the minutes part of startTime as formatted string 
     * (e.g. 00 instead of 0 or null)
     * @return start minutes or empty string
     */
    String getStartMinutesDisplay();

    /**
     * Provides the endTime if summary is calculated by start and endtime.
     * @return endTime
     */
    Date getEndTime();

    /**
     * Sets the endTime if summary is calculated by start and endtime.
     * @return endTime
     */
    void setEndTime(Date endTime);
    
    /**
     * Provides the hours part of endTime
     * @return end hours or null
     */
    Integer getEndHours();
    
    /**
     * Provides the minutes part of endTime
     * @return end minutes or null
     */
    Integer getEndMinutes();
    
    /**
     * Provides the minutes part of endTime as formatted string 
     * (e.g. 00 instead of 0 or null)
     * @return end minutes or empty string
     */
    String getEndMinutesDisplay();

    /**
     * Provides the duration
     * @return duration
     */
    Double getDuration();

    /**
     * Sets the duration if not calculated by start and end time.
     * @return duration
     */
    void setDuration(Double duration);

    /**
     * Indicates that record is charged (e.g. invoice created)
     * @return charged
     */
    boolean isCharged();

    /**
     * Ignore this record when creating invoice
     * @return ignore
     */
    boolean isIgnore();

    /**
     * Changes ignore flag
     * @param user
     * @param ignore
     */
    void changeIgnore(Long user, boolean ignore);

    /**
     * Provides the invoice number when record is charged
     * @return invoiceId
     */
    Long getInvoiceId();

    /**
     * Closes the record by setting invoice id and enabling charged flag.
     * @param invoiceId or null to reset charged status
     */
    void close(Long invoiceId);
 
    /**
     * Updates record values
     * @param user
     * @param recordType
     * @param name
     * @param description
     * @param internalNote
     * @param startTime
     * @param endTime
     * @param duration calculated value if endTime not provided
     */
    void update(
            Long user, 
            ProjectTrackingRecordType recordType, 
            String name, 
            String description,
            String internalNote,
            Date startTime, 
            Date endTime, 
            Double duration);
}
