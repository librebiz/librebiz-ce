/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16-Jan-2007 09:35:34 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Details;
import com.osserp.common.Property;
import com.osserp.common.User;

import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerManager;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Customers;
import com.osserp.core.tasks.ContactUpdateSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CustomerManagerImpl extends AbstractClassifiedContactManager implements CustomerManager {

    private CustomerDetailsManager customerDetailsManager;
    
    /**
     * Creates a new instance of customer mananger. Instances created by this 
     * constructor lack of ability to synchronize changed contact objects with 
     * third party systems.
     * @param customerDetailsManager
     * @param contactsDao
     * @param customers
     */
    protected CustomerManagerImpl(CustomerDetailsManager customerDetailsManager, Contacts contactsDao, Customers customers) {
        super(contactsDao, customers, null);
        this.customerDetailsManager = customerDetailsManager;
    }

    /**
     * Creates a new instance of customer mananger with the ability to 
     * create contact changed events.
     * @param contactsDao
     * @param customers
     * @param contactUpdateSender
     */
    protected CustomerManagerImpl(CustomerDetailsManager customerDetailsManager, Contacts contactsDao, Customers customers, ContactUpdateSender contactUpdateSender) {
        super(contactsDao, customers, contactUpdateSender);
        this.customerDetailsManager = customerDetailsManager;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.customers.CustomerManager#getCustomers(java.lang.Long[])
     */
    public List<Customer> getCustomers(Long[] customerIds) {
        return getCustomersDao().getCustomers(customerIds);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.customers.CustomerManager#getInternalCustomer(java.lang.Long)
     */
    public Customer getInternalCustomer(Long company) {
        return getCustomersDao().fetchInternal(company);
    }

    /* (non-Javadoc)
     * @see com.osserp.common.DetailsManager#findExistingProperties(com.osserp.common.Details)
     */
    public List<Property> findExistingProperties(Details ignorable) {
        return customerDetailsManager.findExistingProperties(ignorable);
    }

    /* (non-Javadoc)
     * @see com.osserp.common.DetailsManager#findExistingValues(java.lang.String, java.util.List)
     */
    public List<Property> findExistingValues(String label, List<Property> ignorable) {
        return customerDetailsManager.findExistingValues(label, ignorable);
    }

    /* (non-Javadoc)
     * @see com.osserp.common.DetailsManager#addProperty(com.osserp.common.User, com.osserp.common.Details, java.lang.String, java.lang.String, java.lang.String)
     */
    public Details addProperty(User user, Details details, String language, String label, String value) throws ClientException {
        return customerDetailsManager.addProperty(user, details, language, label, value);
    }

    /* (non-Javadoc)
     * @see com.osserp.common.DetailsManager#removeProperty(com.osserp.common.User, com.osserp.common.Details, com.osserp.common.Property)
     */
    public Details removeProperty(User user, Details details, Property property) {
        return customerDetailsManager.removeProperty(user, details, property);
    }

    /* (non-Javadoc)
     * @see com.osserp.common.DetailsManager#updateProperty(com.osserp.common.User, com.osserp.common.Details, com.osserp.common.Property, java.lang.String)
     */
    public Details updateProperty(User user, Details details, Property property, String value) {
        return customerDetailsManager.updateProperty(user, details, property, value);
    }

    /* (non-Javadoc)
     * @see com.osserp.common.DetailsManager#updatePropertyLabel(com.osserp.common.User, com.osserp.common.Property, java.lang.String, java.lang.String)
     */
    public void updatePropertyLabel(User user, Property property, String language, String value) {
        customerDetailsManager.updatePropertyLabel(user, property, language, value);
    }

    private Customers getCustomersDao() {
        return (Customers) getContactRelationsDao();
    }
}
