/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 15-Oct-2006 15:45:13 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.User;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.OfficePhone;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.EmployeeCreator;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeDisciplinarian;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeManager;
import com.osserp.core.employees.EmployeeRole;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.employees.EmployeeStatus;
import com.osserp.core.employees.EmployeeType;
import com.osserp.core.hrm.TimeRecordingManager;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.tasks.ContactUpdateSender;
import com.osserp.core.tasks.EmployeeUpdateSender;
import com.osserp.core.telephone.TelephoneConfigurationManager;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.DomainUserManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeManagerImpl extends AbstractClassifiedContactManager implements EmployeeManager {
    private static Logger log = LoggerFactory.getLogger(EmployeeManagerImpl.class.getName());

    private DomainUserManager userManager = null;
    private EmployeeCreator employeeCreator = null;
    private EmployeeUpdateSender ldapSyncSender = null;
    private TimeRecordingManager timeRecordingManager = null;
    private TelephoneConfigurationManager tcManager = null;
    private Suppliers suppliers = null;

    /**
     * Creates a new employee manager
     * @param contactsDao
     * @param employees
     * @param employeeCreator
     * @param suppliers
     * @param contactUpdateSender
     * @param userManager
     * @param ldapSyncSender
     * @param tcManager
     * @param timeRecordingManager
     */
    protected EmployeeManagerImpl(
            Contacts contactsDao,
            Employees employees,
            EmployeeCreator employeeCreator,
            Suppliers suppliers,
            ContactUpdateSender contactUpdateSender,
            DomainUserManager userManager,
            EmployeeUpdateSender ldapSyncSender,
            TelephoneConfigurationManager tcManager,
            TimeRecordingManager timeRecordingManager) {
        super(contactsDao, employees, contactUpdateSender);
        this.employeeCreator = employeeCreator;
        this.userManager = userManager;
        this.ldapSyncSender = ldapSyncSender;
        this.tcManager = tcManager;
        this.timeRecordingManager = timeRecordingManager;
        this.suppliers = suppliers;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.employees.EmployeeManager#create(com.osserp.core.users.DomainUser, com.osserp.core.contacts.Contact, com.osserp.core.employees.EmployeeType, com.osserp.core.system.BranchOffice, com.osserp.core.employees.EmployeeGroup, com.osserp.core.employees.EmployeeStatus, java.util.Date, java.lang.String, java.lang.String)
     */
    public Long create(
            DomainUser user, 
            Contact contact,
            EmployeeType type,
            BranchOffice branch,
            EmployeeGroup group,
            EmployeeStatus status,
            Date beginDate,
            String initials,
            String role) throws ClientException {
        
        if (contact.isEmployee()) {
            ClassifiedContact cus = getEmployeesDao().findByContact(contact.getContactId());
            if (cus != null) {
                if (log.isDebugEnabled()) {
                    log.debug("create() already assigned [contact="
                        + contact.getContactId() + ", id=" + cus.getId() + "]");
                }
                return cus.getId();
            }
        }
        if (group == null) {
            throw new ClientException(ErrorCode.GROUP_MISSING);
        }
        if (type == null) {
            throw new ClientException(ErrorCode.TYPE_MISSING);
        }
        if (status == null) {
            throw new ClientException(ErrorCode.STATUS_MISSING);
        }
        if (beginDate == null) {
            throw new ClientException(ErrorCode.DATE_START_REQUIRED);
        }
        try {
            Employee employee = null;
            employee = employeeCreator.create(
                    user.getEmployee(),
                    contact,
                    type,
                    branch,
                    status,
                    beginDate,
                    initials,
                    role);
            EmployeeRoleConfig config = employee.getDefaultRoleConfig();
            if (config != null) {
                employee = addRole(user, employee, config, group, status);
            }
            return employee.getId();
        } catch (Exception e) {
            throw new BackendException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#getOfficePhones(java.lang.Long)
     */
    public List<OfficePhone> getOfficePhones(Long branchId) {
        Employees employeesDao = getEmployeesDao();
        return employeesDao.getOfficePhones(branchId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#addOfficePhone(com.osserp.core.employees.Employee, java.lang.Long, java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.Long)
     */
    public void addOfficePhone(
            Employee employee,
            Long userId,
            String country,
            String prefix,
            String number,
            String directDial,
            Long branchId)
            throws ClientException {

        if (employee.getBranch() == null) {
            throw new ClientException(ErrorCode.BRANCH_MISSING);
        }
        Employees employeesDao = getEmployeesDao();
        OfficePhone phone = employeesDao.createOfficePhoneNumber(
                userId,
                employee.getContactId(),
                PhoneType.PHONE,
                OfficePhone.BUSINESS,
                country,
                prefix,
                number,
                directDial,
                branchId);
        addOfficePhone(userId, employee, phone);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#addOfficePhone(java.lang.Long, com.osserp.core.employees.Employee, com.osserp.core.contacts.OfficePhone)
     */
    public void addOfficePhone(Long userId, Employee employee, OfficePhone phone) {
        employee.setInternalPhone(phone);
        save(employee);
        updateTelephoneConfiguration(userId, employee.getId(), phone.getInternal());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#removeOfficePhone(java.lang.Long, com.osserp.core.employees.Employee)
     */
    public void removeOfficePhone(Long userId, Employee employee) {
        employee.removeInternalPhone();
        save(employee);
        updateTelephoneConfiguration(userId, employee.getId(), null);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.employees.EmployeeManager#addRoleConfig(com.osserp.core.employees.Employee, com.osserp.core.employees.Employee, com.osserp.core.system.BranchOffice, java.lang.String)
     */
    public Employee addRoleConfig(Employee user, Employee employee, BranchOffice branch, String description) {
        employee.addRoleConfig(user, branch, description);
        if (employee.getRoleConfigs().size() == 1) {
            employee.getRoleConfigs().get(0).setDefaultRole(true);
        }
        save(employee);
        createBillingAccountIfRequired(user, employee, branch);
        return employee;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.employees.EmployeeManager#addRole(com.osserp.core.users.DomainUser, com.osserp.core.employees.Employee, com.osserp.core.employees.EmployeeRoleConfig, com.osserp.core.employees.EmployeeGroup, com.osserp.core.employees.EmployeeStatus)
     */
    public Employee addRole(
            DomainUser user,
            Employee employee,
            EmployeeRoleConfig config,
            EmployeeGroup group,
            EmployeeStatus status) {

        employee.addRole(config, user.getEmployee(), group, status);
        save(employee);
        getEmployeesDao().reload(employee.getId());
        userManager.grantPermissions(user, employee, group);
        return employee;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.employees.EmployeeManager#removeRole(com.osserp.core.users.DomainUser, com.osserp.core.employees.Employee, com.osserp.core.employees.EmployeeRoleConfig, java.lang.Long)
     */
    public Employee removeRole(
            DomainUser user,
            Employee employee,
            EmployeeRoleConfig config,
            Long roleId) {

        Employee obj = getEmployeesDao().get(employee.getId());
        EmployeeGroup group = fetchByRole(config, roleId);
        obj.removeRole(config, user.getEmployee(), roleId);
        save(obj);
        getEmployeesDao().reload(obj.getId());
        userManager.revokePermissions(user, obj, group);
        return obj;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.employees.EmployeeManager#removeRoleConfig(com.osserp.core.users.DomainUser, com.osserp.core.employees.Employee, java.lang.Long)
     */
    public Employee removeRoleConfig(DomainUser user, Employee employee, Long configId) {

        List<EmployeeGroup> deleted = fetchGroupsByConfig(employee, configId);
        employee.removeRoleConfig(configId);
        save(employee);
        if (deleted != null) {
            for (int i = 0, j = deleted.size(); i < j; i++) {
                EmployeeGroup group = deleted.get(i);
                userManager.revokePermissions(user, employee, group);
            }
        }
        return employee;
    }

    private EmployeeGroup fetchByRole(EmployeeRoleConfig config, Long roleId) {
        for (int i = 0, j = config.getRoles().size(); i < j; i++) {
            EmployeeRole role = config.getRoles().get(i);
            if (role.getId().equals(roleId)) {
                return role.getGroup();
            }
        }
        return null;
    }

    private List<EmployeeGroup> fetchGroupsByConfig(Employee employee, Long configId) {
        List<EmployeeGroup> result = new ArrayList<EmployeeGroup>();
        for (int i = 0, j = employee.getRoleConfigs().size(); i < j; i++) {
            EmployeeRoleConfig roleConfig = employee.getRoleConfigs().get(i);
            if (roleConfig.getId().equals(configId)) {
                for (int k = 0, l = roleConfig.getRoles().size(); k < l; k++) {
                    EmployeeRole role = roleConfig.getRoles().get(k);
                    if (role.getGroup() != null) {
                        result.add(role.getGroup());
                    } else if (log.isDebugEnabled()) {
                        log.debug("fetchGroupsByConfig() ignoring role without group [employee="
                                + employee.getId() + ", config="
                                + roleConfig.getId() + ", role="
                                + role.getId() + "]");
                    }
                }
            }
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#save(com.osserp.core.employees.Employee)
     */
    public void save(Employee employee) {
        super.save(employee);
        if (!employee.isActive()) {
            try {
                User user = userManager.find(employee);
                if (user != null) {
                    userManager.disable(user);
                }
            } catch (Throwable t) {
                log.warn("save() ignoring exception while deactivating " +
                        "employee related accounts: " + t.toString(), t);
            }
        }
        getEmployeesDao().reload(employee.getId());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.employees.EmployeeManager#updateDetails(com.osserp.core.employees.Employee, com.osserp.core.employees.Employee, java.lang.Long, com.osserp.core.system.BranchOffice, java.util.Date, java.util.Date, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, boolean, boolean, boolean, boolean)
     */
    public Employee updateDetails(
            Employee user,
            Employee employee,
            BranchOffice branch,
            Long employeeType,
            Date beginDate,
            Date endDate,
            String initials,
            String role,
            String costCenter,
            String signatureSource,
            boolean businessCard,
            boolean internationalBusinessCard,
            boolean cellularPhone,
            boolean emailAccount,
            boolean timeRecordingEnabled) throws ClientException {
        
        employee.setBeginDate(beginDate);
        employee.setEndDate(endDate);
        employee.setInitials(initials);
        employee.setBusinessCard(businessCard);
        employee.setInternationalBusinessCard(internationalBusinessCard);
        employee.setCellularPhone(cellularPhone);
        employee.setEmailAccount(emailAccount);
        employee.setRole(role);
        employee.setCostCenter(costCenter);
        employee.setSignatureSource(signatureSource);

        employee.setTimeRecordingEnabled(timeRecordingEnabled);
        if (timeRecordingEnabled) {
            if (!timeRecordingManager.exists(employee)) {
                try {
                    timeRecordingManager.create(user, employee);
                } catch (Throwable t) {
                    log.error("updateDetails() ignoring failed attempt to add "
                            + "time recording [message=" + t.getMessage() 
                            + "]\n", t);
                    employee.setTimeRecordingEnabled(false);
                }
            }
        }
        EmployeeType type = null;
        if (isSet(employeeType)) {
            type = getType(employeeType);
            employee.setEmployeeType(type);
        }
        save(employee);
        
        if (branch != null && employee.getRoleConfigs().isEmpty()) {
            employee = addRoleConfig(user, employee, branch, null);
            
        } else if (!employee.getRoleConfigs().isEmpty()) {
            for (int i = 0, j = employee.getRoleConfigs().size(); i < j; i++) {
                EmployeeRoleConfig cfg = employee.getRoleConfigs().get(i);
                BranchOffice next = cfg.getBranch();
                createBillingAccountIfRequired(user, employee, next);
            }
        }
        return employee;
    }
    

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#findActiveEmployees()
     */
    public List<Employee> findActiveEmployees() {
        return getEmployeesDao().findActiveEmployees();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#findAllActiveEmployees()
     */
    public List<Option> findAllActiveEmployees() {
        return getEmployeesDao().findActiveNames();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#getStatus()
     */
    public List<EmployeeStatus> getStatus() {
        return getEmployeesDao().getEmployeeStatus();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#getType(java.lang.Long)
     */
    public EmployeeType getType(Long id) {
        try {
            return getEmployeesDao().getEmployeeType(id);
        } catch (Exception e) {
            log.warn("getType() failed [id=" + id + ", message=" + e.getMessage() + "]");
            return null;
        }
    }

    private void updateTelephoneConfiguration(Long userId, Long employeeId, String internal) {
        tcManager.resetInternal(userId, employeeId, internal);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#synchronizeLdapUsers()
     */
    public void synchronizeLdapUsers() {
        if (log.isDebugEnabled()) {
            log.debug("synchronizeLdapUsers() invoked");
        }
        ldapSyncSender.send((Employee) null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#synchronizeLdapUser(com.osserp.core.employees.Employee)
     */
    public void synchronizeLdapUser(Employee employee) {
        if (log.isDebugEnabled()) {
            log.debug("synchronizeLdapUser() invoked [id="
                    + employee.getId() + "]");
        }
        ldapSyncSender.send(employee);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#addDisciplinarian(com.osserp.core.employees.Employee, com.osserp.core.employees.Employee,
     * com.osserp.core.employees.Employee)
     */
    public void addDisciplinarian(Employee user, Employee employee, Employee disciplinarian) throws ClientException {
        getEmployeesDao().addDisciplinarian(user, employee, disciplinarian);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#findDisciplinarians(com.osserp.core.employees.Employee)
     */
    public List<EmployeeDisciplinarian> findDisciplinarians(Employee employee) {
        return getEmployeesDao().findDisciplinarians(employee);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.EmployeeManager#removeDisciplinarian(com.osserp.core.employees.Employee, com.osserp.core.employees.EmployeeDisciplinarian)
     */
    public void removeDisciplinarian(Employee user, EmployeeDisciplinarian disciplinarian) throws ClientException {
        getEmployeesDao().removeDisciplinarian(user, disciplinarian);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.employees.EmployeeManager#isDisciplinarian(com.osserp.core.employees.Employee, com.osserp.core.employees.Employee)
     */
    public boolean isDisciplinarian(Employee employee, Employee disciplinarian) {
        if (employee != null && disciplinarian != null) {
            for (EmployeeDisciplinarian employeeDisciplinarian : findDisciplinarians(employee)) {
                if (disciplinarian.getId().equals(employeeDisciplinarian.getDisciplinarian().getId())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private void createBillingAccountIfRequired(
            Employee user, Employee employee, BranchOffice branch) {
        if (branch != null 
                && employee.getEmployeeType() != null 
                && employee.getEmployeeType().isDefaultType()
                && branch.getCompany() != null) {
            Long supplierId = createBillingAccountId(employee, branch);
            if (suppliers != null && !suppliers.isSalaryAccountExisting(supplierId)) {
                suppliers.createSalaryAccount(user.getId(), supplierId, employee);
            }
        }
    }
    
    private Long createBillingAccountId(Employee employee, BranchOffice branchOffice) {
        StringBuilder sb = new StringBuilder(
                branchOffice.getCompany().getId().toString());
        sb.append(employee.getId());
        return NumberUtil.createLong(sb.toString());
    }

    private Employees getEmployeesDao() {
        return (Employees) getContactRelationsDao();
    }
}
