/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.projects;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.BusinessCase;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectSupplierManager {

    /**
     * Provides existing supplier
     * @param id
     * @return supplier
     */
    ProjectSupplier getById(Long id);

    /**
     * Provides all suppliers by project
     * @param businessCase
     * @return suppliers
     */
    List<ProjectSupplier> getSuppliers(BusinessCase businessCase);

    /**
     * Updates properties
     * @param user
     * @param supplier
     * @param groupName
     * @param deliveryDate
     * @param mountingDate
     * @param mountingDays
     * @param status
     * @return updated relation
     * @throws ClientException on empty groupName
     */
    ProjectSupplier update(
            Employee user,
            ProjectSupplier supplier,
            String groupName,
            Date deliveryDate,
            Date mountingDate,
            Double mountingDays,
            Long status) throws ClientException;

    /**
     * Creates a new project supplier relation
     * @param user
     * @param businessCase
     * @param supplierId
     * @param groupName
     * @param deliveryDate
     * @return project with updated values
     * @throws ClientException on empty groupName
     */
    ProjectSupplier create(
            Employee user,
            BusinessCase businessCase,
            Long supplierId,
            String groupName,
            Date deliveryDate) throws ClientException;

    /**
     * Removes a supplier from project
     * @param user
     * @param supplier
     * @param includePurchaseOrder
     * @throws ClientException if supplier or purchase record not deletable
     */
    void remove(Employee user, ProjectSupplier supplier, boolean includePurchaseOrder) throws ClientException;

    /**
     * Provides an ordered list of all product group and
     * custom project supplier group names.
     * @return
     */
    List<String> getGroupNames();
}
