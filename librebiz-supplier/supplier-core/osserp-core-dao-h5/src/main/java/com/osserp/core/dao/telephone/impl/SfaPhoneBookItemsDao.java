/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on May 25, 2010 1:28:54 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.telephone.SfaPhoneBookItems;
import com.osserp.core.model.telephone.SfaPhoneBookItemImpl;
import com.osserp.core.telephone.SfaPhoneBookItem;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SfaPhoneBookItemsDao extends AbstractDao implements SfaPhoneBookItems {
    private static Logger log = LoggerFactory.getLogger(SfaPhoneBookItemsDao.class.getName());
    protected String sfaPhoneBookItemsTable = null;

    public SfaPhoneBookItemsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        this.sfaPhoneBookItemsTable = getTable(TableKeys.SFA_PHONE_BOOK_ITEMS);
    }

    public void synchronize(PrivateContact privateContact) {
        if (log.isDebugEnabled()) {
            log.debug("synchronize() invoked for phones of PrivateContact [id=" + privateContact.getId() + "]");
        }
        //deactivate items at first, because: emtpy items wouldn't create an update
        //then check if they are still required
        this.deactivatePhoneBookItems(privateContact.getContact().getContactId(), privateContact.getOwner());
        if (!privateContact.isUnused()) {
            Phone mobile = privateContact.getContact().getMobile();
            saveOrUpdate(privateContact, mobile);

            Phone phone = privateContact.getContact().getPhone();
            saveOrUpdate(privateContact, phone);
        }
    }

    private void saveOrUpdate(PrivateContact privateContact, Phone phone) {
        if (log.isDebugEnabled()) {
            log.debug("saveOrUpdate() invoked [privateContactId=" + privateContact.getId() + ", phoneId=" + phone.getId() + "]");
        }
        if (phone.getId() != null) {
            SfaPhoneBookItem sfaPhoneBookItem = fetchPhoneBookItem(phone.getContactId(), phone.getDevice(), privateContact.getOwner());

            Contact contact = privateContact.getContact();
            if (contact != null) {
                sfaPhoneBookItem.setBirthdayItem(contact.getBirthDate());
                sfaPhoneBookItem.setContactId(contact.getContactId());
                sfaPhoneBookItem.setEmail(contact.getEmail());
                sfaPhoneBookItem.setFirstName(contact.getFirstName());
                sfaPhoneBookItem.setLastName(contact.getLastName());
                sfaPhoneBookItem.setName(contact.getDisplayName() + getDeviceDescription(phone.getDevice(), phone.getType()));
                sfaPhoneBookItem.setOrganization((contact.getReferenceContact() != null) ? contact.getReferenceContact().getDisplayName() : null);
                sfaPhoneBookItem.setTitle((contact.getTitle() != null) ? contact.getTitle().getName() : null);
            }
            sfaPhoneBookItem.setFavourite(false);
            sfaPhoneBookItem.setGroupTypeName(null);
            sfaPhoneBookItem.setNote(privateContact.getContactInfo());
            sfaPhoneBookItem.setNumber(phone.getPhoneKey());
            sfaPhoneBookItem.setNumberType(phone.getDevice());
            sfaPhoneBookItem.setSynced(false);
            sfaPhoneBookItem.setUid(privateContact.getOwner());
            sfaPhoneBookItem.setUnused(false);
            getCurrentSession().saveOrUpdate(SfaPhoneBookItemImpl.class.getName(), sfaPhoneBookItem);
        }
    }

    private String getDeviceDescription(Long device, Long type) {
        String typeName = null;
        if (type != null && type.equals(Phone.PRIVATE)) {
            typeName = "P";
        } else if (type != null && type.equals(Phone.BUSINESS)) {
            typeName = "G";
        }
        if (device != null && device.equals(PhoneType.PHONE)) {
            return " (F" + typeName + ")";
        } else if (device != null && device.equals(PhoneType.MOBILE)) {
            return " (M" + typeName + ")";
        }
        return null;
    }

    private SfaPhoneBookItem fetchPhoneBookItem(Long contactId, Long phoneType, String uid) {
        if (log.isDebugEnabled()) {
            log.debug("fetchPhoneBookItem() invoked");
        }
        List<SfaPhoneBookItem> list = getCurrentSession().createQuery(
                "from " + SfaPhoneBookItemImpl.class.getName() 
                + " p where p.contactId = :contactid AND p.numberType = :phonetype AND p.uid = :uid")
                .setParameter("contactid", contactId)
                .setParameter("phonetype", phoneType)
                .setParameter("uid", uid).list();
        return (list.isEmpty() ? new SfaPhoneBookItemImpl() : list.get(0));
    }

    private void deactivatePhoneBookItems(Long contactId, String uid) {
        this.deactivatePhoneBookItems(contactId, null, uid);
    }

    private void deactivatePhoneBookItems(Long contactId, Long phoneType, String uid) {
        StringBuilder sql = new StringBuilder("UPDATE ");
        sql
                .append(sfaPhoneBookItemsTable)
                .append(" SET synced = false, unused = true")
                .append(" WHERE reference_id = ")
                .append(contactId.toString());
        if (phoneType != null) {
            sql
                    .append(" AND number_type = ")
                    .append(phoneType.toString());
        }
        sql
                .append(" AND uid = '")
                .append(uid)
                .append("'");
        jdbcTemplate.update(sql.toString());
    }
}
