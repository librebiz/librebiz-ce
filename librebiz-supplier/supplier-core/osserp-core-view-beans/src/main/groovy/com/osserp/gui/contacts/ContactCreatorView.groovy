/**
 *
 * Copyright (C) 2006, 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 17, 2006 
 * Created on Nov 7, 2012 (Groovy implementation) 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.contacts.Contact

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ContactCreatorView extends ContactCreatingView {
    private static Logger logger = LoggerFactory.getLogger(ContactCreatorView.class.getName())

    boolean ignoreExisting = false
    Long createdContact

    ContactCreatorView() {
        super()
        headerName = 'selectInputForm'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            Long parentId = form.getLong('parent')
            if (parentId) {
                parent = contactSearch.find(parentId)
                headerName = 'newContactPerson'
                selectedType = contactManager.contactPersonType
                // check is performed on createPerson
                ignoreExisting = true
            }
            enableCreateMode()
        } else if (requestIs('disableCreateMode')) {
            headerName = 'selectInputForm'
        } else {
            headerName = 'newContact'
        }
    }

    void confirm() {
        ignoreExisting = true
    }

    @Override
    void save() {
        unlockForm()
        if (updateOnly) {

            countrySelection(form.getLong('country'))
            localitySelection(form.getString('zipcode'))
        } else {

            if (!ignoreExisting) {

                if (selectedType?.business) {
                    existingContacts = contactSearch.findExisting(form.getString('zipcode'), form.getString('company'))
                } else {
                    existingContacts = contactSearch.findExisting(form.getString('zipcode'), form.getString('lastName'))
                }
                if (existingContacts) {
                    lockForm()
                    throw new ClientException(ErrorCode.CONTACT_EQUAL_EXISTS)
                }
            }
            Contact contact

            if (parent) {
                contact = createPerson()
                createdContact = contact.contactId
            } else if (selectedType) {

                if (selectedType.freelance) {
                    contact = createFreelance()
                } else if (selectedType.business) {
                    contact = createBusiness()
                } else {
                    contact = createPrivate()
                }
                setBean(contact)
                createdContact = contact.contactId
            } else {
                // This seems to happen rarely and it's not clear why.
                // We did not get any user report about this but we saw
                // NPEs frequently in the logs.
                // Doing nothing here causes the page to redisplay the
                // contactCreateTypeSelection.jsp which is better than
                // throwing the NPE. We log a warning for now to allow
                // further investigation in case of user reports:
                logger.warn('save: invocation of save while selectedType is null!')
            }
        }
    }

    /**
     * Creates a business contact by current values
     * @throws ClientException if validation while creating failed
     */
    private Contact createBusiness() throws ClientException {
        validate()
        Contact result = contactManager.create(
                domainUser,
                selectedType.id,
                form.getString('companyAffix'),
                form.getString('company'),
                form.getString('note'),
                form.getString('street'),
                form.getString('streetAddon'),
                form.getString('zipcode'),
                form.getString('city'),
                form.getLong('federalStateId'),
                form.getString('federalStateName'),
                form.getLong('country'),
                form.getString('website'),
                form.getString('email'),
                form.getString('phoneNumber'),
                form.getString('faxNumber'),
                form.getString('mobileNumber'),
                Contact.STATUS_OK)

        String lastName = form.getString('lastName')
        if (lastName) {
            // contactPerson values available
            try {
                countryIdContactPerson = getLong('countryContactPerson')
                Contact person = contactManager.create(
                        result,
                        domainUser,
                        form.getLong('salutation'),
                        form.getLong('title'),
                        form.getString('firstName'),
                        lastName,
                        form.getString('streetContactPerson'),
                        form.getString('streetAddonContactPerson'),
                        form.getString('zipcodeContactPerson'),
                        form.getString('cityContactPerson'),
                        form.getLong('federalStateIdContactPerson'),
                        form.getString('federalStateNameContactPerson'),
                        countryIdContactPerson,
                        form.getString('position'),
                        form.getString('section'),
                        form.getString('office'))

                logger.debug("createBusiness: done [contact=${result.getContactId()}, contactPerson=${person.getContactId()}]")

            } catch (ClientException ignorable) {
                // user has to add contact person with a new action
                logger.debug("createBusiness: done, creating person failed with exception [contact=${result.getContactId()}, message=${ignorable.message}]")
            }
        }
        return result
    }

    /**
     * Creates a freelancer or sme contact
     * @throws ClientException if validation while creating failed
     */
    private Contact createFreelance() throws ClientException {
        validate()
        Contact result = contactManager.create(
                domainUser,
                selectedType.id,
                form.getString('companyAffix'),
                form.getString('company'),
                form.getLong('salutation'),
                form.getLong('title'),
                form.getString('firstName'),
                form.getString('lastName'),
                form.getString('note'),
                form.getString('street'),
                form.getString('streetAddon'),
                form.getString('zipcode'),
                form.getString('city'),
                form.getLong('federalStateId'),
                form.getString('federalStateName'),
                form.getLong('country'),
                form.getString('website'),
                form.getString('email'),
                form.getString('phoneNumber'),
                form.getString('faxNumber'),
                form.getString('mobileNumber'),
                Contact.STATUS_OK)
        logger.debug("createFreelance: done [contact=${result.getContactId()}]")
        return result
    }

    /**
     * Creates a private contact by current values
     * @throws ClientException if validation while creating failed
     */
    private Contact createPrivate() throws ClientException {
        validate()
        Contact result = contactManager.create(
                domainUser,
                selectedType.id,
                form.getLong('salutation'),
                form.getLong('title'),
                form.getString('firstName'),
                form.getString('lastName'),
                form.getLong('spouseSalutation'),
                form.getLong('pouseTitle'),
                form.getString('spouseFirstName'),
                form.getString('spouseLastName'),
                form.getDate('birthDate'),
                form.getString('note'),
                form.getString('street'),
                form.getString('streetAddon'),
                form.getString('zipcode'),
                form.getString('city'),
                form.getLong('federalStateId'),
                form.getString('federalStateName'),
                form.getLong('country'),
                form.getString('email'),
                form.getString('phoneNumber'),
                form.getString('faxNumber'),
                form.getString('mobileNumber'),
                Contact.STATUS_OK)
        logger.debug("createPrivate() done [contact=${result.getContactId()}]")
        return result
    }

    private Contact createPerson() throws ClientException {
        validate()
        String firstName = form.getString('firstName')
        String lastName = form.getString('lastName')
        if (contactSearch.contactPersonExisting(parent, lastName, firstName)) {
            throw new ClientException(ErrorCode.NAME_EXISTS)
        }
        Contact contact = contactManager.create(
                parent,
                domainUser,
                form.getLong('salutation'),
                form.getLong('title'),
                firstName,
                lastName,
                form.getDate('birthDate'),
                form.getString('street'),
                form.getString('streetAddon'),
                form.getString('zipcode'),
                form.getString('city'),
                form.getLong('federalStateId'),
                form.getString('federalStateName'),
                form.getLong('country'),
                form.getString('position'),
                form.getString('section'),
                form.getString('office'),
                form.getString('note'),
                form.getString('email'),
                form.getString('phoneNumber'),
                form.getString('faxNumber'),
                form.getString('mobileNumber'))
        logger.debug("createPerson: done [contactId=${contact.contactId}]")
        return contact
    }

}
