/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2014 
 * 
 */
package com.osserp.gui.users

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.ViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/users/accountManagement/*")
@Controller class AccountManagementController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(AccountManagementController.class.getName())

    @Override
    @RequestMapping
    def forward(HttpServletRequest request) {
        def target = super.forward(request)
        AccountManagementView view = getView(request)
        String createWillFail = view.createWillFail()
        if (createWillFail) {
            saveError(request, createWillFail)
            return exit(request)
        }
        return target
    }

    @Override
    @RequestMapping
    def save(HttpServletRequest request) {
        logger.debug("save: invoked [user=${getUserId(request)}]")
        AccountManagementView view = getView(request)
        def target = super.save(request)
        if (!errorAvailable(request)) {
            logger.debug("save: done without errors [user=${getUserId(request)}]")
            return exit(request)
        }
        return target
    }

    @Override
    @RequestMapping
    def disableCreateMode(HttpServletRequest request) {
        logger.debug("disableCreateMode: invoked [user=${getUserId(request)}]")
        exit(request)
    }

    @Override
    @RequestMapping
    def disableEditMode(HttpServletRequest request) {
        logger.debug("disableEditMode: invoked [user=${getUserId(request)}]")
        exit(request)
    }
}
