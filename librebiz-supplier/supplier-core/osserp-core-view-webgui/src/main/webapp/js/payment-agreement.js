/**
 * osserp-core - payment_agreement.js
 * 
 * Copyright 2001-2021 The original author or authors.
 * Licensed under the Apache License, Version 2.0.
 */

function calculatePaymentAgreement() {
  var form = document.forms[0];
  var downpaymentPercent = createDouble(form.downpaymentPercent.value);
  var deliveryInvoicePercent = createDouble(form.deliveryInvoicePercent.value);
  var summary = downpaymentPercent + deliveryInvoicePercent;
 	  
  if (summary > 1) {

    form.finalInvoicePercent.value = 0;
    document.getElementById("fip").firstChild.nodeValue = "0%"; 
    alert(ojsI18n.sumOfPercentages + ' > 100');

  } else if (summary == 1) {

    form.finalInvoicePercent.value = 0;
    document.getElementById("fip").firstChild.nodeValue = "0%"; 

  } else {

    var finalInvoicePercent = 1 - summary;
    form.finalInvoicePercent.value = finalInvoicePercent;
    document.getElementById("fip").firstChild.nodeValue = " " + Math.round(finalInvoicePercent * 100) + "%   "; 
  }
}
 	

function calculateDownpayment() {
  var form = document.forms[0];
  var price = createDouble(document.getElementById("price").firstChild.nodeValue);
  var downpayment = createDouble(form.downpaymentPercent.value);
  var x =  price * downpayment;
  var totalSum = Math.round(x*100)/100; 
  form.downpaymentAmount.value = totalSum; 
  calculateFinalInvoice(); 
}
 	
function calculateDeliveryInvoice() {
  var form = document.forms[0];
  var price = createDouble(document.getElementById("price").firstChild.nodeValue);
  var x =  price * createDouble(form.deliveryInvoicePercent.value);
  var totalSum = Math.round(x*100)/100; 
  form.deliveryInvoiceAmount.value = totalSum;
  calculateFinalInvoice(); 
}
 	
function invalidEditAttempt() {
  initFinalInvoice(false);
  alert(ojsI18n.fieldContentCannotBeChanged);
}

function initFinalInvoice(sendAlert) {
  var form = document.forms[0];
  var price = createDouble(document.getElementById("price").firstChild.nodeValue);
  var downpaymentPercent = createDouble(form.downpaymentPercent.value);
  var deliveryInvoicePercent = createDouble(form.deliveryInvoicePercent.value);
  var downpaymentAmount = createDouble(form.downpaymentAmount.value);
  var deliveryInvoiceAmount = createDouble(form.deliveryInvoiceAmount.value);
  var summary = downpaymentPercent + deliveryInvoicePercent;
 	  
  if (summary > 1) {
    form.finalInvoicePercent.value = 0;
    form.finalInvoiceAmount.value = 0;
    document.getElementById("fip").firstChild.nodeValue = "0%"; 
    if (sendAlert) {
      alert(ojsI18n.sumOfPercentages + ' > 100');
    }

  } else if (summary == 1) {
    form.finalInvoicePercent.value = 0;
    form.finalInvoiceAmount.value = 0;
    document.getElementById("fip").firstChild.nodeValue = "0%"; 

  } else {

    var finalInvoicePercent = 1 - summary;
    var finalInvoiceAmount = Math.round(((price - (downpaymentAmount + deliveryInvoiceAmount)) * 100)) / 100;
    form.finalInvoicePercent.value = finalInvoicePercent;
    form.finalInvoiceAmount.value = finalInvoiceAmount; 
    document.getElementById("fip").firstChild.nodeValue = Math.round(finalInvoicePercent * 100) + "%   "; 
    form.finalInvoiceAmount.value = finalInvoiceAmount;
  }
  form.fixedAmounts.value = "false";
}
 	
function calculateFinalInvoice() {
  initFinalInvoice(true);
}

function calculateDownpaymentAmount() {
  var form = document.forms[0];
  var price = createDouble(document.getElementById("price").firstChild.nodeValue);
  var downpayment = createDouble(form.downpaymentAmount.value);
  var finalAmount =  price - downpayment;
  form.deliveryInvoiceAmount.value = 0;
  form.fixedAmounts.value = "true";
  form.finalInvoiceAmount.value = Math.round(finalAmount * 100)/100;
}

function calculateDeliveryInvoiceAmount() {
  var form = document.forms[0];
  var price = createDouble(document.getElementById("price").firstChild.nodeValue);
  var downpayment = createDouble(form.downpaymentAmount.value);
  var delivery = createDouble(form.deliveryInvoiceAmount.value);
  var finalAmount =  price - (downpayment + delivery);
  form.fixedAmounts.value = "true";
  form.finalInvoiceAmount.value = Math.round(finalAmount * 100)/100;
}

function initPaymentFields() {
  var fixedAmounts = document.getElementById("fixedAmounts").value;
  if (fixedAmounts == "false") {
    showPaymentFields();
  } else {
    hidePaymentFields();
  }
}

function deletePaymentFields() {
  document.getElementById("downpaymentAmount").value = "";
  document.getElementById("deliveryInvoiceAmount").value = "";
  document.getElementById("finalInvoiceAmount").value = "";
}

function hidePaymentFields() {
  var percentElements = document.getElementsByName("percentField");
  for( var x = 0; x < percentElements.length; x++ ) {
    percentElements[x].style.visibility = "hidden";
  }  
}

function showPaymentFields() {
  var percentElements = document.getElementsByName("percentField");
  for( var x = 0; x < percentElements.length; x++ ) {
    percentElements[x].style.visibility = "visible";
  }
  deletePaymentFields();
}

function calculateDownpaymentAmount() {
  var form = document.forms[0];
  var price = createDouble(document.getElementById("price").firstChild.nodeValue);
  var downpayment = createDouble(form.downpaymentAmount.value);
  var finalAmount =  price - downpayment;
  form.deliveryInvoiceAmount.value = 0;
  form.fixedAmounts.value = "true";
  form.finalInvoiceAmount.value = Math.round(finalAmount * 100)/100;
  hidePaymentFields();  
}

function calculateDeliveryInvoiceAmount() {
  var form = document.forms[0];
  var price = createDouble(document.getElementById("price").firstChild.nodeValue);
  var downpayment = createDouble(form.downpaymentAmount.value);
  var delivery = createDouble(form.deliveryInvoiceAmount.value);
  var finalAmount =  price - (downpayment + delivery);
  form.fixedAmounts.value = "true";
  form.finalInvoiceAmount.value = Math.round(finalAmount * 100)/100;
  hidePaymentFields();  
}
