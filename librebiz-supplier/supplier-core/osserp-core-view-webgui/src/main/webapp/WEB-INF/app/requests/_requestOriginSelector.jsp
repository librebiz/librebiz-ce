<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestOriginSelectorView"/>

<div class="row row-modal">
    <div class="col-md-6 panel-area panel-area-modal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="${view.headerName}"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive table-responsive-default">
                <table class="table">
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.requestOriginTypes}">
                                <tr>
                                    <td class="left"><fmt:message key="noSelectionAvailable"/></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="obj" items="${view.requestOriginTypes}" varStatus="s">
                                    <c:choose>
                                        <c:when test="${obj.id == view.selectedRequest.originType}">
                                            <tr class="altrow"><td><o:out value="${obj.name}"/></td></tr>
                                        </c:when>
                                        <c:otherwise>
                                            <tr>
                                                <td><v:link url="/requests/requestOriginSelector/save?id=${obj.id}"><o:out value="${obj.name}"/></v:link></td>
                                            </tr>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
