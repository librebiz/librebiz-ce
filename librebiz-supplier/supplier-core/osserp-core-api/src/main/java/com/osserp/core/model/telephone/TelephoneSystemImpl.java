/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 11:02:00 AM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.TelephoneSystemType;
import com.osserp.core.telephone.TelephoneSystem;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneSystemImpl extends AbstractEntity implements TelephoneSystem {
    private TelephoneSystemType systemType = null;
    private Long branchId = null;
    private String name = null;
    private String ipRange = null;
    private String hostname = null;
    private boolean active = false;

    protected TelephoneSystemImpl() {
        super();
    }

    public TelephoneSystemImpl(TelephoneSystemType systemType, String name, String ipRange, String hostname, boolean active) {
        super();
        this.systemType = systemType;
        this.name = name;
        this.ipRange = ipRange;
        this.hostname = hostname;
        this.active = active;
    }

    public TelephoneSystemType getSystemType() {
        return systemType;
    }

    public void setSystemType(TelephoneSystemType systemType) {
        this.systemType = systemType;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpRange() {
        return ipRange;
    }

    public void setIpRange(String ipRange) {
        this.ipRange = ipRange;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
