/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 26, 2011 at 7:03:14 PM 
 * 
 */
package com.osserp.core.service.requests;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.service.ResourceLocator;
import com.osserp.core.crm.CampaignAssignmentHistory;
import com.osserp.core.dao.Requests;
import com.osserp.core.requests.Request;
import com.osserp.core.service.events.EventTaskTicketCreator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestOriginChangedEventTask extends AbstractRequestChangedEventTask {
    private static Logger log = LoggerFactory.getLogger(RequestOriginChangedEventTask.class.getName());
    private static final String REQUEST_ORIGIN_CHANGED = "requestOriginChanged";
    private OptionsCache options;
    private EventTaskTicketCreator ticketCreator;

    protected RequestOriginChangedEventTask(Requests requests, ResourceLocator resourceLocator, EventTaskTicketCreator ticketCreator, OptionsCache options) {
        super(requests, resourceLocator);
        this.options = options;
        this.ticketCreator = ticketCreator;
    }

    @Override
    protected void executeTask(Request request, Map<String, Object> values) {
        List<CampaignAssignmentHistory> history = getRequests().getCampaignAssignmentHistory(request);
        Long historyId = (Long) values.get("historyId");
        if (historyId != null && !history.isEmpty()) {
            for (int i = 0, j = history.size(); i < j; i++) {
                CampaignAssignmentHistory entry = history.get(i);
                if (entry.getId().equals(historyId)) {
                    String createdBy = "System";
                    if (entry.getCreatedBy() != null && options.getMap("employees").containsKey(entry.getCreatedBy())) {
                        createdBy = options.getMap("employees").get(entry.getCreatedBy()).getName();
                    }
                    StringBuilder text = new StringBuilder();
                    if (entry.getPreviousCampaign() == null) {
                        text.append("None");
                    } else {
                        text.append(entry.getPreviousCampaign().getName());
                    }
                    text.append(" => ");
                    if (entry.getCampaign() == null) {
                        text.append("None");
                    } else {
                        text.append(entry.getCampaign().getName());
                    }
                    text.append(" / ").append(createdBy);
                    ticketCreator.createTickets(
                            REQUEST_ORIGIN_CHANGED,
                            request.getRequestId(),
                            request.getName(),
                            request.getBranch().getId(),
                            request.getManagerId(),
                            request.getSalesId(),
                            entry.getCreatedBy(),
                            text.toString(),
                            null);
                    if (log.isDebugEnabled()) {
                        log.debug("executeTask() done [request=" + request.getRequestId() + ", historyId=" + historyId + "]");
                    }
                    break;
                }
            }
        }
    }

}
