<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.contactPersonView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="contactPersonView" value="${sessionScope.contactPersonView}" />
<c:set var="contact" value="${contactPersonView.bean}" />
<c:set var="actionUrl" scope="request" value="/contactPersons.do" />
<c:set var="editorContactView" scope="request" value="${sessionScope.contactPersonView}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
.city {
	width: 143px;
}
</style>
    </tiles:put>

    <tiles:put name="headline"><fmt:message key="contactPerson" />
    </tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/contactPersons.do?method=exitDisplay"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
            <o:permission role="sync_contacts" info="permissionSyncAllPrivateContactsAgain">
                <li>
                    <o:ajaxLink linkId="privateContactDisplay" url="/privateContacts.do?method=syncAllPrivateContacts" title="startSynchronization"><o:img name="replaceIcon" /></o:ajaxLink>
                </li>
            </o:permission>
            <c:if test="${contactPersonView.privateContactCreateable}">
                <c:choose>
                    <c:when test="${contactPersonView.privateContactOfCurrentEmployee}">
                        <li>
                            <o:ajaxLink linkId="privateContactDisplay" url="${'/privateContacts.do?method=load&id='}${contact.contactId}" title="displayMyContactDetailsLabel">
                                <o:img name="userAddIcon" />
                            </o:ajaxLink>
                        </li>
                        <o:permission role="secretary" info="permissionSecretary">
                            <li>
                                <o:ajaxLink linkId="privateContactDisplay" url="/privateContacts.do?method=enableCreate" title="assignContact">
                                    <o:img name="enabledIcon" />
                                </o:ajaxLink>
                            </li>
                        </o:permission>
                    </c:when>
                    <c:otherwise>
                        <o:permission role="secretary" info="permissionSecretary">
                            <li>
                                <o:ajaxLink linkId="privateContactDisplay" url="/privateContacts.do?method=enableCreate" title="assignContact">
                                    <o:img name="enabledIcon" />
                                </o:ajaxLink>
                            </li>
                        </o:permission>
                        <o:forbidden role="secretary">
                            <li>
                                <a href="<c:url value="/contactPersons.do?method=enablePrivateContact"/>" title="<fmt:message key="addContactToMyContacts"/>">
                                    <o:img name="enabledIcon" />
                                </a>
                            </li>
                        </o:forbidden>
                    </c:otherwise>
                </c:choose>
            </c:if>
            <li><v:link url="/notes/note/forward?id=${contact.contactId}&name=contactNote&exit=/contactPersonDisplay.do" title="displayAndWriteNotes"><o:img name="texteditIcon"/></v:link></li>
            <li><v:link url="/contacts/contactEditor/forward?id=${contact.contactId}&exit=/contactPersonDisplay.do" title="contactEdit"><o:img name="writeIcon"/></v:link></li>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon" /></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="subcolumns">
            <div class="column50l">
                <div class="subcolumnl">
                    <div class="spacer"></div>
                    <c:import url="/pages/contacts/shared/contact_person_header.jsp"/>
                    <div class="spacer"></div>

                    <div class="contentBox">
                        <div class="contentBoxData">
                            <div class="subcolumns">
                                <div class="subcolumn">
                                    <div class="smallSpacer"></div>
                                    <c:import url="/pages/contacts/shared/contact_person_data.jsp"/>
                                </div>
                            </div>
                            <div class="smallSpacer"></div>
                        </div>
                    </div>
                    <div class="spacer"></div>
                </div>
            </div>

            <div class="column50r">
                <div class="subcolumnr">
                    <div class="spacer"></div>
                    <c:set var="communicationView" scope="request" value="${contactPersonView}"/>
                    <c:import url="/pages/contacts/shared/contact_phones.jsp"/>
                    <div class="spacer"></div>
                    <c:import url="/pages/contacts/shared/contact_fax.jsp"/>
                    <div class="spacer"></div>
                    <c:import url="/pages/contacts/shared/contact_mobiles.jsp"/>
                    <div class="spacer"></div>
                    <c:import url="/pages/contacts/shared/contact_emails.jsp"/>
                </div>
            </div>
        </div>
        <div class="spacer"></div>
        
        <c:import url="/pages/contacts/contact_person_parent_display.jsp"/>
        
    </tiles:put>
</tiles:insert>
