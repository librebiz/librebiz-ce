<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="queryDetailConfiguratorView" />

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="${view.headerName}" />
    </div>
</div>
<div class="modalBoxData">

    <div class="col-md-12 panel-area panel-area-default">
        <div class="panel-body">
            <div class="form-body">
                <v:ajaxForm name="queryDetailConfiguratorView" url="${view.baseLink}/save">

                    <c:choose>
                        <c:when test="${view.inputMode}">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><fmt:message key="labelText" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <v:text name="label" value="${view.bean.label}" styleClass="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><fmt:message key="name" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <v:text name="name" value="${view.bean.name}" styleClass="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><fmt:message key="dataType" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select name="type">
                                            <c:forEach var="obj" items="${view.query.availableTypes}">
                                                <option value="<o:out value="${obj}"/>" <c:if test="${view.bean.typeName == obj}"> selected="selected"</c:if>><o:out value="${obj}" /></option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </c:when>
                        <c:otherwise>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><fmt:message key="name" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <v:text name="name" value="${view.bean.name}" styleClass="form-control" />
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><fmt:message key="type" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select name="type">
                                            <c:forEach var="obj" items="${view.query.availableTypes}">
                                                <option value="<o:out value="${obj}"/>" <c:if test="${view.bean.typeName == obj}"> selected="selected"</c:if>><o:out value="${obj}" /></option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><fmt:message key="display"/></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label for="display">
                                                <c:choose>
                                                    <c:when test="${view.createMode}">
                                                        <v:checkbox name="display" value="true" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <v:checkbox name="display" value="${view.bean.display}" />
                                                    </c:otherwise>
                                                </c:choose>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><fmt:message key="size" /></label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <v:text name="width" value="${view.bean.width}" styleClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label><fmt:message key="alignment" /></label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select name="alignment">
                                            <c:forEach var="obj" items="${view.alignments}">
                                                <option value="<o:out value="${obj}"/>" <c:if test="${view.bean.alignment == obj}"> selected="selected"</c:if>><o:out value="${obj}" /></option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>

                        </c:otherwise>
                    </c:choose>

                    <div class="row next">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v:ajaxLink url="${view.baseLink}/exit" targetElement="${view.name}_popup">
                                            <input class="form-control cancel" type="button" value="<fmt:message key="exit"/>" />
                                        </v:ajaxLink>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <o:submit styleClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </v:ajaxForm>
            </div>
        </div>
    </div>

 </div>
