/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2014 
 * 
 */
package com.osserp.gui.users

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.User
import com.osserp.common.UserAuthenticator

import com.osserp.core.employees.Employee
import com.osserp.core.users.AccountCreatorService

import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class AccountManagementView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AccountManagementView.class.getName())

    Employee employee
    String uid

    AccountManagementView() {
        super()
        dependencies = ['contactView']
        headerName = 'changeAccountData'
    }

    String createWillFail() {
        if (createMode) {
            logger.debug("createWillFail: invoked in createMode [id=${employee?.id}]")
            return accountCreatorService.createWillFail(employee)
        }
        return null
    }
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            Long id = form.getLong('id')
            if (id) {
                if (env.contactView?.bean instanceof Employee) { 
                    if (id != env.contactView.bean.id) {
                        throw new ViewContextException("param.id does not match env.contactView.bean.id")
                    }
                    employee = env.contactView.bean
                    logger.debug("initRequest: fetched employee by contactView [id=${employee?.id}]")

                } else if (env.contactView?.bean) {
                    employee = employeeSearch.find(env.contactView.bean.contactId)
                    if (employee?.id != id) {
                        throw new ViewContextException("employee.contactId does not match env.contactView.bean.contactId")
                    }
                    logger.debug("initRequest: fetched employee by contactId search [id=${employee?.id}]")
                }
                if (!employee) {
                    throw new ViewContextException("did not find relating env.contactView")
                }
                uid = accountCreatorService.createUidSuggestion(employee)
                if (env.contactView?.userAccount?.ldapUid) {
                    // user already exists
                    bean = env.contactView.userAccount
                    enableEditMode()
                } else {
                    headerName = 'createAccount'
                    enableCreateMode()
                }
            } else {
                if (!env.contactView?.bean) {
                    throw new ViewContextException('env.contactView.bean does not exist')
                }
                uid = form.getString('uid')
                if (!uid || env.contactView?.userAccount?.ldapUid != uid) {
                    throw new ViewContextException("param.uid does not exist OR does not match env.contactView.userAccount.ldapUid")
                }
                employee = env.contactView.bean
                bean = env.contactView.userAccount
                
                if (accountCreatorService.createRequired(employee)) {
                    enableCreateMode()
                } else {
                    enableEditMode()
                }
            }
        }
    }

    @Override
    void save() {
        String password = form.getString('newPassword')
        String confirmPassword = form.getString('confirmPassword')
        if (!password || !confirmPassword) {
            throw new ClientException(ErrorCode.PASSWORD_MISSING)
        }
        if (password != confirmPassword) {
            throw new ClientException(ErrorCode.PASSWORD_MATCH)
        }
        if (createMode) {
            String uid = form.getString('uid')
            if (!uid) {
                throw new ClientException(ErrorCode.USER_LOGINNAME_MISSING)
            }
            bean = accountCreatorService.create(domainEmployee, employee, uid, password)
            disableCreateMode()
        } else {
            // change password
            bean = userAuthenticator.changePassword(bean, password)
            disableEditMode()
        }
        logger.debug("save: done [user=${bean?.id}, uid=${bean?.ldapUid}]")
    }

    protected AccountCreatorService getAccountCreatorService() {
        getService(AccountCreatorService.class.getName())
    }

    protected UserAuthenticator getUserAuthenticator() {
        getService(UserAuthenticator.class.getName())
    }
}
