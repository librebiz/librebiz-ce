<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.templateView}"/>
<c:set var="dmsUrl" value="${requestScope.templateDocumentUrl}"/>

<c:if test="${!empty view}">
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <fmt:message key="coverLetter" /> - <fmt:message key="${view.contextName}" /> 
        </div>
        <div class="modalBoxHeaderRight">
            <div class="boxnav">
                <ul>
                    <li>
                        <v:ajaxLink url="${dmsUrl}/exit" linkId="exitLink" targetElement="businessCaseTemplate_popup">
                            <o:img name="backIcon" />
                        </v:ajaxLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="modalBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>
                <table class="table">
                    <tr>
                        <th><fmt:message key="template" /></th>
                        <th>&nbsp;</th>
                    </tr>
                    <c:if test="${!empty sessionScope.errors}">
                        <c:set var="errorsColspanCount" scope="request" value="2" />
                        <c:import url="/errors/_errors_table_entry.jsp" />
                    </c:if>
                    <c:forEach var="obj" items="${view.list}">
                        <tr>
                            <td><o:out value="${obj.name}" /></td>
                            <td class="right">
                                <v:link url="${dmsUrl}/print?id=${obj.id}">
                                    <o:img name="printIcon" />
                                </v:link>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <div class="spacer"></div>
            </div>
        </div>
    </div>
</c:if>