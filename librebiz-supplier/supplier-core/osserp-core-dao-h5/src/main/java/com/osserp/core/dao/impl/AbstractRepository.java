/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06-Nov-2006 10:07:07 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.Property;
import com.osserp.common.dao.AbstractHibernateAndSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.employees.Employee;
import com.osserp.core.model.contacts.groups.EmployeeImpl;
import com.osserp.core.model.system.SystemPropertyImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractRepository extends AbstractHibernateAndSpringDao {
    private static Logger log = LoggerFactory.getLogger(AbstractRepository.class.getName());
    protected static final int MAX_SEARCH_FETCH_SIZE = 250;

    public AbstractRepository(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }
    
    protected Employee fetchEmployee(Long id) {
        if (!isEmpty(id)) {
            try {
                return  (Employee) getCurrentSession().get(EmployeeImpl.class, id);
            } catch (Exception e) {
                log.warn("fetchEmployee() ignoring exception [message=" 
                        + e.getMessage() + ", class=" + e.getClass().getName() + "]");
            }
        }
        return null;
    }

    /**
     * Tries to fetch a system property
     * @param name
     * @return property or null if not exists
     */
    protected final Property fetchSystemProperty(String name) {
        return loadSystemProperty(name);
    }

    /**
     * Provides the value of a system property as boolean.
     * @param name
     * @return true if value was found and value is 'true' or 'yes'
     */
    protected final boolean systemPropertyEnabled(String name) {
        String value = getPropertyString(name);
        if ("true".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value)) {
            return true;
        }
        return false;
    }

    /**
     * Provides the value of a system property.
     * @param name
     * @return value or null if not exists
     */
    protected final String getPropertyString(String name) {
        try {
            Property property = fetchSystemProperty(name);
            if (property == null) {
                log.warn("getPropertyString() undefined systemProperty lookup [name=" + name + "]");
            }
            String result = property == null ? null : property.getValue();
            if ("null".equalsIgnoreCase(result)) {
                return null;
            }
            return result;
        } catch (Exception e) {
            log.warn("getPropertyString() systemProperties not available or not accessible");
            return null;
        }
    }

    /**
     * Provides the value of a system property as id
     * @param name
     * @return id or null if not exists
     */
    public final Long getPropertyId(String name) {
        String value = getPropertyString(name);
        try {
            Long result = (value == null ? null : Long.valueOf(value));
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Updates the value of a system property.
     * @param name
     * @param value
     */
    protected final void updatePropertyValue(String name, String value) {
        try {
            SystemPropertyImpl result = loadSystemProperty(name);
            if (result != null) {
                result.setValue(value);
                persistSystemProperty(result);
                if (log.isDebugEnabled()) {
                    log.debug("updatePropertyValue() done [name=" + name + ", value=" + value + "]");
                }
            } else {
                log.warn("updatePropertyValue() ignoring attempt to update a non-existing property [name=" + name + "]");
            }
        } catch (Exception e) {
            log.warn("updatePropertyValue() systemProperties table not available or not accessible");
        }
    }

    private SystemPropertyImpl loadSystemProperty(String name) {
        try {
            StringBuilder hql = new StringBuilder();
            hql
                .append("from ")
                .append(SystemPropertyImpl.class.getName())
                .append(" prop where prop.name = :propertyName");
            List<Property> list =
                    getCurrentSession().createQuery(hql.toString())
            			.setParameter("propertyName", name)
            			.list();
            return list.isEmpty() ? null : (SystemPropertyImpl) list.get(0);
        } catch (Exception e) {
            log.warn("loadSystemProperty() systemProperties table not available or not accessible");
        }
        return null;
    }

    protected void persistSystemProperty(Property property) {
        getCurrentSession().merge(SystemPropertyImpl.class.getName(), property);
    }

}
