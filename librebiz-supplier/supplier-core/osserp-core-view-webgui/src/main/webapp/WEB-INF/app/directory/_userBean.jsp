<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${!empty view.bean}">
    <c:import url="_userAccount.jsp" />
    <c:choose>
        <c:when test="${view.passwordEditMode}">
            <c:import url="_userPassword.jsp" />
        </c:when>
        <c:when test="${view.editMode}">
            <c:import url="_userPropertyChange.jsp" />
        </c:when>
        <c:otherwise>
            <c:import url="_mailAccount.jsp" />
            <c:import url="_userGroups.jsp" />
            <c:import url="_userPermissions.jsp" />
        </c:otherwise>
    </c:choose>
</c:if>
