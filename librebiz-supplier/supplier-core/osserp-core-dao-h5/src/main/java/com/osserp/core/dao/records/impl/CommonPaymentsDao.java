/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Feb-2013 04:27:24 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.CommonPayments;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.CommonPayment;
import com.osserp.core.model.records.CommonPaymentImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CommonPaymentsDao extends AbstractPayments implements CommonPayments {
    private static Logger log = LoggerFactory.getLogger(CommonPaymentsDao.class.getName());

    public CommonPaymentsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            RecordTypes recordTypes) {
        super(jdbcTemplate, tables, sessionFactory, cache, taxRates, recordTypes);
    }

    public CommonPayment create(
            Employee user,
            ClassifiedContact contact,
            Long company,
            Long branch,
            Long bankAccount,
            Long paymentType,
            Long currency,
            BigDecimal amount,
            Date recordDate,
            Date paid,
            String note,
            boolean reducedTax,
            boolean taxFree, 
            Long taxFreeId,
            boolean template,
            String templateName) throws ClientException {

        BillingType type = fetchBillingType(paymentType);
        CommonPayment obj = new CommonPaymentImpl(
                user, 
                contact, 
                company,
                branch,
                bankAccount,
                currency, 
                type, 
                recordDate,
                paid,
                amount,
                note,
                reducedTax,
                taxFree, 
                taxFreeId,
                getTaxRate(reducedTax),
                template,
                templateName);
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), obj);
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + obj.getId()
                    + ", user=" + (user != null ? user.getId() : "null")
                    + ", contact=" + contact.getId()
                    + ", company=" + company
                    + ", branch=" + branch
                    + ", bankAccount=" + bankAccount
                    + ", paymentType=" + paymentType
                    + ", currency=" + currency
                    + ", amounte=" + amount
                    + ", paid=" + paid
                    + ", note=" + note
                    + "]");
        }
        return obj;
    }

    public CommonPayment update(
            Employee user,
            CommonPayment payment,
            Long paymentType,
            Long currency,
            BigDecimal amount,
            Date recordDate,
            Date paid,
            String note,
            boolean reducedTax,
            boolean taxFree, 
            Long taxFreeId,
            boolean template,
            String templateName) {

        BillingType billingType = fetchBillingType(paymentType);
        CommonPaymentImpl obj = (CommonPaymentImpl) getPayment(payment.getId());
        obj.update(user, billingType, currency, amount, recordDate, paid, 
                note, reducedTax, taxFree, taxFreeId, null, template, templateName);
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), obj);
        if (log.isDebugEnabled()) {
            log.debug("update() done [id=" + obj.getId()
                    + ", user=" + (user != null ? user.getId() : "null")
                    + ", paymentType=" + paymentType
                    + ", currency=" + obj.getCurrency()
                    + ", amounte=" + obj.getAmount()
                    + ", created=" + obj.getCreated()
                    + ", paid=" + obj.getPaid()
                    + ", note=" + obj.getNote()
                    + ", reducedTax=" + obj.isReducedTax()
                    + ", taxFree=" + obj.isTaxFree()
                    + ", taxFreeId=" + obj.getTaxFreeId()
                    + ", template=" + obj.isTemplateRecord()
                    + ", templateName=" + obj.getTemplateName()
                    + "]");
        }
        return obj;
    }

    public List<CommonPayment> getTemplates() {
        return getCurrentSession().createQuery(
                "from " + getPersistentClass().getName() 
                + " o where o.templateRecord = true order by o.templateName").list();
    }

    public void disableTemplate(CommonPayment template) {
        template.disableTemplate();
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), template);
    }

    @Override
    protected Class<CommonPaymentImpl> getPersistentClass() {
        return CommonPaymentImpl.class;
    }
}
