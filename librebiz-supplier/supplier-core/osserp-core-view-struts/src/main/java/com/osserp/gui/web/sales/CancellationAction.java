/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 20-Jun-2006 10:56:50 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;

import com.osserp.core.finance.Record;
import com.osserp.core.sales.SalesCreditNote;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.sales.SalesCancellationView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CancellationAction extends AbstractSalesRecordAction {
    private static Logger log = LoggerFactory.getLogger(CancellationAction.class.getName());

    @Override
    protected Class<SalesCancellationView> getViewClass() {
        return SalesCancellationView.class;
    }

    /**
     * Creates a cancellation for current invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward create(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("create() request from " + getUser(session).getId());
        }
        SalesCancellationView view = (SalesCancellationView) createView(request);
        try {
            view.create();
            return mapping.findForward(view.getActionTarget());
        } catch (Exception e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
    }

    /**
     * Deletes a cancellation
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("delete() request from " + getUser(session).getId());
        }
        SalesCancellationView view = getSalesCancellationView(session);
        try {
            view.delete();
        } catch (ClientException c) {
            return saveErrorAndReturn(request, c.getMessage());
        } catch (PermissionException p) {
            return saveErrorAndReturn(request, p.getMessage());
        }
        return exit(mapping, form, request, response);
    }

    /**
     * Creates a cancellation for current invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        SalesCancellationView view = getSalesCancellationView(session);
        if (view.isExitTargetAvailable()) {
            String target = view.getExitTarget();
            if (view.getCanceledView() != null) {
                view.getCanceledView().refresh();
            } else if (log.isDebugEnabled()) {
                log.debug("exit() view does not provide reference to canceled records view");
            }
            RequestUtil.saveExitId(request, view.getExitId());
            removeView(session);
            if (log.isDebugEnabled()) {
                log.debug("exit() done [exitTarget=" + target + "]");
            }
            return mapping.findForward(target);
        }
        if (view.getCanceledView() != null) {
            Record canceled = view.getCanceledView().getRecord();
            Record cancelation = view.getRecord();
            if (cancelation.getReference().equals(canceled.getId())) {
                view.getCanceledView().refresh();
                String target = null;
                if (canceled instanceof SalesInvoice) {
                    target = "salesInvoiceView"; 
                } else if (canceled instanceof SalesCreditNote) {
                    target = "salesCreditNoteView";
                }
                if (target != null) {
                    removeView(session);
                    if (log.isDebugEnabled()) {
                        log.debug("exit() done by canceledView [target=" + target + "]");
                    }
                    return mapping.findForward(target);
                }
            } else {
                log.warn("exit() canceledView.record does not reference cancellation [id="
                        + cancelation.getId() + ", record=" + canceled.getId() + "]");
            }
            
        } else if (log.isDebugEnabled()) {
            log.debug("exit() view does not provide reference to canceled records view");
        }
        if (log.isDebugEnabled()) {
            log.debug("exit() done [exit=exit]");
        }
        return mapping.findForward(Actions.EXIT);
    }

    public SalesCancellationView getSalesCancellationView(HttpSession session) {
        return (SalesCancellationView) fetchView(session);
    }
}
