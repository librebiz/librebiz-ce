<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.telephoneConfigurationSearchView}"/>
<c:set var="sortUrl" scope="page" value="/telephones/telephoneConfigurationSearch/sort"/>

<c:if test="${!empty sessionScope.error}">
	&nbsp;
	<div class="errormessage">
		<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
	</div>
    <o:removeErrors/>
</c:if>
<c:if test="${!empty view}">
	<o:logger write="view not empty" level="debug"/>
	<table class="table table-striped">
		<thead>
			<tr>
				<th><v:ajaxLink url="${sortUrl}?key=uid" targetElement="ajaxContent"><fmt:message key="uid"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=displayName" targetElement="ajaxContent"><fmt:message key="nameOnDisplay"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=internal" targetElement="ajaxContent"><fmt:message key="directDial"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=telephone.mac" targetElement="ajaxContent"><fmt:message key="phone"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=accountCode" targetElement="ajaxContent"><fmt:message key="costCenter"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=emailAddress" targetElement="ajaxContent"><fmt:message key="emailAddress"/></v:ajaxLink></th>
				<th class="scrollbar"></th>
			</tr>
		</thead>
		<tbody class="scrolly" style="height:414px;">
			<c:choose>
				<c:when test="${empty view.list}">
					<tr>
						<td colspan="7"><fmt:message key="error.no.telephoneConfigurations.found"/></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="obj" items="${view.list}">
						<tr>
							<td>
								<v:ajaxLink url="/telephones/telephoneConfigurationPopup/load?id=${obj.id}" linkId="telephoneConfigurationPopupView" title="edit">
									<o:out value="${obj.uid}"/>
								</v:ajaxLink>
							</td>
							<td><o:out value="${obj.displayName}"/></td>
							<td>
								<c:choose>
									<c:when test="${empty obj.internal}">-</c:when>
									<c:otherwise><o:out value="${obj.internal}"/></c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${empty obj.telephone}">-</c:when>
									<c:otherwise><o:out value="${obj.telephone.mac}"/></c:otherwise>
								</c:choose>
							</td>
							<td><o:out value="${obj.accountCode}"/></td>
							<td><o:out value="${obj.emailAddress}"/></td>
							<td></td>
						</tr>
					</c:forEach>
					<tr><td colspan="7" style="height:100%;"></td></tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</c:if>