/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 21, 2008 10:08:32 AM 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.NumberFormatter;

import com.osserp.core.Item;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.Records;
import com.osserp.core.finance.RebateAwareRecord;
import com.osserp.core.finance.RebateManager;
import com.osserp.core.products.Product;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractRebateManager extends AbstractService implements RebateManager {
    private static Logger log = LoggerFactory.getLogger(AbstractRebateManager.class.getName());
    private Products products = null;
    private ProductPrices productPrices = null;
    private Records records = null;

    protected AbstractRebateManager(Products products, ProductPrices productPrices, Records records) {
        super();
        this.products = products;
        this.productPrices = productPrices;
        this.records = records;
    }

    public void rebate(RebateAwareRecord record) {

        if (log.isDebugEnabled()) {
            log.debug("rebate() invoked [record=" + record.getId()
                    + ", contact=" + (record.getContact() == null ? "null" : record.getContact().getId())
                    + "]");
        }
        if (record.isRebateAvailable()) {
            this.removeRebates(record);
        }
        double amountA = 0;
        double amountB = 0;
        double amountC = 0;
        Product discountingA = null;
        Product discountingB = null;
        Product discountingC = null;
        double taxRate = record.getAmounts().getTaxRate();

        if (record.getContact() != null && record.getContact() instanceof Customer) {
            Customer customer = (Customer) record.getContact();
            if (customer != null && log.isDebugEnabled()) {
                log.debug("rebate() customer retrieved [id=" + customer.getId()
                        + ", discountEnabled=" + customer.isDiscountEnabled()
                        + ", discountA=" + customer.getDiscountA()
                        + ", discountB=" + customer.getDiscountB()
                        + ", discountC=" + customer.getDiscountC()
                        + "]");

            }
            if (customer != null && customer.isDiscountEnabled()
                    && (isSet(customer.getDiscountA())
                            || isSet(customer.getDiscountB())
                            || isSet(customer.getDiscountC()))) {
                if (log.isDebugEnabled()) {
                    log.debug("rebate() examining invoice for discount [id="
                            + record.getId() + ", itemCount=" + record.getItems().size()
                            + "]");
                }
                for (int i = 0, j = record.getItems().size(); i < j; i++) {
                    Item next = record.getItems().get(i);
                    if (next.getPrice() != null && next.getPrice().doubleValue() > 0) {
                        if (log.isDebugEnabled()) {
                            log.debug("rebate() checking product [id=" + next.getProduct().getProductId()
                                    + ", price=" + next.getPrice()
                                    + ", amount=" + next.getAmount()
                                    + ", group=" + next.getProduct().getGroup().getId()
                                    + ", class=" + next.getProduct().getGroup().getClassification().getId()
                                    + ", discounting=" + next.getProduct().getGroup().getClassification().getDiscountingProduct()
                                    + "]");
                        }
                        if (next.getProduct().getGroup().isClassA() && isSet(customer.getDiscountA())) {
                            amountA = amountA + next.getAmount().doubleValue();
                            if (discountingA == null) {
                                discountingA = products.find(next.getProduct().getGroup().getClassification().getDiscountingProduct());
                            }
                            if (log.isDebugEnabled()) {
                                log.debug("rebate() added discount A [product=" + next.getProduct().getProductId()
                                        + ", amount=" + next.getAmount()
                                        + "]");
                            }
                        } else if (next.getProduct().getGroup().isClassB() && isSet(customer.getDiscountB())) {
                            amountB = amountB + next.getAmount().doubleValue();
                            if (discountingB == null) {
                                discountingB = products.find(next.getProduct().getGroup().getClassification().getDiscountingProduct());
                            }
                            if (log.isDebugEnabled()) {
                                log.debug("rebate() added discount B [product=" + next.getProduct().getProductId()
                                        + ", amount=" + next.getAmount()
                                        + "]");
                            }
                        } else if (next.getProduct().getGroup().isClassC() && isSet(customer.getDiscountC())) {
                            amountC = amountC + next.getAmount().doubleValue();
                            if (discountingC == null) {
                                discountingC = products.find(next.getProduct().getGroup().getClassification().getDiscountingProduct());
                            }
                            if (log.isDebugEnabled()) {
                                log.debug("rebate() added discount C [product=" + next.getProduct().getProductId()
                                        + ", amount=" + next.getAmount()
                                        + "]");
                            }
                        }
                    }
                }
                if (log.isDebugEnabled()) {
                    log.debug("rebate() discountables calculated [amountA=" + amountA
                            + ", amountB=" + amountB
                            + ", amountC=" + amountC + "]");
                }
                boolean changed = false;
                if (amountA > 0 && discountingA != null) {
                    BigDecimal a = new BigDecimal(amountA);
                    BigDecimal discount = a.multiply(new BigDecimal(customer.getDiscountA()));
                    record.addItem(
                            record.getCompany(),
                            discountingA,
                            null, // customName
                            Double.valueOf(-1),
                            taxRate,
                            discount,
                            record.getCreated(),
                            new BigDecimal(productPrices.getPartnerPrice(discountingA, record.getCreated(), 1d)),
                            false,
                            false,
                            new BigDecimal(productPrices.getPurchasePrice(discountingA, discountingA.getCurrentStock(), record.getCreated())),
                            this.createDiscountNote(amountA, customer.getDiscountA()),
                            true,
                            null); // externalId
                    changed = true;
                    if (log.isDebugEnabled()) {
                        log.debug("rebate() added discount [class=A, product="
                                + discountingA.getProductId()
                                + ", amount=" + amountA
                                + ", rate=" + customer.getDiscountA()
                                + ", discount=" + discount + "]");
                    }
                }
                if (amountB > 0 && discountingB != null) {
                    BigDecimal b = new BigDecimal(amountB);
                    BigDecimal discount = b.multiply(new BigDecimal(customer.getDiscountB()));
                    record.addItem(
                            record.getCompany(),
                            discountingB,
                            null, // customName
                            Double.valueOf(-1),
                            taxRate,
                            discount,
                            record.getCreated(),
                            new BigDecimal(productPrices.getPartnerPrice(discountingB, record.getCreated(), 1d)),
                            false,
                            false,
                            new BigDecimal(productPrices.getPurchasePrice(discountingB, discountingB.getCurrentStock(), record.getCreated())),
                            this.createDiscountNote(amountB, customer.getDiscountB()),
                            true,
                            null); // externalId
                    changed = true;
                    if (log.isDebugEnabled()) {
                        log.debug("rebate() added discount [class=B, product="
                                + discountingB.getProductId()
                                + ", amount=" + amountB
                                + ", rate=" + customer.getDiscountB()
                                + ", discount=" + discount + "]");
                    }
                }
                if (amountC > 0 && discountingC != null) {
                    BigDecimal c = new BigDecimal(amountC);
                    BigDecimal discount = c.multiply(new BigDecimal(customer.getDiscountC()));
                    record.addItem(
                            record.getCompany(),
                            discountingC,
                            null, // customName
                            Double.valueOf(-1),
                            taxRate,
                            discount,
                            record.getCreated(),
                            new BigDecimal(productPrices.getPartnerPrice(discountingC, record.getCreated(), 1d)),
                            false,
                            false,
                            new BigDecimal(productPrices.getPurchasePrice(discountingC, discountingC.getCurrentStock(), record.getCreated())),
                            this.createDiscountNote(amountC, customer.getDiscountC()),
                            true,
                            null); // externalId
                    changed = true;
                    if (log.isDebugEnabled()) {
                        log.debug("rebate() added discount [class=C, product="
                                + discountingC.getProductId()
                                + ", amount=" + amountC
                                + ", rate=" + customer.getDiscountC()
                                + ", discount=" + discount + "]");
                    }
                }
                if (changed) {
                    record.markChanged();
                    records.save(record);
                }
            } else if (log.isInfoEnabled()) {
                log.info("rebate() discount disabled [customer="
                        + (customer == null ? "null" : customer.getId()) + "]");
            }
        }
    }

    public void removeRebates(RebateAwareRecord record) {
        List<Item> rebates = new java.util.ArrayList<Item>();
        for (Iterator<Item> i = record.getItems().iterator(); i.hasNext();) {
            Item next = i.next();
            if (RebateAwareRecord.DISCOUNT_A.equals(next.getProduct().getProductId())
                    || RebateAwareRecord.DISCOUNT_B.equals(next.getProduct().getProductId())
                    || RebateAwareRecord.DISCOUNT_C.equals(next.getProduct().getProductId())) {
                rebates.add(next);
            }
        }
        if (!rebates.isEmpty()) {
            for (int i = 0, j = rebates.size(); i < j; i++) {
                Item nextRebate = rebates.get(i);
                record.deleteItem(nextRebate);
            }
            record.markChanged();
            records.save(record);
        }
    }

    private String createDiscountNote(Double amount, Double discount) {
        StringBuilder note = new StringBuilder();
        if (discount != null) {
            String formattedDiscount = NumberFormatter.getValue((discount * 100), NumberFormatter.CURRENCY);
            note.append(formattedDiscount).append("% / ").append(NumberFormatter.getValue(amount, NumberFormatter.CURRENCY));
        }
        return note.toString();
    }

}
