
# Packages

Since Debian GNU/Linux is the default development and deployment platform
for this project, building the project creates Debian packages by default.


#### librebiz-webapp

The `librebiz-webapp` package contains the `war` and corresponding
configuration files required to setup and run the application.

This package does not contain any dependency on other Debian packages
so one can run the war using a different runtime environment.

Even though this package is essential and the only package required, 
it should be replaceable by a custom implementation and is therefore 
declared as a virtual package.


#### librebiz-common

The `librebiz-common` package contains all files and directories 
expected by Librebiz to exist along with basic setup guide.

This package is optional since one may create these artefacts manually 
using different names, file locations and other customisation and/or
using different environment.


#### librebiz-tomcat8

The `librebiz-tomcat8` provides a complete runtime environment based
on the Debian Tomcat 8 packages provided with Jessie and Stretch.


#### librebiz-tomcat9

The `librebiz-tomcat9` provides a complete runtime environment based
on the Debian Tomcat 9 packages provided with Buster.


#### librebiz

The `librebiz` meta package provides all required dependencies.
