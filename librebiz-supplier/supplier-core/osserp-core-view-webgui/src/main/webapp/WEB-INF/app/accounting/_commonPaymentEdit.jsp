<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.type.name}" />
                <o:out value="${view.bean.number}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:if test="${!empty view.bankAccount}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bankAccountNumber" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.bankAccount.bankAccountNumberIntl}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bankAccountLabel" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.bankAccount.name}" />
                        </div>
                    </div>
                </div>
                <br />
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="type" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <select name="paymentType" size="1" class="form-control">
                            <c:forEach var="obj" items="${view.billingTypes}">
                                <option value="${obj.id}" <c:if test="${view.bean.type.id == obj.id}"> selected="selected"</c:if>>
                                    <o:out value="${obj.name}" />
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>

            <v:date name="created" styleClass="form-control" label="recordDate" picker="true" />

            <v:date name="paid" styleClass="form-control" label="paidOnLabel" picker="true" />

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="amount"><fmt:message key="amount" /></label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <v:number name="amount" format="currency" styleClass="form-control" />
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <span style="margin-left: 0px;">EUR</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="vat"><fmt:message key="vatName" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="taxFree"> <v:checkbox id="taxFree" /> <fmt:message key="exemptOfTax" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="taxFreeSubject" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="taxFreeId" value="${view.bean.taxFreeId}" options="taxFreeDefinitions" styleClass="form-control" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group"></div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="reducedTax"> <v:checkbox id="reducedTax" /> <fmt:message key="reducedTurnoverTax" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bookingText"><fmt:message key="bookingText" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:textarea styleClass="form-control" name="note" rows="3" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="saveAsTemplate"><fmt:message key="templateLabel" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="partner"> <v:checkbox id="template" /> <fmt:message key="saveAsTemplate" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="templateName"><fmt:message key="templateName" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="templateName" styleClass="form-control" />
                    </div>
                </div>
            </div>
            
            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" value="saveInput" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
