<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeGroupConfigView"/>

<div class="subcolumns">
	<div class="subcolumn">
		<div class="spacer"></div>
		<div class="table-responsive table-responsive-default">
			<table class="table table-striped">
				<thead>
					<tr>
						<th class="uid"><fmt:message key="id"/></th>
						<th class="name"><fmt:message key="name"/></th>
						<th class="flag">EXE</th>
						<th class="flag">EXC</th>
						<th class="flag">SAL</th>
						<th class="flag">TEC</th>
						<th class="flag">CS</th>
						<th class="flag">ACC</th>
						<th class="flag">LG</th>
						<th class="flag">HRM</th>
						<th class="flag">PUR</th>
						<th class="flag">WS</th>
						<th class="flag">INS</th>
						<th class="flag">OM</th>
						<th class="flag">IT</th>
						<th class="action">Info</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="obj" items="${view.list}">
						<tr>
							<td class="uid" title="LDAP-<fmt:message key="group"/>: <o:out value="${obj.ldapGroup}"/>"><o:out value="${obj.id}"/></td>
							<td class="name"><v:link url="/admin/employees/employeeGroupConfig/select?id=${obj.id}"><o:out value="${obj.name}"/></v:link></td>
							<td class="flag"><c:choose><c:when test="${obj.executive}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.executiveCompany}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.sales}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.technician}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.cs}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.accounting}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.logistics}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.hrm}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.purchasing}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.wholesale}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.installation}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.om}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="flag"><c:choose><c:when test="${obj.it}">X</c:when><c:otherwise>-</c:otherwise></c:choose></td>
							<td class="action">
								<v:ajaxLink linkId="employeeRoleDisplay" url="/employees/employeeRoleDisplay/forward?id=${obj.id}">
									<img src="<c:url value="${applicationScope.importantIcon}"/>" class="icon" title="<fmt:message key="displayMembers"/>"/>
								</v:ajaxLink>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>		
		<div class="spacer"></div>
	</div>
</div>
