<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.purchaseOrderQueryView}" />
<c:set var="records" value="${view.list}" />
<c:set var="displayPrice" value="false" />
<o:permission role="purchasing,accounting,executive" info="permissionOrderQueriesDisplayPrice">
    <c:set var="displayPrice" value="true" />
</o:permission>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles">
<style type="text/css">
.table .unreleased {
    font-style: italic;
}
</style>
    </tiles:put>

    <tiles:put name="headline">
        <c:choose>
            <c:when test="${view.displayConfirmedMode}">
                <o:listSize value="${view.list}" />
                <fmt:message key="confirmedPurchaseOrders" />
            </c:when>
            <c:otherwise>
                <o:listSize value="${view.list}" />
                <fmt:message key="unconfirmedPurchaseOrders" />
            </c:otherwise>
        </c:choose>
        <c:if test="${view.displayOverdueOnlyMode}"> (<fmt:message key="overdue" />)</c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/purchaseOrderQueries.do?method=exit"/>"><o:img name="backIcon" /></a></li>
            <li><v:link url="/purchasing/purchaseOrderSearch/forward?exit=/purchaseOrderQueryForward.do">
                    <o:img name="searchIcon" />
                </v:link></li>
            <c:if test="${!empty sessionScope.user.permissions['executive'] or !empty sessionScope.user.permissions['purchasing'] or !empty sessionScope.user.permissions['purchase_order_create']}">
                <li><v:link url="/contacts/contactSearch/forward?exit=/purchaseOrderQueryForward.do&selectionExit=purchaseOrderQueryForward&selectionTarget=/purchaseOrderCreateForward.do&context=supplier">
                        <o:img name="newdataIcon" />
                    </v:link></li>
            </c:if>
            <c:choose>
                <c:when test="${view.displayConfirmedMode}">
                    <li><a href="<c:url value="/purchaseOrderQueries.do?method=toggleDisplayConfirmedMode"/>" title="<fmt:message key="displayUnconfirmedOnly"/>"><o:img name="smileIcon" /></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/purchaseOrderQueries.do?method=toggleDisplayConfirmedMode"/>" title="<fmt:message key="displayConfirmedOnly"/>"><o:img name="nosmileIcon" /></a></li>
                </c:otherwise>
            </c:choose>
            <c:if test="${view.userIsPurchaser}">
                <c:choose>
                    <c:when test="${view.displayAllMode}">
                        <li><a href="<c:url value="/purchaseOrderQueries.do?method=toggleDisplayAllMode"/>" title="<fmt:message key="displayMyPurchaseOrdersOnly"/>"><o:img name="peopleIcon" /></a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="<c:url value="/purchaseOrderQueries.do?method=toggleDisplayAllMode"/>" title="<fmt:message key="displayAllPurchasersOrders"/>"><o:img name="personalIcon" /></a></li>
                    </c:otherwise>
                </c:choose>
            </c:if>
            <c:choose>
                <c:when test="${view.displayOverdueOnlyMode}">
                    <li><a href="<c:url value="/purchaseOrderQueries.do?method=toggleDisplayOverdueOnlyMode"/>" title="<fmt:message key="displayAllOpenPurchaseOrders"/>"><o:img name="reloadIcon" /></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/purchaseOrderQueries.do?method=toggleDisplayOverdueOnlyMode"/>" title="<fmt:message key="displayOverduePurchaseOrdersOnly"/>"><o:img name="reloadIcon" /></a></li>
                </c:otherwise>
            </c:choose>
            <li><v:link url="/index" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="recordNumber"><fmt:message key="number" /></th>
                                    <th class="recordNumber"><fmt:message key="carryover" /></th>
                                    <th>
                                        <c:choose>
                                            <c:when test="${!empty view.selectedSupplier}">
                                                <a href="<c:url value="/purchaseOrderQueries.do?method=selectSupplier"/>">
                                                    <fmt:message key="company" />
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:message key="company" />
                                            </c:otherwise>
                                        </c:choose>
                                    </th>
                                    <th class="center"><fmt:message key="ordered" /></th>
                                    <th class="center"><fmt:message key="sent" /></th>
                                    <th class="center"><fmt:message key="delivery" /></th>
                                    <th class="center"><fmt:message key="shortEmployeeLabel" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:choose>
                                    <c:when test="${empty view.list}">
                                        <tr>
                                            <td colspan="7">
                                                <fmt:message key="noOpenOrdersAvailable" />
                                                <v:link url="/contacts/contactSearch/forward?exit=/purchaseOrderQueryForward.do&selectionExit=purchaseOrderQueryForward&selectionTarget=/purchaseOrderCreateForward.do&context=supplier">
                                                    [<fmt:message key="createNew" />]
                                                 </v:link>
                                            </td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="record" items="${view.list}" varStatus="s">
                                            <c:if test="${empty view.selectedSupplier or view.selectedSupplier.id == record.contactId}">
                                                <c:choose>
                                                    <c:when test="${record.supportingOverdue and record.overdue and record.unchangeable}">
                                                        <c:set var="cssClass" value="${'red unreleased'}" />
                                                    </c:when>
                                                    <c:when test="${record.supportingOverdue and record.overdue}">
                                                        <c:set var="cssClass" value="${'red'}" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:set var="cssClass" value="${''}" />
                                                    </c:otherwise>
                                                </c:choose>
                                                <tr id="record${record.id}" class="${cssClass}">
                                                    <td class="recordNumber">
                                                        <a href="<c:url value="/purchaseOrder.do?method=display&exit=purchaseOrderQueryDisplay&id=${record.id}&exitId=record${record.id}"/>">
                                                            <c:choose>
                                                                <c:when test="${empty record.reference}">
                                                                    <o:out value="${record.number}" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <o:out value="${record.referenceNumber}" />
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </a>
                                                    </td>
                                                    <td class="recordNumber">
                                                        <c:if test="${!empty record.reference}">
                                                            <o:out value="${record.number}" />
                                                        </c:if>
                                                    </td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${!empty view.selectedSupplier}">
                                                                <o:out value="${record.contactName}" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="<c:url value="/purchaseOrderQueries.do?method=selectSupplier&id=${record.contactId}"/>"><o:out value="${record.contactName}" /></a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td class="center"><o:date value="${record.created}" /></td>
                                                    <td class="center"><o:date value="${record.dateSent}" /></td>
                                                    <td class="center"><o:date value="${record.delivery}" /></td>
                                                    <td class="center">
                                                        <v:ajaxLink linkId="${'createdBy'}${record.id}PopupView" url="/employees/employeePopup/forward?id=${record.createdBy}">
                                                            <oc:employee initials="true" value="${record.createdBy}" />
                                                        </v:ajaxLink>
                                                    </td>
                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
