/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.groovy.web

import javax.servlet.ServletContext
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.service.Locator
import com.osserp.common.service.ResourceLocator
import com.osserp.common.web.ImageDeployer

/**
 * ResourceLocatorInitListener initializes a generic web application environment.
 * 
 * The listener implements the ServletContextListener and is invoked once during
 * app startup. 
 * 
 * The contextInitialized method will lookup for 'viewServletName' initParam to 
 * initialize the ViewContextListener workflow support. After doing that, the 
 * method sets the 'viewServletHome' contextParam based on defaults and initializes 
 * ServiceLocator and ResourceLocator. Finally the method deploys the image paths.
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ResourceLocatorInitListener extends ResourceLocatorDefaultListener implements ServletContextListener {
    private static Logger logger = LoggerFactory.getLogger(ResourceLocatorInitListener.class.getName())

    void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event)
        // set required workflow attributes
        ServletContext ctx = event.getServletContext()
        String viewServletName = getViewServletName(ctx)
        ctx.setAttribute('viewdir', "/WEB-INF/${viewServletName}".toString())
        if (isBackendAvailable(ctx)) {
            try {

                // initialize the i18n resource locator
                ResourceLocator resourceLocator = (ResourceLocator) getService(ctx, ResourceLocator.class.getName())
                ctx.setAttribute(ResourceLocator.class.getName() + '-resource', resourceLocator)

                // deploy image paths
                ImageDeployer.deployImages(ctx, resourceLocator)

            } catch (Exception e) {
                logger.warn("contextInitialized: failed on attempt to init Service and Resource Locators [message=${e.message}]")
            }

            initializeApplication(ctx)

            if (logger.isDebugEnabled()) {
                logger.debug('contextInitialized: done')
            }

        } else {
            logger.error('contextInitialized: failed. Did not find initialized backend.')
        }
    }

    /**
     * Implementing class should place application specific business logic here.
     * Default implementation does nothing here
     * @param context
     */
    protected void initializeApplication(ServletContext context) {
        // may be overidden if required
    }

    /**
     * Implementing class should initialize backend services (e.g. database) 
     * Default implementation does nothing here
     * @param context
     */
    protected void initializeBackend(ServletContext context) {
        // may be overidden if required
    }

    /**
     * Indicates application with initialized backend. Default implementation 
     * assumes a package or script based installation and everything is setup
     * before application startup (e.g. database create and initialization).
     * @param context
     * @return true if app is initialized  
     */
    protected boolean isBackendAvailable(ServletContext context) {
        return (context.getAttribute(getLocatorContextName(context)) != null)
    }

    protected final Object getService(ServletContext context, String name) {
        Locator locator = context.getAttribute(getLocatorContextName(context))
        if (locator) {
            try {
                return locator.lookupService(name)
            } catch (Exception e) {
                logger.warn("getService: locator does not provide requested service or failed to fetch [name=${name}, message=${e.message}]")
                return null
            }
        }
        logger.warn("getService: locator not available for service lookup [name=${name}]")
    }

    /**
     * Invokes Locator.shutdown to shutdown associated ApplicationContext.
     */
    void contextDestroyed(ServletContextEvent event) {
        super.contextDestroyed(event)
    }

    /**
     * Provides the name to save the initialized service locator
     * @param context
     * @return Locator.LOCATOR_CONTEXT_NAME by default
     */
    protected String getLocatorContextName(ServletContext context) {
        Locator.LOCATOR_CONTEXT_NAME
    }

}
