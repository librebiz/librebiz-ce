<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="clientEdit"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <c:if test="${view.bean.contact.displayName != view.bean.name}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status"><fmt:message key="contact"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${view.bean.contact.displayName}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><fmt:message key="name"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="name" value="${view.bean.name}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="letterWindowName"><fmt:message key="letterWindowName"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="letterWindowName" value="${view.bean.letterWindowName}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><fmt:message key="localCourt"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="localCourt" value="${view.bean.localCourt}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><fmt:message key="tradeRegisterId"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="tradeRegisterEntry" value="${view.bean.tradeRegisterEntry}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><fmt:message key="taxId"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="taxId" value="${view.bean.taxId}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><fmt:message key="vatId"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="vatId" value="${view.bean.vatId}"/>
                        </div>
                    </div>
                </div>
                <c:if test="${view.multipleClientsAvailable}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status"><fmt:message key="internalMargin"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                            <v:text styleClass="form-control" name="internalMargin" value="${view.bean.internalMargin}"/>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label for="partner"> 
                                        <v:checkbox id="partner" /> <fmt:message key="partner" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
                
                <div class="row next">
                    <div class="col-md-4"> </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="/company/clientCompany/disableEditMode"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
            </div>
        </div>
    </div>
        
</div>
