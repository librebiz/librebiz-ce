/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01-Sep-2006 08:34:27 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.ViewName;
import com.osserp.core.finance.VolumeInvoiceProcessor;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.sales.SalesOrderVolumeExportManager;
import com.osserp.core.sales.SalesOrderVolumeExportOperation;
import com.osserp.gui.client.sales.SalesVolumeInvoiceView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesVolumeInvoiceView")
public class SalesVolumeInvoiceViewImpl extends SalesInvoiceViewImpl implements SalesVolumeInvoiceView {
    private static Logger log = LoggerFactory.getLogger(SalesVolumeInvoiceViewImpl.class.getName());
    private List<SalesOrderVolumeExportConfig> configs = new ArrayList<SalesOrderVolumeExportConfig>();
    private SalesOrderVolumeExportOperation operation = null;

    private boolean listMode = false;
    private boolean exportableMode = false;

    /**
     * Creates and initializes volume invoice view by loading available configurations.<br/>
     * Action target is 'success'
     */
    protected SalesVolumeInvoiceViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        SalesOrderVolumeExportManager manager = getVolumeExportManager();
        configs = manager.findConfigs();
    }

    public List<SalesOrderVolumeExportConfig> getConfigs() {
        return configs;
    }

    public SalesOrderVolumeExportConfig getConfig() {
        return (operation == null ? null : operation.getConfig());
    }

    public void selectConfig(Long id) throws ClientException {
        setVolumeView(true);
        if (log.isDebugEnabled()) {
            log.debug("selectConfig() invoked [config=" + id + "]");
        }
        if (id == null) {
            this.operation = null;
        } else {
            SalesOrderVolumeExportConfig config = (SalesOrderVolumeExportConfig) CollectionUtil.getById(configs, id);
            SalesOrderVolumeExportManager manager = getVolumeExportManager();
            this.operation = manager.getOperation(getDomainEmployee(), config);
            disableAllModes();
            if (log.isDebugEnabled()) {
                log.debug("load() done");
            }
        }
    }

    public SalesOrderVolumeExportOperation getOperation() {
        return operation;
    }

    public Map<String, Object> getInvoiceArchiveListing() {
        SalesOrderVolumeExportManager volumeExports = getVolumeExportManager();
        return createListing(volumeExports.getInvoiceArchive(operation), "invoiceArchive", false);
    }

    public Map<String, Object> getUnreleasedInvoiceListing() {
        SalesOrderVolumeExportManager volumeExports = getVolumeExportManager();
        return createListing(volumeExports.getOpenInvoices(operation), "unreleasedInvoices", false);
    }

    public Map<String, Object> getUnrelasedOrderListing() {
        SalesOrderVolumeExportManager volumeExports = getVolumeExportManager();
        return createListing(volumeExports.getUnreleasedOrders(operation), "unreleasedOrders", true);
    }

    public boolean isListMode() {
        return listMode;
    }

    public boolean isExportableMode() {
        return exportableMode;
    }

    public void setExportableMode(boolean exportableMode) {
        disableAllModes();
        if (exportableMode) {
            this.exportableMode = true;
            this.listMode = true;
            setList(new ArrayList(operation.getAvailableOrders()));
        }
    }

    @Override
    public void refresh() {
        if (getBean() != null) {
            try {
                super.refresh();
            } catch (Throwable t) {
            }
        }
        SalesOrderVolumeExportManager volumeExports = getVolumeExportManager();
        try {
            volumeExports.refresh(operation);
        } catch (ClientException e) {
            // this should never happen if user was able to create the operation
            throw new IllegalStateException("unexpected invalid config found", e);
        }
        if (isExportableMode()) {
            setExportableMode(true);
        }
    }

    public void createVolume() throws ClientException, PermissionException {
        this.checkPermission(getCreatePermissions());
        if (operation.getAvailableOrderCount() == 0) {
            throw new ClientException(ErrorCode.NO_DATA_AVAILABLE);
        }
        VolumeInvoiceProcessor processor = getProcessor();
        processor.execute(
                getDomainUser(),
                this.operation.getConfig(),
                this.operation.getAvailableOrders());
        disableAllModes();
        disableCreateMode();
        // reload selected config to reset include billed order flag defaults 
        SalesOrderVolumeExportManager volumeExports = getVolumeExportManager();
        this.operation = volumeExports.getOperation(getDomainEmployee(), operation.getConfig());
    }

    public String getCreatePermissions() {
        return operation.getConfig().getCreatePermissions();
    }

    /**
     * Provides the volume export manager
     * @return volume export manager
     */
    private SalesOrderVolumeExportManager getVolumeExportManager() {
        return (SalesOrderVolumeExportManager) getService(
                SalesOrderVolumeExportManager.class.getName());
    }

    /**
     * Provides the volume invoice processor depending on current operation.
     * @return processor
     */
    private VolumeInvoiceProcessor getProcessor() {
        return (VolumeInvoiceProcessor) getService(operation.getConfig().getVolumeProcessorName());
    }

    /**
     * Creates data for a record listing
     * @param recordList to display in resulting listing page
     * @param headerSuffixKey the page -content- header suffix key
     * @param recordDisplayOnly indicates that added records are of type {@code RecordDisplay} instead of {@code Record}
     * @return record listing data map
     */
    private Map<String, Object> createListing(List recordList, String headerSuffixKey, boolean recordDisplayOnly) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("recordList", recordList);
        StringBuilder header = new StringBuilder(256);
        header.append(getConfig().getName());
        String type = getResourceString(headerSuffixKey);
        if (isSet(type)) {
            header.append(" - ").append(type);
        }
        data.put("header", header);
        data.put("recordDisplay", recordDisplayOnly);
        data.put("exitTarget", "volumeInvoiceRefresh");
        return data;
    }

    /**
     * Disables all list modes and clears current list
     */
    private void disableAllModes() {
        this.listMode = false;
        this.exportableMode = false;
        if (getList() != null) {
            getList().clear();
        }
    }
}
