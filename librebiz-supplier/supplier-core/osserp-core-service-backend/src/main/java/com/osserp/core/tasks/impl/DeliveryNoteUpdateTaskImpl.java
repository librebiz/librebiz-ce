/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 3, 2008 10:01:04 AM 
 * 
 */
package com.osserp.core.tasks.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.service.impl.AbstractXmlMessageHandler;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberFormatter;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsItem;
import com.osserp.core.Item;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.records.SalesDeliveryNotes;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.projects.ProjectFcsManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesDeliveryNote;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.tasks.Task;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DeliveryNoteUpdateTaskImpl extends AbstractXmlMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(DeliveryNoteUpdateTaskImpl.class.getName());

    private SalesDeliveryNotes deliveryNotes = null;
    private SalesOrders orders = null;
    private Projects projects = null;
    private FlowControlActions fcsActions = null;
    private ProjectFcsManager fcsManager = null;
    private Employees employees = null;

    public DeliveryNoteUpdateTaskImpl(
            SalesDeliveryNotes deliveryNotes,
            SalesOrders orders,
            Projects projects,
            FlowControlActions fcsActions,
            ProjectFcsManager fcsManager,
            Employees employees) {
        super();
        this.deliveryNotes = deliveryNotes;
        this.orders = orders;
        this.projects = projects;
        this.fcsActions = fcsActions;
        this.fcsManager = fcsManager;
        this.employees = employees;
    }

    @Override
    protected void execute(Document doc, byte[] file) {
        try {
            Element root = doc.detachRootElement();
            Long id = Long.valueOf(root.getChildText("id"));
            SalesDeliveryNote note = (SalesDeliveryNote) deliveryNotes.load(id);
            if (log.isDebugEnabled()) {
                log.debug("execute() invoked [deliveryNote="
                        + note.getId()
                        + ", status="
                        + note.getStatus()
                        + "]");
            }
            if (DeliveryNote.STAT_SENT.equals(note.getStatus())) {
                Order order = fetchOrderIfExists(note);
                if (order != null
                        && order.getBusinessCaseId() != null
                        && projects.exists(order.getBusinessCaseId())) {

                    Sales sales = projects.load(order.getBusinessCaseId());
                    if (order.getOpenDeliveries().isEmpty()) {
                        if (log.isDebugEnabled()) {
                            log.debug("execute() found closed delivery [id=" + order.getId() + "]");
                        }
                        performClosingDelivery(sales, order, note);

                    } else if (note.isCorrection()) {
                        if (log.isDebugEnabled()) {
                            log.debug("execute() found cancelled delivery [id=" + order.getId() + "]");
                        }
                        performCancelDelivery(sales, order, note);

                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("execute() found partial delivery [id=" + order.getId() + "]");
                        }
                        performPartialDelivery(sales, order, note);
                    }

                } else if (log.isDebugEnabled()) {
                    log.debug("execute() nothing to do, "
                            + "no order assoziated with delivery note ["
                            + note.getId() + "]");
                }
            }
        } catch (Throwable t) {
            log.error("execute() failed [message=" + t.getMessage() + "]", t);
        }
    }

    private void performCancelDelivery(Sales sales, Order order, DeliveryNote note) throws Exception {
        for (int i = 0, j = sales.getFlowControlSheet().size(); i < j; i++) {
            FcsItem next = sales.getFlowControlSheet().get(i);
            if (!next.isCancels() && next.getCanceledBy() == null) {
                FlowControlAction action = (FlowControlAction) next.getAction();
                if (action.isClosingDelivery()) {
                    Long employeeId = deliveryNotes.getLastStatusBy(note);
                    if (employeeId != null) {
                        fcsManager.cancelDeliveryClosing(
                            employees.get(employeeId),
                            (SalesOrder) order);
                        if (log.isDebugEnabled()) {
                            log.debug("performCancelDelivery() delivery closing canceled");
                        }
                        break;
                    }
                }
            }
        }

    }

    private void performClosingDelivery(Sales sales, Order order, DeliveryNote note) throws Exception {
        boolean closedFcsAlreadySet = false;
        for (int i = 0, j = sales.getFlowControlSheet().size(); i < j; i++) {
            FcsItem next = sales.getFlowControlSheet().get(i);
            if (!next.isCancels() && next.getCanceledBy() == null) {
                FlowControlAction action = (FlowControlAction) next.getAction();
                if (isFinalDelivery(action)) {
                    closedFcsAlreadySet = true;
                    break;
                }
            }
        }
        if (!closedFcsAlreadySet) {
            List<FcsAction> actions = fcsActions.findByType(sales.getType().getId());
            for (int i = 0, j = actions.size(); i < j; i++) {
                FlowControlAction action = (FlowControlAction) actions.get(i);
                if (isFinalDelivery(action)) {
                    addFlowControlAction(sales, action, note);
                    break;
                }
            }
        }
    }

    private boolean isFinalDelivery(FlowControlAction action) {
        return action != null && action.isClosingDelivery() && action.isTriggered()
                && "deliveryRequest".equals(action.getStartActionName());
    }

    private void performPartialDelivery(Sales sales, Order order, DeliveryNote note) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("performPartialDelivery() trying to set partial delivery fcs action for sales ["
                    + sales.getId() + "]");
        }
        List<FcsAction> actions = fcsActions.findByType(sales.getType().getId());
        for (int i = 0, j = actions.size(); i < j; i++) {
            FlowControlAction action = (FlowControlAction) actions.get(i);
            if (action.isPartialDelivery()) {
                addFlowControlAction(sales, action, note);
                break;
            }
        }
    }

    private void addFlowControlAction(Sales sales, FlowControlAction action, DeliveryNote note)
            throws Exception {
        Employee system = employees.get(Constants.SYSTEM_EMPLOYEE);
        fcsManager.addFlowControl(
            system,
            sales,
            action,
            DateUtil.getCurrentDate(),
            createDeliveryNote(note),
            null,
            null,
            true);
        if (log.isDebugEnabled()) {
            log.debug("addFlowControlAction() done ["
                    + sales.getId() + ":" + action.getId() + "]");
        }
    }

    private Order fetchOrderIfExists(DeliveryNote note) {
        if (note == null || note.getReference() == null) {
            return null;
        }
        try {
            if (orders.exists(note.getReference())) {
                return (Order) orders.load(note.getReference());
            }
        } catch (Throwable t) {
            log.warn("fetchOrderIfExists() ignoring failure ["
                    + t.toString() + "] in context ["
                    + note.getId() + "]");
        }
        return null;
    }

    private String createDeliveryNote(DeliveryNote note) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0, j = note.getItems().size(); i < j; i++) {
            Item next = note.getItems().get(i);
            buffer
                    .append(NumberFormatter.getIntValue(next.getQuantity()))
                    .append(" x ")
                    .append(next.getProduct().getProductId())
                    .append(" - ")
                    .append(next.getProduct().getName())
                    .append("\n");
        }
        return buffer.toString();
    }

}
