<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="annualReportView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>

	<tiles:put name="headline">
		<fmt:message key="financialData"/> <o:out value="${view.year}" />
        <c:choose>
            <c:when test="${view.salesListMode}">- <fmt:message key="debitors" /></c:when>
            <c:when test="${view.supplierListMode}">- <fmt:message key="creditors" /></c:when>
            <c:when test="${view.cashInListMode}">- <o:listSize value="${view.list}"/> <fmt:message key="cashInLabel" /></c:when>
            <c:when test="${view.cashOutListMode}">- <o:listSize value="${view.list}"/> <fmt:message key="cashOutLabel" /></c:when>
        </c:choose>
	</tiles:put>

	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string">
        <div class="content-area" id="annualReportContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.cashInListMode or view.cashOutListMode}">
                        <div class="col-lg-12 panel-area">
                            <c:import url="_annualReportCashList.jsp"/>
                        </div>
                    </c:when>
                    <c:when test="${view.salesListMode}">
                        <div class="col-lg-12 panel-area">
                            <c:import url="_annualReportSalesRecords.jsp"/>
                        </div>
                    </c:when>
                    <c:when test="${view.supplierListMode}">
                        <div class="col-lg-12 panel-area">
                            <c:import url="_annualReportSupplierRecords.jsp"/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_annualReportSummary.jsp"/>
                        <c:if test="${view.bankAccountDocumentsEnabled}">
                            <c:import url="_annualReportBankDocuments.jsp"/>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
