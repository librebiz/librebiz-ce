<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="credentialConfigHeader"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="credentialContext"><fmt:message key="context"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:select name="context" options="${view.types}" autoupdate="true" styleClass="form-control"
                                nameProperty="contextName" resourceLookup="true" value="${view.selectedType.id}" />
                        </div>
                    </div>
                </div>
                <c:if test="${!empty view.credentialContext}">
                
                    <c:if test="${!empty view.availableKeys and !view.selectNewName}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name"><fmt:message key="name"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <oc:select name="name" options="${view.availableKeys}" autoupdate="true" 
                                        styleClass="form-control" value="${view.selectedKey.id}" undefined="new" />
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${view.selectNewName or empty view.availableKeys}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="credentialContext"><fmt:message key="newNameLabel"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <v:text name="newName" styleClass="form-control"/>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="password"><fmt:message key="password"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="password" name="password" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="repeatPassword"><fmt:message key="repeatPassword"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="password" name="confirmPassword" class="form-control" />
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="button" class="form-control cancel" onclick="gotoUrl('<v:url value="/admin/system/credentialConfig/exit"/>');" value="<fmt:message key="break"/>" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="submit" class="form-control" value="<fmt:message key='save'/>" />
                            </div>
                        </div>
                    </div>
                    
                </c:if>
                    
            </div>
        </div>
    </div>
        
</div>
