/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 22, 2007 8:05:40 AM 
 * 
 */
package com.osserp.common;

import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Calendar extends Month {

    Integer getDay();

    String getDayDisplay();

    String getDayDisplayKey();

    Date getDate();

    void setDate(Integer day, Integer month, Integer year);

    int getDayOfWeek(int day);

    int getWeekOfYear(int day);

    /**
     * Indicates that current instance of day is after day of date
     * @param date
     * @return true if this day is after date ignoring time
     */
    boolean isAfterDay(Date date);

    /**
     * Indicates that day is in the future
     * @return true if this day is in future
     */
    boolean isFuture();

    /**
     * Indicates that day is in the past
     * @return true if this day is in the past
     */
    boolean isPast();

    /**
     * Indicates that day is current day
     * @return true if this day is current day
     */
    boolean isToday();

    /**
     * Indicates that day is one day after current day
     * @return true if this day is tomorrow
     */
    boolean isTomorrow();

    /**
     * Indicates that day is one day before current day
     * @return true if this day is yesterday
     */
    boolean isYesterday();

    /**
     * Indicates that day is saturday or sunday
     * @return weekend
     */
    boolean isWeekend();
}
