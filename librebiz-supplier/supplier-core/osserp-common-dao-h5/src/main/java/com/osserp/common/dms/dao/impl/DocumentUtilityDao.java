/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 4 Aug 2015 
 * 
 */
package com.osserp.common.dms.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.DocumentTypes;
import com.osserp.common.dms.dao.DocumentUtility;
import com.osserp.common.service.SecurityService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DocumentUtilityDao extends FilesystemDocumentsDao implements DocumentUtility {

    private DocumentTypes types;

    public DocumentUtilityDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            String documentTableKey,
            String categoryTableKey,
            DocumentTypes documentTypes,
            DocumentEncoder documentEncoder, 
            SecurityService authenticationProvider) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                documentTableKey,
                categoryTableKey,
                documentEncoder,
                authenticationProvider);
        types = documentTypes;
    }

    public List<DocumentType> getTypes() {
        return types.getList();
    }

    public List<DmsDocument> findByType(Long documentType) {
        StringBuilder query = new StringBuilder("from ");
        query
            .append(getPersistentClass().getName())
            .append(" o where o.type.id = :typeId order by o.id desc");
        
        return getCurrentSession().createQuery(query.toString())
                .setParameter("typeId", documentType).list();
    }

    public int getCountByType(Long documentType) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql
                .append(getDocumentTable())
                .append(" WHERE type_id = ").append(documentType);
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class);
    }

}
