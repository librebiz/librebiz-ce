/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 17, 2010 2:10:02 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateAndSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.telephone.SfaTelephoneActions;
import com.osserp.core.model.telephone.SfaTelephoneActionImpl;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class SfaTelephoneActionsDao extends AbstractHibernateAndSpringDao implements SfaTelephoneActions {
    private static Logger log = LoggerFactory.getLogger(SfaTelephoneActionsDao.class.getName());

    public SfaTelephoneActionsDao() {
        super();
    }

    public SfaTelephoneActionsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public void createAction(String action, String macaddress, Long telephoneSystemId) {
        if (log.isDebugEnabled()) {
            log.debug("createAction() invoked [action=" + action + ", macaddress=" + macaddress + ", telephoneSystemId=" + telephoneSystemId + "]");
        }
        if (action != null || macaddress != null || telephoneSystemId != null) {
            getCurrentSession().saveOrUpdate(SfaTelephoneActionImpl.class.getName(),
                    new SfaTelephoneActionImpl(action, macaddress, telephoneSystemId));
        }
    }
}
