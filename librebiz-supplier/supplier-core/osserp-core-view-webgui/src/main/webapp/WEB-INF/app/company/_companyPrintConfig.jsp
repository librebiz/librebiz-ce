<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="settings" />
                <c:if test="${!empty view.selectedCompany && view.multipleClientsAvailable}">
                    - <o:out value="${view.selectedCompany.name}" rawTitle="${view.selectedCompany.id}" />
                </c:if>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="table-responsive">
                <table class="table">

                    <c:choose>
                        <c:when test="${!empty view.selectedPropertyName}">
                            <c:forEach var="prop" items="${view.selectedProperties}">
                                <c:if test="${prop.name == view.selectedPropertyName}">
                                    <tr>
                                        <td><fmt:message key="${prop.name}" /></td>
                                        <td><v:text styleClass="form-control" name="value" value="${prop.value}" /></td>
                                    </tr>
                                    <tr>
                                        <td><v:submitExit url="${view.baseLink}/selectProperty"/></td>
                                        <td><o:submit styleClass="form-control" /></td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <c:set var="clientLogoExit" scope="request"
                                value="/company/companyPrintConfig/reload" />
                            <c:import url="_companyLogoDisplayTable.jsp" />

                            <tr>
                                <td><fmt:message key="printTestPage" /></td>
                                <td class="pvalue">
                                    <v:link url="${view.baseLink}/printTestRecord?name=salesOffer">
                                        <fmt:message key="salesOffer" />
                                    </v:link>
                                </td>
                            </tr>

                            <c:forEach var="prop" items="${view.selectedProperties}">
                                <c:if test="${prop.displayable and !prop.setupOnly}">
                                    <c:choose>
                                        <c:when test="${prop.unchangeable}">
                                            <tr>
                                                <td><fmt:message key="${prop.name}" /></td>
                                                <td class="pvalue"><o:out value="${prop.value}" /></td>
                                            </tr>
                                        </c:when>
                                        <c:when test="${prop.booleanType}">
                                            <tr>
                                                <td><fmt:message key="${prop.name}" /></td>
                                                <td class="pvalue">
                                                    <v:link url="${view.baseLink}/switchBooleanProperty?name=${prop.name}">
                                                        <o:out value="${prop.value}" />
                                                    </v:link>
                                                </td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <tr>
                                                <td>
                                                    <v:link url="${view.baseLink}/selectProperty?name=${prop.name}">
                                                        <fmt:message key="${prop.name}" />
                                                    </v:link>
                                                </td>
                                                <td class="pvalue"><o:out value="${prop.value}" /></td>
                                            </tr>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                        </c:otherwise>

                    </c:choose>

                </table>
            </div>

        </div>
    </div>
</div>

<o:permission role="runtime_config,organisation_admin" info="permissionChangeDocumentDebugOutput">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="miscellaneousSettings" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="table-responsive">
                    <table class="table">

                        <c:forEach var="prop" items="${view.selectedProperties}">
                            <c:if test="${!prop.displayable and !prop.setupOnly}">
                                <c:choose>
                                    <c:when test="${prop.unchangeable}">
                                        <tr>
                                            <td><fmt:message key="${prop.name}" /></td>
                                            <td class="pvalue"><o:out value="${prop.value}" /></td>
                                        </tr>
                                    </c:when>
                                    <c:when test="${prop.booleanType}">
                                        <tr>
                                            <td><fmt:message key="${prop.name}" /></td>
                                            <td class="pvalue">
                                                <v:link url="${view.baseLink}/switchBooleanProperty?name=${prop.name}">
                                                    <o:out value="${prop.value}" />
                                                </v:link>
                                            </td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                            <td>
                                                <v:link url="${view.baseLink}/selectProperty?name=${prop.name}">
                                                    <fmt:message key="${prop.name}" />
                                                </v:link>
                                            </td>
                                            <td class="pvalue"><o:out value="${prop.value}" /></td>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </c:forEach>

                    </table>
                </div>

            </div>
        </div>
    </div>
</o:permission>
