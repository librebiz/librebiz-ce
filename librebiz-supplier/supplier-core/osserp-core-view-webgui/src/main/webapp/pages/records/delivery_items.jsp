<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:set var="productUrl" value="${requestScope.productUrl}"/>
<c:set var="productAddEnabled" value="${requestScope.productAddEnabled}"/>
<div class="recordItems">
<table class="table table-striped">
    <tr>
        <th id="number">
            <c:choose>
                <c:when test="${record.itemsChangeable or record.itemsEditable}">
                    <c:choose>
                        <c:when test="${!view.productEditMode}">
                            <c:choose>
                                <c:when test="${!empty productUrl and !record.unchangeable}">
                                    <a href="<c:url value="${productUrl}"><c:param name="method" value="editProducts"/></c:url>" title="<fmt:message key="change"/>">
                                        <fmt:message key="product"/>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="product"/>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <a href="<c:url value="${productUrl}"><c:param name="method" value="forwardSelection"/></c:url>" title="<fmt:message key="addProducts"/>">
                                <fmt:message key="product"/>
                            </a>
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    <fmt:message key="product"/>
                </c:otherwise>
            </c:choose>
        </th>
        <th id="description"><fmt:message key="label"/></th>
        <th id="quantity" class="amount"><fmt:message key="quantity"/></th>
        <th id="quantity_unit" class="quantityUnit"><fmt:message key="quantityUnitShort"/></th>
        <c:if test="${!record.unchangeable}">
        <th id="action" class="action" colspan="2"><fmt:message key="actions"/></th>
        </c:if>
    </tr>
    <c:choose>
    <c:when test="${!view.itemEditMode}">
    <c:forEach var="item" items="${record.items}" varStatus="s">
    <c:set var="alternate" value="${((s.index % 2) > 0)}"/>
    <tr id="product${item.product.productId}"<c:if test="${!alternate}"> class="altrow"</c:if>>
        <td>
            <c:choose>
                <c:when test="${!empty productExitTarget}">
                    <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=${productExitTarget}&exitId=product${item.product.productId}"/>" title="<fmt:message key="loadProduct"/>">
                        <o:out value="${item.product.productId}" />
                    </a>
                </c:when>
                <c:otherwise>
                    <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                        <o:out value="${item.product.productId}" />
                    </o:popupSelect>
                </c:otherwise>
            </c:choose>
        </td>
        <td><o:out value="${item.productName}"/></td>
        <td class="amount"><fmt:formatNumber value="${item.quantity}" minFractionDigits="3"/></td>
        <td class="quantityUnit"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
        <c:if test="${!record.unchangeable}">
        <td class="icon center">
            <a href="javascript:onclick=confirmLink('<fmt:message key="title.confirm.delete"/>','<c:url value="${productUrl}"><c:param name="method" value="deleteItem"/><c:param name="id" value="${item.id}"/></c:url>');" title="<fmt:message key="title.position.delete"/>"><o:img name="deleteIcon" styleClass="bigicon"/></a>
        </td>
        <td class="icon center">
            <a href="<c:url value="${productUrl}"><c:param name="method" value="editItem"/><c:param name="id" value="${item.id}"/></c:url>" title="<fmt:message key="title.quantity.change"/>"><o:img name="writeIcon" styleClass="bigicon"/></a>
        </td>
        </c:if>
    </tr>
    <c:if test="${item.product.serialAvailable}">
    <c:set var="serialsDisplay" value="${item.serialsDisplay}"/>
    <c:if test="${!empty serialsDisplay}">
    <tr<c:if test="${!alternate}"> class="altrow"</c:if>>
        <td>&nbsp;</td>
        <td><o:out value="${serialsDisplay}"/></td>
        <td class="amount">&nbsp;</td>
        <td class="quantityUnit">&nbsp;</td>
        <c:if test="${!record.unchangeable}">
        <td class="icon center">
            <a href="javascript:onclick=confirmLink('<fmt:message key="confirmSerialsDelete"/>','<c:url value="${productUrl}"><c:param name="method" value="deleteSerials"/><c:param name="id" value="${item.id}"/></c:url>');" title="<fmt:message key="title.serials.delete"/>">
                <o:img name="deleteIcon" styleClass="bigicon"/>
            </a>
        </td>
        <td class="icon center"> </td>
        </c:if>
    </tr>
    </c:if>
    <c:if test="${!item.serialComplete}">
    <o:form name="recordForm" url="${productUrl}">
  <input type="hidden" name="method" value="addSerial"/>
  <input type="hidden" name="itemId" value="<o:out value="${item.id}"/>"/>
    <tr<c:if test="${!alternate}"> class="altrow"</c:if>>
        <td><fmt:message key="serialNumber"/>:</td>
        <c:choose>
        <c:when test="${item.id == view.lastSerialInputItem}">
        <td><input type="text" id="value" name="serialNumber" value="<o:out value="${view.lastSerialInput}"/>" class="serialNumber"/> <fmt:message key="oneSerialNumberPerPosition"/></td>
        </c:when>
        <c:otherwise>
        <td><input type="text" id="value" name="serialNumber" class="serialNumber"/> <fmt:message key="oneSerialNumberPerPosition"/></td>
        </c:otherwise>
        </c:choose>
        <td class="amount">&nbsp;</td>
        <td class="quantityUnit">&nbsp;</td>
        <td class="icon center">
            <input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="addSerial" onclick="setMethod('addSerial');" style="margin-left: 3px; margin-right: 10px;" class="bigicon" title="<fmt:message key="title.data.save"/>"/>
        </td>
        <td class="icon center"> </td>
    </tr>
    </o:form>
    </c:if>
    </c:if>
    </c:forEach>
    </c:when>
    <%-- EDIT MODE --%>
    <c:otherwise>
    <o:form name="recordForm" url="${productUrl}">
    <input type="hidden" name="method" value="updateItem"/>
    <c:set var="selected" value="${view.currentItem}"/>
    <c:forEach var="item" items="${record.items}" varStatus="s">
    <tr<c:if test="${(s.index % 2) <= 0}"> class="altrow"</c:if>>
        <td>
            <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page"
                header="product" dimension="${popupPageSize}" title="displayProductData">
                <o:out value="${item.product.productId}"/>
            </o:popupSelect>
        </td>
        <td><o:out value="${item.productName}"/></td>
        <c:choose>
        <c:when test="${item.id == selected.id}">
        <td class="amount"><input type="text" name="quantity" class="quantity" value="<fmt:formatNumber value="${item.quantity}" minFractionDigits="3"/>"/></td>
        <td class="quantityUnit"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
        <td class="icon center">
            <a href="<c:url value="${productUrl}"><c:param name="method" value="exitItemEdit"/></c:url>" title="<fmt:message key="title.edit.exit"/>"><o:img name="backIcon" styleClass="bigicon"/></a>
        </td>
        <td class="icon center">
            <input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="updateItem" onclick="setMethod('updateItem');" style="margin-left: 3px; margin-right: 5px;" class="bigicon" title="<fmt:message key="title.data.save"/>"/>
        </td>
        </c:when>
        <c:otherwise>
        <td class="amount"><fmt:formatNumber value="${item.quantity}" minFractionDigits="3"/></td>
        <td class="quantityUnit"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
        <td class="icon center" colspan="2"> </td>
        </c:otherwise>
        </c:choose>
    </tr>
    </c:forEach>
    </o:form>
    </c:otherwise>
    </c:choose>
</table>
</div>
