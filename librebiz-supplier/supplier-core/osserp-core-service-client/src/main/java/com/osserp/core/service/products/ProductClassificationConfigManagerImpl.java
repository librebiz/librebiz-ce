/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 28, 2009 1:11:13 PM 
 * 
 */
package com.osserp.core.service.products;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.SearchRequest;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.dao.ProductCategories;
import com.osserp.core.dao.ProductGroups;
import com.osserp.core.dao.ProductQueries;
import com.osserp.core.dao.ProductSelectionConfigs;
import com.osserp.core.dao.ProductTypes;
import com.osserp.core.model.products.ProductSelectionConfigItemVO;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductClassificationConfigManager;
import com.osserp.core.products.ProductClassificationEntity;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductType;
import com.osserp.core.products.ProductTypeConfig;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductClassificationConfigManagerImpl extends AbstractService implements ProductClassificationConfigManager {
    private static Logger log = LoggerFactory.getLogger(ProductClassificationConfigManagerImpl.class.getName());
    private ProductCategories categories = null;
    private ProductGroups groups = null;
    private ProductTypes types = null;
    private ProductQueries productQueries = null;
    private ProductSelectionConfigs selectionConfigs = null;

    public ProductClassificationConfigManagerImpl(
            ProductQueries productQueries,
            ProductCategories categories,
            ProductGroups groups,
            ProductTypes types,
            ProductSelectionConfigs selectionConfigs) {
        super();
        this.productQueries = productQueries;
        this.categories = categories;
        this.groups = groups;
        this.types = types;
        this.selectionConfigs = selectionConfigs;
    }

    public List<ProductClassificationConfig> getClassificationConfigsDisplay() {
        return productQueries.findClassificationConfigs();
    }

    public ProductCategoryConfig getCategory(Long id) {
        return categories.load(id);
    }

    public List<ProductCategoryConfig> getCategories() {
        return categories.getCategories();
    }

    public List<ProductClassificationEntity> getCategoriesDisplay() {
        return (List<ProductClassificationEntity>) categories.getList();
    }

    public ProductGroupConfig getGroup(Long id) {
        return groups.load(id);
    }

    public List<ProductGroupConfig> getGroups() {
        return groups.getGroups();
    }

    public List<ProductClassificationEntity> getGroupsDisplay() {
        return (List<ProductClassificationEntity>) groups.getList();
    }

    public ProductTypeConfig getType(Long id) {
        return types.load(id);
    }

    public List<ProductTypeConfig> getTypes() {
        return types.getTypes();
    }

    public void removeType(ProductTypeConfig type) throws ClientException {
        ProductSelectionConfigItemVO selectionConfig = new ProductSelectionConfigItemVO(type, null, null);
        List<Product> products = productQueries.find(selectionConfig, true, SearchRequest.IGNORE_FETCHSIZE);
        if (!products.isEmpty()) {
            throw new ClientException(ErrorCode.REFERENCED_PRODUCT_EXISTS, products);
        }
        if (selectionConfigs != null) {
            selectionConfigs.deleteItems(type.getId(), null, null);
        }
        types.delete(type.getId());
        if (log.isDebugEnabled()) {
            log.debug("removeType() done [type=" + type.getId() + "]");
        }
    }

    public List<ProductClassificationEntity> getTypesDisplay() {
        return (List<ProductClassificationEntity>) types.getList();
    }

    public void addCategory(ProductGroupConfig group, ProductClassificationEntity category) {
        ProductCategoryConfig selected = getCategory(category.getId());
        if (selected != null) {
            group.addCategory(selected);
            groups.save(group);
            if (log.isDebugEnabled()) {
                log.debug("addCategory() done [group=" + group.getId()
                        + ", category=" + selected.getId() + "]");
            }
        }
    }

    public void removeCategory(ProductGroupConfig group, ProductCategoryConfig category) throws ClientException {
        ProductSelectionConfigItemVO selectionConfig = new ProductSelectionConfigItemVO(null, null, category);
        List<Product> products = productQueries.find(selectionConfig, true, SearchRequest.IGNORE_FETCHSIZE);
        if (!products.isEmpty()) {
            throw new ClientException(ErrorCode.REFERENCED_PRODUCT_EXISTS, products);
        }
        if (selectionConfigs != null) {
            selectionConfigs.deleteItems(null, group.getId(), category.getId());
        }
        group.removeCategory(category);
        groups.save(group);
        if (log.isDebugEnabled()) {
            log.debug("removeCategory() done [group=" + group.getId()
                    + ", category=" + category.getId() + "]");
        }
    }

    public void moveCategory(ProductGroupConfig group, ProductCategoryConfig category, boolean down) {
        if (category != null) {
            if (down) {
                group.moveNext(category);
            } else {
                group.movePrevious(category);
            }
            groups.save(group);
            if (log.isDebugEnabled()) {
                log.debug("moveCategory() done [group=" + group.getId()
                        + ", category=" + category.getId()
                        + ", down=" + down
                        + ", entities=" + CollectionUtil.createEntityIds(group.getCategories())
                        + "]");
            }
        }
    }

    public void addGroup(ProductTypeConfig type, ProductClassificationEntity group) {
        ProductGroupConfig selected = getGroup(group.getId());
        if (selected != null) {
            type.addGroup(selected);
            types.save(type);
            if (log.isDebugEnabled()) {
                log.debug("addGroup() done [type=" + type.getId() + ", group=" + selected.getId() + "]");
            }
        }
    }

    public void removeGroup(ProductTypeConfig type, ProductGroupConfig group) throws ClientException {
        ProductSelectionConfigItemVO selectionConfig = new ProductSelectionConfigItemVO(null, group, null);
        List<Product> products = productQueries.find(selectionConfig, true, SearchRequest.IGNORE_FETCHSIZE);
        if (!products.isEmpty()) {
            for (Iterator<Product> iter = products.iterator(); iter.hasNext();) {
                boolean typeReferenced = false;
                Product next = iter.next();
                for (int i = 0, j = next.getTypes().size(); i < j; i++) {
                    ProductType nextType = next.getTypes().get(i);
                    if (nextType.getId().equals(type.getId())) {
                        typeReferenced = true;
                    }
                }
                if (!typeReferenced) {
                    iter.remove();
                }
            }
            if (!products.isEmpty()) {
                throw new ClientException(ErrorCode.REFERENCED_PRODUCT_EXISTS, products);
            }
        }
        if (selectionConfigs != null) {
            selectionConfigs.deleteItems(null, group.getId(), null);
        }
        type.removeGroup(group);
        types.save(type);
        if (log.isDebugEnabled()) {
            log.debug("removeGroup() done [type=" + type.getId() + ", group=" + group.getId() + "]");
        }
    }

    public void moveGroup(ProductTypeConfig type, ProductGroupConfig group, boolean down) {
        if (group != null) {
            if (down) {
                type.moveNext(group);
            } else {
                type.movePrevious(group);
            }
            types.save(type);
            if (log.isDebugEnabled()) {
                log.debug("moveGroup() done [type=" + type.getId()
                        + ", group=" + group.getId()
                        + ", down=" + down
                        + ", entities=" + CollectionUtil.createEntityIds(type.getGroups())
                        + "]");
            }
        }
    }

    public void assignNumberRange(ProductClassificationEntity obj, Long numberRangeId) {
        if (obj != null) {
            obj.setNumberRangeId(numberRangeId);
            if (obj instanceof ProductCategoryConfig) {
                categories.save(obj);
            } else if (obj instanceof ProductGroupConfig) {
                groups.save(obj);
            } else {
                log.warn("assignNumberRange() invoked with unkown classification object [class="
                        + obj.getClass().getName() + "]");
            }
        } else {
            log.warn("assignNumberRange() invoked with null as classification!");
        }
    }
}
