/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
interface DirectoryMailboxManager extends DirectoryService {

    /**
     * Creates a set of default values for mailbox users
     * @param client
     * @return mailbox user values without uid, not persistent
     */
    Map createMailboxUserAttributes(String client)

    /**
     * Creates a mailbox user with optional fetchmail account
     * @param userValues
     * @param mailboxValues (optional)
     * @param client context (optional)
     * @return new created mailbox user
     * @throws DirectoryException if validation failed
     */
    DirectoryUser createMailbox(Map userValues, Map mailboxValues, String client) throws DirectoryException

    /**
     * Finds the mailbox of an user if exists
     * @param user
     * @return fetchmailAcccount or null if not exists
     */
    FetchmailAccount findMailbox(DirectoryUser user)

    /**
     * Finds all mailbox useraccounts
     * @param client optional client name
     * @return mailboxes or empty list
     */
    List<DirectoryUser> findMailboxUsers(String client)

    /**
     * Updates fetchmail account
     * @param account
     * @param values
     * @return updated fetchmail account
     */
    FetchmailAccount update(FetchmailAccount account, Map values)

    /**
     * Enables or disables a mailbox
     * @param account 
     * @param enable
     * @return account fetchmail account
     */
    FetchmailAccount updateMailboxActivation(FetchmailAccount account, Boolean enable)
}
