/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 14.10.2004 
 * 
 */
package com.osserp.core.model;

import java.util.Date;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.Address;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AddressImpl extends AbstractEntity implements Address {

    private Long addressType;
    private String header;
    private String name;
    private String note;
    private String street;
    private String streetAddon;
    private String zipcode;
    private String city;
    private Long country = Constants.GERMANY;
    private Long federalStateId;
    private String federalStateName;
    private Long districtId;
    private boolean primary;

    public AddressImpl() {
        super();
    }

    public AddressImpl(Long id, Date created, Long createdBy) {
        super(id, (Long) null, created, createdBy);
    }

    public AddressImpl(
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalStateId,
            String federalStateName,
            Long districtId) {

        this.name = name != null ? name.trim() : null;
        this.street = street != null ? street.trim() : null;
        this.streetAddon = streetAddon != null ? streetAddon.trim() : null;
        this.zipcode = zipcode != null ? zipcode.trim() : null;
        this.city = city != null ? city.trim() : null;
        this.country = (country != null ? country : Constants.GERMANY);
        this.federalStateId = federalStateId;
        this.federalStateName = federalStateName;
        this.districtId = districtId;
        this.primary = true;
    }

    public AddressImpl(
            Long id,
            Long reference,
            Long addressType,
            String name,
            String note,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalStateId,
            String federalStateName,
            Long districtId,
            boolean primarySet,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy) {

        super(id, reference, created, createdBy, changed, changedBy, false);
        this.addressType = addressType;
        this.name = name != null ? name.trim() : null;
        this.street = street != null ? street.trim() : null;
        this.streetAddon = streetAddon != null ? streetAddon.trim() : null;
        this.zipcode = zipcode != null ? zipcode.trim() : null;
        this.city = city != null ? city.trim() : null;
        this.country = (country != null ? country : Constants.GERMANY);
        this.note = note;
        this.federalStateId = federalStateId;
        this.federalStateName = federalStateName;
        this.districtId = districtId;
        this.primary = primarySet;
    }

    public AddressImpl(
            String street,
            String streetAddon,
            String zipcode,
            String city) {

        this.street = street != null ? street.trim() : null;
        this.streetAddon = streetAddon != null ? streetAddon.trim() : null;
        this.zipcode = zipcode != null ? zipcode.trim() : null;
        this.city = city != null ? city.trim() : null;
        this.primary = true;
    }

    public AddressImpl(
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city) {

        this.name = name != null ? name.trim() : null;
        this.street = street != null ? street.trim() : null;
        this.streetAddon = streetAddon != null ? streetAddon.trim() : null;
        this.zipcode = zipcode != null ? zipcode.trim() : null;
        this.city = city != null ? city.trim() : null;
        this.primary = true;
    }

    public AddressImpl(
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country) {

        this.name = name != null ? name.trim() : null;
        this.street = street != null ? street.trim() : null;
        this.streetAddon = streetAddon != null ? streetAddon.trim() : null;
        this.zipcode = zipcode != null ? zipcode.trim() : null;
        this.city = city != null ? city.trim() : null;
        this.country = (country != null ? country : Constants.GERMANY);
        this.primary = true;
    }

    public AddressImpl(
            Long id,
            Long type,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            String city,
            Long country,
            Long districtId,
            Long federalStateId,
            String federalStateName,
            String name,
            String note,
            String street,
            String streetAddon,
            String zipcode,
            boolean primary) {

        super(id, (Long) null, created, createdBy, changed, changedBy, false);
        this.addressType = type;
        this.country = (country != null ? country : Constants.GERMANY);
        this.districtId = districtId;
        this.federalStateId = federalStateId;
        this.federalStateName = federalStateName;
        this.name = name != null ? name.trim() : null;
        this.street = street != null ? street.trim() : null;
        this.streetAddon = streetAddon != null ? streetAddon.trim() : null;
        this.zipcode = zipcode != null ? zipcode.trim() : null;
        this.city = city != null ? city.trim() : null;
        this.note = note;
        this.primary = primary;
    }

    public AddressImpl(Address otherValue) {
        super(otherValue);
        this.addressType = otherValue.getAddressType();
        this.name = otherValue.getName();
        this.note = otherValue.getNote();
        this.street = otherValue.getStreet();
        this.streetAddon = otherValue.getStreetAddon();
        this.zipcode = otherValue.getZipcode();
        this.city = otherValue.getCity();
        this.country = otherValue.getCountry();
        this.federalStateId = otherValue.getFederalStateId();
        this.federalStateName = otherValue.getFederalStateName();
        this.districtId = otherValue.getDistrictId();
        this.primary = otherValue.isPrimary();
    }

    public AddressImpl(Map<String, Object> map) {
        super(map);
        this.addressType = fetchLong(map, "addressType");
        this.name = fetchString(map, "name");
        this.note = fetchString(map, "note");
        this.street = fetchString(map, "street");
        this.streetAddon = fetchString(map, "streetAddon");
        this.zipcode = fetchString(map, "zipcode");
        this.city = fetchString(map, "city");
        Long countryId = fetchLong(map, "country");
        this.country = (countryId != null ? countryId : Constants.GERMANY);
        this.federalStateId = fetchLong(map, "federalStateId");
        this.federalStateName = fetchString(map, "federalStateName");
        this.districtId = fetchLong(map, "districtId");
        this.primary = fetchBoolean(map, "primary");
    }

    public Long getAddressType() {
        return addressType;
    }

    public void setAddressType(Long addressType) {
        this.addressType = addressType;
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();

        putIfExists(map, "addressType", city);
        putIfExists(map, "header", city);
        putIfExists(map, "city", city);
        putIfExists(map, "country", country);
        putIfExists(map, "districtId", districtId);
        putIfExists(map, "federalStateId", federalStateId);
        putIfExists(map, "federalStateName", federalStateName);
        putIfExists(map, "name", name);
        putIfExists(map, "note", note);
        putIfExists(map, "street", street);
        putIfExists(map, "streetAddon", streetAddon);
        putIfExists(map, "zipcode", zipcode);
        putIfExists(map, "primary", primary);
        return map;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetAddon() {
        return streetAddon;
    }

    public void setStreetAddon(String streetAddon) {
        this.streetAddon = streetAddon;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getCountry() {
        return country;
    }

    public void setCountry(Long country) {
        this.country = country;
    }

    public Long getFederalStateId() {
        return federalStateId;
    }

    public void setFederalStateId(Long federalState) {
        this.federalStateId = federalState;
    }

    public String getFederalStateName() {
        return federalStateName;
    }

    public void setFederalStateName(String federalStateName) {
        this.federalStateName = federalStateName;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public String getPostalAddressString() {
        StringBuilder paddress = new StringBuilder();
        if (street != null) {
            paddress.append(street);
            if (zipcode != null || city != null) {
                paddress.append('$');
            }
        }
        if (zipcode != null) {
            paddress.append(zipcode);
            if (city != null) {
                paddress.append(" ");
            }
        }
        if (city != null) {
            paddress.append(city);
        }
        return paddress.toString();
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("addressType", addressType));
        root.addContent(JDOMUtil.createElement("header", header));
        root.addContent(JDOMUtil.createElement("name", name));
        root.addContent(JDOMUtil.createElement("note", note));
        root.addContent(JDOMUtil.createElement("street", street));
        root.addContent(JDOMUtil.createElement("zipcode", zipcode));
        root.addContent(JDOMUtil.createElement("city", city));
        root.addContent(JDOMUtil.createElement("country", country));
        root.addContent(JDOMUtil.createElement("federalStateId", federalStateId));
        root.addContent(JDOMUtil.createElement("federalStateName", federalStateName));
        root.addContent(JDOMUtil.createElement("districtId", districtId));
        root.addContent(JDOMUtil.createElement("primary", primary));
        if (isSet(zipcode) && isSet(city)) {
            String cityName = zipcode + " " + city;
            root.addContent(JDOMUtil.createElement("cityName", cityName));
        }
        return root;
    }
}
