/**
 *
 * Copyright (C) 2019 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 2, 2019
 * 
 */
package com.osserp.core.service.directory;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.UserAuthenticator;

import com.osserp.core.contacts.Salutation;
import com.osserp.core.dao.SystemSetup;
import com.osserp.core.service.impl.AbstractSetupService;
import com.osserp.core.system.InitialSetupService;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.users.AccountCreatorService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class InitialDirectorySetupServiceImpl extends AbstractSetupService implements InitialSetupService {

    private AccountCreatorService accountCreatorService;

    public InitialDirectorySetupServiceImpl(
            SystemSetup systemSetup,
            UserAuthenticator userAuthenticator,
            AccountCreatorService accountCreatorService) {
        super(systemSetup, userAuthenticator);
        this.accountCreatorService = accountCreatorService;
    }

    public DomainUser initCompany(
            DomainUser admin,
            SystemCompany company,
            String companyName,
            Salutation salutation,
            Option title,
            String lastName,
            String firstName,
            String street,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String pcountry,
            String pprefix,
            String pnumber,
            String fcountry,
            String fprefix,
            String fnumber,
            String email,
            String loginName,
            String password,
            String confirmPassword,
            String currentPassword)
            throws ClientException, PermissionException {

        DomainUser result = createUser(
                admin,
                company,
                companyName,
                salutation,
                title,
                lastName,
                firstName,
                street,
                zipcode,
                city,
                country,
                federalState,
                pcountry,
                pprefix,
                pnumber,
                fcountry,
                fprefix,
                fnumber,
                email,
                loginName,
                password,
                confirmPassword,
                currentPassword);
        accountCreatorService.create(
                admin.getEmployee(),
                result.getEmployee(),
                loginName,
                password);
        getSystemSetup().closeSetup();
        return result;
    }
}
