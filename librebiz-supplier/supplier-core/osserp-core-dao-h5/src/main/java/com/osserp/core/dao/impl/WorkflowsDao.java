/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 13, 2009 8:14:38 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.Workflow;
import com.osserp.core.dao.Workflows;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.WorkflowImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class WorkflowsDao extends AbstractRepository implements Workflows {
    private static Logger log = LoggerFactory.getLogger(WorkflowsDao.class.getName());
    private String salesTable = null;
    private String workflowsTable = null;

    protected WorkflowsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        this.salesTable = getTable(TableKeys.PROJECTS);
        this.workflowsTable = getTable(TableKeys.WORKFLOWS);
    }

    public List<Workflow> findAll() {
        StringBuilder hql = new StringBuilder(128);
        hql.append("from ").append(WorkflowImpl.class.getName()).append(" o order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public Long[] getSequenceValues(Workflow workflow) {
        Long[] result = new Long[2];
        StringBuilder sql = new StringBuilder(64);
        sql.append("SELECT min(id) FROM ").append(salesTable).append(" WHERE workflow_id = ?");
        List<Long> ids = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                new Object[] { workflow.getId() },
                new int[] { Types.BIGINT },
                getLongRowMapper());
        if (!ids.isEmpty()) {
            result[0] = ids.get(0);
        }
        sql = new StringBuilder(64);
        sql
                .append("SELECT id FROM ")
                .append(salesTable)
                .append(" WHERE workflow_id = ? AND created = (select max(created) from ")
                .append(salesTable)
                .append(" WHERE workflow_id = ?) order by id desc");
        ids = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                new Object[] { workflow.getId(), workflow.getId() },
                new int[] { Types.BIGINT, Types.BIGINT },
                getLongRowMapper());
        if (!ids.isEmpty()) {
            result[1] = ids.get(0);
        }
        if (log.isDebugEnabled()) {
            log.debug("getSequenceValues() done [min=" + result[0] + ", last=" + result[1] + "]");
        }
        return result;
    }

    public String createSequence(Long id, Long start) {
        // create_project_sequence(targetid bigint, startvalue bigint)",
        // TODO change name to create_workflow_sequence 
        List<String> result = (List<String>) jdbcTemplate.query(
                "select create_project_sequence(?, ?)",
                new Object[] { id, start },
                new int[] { Types.BIGINT, Types.BIGINT },
                getStringRowMapper());
        return result.isEmpty() ? null : result.get(0);
    }

    public void enableIgnoreSequenceFlags() {
        List<Workflow> all = findAll();
        for (int i = 0, j = all.size(); i < j; i++) {
            Workflow next = all.get(i);
            enableIgnoreSequenceFlag(next);
        }
    }
    
    public void enableIgnoreSequenceFlag(Workflow workflow) {
        workflow.setIgnoreSequence(true);
        save(workflow);
    }
    
    public void disableIgnoreSequenceFlags() {
        List<Workflow> all = findAll();
        for (int i = 0, j = all.size(); i < j; i++) {
            Workflow next = all.get(i);
            disableIgnoreSequenceFlag(next);
        }
    }
    
    public void disableIgnoreSequenceFlag(Workflow workflow) {
        workflow.setIgnoreSequence(false);
        save(workflow);
    }

    public boolean exists(Long id) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(workflowsTable).append(" WHERE id = ?");
        int cnt = jdbcTemplate.queryForObject(
                sql.toString(),
                new Object[] { id },
                new int[] { Types.BIGINT }, 
                Integer.class);
        return (cnt > 0);
    }

    public void save(Workflow workflow) {
        getCurrentSession().saveOrUpdate(WorkflowImpl.class.getName(), workflow);
    }

}
