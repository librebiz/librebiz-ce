/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 6, 2004 
 * 
 */
package com.osserp.common;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface User extends Cloneable, PropertyAware {

    /**
     * Returns the id of the related contact
     * @return contactId
     */
    Long getContactId();

    /**
     * Provides an optional company id
     * @return companyId
     */
    Long getCompanyId();

    /**
     * Provides an optional customer id
     * @return customerId
     */
    Long getCustomerId();

    /**
     * Indicates that the user is a domain editor. Domain editors can edit pages, documentation, etc.
     * @return domainEditor
     */
    boolean isDomainEditor();

    /**
     * Indicates that the user is a domain user wich indicates extended attributes provided by implementing class
     * @return domainUser
     */
    boolean isDomainUser();

    /**
     * Indicates that the user is also an employee
     * @return userEmployee
     */
    boolean isUserEmployee();

    /**
     * 
     * @return loginName
     */
    String getLoginName();

    /**
     * 
     * @param loginName
     */
    void setLoginName(String loginName);

    /**
     * Provides users ldap uid
     * @return ldapUid
     */
    String getLdapUid();

    /**
     * Sets the ldap uid
     * @param ldapUid
     */
    void setLdapUid(String ldapUid);

    /**
     * Provides an optional id number if ldap uidnumber differs from users id
     * @return ldapNumber or null if ldapuidnumber is users id (default)
     */
    Long getLdapNumber();

    /**
     * Sets the optional id number for ldap
     * @param ldapNumber (optional)
     */
    void setLdapNumber(Long ldapNumber);

    /**
     * The current password
     * @return password
     */
    String getPassword();

    /**
     * Sets a new password
     * @param password
     */
    void setPassword(String password);

    /**
     * 
     * @return map with key/permission objects
     */
    Map<String, UserPermission> getPermissions();

    /**
     * Is setup admin account?
     * @return true if so
     */
    boolean isSetupAdmin();

    /**
     * Is the account admin?
     * @return true if so
     */
    boolean isAdmin();

    /**
     * Is the account activated?
     * @return true if so
     */
    boolean isActive();

    /**
     * Enables/disbales the account
     * @param activated true for enable
     */
    void setActive(boolean activated);

    /**
     * Is the account hidden in gui? (e.g. deactivated users in lists)
     * @return true if user is deactivated or marked as hidden.
     */
    boolean isHidden();

    /**
     * Provides the date and time the user logged in last before current login
     * @return lastLginTime
     */
    Date getLastLoginTime();

    /**
     * The remote ip the user comes from last time before current
     * @return lastLoginTime
     */
    String getLastLoginIp();

    /**
     * Provides the date and time since user logged into the system
     * @return loginTime
     */
    Date getLoginTime();

    /**
     * Indicates that user is member of company headquarter
     * @return true if so
     */
    boolean isFromHeadquarter();

    /**
     * Indicates that user is guest
     * @return true if so
     */
    boolean isGuest();

    /**
     * The remote ip the user currently comes from
     * @return remoteIp
     */
    String getRemoteIp();

    /**
     * Sets the remote ip the user currently comes from
     * @param remoteIp
     */
    void setRemoteIp(String remoteIp);

    /**
     * The session id
     * @return sessionId
     */
    String getSessionId();

    /**
     * Sets the session id
     * @param sessionId
     */
    void setSessionId(String sessionId);

    /**
     * Provides the locale for this user
     * @return locale
     */
    Locale getLocale();

    /**
     * Provides locale language and country (e.g. en_US, de_DE, ...)
     * @return localeDisplay
     */
    String getLocaleDisplay();

    /**
     * Adds a new permission
     * @param user who adds the permission
     * @param permission
     */
    void addPermission(Long user, String permission);

    /**
     * Removes a permission
     * @param permission
     */
    void removePermission(String permission);

    /**
     * Checks that user has required permissions
     * @param required
     * @throws PermissionException if user has not required permissions
     */
    void checkPermission(String[] required) throws PermissionException;

    /**
     * Indicates that this user has required permissions. Method always
     * returns false if null or empty string provided as permissions param.
     * This method checks for special rules associated with some permissions.
     * Use this method to check user access permssions.
     * @param permissions  
     * @return true if user has permission grant
     */
    boolean isPermissionGrant(String[] permissions);

    /**
     * Inicates if user has dedicated permission assigned. This method checks
     * for matching permission by permission name (e.g. ignores special rules 
     * associated with some special permissions (e.g. executive, _deny, etc.).
     * Use this method to check whether a dedicated permission is assigned
     * before adding a permission.  
     * @param permission
     * @return true if permission is assigned
     */
    boolean isPermissionAssigned(String permission);

    /**
     * Returns the startup target
     * @return startup
     */
    ActionTarget getStartup();

    /**
     * Sets the startup target
     * @param target
     */
    void setStartup(ActionTarget target);

    /**
     * Provides the email address of the user
     * @return email
     */
    String getEmail();

    /**
     * Provides the locale language address of the user
     * @return localeLanguage
     */
    String getLocaleLanguage();

    /**
     * Sets the locale language address of the user param localeLanguage
     */
    void setLocaleLanguage(String localeLanguage);

    /**
     * Provides the locale country address of the user
     * @return localeCountry
     */
    String getLocaleCountry();

    /**
     * Sets the locale country address of the user param localeCountry
     */
    void setLocaleCountry(String localeCountry);

    /**
     * Change users locale
     * @param localeString (e.g. de_DE, de_AT, etc.)
     */
    void changeLocale(String localeString);

    /**
     * Indicates if user should be forced to reset the password
     * @return passwordResetRequired
     */
    boolean isPasswordResetRequired();

    /**
     * Enables/disables passwordReset required flag
     * @param passwordResetRequired
     */
    void setPasswordResetRequired(boolean passwordResetRequired);

    /**
     * Clones the user object
     * @return clone
     */
    Object clone();
}
