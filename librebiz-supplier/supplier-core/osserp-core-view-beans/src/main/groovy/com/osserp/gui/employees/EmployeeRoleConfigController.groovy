/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.employees

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.PopupViewController

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/employees/employeeRoleConfig/*")
@Controller class EmployeeRoleConfigController extends PopupViewController {
    private static Logger logger = LoggerFactory.getLogger(EmployeeRoleConfigController.class.getName())
    
    @RequestMapping
    def createConfig(HttpServletRequest request) {
        logger.debug("createConfig: invoked [user=${getUserId(request)}]")
        EmployeeRoleConfigView view = getView(request)
        try {
            view.createConfig()
        } catch (Exception e) {
            saveError(request, e.message)
        }
        defaultPage
    }
    
    @RequestMapping
    def selectConfig(HttpServletRequest request) {
        logger.debug("selectConfig: invoked [user=${getUserId(request)}]")
        EmployeeRoleConfigView view = getView(request)
        view.selectConfig()
        defaultPage
    }

    @RequestMapping
    def removeConfig(HttpServletRequest request) {
        logger.debug("removeConfig: invoked [user=${getUserId(request)}]")
        EmployeeRoleConfigView view = getView(request)
        view.removeConfig()
        defaultPage
    }
    
    @RequestMapping
    def setDefaultConfig(HttpServletRequest request) {
        logger.debug("setDefaultConfig: invoked [user=${getUserId(request)}]")
        EmployeeRoleConfigView view = getView(request)
        view.setDefaultConfig()
        defaultPage
    }
    
    @RequestMapping
    def createRole(HttpServletRequest request) {
        logger.debug("createRole: invoked [user=${getUserId(request)}]")
        EmployeeRoleConfigView view = getView(request)
        view.createRole()
        defaultPage
    }
    
    @RequestMapping
    def setDefaultRole(HttpServletRequest request) {
        logger.debug("setDefaultRole: invoked [user=${getUserId(request)}]")
        EmployeeRoleConfigView view = getView(request)
        view.setDefaultRole()
        defaultPage
    }
    
    @RequestMapping
    def removeRole(HttpServletRequest request) {
        logger.debug("removeRole: invoked [user=${getUserId(request)}]")
        EmployeeRoleConfigView view = getView(request)
        view.removeRole()
        defaultPage
    }

    @Override    
    @RequestMapping
    def exit(HttpServletRequest request) {
        EmployeeRoleConfigView view = getView(request)
        try {
            view.synchronizeLdap()
        } catch (Exception ignorable) {}
        return super.exit(request)
    }
}
