<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>


<v:form name="flowControlConfigOrderForm" url="${view.baseLink}">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th><v:sortLink key="name"><fmt:message key="action" /></v:sortLink></th>
                    <th class="center" colspan="4"><v:sortLink key="orderId" style="margin-right: 5px;"><fmt:message key="position" /></v:sortLink></th>
                    <th class="number right"><v:sortLink key="status" style="margin-right: 5px;"><fmt:message key="status" /></v:sortLink></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${!empty view.selectedType}">
                        <c:choose>
                            <c:when test="${empty view.list}">
                                <tr>
                                    <td colspan="6"><fmt:message key="noConfigurationAvailable" />.</td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="action" items="${view.list}">
                                    <c:if test="${(view.eolDisplay && action.endOfLife) or (!view.eolDisplay && !action.endOfLife)}">
                                        <tr>
                                            <td>
                                                <v:link url="${view.baseLink}/select?id=${action.id}">
                                                    <span title="id: ${action.id}"><o:out value="${action.name}" /></span>
                                                </v:link>
                                                <input type="hidden" name="id" value="<o:out value="${action.id}"/>" />
                                            </td>
                                            <c:choose>
                                                <c:when test="${view.orderEditMode}">
                                                    <td class="icon center" colspan="4"><v:text name="orderId" value="${action.orderId}" styleClass="shortNumber" /></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="icon right">
                                                        <v:link url="${view.baseLink}/enableOrderEditMode" title="changeSortOrder" style="margin-right: 3px;">
                                                            <o:out value="${action.orderId}" />
                                                        </v:link>
                                                    </td>
                                                    <td class="icon center">
                                                        <v:link url="${view.baseLink}/moveUp?id=${action.id}" title="moveUp">
                                                            <o:img name="upIcon" />
                                                        </v:link>
                                                    </td>
                                                    <td class="icon center">
                                                        <v:link url="${view.baseLink}/moveDown?id=${action.id}" title="moveDown">
                                                            <o:img name="downIcon" />
                                                        </v:link>
                                                    </td>
                                                    <td class="icon center">
                                                        <c:choose>
                                                            <c:when test="${action.endOfLife}">
                                                                <v:link url="${view.baseLink}/changeEolFlag?id=${action.id}" title="actionActivate" style="margin-right: 5px;">
                                                                    <o:img name="enabledIcon" />
                                                                </v:link>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <v:link url="${view.baseLink}/changeEolFlag?id=${action.id}" title="actionDeactivate" style="margin-right: 5px;">
                                                                    <o:img name="disabledIcon" />
                                                                </v:link>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                </c:otherwise>
                                            </c:choose>
                                            <td style="text-align: right;"><span style="margin-right: 5px;"><o:out value="${action.status}" /></span></td>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td colspan="6"><fmt:message key="noBusinessTypeSelected" /> - <fmt:message key="selectionViaSearchIcon" /></td>
                        </tr>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
    <c:if test="${view.orderEditMode}">
        <div style="text-align: right; margin-top: 10px; margin-right: 12px;">
            <input class="cancel" type="button" value="<fmt:message key="exit"/>" onclick="gotoUrl('<v:url value="${view.baseLink}"/>/disableOrderEditMode');" />
            <o:submit />
        </div>
    </c:if>
</v:form>
