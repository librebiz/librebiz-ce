/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31 Mar 2007 14:48:01 
 * 
 */
package com.osserp.common.dms;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DmsManager {

    /**
     * Provides the configuration object of the document management system.
     * @return dms config or null if not found
     */
    DmsConfig getDmsConfig();

    /**
     * Indicates if dms manager is managing documents with dedicated types.
     * @return true if dedicated documentType assigned.
     */
    boolean isDedicatedTypeManager();

    /**
     * Provides the default category as id
     * @param documentType
     * @return default category id or null if not supported
     */
    Long getDefaultCategoryId(Long documentType);

    /**
     * Provides a document type by id
     * @param id
     * @return documentType or null if not exists or not supported by manager instance
     */
    DocumentType getDocumentType(Long id);

    /**
     * Provides all document types supported by manager
     * @return documentTypes supported types
     */
    List<DocumentType> getDocumentTypes();

    /**
     * Tries to find the document with given primary key id
     * @param id
     * @return document or null if no such document exists
     */
    DmsDocument findDocument(Long id);

    /**
     * Gets the document with given primary key id
     * @param id
     * @return document
     * @throws ClientException if no such document exists
     */
    DmsDocument getDocument(Long id) throws ClientException;

    /**
     * Provides all documents related to given messageId
     * @param messageId
     * @return documents by messageId
     */
    List<DmsDocument> findByMessageId(String messageId);

    /**
     * Provides all documents related to given reference
     * @param documentType
     * @return documents by reference
     */
    List<DmsDocument> findByType(DocumentType documentType);

    /**
     * Provides the count of all documents related to given reference
     * @param documentType
     * @return document count by reference
     */
    int countByType(DocumentType documentType);

    /**
     * Finds the document declared as reference
     * @param reference
     * @return document decared as reference or null if not exists
     */
    DmsDocument findReferenced(DmsReference reference);

    /**
     * Finds all documents related to given reference
     * @param reference
     * @return documents by reference
     */
    List<DmsDocument> findByReference(DmsReference reference);

    /**
     * Provides document count related to given reference
     * @param reference
     * @return count
     */
    int getCountByReference(DmsReference reference);

    /**
     * Assigns and moves an imported document to provided reference
     * @param user the user performing the assignment
     * @param reference the reference to an entity to assign document to
     * @param document the imported document to assign
     * @throws ClientException if assignment failed
     */
    void assignDocument(User user, DmsReference reference, DmsDocument document) throws ClientException;

    /**
     * Creates a document for the specified reference using a stream as input
     * @param user
     * @param reference
     * @param file
     * @param note
     * @param source
     * @param categoryId
     * @param validFrom
     * @param validTil
     * @param messageId
     * @throws ClientException
     */
    DmsDocument createDocument(
            User user,
            DmsReference reference,
            FileObject file,
            String note,
            Date source,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId) throws ClientException;

    /**
     * Creates a document for the specified reference using existing file
     * @param user
     * @param reference
     * @param fileName
     * @param absoluteFile
     * @param note
     * @param source
     * @param categoryId
     * @param validFrom
     * @param validTil
     * @param messageId
     * @return created document
     * @throws ClientException if source file not exist
     */
    DmsDocument createDocument(
            User user,
            DmsReference reference,
            String fileName,
            String absoluteFile,
            String note,
            Date source,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId)
        throws ClientException;

    /**
     * Deletes the given document from the backend
     * @param doc
     * @param exportFile relative path and filename to documentConfig.root, e.g. set to 'exports/exportedfile.suffix to create the exported file under
     * /var/lib/osserp/dms/domain0/exports/exportfile.name.
     * @throws ClientException if export failed
     */
    void exportDocument(DmsDocument doc, String exportFile) throws ClientException;

    /**
     * Deletes a previously exported document by relative filename
     * @param exportFile
     * @throws ClientException
     */
    void deleteExportedDocument(String exportFile) throws ClientException;

    /**
     * Updates an existing document using a file object as input
     * @param user
     * @param document
     * @param file
     * @param note
     * @param category
     * @throws ClientException
     */
    void updateDocument(User user, DmsDocument document, FileObject file, String note, Long category) throws ClientException;

    /**
     * Updates the metadata of an existing document
     * @param user
     * @param document
     * @param note
     * @param validFrom
     * @param validTil
     * @throws ClientException
     */
    void updateMetadata(User user, DmsDocument document, String note, Date validFrom, Date validTil) throws ClientException;

    /**
     * Updates an existing document category
     * @param user
     * @param id
     * @param category or null to reset
     */
    void updateDocument(User user, Long id, Long category);

    /**
     * Changes the persistent format of all existing binaries referenced to provided document type. The format is read by invocation of
     * documentType.persistentFormat
     * @param documentType
     * @return count of changed documents
     */
    int changeFormat(DocumentType documentType);

    /**
     * Provides document data as byte array
     * @param document
     * @return documentData
     */
    DocumentData getDocumentData(DmsDocument document);

    /**
     * Provides thumbnail data as byte array
     * @param document
     * @return documentData
     */
    DocumentData getThumbnailData(DmsDocument document);

    /**
     * Deletes the given document from the backend
     * @param doc
     */
    void deleteDocument(DmsDocument doc);

    /**
     * Deletes all documents referenced by message id
     * @param messageId
     */
    void deleteDocuments(String messageId);

    /**
     * Sets the picture with given id as reference picture
     * @param id of the picture
     */
    void setAsReference(Long id);

    /**
     * Gets the reference document related to given reference
     * @param reference under wich we lookup for reference picture
     * @throws ClientException if no reference picture exists
     */
    DmsDocument getReferenceDocument(DmsReference reference) throws ClientException;

}
