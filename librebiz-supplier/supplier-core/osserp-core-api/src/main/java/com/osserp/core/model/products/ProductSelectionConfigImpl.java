/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 6, 2009 5:00:48 PM 
 * 
 */
package com.osserp.core.model.products;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductSelectionContext;
import com.osserp.core.products.ProductType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSelectionConfigImpl extends AbstractProductSelectionConfig {

    protected ProductSelectionConfigImpl() {
        super();
    }

    public ProductSelectionConfigImpl(
            Employee user,
            ProductSelectionContext context,
            String name,
            String description) {
        super(user, context, name, description);
    }

    @Override
    public void addItem(
            Employee user,
            ProductType type,
            ProductGroup group,
            ProductCategory category) {
        ProductSelectionConfigItemImpl obj = new ProductSelectionConfigItemImpl(
                user.getId(),
                this,
                type,
                group,
                category);
        getItems().add(obj);
    }
}
