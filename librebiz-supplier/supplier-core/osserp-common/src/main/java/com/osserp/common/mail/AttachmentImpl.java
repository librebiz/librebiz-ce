/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 21, 2005 
 * 
 */
package com.osserp.common.mail;

import com.osserp.common.beans.AbstractEntity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AttachmentImpl extends AbstractEntity implements Attachment {

    private String contentID = null;
    private String contentType = null;
    private String fileName = null;
    private String mimeType = null;
    private String file = null;
    private long fileSize = 0;
    private byte[] bytes = null;

    /**
     * Default constructor
     */
    protected AttachmentImpl() {
        super();
    }

    /**
     * Constructor for a persistent instance
     * @param id
     */
    public AttachmentImpl(Long id) {
        super(id);
    }

    /**
     * Default constructor for new attachments immediately to send
     * @param fileName
     * @param file
     * @param mimeType
     */
    public AttachmentImpl(String fileName, String file, String mimeType) {
        super();
        this.fileName = fileName;
        this.file = file;
        this.mimeType = mimeType;
    }

    /**
     * Default constructor for received attachments
     * @param contentID
     * @param contentType
     * @param fileName
     * @param file
     * @param fileSize
     * @param mimeType
     */
    public AttachmentImpl(String contentID, String contentType, String fileName, String file, long fileSize, String mimeType) {
        super();
        this.contentID = contentID;
        this.contentType = contentType;
        this.fileName = fileName;
        this.file = file;
        this.fileSize = fileSize;
        this.mimeType = mimeType;
    }

    /**
     * Default constructor for new attachments immediately to send
     * @param fileName
     * @param bytes
     * @param mimeType
     */
    public AttachmentImpl(String fileName, byte[] bytes, String mimeType) {
        super();
        this.fileName = fileName;
        this.bytes = bytes;
        this.mimeType = mimeType;
    }

    /**
     * Default constructor for received attachments
     * @param contentID
     * @param contentType
     * @param fileName
     * @param bytes
     * @param mimeType
     */
    public AttachmentImpl(String contentID, String contentType, String fileName, byte[] bytes, String mimeType) {
        super();
        this.contentID = contentID;
        this.contentType = contentType;
        this.fileName = fileName;
        this.bytes = bytes;
        this.mimeType = mimeType;
    }

    /**
     * Constructor setting all values
     * @param id
     * @param fileName
     * @param file
     * @param mimeType
     */
    public AttachmentImpl(
            Long id,
            String fileName,
            String file,
            String mimeType) {
        super(id);
        this.fileName = fileName;
        this.file = file;
        this.mimeType = mimeType;
    }

    public AttachmentImpl(
            Long id,
            Long reference,
            String contentID,
            String contentType,
            String fileName,
            String mimeType,
            String file,
            long fileSize) {
        super(id, reference);
        this.contentID = contentID;
        this.contentType = contentType;
        this.fileName = fileName;
        this.mimeType = mimeType;
        this.file = file;
        this.fileSize = fileSize;
    }

    public String getContentID() {
        return contentID;
    }

    public void setContentID(String contentID) {
        this.contentID = contentID;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return bytes != null ? bytes.length : fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
