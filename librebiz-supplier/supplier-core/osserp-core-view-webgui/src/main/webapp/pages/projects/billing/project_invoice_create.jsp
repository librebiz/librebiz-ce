<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.businessCaseView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="project" value="${sessionScope.businessCaseView.sales}" />
<c:set var="billing" value="${sessionScope.salesBillingView}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            .input {
                width: 255px;
            }

            #percentageInput, #amountInput {
                display: none;
            }
        </style>
    </tiles:put>
    <tiles:put name="javascript" type="string">
        <script type="text/javascript">
<!--

	function jumpDirect() {
		window.location.href = document.billingForm.selectAction.value;
	}
	
	function enablePercentage() {
		document.getElementById("amountInput").style.display="none";
		document.getElementById("percentageInput").style.display="block";
	}
	
	function disablePercentage() {
		document.getElementById("percentageInput").style.display="none";
		document.getElementById("amountInput").style.display="block";
	}
	
	function validateInvoiceSettings() {
		var form = document.getElementById('billingForm');
		var confirmString = 'Bitte best\u00e4tigen:\n';
		if (form.type.value != 5) {
			return true;
		}
		var percent = getPercentage();
		if (percent < 0) {
			alert('Keine Auswahl Prozentsatz / Pauschalbetrag');
			return false;
		} 
		var amount = createDouble(form.amount.value);
		if (percent == 0) {
			if (amount == null || amount == 0) {
				alert('Eingabe Prozentwert erwartet');
				return false;
			}
			if (amount < 0 || amount > 100) {
				alert('Ung\u00fcltige Eingabe f\u00fcr Prozentwert');
				return false;
			} 
			if (isNaN(amount)) {
				alert('Ung\u00fcltige Eingabe f\u00fcr Prozentwert');
				return false;
			}
			confirmString = confirmString + amount + '% vom Auftragswert berechnen,\n';
		} else {
			if (amount == null || amount == 0) {
				alert('Eingabe Rechnungsbetrag erwartet');
				return false;
			}
			if (amount < 0) {
				alert('Ung\u00fcltige Eingabe f\u00fcr Rechnungsbetrag');
				return false;
			} 
			confirmString = confirmString + 'Pauschalbetrag ' + amount + ' berechnen,\n';
		}
		if (form.note.value == null || form.note.value == '') {
			confirmString = confirmString + 'keine Bemerkung zum Gesch\u00e4ftsfall,\n';
		} else {
			confirmString = confirmString + 'mit Bemerkung zum Gesch\u00e4ftsfall:\n'
				+ form.note.value + '.\n';
		}
		if (form.printOrderItems.checked) {
			confirmString = confirmString + 'Auftragspositionen in Rechnung drucken';
		} else {
			confirmString = confirmString + 'keine Auftragspositionen in Rechnung drucken';
		}
		return confirm(confirmString);
	}
	
	function getPercentage() {
		var form = document.forms[0];
	    if (form.percent[0].checked) {
	    	return 0;
	    } else if (form.percent[1].checked) {
	        return 1;
        } else {
    	    return -1;
        }
	}
	
//-->
        </script>
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="billTo" />
        <o:out value="${project.request.name}" limit="45" />
    </tiles:put>

    <tiles:put name="headline_right">
        <a href="<c:url value="/projectInvoiceEdit.do?method=exit"/>"><o:img name="backIcon" /></a>
        <v:link url="/index" title="backToMenu">
            <o:img name="homeIcon" />
        </v:link>
    </tiles:put>

    <tiles:put name="content" type="string">

        <div class="content-area" id="projectInvoiceCreateContent">
            <div class="row">
                <o:form id="billingForm" name="billingForm" url="/projectInvoiceEdit.do" onsubmit="return validateInvoiceSettings();">
                    <input type="hidden" name="method" value="createInvoice" /> <input type="hidden" name="type" value="<o:out value="${billing.billingType}"/>" />

                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <fmt:message key="newBill" />
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="recordType"> <fmt:message key="type" />
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">

                                        <c:choose>
                                            <c:when test="${empty sessionScope.flowControlAction}">
                                                <select name="selectAction" size="1" class="form-control" onChange="jumpDirect()">
                                                    <c:forEach var="opt" items="${billing.invoiceTypes}">
                                                        <option value="<c:url value="/projectInvoiceEdit.do?method=selectInvoiceType&id=${opt.id}"/>" <c:if test="${billing.billingType == opt.id}">selected="selected"</c:if>>
                                                            <c:choose>
                                                                <c:when test="${opt.downpayment}">
                                                                    <fmt:message key="downpayment" /> - </c:when>
                                                                <c:when test="${opt.invoice}">
                                                                    <fmt:message key="invoice" /> - </c:when>
                                                            </c:choose>
                                                            <o:out value="${opt.name}" />
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="boldtext"><oc:options name="invoiceTypes" value="${billing.billingType}" /></span>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>

                            <c:if test="${billing.billingType == 4}">
                                <div class="row next">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="shortName"> <fmt:message key="invoiceRecipient" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <c:choose>
                                                <c:when test="${empty billing.thirdParty}">
                                                    <span class="boldtext"><fmt:message key="customer" /></span>
                                                    <span style="margin-left: 15px;"><v:link url="/contacts/contactSearch/forward?exit=/projectInvoiceCreateDisplay.do&selectionTarget=/projectInvoiceCustomerSelection.do&context=customer">[<fmt:message key="thirdPartyRecord" />]</v:link></span>
                                                </c:when>
                                                <c:otherwise>
                                                    <span class="boldtext"><o:out value="${billing.thirdParty.displayName}" /></span>
                                                    <span style="margin-left: 15px;"><a href="<c:url value="/projectInvoiceEdit.do?method=deselectCustomer"/>">[<fmt:message key="customer" />]
                                                        </a></span>
                                                    </c:otherwise>
                                                </c:choose>
                                        </div>
                                    </div>
                                </div>
                            </c:if>

                            <c:if test="${billing.billingType == 5}">

                                <div class="row next">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for=""> </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="radio">
                                                <label for="percentalPartialAmount"> <input type="radio" name="percent" value="true" onclick="enablePercentage();" /> <fmt:message key="percentalPartialAmount" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for=""> </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="radio">
                                                <label for="fixedAmount"> <input type="radio" name="percent" value="false" onclick="disablePercentage();" /> <fmt:message key="overallReduction" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for=""> </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <span id="percentageInput"><fmt:message key="enterPercentageQuotationWithoutPercentSymbol" />:</span> <span id="amountInput"><fmt:message key="enterOverallInvoiceAmount" />:</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for=""> </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="amount" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="caseSelection"> <fmt:message key="caseSelection" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <select name="productId" size="1" class="form-control">
                                                <c:forEach var="product" items="${billing.businessCases}">
                                                    <option value="<o:out value="${product.productId}"/>"><o:out value="${product.productId}" />
                                                        <o:out value="${product.name}" /></option>
                                                    </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="caseNote"> <fmt:message key="caseNote" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <textarea class="form-control" name="note" cols="2"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for=""> </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label for="printOrderItems"> <input type="checkbox" name="printOrderItems" value="true" /> <fmt:message key="listOrderPositionsInRecord" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </c:if>

                            <c:if test="${billing.billingType > 0}">

                                <div class="row next">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="orderData"> <fmt:message key="orderData" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label for="printOrderItems"> <input type="checkbox" name="copyNote" value="true" /> <fmt:message key="recordCopyNoteText" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for=""> </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label for="printOrderItems"> <input type="checkbox" name="copyTerms" value="true" /> <fmt:message key="recordCopyConditionsText" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <c:if test="${billing.billingType == 4}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for=""> </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label for="copyItems"> <input type="checkbox" name="copyItems" value="true" /> <fmt:message key="copyItems" />
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>

                                <c:if test="${billing.billingType == 7}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for=""> </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label for="copyItems">
                                                        <input type="checkbox" name="copyItemsDisabled" checked="checked" value="true" disabled="disabled" />
                                                        <fmt:message key="copyItems" /><input type="hidden" name="copyItems" value="true" />
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row next">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="amount"> <fmt:message key="downpaymentInvoiceAmount" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="amount" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                            </div>
                                        </div>
                                    </div>
                                </c:if>

                                <c:if test="${billing.customNumberEnabled or billing.customDateEnabled or billing.unpaidDownpaymentsAvailable}">

                                    <div class="row next">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="furtherOptionsHeadline"> </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <fmt:message key="furtherOptions" />:
                                            </div>
                                        </div>
                                    </div>

                                    <c:if test="${billing.customNumberEnabled}">

                                        <div class="row next">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="customNumberEnabled"> <fmt:message key="recordNumber" />
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <v:text name="recordNumber" styleClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>

                                    <c:if test="${billing.customDateEnabled}">
                                        <v:date name="recordDate" styleClass="form-control" label="recordDate" picker="true" />
                                    </c:if>

                                    <c:if test="${billing.unpaidDownpaymentsAvailable}">
                                        <div class="row next">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="cancelUnpaidDownpayments"> </label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label for="copyItems">
                                                            <c:choose>
                                                                <c:when test="${billing.cancelUnpaidDownpaymentsDefault}">
                                                                    <input type="checkbox" name="cancelDownpayments" value="true" checked="checked" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="checkbox" name="cancelDownpayments" value="true" />
                                                                </c:otherwise>
                                                            </c:choose>
                                                            <fmt:message key="cancelUnpaidDownpayments" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                </c:if>

                                <div class="row next">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <o:submit name="createBill" styleClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </c:if>

                        </div>
                    </div>

                </o:form>
            </div>
        </div>

    </tiles:put>
</tiles:insert>
