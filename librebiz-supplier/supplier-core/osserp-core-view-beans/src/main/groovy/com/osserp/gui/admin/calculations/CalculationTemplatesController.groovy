/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 29, 2011 4:12:32 PM 
 * 
 */
package com.osserp.gui.admin.calculations

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.AdminViewController

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/admin/calculations/calculationTemplates/*")
@Controller class CalculationTemplatesController extends AdminViewController {
    private static Logger logger = LoggerFactory.getLogger(CalculationTemplatesController.class.getName())

    @RequestMapping
    def removeItem(HttpServletRequest request) {
        logger.debug("removeItem: invoked [user=${getUserId(request)}]")
        CalculationTemplatesView view = getView(request)
        view.removeItem()
        return defaultPage
    }

    @RequestMapping
    def toggleOptionalItem(HttpServletRequest request) {
        logger.debug("toggleOptionalItem: invoked [user=${getUserId(request)}]")
        CalculationTemplatesView view = getView(request)
        view.toggleOptionalItem()
        return defaultPage
    }

    @RequestMapping
    def delete(HttpServletRequest request) {
        logger.debug("delete: invoked [user=${getUserId(request)}]")
        CalculationTemplatesView view = getView(request)
        view.delete()
        return defaultPage
    }
}

