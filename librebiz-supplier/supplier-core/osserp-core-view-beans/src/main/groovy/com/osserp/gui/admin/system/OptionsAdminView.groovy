/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 22, 2014 
 * 
 */
package com.osserp.gui.admin.system

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.OptionsCache
import com.osserp.common.util.CollectionUtil

import com.osserp.core.Comparators

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class OptionsAdminView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(OptionsAdminView.class.getName())

    List<String> availableLists = []
    String selectedName
    boolean dedicated

    OptionsAdminView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!availableLists) {
                availableLists = getSystemPropertyArray('supportedOptions')
            }
            String name = form.getString('name')
            if (name) {
                selectOptions(name)
                dedicated = true
            }
        }
    }
    
    @Override
    void save() {
        String name = form.getString('name')
        if (name) {
            if (createMode) {
                optionsCache.add(selectedName, name)
                disableCreateMode()
            } else if (editMode) {
                optionsCache.rename(selectedName, bean.id, name)
                disableEditMode()
                bean = null
            }
            reloadOptions()
        }
    }
    
    @Override
    void select() {
        super.select()
        if (bean) {
            enableEditMode()
        } else {
            disableEditMode()
        }
    }
    
    @Override
    void disableEditMode() {
        super.disableEditMode()
        bean = null
    }
    
    void selectList() {
        selectOptions(form.getString('name'))
        createCustomNavigation()
    }
    
    void toggleEol() {
        Long id = form.getLong('id')
        if (id) {
            optionsCache.toogleEol(selectedName, id)
            reloadOptions()
        }
    }
    
    @Override
    void createCustomNavigation() {
        if (selectedName) {
            nav.defaultNavigation = nav.listNavigation = nav.beanListNavigation = [deselectLink, createLink, homeLink]
        } else {
            nav.defaultNavigation = [exitLink, homeLink]
        }
    }
    
    MenuItem getDeselectLink() {
        navigationLink("/${context.name}/selectList", 'backIcon', 'backToLast')
    }

    private void selectOptions(String name) {
        selectedName = name
        if (selectedName) {
            list = optionsCache.getList(selectedName)
            CollectionUtil.sort(list, Comparators.createOptionNameComparator(false))
        } else {
            selectedName = null
            list = []
        }
    }
    
    private void reloadOptions() {
        if (selectedName) {
            optionsCache.refresh(selectedName)
            list = optionsCache.getList(selectedName)
            CollectionUtil.sort(list, Comparators.createOptionNameComparator(false))
        }
    }
    
    private OptionsCache getOptionsCache() {
        getService(OptionsCache.class.getName())
    }
}
