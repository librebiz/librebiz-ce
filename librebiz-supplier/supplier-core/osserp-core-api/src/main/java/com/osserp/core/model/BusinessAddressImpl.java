/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 4, 2005 
 * 
 */
package com.osserp.core.model;

import org.jdom2.Element;

import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.Address;
import com.osserp.core.BusinessAddress;
import com.osserp.core.customers.Customer;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessAddressImpl extends AddressImpl implements BusinessAddress {
    private static final long serialVersionUID = 42L;

    private String person = null;
    private String phone = null;
    private String fax = null;
    private String mobile = null;
    private String email = null;

    protected BusinessAddressImpl() {
        super();
    }

    /**
     * Constructor for new addresses
     * @param name
     * @param person
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param federalState
     * @param federalStateName
     * @param district
     */
    protected BusinessAddressImpl(
            String name,
            String person,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String federalStateName,
            Long district) {

        super(name, street, streetAddon, zipcode, city, country, federalState, federalStateName, district);
        this.person = person;
    }

    /**
     * Initializes a business address by customer.
     * Id or reference settings will not be copied.
     * @param customer
     */
    public BusinessAddressImpl(Customer customer) {
        super(
            customer.getAddress().getName(),
            customer.getAddress().getStreet(),
            customer.getAddress().getStreetAddon(),
            customer.getAddress().getZipcode(),
            customer.getAddress().getCity(),
            customer.getAddress().getCountry(),
            customer.getAddress().getFederalStateId(),
            customer.getAddress().getFederalStateName(),
            customer.getAddress().getDistrictId());
    }

    /**
     * Initializes a business address by another address.
     * This constructors copies all values including ids.
     * @param address
     */
    public BusinessAddressImpl(Address address) {
        super(address);
    }

    /**
     * Initializes a business address by dedicated values.
     * @param city
     * @param country
     * @param district
     * @param email
     * @param fax
     * @param federalState
     * @param federalStateName
     * @param mobile
     * @param name
     * @param note
     * @param person
     * @param phone
     * @param street
     * @param streetAddon
     * @param zipcode
     */
    public BusinessAddressImpl(
            String city,
            Long country,
            Long district,
            String email,
            String fax,
            Long federalState,
            String federalStateName,
            String mobile,
            String name,
            String note,
            String person,
            String phone,
            String street,
            String streetAddon,
            String zipcode) {

        super(
                null, // id
                BusinessAddress.DELIVERY,
                null, // created
                null, // createdBy
                null, // changed
                null, // changedBy
                city,
                country,
                district,
                federalState,
                federalStateName,
                name,
                note,
                street,
                streetAddon,
                zipcode,
                true);
        this.email = email;
        this.fax = fax;
        this.mobile = mobile;
        this.person = person;
        this.phone = phone;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("person", person));
        root.addContent(JDOMUtil.createElement("phone", phone));
        root.addContent(JDOMUtil.createElement("fax", fax));
        root.addContent(JDOMUtil.createElement("mobile", mobile));
        root.addContent(JDOMUtil.createElement("email", email));
        return root;
    }
}
