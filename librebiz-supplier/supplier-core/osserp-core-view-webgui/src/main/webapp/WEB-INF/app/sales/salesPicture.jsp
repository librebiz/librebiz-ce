<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource />
<v:view viewName="salesPictureView" />
<c:set var="dmsDocumentView" scope="request" value="${sessionScope.salesPictureView}"/>
<c:set var="dmsDocumentUrl" scope="request" value="/sales/salesPicture"/>
<c:set var="dmsDeletePermissions" scope="request" value="delete_sales_documents,delete_sales_pictures,organisation_admin,executive_sales"/>
<c:set var="dmsReferencePermissions" scope="request" value="project_internet_config,organisation_admin,executive_sales"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="picturesTo" />
        <o:out value="${view.businessCase.primaryKey}" /> - <o:out value="${view.businessCase.name}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <c:import url="${viewdir}/dms/_commonDocumentUpload.jsp"/>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
