/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 23, 2003 
 * 
 */
package com.osserp.common.dms.dao.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.ReferencedDocuments;
import com.osserp.common.dms.model.DmsDocumentImpl;
import com.osserp.common.service.SecurityService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class FilesystemDocumentsDao extends AbstractFilesystemDocumentsDao implements ReferencedDocuments {

    private static Logger log = LoggerFactory.getLogger(FilesystemDocumentsDao.class.getName());

    /**
     * Creates a new document dao supporting filesystem persistence.
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param documentTableKey
     * @param categoryTableKey
     * @param documentEncoder
     * @param authenticationProvider
     */
    protected FilesystemDocumentsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            String documentTableKey,
            String categoryTableKey,
            DocumentEncoder documentEncoder,
            SecurityService authenticationProvider) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                null, // documentType (runtime param for filesystem documents)
                documentTableKey,
                categoryTableKey,
                documentEncoder,
                authenticationProvider);
    }

    @Override
    protected DmsDocument createObject(
            User user,
            DocumentType documentType,
            DmsReference reference,
            String fileName,
            long fileSize,
            String note,
            Date sourceDate,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId) {
        return new DmsDocumentImpl(
                reference.getReferenceId(),
                documentType,
                fileName,
                fileSize,
                user.getId(),
                note,
                categoryId,
                sourceDate,
                validFrom,
                validTil,
                messageId);
    }

    public DmsDocument create(User user, DmsReference reference, DmsDocument imported) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [reference=" + reference.getReferenceId()
                    + ", fileName=" + imported.getFileName()
                    + ", realFileName=" + imported.getRealFileName() + "]");
        }
        DocumentType docType = reference.getTypeId() != null ? getDocumentType(reference.getTypeId()) 
                : getDocumentType();
        String note = imported.getNote();
        if (docType != null) {
            if (docType.isNoteRequired() && isEmpty(note)) {
                // Imported document may not provide a note as required by target documentType.
                // Setting filename as note avoids exceptions about this.
                note = imported.getFileName();
            }
        }
        FileObject fileObject = new FileObject(imported.getFileName(), getDocumentData(imported));
        DmsDocument doc = create(
                user,
                reference,
                fileObject,
                note,
                imported.getCreated(),
                imported.getCategoryId(),
                imported.getValidFrom(),
                imported.getValidTil(),
                imported.getMessageId());
        deleteDocument(imported);
        return doc;
    }

    @Override
    protected Class<DmsDocumentImpl> getPersistentClass() {
        return DmsDocumentImpl.class;
    }

}
