<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.salesVolumeInvoiceView}"/>
<c:set var="config" value="${view.operation.config}"/>
<div class="spacer"></div>
<div class="table-responsive" id="table-responsive table-responsive-default">
	<table class="table table-striped" id="table">
		<thead>
			<tr>
				<th><fmt:message key="number"/></th>
				<th><fmt:message key="referenceNumberShort"/></th>
				<th><fmt:message key="customer"/></th>
				<th><fmt:message key="from"/></th>
				<th class="right"><fmt:message key="amount"/></th>
				<th class="right"><fmt:message key="turnoverTax1"/></th>
				<th class="right"><fmt:message key="gross"/></th>
				<th> </th>
				<th>
					<a id="tableSwitcher" href="javascript:switchTable();">+</a>
				</th>
			</tr>
		</thead>
		<tbody id="table_body">
			<c:forEach var="record" items="${view.list}" varStatus="s">
				<tr>
					<td><o:out value="${record.number}"/></td>
					<td>
						<c:choose>
							<c:when test="${config.salesReferenceBySales}">
								<a href="<c:url value="/salesVolumeInvoice.do?method=loadSales&id=${record.businessCaseId}"/>"><o:out value="${record.businessCaseId}"/></a>
							</c:when>
							<c:otherwise>
								<a href="<c:url value="/salesVolumeInvoice.do?method=loadSales&id=${record.reference}"/>"><o:out value="${record.reference}"/></a>
							</c:otherwise>
						</c:choose>
					</td>
					<td><o:out value="${record.contact.displayName}"/></td>
					<td><o:date value="${record.created}"/></td>
					<td class="right"><o:number value="${record.amounts.amount}" format="currency"/></td>
					<td class="right"><o:number value="${record.amounts.taxAmount}" format="currency"/></td>
					<td class="right"><o:number value="${record.amounts.grossAmount}" format="currency"/></td>
					<td><o:out value="${record.currencySymbol}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>