<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="filterEvents"/>
	</div>
</div>
<div class="modalBoxData">
	<div>
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th><fmt:message key="type"/></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody style="font-weight: bold;">
						<tr>
							<td><fmt:message key="all"/></td>
							<td><v:link url="${sessionScope.currentView.baseLink}/filter"><o:img name="enabledIcon" /></v:link></td>
						</tr>
						<tr>
							<td><fmt:message key="fcsEventOrAlert"/></td>
							<td><v:link url="${sessionScope.currentView.baseLink}/filter?category=flowControl"><o:img name="enabledIcon" /></v:link></td>
						</tr>
						<tr>
							<td><fmt:message key="appointment"/></td>
							<td><v:link url="${sessionScope.currentView.baseLink}/filter?category=appointment"><o:img name="enabledIcon" /></v:link></td>
						</tr>
						<tr>
							<td><fmt:message key="salesOrPurchaseOrderChange"/></td>
							<td><v:link url="${sessionScope.currentView.baseLink}/filter?category=information"><o:img name="enabledIcon" /></v:link></td>
						</tr>
					</tbody>
				</table>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>
