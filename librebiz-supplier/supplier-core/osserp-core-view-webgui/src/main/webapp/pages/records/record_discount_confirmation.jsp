<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:if test="${view.confirmDiscountMode}">
<br />
<o:form name="recordForm" url="${requestScope.baseUrl}">
<input type="hidden" name="method" value="confirmDiscount" />
<table style="width: 100%;">
	<tr>
		<td colspan="4"><fmt:message key="recordDiscountConfirmationDialogHeader"/>.</td>
	</tr>
	<tr>
		<td style="width: 110px;"><span class="boldtext"><fmt:message key="authorizationLabel"/>:</span></td>
		
		<td colspan="3">
			<fmt:message key="recordDiscountConfirmationDialogStart"/>
			<input type="text" name="amount" value="<o:number format="currency" value="${view.requiredDiscount}"/>" style="width: 50px; text-align: right;" /> 
			<c:choose><c:when test="${view.discountPercent}"> % </c:when><c:otherwise> &euro;/Eh </c:otherwise></c:choose>
			<fmt:message key="recordDiscountConfirmationDialogEnd"/>.
		</td>
	</tr>
	<tr>
		<td style="width: 110px;"><span class="boldtext"><fmt:message key="statement"/>:</span></td>
		<td colspan="3">
			<input type="text" name="note" style="width: 560px;"/> 
			<a href="<c:url value="${requestScope.baseUrl}"><c:param name="method" value="disableConfirmDiscountMode"/></c:url>" title="<fmt:message key="exitEdit"/>"><img src="<c:url value="${applicationScope.backIcon}"/>" class="bigicon" style="margin-left: 15px;"/></a>
			<input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="confirmDiscount" onclick="setMethod('confirmDiscount');" style="margin-left: 3px; margin-right: 5px;" class="bigicon" title="<fmt:message key="saveInput"/>"/>
		</td>
	</tr>
</table>
</o:form>
<br />
</c:if>

