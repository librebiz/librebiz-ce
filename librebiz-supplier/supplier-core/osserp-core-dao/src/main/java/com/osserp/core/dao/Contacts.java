/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.mail.EmailAddress;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactGroup;
import com.osserp.core.contacts.ContactListEntry;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.mail.NewsletterRecipient;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Contacts extends Persons {
    
    static final int PATTERN_ONLY = 0;
    static final int WITH_PROJECTS = 1;
    static final int WITH_PLANS = 2;

    /**
     * Indicates that contact with given id exists
     * @param id
     * @return exists
     */
    boolean exists(Long id);

    /**
     * Loads the contact object by primary key. This method throws a runtime
     * exception if no such contact exists
     * @param contactId
     * @return contact
     */
    Contact loadContact(Long contactId);

    /**
     * Tries to fetch a contact by provided primary key.
     * @param contactId
     * @return contact object or null if not exists
     */
    Contact fetchContact(Long contactId);

    /**
     * Provides the contact by matching email address (ignoring case). 
     * @param emailAddress
     * @return contact or null if email address not exists
     */
    Contact fetchByEmailAddress(String emailAddress);

    /**
     * Changes the type of a contact to business contact
     * @param changedBy
     * @param contactToChange
     * @param company name
     * @throws ClientException if such a name already exists
     */
    void changeType(Long changedBy, Contact contactToChange, String company) throws ClientException;

    /**
     * Updates contact values on the backend
     * @param contact
     */
    void save(Contact contact);

    /**
     * Activates the specified relation on given contact
     * @param contact
     * @param contactGroup
     */
    void activateRelation(Contact contact, Long contactGroup);

    /**
     * Tries to find all contacts whose are member of specified contact group
     * @param contactGroup
     * @return contacts
     */
    List<Contact> findAllByGroup(Long contactGroup);

    /**
     * Provides all contacts with given email pattern
     * @param email
     * @return contacts or empty list
     */
    List<Contact> findByEmail(String email);

    /**
     * Provides the contact id / email pairs matching given email address
     * @param email
     * @return all existing contacts with same address
     */
    List<Option> findExistingEmailContacts(String email);

    /**
     * Finds all contacts with provided email address. This should be always 
     * one or none since default gui does not allow an address to be assigned
     * twice. But there might be some cases where the same address is assigned
     * to more than one contact so we don't limit the result here.   
     * @param emailAddress the address to lookup for exact match
     * @return contacts with provided email address assigned or empty list
     */
    List<ContactListEntry> findByEmailAddress(String emailAddress);

    /**
     * Finds the email address to send invoice per mail
     * @param contact
     * @return email address set as invoice receipt or other if not exists.
     */
    EmailAddress findBillingAddress(Contact contact);

    /**
     * Finds all contacts matching given email pattern
     * @param pattern to lookup for
     * @return empty list or list of ContactListEntry objects
     */
    List<ContactListEntry> findByEmailPattern(String pattern);

    /**
     * Finds all contacts matching given zipcode pattern
     * @param pattern to lookup for
     * @param startsWith indicates whether zipcode should match from beginning
     * @return empty list or list of ContactListEntry objects
     */
    List<ContactListEntry> findByZipcodePattern(String pattern, boolean startsWith);

    /**
     * Tries to find contacts whose zipcode and lastname values match given values
     * @param zipcode
     * @param lastName
     * @return existing contacts
     * @throws ClientException if email already exists
     */
    List<Contact> findExisting(String zipcode, String lastName);

    /**
     * Provides a list of all contacts related to another contact, e.g. contacts contact persons
     * @param contactId
     * @return list contact persons
     */
    List<Contact> findContactPersons(Long contactId);

    /**
     * Provides all contacts receiving or not receiving a newsletter
     * @param active indicates whether receiving or not receiving should be provided
     * @return recipients as newsletter recipiemnt objects
     */
    List<NewsletterRecipient> findNewsletterRecipients(boolean active);

    /**
     * Updates the newsletter status of the contacts.
     * @param contacts as array of contact ids
     * @param status
     */
    void updateNewsletterStatus(Long[] contacts, boolean status);

    /**
     * Provides all available contact groups ordered by name
     * @return contactGroups
     */
    List<ContactGroup> getContactGroups();

    /**
     * Provides all available contact types ordered by name
     * @return contactTypes
     */
    List<ContactType> getContactTypes();

    /**
     * Provides the contact group by id
     * @param id of the contact group
     * @return contactGroup
     */
    ContactGroup getContactGroupById(Long contactGroupId);

    /**
     * Updates the branch office flag
     * @param id
     * @param branch
     */
    void updateBranchFlag(Long id, boolean branch);
}
