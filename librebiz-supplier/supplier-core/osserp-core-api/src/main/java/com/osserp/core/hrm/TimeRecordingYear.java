/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 6, 2008 10:11:36 PM 
 * 
 */
package com.osserp.core.hrm;

import java.util.List;

import com.osserp.common.Month;
import com.osserp.common.Year;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordingYear extends Year {

    /**
     * Tests if year is current year
     * @return true if year is current
     */
    boolean isCurrent();

    /**
     * Tests if year is the first year in associated period
     * @return true if year is same as specified via <br/>
     * validFrom property in time recording period
     */
    boolean isFirst();

    /**
     * Provides supported months of year.
     * @return supportedMonths
     */
    List<TimeRecordingMonth> getSupportedMonths();

    /**
     * Provides currently selected month
     * @return selectedMonth
     */
    TimeRecordingMonth getSelectedMonth();

    /**
     * Selects a month. If month is null, the first month will be selected, e.g. it's not possible to set selected month value to null at this time
     * @param month
     */
    void selectMonth(Month month);

    /**
     * Tries to fetch a month.
     * @param month
     * @return timeRecordingMonth or null if not supported
     */
    TimeRecordingMonth fetchMonth(Month month);

    /**
     * Provides carryover minutes of year
     * @return carryoverMinutes
     */
    int getCarryoverMinutes();

    /**
     * Provides consumed minutes leave
     * @return consumed minutes leave
     */
    int getConsumedMinutesLeave();

    /**
     * Provides minutes lost
     * @return minutesLost
     */
    int getMinutesLost();

    /**
     * Provides carryover minutes leave
     * @return carryover minutes leave
     */
    int getCarryoverMinutesLeave();

    /**
     * Provides carryover leave in days
     * @return carryover leave in days
     */
    double getCarryoverLeave();

    /**
     * Provides remaining minutes leave
     * @return remaining minutes leave
     */
    int getRemainingMinutesLeave();

    /**
     * Provides annual leave in days
     * @return annualLeave
     */
    double getAnnualLeave();

    /**
     * Provides expired leave in days
     * @return expiredLeave
     */
    double getExpiredLeave();

    /**
     * Provides consumed leave in days
     * @return consumedLeave
     */
    double getConsumedLeave();

    /**
     * Provides remaining leave in days
     * @return remainingLeave
     */
    double getRemainingLeave();
}
