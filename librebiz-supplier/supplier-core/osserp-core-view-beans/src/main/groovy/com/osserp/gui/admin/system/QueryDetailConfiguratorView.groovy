/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 11.05.2011 17:16:59 
 * 
 */
package com.osserp.gui.admin.system

import com.osserp.common.ErrorCode
import com.osserp.common.dao.JdbcOutput
import com.osserp.common.dao.JdbcParameter
import com.osserp.common.dao.JdbcQuery
import com.osserp.common.dao.QueryConfigurator
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class QueryDetailConfiguratorView extends CoreView {

    private final String INPUT = 'input'
    private final String OUTPUT = 'output'

    List alignments = ['left', 'right', 'center']

    JdbcQuery query = null
    JdbcParameter selectedParameter = null
    JdbcOutput selectedOutput = null

    QueryDetailConfiguratorView() {
        enablePopupView()
        dependencies = ['queryConfigView']
        env.inputMode = false
        env.outputMode = true
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!env.queryConfigView || !env.queryConfigView?.bean) {
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
        }

        if (forwardRequest) {
            query = env.queryConfigView.bean
            Long id = form.getLong('id')
            if (id) {
                enableEditMode()
            } else {
                enableCreateMode()
            }
            String mode = form.getString('mode')
            if (mode == INPUT) {
                setupInputMode(id)
            } else {
                setupOutputMode(id)
            }
        }
    }

    private void setupInputMode(Long id) {
        env.inputMode = true
        env.outputMode = false
        headerName = 'addParameter'
        if (id) {
            selectedParameter = env.queryConfigView.bean.fetchParameter(id)
            bean = selectedParameter
            headerName = 'editParameter'
        }
    }

    private void setupOutputMode(Long id) {
        env.inputMode = false
        env.outputMode = true
        headerName = 'addOutputCleft'
        if (id) {
            selectedOutput = env.queryConfigView.bean.fetchOutput(id)
            bean = selectedOutput
            headerName = 'editOutputCleft'
        }
    }

    @Override
    void save() {
        if (inputMode) {
            saveParameter()
        } else {
            saveOutput()
        }
        env.queryConfigView.reload()
    }

    private void saveParameter() {
        String label = form.getString('label')
        String name = form.getString('name')
        String type = form.getString('type')
        if (!label || !name || !type) {
            throw new ViewContextException(ErrorCode.VALUES_MISSING)
        }
        if (createMode) {
            query.addParameter(label, name, type)
        } else {
            query.updateParameter(selectedParameter.id, label, name, type)
        }
        queryConfigurator.save(query)
    }

    private void saveOutput() {
        String name = form.getString('name')
        String type = form.getString('type')
        Integer width = form.getInteger('width') ?: 0
        String alignment = form.getString('alignment')
        boolean display = form.getBoolean('display')
        if (!name || !type || !alignment) {
            throw new ViewContextException(ErrorCode.VALUES_MISSING)
        }
        if (createMode) {
            query.addOutput(name, type, width, alignment, display)
        } else {
            query.updateOutput(selectedOutput.id, name, type, width, alignment, display)
        }
        queryConfigurator.save(query)
    }

    boolean isInputMode() {
        env.inputMode
    }

    boolean isOutputMode() {
        env.outputMode
    }

    private QueryConfigurator getQueryConfigurator() {
        getService(QueryConfigurator.class.getName())
    }
}
