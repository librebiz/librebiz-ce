/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 5, 2004 
 * 
 */
package com.osserp.common;

import java.util.List;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ClientException extends Exception {

    private static final long serialVersionUID = 42L;

    private Object errorObject = null;
    private String key = null;
    private String type = null;
    private Long errorId = null;
    private List<String> errorKeys = new java.util.ArrayList<String>();

    /**
     * 
     */
    public ClientException() {
        super();
    }

    /**
     * @param message
     */
    public ClientException(String message) {
        super(message);
    }

    /**
     * @param errorObject
     */
    public ClientException(Object errorObject) {
        super();
        this.errorObject = errorObject;
    }

    /**
     * @param message
     * @param errorObject
     */
    public ClientException(String message, Object errorObject) {
        super(message);
        this.errorObject = errorObject;
    }

    /**
     * @param message
     * @param id
     */
    public ClientException(String message, Long id) {
        super(message);
        this.errorId = id;
    }

    /**
     * @param key
     * @param message
     */
    public ClientException(String key, String message) {
        super(message);
        this.key = key;
    }

    /**
     * @param key
     * @param message
     * @param type
     */
    public ClientException(String key, String message, String type) {
        super(message);
        this.key = key;
        this.type = type;
    }

    /**
     * @param cause
     */
    public ClientException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public ClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public String getKey() {
        return key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getErrorId() {
        return errorId;
    }

    protected void setErrorId(Long errorId) {
        this.errorId = errorId;
    }

    public Object getErrorObject() {
        return errorObject;
    }

    protected void setErrorObject(Object errorObject) {
        this.errorObject = errorObject;
    }

    public List<String> getErrorKeys() {
        return errorKeys;
    }

    public void addErrorKey(String errorKey) {
        boolean exists = false;
        for (int i = 0, j = errorKeys.size(); i < j; i++) {
            String next = errorKeys.get(i);
            if (next.equals(errorKey)) {
                exists = true;
                break;
            }
        }
        if (!exists) {
            this.errorKeys.add(errorKey);
        }
    }
}
