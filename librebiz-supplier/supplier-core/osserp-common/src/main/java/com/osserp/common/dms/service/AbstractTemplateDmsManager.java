/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Dec 4, 2009
 *
 */
package com.osserp.common.dms.service;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.TemplateDmsManager;
import com.osserp.common.dms.dao.Templates;

/**
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class AbstractTemplateDmsManager extends AbstractDmsManager implements TemplateDmsManager {
    private Long typeId = null;

    /**
     * Creates a new template dms manager.
     * @param templates
     * @param typeId id of required type 
     */
    protected AbstractTemplateDmsManager(Templates templates, Long typeId) {
        super(templates);
        this.typeId = typeId;
    }
    
    public DmsDocument create(
            User user,
            String filename,
            FileObject fileObject,
            String note,
            Long categoryId) 
        throws ClientException {
        return getTemplates().create(
                user, 
                getSupportedType(), 
                filename, 
                fileObject, 
                note, 
                categoryId);
    }
    
    public DocumentType getSupportedType() {
        if (typeId == null) {
            throw new IllegalStateException("typeId not initialized");
        }
        return getTemplates().getDocumentType(typeId);
    }

    public List<DmsDocument> findDocuments() {
        List<DmsDocument> result = new ArrayList<>();
        List<DmsDocument> all = getTemplates().findByType(getSupportedType());
        for (int i = 0, j = all.size(); i < j; i++) {
            DmsDocument next = all.get(i);
            if (!next.isUnused()) {
                result.add(next);
            }
        }
        return result;
    }

    public void delete(DmsDocument document) throws ClientException {
        getTemplates().deleteDocument(document);
    }

    /**
     * Casts documents to templates as provided via constructor.
     * @return templates dao
     */
    protected final Templates getTemplates() {
        return (Templates) getDocumentsDao();
    }
}
