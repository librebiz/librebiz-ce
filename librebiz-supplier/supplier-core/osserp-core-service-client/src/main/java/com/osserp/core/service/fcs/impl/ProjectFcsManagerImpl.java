/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Feb-2007 10:19:55 
 * 
 */
package com.osserp.core.service.fcs.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.DateFormatter;

import com.osserp.core.BusinessCase;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.FcsItem;
import com.osserp.core.Item;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.dao.FlowControlWastebaskets;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.FlowControlItemImpl;
import com.osserp.core.products.Package;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductDetails;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.projects.Project;
import com.osserp.core.projects.ProjectFcsManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesClosedStatus;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.service.fcs.ProjectFcsEventCreator;
import com.osserp.core.tasks.ProductPlanningCacheSender;
import com.osserp.core.tasks.SalesMonitoringCacheSender;
import com.osserp.core.tasks.SalesVolumeReportSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectFcsManagerImpl extends AbstractFcsManager implements ProjectFcsManager {
    private static Logger log = LoggerFactory.getLogger(ProjectFcsManagerImpl.class.getName());

    private Products products = null;
    private ProductPlanningCacheSender productPlanningCacheTask = null;
    private SalesVolumeReportSender salesVolumeReportTask = null;
    private SalesMonitoringCacheSender salesMonitoringCacheTask = null;
    private Projects projects = null;
    private SalesInvoices invoices = null;
    private SalesOrderManager orderManager = null;

    public ProjectFcsManagerImpl(
            FlowControlActions actions,
            ProjectFcsEventCreator eventJobs,
            FlowControlWastebaskets wastebaskets,
            Projects projects,
            SalesOrderManager orderManager,
            SalesInvoices invoices,
            Products products,
            BusinessNotes businessNotes,
            ProductPlanningCacheSender productPlanningCacheTask,
            SalesVolumeReportSender salesVolumeReportTask,
            SalesMonitoringCacheSender salesMonitoringCacheTask) {

        super(actions, eventJobs, wastebaskets, businessNotes);
        this.projects = projects;
        this.orderManager = orderManager;
        this.invoices = invoices;
        this.productPlanningCacheTask = productPlanningCacheTask;
        this.products = products;
        this.salesVolumeReportTask = salesVolumeReportTask;
        this.salesMonitoringCacheTask = salesMonitoringCacheTask;
    }
    
    public BusinessCase resetCancellation(Employee employee, BusinessCase businessCase) throws ClientException {
        Sales sales = (Sales) businessCase;
        SalesOrder order = fetchOrder(sales);
        if (order != null) {
            order.markChanged();
            orderManager.persist(order);
        }
        String cancellationNote = sales.resetCancellation(employee.getId());
        projects.save(sales);
        addBusinessNote(employee, sales, cancellationNote);
        return projects.load(sales.getId());
    }

    public BusinessCase resetClosed(Employee employee, BusinessCase businessCase) throws ClientException {
        Sales sales = (Sales) businessCase;
        SalesOrder order = fetchOrder(sales);
        if (order != null) {
            order.markChanged();
            orderManager.persist(order);
        }
        String reopenNote = sales.resetClosed(employee.getId());
        projects.save(sales);
        addBusinessNote(employee, sales, reopenNote);
        return projects.load(sales.getId());
    }
    
    public void addFlowControl(
            Employee employee,
            BusinessCase businessCase,
            FcsAction actionObj,
            Date created,
            String note,
            String headline,
            FcsClosing closing,
            boolean ignoreDependencies) throws ClientException {

        if (actionObj == null) {
            log.error("addFlowControl() invoked with null value for fcs action object");
            throw new IllegalArgumentException("actionObj param must not be null");
        }
        FlowControlAction action = (FlowControlAction) actionObj;
        Sales sales = (Sales) businessCase;

        if (log.isDebugEnabled()) {
            logAddRequest(sales, employee, action);
        }
        SalesOrder order = (SalesOrder) orderManager.getBySales(sales);
        if (order == null) {
            if (log.isDebugEnabled()) {
                log.debug("addFlowControl() invalid state: did not find order...");
            }
            throw new ClientException(ErrorCode.ORDER_MISSING);
        }
        if (action.isCancelling() || action.isClosing()) {
            if (log.isDebugEnabled()) {
                log.debug("addFlowControl() action is cancelling or closing...");
            }
            if (order.isChangeableDeliveryAvailable()) {
                if (log.isDebugEnabled()) {
                    log.debug("addFlowControl() invalid state: changeable deliveries found...");
                }
                throw new ClientException(ErrorCode.UNRELEASED_RECORD);
                
            } else if (log.isDebugEnabled()) {
                log.debug("addFlowControl() no changeable deliveries found, ok...");
            }
            
            if (action.isCancelling()) {
                if (log.isDebugEnabled()) {
                    log.debug("addFlowControl() action is cancelling...");
                }
                if (!order.getDeliveredGoods().isEmpty()) {
                    throw new ClientException(ErrorCode.ORDER_DELIVERIES_FOUND);
                }
                validateCancel(sales);
            } else if (log.isDebugEnabled()) {
                log.debug("addFlowControl() action not cancelling...");
            }
            if (action.isClosing() && !action.isClosingUnconditionally()) {
                if (log.isDebugEnabled()) {
                    log.debug("addFlowControl() action is closing...");
                }
                validateClosing(sales, order);
                
            } else if (log.isDebugEnabled()) {
                log.debug("addFlowControl() action not closing...");
            }
        }
        validateBeforeAdd(action, created, note, headline);
        List<FcsAction> todos = getFcsActions().createActionList(sales);
        if (!ignoreDependencies) {
            if (log.isDebugEnabled()) {
                log.debug("addFlowControl() checking dependencies...");
            }
            checkDependencies(todos, action);
        }
        boolean added = sales.addFlowControl(
                action,
                employee.getId(),
                created,
                note,
                headline,
                closing);

        if (added) {
            if (log.isDebugEnabled()) {
                log.debug("addFlowControl() added [action=" + action.getId()
                        + "] trying to persist status");
            }
            saveBusinessCase(sales);
            if (log.isDebugEnabled()) {
                log.debug("addFlowControl() persist [sales="
                        + sales.getId() + "] done");
            }
            if (action.isStartingUp()) {
                if (sales instanceof Project) {
                    ((Project) sales).setStartUp(
                            created == null
                                    ? new Date(System.currentTimeMillis())
                                    : created);
                    saveBusinessCase(sales);
                    if (log.isDebugEnabled()) {
                        log.debug("addFlowControl() startup set [date="
                                + ((Project) sales).getStartUp() + "]");
                    }
                } else {
                    log.warn("addFlowControl() found startup flag [action="
                            + action.getId() + "] but sales [" + sales.getId()
                            + "] was instanceof [" + sales.getClass().getName()
                            + "]");
                }
            }
            if (action.isStopping()) {
                sales.setStopped(true);
                saveBusinessCase(sales);
                orderManager.resetDeliveryDate(sales);

            } else if (sales.isStopped()) {
                sales.setStopped(false);
                saveBusinessCase(sales);
            }
            if (sales.isClosed() || sales.isCancelled()) {
                if (log.isDebugEnabled()) {
                    log.debug("addFlowControl() sales [" + sales.getId()
                            + "] is cancelled or closed, closing order");
                }
                SalesClosedStatus defaultStatus = projects.getClosedSatusDefault();
                sales.close(defaultStatus, null);
                saveBusinessCase(sales);
                orderManager.close(employee, sales);
                cleanupWastebasket(sales.getId());
            }
            if (action.isClosingService()) {
                orderManager.closeService(employee, order);
            }
            if (action.isPerformEvents()) {

                createEvents(
                    sales,
                    action,
                    created,
                    employee.getId(),
                    sales.getId(),
                    note,
                    headline,
                    false);
                if (log.isDebugEnabled()) {
                    log.debug("addFlowControl() event job created");
                }
            } else if (log.isDebugEnabled()) {
                log.debug("addFlowControl() perform events disabled [action=" + action.getId() + "]");
            }
            // perform queue related actions at the end of an action if configured  
            if (action.isPerformAction() && action.isPerformAtEnd()) {
                // TODO perform post actions
                //this.performPostActions(action, employee.getId(), sale, false);
                /*
                 * if (log.isDebugEnabled()) { log.debug("addFlowControl() end action performed on action " + action.getId() + " - " + action.getName()); }
                 */
            }
            productPlanningCacheTask.send(order.getItems());
            salesVolumeReportTask.send(sales);
            salesMonitoringCacheTask.send(sales.getType().getCompany());
            
        } else {
            log.warn("addFlowControl() ignoring already added action "
                    + action.getId()
                    + " on sales "
                    + sales.getId());
        }
    }
    
    public void cancelDeliveryClosing(Employee user, SalesOrder order) {
        if (log.isDebugEnabled()) {
            log.debug("cancelDeliveryClosing() invoked [order= " + order.getId() + "]");
        }
        try {
            Sales sales = projects.load(order.getBusinessCaseId());
            for (int i = 0, j = sales.getFlowControlSheet().size(); i < j; i++) {
                FcsItem next = sales.getFlowControlSheet().get(i);
                FlowControlAction action = (FlowControlAction) next.getAction();
                // 	check if not canceled delivery closing
                if (action.isClosingDelivery()
                        && !next.isCancels()
                        && next.getCanceledBy() == null) {
                    try {
                        cancelFlowControl(
                                user.getId(),
                                sales,
                                next,
                                "Auto-Reset");
                        if (log.isDebugEnabled()) {
                            log.debug("resetDeliveryClosing() done for " + sales.getId());
                        }
                    } catch (Exception e) {
                    }
                }
            }
            salesVolumeReportTask.send(sales);
            salesMonitoringCacheTask.send(sales.getType().getCompany());
        } catch (Throwable t) {
            log.error("cancelDeliveryClosing() ignoring failure: "
                    + t.toString(), t);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.service.AbstractFcsManager#cancelFlowControl(java.lang.Long, com.osserp.core.BusinessCase, com.osserp.core.FcsItem,
     * java.lang.String)
     */
    @Override
    public boolean cancelFlowControl(Long employeeId, BusinessCase businessCase, FcsItem item, String note) throws ClientException {
        boolean canceled = super.cancelFlowControl(employeeId, businessCase, item, note);
        if (canceled && businessCase instanceof Sales) {
            Sales sales = (Sales) businessCase;
            salesVolumeReportTask.send(sales);
            salesMonitoringCacheTask.send(sales.getType().getCompany());
        }
        return canceled;
    }

    @Override
    public void throwFlowControl(Long employeeId, BusinessCase businessCase, Long actionId) throws ClientException {
        FlowControlAction selected = (FlowControlAction) getFcsActions().load(actionId);
        if (selected.isStartingUp()) {
            throw new ClientException(ErrorCode.FLOW_CONTROL_WASTEBASKET);
        }
        super.throwFlowControl(employeeId, businessCase, actionId);
    }
    
    public void createInitialEvents(Sales sales) {
        if (sales != null && sales.getId() != null) {
            FcsAction initialAction = getStartingAction(sales);
            if (initialAction != null && initialAction.isPerformEvents()) {

                createEvents(
                        sales,
                        initialAction,
                        sales.getCreated(),
                        sales.getCreatedBy(),
                        sales.getPrimaryKey(),
                        null, // note
                        null, // headline
                        false);
                    
            } else if (log.isDebugEnabled()) {
                if (initialAction == null) {
                    log.debug("createEvents() initial fcs action not found [type=" 
                            + sales.getType().getId() + "]");
                } else {
                    log.debug("createEvents() initial fcs does not perform events [type=" 
                            + sales.getType().getId()
                            + ", action=" + initialAction.getId()
                            + "]");
                }
            }
            if (salesVolumeReportTask != null) {
                try {
                    salesVolumeReportTask.send(sales);
                } catch (Throwable e) {
                    log.error("createEvents() failed to send salesVolumeReport update event [message="
                            + e.getMessage() + "]");
                }
            }
            if (salesMonitoringCacheTask != null) {
                try {
                    salesMonitoringCacheTask.send(sales.getType().getCompany());
                } catch (Throwable e) {
                    log.error("createEvents() failed to send salesMonitoringCache update event [message="
                            + e.getMessage() + "]");
                }
            }
        }
    }

    private FcsAction getStartingAction(Sales sales) {
        Long businessType = sales.getType().getId();
        List<FcsAction> all = getFcsActions().findByType(businessType);
        if (!all.isEmpty()) {
            return all.get(0);
        }
        return null;
    }

    @Override
    protected FcsItem createCancelItem(
            BusinessCase businessCase,
            FcsAction action,
            Long employeeId,
            String note) {

        return new FlowControlItemImpl(
                (Sales) businessCase,
                action,
                new Date(System.currentTimeMillis()),
                employeeId,
                note,
                null,
                true,
                null);
    }

    @Override
    protected void saveBusinessCase(BusinessCase object) {
        projects.save((Sales) object);
    }

    @Override
    protected void saveFcsItem(FcsItem object) {
        projects.save(object);
    }
    
    private SalesOrder fetchOrder(Sales sales) {
        return (SalesOrder) orderManager.getBySales(sales);
    }

    private void validateCancel(Sales sales) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("validateCancel() invoked...");
        }
        validateAccountBallance(sales);
        if (log.isDebugEnabled()) {
            log.debug("validateCancel() checking that invoices are cancelled...");
        }
        List<SalesInvoice> invoiceList = invoices.getBySales(sales);
        for (int i = 0, j = invoiceList.size(); i < j; i++) {
            SalesInvoice next = invoiceList.get(i);
            if (!next.isCanceled()) {
                if (log.isDebugEnabled()) {
                    log.debug("validateCancel() failed, invoice ["
                            + next.getId() + "] not cancelled!");
                }
                throw new ClientException(ErrorCode.INVOICES_FOUND_CANCEL_DENIED);
            }
            if (next.getCancellation() != null && !next.getCancellation().isUnchangeable()) {
                throw new ClientException(ErrorCode.UNRELEASED_RECORD);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("validateCancel() done, ok...");
        }
    }

    private void validateClosing(Sales sales, SalesOrder order) throws ClientException {
        boolean donation = order.getPaymentAgreement() != null
                && order.getPaymentAgreement().getPaymentCondition() != null
                && order.getPaymentAgreement().getPaymentCondition().isDonation();
        if (!donation) {
            validateAccountBallance(sales);
        }
        if (!orderManager.isBilled(order)) {
            List<SalesInvoice> invoiceList = invoices.getBySales(sales);
            if (invoiceList.isEmpty() && !sales.getType().isWorkflowOnly()) {
                // goods are not billed, check that order contains free services
                if (!sales.getType().isService() || !donation) {
                    throw new ClientException(ErrorCode.SERVICES_NOT_BILLED);
                }
                boolean freeServiceAvailable = donation ? true : false;
                boolean noneFreeServiceAvailable = false;
                for (int i = 0, j = order.getItems().size(); i < j; i++) {
                    Item nextItem = order.getItems().get(i);
                    if (nextItem.getProduct().isPlant()) {
                        Product pkgProduct = products.load(nextItem.getProduct().getProductId());
                        ProductDetails details = pkgProduct.getDetails();
                        if (details == null || !(details instanceof Package)) {
                            throw new ClientException(ErrorCode.PACKAGE_CONFIG_INVALID);
                        }
                        Package pkg = (Package) details;
                        if (pkg.isFreeService()) {
                            freeServiceAvailable = true;
                        } else {
                            noneFreeServiceAvailable = true;
                        }
                    }
                }
                if (!freeServiceAvailable || noneFreeServiceAvailable) {
                    throw new ClientException(ErrorCode.SERVICES_NOT_BILLED);
                }
            }
        }
    }

    private void validateAccountBallance(Sales sales) throws ClientException {
        double accountBalance = projects.getAccountBalance(sales.getId());
        if (accountBalance != 0) {
            throw new ClientException(ErrorCode.ACCOUNT_UNBALANCED);
        }
        if (log.isDebugEnabled()) {
            log.debug("validateAccountBallance() done, ok...");
        }
    }

    private void logAddRequest(Sales sales, Employee employee, FlowControlAction action) {
        StringBuffer buffer = new StringBuffer();
        try {
            int fcsSize = sales.getFlowControlSheet() == null ? 0 : sales.getFlowControlSheet().size();
            buffer
                    .append("addFlowControl() invoked [employee=")
                    .append((employee == null ? "null" : employee.getId()))
                    .append(", sales=").append(sales.getId())
                    .append(", action=").append(action.getId())
                    .append(" - ").append(action.getName())
                    .append(", fcsCount=").append(fcsSize)
                    .append("]:\n");
            if (fcsSize > 0) {
                for (int i = 0, j = fcsSize; i < j; i++) {
                    FcsItem next = sales.getFlowControlSheet().get(i);
                    buffer
                            .append("\n[created=")
                            .append(DateFormatter.getDate(next.getCreated()))
                            .append(",id=")
                            .append(next.getAction().getId())
                            .append(",name=")
                            .append(next.getAction().getName())
                            .append("]]");
                }
                buffer.append("\n");
            }
            log.debug(buffer.toString());
        } catch (Throwable e) {
            log.info("logAddRequest() failed, message ('til crashed): " + buffer.toString());
        }
    }
}
