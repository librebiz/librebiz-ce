/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 12, 2016
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.util.DateUtil;

import com.osserp.core.finance.FinanceReport;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public abstract class AbstractFinanceReport extends AbstractOption implements FinanceReport {

    private Date startDate;
    private Date stopDate;
    private int year;

    /**
     * Creates an empty finance report
     */
    protected AbstractFinanceReport() {
        super();
    }
    
    /**
     * Creates a new annual finance report. Start and end date will be 
     * initialized with year/01/01 and year/12/31.
     * @param company the company id is stored as reference property
     * @param reportYear the year is stored as primaryKey property, e.g. id
     */
    public AbstractFinanceReport(Long company, int reportYear) {
        super(Long.valueOf(reportYear), company, "annualReport");
        year = reportYear;
        if (reportYear == 0) {
            reportYear = DateUtil.getCurrentYear();
            year = reportYear;
            setId(Long.valueOf(reportYear));
        }
        startDate = DateUtil.getFirstDayOfYear(year);
        stopDate = DateUtil.getFirstDayOfYear(year + 1);
    }
    
    /**
     * Creates a new initialized finance report
     * @param createdBy
     * @param reference
     * @param name
     * @param startDate
     * @param stopDate
     */
    public AbstractFinanceReport(
            Long createdBy, 
            Long reference,
            String name, 
            Date startDate, 
            Date stopDate) {
        super(name, reference, createdBy);
        this.startDate = startDate;
        this.stopDate = stopDate;
    }
    
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}
