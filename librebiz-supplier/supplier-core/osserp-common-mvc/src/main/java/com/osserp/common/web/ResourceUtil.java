/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.common.web;

import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.context.WebApplicationContext;

import com.osserp.common.Constants;
import com.osserp.common.User;
import com.osserp.common.service.ResourceLocator;

/**
* Provides ResourceLocator and ServiceLocator access for webapps.
*  
* Using this class requires an initialized ServiceLocator and ResourceLocator
* available to work. 
* E.g. you must provide a /com/osserp/common/service/impl/ServiceLocator.xml 
* having the com/osserp/common/resources/ResourceLocatorConfig.xml imported
* in your classpath.
* 
 * @author Rainer Kirchner <rk@osserp.com>
* 
*/
public class ResourceUtil {
   private static Logger log = LoggerFactory.getLogger(ResourceUtil.class.getName());

   /**
    * Gets an available service by name
    * @param request
    * @param serviceName
    * @return service 
    * @throws RuntimeException if serviceLocator could not be initialized or service not exists 
    */
   public static Object getService(HttpServletRequest request, String serviceName) {
       return fetchService(request.getSession(), serviceName);
   }

   public static void reloadResources(HttpServletRequest request) {
       Locale locale = getLocale(request.getSession(), request);
       ResourceLocator resLocator = (ResourceLocator) getService(request, ResourceLocator.class.getName());
       resLocator.reload(locale);
   }

   public static void initLocale(HttpServletRequest request) {
       HttpSession session = request.getSession(true);
       if (session.getAttribute(Globals.LOCALIZATION_CONTEXT_SESSION) == null) {
           Locale locale = getLocale(session, request);
           if (log.isDebugEnabled()) {
               log.debug("initLocale() locale fetched [language="
                       + locale.getLanguage()
                       + ", country="
                       + locale.getCountry() + "]");
           }
           createLocalizationContext(session, locale);
       }
   }

   public static void updateLocale(HttpServletRequest request) {
       HttpSession session = request.getSession(true);
       Locale locale = getLocale(session, request);
       if (log.isDebugEnabled()) {
           log.debug("updateLocale() invoked [language="
                   + locale.getLanguage() + ", country=" + locale.getCountry() + "]");
       }
       createLocalizationContext(session, locale);
   }

   private static void createLocalizationContext(HttpSession session, Locale locale) {
       ResourceLocator resLocator = (ResourceLocator) fetchService(session, ResourceLocator.class.getName());
       if (resLocator != null) {
           ResourceBundle bundle = resLocator.getResourceBundle(locale);
           if (bundle != null) {
               LocalizationContext context = new LocalizationContext(bundle, locale);
               session.setAttribute(Globals.LOCALIZATION_CONTEXT_SESSION, context);
           } else if (log.isDebugEnabled()) {
               log.debug("createLocalizationContext() no resource bundle found [language="
                   + locale.getLanguage() + ", country=" + locale.getCountry() + "]");
           }
       }
   }

   private static Object fetchService(HttpSession session, String serviceName) {
       ServletContext context = session.getServletContext();
       Enumeration<String> attrNames = context.getAttributeNames();
       while (attrNames.hasMoreElements()) {
           String attrName = attrNames.nextElement();
           if (attrName.startsWith(Globals.SPRING_WEB) 
                   && context.getAttribute(attrName) instanceof WebApplicationContext) {
               WebApplicationContext wctx = (WebApplicationContext) context.getAttribute(attrName);
               if (wctx.containsBean(serviceName)) {
                   try {
                       Object obj = wctx.getBean(serviceName);
                       if (obj != null) {
                           return obj;
                       }
                       
                   } catch (Exception e) {
                       log.info("fetchService() ignoring failed lookup [name=" + serviceName + "]", e); 
                   }
               }
           }
       } 
       log.warn("fetchService: webApplicationContext not found!");
       return null;
   }

   private static Locale getLocale(HttpSession session, HttpServletRequest request) {
       try {
           User user = SessionUtil.getUser(session);
           return user.getLocale();
       } catch (Exception e) {
           // ignore this
       }
       if (isLocaleFallbackByRequest(request) && request.getLocale() != null) {
           return request.getLocale();
       }
       return Constants.DEFAULT_LOCALE_OBJECT;
   }

   private static boolean isLocaleFallbackByRequest(HttpServletRequest request) {
       return SessionUtil.isContextObjectEnabled(
               request.getSession().getServletContext(), 
               Globals.LOCALE_FALLBACK_REQUEST);
   }

   public static void disableLocaleFallbackByRequest(ServletContext ctx) {
       SessionUtil.addContextObject(ctx, Globals.LOCALE_FALLBACK_REQUEST, false);
   }

   public static void enableLocaleFallbackByRequest(ServletContext ctx) {
       SessionUtil.addContextObject(ctx, Globals.LOCALE_FALLBACK_REQUEST, true);
   }
}
