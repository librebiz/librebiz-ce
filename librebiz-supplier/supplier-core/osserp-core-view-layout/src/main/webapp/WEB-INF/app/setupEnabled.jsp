<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main_public.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="setupContent">
            <div class="row" style="margin-top: 50px;">

                <div class="col-md-3"> </div>
                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <fmt:message key="setupEnabledHeader" />
                            </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="setupEnabledLabel" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <fmt:message key="setupEnabledHint" />
                                    <br /><br />
                                    <v:link url="/login/forward"><fmt:message key="setupEnabledLoginHint" /></v:link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"> </div>

            </div>
        </div>
    </tiles:put>
</tiles:insert>
<c:remove var="user" scope="session"/>
<c:remove var="portalView" scope="session"/>
