<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="accountTransactionView" />
<c:set var="bankAccountSelectionUrl" scope="request" value="/accounting/accountTransaction" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="styles" type="string">
        <style type="text/css">
</style>
    </tiles:put>
    <tiles:put name="headline">
        <c:choose>
            <c:when test="${view.createMode}">
                <fmt:message key="newBooking" />
            </c:when>
            <c:otherwise>
                <c:if test="${!empty view.list}">
                    <o:listSize value="${view.list}" />
                </c:if>
                <fmt:message key="commonTransactionsLabel" />
                <c:if test="${!empty view.selectedBankAccount}"> - <o:out value="${view.selectedBankAccount.name}" />
                </c:if>
            </c:otherwise>
        </c:choose>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="accountTransactionContent">
            <c:choose>
                <c:when test="${empty view.selectedBankAccount and (!view.supplierMode or 
                        (view.supplierMode and view.createMode))}">
                    <c:import url="_bankAccountSelectionList.jsp" />
                </c:when>
                <c:when test="${view.createMode}">
                    <v:form name="accountTransactionForm" url="/accounting/accountTransaction/save">
                        <c:choose>
                            <c:when test="${not empty view.selectedContact and not empty view.selectedType}">
                                <c:import url="_accountTransactionCreate.jsp" />
                            </c:when>
                            <c:otherwise>
                                <c:import url="_accountTransactionConfig.jsp" />
                            </c:otherwise>
                        </c:choose>
                    </v:form>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <c:import url="_accountTransactionList.jsp" />
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </tiles:put>
</tiles:insert>
