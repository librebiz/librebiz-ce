/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 19:20:50 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Option;

import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.finance.RecordDiscount;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Stock;
import com.osserp.core.products.Product;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.sales.SalesUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderImpl extends AbstractOrder implements SalesOrder {
    private static Logger log = LoggerFactory.getLogger(SalesOrderImpl.class.getName());

    private boolean delivered = false;
    private boolean mainDelivered = false;
    private String calculationInfo = null;
    private String calculatorName = null;
    private Double minimalMargin = SalesUtil.getDefaultMinimalMargin();
    private Double targetMargin = SalesUtil.getDefaultTargetMargin();

    /**
     * Default constructor required by Serializable
     */
    protected SalesOrderImpl() {
        super();
    }

    /**
     * Creates a new empty order related to given sale
     * @param id
     * @param type
     * @param bookingType
     * @param sales
     * @param paymentAgreement
     * @param createdBy
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     * @param calculatorName
     * @param dateConfirmed
     * @param note
     */
    public SalesOrderImpl(
            Long id,
            RecordType type,
            BookingType bookingType,
            Sales sales,
            PaymentAgreement paymentAgreement,
            Employee createdBy,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable,
            String calculatorName,
            Date dateConfirmed,
            String note) {
        this(
                id,
                type,
                bookingType,
                sales.getType().getCompany(),
                sales.getBranch().getId(),
                null,
                sales.getRequest().getCustomer(),
                createdBy,
                (sales.getId() != null ? sales.getId() : null),
                taxRate,
                reducedTaxRate,
                sales.getType().getMinimalMargin(),
                sales.getType().getTargetMargin(),
                itemsChangeable,
                itemsEditable);
        if (log.isDebugEnabled()) {
            log.debug("<init> super invoked [created=" + getCreated() + "]");
        }
        if (sales.getType().isDirectSales()) {
            setCreated(sales.getCreated());
        }
        setConfirmationDate(dateConfirmed == null ? getCreated() : dateConfirmed);
        setConfirmationNote(note);
        setBusinessCaseId(sales.getId());
        initKanbanDeliveryFlag(sales);
        initPaymentAgreement(paymentAgreement);
        setCalculatorName(calculatorName);
    }

    /**
     * Creates a new empty order related to given sale
     * @param id
     * @param type
     * @param bookingType
     * @param sales
     * @param paymentAgreement
     * @param createdBy
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    public SalesOrderImpl(
            Long id,
            RecordType type,
            BookingType bookingType,
            Sales sales,
            PaymentAgreement paymentAgreement,
            Employee createdBy,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {
        this(
                id,
                type,
                bookingType,
                sales.getType().getCompany(),
                sales.getBranch().getId(),
                null,
                sales.getRequest().getCustomer(),
                createdBy,
                (sales.getId() != null ? sales.getId() : null),
                taxRate,
                reducedTaxRate,
                sales.getType().getMinimalMargin(),
                sales.getType().getTargetMargin(),
                itemsChangeable,
                itemsEditable);
        setBusinessCaseId(sales.getId());
        initKanbanDeliveryFlag(sales);
        initPaymentAgreement(paymentAgreement);
    }

    /**
     * TODO if not longer required or usage should be documented Creates a new internal order related to given sale
     * @param id
     * @param type
     * @param bookingType
     * @param paymentAgreement
     * @param company
     * @param reference
     * @param customer
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param minimalMargin
     * @param targetMargin
     */
    public SalesOrderImpl(
            Long id,
            RecordType type,
            BookingType bookingType,
            PaymentAgreement paymentAgreement,
            Long company,
            Long branchId,
            Long reference,
            Customer customer,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            Double minimalMargin,
            Double targetMargin) {
        this(
                id,
                type,
                bookingType,
                company,
                branchId,
                reference,
                customer,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                minimalMargin,
                targetMargin,
                true,
                true);
        initPaymentAgreement(paymentAgreement);
    }

    /**
     * Creates a new empty order related to given sale
     * @param id
     * @param type
     * @param bookingType
     * @param sales
     * @param createdBy
     * @param offer
     * @param stock
     * @param dateConfirmed
     * @param confirmationNote
     */
    public SalesOrderImpl(
            Long id,
            RecordType type,
            BookingType bookingType,
            Sales sales,
            Employee createdBy,
            SalesOffer offer,
            Stock stock,
            Date dateConfirmed,
            String confirmationNote) {
        this(
                id,
                type,
                bookingType,
                sales.getType().getCompany(),
                sales.getBranch().getId(),
                null,
                sales.getRequest().getCustomer(),
                createdBy,
                sales.getId(),
                offer.getAmounts().getTaxRate(),
                offer.getAmounts().getReducedTaxRate(),
                sales.getType().getMinimalMargin(),
                sales.getType().getTargetMargin(),
                (sales.getType().isDirectSales() || !sales.getType().isRecordByCalculation()),
                (sales.getType().isDirectSales() || !sales.getType().isRecordByCalculation()));
        if (log.isDebugEnabled()) {
            log.debug("<init> invoked from " + (createdBy == null ? "null" : createdBy.getId()));
        }
        List<RecordInfo> infos = new ArrayList<RecordInfo>();
        for (RecordInfo info : offer.getInfos()) {
            infos.add(new SalesOrderInfoImpl(this, info.getInfoId(), info.getCreatedBy()));
        }
        setInfos(infos);
        setNote(offer.getNote());
        initKanbanDeliveryFlag(sales);
        if (getPaymentAgreements().isEmpty()) {
            getPaymentAgreements().add(new SalesOrderPaymentAgreementImpl(this, offer));
        }
        calculatorName = offer.getCalculatorName();
        if (dateConfirmed != null) {
            setConfirmationDate(dateConfirmed);
        }
        if (isSet(confirmationNote)) {
            setConfirmationNote(confirmationNote);
        }
        if (offer.isTaxFree()) {
            setTaxFree(true);
            setTaxFreeId(offer.getTaxFreeId());
        }
        updateItems(offer);
        if (stock != null) {
            for (Iterator<Item> i = getItems().iterator(); i.hasNext();) {
                Item next = i.next();
                next.setStockId(stock.getId());
            }
        }
    }

    /**
     * Creates a new and empty sales order with all required values to set
     * @param id
     * @param type
     * @param bookingType
     * @param company
     * @param branchId
     * @param reference
     * @param customer
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param minimalMargin
     * @param targetMargin
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected SalesOrderImpl(
            Long id,
            RecordType type,
            BookingType bookingType,
            Long company,
            Long branchId,
            Long reference,
            Customer customer,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            Double minimalMargin,
            Double targetMargin,
            boolean itemsChangeable,
            boolean itemsEditable) {
        super(
                id,
                company,
                branchId,
                type, //RecordType.ORDER,
                bookingType,
                reference,
                customer,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                itemsChangeable,
                itemsEditable);
        if (customer.getPaymentAgreement() != null &&
                customer.getPaymentAgreement().isTaxFree()) {
            setTaxFree(customer.getPaymentAgreement().isTaxFree());
            setTaxFreeId(customer.getPaymentAgreement().getTaxFreeId());
        }
        this.minimalMargin = minimalMargin;
        this.targetMargin = targetMargin;
    }

    @Override
    public boolean isSales() {
        return true;
    }

    @Override
    public String getNumber() {
        StringBuilder number = new StringBuilder(super.getNumber());
        Integer version = getVersion();
        if (version != null && version.intValue() > 0) {
            number.append("-").append(version);
        }
        return number.toString();
    }

    public String getCalculationInfo() {
        return calculationInfo;
    }

    public void setCalculationInfo(String calculationInfo) {
        this.calculationInfo = calculationInfo;
    }

    public String getCalculatorName() {
        return calculatorName;
    }

    public void setCalculatorName(String calculatorName) {
        this.calculatorName = calculatorName;
    }

    public Double getMinimalMargin() {
        return minimalMargin;
    }

    public void setMinimalMargin(Double minimalMargin) {
        this.minimalMargin = minimalMargin;
    }

    public Double getTargetMargin() {
        return targetMargin;
    }

    public void setTargetMargin(Double targetMargin) {
        this.targetMargin = targetMargin;
    }

    public boolean isCorrection() {
        return false;
    }

    public boolean isDelivered() {
        return delivered;
    }

    protected void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public void closeDelivery() {
        this.delivered = true;
    }

    public void resetDelivered() {
        this.delivered = false;
    }

    public boolean isMainDelivered() {
        return mainDelivered;
    }

    protected void setMainDelivered(boolean mainDelivered) {
        this.mainDelivered = mainDelivered;
    }

    public boolean isLinkedWithSale() {
        return (getReference() != null);
    }

    public final void updateByCalculation(Calculation calculation) {
        updateItems(calculation.getPositions(), calculation.getOptions());
        calculatorName = calculation.getCalculatorClass();
    }

    public Map<Long, Item> getDeliveredGoods() {
        Map<Long, Item> deliveredGoods = new java.util.HashMap<Long, Item>();
        for (int i = 0, j = getDeliveryNotes().size(); i < j; i++) {
            DeliveryNote deliveryNote = getDeliveryNotes().get(i);

            for (int k = 0, l = deliveryNote.getItems().size(); k < l; k++) {
                Item item = deliveryNote.getItems().get(k);
                if (deliveredGoods.containsKey(item.getProduct().getProductId())) {
                    Item existing = deliveredGoods.get(item.getProduct().getProductId());
                    double quantity = existing.getQuantity() + item.getQuantity();
                    if (quantity == 0) {
                        deliveredGoods.remove(item.getProduct().getProductId());
                    } else {
                        existing.setQuantity(quantity);
                    }
                } else {
                    deliveredGoods.put(
                            item.getProduct().getProductId(),
                            (Item) item.clone());
                }
            }
        }
        return deliveredGoods;
    }

    @Override
    public void refreshDeliveries() {
        super.refreshDeliveries();
        if (getOpenDeliveries().isEmpty()) {
            mainDelivered = true;
        } else {
            boolean hasMainDeliveries = false;
            for (int i = 0, j = getItems().size(); i < j; i++) {
                Item next = getItems().get(i);
                if (next.isDeliveryNoteAffecting()) {
                    hasMainDeliveries = true;
                }
            }
            if (hasMainDeliveries) {
                mainDelivered = false;
            } else {
                mainDelivered = true;
            }
        }
    }

    public void removeDelivery(DeliveryNote note) {
        if (log.isDebugEnabled()) {
            log.debug("removeDelivery() invoked [order=" + getId()
                    + ", delivery=" + note.getId() + "]");
        }
        for (Iterator<DeliveryNote> i = getDeliveryNotes().iterator(); i.hasNext();) {
            DeliveryNote next = i.next();
            if (next.getId().equals(note.getId())) {
                if (note.isUnchangeable()) {
                    resetDelivery(note);
                }
                i.remove();
                break;
            }
        }
        this.refreshDeliveries();
    }

    private void resetDelivery(DeliveryNote note) {
        for (int i = 0, j = note.getItems().size(); i < j; i++) {
            Item next = note.getItems().get(i);
            if (next.isDeliveryNoteAffecting()) {
                setMainDelivered(false);
            }
            resetDelivery(next);
        }
        setDelivered(false);
    }

    private void resetDelivery(Item item) {
        double deliveredCount = item.getQuantity();
        for (int i = 0, j = getItems().size(); i < j; i++) {
            if (deliveredCount == 0) {
                break;
            }
            Item next = getItems().get(i);
            if (next.getProduct().getProductId().equals(
                    item.getProduct().getProductId())
                    && next.getDelivered() != 0) {

                if (next.getDelivered() >= deliveredCount) {
                    next.setDelivered(next.getDelivered() - deliveredCount);
                    deliveredCount = 0;
                } else {
                    deliveredCount = deliveredCount - next.getDelivered();
                    next.setDelivered(0d);
                }
            }
        }
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new SalesOrderItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }

    @Override
    protected Item createOptionItem(
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice) {

        return new SalesOrderOptionItemImpl(
                this,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                false,
                false,
                purchasePrice,
                note,
                includePrice,
                null);
    }

    @Override
    protected RecordPaymentAgreement createPaymentAgreement() {
        return new SalesOrderPaymentAgreementImpl(this);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        return new SalesOrderInfoImpl(this, info, user);
    }

    @Override
    protected RecordDiscount createDiscount(
            Employee employee,
            Double discount,
            String note) {
        return new SalesOrderDiscountImpl(this, employee, discount, note);
    }

    private void initKanbanDeliveryFlag(Sales sale) {
        if (sale != null && sale.getType() != null && sale.getType().isDirectSales()) {
            setKanbanDeliveryEnabled(true);
        }
    }

    private void initPaymentAgreement(PaymentAgreement paymentAgreement) {
        if (getPaymentAgreements().isEmpty()) {
            SalesOrderPaymentAgreementImpl obj = new SalesOrderPaymentAgreementImpl(
                    this, paymentAgreement);
            if (obj.getDownpaymentPercent() == null
                    || obj.getDeliveryInvoicePercent() == null
                    || obj.getFinalInvoicePercent() == null) {
                obj.setDeliveryInvoicePercent(0d);
                obj.setDownpaymentPercent(0d);
                obj.setFinalInvoicePercent(1d);
            }
            getPaymentAgreements().add(obj);
        }
    }
}
