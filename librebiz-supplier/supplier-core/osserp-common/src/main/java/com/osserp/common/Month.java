/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 30, 2008 10:23:36 AM 
 * 
 */
package com.osserp.common;

import java.io.Serializable;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Month extends Serializable, Year {

    /**
     * Provides the month of year
     * @return month (0-11)
     */
    Integer getMonth();

    /**
     * Provides the month of year as long value
     * @return month (0-11)
     */
    Long getMonthAsLong();

    /**
     * Provides month and year display
     * @return mm-yyyy
     */
    String getMonthAndYearDisplay();

    /**
     * Provides year and month display
     * @return yyyy-mm
     */
    String getYearAndMonthDisplay();

    /**
     * Provides short year and month display
     * @return yy-mm
     */
    String getShortYearAndMonthDisplay();

    /**
     * Days of month count
     * @return daysOfMonthCount
     */
    int getDaysOfMonthCount();

    /**
     * Days of last month count
     * @return daysOfLastMonthCount
     */
    int getDaysOfLastMonthCount();

    /**
     * Tests if month represented by this object is after month param
     * @param month
     * @return true if after month
     */
    boolean isAfter(Month month);

    /**
     * Tests if month represented by this object is before month param
     * @param month
     * @return true if before month
     */
    boolean isBefore(Month month);

    /**
     * Tests if month represented by this object is same as month param
     * @param month
     * @return true if month and year are equal
     */
    boolean isSame(Month month);
}
