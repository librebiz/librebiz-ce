/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2011 12:24:27 PM 
 * 
 */
package com.osserp.core.model.crm;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.crm.Campaign;
import com.osserp.core.crm.CampaignGroup;
import com.osserp.core.crm.CampaignType;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class CampaignImpl extends AbstractOption implements Campaign {

    private CampaignGroup group = null;
    private CampaignType type = null;
    private Long company = null;
    private Long branch = null;
    private boolean parent = false;
    private Date campaignStarts = null;
    private Date campaignEnds = null;
    private String reach = null;

    protected CampaignImpl() {
        super();
    }

    /**
     * Creates a new campaign
     * @param createdBy
     * @param name required
     * @param reach required
     * @param description
     * @param group
     * @param type
     * @param parent
     * @param company
     * @param branch
     * @param start
     * @param end
     */
    public CampaignImpl(
            Long createdBy,
            String name,
            String reach,
            String description,
            CampaignGroup group,
            CampaignType type,
            Campaign parent,
            Long company,
            Long branch,
            Date start,
            Date end) {
        super((Long) null, (parent == null ? null : parent.getId()), name, description, null, createdBy);
        this.group = group;
        this.type = type;
        this.company = company;
        this.branch = branch;
        this.reach = reach;
        this.campaignStarts = start;
        this.campaignEnds = end;
    }

    public CampaignGroup getGroup() {
        return group;
    }

    public void setGroup(CampaignGroup group) {
        this.group = group;
    }

    public CampaignType getType() {
        return type;
    }

    public void setType(CampaignType type) {
        this.type = type;
    }

    public boolean isParent() {
        return parent;
    }

    public void setParent(boolean parent) {
        this.parent = parent;
    }

    public void updateParent(Campaign parent) {
        if (parent != null) {
            setReference(parent.getId());
        }
    }

    public String getReach() {
        return reach;
    }

    public void setReach(String reach) {
        this.reach = reach;
    }

    public Long getCompany() {
        return company;
    }

    public void setCompany(Long company) {
        this.company = company;
    }

    public Long getBranch() {
        return branch;
    }

    public void setBranch(Long branch) {
        this.branch = branch;
    }

    public Date getCampaignStarts() {
        return campaignStarts;
    }

    public void setCampaignStarts(Date campaignStarts) {
        this.campaignStarts = campaignStarts;
    }

    public Date getCampaignEnds() {
        return campaignEnds;
    }

    public void setCampaignEnds(Date campaignEnds) {
        this.campaignEnds = campaignEnds;
    }

    public boolean isIgnoreCompany() {
        return REACH_COMPANY_UP.equals(reach);
    }

}
