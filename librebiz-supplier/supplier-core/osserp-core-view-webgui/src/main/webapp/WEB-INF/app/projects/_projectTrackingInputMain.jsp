<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="projectTrackingView" />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="type"><fmt:message key="type" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <oc:select name="type" styleClass="form-control" options="projectTrackingTypes" />
        </div>
    </div>
</div>

<v:date name="startDate" styleClass="form-control" label="periodStart" picker="true" />

<v:date name="endDate" styleClass="form-control" label="periodEnd" picker="true" />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="description"><fmt:message key="description" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="name" styleClass="form-control" />
        </div>
    </div>
</div>

