/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 31, 2011 9:41:36 AM 
 * 
 */
package com.osserp.core.calc;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.EntityRelation;

import com.osserp.core.products.Product;

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public interface CalculationTemplateItem extends EntityRelation {

    /**
     * Provides the product of item
     * @return product
     */
    Product getProduct();

    /**
     * Provides the create date of partner and purchase price
     * @return priceDate
     */
    Date getPriceDate();

    /**
     * Provides the partner price
     * @return partnerPrice
     */
    BigDecimal getPartnerPrice();

    /**
     * Indicates that partner price is editable
     * @return partnerPriceEditable
     */
    boolean isPartnerPriceEditable();

    /**
     * Provides the purchase price
     * @return purchasePrice
     */
    BigDecimal getPurchasePrice();

    /**
     * Updates price values
     * @param priceDate
     * @param partnerPrice
     * @param partnerPriceEditable
     * @param purchasePrice
     */
    void update(Date priceDate, Double partnerPrice, boolean partnerPriceEditable, Double purchasePrice);

    /**
     * Provides the note for this item
     * @return note
     */
    String getNote();

    /**
     * Provides the calculation config group id
     * @return calculationConfigGroupId
     */
    Long getCalculationConfigGroupId();

    /**
     * Indicates if product is optional
     * @return optional
     */
    boolean isOptional();

    /**
     * Eanbles/disables optional flag
     * @param optional
     */
    void setOptional(boolean optional);

}
