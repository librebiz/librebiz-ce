/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 22, 2012 9:24:51 PM 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactManager
import com.osserp.core.contacts.ContactSearch
import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ContactPersonsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ContactPersonsView.class.getName())

    Contact related

    ContactPersonsView() {
        dependencies = ['contactView']
        headerName = 'contactPerson'
        providesCreateMode()
        enableAutoreload()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            Long id = form.getLong('id')
            if (id) {
                related = contactManager.find(id)
            }
            if (!related && env.contactView?.bean) {
                related = env.contactView?.bean
            }
            if (!related) {
                throw new ViewContextException(ErrorCode.INVALID_CONTEXT, this)
            }
            reload()
        }
    }


    @Override
    void reload() {
        if (related) {
            list = contactSearch.findContactPersons(related.getContactId())
        }
    }

    @Override
    MenuItem getCreateLink() {
        navigationLink("/contacts/contactCreator/forward?parent=${related?.contactId}&exit=/contacts/contactPersons/reload", 
            'newdataIcon', 'create')
    }

    protected ContactManager getContactManager() {
        getService(ContactManager.class.getName())
    }

    protected ContactSearch getContactSearch() {
        getService(ContactSearch.class.getName())
    }
}
