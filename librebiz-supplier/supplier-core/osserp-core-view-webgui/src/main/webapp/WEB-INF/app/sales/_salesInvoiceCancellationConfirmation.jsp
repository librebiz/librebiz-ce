<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="invoiceNumberShort" />
                <o:out value="${view.record.number}" />
                <span> / </span>
                <o:date value="${view.record.created}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="selectedActionName"> <v:link url="${view.baseLink}/selectPaymentType" title="selectType">
                            <fmt:message key="action" />
                        </v:link>
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <fmt:message key="salesCancellation"/>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="furtherOptionsHeadline"> </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <fmt:message key="furtherOptions" />:
                </div>
            </div>
        </div>

        <v:date name="cancelDate" label="date" picker="true" styleClass="form-control" />

        <c:if test="${view.customNumberEnabled}">
            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="customNumberEnabled"> <fmt:message key="recordNumber" /></label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <v:text name="recordNumber" styleClass="form-control" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    </div>
                </div>
            </div>
        </c:if>

        <div class="row next">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <o:submit value="cancellation" styleClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
