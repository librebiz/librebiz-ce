/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 15, 2008 11:26:13 AM 
 * 
 */
package com.osserp.core.model.products;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.CollectionUtil;
import com.osserp.core.Comparators;
import com.osserp.core.finance.Stock;
import com.osserp.core.planning.ProductPlanning;
import com.osserp.core.planning.SalesPlanningSummary;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductPlanningImpl extends AbstractEntity implements ProductPlanning {
    private Stock stock = null;
    private Date horizon = null;
    private boolean confirmedPurchaseOrdersOnly = false;
    private List<OrderItemsDisplay> openItems = new LinkedList<OrderItemsDisplay>();
    private List<OrderItemsDisplay> openPurchaseItems = new LinkedList<OrderItemsDisplay>();
    private List<OrderItemsDisplay> unreleasedItems = new LinkedList<OrderItemsDisplay>();
    private List<OrderItemsDisplay> vacantItems = new LinkedList<OrderItemsDisplay>();

    /**
     * Default constructor for creating new plannings
     * @param stock
     */
    public ProductPlanningImpl(Stock stock) {
        super(stock.getId());
        this.stock = stock;
    }

    /**
     * Constructor required for object serialization
     */
    protected ProductPlanningImpl() {
        super();
    }

    public boolean isConfirmedPurchaseOrdersOnly() {
        return confirmedPurchaseOrdersOnly;
    }

    public Date getHorizon() {
        return horizon;
    }

    public Stock getStock() {
        return stock;
    }

    public List<OrderItemsDisplay> getOpenItems() {
        return openItems;
    }

    public List<OrderItemsDisplay> getOpenPurchaseItems() {
        return openPurchaseItems;
    }

    public List<OrderItemsDisplay> getUnreleasedItems() {
        return unreleasedItems;
    }

    public List<OrderItemsDisplay> getVacantItems() {
        return vacantItems;
    }

    public List<OrderItemsDisplay> createList(
            Long selectedProduct,
            Long selectedProductCategory,
            Long selectedProductGroup,
            Long selectedProductType,
            boolean listUnreleased,
            boolean listVacant) {

        List<OrderItemsDisplay> list = new java.util.ArrayList<OrderItemsDisplay>();
        if (listUnreleased) {
            list.addAll(unreleasedItems);
        } else {
            List<OrderItemsDisplay> open = openItems;
            list.addAll(open);
            if (listVacant) {
                List<OrderItemsDisplay> vacant = vacantItems;
                list.addAll(vacant);
            }
            CollectionUtil.sort(list, Comparators.createOrderItemsDeliveryDateComparator());
        }
        if (selectedProduct != null) {
            for (Iterator<OrderItemsDisplay> i = list.iterator(); i.hasNext();) {
                OrderItemsDisplay next = i.next();
                if (!next.getProductId().equals(selectedProduct)) {
                    i.remove();
                }
            }
        } else {
            this.removeNoneSelectedCategories(selectedProductCategory, list);
            this.removeNoneSelectedGroups(selectedProductGroup, list);
        }
        this.updatePurchaseOrderCount(list);
        return list;
    }

    public List<SalesPlanningSummary> createSummary(List<OrderItemsDisplay> list) {
        this.updatePurchaseOrderCount(list);
        List<SalesPlanningSummary> result = new ArrayList<SalesPlanningSummary>();
        Set<Long> summaryContent = new HashSet<Long>();

        for (int i = 0, j = list.size(); i < j; i++) {
            OrderItemsDisplay next = list.get(i);
            if (summaryContent.contains(next.getProductId())) {
                for (int k = 0, l = result.size(); k < l; k++) {
                    SalesPlanningSummary existing = result.get(k);
                    if (existing.getProductId().equals(next.getProductId())) {
                        existing.addQuantity(next.getOutstanding());
                        existing.addItem(next);
                        break;
                    }
                }
            } else {
                SalesPlanningSummary sum = new SalesPlanningSummary(
                        next.getStockId(),
                        next.getProductId(),
                        next.getProductGroup(),
                        next.getProductName(),
                        next.getOutstanding(),
                        next.getVacantOrdered(),
                        next.getStock(),
                        next.getReceipt(),
                        next.getExpected(),
                        next.getSalesReceipt());
                sum.addItem(next);
                result.add(sum);
                summaryContent.add(next.getProductId());
            }
        }
        for (int i = 0, j = result.size(); i < j; i++) {
            SalesPlanningSummary next = result.get(i);
            if (next.getExpected() > 0) {
                List<OrderItemsDisplay> pos = getOpenPurchaseByProduct(next.getProductId());
                for (int k = 0, l = pos.size(); k < l; k++) {
                    OrderItemsDisplay pitem = pos.get(k);
                    if (!confirmedPurchaseOrdersOnly || pitem.isDeliveryConfirmed()) {
                        next.addItem(pos.get(k));
                    }
                }
            }
        }
        for (int i = 0, j = result.size(); i < j; i++) {
            SalesPlanningSummary next = result.get(i);
            Double current = next.getStock() + next.getReceipt();
            for (int k = 0, l = next.getItems().size(); k < l; k++) {
                OrderItemsDisplay order = next.getItems().get(k);
                if (order.isSalesItem()) {
                    current = current - order.getOutstanding();
                } else {
                    current = current + order.getOutstanding();
                }
                order.setExpectedStock(current);
                if (current < 0) {
                    next.setLazy(true);
                }
            }
        }
        CollectionUtil.sort(result, Comparators.createProductPlanningByNameComparator());
        return result;
    }

    public boolean isAvailableForDelivery(Long salesId) {
        // TODO check criteria for listVacant and confirmedPurchaseOrdersOnly
        List<SalesPlanningSummary> summary = createSummary(false);
        for (int i = 0, j = summary.size(); i < j; i++) {
            SalesPlanningSummary nextSummary = summary.get(i);
            if (!nextSummary.getItems().isEmpty()) {
                for (int k = 0, l = nextSummary.getItems().size(); k < l; k++) {
                    OrderItemsDisplay nextItem = nextSummary.getItems().get(k);
                    if (nextItem.isSalesItem() && salesId.equals(nextItem.getSalesId())) {
                        if (nextItem.isLazy()) {
                            /*
                             * if (log.isDebugEnabled()) { log.debug("isAvailableForDelivery() reports false on product [" + nextItem.getProductId() + "]"); }
                             */
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public void refresh(
            List<OrderItemsDisplay> openSalesOrderItems,
            List<OrderItemsDisplay> purchaseOrderedItems,
            List<OrderItemsDisplay> unreleasedSalesOrderItems,
            List<OrderItemsDisplay> vacantSalesOrderItems) {
        openItems = openSalesOrderItems;
        openPurchaseItems = purchaseOrderedItems;
        unreleasedItems = unreleasedSalesOrderItems;
        vacantItems = vacantSalesOrderItems;
    }

    public ProductPlanning getCopy(boolean includeConfirmedPurchaseOrdersOnly) {
        return new ProductPlanningImpl(this, includeConfirmedPurchaseOrdersOnly);
    }

    public ProductPlanning getCopy(boolean includeConfirmedPurchaseOrdersOnly, Date planningHorizon) {
        return new ProductPlanningImpl(this, includeConfirmedPurchaseOrdersOnly, planningHorizon);
    }

    /**
     * Creates a new planning by existing planning and planning horizon
     * @param existing
     * @param confirmedPurchaseOrdersOnly
     */
    private ProductPlanningImpl(ProductPlanning existing, boolean confirmedPurchaseOrdersOnly) {
        super();
        this.confirmedPurchaseOrdersOnly = confirmedPurchaseOrdersOnly;
        stock = existing.getStock();
        openItems = createClone(existing.getOpenItems());
        openPurchaseItems = createClone(existing.getOpenPurchaseItems());
        unreleasedItems = createClone(existing.getUnreleasedItems());
        vacantItems = createClone(existing.getVacantItems());
        updatePurchaseOrderCount();
    }

    /**
     * Creates a new planning by existing planning and planning horizon
     * @param existing
     * @param confirmedPurchaseOrdersOnly
     * @param horizon or null for unlimited horizon
     */
    private ProductPlanningImpl(ProductPlanning existing, boolean confirmedPurchaseOrdersOnly, Date horizon) {
        super();
        this.confirmedPurchaseOrdersOnly = confirmedPurchaseOrdersOnly;
        this.horizon = horizon;
        this.stock = existing.getStock();
        if (horizon == null) {
            openItems = createClone(existing.getOpenItems());
            openPurchaseItems = createClone(existing.getOpenPurchaseItems());
            unreleasedItems = createClone(existing.getUnreleasedItems());
            vacantItems = createClone(existing.getVacantItems());
        } else {
            openPurchaseItems = createCloneByHorizon(existing.getOpenPurchaseItems(), horizon);
            openItems = createCloneByHorizon(existing.getOpenItems(), horizon);
            unreleasedItems = createCloneByHorizon(existing.getUnreleasedItems(), horizon);
            vacantItems = createCloneByHorizon(existing.getVacantItems(), horizon);
        }
        updatePurchaseOrderCount();
    }

    private List<OrderItemsDisplay> createClone(List<OrderItemsDisplay> items) {
        List<OrderItemsDisplay> result = new LinkedList<OrderItemsDisplay>();
        for (Iterator<OrderItemsDisplay> i = items.iterator(); i.hasNext();) {
            result.add(new OrderItemsDisplay(i.next()));
        }
        return result;
    }

    private List<OrderItemsDisplay> createCloneByHorizon(List<OrderItemsDisplay> items, Date planningHorizon) {
        List<OrderItemsDisplay> result = new LinkedList<OrderItemsDisplay>();
        for (Iterator<OrderItemsDisplay> i = items.iterator(); i.hasNext();) {
            OrderItemsDisplay next = i.next();
            if (next.getDelivery() != null && next.getDelivery().before(planningHorizon)) {
                result.add(next);
            }
        }
        return result;
    }

    private void updatePurchaseOrderCount() {
        updatePurchaseOrderCount(openItems);
        updatePurchaseOrderCount(vacantItems);
        updatePurchaseOrderCount(unreleasedItems);
        updatePurchaseOrderCount(openPurchaseItems);
    }

    private void updatePurchaseOrderCount(List<OrderItemsDisplay> list) {
        for (Iterator<OrderItemsDisplay> i = list.iterator(); i.hasNext();) {
            OrderItemsDisplay next = i.next();
            next.updateExpected(getPurchaseOrderedCount(next.getProductId()));
        }
    }

    private Double getPurchaseOrderedCount(Long productId) {
        double d = 0;
        for (int i = 0, j = openPurchaseItems.size(); i < j; i++) {
            OrderItemsDisplay next = openPurchaseItems.get(i);
            if (next.getProductId().equals(productId)) {
                if (!confirmedPurchaseOrdersOnly || next.isDeliveryConfirmed()) {
                    d = d + next.getOutstanding();
                }
            }
        }
        return d;
    }

    public List<SalesPlanningSummary> getItems(Long salesId, boolean listVacant) {
        List<SalesPlanningSummary> result = new ArrayList<SalesPlanningSummary>();
        List<SalesPlanningSummary> summary = createSummary(listVacant);
        for (int i = 0, j = summary.size(); i < j; i++) {
            SalesPlanningSummary nextSummary = summary.get(i);
            if (!nextSummary.getItems().isEmpty()) {
                for (int k = 0, l = nextSummary.getItems().size(); k < l; k++) {
                    OrderItemsDisplay nextItem = nextSummary.getItems().get(k);
                    if (nextItem.isSalesItem() && salesId.equals(nextItem.getSalesId())) {
                        result.add(nextSummary);
                        break;
                    }
                }
            }
        }
        return result;
    }

    private List<SalesPlanningSummary> createSummary(boolean listVacant) {
        List<OrderItemsDisplay> list = createList(null, null, null, null, false, listVacant);
        return createSummary(list);
    }

    private void removeNoneSelectedCategories(Long selectedProductCategory, List<OrderItemsDisplay> list) {
        if (selectedProductCategory != null) {
            if (selectedProductCategory > 0) {
                for (Iterator<OrderItemsDisplay> i = list.iterator(); i.hasNext();) {
                    OrderItemsDisplay next = i.next();
                    if (!selectedProductCategory.equals(next.getProductCategory())) {
                        i.remove();
                    }
                }
            }
        }
    }

    private void removeNoneSelectedGroups(Long selectedProductGroup, List<OrderItemsDisplay> list) {
        if (selectedProductGroup != null) {
            if (selectedProductGroup > 0) {
                for (Iterator<OrderItemsDisplay> i = list.iterator(); i.hasNext();) {
                    OrderItemsDisplay next = i.next();
                    if (!next.getProductGroup().equals(selectedProductGroup)) {
                        i.remove();
                    }
                }
            }
        }
    }

    private List<OrderItemsDisplay> getOpenPurchaseByProduct(Long product) {
        List<OrderItemsDisplay> list = new ArrayList<OrderItemsDisplay>();
        for (int i = 0, j = openPurchaseItems.size(); i < j; i++) {
            OrderItemsDisplay next = openPurchaseItems.get(i);
            if (next.getProductId().equals(product)) {
                list.add(next);
            }
        }
        return list;
    }

}
