<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.synchronizeView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
</style>
</tiles:put>
<tiles:put name="javascript" type="string">
<script type="text/javascript">
    function checkSearchException() {
        try {
            if ($('employeeId')) {
                if ($('employeeId').value == 0) {
                    return false;
                }
            }
            activityMessage();
            return true;
        } catch (e) {
            return true;
        }
    }
    
    function activityMessage() {
        try {
            if ($('ajaxContent')) {
                $('ajaxContent').innerHTML = "<div style='width: 100%; text-align: center;'><div style='text-align:center; border:1px solid #D6DDE6; padding:10px;'><img src='/osdb/img/ajax_loader.gif' style='margin:5px;'/><br/>" + ojsI18n.start_synchronisation + ojsI18n.please_wait + "</div></div>";
            }
        } catch (e) {
            //do nothing
        }
    }
</script>
</tiles:put>
<tiles:put name="headline"><fmt:message key="synchronization"/></tiles:put>
<tiles:put name="headline_right">
<ul><li><v:link url="/admin" title="backToMenu"><o:img name="homeIcon"/></v:link></li></ul>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
        <div id="ajaxDiv">
            <div id="ajaxContent">
                <c:import url="/pages/admin/synchronize_ajax.jsp"/>
            </div>
        </div>
    </div>
</div>
</tiles:put> 
</tiles:insert>
