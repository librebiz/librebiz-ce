/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 27 Oct 2011 17:39:55 
 * 
 */
package com.osserp.core.service.sales;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

import com.osserp.core.BusinessType;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.Letters;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Order;
import com.osserp.core.model.SalesRequestImpl;
import com.osserp.core.projects.ProjectFcsManager;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesCreator;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesOfferManager;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * @author tn <tn@osserp.com>
 * 
 */
public class SalesCreatorImpl extends AbstractService implements SalesCreator {
    private static Logger log = LoggerFactory.getLogger(SalesCreatorImpl.class.getName());

    private BranchOffices branchOffices;
    private Letters letters;
    private ProjectFcsManager projectFcsManager;
    private PlatformTransactionManager transactionManager;
    private Projects projects;
    private Requests requests;
    private SalesOfferManager salesOfferManager;
    private SalesOrders salesOrders;

    /**
     * Creates a new salesCreator
     * @param transactionManager
     * @param branchOffices
     * @param projects
     * @param projectFcsManager
     * @param requests
     * @param salesOfferManager
     * @param salesOrders
     * @param letters
     */
    protected SalesCreatorImpl(
            PlatformTransactionManager transactionManager,
            BranchOffices branchOffices,
            Projects projects,
            ProjectFcsManager projectFcsManager,
            Requests requests,
            SalesOfferManager salesOfferManager,
            SalesOrders salesOrders,
            Letters letters) {
        super();
        this.transactionManager = transactionManager;
        this.branchOffices = branchOffices;
        this.projects = projects;
        this.projectFcsManager = projectFcsManager;
        this.requests = requests;
        this.salesOfferManager = salesOfferManager;
        this.salesOrders = salesOrders;
        this.letters = letters;
    }

    public Sales create(final DomainUser user, final Request request, final Long selectedOffer, Date dateConfirmed, String note) throws ClientException {

        final List<SalesOffer> existing = new ArrayList(salesOfferManager.findByReference(user.getEmployee(), request));
        if (existing.isEmpty() && !request.getType().isDirectSales()) {
            throw new ClientException(ErrorCode.OFFER_MISSING);
        }
        Object result = null;
        try {
            result = createSalesInTransaction(user, request, existing, selectedOffer, dateConfirmed, note);
        } catch (Exception e) {
            log.error("create() failed in transaction [message=" + e.getMessage() + ", class=" + e.getClass().getName() + "]", e);
        }
        Sales sales = getSales(result);
        if (log.isDebugEnabled()) {
            log.debug("create() fetched created sales [id=" + sales.getId()
                    + ", status=" + sales.getStatus()
                    + ", request=" + sales.getRequest().getRequestId()
                    + ", requestStatus=" + sales.getRequest().getStatus()
                    + "]");
        }
        if (letters != null) {
            try {
                letters.updateReferences(
                        sales.getRequest().getCustomer().getId(),
                        sales.getType().getRequestLetterType(),
                        sales.getRequest().getRequestId(),
                        sales.getType().getSalesLetterType(),
                        sales.getId());
            } catch (Exception e) {
                log.warn("create() ignoring exception on attempt to transfer letters [sales=" + sales.getId()
                        + ", message=" + e.getMessage() + "]", e);

            }
        }
        if (projectFcsManager != null) {
            projectFcsManager.createInitialEvents(sales);
        }
        return sales;
    }

    private Object createSalesInTransaction(final DomainUser user, final Request request, final List<SalesOffer> existing, final Long selectedOffer,
            final Date dateConfirmed, final String note) {
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        Object result = transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction(TransactionStatus transactionStatus) {
                try {
                    // set selected offer, unset previous 
                    SalesOffer selected = null;
                    for (int i = 0, j = existing.size(); i < j; i++) {
                        SalesOffer next = existing.get(i);
                        if (!next.getId().equals(selectedOffer)) {
                            salesOfferManager.setHistorical(next);
                        } else {
                            selected = next;
                        }
                    }
                    if (selected == null && !request.getType().isDirectSales()) {
                        throw new ClientException(ErrorCode.OFFER_ACTIVATION);
                    }
                    if (selected != null) {
                        selected.updateStatus(SalesOffer.STAT_CLOSED);
                        salesOfferManager.persist(selected);
                        if (log.isDebugEnabled()) {
                            log.debug("createSalesInTransaction() offer closed [id=" + selected.getId() + "]");
                        }
                    }
                    Sales sales = projects.create(user.getEmployee().getId(), request);
                    Order order = salesOrders.create(
                            user.getEmployee(),
                            sales,
                            sales.getType().getCalculatorName(),
                            selected,
                            dateConfirmed,
                            note);
                    if (log.isDebugEnabled()) {
                        log.debug("createSalesInTransaction() order created [id=" + order.getId() + ", sales=" + sales.getId() + "]");
                    }
                    return sales.getId();
                } catch (Exception e) {
                    log.error("createSalesInTransaction() failed [message=" + e.getMessage() + ", type=" + e.getClass().getName() + "]", e);
                    transactionStatus.setRollbackOnly();
                    if (e instanceof ClientException) {
                        return e;
                    }
                    return new ClientException(ErrorCode.UNKNOWN);
                }
            }
        });
        return result;
    }

    public Sales create(final DomainUser user, final Employee salesPerson, final Sales related, final BusinessType type, final String name)
            throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [user="
                    + user.getId()
                    + ", relatedSales=" + related.getId()
                    + ", salesPerson=" + (salesPerson == null ? "null" : salesPerson.getId())
                    + ", type=" + (type == null ? "null" : type.getId())
                    + ", name=" + name
                    + "]");
        }
        if (related == null) {
            throw new BackendException("related sales must not be null");
        }
        if (type == null) {
            throw new ClientException(ErrorCode.TYPE_MISSING);
        }
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        Object result = transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction(TransactionStatus transactionStatus) {
                try {
                    Request request = new SalesRequestImpl(user, type, related.getRequest().getCustomer());
                    if (log.isDebugEnabled()) {
                        log.debug("create() empty request created [name=" + request.getName() + "]");
                    }
                    if (name != null && name.length() > 0) {
                        request.setName(name);
                        if (log.isDebugEnabled()) {
                            log.debug("create() request updated [name=" + request.getName() + "]");
                        }
                    }
                    if (type.getCompany().equals(related.getBranch().getCompany().getId())) {
                        request.updateSales(salesPerson, related.getBranch());
                    } else {
                        BranchOffice defaultBranch = null;
                        List<BranchOffice> offices = branchOffices.getByCompany(type.getCompany());
                        for (int i = 0, j = offices.size(); i < j; i++) {
                            defaultBranch = offices.get(i);
                            if (defaultBranch.isHeadquarter()) {
                                break;
                            }
                        }
                        if (defaultBranch == null) {
                            throw new ClientException(ErrorCode.BRANCH_MISSING);
                        }
                        request.updateSales(salesPerson, defaultBranch);
                    }
                    requests.save(request);
                    Sales sales = projects.create(user.getEmployee().getId(), request);
                    sales.setParentId(related.getId());
                    projects.save(sales);
                    Order order = salesOrders.create(
                            user.getEmployee(), sales, type.getCalculatorName(), null, null, null);
                    if (log.isDebugEnabled()) {
                        log.debug("create() sales created [id=" + sales.getId() 
                            + ", request=" + sales.getRequest().getRequestId() 
                            + ", order=" + order.getId() + "]");
                    }
                    return sales.getId();

                } catch (Exception e) {
                    log.error("create() failed [message=" + e.getMessage() + ", type=" + e.getClass().getName() + "]", e);
                    transactionStatus.setRollbackOnly();
                    if (e instanceof ClientException) {
                        return e;
                    }
                    return new ClientException(ErrorCode.UNKNOWN);
                }
            }
        });
        Sales sales = getSales(result);
        if (projectFcsManager != null) {
            projectFcsManager.createInitialEvents(sales);
        }
        return sales;
    }

    private Sales getSales(Object result) throws ClientException {
        if (result instanceof Long) {
            return projects.load((Long) result);
        }
        if (result instanceof ClientException) {
            throw ((ClientException) result);
        }
        if (result instanceof Sales) {
            return projects.load(((Sales) result).getId());
        }
        throw new ClientException(ErrorCode.UNKNOWN);
    }
}
