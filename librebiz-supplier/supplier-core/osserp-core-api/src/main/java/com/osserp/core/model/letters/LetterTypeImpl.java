/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 9, 2008 11:27:38 PM 
 * 
 */
package com.osserp.core.model.letters;

import com.osserp.core.dms.LetterType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LetterTypeImpl extends AbstractLetterComponent implements LetterType {

    private String infoKeys = null;
    private String parameterKeys = null;
    private String clientContext = null;
    private String contactSearch = null;
    private Long contactType = null;
    private String stylesheetName = null;
    private boolean disabled;
    private boolean autoselect;

    protected LetterTypeImpl() {
        super();
    }

    protected LetterTypeImpl(LetterType other) {
        super((AbstractLetterComponent) other);
        infoKeys = other.getInfoKeys();
        parameterKeys = other.getParameterKeys();
        clientContext = other.getClientContext();
        contactSearch = other.getContactSearch();
        contactType = other.getContactType();
        stylesheetName = other.getStylesheetName();
        autoselect = other.isAutoselect();
        disabled = other.isDisabled();
    }

    public String getInfoKeys() {
        return infoKeys;
    }

    protected void setInfoKeys(String infoKeys) {
        this.infoKeys = infoKeys;
    }

    public String getParameterKeys() {
        return parameterKeys;
    }

    protected void setParameterKeys(String parameterKeys) {
        this.parameterKeys = parameterKeys;
    }

    public String getClientContext() {
        return clientContext;
    }

    protected void setClientContext(String clientContext) {
        this.clientContext = clientContext;
    }

    public String getContactSearch() {
        return contactSearch;
    }

    protected void setContactSearch(String contactSearch) {
        this.contactSearch = contactSearch;
    }

    public Long getContactType() {
        return contactType;
    }

    protected void setContactType(Long contactType) {
        this.contactType = contactType;
    }

    public String getStylesheetName() {
        return stylesheetName;
    }

    public void setStylesheetName(String stylesheetName) {
        this.stylesheetName = stylesheetName;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isAutoselect() {
        return autoselect;
    }

    public void setAutoselect(boolean autoselect) {
        this.autoselect = autoselect;
    }

}
