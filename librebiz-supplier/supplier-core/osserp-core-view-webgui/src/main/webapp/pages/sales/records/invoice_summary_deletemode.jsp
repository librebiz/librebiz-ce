<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:set var="record" value="${sessionScope.recordView.record}" />

<c:set var="paymentsRelated" value="false" scope="page" />
<c:if test="${!empty record.payments && !record.downpayment}">
    <c:forEach var="payment" items="${record.payments}" varStatus="s">
        <c:if test="${payment.recordId == record.id}">
            <%-- payment is relating to the invoice => record not deleteable --%>
            <c:set var="paymentsRelated" value="true" scope="page" />
        </c:if>
    </c:forEach>
</c:if>

<table class="recordSummary">
    <tr>
        <td class="phead">
            <fmt:message key="invoiceAmount" />
            <span class="desc"> (<fmt:message key="netTurnoverTaxGross" />)</span>
        </td>
        <c:choose>
            <c:when test="${record.downpayment}">
                <td class="pnet"><o:number value="${record.amount}" format="currency" /></td>
                <td class="ptax"><o:number value="${record.taxAmount}" format="currency" /></td>
                <td class="pamount"><o:number value="${record.grossAmount}" format="currency" /></td>
            </c:when>
            <c:otherwise>
                <td class="pnet"><o:number value="${record.amounts.amount}" format="currency" /></td>
                <td class="ptax"><o:number value="${record.amounts.taxAmount}" format="currency" /></td>
                <td class="pamount"><o:number value="${record.amounts.grossAmount}" format="currency" /></td>
            </c:otherwise>
        </c:choose>
        <td class="paction"><c:if test="${empty record.payments or (!record.downpayment and paymentsRelated != 'true')}">
                <a href="<c:url value="/salesInvoice.do"><c:param name="method" value="deleteInvoice"/></c:url>"> <img src="<c:url value="${applicationScope.deleteIcon}"/>" class="smallicon" />
                </a>
            </c:if></td>
    </tr>
    <c:if test="${!empty record.payments}">
        <tr>
            <td colspan="4" class="pspace"></td>
        </tr>
        <c:forEach var="payment" items="${record.payments}" varStatus="s">
            <tr>
                <c:choose>
                    <c:when test="${s.index == 0}">
                        <td colspan="2" class="payment"><fmt:message key="payments" /></td>
                    </c:when>
                    <c:otherwise>
                        <td colspan="2" class="payment"></td>
                    </c:otherwise>
                </c:choose>
                <td class="pdate"><o:date value="${payment.paid}" /></td>
                <td class="pamount"><c:choose>
                        <c:when test="${record.id == payment.recordId}">
                            <span class="error" title="<fmt:message key="deletePaymentFirst"/>"> <o:number value="${payment.amount}" format="currency" />
                            </span>
                        </c:when>
                        <c:otherwise>
                            <o:number value="${payment.amount}" format="currency" />
                        </c:otherwise>
                    </c:choose></td>
                <td class="paction"></td>
            </tr>
        </c:forEach>
    </c:if>
    <tr>
        <td colspan="4" class="pspace"></td>
    </tr>
    <tr>
        <td colspan="2" class="payment"><fmt:message key="openAmount" /></td>
        <td class="pdate"><o:date current="true" /></td>
        <td class="pamount"><o:number value="${record.dueAmount}" format="currency" /></td>
        <td class="paction"></td>
    </tr>
    <tr>
        <td colspan="4" class="plast"></td>
    </tr>
</table>
