/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 03, 2009 13:56:37 PM 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.Date;

import com.osserp.common.Calendar;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.DateUtil;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecordMarker;
import com.osserp.core.hrm.TimeRecordMarkerType;

/**
 * 
 * @author jg <jg@osserp.com>
 * 
 */
public class TimeRecordMarkerImpl extends AbstractEntity implements TimeRecordMarker {

    private TimeRecordMarkerType type = null;
    private Date markerDate = null;
    private Double markerValue = null;
    private String note = null;

    protected TimeRecordMarkerImpl() {
        super();
    }

    public TimeRecordMarkerImpl(
            TimeRecordMarkerType type,
            Long reference,
            Employee user,
            Date markerDate,
            Double markerValue,
            String note) {
        super(null, reference, DateUtil.getCurrentDate(), user.getId());
        this.type = type;
        this.note = note;
        this.markerValue = markerValue;
        this.markerDate = (markerDate == null ? DateUtil.getCurrentDate() : markerDate);
    }

    protected TimeRecordMarkerImpl(
            TimeRecordMarkerType type,
            Long reference,
            Long user,
            Date markerDate,
            Double markerValue,
            String note) {
        super(null, reference, DateUtil.getCurrentDate(), user);
        this.type = type;
        this.note = note;
        this.markerValue = markerValue;
        this.markerDate = (markerDate == null ? DateUtil.getCurrentDate() : markerDate);
    }

    public TimeRecordMarkerType getType() {
        return type;
    }

    protected void setType(TimeRecordMarkerType type) {
        this.type = type;
    }

    public Double getMarkerValue() {
        return markerValue;
    }

    protected void setMarkerValue(Double markerValue) {
        this.markerValue = markerValue;
    }

    public Date getMarkerDate() {
        return markerDate;
    }

    protected void setMarkerDate(Date markerDate) {
        this.markerDate = markerDate;
    }

    public String getNote() {
        return note;
    }

    protected void setNote(String note) {
        this.note = note;
    }

    public boolean isCurrentDay() {
        return DateUtil.isToday(markerDate);
    }

    public boolean isMemberOf(int year) {
        int thisYear = DateUtil.getYear(markerDate);
        return (thisYear == year);
    }

    public boolean isMemberOf(int month, int year) {
        int thisMonth = DateUtil.getMonth(markerDate);
        return (thisMonth == month && isMemberOf(year));
    }

    public boolean isDay(Calendar day) {
        if (isMemberOf(day.getMonth(), day.getYear())) {
            return day.getDay().equals(DateUtil.getDay(markerDate));
        }
        return false;
    }

}
