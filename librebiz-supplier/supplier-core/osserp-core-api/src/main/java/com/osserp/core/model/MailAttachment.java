/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.model;


import com.osserp.common.beans.AbstractEntity;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class MailAttachment extends AbstractEntity {

    private String contentID;
    private String contentType;
    private String fileName;
    private String fileStored;
    private String fileType;
    private long fileSize;
    private boolean ignorable;
    private int orderId;

    protected MailAttachment() {
        super();
    }

    public MailAttachment(
            Long reference,
            String contentID,
            String contentType,
            String fileName,
            String fileStored,
            String fileType,
            long fileSize,
            boolean ignorable,
            int orderId) {
        super((Long) null, reference);
        this.contentID = contentID;
        this.contentType = contentType;
        this.fileName = fileName;
        this.fileStored = fileStored;
        this.fileSize = fileSize;
        this.fileType = fileType;
        this.ignorable = ignorable;
        this.orderId = orderId;
    }

    public String getContentID() {
        return contentID;
    }

    public void setContentID(String contentID) {
        this.contentID = contentID;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileStored() {
        return fileStored;
    }

    public void setFileStored(String fileStored) {
        this.fileStored = fileStored;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public boolean isIgnorable() {
        return ignorable;
    }

    public void setIgnorable(boolean ignorable) {
        this.ignorable = ignorable;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

}
