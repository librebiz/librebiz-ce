<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesOfferView}">
<o:logger write="offer_info_edit.jsp no view found" level="warn"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesOfferView}"/>
<c:set var="record" value="${sessionScope.recordView.record}"/>
<c:set var="conditionCommand" scope="request" value="/salesOfferInfos.do"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>

<tiles:put name="headline">
<c:import url="/pages/records/record_headline_edit.jsp"/>
</tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/salesOfferInfos.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string"> 
<c:import url="/pages/records/record_conditions_edit.jsp"/>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
