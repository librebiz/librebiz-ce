/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22.08.2015 
 * 
 */
package com.osserp.core.model.records;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.finance.BillingClass;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BillingClassImpl extends AbstractOption implements BillingClass {

    private boolean payment = false;
    private boolean outflowOfCash = false;
    private String contextName = null;

    /**
     * Default constructor required by Serializable.
     */
    protected BillingClassImpl() {
        super();
    }
    
    /**
     * Internal object based constructor used by clone.
     */
    protected BillingClassImpl(BillingClass vo) {
        super(vo);
        payment = vo.isPayment();
        outflowOfCash = vo.isOutflowOfCash();
    }

    /**
     * Constructor for new billing classes.
     * @param name
     * @param createdBy
     * @param payment
     * @param outflowOfCash
     * @param contextName
     */
    public BillingClassImpl(
            String name, 
            Long createdBy, 
            boolean payment, 
            boolean outflowOfCash,
            String contextName) {
        super(name, createdBy);
        this.payment = payment;
        this.outflowOfCash = outflowOfCash;
        this.contextName = contextName;
    }
    
    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public boolean isPayment() {
        return payment;
    }
    
    public void setPayment(boolean payment) {
        this.payment = payment;
    }
    
    public boolean isOutflowOfCash() {
        return outflowOfCash;
    }
    
    public void setOutflowOfCash(boolean outflowOfCash) {
        this.outflowOfCash = outflowOfCash;
    }

    @Override
    public Object clone() {
        return new BillingClassImpl(this);
    }
    
}
