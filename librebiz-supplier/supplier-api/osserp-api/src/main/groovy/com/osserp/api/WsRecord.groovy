/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Sep 7, 2017 
 * 
 */
package com.osserp.api

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Entity
@Table(name='ws_record')
class WsRecord extends FinanceRecord {
    private static final long serialVersionUID = 4L

    WsRecord() {
        super()
    }

    WsRecord(Long contactId, Long typeId, Long paymentId, Long shippingId) {
        super()
        this.contactId = contactId    
        this.typeId = typeId
        this.paymentId = paymentId    
        this.shippingId = shippingId    
    }

    @Column(name='contact_id')
    Long contactId

    @Column(name='type_id')
    Long typeId

    @OneToMany(mappedBy = 'wsRecord', cascade = CascadeType.ALL, orphanRemoval = true)
    List<WsRecordItem> items = []

    void addItem(WsProduct product, Double quantity, BigDecimal price, String note) {
        WsRecordItem item = new WsRecordItem(this, product, quantity, price, note)
        items << item
    }

    void deleteItem(WsRecordItem item) {
        for (Iterator i = items.iterator(); i.hasNext();) {
            WsRecordItem nextItem = i.next()
            if (nextItem.primaryKey == item.primaryKey) {
                i.remove()
            }
        }
    }
}
