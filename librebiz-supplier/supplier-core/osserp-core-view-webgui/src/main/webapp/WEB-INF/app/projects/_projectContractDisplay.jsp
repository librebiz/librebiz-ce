<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="project" value="${view.businessCase}" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="contractDetails" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:if test="${!empty view.bean.type}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="displayName"><fmt:message key="type" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.bean.type.name}" />
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty view.bean.status}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="status"><fmt:message key="status" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.bean.status.name}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!empty view.bean.contractStart}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="termStart"><fmt:message key="termStart" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.bean.contractStart}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!empty view.bean.monthsRunning}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="term"><fmt:message key="term" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.bean.monthsRunning == 0}">
                                    <fmt:message key="undefinedEndDate" />
                                </c:when>
                                <c:otherwise>
                                    <o:out value="${view.bean.monthsRunning}" />
                                    <fmt:message key="months" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!empty view.bean.billingDays}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="billingInterval"><fmt:message key="billingInterval" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.bean.billingDays == 31}">
                                    <fmt:message key="monthly" />
                                </c:when>
                                <c:otherwise>
                                    <o:out value="${view.bean.billingDays}" />
                                    <fmt:message key="daysLabel" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </c:if>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recording"><fmt:message key="hoursRecording" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.trackingRequired}">
                                <fmt:message key="yes" />
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="no" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
            <c:if test="${view.bean.trackingRequired and !empty view.bean.trackingTypeDefault}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recordingType"><fmt:message key="recording" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:options name="projectTrackingTypes" value="${view.bean.trackingTypeDefault}" />
                        </div>
                    </div>
                </div>
            </c:if>

        </div>
    </div>
</div>
