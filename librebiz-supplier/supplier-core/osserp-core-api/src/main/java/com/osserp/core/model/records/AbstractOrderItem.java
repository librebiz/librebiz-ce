/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 2 Mar 2007 09:17:33 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.util.DateFormatter;

import com.osserp.core.Item;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.Record;
import com.osserp.core.model.ItemImpl;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOrderItem extends ItemImpl implements OrderItem {

    /**
     * Creates an empty item.
     */
    protected AbstractOrderItem() {
        super();
    }

    /**
     * Creates a new item by another item
     * @param item
     */
    protected AbstractOrderItem(Item item) {
        super(item);
    }

    /**
     * Creates a new item
     * @param reference
     * @param stockId
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param priceDate
     * @param partnerPrice
     * @param partnerPriceEditable
     * @param partnerPriceOverridden
     * @param purchasePrice
     * @param taxRate
     * @param note
     * @param includePrice
     * @param delivery
     * @param orderId
     * @param externalId
     */
    protected AbstractOrderItem(
            Record reference,
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            boolean includePrice,
            Date delivery,
            int orderId,
            Long externalId) {
        super(
                reference,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                orderId,
                externalId);
        setDelivery(delivery);
    }

    @Override
    public boolean isProvidingDeliveryDate() {
        return true;
    }

    @Override
    public Element getXML(boolean formatValues) {
        Element result = super.getXML(formatValues);
        if (getDelivery() != null) {
            result.addContent(new Element("delivery").setText(
                    DateFormatter.getDate(getDelivery())));
        } else {
            result.addContent(new Element("delivery"));
        }
        if (getDeliveryNote() != null) {
            result.addContent(new Element("deliveryNote").setText(getDeliveryNote()));
        } else {
            result.addContent(new Element("deliveryNote"));
        }
        return result;
    }
}
