/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 1, 2010 11:14:27 AM 
 * 
 */
package com.osserp.core.telephone;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneConfiguration extends AbstractTelephoneConfiguration {

    /**
     * Returns the telephone
     * @return telephone
     */
    Telephone getTelephone();

    /**
     * Sets the telephone
     * @param telephone
     */
    void setTelephone(Telephone telephone);

    /**
     * Returns the employeeId
     * @return employeeId
     */
    Long getEmployeeId();

    /**
     * Returns the internal
     * @return internal
     */
    String getInternal();

    /**
     * Sets the internal
     * @param internal
     */
    void setInternal(String internal);

    /**
     * Indicates that internal is editable
     * @return internal
     */
    boolean isInternalEditable();
}
