/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13-Feb-2007 13:49:00 
 * 
 */
package com.osserp.common.beans;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractClass extends Object {

    /**
     * Default constructor required by Serializable
     */
    protected AbstractClass() {
        super();
    }

    /**
     * Provides a map if implementing class requires to implement {@link com.osserp.common.Mappable#getMapped()}
     * @return new initialized map
     */
    protected Map<String, Object> getMapped() {
        return new HashMap<String, Object>();
    }

    /**
     * Adds a value to a map only if value is not null
     * @param map
     * @param key
     * @param value
     */
    protected void putIfExists(Map<String, Object> map, String key, Object value) {
        if (map != null && value != null) {
            map.put(key, value);
        }
    }

    /**
     * Adds a value to a map only if value is not null
     * @param map
     * @param key
     * @param value
     */
    protected void putFormatted(Map<String, Object> map, String key, Date value) {
        if (map != null && value != null) {
            map.put(key, DateFormatter.getDateAndTime(value));
        }
    }

    /**
     * Creates a date by string value
     * @param value
     * @return date for valid strings or null in any other case including exceptions
     */
    protected Date fetchDate(String value) {
        try {
            return DateUtil.createDate(value, false);
        } catch (Exception c) {
            return null;
        }
    }

    /**
     * Creates a date by string value
     * @param value
     * @return date for valid strings or null in any other case including exceptions
     * @throws ClientException if number is null or invalid format
     */
    protected Date createDate(String value) throws ClientException {
        try {
            return DateUtil.createDate(value, false);
        } catch (Exception c) {
            throw new ClientException(ErrorCode.NUMBER_EXPECTED);
        }
    }

    /**
     * Creates a decimal by a string throwing a checked exception if fails
     * @param number
     * @return value or 0
     */
    protected BigDecimal createDecimal(String number) {
        try {
            return NumberUtil.createDecimal(number);
        } catch (Throwable t) {
        }
        return new BigDecimal(0);
    }

    /**
     * Parses a string for decimal value while ignoring null values.
     * @param number
     * @return value or null if number is null or invalid format
     */
    protected BigDecimal parseDecimal(String number) {
        if (number != null) {
            return NumberUtil.fetchDecimal(number);
        }
        return null;
    }

    /**
     * Creates a double by a string throwing a checked exception if fails
     * @param number
     * @return value
     * @throws ClientException if number is null or invalid format
     */
    protected Double createDouble(String number) throws ClientException {
        try {
            return Double.valueOf(number);
        } catch (Throwable t) {
            throw new ClientException(ErrorCode.NUMBER_EXPECTED);
        }
    }

    /**
     * Parses a string for double value while ignoring null values.
     * @param number
     * @return value or null if number is null or invalid format
     */
    protected Double parseDouble(String number) {
        if (number != null) {
            try {
                return Double.valueOf(number);
            } catch (NumberFormatException e) {
            }
        }
        return null;
    }

    /**
     * Creates a long by a string throwing a checked exception if fails
     * @param number
     * @return value
     * @throws ClientException if number is null or invalid format
     */
    protected Long createLong(String number) throws ClientException {
        try {
            return Long.valueOf(number);
        } catch (Throwable t) {
            throw new ClientException(ErrorCode.NUMBER_EXPECTED);
        }
    }

    /**
     * Parses a string for long value while ignoring null values.
     * @param number
     * @return value or null if number is null or invalid format
     */
    protected Long parseLong(String number) {
        if (number != null) {
            try {
                return Long.valueOf(number);
            } catch (NumberFormatException e) {
            }
        }
        return null;
    }

    /**
     * Creates an integer by a string throwing a checked exception if fails
     * @param s
     * @return value
     * @throws ClientException if number is null or invalid format
     */
    protected Integer createInteger(String s) throws ClientException {
        try {
            return Integer.valueOf(s);
        } catch (Throwable t) {
            throw new ClientException(ErrorCode.NUMBER_EXPECTED);
        }
    }

    /**
     * Parses a string for integer value while ignoring null values.
     * @param number
     * @return value or null if number is null or invalid format
     */
    protected Integer parseInteger(String number) {
        if (number != null) {
            try {
                return Integer.valueOf(number);
            } catch (NumberFormatException e) {
            }
        }
        return null;
    }

    protected String createString(Date date) {
        return DateFormatter.getDate(date);
    }

    protected String createDateForSheet(Date date) {
        return DateFormatter.getISO(date);
    }

    protected String createString(Double db) {
        return NumberFormatter.getValue(db, NumberFormatter.CURRENCY);
    }

    protected String createCurrencyForSheet(Double db) {
        return NumberFormatter.getValue(db, NumberFormatter.SHEET, 2);
    }

    protected String createString(Integer i) {
        return (i == null) ? "" : i.toString();
    }

    protected String createString(Long l) {
        return (l == null) ? "" : l.toString();
    }

    protected String createString(String s) {
        return (s == null) ? "" : s;
    }

    protected boolean createBoolean(int i) {
        return (i > 0);
    }

    protected String[] getStringArray(String s) {
        if (s == null || s.length() < 1) {
            return null;
        }
        StringBuffer buffer = new StringBuffer(s);
        StringUtil.deleteBlanks(buffer);
        return StringUtil.getTokenArray(buffer.toString(), ",");
    }

    protected boolean isEmpty(Collection col) {
        return (col == null || col.isEmpty());
    }

    /**
     * Checks if value is null or 0
     * @param s
     * @return true if so
     */
    protected boolean isNotSet(BigDecimal s) {
        return (s == null || s.doubleValue() == 0);
    }

    /**
     * Checks if given string is null or empty
     * @param s
     * @return true if so
     */
    protected boolean isNotSet(String s) {
        return (s == null || s.length() < 1);
    }

    /**
     * Checks if given string is null or empty
     * @param array
     * @return true if so
     */
    protected boolean isNotSet(String[] array) {
        return (array == null || array.length < 1);
    }

    /**
     * Checks wether given long is null or it's value is 0
     * @param l
     * @return true if so
     */
    protected boolean isNotSet(Long l) {
        return (l == null || l == 0);
    }

    /**
     * Checks wether given double is null or it's value is 0
     * @param d
     * @return true if so
     */
    protected boolean isNotSet(Double d) {
        return (d == null || d == 0);
    }

    /**
     * Checks wether collection is null or empty
     * @param col
     * @return true if so
     */
    protected boolean isNotSet(Collection col) {
        return (col == null || col.isEmpty());
    }

    /**
     * Checks wether given integer is null or it's value is 0
     * @param i
     * @return true if so
     */
    protected boolean isNotSet(Integer i) {
        return (i == null || i == 0);
    }

    /**
     * Checks wether big decimal neither null nor 0
     * @param bd
     * @return true if so
     */
    protected boolean isSet(BigDecimal bd) {
        return (bd != null && bd.doubleValue() != 0);
    }

    /**
     * Checks wether given string neither null nor empty
     * @param s
     * @return true if so
     */
    protected boolean isSet(String s) {
        return (s != null && s.length() > 0);
    }

    /**
     * Checks wether given array is neither null nor empty
     * @param array
     * @return true if so
     */
    protected boolean isSet(String[] array) {
        return (array != null && array.length > 0);
    }

    /**
     * Checks wether given long neither null nor 0
     * @param l
     * @return true if so
     */
    protected boolean isSet(Long l) {
        return (l != null && l != 0);
    }

    /**
     * Checks wether given double neither null nor 0
     * @param d
     * @return true if so
     */
    protected boolean isSet(Double d) {
        return (d != null && d != 0);
    }

    /**
     * Checks wether given integer neither null nor 0
     * @param i
     * @return true if so
     */
    protected boolean isSet(Integer i) {
        return (i != null && i != 0);
    }

    /**
     * Savely big decimal fetching
     * @param value
     * @return value or 0 if null
     */
    protected BigDecimal fetchBigDecimal(BigDecimal value) {
        return (value == null) ? new BigDecimal(0) : value;
    }

    /**
     * Savely double fetching
     * @param value
     * @return value or 0 if null
     */
    protected Double fetchDouble(Double value) {
        return (value == null ? 0D : value);
    }

    /**
     * Tries to fetch a boolean from a map
     * @param map containing value
     * @param key of the value
     * @return fetched value or false
     */
    protected boolean fetchBoolean(Map map, String key) {
        if (map.containsKey(key)) {
            Object value = map.get(key);
            if (value != null) {
                if (value instanceof Boolean) {
                    return (Boolean) value;
                }
                if (value instanceof String) {
                    return Boolean.valueOf((String) value);
                }
            }
        }
        return false;
    }

    /**
     * Tries to fetch an integer from a map
     * @param map containing value
     * @param key of the value
     * @return fetched value or null
     */
    protected Integer fetchInteger(Map map, String key) {
        if (map.containsKey(key)) {
            Object value = map.get(key);
            if (value != null) {
                if (value instanceof Integer) {
                    return (Integer) value;
                }
                if (value instanceof Long) {
                    return ((Long) value).intValue();
                }
                if (value instanceof String) {
                    try {
                        return Integer.valueOf((String) value);
                    } catch (NumberFormatException nfe) {
                        // may be
                    }
                }
            }
        }
        return null;
    }

    /**
     * Tries to fetch a long from a map
     * @param map containing value
     * @param key of the value
     * @return fetched value or null
     */
    protected Long fetchLong(Map map, String key) {
        if (map.containsKey(key)) {
            Object value = map.get(key);
            if (value != null) {
                if (value instanceof Long) {
                    return (Long) value;
                }
                if (value instanceof Integer) {
                    return (Long) value;
                }
                if (value instanceof String) {
                    try {
                        return Long.valueOf((String) value);
                    } catch (NumberFormatException nfe) {
                        // may be
                    }
                }
            }
        }
        return null;
    }

    /**
     * Tries to fetch a date from a map
     * @param map containing value
     * @param key of the value
     * @return fetched date or null
     */
    protected Date fetchDate(Map map, String key) {
        if (map.containsKey(key)) {
            Object value = map.get(key);
            if (value != null) {
                if (value instanceof Date) {
                    return (Date) value;
                }
                if (value instanceof Long) {
                    return new Date(((Long) value));
                }
                if (value instanceof Integer) {
                    return new Date(((Integer) value).longValue());
                }
                if (value instanceof String) {
                    return fetchDate((String) value);
                }
            }
        }
        return null;
    }

    /**
     * Tries to fetch a double from a map
     * @param map containing value
     * @param key of the value
     * @return fetched double, null if not found or a double with 0 value if unparseable string value found
     */
    protected Double fetchDouble(Map map, String key) {
        if (map.containsKey(key)) {
            Object value = map.get(key);
            if (value != null) {
                if (value instanceof Double) {
                    return (Double) value;
                }
                if (value instanceof Long) {
                    return ((Long) value).doubleValue();
                }
                if (value instanceof Integer) {
                    return ((Integer) value).doubleValue();
                }
                if (value instanceof String) {
                    try {
                        return Double.valueOf((String) value);
                    } catch (NumberFormatException nfe) {
                        return NumberUtil.createDouble((String) value);
                    }
                }
            }
        }
        return 0D;
    }

    /**
     * Tries to fetch a map from a map
     * @param map containing value
     * @param key of the value
     * @return fetched or empty map
     */
    protected Map fetchMap(Map map, String key) {
        if (map.containsKey(key)) {
            Object value = map.get(key);
            if (value != null && value instanceof Map) {
                return (Map) value;
            }
        }
        return new HashMap<>();
    }

    /**
     * Tries to fetch a string from a map
     * @param map containing value
     * @param key of the value
     * @return fetched string or null
     */
    protected String fetchString(Map map, String key) {
        if (map.containsKey(key)) {
            Object value = map.get(key);
            if (value != null && value instanceof String) {
                return (String) value;
            }
        }
        return null;
    }

    protected boolean valueChanged(String src, String target) {
        if (isNotSet(src) && isNotSet(target)) {
            return false;
        }
        if (isNotSet(src) || isNotSet(target)) {
            return true;
        }
        return !src.equals(target);
    }

    protected boolean valueChanged(Long src, Long target) {
        if (isNotSet(src) && isNotSet(target)) {
            return false;
        }
        if (isNotSet(src) || isNotSet(target)) {
            return true;
        }
        return !src.equals(target);
    }

    protected boolean valueChanged(Date src, Date target) {
        if (src == null && target == null) {
            return false;
        }
        if (src == null || target == null) {
            return true;
        }
        return !src.equals(target);
    }
    
    public String objectToString(Object obj) {
        return StringUtil.toString(obj);
    }

    @Override
    public String toString() {
        return StringUtil.toString(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
