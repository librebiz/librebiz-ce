/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2017 
 * 
 */
package com.osserp.core.dao;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.contacts.Salutation;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SystemSetup {
    
    /**
     * Provides the expected setup user. 
     * @param admin same as result if everything is ok 
     * @return expected admin user
     * @throws ClientException on invalid application state or wrong admin user  
     */
    DomainUser getExpectedUser(DomainUser admin) throws ClientException;
    
    /**
     * Initializes the client companys
     * @param admin
     * @param loginName
     * @param company
     * @param companyName
     * @param salutation
     * @param title
     * @param lastName
     * @param firstName
     * @param street
     * @param zipcode
     * @param city
     * @param country
     * @param federalState
     * @param pcountry
     * @param pprefix
     * @param pnumber
     * @param fcountry
     * @param fprefix
     * @param fnumber
     * @param email
     * @return
     * @throws ClientException if validation failed
     */
    DomainUser initCompany(
            DomainUser admin,
            String loginName,
            SystemCompany company,
            String companyName,
            Salutation salutation,
            Option title,
            String lastName,
            String firstName,
            String street,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String pcountry,
            String pprefix,
            String pnumber,
            String fcountry,
            String fprefix,
            String fnumber,
            String email) throws ClientException;
    
    void closeSetup(); 
}
