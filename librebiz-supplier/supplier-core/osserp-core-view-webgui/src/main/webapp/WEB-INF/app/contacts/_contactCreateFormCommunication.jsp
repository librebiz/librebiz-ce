<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="${sessionScope.contactInputViewName}" />
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="phone"><fmt:message key="phone" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="phoneNumber" styleClass="form-control" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="fax"><fmt:message key="fax" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="faxNumber" styleClass="form-control" />
        </div>
    </div>
</div>

<oc:systemPropertyEnabled name="contactB2BMobilePhoneEnabled">
    <c:set var="companyMobileEnabled" value="true" />
</oc:systemPropertyEnabled>
<c:if test="${view.personSelected or !empty companyMobileEnabled}">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="mobile"><fmt:message key="mobile" /></label>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <v:text name="mobileNumber" styleClass="form-control" />
            </div>
        </div>
    </div>
</c:if>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="email"><fmt:message key="email" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="email" styleClass="form-control" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="note"><fmt:message key="note" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:textarea styleClass="form-control" name="note" rows="2" />
        </div>
    </div>
</div>
