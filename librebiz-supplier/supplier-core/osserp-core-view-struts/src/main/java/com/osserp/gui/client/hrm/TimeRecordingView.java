/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 3:03:27 PM 
 * 
 */
package com.osserp.gui.client.hrm;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingPeriod;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("timeRecordingView")
public interface TimeRecordingView extends TimeRecordingAwareView {

    /**
     * Check current day open and select the booking type
     */
    public void checkDayOpen();

    /**
     * Provides available interval types
     * @return intervalTypes
     */
    public List<TimeRecordType> getIntervalTypes();

    /**
     * Provides available start types
     * @return startTypes
     */
    public List<TimeRecordType> getStartTypes();

    /**
     * Provides available marker types
     * @return markerTypes
     */
    public List<TimeRecordMarkerType> getMarkerTypes();

    /**
     * Provides the stop type
     * @return stopType
     */
    public TimeRecordType getStopType();

    /**
     * Provides current selected type
     * @return selectedType
     */
    public TimeRecordType getSelectedType();

    /**
     * Selects a record type
     * @param id
     */
    public void selectType(Long id);

    /**
     * Indicates that current profile is currently recording, e.g. open start booking exists
     * @return currentlyRecording
     */
    public boolean isCurrentlyRecording();

    /**
     * Indicates that manual recording is selected
     * @return manualRecording
     */
    public boolean isManualRecordingMode();

    /**
     * Enables/disables manual recording mode
     * @param manualRecordingMode
     */
    public void setManualRecordingMode(boolean manualRecordingMode);

    /**
     * Create a new time recording period
     * @throws PermissionException if user has not required permissions
     */
    public void createPeriod() throws PermissionException;

    /**
     * Provides the last time record
     * @return lastRecord or null if none exists
     */
    public TimeRecord getLastRecord();

    /**
     * Removes a record if user has required permissions
     * @param id
     * @throws PermissionException if user has not required permissions
     */
    public void removeRecord(Long id) throws PermissionException;

    /**
     * Provides available time recording periods in this recording
     * @return available periods
     */
    public List<TimeRecordingPeriod> getAvailablePeriods();

    /**
     * Removes a marker if user has required permissions
     * @param id
     * @throws PermissionException if user has not required permissions
     */
    public void removeMarker(Long id) throws PermissionException;

    /**
     * Provides a selected record
     * @return selectedRecord
     */
    public TimeRecord getSelectedRecord();

    /**
     * Selectes a record
     * @param id
     */
    public void selectRecord(Long id);

    /**
     * Provides corrections of current selected record
     * @return record corrections or empty list
     */
    public List<TimeRecordCorrection> getRecordCorrections();

    /**
     * Adds a correction for current selected record
     * @param form
     * @throws ClientException if time not exists or not valid
     */
    public void updateRecord(Form form) throws ClientException;

    /**
     * Selects a date
     * @param day
     * @param month
     * @param year
     * @return true if selected is current
     */
    public boolean selectDate(Integer day, Integer month, Integer year);

    /**
     * Provides available configs
     * @return availableConfigs
     */
    public List<TimeRecordingConfig> getAvailableConfigs();

    /**
     * Provides the id of the next period or null if no next period available
     * @return nextPeriodId
     */
    public Long getNextPeriodId();

    /**
     * Indicates that view is in config change mode
     * @return configChangeMode
     */
    public boolean isConfigChangeMode();

    /**
     * Enables/disables config change modee
     * @param configChangeMode
     * @throws PermissionException if user has not required permissions
     */
    public void changeConfigMode(boolean configChangeMode) throws PermissionException;

    /**
     * Changes config of current selected period
     * @param id
     * @throws ClientException if no period selected
     */
    public void changeConfig(Long id) throws ClientException;

    /**
     * Updates current selected period. Expected form values.: <br/>
     * request param 'validFrom' <br/>
     * request param 'validTil' <br/>
     * request param 'carryoverLeave' <br/>
     * request param 'carryoverHours' <br/>
     * Note: No action will be perfomed for null values
     * @param form
     * @throws ClientException
     * @throws PermissionException if user has not required permissions
     */
    public void updatePeriod(Form form) throws ClientException, PermissionException;

    /**
     * Add marker
     * @param form
     * @throws ClientException
     * @throws PermissionException if user has not required permissions
     */
    public void addMarker(Form form) throws ClientException, PermissionException;

    public boolean isListDescendentMode();

    public void toggleListDescendentMode();

    public boolean isListDetailsMode();

    public void toggleListDetailsMode();

    public boolean isDisplayJournalMode();

    public void toggleDisplayJournalMode();

    public boolean isDisplayMarkerMode();

    public void toggleDisplayMarkerMode();

    public boolean isInvokedInApprovalMode();
}
