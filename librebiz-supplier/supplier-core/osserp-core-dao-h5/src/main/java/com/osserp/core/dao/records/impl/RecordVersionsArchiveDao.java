/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 16, 2006 5:09:55 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.User;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.impl.DatabaseDocumentReference;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.service.SecurityService;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.records.RecordVersionsArchive;
import com.osserp.core.dao.records.RecordsArchive;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDocument;
import com.osserp.core.model.records.RecordDocumentVersionImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordVersionsArchiveDao extends AbstractRecordsArchive implements RecordVersionsArchive {
    private static Logger log = LoggerFactory.getLogger(RecordVersionsArchiveDao.class.getName());
    private RecordsArchive recordsArchive = null;
    private String sequenceName = null;

    public RecordVersionsArchiveDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Long documentTypeId,
            String documentTableKey,
            String binaryTableKey,
            RecordsArchive recordsArchive,
            DocumentEncoder documentEncoder, 
            SecurityService authenticationProvider) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                documentTypeId,
                documentTableKey,
                null, // categoryTableKey not supported by record type
                binaryTableKey,
                documentEncoder, 
                authenticationProvider);
        this.recordsArchive = recordsArchive;
        this.sequenceName = getTable(TableKeys.RECORD_DOCUMENT_VERSION_SEQ);
    }

    @Override
    protected DmsDocument createObject(
            User user, 
            DocumentType documentType, 
            DmsReference reference, 
            String fileName, 
            long fileSize, 
            String note, 
            Date sourceDate, 
            Long categoryId, 
            Date validFrom, 
            Date validTil,
            String messageId) {
        Long id = getNextLong(sequenceName);
        return new RecordDocumentVersionImpl(id, (Record) reference.getReference(),
                documentType, fileName, fileSize, user.getId(), note);
    }

    @Override
    protected Class<RecordDocumentVersionImpl> getPersistentClass() {
        return RecordDocumentVersionImpl.class;
    }

    /**
     * Saves a document for the specified reference
     * @param User user
     * @param Record record
     */
    public boolean create(User user, Record record) {

        if (log.isDebugEnabled()) {
            log.debug("create() invoked");
        }
        Integer previousVersion = Integer.valueOf(0);
        if (record instanceof Order) {
            Order order = (Order) record;
            previousVersion = Integer.valueOf(order.getVersion().intValue() - 1);
        }
        DocumentType documentType = getDocumentType();
        DmsReference reference = new DmsReference(record, record.getType().getId());
        List<DmsDocument> existing = recordsArchive.findByReference(reference);
        if (!empty(existing)) {
            if (log.isDebugEnabled()) {
                log.debug("create() existing found, trying to create version entry...");
            }
            DmsDocument doc = existing.get(0);
            DmsDocument archived = createObject(
                    user,
                    documentType,
                    reference,
                    doc.getFileName(),
                    doc.getFileSize(),
                    doc.getNote(),
                    null,
                    null,
                    null,
                    null,
                    null);
            archived.setPersistenceFormat(doc.getPersistenceFormat());
            if (archived instanceof RecordDocument) {
                RecordDocument target = (RecordDocument) archived;
                target.setVersion(previousVersion);
                target.setContactId(record.getContact().getId());
                target.setContactType(record.getContact().getType().getId());
            }
            save(archived);
            if (archived.getId() != null) {
                try {
                    byte[] data = recordsArchive.getDocumentData(doc);
                    store(new DatabaseDocumentReference(archived.getId()), data);
                } catch (Throwable t) {
                    log.error("create() failed while copying binary data: "
                            + t.toString(), t);
                    return false;
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("create() done for new document " + archived.getId());
            }
        }
        return true;
    }

    public boolean createCustom(User user, Record record, byte[] pdf, String filename) {
        DocumentType documentType = getDocumentType();
        DmsReference reference = new DmsReference(record, record.getType().getId());
        DmsDocument archived = createObject(
                user,
                documentType,
                reference,
                filename,
                pdf.length,
                null,
                null,
                null,
                null,
                null,
                null);
        archived.setPersistenceFormat(archived.getPersistenceFormat());
        if (archived instanceof RecordDocument) {
            RecordDocument target = (RecordDocument) archived;
            if (record.getCustomHeader() != null) {
                target.setHeaderName(record.getCustomHeader());
            }
            target.setVersion(0);
            target.setContactId(record.getContact().getId());
            target.setContactType(record.getContact().getType().getId());
        }
        save(archived);
        if (archived.getId() != null) {
            try {
                store(new DatabaseDocumentReference(archived.getId()), pdf);
            } catch (Throwable t) {
                log.error("createCustom: failed on attempt to save binary: "
                        + t.toString(), t);
                return false;
            }
        }
        return true;
    }

    public List<RecordDocument> find(Record record) {
        List<RecordDocument> existing = new ArrayList(findByReference(
                new DmsReference(record, record.getType().getId())));
        if (!existing.isEmpty()) {
            CollectionUtil.sort(existing, createVersionComparator());
        }
        return existing;
    }

    private Comparator createVersionComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                if (a == null && b == null) {
                    return 0;
                }
                if (a == null) {
                    return -1;
                }
                if (b == null) {
                    return 1;
                }
                Integer vA = ((RecordDocument) a).getVersion();
                Integer vB = ((RecordDocument) b).getVersion();
                return vA.compareTo(vB) * (-1);
            }
        };
    }
}
