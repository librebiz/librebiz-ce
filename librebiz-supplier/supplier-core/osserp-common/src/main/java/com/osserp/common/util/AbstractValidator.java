/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 14, 2005 
 * 
 */
package com.osserp.common.util;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractValidator {

    /** Represents an undefined double value selection; value = 0 */
    private static final Double D_UNDEFINED = 0d;

    /** Represents an undefined double value selection; value = 0 */
    private static final Integer I_UNDEFINED = Integer.valueOf(0);

    /** Represents an undefined long value selection; value = 0 */
    private static final Long L_UNDEFINED = 0L;

    /**
     * Checks if given string is null or empty
     * @param s
     * @return true if so
     */
    protected static boolean isNotSet(String s) {
        return (s == null || s.length() < 1);
    }

    /**
     * Checks wether given long is null or it's value is 0
     * @param l
     * @return true if so
     */
    protected static boolean isNotSet(Long l) {
        return (l == null || L_UNDEFINED.equals(l));
    }

    /**
     * Checks wether given double is null or it's value is 0
     * @param d
     * @return true if so
     */
    protected static boolean isNotSet(Double d) {
        return (d == null || D_UNDEFINED.equals(d));
    }

    /**
     * Checks wether given integer is null or it's value is 0
     * @param i
     * @return true if so
     */
    protected static boolean isNotSet(Integer i) {
        return (i == null || I_UNDEFINED.equals(i));
    }

    /**
     * Checks wether given string wether null nor empty
     * @param s
     * @return true if so
     */
    protected static boolean isSet(String s) {
        return (s != null && s.length() > 0);
    }

    /**
     * Checks wether given long is null or it's value is 0
     * @param l
     * @return true if so
     */
    protected static boolean isSet(Long l) {
        return (l != null && !L_UNDEFINED.equals(l));
    }

    /**
     * Checks wether given double is null or it's value is 0
     * @param d
     * @return true if so
     */
    protected static boolean isSet(Double d) {
        return (d != null && !D_UNDEFINED.equals(d));
    }

    /**
     * Checks wether given integer is null or it's value is 0
     * @param i
     * @return true if so
     */
    protected static boolean isSet(Integer i) {
        return (i != null && !I_UNDEFINED.equals(i));
    }
}
