/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Sep 27, 2017 
 * 
 */
package com.osserp.api


import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@Entity
@Table(name='ws_option')
class WsOption extends WsObject {
    private static final long serialVersionUID = 4L

    WsOption() {
        super()
    }

    WsOption(String context, String name, String value) {
        super()
        this.context = context
        this.name = name
        this.value = value
    }

    @Column(name='context')
    String context

    @Column(name='name')
    String name

    @Column(name='val')
    String value

    @Column(name='lang')
    String lang

    boolean isEnabled() {
        "true".equalsIgnoreCase(value)
    }

    Long getValueAsLong() {
        try {
            return Long.valueOf(value)
        } catch (Throwable ignorable) {
            return null
        }
    }
}
