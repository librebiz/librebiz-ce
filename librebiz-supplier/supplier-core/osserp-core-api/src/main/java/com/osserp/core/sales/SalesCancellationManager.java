/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 19-Jun-2006 12:50:22 
 * 
 */
package com.osserp.core.sales;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesCancellationManager extends RecordManager {

    /**
     * Cancels a credit note
     * @param user
     * @param note
     * @param date
     * @param customId
     * @return new created cancellation
     * @throws ClientException if voucher already canceled
     */
    Cancellation create(Employee user, CreditNote note, Date date, Long customId) throws ClientException;

    /**
     * Cancels an invoice
     * @param user
     * @param note
     * @param date
     * @param customId
     * @return new created cancellation
     * @throws ClientException if voucher already canceled or invalid custom number provided
     */
    Cancellation create(Employee user, Invoice invoice, Date date, Long customId) throws ClientException;

    /**
     * Finds the related cancellation
     * @param cancellable
     * @return cancellation or null if no such object exists
     */
    Cancellation find(CancellableRecord cancellable);

    /**
     * Updates the specified values
     * @param record
     * @param signaturLeft
     * @param signaturRight
     * @param personId
     * @param note
     * @throws ClientException if record is unchangeable
     */
    void update(
            Record record,
            Long signatureLeft,
            Long signatureRight,
            Long personId,
            String note,
            boolean printBusinessCaseId,
            boolean printBusinessCaseInfo)
            throws ClientException;
}
