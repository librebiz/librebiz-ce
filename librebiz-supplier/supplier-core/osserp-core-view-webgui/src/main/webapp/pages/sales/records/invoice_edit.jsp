<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesInvoiceView}">
<o:logger write="invoice_edit.jsp no view found" level="warn"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesInvoiceView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="conditionCommand" scope="request" value="/salesInvoiceInfos.do"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records_edit.css"/>
</style>
</tiles:put>
	
<tiles:put name="headline">
<c:import url="/pages/records/record_headline_edit.jsp"/>
</tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/salesInvoice.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<v:link url="/records/recordPrint/print?view=salesInvoiceView" title="documentPrint"><o:img name="printIcon" /></v:link>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
 
                    <o:form name="recordForm" url="/salesInvoice.do">
                    <input type="hidden" name="method" value="update"/>
                    <table class="table">
                        <tr>
                            <td class="recordEditLeft" valign="top">
                                <table>
                                    <c:if test="${recordView.recordNumberChangeable}">
                                        <tr>
                                            <td><fmt:message key="invoiceNumber"/></td>
                                            <td><input type="text" name="recordId" class="recordInput" value="<o:out value="${record.id}"/>"/></td>
                                        </tr>
                                    </c:if>
                
                                    <c:import url="/pages/records/record_sigs_edit.jsp"/>
                                    <c:import url="/pages/records/invoice_payment_edit.jsp"/>
                                    <c:import url="/pages/records/record_taxfree_edit.jsp"/>
                                    <c:import url="/pages/records/invoice_taxpoint_edit.jsp"/>
                                    <c:import url="/pages/records/record_custom_header_edit.jsp"/>
                                    <c:import url="/pages/records/record_notes_edit.jsp"/>
                                </table>
                            </td>
                            <td class="recordEditRight" valign="top">
                            <c:choose>  
                            <c:when test="${record.downpayment}">
                                <table style="margin-top: 15px;">
                                    <tr>
                                        <td><fmt:message key="totalAmount"/></td>
                                        <td class="right"><o:number value="${record.amounts.amount}" format="currency"/></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="thereofProportionate"/></td>
                                        <td class="right"><o:number value="${record.amount}" format="currency"/></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="info_box">
                                                <a href="javascript:;" style="color: white; background-color: #EB8802; text-decoration: none;">
                                                    <img src="<c:url value="${applicationScope.infoIcon}"/>" class="info_icon"/>
                                                    <span><fmt:message key="ifDeviantAccordAfterPaymentPractice"/></span>
                                                </a>
                                                &nbsp;<fmt:message key="amount"/>
                                            </div>
                                        </td>
                                        <td class="right">
                                            <c:choose>
                                            <c:when test="${record.fixedAmount}">
                                                <input type="text" name="amount" class="input rinput" style="text-align: right;" value="<o:number value="${record.amount}" format="currency"/>"/>
                                            </c:when>
                                            <c:otherwise>
                                                <input type="text" name="amount" class="input rinput" style="text-align: right;" />
                                            </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>
                                    <c:import url="/pages/records/record_currency_edit.jsp"/>
                                    <c:import url="/pages/records/record_language_edit.jsp"/>
                                    <c:import url="/pages/records/record_taxrate_edit.jsp"/>
                                    <c:import url="/pages/records/record_print_options.jsp"/>
                                    <c:import url="/pages/records/record_save_changes.jsp"/>
                                </table>
                            </c:when>
                            <c:otherwise>
                                <table>
                                    <c:import url="/pages/records/record_amount_display.jsp"/>
                                    <c:import url="/pages/records/record_currency_edit.jsp"/>
                                    <c:import url="/pages/records/record_language_edit.jsp"/>
                                    <c:import url="/pages/records/record_taxrate_edit.jsp"/>
                                    <c:if test="${!record.downpayment}">
                                        <oc:systemPropertyEnabled name="deEst35aSupport">
                                            <tr>
                                                <td><fmt:message key="deEst35a"/></td>
                                                <td class="right"><input type="text" name="deEst35a" class="input rinput" value="<o:out value="${record.deEst35a}"/>" style="width:100px;" /></td>
                                            </tr>
                                            <tr>
                                                <td><fmt:message key="deEst35aTax"/></td>
                                                <td class="right"><input type="text" name="deEst35aTax" class="input rinput" value="<o:out value="${record.deEst35aTax}"/>" style="width:100px;" /></td>
                                            </tr>
                                        </oc:systemPropertyEnabled>
                                    </c:if>
                                    <c:import url="/pages/records/record_print_options.jsp"/>
                                    <c:import url="/pages/records/record_save_changes.jsp"/>
                                </table>
                            </c:otherwise>
                            </c:choose> 
                            </td>
                        </tr>
                    </table>
                    </o:form>
                </div>
            </div>
        </div>
    </div>
</div>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
