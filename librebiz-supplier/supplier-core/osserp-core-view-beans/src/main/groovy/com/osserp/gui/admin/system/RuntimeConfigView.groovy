/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2011 3:20:26 PM 
 * 
 */
package com.osserp.gui.admin.system

import com.osserp.common.OptionsCache
import com.osserp.common.Property
import com.osserp.common.util.EmailValidator

import com.osserp.core.products.ProductManager
import com.osserp.core.products.ProductSummaryManager
import com.osserp.core.employees.EmployeeSearch
import com.osserp.core.sales.SalesMonitoringManager
import com.osserp.core.sales.SalesRevenueManager

import com.osserp.gui.CoreAdminView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * @author tn <tn@osserp.com>
 * 
 */
class RuntimeConfigView extends CoreAdminView {
    
    RuntimeConfigView() {
        super()
        permissions = 'runtime_config'
    }

    Boolean getCheckMX() {
        EmailValidator.isCheckMXEnabled()
    }

    def disableMXCheck() {
        EmailValidator.disableCheckMX()
    }

    def enableMXCheck() {
        EmailValidator.enableCheckMX()
    }

    Boolean getCheckSMTP() {
        EmailValidator.isCheckSMTPEnabled()
    }

    void disableSMTPCheck() {
        EmailValidator.disableCheckSMTP()
    }

    void enableSMTPCheck() {
        EmailValidator.enableCheckSMTP()
    }

    void reloadProductSummary() {
        ProductSummaryManager productSummaryManager = getService(ProductSummaryManager.class.getName())
        productSummaryManager.refreshPlanningCache()
    }
    
    void reloadOptions() {
        OptionsCache cache = getService(OptionsCache.class.getName())
        cache.refreshAll()
    }

    void reloadSalesMonitoringCache() {
        SalesMonitoringManager salesMonitoringManager = getService(SalesMonitoringManager.class.getName())
        salesMonitoringManager.reload()
    }

    void reloadSalesRevenueCalculatorConfigs() {
        SalesRevenueManager salesRevenueManager = getService(SalesRevenueManager.class.getName())
        salesRevenueManager.reloadConfigs()
    }
    
    void synchronizePublicProducts() {
        ProductManager productManager = getService(ProductManager.class.getName())
        productManager.syncPublicAvailable()
    }
    
    void synchronizePublicProductMedia() {
        ProductManager productManager = getService(ProductManager.class.getName())
        productManager.syncPublicAvailableMedia()
    }
}
