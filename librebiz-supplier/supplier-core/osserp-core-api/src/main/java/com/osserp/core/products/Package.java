/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Sep-2006 08:32:07 
 * 
 */
package com.osserp.core.products;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Package extends ProductDetails {

    /**
     * Indicates that package will be self mounted
     * @return selfMounting
     */
    boolean isSelfMounting();

    /**
     * Sets self mounting
     * @param selfMounting
     */
    void setSelfMounting(boolean selfMounting);

    /**
     * Indicates that this package requires an extra charge
     * @return extraChargeAvailable
     */
    boolean isExtraChargeAvailable();

    /**
     * Indicates that the extra charge is a percentage value
     * @return extraChargePercentAvailable
     */
    boolean isExtraChargePercentAvailable();

    /**
     * Provides the extra charge percetage value
     * @return extraChargePercent
     */
    Double getExtraChargePercent();

    void setExtraChargePercent(Double extraChargePercent);

    /**
     * Indicates that the extra charge is a charge per unit
     * @return true if so
     */
    boolean isExtraChargePerUnitAvailable();

    /**
     * Provides an extra charge charged as on per unit basis
     * @return extraChargePerUnit
     */
    Double getExtraChargePerUnit();

    void setExtraChargePerUnit(Double extraChargePerUnit);

    /**
     * Indicates that package represents only an extension to an existing plant
     * @return extension
     */
    boolean isExtension();

    void setExtension(boolean extension);

    /**
     * Indicates that package represents an material exchange on an existing plant
     * @return exchange
     */
    boolean isExchange();

    void setExchange(boolean exchange);

    /**
     * Indicates that package represents a warranty service or replacement
     * @return warrantyService
     */
    boolean isWarrantyService();

    void setWarrantyService(boolean warrantyService);

    /**
     * Indicates that package represents a free service
     * @return freeService
     */
    boolean isFreeService();

    void setFreeService(boolean freeService);
}
