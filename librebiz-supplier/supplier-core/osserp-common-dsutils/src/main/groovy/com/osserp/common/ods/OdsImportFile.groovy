/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.ods

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.github.miachm.sods.Range
import com.github.miachm.sods.Sheet
import com.github.miachm.sods.SpreadSheet

import com.osserp.common.ds.ImportException
import com.osserp.common.ds.ImportFile

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class OdsImportFile implements ImportFile {
    private static Logger logger = LoggerFactory.getLogger(OdsImportFile.class.getName())

    private Range sheet

    String filename
    String[] columnNames
    private int columnCount

    int rowCount
    int header = 0
    int lastRow = 0

    OdsImportFile(File file, Map<String, Object> params) throws ImportException {
        assert file
        if (!file.exists()) {
            throw new ImportException('file does not exist')
        }
        if (!file.canRead()) {
            throw new ImportException('file not readable')
        }
        filename = file.absolutePath
        sheet = fetchSheet(filename)
        columnCount = sheet.numColumns
        rowCount = sheet.numRows
        boolean providesHeader = params?.providesHeader
        if (providesHeader) {
            columnNames = new String[columnCount]
            for (int i = 0; i < columnCount; i++) {
                String colname = OdsUtil.createColumnName(sheet.getCell(0, i).getValue())
                columnNames[i] = colname.toLowerCase()
            }
            header = 1
        } else {
            columnNames = new String[columnCount]
            for (int i = 0; i < columnCount; i++) {
                String cname = Integer.valueOf(i+1).toString();
                columnNames[i] = "c${cname}".toString()
            }
        }
        lastRow = header
    }

    boolean hasNext() {
        (sheet.numRows > header)
    }

    /**
     * Provides the next column as raw column data
     * @return raw column data 
     */
    Object[] getNextRow() {
        Object[] nextRow = new Object[columnCount]
        for (int i = 0; i < columnCount; i++) {
            def value
            try {
                value = sheet.getCell(header, i).getValue()
            } catch (Throwable e) {
                value = null
            }
            if (value) {
                nextRow[i] = value.toString()
            } else {
                nextRow[i] = ''
            }
        }
        lastRow++
        try {
            sheet.getSheet().deleteRow(header)
            sheet = sheet.getSheet().getDataRange()
        } catch (Exception e) {
            return null
        }
        return nextRow
    }

    /**
     * Provides the next column as mapped data
     * @return mapped values
     */
    Map<String, String> getNextMap() {
        if (!hasNext()) {
            return null
        }
        Object[] data = nextRow
        boolean allEmpty = true
        def vals = [:]
        for (int i = 0; i < columnCount; i++) {
            def datval = data[i]
            vals[columnNames[i]] = datval
            if (datval) {
                allEmpty = false
            }
        }
        if (allEmpty) {
            logger.debug('getNextMap: found empty row')
            return null
        }
        return vals
    }

    List<Map<String, String>> getMapped() {
        int currentRow = header
        def result = []
        while (currentRow <= rowCount && currentRow < rowCount) {
            Object[] data = nextRow
            if (data) {
                def vals = [:]
                for (int i = 0; i < columnCount; i++) {
                    vals[columnNames[i]] = data[i]
                }
                result << vals
            }
            currentRow++
        }
        return result
    }

    List<String> getColumns() {
        List<String> res = []
        if (columnNames) {
            columnNames.each { res << it }
        }
        return res
    }

    private Range fetchSheet(String filename) {
        try {
            SpreadSheet spreadsheet = new SpreadSheet(new File(filename))
            List<Sheet> sheets = spreadsheet.getSheets()
            if (sheets) {
                return sheets.get(0).getDataRange()
            }
        } catch (IOException e) {
            logger.error("fetchSheet: Failed to read file, message: ${e.message}", e)
        }
        return null
    }
}
