/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 25, 2012 11:26:03 AM 
 * 
 */
package com.osserp.core.model.calc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.beans.OptionImpl;

import com.osserp.core.calc.CalculationByTemplateConfig;
import com.osserp.core.calc.CalculationTemplate;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * @author tn <tn@osserp.com>
 * 
 */
public class CalculationByTemplateConfigImpl extends OptionImpl implements CalculationByTemplateConfig {

    private boolean createRecord = false;
    private List<CalculationTemplate> templates = new ArrayList<CalculationTemplate>();

    protected CalculationByTemplateConfigImpl() {
        super();
    }

    public boolean isCreateRecord() {
        return createRecord;
    }

    public void setCreateRecord(boolean createRecord) {
        this.createRecord = createRecord;
    }

    public void addTemplate(CalculationTemplate template) {
        templates.add(template);
    }

    public void removeTemplate(Long templateId) {
        for (Iterator<CalculationTemplate> iterator = templates.iterator(); iterator.hasNext();) {
            CalculationTemplate template = iterator.next();
            if (template.getId().equals(templateId)) {
                iterator.remove();
                break;
            }
        }
    }

    public List<CalculationTemplate> getTemplates() {
        return templates;
    }

    protected void setTemplates(List<CalculationTemplate> templates) {
        this.templates = templates;
    }
}
