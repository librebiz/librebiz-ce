/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01-Feb-2007 21:17:13 
 * 
 */
package com.osserp.core.service.records;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

import com.osserp.core.Item;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.RecordActions;
import com.osserp.core.dao.records.SalesDeliveryNotes;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.SerialNumberDisplay;
import com.osserp.core.products.Product;
import com.osserp.core.projects.ProjectFcsManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesDeliveryNoteManager;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;
import com.osserp.core.tasks.DeliveryNoteUpdateSender;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesDeliveryNoteManagerImpl extends AbstractDeliveryNoteManager
        implements SalesDeliveryNoteManager {
    private static Logger log = LoggerFactory.getLogger(SalesDeliveryNoteManagerImpl.class.getName());
    private ProjectFcsManager fcsManager = null;
    private DeliveryNoteUpdateSender deliveryNoteUpdateSender = null;
    private RecordActions recordActions = null;

    public SalesDeliveryNoteManagerImpl(
            SalesDeliveryNotes deliveryNotes,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            SalesOrders salesOrders,
            ProjectFcsManager fcsManager,
            DeliveryNoteUpdateSender deliveryNoteUpdateSender,
            RecordActions recordActions) {

        super(deliveryNotes, systemConfigManager, products, productPlanningTask, salesOrders);
        this.deliveryNoteUpdateSender = deliveryNoteUpdateSender;
        this.fcsManager = fcsManager;
        this.recordActions = recordActions;
    }

    public List<DeliveryNote> getByOrder(Long orderId) {
        return new ArrayList(getSalesDeliveryNotesDao().getByReference(orderId));
    }

    public List<DeliveryNote> getBySales(Sales sales) {
        try {
            SalesOrders orders = (SalesOrders) getOrders();
            Order o = orders.find(sales);
            return o.getDeliveryNotes();
        } catch (Throwable t) {
            return new java.util.ArrayList<DeliveryNote>();
        }
    }

    public DeliveryNote create(Employee user, Order order, DeliveryNoteType type, Date deliveryDate) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked from user "
                    + (user == null ? "null" : user.getId())
                    + " for order " + (order == null ? "null" : order.getId()));
        }
        return getSalesDeliveryNotesDao().create(user, order, type, deliveryDate);
    }

    public DeliveryNote create(Employee user, Invoice invoice) {
        return getSalesDeliveryNotesDao().create(user, invoice);
    }

    public boolean exists(Invoice invoice) {
        return getSalesDeliveryNotesDao().exists(invoice);
    }

    public DeliveryNote createRollin(Employee user, Cancellation cancellation) {
        DeliveryNote correction = getSalesDeliveryNotesDao().createRollin(user, cancellation);
        if (correction != null) {
            try {
                this.updateStatus(correction, user, Cancellation.STAT_SENT);
            } catch (Exception ignorable) {
                // should never happen here
            }
        }
        return correction;
    }

    public DeliveryNote createRollin(Employee user, CreditNote note) {
        if (note.isDeliveryExisting() && note.isRollinAwareItemAvailable()) {
            DeliveryNote deliveryNote = getSalesDeliveryNotesDao().createRollin(user, note);
            return deliveryNote;
        }
        return null;
    }

    public DeliveryNote createRollin(Employee user, Sales sales) throws ClientException {
        if (sales.isCancelled() || sales.isClosed()) {
            throw new ClientException(ErrorCode.ORDER_CLOSED);
        }
        SalesOrders orders = (SalesOrders) getOrders();
        Order order = orders.find(sales);
        if (log.isDebugEnabled()) {
            log.debug("createRollin() invoked for order [" + order.getId() + "]");
        }
        List<Item> deliveredGoods = order.getDeliveries();
        if (deliveredGoods.isEmpty()) {
            throw new ClientException(ErrorCode.DELIVERY_MISSING);
        }
        DeliveryNote created = getSalesDeliveryNotesDao().createRollin(
                user, order, deliveredGoods);
        return created;
    }

    public void removeRollin(Employee user, CreditNote note) {
    	getSalesDeliveryNotesDao().removeRollin(user, note);
	}

	public boolean isAvailableOnStock(DeliveryNote record) {
        boolean result = true;
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            Item item = record.getItems().get(i);
            getProducts().loadSummary(item.getProduct());
            item.getProduct().enableStock(item.getStockId());
            if (item.getProduct().isAffectsStock()) {
                Double toDeliver = (item.getQuantity() - (item.getDelivered() == null ? 0 : item.getDelivered()));
                Double inStock = item.getProduct().getSummary().getStock();
                Double inStockReceipt = item.getProduct().getSummary().getReceipt();
                if (toDeliver > (inStock + inStockReceipt)) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    public void checkOpenDeliveries(Order order, DeliveryNote selected) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("checkOpenDeliveries() invoked [order=" + order.getId()
                    + ", deliveryNote=" + selected.getId() + "]");
        }
        List<Item> open = order.getOpenDeliveries();
        if (open.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("checkOpenDeliveries() no open deliveries found [order=" + order.getId()
                        + ", deliveryNote=" + selected.getId() + "]");
            }
            throw new ClientException(ErrorCode.DELIVERY_ITEMS_MISSING);
        }
        for (int i = 0, j = selected.getItems().size(); i < j; i++) {
            Item plannedDelivery = selected.getItems().get(i);
            boolean isToDeliver = false;
            for (int k = 0, l = open.size(); k < l; k++) {
                Item next = open.get(k);
                if (next.getProduct().getProductId().equals(plannedDelivery.getProduct().getProductId())) {
                    isToDeliver = true;
                    if (plannedDelivery.getQuantity() > next.getQuantity()) {
                        throw new ClientException(ErrorCode.DELIVERY_ITEMS_OVERFLOW);
                    }
                    break;
                }
            }
            if (!isToDeliver) {
                throw new ClientException(ErrorCode.DELIVERY_ITEMS_MISSING);
            }
            if (plannedDelivery.getProduct().isAffectsStock()
                    && !plannedDelivery.getProduct().isKanban()) {
                Product product = plannedDelivery.getProduct();
                getProducts().loadSummary(product);
                product.enableStock(plannedDelivery.getStockId());
                double available = product.getSummary().getStock() + product.getSummary().getReceipt();
                if (available < plannedDelivery.getQuantity()) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkOpenDeliveries() stock availability violation [deliveryNote=" + selected.getId()
                                + ", product=" + plannedDelivery.getProduct().getProductId()
                                + ", quantity=" + plannedDelivery.getQuantity()
                                + ", available=" + available
                                + ", targetStock=" + plannedDelivery.getStockId()
                                + "]");
                    }
                    throw new ClientException(ErrorCode.STOCK_AVAILABILITY);
                }
            }
        }
    }

    public List<OrderItemsDisplay> getUnreleasedItems(Product product) {
        return getSalesDeliveryNotesDao().getUnreleasedItems(product == null ? null : product.getProductId());
    }

    public List<OrderItemsDisplay> getUnreleasedItemsByManager(Employee manager, Long stockId) {
        return getSalesDeliveryNotesDao().getUnreleasedItemsByManager(manager == null ? null : manager.getId(), stockId);
    }

    @Override
    public void delete(Record record, Employee user) throws ClientException {
        if (record instanceof DeliveryNote) {
            DeliveryNote deliveryNote = (DeliveryNote) record;
            List<Item> items = new ArrayList<Item>();
            items.addAll(deliveryNote.getItems());
            SalesOrder order = (SalesOrder) getOrders().load(deliveryNote.getReference());
            items.addAll(order.getItems());
            order.removeDelivery(deliveryNote);
            getOrders().save(order);
            if (deliveryNote.isUnchangeable()) {
                fcsManager.cancelDeliveryClosing(user, order);
            }
            super.delete(deliveryNote, user);
            reloadSummary(deliveryNote);
            if (log.isDebugEnabled()) {
                log.debug("delete() delivery note deleted [id=" + record.getId() + "]");
            }
            createProductPlanningRefreshTask(record, items);
        }
    }

    @Override
    protected void performAdditionalSerialChecks(Product product, String serialNumber) throws ClientException {
        boolean purchaseExists = false;
        List<SerialNumberDisplay> list = recordActions.findSerialNumbers(serialNumber, true);
        if (!list.isEmpty()) {
            for (int i = 0, j = list.size(); i < j; i++) {
                SerialNumberDisplay next = list.get(i);
                if (!next.isSales() && next.getProductId().equals(product.getProductId())) {
                    purchaseExists = true;
                    break;
                }
            }
        }
        if (!purchaseExists) {
            throw new ClientException(ErrorCode.SERIAL_NOT_EXISTING);
        }
    }

    @Override
    protected void doAfterStatusUpdate(Record record, Employee employee, Long oldStatus) {
        super.doAfterStatusUpdate(record, employee, oldStatus);
        if (record instanceof DeliveryNote) {
            deliveryNoteUpdateSender.send((DeliveryNote) record);
        }
    }

    protected void doAfterPrintValidation(Record record, Long nextStatus) throws ClientException {
        DeliveryNote selected = (DeliveryNote) record;
        Order order = (Order) getOrders().load(selected.getReference());
        if (!selected.isSerialsCompleted()) {
            throw new ClientException(ErrorCode.SERIALS_MISSING);
        }
        for (int i = 0, j = selected.getItems().size(); i < j; i++) {
            Item next = selected.getItems().get(i);
            Double quantity = next.getQuantity();
            if (quantity < 0) {
                quantity = quantity * (-1);
            }
            if (next.getSerials().size() > quantity) {
                throw new ClientException(ErrorCode.SERIALS_OVERFLOW);
            }
        }
        if (order != null) {
            if (selected.getBookingType() instanceof DeliveryNoteType &&
                    !((DeliveryNoteType) selected.getBookingType()).isParentContent()) {
                for (int i = 0, j = selected.getItems().size(); i < j; i++) {
                    Item deliveryItem = selected.getItems().get(i);
                    for (int k = 0, l = order.getItems().size(); k < l; k++) {
                        Item orderItem = order.getItems().get(k);
                        if (deliveryItem.getProduct().getProductId().equals(
                                orderItem.getProduct().getProductId())) {
                            throw new ClientException(ErrorCode.PRODUCT_SELECTION_INVALID);
                        }
                    }
                }
                if (!isAvailableOnStock(selected)) {
                    throw new ClientException(ErrorCode.QUANTITY_CORRECTION_REQUIRED);
                }
            } else if (!(selected.isCorrection() || selected.isCorrectionAvailable())) {
                checkOpenDeliveries(order, selected);
            }
        }
    }

    private SalesDeliveryNotes getSalesDeliveryNotesDao() {
        return (SalesDeliveryNotes) getDao();
    }

}
