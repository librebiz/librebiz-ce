/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao;

import java.util.List;
import java.util.Map;

import com.osserp.core.mail.MailTemplate;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface MailTemplates {

    /**
     * Provides available templates
     * @return templates
     */
    List<MailTemplate> findTemplates();

    /**
     * Finds a mail template by name
     * @param name
     * @return mailTemplate or null if not exists
     */
    MailTemplate findTemplate(String name);

    /**
     * Provides template by id
     * @param id
     * @return mailTemplate
     * @throws RuntimeException if no such object exists
     */
    MailTemplate getTemplate(Long id);

    /**
     * Updates existing template
     * @param mailTemplate
     */
    void update(MailTemplate mailTemplate);

    /**
     * Creates and sends a mail message by configured template name and recipients as provided by values or mail template config.
     * @param templateName
     * @param values
     */
    void send(String templateName, Map values);

    /**
     * Creates and sends a mail message by configured template name and given recipient.
     * @param templateName
     * @param recipient
     * @param values
     */
    void send(String templateName, String recipient, Map values);

    /**
     * Creates a mail message by configured template name and given recipients.
     * @param templateName
     * @param recipients
     * @param values
     */
    void send(String templateName, String[] recipients, Map values);

}
