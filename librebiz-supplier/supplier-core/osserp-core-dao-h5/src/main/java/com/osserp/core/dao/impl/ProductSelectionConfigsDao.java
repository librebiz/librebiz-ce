/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 6, 2009 6:17:59 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.AbstractHibernateAndSpringEntityDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.ProductSelectionConfigs;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.products.ProductSelectionConfigImpl;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionContext;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSelectionConfigsDao extends AbstractHibernateAndSpringEntityDao implements ProductSelectionConfigs {

    protected String productSelectionConfigs = getTable(TableKeys.PRODUCT_SELECTION_CONFIGS);
    protected String productSelectionConfigItems = getTable(TableKeys.PRODUCT_SELECTION_CONFIG_ITEMS);

    protected ProductSelectionConfigsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    @Override
    public List<ProductSelectionConfig> findAll() {
        List<ProductSelectionConfig> result = new ArrayList(super.findAll());
        for (Iterator<ProductSelectionConfig> i = result.iterator(); i.hasNext();) {
            ProductSelectionConfig next = i.next();
            if (next.isDisabled()) {
                i.remove();
            }
        }
        return result;
    }

    public List<ProductSelectionConfig> findAll(ProductSelectionContext context) {
        StringBuilder hql = new StringBuilder();
        hql
                .append("from ")
                .append(getEntityClass().getName())
                .append(" ec where ec.context.id = :contextId and ")
                .append("ec.endOfLife = false order by ec.name");
        return getCurrentSession().createQuery(hql.toString())
                .setParameter("contextId", context.getId()).list();
    }

    public ProductSelectionConfig load(Long id) {
        return (ProductSelectionConfig) loadEntity(id);
    }

    public List<ProductSelectionConfig> load(List<Long> primaryKeys) {
        return new ArrayList(super.loadEntities(primaryKeys));
    }

    public ProductSelectionConfig create(Employee user,
            ProductSelectionContext context, String name, String description)
            throws ClientException {
        if (name == null || name == "") {
            throw new ClientException(ErrorCode.NAME_MISSING);
        } else if (description == null || description == "") {
            throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
        }
        ProductSelectionConfig obj = new ProductSelectionConfigImpl(user, context, name, description);
        save(obj);
        return obj;
    }

    public ProductSelectionConfig restrict(
            Employee user,
            ProductSelectionConfig config,
            SystemCompany company,
            BranchOffice branch) throws ClientException {
        if (config instanceof ProductSelectionConfigImpl) {
            ProductSelectionConfigImpl cfg = (ProductSelectionConfigImpl) config;
            cfg.setCompany(company);
            cfg.setBranch(branch);
            save(cfg);
        }
        return load(config.getId());
    }

    public void save(ProductSelectionConfig config) {
        super.save(config);
    }

    public void update(Long userId, String name, String description, Long configId) throws ClientException {
        if (name == null || name == "") {
            throw new ClientException(ErrorCode.NAME_MISSING);
        } else if (description == null || description == "") {
            throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
        } else if (configId == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        StringBuilder sql = new StringBuilder(256);
        sql
                .append("UPDATE ")
                .append(productSelectionConfigs)
                .append(" SET name = ?, description = ?, changed = now(), changed_by = ? WHERE id = ?");
        final Object[] params = { name, description, userId, configId };
        final int[] types = { Types.VARCHAR, Types.VARCHAR, Types.BIGINT, Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }

    public void addItem(Long userId, Long configId, String columnName, Long itemId) throws ClientException {
        if (itemId == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        StringBuilder sql = new StringBuilder(256);
        sql
                .append("INSERT INTO ")
                .append(productSelectionConfigItems)
                .append(" (reference_id, " + columnName + ", created_by) VALUES (?, ?, ?)");
        final Object[] params = { configId, itemId, userId };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }

    public void addItem(Long userId, Long configId, String typeColumnName, Long typeId, String groupColumnName, Long groupId, String categoryColumnName,
            Long categoryId) throws ClientException {
        if (typeId == null && groupId == null && categoryId == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        StringBuilder sql = new StringBuilder(256);
        sql
                .append("INSERT INTO ")
                .append(productSelectionConfigItems)
                .append(" (reference_id, " + typeColumnName + ", " + groupColumnName + ", " + categoryColumnName + ", created_by) VALUES (?, ?, ?, ?, ?)");
        final Object[] params = { configId, typeId, groupId, categoryId, userId };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.BIGINT };

        jdbcTemplate.update(sql.toString(), params, types);
    }

    public void deleteItem(Long itemId) throws ClientException {
        if (itemId == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("DELETE FROM ")
                .append(productSelectionConfigItems)
                .append(" WHERE id = ?");
        final Object[] params = { itemId };
        final int[] types = { Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }

    public void deleteItems(Long typeId, Long groupId, Long categoryId) {
        StringBuilder sql = new StringBuilder(64);
        if (categoryId != null) {
            sql
                    .append("DELETE FROM ")
                    .append(productSelectionConfigItems)
                    .append(" WHERE product_category_id = ?");
            final Object[] params = { categoryId };
            final int[] types = { Types.BIGINT };
            jdbcTemplate.update(sql.toString(), params, types);

        } else if (groupId != null) {
            sql
                    .append("DELETE FROM ")
                    .append(productSelectionConfigItems)
                    .append(" WHERE product_group_id = ?");
            final Object[] params = { groupId };
            final int[] types = { Types.BIGINT };
            jdbcTemplate.update(sql.toString(), params, types);
        }
        if (typeId != null) {
            sql = new StringBuilder("DELETE FROM ");
            sql
                    .append(productSelectionConfigItems)
                    .append(" WHERE product_type_id = ?");
            final Object[] params = { typeId };
            final int[] types = { Types.BIGINT };
            jdbcTemplate.update(sql.toString(), params, types);
        }
    }

    public void toggleExclusion(Long itemId) throws ClientException {
        if (itemId == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT is_exclusion FROM ")
                .append(productSelectionConfigItems)
                .append(" WHERE id = ")
                .append(itemId);
        List<Boolean> resultList = (List<Boolean>) jdbcTemplate.query(sql.toString(), getBooleanRowMapper());
        Boolean result = resultList.isEmpty() ? false : resultList.get(0);
        sql = new StringBuilder(64);
        sql
                .append("UPDATE ")
                .append(productSelectionConfigItems)
                .append(" SET is_exclusion = ? WHERE id = ?");
        final Object[] params = { !result, itemId };
        final int[] types = { Types.BOOLEAN, Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }

    @Override
    protected Class<ProductSelectionConfigImpl> getEntityClass() {
        return ProductSelectionConfigImpl.class;
    }

    @Override
    protected String appendFindAllSuffix(StringBuilder buffer) {
        buffer.append(" order by ec.name");
        return buffer.toString();
    }
}
