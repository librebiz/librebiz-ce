/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 18-Feb-2007 11:22:11 
 * 
 */
package com.osserp.core.model.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.XmlAwareEntity;
import com.osserp.common.beans.AbstractOption;
import com.osserp.common.util.PhoneUtil;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 */
public class BranchOfficeImpl extends AbstractOption implements BranchOffice, XmlAwareEntity {

    private Contact contact = null;
    private SystemCompany company = null;
    private Long branchOf = null;
    private Long costCenter = null;
    private boolean headquarter = false;
    private boolean activated = false;
    
    private String shortname = null;
    private String shortkey = null;
    
    private Date startup = null;
    private Date endup = null;
    
    private String email = null;
    private String phoneCountry = null;
    private String phonePrefix = null;
    private String phoneNumber = null;
    
    private boolean statuaryOffice = false;
    private String statOfficeCountry = null;
    private String statOfficeRegisterNumber = null;
    private String statOfficeRegisteredIn = null;
    
    private boolean thirdparty = false;
    private boolean customTemplates = false; 
    private boolean recordTemplatesDefault = false;
    
    private boolean logoAvailable = false;
    private String logoPath = null;
    private String vatId = null;
    private String vatLabel = null;
    private Long posAccountId = null;

    private List<EmployeeGroup> groups = new ArrayList<EmployeeGroup>();

    protected BranchOfficeImpl() {
        super();
    }

    public BranchOfficeImpl(
            Long id,
            SystemCompany company,
            Contact contact,
            String name,
            String shortname,
            String shortkey) {
        super(id, company == null ? null : company.getId(), name, null);
        this.contact = contact;
        this.shortname = shortname;
        this.shortkey = shortkey;
        this.activated = true;
    }

    protected BranchOfficeImpl(BranchOffice o) {
        super(o);
        if (o.getContact() != null) {
            contact = (Contact) o.getContact().clone();
            //this.contact = o.getContact();
        }
        if (o.getCompany() != null) {
            //this.company = (SystemCompany) o.getCompany().clone();
            //this.company = o.getCompany();
        }
        shortname = o.getShortname();
        shortkey = o.getShortkey();
        activated = o.isActivated();
        headquarter = o.isHeadquarter();
        startup = o.getStartup();
        endup = o.getEndup();
        branchOf = o.getBranchOf();
        email = o.getEmail();
        phoneCountry = o.getPhoneCountry();
        phonePrefix = o.getPhonePrefix();
        phoneNumber = o.getPhoneNumber();
        posAccountId = o.getPosAccountId();
        costCenter = o.getCostCenter();
        statuaryOffice = o.isStatuaryOffice();
        statOfficeCountry = o.getStatOfficeCountry();
        statOfficeRegisterNumber = o.getStatOfficeRegisterNumber();
        statOfficeRegisteredIn = o.getStatOfficeRegisteredIn();
        thirdparty = o.isThirdparty();
        customTemplates = o.isCustomTemplates();
        recordTemplatesDefault = o.isRecordTemplatesDefault();
        logoAvailable = o.isLogoAvailable();
        logoPath = o.getLogoPath();
    }

    @Override
    public Object clone() {
        return new BranchOfficeImpl(this);
    }

    public Contact getContact() {
        return contact;
    }

    protected void setContact(Contact contact) {
        this.contact = contact;
    }

    public SystemCompany getCompany() {
        return company;
    }

    public void setCompany(SystemCompany company) {
        this.company = company;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getShortkey() {
        return shortkey;
    }

    public void setShortkey(String shortkey) {
        this.shortkey = shortkey;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public boolean isHeadquarter() {
        return headquarter;
    }

    public void setHeadquarter(boolean headquarter) {
        this.headquarter = headquarter;
    }

    public boolean isStatuaryOffice() {
        return statuaryOffice;
    }

    public void setStatuaryOffice(boolean statuaryOffice) {
        this.statuaryOffice = statuaryOffice;
    }

    public String getStatOfficeCountry() {
        return statOfficeCountry;
    }

    public void setStatOfficeCountry(String statOfficeCountry) {
        this.statOfficeCountry = statOfficeCountry;
    }

    public String getStatOfficeRegisterNumber() {
        return statOfficeRegisterNumber;
    }

    public void setStatOfficeRegisterNumber(String statOfficeRegisterNumber) {
        this.statOfficeRegisterNumber = statOfficeRegisterNumber;
    }

    public String getStatOfficeRegisteredIn() {
        return statOfficeRegisteredIn;
    }

    public void setStatOfficeRegisteredIn(String statOfficeRegisteredIn) {
        this.statOfficeRegisteredIn = statOfficeRegisteredIn;
    }

    public boolean isThirdparty() {
        return thirdparty;
    }

    public void setThirdparty(boolean thirdparty) {
        this.thirdparty = thirdparty;
    }

    public boolean isCustomTemplates() {
        return customTemplates;
    }

    public void setCustomTemplates(boolean customTemplates) {
        this.customTemplates = customTemplates;
    }

    public boolean isRecordTemplatesDefault() {
        return recordTemplatesDefault;
    }

    public void setRecordTemplatesDefault(boolean recordTemplatesDefault) {
        this.recordTemplatesDefault = recordTemplatesDefault;
    }

    public void updateThirdparty(boolean thirdparty, boolean customTemplates, boolean recordTemplatesDefault) {
        this.thirdparty = thirdparty;
        this.customTemplates = customTemplates;
        this.recordTemplatesDefault = recordTemplatesDefault;
    }
    
    public boolean isDefaultTemplateSelection() {
        return !isThirdparty() || (isThirdparty() && !isCustomTemplates());        
    }

    public Date getStartup() {
        return startup;
    }

    protected void setStartup(Date startup) {
        this.startup = startup;
    }

    public Date getEndup() {
        return endup;
    }

    protected void setEndup(Date endup) {
        this.endup = endup;
    }

    public Long getBranchOf() {
        return branchOf;
    }

    public void setBranchOf(Long branchOf) {
        this.branchOf = branchOf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneCountry() {
        return phoneCountry;
    }

    public void setPhoneCountry(String phoneCountry) {
        this.phoneCountry = phoneCountry;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneDisplay() {
        if (!isPhoneAvailable()) {
            return null;
        }
        return PhoneUtil.createInternational(phoneCountry, phonePrefix, phoneNumber);
    }

    public boolean isPhoneAvailable() {
        return (isSet(phonePrefix) && isSet(phoneNumber));
    }

    public Long getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(Long costCenter) {
        this.costCenter = costCenter;
    }

    public boolean isLogoAvailable() {
        return logoAvailable;
    }

    public void setLogoAvailable(boolean logoAvailable) {
        this.logoAvailable = logoAvailable;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public String getVatLabel() {
        return vatLabel;
    }

    public void setVatLabel(String vatLabel) {
        this.vatLabel = vatLabel;
    }

    public Long getPosAccountId() {
        return posAccountId;
    }

    public void setPosAccountId(Long posAccountId) {
        this.posAccountId = posAccountId;
    }

    public List<EmployeeGroup> getGroups() {
        return groups;
    }

    protected void setGroups(List<EmployeeGroup> groups) {
        this.groups = groups;
    }

    public void addGroup(EmployeeGroup group) {
        boolean alreadyAdded = false;
        for (int i = 0, j = groups.size(); i < j; i++) {
            EmployeeGroup next = groups.get(i);
            if (next.getId().equals(group.getId())) {
                alreadyAdded = true;
            }
        }
        if (!alreadyAdded) {
            groups.add(group);
        }
    }

    public boolean removeGroup(Long id) {
        for (Iterator<EmployeeGroup> i = groups.iterator(); i.hasNext();) {
            EmployeeGroup next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                return true;
            }
        }
        return false;
    }

    public Map<String, Object> getCompanyMap() {
        SystemCompany systemCompany = getCompany();
        Map<String, Object> systemCompanyMap = systemCompany.getMapped();
        systemCompanyMap.put("branchOffice", getMapped());
        Map<String, Object> values = new HashMap<String, Object>();
        putIfExists(values, "systemCompany", systemCompanyMap);
        return values;
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();

        if (contact != null) {
            Map<String, Object> contactMap = contact.getMapped();
            if (email != null) {
                contactMap.put("email", email);
            }
            putIfExists(map, "contact", contactMap);
            // TODO check usage and remove:
            putIfExists(map, "contactId", contact.getContactId());
        }

        putIfExists(map, "branchOf", branchOf);
        putIfExists(map, "company", getCompany().getMapped(isStatuaryOffice()));
        putIfExists(map, "email", email);
        putIfExists(map, "phoneCountry", phoneCountry);
        putIfExists(map, "phoneNumber", phoneNumber);
        putIfExists(map, "phonePrefix", phonePrefix);
        putIfExists(map, "reference", getCompany().getId());
        putIfExists(map, "resourceKey", getResourceKey());
        putIfExists(map, "shortkey", shortkey);
        putIfExists(map, "shortname", shortname);
        putFormatted(map, "startup", startup);
        putIfExists(map, "activated", activated);
        putIfExists(map, "isHeadquarter", headquarter);
        putIfExists(map, "statOfficeCountry", statOfficeCountry);
        putIfExists(map, "statOfficeRegisteredIn", statOfficeRegisteredIn);
        putIfExists(map, "statOfficeRegisterNumber", statOfficeRegisterNumber);
        putIfExists(map, "thirdparty", thirdparty);
        putIfExists(map, "logoAvailable", logoAvailable);
        putIfExists(map, "logoPath", logoPath);
        putIfExists(map, "vatId", vatId);

        return map;
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("contact", contact));
        root.addContent(company.getXML("company"));
        root.addContent(JDOMUtil.createElement("shortname", shortname));
        root.addContent(JDOMUtil.createElement("shortkey", shortkey));
        root.addContent(JDOMUtil.createElement("activated", activated));
        root.addContent(JDOMUtil.createElement("headquarter", headquarter));
        root.addContent(JDOMUtil.createElement("startup", startup));
        root.addContent(JDOMUtil.createElement("branchOf", branchOf));
        root.addContent(JDOMUtil.createElement("email", email));
        root.addContent(JDOMUtil.createElement("phoneCountry", phoneCountry));
        root.addContent(JDOMUtil.createElement("phonePrefix", phonePrefix));
        root.addContent(JDOMUtil.createElement("phoneNumber", phoneNumber));
        root.addContent(JDOMUtil.createElement("thirdparty", thirdparty));
        root.addContent(JDOMUtil.createElement("logoAvailable", logoAvailable));
        root.addContent(JDOMUtil.createElement("logoPath", logoPath));
        root.addContent(JDOMUtil.createElement("vatId", vatId));
        return root;
    }
    
}
