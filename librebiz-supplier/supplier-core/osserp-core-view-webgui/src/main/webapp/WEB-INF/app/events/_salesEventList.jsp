<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="sortArrow">
    <c:choose>
	   <c:when test="${view.sortDescending}">&#x25BE;</c:when>
	   <c:otherwise>&#x25B4;</c:otherwise>
    </c:choose>
</c:set>
<div class="table-responsive table-responsive-default">
	<table class="table table-striped" style="margin-top: 4px;">
		<thead style="margin-top: -5px;">
			<tr>
				<th class="dateTime">
                    <v:link url="/events/salesEvents/sort?by=eventDate">
                        <fmt:message key="appointment"/>
                        <c:if test="${view.sortBy == 'appointmentDate'}"><c:out escapeXml="false" value="${sortArrow}"/></c:if>
                    </v:link>
				</th>
				<th class="eventName"><fmt:message key="job"/></th>
				<th class="recipientName"><fmt:message key="receiver"/></th>
				<th class="dateTime">
                    <v:link url="/events/salesEvents/sort?by=created">
                        <fmt:message key="dateTime"/>
                        <c:if test="${view.sortBy == 'created'}"><c:out escapeXml="false" value="${sortArrow}"/></c:if>
                    </v:link>
				</th>
				<th class="center" colspan="2"><fmt:message key="action"/></th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty view or empty view.list}">
					<tr>
						<td colspan="5"><fmt:message key="noEventsAvailable"/></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="event" items="${view.list}" varStatus="s">
						<tr>
                            <c:choose>
                                <c:when test="${event.action.type.appointment}">
                                    <td class="dateTime"><o:date value="${event.appointmentDate}" addcentury="false" addtime="true"/></td>
                                </c:when>
                                <c:otherwise>
                                    <td class="dateTime"><o:date value="${event.expires}" addcentury="false"/></td>
                                </c:otherwise>
                            </c:choose>
							<td class="eventName">
								<c:choose>
									<c:when test="${event.alert}">
										<c:choose>
											<c:when test="${!empty event.headline}"><span class="errortext"><o:out value="${event.headline}"/></span></c:when>
											<c:otherwise><span class="errortext"><o:out value="${event.action.name}"/></span></c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${event.action.info}">[I]&nbsp;</c:when>
											<c:otherwise>[A]&nbsp;</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${!empty event.headline}"><o:out value="${event.headline}"/></c:when>
											<c:otherwise><o:out value="${event.action.name}"/></c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</td>
							<td class="recipientName">
								<c:choose>
									<c:when test="${event.action.sendPool and !empty event.action.pool.name}">
										<o:out value="${event.action.pool.name}"/>
									</c:when>
									<c:otherwise>
										<o:ajaxLink linkId="recipientId" url="${'/employeeInfo.do?id='}${event.recipientId}">
											<oc:employee value="${event.recipientId}"/>
										</o:ajaxLink>
									</c:otherwise>
								</c:choose>
							</td>
                            <td class="dateTime"><o:date value="${event.created}" addcentury="false" addtime="true"/></td>
                            <td class="icon center">
                                <v:ajaxLink url="/events/eventEditor/forward?id=${event.id}&view=salesEventsView" linkId="eventEditor"><o:img name="texteditIcon" /></v:ajaxLink>
                            </td>
                            <td class="icon center">
                                <c:choose>
                                    <c:when test="${event.action.closeableByTarget}">
                                        <a href="javascript:confirmLink('<fmt:message key="confirmCloseTodo"/>','<v:url value="/events/salesEvents/closeEvents?id=${event.id}"/>');" title="<fmt:message key="closeTodo"/>"><o:img name="deleteIcon"/></a>
                                    </c:when>
                                    <c:otherwise>
                                        <o:permission role="close_todos,executive" info="permissionCloseTodo">
                                            <c:if test="${view.user.employee.companyExecutive}">
                                                <a href="javascript:confirmLink('<fmt:message key="confirmCloseTodo"/>','<v:url value="/events/salesEvents/closeEvents?id=${event.id}"/>');" title="<fmt:message key="closeTodo"/>"><o:img name="deleteIcon"/></a>
                                            </c:if>
                                        </o:permission>
                                    </c:otherwise>
                                </c:choose>
                            </td>
						</tr>
						<c:if test="${!empty event.message or !empty event.notes}">
							<tr>
								<td class="dateTime">&nbsp;</td>
								<td valign="top" colspan="3">
									<c:forEach var="note" items="${event.notes}">
										<span class="boldtext"><o:date value="${note.created}" addtime="true"/> <oc:employee value="${note.createdBy}"/>:</span><br />
										<o:out value="${note.note}"/><br />
									</c:forEach>
									<c:if test="${!empty event.message}">
										<c:if test="${!empty event.action.callerId}"><span class="boldtext"><fmt:message key="infoFromFcs"/> <oc:options name="${event.action.type.flowControlActionsName}" value="${event.action.callerId}"/>:</span><br /></c:if>
										<o:textOut value="${event.message}"/>
									</c:if>
								</td>
								<td class="action"> </td>
							</tr>
						</c:if>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>
