<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="autocompleteUrl" value="${requestScope.autocompleteUrl}"/>
<c:set var="heightBody" value="445"/>
<c:if test="${!empty requestScope.defaultBodyHeight}"><c:set var="heightBody" value="${requestScope.defaultBodyHeight}"/></c:if>
<c:set var="heightTop" value="180"/>
<c:if test="${!empty requestScope.defaultTopHeight}"><c:set var="heightTop" value="${requestScope.defaultTopHeight}"/></c:if>
<script type="text/javascript">
  function switchTable() {
    if($('table-responsive').style.height == "369px") {
			$('topContent').style.display = 'none';
      $('tableSwitcher').innerHTML = "-";
			$('table-responsive').style.height = '505px';
    } else {
			$('topContent').style.display = 'block';
			$('tableSwitcher').innerHTML = "+";
			$('table-responsive').style.height = '369px';
    }
  }
</script>
