/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.service.mail;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.PermissionError;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.mail.Attachment;
import com.osserp.common.mail.ReceivedMail;
import com.osserp.common.mail.ReceivedMailInfo;
import com.osserp.common.util.FileUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessNote;
import com.osserp.core.contacts.Contact;
import com.osserp.core.customers.Customer;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.dao.MailMessages;
import com.osserp.core.dao.Users;
import com.osserp.core.dms.CoreDocumentType;
import com.osserp.core.mail.FetchmailInboxManager;
import com.osserp.core.users.DomainUser;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class FetchmailInboxManagerImpl implements FetchmailInboxManager {
    private static Logger log = LoggerFactory.getLogger(FetchmailInboxManagerImpl.class.getName());

    private MailMessages mailMessages;
    private DmsManager dmsManager;
    private BusinessNotes businessNotes;
    private Users users;

    protected FetchmailInboxManagerImpl() {
        super();
    }

    protected FetchmailInboxManagerImpl(
            MailMessages mailMessages,
            DmsManager dmsManager,
            BusinessNotes businessNotes,
            Users users) {
        super();
        this.mailMessages = mailMessages;
        this.dmsManager = dmsManager;
        this.businessNotes = businessNotes;
        this.users = users;
    }

    public List<ReceivedMailInfo> findByBusinessCase(DomainUser user, BusinessCase bc) {
        assert user != null && bc != null;
        return mailMessages.findByBusinessCase(
                user.getEmployee() == null ? user.getId() : user.getEmployee().getId(),
                bc.getPrimaryKey(), bc.getCustomer().getId());
    }

    public int getCountByBusinessCase(DomainUser user, BusinessCase bc) {
        assert bc != null;
        return mailMessages.getCountByBusinessCase(
                user.getEmployee() == null ? user.getId() : user.getEmployee().getId(),
                bc.getPrimaryKey(), bc.getCustomer().getId());
    }

    public List<ReceivedMailInfo> findByCustomer(Customer customer) {
        assert customer != null;
        return mailMessages.findByContact(customer.getId(), Contact.CUSTOMER);
    }

    public List<ReceivedMailInfo> findByStatus(DomainUser user, Integer status) {
        if (user == null) {
            throw new PermissionError();
        }
        List<ReceivedMailInfo> result = mailMessages.findByStatus(status);
        if (!user.isPermissionGrant(FetchmailInboxManager.INBOX_ADMIN_PERMISSIONS)) {
            for (Iterator<ReceivedMailInfo> i = result.iterator(); i.hasNext();) {
                ReceivedMailInfo next = i.next();
                if (next.getUserId() == null || !next.getUserId().equals(user.getId())) {
                    i.remove();
                }
            }
        }
        return result;
    }

    public List<ReceivedMailInfo> findBySupplier(Supplier supplier) {
        assert supplier != null;
        return mailMessages.findByContact(supplier.getId(), Contact.SUPPLIER);
    }

    public List<ReceivedMailInfo> findByUser(DomainUser user) {
        assert user != null;
        return mailMessages.findByUser(
                user.getEmployee() == null ? user.getId() : user.getEmployee().getId());
    }

    public int getCountByUser(DomainUser user) {
        assert user != null;
        return mailMessages.getCountByUser(
                user.getEmployee() == null ? user.getId() : user.getEmployee().getId());
    }

    public ReceivedMail getMessage(String messageId) {
        assert messageId != null;
        return mailMessages.getReceivedMail(messageId);
    }

    public void delete(ReceivedMail mail) {
        assert mail != null;
        mailMessages.remove(mail.getMessageId());
    }

    public ReceivedMail setBusinessCase(ReceivedMail mail, Long reference, Long referenceType, Long businessId) {
        assert mail != null;
        return mailMessages.setBusinessReference(mail, reference, referenceType, businessId);
    }

    public ReceivedMail setUser(ReceivedMail mail, Long userId) {
        assert mail != null;
        return mailMessages.setUserReference(mail, userId);
    }

    public ReceivedMail resetReferences(ReceivedMail mail) {
        assert mail != null;
        return mailMessages.resetBusinessReference(mail);
    }

    public ReceivedMail deleteAttachment(ReceivedMail mail, Attachment attachment) {
        assert mail != null && attachment != null;
        return mailMessages.deleteAttachment(mail, attachment.getId());
    }

    public ReceivedMail ignoreAttachment(ReceivedMail mail, Attachment attachment) {
        assert mail != null && attachment != null;
        return mailMessages.ignoreAttachment(mail, attachment.getId());
    }

    public ReceivedMail moveAttachmentsByType(
            DomainUser user,
            ReceivedMail mail,
            BusinessCase bc) throws ClientException {
        assert mail != null && bc != null;
        ReceivedMail result = null;
        for (Attachment attachment: mail.getAttachments()) {
            if (attachment.getContentType() != null
                    && attachment.getContentType().startsWith("image")) {
                result = moveToPictures(user, mail, attachment, bc);
            } else {
                result = moveToDocuments(user, mail, attachment, bc);
            }
        }
        return result != null ? result : mail;
    }

    public ReceivedMail moveToDocuments(
            DomainUser user,
            ReceivedMail mail,
            Attachment attachment,
            BusinessCase bc) throws ClientException {
        assert mail != null && attachment != null && bc != null;
        DmsReference dmsReference = bc.isSalesContext() ?
                new DmsReference(CoreDocumentType.SALES_DOCUMENT, bc.getPrimaryKey()) :
                    new DmsReference(CoreDocumentType.REQUEST_DOCUMENT, bc.getPrimaryKey());
        return transfer(user, mail, attachment, dmsReference);
    }

    public ReceivedMail moveToPictures(
            DomainUser user,
            ReceivedMail mail,
            Attachment attachment,
            BusinessCase bc) throws ClientException {
        assert mail != null && attachment != null && bc != null;
        DmsReference dmsReference = bc.isSalesContext() ?
                new DmsReference(CoreDocumentType.SALES_PICTURE, bc.getPrimaryKey()) :
                    new DmsReference(CoreDocumentType.REQUEST_PICTURE, bc.getPrimaryKey());
        return transfer(user, mail, attachment, dmsReference);
    }

    private ReceivedMail transfer(
            DomainUser user,
            ReceivedMail mail,
            Attachment attachment,
            DmsReference dmsReference) throws ClientException {
        assert mail != null && attachment != null && dmsReference != null;
        if (FileUtil.exists(attachment.getFile())) {
            dmsManager.createDocument(
                    user,
                    dmsReference,
                    attachment.getFileName(),
                    attachment.getFile(),
                    attachment.getFileName(),
                    mail.getReceivedDate(),
                    null,
                    null,
                    null,
                    mail.getMessageId());
            return mailMessages.deleteAttachment(mail, attachment.getId());

        } else if (log.isInfoEnabled()) {
            log.info("transferImage: File no longer exists: " + attachment.getFile());
        }
        return mail;
    }

    public void moveToNotes(
            DomainUser user,
            ReceivedMail mail,
            BusinessCase bc,
            Integer attachmentAction,
            String updatedText) throws ClientException {

        if (FetchmailInboxManager.MOVE_ATTACHMENTS_BY_TYPE.equals(attachmentAction)
                || FetchmailInboxManager.MOVE_ATTACHMENTS_BY_TYPE_DELETE_TEXT.equals(attachmentAction)) {
            moveAttachmentsByType(user, mail, bc);
        }
        if (!FetchmailInboxManager.MOVE_ATTACHMENTS_BY_TYPE_DELETE_TEXT.equals(attachmentAction)) {
            businessNotes.create(
                    businessNotes.getType("salesNote"), 
                    bc.getPrimaryKey(), 
                    (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()), 
                    updatedText != null ? updatedText : mail.getText(),
                    mail.getSubject(),
                    mail.getMessageId(),
                    true);
        }
        mailMessages.remove(mail.getMessageId());
    }

    public void restoreMoved(DomainUser user, ReceivedMail mail) throws ClientException {
        assert user != null && mail != null;
        BusinessNote note = businessNotes.findByMessageId(mail.getMessageId());
        if (note != null) {
            businessNotes.delete(note);
        }
        dmsManager.deleteDocuments(mail.getMessageId());
        mailMessages.resetMoved(mail.getMessageId());
    }

    // Getters and setters

    protected MailMessages getMailMessages() {
        return mailMessages;
    }

    public void setMailMessages(MailMessages mailMessages) {
        this.mailMessages = mailMessages;
    }

    protected DmsManager getDmsManager() {
        return dmsManager;
    }

    public void setDmsManager(DmsManager dmsManager) {
        this.dmsManager = dmsManager;
    }

    protected BusinessNotes getBusinessNotes() {
        return businessNotes;
    }

    public void setBusinessNotes(BusinessNotes businessNotes) {
        this.businessNotes = businessNotes;
    }

    protected Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

}
