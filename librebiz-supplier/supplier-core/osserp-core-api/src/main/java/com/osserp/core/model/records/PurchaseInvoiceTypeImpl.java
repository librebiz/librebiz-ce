/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2009 1:23:36 PM 
 * 
 */
package com.osserp.core.model.records;

import org.jdom2.Element;

import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.purchasing.PurchaseInvoiceType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class PurchaseInvoiceTypeImpl extends AbstractBookingType implements PurchaseInvoiceType {

    private boolean createByOrder = false;
    private boolean directInvoiceBookingEnabled = false;
    private boolean stockAffecting = false;
    private boolean autoCreated = false;
    private boolean supplierSelectionEnabled = false;
    private boolean credit = false;
    private boolean supportingPayments = false;

    protected PurchaseInvoiceTypeImpl() {
        super();
    }

    public boolean isCreateByOrder() {
        return createByOrder;
    }

    public void setCreateByOrder(boolean createByOrder) {
        this.createByOrder = createByOrder;
    }

    public boolean isDirectInvoiceBookingEnabled() {
        return directInvoiceBookingEnabled;
    }

    public void setDirectInvoiceBookingEnabled(boolean directInvoiceBookingEnabled) {
        this.directInvoiceBookingEnabled = directInvoiceBookingEnabled;
    }

    public boolean isStockAffecting() {
        return stockAffecting;
    }

    protected void setStockAffecting(boolean stockAffecting) {
        this.stockAffecting = stockAffecting;
    }

    public boolean isAutoCreated() {
        return autoCreated;
    }

    protected void setAutoCreated(boolean autoCreated) {
        this.autoCreated = autoCreated;
    }

    public boolean isCredit() {
        return credit;
    }

    protected void setCredit(boolean credit) {
        this.credit = credit;
    }

    public boolean isSupplierSelectionEnabled() {
        return supplierSelectionEnabled;
    }

    protected void setSupplierSelectionEnabled(boolean supplierSelectionEnabled) {
        this.supplierSelectionEnabled = supplierSelectionEnabled;
    }

    public boolean isSupportingPayments() {
        return supportingPayments;
    }

    protected void setSupportingPayments(boolean supportingPayments) {
        this.supportingPayments = supportingPayments;
    }

    @Override
    public boolean isUseNameOnOutput() {
        return true;
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("createByOrder", createByOrder));
        root.addContent(JDOMUtil.createElement("directInvoiceBookingEnabled", directInvoiceBookingEnabled));
        root.addContent(JDOMUtil.createElement("stockAffecting", stockAffecting));
        root.addContent(JDOMUtil.createElement("autoCreated", autoCreated));
        root.addContent(JDOMUtil.createElement("supplierSelectionEnabled", supplierSelectionEnabled));
        root.addContent(JDOMUtil.createElement("credit", credit));
        root.addContent(JDOMUtil.createElement("supportingPayments", supportingPayments));
        return root;
    }
}
