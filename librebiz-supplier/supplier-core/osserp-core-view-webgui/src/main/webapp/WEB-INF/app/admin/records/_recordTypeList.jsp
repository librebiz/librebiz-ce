<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="list" value="${requestScope.recordTypeList}" />
<c:set var="selectionUrl" value="${view.baseLink}/select?id=" />
<c:if test="${!empty requestScope.selectionUrl}">
<c:set var="selectionUrl" value="${requestScope.selectionUrl}" />
</c:if>
<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="listid"><fmt:message key="id" /></th>
                    <th class="listprefix"><fmt:message key="prefix" /></th>
                    <th class="listname"><fmt:message key="name" /></th>
                    <th class="listnumber"><fmt:message key="number" /></th>
                    <th class="listyesno"><fmt:message key="stock" /></th>
                    <th class="listyesno">PDF</th>
                    <th class="action"></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="obj" items="${view.list}">
                    <tr>
                        <td class="listid"><o:out value="${obj.id}" /></td>
                        <td class="listprefix"><o:out value="${obj.numberPrefix}" /></td>
                        <td class="listname">
                            <v:link url="${selectionUrl}${obj.id}">
                                <o:out value="${obj.name}" />
                            </v:link>
                        </td>
                        <td class="listnumber"><fmt:message key="${obj.numberCreatorName}"/></td>
                        <td class="listyesno"><c:choose><c:when test="${obj.stockAware}"><fmt:message key="bigYes"/></c:when><c:otherwise><fmt:message key="bigNo"/></c:otherwise></c:choose></td>
                        <td class="listyesno"><c:choose><c:when test="${obj.supportingPrint}"><fmt:message key="bigYes"/></c:when><c:otherwise><fmt:message key="bigNo"/></c:otherwise></c:choose></td>
                        <td class="action"> </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
