<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.view}"/>
<c:set var="businessCase" value="${view.businessCase}"/>

<c:choose>
    <c:when test="${view.flowControlCancelMode}">
        <c:set var="item" value="${view.selectedItem}"/>
    </c:when>
    <c:otherwise>
        <c:set var="action" value="${view.selectedAction}"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${view.flowControlCancelMode}">
        <v:form name="noteForm" url="${view.baseLink}/cancelAction">
            
            <div class="col-md-6 panel-area panel-area-default">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><fmt:message key="cancellation"/>: <o:out value="${item.action.name}"/></h4>
                    </div>
                </div>
                <div class="panel-body">
    
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="cancellationNote"><fmt:message key="reasonOfCancellation" /></label>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <v:textarea styleClass="form-control" name="note" rows="6"/>
                            </div>
                        </div>
                    </div>
                    <div class="row next">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v:submitExit url="${view.baseLink}/disableEditMode" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <o:submit styleClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </v:form>   
    </c:when>
    <c:otherwise>
        <v:form name="noteForm" url="${view.baseLink}/addAction">
            
            <div class="col-md-6 panel-area panel-area-default">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><o:out value="${action.name}"/></h4>
                    </div>
                </div>
                <div class="panel-body">
    
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="date"><fmt:message key="date" /></label>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <v:date name="created" picker="true" styleClass="form-control date inline" />
                                <small style="margin-left: 5px;">(<fmt:message key="currentDateIfEmpty"/>)</small>
                            </div>
                        </div>
                    </div>
                    
                    <c:if test="${action.displayReverse}">
    
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="title"><fmt:message key="title" /></label>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <v:text name="headline" styleClass="form-control" maxlength="40"/>
                                    <small>(<fmt:message key="replacesActionName"/>)</small>
                                </div>
                            </div>
                        </div>
                    </c:if>
    
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="note"><fmt:message key="note" /></label>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <v:textarea styleClass="form-control" name="note" rows="6"/>
                            </div>
                        </div>
                    </div>
                    
                    <c:if test="${view.providesClosings}">
    
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="status"><fmt:message key="status" /></label>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <oc:select name="closing" options="${view.closings}" emptyPrompt="true" styleClass="form-control" />
                                    <o:permission role="fcs_closing_create,fcs_admin,event_config" info="permissionCreateFcsClosing">
                                        <o:img name="newdataIcon" onclick="javascript:showElement('newstatus');" title="createNewText" />
                                    </o:permission>
                                </div>
                            </div>
                        </div>
    
                        <div class="row" id="newstatus" style="visibility: hidden;">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="newStatus"><fmt:message key="new" /></label>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <v:text name="closingName" styleClass="form-control"/>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    
                    <div class="row next">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v:submitExit url="${view.baseLink}/disableEditMode" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <o:submit styleClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </v:form> 
    </c:otherwise>
</c:choose>
