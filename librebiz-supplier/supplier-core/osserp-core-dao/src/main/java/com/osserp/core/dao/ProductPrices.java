/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 28, 2011 10:55:04 AM 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;
import java.util.List;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSalesPrice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductPrices {

    /**
     * Creates a new sales price history entry for an product
     * @param employee performing the update
     * @param product
     * @param validFrom
     */
    void createSalesPriceHistory(Employee employee, Product product, Date validFrom);

    /**
     * Loads sales price history of an product
     * @param productId
     * @return priceHistory
     */
    List<ProductSalesPrice> loadPriceHistory(Long productId);

    /**
     * Updates validation date of price history entries
     * @param createdBy
     * @param priceHistoryId
     * @param validFrom
     * @return updated priceHistory
     */
    List<ProductSalesPrice> updatePriceHistory(Long createdBy, Long priceHistoryId, Date validFrom);

    /**
     * Provides the partner price by date
     * @param product
     * @param date
     * @param quantity required if product.priceByQuantity is enabled
     * @return partner price at date or current if none found, 0 if never configured before
     */
    Double getPartnerPrice(Product product, Date date, Double quantity);

    /**
     * Provides the purchase price for a date. By default average purchase price will be fetched. If no average purchase price could be calculated, last will be
     * used.
     * @param product
     * @param stockId
     * @param date
     * @return purchasePrice
     */
    Double getPurchasePrice(Product product, Long stockId, Date date);

}
