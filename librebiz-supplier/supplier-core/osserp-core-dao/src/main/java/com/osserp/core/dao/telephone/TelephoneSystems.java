/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 2:10:27 PM 
 * 
 */
package com.osserp.core.dao.telephone;

import java.util.List;

import com.osserp.core.telephone.TelephoneSystem;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneSystems {

    /**
     * Tries to find a telephone system by id
     * @param id
     * @return telephone system or null if not found
     */
    TelephoneSystem find(Long id);

    /**
     * Tries to find all telephone systems by type
     * @param type
     * @return telephone systems or null if not found
     */
    List<TelephoneSystem> findByType(Long type);

    /**
     * Tries to find telephone system by branch
     * @param branch
     * @return telephone system or null if not found
     */
    TelephoneSystem findByBranch(Long branch);

    /**
     * Provides all telephone systems
     * @return telephone systems
     */
    List<TelephoneSystem> getAll();

    /**
     * Creates new telephoneSystem
     * @param telephoneSystem
     * @return telephoneSystem
     */
    TelephoneSystem createTelephoneSystem(TelephoneSystem telephoneSystem);

    /**
     * Persists current telephone system values
     * @param telephoneSystem
     */
    void save(TelephoneSystem telephoneSystem);
}
