/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.cli

import com.osserp.common.beans.CommonClass

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ShellAware extends CommonClass {

    static final int ERROR = -10
    static final int UNDEFINED = -1
    static final int EXIT = 0
    static final int CHANGED = 1
    static final int SUCCESS = 10


    /**
     * Collects a value from stdin
     * @param prompt for value
     * @param defaultValue if user leaves blank
     * @return user input, defaultValue or null if user leaves blank while no default exists
     */
    protected String collectValue(String prompt, String defaultValue) {
        String output = ""
        if (defaultValue) {
            output = "Leave blank for default [$defaultValue]: "
        } else {
            output = "Leave blank to ignore: "
        }
        print output
        def input = readline()
        if (!input && defaultValue) {
            return defaultValue
        }
        return input
    }

    /**
     * Prompts for and collects all names from stdin
     * @param names
     * @param previous values (optional)
     * @return input values and status (CHANGED if all collected, EXIT otherwise)
     */
    protected Map collectCommonValues(List<String> names, Map previous) {
        Map result = (previous) ? previous : [status: UNDEFINED]
        println 'Please enter required values, [q]uit to exit:'

        for (name in names) {
            result[name] = collectCommonValue(name, previous)
            if (!result[name]) {
                result.status = EXIT
                return result
            }
        }
        result.status = CHANGED
        return result
    }

    private String collectCommonValue(String name, Map previous) {
        readvalue(name, (previous ? previous[name] : null))
    }

    /**
     * Analyses the status of an action input. Action status back or quit is
     * returned if matching modes selected. Otherwise result is undefined
     * @return status
     */
    protected int getExitStatus(String action) {
        if (action == 'b' || action == 'back') {
            return ShellDialog.CHANGED
        }
        if (action == 'q' || action == 'quit') {
            return ShellDialog.EXIT
        }
        return ShellDialog.UNDEFINED
    }

    /**
     * Asks a question
     * @param question
     * @param defaultValue
     * @return true if user answers y or Y
     */
    protected boolean ask(String question, boolean defaultValue) {
        print "$question? ${defaultValue ? '[Y/n]' : '[N/y]'}: "
        def mbflag = readline()
        if (!mbflag && defaultValue) {
            return true
        } else if (mbflag && ('y' == mbflag || 'Y' == mbflag)) {
            return true
        } else if (mbflag && ('n' == mbflag || 'N' == mbflag)) {
            return false
        }
        return false
    }

    /**
     * Performs a system lookup for current user
     * @return currentUser of the system
     */
    protected String getCurrentUser() {
        System.getProperty("user.name")
    }


    /**
     * Reads hidden input from stdin
     * @return hiddenline
     */
    protected String readhidden(String name) {
        def result = null
        Console console
        char[] passwd
        if ((console = System.console()) != null &&
        (passwd = console.readPassword("%s", "$name: ")) != null) {
            result = new String(passwd)
        }
        return result
    }


    /**
     * Reads a value from stdin
     * @return readline
     */
    protected String readline() {
        def input = new BufferedReader(new InputStreamReader(System.in))
        input.readLine()
    }


    /**
     * Reads a username from stdin, uses current user as default for missing input
     * @param previousSelected
     * @return userName
     */
    protected String readuser(String previousSelected) {
        def cu = previousSelected ?: getCurrentUser()
        println "Enter username, leave empty if ${cu}: "
        return readvalue(null, cu)
    }


    /**
     * Prompts for command line input and reads chars from stdin
     * @param prompt for input
     * @param previous value (optional)
     * @return input value or null if user enters [q[uit]]
     */
    protected String readvalue(String name, String[] previousValueContainer) {
        def previous = (previousValueContainer ? previousValueContainer[0] : null)
        boolean password = false
        if (name) {
            password = (name =~ /assword/)
            if (!password) {
                print name
                if (previous) {
                    print " - blank keeps current value [$previous]"
                }
                print ': '
            }
        }
        def input = null
        def status = UNDEFINED
        while (status < EXIT) {
            if (password) {
                input = readhidden(name)
            } else {
                input = readline()
            }
            if (input && (input == 'q' || input == 'quit')) {
                input = null
                status = EXIT
            } else if (input) {
                status = EXIT
            } else if (previous) {
                input = previous
                status = EXIT
            } else {
                print 'Input required, enter value or [q]uit to exit: '
            }
        }
        return input
    }


    /**
     * Aborts the application
     */
    protected def abort() {
        System.exit(1)
    }


    /**
     * Exits the application
     */
    protected def quit() {
        println 'Bye'
        System.exit(0)
    }
}
