/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 28-Aug-2006 13:14:32 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.customers.Customer;
import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.Records;
import com.osserp.core.dao.records.SalesCancellations;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesDeliveryNotes;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOffers;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.dao.records.SalesRecordQueries;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordType;
import com.osserp.core.sales.SalesRecordSummary;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRecordQueriesDao extends AbstractDao implements SalesRecordQueries {

    private OptionsCache options = null;
    private SalesCancellations cancellationsDao = null;
    private SalesCreditNotes creditNotesDao = null;
    private SalesDeliveryNotes deliveryNotesDao = null;
    private SalesInvoices invoicesDao = null;
    private SalesOrderDownpayments downpaymentsDao = null;
    private SalesOffers offersDao = null;
    private SalesOrders ordersDao = null;
    private String salesRecordSummaryView = null;
    private String salesRecordSummaryNorefView = null;

    public SalesRecordQueriesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache options,
            SalesCancellations cancellationsDao,
            SalesCreditNotes creditNotesDao,
            SalesDeliveryNotes deliveryNotesDao,
            SalesInvoices invoicesDao,
            SalesOrderDownpayments downpaymentsDao,
            SalesOffers offersDao,
            SalesOrders ordersDao) {
        super(jdbcTemplate, tables, sessionFactory);
        this.options = options;
        this.cancellationsDao = cancellationsDao;
        this.creditNotesDao = creditNotesDao;
        this.deliveryNotesDao = deliveryNotesDao;
        this.invoicesDao = invoicesDao;
        this.downpaymentsDao = downpaymentsDao;
        this.offersDao = offersDao;
        this.ordersDao = ordersDao;
        this.salesRecordSummaryView = getTable("salesRecordSummaryView");
        this.salesRecordSummaryNorefView = getTable("salesRecordSummaryNorefView"); 
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.SalesRecordQueries#getAccountBalance(com.osserp.core.customers.Customer)
     */
    public Double getAccountBalance(Customer customer) {
        String sql = "SELECT get_customer_account_balance(?)";
        final Object[] params = { customer.getId() };
        final int[] types = { Types.BIGINT };
        List<Double> result = (List<Double>) jdbcTemplate.query(
                sql,
                params,
                types,
                getDoubleRowMapper());
        return (empty(result) ? 0d : result.get(0));
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.SalesRecordQueries#getSummary(com.osserp.core.customers.Customer)
     */
    public List<SalesRecordSummary> getSummary(Customer customer) {
        StringBuilder sql = new StringBuilder(SalesRecordSummaryRowMapper.SELECT_FROM);
        sql.append(salesRecordSummaryView).append(" WHERE customer = ? ORDER BY created desc");
        final Object[] params = { customer.getId() };
        final int[] types = { Types.BIGINT };
        return (List<SalesRecordSummary>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                SalesRecordSummaryRowMapper.getReader(options));
    }
    
    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.SalesRecordQueries#getUnreferencedSummary(java.util.Date, java.util.Date)
     */
    public List<SalesRecordSummary> getUnreferencedSummary(Date from, Date til) {
        StringBuilder sql = new StringBuilder(SalesRecordSummaryRowMapper.SELECT_FROM);
        sql.append(salesRecordSummaryNorefView).append(" WHERE created >= ? AND created <= ? ORDER BY created desc");
        final Object[] params = { from, til };
        final int[] types = { Types.TIMESTAMP, Types.TIMESTAMP };        
        return (List<SalesRecordSummary>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                SalesRecordSummaryRowMapper.getReader(options));
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.AccountingRecordQueries#getSummaryByDate(java.lang.Long, java.util.Date, java.util.Date)
     */
    public List<SalesRecordSummary> getSummaryByDate(Long company, Date startDate, Date stopDate) {
        assert startDate != null && stopDate != null;
        StringBuilder sql = new StringBuilder(SalesRecordSummaryRowMapper.SELECT_FROM);
        sql.append(salesRecordSummaryView)
            .append(" WHERE company = ? AND created >= ? AND created <= ? ORDER BY created desc");
        final Object[] params = { company, startDate, stopDate };
        final int[] types = { Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP };
        return (List<SalesRecordSummary>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                SalesRecordSummaryRowMapper.getReader(options));
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.records.SalesRecordQueries#getUnreleased(java.lang.Long, java.lang.Long)
     */
    public List<RecordDisplay> getUnreleased(Long recordType, Long company) {
        Records dao = getDaoByType(recordType);
        return dao.findUnreleased(company);
    }

    private Records getDaoByType(Long recordType) {
        if (RecordType.SALES_ORDER.equals(recordType)) {
            return ordersDao;
        } else if (RecordType.SALES_DELIVERY_NOTE.equals(recordType)) {
            return deliveryNotesDao;
        } else if (RecordType.SALES_CREDIT_NOTE.equals(recordType)) {
            return creditNotesDao;
        } else if (RecordType.SALES_CANCELLATION.equals(recordType)) {
            return cancellationsDao;
        } else if (RecordType.SALES_DOWNPAYMENT.equals(recordType)) {
            return downpaymentsDao;
        } else if (RecordType.SALES_INVOICE.equals(recordType)) {
            return invoicesDao;
        } else if (RecordType.SALES_OFFER.equals(recordType)) {
            return offersDao;
        }
        throw new IllegalArgumentException();
    }
}
