<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<o:logger write="page invocation" level="debug"/>
<c:if test="${!empty sessionScope.contactView and !empty requestScope.paymentUrl}">
    <c:set var="contactView" value="${sessionScope.contactView}" />
    <c:set var="url" value="${requestScope.paymentUrl}" />
    <c:set var="contact" value="${contactView.bean}" />
    <c:set var="payment" value="${contact.paymentAgreement}" />
    <c:choose>
        <c:when test="${!contactView.paymentAgreementEditMode}">
            <tr>
                <td><o:permission role="purchasing,accounting,supplier_change,customer_service,sales,sales_executive,accounting_accountant_deny" info="permissionPaymentAgreementShowEdit">
                        <c:choose>
                            <c:when test="${!contact.grantContactNone}">
                                <a href="<c:url value="${url}"><c:param name="method" value="enablePaymentAgreementEdit"/></c:url>" title="<fmt:message key="changeDetails"/>">
                                    <fmt:message key="paymentTarget" />
                                </a>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="paymentTarget" />
                            </c:otherwise>
                        </c:choose>
                    </o:permission> <o:forbidden role="purchasing,accounting,supplier_change,customer_service,sales,sales_executive,accounting_accountant_deny" info="permissionPaymentAgreementShowEdit">
                        <fmt:message key="paymentTarget" />
                    </o:forbidden></td>
                <td><c:choose>
                        <c:when test="${empty payment.paymentCondition.id}">
                            <fmt:message key="manualAssignment" />
                        </c:when>
                        <c:otherwise>
                            <a href="javascript:ojsCommon.toggleDisplay('paymentAgreement', 'table-row-group');" title="<fmt:message key="display"/>"> <oc:options name="recordPaymentConditions" value="${payment.paymentCondition.id}" />
                            </a>
                        </c:otherwise>
                    </c:choose></td>
            </tr>
            <tbody id="paymentAgreement" style="display: none;">
                <c:choose>
                    <c:when test="${payment.hidePayments}">
                        <tr>
                            <td>&nbsp;</td>
                            <td><fmt:message key="hidePaymentStepsDisplayHint" /></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td>&nbsp;</td>
                            <td><span class="boldtext"><fmt:formatNumber value="${payment.downpaymentPercent}" type="percent" /></span> <oc:options name="recordPaymentTargets" value="${payment.downpaymentTargetId}" />, <br /> <span class="boldtext"><fmt:formatNumber
                                        value="${payment.deliveryInvoicePercent}" type="percent" /></span> <oc:options name="recordPaymentTargets" value="${payment.deliveryInvoiceTargetId}" />, <br /> <span class="boldtext"><fmt:formatNumber
                                        value="${payment.finalInvoicePercent}" type="percent" /></span> <oc:options name="recordPaymentTargets" value="${payment.finalInvoiceTargetId}" /></td>
                        </tr>
                    </c:otherwise>
                </c:choose>
                <c:if test="${payment.taxFree}">
                    <tr>
                        <td><fmt:message key="taxFreeTitle" /></td>
                        <td><oc:options name="taxFreeDefinitions" value="${payment.taxFreeId}" /></td>
                    </tr>
                </c:if>
                <c:if test="${!empty payment.note}">
                    <tr>
                        <td>&nbsp;</td>
                        <td><fmt:message key="specialAgreement" />:</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><o:out value="${payment.note}" /></td>
                    </tr>
                </c:if>
                <c:if test="${!empty payment.billingAccountId}">
                    <tr>
                        <td>&nbsp;</td>
                        <td><fmt:message key="billingAccount" />: <oc:options name="bankAccounts" value="${payment.billingAccountId}" /></td>
                    </tr>
                </c:if>
            </tbody>
        </c:when>
        <c:otherwise>
            <o:form id="paymentAgreementForm" name="customerForm" url="${url}">
                <input type="hidden" name="method" value="updatePaymentAgreement" />
                <tr>
                    <td><fmt:message key="payment" /></td>
                    <td><oc:select name="paymentConditionId" value="${payment.paymentCondition.id}" options="recordPaymentConditions" prompt="false" /></td>
                </tr>
                <c:choose>
                    <c:when test="${payment.hidePayments}">
                        <tr>
                            <td class="formLabelRight"> </td>
                            <td>
                                <span><input type="checkbox" name="hidePayments" checked="checked" /></span>
                                <span style="margin-left:10px;"><fmt:message key="hidePaymentStepsDisplayHint" /></span>
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td class="formLabelRight"> </td>
                            <td>
                                <span><input type="checkbox" name="hidePayments" /></span>
                                <span style="margin-left:10px;"><fmt:message key="hidePaymentStepsDisplayHint" /></span>
                            </td>
                        </tr>
                    </c:otherwise>
                </c:choose>

                <tr>
                    <td> </td>
                    <td>
                        <table>
                            <tr>
                                <td style="width:25%"><oc:selectPercentage name="downpaymentPercent" value="${payment.downpaymentPercent}" options="billingPercentages" onchange="calculatePaymentAgreement();" prompt="false" /></td>
                                <td style="width:75%"><oc:select name="downpaymentTargetId" value="${payment.downpaymentTargetId}" options="recordPaymentTargets" prompt="false" /></td>
                            </tr>
                            <tr>
                                <td style="width:25%"><oc:selectPercentage name="deliveryInvoicePercent" value="${payment.deliveryInvoicePercent}" options="billingPercentages" onchange="calculatePaymentAgreement();" prompt="false" /></td>
                                <td style="width:75%"><oc:select name="deliveryInvoiceTargetId" value="${payment.deliveryInvoiceTargetId}" options="recordPaymentTargets" prompt="false" /></td>
                            </tr>
                            <input type="hidden" name="finalInvoicePercent" value="<o:out value="${payment.finalInvoicePercent}"/>" />
                            <tr>
                                <td style="width:25%; text-align:right;"><span id="fip" style="text-align: right" class="selectPercentage"><o:number value="${payment.finalInvoicePercent}" format="percent" />%</span></td>
                                <td style="width:75%"><oc:select name="finalInvoiceTargetId" value="${payment.finalInvoiceTargetId}" options="recordPaymentTargets" prompt="false" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td><fmt:message key="specialAgreement" />:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><textarea class="form-control" name="note" cols="46" rows="3"><o:out value="${payment.note}" /></textarea></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><c:choose>
                            <c:when test="${payment.taxFree}">
                                <input type="checkbox" name="taxFree" checked="checked" />
                                <fmt:message key="taxFreeTitle" />
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="taxFree" />
                                <fmt:message key="taxFreeTitle" />
                            </c:otherwise>
                        </c:choose></td>
                </tr>
                <tr>
                    <td><fmt:message key="reason" /></td>
                    <td><oc:select name="taxFreeId" value="${payment.taxFreeId}" options="taxFreeDefinitions" prompt="false" /></td>
                </tr>
                <tr>
                    <td><fmt:message key="billingAccount" /></td>
                    <td><oc:select name="billingAccountId" value="${payment.billingAccountId}" options="bankAccounts" prompt="true" /></td>
                </tr>
                <tr>
                    <td class="row-submit">
                        <a href="<c:url value="${url}?method=disablePaymentAgreementEdit"/>"><input type="button" value="<fmt:message key="break"/>" class="cancel" /></a>
                    </td>
                    <td class="row-submit">
                        <o:submit/>
                    </td>
                </tr>
            </o:form>
        </c:otherwise>
    </c:choose>
</c:if>