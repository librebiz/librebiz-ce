/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2008 
 * 
 */
package com.osserp.core.contacts;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.mail.EmailAddress;

import com.osserp.core.employees.Employee;
import com.osserp.core.mail.NewsletterRecipient;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ContactManager extends PersonManager {

    /**
     * Creates an initialized contact object to use as vo on create
     * @return contact object with some default values set
     */
    Contact createEmpty();

    /**
     * Creates a business contact
     * @param user
     * @param type
     * @param companyAffix
     * @param companyName
     * @param note
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param website
     * @param email
     * @param phoneNumber
     * @param faxNumber
     * @param mobileNumber
     * @param initialStatus
     * @return contact
     * @throws ClientException if validation failed
     */
    Contact create(
            DomainUser user,
            Long type,
            String companyAffix,
            String companyName,
            String note,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String website,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException;

    /**
     * Creates a freelancer or SME contact
     * @param user
     * @param type
     * @param companyAffix
     * @param companyName
     * @param salutationId
     * @param titleId
     * @param firstName
     * @param lastName
     * @param note
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param website
     * @param email
     * @param phoneNumber
     * @param faxNumber
     * @param mobileNumber
     * @param initialStatus
     * @return contact new created contact
     * @throws ClientException
     */
    Contact create(
            DomainUser user,
            Long type,
            String companyAffix,
            String companyName,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            String note,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String website,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException;

    /**
     * Creates a private contact
     * @param user
     * @param type
     * @param salutationId
     * @param titleId
     * @param firstName
     * @param lastName
     * @param spouseSalutationId
     * @param spouseTitleId
     * @param spouseFirstName
     * @param spouseLastName
     * @param birthDate
     * @param note
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param email
     * @param phoneNumber
     * @param faxNumber
     * @param mobileNumber
     * @param initialStatus
     * @return contact
     * @throws ClientException if validation failed
     */
    Contact create(
            DomainUser user,
            Long type,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            Long spouseSalutationId,
            Long spouseTitleId,
            String spouseFirstName,
            String spouseLastName,
            Date birthDate,
            String note,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException;

    /**
     * Creates an initial contact person for companies
     * @param contactRef
     * @param user
     * @param salutationId
     * @param titleId
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param position
     * @param section
     * @param office
     * @return new created contact person
     * @throws ClientException if validation failed
     */
    Contact create(
            Contact contactRef,
            DomainUser user,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office)
            throws ClientException;

    /**
     * Creates a new contact person
     * @param contactRef related to new contact person
     * @param user
     * @param salutationId
     * @param titleId
     * @param firstName
     * @param lastName
     * @param birthDate
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param position
     * @param section
     * @param office
     * @param note
     * @param email
     * @param phoneNumber
     * @param faxNumber
     * @param mobileNumber
     * @return contact new created
     * @throws ClientException if validation failed
     */
    Contact create(
            Contact contactRef,
            DomainUser user,
            Long salutationId,
            Long titleId,
            String firstName,
            String lastName,
            Date birthDate,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office,
            String note,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber)
            throws ClientException;
    
    /**
     * Updates a contact
     * @param user
     * @param contact
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param street
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param position
     * @param section
     * @param office
     * @param website
     * @param birthdate
     * @param spouseSalutation
     * @param spouseTitle
     * @param spouseFirstName
     * @param spouseLastName
     * @param grantContactPerEmail
     * @param grantContactPerEmailNote
     * @param grantContactPerPhone
     * @param grantContactPerPhoneNote
     * @param grantContactNone
     * @param grantContactNoneNote
     * @param newsletterConfirmationValue
     * @param newsletterConfirmationNoteValue
     * @param vipValue
     * @param vipNoteValue
     * @return contact
     * @throws ClientException if validation failed
     * @throws PermissionException if required permission not grant
     */
    Contact updateContact(
            DomainUser user,
            Contact contact,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office,
            String website,
            Date birthdate,
            Salutation spouseSalutation,
            Option spouseTitle,
            String spouseFirstName, 
            String spouseLastName,
            boolean grantContactPerEmail, 
            String grantContactPerEmailNote, 
            boolean grantContactPerPhone, 
            String grantContactPerPhoneNote,
            boolean grantContactNone,
            String grantContactNoneNote,
            boolean newsletterConfirmationValue, 
            String newsletterConfirmationNoteValue,
            boolean vipValue, 
            String vipNoteValue) throws ClientException, PermissionException;
    
    /**
     * Updates values of an existing person
     * @param user
     * @param contact
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param street
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param position
     * @param section
     * @param office
     * @return person with updated values
     * @throws ClientException
     * @throws PermissionException
     */
    ContactPerson updateContactPerson(
            DomainUser user,
            ContactPerson contact,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office) throws ClientException, PermissionException;
    
    
    /**
     * Provides the contact type of contact persons
     * @return contactType contactPerson type
     */
    ContactType getContactPersonType();

    /**
     * Provides all contacts receiving or not receiving a newsletter
     * @param active indicates whether receiving or not receiving should be provided
     * @return recipients as newsletter recipiemnt objects
     */
    List<NewsletterRecipient> findNewsletterRecipients(boolean active);

    /**
     * Updates the newsletter status of the contacts.
     * @param contacts as array of contact ids
     * @param status
     */
    void updateNewsletterStatus(Long[] contacts, boolean status);

    /**
     * Indicates that contact is deletable
     * @param contact
     * @return deletable
     */
    boolean isDeletable(Contact contact);

    /**
     * Indicates limited validation
     * @return requiredValidationOnly
     */
    boolean isRequiredValidationOnly();

    /**
     * Adds a new email address
     * @param user
     * @param contact
     * @param type
     * @param email
     * @param primary
     * @param aliasOf
     * @throws ClientException if validation failed or address already exists
     */
    void addEmail(Employee user, Contact contact, Long type, String email, boolean primary, Long aliasOf) throws ClientException;

    /**
     * Updates an email address
     * @param user
     * @param contact
     * @param address
     * @param type
     * @param email
     * @param primary
     * @param aliasOf
     * @throws ClientException if validation failed or address already exists
     */
    void updateEmail(Employee user, Contact contact, EmailAddress address, Long type, String email, boolean primary, Long aliasOf) throws ClientException;

    /**
     * Deletes an email address
     * @param contact
     * @param id
     */
    void deleteEmail(Contact contact, Long id);

    /**
     * Adds a phone number
     * @param user
     * @param contact
     * @param device
     * @param type
     * @param number
     * @param note
     * @param primary
     * @throws ClientException
     */
    void addPhone(
            Employee user,
            Contact contact,
            Long device,
            Long type,
            String number,
            String note,
            boolean primary) throws ClientException;

    /**
     * Updates existing phone number
     * @param user
     * @param contact
     * @param phone
     * @param type
     * @param country
     * @param prefix
     * @param number
     * @param note
     * @param primary
     * @throws ClientException
     */
    void updatePhone(
            Employee user,
            Contact contact,
            Phone phone,
            Long type,
            String country,
            String prefix,
            String number,
            String note,
            boolean primary) throws ClientException;

    /**
     * Deletes a phone number
     * @param user
     * @param contact
     * @param id
     */
    void deletePhone(Employee user, Contact contact, Long id);

    /**
     * Updates the branch office flag
     * @param id
     * @param branch
     */
    void updateBranchFlag(Long id, boolean branch);
}
