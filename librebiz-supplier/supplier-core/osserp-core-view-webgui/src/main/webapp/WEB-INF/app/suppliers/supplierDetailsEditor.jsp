<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="supplierDetailsEditorView" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
    </tiles:put>

    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="supplierDetailsEditorContent">
            <v:form id="supplierDetailsEditorForm" url="${view.baseLink}/save">
                <div class="row">
                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <o:out value="${view.bean.displayName}" />
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-body">

                                <div class="row next">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="shortkey"><fmt:message key="shortkey" /></label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <v:text styleClass="form-control" name="shortkey" value="${view.bean.shortkey}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row next">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <v:submitExit url="${view.baseLink}/disableEditMode" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <o:submit styleClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </v:form>
        </div>
    </tiles:put>
</tiles:insert>
