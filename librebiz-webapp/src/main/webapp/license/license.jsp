<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>

<div class="content-container">
    <div class="main-content">
        <h4 id="coreApplication">Librebiz ERP and CRM</h4>
        <h5 id="copyright-core">
            Copyright &copy; 2017-<o:date format="year" />
            <a href="http://librebiz.org" target="_blank"> Librebiz Developers</a> - 
            All rights reserved.
        </h5>
        <c:import url="/license/license_header.jsp"/>
        <br />
        <h4 id="coreLibraries" class="content-next">Core Libraries</h4>
        <h5 id="copyright-libs">
            Copyright &copy; 2001-<o:date format="year"/> <a href="http://osserp.com" target="_blank">
            The original author or authors</a> as stated by the @author tag in the
            Source Form Files.
        </h5>
        <c:import url="/license/license_info.jsp"/>
        <br />
        <h4 id="thirdpartyLibraries">Third-Party Frameworks and Libraries</h4>
        <p>
            The distribution includes third-party software packages licensed under
            various open source licenses. See below for complete list.
        </p>
        <br />
        <h4>Trademarks</h4>
        <p>
            All brand and product names are trademarks, registered trademarks or
            service marks of their respective holders.
        </p>
        <br />
        <h4>Appendix A - Product License Text</h4>
        <h5>GNU Affero General Public License Version 3</h5>
        <p id="agpl3">
            <iframe src="<c:url value="/license/text/agpl-3"/>" class="license-text" id="agpl_3" name="GNU Affero General Public License Version 3"></iframe>
        </p>
        <br />
        <h4>Appendix B - Third Party Software License List</h4>
        <p id="thirdparty-list">
            <iframe src="<c:url value="/THIRD-PARTY"/>" class="license-text" name="THIRD-PARTY"></iframe>
        </p>
        <p>
            Commercial support available. Contact <a href="mailto:developers@librebiz.org">developers@librebiz.org</a> for details.
        </p>
    </div>
</div>
