/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.transaction.annotation.Transactional;

import org.apache.velocity.app.VelocityEngine;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ErrorCode;
import com.osserp.common.mail.MailSender;
import com.osserp.common.service.TemplateManager;
import com.osserp.common.service.impl.AbstractTemplateMailSender;
import com.osserp.common.util.StringUtil;

import com.osserp.core.dao.MailTemplates;
import com.osserp.core.mail.MailTemplate;
import com.osserp.core.model.MailTemplateImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Transactional
public class MailTemplatesDao extends AbstractTemplateMailSender implements MailTemplates {
    private static Logger log = LoggerFactory.getLogger(MailTemplatesDao.class.getName());

    private SessionFactory sessionFactory = null;

    protected MailTemplatesDao(
            SessionFactory sessionFactory,
            MailSender sender,
            VelocityEngine engine) {
        super(sender, engine);
        this.sessionFactory = sessionFactory;
    }

    public List<MailTemplate> findTemplates() {
        return getCurrentSession().createQuery("from " + MailTemplateImpl.class.getName()
                + " o order by o.name").list();
    }

    public MailTemplate findTemplate(String name) {
        List<MailTemplate> list = getCurrentSession().createQuery(
                "from " + MailTemplateImpl.class.getName()
                + " o where o.template = :name")
                .setParameter("name", name).list();
        return (list.isEmpty() ? null : list.get(0));
    }

    private MailTemplate getMailTemplate(String name) {
        MailTemplate tpl = findTemplate(name);
        if (tpl == null) {
            throw new BackendException("MailTemplate not exists [name=" + name + "]");
        }
        return tpl;
    }

    public MailTemplate getTemplate(Long id) {
        return (MailTemplate) getCurrentSession().load(MailTemplateImpl.class, id);
    }

    public void update(MailTemplate mailTemplate) {
        getCurrentSession().saveOrUpdate(mailTemplate);
    }

    public final void send(String templateName, Map values) {
        MailTemplate mailTemplate = getMailTemplate(templateName);
        if (!mailTemplate.isDisabled()) {
            String[] recipients = getRecipients(values, mailTemplate);
            String originator = getOriginator(values, mailTemplate);
            String subject = getSubject(values, mailTemplate);
            send(originator, subject, recipients, templateName, values);
        } else if (log.isInfoEnabled()) {
            log.info("Ignoring to send mail since template is disabled: " + templateName);
        }
    }

    public final void send(String templateName, String recipient, Map values) {
        send(templateName, new String[] { recipient }, values);
    }

    public final void send(String templateName, String[] recipients, Map values) {
        MailTemplate mailTemplate = getMailTemplate(templateName);
        if (!mailTemplate.isDisabled()) {
            String[] rcpts = (recipients == null ? getRecipients(values, mailTemplate) : recipients);
            String originator = getOriginator(values, mailTemplate);
            String subject = getSubject(values, mailTemplate);
            send(originator, subject, rcpts, templateName, values);
        } else if (log.isInfoEnabled()) {
            log.info("Ignoring to send mail since template is disabled: " + templateName);
        }
    }

    /**
     * Method first tries to determine the originator by {@link com.osserp.common.service.TemplateManager#MAIL_ORIGINATOR} and then looks up for originator
     * property in mailTemplate.
     * @param values may contain an originator
     * @param mailTemplate may contain an originator
     * @return originator
     * @throws BackendException if no originator defined
     */
    private String getOriginator(Map values, MailTemplate mailTemplate) {
        String result = (String) values.get(TemplateManager.MAIL_ORIGINATOR);
        if (isNotSet(result) && mailTemplate != null) {
            result = mailTemplate.getOriginator();
        }
        if (isNotSet(result)) {
            log.error("getOriginator() failed: Unable to locate originator in mail template or map");
            throw new BackendException(ErrorCode.ORIGINATOR_MISSING);
        }
        return result;
    }

    /**
     * Method first tries to determine the recipients by {@link com.osserp.common.service.TemplateManager#MAIL_RECIPIENTS} and then looks up for recipients
     * property in mailTemplate.
     * @param values may contain a recipient
     * @param mailTemplate may contain a recipient
     * @return recipients
     * @throws BackendException if no recipients defined
     */
    private String[] getRecipients(Map values, MailTemplate mailTemplate) {
        String recipients = (String) values.get(TemplateManager.MAIL_RECIPIENTS);
        if (isNotSet(recipients) && mailTemplate != null) {
            recipients = mailTemplate.getRecipients();
        }
        String[] rcpts = StringUtil.getTokenArray(recipients);
        if (rcpts == null || rcpts.length < 1) {
            log.error("getRecipients() failed: Unable to locate recipients in mail template or map");
            throw new BackendException(ErrorCode.RECIPIENT_MISSING);
        }
        return rcpts;
    }

    /**
     * Method first tries to determine the subject by {@link com.osserp.common.service.TemplateManager#MAIL_SUBJECT} and then looks up for subject property in
     * mailTemplate.
     * @param values may contain a subject
     * @param mailTemplate may contain a subject
     * @return subject
     * @throws BackendException if no subject defined
     */
    private String getSubject(Map values, MailTemplate mailTemplate) {
        String result = (String) values.get(TemplateManager.MAIL_SUBJECT);
        if (isNotSet(result) && mailTemplate != null) {
            result = mailTemplate.getSubject();
        }
        if (isNotSet(result)) {
            log.error("getSubject() failed: Unable to locate subject in mail template or map");
            throw new BackendException(ErrorCode.SUBJECT_MISSING);
        }
        return result;
    }

    protected Session getCurrentSession() {
        try {
            return this.sessionFactory.getCurrentSession();
        } catch (Exception e) {
            log.error("getCurrentSession: failed [message=" + e.getMessage() + "]", e);
            throw new BackendException(e);
        }
    }
}
