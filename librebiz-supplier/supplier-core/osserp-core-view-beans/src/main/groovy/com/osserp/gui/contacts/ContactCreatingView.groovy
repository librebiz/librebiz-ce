/**
 *
 * Copyright (C) 2006, 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 17, 2006 
 * Created on Nov 7, 2012 (Groovy implementation) 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Constants

import com.osserp.core.AddressLocality
import com.osserp.core.Options
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactCountry
import com.osserp.core.contacts.PersonManager

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class ContactCreatingView extends ContactEditingView {
    private static Logger logger = LoggerFactory.getLogger(ContactCreatingView.class.getName())
    
    List<Contact> existingContacts = []
    Long countryIdContactPerson = Constants.GERMANY 

    ContactCreatingView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            Contact vo = contactManager.createEmpty()
            vo.address.country = Constants.GERMANY
            bean = vo
        }
    }
    
    @Override
    protected PersonManager getPersonManager() {
        contactManager
    }

    void selectCountry() {
        countrySelection(form.getLong('id'))
    }
    
    protected void countrySelection(Long id) {
        bean.address.country = id
        if (bean.address.country) {
            ContactCountry obj = getOption(Options.COUNTRY_NAMES, bean.address.country)
            if (obj.customFederalState) {
                customFederalState = true
            } else {
                customFederalState = false
            }
        }
        logger.debug("countrySelection: done [country=${bean.address.country}, customFederalState=${customFederalState}]")
    }
    
    /**
     * Performs a locality lookup by zipcode and adds city to form field if unique result was found
     * @param zipcode
     */
    protected void localitySelection(String zipcode) {
        bean.address.zipcode = zipcode
        List<AddressLocality> localities = systemConfigManager.getLocalitySelection(bean.address.zipcode)
        if (localities.size() == 1) {
            bean.address.city = localities.get(0).city
            form.setValue('city', bean.address.city)
            logger.debug("localitySelection: done [zipcode=${bean.address.zipcode}, city=${bean.address.city}]")
        }
    }

    void selectType() {
        selectSelectedType(form.getLong('id'))
    }
}
