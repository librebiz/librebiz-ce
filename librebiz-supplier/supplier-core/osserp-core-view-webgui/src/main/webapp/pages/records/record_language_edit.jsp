<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="view" value="${sessionScope.recordView}" />
<c:set var="record" value="${view.record}" />
<tr>
    <td><fmt:message key="language" /></td>
    <td class="right">
        <select name="language" style="width: 100px; text-align: right;">
            <c:choose>
                <c:when test="${empty record.language}">
                    <option value="de" selected="selected">DE</option>
                    <option value="en">EN</option>
                </c:when>
                <c:when test="${record.language == 'de'}">
                    <option value="de" selected="selected">DE</option>
                    <option value="en">EN</option>
                </c:when>
                <c:when test="${record.language == 'en'}">
                    <option value="en" selected="selected">EN</option>
                    <option value="de">DE</option>
                </c:when>
            </c:choose>
        </select>
    </td>
</tr>
