/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 10, 2007 4:34:05 PM 
 * 
 */
package com.osserp.core.model.events;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.Parameter;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventConfig;
import com.osserp.core.events.EventTicket;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventTicketImpl extends AbstractEntity
        implements EventTicket {

    private EventConfig config = null;
    private Long configTypeId = null;
    private String description = null;
    private String message = null;
    private String headline = null;
    private boolean canceled = false;
    private boolean sent = false;
    private boolean closed = false;
    private Long callerId = null;
    private List<Parameter> parameters = new ArrayList<Parameter>();
    private List<EventTicketRecipient> recipientsList = new ArrayList<EventTicketRecipient>();

    protected EventTicketImpl() {
        super();
    }

    /**
     * Action ticket constructor
     * @param action
     * @param createdBy
     * @param reference
     * @param description
     * @param message
     * @param headline
     * @param canceled
     */
    public EventTicketImpl(
            EventAction action,
            Long createdBy,
            Long reference,
            String description,
            String message,
            String headline,
            boolean canceled) {

        super((Long) null, reference, createdBy);
        this.config = action;
        this.configTypeId = ((action != null && action.getType() != null) ? action.getType().getId() : null);
        this.description = description;
        this.message = message;
        this.headline = headline;
        this.canceled = canceled;
    }

    /**
     * Termination ticket constructor
     * @param callerId
     * @param configTypeId
     * @param createdBy
     * @param reference
     * @param canceled
     */
    public EventTicketImpl(
            Long callerId,
            Long configTypeId,
            Long createdBy,
            Long reference,
            boolean canceled) {

        super((Long) null, reference, createdBy);
        this.callerId = callerId;
        this.configTypeId = configTypeId;
        this.canceled = canceled;
    }

    public EventConfig getConfig() {
        return config;
    }

    protected void setConfig(EventConfig config) {
        this.config = config;
    }

    public Long getConfigTypeId() {
        return configTypeId;
    }

    protected void setConfigTypeId(Long configTypeId) {
        this.configTypeId = configTypeId;
    }

    public String getDescription() {
        return description;
    }

    protected void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public Long getCallerId() {
        return callerId;
    }

    public void setCallerId(Long callerId) {
        this.callerId = callerId;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    protected void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public void addParameter(String name, String value) {
        this.parameters.add(new EventTicketParameter(this, name, value));
    }

    public Long[] getRecipients() {
        int size = recipientsList.size();
        Long[] recipients = new Long[size];
        for (int i = 0; i < size; i++) {
            recipients[i] = recipientsList.get(i).getRecipient();
        }
        return recipients;
    }

    public void addRecipient(Long recipient) {
        this.recipientsList.add(new EventTicketRecipient(this, recipient));
    }

    public boolean isCanceled() {
        return canceled;
    }

    protected void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public void close() {
        this.closed = true;
    }

    protected List<EventTicketRecipient> getRecipientsList() {
        return recipientsList;
    }

    protected void setRecipientsList(List<EventTicketRecipient> recipientsList) {
        this.recipientsList = recipientsList;
    }
}
