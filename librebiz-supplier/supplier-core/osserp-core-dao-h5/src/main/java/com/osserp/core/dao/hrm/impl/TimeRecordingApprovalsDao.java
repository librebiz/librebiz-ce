/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 4, 2011 2:33:12 PM 
 * 
 */
package com.osserp.core.dao.hrm.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.hrm.TimeRecordingApprovals;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordStatus;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingApproval;
import com.osserp.core.model.hrm.TimeRecordImpl;
import com.osserp.core.model.hrm.TimeRecordingApprovalImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingApprovalsDao extends AbstractTimeRecordingAware implements TimeRecordingApprovals {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingApprovalsDao.class.getName());

    protected TimeRecordingApprovalsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache options,
            String timeRecordingTableKey,
            String timeRecordingPeriodTableKey,
            String timeRecordTableKey,
            String timeRecordCorrectionsTableKey,
            String timeRecordingApprovalsTableKey) {
        super(jdbcTemplate, tables, sessionFactory, options, timeRecordingTableKey,
                timeRecordingPeriodTableKey, timeRecordTableKey,
                timeRecordCorrectionsTableKey, timeRecordingApprovalsTableKey);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.TimeRecordings#createApproval(com.osserp.core.employees.Employee, com.osserp.core.hrm.TimeRecording,
     * com.osserp.core.hrm.TimeRecord, java.util.Date, java.util.Date, java.lang.String)
     */
    public TimeRecordingApproval createApproval(
            Employee user,
            TimeRecording recording,
            TimeRecord record,
            Date start,
            Date end,
            String note) {
        List<TimeRecordCorrection> corrections = fetchCorrections(record);
        TimeRecordCorrection correction = (corrections.isEmpty() ? null : corrections.get(0));
        TimeRecordingApprovalImpl approval = new TimeRecordingApprovalImpl(user, recording, record, correction, start, end, note);
        save(approval);
        return approval;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.TimeRecordings#createApproval(com.osserp.core.employees.Employee, com.osserp.core.hrm.TimeRecording,
     * com.osserp.core.hrm.TimeRecordType, java.util.Date, java.util.Date, java.lang.String)
     */
    public TimeRecordingApproval createApproval(
            Employee user,
            TimeRecording recording,
            TimeRecordType type,
            Date start,
            Date end,
            String note) {
        TimeRecordingApprovalImpl approval = new TimeRecordingApprovalImpl(user, recording, type, start, end, note);
        save(approval);
        return approval;
    }

    public TimeRecordingApproval getApproval(Long id) {
        return (TimeRecordingApproval) getCurrentSession().load(TimeRecordingApprovalImpl.class.getName(), id);
    }

    public List<TimeRecordingApproval> getApprovals(Employee employee) {
        return getCurrentSession().createQuery(
                "from " + TimeRecordingApprovalImpl.class.getName() +
                " o where employeeId = :employeeId order by o.id")
                .setParameter("employeeId", employee.getId()).list();
    }

    public void closeApproval(Employee user, TimeRecordingApproval approval, boolean approved) {
        if (approval.getRecord() != null) {
            TimeRecord record = approval.getRecord();
            if (approved) {
                TimeRecordStatus status = getStatus(TimeRecordStatus.APPROVED);
                record.update(status, null);
                save(record);
                approval.updateStatus(user, TimeRecordingApproval.STATUS_APPROVED, true);
                save(approval);
            } else {
                List<TimeRecordCorrection> corrections = fetchCorrections(record);
                if (!corrections.isEmpty()) {
                    // restore previous booking
                    TimeRecordCorrection correction = corrections.get(0);
                    record.resetCorrection(correction);
                    save(record);
                    TimeRecord closing = getClosingRecord(record.getId());
                    if (closing != null) {
                        closing.resetCorrection(correction);
                        save(closing);
                    }
                    approval.updateStatus(user, TimeRecordingApproval.STATUS_NOT_APPROVED, true);
                    save(approval);
                    StringBuilder sql = new StringBuilder(256);
                    // remove corrections
                    sql
                            .append("DELETE FROM ")
                            .append(getRecordCorrectionsTableName())
                            .append(" WHERE reference_id = ")
                            .append(record.getId());
                    int cnt = jdbcTemplate.update(sql.toString());
                    if (log.isDebugEnabled()) {
                        log.debug("closeApproval() done [id=" + approval.getId()
                                + ", status=" + approval.getStatus()
                                + ", deletedCorrections=" + cnt
                                + "]");
                    }
                }
            }
        } else if (approval.getType() != null) {
            TimeRecordType type = approval.getType();
            if (approved) {
                StringBuilder sql = new StringBuilder(256);
                sql
                        .append("UPDATE ")
                        .append(getRecordTableName())
                        .append(" SET status_id = ")
                        .append(TimeRecordStatus.APPROVED)
                        .append(" WHERE reference_id IN (SELECT id FROM ")
                        .append(getRecordingPeriodTableName())
                        .append(" WHERE reference_id = ")
                        .append(approval.getReference())
                        .append(") AND type_id = ")
                        .append(type.getId())
                        .append(" AND (record_value >= '")
                        .append(approval.getStartDate())
                        .append("' AND record_value <= '")
                        .append(approval.getEndDate())
                        .append("')");
                int updatedCount = jdbcTemplate.update(sql.toString());
                approval.updateStatus(user, TimeRecordingApproval.STATUS_APPROVED, false);
                save(approval);
                if (log.isDebugEnabled()) {
                    log.debug("closeApproval() done, updated records [id=" + approval.getId()
                            + ", status=" + TimeRecordingApproval.STATUS_APPROVED
                            + ", count=" + updatedCount + "]");
                }

            } else {
                StringBuilder sql = new StringBuilder(256);
                // remove closings
                sql
                        .append("DELETE FROM ")
                        .append(getRecordTableName())
                        .append(" WHERE type_id = ")
                        .append(TimeRecordType.CLOSING)
                        .append(" AND closing_id IN (SELECT id FROM ")
                        .append(getRecordTableName())
                        .append(" WHERE reference_id IN (SELECT id FROM ")
                        .append(getRecordingPeriodTableName())
                        .append(" WHERE reference_id = ")
                        .append(approval.getReference())
                        .append(") AND type_id = ")
                        .append(type.getId())
                        .append(" AND (record_value >= '")
                        .append(approval.getStartDate())
                        .append("' AND record_value <= '")
                        .append(approval.getEndDate())
                        .append("'))");
                int closingCount = jdbcTemplate.update(sql.toString());

                sql = new StringBuilder(256);
                // remove corrections
                sql
                        .append("DELETE FROM ")
                        .append(getRecordCorrectionsTableName())
                        .append(" WHERE reference_id IN (")
                        .append("SELECT id FROM ")
                        .append(getRecordingTableName())
                        .append(" WHERE reference_id IN (SELECT id FROM ")
                        .append(getRecordingPeriodTableName())
                        .append(" WHERE reference_id = ")
                        .append(approval.getReference())
                        .append(") AND type_id = ")
                        .append(type.getId())
                        .append(" AND (record_value >= '")
                        .append(approval.getStartDate())
                        .append("' AND record_value <= '")
                        .append(approval.getEndDate())
                        .append("'))");
                jdbcTemplate.update(sql.toString());

                sql = new StringBuilder(256);
                // remove records
                sql
                        .append("DELETE FROM ")
                        .append(getRecordTableName())
                        .append(" WHERE reference_id IN (SELECT id FROM ")
                        .append(getRecordingPeriodTableName())
                        .append(" WHERE reference_id = ")
                        .append(approval.getReference())
                        .append(") AND type_id = ")
                        .append(type.getId())
                        .append(" AND (record_value >= '")
                        .append(approval.getStartDate())
                        .append("' AND record_value <= '")
                        .append(approval.getEndDate())
                        .append("')");
                int openingCount = jdbcTemplate.update(sql.toString());
                approval.updateStatus(user, TimeRecordingApproval.STATUS_NOT_APPROVED, false);
                save(approval);
                if (log.isDebugEnabled()) {
                    log.debug("closeApproval() done, deleted records [id=" + approval.getId()
                            + ", status=" + TimeRecordingApproval.STATUS_NOT_APPROVED
                            + ", openingCount=" + openingCount
                            + ", closedCount=" + closingCount + "]");
                }
            }
        }
    }

    private void save(TimeRecordingApproval approval) {
        getCurrentSession().saveOrUpdate(TimeRecordingApprovalImpl.class.getName(), approval);
    }

    /**
     * Provides closing record of a starting record. Method is required by closeApproval.
     * @param id of the starting record
     * @return closing record or null if not exists
     */
    private TimeRecord getClosingRecord(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("getClosingRecord() invoked [record=" + id + "]");
        }
        StringBuilder hql = new StringBuilder("from ");
        hql.append(TimeRecordImpl.class.getName()).append(" o where o.closing = ").append(id);
        List<TimeRecord> records = getCurrentSession().createQuery(hql.toString()).list();
        TimeRecord record = records.isEmpty() ? null : records.get(0);
        if (log.isDebugEnabled()) {
            log.debug("getClosingRecord() done [record=" + id + ", closing=" + (record == null ? "null" : record.getId()) + "]");
        }
        return record;
    }

}
