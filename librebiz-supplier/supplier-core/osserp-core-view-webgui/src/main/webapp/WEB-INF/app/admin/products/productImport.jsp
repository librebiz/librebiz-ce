<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>
<v:view viewName="productImportView"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="styles" type="string">
		<style type="text/css">
        .importCount {
            text-align: right;
        }
		</style>
	</tiles:put>
	<tiles:put name="headline">
        <fmt:message key="productImportLabel"/>
	</tiles:put>
	<tiles:put name="headline_right">
        <v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area" id="productImportContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.createMode}">
                        <v:form id="productImportForm" name="productImportForm" url="/admin/products/productImport/save" multipart="true" >
                            <div class="col-md-6 panel-area panel-area-default">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 style="text-transform: none;"><fmt:message key="productImportNew"/></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="fileUpload"><fmt:message key="selection"/></label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <o:fileUpload />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row next">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label for="importClassificationOnly">
                                                            <v:checkbox name="performActions" value="${view.performActions}"/>
                                                            <fmt:message key="importClassificationOnly" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row next">
                                            <div class="col-md-4"> </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <o:submitExit url="${view.appLink}/exit"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <o:submit value="importAction" styleClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </v:form>
					</c:when>
					<c:otherwise>
                        <div class="col-lg-12 panel-area">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="importUser"><fmt:message key="uploadBy" /></th>
                                            <th class="importDate"><fmt:message key="date"/></th>
                                            <th class="importFile"><fmt:message key="importFile"/></th>
                                            <th class="importStatus"><fmt:message key="status"/></th>
                                            <th class="importCount"><fmt:message key="numberShort"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:if test="${empty view.existing}">
                                            <tr><td colspan="5"><fmt:message key="productImportListEmpty" /></td></tr>
                                        </c:if>
                                        <c:forEach items="${view.existing}" var="obj">
                                            <tr>
                                                <td class="importUser">
                                                    <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${obj.userId}">
                                                        <oc:employee value="${obj.userId}"/>
                                                    </v:ajaxLink>
                                                </td>
                                                <td class="importDate"><o:date value="${obj.changed}" addtime="true"/></td>
                                                <td class="importFile"><o:out value="${obj.filename}"/></td>
                                                <td class="importStatus">
                                                    <c:choose>
                                                        <c:when test="${empty obj.errorReport}">
                                                            <fmt:message key="${obj.status}"/>
                                                        </c:when>
                                                        <%--
                                                        <c:otherwise>
                                                            <v:ajaxLink linkId="productImportErrorReportView" url="/admin/products/productImportErrorReport/forward?id=${obj.taskId}">
                                                                <fmt:message key="${obj.status}"/>
                                                            </v:ajaxLink>
                                                        </c:otherwise>
                                                        --%>
                                                    </c:choose>
                                                </td>
                                                <td class="importCount"><o:out value="${obj.result}"/></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
