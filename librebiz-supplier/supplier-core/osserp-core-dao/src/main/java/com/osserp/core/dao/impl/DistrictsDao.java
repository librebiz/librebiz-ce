/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2006 9:41:06 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.BackendException;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.NumberUtil;
import com.osserp.core.AddressLocality;
import com.osserp.core.District;
import com.osserp.core.Options;
import com.osserp.core.dao.Districts;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.DistrictVO;
import com.osserp.core.model.AddressLocalityVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DistrictsDao extends AbstractTablesAwareSpringDao implements Districts {
    private static Logger log = LoggerFactory.getLogger(DistrictsDao.class.getName());
    private static final String REGION_COLUMNS = "id, zipcode, description, city, county";
    private static final int REGION_FETCHSIZE = 25;
    private String regionTable;
    
    public DistrictsDao(JdbcTemplate jdbcTemplate, Tables tables) {
        super(jdbcTemplate, tables);
        regionTable = getTable(TableKeys.CONTACT_ZIPCODES);
    }

    public List<District> getList() {
        StringBuffer query = new StringBuffer(64);
        query
                .append("SELECT id,name,ctp,state_id FROM ")
                .append(getTable(Options.DISTRICTS))
                .append(" ORDER BY id");
        return (List) jdbcTemplate.query(
                query.toString(),
                new RowMapperResultSetExtractor(new DistrictRowMapper()));
    }

    public List<AddressLocality> findByZipcode(String pattern) {
        List<AddressLocality> result = new ArrayList<>();
        if (NumberUtil.isInteger(pattern)) {
            try {
                StringBuffer query = new StringBuffer(64);
                query
                    .append("SELECT ")
                    .append(REGION_COLUMNS)
                    .append(" FROM ")
                    .append(regionTable)
                    .append(" WHERE zipcode ~* '^")
                    .append(pattern)
                    .append("' ORDER BY zipcode LIMIT ").append(REGION_FETCHSIZE);
                result = (List<AddressLocality>) jdbcTemplate.query(
                    query.toString(),
                    getLocalityReader());
                if (log.isDebugEnabled()) {
                    log.debug("findByZipcode() done [pattern=" + pattern + ", count=" + result.size() + "]");
                }
            } catch (Exception e) {
                throw new BackendException(e);
            }
        }
        return result;
    }
    

    protected class DistrictRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new DistrictVO(
                    Long.valueOf(rs.getLong(1)),
                    rs.getString(2),
                    rs.getString(3),
                    Long.valueOf(rs.getLong(4)));
        }
    }

    protected RowMapperResultSetExtractor getLocalityReader() {
        return new RowMapperResultSetExtractor(new LocalityRowMapper());
    }

    private class LocalityRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new AddressLocalityVO(
                Long.valueOf(rs.getLong(1)),
                rs.getString(3),  // description 
                rs.getString(2),  // zipcode 
                rs.getString(4),  // city 
                rs.getString(5)); // county
        }
    }
}
