/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 10, 2006 12:40:59 PM 
 * 
 */
package com.osserp.core.sales;

import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderManager;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordPaymentAgreement;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrderManager extends OrderManager {

    /**
     * Creates a new internal order related to given sales
     * @param user
     * @param company
     * @param branch
     * @param customer
     * @param bookingType
     * @param sales
     * @throws ClientException if branch not configured
     */
    Order create(
            Employee user,
            Long company,
            Long branch,
            Customer customer,
            Long bookingType,
            Sales sales) throws ClientException;

    /**
     * Creates a new sales order using items from other record
     * @param user
     * @param sales
     * @param other
     * @param overridePrice
     * @return new sales order
     * @throws ClientException if object validation failed
     */
    Order createByRecord(
            Employee user,
            Sales sales,
            Record other,
            boolean overridePrice) throws ClientException;

    /**
     * Creates a new empty or dummy order for sales without regular workflow, e.g. 
     * imported and/or incomplete thirdparty artefacts or misconfigured project types.  
     * @param user
     * @param sales
     * @return order
     * @throws ClientException if create missing orders is not supported by system configuration
     */
    Order createMissing(Employee user, Sales sales) throws ClientException;

    /**
     * Resets the status and creates a new version of the related order
     * @param user
     * @param sales
     * @param backupItems
     * @param backupContext
     * @return order
     */
    Order createNewVersion(Employee user, Sales sales, boolean backupItems, String backupContext);

    /**
     * Creates a new warranty order
     * @param user
     * @param sales
     * @param itemsToReplace
     * @return order
     */
    Order createWarrantyOrder(Employee user, Sales sales, List<Item> itemsToReplace);

    /**
     * Provides all orders of a customer
     * @param customer
     * @return orders or empty list
     */
    List<Order> getByCustomer(Customer customer);

    /**
     * Indicates if an order is referenced by a sales case
     * @param sales
     * @return true if a related order exists
     */
    boolean exists(Sales sales);

    /**
     * Tries to find an order related to given sale
     * @param sales
     * @return order or null if not exists
     */
    Order getBySales(Sales sales);

    /**
     * Tries to find an order related to given sale
     * @param sales
     * @return orders
     */
    List<Order> findServiceOrders(Sales sales);

    /**
     * Looks up for order of a known company and reference
     * @param company
     * @param reference
     * @return orders or empty list if no such entity exists
     */
    List<RecordDisplay> findByCompany(Long company, Long reference);

    /**
     * Looks up for order of a known company and reference
     * @param company
     * @param openOnly
     * @return orders or empty list if no such entity exists
     */
    List<RecordDisplay> findByCompany(Long company, boolean openOnly);

    /**
     * Provides all orders contained in id list
     * @param list of id values
     * @return orders
     */
    List<Order> get(List<Long> list);

    /**
     * Checks if order is billed
     * @param order
     * @return
     */
    boolean isBilled(Order order);

    /**
     * Updates an order related to given project with a new calculation
     * @param user
     * @param sales
     * @param calculation
     */
    void updateByCalculation(Employee user, Sales sales, Calculation calculation);

    /**
     * Confirms a discount
     * @param user whose confirming
     * @param order to confirm
     * @param discount to confirm
     * @param note
     */
    void confirmDiscount(Employee user, Order order, Double discount, String note);

    /**
     * Closes an order
     * @param user
     * @param sales
     */
    void close(Employee user, Sales sales);

    /**
     * Closes an order
     * @param user
     * @param order to close
     */
    void close(Employee user, Order order);

    /**
     * Re-opens an order by setting status from 'released' to 'change' if possible 
     * @param user 
     * @param sales
     * @throws ClientException if order is closed or delivery or invoice records found
     */
    void reopen(Employee user, Sales sales) throws ClientException;

    /**
     * Resets the delivery date of an order related to a given sales process
     * @param sales
     */
    void resetDeliveryDate(Sales sales);

    /**
     * Provides the payment agreement of an order if exists
     * @return paymentAgreement or null if none exists
     */
    RecordPaymentAgreement findPaymentAgreement(Long orderId);

    /**
     * Changes the stock id of all items
     * @param user
     * @param record
     * @param stockId
     */
    void changeStock(Employee user, Record record, Long stockId);

    /**
     * Changes the stock id of an item
     * @param user
     * @param record
     * @param item
     * @param stockId
     */
    void changeStock(Employee user, Record record, Item item, Long stockId);

    /**
     * Checks the delivery status against the provided delivery type. This method is used
     * to check if a corresponding flowControl action could be set.
     * @param order
     * @param deliveryType as 'partial' or 'final'
     * @throws ClientException if not deliveries not performed as signaled by deliveryType
     */
    void checkDeliveryStatus(Order order, String deliveryType) throws ClientException;

    /**
     * Checks if at least one related invoice or downpayment with status released exists.
     * @param order
     * @throws ClientException if no invoice exists or any invoice with unreleased status was found
     */
    void checkInvoiceStatus(Order order) throws ClientException;

    /**
     * Sends a package list update event
     * @param user
     * @param eventName
     * @param sales
     * @param message
     */
    void sendUpdateEvent(Employee user, Sales sales, String eventName, String message);
}
