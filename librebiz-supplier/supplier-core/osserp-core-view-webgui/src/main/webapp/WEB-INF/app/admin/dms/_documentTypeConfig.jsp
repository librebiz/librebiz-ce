<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="documentCryptConfigHeader" /></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <c:choose>
                    <c:when test="${view.securityServiceSetupRequired}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status"><fmt:message key="status"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <fmt:message key="credentialStorageSetupRequired" />
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:when test="${!view.credentialsAvailable}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="password"><fmt:message key="password"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <v:link url="/admin/system/credentialConfig/forward?context=${view.credentialContext}&name=${view.bean.id}&exit=/admin/dms/documentTypeConfig/reload">
                                        <fmt:message key="noPasswordDefined" />
                                    </v:link>
                                </div>
                            </div>
                        </div>
                        <c:if test="${!empty view.existingCredentials}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status"><fmt:message key="transferFromLabel"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <c:forEach var="cred" items="${view.existingCredentials}" varStatus="status">
                                        <c:if test="${status.index > 0}"><br></c:if>
                                        <v:link url="/admin/dms/documentTypeConfig/copyCredentials?id=${cred.id}" title="transferExistingCredential" confirm="true" header="transferExistingCredential" message="copyPasswordConfirm">
                                            <o:out value="${cred.name}"/>
                                        </v:link>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                        </c:if>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status"><fmt:message key="status"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <fmt:message key="notConfigured" />
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${!empty view.bean}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="documentTypeName"><fmt:message key="documentType"/></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <o:out value="${view.bean.name}"/>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="password"><fmt:message key="password"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${view.credentialsDisplayAvailable}">
                                            <v:ajaxLink url="/admin/dms/documentTypeConfig/displayCredentials" linkId="documentTypeCredentials" closeOnly="true">
                                                <fmt:message key="passwordConfigured" />
                                            </v:ajaxLink>
                                        </c:when>
                                        <c:otherwise><fmt:message key="passwordConfigured" /></c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="documentCount"><fmt:message key="documentCount"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${view.documentCount}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status"><fmt:message key="status"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${view.bean.formatPlain and view.bean.locked}">
                                            <fmt:message key="runningDecryption" />
                                        </c:when>
                                        <c:when test="${view.bean.formatPlain}">
                                            <v:link url="/admin/dms/documentTypeConfig/changeFormat" title="encryptDocuments" confirm="true" header="longRunningTask" message="encryptDocuments">
                                                <fmt:message key="notEncrypted" />
                                            </v:link>
                                        </c:when>
                                        <c:when test="${view.bean.locked}">
                                            <fmt:message key="runningEncryption" />
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="/admin/dms/documentTypeConfig/changeFormat" title="decryptDocuments" confirm="true" header="longRunningTask" message="decryptDocuments">
                                                <fmt:message key="encrypted" />
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose>
                                
                                </div>
                            </div>
                        </div>
                    
                    </c:otherwise>
                </c:choose>
                    
            </div>
        </div>
    </div>
        
</div>
