/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 2, 2004 
 * 
 */
package com.osserp.core;

import com.osserp.common.ClientException;
import com.osserp.common.Mappable;
import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessType extends Mappable, Option {

    static final String NONE_COMMISSION = "none";

    /**
     * Provides a short code for the type used by listings
     * @return key
     */
    String getKey();

    /**
     * Provides the name of the common business context.
     * This setting mostly has impacts on request context behaviour opposed
     * to salesContext property which affects business case in sales context. 
     * @return context name as i18n string
     */
    String getContext();

    /**
     * Provides the name of the sales context.
     * @return salesContext name as i18n string
     */
    String getSalesContext();

    /**
     * Provides the associated workflow
     * @return workflow
     */
    Workflow getWorkflow();

    /**
     * Provides the associated company id
     * @return company id
     */
    Long getCompany();

    /**
     * The default branch is used if context does not provide one
     * @return defaultBranch
     */
    Long getDefaultBranch();

    /**
     * Indicates if defaultBranch overrides branch provided by contex
     * @return forceDefaultBranch
     */
    boolean isForceDefaultBranch();

    /**
     * Provides the id of the default contractType
     * @return defaultContractType
     */
    Long getDefaultContractType();

    /**
     * Indicates if request type supports sales commission calculation
     * @return commissionSupported
     */
    boolean isCommissionSupported();

    /**
     * Indicates if request type supports installation date
     * @return installationDateSupported
     */
    boolean isInstallationDateSupported();

    /**
     * Indicates if request type supports installation date
     * @return installationDateSupported
     */
    boolean isStartupSupported();

    /**
     * Indicates if depending records should created by calculation
     * @return recordByCalculation
     */
    boolean isRecordByCalculation();

    /**
     * Provides the primary key of the associated calculation config
     * @return calculation config id
     */
    Long getCalculationConfig();

    /**
     * Provides the default calculator name
     * @return calculatorName
     */
    String getCalculatorName();

    /**
     * Indicates if calculations will be created by event.
     * @return calculationByEvent
     */
    boolean isCalculationByEvent();

    /**
     * Provides the id of the associated template config
     * @return calculationByEventConfig
     */
    Long getCalculationByEventConfig();

    /**
     * Provides the commission type
     * @return commissionType
     */
    String getCommissionType();

    /**
     * Sets the commission type
     * @param commissionType
     */
    void setCommissionType(String type);

    /**
     * Indicates if an externalId is provided on create 
     * @return true if external id provided 
     */
    boolean isExternalIdProvided();

    /**
     * Provides a comma separated list of keys indicating the external 
     * request is a qualified request (e.g. not interest-only).
     * @return list with keys or null if not defined
     */
    String getExternalStatusQualifiedRequestKeys();

    /**
     * Provides a comma separated list of keys indicating the external 
     * request is a sales (e.g. something ordered).
     * @return list with keys or null if not defined
     */
    String getExternalStatusSalesKeys();

    /**
     * Provides a comma separated list of keys indicating the external 
     * request is a closed sales (e.g. nothing to do, documentation only).
     * @return list with keys or null if not defined
     */
    String getExternalStatusSalesClosedKeys();
    
    /**
     * Indicates if any of externalStatus*Keys matches provided status
     * @param externalStatus text of external status to lookup for match
     * @return true if any of available external status keys is matching
     */
    boolean isMatchingExternalStatus(String externalStatus);

    boolean isExternalStatusQualifiedRequest(String externalStatus);

    boolean isExternalStatusSales(String externalStatus);

    boolean isExternalStatusSalesClosed(String externalStatus);
    
    
    boolean isIncludePriceByDefault();

    boolean isActive();

    boolean isProjectRequest();

    boolean isRequestContextOnly();

    boolean isDirectSales();

    boolean isTrading();

    boolean isInterestContext();

    boolean isSales();

    boolean isSalesPersonByCollector();

    boolean isService();

    boolean isSubscription();

    boolean isSupportingCustomDelivery();

    boolean isStandardProject();

    boolean isWholeSale();

    boolean isSupportingOffers();

    boolean isSupportingPictures();

    boolean isCreateSla();

    boolean isPackageRequired();

    boolean isSupportingDownpayments();

    boolean isSupportingConfig();

    boolean isSupportingCapacity();

    String getCapacityFormat();

    String getCapacityUnit();

    String getCapacityLabel();

    String getCapacityCalculationMethod();

    String getConfigPermissions();

    Long getDefaultOrigin();

    Long getDefaultOriginType();

    Long getCustomDeliveryNoteType();

    /**
     * Provides the minimal margin
     * @return minimalMargin
     */
    Double getMinimalMargin();

    /**
     * Provides the target margin
     * @return targetMargin
     */
    Double getTargetMargin();

    /**
     * Provides the id of the requestLetter type
     * @return requestLetterType
     */
    Long getRequestLetterType();

    /**
     * Provides the id of the salesLetter type
     * @return salesLetterType
     */
    Long getSalesLetterType();

    /**
     * Update branch on salesPerson change 
     * @return salesPersonChangeBranch
     */
    boolean isUpdateBranchOnSalesPersonChange();

    /**
     * Enables/disables force branch update on salesPerson change 
     * @param updateBranchOnSalesPersonChange true if salesPerson change should force branch update
     */
    void setUpdateBranchOnSalesPersonChange(boolean updateBranchOnSalesPersonChange);

    /**
     * Indicates that associated order should be closed when sales is closed
     * @return true if order should be closed
     */
    boolean isCloseOrderOnClose();

    boolean isDummy();

    /**
     * Indicates that a business case of implementing type provide workflow only.
     * They are not intend to check and create invoices, payments, etc. 
     * @return true for workflow only business types
     */
    boolean isWorkflowOnly();

    /**
     * Provides the default language
     * @return language
     */
    String getLanguage();

    /**
     * Updates configurable business type properties
     * @param name
     * @param key
     * @param context
     * @param salesContext
     * @param active
     * @param requestContextOnly
     * @param directSales
     * @param trading
     * @param packageRequired
     * @param interestContext
     * @param salesPersonByCollector
     * @param supportingDownpayments
     * @param supportingPictures
     * @param supportingOffers
     * @param createSla
     * @param includePriceByDefault
     * @param dummy
     * @param service
     * @param subscription
     * @param wholeSale
     * @param commissionType
     * @param capacityFormat
     * @param capacityUnit
     * @param capacityLabel
     * @param language
     * @param recordByCalculation
     * @param calculationConfig
     * @param requestLetterType
     * @param salesLetterType
     * @param installationDateSupported
     * @param startupSupported
     * @param externalIdProvided
     * @param externalStatusQualifiedRequestKeys
     * @param externalStatusSalesKeys
     * @param externalStatusSalesClosedKeys
     * @param defaultContractType
     * @param defaultOrigin
     * @param defaultOriginType
     * @throws ClientException
     */
    void update(
            String name,
            String key,
            String context,
            String salesContext,
            boolean active,
            boolean requestContextOnly,
            boolean directSales,
            boolean trading,
            boolean packageRequired,
            boolean interestContext,
            boolean salesPersonByCollector,
            boolean supportingDownpayments,
            boolean supportingPictures,
            boolean supportingOffers,
            boolean createSla,
            boolean includePriceByDefault,
            boolean dummy,
            boolean service,
            boolean subscription,
            boolean wholeSale,
            String commissionType,
            String capacityFormat,
            String capacityUnit,
            String capacityLabel,
            String language,
            boolean recordByCalculation,
            Long calculationConfig,
            Long requestLetterType,
            Long salesLetterType,
            boolean installationDateSupported,
            boolean startupSupported,
            boolean externalIdProvided,
            String externalStatusQualifiedRequestKeys, 
            String externalStatusSalesKeys,
            String externalStatusSalesClosedKeys,
            Long defaultContractType,
            Long defaultOrigin,
            Long defaultOriginType) throws ClientException;

}
