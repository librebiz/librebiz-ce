<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="newsletterConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
    
.contactName {
    width: 240px;
}

.contactCity {
    width: 210px;
}

.contactEmail {
    width: 270px;
}

.table .action {
    width: 35px;
    text-align: center;
}
      
</style>
</tiles:put>
<tiles:put name="headline">
<c:choose>
<c:when test="${view.listDeactivated}">
<fmt:message key="deactivatedRecipients"/> [<o:listSize value="${view.list}"/>]
</c:when>
<c:otherwise>
<fmt:message key="activatedRecipients"/> [<o:listSize value="${view.list}"/>]
</c:otherwise>
</c:choose>
</tiles:put>
<tiles:put name="headline_right">
<v:navigation/>
</tiles:put>
<tiles:put name="content" type="string">
    <v:form url="/mail/newsletterConfig/save">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>
                <div class="table-responsive table-responsive-default">
                    <table class="table table-striped">
	       				<thead>
			     			<tr>
				    			<th class="contactName"><fmt:message key="companySlashName"/></th>
                                <th class="contactCity"><fmt:message key="city"/></th>
                                <th class="contactEmail"><fmt:message key="email"/></th>
                                <th class="action"> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="contact" items="${view.list}" varStatus="s">
                                <tr>
                                    <td class="contactName">
                                        <c:choose>
                                            <c:when test="${contact.contactPerson}">
                                                <a href="<c:url value="/contactPersons.do?method=load&id=${contact.id}&exit=newsletterConfig"/>"><o:out value="${contact.name}"/></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="<c:url value="/contacts.do?method=load&id=${contact.id}&exit=newsletterConfig"/>"><o:out value="${contact.name}"/></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="contactCity"><o:out value="${contact.city}"/></td>
                                    <td class="contactEmail"><o:out value="${contact.address}"/></td>
                                    <td class="action">
                                        <input type="checkbox" name="ids" value="<o:out value="${contact.id}"/>"/>
                                    </td>
                                </tr>
                            </c:forEach>
                            <c:if test="${!empty view.list}">
                                <tr>
                                    <td colspan="3">
                                        <c:choose>
                                            <c:when test="${view.listDeactivated}"><fmt:message key="activateSelectedRecipients"/></c:when>
                                            <c:otherwise><fmt:message key="deactivateSelectedRecipients"/></c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="action">
                                        <input type="image" name="submit" src="<c:url value="${applicationScope.saveIcon}"/>" class="bigicon"/>
                                    </td>
                                </tr>
                            </c:if>
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
    </v:form>
</tiles:put>
</tiles:insert>
