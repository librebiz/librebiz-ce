/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 30-Sep-2007 12:50:17 
 * Created on Oct 27, 2010 9:44:46 AM (Groovy implementation) 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.NumberUtil

import com.osserp.core.planning.ProductPlanningManager
import com.osserp.core.sales.SalesMonitoringManager
import com.osserp.core.sales.SalesUtil
import com.osserp.core.users.Permissions

/**
 *
 * @author rk <rk@osserp.com>
 * @author jg <jg@osserp.com>
 * 
 */
public class SalesProcessView extends AbstractReportingView {
    private static Logger logger = LoggerFactory.getLogger(SalesProcessView.class.getName())

    SalesProcessView() {
        super()
        selectors.branch = 'projectAwareBranchsSelection'
        selectors.status = true
        toggles.stopped = true
        refreshLink = true
        permissions = 'open_project_list,executive,executive_tecstaff,executive_branch'
        values.stopped = false
    }

    @Override
    void reload() {
        if (forwardRequest) {
            Long minStatus = NumberUtil.createLong(getUserProperty('salesMonitoringStatus'))
            if (minStatus && !form.getLong('statusValue')) {
                values.statusValue = minStatus
            }
        }
        if (values.company) {
            logger.debug("reload: invoked [company=${values.company}]")
            scrollPosition = 0
            list.clear()
            def completeList = salesMonitoringManager.getSalesMonitoring(values.company)
            Long manager = managerIdIfProjectManagerContext
            if (manager) {
                def tmpList = []
                completeList.each() {
                    if (it.managerId == manager || it.managerSubId == manager) {
                        tmpList << it
                    }
                }
                completeList = tmpList
            }
            completeList = SalesUtil.filter(domainUser, completeList)
            completeList.sort{it.openAmount}
            actualSortKey = 'openAmount'
            completeList = completeList.reverse()
            completeList.each() {
                it.setVacant(!productPlanningManager.isAvailableForDelivery(it))
                boolean stopped = (values.stopped && it.stopped) || (!values.stopped && !it.stopped)
                boolean branch = values.branch ? values.branch == it.branchId : true
                boolean status = true
                if (values.statusValue) {
                    status  = values.statusLower ? it.status <= values.statusValue : it.status > values.statusValue
                }
                if (stopped && branch && status) {
                    list << it
                }
            }
        }
    }

    private Long getManagerIdIfProjectManagerContext() {
        return (domainUser.branchExecutive
        && domainUser.projectManager
        && !Permissions.hasPermission(user, Permissions.OPEN_PROJECT_LIST)) ? domainUser.employee.id : null
    }
    
    protected ProductPlanningManager getProductPlanningManager() {
        getService(ProductPlanningManager.class.getName())
    }
    
    protected SalesMonitoringManager getSalesMonitoringManager() {
        getService(SalesMonitoringManager.class.getName())
    }
    
}
