<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${(view.bean.employeeAccount or view.bean.systemAccount) and !empty view.userPermissions}">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="systemPermissions" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <v:form name="dynamicForm" url="${view.baseLink}/save">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="center"><fmt:message key="id" /></th>
                                <th style="width: 80%;"><fmt:message key="system" /></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="permission" items="${view.userPermissions}">
                                <tr>
                                    <td class="center"><o:out value="${permission.values['gidnumber']}" /></td>
                                    <td><o:out value="${permission.values['cn']}" /></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </v:form>
        </div>
    </div>
</c:if>
