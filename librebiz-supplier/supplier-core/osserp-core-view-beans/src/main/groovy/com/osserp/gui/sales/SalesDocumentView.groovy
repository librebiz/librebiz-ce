/**
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dms.DmsReference
import com.osserp.core.dms.CoreDocumentType
import com.osserp.core.dms.LetterManager
import com.osserp.core.dms.LetterType
import com.osserp.gui.dms.BusinessCaseDocumentView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesDocumentView extends BusinessCaseDocumentView {
    private static Logger logger = LoggerFactory.getLogger(SalesDocumentView.class.getName())
    
    SalesDocumentView() {
        super()
        referenceSupport = false
    }
    
    @Override
    protected DmsReference getDmsReference() {
        new DmsReference(CoreDocumentType.SALES_DOCUMENT, businessCase.primaryKey)
    }

    @Override
    public void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
    }
    
    int getLetterCount() {
        if (businessCase) {
            LetterType type = letterManager.getType(CoreDocumentType.SALES_LETTER_TYPE)
            def customer = env.businessCaseView?.sales?.request?.customer
            if (type && customer) {
                return letterManager.getCount(type, customer, businessCase.primaryKey)
            }
        }
        return 0
    }

    int getPictureCount() {
        if (businessCase) {
            return dmsManager.getCountByReference(new DmsReference(CoreDocumentType.SALES_PICTURE, businessCase.primaryKey))
        }
        return 0
    }

    protected LetterManager getLetterManager() {
        getService(LetterManager.class.getName())
    }
}
