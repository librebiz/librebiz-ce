/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 5:18:45 PM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Order;
import com.osserp.core.sales.Sales;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrderDownpayments extends Invoices, PaymentAwareRecords, SalesRecords {

    int countBySales(Long salesId);

    /**
     * Creates a new downpayment
     * @param billingType
     * @param order
     * @param createdBy
     * @param copyNote
     * @param copyTerm
     * @param copyItems
     * @param userDefinedId
     * @param userDefinedDate
     * @return new created downpayment
     * @throws ClientException if invalid payment configuration found
     */
    Downpayment create(
            Long billingType,
            Order order,
            Employee createdBy,
            boolean copyNote,
            boolean copyTerms,
            boolean copyItems,
            Long userDefinedId, 
            Date userDefinedDate) throws ClientException;

    /**
     * Restores canceled invoice
     * @param user
     * @param invoice
     * @return restored downpayment
     * @throws ClientException
     */
    Downpayment restoreCanceled(Employee user, Downpayment invoice) throws ClientException;

    /**
     * Cancels downpayment
     * @param user
     * @param invoice
     * @param date
     * @throws ClientException
     */
    Downpayment cancel(Employee user, Downpayment invoice, Date date) throws ClientException;

    /**
     * Updates the branch office by sales
     * @param sales
     */
    void changeBranch(Sales sales);

    /**
     * Provides all downpayments related to given sales order
     * @param order
     * @return downpayments or empty list
     */
    List<Downpayment> getByOrder(Order order);

    /**
     * Provides canceled downpayments by order
     * @param order
     * @return canceled downpayments or empty list if none exist.
     */
    List<Downpayment> getCanceledByOrder(Order order);

}
