/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Dec 7, 2012 9:29:13 AM
 *
 */
package com.osserp.core.dao.records.impl

import groovy.sql.Sql

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dao.DataSourceService
import com.osserp.common.util.CalendarUtil

import com.osserp.core.employees.Employee
import com.osserp.core.finance.RecordNumberCreator
import com.osserp.core.finance.RecordType

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class NumberCreatorByDate extends DataSourceService implements RecordNumberCreator {
    private static Logger logger = LoggerFactory.getLogger(NumberCreatorByDate.class.getName())

    Long create(Employee user, Long company, RecordType type, Date created) {
        String number = CalendarUtil.getReverseDate(created)
        logger.debug("create: initial number created [result=${number}, manualDate=${created}]")
        try {
            Long other = getExisting(type, Long.valueOf(number))
            String existing = other?.toString()
            int numberl = number.length()
            int existingl = existing?.length() ?: numberl
            int overlap = existingl - numberl
            Long val = 1
            if (overlap > 0) {
                val = Long.valueOf(existing.substring(numberl, existingl)) + 1
            }
            int vall = val.toString().length()
            int loops = (overlap > type.uniqueNumberDigitCount ? overlap : type.uniqueNumberDigitCount)
            StringBuilder buffer = new StringBuilder(number)
            for (int i = vall; i < loops; i++) {
                buffer.append('0')
            }
            buffer.append(val)
            number = buffer.toString()
        } catch (Exception e) {
            logger.error("create: caught ignored exception [message=${e.message}]", e)
        }
        Long.valueOf(number)
    }

    private Long getExisting(RecordType type, Long id) {
        def query = "SELECT id FROM ${getTableName(type)} WHERE id::varchar(50) LIKE '${id}%' ORDER BY id DESC LIMIT 1"
        logger.debug("getExisting: invoked [type=${type?.id}, id=${id}, sql=${query.toString()}]")
        Long result
        try {
            Sql sql = getSql()
            sql.eachRow(query.toString()) { obj ->
                result = obj.id
            }
        } catch (Exception e) {
            logger.error("getExisting: failed [message=${e.message}]", e)
        }
        result
    }

    private String getTableName(RecordType type) {
        type?.tableName ?: 'records'
    }
}
