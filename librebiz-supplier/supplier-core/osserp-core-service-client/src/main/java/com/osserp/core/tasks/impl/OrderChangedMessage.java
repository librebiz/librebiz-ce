/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.tasks.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.osserp.common.util.StringUtil;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.RecordChangedInfo;
import com.osserp.core.sales.Sales;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class OrderChangedMessage implements Serializable {

    private Long createdBy;
    private Order order;
    private Sales sales;
    private String message;
    private String eventName;
    private Employee user;
    private List<DeliveryDateChangedInfo> deliveryDateChangedInfos = new ArrayList<DeliveryDateChangedInfo>();
    private List<ItemChangedInfo> itemChangedInfos = new ArrayList<ItemChangedInfo>();

    /**
     * Default constructor
     */
    protected OrderChangedMessage() {
        super();
    }

    /**
     * Creates a new order changed event
     * @param eventName
     * @param order
     * @param recordChangedInfos
     */
    protected OrderChangedMessage(String eventName, Order order, List<? extends RecordChangedInfo> recordChangedInfos) {
        super();
        this.eventName = eventName;
        this.order = order;
        if (recordChangedInfos != null) {
            for (int i = 0, j = recordChangedInfos.size(); i < j; i++) {
                RecordChangedInfo info = recordChangedInfos.get(i);
                createdBy = info.getCreatedBy();
                if (info instanceof DeliveryDateChangedInfo) {
                    deliveryDateChangedInfos.add((DeliveryDateChangedInfo) info);
                } else if (info instanceof ItemChangedInfo) {
                    itemChangedInfos.add((ItemChangedInfo) info);
                }
            }
        }
    }

    /**
     * Creates a new order changed event
     * @param eventName
     * @param order
     * @param recordChangedInfo
     */
    protected OrderChangedMessage(String eventName, Order order, RecordChangedInfo recordChangedInfo) {
        super();
        this.eventName = eventName;
        this.order = order;
        if (recordChangedInfo != null) {
            this.createdBy = recordChangedInfo.getCreatedBy();
            if (recordChangedInfo instanceof DeliveryDateChangedInfo) {
                deliveryDateChangedInfos.add((DeliveryDateChangedInfo) recordChangedInfo);
            } else if (recordChangedInfo instanceof ItemChangedInfo) {
                itemChangedInfos.add((ItemChangedInfo) recordChangedInfo);
            }
        }
    }

    /**
     * Creates a new order changed event
     * @param user
     * @param eventName
     * @param order
     * @param message
     */
    protected OrderChangedMessage(Employee user, String eventName, Order order, String message) {
        super();
        this.user = user;
        this.eventName = eventName;
        this.order = order;
        this.message = message;
    }

    /**
     * Creates a new order changed event
     * @param user
     * @param eventName
     * @param sales
     * @param message
     */
    protected OrderChangedMessage(Employee user, String eventName, Sales sales, String message) {
        super();
        this.eventName = eventName;
        this.sales = sales;
        this.message = message;
    }

    public String getEventName() {
        return eventName;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public Order getOrder() {
        return order;
    }

    public Sales getSales() {
        return sales;
    }

    public Employee getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public List<DeliveryDateChangedInfo> getDeliveryDateChangedInfos() {
        return deliveryDateChangedInfos;
    }

    public List<ItemChangedInfo> getItemChangedInfos() {
        return itemChangedInfos;
    }

    protected void setEventName(String eventName) {
        this.eventName = eventName;
    }

    protected void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    protected void setOrder(Order order) {
        this.order = order;
    }

    protected void setSales(Sales sales) {
        this.sales = sales;
    }

    protected void setUser(Employee user) {
        this.user = user;
    }

    protected void setMessage(String message) {
        this.message = message;
    }

    protected void setDeliveryDateChangedInfos(
            List<DeliveryDateChangedInfo> deliveryDateChangedInfos) {
        this.deliveryDateChangedInfos = deliveryDateChangedInfos;
    }

    protected void setItemChangedInfos(List<ItemChangedInfo> itemChangedInfos) {
        this.itemChangedInfos = itemChangedInfos;
    }

    @Override
    public String toString() {
        return StringUtil.toString(this);
    }

    /**
     * @return the id of the referenced project or purchase order
     */
    public Long getEventReference() {
        return (sales != null) ? sales.getId() : order.getId();
    }
}
