<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<c:set var="isPopup" value="true" scope="request"/>
<style type="text/css">
<tiles:getAsString name="styles" ignore="true" />
</style>
<div class="row">
    <div class="col-md-12">
        <div style="text-align: right; margin-right: 10px;" id="alignment_popup_close">&nbsp;</div>
    </div>
    <div class="col-md-12 panel-area">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><tiles:getAsString name="headline" ignore="true" /></h4>
            </div>
        </div>
        <div class="panel-body">
            <tiles:insert attribute="content" ignore="true" />
        </div><!-- panel-body -->
    </div><!-- panel-area -->
</div><!-- row -->
<o:removeErrors/>
