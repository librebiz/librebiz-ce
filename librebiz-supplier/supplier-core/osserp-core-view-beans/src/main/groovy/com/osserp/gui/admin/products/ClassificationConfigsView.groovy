/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 25, 2011 4:26:50 PM 
 * 
 */
package com.osserp.gui.admin.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.util.CollectionUtil
import com.osserp.core.products.Product
import com.osserp.core.products.ProductCategoryConfig
import com.osserp.core.products.ProductClassificationConfig
import com.osserp.core.products.ProductClassificationConfigManager
import com.osserp.core.products.ProductGroupConfig
import com.osserp.core.products.ProductTypeConfig

import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
class ClassificationConfigsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ClassificationConfigsView.class.getName())

    List<ProductClassificationConfig> productClassificationConfigs = []
    ProductGroupConfig selectedGroup
    ProductCategoryConfig selectedCategory
    String removalActive
    Long removalId

    List containingGroups = []
    List containingCategories = []
    List productList = []

    ClassificationConfigsView() {
        headerName = 'configureProductClassificationConfigs'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        productClassificationConfigs = productClassificationConfigManager.classificationConfigsDisplay
        if (!list) {
            list = productClassificationConfigManager.getTypes()
        }
    }

    @Override
    void reload() {
        list = productClassificationConfigManager.getTypes()
        if (bean) {
            bean = CollectionUtil.getById(list, bean.id)
            if (bean.groups) {
                containingGroups = generateIdList(bean.groups)
                if (selectedGroup) {
                    selectedGroup = CollectionUtil.getById(bean.groups, selectedGroup.id)
                    containingCategories = generateIdList(selectedGroup.categories)
                    if (selectedCategory) {
                        selectedCategory = CollectionUtil.getById(selectedGroup.categories, selectedCategory.id)
                    }
                }
            }
        }
    }

    @Override
    void select() {
        super.select()
        selectedGroup = null
        selectedCategory = null
        if (bean?.groups) {
            containingGroups = generateIdList(bean.groups)
        }
        Long groupId = form.getLong('group')
        if (groupId) {
            groupSelected(groupId)
            Long categoryId = form.getLong('category')
            if (categoryId) {
                categorySelected(categoryId)
            }
        }
    }

    /**
     * Looks up for productCategory.id  as 'category' and numberRange.id as 'id' param
     * and assigns the numberRange id to the category if both params provided
     */
    void assignCategoryNumberRange() {
        Long id = form.getLong('id')
        Long category = form.getLong('category')
        if (category) {
            ProductCategoryConfig cat = CollectionUtil.getById(selectedGroup.categories, id)
            if (cat) {
                productClassificationConfigManager.assignNumberRange(cat, id)
                reload()
            }
        }
    }

    /**
     * Looks up for productGroup.id  as 'group' and numberRange.id as 'id' form param 
     * and assigns the numberRange to the group if both params provided 
     */
    void assignGroupNumberRange() {
        Long id = form.getLong('id')
        Long group = form.getLong('group')
        if (group) {
            ProductGroupConfig grp = CollectionUtil.getById(bean.groups, group)
            if (grp) {
                productClassificationConfigManager.assignNumberRange(grp, id)
                reload()
            }
        }
    }

    void removeType() {
        if (bean) {
            try {
                productClassificationConfigManager.removeType(bean)
                bean = null
                containingGroups = []
                removalActive = null
                removalId = null
                reload()
            } catch (ClientException e) {
                if (e.errorObject instanceof List) {
                    logger.debug("removeType: caught exception [message=${e.message}, productsFound=${e.errorObject.size()}]")
                    productList = e.errorObject
                    removalActive = 'type'
                    removalId = bean.id
                    throw e
                } else {
                    logger.debug("removeType: caught exception [message=${e.message}, productsFound=no]\nException:\n${e}")
                }
            }
        }
    }

    void selectGroup() {
        groupSelected(form.getLong('id'))
    }

    void groupSelected(Long groupId) {
        if (groupId) {
            selectedGroup = CollectionUtil.getById(bean.groups, groupId)
            containingCategories = generateIdList(selectedGroup.categories)
        }
        selectedCategory = null
    }

    void selectCategory() {
        Long id = form.getLong('id')
        if (id) {
            selectedCategory = CollectionUtil.getById(selectedGroup.categories, id)
        }
    }

    void categorySelected(Long categoryId) {
        if (categoryId) {
            selectedCategory = CollectionUtil.getById(selectedGroup.categories, categoryId)
        }
    }

    void moveGroupUp() {
        def id = form.getLong('id')
        if (id) {
            moveGroup(id, false)
        }
    }

    void moveGroupDown() {
        def id = form.getLong('id')
        if (id) {
            moveGroup(id, true)
        }
    }

    void addGroup() {
        def id = form.getLong('id')
        if (bean && id) {
            def group = productClassificationConfigManager.getGroup(id)
            if (group) {
                productClassificationConfigManager.addGroup(bean, group)
                reload()
            }
        }
    }

    void removeGroup() {
        def id = form.getLong('id')
        if (bean && id) {
            ProductGroupConfig group = bean.fetchGroup(id)
            if (group) {
                try {
                    productClassificationConfigManager.removeGroup(bean, group)
                    reload()
                    removalActive = null
                    removalId = null
                } catch (ClientException e) {
                    if (e.errorObject instanceof List) {
                        logger.debug("removeGroup: caught exception [message=${e.message}, productsFound=${e.errorObject.size()}]")
                        productList = e.errorObject
                        removalActive = 'group'
                        removalId = id
                        throw e
                    } else {
                        logger.debug("removeGroup: caught unexpected exception [message=${e.message}, productsFound=no]\nException:\n${e}")
                    }
                }
            }
        }
    }

    void moveCategoryUp() {
        def id = form.getLong('id')
        if (id) {
            moveCategory(id, false)
        }
    }

    void moveCategoryDown() {
        def id = form.getLong('id')
        if (id) {
            moveCategory(id, true)
        }
    }

    void addCategory() {
        def id = form.getLong('id')
        if (selectedGroup && id) {
            def cat = productClassificationConfigManager.getCategory(id)
            if (cat) {
                productClassificationConfigManager.addCategory(selectedGroup, cat)
                reload()
            }
        }
    }

    void removeCategory() {
        def id = form.getLong('id')
        if (selectedGroup && id) {
            ProductCategoryConfig cat = selectedGroup.fetchCategory(id)
            if (cat) {
                try {
                    productClassificationConfigManager.removeCategory(selectedGroup, cat)
                    reload()
                    removalActive = null
                    removalId = null
                } catch (ClientException e) {
                    if (e.errorObject instanceof List) {
                        logger.debug("removeCategory: caught exception [message=${e.message}, productsFound=${e.errorObject.size()}]")
                        productList = e.errorObject
                        removalActive = 'category'
                        removalId = id
                        throw e
                    } else {
                        logger.debug("removeCategory: caught exception [message=${e.message}, productsFound=no]\nException:\n${e}")
                    }
                }
            }
        }
    }

    private void moveGroup(Long id, boolean down) {
        ProductGroupConfig group = CollectionUtil.getById(bean.groups, id)
        productClassificationConfigManager.moveGroup(bean, group, down)
        reload()
    }

    private void moveCategory(Long id, boolean down) {
        ProductCategoryConfig cat = CollectionUtil.getById(selectedGroup.categories, id)
        productClassificationConfigManager.moveCategory(selectedGroup, cat, down)
        reload()
    }

    private List generateIdList(List l) {
        def ret = []
        l?.each { ret << it.id }
        return ret
    }

    private ProductClassificationConfigManager getProductClassificationConfigManager() {
        getService(ProductClassificationConfigManager.class.getName())
    }
}
