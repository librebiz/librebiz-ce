/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.dms

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.dms.DocumentData
import com.osserp.common.web.RequestUtil
import com.osserp.groovy.web.ViewController

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractDocumentController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(AbstractDocumentController.class.getName())

    @RequestMapping
    def delete(HttpServletRequest request) {
        AbstractDocumentView view = getView(request)
        view.delete()
        return defaultPage
    }

    @RequestMapping
    def setReference(HttpServletRequest request) {
        AbstractDocumentView view = getView(request)
        view.setReference()
        return defaultPage
    }

    @RequestMapping
    def syncWithClient(HttpServletRequest request) {
        AbstractDocumentView view = getView(request)
        view.syncWithClient()
        return defaultPage
    }

    @RequestMapping
    def assignImport(HttpServletRequest request) {
        AbstractDocumentView view = getView(request)
        try {
            view.assignImport()
        } catch (Exception e) {
            saveError(request, e.message)
        }
        return defaultPage
    }

    @RequestMapping
    def toggleDocumentsImported(HttpServletRequest request) {
        AbstractDocumentView view = getView(request)
        view.toggleDocumentsImported()
        return defaultPage
    }

    @RequestMapping
    def toggleGalleryMode(HttpServletRequest request) {
        AbstractDocumentView view = getView(request)
        view.toggleGalleryMode()
        return defaultPage
    }

    @RequestMapping
    def selectImage(HttpServletRequest request) {
        AbstractDocumentView view = getView(request)
        view.selectImage()
        return defaultPage
    }

    @RequestMapping
    def updateMetadata(HttpServletRequest request) {
        AbstractDocumentView view = getView(request)
        try {
            view.updateMetadata()
        } catch (Exception e) {
            saveError(request, e.message)
        }
        return defaultPage
    }

    @RequestMapping
    synchronized def thumbnail(HttpServletRequest request, HttpServletResponse response) {
        AbstractDocumentView view = getView(request)
        DocumentData image = view.getThumbnail(
            RequestUtil.fetchId(request), RequestUtil.fetchLong(request, 'type'))
        if (image) {

            int length = image.getContentLength()
            String contentType = image.getContentType()
            String header = image.getContentDisposition()
            byte[] data = image.getBytes()
            try {
                response.setContentLength(length)
                response.setContentType(contentType)
                response.setHeader("Content-disposition", header)
                response.getOutputStream().write(data)
                response.getOutputStream().flush()
            } catch (Exception e) {
                logger.error("thumbnail: failed [message=${e.message}, class=${e.getClass().getName()}]", e)
                return
            }
        } else {
            logger.warn('thumbnail: image not found')
            return
        }
    }

}
