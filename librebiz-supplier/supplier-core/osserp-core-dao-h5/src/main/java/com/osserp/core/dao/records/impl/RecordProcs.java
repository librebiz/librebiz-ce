/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 10, 2007 10:06:54 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.OptionsCache;

import com.osserp.core.Options;
import com.osserp.core.dao.impl.Schema;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordProcs extends Schema {
    private static Logger log = LoggerFactory.getLogger(RecordProcs.class.getName());

    /**
     * Indicates that product is deleteable (e.g. not referenced in any record) $1 : productid BIGINT
     */
    public static final String IS_PRODUCT_DELETEABLE =
            "SELECT " + SCHEMA + "is_product_deleteable(?)";

    /**
     * Provides open purchase order items by product id $1 : stockid BIGINT $2 : productid BIGINT
     */
    public static final String PURCHASE_ORDERS_OPEN_ITEMS_BY_STOCK_AND_PRODUCT =
            "SELECT * FROM " + SCHEMA + "get_purchase_order_open_items(?,?)";

    /**
     * Provides unreleased items of sales delivery notes by product id $1 : productid BIGINT
     */
    public static final String SALES_DELIVERY_NOTE_UNRELEASED_ITEMS_BY_PRODUCT =
            "SELECT * FROM " + SCHEMA + "get_sales_delivery_note_unreleased_items(?)";

    /**
     * Provides open sales order items by product id If product id is null all products will be fetched $1 : stockid BIGINT $2 : productid BIGINT
     */
    public static final String SALES_ORDERS_OPEN_ITEMS_BY_STOCK_AND_PRODUCT =
            "SELECT * FROM " + SCHEMA + "get_sales_order_open_items(?,?)";

    /**
     * Provides open sales order items by product id If product id is null all products will be fetched $1 : stockid BIGINT $2 : productid BIGINT
     */
    public static final String SALES_ORDERS_UNRELEASED_ITEMS_BY_STOCK_AND_PRODUCT =
            "SELECT * FROM " + SCHEMA + "get_sales_order_unreleased_items(?,?)";

    /**
     * Provides vacant sales order items by product id If product id is null all products will be fetched $1 : stockid BIGINT $2 : productid BIGINT
     */
    public static final String SALES_ORDERS_VACANT_ITEMS_BY_STOCK_AND_PRODUCT =
            "SELECT * FROM " + SCHEMA + "get_sales_order_vacant_items(?,?)";


    /**
     * Provides a bookingType by recordType and id
     * @param namesCache
     * @param recordType
     * @param id
     * @return bookingType or null if not exists or recordType or id not provided
     */
    public static final BookingType getBookingType(OptionsCache namesCache, RecordType recordType, Long id) {
        BookingType type = null;
        if (recordType != null && recordType.isSupportingBookingTypes() 
                && recordType.getResourceKey() != null && id != null && id > 0) {
            type = (BookingType) namesCache.getMapped((recordType.getResourceKey() + "Types"), id);
        }
        return type;
    }

    /**
     * Provides a map of all existing record types including payments
     * @param namesCache
     * @return map with record and payment types
     */
    public static final RecordType getRecordType(OptionsCache namesCache, Long id) {
        RecordType type = null;
        if (id != null && id > 0) {
            type = (RecordType) namesCache.getMapped(Options.RECORD_TYPES, id);
            if (type == null) {
                type =  (RecordType) namesCache.getMapped(Options.PAYMENT_TYPES, id);
            }
        } 
        return type;
    }

    /**
     * Provides a map of all existing record types including payments
     * @param namesCache
     * @return map with record and payment types
     */
    public static final Map<Long, RecordType> getRecordTypes(OptionsCache namesCache) {
        try {
            Map<Long, RecordType> result = new HashMap(namesCache.getMap(Options.RECORD_TYPES));
            result.putAll(new HashMap(namesCache.getMap(Options.PAYMENT_TYPES)));
            return result;
        } catch (Exception e) {
            return new HashMap<Long, RecordType>();
        }
    }

    /**
     * Provides a map of all existing record types including payments
     * @param namesCache
     * @return map with record and payment types
     */
    public static final Map<Long, RecordType> getBillingTypes(OptionsCache namesCache) {
        try {
            Map<Long, RecordType> result = new HashMap(namesCache.getMap(Options.RECORD_TYPES));
            List<BillingType> billingTypes = new ArrayList(namesCache.getList(Options.BILLING_TYPES));
            for (int i = 0, j = billingTypes.size(); i < j; i++) {
                BillingType next = billingTypes.get(i);
                // billingTypes with associated recordType are already added by 
                // the recordTypes assignment above
                if (next.getRecordType() == null || next.getRecordType() == 0) {
                    result.put(next.getId(), next);
                } else if (log.isDebugEnabled()) {
                    log.debug("getBillingTypes() ignoring type with associated recordType [id="
                            + next.getId() + ", recordType=" + next.getRecordType() + "]");
                }
            }
            return result;
        } catch (Exception e) {
            return new HashMap<Long, RecordType>();
        }
    }
}
