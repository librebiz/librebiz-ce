/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Aug 02, 2011 8:50:59 AM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.telephone.TelephoneCallContacts;
import com.osserp.core.model.telephone.TelephoneCallContactImpl;
import com.osserp.core.telephone.TelephoneCallContact;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneCallContactsDao extends AbstractDao implements TelephoneCallContacts {

    protected TelephoneCallContactsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public List<TelephoneCallContact> find(Long telephoneCallId) {
        if (telephoneCallId != null) {
            return getCurrentSession().createQuery("from "
                    + TelephoneCallContactImpl.class.getName()
                    + " tcc where tcc.callId = :callid")
                    .setParameter("callid", telephoneCallId ).list();
        }
        return new ArrayList<TelephoneCallContact>();
    }
}
