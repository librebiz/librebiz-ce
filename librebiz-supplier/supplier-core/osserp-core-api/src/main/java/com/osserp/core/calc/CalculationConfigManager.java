/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 26, 2005 
 * 
 */
package com.osserp.core.calc;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.BusinessType;
import com.osserp.core.ItemPosition;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CalculationConfigManager {

    /**
     * Creates a new calculation config of specified calculation type
     * @param type of the calculation
     * @param name
     * @return config new created
     * @throws ClientException if a config of given type for given project type already exists
     */
    CalculationConfig createConfig(CalculationType type, String name) throws ClientException;

    /**
     * Provides a calculation config value object
     * @param id of the calculation config
     * @return calculationConfig values
     */
    CalculationConfig getConfig(Long id);

    /**
     * Finds a calculation config related to given calculation and project type
     * @param requestType the requested config is designated for
     * @return config matching the given parms
     */
    CalculationConfig findConfig(BusinessType requestType);

    /**
     * Provides a list of all available calculation configs without related product groups and products. The list is for selections only.
     * @return configs as value objects
     */
    List<CalculationConfig> getConfigs();

    /**
     * Provides a list of all available calculation configs of given type without related product groups and products. The list is for selections only.
     * @param typeId of the requested configs
     * @return config value objects
     */
    List<CalculationConfig> getConfigs(Long typeId);

    /**
     * Updates a calculation config
     * @param config
     * @param name to update
     * @throws ClientException if name not already existing under another calculation config
     */
    CalculationConfig updateConfig(CalculationConfig config, String name) throws ClientException;

    /**
     * Finds a calculation group if exists
     * @param position
     * @return calculationConfigGroup values or null if not exists
     */
    CalculationConfigGroup findGroup(ItemPosition position);

    /**
     * Finds a calculation group if exists
     * @param id
     * @return calculationConfigGroup or null if not exists
     */
    CalculationConfigGroup findGroup(Long id);

    /**
     * Adds a new calculation group to a calculation config
     * @param config
     * @param name
     * @param discounts
     * @param option
     * @throws ClientException if name is null or already existing
     */
    CalculationConfigGroup addGroup(CalculationConfig config, String name, boolean discounts, boolean option) throws ClientException;

    /**
     * Adds a new calculation group to a calculation config
     * @param config
     * @param name
     * @param discounts
     * @param option
     * @throws ClientException if name is null or already existing
     */
    CalculationConfigGroup updateGroup(CalculationConfig config, CalculationConfigGroup group, String name, boolean discounts, boolean option)
            throws ClientException;

    /**
     * Removes a group from a calculation config
     * @param config
     * @param groupId of the group to remove
     */
    void removeGroup(CalculationConfig config, Long groupId);

    /**
     * Adds an product selection to calculation config
     * @param config
     * @param groupId
     * @param configId
     * @return updated config
     */
    CalculationConfig addProductSelection(CalculationConfig config, Long groupId, Long configId);

    /**
     * Removes an product selection from calculation config
     * @param config
     * @param groupId
     * @param configId
     * @return updated config
     */
    CalculationConfig removeProductSelection(CalculationConfig config, Long groupId, Long configId);

    /**
     * Adds or removes part list declaration and config
     * @param config
     * @param groupId
     * @param partListConfig or null to remove previous selection
     * @return config
     */
    CalculationConfig setPartList(CalculationConfig config, Long groupId, CalculationConfig partListConfig);

    /**
     * Creates a new calculation type
     * @param name for the new type
     * @param description for the new type
     * @return new created type
     * @throws ClientException if name or description not set or name exists
     */
    CalculationType createType(String name, String description) throws ClientException;

    /**
     * Updates a calculation type
     * @param type
     * @param name to update
     * @param description to update
     * @throws ClientException if name or description not set or name already existing under another calculation type id
     */
    CalculationType updateType(CalculationType type, String name, String description) throws ClientException;

    /**
     * Provides a calculation type by primary key.
     * @return type
     * @throws ActionException if type not exists
     */
    CalculationType getType(Long id);

    /**
     * Provides a list of all available calculation types. Use this method to display a list of available types for create new config actions
     * @return calculation types
     */
    List<CalculationType> getTypes();
}
