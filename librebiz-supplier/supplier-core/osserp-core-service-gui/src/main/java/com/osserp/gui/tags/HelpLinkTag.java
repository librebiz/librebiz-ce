/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 8, 2006 2:24:52 PM 
 * 
 */
package com.osserp.gui.tags;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.web.Page;
import com.osserp.common.web.SessionUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class HelpLinkTag extends TagSupport {

    private static final long serialVersionUID = 42L;
    private static Logger log = LoggerFactory.getLogger(HelpLinkTag.class.getName());

    @Override
    public int doStartTag() throws JspException {
        try {
            HttpSession session = pageContext.getSession();
            Page page = SessionUtil.getPage(session);
            if (log.isDebugEnabled()) {
                if (page == null) {
                    log.debug("doStartTag() no page found");
                } else {
                    log.debug("doStartTag() page found: " + page.getName());
                }
            }
            /*
             * NodeService service = ServiceManager.createHelpDivService(session); if (service.createRequired()) { if (log.isInfoEnabled()) { try { if
             * (service.isViewAvailable()) { Node node = ((NodeView) service.getView()).getCurrentNode(); if (node != null) {
             * log.info("doStartTag() removing help from " + node.getName() + " for " + page.getName()); } } } catch (Throwable debug) { } }
             * service.removeView(); }
             */
        } catch (Throwable t) {
            log.warn("doStartTag() ignoring exception: " + t.toString());
        }
        return (SKIP_BODY);
    }

    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }
}
