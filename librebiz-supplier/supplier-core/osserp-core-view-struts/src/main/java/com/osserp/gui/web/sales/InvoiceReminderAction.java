/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 11, 2007 11:26:11 PM 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.gui.client.records.InvoiceReminderView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class InvoiceReminderAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(InvoiceReminderAction.class.getName());

    @Override
    public ActionForward forward(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        InvoiceReminderView view = createReminderView(request);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Adds a company to current company selection
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward addCompany(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("addCompany() request from " + getUser(session).getId());
        }
        InvoiceReminderView view = getReminderView(session);
        view.addCompany(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Removes a company from current company selection
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward removeCompany(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("removeCompany() request from " + getUser(session).getId());
        }
        InvoiceReminderView view = getReminderView(session);
        view.removeCompany(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles view to include/not include internal invoices
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleIncludeInternal(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleIncludeInternal() request from " + getUser(session).getId());
        }
        InvoiceReminderView view = getReminderView(session);
        view.toggleIncludeInternal();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Performs a new query
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward refresh(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("refresh() request from " + getUser(session).getId());
        }
        InvoiceReminderView view = getReminderView(session);
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to xls output
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward xls(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("xls() request from " + getUser(session).getId());
        }
        InvoiceReminderView view = getReminderView(session);
        return forwardXlsOutput(mapping, request, view.getSpreadsheet());
    }

    @Override
    protected Class<InvoiceReminderView> getViewClass() {
        return InvoiceReminderView.class;
    }

    private InvoiceReminderView fetchReminderView(HttpSession session) throws PermissionException {
        return (InvoiceReminderView) fetchView(session);
    }

    private InvoiceReminderView getReminderView(HttpSession session) throws PermissionException {
        InvoiceReminderView view = fetchReminderView(session);
        if (view == null) {
            log.warn("getReminderView() failed, view not bound!");
        }
        return view;
    }

    private InvoiceReminderView createReminderView(HttpServletRequest request) throws ClientException, PermissionException {
        InvoiceReminderView view = fetchReminderView(request.getSession());
        if (view == null) {
            view = (InvoiceReminderView) createView(request);
            String exit = RequestUtil.getExit(request);
            if (exit != null) {
                view.setExitTarget(exit);
            }
        }
        return view;
    }
}
