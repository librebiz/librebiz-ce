<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="customerQueryIndexView" />
<c:set var="customerListingView" scope="request" value="${sessionScope.customerQueryIndexView}"/>
<c:set var="customerListName" scope="request" value="${view.selectedQueryType}"/>
<c:set var="customerSelectExitTarget" scope="request" value="customerQueryIndex"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>

    <tiles:put name="headline">
        <c:if test="${!empty view.list}"><o:listSize value="${view.list}"/> </c:if>
        <fmt:message key="${view.headerName}"/>
        <c:if test="${view.headerName == 'customerlistLastSalesBefore' && !empty view.dateBefore}">
            <o:date value="${view.dateBefore}"/>
        </c:if>
    </tiles:put>

    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="customerQueryIndexContent">
            <div class="row">
                <c:choose>
                    <c:when test="${!empty view.list}">
                        <c:import url="_customerQueryResult.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_customerQuerySelection.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
