/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 27, 2009 7:52:03 PM 
 * 
 */
package com.osserp.core.model.products;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.util.CollectionUtil;

import com.osserp.core.products.ProductClassificationEntity;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductType;
import com.osserp.core.products.ProductTypeConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductTypeConfigImpl extends AbstractProductType implements ProductTypeConfig {

    private List<ProductGroupConfig> groupList = new ArrayList<ProductGroupConfig>();

    protected ProductTypeConfigImpl() {
        super();
    }

    /**
     * Constructor for new product types
     * @param name
     * @param description
     * @param createdBy
     */
    public ProductTypeConfigImpl(String name, String description, Long createdBy) {
        super(name, description, createdBy);
    }

    protected ProductTypeConfigImpl(ProductTypeConfig o) {
        super();
        if (!o.getGroups().isEmpty()) {
            for (int i = 0, j = o.getGroups().size(); i < j; i++) {
                ProductGroupConfig next = o.getGroups().get(i);
                groupList.add((ProductGroupConfig) next.clone());
            }
        }
    }

    public List<ProductGroupConfig> getGroups() {
        return new ArrayList<ProductGroupConfig>(groupList);
    }

    public void addGroup(ProductGroupConfig group) {
        if (!supportsGroup(group)) {
            groupList.add(group);
        }
    }

    public void removeGroup(ProductGroupConfig group) {
        for (Iterator<ProductGroupConfig> i = groupList.iterator(); i.hasNext();) {
            ProductGroupConfig existing = i.next();
            if (existing.getId().equals(group.getId())) {
                i.remove();
                break;
            }
        }
    }

    public ProductGroupConfig fetchGroup(Long id) {
        for (int i = 0, j = groupList.size(); i < j; i++) {
            ProductGroupConfig existing = groupList.get(i);
            if (existing.getId().equals(id)) {
                return existing;
            }
        }
        return null;
    }

    public boolean supportsGroup(ProductClassificationEntity group) {
        return (fetchGroup(group.getId()) != null);
    }

    public void moveNext(ProductGroupConfig group) {
        if (group != null) {
            CollectionUtil.moveNext(groupList, group);
        }
    }

    public void movePrevious(ProductGroupConfig group) {
        if (group != null) {
            CollectionUtil.movePrevious(groupList, group);
        }
    }

    public ProductType getProductType() {
        return new ProductTypeImpl(this);
    }

    protected List<ProductGroupConfig> getGroupList() {
        return groupList;
    }

    protected void setGroupList(List<ProductGroupConfig> groupList) {
        this.groupList = groupList;
    }

    @Override
    public Object clone() {
        return new ProductTypeConfigImpl(this);
    }
}
