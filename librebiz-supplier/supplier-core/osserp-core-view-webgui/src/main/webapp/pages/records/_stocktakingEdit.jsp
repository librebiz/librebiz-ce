<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.stocktakingView}" />
<c:set var="obj" value="${view.bean}" />
<o:form name="stocktakingForm" url="/stocktakings.do">
    <input type="hidden" name="method" value="save" />
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="editActualStockTaking" /></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="stockTakingType" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${obj.type.name}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="createdBy" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:ajaxLink linkId="createdBy" url="${'/employees/employeePopup/forward?id='}${obj.createdBy}">
                                <oc:employee value="${obj.createdBy}" />
                            </v:ajaxLink>
                            <span> / </span><o:date value="${obj.created}" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="stockTakingName" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="<o:out value="${obj.name}" />" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="liable" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="counter" class="form-control" size="1">
                                <option value="0"><fmt:message key="selection" /></option>
                                <c:forEach var="emp" items="${view.employees}">
                                    <option value="<o:out value="${emp.id}"/>" <c:if test="${obj.counter == emp.id}">selected="selected"</c:if>><o:out value="${emp.name}" /></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="input" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="writer" class="form-control" size="1">
                                <option value="0"><fmt:message key="selection" /></option>
                                <c:forEach var="emp" items="${view.employees}">
                                    <option value="<o:out value="${emp.id}"/>" <c:if test="${obj.writer == emp.id}">selected="selected"</c:if>><o:out value="${emp.name}" /></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="comments" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <textarea class="form-control" name="note" rows="3"><o:out value="${obj.note}" /></textarea>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4"> </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/stocktakings.do?method=disableEdit"/>');" value="<fmt:message key="exit"/>" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" value="save" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</o:form>
