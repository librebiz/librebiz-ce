/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 23, 2008 2:01:46 PM 
 * 
 */
package com.osserp.gui.web.hrm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestForm;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.gui.client.hrm.TimeRecordingView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingAction.class.getName());

    @Override
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        super.forward(mapping, form, request, response);
        TimeRecordingView view = getTimeRecordingView(request.getSession());
        view.checkDayOpen();
        if (view.getTimeRecording() == null
                || view.getTimeRecording().getSelectedPeriod() == null) {
            return saveErrorAndReturn(request, "timerecording.noPeriodSelected");
        }
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    public ActionForward enableEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        super.enableEdit(mapping, form, request, response);
        TimeRecordingView view = getTimeRecordingView(request.getSession());
        view.checkDayOpen();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to 'displaySummary' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward displaySummary(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("displaySummary() request from " + getUser(session).getId());
        }
        return mapping.findForward("displaySummary");
    }

    /**
     * Forwards to 'displayAddMarker' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward displayAddMarker(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("displayAddMarker() request from " + getUser(session).getId());
        }
        return mapping.findForward("displayAddMarker");
    }

    /**
     * Selects time record type
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectType(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectType() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        view.selectType(RequestUtil.fetchId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables manual recording mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableManualRecordingMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableManualRecordingMode() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        view.setManualRecordingMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables manual recording mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableManualRecordingMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableManualRecordingMode() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        view.setManualRecordingMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles list descendent mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleListDescendentMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleListDescendentMode() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        view.toggleListDescendentMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles list details mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleListDetailsMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleListDetailsMode() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        view.toggleListDetailsMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles list details mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleDisplayJournalMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleDisplayJournalMode() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        view.toggleDisplayJournalMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles list marker mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleDisplayMarkerMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleDisplayMarkerMode() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        view.toggleDisplayMarkerMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes a time record by 'id' param
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward deleteRecord(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("deleteRecord() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.removeRecord(RequestUtil.fetchId(request));
            return mapping.findForward(view.getActionTarget());
        } catch (PermissionException t) {
            return saveErrorAndReturn(request, t.getMessage());
        }
    }

    /**
     * Deletes a marker by 'id' param
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward deleteMarker(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("deleteMarker() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.removeMarker(RequestUtil.fetchId(request));
            return mapping.findForward(view.getActionTarget());
        } catch (PermissionException t) {
            return saveErrorAndReturn(request, t.getMessage());
        }
    }

    /**
     * Forwards to create a new record with preselected day <br/>
     * param day as d <br/>
     * param month as m <br/>
     * param year as y
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardCreate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardCreate() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        Integer day = RequestUtil.getInteger(request, "d");
        Integer month = RequestUtil.getInteger(request, "m");
        Integer year = RequestUtil.getInteger(request, "y");
        request.setAttribute("intervalOnly", RequestUtil.getBoolean(request, "intervalOnly"));
        view.selectDate(day, month, year);
        view.setEditMode(true);
        return mapping.findForward("typeSelection");
    }

    /**
     * Forwards to period select popup
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward showPeriods(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("showPeriods() request from " + getUser(session).getId());
        }
        return mapping.findForward("showPeriods");
    }

    /**
     * Selects a time recording month. Expected params: <br/>
     * month <br/>
     * year
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectMonth(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectMonth() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.getTimeRecording().getSelectedPeriod().selectMonth(RequestUtil.fetchMonth(request));
        } catch (Throwable t) {
            log.warn("selectMonth() failed [message=" + t.getMessage() + "]");
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Selects a period. Expected params: <br/>
     * id
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectPeriod(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectPeriod() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.getTimeRecording().selectPeriod(RequestUtil.fetchId(request));
            view.getTimeRecording().getSelectedPeriod().selectMonth(null);
        } catch (Throwable t) {
            log.warn("selectMonth() failed [message=" + t.getMessage() + "]");
        }
        return reloadParent(mapping);
    }

    /**
     * Creates a period
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createPeriod(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createPeriod() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.createPeriod();
        } catch (Throwable t) {
            log.warn("createPeriod() failed [message=" + t.getMessage() + "]");
        }
        return reloadParent(mapping);
    }

    /**
     * Selects a time record and forwards to 'recordUpdate' target if id param with valid record id exists. Otherwise selection will be resetted to null and
     * forward goes to views action target. Expected params: <br/>
     * id for selection, null for reset
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectRecord(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectRecord() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        view.selectRecord(RequestUtil.fetchId(request));
        if (view.getSelectedRecord() != null) {
            // enable request
            String target = RequestUtil.getTarget(request);
            if (target != null) {
                if (log.isDebugEnabled()) {
                    log.debug("selectRecord() found forward [target=" + target + "]");
                }
                return mapping.findForward(target);
            }
            return mapping.findForward("recordDisplay");
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates a time record. Forwards to 'recordUpdate' target if errors found. Returns reload parent popup if finalized
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateRecord(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateRecord() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.updateRecord(new RequestForm(request));
            return reloadParent(mapping);
        } catch (ClientException e) {
            saveError(request, e.getMessage());
            return mapping.findForward("recordUpdate");
        }
    }

    /**
     * Enables config change mode for current selected period
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableConfigChangeMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableConfigChangeMode() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.changeConfigMode(true);
        } catch (PermissionException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables config change mode for current selected period
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableConfigChangeMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableConfigChangeMode() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.changeConfigMode(false);
        } catch (PermissionException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates a time record. Forwards to 'recordUpdate' target if errors found. Returns reload parent popup if finalized
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeConfig() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.changeConfig(RequestUtil.fetchId(request));
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return forward(mapping, form, request, response);
    }

    /**
     * Updates a time record. Forwards to 'recordUpdate' target if errors found. Returns reload parent popup if finalized
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updatePeriod(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updatePeriod() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.updatePeriod(new RequestForm(request));
            return reloadParent(mapping);
        } catch (Exception e) {
            saveError(request, e.getMessage());
            return mapping.findForward("periodUpdate");
        }
    }

    /**
     * Add marker Returns reload parent popup if finalized
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward addMarker(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("addMarker() request from " + getUser(session).getId());
        }
        TimeRecordingView view = getTimeRecordingView(session);
        try {
            view.addMarker(new RequestForm(request));
            return reloadParent(mapping);
        } catch (Exception e) {
            saveError(request, e.getMessage());
            return mapping.findForward("displayAddMarker");
        }
    }

    @Override
    public ActionForward enableSetup(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        super.enableSetup(mapping, form, request, response);
        return mapping.findForward("periodUpdate");
    }

    @Override
    protected Class<? extends View> getViewClass() {
        return TimeRecordingView.class;
    }

    private TimeRecordingView getTimeRecordingView(HttpSession session) {
        TimeRecordingView view = (TimeRecordingView) fetchView(session);
        if (view == null) {
            throw new ActionException("view not bound [class=" + TimeRecordingView.class.getName() + "]");
        }
        return view;
    }
}
