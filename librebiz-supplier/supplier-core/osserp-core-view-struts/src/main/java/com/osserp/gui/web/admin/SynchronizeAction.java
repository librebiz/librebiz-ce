/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Sep 16, 2009 11:47:18 AM 
 * 
 */
package com.osserp.gui.web.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.web.Actions;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.gui.client.admin.SynchronizeView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class SynchronizeAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(SynchronizeAction.class.getName());

    @Override
    protected Class<? extends View> getViewClass() {
        return SynchronizeView.class;
    }

    /**
     * Synchronizes everything
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward syncAll(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("syncAll() request from " + getUser(session).getId());
        }
        SynchronizeView view = (SynchronizeView) fetchView(session);
        if (view == null) {
            view = (SynchronizeView) createView(request);
        }
        view.syncAll();
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Synchronizes all branches
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward syncBranches(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("syncBranches() request from " + getUser(session).getId());
        }
        SynchronizeView view = (SynchronizeView) fetchView(session);
        if (view == null) {
            view = (SynchronizeView) createView(request);
        }
        view.syncBranches();
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Synchronizes all employee groups
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward syncEmployeeGroups(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("syncEmployeeGroups() request from " + getUser(session).getId());
        }
        SynchronizeView view = (SynchronizeView) fetchView(session);
        if (view == null) {
            view = (SynchronizeView) createView(request);
        }
        view.syncEmployeeGroups();
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Synchronizes all employees
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward syncEmployees(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("syncEmployees() request from " + getUser(session).getId());
        }
        SynchronizeView view = (SynchronizeView) fetchView(session);
        if (view == null) {
            view = (SynchronizeView) createView(request);
        }
        view.syncEmployees();
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Synchronizes the employee with private cotnacts and group relations
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward syncEmployee(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("syncEmployee() request from " + getUser(session).getId());
        }
        SynchronizeView view = (SynchronizeView) fetchView(session);
        Long id = RequestUtil.getId(request);
        view.syncEmployee(id);
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Synchronizes all private contacts
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward syncPrivateContacts(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("syncPrivateContacts() request from " + getUser(session).getId());
        }
        SynchronizeView view = (SynchronizeView) fetchView(session);
        if (view == null) {
            view = (SynchronizeView) createView(request);
        }
        view.syncPrivateContacts();
        return mapping.findForward(Actions.DISPLAY);
    }
}
