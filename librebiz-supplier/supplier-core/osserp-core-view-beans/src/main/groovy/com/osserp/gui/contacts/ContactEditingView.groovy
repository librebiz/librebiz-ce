/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 20, 2016 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.Constants
import com.osserp.common.ErrorCode
import com.osserp.common.Option
import com.osserp.common.util.EmailValidator
import com.osserp.common.util.PhoneUtil

import com.osserp.core.Address
import com.osserp.core.Options
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactManager
import com.osserp.core.contacts.ContactSearch
import com.osserp.core.contacts.ContactType
import com.osserp.core.contacts.ContactValidator
import com.osserp.core.contacts.Person
import com.osserp.core.contacts.PersonManager
import com.osserp.core.contacts.Salutation

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class ContactEditingView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ContactEditingView.class.getName())
    public static final boolean REQUIRED = true
    public static final boolean OPTIONAL = false

    List<ContactType> contactTypes = []
    ContactType selectedType

    Contact parent

    boolean requiredValidationOnly = false
    boolean customFederalState = false

    ContactEditingView() {
        super()
    }

    Person getPerson() {
        bean
    }

    protected abstract void countrySelection(Long id);

    protected abstract void localitySelection(String zipcode);

    protected abstract PersonManager getPersonManager();

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            contactTypes = getOptions(Options.CONTACT_TYPES)
            requiredValidationOnly = contactManager.requiredValidationOnly
            addressAutocompletionSupport = addressAutocompletionSupportEnabled
        }
    }

    void selectSelectedType(Long id) {
        if (!id) {
            selectedType = null
        } else {
            selectedType = contactTypes.find { it.id == id }
        }
    }

    boolean isPersonSelected() {
        (selectedType?.privatePerson || selectedType?.contactPerson || selectedType?.freelance)
    }

    boolean isPrivateContactSelected() {
        selectedType?.privatePerson
    }

    boolean isContactPersonSelected() {
        selectedType?.contactPerson
    }

    boolean isFreelanceSelected() {
        selectedType?.freelance
    }

    /**
     * Validates all input values
     * @throws ClientException if validation failed
     */
    void validate() throws ClientException {
        validateContact()
        validateAddress(parent != null || requiredValidationOnly ? OPTIONAL : REQUIRED)
        validateCommunication(OPTIONAL)
    }

    /**
     * Validates the address values provided by form
     * @param required if the address must be complete
     * @throws ClientException if validation failed
     */
    protected void validateAddress(boolean required) throws ClientException {
        Long addressType = selectedType?.isContactPerson() || selectedType?.isPrivatePerson() ? Address.HOME : Address.BUSINESS
        ContactValidator.validateAddress(
                selectedType?.id,
                addressType,
                form.getString('street'),
                form.getString('zipcode'),
                form.getString('city'),
                form.getLong('country'),
                required)
    }

    /**
     * Validates the contact values provided by form
     * @throws ClientException if validation failed
     */
    protected void validateContact() throws ClientException {
        if (!requiredValidationOnly) {

            if (isPrivateContactSelected() || isContactPersonSelected()) {
                ContactValidator.validateContact(
                        selectedType?.id,
                        form.getLong('salutation'),
                        form.getString('firstName'),
                        form.getString('lastName'))

                Date birthDate = form.getDate('birthDate')
                if (birthDate) {
                    Date now = new Date(System.currentTimeMillis())
                    if (now.before(birthDate)) {
                        throw new ClientException(ErrorCode.DATE_FUTURE)
                    }
                }
            } else {
                ContactValidator.validateContact(
                        selectedType?.id,
                        form.getLong('salutation'),
                        (String) null,
                        createMode ? form.getString('company') : form.getString('lastName'))
            }
        }
    }

    /**
     * Creates primary communication numbers and email address
     * @throws ClientException if a value in object is invalid
     */
    protected void validateCommunication(boolean required) throws ClientException {
        validateEmail(required)
        validateFax(required)
        validateMobile(required)
        validatePhone(required)
    }

    /**
     * Validates the email address provided by form['email']
     * @param required if email must not null or empty
     * @throws ClientException if email is invalid or empty when required is set true.
     */
    protected void validateEmail(boolean required) throws ClientException {
        EmailValidator.validate(form.getString('email'), required)
    }

    /**
     * Validates the fax number provided by form['fprefix'] and form['fnumber']
     * @param required if fax number must not null or empty
     * @throws ClientException if fax number is invalid or empty when required is set true.
     */
    protected void validateFax(boolean required) throws ClientException {
        PhoneUtil.validateFax(form.getString('fprefix'), form.getString('fnumber'), required)
    }

    /**
     * Validates the mobile number provided by form['mprefix'] and form['mnumber']
     * @param required if mobile number must not null or empty
     * @throws ClientException if mobile number is invalid or empty when required is set true.
     */
    protected void validateMobile(boolean required) throws ClientException {
        PhoneUtil.validateMobile(form.getString('mprefix'), form.getString('mnumber'), required)
    }

    /**
     * Validates the phone number provided by form['pprefix'] and form['pnumber']
     * @param required if phone number must not null or empty
     * @throws ClientException if phone number is invalid or empty when required is set true.
     */
    protected void validatePhone(boolean required) throws ClientException {
        PhoneUtil.validatePhone(form.getString('pprefix'), form.getString('pnumber'), required)
    }

    protected Salutation getSalutation(Long id) {
        if (!id) {
            id = Constants.LONG_NULL
        }
        getOption(Options.SALUTATIONS, id)
    }

    protected Option getTitle(Long id) {
        getOption(Options.TITLES, id)
    }

    protected ContactManager getContactManager() {
        getService(ContactManager.class.getName())
    }

    protected ContactSearch getContactSearch() {
        getService(ContactSearch.class.getName())
    }
}
