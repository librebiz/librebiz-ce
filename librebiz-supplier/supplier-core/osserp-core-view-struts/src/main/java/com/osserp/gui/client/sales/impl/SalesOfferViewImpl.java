/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 24, 2006 12:19:21 PM 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.BusinessCase;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculatorService;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesOfferManager;
import com.osserp.core.sales.SalesRevenue;
import com.osserp.core.sales.SalesRevenueManager;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.records.impl.AbstractDiscountAwareRecordView;
import com.osserp.gui.client.sales.SalesOfferView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesOfferView")
public class SalesOfferViewImpl extends AbstractDiscountAwareRecordView implements SalesOfferView {
    private static Logger log = LoggerFactory.getLogger(SalesOfferViewImpl.class.getName());

    private BusinessCaseView businessCaseView = null;
    private boolean copyMode = false;

    protected SalesOfferViewImpl() {
        super();
    }

    protected SalesOfferViewImpl(String confirmDiscountPermissions) {
        super(confirmDiscountPermissions);
    }

    public void load(BusinessCaseView businessCaseView) throws ClientException {
        this.businessCaseView = businessCaseView;
        SalesOfferManager manager = (SalesOfferManager) getRecordManager();
        List<Offer> offers = manager.findByReference(getDomainUser()
                .getEmployee(), businessCaseView.getRequest());
        if (offers.size() == 1) {
            setRecord(loadRecord(offers.get(0).getId()));
        }
        setList(offers);
        loadBusinessTemplates(businessCaseView.getRequest());
    }

    public void createOffer(Calculation calc, boolean copy) throws ClientException {
        SalesOfferManager manager = (SalesOfferManager) getRecordManager();
        Offer newOffer = manager.create(getDomainEmployee(), businessCaseView.getRequest(), calc);
        if (copy) {
            Offer oldOffer = (Offer) getRecord();
            manager.update(newOffer,
                    null,
                    null,
                    oldOffer.getSignatureLeft(),
                    oldOffer.getSignatureRight(),
                    oldOffer.getPersonId(),
                    oldOffer.getNote(),
                    (oldOffer.getTaxFreeId() == null ? false : true),
                    oldOffer.getTaxFreeId(),
                    oldOffer.getAmounts().getTaxRate(),
                    oldOffer.getAmounts().getReducedTaxRate(),
                    oldOffer.getCurrency(),
                    oldOffer.getLanguage(),
                    oldOffer.getBranchId(),
                    oldOffer.getShippingId(),
                    oldOffer.getCustomHeader(),
                    oldOffer.isPrintComplimentaryClose(),
                    oldOffer.isPrintProjectmanager(),
                    oldOffer.isPrintSalesperson(),
                    oldOffer.isPrintBusinessCaseId(),
                    oldOffer.isPrintBusinessCaseInfo(),
                    oldOffer.isPrintRecordDate(),
                    oldOffer.isPrintRecordDateByStatus(),
                    oldOffer.isPrintPaymentTarget(),
                    oldOffer.isPrintConfirmationPlaceholder(),
                    oldOffer.isPrintCoverLetter(),
                    null); // calculationInfo was already set by create,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  // null keeps existing info untouched 
        }
        load(businessCaseView);
        setRecord(manager.getByCalculation(calc));
    }

    public void load(BusinessCaseView businessCaseView, Calculation calc) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("load(businessCaseView, calc) invoked [businessCase="
                    + businessCaseView.getId()
                    + ", calc=" + calc.getId() + "]");
        }
        load(businessCaseView);
        SalesOfferManager manager = (SalesOfferManager) getRecordManager();
        Offer offer = manager.getByCalculation(calc);
        setRecord(offer);
    }

    public BusinessCaseView getBusinessCaseView() {
        return businessCaseView;
    }

    protected void setBusinessCaseView(BusinessCaseView businessCaseView) {
        this.businessCaseView = businessCaseView;
    }

    public boolean isBusinessCaseClosed() {
        if (businessCaseView == null || businessCaseView.getBusinessCase() == null) {
            return true;
        }
        return businessCaseView.getRequest().isClosed();
    }

    public boolean initializeActivated() {
        if (getList() == null || getList().isEmpty()) {
            return false;
        }
        for (int i = 0, j = getList().size(); i < j; i++) {
            Offer current = (Offer) getList().get(i);
            if (Offer.STAT_CLOSED.equals(current.getStatus())) {
                setRecord(current);
                return true;
            }
        }
        return false;
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        Offer record = (Offer) getRecord();
        RecordManager manager = getRecordManager();
        ((SalesOfferManager) manager).update(
                record,
                form.getDate("delivery"), // unused, see comment in page
                form.getDate("validUntil"),
                form.getLong("signatureLeft"),
                form.getLong("signatureRight"),
                form.getLong("personId"),
                form.getString("note"),
                form.getBoolean("taxFree"),
                form.getLong("taxFreeId"),
                form.getDouble("taxRate"),
                form.getDouble("reducedTaxRate"),
                form.getLong("currency"),
                form.getString("language"),
                form.getLong("branch"),
                form.getLong("shippingId"),
                form.getString("customHeader"),
                form.getBoolean("printComplimentaryClose"),
                form.getBoolean("printProjectmanager"),
                form.getBoolean("printSalesperson"),
                form.getBoolean("printBusinessCaseId"),
                form.getBoolean("printBusinessCaseInfo"),
                form.getBoolean("printRecordDate"),
                form.getBoolean("printRecordDateByStatus"),
                form.getBoolean("printPaymentTarget"),
                form.getBoolean("printConfirmationPlaceholder"),
                form.getBoolean("printCoverLetter"),
                form.getString("name"));
        refresh();
        savePrintOptionDefaults(form, getRecord());
    }

    public boolean isUnreleasedOfferAvailable() {
        if (businessCaseView != null &&
                getList().size() < getSalesOfferManager()
                        .getReferencedCount(businessCaseView.getRequest())) {
            refreshList();
        }
        List<Offer> list = getList();
        if (list == null || list.isEmpty()) {
            return false;
        }
        for (int i = 0, j = list.size(); i < j; i++) {
            Offer next = list.get(i);
            if (!next.isUnchangeable()) {
                return true;
            }
        }
        return false;
    }

    public boolean isOnlyOneOfferAvailable() {
        try {
            return (getList() != null && getList().size() == 1);
        } catch (Exception e) {
            return true;
        }
    }

    // copy a calulation independent offer

    public boolean isCreateCopyAvailable() {
        return (!copyMode && businessCaseView != null
                && !businessCaseView.getRequest().getType().isRecordByCalculation()
                && !isUnreleasedOfferAvailable()
                && ((getBean() == null && !getList().isEmpty()) ||
                        (getBean() != null && getRecord().isUnchangeable()))
                && !isCopyUnavailableByContextSwitch());
    }

    public void createCopy(Form form) throws ClientException {
        SalesOffer offer = null;
        Long id = form.getLong("id");
        if (isSet(id)) {
            offer = (SalesOffer) getSalesOfferManager().find(id);
        }
        if (offer == null) {
            offer = (SalesOffer) getRecord();
        }
        if (offer != null && offer.isUnchangeable()
                && businessCaseView != null
                && !businessCaseView.getRequest().getType().isRecordByCalculation()) {
            if (log.isDebugEnabled()) {
                log.debug("createCopy() invoked [offer=" + offer.getId() + "]");
            }
            SalesOfferManager salesOfferManager = getSalesOfferManager();
            Record createdCopy = salesOfferManager.create(getDomainEmployee(), offer);
            getList().add(createdCopy);
            setRecord(createdCopy);

        } else {
            log.warn("createCopy() invoked in invalid state [offer=" + (offer == null ? "null" :
                (offer.getId() + ", unchangeable=" + offer.isUnchangeable()
                    + ", businessCaseAvailable=" + (businessCaseView != null)
                    + ", businessCaseId=" + ((businessCaseView == null) ? "null" :
                        businessCaseView.getBusinessCase().getPrimaryKey().toString()))) + "]");
        }
    }

    // copy a calulation based offer 

    public boolean isCreateCopyByCalculationAvailable() {
        return (!copyMode
                && businessCaseView != null
                && businessCaseView.getRequest().getType().isRecordByCalculation()
                && !isUnreleasedOfferAvailable()
                && (getBean() == null || getRecord().isUnchangeable())
                && !isCopyUnavailableByContextSwitch());
    }

    public boolean isCopyMode() {
        return copyMode;
    }

    public void setCopyMode(boolean copyMode) {
        this.copyMode = copyMode;
    }

    protected boolean isCopyUnavailableByContextSwitch() {
        if (businessCaseView != null && businessCaseView.getRequest().getType().isRecordByCalculation()
                && getBean() != null && getCalculation() == null) {
            return true;
        }
        return false;
    }

    // end copy methods

    public boolean isStatusResetAvailable() {
        if (!isUnreleasedOfferAvailable()
                && getBean() != null) {
            Offer offer = (Offer) getRecord();
            if (!offer.getMailLogs().isEmpty()) {
                return false;
            }
            if (getList().size() > 1) {
                for (int i = 0, j = getList().size(); i < j; i++) {
                    Offer current = (Offer) getList().get(i);
                    if (!current.getId().equals(offer.getId())
                            && current.getCreated().after(offer.getCreated())) {
                        return false;
                    }
                }
            }
            if (getDomainUser().isPermissionAssigned("sales_offer_change_status")
                    || getDomainEmployee().getId().equals(offer.getCreatedBy())) {
                return true;
            }
        }
        return false;
    }

    public void resetStatus() {
        if (isStatusResetAvailable()) {
            Offer offer = (Offer) getRecord();
            SalesOfferManager manager = getSalesOfferManager();
            try {
                manager.resetStatus(offer, getDomainEmployee());
                setBean(manager.load(offer.getId()));
            } catch (Exception e) {
                log.error("Failed to reset status: " + e.getMessage(), e);
            }
        }
    }

    public Calculation getCalculation() {
        Offer offer = (Offer) getRecord();
        SalesOfferManager manager = getSalesOfferManager();
        Calculation calc = manager.getCalculation(offer);
        if (log.isDebugEnabled()) {
            log.debug("getCalculation() fetched calculation ["
                    + (calc == null ? "null" : calc.getId()) + "], offer ["
                    + (offer == null ? "null" : offer.getId()) + "]");
        }
        return calc;
    }

    public boolean isDiscountPercent() {
        SalesOffer offer = (SalesOffer) getRecord();
        if (offer == null) {
            log.warn("isDiscountPercent() invoked when even no offer selected!");
            return false;
        }
        return CalculatorService.CALCULATOR_BY_ITEMS.equals(offer.getCalculatorName());
    }

    @Override
    protected void checkDiscount() throws ClientException {
        SalesOffer offer = (SalesOffer) getRecord();
        BusinessCase request = (businessCaseView == null ? null
                : businessCaseView.getBusinessCase());
        if (request == null) {
            log.warn("checkDiscount() ignoring invocation outside business case context");
            return;
        }
        if (request.getType().isWholeSale()) {
            setRequiredDiscount(0d);
        } else {
            SalesRevenueManager salesRevenueManager = getSalesRevenueManager();
            try {
                salesRevenueManager.checkDiscount(getDomainUser(), offer, request);
            } catch (Exception e) {
                SalesRevenue revenue = salesRevenueManager.getRevenue(offer.getId());
                if (revenue.isProfitLowerMinimum()) {
                    setRequiredDiscount(revenue.getMissingMarginPercent());
                    setConfirmDiscountMode(true);
                } else {
                    setRequiredDiscount(0d);
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("setDiscountByItems() done [sales=" + request.getPrimaryKey()
                        + ", order=" + offer.getId()
                        + ", requiredDiscount=" + getRequiredDiscount()
                        + "]");
            }
        }
    }

    @Override
    public boolean isProvidingStatusHistory() {
        return true;
    }

    @Override
    public void setSelection(Long id) {
        super.setSelection(id);
        if (getBean() == null && id != null) {
            Record offer = getRecordManager().find(id);
            if (offer != null) {
                setBean(offer);
            }
        }
    }

    @Override
    public RecordManager getRecordManager() {
        return getSalesOfferManager();
    }

    protected SalesOfferManager getSalesOfferManager() {
        return (SalesOfferManager) getService(SalesOfferManager.class.getName());
    }

    protected SalesRevenueManager getSalesRevenueManager() {
        return (SalesRevenueManager) getService(SalesRevenueManager.class.getName());
    }

    private void refreshList() {
        if (businessCaseView != null) {
            List offers = getRecordManager().getByReference(
                    businessCaseView.getRequest().getRequestId());
            Offer offer = null;
            if (offers.size() == 1) {
                offer = (Offer) offers.get(0);
            } else {
                Offer so = (Offer) getBean();
                if (so != null) {
                    for (int i = 0, j = offers.size(); i < j; i++) {
                        Offer current = (Offer) offers.get(i);
                        if (current.getId().equals(so.getId())) {
                            offer = current;
                            break;
                        }
                    }
                }
            }
            if (offer != null) {
                setRecord(getRecordManager().load(offer.getId()));
            }
            setList(offers);
        }
    }
}
