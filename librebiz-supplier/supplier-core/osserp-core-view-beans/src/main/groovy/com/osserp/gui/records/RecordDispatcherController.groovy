/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.records

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.web.RequestUtil
import com.osserp.groovy.web.AbstractController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Controller class RecordDispatcherController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(RecordDispatcherController.class.getName())

    @RequestMapping('/purchaseInvoiceDisplay')
    def purchaseInvoiceDisplay(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("purchaseInvoiceDisplay: invoked [user=${getUserId(request)}]")
        redirect(request, createRedirectLink(request, '/purchaseInvoice.do?method=display'))
    }

    @RequestMapping('/purchaseInvoiceRedisplay')
    def purchaseInvoiceRedisplay(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("purchaseInvoiceRedisplay: invoked [user=${getUserId(request)}]")
        redirect(request, '/purchaseInvoice.do?method=redisplay')
    }

    @RequestMapping('/salesInvoiceDisplay')
    def salesInvoiceDisplay(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("salesInvoiceDisplay: invoked [user=${getUserId(request)}]")
        redirect(request, createRedirectLink(request, '/salesInvoice.do?method=forward'))
    }

    @RequestMapping('/salesCreditNoteReload')
    def salesCreditNoteDisplay(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("salesCreditNoteReload: invoked [user=${getUserId(request)}]")
        redirect(request, createRedirectLink(request, '/salesCreditNote.do?method=reload'))
    }

    String createRedirectLink(HttpServletRequest request, String url) {
        String target = url
        Long id = RequestUtil.fetchId(request)
        if (id) {
            target = "${url}&id=${id}"
        }
        logger.debug("createRedirectLink: done [target=${target}]")
        return target
    }
}
