/**
 * osserp-core - osserp-forms-0.1.2.js - Common functions.
 * Copyright 2001-2021 The original author or authors.
 * Licensed under the Apache License, Version 2.0.
 * 
 * ojsForms - A collection of form related functions.
 */

var ojsForms = {};
var fleegix = {};
fleegix.form = {};


ojsForms.checkAll = function(inputs) {
	for (i = 0; i < inputs.length; i++) {
		inputs[i].checked = true ;
	}
};

ojsForms.uncheckAll = function(inputs) {
	for (i = 0; i < inputs.length; i++) {
		inputs[i].checked = false ;
	}
};

/*
 * @param formName
 */
function jumpToFormUrl(formName) {
	var form = document.getElementById(formName);
	var jumpUrl = form.jumpUrl.value;
	location.href=jumpUrl;
};
	
/*
 * @param targetElement
 * sets the focus on the first element of the first form
 */
ojsForms.focusFirstElement = function(targetElement) {
	if ($(targetElement).getElementsByTagName('form')[0]) {
		$(targetElement).getElementsByTagName('form')[0].focusFirstElement();
	}
};
	
/*
 * @param form
 * returns true if contact details are valid
 */
ojsForms.validateContactForm = function(form) {
	if (!ojsForms.validateContact(form)) {
		return false;
	}
	if (!ojsForms.validateCommunication(form)) {
		return false;
	}
	return true;
};

ojsForms.validateContactPersonForm = function(form) {
	if (!ojsForms.validatePerson(form)) {
		return false;
	}
	if (!ojsForms.validateCommunication(form)) {
		return false;
	}
	return true;
};

ojsForms.validateForm = function(form) {
	var elements = form.getElementsByClassName("mandatory");
	var value = null;
	for (var i=0; i<elements.length; i++) {
		value = elements[i].value;
		if(elements[i].style.visibility != "hidden" && (value == "" || value == null)) {
			elements[i].focus();
			alert(ojsI18n.mandatoryValuesMissing);
			return false;
		}
	}

	elements = form.getElementsByClassName("integer");
	for (i=0; i<elements.length; i++) {
		value = elements[i].value;
		if(value != null && value != "" && !ojsCommon.isInt(value)) {
			elements[i].focus();
			alert(value + " " + ojsI18n.isNotAnInteger);
			return false;
		}
	}

	elements = form.getElementsByClassName("floatingPoint");
	for (i=0; i<elements.length; i++) {
		value = elements[i].value;
		if(value != null && value != "" && !ojsCommon.isFloat(value)) {
			elements[i].focus();
			alert(value + " " + ojsI18n.isNotADecimalValue);
			return false;
		}
	}

	return true;
};
	
/*
 * @param form
 * returns true if name, street, zipcode & city of the given contact form are valid
 */
ojsForms.validateContact = function(form) {
	var inputs = new Array(form.lastName, form.street, form.zipcode, form.city);
	for (var i=0; i<inputs.length; i++) {
		if (inputs[i].value.length > 255) {
			alert(ojsI18n.max255chars);
			return false;
		}
		var badwords = new Array("href=", "src=", "onclick=", "onmousedown=", "onmouseup=");
		for (var j=0; j<badwords.length; j++) {
			if (inputs[i].value.indexOf(badwords[j]) >= 0) {
				alert(ojsI18n.noCodeInjection);
				return false;
			}
		}
	}

	try {
		if (form.company == null || form.company.value.blank() || form.company.value.length < 1 || form.company.value == null) {
			if (form.salutation.value == '' || form.salutation.value == 0) {
				alert(ojsI18n.promptSalutation);
				return false;
			}
		}
	} catch (notExistingFormValue) {
		ojsCommon.consoleDebug(notExistingFormValue);
	}

	try {
		if (form.firstName.value.length > 0 && (form.firstName.value.blank() || ojsCommon.isLowerCase(form.firstName.value[0]))) {
			alert(ojsI18n.invalidFirstName);
			return false;
		}
	} catch (notExistingFormValue) {
		ojsCommon.consoleDebug(notExistingFormValue);
	}

	try {
		if (form.company == null || form.company.value.blank() || form.company.value.length < 1 || form.company.value == null) {
			if (form.lastName.value.blank() || form.lastName.value.length < 1 || form.lastName.value == null)	{
				alert(ojsI18n.missingLastName);
				return false;
			} else if (form.privateContact.value == "true" && ojsCommon.isLowerCase(form.lastName.value[0])) {
				alert(ojsI18n.invalidLastName);
				return false;
			}
		}
	} catch (notExistingFormValue) {
		ojsCommon.consoleDebug(notExistingFormValue);
	}

	try {
		if ( form.street.value.blank() ||	form.street.value.length < 1 || form.street.value == null )	{
			alert(ojsI18n.missingStreet);
			return false;
		} else if (ojsCommon.isLowerCase(form.street.value[0])) {
			alert(ojsI18n.invalidStreet);
			return false;
		}
	} catch (notExistingFormValue) {
		ojsCommon.consoleDebug(notExistingFormValue);
	}

	try {
		if ( form.zipcode.value.blank() || form.zipcode.value.length < 1 || form.zipcode.value == null )	{
			alert(ojsI18n.missingZipcode);
			return false;
		}
	} catch (notExistingFormValue) {
		ojsCommon.consoleDebug(notExistingFormValue);
	}

	try {
		if ( form.city.value.blank() ||	form.city.value.length < 1 || form.city.value.length == null )	{
			alert(ojsI18n.missingCity);
			return false;
		} else if (ojsCommon.isLowerCase(form.city.value[0])) {
			alert(ojsI18n.invalidCity);
			return false;
    }
	} catch (notExistingFormValue) {
		ojsCommon.consoleDebug(notExistingFormValue);
	}

	try {
		if ( !ojsForms.validDate(form.birthDate.value, false) )	{
			alert(ojsI18n.invalidBirthDate);
			return false;
		}
	} catch (notExistingFormValue) {
		ojsCommon.consoleDebug(notExistingFormValue);
	}

	return true;
};
	
/*
 * @param form
 * returns true if salutation & lastname of the given contact form are valid
 */
ojsForms.validatePerson = function(form) {
	try {
		if (form.salutation.value == '' || form.salutation.value == 0) {
			alert(ojsI18n.promptSalutation);
			return false;
		}
	} catch (notExistingFormValue) {
		ojsCommon.consoleDebug(notExistingFormValue);
	}
	try {
		if (form.lastName.value == '') {
			alert(ojsI18n.promptLastName);
			return false;
		}
	} catch (notExistingFormValue) {
		ojsCommon.consoleDebug(notExistingFormValue);
	}
	return true;
};
	
/*
 * @param form
 * returns true if phone, fax, mobile & email of the given contact form are valid
 */
ojsForms.validateCommunication = function(form) {
	if (form.pprefix.value != '' || form.pnumber.value != '') {
		if (!validatePhone(form.pprefix.value, form.pnumber.value, ojsI18n.phone)) {
			return false;
		}
	}
	if (form.fprefix.value != '' || form.fnumber.value != '') {
		if (!validatePhone(form.fprefix.value, form.fnumber.value, ojsI18n.fax)) {
			return false;
		}
	}
	if (form.mprefix.value != '' || form.mnumber.value != '') {
		if (!validatePhone(form.mprefix.value, form.mnumber.value, ojsI18n.mobile)) {
			return false;
		}
	}
	return true;
};

/*
 * @param date
 * @param required
 * returns true if given date is valid
 */
ojsForms.validDate = function(date, required) {
	var centuryPrefiy = new String(new Date().getFullYear()).substr(0,2);
	var currentYear = new Date().getFullYear();

	if (!required && date.length == 0) {
		return true;
	}

	if (date.indexOf("/") >= 0) {
		date = date.replace("/", ".");
	}
	if (date.indexOf(",") >= 0) {
		date = date.replace(",", ".");
	}
	if (date.indexOf("-") >= 0) {
		date = date.replace("-", ".");
	}
	if (date.length == 5) {
		date += "." + currentYear;
	}
	if (date.length == 8) {
		date = date.substr(0,6) + centuryPrefiy + date.substr(6,2);
	}

	var d = new Number(date.substr(0,2));
	var m = new Number(date.substr(3,2));
	var y = new Number(date.substr(6,4));

	if (
		!ojsCommon.isInt(d)  || !ojsCommon.isInt(m) || !ojsCommon.isInt(y) ||
		date.substr(2,1) != "." || date.substr(5,1) != "." ||
		d < 1 || d > 31 || m < 1 || m > 12 || y < (currentYear-200) || y > (currentYear+200) ||
		!ojsCommon.consistsOf(date, "0123456789.") ||
		date.length != 10
		) {
		return false;
	}

	return true;
};

function validatePhoneEditor() {
	var form = document.getElementById('contactForm');
	if (form.type.value == '' || form.type.value == 0) {
		alert(ojsI18n.promptType);
		return false;
	}
	if (form.prefix.value == '') {
		alert(ojsI18n.promptPhonePrefix);
		return false;
	}
	if (form.number.value == '') {
		alert(ojsI18n.promptPhoneNumber);
		return false;
	}
	if (!validatePhoneNumber(form.prefix.value, '')) {
		return false;
	}
	if (!validatePhoneNumber(form.number.value, '')) {
		return false;
	}
	return true;
}
	
/*
 * @param prefix
 * @param number
 * @param message
 * returns true if given phone is valid
 */
function validatePhone(prefix, number, message) {
	if (prefix == '') {
		if (message != '') {
			alert(message + ': ' + ojsI18n.error_prefix_missing);
		} else {
			alert(ojsI18n.error_prefix_missing);
		}
		return false;
			
	} else if (!validatePhoneNumber(prefix, message)) {
		return false;
	}
	if (number == '') {
		if (message != '') {
			alert(message + ': ' + ojsI18n.error_number_missing);
		} else {
			alert(ojsI18n.error_number_missing);
		}
		return false;
			
	} else if (!validatePhoneNumber(number, message)) {
		return false;
	}
	return true;
}

/*
 * @param number
 * @param message
 * returns true if given phone number is valid
 */
function validatePhoneNumber(number, message) {
	var strLen = number.length;
	for (var i = 0; i < strLen ; i++) {
		if (isNaN(parseInt(number.charAt(i)))) {
			if (number.charAt(i) != '-' && number.charAt(i) != ' ') {
				if (message != '') {
					alert(message + '; ' + ojsI18n.invalidToken + '[' + number.charAt(i) 
						+ ']' + ojsI18n.atPosition + '=  ' + (i+1) + ' [' + number + ']');
				} else {
					alert(ojsI18n.invalidToken + '[' + number.charAt(i) 
						+ ']' + ojsI18n.atPosition + '=  ' + (i+1) + ' [' + number + ']');
				}
				return false;
			}
		}
	}
	return true;
}
	
/*
 * @param element
 */
function checkSalutationSelection(element) {
	var value = element.value;
	var form = document.getElementById('contactForm');
	var current = form.currentSalutation.value;
	var url = ojsCommon.contextPath + "/contacts.do?method=enableTypeChange";
	if (current != 3 && value == 3) {
		confirmLink(ojsI18n.promptChangePrivateToCompany, url);
	}
}

ojsForms.formToHash = function(form, params) {
	return $H(fleegix.form.toObject(form, params));
};

/* --- Third Party Components --- */

/**
 * Converts the values in an HTML form into a JS object
 * Elements with multiple values like sets of radio buttons
 * become arrays
 * @param f -- HTML form element to convert into a JS object
 * @param o -- JS Object of options:
 *    pedantic: (Boolean) include the values of elements like
 *      button or image
 *    hierarchical: (Boolean) if the form is using Rails-/PHP-style
 *      name="foo[bar]" inputs, setting this option to
 *      true will create a hierarchy of objects in the
 *      resulting JS object, where some of the properties
 *      of the objects are sub-objects with values pulled
 *      from the form. Note: this only supports one level
 *      of nestedness
 * hierarchical option code by Kevin Faulhaber, kjf@kjfx.net
 * @returns JavaScript object representation of the contents
 * of the form.
 *
 * (Taken from the fleegix.js framework. LICENSE: ../licenses/apache-license-2.0.txt)
 */
fleegix.form.toObject= function (f, o) {
  var opts = o || {};
  var h = {};
  function expandToArr(orig, val) {
    if (orig) {
      var r = null;
      if (typeof orig == 'string') {
        r = [];
        r.push(orig);
      }
      else { r = orig; }
      r.push(val);
      return r;
    }
    else { return val; }
  }

  for (var i = 0; i < f.elements.length; i++) {
    var elem = f.elements[i];
    // Elements should have a name
    if (elem.name) {
      var st = elem.name.indexOf('[');
      var sp = elem.name.indexOf(']');
      var sb = '';
      var en = '';
      var c;
      var n;
      // Using Rails-/PHP-style name="foo[bar]"
      // means you can go hierarchical if you want
      if (opts.hierarchical && (st > 0) && (sp > 2)) {
          sb = elem.name.substring(0, st);
          en = elem.name.substring(st + 1, sp);
          if (typeof h[sb] == 'undefined') { h[sb] = {}; }
          c = h[sb];
          n = en;
      }
      else {
          c = h;
          n = elem.name;
      }
      switch (elem.type) {
        // Text fields, hidden form elements, etc.
        case 'text':
        case 'hidden':
        case 'password':
        case 'textarea':
        case 'select-one':
          c[n] = elem.value;
          break;
        // Multi-option select
        case 'select-multiple':
          for(var j = 0; j < elem.options.length; j++) {
            var e = elem.options[j];
            if(e.selected) {
              c[n] = expandToArr(c[n], e.value);
            }
          }
          break;
        // Radio buttons
        case 'radio':
          if (elem.checked) {
            c[n] = elem.value;
          }
          break;
        // Checkboxes
        case 'checkbox':
          if (elem.checked) {
            c[n] = expandToArr(c[n], elem.value);
          }
          break;
        // Pedantic
        case 'submit':
        case 'reset':
        case 'file':
        case 'image':
        case 'button':
          if (opts.pedantic) { c[n] = elem.value; }
          break;
      }
    }
  }
  return h;
};
