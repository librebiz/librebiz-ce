<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.timeRecordingQueryView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="queryView" value="${sessionScope.timeRecordingQueryView}" />
<c:set var="email" value="${queryView.mail}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="timeRecordingQuery" />
    </tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/timeRecordingQuery.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
            <c:if test="${!queryView.editMode}">
                <li><a href="<c:url value="/timeRecordingQuery.do?method=enableEdit"/>" title="<fmt:message key="timeRecordingQuery"/>"><o:img name="newdataIcon" /></a></li>
            </c:if>
            <li><v:link url="/admin" title="backToMenu"><o:img name="homeIcon" /></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="timeRecordingQueryContent">
            <div class="row">
                <c:choose>
                    <c:when test="${queryView.editMode}">
                        <o:form id="timeRecordingForm" name="timeRecordingForm" url="/timeRecordingQuery.do">
                            <input type="hidden" name="method" value="save" />

                            <div class="col-md-6 panel-area panel-area-default">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4><o:out value="${type.id}"/><span> - </span><o:out value="${type.name}"/></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="form-body">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <fmt:message key="email" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="email" value="<o:out value="${email}"/>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <fmt:message key="date"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="date" id="dateInput" />
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <o:datepicker input="dateInput"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <fmt:message key="timeRecordingQueryType" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <select class="form-control" name="typeId">
                                                        <option value="0"><fmt:message key="timeRecordingQueryTimebank" /></option>
                                                        <option value="1"><fmt:message key="timeRecordingQueryBookings" /></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row next">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/timeRecordingConfig.do?method=forward"/>');" value="<fmt:message key="break"/>" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <o:submit styleClass="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </o:form>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-6 panel-area panel-area-default">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4><fmt:message key="timeRecordingQueryRun" /></h4>
                                </div>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
