/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 17, 2013 at 12:22:31 AM 
 * 
 */
package com.osserp.gui.admin.employees

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Permission
import com.osserp.common.UserManager
import com.osserp.common.util.CollectionUtil
import com.osserp.core.Comparators
import com.osserp.core.employees.EmployeeGroup
import com.osserp.core.employees.EmployeeGroupManager
import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class EmployeeGroupPermissionView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(EmployeeGroupPermissionView.class.getName())

    private Map<String, Permission> existingPermissions = new HashMap<String, Permission>()
    boolean hiddenEnabled = false

    List<EmployeeGroup> groupsByPermission = []
    String groupsByPermissionName

    EmployeeGroupPermissionView() {
        headerName = 'employeeGroupPermissions'
        dependencies = ['employeeGroupConfigView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            List<Permission> all = userManager.permissions
            existingPermissions.clear()
            all.each {
                existingPermissions.put(it.permission, it)
            }

            Long groupId = form.getLong('id')
            if (groupId) {
                bean = employeeGroupManager.getGroup(groupId)
                logger.debug("initRequest: fetched group by id [group=${bean?.id}]")
            } else {
                bean = env.employeeGroupConfigView?.bean
                logger.debug("initRequest: fetched group by employeeGroupConfigView [group=${bean?.id}]")
            }
        }
    }

    List<Permission> getAvailablePermissions() {
        List<Permission> result = []
        Set<String> all = existingPermissions.keySet()
        EmployeeGroup group = bean
        all.each {
            if (!group.isPermissionGrant(it)) {
                Permission perm = existingPermissions.get(it)
                if (perm && (perm.isDisplayable() || hiddenEnabled)) {
                    result << existingPermissions.get(it)
                }
            }
        }
        CollectionUtil.sort(result, Comparators.createPermissionComparator())
        return result
    }

    List<Permission> getCurrentPermissions() {
        List<Permission> result = []
        EmployeeGroup group = bean
        group?.permissions.each {
            result << existingPermissions.get(it.name)
        }
        CollectionUtil.sort(result, Comparators.createPermissionComparator())
        return result
    }

    void addPermission() {
        String permission = form.getString('permission')
        EmployeeGroup group = bean
        group.addPermission(domainEmployee, permission)
        employeeGroupManager.save(group)
        env.employeeGroupConfigView?.reload()
    }

    void removePermission() {
        String permission = form.getString('permission')
        EmployeeGroup group = bean
        group.removePermission(permission)
        employeeGroupManager.save(group)
        env.employeeGroupConfigView?.reload()
    }

    void loadGroupsByPermission() {
        String permission = form.getString('permission')
        groupsByPermission = employeeGroupManager.findGroupDisplayByPermission(permission)
        groupsByPermissionName = fetchPermissionDisplayName(permission)
    }

    private String fetchPermissionDisplayName(String permission) {
        Permission p = existingPermissions[permission]
        return p?.description
    }

    private EmployeeGroupManager getEmployeeGroupManager() {
        getService(EmployeeGroupManager.class.getName())
    }

    private UserManager getUserManager() {
        getService(UserManager.class.getName())
    }
}
