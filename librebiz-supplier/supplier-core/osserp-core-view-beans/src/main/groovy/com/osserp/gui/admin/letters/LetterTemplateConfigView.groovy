/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2008 12:35:30 AM 
 * 
 */
package com.osserp.gui.admin.letters

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.dms.DocumentData
import com.osserp.common.util.CollectionUtil

import com.osserp.core.dms.LetterContentManager
import com.osserp.core.dms.LetterTemplate
import com.osserp.core.dms.LetterType
import com.osserp.core.employees.Employee

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.letters.AbstractLetterView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class LetterTemplateConfigView extends AbstractLetterView {
    private static Logger logger = LoggerFactory.getLogger(LetterTemplateConfigView.class.getName())

    List<LetterType> availableTypes = []

    LetterTemplateConfigView() {
        super()
        providesCreateMode()
        providesEditMode()
        providesCustomNavigation()
        headerName  = 'letterTemplatesLabel'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            Long id = form.getLong('id')
            if (id) {
                selectedType = letterTemplateManager.getType(id)
                list = letterTemplateManager.findTemplates(selectedType)
            } else {
                id = form.getLong('templateId')
                if (id) {
                    LetterTemplate tpl = letterTemplateManager.find(id)
                    if (tpl) {
                        selectedType = tpl.type
                        bean = tpl
                        enableExternalInvocationMode()
                    }
                }
                if (!externalInvocationMode && !availableTypes) { 
                    availableTypes = letterTemplateManager.getTypes()
                    env.supportingTypeSelection = true
                }
            }
            logger.debug('initRequest: forwardRequest exec done')
        }
    }

    void selectType() {
        Long id = form.getLong('id')
        if (id) {
            selectedType = CollectionUtil.getById(availableTypes, id)
        } else {
            selectedType = null
        }
        reload()
    }

    DocumentData getPdf() throws ClientException {
        LetterTemplate tpl
        Long id = form.getLong('id')
        if (id) {
            tpl = letterTemplateManager.find(id)
        } else {
            tpl = bean
        }
        Employee current = getDomainEmployee()
        String header = (current.getSalutation() != null ? current.getSalutation().getName() : '')
        tpl.addAddress(current.getAddress(), header, current.getDisplayName())
        DocumentData doc = letterTemplateManager.getPdf(domainUser, tpl)
        if (!doc) {
            throw new ClientException(ErrorCode.PREVIEW_NOT_AVAILABLE)
        }
        return doc
    }

    @Override
    void save() {

        if (createMode) {
            LetterTemplate existing
            Long templateId = form.getLong('templateId')
            if (templateId) {
                existing = CollectionUtil.getById(list, templateId)
            }
            bean = letterTemplateManager.create(
                    domainUser,
                    selectedType,
                    form.getString('name'),
                    form.getLong('branch'),
                    existing)
            disableCreateMode()
            enableEditMode()
        } else if (editMode) {

            letterTemplateManager.update(
                    domainUser,
                    bean,
                    form.getString('templateName'),
                    form.getString('subject'),
                    form.getString('salutation'),
                    form.getString('greetings'),
                    form.getString('language'),
                    form.getLong('signatureLeft'),
                    form.getLong('signatureRight'),
                    form.getBoolean('ignoreSalutation'),
                    form.getBoolean('ignoreGreetings'),
                    form.getBoolean('ignoreHeader'),
                    form.getBoolean('ignoreSubject'),
                    bean.type.embedded,
                    form.getBoolean('embeddedAbove'),
                    form.getString('embeddedSpaceAfter'),
                    getContentKeepTogetherValue(),
                    form.getBoolean('defaultTemplate'))
            disableEditMode()
        } else if (selectedParagraph) {
            updateParagraph()
        }
        reload()
    }

    void delete() {
        if (bean instanceof LetterTemplate) {
            letterTemplateManager.delete(domainUser, bean)
            bean = null
            reload()
        }
    }

    @Override
    void createCustomNavigation() {
        if (supportingTypeSelection) {

            if (!selectedType) {
                nav.defaultNavigation = [exitLink, homeLink]
            } else {
                nav.listNavigation = [deselectTypeLink, createLink, homeLink]
                nav.defaultNavigation = [deselectTypeLink, homeLink]
            }
        }
        nav.editNavigation = [disableEditLink, printLink, homeLink]

        if (selectedParagraph) {
            nav.beanListNavigation = [deselectParagraphLink, printLink, homeLink]
        } else if (!isPermissionGrant(letterTemplateManager.deletePermissions)) {
            nav.beanListNavigation = [selectExitLink, printLink, createParagraphLink, editLink, homeLink]
        } else {
            nav.beanListNavigation = [selectExitLink, printLink, createParagraphLink, editLink, deleteLink, homeLink]
        }
    }

    /**
     * Provides the deselect type link
     * @return deselectTypeLink
     */
    protected MenuItem getDeselectTypeLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/selectType", icon: 'backIcon', title: 'backToLast')
    }

    /**
     * Provides the deselect type link
     * @return deselectTypeLink
     */
    protected MenuItem getDeleteLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/delete", icon: 'deleteIcon', title: 'templateDelete', confirmLink: true)
    }

    @Override
    void reload() {
        if (selectedType) {
            list = letterTemplateManager.findTemplates(selectedType)
            if (bean) {
                bean = CollectionUtil.getById(list, bean.id)
            }
        } else {
            list = []
        }
    }

    @Override
    protected String getPrintLetterTitle() {
        'printTestLetterTitle'
    }

    @Override
    protected LetterContentManager getLetterContentManager() {
        getLetterTemplateManager()
    }
}
