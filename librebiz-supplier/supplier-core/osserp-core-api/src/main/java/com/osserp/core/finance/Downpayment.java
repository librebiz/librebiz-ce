/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 9, 2006 10:34:45 PM 
 * 
 */
package com.osserp.core.finance;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Downpayment extends CancellableRecord, Invoice {

    /**
     * @return date canceled
     */
    Date getDateCanceled();

    /**
     * @return id canceled
     */
    Long getIdCanceled();

    /**
     * Updates the prorate amount of this downpayment
     * @param amount
     */
    void updateProrate(BigDecimal amount);

    /**
     * Indicates that the amount is fixed and not calculated by prorated percentage
     * @return true if so
     */
    boolean isFixedAmount();

    /**
     * Indicates that this records amounts are none prorated amounts as downpayments normally are. Amounts are set by individual record positions as for normal
     * invoices.
     * @return true if so
     */
    boolean isInvoiceBehaviour();

    /**
     * Provides the prorated percent for this invoice
     * @return proratePercent
     */
    BigDecimal getProratePercent();

    /**
     * Provides the prorate amount for this invoice
     * @return prorateAmount
     */
    BigDecimal getAmount();

    /**
     * Provides the gross amount for this invoice
     * @return grossAmount
     */
    BigDecimal getGrossAmount();

    /**
     * Provides the base tax amount for this invoice
     * @return taxAmount
     */
    BigDecimal getTaxAmount();

    /**
     * Provides the reduced tax amount for this invoice
     * @return reducedTaxAmount
     */
    BigDecimal getReducedTaxAmount();

    /**
     * Provides the base tax
     * @return tax
     */
    Double getTax();

    /**
     * Provides the reduced tax
     * @return reducedTax
     */
    Double getReducedTax();
}
