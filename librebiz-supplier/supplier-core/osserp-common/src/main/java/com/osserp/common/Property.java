/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Mar-2011 11:46:33 
 * 
 */
package com.osserp.common;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Provides a property
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Property extends Cloneable, Entity, Parameter {

    /**
     * Provides the data type (class name) for property value
     * @return type default is java.lang.String
     */
    String getType();

    /**
     * Overrides data type
     * @param type
     */
    void setType(String type);

    /**
     * Indicates if property type definition is boolean
     * @return true if property type set to java.lang.Boolean
     */
    boolean isBooleanType();
    
    /**
     * Indicates if property type definition is date
     * @return true if property type set to java.util.Date
     */
    boolean isDateType();

    /**
     * Indicates if a property of type boolean is enabled
     * @return true if property value string is 'true' or 'yes', false otherwise
     */
    boolean isEnabled();

    /**
     * Sets the string value of property to true
     */
    void enable();

    /**
     * Sets the string value of property to false
     */
    void disable();

    /**
     * Provides current value as date if format is convertible
     * @return date or null if not
     */
    Date getAsDate();

    /**
     * Provides current value as decimal if format is convertible
     * @return decimal or null if not
     */
    BigDecimal getAsDecimal();

    /**
     * Provides current value as double if format is convertible
     * @return double or null if not
     */
    Double getAsDouble();

    /**
     * Provides current value as integer if format is convertible
     * @return integer or null if not
     */
    Integer getAsInteger();

    /**
     * Provides current value as long if format is convertible
     * @return long or null if not
     */
    Long getAsLong();

    /**
     * Provides current value as string if format is convertible
     * @return value as string, empty string if value is null
     */
    String getAsString();

    /**
     * Provides an optional order id
     * @return the orderId
     */
    int getOrderId();

    /**
     * Sets a new order id
     * @param orderId the orderId to set
     */
    void setOrderId(int orderId);

    /**
     * Converts and stores a boolean value
     * @param booleanValue
     */
    void setBoolean(boolean booleanValue);

    /**
     * Provides an optional name of the relating context 
     * @return context name
     */
    String getContextName();

    /**
     * Indicates if the property is visible (e.g. changeable at runtime)
     * @return displayable
     */
    boolean isDisplayable();

    /**
     * Indicates if property must not be used / managed outside setup context 
     * @return true if setup-only property
     */
    boolean isSetupOnly();

    /**
     * Indicates if property value is unchangeable via gui.
     * @return true if unchangeable
     */
    boolean isUnchangeable();

    /**
     * Clones property object
     * @return clone
     */
    Object clone();
}
