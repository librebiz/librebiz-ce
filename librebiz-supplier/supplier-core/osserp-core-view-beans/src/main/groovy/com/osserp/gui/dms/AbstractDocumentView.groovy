/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.dms

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.FileObject
import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsReference
import com.osserp.common.dms.DocumentData
import com.osserp.common.dms.DocumentType

import com.osserp.core.dms.CoreDocumentType

import com.osserp.groovy.web.MenuItem
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class AbstractDocumentView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractDocumentView.class.getName())

    protected Long lastChangedId
    List<DmsDocument> documentsImported = []
    boolean assignImportsSupport = true
    boolean documentsImportedDisplay = false
    boolean galleryMode = false
    boolean referenceSupport = true
    boolean documentTypePicture = false
    boolean multipleFileUploadSupport = false
    protected DocumentType _referencedDocumentType

    String thumbnailHeightList = '128px'
    String thumbnailHeightDisplay = '720px'
    String thumbnailHeightGallery = '240px'
    String thumbnailCols = 'col-sm-6 col-md-3'
    DmsDocument selectedImage

    public AbstractDocumentView() {
        super()
        providesCustomNavigation()
    }

    protected abstract DmsReference getDmsReference()

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            thumbnailCols = getSystemProperty('dmsGalleryThumbnailCols', thumbnailCols)
            thumbnailHeightGallery = getSystemProperty('dmsGalleryThumbnailHeight', thumbnailHeightGallery)
            thumbnailHeightDisplay = getSystemProperty('dmsGalleryImageHeight', thumbnailHeightDisplay)
            thumbnailHeightList = getSystemProperty('dmsListThumbnailHeight', thumbnailHeightList)
        }
    }

    void reload() {
        list = dmsManager.findByReference(dmsReference)
        loadImports()
        if (forwardRequest && list && supportingThumbnails && thumbnailAvailable) {
            galleryMode = true
        }
    }

    String getDocumentNotePlaceholder() {
        documentType?.noteRequired ? 'descriptionOfContentLabel'
                : 'optionalDescriptionLabel'
    }

    String getDocumentTypeName() {
        documentTypePicture ? 'picture' : 'document'
    }

    @Override
    protected void createCustomNavigation() {
        if (supportingThumbnails && thumbnailAvailable) {
            if (galleryMode) {
                nav.listNavigation = [ exitLink, galleryEnabledLink, homeLink ]
            } else {
                nav.listNavigation = [ exitLink, galleryDisabledLink, homeLink ]
            }
        }
        nav.defaultNavigation = nav.listNavigation
    }

    boolean isValidityPeriodSupported() {
        documentType?.validityPeriodSupported
    }

    @Override
    void save() {
        String note = form.getString('note')
        Date source = form.getDate('sourceDate')
        Date validFrom = form.getDate('validFrom')
        Date validTil = form.getDate('validTil')
        Long categoryId = fetchCategory()
        List<FileObject> uploads = form.getUploads()
        logger.debug("save: invoked [uploadCount=${uploads.size()}, note=$note, categoryId=${categoryId}]")
        if (!uploads) {
            throw new ClientException(ErrorCode.FILE_MISSING)
        }
        if (!multipleFileUploadSupport || uploads.size() == 1) {
            DmsDocument doc = create(
                uploads.get(0),
                note,
                source,
                categoryId,
                validFrom,
                validTil)
            lastChangedId = doc?.id
        } else {
            uploads.each { FileObject file ->
                try {
                    DmsDocument doc = create(
                        file,
                        null,
                        source,
                        categoryId,
                        validFrom,
                        validTil)
                    lastChangedId = doc?.id
                } catch (ClientException c) {
                    reload()
                    throw c
                }
            }
        }
        disableCreateMode()
        reload()
    }

    protected DmsDocument create(FileObject file, String note, Date source, Long categoryId, Date validFrom, Date validTil) {
        DmsDocument doc = dmsManager.createDocument(
            domainUser,
            dmsReference,
            file,
            note,
            source,
            categoryId,
            validFrom,
            validTil,
            null)
        return doc
    }

    void updateMetadata() {
        String note = form.getString('note')
        Date validFrom = form.getDate('validFrom')
        Date validTil = form.getDate('validTil')
        DmsDocument doc = dmsManager.updateMetadata(
                domainUser,
                bean,
                note,
                validFrom,
                validTil)
        lastChangedId = bean?.id
        select()
        reload()
    }
    
    @Override
    void select() {
        super.select()
        if (bean) {
            enableEditMode()
        } else {
            disableEditMode()
        }
    }
    
    @Override
    void disableEditMode() {
        super.disableEditMode()
        bean = null
        reload()
    }

    void delete() {
        DmsDocument doc = fetchDocument(getLong('id'))
        dmsManager.deleteDocument(doc)
        lastChangedId = doc?.id
        reload()
    }

    void setReference() {
        def id = form.getLong('id')
        dmsManager.setAsReference(id)
        lastChangedId = id
        reload()
    }

    protected Long fetchCategory() {
        Long cat = form.getLong('categoryId')
        if (!cat) {
            cat = categoryDefault
        }
        return cat
    }

    boolean isSupportingThumbnails() {
        return documentType?.createThumbmail
    }

    boolean isThumbnailAvailable() {
        !(list.findAll { it.thumbnailAvailable }).empty
    }

    synchronized DocumentData getThumbnail(Long documentId, Long typeId) {
        DmsDocument doc = list.find { it.id == documentId && it.type.id == typeId }
        return !doc ? null : dmsManager.getThumbnailData(doc)
    }

    void toggleGalleryMode() {
        galleryMode = !galleryMode
    }

    void selectImage() {
        def id = form.getLong('id')
        def type = form.getLong('type')
        if (!id || !type) {
            selectedImage = null
        } else {
            selectedImage = list.find { it.id == id && it.type.id == type }
        }
    }

    protected MenuItem getGalleryEnabledLink() {
        navigationLink("/${context.name}/toggleGalleryMode", 'showDetailsIcon', 'showDetails')
    }

    protected MenuItem getGalleryDisabledLink() {
        navigationLink("/${context.name}/toggleGalleryMode", 'hideDetailsIcon', 'hideDetails')
    }

    boolean isSyncClientEnabled() {
        false
    }

    void syncWithClient() {
        // override if required
    }

    void loadImports() {
        documentsImported = dmsManager.findByReference(
                new DmsReference(CoreDocumentType.DOCUMENT_IMPORT, domainUser.id))
        logger.debug("loadImports: done [count=${documentsImported.size()}, assignImports=${assignImportsSupport}]")
    }

    Integer getDocumentsImportedCount() {
        !documentsImported ? 0 : documentsImported.size()
    }

    void toggleDocumentsImported() {
        documentsImportedDisplay = !documentsImportedDisplay
    }

    void assignImport() {
        Long id = form.getLong('id')
        if (id) {
            DmsDocument selectedDoc = documentsImported.find { it.id == id }
            if (selectedDoc) {
                dmsManager.assignDocument(domainUser, dmsReference, selectedDoc)
                reload()
            }
        }
    }

    final DocumentType getDocumentType() {
        Long type = dmsReference.getTypeId()
        if (type) {
            if (_referencedDocumentType == null || _referencedDocumentType != type) {
                _referencedDocumentType = dmsManager.getDocumentType(type)
            }
            return _referencedDocumentType
        }
        return null
    }

    /**
     * Provides the default category if referenced documentType supports this
     * @return default category id or null as default
     */
    protected Long getCategoryDefault() {
        dmsManager.getDefaultCategoryId(dmsReference?.typeId)
    }

    /**
     * Tries to fetch a document from persistent storage
     * @param id the primary key of the document to fetch
     * @return document or null if not exists or id not provided (e.g. null)
     */
    protected DmsDocument fetchDocument(Long id) {
        !id ? null : dmsManager.getDocument(id)
    }

    protected DmsManager getDmsManager() {
        getService(DmsManager.class.getName())
    }
}
