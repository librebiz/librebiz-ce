<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:set var="creditNoteUrl" value="${requestScope.creditNoteUrl}"/>
<c:if test="${!record.downpayment and !empty record.creditNotes}">
<div class="recordLabelValue">
	<fmt:message key="creditNotes"/>: 
	<c:forEach var="note" items="${record.creditNotes}">
	<br/>
        <c:choose>
            <c:when test="${!requestScope.referenceLinkDisabled}">
                <a href="<c:url value="${creditNoteUrl}?method=display&readonly=false&exit=${requestScope.creditNoteExitTarget}&id=${note.id}"/>">
                    <o:out value="${note.number}"/> <fmt:message key="ofDate"/> <fmt:formatDate value="${note.created}"/>
                </a>
            </c:when>
            <c:otherwise>
                <o:out value="${note.number}"/> <fmt:message key="ofDate"/> <fmt:formatDate value="${note.created}"/>
            </c:otherwise>
        </c:choose>
	</c:forEach>
</div>
</c:if>
