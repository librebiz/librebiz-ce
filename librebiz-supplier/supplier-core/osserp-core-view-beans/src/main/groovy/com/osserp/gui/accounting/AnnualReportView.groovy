/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 12, 2016
 * 
 */
package com.osserp.gui.accounting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DocumentData
import com.osserp.common.util.DateUtil

import com.osserp.core.BankAccount
import com.osserp.core.finance.AnnualReport
import com.osserp.core.finance.AnnualReportManager
import com.osserp.core.finance.TaxReport
import com.osserp.core.finance.TaxReportManager
import com.osserp.core.model.BankAccountVO

import com.osserp.groovy.web.MenuItem

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class AnnualReportView extends AbstractAccountingView {
    private static Logger logger = LoggerFactory.getLogger(AnnualReportView.class.getName())
    
    boolean bankAccountDocumentsEnabled
    boolean salesListMode
    boolean supplierListMode
    boolean cashInListMode
    boolean cashOutListMode
    
    
    AnnualReportView() {
        super()
        enableAutoreload()
        enableCompanyPreselection()
        enableBankAccountPreselection()
        providesCustomNavigation()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            
            if (!year) {
                // super did not find a year, use current to init
                year = DateUtil.getCurrentYear()
            }

            Long company = getLong('company')
            if (company) {
                companies = systemConfigManager.companies
                selectSelectedCompany(company)
            }
            reload()
        }
    }
    
    @Override
    void reload() {
        if (selectedCompany && year) {
            bean = annualReportManager.getReport(selectedCompany, year)
            if (salesListMode) {
                list = annualReport.salesRecords
            } else if (supplierListMode) {
                list = annualReport.supplierRecords
            }
        } else {
            logger.debug("reload: nothing to do [selectedCompany=${selectedCompany?.id}, year=${year}]")
        }
        bankAccounts = []
        if (selectedCompany?.bankAccounts) {
            List<BankAccount> baccounts = selectedCompany?.bankAccounts
            baccounts.each { BankAccount ba ->
                List<DmsDocument> docs = annualReportManager.getBankAccountDocuments(ba.id, year)
                bankAccounts << new BankAccountVO(ba, docs)
            }
        }
    }
    
    void enableCashInListMode() {
        select()
        list = annualReport.cashInList
        if (list) {
            cashInListMode = true
        }
    }

    void enableCashOutListMode() {
        select()
        list = annualReport.cashOutList
        if (list) {
            cashOutListMode = true
        }
    }

    void enableSalesListMode() {
        select()
        list = annualReport.salesRecords
        if (list) {
            salesListMode = true
        }
    }
    
    void enableSupplierListMode() {
        select()
        list = annualReport.supplierRecords
        if (list) {
            supplierListMode = true
        }
    }
    
    void selectBankAccountDocuments() {
        select()
        Long id = getLong('id')
        if (id && bankAccounts) { 
            selectedBankAccount = bankAccounts.find { it.id == id }
            if (selectedBankAccount.accountDocuments) {
                list = selectedBankAccount.accountDocuments
                bankAccountDocumentsEnabled = true
            } else {
                throw new ClientException('noDataAvailable')
            }
        }
    }
    
    void select() {
        resetModes()
        list = []
        selectedBankAccount = null
        nav.beanListNavigation = [selectExitLink, homeLink]
    }
    
    protected void resetModes() {
        bankAccountDocumentsEnabled = false
        salesListMode = false
        supplierListMode = false
        cashInListMode = false
        cashOutListMode = false
    }
    
    AnnualReport getAnnualReport() {
        bean
    }
    
    DocumentData getReportArchive() {
        annualReportManager.getReportArchive(annualReport)
    }
    
    String getSpreadsheet() {
        if (bean) {
            if (cashInListMode) {
                return annualReportManager.createCashInSheet(annualReport)
            }
            if (cashOutListMode) {
                return annualReportManager.createCashOutSheet(annualReport)
            }
            if (salesListMode) {
                return annualReportManager.createSalesSheet(annualReport)
            } 
            if (supplierListMode) {
                return annualReportManager.createSupplierSheet(annualReport)
            } 
        }
        return ''
    }
    
    @Override
    void createCustomNavigation() {
        if (salesListMode || supplierListMode || cashInListMode || cashOutListMode) {
            nav.beanListNavigation = [selectExitLink, xlsLink, homeLink]
        } else {
            nav.beanListNavigation = [selectExitLink, homeLink]
        }
    }
    
    protected MenuItem getXlsLink() {
        new MenuItem(
            link: "${context.fullPath}/accounting/annualReport/renderXls", 
            icon: 'tableIcon', title: 'printXls')
    }

    protected AnnualReportManager getAnnualReportManager() {
        getService(AnnualReportManager.class.getName())
    }

    protected DmsManager getDmsManager() {
        getService(DmsManager.class.getName())
    }
}
