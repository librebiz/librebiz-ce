/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 30, 2007 11:55:54 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.BackendException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.AbstractSpringDao;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.xml.JDOMParser;

import com.osserp.core.dao.ProductSummaries;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.finance.Stock;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSummary;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSummaryCacheImpl extends AbstractSpringDao implements ProductSummaryCache {
    private static Logger log = LoggerFactory.getLogger(ProductSummaryCacheImpl.class.getName());
    private Map<Long, Map<Long, ProductSummary>> caches = Collections.synchronizedMap(new ConcurrentHashMap<>());
    private ProductSummaries summaries = null;
    private SystemCompanies companies = null;

    /**
     * Creates a new summary cache
     * @param jdbcTemplate
     * @param companies
     * @param summaries
     */
    public ProductSummaryCacheImpl(JdbcTemplate jdbcTemplate, SystemCompanies companies, ProductSummaries summaries) {
        super(jdbcTemplate);
        this.summaries = summaries;
        this.companies = companies;
        List<Stock> stocks = companies.getStocks();
        for (Iterator<Stock> i = stocks.iterator(); i.hasNext();) {
            Map<Long, ProductSummary> _cache = Collections.synchronizedMap(new HashMap<Long, ProductSummary>());
            caches.put(i.next().getId(), _cache);
        }
    }

    public void clear() {
        for (Iterator<Long> i = caches.keySet().iterator(); i.hasNext();) {
            Map<Long, ProductSummary> _cache = caches.get(i.next());
            _cache.clear();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.ProductSummaryCache#getSummary(java.lang.Long, java.lang.Long)
     */
    public ProductSummary getSummary(Long stockId, Long productId) {
        Map<Long, ProductSummary> _cache = caches.get(stockId);
        ProductSummary obj = _cache.get(productId);
        if (obj == null) {
            obj = summaries.load(stockId, productId);
            if (obj != null) {
                _cache.put(productId, obj);
            }
        }
        return obj;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.ProductSummaryCache#loadSummary(com.osserp.core.products.Product)
     */
    public void loadSummary(Product product) {
        for (Iterator<Long> i = caches.keySet().iterator(); i.hasNext();) {
            Long nextStock = i.next();
            ProductSummary summary = getSummary(nextStock, product.getProductId());
            product.addSummary(summary);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.common.service.Cacheable#refresh(java.lang.String)
     */
    public void refresh(String eventXml) {
        Set<Long> productIds = fetchProductIds(eventXml);
        if (productIds.isEmpty()) {
            refreshAll();
        } else {
            refresh(productIds);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.ProductSummaryCache#refreshAndLoad(com.osserp.core.products.Product)
     */
    public Product refreshAndLoad(Product product) {
        Long stockId = product.getSummary() == null ? getDefaultStock()
                : product.getSummary().getStockId();
        if (stockId != null) {
            refresh(stockId, product.getProductId());
            loadSummary(product);
            product.enableStock(stockId);
        }
        return product;
    }

    private void refreshAll() {
        if (log.isDebugEnabled()) {
            log.debug("refreshAll() invoked...");
        }
        long start = System.currentTimeMillis();
        int cnt = 0;
        Object[] stocks = caches.keySet().toArray();
        for (int i = 0; i < stocks.length; i++) {
            Long nextStock = (Long) stocks[i];
            Map<Long, ProductSummary> _cache = caches.get(nextStock);
            Object[] productIds = _cache.keySet().toArray();
            for (int j = 0; j < productIds.length; j++) {
                Long productId = (Long) productIds[j];
                ProductSummary summary = summaries.load(nextStock, productId);
                _cache.put(productId, summary);
                cnt++;
            }
        }
        start = System.currentTimeMillis() - start;
        if (log.isDebugEnabled()) {
            log.debug("refreshAll() done [count=" + cnt + ", time="
                    + start + "ms]");
        }
    }

    private void refresh(Set<Long> productIds) {
        if (log.isDebugEnabled()) {
            log.debug("refresh() invoked [count=" + productIds.size() + "]");
        }
        long start = System.currentTimeMillis();
        int cnt = 0;
        for (Iterator<Long> i = caches.keySet().iterator(); i.hasNext();) {
            Long nextStock = i.next();
            for (Iterator<Long> j = productIds.iterator(); j.hasNext();) {
                refresh(nextStock, j.next());
                cnt++;
            }
        }
        start = System.currentTimeMillis() - start;
        if (log.isDebugEnabled()) {
            log.debug("refresh() done [count=" + cnt + ", time=" + start + "ms]");
        }
    }

    private Set<Long> fetchProductIds(String eventXml) {
        Set<Long> list = new java.util.HashSet<Long>();
        try {
            Document doc = JDOMParser.getDocument(eventXml);
            Element root = doc.getRootElement();
            List<Element> initiators = root.getChildren("initiator");
            for (int i = 0, j = initiators.size(); i < j; i++) {
                Element nextInitiator = initiators.get(i);
                List<Element> items = nextInitiator.getChild("items").getChildren("item");
                for (int k = 0, l = items.size(); k < l; k++) {
                    Element nextItem = items.get(k);
                    Long productId = NumberUtil.createLong(nextItem.getChildText("productId"));
                    if (productId != null) {
                        list.add(productId);
                    }
                }
            }

        } catch (Throwable t) {
        }
        return list;
    }

    private void refresh(Long stockId, Long productId) {
        ProductSummary summary = summaries.load(stockId, productId);
        Map<Long, ProductSummary> _cache = caches.get(stockId);
        _cache.put(productId, summary);
    }

    private Long getDefaultStock() {
        Stock ds = companies.getDefaultStock();
        if (ds == null) {
            throw new BackendException(ErrorCode.STOCK_SELECTION_MISSING);
        }
        return ds.getId();
    }
}
