/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 11, 2011 12:06:00 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.Item;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.records.PurchaseInvoices;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.dao.records.SalesRevenueCostCollector;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Stock;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseOrder;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRevenueCostCollectorByPurchaseDao extends AbstractSalesRevenueCostCollector implements SalesRevenueCostCollector {
    private static Logger log = LoggerFactory.getLogger(SalesRevenueCostCollectorByPurchaseDao.class.getName());

    private PurchaseInvoices purchaseInvoices;
    private PurchaseOrders purchaseOrders;
    private RecordSearch recordSearch;
    private SystemCompanies systemCompanies;

    protected SalesRevenueCostCollectorByPurchaseDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            PurchaseInvoices purchaseInvoices,
            PurchaseOrders purchaseOrders,
            RecordSearch recordSearch,
            SystemCompanies systemCompanies) {
        super(jdbcTemplate, tables, sessionFactory);
        this.purchaseInvoices = purchaseInvoices;
        this.purchaseOrders = purchaseOrders;
        this.recordSearch = recordSearch;
        this.systemCompanies = systemCompanies;
    }

    protected Stock getDefaultStock() {
        return systemCompanies.getDefaultStock();
    }

    @Override
    public List<Item> getCosts(Record order) {
        List<Item> result = new ArrayList<>();
        Map<Long, PurchaseInvoice> addedInvoices = new HashMap<>();
        if (order.getBusinessCaseId() != null) {
            List<RecordDisplay> purchases = recordSearch.findPurchases(order.getBusinessCaseId());
            for (Iterator<RecordDisplay> i = purchases.iterator(); i.hasNext();) {
                RecordDisplay next = i.next();
                if (RecordType.PURCHASE_INVOICE.equals(next.getType())) {
                    PurchaseInvoice record = (PurchaseInvoice) purchaseInvoices.load(next.getId());
                    if (!addedInvoices.containsKey(record.getId())) {
                        addedInvoices.put(record.getId(), record);
                        if (record.getReference() != null) {
                        }
                        for (Iterator<Item> itemIterator = record.getItems().iterator(); itemIterator.hasNext();) {
                            Item item = (Item) itemIterator.next().clone();
                            if (item.getPrice() != null && (item.getPrice().doubleValue() > 0 || item.getPrice().doubleValue() < 0)) {
                                item.setPartnerPrice(item.getPrice());
                                result.add(item);
                                if (log.isDebugEnabled()) {
                                    log.debug("getCosts: Adding item [invoice=" + record.getId() +
                                            ", product=" + item.getProduct().getProductId() +
                                            ", quantity=" + item.getQuantity() +
                                            ", price=" + item.getPrice());
                                }
                            }
                        }
                    }
                }
            }
            for (Iterator<RecordDisplay> i = purchases.iterator(); i.hasNext();) {
                RecordDisplay next = i.next();
                if (RecordType.PURCHASE_ORDER.equals(next.getType())) {
                    PurchaseOrder record = (PurchaseOrder) purchaseOrders.load(next.getId());
                    for (Iterator<Item> itemIterator = record.getItems().iterator(); itemIterator.hasNext();) {
                        Item item = (Item) itemIterator.next().clone();
                        if (item.getPrice() != null && (item.getPrice().doubleValue() > 0 || item.getPrice().doubleValue() < 0)) {
                            item.setPartnerPrice(item.getPrice());
                            result.add(item);
                            if (log.isDebugEnabled()) {
                                log.debug("getCosts: Adding item [order=" + record.getId() +
                                        ", product=" + item.getProduct().getProductId() +
                                        ", quantity=" + item.getQuantity() +
                                        ", price=" + item.getPrice());
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<Item> getThirdpartyCosts(Record order) {
        return new ArrayList<>();
    }

    @Override
    public boolean isProvidingThirdpartyCosts() {
        return false;
    }
}
