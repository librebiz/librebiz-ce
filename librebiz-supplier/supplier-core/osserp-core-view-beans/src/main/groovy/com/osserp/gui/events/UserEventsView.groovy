/**
 *
 * Copyright (C) 2007, 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Aug 16, 2010 
 * 
 */
package com.osserp.gui.events

import javax.servlet.http.HttpSession

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.PermissionException
import com.osserp.common.UserManager
import com.osserp.common.util.DateUtil
import com.osserp.core.employees.EmployeeDisplay
import com.osserp.core.employees.EmployeeSearch
import com.osserp.core.events.Event
import com.osserp.core.events.EventManager
import com.osserp.groovy.web.MenuItem

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class UserEventsView extends AbstractEventView {
    private static Logger logger = LoggerFactory.getLogger(UserEventsView.class.getName())

    EmployeeDisplay selectedEmployee
    boolean employeeSelectionEnabled
    boolean includeDeactivated

    String eventFilterCategory


    @Override
    void initRequest(Map atts, Map headers, Map params) {
        logger.debug("initRequest: invoked")
        super.initRequest(atts, headers, params)
        eventFilterCategory = domainUser?.properties['eventFilterCategory']?.value
    }

    @Override
    protected void createCustomNavigation() {
        if (foreignView) {
            if (closeMode) {
                nav.currentNavigation = [closeLink, filterLink, switchActivationLink, homeLink]
            } else {
                nav.currentNavigation = [exitLink, closeLink, filterLink, switchActivationLink, homeLink]
            }
        } else {
            nav.currentNavigation = [closeLink, filterLink, switchActivationLink, calendarLink]
        }
    }

    MenuItem getCloseLink() {
        if (closeMode)
            new MenuItem(link: "${context.fullPath}/${context.name}/disableCloseMode", icon: 'backIcon', title: 'back')
        else
            new MenuItem(link: "${context.fullPath}/${context.name}/enableCloseMode", icon: 'deleteIcon', title: 'closeMultipeTodos')
    }

    MenuItem getSwitchActivationLink() {
        if (includeDeactivated)
            new MenuItem(link: "${context.fullPath}/${context.name}/switchActivationMode", icon: 'reloadIcon', title: 'displayVisibleOnly')
        else
            new MenuItem(link: "${context.fullPath}/${context.name}/switchActivationMode", icon: 'reloadIcon', title: 'displayInvisibleToo')
    }

    MenuItem getCalendarLink() {
        new MenuItem(link: "${context.fullPath}/users/userCalendar/forward", icon: 'replaceIcon', title: 'calendarView')
    }

    MenuItem getFilterLink() {
        new MenuItem(link: "${context.fullPath}/events/eventFilter/forward", icon: 'filterIcon', title: 'filterEventsByType', ajaxPopup: true)
    }

    /**
     * Loads events of current or selected user
     */
    void load() {
        def employee
        if (!employeeSelectionEnabled) {
            employee = getDomainUser().employee
        } else {
            employee = selectedEmployee
        }
        list = fetchEvents(employee.id)
        logger.debug("load: done [count=${list.size()}]")
    }

    void switchActivationMode() {
        includeDeactivated = !includeDeactivated
        try {
            refreshEvents()
        } catch (PermissionException e) {
            logger.info("switchActivationMode: ignoring exception [message=${e.message}]")
        }
        createCustomNavigation()
    }
    
    @Override
    void reload() {
        refreshEvents()
    }

    @Override
    void refreshEvents() {
        if (selectedEmployee) {
            list = fetchEvents(selectedEmployee.id)
        } else {
            list = fetchEvents(domainUser.employee.id)

            if (bean) {
                Event current = bean
                if (current.recipientId == domainUser.employee.id) {
                    def size = list.size()
                    for (int i = 0; i < size; i++) {
                        Event next = list.get(i)
                        if (next.id == current.id) {
                            bean = next
                            break
                        }
                    }
                }
            } else if (lastId) {
                Event current = eventManager.findEvent(lastId)
                def size = list.size()
                for (int i = 0; i < size; i++) {
                    Event next = list.get(i)
                    if (next.id == current.id) {
                        list.set(i, current)
                        logger.debug("refreshEvents() update changed done [count=${list.size()}]")
                        break
                    }
                }
                lastId = null
            }
        }
        logger.debug("refreshEvents() done [count=${list.size()}]")
    }

    void setSelectedEmployee(Long id) {
        List<EmployeeDisplay> users = employees
        def size = users.size()
        for (int i = 0; i < size; i++) {
            EmployeeDisplay current = users.get(i)
            if (current.id == id) {
                selectedEmployee = current
                employeeSelectionEnabled = true
                load()
                break
            }
        }
    }

    void selectUser() {
        setSelectedEmployee(form.getLong('id'))
        createCustomNavigation()
    }

    List<EmployeeDisplay> getEmployees() {
        employeeSearch.findActive()
    }

    boolean isForeignView() {
        (selectedEmployee != null)
    }

    void filter() {
        eventFilterCategory = form.getString("category")
        user = userManager.setProperty(user, user, 'eventFilterCategory', eventFilterCategory)
        refreshEvents()
        createCustomNavigation()
    }

    protected List fetchEvents(Long employeeId) throws PermissionException {
        eventManager.findEventsByRecipient(
                employeeId, includeDeactivated, sortBy, sortDescending, eventFilterCategory)
    }

    protected UserManager getUserManager() {
        getService(UserManager.class.getName())
    }
}
