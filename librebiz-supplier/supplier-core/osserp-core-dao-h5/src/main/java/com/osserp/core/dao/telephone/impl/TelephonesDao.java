/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 2:42:27 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.telephone.Telephones;
import com.osserp.core.model.telephone.TelephoneImpl;
import com.osserp.core.telephone.Telephone;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephonesDao extends AbstractDao implements Telephones {

    protected TelephonesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public Telephone find(Long id) {
        if (id != null) {
            List<Telephone> list = getCurrentSession().createQuery(
                    "from " + TelephoneImpl.class.getName() 
                    + " t where t.id = :pk and t.eol = false")
                    .setParameter("pk", id).list();
            if (!list.isEmpty()) {
                return list.get(0);
            }
        }
        return null;
    }

    public Telephone findByMacaddress(String mac) {
        if (mac != null) {
            List<Telephone> list = getCurrentSession().createQuery(
                    "from " + TelephoneImpl.class.getName() 
                    + " t where t.mac = :mac")
                    .setParameter("mac", mac ).list();
            if (!list.isEmpty()) {
                return list.get(0);
            }
        }
        return null;
    }

    public List<Telephone> findByReference(Long reference) {
        List<Telephone> list = new ArrayList<Telephone>();
        list = getCurrentSession().createQuery(
                "from " + TelephoneImpl.class.getName() 
                + " t where t.telephoneSystem.id = :systemid and t.eol = false order by t.mac")
                .setParameter("systemid", reference)
                .list();
        return list;
    }

    public List<Telephone> findByBranch(Long branch) {
        List<Telephone> list = new ArrayList<Telephone>();
        list = getCurrentSession().createQuery(
                "from " + TelephoneImpl.class.getName() 
                + " t where t.telephoneSystem.branchId = :branch and t.eol = ? order by t.mac")
                .setParameter("branch", branch ).list();
        return list;
    }

    public List<Telephone> getAll() {
        return getCurrentSession().createQuery("from " + TelephoneImpl.class.getName()
                + " t where t.eol = false order by t.mac").list();
    }

    public void save(Telephone telephone) {
        getCurrentSession().saveOrUpdate(TelephoneImpl.class.getName(), telephone);
    }

    public Telephone createTelephone(Telephone telephone) {
        save(telephone);
        return telephone;
    }
}
