/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 5:08:27 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.Item;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.ProductSummaries;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.DeliveryNoteTypes;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesDeliveryNotes;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.DeletedRecordImpl;
import com.osserp.core.model.records.DeliveryNoteTypeImpl;
import com.osserp.core.model.records.SalesDeliveryNoteImpl;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSummary;
import com.osserp.core.users.DomainUser;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesDeliveryNotesDao extends AbstractSalesRecords implements SalesDeliveryNotes {
    private static Logger log = LoggerFactory.getLogger(SalesDeliveryNotesDao.class.getName());
    
    private static final String DELETE_PERMISSION_DEFAULT = "executive_accounting,delivery_note_delete";
    
    private String projectsTable = null;
    private String salesDeliveryNotesTable = null;
    private String salesDeliveryNoteItemsTable = null;
    private String salesInvoicesTable = null;
    private String salesInvoiceItemsTable = null;
    private SalesOrders salesOrders = null;
    private Products products = null;
    private DeliveryNoteTypes types = null;
    private ProductSummaries productSummaries = null;
    private ProductSummaryCache summaryCache = null;
    private Long cancellationDeliveryTypeId = null;
    private Long creditNoteDeliveryTypeId = null;
    private Long invoiceDeliveryTypeId = null;
    private Long rollinDeliveryTypeId = null;

    public SalesDeliveryNotesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            SalesOrders salesOrders,
            Products products,
            DeliveryNoteTypes types,
            ProductSummaryCache summaryCache,
            Long cancellationDeliveryTypeId,
            Long invoiceDeliveryTypeId,
            Long rollinDeliveryTypeId,
            Long creditNoteDeliveryTypeId,
            ProductSummaries productSummaries) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs);
        this.projectsTable = getTable(TableKeys.PROJECTS);
        this.salesDeliveryNotesTable = getTable(TableKeys.SALES_DELIVERY_NOTES);
        this.salesDeliveryNoteItemsTable =  getTable(TableKeys.SALES_DELIVERY_NOTE_ITEMS);
        this.salesInvoicesTable =  getTable(TableKeys.SALES_INVOICES);
        this.salesInvoiceItemsTable = getTable(TableKeys.SALES_INVOICE_ITEMS);
        this.salesOrders = salesOrders;
        this.products = products;
        this.types = types;
        this.summaryCache = summaryCache;
        this.cancellationDeliveryTypeId = cancellationDeliveryTypeId;
        this.invoiceDeliveryTypeId = invoiceDeliveryTypeId;
        this.rollinDeliveryTypeId = rollinDeliveryTypeId;
        this.creditNoteDeliveryTypeId = creditNoteDeliveryTypeId;
        this.productSummaries = productSummaries;
        initializeDeletePermissions(DELETE_PERMISSION_DEFAULT);
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.SALES_DELIVERY_NOTE;
    }

    public DeliveryNote create(Employee user, Order order, DeliveryNoteType type, Date deliveryDate) throws ClientException {
        order = (Order) salesOrders.load(order.getId());
        List<Item> items = new ArrayList<>();
        if (type.isParentContent()) {
            items = getItemsToDeliver(order);
            if (items.size() < 1) {
                throw new ClientException(ErrorCode.DELIVERY_ITEMS_MISSING);
            }
        }
        return create(user, order, type, deliveryDate, items, false);
    }

    public DeliveryNote create(Employee user, Invoice invoice) {
        List<Item> items = getItemsToDeliver(invoice);
        if (items.size() < 1) {
            return null;
        }
        RecordType recordType = getRecordType();
        DeliveryNoteType type = types.load(invoiceDeliveryTypeId);
        SalesDeliveryNoteImpl obj = new SalesDeliveryNoteImpl(
                getNextRecordId(user, invoice.getCompany(), recordType, null),
                recordType,
                type,
                invoice,
                user,
                items,
                false);
        saveInitial(obj);
        return obj;
    }

    public boolean exists(Invoice invoice) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT count(*) FROM ")
                .append(salesDeliveryNotesTable)
                .append(" WHERE reference_id = ")
                .append(invoice.getId());
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class) > 0;
    }

    public DeliveryNote createRollin(Employee user, Cancellation cancellation) {
        if (log.isDebugEnabled()) {
            log.debug("createRollin() invoked [employee=" + (user == null ? "null" : user.getId())
                    + ", record=" + cancellation.getId()
                    + ", type=" + cancellation.getType().getId()
                    + ", reference=" + cancellation.getReference()
                    + "]");
        }
        List<DeliveryNote> existing = new ArrayList(getByReference(cancellation.getReference()));
        if (!existing.isEmpty()) {
            DeliveryNote note = existing.get(0);
            if (log.isDebugEnabled()) {
                log.debug("createRollin() found existing delivery [id=" + note.getId() + "]");
            }
            RecordType recordType = getRecordType();
            DeliveryNoteType type = types.load(cancellationDeliveryTypeId);
            SalesDeliveryNoteImpl obj = new SalesDeliveryNoteImpl(
                    getNextRecordId(user, cancellation.getCompany(), recordType, null),
                    recordType,
                    type,
                    cancellation,
                    user,
                    note.getItems(),
                    true);
            saveInitial(obj);
            return obj;
        }
        return null;
    }

    public DeliveryNote createRollin(Employee user, CreditNote creditNote) {
        if (!creditNote.isRollinAwareItemAvailable()) {
            return null;
        }
        Order order = getOrderByInvoice(creditNote.getReference());
        List<DeliveryNote> existing = new ArrayList(getByReference(creditNote.getId()));

        if (existing.isEmpty() && (order == null || creditNote.isDeliveryExisting())) {
            if (log.isDebugEnabled()) {
                log.debug("createRollin() no existing deliveries found [creditNote="
                        + creditNote.getId()
                        + ", reference=" + creditNote.getReference()
                        + ", order=" + (order == null ? "null" : order.getId())
                        + ", deliveryNoteRequired=" + creditNote.isDeliveryExisting()
                        + "]");
            }
            RecordType recordType = getRecordType();
            DeliveryNoteType type = types.load(creditNoteDeliveryTypeId);
            SalesDeliveryNoteImpl obj = new SalesDeliveryNoteImpl(
                    getNextRecordId(user, creditNote.getCompany(), recordType, null),
                    recordType,
                    type,
                    creditNote,
                    user,
                    creditNote.getRollinAwareItems(),
                    true);
            saveInitial(obj);
            if (log.isDebugEnabled()) {
                log.debug("createRollin() done by credit note [id=" + obj.getId()
                        + ", reference=" + obj.getReference() + "]");
            }
            return obj;
        }
        return null;
    }

    public void removeRollin(Employee user, CreditNote creditNote) {
        List<DeliveryNote> existing = new ArrayList(getByReference(creditNote.getId()));

        if (!existing.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("removeRollin() existing deliveries found [creditNote="
                        + creditNote.getId()
                        + ", count=" + existing.size()
                        + "]");
            }
            for (int i = 0, j = existing.size(); i < j; i++) {
            	DeliveryNote note = existing.get(i);
            	delete(note, user);
            }
        }
    }

    private Order getOrderByInvoice(Long invoiceId) {
        if (invoiceId == null) {
            return null;
        }
        StringBuilder sql = new StringBuilder("SELECT reference_id FROM ");
        sql
                .append(getTable(TableKeys.SALES_INVOICES))
                .append(" WHERE id = ")
                .append(invoiceId);
        List<Long> result = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        return (result.isEmpty() ? null : (Order) salesOrders.load(result.get(0)));
    }

    public DeliveryNote createRollin(Employee user, Order order, List<Item> rollIns) {
        DeliveryNoteType type = types.load(rollinDeliveryTypeId);
        return createRollin(user, order, type, rollIns);
    }

    private DeliveryNote createRollin(Employee user, Order order, DeliveryNoteType type, List<Item> rollIns) {
        return create(user, order, type, null, rollIns, true);
    }

    private DeliveryNote create(
            Employee user,
            Order order,
            DeliveryNoteType type,
            Date deliveryDate,
            List<Item> deliveries,
            boolean reverse) {

        RecordType recordType = getRecordType();
        SalesDeliveryNoteImpl obj = new SalesDeliveryNoteImpl(
                getNextRecordId(user, order.getCompany(), recordType, deliveryDate),
                recordType,
                type,
                order,
                user,
                deliveryDate,
                deliveries,
                reverse);
        saveInitial(obj);
        order.getDeliveryNotes().add(obj);
        salesOrders.save(order);
        return obj;
    }

    @Override
    public void delete(Record record, Employee user) {
        try {
            super.delete(record, user);
            if (record.isUnchangeable()) {
                if (log.isDebugEnabled()) {
                    log.debug("delete() record was unchangeable, creating log entry");
                }
                DeletedRecordImpl obj = new DeletedRecordImpl(
                        record.getId(),
                        record.getType(),
                        record.getReference(),
                        record.getCreated(),
                        record.getCreatedBy(),
                        user == null ? null : user.getId(),
                        record.getBusinessCaseId());
                getCurrentSession().saveOrUpdate(DeletedRecordImpl.class.getName(), obj);
                if (log.isDebugEnabled()) {
                    log.debug("delete() database log entry created");
                }
            }
        } catch (Throwable e) {
            log.error("delete() failed [class=" + e.getClass().getName()
                    + ", message=" + e.getMessage()
                    + "]\nStackTrace:\n\n", e);
        }
    }

    @Override
    protected boolean deletePermissionGrantByRecordType(DomainUser user, Record record, boolean defaultPermissionGrant) {
        if (record.isUnchangeable() && defaultPermissionGrant) {
            StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
            sql
                .append(salesInvoiceItemsTable)
                .append(" WHERE reference_id IN (SELECT id FROM ")
                .append(salesInvoicesTable)
                .append(" WHERE status <> ")
                .append(Record.STAT_CANCELED)
                .append(" AND reference_id = ?) AND product_id IN (SELECT product_id FROM ")
                .append(salesDeliveryNoteItemsTable)
                .append(" WHERE reference_id = ?)");
            boolean invoiceExists = (jdbcTemplate.queryForObject(sql.toString(),
                    new Object[] { record.getReference(), record.getId() },
                    new int[] { Types.BIGINT, Types.BIGINT },
                    Integer.class).intValue() > 0);
            if (invoiceExists) {
                return false;
            }
        }
        return defaultPermissionGrant;
    }

    public boolean exists(Long recordId) {
        return exists(salesDeliveryNotesTable, recordId);
    }

    public int count(Long referenceId) {
        return count(salesDeliveryNotesTable, referenceId);
    }

    public int countBySales(Long salesId) {
        return countBySales(salesDeliveryNotesTable, salesId);
    }

    public List<Long> getPrinted() {
        return getPrinted(salesDeliveryNotesTable);
    }

    public Date getCreated(Long id) {
        return getCreated(salesDeliveryNotesTable, id);
    }

    public List<OrderItemsDisplay> getUnreleasedItems(Long productId) {
        if (log.isDebugEnabled()) {
            log.debug("getUnreleasedItems() invoked [product=" + productId + "]");
        }
        Product product = products.find(productId);
        if (product != null) {
            return (List<OrderItemsDisplay>) jdbcTemplate.query(
                    RecordProcs.SALES_DELIVERY_NOTE_UNRELEASED_ITEMS_BY_PRODUCT,
                    new Object[] { productId },
                    new int[] { Types.BIGINT },
                    OrderItemsDisplayRowMapper.getReader(
                            product.getDefaultStock(),
                            summaryCache,
                            true,
                            true));
        }
        log.debug("getUnreleasedItems() invoked for nonexisting product [productId=" + productId + "]");
        return new ArrayList<OrderItemsDisplay>();
    }

    public List<OrderItemsDisplay> getUnreleasedItemsByManager(Long managerId, Long stockId) {
        if (log.isDebugEnabled()) {
            log.debug("getUnreleasedItemsByManager() invoked [manager=" + managerId + ", stock=" + stockId + "]");
        }
        if (managerId != null) {
            StringBuilder sql = new StringBuilder();
            sql
                    .append(RecordProcs.SALES_DELIVERY_NOTE_UNRELEASED_ITEMS_BY_PRODUCT)
                    .append(" WHERE sales IN (SELECT id FROM ")
                    .append(projectsTable)
                    .append(" WHERE manager_id = ?")
                    .append(" OR manager_sub_id = ?")
                    .append(") ORDER BY created");
            return (List<OrderItemsDisplay>) jdbcTemplate.query(
                    sql.toString(),
                    new Object[] { null, managerId, managerId },
                    new int[] { Types.BIGINT, Types.BIGINT, Types.BIGINT },
                    OrderItemsDisplayRowMapper.getReader(stockId, summaryCache, true, true));
        }
        return (List<OrderItemsDisplay>) jdbcTemplate.query(
                RecordProcs.SALES_DELIVERY_NOTE_UNRELEASED_ITEMS_BY_PRODUCT,
                new Object[] { null },
                new int[] { Types.BIGINT },
                OrderItemsDisplayRowMapper.getReader(stockId, summaryCache, true, true));
    }

    public List<DeliveryNote> getByOrder(Order order) {
        List<Record> records = new ArrayList(getByReference(order.getId()));
        List<DeliveryNote> result = new ArrayList<DeliveryNote>();
        for (int i = 0, j = records.size(); i < j; i++) {
            result.add((DeliveryNote) records.get(i));
        }
        return result;
    }

    @Override
    protected Class<SalesDeliveryNoteImpl> getPersistentClass() {
        return SalesDeliveryNoteImpl.class;
    }

    @Override
    protected Class<DeliveryNoteTypeImpl> getPersistentBookingTypeClass() {
        return DeliveryNoteTypeImpl.class;
    }

    private List<Item> getItemsToDeliver(Order order) {
        if (log.isDebugEnabled()) {
            log.debug("getItemsToDeliver() invoked [order=" + order.getId() + "]");
        }
        for (int i = 0, j = order.getItems().size(); i < j; i++) {
            Item next = order.getItems().get(i);
            if (next.getProduct() != null && next.getProduct().isAffectsStock()) {
                next.getProduct().addSummary(productSummaries.load(next.getStockId(), next.getProduct().getProductId()));
                next.getProduct().enableStock(next.getStockId());
            }
        }
        for (int i = 0, j = order.getDeliveryNotes().size(); i < j; i++) {
            Record delivery = order.getDeliveryNotes().get(i);
            for (int k = 0, l = delivery.getItems().size(); k < l; k++) {
                Item next = order.getItems().get(k);
                if (next.getProduct() != null && next.getProduct().isAffectsStock()) {
                    next.getProduct().addSummary(productSummaries.load(next.getStockId(), next.getProduct().getProductId()));
                    next.getProduct().enableStock(next.getStockId());
                }
            }
        }
        List<Item> deliveryList = new ArrayList<Item>();
        List<Item> openDeliveries = order.getOpenDeliveries();
        for (int i = 0, j = openDeliveries.size(); i < j; i++) {
            Item open = (Item) openDeliveries.get(i).clone();
            Product product = open.getProduct();
            if (product.isKanban()) {
                deliveryList.add(open);
                if (log.isDebugEnabled()) {
                    log.debug("getItemsToDeliver() adding kanban " +
                            "[product=" + product.getProductId() +
                            ", quantity=" + open.getQuantity() + "]");
                }
            } else {
                ProductSummary summary = product.getSummary();//this.products.loadSummary(open.getStockId(), product.getProductId());
                if (open.getQuantity() > 0) {
                    double available = summary.getStock() + summary.getReceipt();
                    if (available > 0 && open.getQuantity() > 0) {
                        if (available < open.getQuantity()) {
                            open.setQuantity(available);
                        }
                        if (log.isDebugEnabled()) {
                            log.debug("getItemsToDeliver() adding regular " +
                                    "[product=" + product.getProductId() +
                                    ", quantity=" + open.getQuantity() + "]");
                        }
                        deliveryList.add(open);
                    }
                } else if (open.getQuantity() < 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("getItemsToDeliver() adding receipt " +
                                "[product=" + product.getProductId() +
                                ", quantity=" + open.getQuantity() + "]");
                    }
                    deliveryList.add(open);
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getItemsToDeliver() done [order=" + order.getId() +
                    ", count=" + deliveryList.size() + "]");
        }
        return deliveryList;
    }

    private List<Item> getItemsToDeliver(Invoice invoice) {
        List<Item> deliveryList = new ArrayList<Item>();
        for (int i = 0, j = invoice.getItems().size(); i < j; i++) {
            Item open = invoice.getItems().get(i);
            if (open.isDeliveryNoteAffecting()) {
                deliveryList.add(open);
            }
        }
        return deliveryList;
    }
}
