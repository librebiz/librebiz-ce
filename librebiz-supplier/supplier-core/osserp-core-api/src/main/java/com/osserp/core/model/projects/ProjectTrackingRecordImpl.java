/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.projects;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;

import com.osserp.core.projects.ProjectTracking;
import com.osserp.core.projects.ProjectTrackingRecord;
import com.osserp.core.projects.ProjectTrackingRecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectTrackingRecordImpl extends AbstractOption implements ProjectTrackingRecord {

    private ProjectTrackingRecordType type;

    private Date startTime;
    private Date endTime;
    private Double duration = 0d;
    private String internalNote;

    private boolean charged = false;
    private boolean ignore = false;
    private Long invoiceId;

    protected ProjectTrackingRecordImpl() {
        super();
    }

    /**
     * Creates a new project tracking record by start and end date or duration as 2nd choice.
     * @param user
     * @param reference
     * @param recordType
     * @param name
     * @param description
     * @param internalNote
     * @param startDate
     * @param endDate
     * @param time direct hours input as alternative to start and end date. Do not use in conjunction.
     */
    protected ProjectTrackingRecordImpl(
            Long user,
            ProjectTracking reference,
            ProjectTrackingRecordType recordType,
            String name,
            String description,
            String internalNote,
            Date startDate,
            Date endDate,
            Double time) {
        super((Long) null, reference.getId(), name, description, (Date) null, user);
        this.type = recordType;
        this.internalNote = internalNote;
        updateDateValues(startDate, endDate, time);
    }
    
    public boolean isSingleDay() {
        return endTime == null ? true : DateUtil.isSameDay(startTime, endTime);
    }

    public ProjectTrackingRecordType getType() {
        return type;
    }

    public void setType(ProjectTrackingRecordType type) {
        this.type = type;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    
    public Integer getStartHours() {
        return DateUtil.getTimeHours(startTime);
    }
    
    public Integer getStartMinutes() {
        return DateUtil.getTimeMinutes(startTime);
    }

    public String getStartMinutesDisplay() {
        if (startTime == null) {
            return null;
        }
        return DateFormatter.getFormatted(getStartMinutes());
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    
    public Integer getEndHours() {
        return DateUtil.getTimeHours(endTime);
    }
    
    public Integer getEndMinutes() {
        return DateUtil.getTimeMinutes(endTime);
    }

    public String getEndMinutesDisplay() {
        if (endTime == null) {
            return null;
        }
        return DateFormatter.getFormatted(getEndMinutes());
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public String getInternalNote() {
        return internalNote;
    }

    public void setInternalNote(String internalNote) {
        this.internalNote = internalNote;
    }

    public boolean isCharged() {
        return charged;
    }

    public void setCharged(boolean charged) {
        this.charged = charged;
    }

    public boolean isIgnore() {
        return ignore;
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public void changeIgnore(Long user, boolean ignore) {
        this.ignore = ignore;
        updateChanged(user);
    }

    public void close(Long invoice) {
        invoiceId = invoice;
        if (invoiceId != null) {
            charged = true;
        } else {
            charged = false;
        }
    }

    public void update(
            Long user, 
            ProjectTrackingRecordType recordType, 
            String name, 
            String description,
            String internalNote,
            Date startTime, 
            Date endTime, 
            Double duration) {
        type = recordType;
        setName(name);
        setDescription(description);
        setInternalNote(internalNote);
        updateDateValues(startTime, endTime, duration);
        updateChanged(user);
    }
    
    private void updateDateValues(Date startDate, Date endDate, Double time) {
        if (startDate != null && endDate != null) {
            startTime = startDate;
            endTime = endDate;
            duration = DateUtil.getDurationHours(startTime, endTime); 
        } else {
            startTime = startDate;
            duration = time;
        }
    }
}
