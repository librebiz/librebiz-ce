/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 7, 2009 4:39:40 PM 
 * 
 */
package com.osserp.core.service.calc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.ItemList;
import com.osserp.core.ItemListManager;
import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.ProductSelections;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.ItemLists;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductUtil;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractItemListManager extends AbstractService implements ItemListManager {
    private Products products = null;
    private ProductPrices productPrices = null;
    private ProductSelections productSelections = null;
    private ItemLists itemLists = null;

    /**
     * Creates a new ItemListManager
     * @param products
     * @param productPrices
     * @param productSelections
     * @param itemLists
     */
    public AbstractItemListManager(Products products, ProductPrices productPrices, ProductSelections productSelections, ItemLists itemLists) {
        this.products = products;
        this.productPrices = productPrices;
        this.productSelections = productSelections;
        this.itemLists = itemLists;
    }

    public ItemList getActive(Long reference) {
        return itemLists.getActive(reference);
    }

    public ItemList getList(Long id) {
        return itemLists.load(id);
    }

    public final ItemList updateItem(
            DomainUser user,
            ItemList itemList,
            Long itemId,
            String customName,
            Double quantity,
            BigDecimal partnerPrice,
            boolean partnerPriceOverridden,
            boolean partnerPriceOverriddenByPermission,
            BigDecimal price,
            String note)
            throws ClientException {
        ItemList obj = itemLists.load(itemList.getId());
        obj.updateItem(
                itemId, 
                customName, 
                quantity, 
                partnerPrice, 
                partnerPriceOverridden, 
                partnerPriceOverriddenByPermission, 
                price, 
                note);
        return save(user, obj);
    }

    public ItemList replaceItem(
            DomainUser user,
            ItemList itemList,
            Long itemId,
            Long productId,
            Double quantity,
            BigDecimal price,
            String note)
            throws ClientException {

        Product product = products.load(productId);
        ItemList obj = itemLists.load(itemList.getId());
        List<ProductSelectionConfigItem> partnerPriceEditables = getPartnerPriceEditables();
        obj.replaceItem(
                itemId,
                product,
                quantity,
                price,
                itemList.getInitialCreated(),
                new BigDecimal(productPrices.getPartnerPrice(product, itemList.getInitialCreated(), quantity)),
                ProductUtil.isPartnerPriceEditable(partnerPriceEditables, product),
                new BigDecimal(productPrices.getPurchasePrice(product, product.getDefaultStock(), itemList.getInitialCreated())),
                note);
        return save(user, obj);
    }

    public ItemList moveUpItem(DomainUser user, ItemList itemList, Long itemId) {
        ItemList obj = itemLists.load(itemList.getId());
        obj.moveUpItem(itemId);
        return save(user, obj);
    }

    public ItemList moveDownItem(DomainUser user, ItemList itemList, Long itemId) {
        ItemList obj = itemLists.load(itemList.getId());
        obj.moveDownItem(itemId);
        return save(user, obj);
    }

    protected final Products getProducts() {
        return products;
    }

    protected final ProductPrices getProductPrices() {
        return productPrices;
    }

    protected final ItemLists getItemLists() {
        return itemLists;
    }

    protected List<ProductSelectionConfigItem> getPartnerPriceEditables() {
        return productSelections.findByEditablePartnerPrice();
    }

    private ItemList save(DomainUser user, ItemList itemList) {
        itemList.setChangedBy(user.getEmployee().getId());
        itemList.setChanged(new Date(System.currentTimeMillis()));
        getItemLists().save(itemList);
        return getItemLists().load(itemList.getId());
    }

}
