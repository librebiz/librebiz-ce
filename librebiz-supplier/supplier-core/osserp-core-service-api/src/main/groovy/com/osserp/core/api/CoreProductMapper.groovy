/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 15.12.2012 
 * 
 */
package com.osserp.core.api

import com.osserp.api.WSProduct

import com.osserp.common.OptionsCache

import com.osserp.core.api.ProductImportVO
import com.osserp.core.dao.Products
import com.osserp.core.dao.QuantityUnits
import com.osserp.core.dao.SystemConfigs
import com.osserp.core.products.Product
import com.osserp.core.products.ProductClassificationConfig;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CoreProductMapper extends AbstractProductMapper {

    protected CoreProductMapper(OptionsCache optionsCache, SystemConfigs systemConfigs, Products products, QuantityUnits quantityUnits) {
        super(optionsCache, systemConfigs, products, quantityUnits);
    }

    Product map(WSProduct product) {
        if (product.primaryKey) {
            Product obj = products.find(product.primaryKey)
            if (obj) {

                obj.name = product.name
                //obj. product.info
                obj.description = product.description
                //obj.shortDescription = product.shortDescription
                obj.marketingText = product.marketingText

                if (product.gtin) {
                    obj.gtin13 = product.gtin
                }

                Long manufacturerId = fetchManufacturer(product.manufacturer, obj.group.id)
                if (manufacturerId) {
                    obj.manufacturer = manufacturerId 
                }
                if (obj.details) {
                    Long color = fetchColor(product.color)
                    if (color) {
                        obj.details.color = color
                    }
                    if (product.width) {
                        obj.details.width = product.width
                    }
                    if (product.height) {
                        obj.details.height = product.height
                    }
                    if (product.lenght) {
                        obj.details.depth = product.lenght
                    }
                    if (product.measurementUnit) {
                        obj.details.measurementUnit = product.measurementUnit
                    }
                    if (product.techNote) {
                        obj.details.techNote = product.techNote
                    }
                    if (product.weight) {
                        obj.details.weight = product.weight
                    }
                    if (product.weightUnit) {
                        obj.details.weightUnit = product.weightUnit
                    }
                }
                Long quantityUnit = fetchQuantityUnit(product.quantityUnit)
                if (quantityUnit) {
                    obj.quantityUnit = quantityUnit
                } else if (!obj.quantityUnit && obj.group?.quantityUnit) {
                    obj.quantityUnit = obj.group.quantityUnit
                }
                if (product.power) {
                    obj.power = product.power
                }
                Long powerUnit = fetchPowerUnit(product.powerUnit)
                if (powerUnit) {
                    obj.powerUnit = powerUnit
                } else if (!obj.powerUnit && obj.group?.powerUnit) {
                    obj.powerUnit = obj.group.powerUnit
                }
                if (product.consumerPrice) {
                    obj.consumerPrice = product.consumerPrice 
                }
                if (product.resellerPrice) {
                    obj.resellerPrice = product.resellerPrice
                }
                if (product.partnerPrice) {
                    obj.partnerPrice = product.partnerPrice
                }
                if (product.purchasePrice) {
                    obj.estimatedPurchasePrice = product.purchasePrice
                }
                if (product instanceof ProductImportVO) {
                    ProductImportVO ivo = product
                    obj.setMatchcode(ivo.matchcode)
                    obj.setAffectsStock(ivo.stockAffecting)
                    obj.setBundle(ivo.bundle)
                    obj.setKanban(ivo.kanban)
                    obj.setNullable(ivo.nullable)
                    obj.setPlant(ivo.plant)
                    obj.setQuantityByPlant(ivo.quantityByPlant)
                    if (ivo.stockId) {
                        obj.setDefaultStock(ivo.stockId)
                    }
                    if (ivo.matchcode) {
                        obj.setMatchcode(ivo.matchcode)
                    }
                }
                return obj
            }
        }
    }

    ProductClassificationConfig fetchConfig(WSProduct wsProduct) {
        Long rangeStart = null
        Long rangeEnd = null
        if (wsProduct instanceof ProductImportVO) {
            ProductImportVO vo = wsProduct
            rangeStart = vo.rangeStart
            rangeEnd = vo.rangeEnd
        }
        return products.fetchOrCreate(
            wsProduct.typeId,
            wsProduct.type,
            wsProduct.groupId,
            wsProduct.group,
            wsProduct.categoryId,
            wsProduct.category,
            rangeStart,
            rangeEnd)
    }
}
