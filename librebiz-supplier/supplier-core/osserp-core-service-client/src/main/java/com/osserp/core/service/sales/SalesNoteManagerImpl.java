/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 28, 2010 11:19:46 AM 
 * 
 */
package com.osserp.core.service.sales;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.SelectOption;
import com.osserp.common.mail.Mail;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.User;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessNote;
import com.osserp.core.NoteAware;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.MailTemplates;
import com.osserp.core.dao.ProjectSuppliers;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.Users;
import com.osserp.core.employees.Employee;
import com.osserp.core.mail.MailMessageContext;
import com.osserp.core.mail.MailMessageRecipient;
import com.osserp.core.mail.MailTemplate;
import com.osserp.core.model.NoteAwareVO;
import com.osserp.core.projects.ProjectSupplier;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesNoteManager;
import com.osserp.core.service.impl.AbstractNoteManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesNoteManagerImpl extends AbstractNoteManager implements SalesNoteManager {
    private static Logger log = LoggerFactory.getLogger(SalesNoteManagerImpl.class.getName());
    private Projects projects = null;
    private ProjectSuppliers suppliers = null;
    private Requests requests = null;
    private ResourceLocator resourceLocator = null;
    private Users users = null;

    protected SalesNoteManagerImpl(
            BusinessNotes businessNotes,
            MailTemplates mailTemplates,
            Employees employees,
            Requests requests,
            Projects projects,
            ProjectSuppliers suppliers,
            Users users,
            ResourceLocator resourceLocator) {
        super(businessNotes, mailTemplates, employees);
        this.projects = projects;
        this.requests = requests;
        this.suppliers = suppliers;
        this.users = users;
        this.resourceLocator = resourceLocator;
    }

    @Override
    public NoteAware load(Long primaryKey) {
        Request request = null;
        if (requests.exists(primaryKey)) {
            request = requests.load(primaryKey);
        } else {
            request = getSales(primaryKey).getRequest();
        }
        return create(request);
    }

    @Override
    protected String getTypeName() {
        return "salesNote";
    }

    public MailMessageContext setupMail(Employee user, NoteAware noteAware, Map<String, String> opts) throws ClientException {
        MailTemplate mtpl = getMailTemplate();
        String originator = getOriginator(user, mtpl);
        String message = opts.get("message");
        Sales sales = null;
        Request request = null;
        if (requests.exists(noteAware.getPrimaryKey())) {
            request = requests.load(noteAware.getPrimaryKey());
        } else {
            sales = getSales(noteAware.getPrimaryKey());
            request = sales.getRequest();
        }
        Long businessId = (sales != null ? sales.getId() : request.getRequestId());
        StringBuilder subject = new StringBuilder(businessId.toString());
        subject.append(" - ").append(request.getName());

        Mail mail = new Mail(originator, subject.toString(), message);
        return addSuggestedRecipients(new MailMessageContext(mtpl, mail, opts), (sales != null ? sales : request));
    }

    @Override
    public MailMessageContext setupMail(Employee user, BusinessNote note, Map<String, String> opts) {
        MailMessageContext ctx = super.setupMail(user, note, opts);
        BusinessCase businessCase = null;
        if (requests.exists(note.getReference())) {
            businessCase = requests.load(note.getReference());
        } else {
            businessCase = getSales(note.getReference());
        }
        if (businessCase != null) {
            return addSuggestedRecipients(ctx, businessCase);
        }
        return ctx;
    }

    protected MailMessageContext addSuggestedRecipients(MailMessageContext ctx, BusinessCase businessCase) {
        Sales sales = null;
        Request request = null;
        if (businessCase instanceof Sales) {
            sales = (Sales) businessCase;
            request = sales.getRequest();
        } else {
            request = (Request) businessCase;
        }
        List<MailMessageRecipient> recipientSuggestions = new ArrayList<>();
        Set<Long> involved = new HashSet<>();

        if (request.getSalesId() != null) {
            if (!addRecipientIfRequired(ctx.getMail(), request.getSalesId())) {
                addEmployeeSuggestion(recipientSuggestions, request.getSalesId(), "sales", "involved");
                involved.add(request.getSalesId());
            }
        }
        if (request.getSalesCoId() != null) {
            if (!addRecipientIfRequired(ctx.getMail(), request.getSalesCoId())) {
                addEmployeeSuggestion(recipientSuggestions, request.getSalesCoId(), "coSales", "involved");
                involved.add(request.getSalesCoId());
            }
        }
        if (sales != null) {
            if (sales.getManagerId() != null) {
                if (!addRecipientIfRequired(ctx.getMail(), sales.getManagerId())) {
                    addEmployeeSuggestion(recipientSuggestions,
                            sales.getManagerId(), "responsible", "involved");
                    involved.add(sales.getManagerId());
                }
            }
            if (sales.getManagerSubstituteId() != null) {
                if (!addRecipientIfRequired(ctx.getMail(), sales.getManagerSubstituteId())) {
                    addEmployeeSuggestion(recipientSuggestions,
                            sales.getManagerSubstituteId(), "coResponsible", "involved");
                    involved.add(sales.getManagerSubstituteId());
                }
            }
        }
        List<SelectOption> employeeList = getEmployees().findEmailsAndNames(new HashSet<>());
        String groupName = resourceLocator.getMessage("employee");
        employeeList.forEach(employee -> {
            MailMessageRecipient rcpt = new MailMessageRecipient(
                    groupName,
                    employee.getLabelProperty(),
                    employee.getProperty(),
                    "employee");
            recipientSuggestions.add(rcpt);
        });
        if (request.getCustomer() != null && request.getCustomer().getEmail() != null) {
            MailMessageRecipient rcpt = new MailMessageRecipient(
                    resourceLocator.getMessage("customer"),
                    request.getCustomer().getDisplayName(),
                    request.getCustomer().getEmail(),
                    "customer");
            recipientSuggestions.add(rcpt);
        }
        List<ProjectSupplier> supplierList = suppliers.getSuppliers(businessCase.getPrimaryKey());
        supplierList.forEach(supplier -> {
            if (supplier.getSupplier().getEmail() != null &&
                    !alreadyAdded(recipientSuggestions, supplier.getSupplier().getEmail())) {
                MailMessageRecipient rcpt = new MailMessageRecipient(
                        supplier.getGroupName(),
                        supplier.getSupplier().getDisplayName(),
                        supplier.getSupplier().getEmail(),
                        "supplier");
                recipientSuggestions.add(rcpt);
            }
        });
        if (!recipientSuggestions.isEmpty()) {
            ctx.setRecipientSuggestions(recipientSuggestions);
        }
        if (log.isDebugEnabled()) {
            log.debug("addSuggestedRecipients: Found " + supplierList.size() +
                    " suppliers, total suggestions: " + ctx.getRecipientSuggestions().size());
        }
        return ctx;
    }

    private boolean addRecipientIfRequired(Mail mail, Long employeeId) {
        Long userId = users.getUserByEmployee(employeeId);
        if (userId != null) {
            User salesPerson = users.find(userId);
            if (salesPerson.isPropertyEnabled("salesNoteMailAutoCC")) {
                addRecipient(mail, employeeId, "cc");
                return true;
            } else if (salesPerson.isPropertyEnabled("salesNoteMailAutoBCC")) {
                addRecipient(mail, employeeId, "bcc");
                return true;
            }
        }
        return false;
    }

    private void addEmployeeSuggestion(
            List<MailMessageRecipient> recipientSuggestions,
            Long employeeId,
            String groupName,
            String selector) {
        MailMessageRecipient rcpt = fetchEmployeeAddress(employeeId, groupName, selector);
        if (rcpt != null && !alreadyAdded(recipientSuggestions, rcpt.getEmail())) {
            recipientSuggestions.add(rcpt);
        }
    }

    private MailMessageRecipient fetchEmployeeAddress(Long employeeId, String groupName, String selector) {
        Employee employee = getEmployee(employeeId);
        if (employee != null) {
            String address = employee.getEmail();
            if (address != null) {
                MailMessageRecipient rcpt = new MailMessageRecipient(
                        resourceLocator.getMessage(groupName != null ? groupName : "employee"),
                        employee.getName(),
                        address,
                        selector);
                return rcpt;
            }
        }
        return null;
    }

    private boolean alreadyAdded(List<MailMessageRecipient> recipientSuggestions, String emailAddress) {
        for (int i = 0; i < recipientSuggestions.size(); i++) {
            MailMessageRecipient recipient = recipientSuggestions.get(i);
            if (recipient.getEmail().equalsIgnoreCase(emailAddress)) {
                return true;
            }
        }
        return false;
    }

    private Sales getSales(Long id) {
        return projects.load(id);
    }

    private NoteAware create(Request request) {
        List<BusinessNote> notes = getBusinessNotes().findByReference(getTypeName(), request.getRequestId());
        return new NoteAwareVO(request.getRequestId(), request.getName(), notes);
    }
}
