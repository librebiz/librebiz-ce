/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.dao.impl;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.dao.ProductSelectionConfigs;
import com.osserp.core.sales.SalesRevenueConfig;
import com.osserp.core.sales.SalesRevenueConfigProvider;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class SalesRevenueConfigProviderImpl extends AbstractOption implements SalesRevenueConfigProvider {

    private ProductSelectionConfigs productSelectionConfigs;

    private Long hardwareCostsAFilterId;
    private Long hardwareCostsBFilterId;
    private Long hardwareCostsCFilterId;
    private Long otherCostsFilterId;
    private Long foreignCostsFilterId;
    private Long developmentCostsFilterId;
    private Long unknownCostsFilterId;

    protected SalesRevenueConfigProviderImpl() {
        super();
    }

    protected SalesRevenueConfigProviderImpl(
            ProductSelectionConfigs productSelectionConfigs,
            Long hardwareCostsAFilterId,
            Long hardwareCostsBFilterId,
            Long hardwareCostsCFilterId,
            Long otherCostsFilterId,
            Long foreignCostsFilterId,
            Long developmentCostsFilterId,
            Long unknownCostsFilterId,
            String name) {
        super(name);
        this.productSelectionConfigs = productSelectionConfigs;
        this.hardwareCostsAFilterId = hardwareCostsAFilterId;
        this.hardwareCostsBFilterId = hardwareCostsBFilterId;
        this.hardwareCostsCFilterId = hardwareCostsCFilterId;
        this.otherCostsFilterId = otherCostsFilterId;
        this.foreignCostsFilterId = foreignCostsFilterId;
        this.developmentCostsFilterId = developmentCostsFilterId;
        this.unknownCostsFilterId = unknownCostsFilterId;
    }

    public SalesRevenueConfig getConfig() {
        SalesRevenueConfig config = new SalesRevenueConfig(getName());
        config.setFilters(
                productSelectionConfigs.load(getHardwareCostsAFilterId()),
                productSelectionConfigs.load(getHardwareCostsBFilterId()),
                productSelectionConfigs.load(getHardwareCostsCFilterId()),
                productSelectionConfigs.load(getOtherCostsFilterId()),
                productSelectionConfigs.load(getForeignCostsFilterId()),
                productSelectionConfigs.load(getDevelopmentCostsFilterId()),
                productSelectionConfigs.load(getUnknownCostsFilterId()));
        return config;
    }

    public Long getHardwareCostsAFilterId() {
        return hardwareCostsAFilterId;
    }

    public Long getHardwareCostsBFilterId() {
        return hardwareCostsBFilterId;
    }

    public Long getHardwareCostsCFilterId() {
        return hardwareCostsCFilterId;
    }

    public Long getOtherCostsFilterId() {
        return otherCostsFilterId;
    }

    public Long getForeignCostsFilterId() {
        return foreignCostsFilterId;
    }

    public Long getDevelopmentCostsFilterId() {
        return developmentCostsFilterId;
    }

    public Long getUnknownCostsFilterId() {
        return unknownCostsFilterId;
    }

    protected void setHardwareCostsAFilterId(Long hardwareCostsAFilterId) {
        this.hardwareCostsAFilterId = hardwareCostsAFilterId;
    }

    protected void setHardwareCostsBFilterId(Long hardwareCostsBFilterId) {
        this.hardwareCostsBFilterId = hardwareCostsBFilterId;
    }

    protected void setHardwareCostsCFilterId(Long hardwareCostsCFilterId) {
        this.hardwareCostsCFilterId = hardwareCostsCFilterId;
    }

    protected void setOtherCostsFilterId(Long otherCostsFilterId) {
        this.otherCostsFilterId = otherCostsFilterId;
    }

    protected void setForeignCostsFilterId(Long foreignCostsFilterId) {
        this.foreignCostsFilterId = foreignCostsFilterId;
    }

    protected void setDevelopmentCostsFilterId(Long developmentCostsFilterId) {
        this.developmentCostsFilterId = developmentCostsFilterId;
    }

    protected void setUnknownCostsFilterId(Long unknownCostsFilterId) {
        this.unknownCostsFilterId = unknownCostsFilterId;
    }

}
