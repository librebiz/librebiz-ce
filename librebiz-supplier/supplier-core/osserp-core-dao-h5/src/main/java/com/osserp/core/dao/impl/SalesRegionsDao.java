/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 18, 2011 2:23:30 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.SalesRegions;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.SalesRegionConfigImpl;
import com.osserp.core.model.SalesRegionImpl;
import com.osserp.core.model.SalesRegionZipcode;
import com.osserp.core.sales.SalesRegion;
import com.osserp.core.sales.SalesRegionConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRegionsDao extends AbstractDao implements SalesRegions {
    private static Logger log = LoggerFactory.getLogger(SalesRegionsDao.class.getName());

    protected SalesRegionsDao(JdbcTemplate jdbcTemplate, Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public SalesRegionConfig getConfig() {
        List<SalesRegionConfig> configs = getCurrentSession().createQuery(
                "from " + SalesRegionConfigImpl.class.getName()).list();
        return (configs.isEmpty() ? null : configs.get(0));
    }

    public SalesRegion create(Employee user, String name, Employee sales, Employee salesExecutive) {
        SalesRegionImpl obj = new SalesRegionImpl(user, name, sales, salesExecutive);
        save(obj);
        return obj;
    }

    public void delete(Long id) {
        try {
            SalesRegion region = getRegion(id);
            getCurrentSession().delete(SalesRegionImpl.class.getName(), region);
        } catch (Exception e) {
            log.warn("delete() failed [id=" + id + ", message=" + e.getMessage() + "]", e);
        }
    }

    public SalesRegion find(String zipcode) {
        List<SalesRegionZipcode> matchings = getCurrentSession().createQuery(
                "from " + SalesRegionZipcode.class.getName() 
                + " o where o.zipcode = :zip").setParameter("zip", zipcode).list();
        SalesRegionZipcode matching = (matchings.isEmpty() ? null : matchings.get(0));
        if (matching == null) {
            if (log.isDebugEnabled()) {
                log.debug("find() did not find matching [zipcode=" + zipcode + "]");
            }
            return null;
        }
        if (log.isDebugEnabled()) {
            log.debug("find() found matching [zipcode=" + zipcode + ", region=" + matching.getReference() + "]");
        }
        return getRegion(matching.getReference());
    }

    public List<SalesRegion> find(Employee sales) {
        return getCurrentSession().createQuery(
                "from " + SalesRegionImpl.class.getName() 
                + " o where o.sales.id = :salesId")
                .setParameter("salesId", sales.getId()).list();
    }

    public List<SalesRegion> findAll() {
        return getCurrentSession().createQuery(
                "from " + SalesRegionImpl.class.getName()
                + " o order by o.id").list();
    }

    public SalesRegion getRegion(Long id) {
        return (SalesRegion) getCurrentSession().load(SalesRegionImpl.class.getName(), id);
    }

    public boolean isRegionAssigned(Employee sales) {
        List<SalesRegion> matching = getCurrentSession().createQuery(
                "from " + SalesRegionImpl.class.getName() + " o where o.sales.id = :salesId")
                .setParameter("salesId", sales.getId()).list();
        return (matching.isEmpty() ? false : true);
    }

    public void save(SalesRegion region) {
        getCurrentSession().saveOrUpdate(SalesRegionImpl.class.getName(), region);
    }

}
