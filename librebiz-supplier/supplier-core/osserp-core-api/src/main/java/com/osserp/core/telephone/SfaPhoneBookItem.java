/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on May 25, 2010 1:28:54 PM 
 * 
 */
package com.osserp.core.telephone;

import java.util.Date;

import com.osserp.common.Entity;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface SfaPhoneBookItem extends Entity {

    /**
     * Indicates if synced is true
     * @return synced
     */
    boolean isSynced();

    /**
     * Sets synced
     * @param synced
     */
    void setSynced(boolean synced);

    /**
     * Indicates if unused is true
     * @return unused
     */
    boolean isUnused();

    /**
     * Sets unused
     * @param unused
     */
    void setUnused(boolean unused);

    /**
     * Returns the uid
     * @return uid
     */
    String getUid();

    /**
     * Sets the uid
     * @param uid
     */
    void setUid(String uid);

    /**
     * Returns the contactId
     * @return contactId
     */
    Long getContactId();

    /**
     * Sets the contactId
     * @param contactId
     */
    void setContactId(Long contactId);

    /**
     * Returns the name
     * @return name
     */
    String getName();

    /**
     * Sets the name
     * @param name
     */
    void setName(String name);

    /**
     * Returns the number
     * @return number
     */
    String getNumber();

    /**
     * Sets the number
     * @param number
     */
    void setNumber(String number);

    /**
     * Returns the firstName
     * @return firstName
     */
    String getFirstName();

    /**
     * Sets the firstName
     * @param firstName
     */
    void setFirstName(String firstName);

    /**
     * Returns the lastName
     * @return lastName
     */
    String getLastName();

    /**
     * Sets the lastName
     * @param lastName
     */
    void setLastName(String lastName);

    /**
     * Returns the numberType
     * @return numberType
     */
    Long getNumberType();

    /**
     * Sets the numberType
     * @param numberType
     */
    void setNumberType(Long numberType);

    /**
     * Returns the title
     * @return title
     */
    String getTitle();

    /**
     * Sets the title
     * @param title
     */
    void setTitle(String title);

    /**
     * Returns the organization
     * @return organization
     */
    String getOrganization();

    /**
     * Sets the organization
     * @param organization
     */
    void setOrganization(String organization);

    /**
     * Returns the email
     * @return email
     */
    String getEmail();

    /**
     * Sets the email
     * @param email
     */
    void setEmail(String email);

    /**
     * Returns the note
     * @return note
     */
    String getNote();

    /**
     * Sets the note
     * @param note
     */
    void setNote(String note);

    /**
     * Returns the groupTypeName
     * @return groupTypeName
     */
    String getGroupTypeName();

    /**
     * Sets the groupTypeName
     * @param groupTypeName
     */
    void setGroupTypeName(String groupTypeName);

    /**
     * Returns the birthdayItem
     * @return birthdayItem
     */
    Date getBirthdayItem();

    /**
     * Sets the birthdayItem
     * @return birthdayItem
     */
    void setBirthdayItem(Date birthdayItem);

    /**
     * Returns the favourite
     * @return favourite
     */
    boolean isFavourite();

    /**
     * Sets the favourite
     * @param favourite
     */
    void setFavourite(boolean favourite);
}
