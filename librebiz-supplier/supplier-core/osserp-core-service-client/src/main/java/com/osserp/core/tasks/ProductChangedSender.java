/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 8, 2012 at 14:49:32 
 * 
 */
package com.osserp.core.tasks;

import java.util.Map;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductChangedSender {

    static final String PRODUCT_ID = "productId";
    static final String PRODUCT_DOCUMENT_ID = "productDocumentId";
    static final String PRODUCT_DOCUMENT_TYPE = "productDocumentType";
    
    static final String EVENT = "event";
    static final String EVENT_PRODUCT_CHANGED = "productChanged";
    static final String EVENT_PRODUCT_MEDIA_CHANGED = "productMediaChanged";

    /**
     * Sends minimal required informations about an product changed event.
     * @param articeId
     * @param eventName
     */
    void send(Long productId, String eventName);

    /**
     * Sends informations about product change events.
     * @param eventChangedValues
     */
    void send(Map<String, Object> eventChangedValues);

    /**
     * Sends the default event 'productChanged'. Any basic product property may have been changed.
     * @param articeId
     */
    void sendChanged(Long productId);

    /**
     * Sends the default media event 'productMediaChanged'. 
     * @param articeId
     * @param documentType the internal document type of the media (e.g. picture, datasheet, etc.)
     * @param documentId
     */
    void sendMediaChanged(Long productId, Long documentType, Long documentId);

    /**
     * Creates synchronisation tasks for all products with enabled public available flag.
     * Invoking this method only makes sense if clientSync is configured and enabled. 
     */
    void syncPublic();

    /**
     * Creates media synchronization tasks for all product related media with enabled 
     * public availabilty flag on referenced product.
     */
    void syncPublicMedia();

}
