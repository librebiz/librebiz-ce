<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="headline">Software License Agreement - SLA</tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
.license-text {
    width: 98%;
    height: 40em;
    margin-left: auto;
    margin-right: auto;
    border: 1px solid grey;
}
.license-copyinfo {
    font-style: italic;
}
</style>
</tiles:put>
<tiles:put name="content" type="string" >
    <div class="content-area" id="licenseContent">
        <div class="row">
            <div class="license-area">
                <c:import url="license.jsp"/>
            </div>
        </div>
    </div>
</tiles:put>
</tiles:insert>
