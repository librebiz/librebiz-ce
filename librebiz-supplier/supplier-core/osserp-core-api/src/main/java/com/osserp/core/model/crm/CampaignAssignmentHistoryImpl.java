/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.crm;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.crm.Campaign;
import com.osserp.core.crm.CampaignAssignmentHistory;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CampaignAssignmentHistoryImpl extends AbstractEntity implements CampaignAssignmentHistory {

    private Campaign campaign;
    private Campaign previousCampaign;

    protected CampaignAssignmentHistoryImpl() {
        super();
    }

    public CampaignAssignmentHistoryImpl(Employee user, Request request, Campaign campaign, Campaign previousCampaign) {
        super((Long) null, request.getRequestId(), user.getId());
        this.campaign = campaign;
        this.previousCampaign = previousCampaign;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public Campaign getPreviousCampaign() {
        return previousCampaign;
    }

    public void setPreviousCampaign(Campaign previousCampaign) {
        this.previousCampaign = previousCampaign;
    }
}
