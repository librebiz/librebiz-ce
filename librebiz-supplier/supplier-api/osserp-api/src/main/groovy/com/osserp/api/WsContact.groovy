/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api

import javax.persistence.Column
import javax.persistence.Transient
import javax.persistence.Entity
import javax.persistence.Table

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Entity
@Table(name='ws_contact')
class WsContact extends WsObject implements WSContact {
    private static final long serialVersionUID = 4L
    
    public static final Long PRIVATE_CONTACT = 1L
    public static final Long BUSINESS_CONTACT = 2L
    
    public static final String STATUS_INTEREST = 'interest'
    public static final String STATUS_CONTACT_FORM = 'contact_form'
    public static final String STATUS_CUSTOMER = 'customer'
    public static final String STATUS_RESELLER = 'reseller'
    
    @Transient
    boolean justCreated = false
    
    String status = STATUS_CUSTOMER
    
    boolean isPrivateContact() {
        if (PRIVATE_CONTACT == contactTypeId) {
            return true
        }
        if (!company) {
            return true
        }
        return false
    }

    boolean isReseller() {
        STATUS_RESELLER == status 
    }
    
    WsContact() {
        super()
    }
    
    WsContact(Long contactTypeId, String status) {
        super()
        this.contactTypeId = contactTypeId
        this.status = status
    }
    
    String branch
    @Column(name='branch_id')
    Long branchId
    
    @Column(name='contact_type_id')
    Long contactTypeId

    String company
    
    String salutation
    @Column(name='salutation_id')
    Long salutationId

    String title
    @Column(name='title_id')
    Long titleId

    @Column(name='firstname')
    String firstName
    @Column(name='lastname')
    String lastName

    @Column(name='date_of_birth')
    Date dateOfBirth

    String groupname
    @Column(name='jobtitle')
    String jobTitle

    String street
    @Column(name='street_addon')
    String streetAddon
    String city
    @Column(name='federal_state')
    String federalState
    String zipcode
    String country
    @Column(name='country_id')
    Long countryId

    @Column(name='b_phone')
    String businessPhone
    @Column(name='b_mobile')
    String businessMobile
    @Column(name='b_fax')
    String businessFax
    @Column(name='b_email')
    String businessEmail

    @Column(name='p_phone')
    String privatePhone
    @Column(name='p_mobile')
    String privateMobile
    @Column(name='p_fax')
    String privateFax
    @Column(name='p_email')
    String privateEmail

    String website
    @Column(name='contact_note')
    String contactNote
    
    String password
    
    @Column(name='grant_email')
    boolean grantEmail
    @Column(name='grant_phone')
    boolean grantPhone
    @Column(name='grant_non')
    boolean grantNon
    
    @Column(name='business_case')
    boolean referenceBusinessCase
    
    boolean signon
    @Column(name='signon_date')
    Date signonDate
    @Column(name='signon_by')
    String signonBy
    @Column(name='signon_key')
    String signonKey
    @Column(name='signon_status')
    String signonStatus

    String uid
    
    @Column(name='payment_id')
    Long paymentId
    
    @Column(name='shipping_id')
    Long shippingId

    @Column(name='remoteip')
    String remoteIp
    
    @Column(name='remoteip_last')
    String remoteIpLast

    @Column(name='remoteip_last_date')
    Date remoteIpLastDate
}
