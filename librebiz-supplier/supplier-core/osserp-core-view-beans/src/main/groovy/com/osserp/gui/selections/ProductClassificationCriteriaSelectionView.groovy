/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Apr 5, 2011 11:29:40 AM 
 * 
 */
package com.osserp.gui.selections

import com.osserp.core.products.ProductCategoryManager
import com.osserp.core.products.ProductGroupManager
import com.osserp.core.products.ProductTypeManager
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
class ProductClassificationCriteriaSelectionView extends AbstractSelectionView {

    private final String TYPE = 'type'
    private final String GROUP = 'group'
    private final String CATEGORY = 'category'

    def activeMode = TYPE

    ProductClassificationCriteriaSelectionView() {
        super()
        headerName = 'selectionProductTypes'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        def mode = form.getString('mode')
        if (mode == GROUP) {
            activeMode = GROUP
            headerName = 'selectionProductGroups'
            list = productGroupManager.list
        } else if (mode == CATEGORY) {
            activeMode = CATEGORY
            headerName = 'selectionProductCategories'
            list = productCategoryManager.list
        } else {
            list = productTypeManager.list
        }

        targetParams.remove('mode')
    }

    private ProductTypeManager getProductTypeManager() {
        getService(ProductTypeManager.class.getName())
    }

    private ProductGroupManager getProductGroupManager() {
        getService(ProductGroupManager.class.getName())
    }

    private ProductCategoryManager getProductCategoryManager() {
        getService(ProductCategoryManager.class.getName())
    }
}
