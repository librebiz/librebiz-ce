/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 29, 2008 8:28:41 AM 
 * 
 */
package com.osserp.core.sales;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.RecordDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrderVolumeExportManager {

    /**
     * Provides all available volume export configs
     * @return configs
     */
    List<SalesOrderVolumeExportConfig> findConfigs();

    /**
     * Provides a config by id
     * @return config
     */
    SalesOrderVolumeExportConfig findConfig(Long id);

    /**
     * Creates a volume export operation
     * @param user
     * @param config
     * @return operation
     * @throws ClientException if config is invalid
     */
    SalesOrderVolumeExportOperation getOperation(Employee user, SalesOrderVolumeExportConfig config) throws ClientException;

    /**
     * Refreshs operation with updated values from backend
     * @param operation
     * @throws ClientException if config is invalid
     */
    void refresh(SalesOrderVolumeExportOperation operation) throws ClientException;

    /**
     * Provides volume invoice archive
     * @param config
     * @return archived invoices
     */
    List<Invoice> getInvoiceArchive(SalesOrderVolumeExportOperation operation);

    /**
     * Provides all open volume invoices
     * @param config
     * @return open inoices
     */
    List<Invoice> getOpenInvoices(SalesOrderVolumeExportOperation operation);

    /**
     * Provides a list of all unreleased orders related to company and customer
     * @param operation
     * @return unreleased
     */
    List<RecordDisplay> getUnreleasedOrders(SalesOrderVolumeExportOperation operation);
}
