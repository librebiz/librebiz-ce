<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${!empty sessionScope.salesCreatorView.target}">
    <c:set var="target" value="${sessionScope.salesCreatorView.target}"/>
    <o:logger write="finalized salesCreatorView found [target=${target}]" level="debug"/>
    <c:remove var="salesCreatorView"/>
    <c:remove var="salesOfferView"/>
    <c:redirect url="${target}"/>
</c:if>
<c:if test="${empty sessionScope.salesOfferView or empty sessionScope.salesOfferView.bean}">
    <o:logger write="view not bound: salesOfferView" level="warn"/>
    <c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesOfferView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="baseUrl" scope="request" value="/salesOffer.do"/>
<c:set var="infoUrl" scope="request" value="/salesOfferInfos.do"/>
<c:set var="discountDisplayUrl" scope="request" value="/salesOfferDiscountDisplay.do"/>
<c:set var="productExitTarget" scope="request" value="salesOfferReload"/>
<c:set var="productUrl" scope="request" value="/salesOfferProducts.do"/>
<c:remove var="salesOfferProductForwardExecuted" scope="session"/>		

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records.css"/>
</style>
</tiles:put>
<tiles:put name="javascript" type="string">
	<script type="text/javascript">
		function displayCreateInfo() {
			$('exitButton').innerHTML = "";
			$('saveButton').innerHTML = "<input type='button' value='<fmt:message key="waitPleaseWithDots"/>'/>";
        }
	</script>
</tiles:put>
<tiles:put name="headline">
    <c:import url="/pages/sales/records/voucher_header.jsp"/>
</tiles:put>

<c:choose>
<c:when test="${recordView.setupMode}">
<tiles:put name="headline_right">
<a href="<c:url value="/salesOffer.do?method=disableSetup"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>
<tiles:put name="content" type="string">
<c:import url="/pages/sales/records/offer_settings.jsp"/>
</tiles:put> 
</c:when>
<c:otherwise>
<tiles:put name="headline_right">
<c:choose>
<c:when test="${recordView.copyMode}">
<a href="<c:url value="/salesOffer.do?method=disableCopyMode"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
</c:when>
<c:otherwise>
<a href="<c:url value="/salesOffer.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
</c:otherwise>
</c:choose>
<c:if test="${!recordView.copyMode and !empty sessionScope.businessCaseView and !sessionScope.businessCaseView.request.closed}">
<o:permission role="executive,executive_sales,sales_revenue" info="permissionPostCalculation">
<v:ajaxLink linkId="salesRevenueView" url="/sales/salesRevenue/forward?id=${record.id}"><o:img name="moneyIcon"/></v:ajaxLink>
</o:permission>
</c:if>
<c:if test="${!empty recordView.businessTemplates}">
<v:ajaxLink linkId="businessTemplateDocumentView" url="/requests/requestTemplateDocument/forward?record=${record.id}"><o:img name="letterIcon"/></v:ajaxLink>
</c:if>
<c:if test="${!record.unchangeable and !empty record.items}">
<v:link url="/records/recordPrint/release?view=salesOfferView" confirm="true" message="confirmRecordRelease" title="recordRelease"><o:img name="enabledIcon" /></v:link>
</c:if>
<v:link url="/records/recordPrint/print?view=salesOfferView" title="documentPrint"><o:img name="printIcon" /></v:link>
<c:if test="${record.unchangeable && !recordView.copyMode}">
<v:link url="/records/recordMail/forward?view=salesOfferView&exit=/salesOffer.do?method=reload" title="sendViaEmail"><o:img name="mailIcon" /></v:link>
</c:if>
<c:if test="${!record.unchangeable and !recordView.copyMode}">
<c:set var="ignoreItemSupport" scope="request" value="true"/>
<c:if test="${!empty sessionScope.businessCaseView and sessionScope.businessCaseView.request.type.recordByCalculation}">
<a href="<c:url value="/salesOfferCalculationForward.do"/>" title="<fmt:message key="title.offer.recalculate"/>"><o:img name="calcIcon"/></a>
</c:if>
<a href="<c:url value="/salesOffer.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon"/></a>
</c:if>
<c:if test="${!record.closed}"> 
<o:permission role="executive,executive_sales,create_offer_copy" info="createOfferCopy">
<c:choose>
	<c:when test="${recordView.createCopyByCalculationAvailable}">
		<a href="<c:url value="/salesOffer.do?method=enableCopyMode"/>" title="<fmt:message key="createByCopy"/>"><o:img name="copyIcon"/></a>
	</c:when>
	<c:when test="${recordView.createCopyAvailable}">
		<a href="<c:url value="/salesOffer.do?method=createCopy"/>" title="<fmt:message key="createByCopy"/>"><o:img name="copyIcon"/></a>
	</c:when>
</c:choose>
</o:permission>
<c:if test="${record.contact.discountEnabled or record.rebateAvailable 
				or (!record.historical and record.unchangeable)}">
	<a href="<c:url value="/salesOffer.do?method=enableSetup"/>" title="<fmt:message key="settings"/>"><o:img name="configureIcon" /></a>
</c:if>
</c:if>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                    
                    <c:import url="/pages/records/record_discount_confirmation.jsp"/>
                            
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
                    <c:if test="${recordView.copyMode}">
                    <div class="column100">
                        <o:form name="recordForm" url="/salesOffer.do">
                        <input type="hidden" name="method" value="createCopy" />
                        <div class="recordLabelValue">
                            <span style="margin-right: 20px;"><fmt:message key="copyOfferInputPrompt"/></span>
                            <input type="text" name="name" value="" style="width: 320px;" />
                            <span style="margin-left: 20px;"><input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="createCopy" onclick="setMethod('createOfferCopy');" class="bigicon" title="<fmt:message key="save"/>"/></span>
                        </div>
                        </o:form>
                    </div>
                    </c:if>
                    <div class="column66l">
                        <c:if test="${!empty record.validUntil}">
                        <div class="recordLabelValue">
                            <fmt:message key="validUntil"/> <fmt:formatDate value="${record.validUntil}"/>
                        </div>
                        </c:if>
                        <c:import url="/pages/records/record_sigs_display.jsp"/>
                        <c:import url="/pages/records/record_payment_display.jsp"/>
                        <c:import url="/pages/records/record_discount_display.jsp"/>
                        <c:import url="/pages/records/record_shipping_display.jsp"/>
                        <c:import url="/pages/records/record_embedded_text.jsp"/>
                        <c:if test="${!record.type.displayNotesBelowItems}">
                            <c:import url="/pages/records/record_note_display.jsp"/>
                        </c:if>
                    </div>
                    <div class="column33r">
                        <c:import url="/pages/records/record_status.jsp"/>
                        <%-- column was removed, see offer_edit for details
                        <c:import url="/pages/records/record_delivery_date_display.jsp"/>
                        --%>
                        <c:if test="${!empty record.calculationInfo}">
                        <div class="recordLabelValue">
                            <fmt:message key="calculationInfo"/>: <span style="padding-left: 20px;"><o:out value="${record.calculationInfo}"/></span>
                        </div>
                        </c:if>
                        <c:if test="${record.canceled and !recordView.createCopyByCalculationAvailable and !empty sessionScope.businessCaseView and !sessionScope.businessCaseView.request.closed}">
                        <o:permission role="executive,executive_sales,create_offer_copy" info="createOfferCopy">
                        <div class="recordLabelValue">
                            <a href="<c:url value="/salesOffer.do?method=createCopy"/>">
                                <fmt:message key="createByCopy"/>
                            </a>
                        </div>
                        </o:permission>
                        </c:if>
                        <c:if test="${!record.canceled and record.unchangeable and !empty sessionScope.businessCaseView and !(sessionScope.businessCaseView.request.type.interestContext) and !(sessionScope.businessCaseView.request.closed)}">
                        <div class="recordLabelValue">
                            <v:ajaxLink linkId="salesCreatorView" url="/sales/salesCreator/forward?id=${record.id}">
                                <fmt:message key="salesConclusion"/>
                            </v:ajaxLink>
                        </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
 
        <c:import url="/pages/records/record_items.jsp"/>
        <c:import url="/pages/records/record_options.jsp"/>
        <c:if test="${record.type.displayNotesBelowItems}">
            <c:import url="/pages/records/record_note_display.jsp"/>
        </c:if>
        <c:import url="/pages/records/record_conditions.jsp"/>
    </div>
</div>
</tiles:put> 
</c:otherwise>
</c:choose>
</tiles:insert>
<c:remove var="recordView" scope="session"/>
