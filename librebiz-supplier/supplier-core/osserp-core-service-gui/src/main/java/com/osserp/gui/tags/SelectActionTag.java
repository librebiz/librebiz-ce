/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 31, 2004 
 * 
 */
package com.osserp.gui.tags;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.beanutils.PropertyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Option;
import com.osserp.common.web.Actions;
import com.osserp.common.web.tags.AbstractBeanTag;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SelectActionTag extends AbstractBeanTag {
    private static final long serialVersionUID = 42L;
    private static Logger log = LoggerFactory.getLogger(SelectActionTag.class.getName());

    /** The action to execute if a selection is performed */
    protected String action = null;

    /**
     * Returns the action
     * @return action.
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the action
     * @param action
     */
    public void setAction(String action) {
        this.action = action;
    }

    /** The form where we retrieve the selected option */
    protected String form = null;

    /**
     * Returns the form
     * @return form.
     */
    public String getForm() {
        return form;
    }

    /**
     * Sets the form
     * @param form
     */
    public void setForm(String form) {
        this.form = form;
    }

    /** The value of the form where we retrieve the selected option */
    protected String value = null;

    /**
     * Returns the value
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /** The key under wich the list is stored */
    protected String list = null;

    /**
     * Returns the list
     * @return list.
     */
    public String getList() {
        return list;
    }

    /**
     * Sets the list
     * @param list
     */
    public void setList(String list) {
        this.list = list;
    }

    /** The scope where the list is stored */
    protected String listScope = null;

    /**
     * Returns the listScope
     * @return listScope.
     */
    public String getListScope() {
        return listScope;
    }

    /**
     * Sets the listScope
     * @param listScope
     */
    public void setListScope(String listScope) {
        this.listScope = listScope;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            String output = getOutput();
            out.print(output);
        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (SKIP_BODY);
    }

    private String getOutput() throws JspException {
        String context =
                ((HttpServletRequest) pageContext.getRequest()).getContextPath();
        Object listObj = null;
        List optionList = null;
        Object bean = null;

        listObj = getObject(list, listScope);
        if (listObj == null) {
            throw new JspException("getOutput() no bean found under " + list);
        }
        if (listObj instanceof List) {
            optionList = (List) listObj;
        } else {
            throw new JspException("getOutput() you must specifiy a property if" +
                    " object under name is no list!");
        }

        bean = getObject();
        if (bean == null) {
            throw new JspException("getOutput() no bean found [name=" + name + "]");
        }
        if (property == null && !(bean instanceof Long)) {
                throw new JspException("getOutput() you must set an instance" +
                        " of Long if property not provided");
        } 
        try {
            Long lvalue = (Long) PropertyUtils.getProperty(bean, property);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getOutput() failed [property=" + property + "]");
            }
            throw new JspException("getOutput() you must specifiy a property " +
                    "where we retrieve the selected value!");
        }

        StringBuffer buffer = new StringBuffer(196);
        buffer.append(getStartTag());
        for (Iterator i = optionList.iterator(); i.hasNext();) {
            Option opt = (Option) i.next();
            if (opt.getId().equals(value)) {
                buffer.append(getOptionTag(context, opt, true));
            } else {
                buffer.append(getOptionTag(context, opt, false));
            }
        }
        buffer.append("</select>");
        return buffer.toString();
    }

    private StringBuffer getStartTag() {
        StringBuffer buffer = new StringBuffer(128);
        buffer
                .append("<select name=\"")
                .append(getFormValue())
                .append("\" size=\"1\"");
        buffer.append(renderStyles());
        buffer.append(" onChange=\"gotoUrl(value);\">");
        return buffer;
    }

    private String getOptionTag(String contextPath, Option o, boolean selected) {
        StringBuffer buffer = new StringBuffer(96);
        buffer
                .append("<option value=\"")
                .append(contextPath)
                .append(action)
                .append("?")
                .append(Actions.ID)
                .append("=")
                .append(o.getId())
                .append("\"");
        if (selected) {
            buffer.append(" selected=\"selected\"");
        }
        buffer
                .append(">")
                .append(o.getName())
                .append("</option>");
        return buffer.toString();
    }

    public String getFormValue() {
        if (value == null) {
            return "selectAction";
        }
        return value;
    }

    @Override
    public void release() {
        super.release();
        this.action = null;
        this.form = null;
        this.list = null;
        this.listScope = null;
    }

}
