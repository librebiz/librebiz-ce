/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Jan-2007 12:56:50 
 * 
 */
package com.osserp.core.service.records;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

import com.osserp.core.calc.Calculation;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.records.SalesOffers;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.Record;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesOfferManager;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;
import com.osserp.core.tasks.RequestListCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOfferManagerImpl extends AbstractOfferManager implements SalesOfferManager {
    private static Logger log = LoggerFactory.getLogger(SalesOfferManagerImpl.class.getName());
    private Requests requests = null;
    private RequestListCacheSender requestListCache = null;

    public SalesOfferManagerImpl(
            SalesOffers salesOffers,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            Requests requests,
            RequestListCacheSender requestListCacheSender) {
        super(salesOffers, systemConfigManager, products, productPlanningTask);
        this.requests = requests;
        this.requestListCache = requestListCacheSender;
    }

    public Offer findActivated(Request request) {
        if (log.isDebugEnabled()) {
            log.debug("findActivated() invoked for request " + request.getRequestId());
        }
        return getOffersDao().findActivated(request);
    }

    public Offer findByCalculation(Calculation calc) {
        assert (calc != null);
        SalesOffers offers = getOffersDao();
        return offers.findByCalculation(calc);
    }

    public Offer getByCalculation(Calculation calc) throws ClientException {
        Offer offer = findByCalculation(calc);
        if (offer == null) {
            log.error("getCalculation() no related offer found!");
            throw new ClientException(ErrorCode.CALCULATION_INCOMPLETE);
        }
        return offer;
    }

    public Offer findWithoutCalculation(Request request) {
        assert (request != null);
        if (log.isDebugEnabled()) {
            log.debug("findWithoutCalculation() invoked [request="
                    + request.getPrimaryKey() + "]");
        }
        return getOffersDao().findWithoutCalculation(request.getPrimaryKey());
    }

    public List<Offer> findByReference(Employee user, Request request) throws ClientException {
        if (request.getCustomer().getPaymentAgreement() == null && request.getType().isWholeSale()) {
            throw new ClientException(ErrorCode.CUSTOMER_PAYMENT_AGREEMENT_MISSING);
        }
        List<Offer> result = getOffersDao().getByRequest(request);
        if (result.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("findByReference() did not find existing offers [request=" + request.getRequestId()
                        + ", status=" + request.getStatus()
                        + "]");
            }
            result.add(getOffersDao().create(request, user));
        }
        return result;
    }

    public int getReferencedCount(Request request) {
        return getOffersDao().count(request.getPrimaryKey());
    }

    public Offer create(Employee user, Request request, Calculation calculation) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [calculation=" + calculation.getId() + "]");
        }
        return getOffersDao().create(request, calculation, user);
    }

    public Offer create(Employee user, Offer offer) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [offer=" + (offer == null ? "null" : offer.getId()) + "]");
        }
        return getOffersDao().create(user, offer);
    }

    public void update(Employee user, Request request, Calculation calculation) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("update() invoked [calculation=" + calculation.getId() + "]");
        }
        SalesOffers offers = getOffersDao();
        SalesOffer offer = (SalesOffer) offers.findByCalculation(calculation);
        if (offer != null) {
            if (log.isDebugEnabled()) {
                log.debug("update() offer found [id=" + offer.getId() + "]");
            }
            offer.updateByCalculation(calculation);
            getDao().save(offer);
        } else {
            offer = (SalesOffer) create(user, request, calculation);
        }
    }

    public void updateCalculationInfo(Employee user, Calculation calculation) {
        SalesOffers offers = getOffersDao();
        SalesOffer offer = (SalesOffer) offers.findByCalculation(calculation);
        if (offer != null) {
            if (log.isDebugEnabled()) {
                log.debug("updateCalculationInfo() offer found [calculation="
                        + calculation.getId() + ", offer=" + offer.getId() + "]");
            }
            offer.setCalculationInfo(calculation.getName());
            persist(offer);
        }
    }

    public boolean unreleasedOfferExisting(Request request) {
        List<Offer> result = getOffersDao().getByRequest(request);
        for (int i = 0, j = result.size(); i < j; i++) {
            Offer offer = result.get(i);
            if (!offer.isUnchangeable()) {
                return true;
            }
        }
        return false;
    }

    public Calculation getCalculation(Offer offer) {
        return getOffersDao().getCalculation(offer);
    }

    public void cancel(Employee user, Request request) {
        if (getReferencedCount(request) > 0) {
            List<Offer> existing = getOffersDao().getByRequest(request);
            if (!existing.isEmpty()) {
                for (Iterator<Offer> i = existing.iterator(); i.hasNext();) {
                    Offer next = i.next();
                    try {
                        updateStatus(next, user, Record.STAT_CANCELED);
                        if (log.isDebugEnabled()) {
                            log.debug("cancel() done [id=" + next.getId() + "]");
                        }
                    } catch (ClientException e) {
                        // we ignore this here
                        log.warn("cancel() failed on status update [id="
                                + next.getId() + ", status="
                                + Record.STAT_CANCELED
                                + ", message="
                                + e.getMessage()
                                + "]");
                    }
                }
            }
        }
    }

    public void resetCancelled(Employee user, Request request) {
        if (getReferencedCount(request) > 0) {
            List<Offer> result = getOffersDao().getByRequest(request);
            for (int i = 0, j = result.size(); i < j; i++) {
                Offer offer = result.get(i);
                if (offer.isCanceled()) {
                    offer.markChanged();
                    offer.setChangedBy(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
                    getOffersDao().save(offer);
                    if (log.isDebugEnabled()) {
                        log.debug("resetCancel() done [id=" + offer.getId() + "]");
                    }
                }
            }
        }
    }

    public void setHistorical(SalesOffer offer) {
        getOffersDao().deselectOffer(offer);
    }

    @Override
    protected void doAfterStatusUpdate(Record record, Employee employee, Long oldStatus) {
        try {
            if (record.getReference() != null && requests.exists(record.getReference())) {
                Request request = requests.load(record.getReference());
                request.setCapacity(getOffersDao().getAveragePlantPower(record.getReference()));
                if (isNotSet(record.getShippingId()) && isSet(request.getShippingId())) {
                    request.setShippingId(null);
                } else if (isSet(record.getShippingId())) {
                    request.setShippingId(record.getShippingId());
                }
                requests.save(request);
            }
        } catch (Exception e) {
            log.warn("doAfterStatusUpdate() failed on attempt to set average plant capacity [record="
                    + record.getId() + ", request=" + record.getReference() + ", message=" + e.getMessage()
                    + "]", e);
        }
        try {
            requestListCache.send(record.getId(), "salesOffer");
        } catch (Exception e) {
            log.warn("doAfterStatusUpdate() failed on attempt to refresh request lists [record="
                    + record.getId() + ", request=" + record.getReference() + ", message=" + e.getMessage()
                    + "]");
        }

    }

    protected void doAfterPrintValidation(Record record, Long nextStatus) throws ClientException {
        SalesOffer offer = (SalesOffer) record;
        if (offer.getPaymentAgreement() == null) {
            throw new ClientException(ErrorCode.PAYMENT_AGREEMENT_MISSING);
        }
        if (offer.getPaymentAgreement().getPaymentCondition() == null) {
            throw new ClientException(ErrorCode.PAYMENT_CONDITION_MISSING);
        }
    }

    private SalesOffers getOffersDao() {
        return (SalesOffers) getDao();
    }
}
