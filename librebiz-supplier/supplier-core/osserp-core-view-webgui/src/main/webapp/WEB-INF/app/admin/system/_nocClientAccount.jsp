<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="directoryConfig"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <c:choose>
                    <c:when test="${!empty directoryClientStatus}">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status"><fmt:message key="status"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${view.directoryClientStatus}"/>
                                </div>
                            </div>
                        </div>

                    </c:when>
                    <c:otherwise>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="description"><fmt:message key="description"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${view.directoryClient.values['description']}"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="contactAddress"><fmt:message key="contactAddress"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${view.directoryClient.values['mail']}"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="directoryNumber"><fmt:message key="directoryNumber"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${view.directoryClient.values['gidnumber']}"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="defaultGroup"><fmt:message key="defaultGroup"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${view.directoryClient.values['clientgroupdefault']}"/>
                                </div>
                            </div>
                        </div>

                        <c:if test="${view.client.directorySupport}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="customerNo"><fmt:message key="customerNo"/></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <o:out value="${view.directoryClient.values['osserpcustomer']}"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="accessData"><fmt:message key="accessData"/></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <v:link url="/admin/system/credentialConfig/forward?context=${view.credentialContext}&name=${view.client.name}&exit=/admin/system/nocClient/reload">
                                            <c:choose>
                                                <c:when test="${view.clientCredentialsAvailable}"><fmt:message key="configured"/></c:when>
                                                <c:otherwise><fmt:message key="notConfigured"/></c:otherwise>
                                            </c:choose>
                                        </v:link>
                                    </div>
                                </div>
                            </div>
                        </c:if>

                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
