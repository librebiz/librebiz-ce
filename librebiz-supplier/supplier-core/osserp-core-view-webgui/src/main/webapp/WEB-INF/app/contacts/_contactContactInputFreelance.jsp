<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="${sessionScope.contactInputViewName}" />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="owner" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <oc:select name="spouseSalutation" options="salutations" styleClass="form-control" prompt="false" value="${view.bean.salutation.id}" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <oc:select name="spouseTitle" options="titles" styleClass="form-control" prompt="false" value="${view.bean.title.id}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="firstName" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="spouseFirstName" styleClass="form-control" value="${view.bean.spouseFirstName}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="lastName" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="spouseLastName" styleClass="form-control" value="${view.bean.spouseLastName}" />
        </div>
    </div>
</div>
