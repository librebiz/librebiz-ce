/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.telephone.TelephoneGroupManager
import com.osserp.core.telephone.TelephoneSystem
import com.osserp.core.telephone.TelephoneSystemManager

import com.osserp.gui.CoreView

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneGroupCreateView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TelephoneGroupCreateView.class.getName())

    private static final String TELEPHONE_GROUP_CREATE_PERMISSIONS = 'telephone_system_admin'
    String groupname
    Long system
    List<TelephoneSystem> telephoneSystems = []

    TelephoneGroupCreateView() {
        enablePopupView()
        dependencies = ['telephoneGroupView']
        headerName = 'createNewTelephoneGroup'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!telephoneSystems) {
                telephoneSystems = telephoneSystemManager.getAll()
            }
        }
    }

    @Override
    void save() {
        bean = null
        this.checkPermission(TELEPHONE_GROUP_CREATE_PERMISSIONS)
        groupname = form.getString('name')
        system = form.getLong('system')
        if (!groupname) {
            throw new ClientException(ErrorCode.NAME_MISSING)
        } else if (!system) {
            throw new ClientException(ErrorCode.NO_TELEPHONE_SYSTEM_SET)
        }
        try {
            bean = telephoneGroupManager.create(user.id, groupname, system)
        } catch (ClientException t) {
            throw new ClientException(t.message)
        }
        if (bean) {
            env?.telephoneGroupView?.reload()
        }
    }

    private TelephoneGroupManager getTelephoneGroupManager() {
        getService(TelephoneGroupManager.class.getName())
    }

    private TelephoneSystemManager getTelephoneSystemManager() {
        getService(TelephoneSystemManager.class.getName())
    }
}
