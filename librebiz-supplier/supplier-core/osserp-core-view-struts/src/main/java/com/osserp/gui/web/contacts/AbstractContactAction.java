/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Oct-2004 
 * 
 */
package com.osserp.gui.web.contacts;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.gui.client.contacts.ContactSettingsView;
import com.osserp.gui.client.contacts.ContactView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractContactAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(AbstractContactAction.class.getName());

    @Override
    protected Class<? extends ContactView> getViewClass() {
        return ContactView.class;
    }

    protected ContactView fetchContactView(HttpSession session) {
        return (ContactView) fetchView(session, ContactView.class);
    }

    protected ContactView getContactView(HttpSession session) {
        ContactView view = fetchContactView(session);
        if (view == null) {
            log.warn("getContactView() failed, view not bound [class=" + ContactView.class.getName() + "]");
            throw new ActionException("view not bound");
        }
        return view;
    }

    protected ContactSettingsView fetchContactSettingsView(HttpSession session) {
        return (ContactSettingsView) fetchView(session, ContactSettingsView.class);
    }

    protected ContactSettingsView getContactSettingsView(HttpSession session) {
        ContactSettingsView view = fetchContactSettingsView(session);
        if (view == null) {
            log.warn("getContactView() failed, view not bound [class=" + ContactSettingsView.class.getName() + "]");
            throw new ActionException("view not bound");
        }
        return view;
    }
}
