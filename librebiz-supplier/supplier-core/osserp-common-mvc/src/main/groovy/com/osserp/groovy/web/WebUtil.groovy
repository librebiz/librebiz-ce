/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web

import javax.servlet.ServletContext
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.web.context.WebApplicationContext

import com.osserp.common.Option
import com.osserp.common.util.DateFormatter
import com.osserp.common.util.NumberFormatter
import com.osserp.common.util.ReflectionUtil
import com.osserp.common.web.Globals
import com.osserp.common.web.WebConfig

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class WebUtil {
    private static Logger logger = LoggerFactory.getLogger(WebUtil.class.getName())
    public static final String VIEW_SERVLET_NAME = 'viewServletName'
    public static final String VIEW_SERVLET_HOME = 'viewServletHome'

    /**
     * 
     * Provides the application context.
     * @param servletContext
     * @param servletName
     * @return webApplicationContext
     */
    static final WebApplicationContext getWebApplicationContext(ServletContext servletContext, String servletName) {
        servletContext.getAttribute("org.springframework.web.servlet.FrameworkServlet.CONTEXT.${servletName}")
    }
    
    /**
     * Provides current view if available
     * @param request
     * @return current view or null if not exists
     */
    static final View fetchCurrentView(HttpServletRequest request) {
        request.getAttribute(View.CURRENT)
    }
    
    /**
     * Provides current view if available
     * @param session
     * @return current view or null if not exists
     */
    static final View fetchCurrentView(HttpSession session) {
        session.getAttribute(View.CURRENT)
    }

    /**
     * Stores a view as current view
     * @param request scope
     * @param view to save
     */
    static final def setCurrentView(HttpServletRequest request, View view) {
        request.setAttribute(View.CURRENT, view)
    }

    /**
     * Provides the name of the view by controller name
     * @return name of associated view based on convention
     */
    static final String getViewName(Class clazz) {
        def	name = clazz.getSimpleName()
        name = "${name.substring(0,1).toLowerCase()}${name.substring(1, name.length())}"
        def cl = name.indexOf('Controller')
        if (cl > -1) {
            name = name.substring(0, cl)
        }
        name.indexOf('View') > -1 ? name : "${name}View"
    }

    /**
     * Fetches view by request
     * @return view or null if none available
     */
    static final View fetchView(HttpServletRequest request, Class clazz) {
        request.getSession().getAttribute(getViewName(clazz))
    }

    /**
     * Gets responsible view by request
     * @return view
     * @throws ViewException if view was not found
     */
    static final View getView(HttpServletRequest request, Class clazz) throws ViewException {
        def view = fetchView(request, clazz)
        if (!view) {
            logger.warn("view not bound: ${clazz.getName()}")
            throw new ViewException("view not bound: ${clazz.getName()}")
        }
        return view
    }

    /**
     * Fetches view by request
     * @return view or null if none available
     */
    static View fetchView(HttpServletRequest request, String viewName) {
        def name = viewName.indexOf('View') > -1 ? viewName : "${viewName}View"
        request.getSession().getAttribute(name)
    }

    /**
     * Gets responsible view by request
     * @return view
     * @throws ViewException if view was not found
     */
    static View getView(HttpServletRequest request, String viewName) throws ViewException {
        def view = fetchView(request, viewName)
        if (!view) {
            logger.warn("view not bound: $viewName")
            throw new ViewException("view not bound: $viewName")
        }
        return view
    }

    /**
     * Fetches OutputView by request
     * @return view or null if none available
     */
    static final OutputView fetchOutputView(HttpServletRequest request, Class clazz) {
        request.getSession().getAttribute(getViewName(clazz))
    }

    /**
     * Gets responsible OutputView by request
     * @return view
     * @throws ViewException if view was not found
     */
    static final OutputView getOutputView(HttpServletRequest request, Class clazz) throws ViewException {
        def view = fetchOutputView(request, clazz)
        if (!view) {
            logger.warn("view not bound: ${clazz.getName()}")
            throw new ViewException("view not bound: ${clazz.getName()}")
        }
        return view
    }

    /**
     * Provides the view home path.   
     * @param servletContext 
     * @return viewHome, e.g. '/index'
     */
    static String getViewHome(ServletContext servletContext) {
        servletContext.getAttribute(VIEW_SERVLET_HOME)
    }

    /**
     * Provides the view path. The view path is defined as webapp root path + view servlet path  
     * @param servletContext 
     * @param request 
     * @return viewPath, e.g. '/web/app' if webapp root is '/web' and viewServletName is 'app' for example
     */
    static String getViewPath(ServletContext servletContext, HttpServletRequest request) {
        def appPath = ""
        if (servletContext.getAttribute(VIEW_SERVLET_NAME)) {
            appPath = "/${servletContext.getAttribute(VIEW_SERVLET_NAME)}"
        }
        "${request.contextPath}${appPath}"
    }

    /**
     * Provides the path of the view servlet. The view servlet path defines a dedicated path
     * if view servlet is not running under webapp-root directly.   
     * View servlet path is expected to be available as context attribute 'viewServletName' to take effect.
     * @param servletContext 
     * @return viewServletPath, e.g. '/app' if viewServletName is 'app' for example
     */
    static String getViewServletPath(ServletContext servletContext) {
        def appPath = ""
        if (servletContext.getAttribute(VIEW_SERVLET_NAME)) {
            appPath = "/${servletContext.getAttribute(VIEW_SERVLET_NAME)}"
        }
        return appPath
    }
    
    static final Map getViewParams(String url) {
        def result = [:]
        if (!url) {
            result = [contextPath: '/', method: 'forward', viewName: 'home']
        } else if (url =~ /^\/[a-zA-Z]+[\/[a-zA-Z]+]+/) {
            def lastPos = url.lastIndexOf('/')
            def contextPath = url.substring(0, lastPos)
            def method = url.substring(lastPos+1, url.length())
            def viewName = contextPath.substring(contextPath.lastIndexOf('/')+1, contextPath.length())
            result = [contextPath: contextPath, method: method, viewName: viewName]
        }
        return result
    }

    static String getString(Object obj) {
        if (obj == null) {
            return ''
        }
        if (obj instanceof Date) {
            return DateFormatter.getDate(obj)
        } else if (obj instanceof Double || obj instanceof BigDecimal) {
            return NumberFormatter.getValue(obj, NumberFormatter.CURRENCY)
        } else if (obj instanceof Option) {
            return obj.id
        }
        return obj
    }

    static WebConfig fetchWebConfig(HttpSession session) {
        Object wcfg = session.getServletContext().getAttribute(Globals.WEB_CONFIG)
        if (wcfg instanceof WebConfig) {
            logger.debug('fetchWebConfig: done [found=true]')
            return wcfg
        } else {
            logger.debug('fetchWebConfig: done [found=false]')
        }
        return null
    }

    /**
     * Creates an app link if action is no redirect:<br/>
     * '/actionPath/action' => '/app/actionPath/action'
     * @param request
     * @param action
     * @return app link
     */
    static String createLink(HttpServletRequest request, String action) {
        String appCtxPath = WebUtil.getViewServletPath(request.session.servletContext)
        if (action.startsWith(appCtxPath) || isRedirect(action)) {
            return action
        }
        return "${appCtxPath}${action}"
    }

    /**
     * Creates an app link url with context path if action is no redirect:<br/>
     * '/actionPath/action' => '/ctxname/app/actionPath/action'
     * @param request
     * @param action
     * @return app link url
     */
    static String createUrl(HttpServletRequest request, String action) {
        String url = createLink(request, action)
        if (!url.startsWith(request.contextPath) && request.contextPath.length() > 1) {
            url = "${request.contextPath}${url}"
        }
        return url
    }

    static String redirect(HttpServletRequest request, String action) {
        isRedirect(action) ? action : "redirect:${createLink(request, action)}"
    }

    static Boolean isRedirect(String url) {
        (url?.indexOf('redirect') > -1)
    }

    static Boolean isNativeRequest(String url) {
        (url?.startsWith('http:') || url?.startsWith('https:'))
    }

    /**
     * Detects struts requests. Method is deprecated when struts is removed.
     * @param url
     * @return true if url is struts request
     */
    @Deprecated
    static Boolean isStrutsRequest(String url) {
        int paramsExist = url.indexOf('?')
        int doExists = url.indexOf('.do')
        url.contains('.do') && (paramsExist < 0 || paramsExist > doExists)
    }

    static final String getFormValue(View view, String name, String objectpath, boolean ignoreModes) {
        if (view) {
            logger.debug("getFormValue: view found [name=${view.name}, property=${name}, ignoreModes=${ignoreModes}, editMode=${view.editMode}, createMode=${view.createMode}]")
        } else {
            logger.warn("getFormValue: no view provided [property=${name}]")
            return null
        }
        if (view && (ignoreModes || (view.isEditMode() || view.isCreateMode()))) {

            if (view.form?.params[name]) {
                    
                logger.debug("getFormValue: value found by form param [name=${name}, value=${view.form.params[name]}]")
                return view.form?.params[name]
                   
            } else if (view.bean && objectpath) {
                
                def result = ReflectionUtil.getValue(view.bean, objectpath)
                if (!result || 'null' == result) {
                    return null
                }
                return valueByFetchedObject(result)
                    
            } else if (view.bean && propertyExists(view.bean, name) && view.bean[name] != null) {
                return valueByFetchedObject(view.bean[name])
               
            } else {
                logger.debug('getFormValue: did not find value in form params or bean')
            }
        } else {
            logger.debug('getFormValue: nothing to do, required modes not enabled')
        }
        return null
    }

    static String valueByFetchedObject(Object o) {
        if (o instanceof Option) {
            def result = o.id?.toString()
            logger.debug("valueByFetchedObject: value found by option [result=${result}]")
            if (result) {
                return result
            }
        } else if (o instanceof Date) {
            return DateFormatter.getDate(o)
        }
        return o == null ? null : o.toString()
    }

    static boolean propertyExists(Object bean, String property) {
        try {
            bean[property]
            return true
        } catch (MissingPropertyException e) {
            // don't log possible exception here
        }
        logger.debug("propertyExists: reports false [name=${property}, bean=${bean?.getClass().getName()}")
        false
    }

    static String appendParam(String url, String param, Object value) {
        if (!value) {
            return url
        }
        if (url?.indexOf('?') > -1) {
            return "${url}&${param}=${value}"
        }
        return "${url}?${param}=${value}"
    }
}
