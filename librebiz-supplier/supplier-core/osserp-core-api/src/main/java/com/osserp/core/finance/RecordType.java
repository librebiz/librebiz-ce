/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 4, 2006 9:47:47 AM 
 * 
 */
package com.osserp.core.finance;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordType extends Option {

    static final Long COMMON_PAYMENT = 100L;
    
    static final Long SALES_OFFER = 1L;
    static final Long SALES_ORDER = 2L;
    static final Long SALES_DELIVERY_NOTE = 3L;
    static final Long SALES_DOWNPAYMENT = 4L;
    static final Long SALES_INVOICE = 5L;
    static final Long SALES_PAYMENT = 8L;
    static final Long SALES_CREDIT_NOTE = 9L;
    static final Long SALES_CANCELLATION = 10L;

    static final Long PURCHASE_OFFER = 101L;
    static final Long PURCHASE_ORDER = 102L;
    static final Long PURCHASE_RECEIPT = 103L;
    static final Long PURCHASE_INVOICE = 105L;
    static final Long PURCHASE_RETURN = 106L;
    static final Long PURCHASE_PAYMENT = 108L;
    static final Long PURCHASE_INVOICE_SERVICE = 107L;
    static final Long CORRECTION_OF_RECORD = 111L;

    /**
     * Indicates that additional conditions should be added on create if supportingConditions is enabled
     * @return addConditionsOnCreate
     */
    boolean isAddConditionsOnCreate();

    /**
     * Indicates that additional conditions should be added on create if supportingConditions is enabled
     * @return addConditionsOnCreate
     */
    boolean isAddTermsAndConditionsOnPrint();

    void setAddTermsAndConditionsOnPrint(boolean addTermsAndConditionsOnPrint);

    /**
     * Indicates that rignt-to-cancel document should be added on print
     * @return addRightToCancelOnPrint
     */
    boolean isAddRightToCancelOnPrint();

    void setAddRightToCancelOnPrint(boolean addRightToCancelOnPrint);

    /**
     * Indicates that implementing record supports additional conditions
     * @return supportingConditions
     */
    boolean isSupportingConditions();

    /**
     * Indicates that implementing record relates to type specific bookings
     * @return supportingBookingTypes
     */
    boolean isSupportingBookingTypes();

    /**
     * Indicates support for released document address corrections.
     * @return supportingCorrections
     */
    boolean isSupportingCorrections();

    /**
     * Indicates that implementing record provides a delivery date
     * @return supportingDeliveryDate
     */
    boolean isSupportingDeliveryDate();

    /**
     * Indicates that implementing record provides an item based delivery date
     * @return supportingDeliveryDateOnItems
     */
    boolean isSupportingDeliveryDateOnItems();

    /**
     * Indicates that implementing record provides acknowledged delivery dates
     * @return supportingDeliveryAck
     */
    boolean isSupportingDeliveryAck();

    /**
     * Indicates that record supports changing of delivery date after releasing
     * @return itemsChangeableWhenReleased
     */
    boolean isItemsChangeableWhenReleased();

    /**
     * Indicates that record is product planning aware
     * @return planningProductAware
     */
    boolean isPlanningProductAware();

    /**
     * Indicates that record is stock aware. Stock aware records provide a persistent stock id on item
     * @return stockAware
     */
    boolean isStockAware();

    /**
     * Indicates outpayment support (e.g. creditnotes, cancellations, ...)
     * @return supportingOutpayments
     */
    boolean isSupportingOutpayments();

    /**
     * Indicates payment support (e.g. invoices, downpayments, ...)
     * @return supportingPayments
     */
    boolean isSupportingPayments();

    /**
     * Indicates print support (e.g. create pdf documents)
     * @return supportingPrint
     */
    boolean isSupportingPrint();
    
    /**
     * Indicates if records referencing this type are payments 
     * @return true if payment
     */
    boolean isPayment();

    /**
     * Indicates if record number should be formatted
     * @return formatNumber
     */
    boolean isFormatNumber();

    /**
     * Provides an optional record number prefix
     * @return numberPrefix
     */
    String getNumberPrefix();

    void setNumberPrefix(String numberPrefix);

    /**
     * Indicates if number should be prefixed with number prefix
     * @return true if number should be prefixed
     */
    boolean isAddNumberPrefix();

    void setAddNumberPrefix(boolean addNumberPrefix);

    /**
     * Provides display price permissions
     * @return displayPricePermissions
     */
    String getDisplayPricePermissions();

    /**
     * Sets display price permissions
     * @param displayPricePermissions
     */
    void setDisplayPricePermissions(String displayPricePermissions);

    /**
     * Provides the associated table name
     * @return tableName
     */
    String getTableName();

    /**
     * Indicates if numberCreator provides next id by a sequence.
     * @return numberCreatorBySequence
     */
    boolean isNumberCreatorBySequence();

    /**
     * Indicates if numberCreator is fixed (e.g. not manageable via gui)
     * @return true if numberCreator setting is unchangeable
     */
    boolean isNumberCreatorFixed();

    /**
     * Provides the name of the responsible numberCreator
     * @return numberCreatorName
     */
    String getNumberCreatorName();

    /**
     * Provides the number of digits to append to a number if number is not unique
     * @return uniqueNumberDigitCount
     */
    int getUniqueNumberDigitCount();

    void setUniqueNumberDigitCount(int uniqueNumberDigitCount);

    /**
     * Provides an optional documentType if recordType should support document uploads
     * @return documentTypeId
     */
    Long getDocumentTypeId();
    
    /**
     * Provides the default status to set on record release.
     * @return Record.STAT_SENT by default
     */
    Long getReleasedStatus();

    /**
     * Indicates if record created date could be overidden
     * @return overrideCreatedDate
     */
    boolean isOverrideCreatedDate();

    /**
     * Indicates if notes should be displayed below items
     * @return true if notes should be displayed below.
     */
    boolean isDisplayNotesBelowItems();

    void setDisplayNotesBelowItems(boolean displayNotesBelowItems);

    /**
     * @return true if product datasheets should be added
     */
    boolean isAddProductDatasheet();

    void setAddProductDatasheet(boolean addProductDatasheet);

    /**
     * Changes type of number creator and digits
     * @param user
     * @param name
     * @param digits count of digits to append to byDate generator
     */
    void updateNumberCreator(Long user, String name, int digits);

    /**
     * Provides required permissions to utilize type
     * @return permissions
     */
    String getPermissions();

}
