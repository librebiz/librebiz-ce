/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.crm

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.crm.Campaign
import com.osserp.core.crm.CampaignGroup
import com.osserp.core.crm.CampaignManager
import com.osserp.core.crm.CampaignType

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class AbstractCampaignView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractCampaignView.class.getName())

    List<CampaignGroup> groups = []
    CampaignGroup selectedGroup
    List<CampaignType> types = []
    CampaignType selectedType
    List<String> reachList = []
    boolean reachListAvailable = false

    AbstractCampaignView() {
        super()
        enableCompanyPreselection()
        env.includeEol = false
        reachList << Campaign.REACH_COMPANY_UP
        reachList << Campaign.REACH_BRANCH_UP
        reachList << Campaign.REACH_BRANCH_DOWN
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            groups = campaignManager.findGroups()
            types = campaignManager.findTypes()
            if (multipleClientsAvailable) {
                companies = systemConfigManager.getActiveCompanies()
            }
            if (multipleClientsAvailable || branchSelectionAvailable) {
                reachListAvailable = true
            }
        }
    }

    boolean isGroupSelectionMode() {
        env.groupSelectionMode
    }

    def disableGroupSelectionMode() {
        env.groupSelectionMode = false
    }

    def enableGroupSelectionMode() {
        env.groupSelectionMode = true
    }

    def selectGroup() {
        Long id = form.getLong('id')
        if (id) {
            selectedGroup = groups.find { it.id == id }
        } else {
            selectedGroup = null
        }
        disableGroupSelectionMode()
    }

    boolean isTypeSelectionMode() {
        env.typeSelectionMode
    }

    def disableTypeSelectionMode() {
        env.typeSelectionMode = false
    }

    def enableTypeSelectionMode() {
        env.typeSelectionMode = true
    }

    def selectType() {
        Long id = form.getLong('id')
        if (id) {
            selectedType = types.find { it.id == id }
        } else {
            selectedType = null
        }
        disableTypeSelectionMode()
    }

    MenuItem getToggleDisplayEolLink() {
        navigationLink("/${context.name}/toggleDisplayEol", 'reloadIcon', 'toggleDeactivatedDisplay')
    }

    boolean isDisplayEol() {
        env.includeEol
    }

    void toggleDisplayEol() {
        if (displayEol) {
            env.includeEol = false
        } else {
            env.includeEol = true
        }
    }

    protected CampaignManager getCampaignManager() {
        getService(CampaignManager.class.getName())
    }
}
