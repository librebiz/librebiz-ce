/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 30, 2016 
 * 
 */
package com.osserp.gui.admin.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Parameter

import com.osserp.core.finance.FinanceRecord
import com.osserp.core.finance.RecordConfigManager
import com.osserp.core.finance.RecordNumberConfig
import com.osserp.core.finance.RecordSearch
import com.osserp.core.finance.RecordType

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class RecordTypeConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(RecordTypeConfigView.class.getName())

    Long selectedId
    Long numberSuggestion
    RecordNumberConfig numberConfig
    List<FinanceRecord> latestRecords = []
    List<Parameter> numberConfigs = []
    Parameter selectedNumberConfig
    boolean rightToCancelAvailable
    boolean termsAvailable

    RecordTypeConfigView() {
        super()
        providesEditMode()
        headerName = 'recordTypeConfig'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            rightToCancelAvailable = isSystemPropertyEnabled('recordRightToCancelAvailable')
            termsAvailable = isSystemPropertyEnabled('recordTermsAndConditionsAvailable')
            numberConfigs = recordConfigManager.recordNumberMethods
            Long id = getLong('id')
            if (id) {
                selectedId = id
            }
            reload()
        }
    }

    @Override
    void reload() {
        list = recordConfigManager.recordTypes
        if (selectedId) {
            bean = list.find { it.id == selectedId }
        }
        doAfterSelection()
    }

    @Override
    void select() {
        super.select()
        doAfterSelection()
    }

    void doAfterSelection() {
        if (bean) {
            numberConfig = recordConfigManager.getRecordNumberConfig(recordType, clientByCurrentUser?.id)
            latestRecords = recordSearch.findLatest(recordType.id)
            
            if (numberConfig) {
                logger.debug("doAfterSelection: done [numberConfig=${numberConfig.id}, name=${numberConfig.name}, records=${latestRecords.size()}]")
            } else {
                logger.debug("doAfterSelection: done [numberConfig=none, records=${latestRecords.size()}]")
            }

        } else {
            numberConfig = null
            latestRecords = []
        }
    }

    @Override
    void save() {
        if (bean) {
            recordType.name = getString('name')
            recordType.numberPrefix = getString('numberPrefix')
            recordType.addNumberPrefix = getBoolean('addNumberPrefix')
            recordType.displayNotesBelowItems = getBoolean('displayNotesBelowItems')
            recordType.addTermsAndConditionsOnPrint = getBoolean('addTermsAndConditionsOnPrint')
            recordType.addProductDatasheet = getBoolean('addProductDatasheet')
            recordType.addRightToCancelOnPrint = getBoolean('addRightToCancelOnPrint')
            if ('date' == recordType.numberCreatorName) {
                recordType.uniqueNumberDigitCount = getInteger('uniqueNumberDigitCount')
            }
            bean = recordConfigManager.update(domainUser, recordType)
            disableEditMode()
            reload()
        }
    }

    boolean isNumberConfigMode() {
        env.numberConfigMode
    }

    void disableNumberConfigMode() {
        env.numberConfigMode = false
    }

    void enableNumberConfigMode() {
        env.numberConfigMode = true
    }

    void selectNumberConfig() {
        String selectedName = getString('name')
        if (selectedName) {
            selectedNumberConfig = numberConfigs.find { it.name == selectedName }
            if ('sequence' == selectedNumberConfig) {
                if (!numberConfig?.highestNumber) {
                    numberSuggestion = numberConfig?.defaultNumber 
                }
            }
            
        } else {
            selectedNumberConfig = null
            numberSuggestion = null
        }
    }

    void resetNumberConfigSelection() {
        selectedNumberConfig = null
    }

    void changeNumberConfig() {
        bean = recordConfigManager.updateNumberConfig(
            domainUser, 
            recordType,
            selectedNumberConfig.name,
            getLong('value'))
        resetNumberConfigSelection()
        reload()
    }

    RecordType getRecordType() {
        bean
    }

    protected RecordSearch getRecordSearch() {
        getService(RecordSearch.class.getName())
    }

    protected RecordConfigManager getRecordConfigManager() {
        getService(RecordConfigManager.class.getName())
    }
}
