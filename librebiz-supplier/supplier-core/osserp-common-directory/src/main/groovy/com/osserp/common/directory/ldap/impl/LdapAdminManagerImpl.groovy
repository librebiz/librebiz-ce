/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 */
package com.osserp.common.directory.ldap.impl

import com.osserp.common.directory.ldap.LdapAdminManager
import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.LdapException
import com.osserp.common.directory.ldap.dao.AdminAccounts
import com.osserp.common.directory.ldap.dao.DbmailAccounts
import com.osserp.common.directory.ldap.dao.Organization
import com.osserp.common.directory.ldap.dao.OrganizationalUnits

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class LdapAdminManagerImpl extends LdapManagerImpl implements LdapAdminManager {

    private AdminAccounts adminAccounts
    private DbmailAccounts dbmailAccounts
    private Organization organization
    private OrganizationalUnits organizationalUnits

    protected LdapAdminManagerImpl() {
        super()
    }

    LdapAdminManagerImpl(String configFile) {
        super(configFile)
        initDaos()
    }

    LdapAdminManagerImpl(LdapConfig configInstance) {
        super(configInstance)
        initDaos()
    }

    LdapAdminManagerImpl(LdapManagerImpl otherManger) {
        super(otherManger)
        initDaos()
    }

    void initDaos() {
        adminAccounts = new AdminAccountsImpl(config)
        dbmailAccounts = new DbmailAccountsImpl(config)
        organization = new OrganizationImpl(config)
        organizationalUnits = new OrganizationalUnitsImpl(config)
    }

    Map accountLogin(String uid, String password, String client) throws LdapException {
        adminAccounts.login(uid, password, client)
    }

    Map createAdminAccount(String uid, String password, String client) {
        Map atts = [uid: uid, password: password]
        adminAccounts.create(atts, client)
    }

    List<Map> lookupAdminAccounts(String client) {
        if (client) {
            return adminAccounts.findAll(client)
        }
        List<Map> accounts = adminAccounts.findAll(null)
        List<Map> result = []
        accounts.each {
            if (it.dn.indexOf('ou=clients') < 0) {
                result << it
            }
        }
        return result
    }

    Map lookupAdminAccount(String uid, String client) {
        if (!client) {
            return adminAccounts.find(uid, null)
        }
        List<Map> accounts = lookupAdminAccounts(client)
        Map result = [:]
        accounts.each {
            if (it.cn == uid || it.uid == uid) {
                result = it
            }
        }
        return result
    }

    Map changeAdminAccountPassword(Map account, String password) {
        adminAccounts.changePassword(account, password)
    }

    Map createClient(String name, Long customerId, String description, String mailaddress, boolean mailenabled) {
        Map client = clients.create(name, customerId, description, mailaddress, mailenabled)
        organizationalUnits.create('Domains', name)
        organizationalUnits.create('Groups', name)
        organizationalUnits.create('Services', name)
        organizationalUnits.create('Users', name)
        return client
    }

    Map updateClient(Map client, Map values) {
        clients.update(client, values)
    }

    void deleteClient(Map client) {
        clients.delete(client)
    }

    List<Map> lookupClients() {
        clients.findAll()
    }

    void changeOrganizationPassword(String password) {
        organization.changePassword(password)
    }

    Map organizationLogin(String password) throws LdapException {
        organization.login(password)
    }

    List<Map> lookupOrganizationalUnits(String client) {
        organizationalUnits.findAll(client)
    }

    List<Map> getMaildomains() {
        dbmailAccounts.allMaildomains
    }

    void changeMailhostname(String currentHost, String newHost, String domain) {
        dbmailAccounts.updateMailhost(currentHost, newHost, domain)
    }
}
