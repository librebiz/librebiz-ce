<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.projectsByActionView}" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            .recordNumber {
                width: 60px;
            }
            
            .recordCustomer {
                text-align: left;
                width: 225px;
            }
            
            .recordStatus {
                text-align: right;
                width: 20px;
            }
            
            .recordAmount {
                text-align: right;
                width: 70px;
            }
            
            .recordEmployee {
                text-align: left;
                width: 150px;
            }
            
            .action {
                text-align: right;
                width: 85px;
            }
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <c:if test="${view.byMissingAction}">
            <fmt:message key="without" />
        </c:if>
        <oc:options name="flowControlActions" value="${view.action}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/projectsByAction.do?method=exitList"/>"><o:img name="backIcon" /></a></li>
            <c:if test="${!empty view.list}">
                <li><a href="<c:url value="/projectsByAction.do?method=customerXml"/>" title="<fmt:message key='exportCustomerDataOnly'/>" target="_blank"><o:img name="peopleIcon" /></a></li>
                <li><a href="<c:url value="/projectsByAction.do?method=salesXml"/>" title="<fmt:message key='exportProjectData'/>" target="_blank"><o:img name="tableIcon" /></a></li>
            </c:if>
            <li><v:link url="/index" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="byActionListContent">
            <div class="row">
                <c:choose>
                    <c:when test="${empty view.list}">
                        <div class="recordheader" style="margin-left: 20px;">
                            <p class="errortext">
                                <fmt:message key="adviceNoActualOrdersFound" />
                            </p>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${view.byMissingAction}">
                                <c:set var="titleKey" value="lastFcs" />
                            </c:when>
                            <c:otherwise>
                                <c:set var="titleKey" value="actionAt" />
                            </c:otherwise>
                        </c:choose>
                        <div class="col-lg-12 panel-area">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="recordNumber"><fmt:message key="number" /></th>
                                            <th class="recordCustomer"><fmt:message key="consignment" /></th>
                                            <th class="recordStatus">%</th>
                                            <th class="recordAmount"><fmt:message key="value" /></th>
                                            <th class="recordEmployee"><fmt:message key="sales" /></th>
                                            <th class="recordEmployee"><fmt:message key="projection" /></th>
                                            <th class="action"><span title="<fmt:message key="${titleKey}"/>"><fmt:message key="action" /></span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="project" items="${view.list}" varStatus="s">
                                            <tr>
                                                <td class="recordNumber"><a href="<c:url value="/loadSales.do?target=byActionList&id=${project.id}"/>" title="<o:out value="${project.type.name}"/>"><o:out value="${project.id}" /></a></td>
                                                <td class="recordCustomer"><a href="<c:url value="/loadSales.do?target=byActionList&id=${project.id}"/>" title="<o:out value="${project.type.name}"/>"><o:out value="${project.name}" limit="40" /></a></td>
                                                <td class="recordStatus"><v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${project.id}" linkId="businessCaseDisplayView">
                                                        <o:out value="${project.status}" />
                                                    </v:ajaxLink></td>
                                                <td class="recordAmount"><o:number value="${project.plantCapacity}" format="currency" /></td>
                                                <td class="recordEmployee"><v:ajaxLink linkId="salesDisplayPopupView" url="/employees/employeePopup/forward?id=${project.salesId}">
                                                        <oc:employee value="${project.salesId}" limit="20" />
                                                    </v:ajaxLink></td>
                                                <td class="recordEmployee"><v:ajaxLink linkId="projectorPopupView" url="/employees/employeePopup/forward?id=${project.managerId}">
                                                        <oc:employee value="${project.managerId}" limit="20" />
                                                    </v:ajaxLink></td>
                                                <td class="action"><o:date value="${project.actionCreated}" addcentury="false" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
