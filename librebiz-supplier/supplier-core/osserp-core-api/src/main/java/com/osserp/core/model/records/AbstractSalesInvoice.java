/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Feb-2007 12:37:21 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractSalesInvoice extends AbstractInvoice {

    /**
     * Default constructor required by Serializable
     */
    public AbstractSalesInvoice() {
        super();
    }

    /**
     * Creates a new -order independent- sales invoice
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param customer
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected AbstractSalesInvoice(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            Customer customer,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                customer,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                itemsChangeable,
                itemsEditable);
        if (customer.getPaymentAgreement() != null) {
            if (customer.getPaymentAgreement().isTaxFree()) {
                setTaxFree(true);
                setTaxFreeId(customer.getPaymentAgreement().getTaxFreeId());
            } else {
                setTaxFree(false);
            }
            if (customer.getPaymentAgreement().getPaymentCondition() != null) {
                setPaymentCondition(customer.getPaymentAgreement().getPaymentCondition());
            }
        }
        setInternal(customer.isInternal());
    }

    /**
     * Creates a new sales invoice by order
     * @param id
     * @param type
     * @param order
     * @param createdBy
     */
    protected AbstractSalesInvoice(
            Long id,
            RecordType type,
            Order order,
            Employee createdBy) {
        super(
                id,
                type,
                order,
                createdBy);
    }

    /**
     * Creates a new invoice by other invoice.
     * @param id
     * @param type
     * @param reference
     * @param createdBy
     * @param copyReferenceId indicates if id of reference should be set as reference of new record
     * @param changeable indictes if items should be changeable after create by reference.items 
     */
    protected AbstractSalesInvoice(
            Long id,
            RecordType type,
            Invoice reference,
            Employee createdBy,
            boolean copyReferenceId,
            boolean changeable) {
        super(id, type, reference, createdBy, copyReferenceId, changeable);
    }

    @Override
    protected Payment createPayment(
            Employee user,
            BillingType type,
            BigDecimal amount,
            Date paid,
            Long bankAccountId) {
        return new SalesPaymentImpl(
                this,
                type,
                paid,
                user,
                amount,
                bankAccountId);
    }

    @Override
    protected Payment createCustomPayment(
            Employee user,
            BillingType type,
            BigDecimal amount,
            Date paid,
            String customHeader,
            String note,
            Long bankAccountId) {
        return new SalesPaymentImpl(
                this,
                type,
                paid,
                customHeader,
                note,
                user,
                amount,
                bankAccountId);
    }

    @Override
    public void updateStatus(Long status) {
        Long current = getStatus();
        if (current == null) {
            setStatus(status);
        } else if (Record.STAT_CLOSED.equals(current)
                && Record.STAT_SENT.equals(status)) {
            // an invoice can toggle between higher and lower status
            // in this special case; an invoice es considered as paid if
            // status is closed so there is a need to reset the paid
            // flag in some cases
            setStatus(status);
        } else if (Record.STAT_CANCELED.equals(current)
                && Record.STAT_SENT.equals(status)) {
            setStatus(status);
            setCanceled(false);
        } else {
            super.updateStatus(status);
        }
    }
}
