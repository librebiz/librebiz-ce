/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Nov 1, 2010 10:58:40 AM 
 * 
 */
package com.osserp.gui.reporting.selectors

import javax.servlet.http.HttpServletRequest

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.PopupViewController

/**
 *
 * @author jg <jg@osserp.com>
 * 
 */
@RequestMapping("/reporting/selectors/selectBranchPopup/*")
@Controller class SelectBranchPopupController extends PopupViewController {

    @RequestMapping
    @Override
    def save(HttpServletRequest request) {
        super.save(request)
        reloadParent
    }
}
