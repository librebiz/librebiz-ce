/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 29, 2011 3:19:53 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessType;
import com.osserp.core.calc.CalculationByTemplateConfig;
import com.osserp.core.calc.CalculationTemplate;
import com.osserp.core.calc.CalculationTemplateItem;
import com.osserp.core.dao.CalculationTemplates;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.calc.CalculationByTemplateConfigImpl;
import com.osserp.core.model.calc.CalculationTemplateImpl;
import com.osserp.core.model.calc.CalculationTemplateItemImpl;
import com.osserp.core.products.Product;

/**
 * 
 * @author jg <jg@osserp.com>
 * @author tn <tn@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class CalculationTemplatesDao extends AbstractDao implements CalculationTemplates {
    private static Logger log = LoggerFactory.getLogger(CalculationTemplatesDao.class.getName());

    private String calculationTemplatesTable = null;

    public CalculationTemplatesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        calculationTemplatesTable = getTable(TableKeys.CALCULATION_TEMPLATES);
    }

    public CalculationByTemplateConfig loadCalculationByTemplateConfig(Long id) {
        return (CalculationByTemplateConfig) getCurrentSession().load(CalculationByTemplateConfigImpl.class.getName(), id);
    }

    public void save(CalculationByTemplateConfig config) {
        getCurrentSession().saveOrUpdate(CalculationByTemplateConfigImpl.class.getName(), config);
    }

    public CalculationTemplate load(Long id) {
        return (CalculationTemplate) getCurrentSession().load(CalculationTemplateImpl.class.getName(), id);
    }

    public List<CalculationTemplate> findAll() {
        return getCurrentSession().createQuery("from " + CalculationTemplateImpl.class.getName()).list();
    }

    public CalculationTemplate findByProductAndType(Long productId, Long businessTypeId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
                .append(CalculationTemplateImpl.class.getName())
                .append(" o where o.product.productId = :productid ")
                .append("and o.businessType.id = :businesstype");
        List<CalculationTemplate> list = getCurrentSession().createQuery(hql.toString())
                .setParameter("productid", productId)
                .setParameter("businesstype", businessTypeId)
                .list();
        if (!list.isEmpty()) {
            CalculationTemplate template = list.get(0);
            if (log.isDebugEnabled()) {
                log.debug("findByProductAndType() found [template="
                        + (template == null ? "null" : template.getId())
                        + "]");
            }
            return template;
        }
        return null;
    }

    public CalculationTemplate create(String name, Product product, BusinessType businessType) {
        CalculationTemplate calculationTemplate = new CalculationTemplateImpl(name, product, businessType);
        getCurrentSession().save(calculationTemplate);
        return calculationTemplate;
    }

    public void addProduct(CalculationTemplate calculationTemplate, Product product, String note, Long group) {
        CalculationTemplateItem item = new CalculationTemplateItemImpl(calculationTemplate.getId(), product, note, group);
        calculationTemplate.addItem(item);
        getCurrentSession().saveOrUpdate(calculationTemplate);
    }

    public void removeItem(CalculationTemplate calculationTemplate, Long itemId) {
        calculationTemplate.removeItem(itemId);
        getCurrentSession().saveOrUpdate(CalculationTemplateImpl.class.getName(), calculationTemplate);
    }

    public void toggleOptionalItem(CalculationTemplate calculationTemplate, Long itemId) {
        calculationTemplate.toggleOptionalItem(itemId);
        getCurrentSession().saveOrUpdate(CalculationTemplateImpl.class.getName(), calculationTemplate);
    }

    public void delete(Long id) {
        CalculationTemplate calculationTemplate = load(id);
        getCurrentSession().delete(calculationTemplate);
    }

    public boolean isTemplateAvailable(Long productId, Long businessTypeId) {
        if (log.isDebugEnabled()) {
            log.debug("isTemplateAvailable() invoked [product=" + productId + ", businessType=" + businessTypeId + "]");
        }
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(calculationTemplatesTable).append(" WHERE product_id = ? AND request_type_id = ?");
        return jdbcTemplate.queryForObject(
                sql.toString(),
                new Object[] { productId, businessTypeId },
                new int[] { Types.BIGINT, Types.BIGINT },
                Integer.class) > 0;
    }
}
