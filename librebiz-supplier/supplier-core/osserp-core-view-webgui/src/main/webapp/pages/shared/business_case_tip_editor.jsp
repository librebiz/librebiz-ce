<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:set var="view" value="${sessionScope.businessCaseView}" />
<c:set var="businessCase" value="${sessionScope.businessCaseView.request}" />
<c:choose>
    <c:when test="${view.salesContext}">
        <c:set var="url" value="/sales.do" />
    </c:when>
    <c:otherwise>
        <c:set var="url" value="/requests.do" />
    </c:otherwise>
</c:choose>

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="selection" />
        <fmt:message key="tipProvider" />
    </div>
</div>
<div class="modalBoxData">
    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <o:form name="requestForm" url="${url}">
                <input type="hidden" name="method" value="updateTipConfig" />
                <table class="valueTable">
                    <tbody>
                        <tr>
                            <td><fmt:message key="tipProvider" /></td>
                            <td><o:ajaxSelect selectId="tipId" targetElement="tipConfig_popup">
                                    <option value="<c:url value="${url}"><c:param name="method" value="setTipConfig"/><c:param name="id" value="-1"/></c:url>"><fmt:message key="selection" /></option>
                                    <c:forEach var="tip" items="${view.tips}">
                                        <option value="<c:url value="${url}"><c:param name="method" value="setTipConfig"/><c:param name="id" value="${tip.id}"/></c:url>" <c:if test="${!empty view.selectedTipProvider and view.selectedTipProvider.id == tip.id}">selected="selected"</c:if>><o:out value="${tip.displayName}" /> /
                                            <o:out value="${tip.address.city}" /></option>
                                    </c:forEach>
                                    <c:if test="${!empty businessCase.tip}">
                                        <c:choose>
                                            <c:when test="${view.tipProviderDeleteMode}">
                                                <option selected="selected" value="<c:url value="${url}"><c:param name="method" value="setTipConfig"/><c:param name="id" value="0"/></c:url>"><fmt:message key="delete" /></option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="<c:url value="${url}"><c:param name="method" value="setTipConfig"/><c:param name="id" value="0"/></c:url>"><fmt:message key="delete" /></option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </o:ajaxSelect></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="tipCommission" /></td>
                            <td><c:choose>
                                    <c:when test="${!empty view.selectedTipProvider}">
                                        <input type="text" name="tipCommission" value="<o:number value="${view.selectedTipProvider.tipCommission}" format="currency"/>" class="right" style="width: 170px;" />
                                        <fmt:message key="euro" />
                                        <input type="hidden" name="tipId" value="<o:out value="${view.selectedTipProvider.id}"/>">
                                    </c:when>
                                    <c:otherwise>
                                        <input type="text" name="tipCommission" value="<o:number value="${0}" format="currency"/>" class="right" style="width: 170px;" />
                                        <fmt:message key="euro" />
                                    </c:otherwise>
                                </c:choose></td>
                        </tr>
                        <tr>
                            <td class="row-submit"><input class="cancel" type="button" onclick="ojsCommon.setVisibility('tipConfig_popup', 'hidden');" value="<fmt:message key="exit"/>" /></td>
                            <td class="row-submit"><o:submit /></td>
                        </tr>
                    </tbody>
                </table>
            </o:form>
            <div class="spacer"></div>
        </div>
    </div>
</div>
