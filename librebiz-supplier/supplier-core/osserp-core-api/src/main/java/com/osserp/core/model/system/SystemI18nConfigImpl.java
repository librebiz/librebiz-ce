/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on May 23, 2007 8:55:06 AM 
 * 
 */
package com.osserp.core.model.system;

import java.util.Set;

import com.osserp.common.beans.OptionImpl;

import com.osserp.core.system.SystemI18nConfig;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class SystemI18nConfigImpl extends OptionImpl implements SystemI18nConfig {

    protected SystemI18nConfigImpl() {
        super();
    }

    public Set getLocales() {
        return null;
    }
}
