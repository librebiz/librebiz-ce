SET search_path = osserp, pg_catalog;

INSERT INTO system_properties VALUES (233, 'yearlySequenceIgnorables', NULL, 'java.lang.String', true, false, NULL, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 233, true);

UPDATE system_properties SET pvalue = '0.15.17', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
