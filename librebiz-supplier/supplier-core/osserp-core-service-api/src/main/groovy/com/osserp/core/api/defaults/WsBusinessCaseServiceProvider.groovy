/**
 *
 * Copyright (C) 2012 The original author or authors.
 * 
 * Created on 22.12.2012 
 * 
 */
package com.osserp.core.api.defaults

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WSBusinessCase
import com.osserp.api.WSBusinessCaseService
import com.osserp.api.WSObjectException

import com.osserp.common.ClientException
import com.osserp.common.service.Locator

import com.osserp.core.service.impl.AbstractWebService

/**
 *
 * @author rk <rk@osserp.com>
 *
 */
class WsBusinessCaseServiceProvider extends AbstractWebService implements WSBusinessCaseService {
    private static Logger logger = LoggerFactory.getLogger(WsBusinessCaseServiceProvider.class.getName())

    public WsBusinessCaseServiceProvider(Locator locator) {
        super(locator)
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSBusinessCaseService#fetchBusinessCaseById(java.lang.Long)
     */
    public WSBusinessCase fetchBusinessCaseById(Long businessCaseId) {
        WSBusinessCase result
        try {
            result = wsBusinessCaseService.fetchBusinessCaseById(businessCaseId)
        } catch (Exception e) {
            logger.error("fetchBusinessCaseById: caught exception [businessCaseId=${businessCaseId}, message=${e.message}]", e)
        }
        result
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSBusinessCaseService#getBusinessCaseById(java.lang.Long)
     */
    public WSBusinessCase getBusinessCaseById(Long businessCaseId) throws WSObjectException {
        WSBusinessCase result
        try {
            result = wsBusinessCaseService.getBusinessCaseById(businessCaseId)
        } catch (Exception e) {
            logger.error("getBusinessCaseById: caught exception [businessCaseId=${businessCaseId}, message=${e.message}]", e)
            throw e        
        }
        result
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSBusinessCaseService#createOrUpdate(com.osserp.api.WSBusinessCase)
     */
    public WSBusinessCase createOrUpdate(WSBusinessCase wsBusinessCase) {
        WSBusinessCase result
        try {
            result = wsBusinessCaseService.createOrUpdate(wsBusinessCase)
        } catch (ClientException c) {
            logger.error("createOrUpdate: caught well known exception [id=${wsBusinessCase?.primaryKey}, message=${c.message}]")
        } catch (Exception e) {
            logger.error("createOrUpdate: caught unexpected exception [id=${wsBusinessCase?.primaryKey}, message=${e.message}]", e)
        }
        result
    }

    protected WSBusinessCaseService getWsBusinessCaseService() {
        getService(WSBusinessCaseService.class.getName())
    }

}
