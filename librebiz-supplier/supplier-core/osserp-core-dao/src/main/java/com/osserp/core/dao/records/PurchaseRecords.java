/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 15, 2008 6:06:50 PM 
 * 
 */
package com.osserp.core.dao.records;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.finance.Record;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PurchaseRecords extends Records {

    /**
     * Indicates that provided contact is involved in any record of implementing class' type.
     * @param contact
     * @return true if a record with a relation to provided contact was found
     */
    boolean isContactInvolved(ClassifiedContact contact);
    
    /**
     * Looks up for a record with the provided supplier reference.
     * @param supplier id of the supplier
     * @param supplierReference suppliers record number
     * @return record or null if no such record exists
     */
    Record getBySupplierReference(Long supplier, String supplierReference);
}
