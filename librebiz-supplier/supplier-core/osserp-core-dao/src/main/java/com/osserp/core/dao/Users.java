/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2005 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ActionTarget;
import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.Permission;
import com.osserp.common.User;

import com.osserp.core.contacts.Contact;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Users {

    /**
     * Tries to log in by name and password
     * @param loginName
     * @param password
     * @param remoteIp
     * @return user
     * @throws ClientException if login name and password doesn't match
     */
    User login(String loginName, String password, String remoteIp) throws ClientException;

    /**
     * Creates a new user
     * @param createdBy
     * @param employee
     * @return new created user
     */
    User create(Long createdBy, Employee employee);

    /**
     * Provides a list of all permissions
     * @return permissions
     */
    List<Permission> getPermissions();

    /**
     * Grants a permission to an employee.
     * @param grantBy
     * @param employee
     * @param permission
     * @return user with updated permissions
     */
    User grantPermission(User grantBy, Employee employee, String permission);

    /**
     * Grants a permission to a user.
     * @param grantBy
     * @param user
     * @param permission
     * @return user with updated permissions
     */
    User grantPermission(User grantBy, User user, String permission);

    /**
     * Grants all permissions defined by employee group
     * @param grantBy
     * @param employee
     * @param group
     */
    void grantPermissions(Employee grantBy, Employee employee, EmployeeGroup group);

    /**
     * Revokes a permission from an employee.
     * @param revokedBy
     * @param employee
     * @param permission
     * @return user with updated permissions
     */
    User revokePermission(User revokedBy, Employee employee, String permission);

    /**
     * Revokes a permission from a user.
     * @param revokedBy
     * @param user
     * @param permission
     * @return user with updated permissions
     */
    User revokePermission(User revokedBy, User user, String permission);

    /**
     * Revokes all permissions defined by employee group
     * @param revokedBy
     * @param employee
     * @param group
     */
    void revokePermissions(Employee revokedBy, Employee employee, EmployeeGroup group);

    /**
     * Persists user
     * @param user
     */
    void save(User user);

    /**
     * Tries to find the user by id
     * @return user with given id or null if not exists
     */
    User find(Long id);

    /**
     * Tries to find a user by login name
     * @param loginName
     * @return user
     * @throws ClientException if no user exists
     */
    User find(String loginName);

    /**
     * Tries to fetch a user by login name
     * @param loginName
     * @return user
     * @throws ClientException if no user exists
     */
    User get(String loginName) throws ClientException;

    /**
     * Tries to find a user by employee
     * @param employee
     * @return user account related to employee or null if none exist
     */
    User find(Employee employee);

    /**
     * Tries to find a user by contact
     * @param contact
     * @return account related to employee or null if none exist
     */
    User find(Contact contact);

    /**
     * Provides all active users
     * @return active
     */
    List<User> findActive();

    /**
     * Checks whether a permission is grant to a user
     * @param userId where we lookup for permission
     * @param permission to check
     * @return true if permission is grant
     */
    boolean isPermissionGrant(Long userId, String permission);

    /**
     * Provides a list of available users
     * @return list of id and name values
     */
    List<Option> getLoginNames();

    /**
     * Provides all user names as id / value objects
     * @return names
     */
    List<Option> getNames();

    /**
     * Provides employees user id if exists
     * @param employeeId
     * @return userId
     */
    Long getUserByEmployee(Long employeeId);

    /**
     * Provides all users whose grant given permission
     * @param permission
     * @return users as id and display name values
     */
    List<Option> getUsersByPermission(String permission);

    /**
     * Provides an users related contact id
     * @param userId
     * @return contactId
     */
    Long getContactId(Long userId);

    /**
     * Provides a list of all startup targets
     * @return startupTargets
     */
    List<ActionTarget> getStartupTargets();
    
    /**
     * Updates history record
     * @param user
     * @param host
     */
    void updateHistory(User user, String host);
}
