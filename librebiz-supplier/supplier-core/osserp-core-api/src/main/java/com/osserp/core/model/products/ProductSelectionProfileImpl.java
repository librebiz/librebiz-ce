/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 27, 2009 2:27:35 PM 
 * 
 */
package com.osserp.core.model.products;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionProfile;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSelectionProfileImpl extends AbstractProductSelection implements ProductSelectionProfile {

    private boolean defaultProfile = false;
    private boolean ignoreMixedRecords = false;
    private List<ProductSelectionConfig> productSelectionList = new ArrayList<ProductSelectionConfig>();

    protected ProductSelectionProfileImpl() {
        super();
    }

    public void addProductSelection(ProductSelectionConfig selectionConfig) {
        boolean added = false;
        for (int i = 0, j = productSelectionList.size(); i < j; i++) {
            ProductSelectionConfig next = productSelectionList.get(i);
            if (next.getId().equals(selectionConfig.getId())) {
                added = true;
                break;
            }
        }
        if (!added) {
            this.productSelectionList.add(selectionConfig);
        }
    }

    public void removeProductSelection(Long id) {
        for (Iterator<ProductSelectionConfig> i = productSelectionList.iterator(); i.hasNext();) {
            ProductSelectionConfig next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                break;
            }
        }
    }

    public List<ProductSelectionConfig> getProductSelections() {
        // List is readonly. Adding and removing from list is provided by explicit calls
        // to addProductSelection and removeProductSelection only.
        return new ArrayList<ProductSelectionConfig>(productSelectionList);
    }

    public boolean isDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(boolean defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public boolean isIgnoreMixedRecords() {
        return ignoreMixedRecords;
    }

    public void setIgnoreMixedRecords(boolean ignoreMixedRecords) {
        this.ignoreMixedRecords = ignoreMixedRecords;
    }

    protected List<ProductSelectionConfig> getProductSelectionList() {
        return productSelectionList;
    }

    protected void setProductSelectionList(
            List<ProductSelectionConfig> productSelectionList) {
        this.productSelectionList = productSelectionList;
    }
}
