<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <fmt:message key="deliveryAuthorizationProperty" />
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="checkbox">
                <label for="enablingDeliveryCheckbox"> <v:checkbox name="enablingDelivery" />
                </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <fmt:message key="deliveryCompletedProperty" />
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="checkbox">
                <label for="completesDeliveryCheckbox"> <v:checkbox name="closingDelivery" />
                </label>
            </div>
        </div>
    </div>
</div>
