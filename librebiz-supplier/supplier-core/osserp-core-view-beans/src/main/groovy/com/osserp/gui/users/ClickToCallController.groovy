/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 16, 2011 
 * 
 */
package com.osserp.gui.users

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.groovy.web.PopupViewController
import com.osserp.gui.users.ClickToCallView

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
@RequestMapping("/users/clickToCall/*")
@Controller class ClickToCallController extends PopupViewController {
    private static Logger logger = LoggerFactory.getLogger(ClickToCallController.class.getName())

    /*@RequestMapping
     def selectBranch(HttpServletRequest request) {
     if (logger.isDebugEnabled()) {
     logger.debug("selectBranch invoked...")
     }
     ClickToCallView view = getView(request)
     view?.selectBranch()
     return pageArea
     }*/

    @RequestMapping
    def enableClickToCall(HttpServletRequest request) {
        logger.debug('enableClickToCall invoked...')
        ClickToCallView view = getView(request)
        view?.enableClickToCall()
        reloadParent
    }

    @RequestMapping
    def disableClickToCall(HttpServletRequest request) {
        logger.debug('disableClickToCall invoked...')
        ClickToCallView view = getView(request)
        view?.disableClickToCall()
        reloadParent
    }

    @RequestMapping
    def clickToCall(HttpServletRequest request) {
        logger.debug('clickToCall invoked...')
        ClickToCallView view = getView(request)
        try {
            view?.clickToCall()
        } catch (ClientException t) {
            if (logger.debugEnabled) {
                logger.debug("clickToCall: caught exception [message=${t.message}]")
            }
            request.getSession().setAttribute('error', t.message)
        }
        return pageArea
    }

    @RequestMapping
    def doClickToCall(HttpServletRequest request) {
        logger.debug('doClickToCall invoked...')
        ClickToCallView view = getView(request)
        try {
            view?.doClickToCall()
        } catch (ClientException t) {
            if (logger.debugEnabled) {
                logger.debug("doClickToCall: caught exception [message=${t.message}]")
            }
            request.getSession().setAttribute('error', t.message)
        }
        return pageArea
    }
}
