/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2008 12:36:34 PM 
 * 
 */
package com.osserp.gui.web.contacts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Interest;
import com.osserp.gui.client.contacts.ContactView;
import com.osserp.gui.client.contacts.InterestView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class InterestAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(InterestAction.class.getName());

    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        InterestView view = (InterestView) fetchView(request.getSession());
        view.deleteCurrent();
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward closeByCreateContact(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("closeByCreateContact() request from " + getUser(session).getId());
        }
        InterestView view = getInterestView(session);
        Long interestId = null;
        if (view.getBean() == null) {
            interestId = RequestUtil.getId(request);
        } else {
            interestId = ((Interest) view.getBean()).getId(); 
        }
        return new ActionForward(("/app/contacts/contactCreator/forward?interest=" + interestId), true);
    }

    public ActionForward closeByNewContact(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("closeByNewContact() request from " + getUser(session).getId());
        }
        InterestView view = getInterestView(session);
        Long contactId = RequestUtil.getId(request);
        view.closeByCreateContact(contactId);
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward closeByExistingContact(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("closeByExistingContact() request from " + getUser(session).getId());
        }
        InterestView view = getInterestView(session);
        Long contactId = RequestUtil.getId(request);
        Contact contact = view.closeByExistingContact(contactId);
        ContactView contactView = (ContactView) createView(request, ContactView.class.getName());
        contactView.load(contact);
        contactView.setExitTarget(view.getName());
        return mapping.findForward(contactView.getForwardTarget());
    }

    protected InterestView getInterestView(HttpSession session) {
        InterestView view = (InterestView) fetchView(session);
        if (view == null) {
            throw new ActionException(ActionException.NO_VIEW_BOUND);
        }
        return view;
    }

    @Override
    protected Class getViewClass() {
        return InterestView.class;
    }
}
