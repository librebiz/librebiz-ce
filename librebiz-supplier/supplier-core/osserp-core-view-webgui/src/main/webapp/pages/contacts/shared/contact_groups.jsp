<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-app" prefix="oa" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactView}"/>
<c:set var="contact" value="${contactView.bean}"/>

<table class="valueTable first33">
    <tbody>
        <c:choose>
            <c:when test="${not empty contact.reference}">
                <tr>
                    <td><fmt:message key="group"/></td>
                    <td><a href="<c:url value="/contacts.do"><c:param name="method" value="load"/><c:choose><c:when test="${!empty contactView.exitTarget}"><c:param name="exit" value="${contactView.exitTarget}"/></c:when><c:otherwise><c:param name="exit" value="index"/></c:otherwise></c:choose><c:param name="id" value="${contact.reference}"/></c:url>"><fmt:message key="contactIsContactPerson"/></a></td>
                </tr>
            </c:when>
            <c:otherwise>
                <c:choose>
                    <c:when test="${!contactView.assignMode}">
                        <tr>
                            <td>
                                <o:permission role="contact_assign,employee_assign,employee_activation,client_edit" andTrue="${contactView.assignModeAvailable}" info="permissionEditGroups">
                                    <c:choose>
                                        <c:when test="${!contact.client && !contact.branchOffice && !contact.grantContactNone}">
                                            <a href="<c:url value="/contacts.do?method=enableAssignment"/>"><fmt:message key="groups"/></a>
                                        </c:when>
                                        <c:otherwise><fmt:message key="groups"/></c:otherwise>
                                    </c:choose>
                                </o:permission>
                                <o:forbidden role="contact_assign,employee_assign,employee_activation,client_edit">
                                    <fmt:message key="groups"/>
                                </o:forbidden>
                            </td>
                            <td><oa:contactGroups value="${contactView}"/></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td><fmt:message key="group"/></td>
                            <td><fmt:message key="promptSelect"/></td>
                        </tr>
                        <c:if test="${!user.admin && !contact.customer}">
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="javascript:onclick=confirmLink('<fmt:message key="confirmCustomerCreate"/>','<c:url value="/contactAssign.do?method=assignCustomer"/>');" ><fmt:message key="select.customer"/></a></td>
                            </tr>
                        </c:if>
                        <c:if test="${!user.admin && !contact.supplier}">
                            <tr>
                                <td>&nbsp;</td>
                                <td><a href="javascript:onclick=confirmLink('<fmt:message key="confirmSupplierCreate"/>','<c:url value="/contactAssign.do?method=assignSupplier"/>');" ><fmt:message key="select.supplier"/></a></td>
                            </tr>
                        </c:if>
                        <c:if test="${!contact.employee && !contact.business && !contact.freelance}">
                            <o:permission role="employee_assign,employee_activation,client_edit,hrm" info="permissionAssignEmployee">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><v:link url="/employees/employeeSetup/forward?exit=/contactAssignExit.do"><fmt:message key="employee"/></v:link></td>
                                </tr>
                            </o:permission>
                        </c:if>
                        <tr>
                            <td>&nbsp;</td>
                            <td><a href="<c:url value="/contactAssign.do?method=exit"/>"><fmt:message key="exitAssignment"/></a></td>
                        </tr>
                    </c:otherwise>
                </c:choose>
            </c:otherwise>
        </c:choose>
    </tbody>
</table>
