/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 21, 2008 1:24:28 PM 
 * 
 */
package com.osserp.core.tasks.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.service.impl.AbstractXmlMessageHandler;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.dao.Employees;
import com.osserp.core.directory.DirectoryGroupSynchronizer;
import com.osserp.core.directory.DirectoryUserSynchronizer;
import com.osserp.core.employees.Employee;
import com.osserp.core.tasks.Task;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeUpdateTaskImpl extends AbstractXmlMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(EmployeeUpdateTaskImpl.class.getName());

    private Employees employees = null;
    private DirectoryGroupSynchronizer directoryGroupSynchronizer = null;
    private DirectoryUserSynchronizer directoryUserSynchronizer = null;

    // TODO LDAP revise employee synchronization
    protected EmployeeUpdateTaskImpl(
            Employees employees,
            DirectoryGroupSynchronizer directoryGroupSynchronizer,
            DirectoryUserSynchronizer directoryUserSynchronizer) {
        super();
        this.employees = employees;
        this.directoryGroupSynchronizer = directoryGroupSynchronizer;
        this.directoryUserSynchronizer = directoryUserSynchronizer;
    }

    @Override
    protected void execute(Document xml, byte[] file) {
        Element root = xml.getRootElement();
        String type = root.getAttributeValue("type");
        if (type != null) {
            if (type.equals("user")) {
                synchronizeEmployees(root.getText());
            } else if (type.equals("group")) {
                if (directoryGroupSynchronizer != null) {
                    Long gid = NumberUtil.createLong(root.getText());
                    directoryGroupSynchronizer.synchronize(gid);
                    if (log.isDebugEnabled()) {
                        log.debug("execute() done [id=" + (gid == null ? "all" : gid) + "]");
                    }
                }
            } else {
                log.warn("execute() invalid type specified, aborting [xml="
                        + JDOMUtil.getCompactString(xml) + "]");
            }
        } else {
            log.warn("execute() no type specified, aborting [xml="
                    + JDOMUtil.getCompactString(xml) + "]");
        }
    }

    private void synchronizeEmployees(String idString) {
        Long employeeId = NumberUtil.createLong(idString);
        Employee employee = (employeeId == null) ? null : (Employee) employees.find(employeeId);
        if (employee != null) {
            if (log.isDebugEnabled()) {
                log.debug("synchronizeEmployee() syncing employee [id=" + employee.getId() + "]");
            }

            try {
                directoryUserSynchronizer.synchronizeEmployee(employee);
            } catch (Exception e) {
                log.error("synchronizeEmployee() failed [id=" + employee.getId()
                        + ", message=" + e.getMessage() + "]", e);
            }
        } else {
            // TODO implement
            if (log.isDebugEnabled()) {
                log.debug("synchronizeEmployee() syncing all employees...");
            }
        }
    }
}
