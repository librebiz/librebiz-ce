/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.tasks;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingEvent implements Serializable {
    private Long userId = null;
    private Long timeRecordingId = null;
    private Long timeRecordId = null;
    private Long timeRecordTypeId = null;
    private Date startDate = null;
    private Date stopDate = null;
    private String note = null;
    private Date created = null;
    private boolean update = false;

    /**
     * Default constructor required for object serialization
     */
    protected TimeRecordingEvent() {
        super();
    }

    /**
     * Creates an event for new created events
     * @param userId
     * @param timeRecordingId
     * @param timeRecordTypeId
     * @param startDate
     * @param stopDate
     * @param note
     */
    public TimeRecordingEvent(
            Long userId,
            Long timeRecordingId,
            Long timeRecordTypeId,
            Date startDate,
            Date stopDate,
            String note) {
        super();
        this.userId = userId;
        this.timeRecordingId = timeRecordingId;
        this.timeRecordTypeId = timeRecordTypeId;
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.note = note;
        this.created = new Date(System.currentTimeMillis());
    }

    /**
     * Creates an event for new created events
     * @param userId
     * @param timeRecordingId
     * @param timeRecordId
     * @param timeRecordTypeId
     * @param startDate
     * @param stopDate
     * @param note
     */
    public TimeRecordingEvent(
            Long userId,
            Long timeRecordingId,
            Long timeRecordId,
            Long timeRecordTypeId,
            Date startDate,
            Date stopDate,
            String note) {
        super();
        this.userId = userId;
        this.timeRecordingId = timeRecordingId;
        this.timeRecordId = timeRecordId;
        this.timeRecordTypeId = timeRecordTypeId;
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.note = note;
        this.created = new Date(System.currentTimeMillis());
        this.update = true;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @return the timeRecordingId
     */
    public Long getTimeRecordingId() {
        return timeRecordingId;
    }

    /**
     * @return the timeRecordId
     */
    public Long getTimeRecordId() {
        return timeRecordId;
    }

    /**
     * @return the timeRecordTypeId
     */
    public Long getTimeRecordTypeId() {
        return timeRecordTypeId;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @return the stopDate
     */
    public Date getStopDate() {
        return stopDate;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @return the update
     */
    public boolean isUpdate() {
        return update;
    }

    /**
     * @param userId the userId to set
     */
    protected void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @param timeRecordingId the timeRecordingId to set
     */
    protected void setTimeRecordingId(Long timeRecordingId) {
        this.timeRecordingId = timeRecordingId;
    }

    /**
     * @param timeRecordId the timeRecordId to set
     */
    protected void setTimeRecordId(Long timeRecordId) {
        this.timeRecordId = timeRecordId;
    }

    /**
     * @param timeRecordTypeId the timeRecordTypeId to set
     */
    protected void setTimeRecordTypeId(Long timeRecordTypeId) {
        this.timeRecordTypeId = timeRecordTypeId;
    }

    /**
     * @param startDate the startDate to set
     */
    protected void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @param stopDate the stopDate to set
     */
    protected void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    /**
     * @param note the note to set
     */
    protected void setNote(String note) {
        this.note = note;
    }

}
