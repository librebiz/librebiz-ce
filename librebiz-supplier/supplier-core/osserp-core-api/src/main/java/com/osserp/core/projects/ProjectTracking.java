/**
 *
 * Copyright (C) 2009, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.projects;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.Status;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectTracking extends Option {
    
    static final Long STATUS_BILLED = 5L;
    static final Long STATUS_CLOSED = 3L;
    static final Long STATUS_EDIT = 2L;
    static final Long STATUS_NEW = 0L;

    /**
     * Provides the start date of the tracking. This value may not be the same
     * as the value of the date of the first record. A contract and also the 
     * official start of tracking may be the 1st of a month but first working 
     * day is 3rd.
     * @return startDate
     */
    Date getStartDate();

    /**
     * Updates the startDate
     * @param startDate
     */
    void setStartDate(Date startDate);

    /**
     * The end date if this is fixed or known.
     * @return endDate
     */
    Date getEndDate();

    /**
     * Updates the end date
     * @param endDate
     */
    void setEndDate(Date endDate);

    /**
     * Provides the tracking type
     * @return type
     */
    ProjectTrackingType getType();

    /**
     * Updates meta data
     * @param user
     * @param type
     * @param name
     * @param startDate
     * @param endDate
     */
    void update(Long user, ProjectTrackingType type, String name, Date startDate, Date endDate);

    /**
     * Updates tracking status
     * @param user
     * @param status
     */
    void update(Long user, Status status);
    
    /**
     * Provides the associated records
     * @return records
     */
    List<ProjectTrackingRecord> getRecords();

    /**
     * Adds a new record
     * @param user
     * @param recordType
     * @param name
     * @param description
     * @param internalNote
     * @param startDate
     * @param endDate
     * @param time
     * @throws ClientException if validation failed
     */
    void addRecord(
            Long user, 
            ProjectTrackingRecordType recordType, 
            String name, 
            String description, 
            String internalNote, 
            Date startDate, 
            Date endDate, 
            Double time) throws ClientException;

    /**
     * Updates an existing record
     * @param user
     * @param record
     * @param recordType
     * @param name
     * @param description
     * @param internalNote
     * @param startDate
     * @param endDate
     * @param time
     * @throws ClientException if validation failed
     */
    void updateRecord(
            Long user, 
            ProjectTrackingRecord record, 
            ProjectTrackingRecordType recordType, 
            String name, 
            String description, 
            String internalNote, 
            Date startDate, 
            Date endDate, 
            Double time) throws ClientException;

    /**
     * Toggles ignore flag of a record and recalculates summary
     * @param user
     * @param record
     */
    void toggleIgnore(Long user, Long record); 
    
    /**
     * Removes a record from record list
     * @param id the primary key of the record to remove
     */
    void removeRecord(Long id);
    
    /**
     * Provides the status of tracking.
     * @return status
     */
    Status getStatus();
    
    /**
     * Provides the summarized hours of all related tracking records.
     * @return totalTime
     */
    Double getTotalTime();
    
    /**
     * Indicates if records should be print in reverse order
     * @return printReverse
     */
    boolean isPrintReverse();
    
    /**
     * Sets record print order
     * @param printReverse
     */
    void setPrintReverse(boolean printReverse);

    /**
     * Indicates if worker should be print
     * @return the printWorker
     */
    boolean isPrintWorker();

    /**
     * Sets if worker should be print
     * @param printWorker
     */
    void setPrintWorker(boolean printWorker);

    /**
     * Provides the id of the worker
     * @return worker id
     */
    Long getWorker();

    /**
     * Sets the id of the worker
     * @param worker id of the worker to set
     */
    void setWorker(Long worker);
    
    /**
     * Indicates if tracking is unchangeable
     * @return true if closed
     */
    boolean isClosed();
}
