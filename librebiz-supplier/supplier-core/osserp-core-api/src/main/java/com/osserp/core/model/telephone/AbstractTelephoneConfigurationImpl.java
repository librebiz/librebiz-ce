/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 1, 2010 11:16:00 AM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.AbstractTelephoneConfiguration;
import com.osserp.core.telephone.TelephoneGroup;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public abstract class AbstractTelephoneConfigurationImpl extends AbstractEntity implements AbstractTelephoneConfiguration {

    private boolean defaultConfig = false;

    private String accountCode = null;
    private String displayName = null;
    private String emailAddress = null;
    private String uid = null;

    private boolean available = false;
    private boolean busyOnBusy = false;

    private boolean telephoneExchange = false;
    private TelephoneGroup telephoneGroup = null;

    private boolean voiceMailEnabled = false;
    private Integer voiceMailDelaySeconds = null;
    private boolean voiceMailEnabledIfBusy = false;
    private boolean voiceMailEnabledIfUnavailable = false;
    private boolean voiceMailSendAsEmail = true;
    private boolean voiceMailDeleteAfterEmailSend = false;

    private String parallelCallTargetPhoneNumber = null;
    private boolean parallelCallEnabled = false;
    private Integer parallelCallSetDelaySeconds = null;

    private String callForwardTargetPhoneNumber = null;
    private Integer callForwardDelaySeconds = null;
    private boolean callForwardEnabled = false;
    private String callForwardIfBusyTargetPhoneNumber = null;
    private boolean callForwardIfBusyEnabled = false;

    protected AbstractTelephoneConfigurationImpl() {
        super();
    }

    public AbstractTelephoneConfigurationImpl(String accountCode, String displayName, String emailAddress, String uid) {
        this.accountCode = accountCode;
        this.displayName = displayName;
        this.emailAddress = emailAddress;
        this.uid = uid;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isVoiceMailEnabled() {
        return voiceMailEnabled;
    }

    public void setVoiceMailEnabled(boolean voiceMailEnabled) {
        this.voiceMailEnabled = voiceMailEnabled;
    }

    public boolean isTelephoneExchange() {
        return telephoneExchange;
    }

    public void setTelephoneExchange(boolean telephoneExchange) {
        this.telephoneExchange = telephoneExchange;
    }

    public TelephoneGroup getTelephoneGroup() {
        return telephoneGroup;
    }

    public void setTelephoneGroup(TelephoneGroup telephoneGroup) {
        this.telephoneGroup = telephoneGroup;
    }

    public boolean isVoiceMailSendAsEmail() {
        return voiceMailSendAsEmail;
    }

    public void setVoiceMailSendAsEmail(boolean voiceMailSendAsEmail) {
        this.voiceMailSendAsEmail = voiceMailSendAsEmail;
    }

    public boolean isVoiceMailDeleteAfterEmailSend() {
        return voiceMailDeleteAfterEmailSend;
    }

    public void setVoiceMailDeleteAfterEmailSend(boolean voiceMailDeleteAfterEmailSend) {
        this.voiceMailDeleteAfterEmailSend = voiceMailDeleteAfterEmailSend;
    }

    public boolean isBusyOnBusy() {
        return busyOnBusy;
    }

    public void setBusyOnBusy(boolean busyOnBusy) {
        this.busyOnBusy = busyOnBusy;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Integer getVoiceMailDelaySeconds() {
        return voiceMailDelaySeconds;
    }

    public void setVoiceMailDelaySeconds(Integer voiceMailDelaySeconds) {
        this.voiceMailDelaySeconds = voiceMailDelaySeconds;
    }

    public boolean isVoiceMailEnabledIfBusy() {
        return voiceMailEnabledIfBusy;
    }

    public void setVoiceMailEnabledIfBusy(boolean voiceMailEnabledIfBusy) {
        this.voiceMailEnabledIfBusy = voiceMailEnabledIfBusy;
    }

    public boolean isVoiceMailEnabledIfUnavailable() {
        return voiceMailEnabledIfUnavailable;
    }

    public void setVoiceMailEnabledIfUnavailable(
            boolean voiceMailEnabledIfUnavailable) {
        this.voiceMailEnabledIfUnavailable = voiceMailEnabledIfUnavailable;
    }

    public String getParallelCallTargetPhoneNumber() {
        return parallelCallTargetPhoneNumber;
    }

    public void setParallelCallTargetPhoneNumber(
            String parallelCallTargetPhoneNumber) {
        this.parallelCallTargetPhoneNumber = parallelCallTargetPhoneNumber;
    }

    public boolean isParallelCallEnabled() {
        return parallelCallEnabled;
    }

    public void setParallelCallEnabled(boolean parallelCallEnabled) {
        this.parallelCallEnabled = parallelCallEnabled;
    }

    public Integer getParallelCallSetDelaySeconds() {
        return parallelCallSetDelaySeconds;
    }

    public void setParallelCallSetDelaySeconds(Integer parallelCallSetDelaySeconds) {
        this.parallelCallSetDelaySeconds = parallelCallSetDelaySeconds;
    }

    public String getCallForwardTargetPhoneNumber() {
        return callForwardTargetPhoneNumber;
    }

    public void setCallForwardTargetPhoneNumber(String callForwardTargetPhoneNumber) {
        this.callForwardTargetPhoneNumber = callForwardTargetPhoneNumber;
    }

    public Integer getCallForwardDelaySeconds() {
        return callForwardDelaySeconds;
    }

    public void setCallForwardDelaySeconds(Integer callForwardDelaySeconds) {
        this.callForwardDelaySeconds = callForwardDelaySeconds;
    }

    public boolean isCallForwardEnabled() {
        return callForwardEnabled;
    }

    public void setCallForwardEnabled(boolean callForwardEnabled) {
        this.callForwardEnabled = callForwardEnabled;
    }

    public String getCallForwardIfBusyTargetPhoneNumber() {
        return callForwardIfBusyTargetPhoneNumber;
    }

    public void setCallForwardIfBusyTargetPhoneNumber(
            String callForwardIfBusyTargetPhoneNumber) {
        this.callForwardIfBusyTargetPhoneNumber = callForwardIfBusyTargetPhoneNumber;
    }

    public boolean isCallForwardIfBusyEnabled() {
        return callForwardIfBusyEnabled;
    }

    public void setCallForwardIfBusyEnabled(boolean callForwardIfBusyEnabled) {
        this.callForwardIfBusyEnabled = callForwardIfBusyEnabled;
    }

    public boolean isDefaultConfig() {
        return defaultConfig;
    }

    protected void setDefaultConfig(boolean defaultConfig) {
        this.defaultConfig = defaultConfig;
    }

}
