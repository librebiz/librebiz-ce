<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${!empty sessionScope.recordView and sessionScope.recordView.confirmDelete}">
<br />
<table style="width: 99%;">
	<tr>
		<td width="60%">
			<span style="font-weight: bold;">&nbsp;<fmt:message key="deleteRecordConfirm"/>:&nbsp;&nbsp;&nbsp;</span>
			<a href="<c:url value="${requestScope.deleteRecordUrl}"><c:param name="method" value="delete"/></c:url>">[<fmt:message key="reallyDeleteRecord"/>]</a>
		</td>
		<td width="40%" align="right">
			<a href="<c:url value="${requestScope.deleteRecordUrl}"><c:param name="method" value="exitDelete"/></c:url>">[<fmt:message key="abortDeleting"/>]</a>
		</td>
	</tr>
</table>
<br />
</c:if>
