<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="settings" value="${sessionScope.projectsByAction}"/>
<c:set var="view" value="${sessionScope.projectsByActionView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">

.table > tbody > tr > td {
    border-top: 0px;
}

.queryLabel {
    width: 30%;
    text-align: right;
}

.querySpacer {
    width: 3%;
}

.queryAction {
    width: 35%;
}

.queryHint {
    width: 30%;
}

.datePickerField {
    padding-left: 10px;
}

</style>
</tiles:put>
<tiles:put name="headline"><fmt:message key="queryForOrders"/></tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/projectsByAction.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
        <table class="table">
            <tr>
                <td class="queryLabel"><fmt:message key="searchFor"/></td>
                <td class="querySpacer"> </td>
                <td class="queryAction">
                    <oc:select name="selectAction" value="${view.selectedType}" options="${view.requestTypes}" url="${'/projectsByAction.do?method=selectType&id='}" onchange="gotoUrl(this.value)" styleClass="form-control" style="width: 75%;" prompt="true"/>
                </td>
                <td class="queryHint"> </td>
            </tr>
        </table>
        <div class="spacer"></div>
        <o:form id="searchByAction" name="searchForm" url="/projectsByAction.do">
            <input type="hidden" name="method" value="searchByAction" />
            <table class="table">
                <tr>
                    <td class="queryLabel"> </td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction boldtext"><fmt:message key="ordersWithFcsAction"/></td>
                    <td class="queryHint"> </td>
                </tr>
                 <tr><td colspan="4"> </td></tr>
                <tr>
                    <td class="queryLabel"><fmt:message key="searchFor"/></td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction">
                        <select name="id" size="1" class="form-control" style="width: 75%;">
                            <c:forEach var="type" items="${view.actions}">
                                <c:choose>
                                    <c:when test="${type.id == view.action}">
                                        <option value="<o:out value="${type.id}"/>" selected="selected"><o:out value="${type.name}"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<o:out value="${type.id}"/>"><o:out value="${type.name}"/></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </td>
                    <td class="queryHint">
                        <c:choose>
                            <c:when test="${view.withClosed}">
                                <input type="checkbox" name="withClosed" value="true" checked="checked"/>&nbsp;<fmt:message key="evenClosed"/>
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="withClosed" value="true" />&nbsp;<fmt:message key="evenClosed"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td class="queryLabel"><fmt:message key="from"/></td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction">
                        <table>
                            <tr>
                                <td><input type="text" id="from" name="from" value="<o:date value="${view.from}"/>" class="form-control" /></td>
                                <td><span class="datePickerField"><o:datepicker input="from"/></span></td>
                            </tr>
                        </table>
                    </td>
                    <td class="queryHint">
                        <c:choose>
                            <c:when test="${view.withCanceled}">
                                <input type="checkbox" name="withCanceled" value="true" checked="checked"/>&nbsp;<fmt:message key="evenCanceled"/>
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="withCanceled" value="true" />&nbsp;<fmt:message key="evenCanceled"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td class="queryLabel"><fmt:message key="until"/></td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction">
                        <table>
                            <tr>
                                <td><input type="text" id="until" name="until" value="<o:date value="${view.until}"/>" class="form-control" /></td>
                                <td><span class="datePickerField"><o:datepicker input="until"/></span></td>
                            </tr>
                        </table>
                    </td>
                    <td class="queryHint"> </td>
                </tr>
                <tr>
                    <td class="queryLabel"><fmt:message key="zipcode"/></td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction">
                        <table>
                            <tr>
                                <td><input type="text" name="zipcode" value="<o:date value="${view.zipcode}"/>" class="form-control" /></td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td class="queryHint"><input type="submit" name="submit" value="<fmt:message key="search"/>" class="submit" onclick="setMethod('searchByAction');" /></td>
                </tr>
                 <tr><td colspan="4"> </td></tr>
            </table>
        </o:form>
        <div class="spacer"></div>
        <o:form id="searchByMissingAction" name="searchForm" url="/projectsByAction.do">
            <input type="hidden" name="method" value="searchByMissingAction" />
            <table class="table">
                <tr>
                    <td class="queryLabel"> </td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction boldtext"><fmt:message key="ordersWithoutFcsAction"/></td>
                    <td class="queryHint"> </td>
                </tr>
                 <tr><td colspan="4"> </td></tr>
                <tr>
                    <td class="queryLabel"><fmt:message key="searchWithout"/></td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction">
                        <select name="id" size="1" class="form-control" style="width: 75%;">
                            <c:forEach var="type" items="${view.actions}">
                                <c:choose>
                                    <c:when test="${type.id == view.action}">
                                        <option value="<o:out value="${type.id}"/>" selected="selected"><o:out value="${type.name}"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<o:out value="${type.id}"/>"><o:out value="${type.name}"/></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </td>
                    <td class="queryHint"> </td>
                </tr>
                <tr>
                    <td class="queryLabel"><fmt:message key="with"/></td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction">
                        <select name="withAction" size="1" class="form-control" style="width: 75%;">
                            <c:forEach var="type" items="${view.actions}">
                                <c:choose>
                                    <c:when test="${type.id == view.withAction}">
                                        <option value="<o:out value="${type.id}"/>" selected="selected"><o:out value="${type.name}"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<o:out value="${type.id}"/>"><o:out value="${type.name}"/></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </td>
                    <td class="queryHint"> </td>
                </tr>
                <tr>
                    <td class="queryLabel"> </td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction">
                        <c:choose>
                            <c:when test="${view.withClosed}">
                                <input type="checkbox" name="withClosed" value="true" checked="checked"/>&nbsp;<fmt:message key="evenClosed"/>
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="withClosed" value="true" />&nbsp;<fmt:message key="evenClosed"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td class="queryHint"> </td>
                </tr>
                <tr>
                    <td class="queryLabel"> </td>
                    <td class="querySpacer"> </td>
                    <td class="queryAction">
                        <c:choose>
                            <c:when test="${view.withCanceled}">
                                <input type="checkbox" name="withCanceled" value="true" checked="checked"/>&nbsp;<fmt:message key="evenCanceled"/>
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="withCanceled" value="true" />&nbsp;<fmt:message key="evenCanceled"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td class="queryHint"><input type="submit" name="submit" value="<fmt:message key="search"/>" class="submit" onclick="setMethod('searchByMissingAction');"/></td>
                </tr>
                 <tr><td colspan="4"> </td></tr>
            </table>
        </o:form>
    </div>
</div>
</tiles:put>
</tiles:insert>
