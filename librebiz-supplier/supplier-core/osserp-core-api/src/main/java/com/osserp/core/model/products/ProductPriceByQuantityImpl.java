/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 13, 2011 6:59:05 PM 
 * 
 */
package com.osserp.core.model.products;

import java.util.Date;

import com.osserp.common.Constants;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.employees.Employee;
import com.osserp.core.model.AbstractPriceAware;
import com.osserp.core.products.ProductPriceByQuantity;
import com.osserp.core.products.ProductPriceByQuantityMatrix;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductPriceByQuantityImpl extends AbstractPriceAware implements ProductPriceByQuantity {

    private Long id;
    private Long reference;
    private Long productId = null;

    private Double rangeStart = Constants.DOUBLE_NULL;
    private Double rangeEnd = Constants.DOUBLE_NULL;

    private Date created;
    private Long createdBy;
    private Date changed;
    private Long changedBy;

    private boolean endOfLife = false;

    protected ProductPriceByQuantityImpl() {
        super();
    }

    protected ProductPriceByQuantityImpl(
            Employee user,
            ProductPriceByQuantityMatrix range,
            Double rangeStart,
            Double rangeEnd) {
        super();
        this.productId = range.getReference();
        this.reference = range.getId();
        this.rangeEnd = rangeEnd;
        this.rangeStart = rangeStart;
        setConsumerMargin(range.getConsumerMargin());
        setConsumerMinimumMargin(range.getPartnerMinimumMargin());
        setPartnerMargin(range.getPartnerMargin());
        setPartnerMinimumMargin(range.getPartnerMinimumMargin());
        setResellerMargin(range.getResellerMargin());
        setResellerMinimumMargin(range.getResellerMinimumMargin());
        created = new Date(System.currentTimeMillis());
        createdBy = user == null ? Constants.SYSTEM_EMPLOYEE : user.getId();
    }

    protected ProductPriceByQuantityImpl(ProductPriceByQuantity other) {
        super(other);
        productId = other.getProductId();
        rangeEnd = other.getRangeEnd();
        rangeStart = other.getRangeStart();
    }

    public Long getPrimaryKey() {
        return id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReference() {
        return reference;
    }

    public void setReference(Long reference) {
        this.reference = reference;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getRangeStart() {
        return rangeStart;
    }

    public void setRangeStart(Double rangeStart) {
        this.rangeStart = rangeStart;
    }

    public void resetRangeEnd(Employee user) {
        rangeEnd = Constants.DOUBLE_NULL;
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
    }

    public Double getRangeEnd() {
        return rangeEnd;
    }

    public void setRangeEnd(Double rangeEnd) {
        this.rangeEnd = rangeEnd;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getChanged() {
        return changed;
    }

    public void setChanged(Date changed) {
        this.changed = changed;
    }

    public Long getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(Long changedBy) {
        this.changedBy = changedBy;
    }

    public boolean isEndOfLife() {
        return endOfLife;
    }

    public void setEndOfLife(boolean endOfLife) {
        this.endOfLife = endOfLife;
    }

    public boolean isInitial() {
        return !isSet(rangeStart);
    }

    public boolean isLast() {
        return !isSet(rangeEnd);
    }

    public void update(Double rangeEnd) {
        this.rangeEnd = rangeEnd;
    }

    public void update(Double consumerPrice, Double resellerPrice, Double partnerPrice) {
        setConsumerPrice(NumberUtil.round(consumerPrice, 2));
        setResellerPrice(NumberUtil.round(resellerPrice, 2));
        setPartnerPrice(NumberUtil.round(partnerPrice, 2));
    }

    public void updateMinimumMargin(
            Employee user,
            Double consumerMinimumMargin,
            Double resellerMinimumMargin,
            Double partnerMinimumMargin) {
        setConsumerMinimumMargin(consumerMinimumMargin);
        setResellerMinimumMargin(resellerMinimumMargin);
        setPartnerMinimumMargin(partnerMinimumMargin);
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
    }
}
