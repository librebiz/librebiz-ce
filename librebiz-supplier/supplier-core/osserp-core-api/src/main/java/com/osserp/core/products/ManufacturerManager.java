/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 3, 2005 
 * 
 */
package com.osserp.core.products;

import java.util.List;

import com.osserp.common.ClientException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ManufacturerManager {

    /**
     * Returns the manufacturers related to a given product group
     * @param groupId
     * @return list of manufacturers ordered by name
     * @throws ClientException if no group specified
     */
    List findByGroup(Long groupId) throws ClientException;

    /**
     * Adds a new manufacturer to the given product group
     * @param name of the manufacturer
     * @param groupId where we add the name
     * @throws ClientException if name or groupid is null or name already existing under specified group id
     */
    Manufacturer add(String name, Long groupId) throws ClientException;

    /**
     * Renames a manufacturer
     * @param id of the manufacturer
     * @param new name
     * @throws ClientException if id or name is null or name already exists under another id
     */
    void rename(Long id, String name) throws ClientException;
}
