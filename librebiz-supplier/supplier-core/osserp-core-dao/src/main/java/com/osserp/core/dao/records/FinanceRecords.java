/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 14, 2015 7:40:02 PM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.Date;
import java.util.List;

import com.osserp.core.finance.FinanceRecord;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FinanceRecords {

    /**
     * Finds all records related to given reference
     * @param reference
     * @return referenced
     */
    List<? extends FinanceRecord> getByReference(Long reference);

    /**
     * Provides all records of implementing type related to a contact
     * @param contactId
     * @return related
     */
    List<? extends FinanceRecord> getByContact(Long contactId);

    /**
     * Provides all records of implementing type related to a contact and company
     * @param contactId
     * @param companyId
     * @return related
     */
    List<? extends FinanceRecord> getByContact(Long contactId, Long companyId);

    /**
     * Provides all records of implementing type between start and stop date
     * @param startDate
     * @param stopDate
     * @return related
     */
    List<? extends FinanceRecord> getByDate(Date startDate, Date endDate);
    
    /**
     * Provides latest records
     * @param startDate
     * @return latest records
     */
    List<? extends FinanceRecord> getLatest(Date startDate);

    /**
     * Updates the document reference
     * @param user
     * @param record
     * @param id
     * @return updated record
     */
    FinanceRecord updateDocumentReference(Long user, FinanceRecord record, Long id);
}
