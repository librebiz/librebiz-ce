<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:if test="${!empty sessionScope.message}">
<div class="message"><p style="text-align: center;"><o:out value="${sessionScope.message}"/></p></div>
<c:remove var="message" scope="session"/>
</c:if>
