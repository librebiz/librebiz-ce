/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 20, 2016 
 * 
 */
package com.osserp.gui.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.util.Quantities

import com.osserp.core.products.Product

import com.osserp.groovy.web.ViewContextException

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ProductEditorView extends AbstractProductView {
    private static Logger logger = LoggerFactory.getLogger(ProductEditorView.class.getName())

    ProductEditorView() {
        super()
        dependencies = ['productView']
        headerName = 'product_edit'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            Long id = getLong('productId')
            if (env.productView?.product) {
                bean = env.productView?.product
                if (id != null && id != product.productId) {
                    logger.warn('initRequest: provided productId did not match existing productView.product.productId')
                    throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
                }
            } else if (!id) {
                logger.warn('initRequest: did not find id nor productView')
                throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
            } 
            enableEditMode()
        }
    }

    @Override
    void save() {
        Product vo = getProduct()
        String aname = getString('name')
        if (aname) {
            vo.setName(aname)
        }
        String matchcode = getString('matchcode')
        if (matchcode) {
            vo.setMatchcode(matchcode)
        }
        vo.setDescription(getString('description'))
        vo.setDescriptionShort(getString('descriptionShort'))
        vo.setPurchaseText(getString('purchaseText'))
        vo.setManufacturer(getLong('manufacturer'))
        vo.setMarketingText(getString('marketingText'))
        vo.setNullable(getBoolean('nullable'))
        vo.setOfferUsageOnly(getBoolean('offerUsageOnly'))
        vo.setBillingNoteRequired(getBoolean('billingNoteRequired'))
        vo.setEndOfLife(getBoolean('endOfLife'))
        vo.setGtin13(getString('gtin13'))
        vo.setGtin14(getString('gtin14'))
        vo.setGtin8(getString('gtin8'))
        
        if (!productReferenced || isPermissionGrant('product_edit_even_locked')) {
            if (productReferenced && logger.isDebugEnabled()) {
                logger.debug("save() updating locked values [product=${vo.productId}, user=${domainEmployee.id}]")
            }
            vo.setAffectsStock(getBoolean('affectsStock'))
            vo.setBundle(getBoolean('bundle'))
            vo.setComponent(getBoolean('component'))
            vo.setVirtual(getBoolean('virtual'))
            vo.setService(getBoolean('service'))
            vo.setPlant(getBoolean('plant'))
            vo.setTerm(getBoolean('term'))
            vo.setKanban(getBoolean('kanban'))
            vo.setQuantityUnit(getLong('quantityUnit'))
            if (Quantities.isTimeAware(vo.getQuantityUnit())) {
                vo.setQuantityUnitTime(true)
            } else {
                vo.setQuantityUnitTime(false)
            }
            vo.setQuantityByPlant(getBoolean('quantityByPlant'))
            vo.setPackagingUnit(getInteger('packagingUnit'))
            vo.setReducedTax(getBoolean('reducedTax'))
            vo.setPower(getDouble('power'))
            vo.setPowerUnit(getLong('powerUnit'))
            vo.setPublicAvailable(getBoolean('publicAvailable'))
            if (vo.getPower() && !vo.getPowerUnit()) {
                throw new ClientException(ErrorCode.POWER_UNIT_MISSING)
            }
            if (vo.getPowerUnit() != null && vo.getPowerUnit().equals(0L)) {
                vo.setPowerUnit(null)
            }

            if (vo.isAffectsStock() && vo.isService()) {
                throw new ClientException(ErrorCode.STOCK_OR_SERVICE)
            }
            if (vo.isAffectsStock() || vo.isService()) {
                vo.setPlanning(true)
            } else {
                vo.setPlanning(false)
            }
        }
        vo.setChangedBy(domainEmployee.id)
        vo.setChanged(new Date(System.currentTimeMillis()))
        productManager.update(vo)
        productSummaryManager.reloadSummary(vo)
        if (vo.isEndOfLife() && vo.isAvailableOnAnyStock()) {
            vo.setEndOfLife(false)
            productManager.update(vo)
            logger.debug("save: resetting eol flag while stock != 0")
        }
        bean = productManager.get(vo.productId)
        disableEditMode()
        if (env.productView) {
            env.productView.reload()
        }
        logger.debug("save: done [product=${product?.productId}]")
    }

    boolean isProductReferenced() {
        // TODO request answer
        false
    }

}
