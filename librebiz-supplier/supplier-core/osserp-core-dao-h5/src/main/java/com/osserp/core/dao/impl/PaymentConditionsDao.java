/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 14-Oct-2006 12:45:20 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.Options;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.model.PaymentConditionImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PaymentConditionsDao extends AbstractDao implements PaymentConditions {
    private static Logger log = LoggerFactory.getLogger(PaymentConditionsDao.class.getName());

    private String paymentConditionDefaultsTable = null;
    private OptionsCache optionsCache = null;

    public PaymentConditionsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables, SessionFactory sessionFactory,
            OptionsCache optionsCache) {
        super(jdbcTemplate, tables, sessionFactory);
        this.optionsCache = optionsCache;
        this.paymentConditionDefaultsTable = getTable(
                TableKeys.RECORD_PAYMENT_CONDITION_DEFAULTS);
    }

    public List<PaymentCondition> getPaymentConditions() {
        return getCurrentSession().createQuery("from " + PaymentConditionImpl.class.getName()).list();
    }

    public PaymentCondition load(Long id) {
        return (PaymentCondition) getCurrentSession().load(PaymentConditionImpl.class, id);
    }

    public PaymentCondition create(
            String name,
            Integer paymentTarget,
            boolean paid,
            boolean withDiscount,
            Double discount1,
            Integer discount1Target,
            Double discount2,
            Integer discount2Target) throws ClientException {

        if (isEmpty(name)) {
            if (log.isDebugEnabled()) {
                log.debug("create() validation failed, no name found");
            }
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        if (withDiscount &&
                (discount1 == null || discount1.doubleValue() == 0)
                && (discount2 == null || discount2.doubleValue() == 0)) {
            if (log.isDebugEnabled()) {
                log.debug("create() validation failed," +
                        " discount enabled but no discounts selected");
            }
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        if (discount1 == null) {
            discount1 = 0d;
        }
        if (discount2 == null) {
            discount2 = 0d;
        }
        List existing = getPaymentConditions();
        for (int i = 0, j = existing.size(); i < j; i++) {
            PaymentCondition con = (PaymentCondition) existing.get(i);
            if (con.getName().equals(name)) {
                if (log.isDebugEnabled()) {
                    log.debug("addPaymentCondition() validation failed," +
                            " name already exists");
                }
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        PaymentConditionImpl obj = new PaymentConditionImpl(
                name,
                paymentTarget,
                false,
                paid,
                withDiscount,
                discount1,
                discount1Target,
                discount2,
                discount2Target);
        save(obj);
        return obj;
    }

    public void save(PaymentCondition obj) {
        getCurrentSession().saveOrUpdate("PaymentConditionImpl", obj);
        if (optionsCache != null) {
            optionsCache.refresh(Options.RECORD_PAYMENT_CONDITIONS);
        }
    }

    public PaymentCondition getPaymentConditionDefault(boolean sales) {
        StringBuilder sql = new StringBuilder();
        if (sales) {
            sql.append("select sales from ");
        } else {
            sql.append("select purchasing from ");
        }
        sql
                .append(paymentConditionDefaultsTable)
                .append(" where isactive = true");
        List<Long> result = (List) jdbcTemplate.query(
                sql.toString(),
                getLongRowMapper());
        return (result.isEmpty() ? null
                : load(result.get(0)));
    }
}
