<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:userContext/>
<v:view viewName="accountingIndexView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
        .table > tbody > tr > td {                                                             
            border-top: 0px;
        }
        </style>
    </tiles:put>
	<tiles:put name="headline">
		<fmt:message key="accountingLabel"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="accountingIndexContent">
            <div class="row">
                <c:import url="_bankAccountsDisplay.jsp"/>

                <c:import url="_accountingActionSelection.jsp"/>

                <oc:systemPropertyEnabled name="taxReportEnabled">
                    <c:set var="taxReportList" scope="request" value="${view.taxReports}"/>
                    <c:set var="taxReportExitTarget" scope="request" value="/accounting/accountingIndex/forward"/>
                    <c:set var="taxReportCreateUrl" scope="request" value="/accounting/taxReport/forwardCreate"/>
                    <c:set var="taxReportSelectUrl" scope="request" value="/accounting/taxReport/forward"/>
                   <c:import url="${viewdir}/accounting/_taxReportSelectionList.jsp"/>
                </oc:systemPropertyEnabled>

            </div>
        </div>
    </tiles:put>
</tiles:insert>
