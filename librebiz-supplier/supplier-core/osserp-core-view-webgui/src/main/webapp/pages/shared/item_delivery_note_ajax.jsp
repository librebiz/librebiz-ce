<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:if test="${!empty requestScope.deliveryNoteItem}">
	<c:set var="obj" value="${requestScope.deliveryNoteItem}"/>
</c:if>

<table class="table" style="width: 400px; background-color: #FFFFFF; text-align:left;">
<c:choose>
	<c:when test="${empty obj}">
		<tr>
			<th><fmt:message key="info"/></th>
		</tr>
		<tr>
			<td>
				<fmt:message key="noInformationAvailable"/>
			</td>
		</tr>
	</c:when>
	<c:otherwise>
			<tr>
				<th><o:out value="${obj.id}"/></th>
				<th class="small"><o:out value="${obj.salesName}"/></th>
			</tr>
			<tr>
				<td style="border: 0px;"><fmt:message key="product"/></td>
				<td style="border: 0px;"><o:out value="${obj.productId}"/></td>
			</tr>
			<tr>
				<td style="border: 0px;"><fmt:message key="name"/></td>
				<td style="border: 0px;"><o:out value="${obj.productName}"/></td>
			</tr>
			<tr>
				<td style="border: 0px;"><fmt:message key="quantity"/></td>
				<td style="border: 0px;"><o:number value="${obj.outstanding}" format="integer"/></td>
			</tr>
			<tr>
				<td style="border: 0px;"><fmt:message key="delivery"/></td>
				<td style="border: 0px;"><o:date value="${obj.delivery}"/></td>
			</tr>
			<tr>
				<td style="border: 0px;"><fmt:message key="info"/></td>
				<td style="border: 0px;"><o:out value="${obj.deliveryNote}"/></td>
			</tr>
	</c:otherwise>
	</c:choose>
</table>
