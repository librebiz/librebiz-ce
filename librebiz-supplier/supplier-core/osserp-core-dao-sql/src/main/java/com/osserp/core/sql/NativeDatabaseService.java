/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 30, 2017 
 * 
 */
package com.osserp.core.sql;

import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class NativeDatabaseService {
    
    private String dbmsUrl = null;
    private String dbName = null;
    private String dbUser = null;
    private String dbPwd = null;
    
    private String dbHost = null;
    private int dbPort = 0;

    public NativeDatabaseService(
            String dbDriver, 
            String datasourceString,
            String dbUser, 
            String dbPwd) {
        super();
        this.dbUser = dbUser;
        this.dbPwd = dbPwd;
        initDatabaseHost(dbDriver, datasourceString);
    }
    
    /**
     * Provides the dbmsUrl + dbName
     * @return databaseUrl string or null if url or name not set
     */
    public final String getDatabaseUrl() {
        return (getDbmsUrl() == null || getDbName() == null)
                ? null : getDbmsUrl() + "/" + getDbName();
    }
    
    protected Connection getConnection() throws Exception {
        if (!isDatabaseSettingsAvailable()) {
            throw new IllegalStateException("Connection settings not initialized");
        }
        return DriverManager.getConnection(getDatabaseUrl(), getDbUser(), getDbPwd());
    }
    
    protected void initDatabaseHost(String jdbcDriver, String jdbcDatasourceString) {
        if (jdbcDriver != null && jdbcDatasourceString != null 
                && jdbcDatasourceString.lastIndexOf("/") > 1) {
            String cleanURI = jdbcDatasourceString.substring(5);
            URI uri = URI.create(cleanURI);
            dbPort = uri.getPort();
            dbHost = uri.getHost();
            dbName = jdbcDatasourceString.substring(jdbcDatasourceString.lastIndexOf("/") + 1,
                    jdbcDatasourceString.length());
            dbmsUrl = jdbcDatasourceString.substring(0, jdbcDatasourceString.lastIndexOf("/"));
        }
    }

    protected boolean isDatabaseSettingsAvailable() {
        return dbmsUrl != null 
                && dbName != null 
                && dbUser != null && 
                dbPwd != null
                && dbmsUrl.length() > 0 
                && dbName.length() > 0 
                && dbUser.length() > 0 
                && dbPwd.length() > 0;
    }
    
    /**
     * Closes the specified resource objects
     * @param rs to close
     * @param stmt to close
     * @param con to close
     */
    protected void close(ResultSet rs, Statement stmt, Connection con) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ignorable) {
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ignorable) {
            }
        }
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ignorable) {
            }
        }
    }
    
    protected Long createLong(String value) {
        try {
            return value == null ? null : Long.valueOf(value);
        } catch (Throwable e) {
            return null;
        }
    }

    public String getDbHost() {
        return dbHost;
    }

    protected void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public int getDbPort() {
        return dbPort;
    }

    protected void setDbPort(int dbPort) {
        this.dbPort = dbPort;
    }

    public String getDbName() {
        return dbName;
    }

    protected void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbmsUrl() {
        return dbmsUrl;
    }

    protected void setDbmsUrl(String dbmsUrl) {
        this.dbmsUrl = dbmsUrl;
    }

    protected String getDbUser() {
        return dbUser;
    }

    protected void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    protected String getDbPwd() {
        return dbPwd;
    }

    protected void setDbPwd(String dbPwd) {
        this.dbPwd = dbPwd;
    }
}
