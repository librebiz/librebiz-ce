<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:if test="${empty sessionScope.user or empty sessionScope.portalView}">
<o:logger write="no user or portal available, sending info about probably timeout" level="info"/>
<c:redirect url="/errors/error_timeout.jsp"/>
</c:if>
