/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 7, 2007 9:46:44 PM 
 * 
 */
package com.osserp.core.model.records;

import org.jdom2.Element;

import com.osserp.common.beans.OptionImpl;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.finance.BookingType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractBookingType extends OptionImpl implements BookingType {

    private int order = 0;
    private Long transferType = null;
    private boolean useNameOnOutput = false;
    private String key = null;
    private boolean printable = false;

    protected AbstractBookingType() {
        super();
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Long getTransferType() {
        return transferType;
    }

    protected void setTransferType(Long transferType) {
        this.transferType = transferType;
    }

    public boolean isUseNameOnOutput() {
        return useNameOnOutput;
    }

    protected void setUseNameOnOutput(boolean useNameOnOutput) {
        this.useNameOnOutput = useNameOnOutput;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isPrintable() {
        return printable;
    }

    protected void setPrintable(boolean printable) {
        this.printable = printable;
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("order", order));
        root.addContent(JDOMUtil.createElement("transferType", transferType));
        root.addContent(JDOMUtil.createElement("useNameOnOutput", useNameOnOutput));
        root.addContent(JDOMUtil.createElement("key", key));
        root.addContent(JDOMUtil.createElement("printable", printable));
        return root;
    }
}
