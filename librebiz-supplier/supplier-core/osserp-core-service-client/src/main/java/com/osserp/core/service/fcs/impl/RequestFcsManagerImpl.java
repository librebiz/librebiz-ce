/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 9:51:26 PM 
 * 
 */
package com.osserp.core.service.fcs.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.FcsItem;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.RequestFcsActions;
import com.osserp.core.dao.RequestFcsWastebaskets;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.RequestFcsItemImpl;
import com.osserp.core.model.RequestImpl;
import com.osserp.core.requests.Request;
import com.osserp.core.requests.RequestFcsAction;
import com.osserp.core.requests.RequestFcsManager;
import com.osserp.core.sales.SalesOfferManager;
import com.osserp.core.service.fcs.RequestFcsEventCreator;
import com.osserp.core.tasks.RequestListCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestFcsManagerImpl extends AbstractFcsManager implements RequestFcsManager {
    private static Logger log = LoggerFactory.getLogger(RequestFcsManagerImpl.class.getName());

    private Requests requests = null;
    private Employees employees = null;
    private SalesOfferManager salesOfferManager = null;
    private RequestListCacheSender requestList = null;
    private SystemConfigs systemConfigs = null;
   

    public RequestFcsManagerImpl(
            RequestFcsActions actions,
            RequestFcsEventCreator eventJobs,
            RequestFcsWastebaskets wastebaskets,
            Requests requests,
            Employees employees,
            SalesOfferManager salesOfferManager,
            BusinessNotes businessNotes,
            SystemConfigs systemConfigs,
            RequestListCacheSender requestList) {

        super(actions, eventJobs, wastebaskets, businessNotes);
        this.requests = requests;
        this.employees = employees;
        this.salesOfferManager = salesOfferManager;
        this.requestList = requestList;
        this.systemConfigs = systemConfigs;
    }

    public BusinessCase resetCancellation(Employee employee, BusinessCase businessCase) throws ClientException {
        if (!(businessCase instanceof RequestImpl) || !businessCase.isCancelled()) {
            log.warn("initializeFlowControl() invoked with invalid status [businessCase="
                    + (businessCase == null ? "null" : (businessCase.getPrimaryKey()
                            + ", class=" + businessCase.getClass().getName()
                            + ", cancelled=" + businessCase.isCancelled()))
                    + "]");
            throw new ClientException(ErrorCode.INVALID_CONTEXT);
        }
        RequestImpl obj = (RequestImpl) businessCase;
        int highestStatus = 0;
        StringBuilder businessNote = new StringBuilder("Cancellation reset - ");
        for (Iterator<FcsItem> i = obj.getFlowControlSheet().iterator(); i.hasNext();) {
            FcsItem next = i.next();
            if (next.getAction().isCancelling()) {
                if (log.isDebugEnabled()) {
                    log.debug("resetCancellation() removing cancelling action [id=" + next.getId()
                            + ", action=" + next.getAction().getId()
                            + ", createdBy" + next.getCreatedBy()
                            + ", date=" + next.getCreated()
                            + "]");
                }
                businessNote.append(next.getAction().getName());
                
                if (next.getNote() != null) {
                    businessNote.append("\n").append(next.getNote());
                }
                i.remove();
            } else {
                if (next.getAction().getStatus() != null && next.getAction().getStatus() > highestStatus) {
                    highestStatus = next.getAction().getStatus();
                }
            }
        }
        Date changed = new Date(System.currentTimeMillis());
        obj.setStatus(Long.valueOf(highestStatus));
        obj.setChanged(changed);
        obj.setChangedBy(employee == null ? Constants.SYSTEM_EMPLOYEE : employee.getId());
        obj.setStopped(false);
        requests.save(obj);
        
        if (systemConfigs.isSystemPropertyEnabled("requestRestartOfferResetEnabled")) {
            try {
                salesOfferManager.resetCancelled(employee, obj);
            } catch (Exception e) {
                log.error("resetCancellation() failed to reset offers [message=" + e.getMessage() + "]", e);
            }
        }

        addBusinessNote(employee, obj, businessNote.toString());
        
        return obj;
    }

    public BusinessCase resetClosed(Employee employee, BusinessCase businessCase) throws ClientException {
        throw new UnsupportedOperationException("Operation not supported in request mode");
    }

    public void initializeFlowControl(Request request) {
        if (request == null) {
            log.warn("initializeFlowControl() invoked with request NULL!");
            throw new BackendException("request was null!");
        }
        List<FcsAction> all = getFcsActions().findByType(request.getType().getId());
        if (!all.isEmpty()) {
            FcsAction initial = all.get(0);
            if (log.isDebugEnabled()) {
                StringBuffer buffer = new StringBuffer();
                buffer
                        .append("initializeFlowControl() invoked, [employee=")
                        .append(request.getCreatedBy())
                        .append(",request=").append(request.getRequestId())
                        .append(",action=").append(initial.getId())
                        .append("]");
                log.debug(buffer.toString());
            }
            try {

                request.addFlowControl(
                        initial,
                        employees.getEmployeeByUser(request.getCreatedBy()),
                        request.getCreated(),
                        null,
                        null,
                        null);
                saveBusinessCase(request);
            } catch (ClientException e) {
                // should never happen on initial action
                log.warn("initializeFlowControl() caught unexpected ClientException [message=" + e.getMessage() + "]");
            }
            if (initial.isPerformEvents() && !eventNoteExcluded(initial, request.getType())
                    && !request.getType().isSalesPersonByCollector()) {

                createEvents(
                    request,
                    initial,
                    request.getCreated(),
                    request.getCreatedBy(),
                    request.getRequestId(),
                    null,
                    null,
                    false);
            }
            /*
             * if (initial.isPerformAction() && initial.isPerformAtEnd() && initial.getEndActionName() != null) {
             * 
             * // TODO perform end actions //this.mqManager.sendTicket(initial.getEndActionName(), sale); }
             */
            if (log.isDebugEnabled()) {
                log.debug("initializeFlowControl() [request="
                        + request.getRequestId()
                        + "] done");
            }
        }
        refreshRequestList(request);
    }

    public void addFlowControl(
            Employee employee,
            BusinessCase businessCase,
            FcsAction action,
            Date created,
            String note,
            String headline,
            FcsClosing closing,
            boolean ignoreDependencies) throws ClientException {

        Long employeeId = (employee == null ? Constants.SYSTEM_EMPLOYEE : employee.getId());
        Request request = (Request) businessCase;
        if (log.isDebugEnabled()) {
            StringBuffer buffer = new StringBuffer();
            buffer
                    .append("addFlowControl() [employee=")
                    .append(employeeId)
                    .append(",request=").append(request.getRequestId())
                    .append(",action=").append(action.getId())
                    .append("]");
            log.debug(buffer.toString());
        }
        if (action instanceof RequestFcsAction) {
            RequestFcsAction rfa = (RequestFcsAction) action;
            if (rfa.isSalesSelecting() && businessCase.getSalesId() == null) {
                throw new ClientException(ErrorCode.SALESPERSON_MISSING);
            }
            if (rfa.isManagerSelecting() && businessCase.getManagerId() == null) {
                throw new ClientException(ErrorCode.MANAGER_MISSING);
            }
        }
        validateBeforeAdd(action, created, note, headline);
        List<FcsAction> todos = getFcsActions().createActionList(request);
        if (!ignoreDependencies) {
            checkDependencies(todos, action);
        }
        boolean added = request.addFlowControl(
                action,
                employeeId,
                created,
                note,
                headline,
                closing);

        if (added) {
            if (log.isDebugEnabled()) {
                log.debug("addFlowControl() added [action=" + action.getId()
                        + "], trying to persist status");
            }
            if (action.isStopping()) {
                request.setStopped(true);
            } else {
                // revert stopped flag on new action  
                request.setStopped(false);
            }
            saveBusinessCase(request);
            if (action.isCancelling()) {
                getFcsEventJobs().closeAllEvents(employee, request);
                salesOfferManager.cancel(employee, request);
            }
            if (action.isPerformEvents() && !eventNoteExcluded(action, request.getType())) {

                createEvents(
                    request,
                    action,
                    created,
                    employeeId,
                    request.getRequestId(),
                    note,
                    headline,
                    false);
            }
            // perform queue related actions at the end of an action if configured
            /*
             * if (action.isPerformAction() && action.isPerformAtEnd()) {
             * 
             * this.performQueues(action, employee.getId(), request); }
             */
            if (log.isDebugEnabled()) {
                log.debug("addFlowControl() done");
            }
            refreshRequestList(request);
        }
    }

    private boolean eventNoteExcluded(FcsAction action, BusinessType businessType) {
        if (action instanceof RequestFcsAction) {
            RequestFcsAction rfa = (RequestFcsAction) action;
            return rfa.getBusinessTypeExcludes().contains(businessType.getId());
        }
        return false;
    }

    @Override
    public boolean cancelFlowControl(Long employeeId, BusinessCase businessCase, FcsItem item, String note) throws ClientException {
        if (super.cancelFlowControl(employeeId, businessCase, item, note)) {
            // Reset request if previous canceling was cancelled.
            if (item.getAction().isCancelling()) {
                int mostCurrentStatus = 0;
                for (int i = 0, j = businessCase.getFlowControlSheet().size(); i < j; i++) {
                    FcsItem next = businessCase.getFlowControlSheet().get(i);
                    if (next.getAction().getStatus() > mostCurrentStatus) {
                        mostCurrentStatus = next.getAction().getStatus();
                    }
                }
                Request request = (Request) businessCase;
                request.setStatus(Long.valueOf(mostCurrentStatus));
                saveBusinessCase(businessCase);
                refreshRequestList(request);
            } else if (item.getAction().isStopping()) {
                // revert stopped flag when cancelling stopping action  
                Request request = (Request) businessCase;
                request.setStopped(false);
                saveBusinessCase(businessCase);
                refreshRequestList(request);
            }
            return true;
        }
        return false;
    }

    @Override
    protected FcsItem createCancelItem(BusinessCase businessCase, FcsAction action, Long employeeId, String note) {
        return new RequestFcsItemImpl(
                (Request) businessCase,
                action,
                new Date(System.currentTimeMillis()),
                employeeId,
                note,
                null,
                true,
                null);
    }

    @Override
    protected void saveBusinessCase(BusinessCase businessCase) {
        requests.save((Request) businessCase);
    }

    @Override
    protected void saveFcsItem(FcsItem object) {
        requests.save(object);
    }

    private void refreshRequestList(Request request) {
        try {
            requestList.send(request.getRequestId(), "request");
        } catch (Exception e) {
            log.warn("initializeFlowControl() ignoring failed attempt to refresh request list cache [message=" + e.getMessage() + "]");
        }
    }
}
