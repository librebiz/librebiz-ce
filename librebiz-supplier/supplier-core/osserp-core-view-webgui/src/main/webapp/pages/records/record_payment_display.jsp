<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:set var="payment" value="${record.paymentAgreement}"/>
<c:set var="changeAllowed" value="true"/>
<c:if test="${!empty requestScope.paymentConditionEditEnabled}">
<c:set var="changeAllowed" value="${requestScope.paymentConditionEditEnabled}"/>
</c:if>

<div class="recordLabelValue">
<c:choose>
<c:when test="${changeAllowed and !record.closed and (!record.unchangeable or !record.sales)}">
<v:link url="/records/recordPaymentAgreement/forward?id=${record.id}&name=${view.name}&exit=${requestScope.baseUrl}?method=reload"><fmt:message key="paymentConditions"/>:</v:link>
</c:when>
<c:otherwise>
<fmt:message key="paymentConditions"/>:
</c:otherwise>
</c:choose>
<c:choose>
<c:when test="${payment.hidePayments and empty payment.note}">
<oc:options name="recordPaymentConditions" value="${payment.paymentCondition.id}"/>
</c:when>
<c:otherwise>
<a style="margin-left: 20px;" href="javascript:changeConditionDisplay();" title="<fmt:message key="turnConditionDisplayOnOff"/>">
<c:choose>
<c:when test="${payment.hidePayments}">
    <fmt:message key="specialAgreement"/> / <oc:options name="recordPaymentConditions" value="${payment.paymentCondition.id}"/>
</c:when>
<c:when test="${payment.amountsFixed}"><fmt:message key="flatAgreement"/> / <o:out value="${payment.paymentCondition.paymentTarget}"/></c:when>
<c:otherwise><o:number value="${payment.downpaymentPercent}" format="percent"/>-<o:number value="${payment.deliveryInvoicePercent}" format="percent"/>-<o:number value="${payment.finalInvoicePercent}" format="percent"/> / <o:out value="${payment.paymentCondition.paymentTarget}"/></c:otherwise>
</c:choose>
</a>
</c:otherwise>
</c:choose>
</div>
<div class="conditionsFrame" style="margin-left: 10px; width: 90%;">
<div id="conditions" class="conditions" style="display:none;">
<c:if test="${!payment.hidePayments}">
<span class="boldtext"><fmt:message key="payment"/>: </span><span><oc:options name="recordPaymentConditions" value="${payment.paymentCondition.id}"/></span><c:if test="${record.taxFree}"> (<fmt:message key="taxFree"/>) <span class="info_box"><a href="javascript:;" class="info"><img src="<c:url value="${applicationScope.infoIcon}"/>" class="info_icon"/><span><oc:options name="taxFreeDefinitions" value="${record.taxFreeId}"/></span></a></span></c:if>
<c:choose>
<c:when test="${payment.amountsFixed}">
	<br/><span class="boldtext"><o:number value="${payment.downpaymentAmount}" format="currency"/></span> <oc:options name="recordPaymentTargets" value="${payment.downpaymentTargetId}"/>,
	<br/><span class="boldtext"><o:number value="${payment.deliveryInvoiceAmount}" format="currency"/></span> <oc:options name="recordPaymentTargets" value="${payment.deliveryInvoiceTargetId}"/>,
	<br/><span class="boldtext"><o:number value="${payment.finalInvoiceAmount}" format="currency"/></span> <oc:options name="recordPaymentTargets" value="${payment.finalInvoiceTargetId}"/>
</c:when>
<c:otherwise>
	<br/><span class="boldtext"><fmt:formatNumber value="${payment.downpaymentPercent}" type="percent"/></span> <oc:options name="recordPaymentTargets" value="${payment.downpaymentTargetId}"/>,
	<br/><span class="boldtext"><fmt:formatNumber value="${payment.deliveryInvoicePercent}" type="percent"/></span> <oc:options name="recordPaymentTargets" value="${payment.deliveryInvoiceTargetId}"/>,
	<br/><span class="boldtext"><fmt:formatNumber value="${payment.finalInvoicePercent}" type="percent"/></span> <oc:options name="recordPaymentTargets" value="${payment.finalInvoiceTargetId}"/>
</c:otherwise>
</c:choose>
</c:if>
<c:if test="${!empty payment.note}">
	<br/><span class="boldtext"><fmt:message key="specialAgreement"/>:</span>
	<o:textOut value="${payment.note}"/>
</c:if>
</div>
</div>
