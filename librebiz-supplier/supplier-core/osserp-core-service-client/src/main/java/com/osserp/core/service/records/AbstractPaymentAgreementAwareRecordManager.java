/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 13, 2011
 *  
 */
package com.osserp.core.service.records;

import com.osserp.common.ClientException;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.Records;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.PaymentAgreementAwareRecord;
import com.osserp.core.finance.PaymentAgreementAwareRecordManager;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPaymentAgreementAwareRecordManager extends
        AbstractRecordManager implements PaymentAgreementAwareRecordManager {

    /**
     * Creates a new recordManager for paymentAgreementAware records
     * @param records
     * @param systemConfigManager
     * @param productPlanningTask
     */
    protected AbstractPaymentAgreementAwareRecordManager(
            Records records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask) {
        super(records, systemConfigManager, products, productPlanningTask);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.service.records.AbstractRecordManager#update(com.osserp.core.finance.PaymentAgreementAwareRecord, boolean, boolean,
     * java.lang.Double, java.lang.Double, java.lang.Double, java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long, java.lang.String,
     * com.osserp.core.employees.Employee)
     */
    @Override
    public final void update(
            PaymentAgreementAwareRecord record,
            boolean hidePayments,
            boolean amountsFixed,
            Double downpayment,
            Double deliveryInvoice,
            Double finalInvoice,
            Long downpaymentTargetId,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            Long paymentConditionId,
            String note,
            Employee user) throws ClientException {
        super.update(
                record,
                hidePayments,
                amountsFixed,
                downpayment,
                deliveryInvoice,
                finalInvoice,
                downpaymentTargetId,
                deliveryInvoiceTargetId,
                finalInvoiceTargetId,
                paymentConditionId,
                note,
                user);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.PaymentAgreementAwareRecordManager#switchPaymentFixedAmountsFlag(com.osserp.core.finance.PaymentAgreementAwareRecord)
     */
    public void switchPaymentFixedAmountsFlag(PaymentAgreementAwareRecord record) {
        record.switchPaymentFixedAmountsFlag();
        persist(record);
    }
}
