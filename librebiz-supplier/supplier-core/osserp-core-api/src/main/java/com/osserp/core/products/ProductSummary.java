/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 6, 2007 9:11:51 PM 
 * 
 */
package com.osserp.core.products;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductSummary extends Serializable {

    /**
     * 
     * Provides the stock id values belong to
     * @return stockId
     */
    Long getStockId();

    /**
     * Provides the quantity currently on stock
     * @return stock
     */
    Double getStock();

    /**
     * Provides the quantity currently on stock receipt
     * @return receipt
     */
    Double getReceipt();

    /**
     * Provides the quantity current quantity by sales receipt
     * @return salesReceipt
     */
    Double getSalesReceipt();

    /**
     * Provides the quantity currently expected from suppliers
     * @return expected
     */
    Double getExpected();

    /**
     * Provides the quantity currently ordered from customers
     * @return ordered
     */
    Double getOrdered();

    /**
     * Provides vacant quantity
     * @return vacant
     */
    Double getVacant();

    /**
     * Provides sum of ordered and vacant
     * @return orderedAndVacant
     */
    Double getOrderedAndVacant();

    /**
     * Provides the quantity currently in unreleased orders from customers
     * @return unreleased
     */
    Double getUnreleased();

    /**
     * Provides the available count on stock, e.g. summary of stock and receipt
     * @return availableStock
     */
    Double getAvailableStock();

    /**
     * Provides the availabilty, e.g. (expected + (stock + receipt) - ordered)
     * @return availableStock
     */
    Double getAvailability();

    /**
     * Provides the vacant availability
     * @return vacantAvailability
     */
    Double getVacantAvailability();

    /**
     * Provides the average purchase price
     * @return averagePurchasePrice
     */
    Double getAveragePurchasePrice();

    /**
     * Provides the last purchase price
     * @return lastPurchasePrice
     */
    Double getLastPurchasePrice();

    /**
     * Indicates that this summary is related to an product with open purchase orders, e.g. ordered < expected + receipt
     * @return purchaseOrdered
     */
    boolean isPurchaseOrdered();

    /**
     * Provides the last price paid
     * @return price
     */
    Double getLastPricePaid();

    /**
     * Sets the price
     * @param lastPricePaid
     */
    void setLastPricePaid(Double lastPricePaid);

    /**
     * Provides the date when last price was paid
     * @return date
     */
    Date getLastPricePaidOn();

    /**
     * Sets the date for the last price
     * @param lastPricePaidOn
     */
    void setLastPricePaidOn(Date lastPricePaidOn);

    /**
     * Marks summary as kanban (zeros counters)
     */
    void markAsKanban();
}
