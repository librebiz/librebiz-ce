/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 26, 2008 12:23:19 PM 
 * 
 */
package com.osserp.core.model.records;

import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordMailLog;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordMailLogImpl extends AbstractEntity implements RecordMailLog {

    private Long type = null;
    private String originator = null;
    private String recipient = null;
    private String recipientsCC = null;
    private String recipientsBCC = null;
    private String messageID = null;

    protected RecordMailLogImpl() {
        super();
    }

    public RecordMailLogImpl(
            Long createdBy,
            Record record,
            String originator,
            String recipient,
            String recipientsCC,
            String recipientsBCC,
            String messageID) {
        super((Long) null, record.getId(), createdBy);
        this.type = record.getType().getId();
        this.originator = originator;
        this.recipient = recipient;
        this.recipientsCC = recipientsCC;
        this.recipientsBCC = recipientsBCC;
        this.messageID = messageID;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getRecipientsCC() {
        return recipientsCC;
    }

    public void setRecipientsCC(String recipientsCC) {
        this.recipientsCC = recipientsCC;
    }

    public String getRecipientsBCC() {
        return recipientsBCC;
    }

    public void setRecipientsBCC(String recipientsBCC) {
        this.recipientsBCC = recipientsBCC;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

}
