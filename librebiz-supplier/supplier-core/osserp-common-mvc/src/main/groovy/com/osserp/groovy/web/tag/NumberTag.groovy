/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web.tag

import com.osserp.common.util.DateFormatter
import com.osserp.common.util.NumberFormatter

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class NumberTag extends AbstractInputTag {
    private static final String COUNT = 'count'
    private static final String CURRENCY = 'currency'
    private static final String DECIMAL = 'decimal'
    private static final String INTEGER = 'integer'
    private static final String HOURS = 'hours'
    private static final String PERCENT = 'percent'
    private static final String TAX = 'tax'

    String format

    @Override
    protected String getObjectValue() {
        if (value == null) {
            return ''
        }
        if (!format) {
            format = CURRENCY
        }
        try {
            if (format.equalsIgnoreCase(HOURS) && value instanceof Double) {
                return DateFormatter.getHoursAndMinutes((Double) value)
            }
            BigDecimal d
            if (value instanceof BigDecimal) {
                d = (BigDecimal) value
            } else if (value instanceof Double) {
                d = new BigDecimal(((Double) value).toString())
            } else if (value instanceof String) {
                d = new BigDecimal((String) value)
            }
            if (d != null) {
                // explicit check for null to avoid that a value of 0 is interpreted as null

                if (format.equalsIgnoreCase(CURRENCY)) {
                    return NumberFormatter.getValue(d, NumberFormatter.CURRENCY)
                } else if (format.equalsIgnoreCase(COUNT)) {
                    return NumberFormatter.getValue(d, NumberFormatter.COUNT)
                } else if (format.equalsIgnoreCase(INTEGER)) {
                    return NumberFormatter.getIntValue(d)
                } else if (format.equalsIgnoreCase(DECIMAL)) {
                    return NumberFormatter.getValue(d, NumberFormatter.DECIMAL)
                } else if (format.equalsIgnoreCase(PERCENT)) {
                    return NumberFormatter.getValue(d, NumberFormatter.PERCENT)
                } else if (format.equalsIgnoreCase(TAX)) {
                    return NumberFormatter.getValue(d, NumberFormatter.TAX_RATE)
                }
                return d.toPlainString()
            }
            return value.toString()
        } catch (Exception e) {
            return ''
        }
    }
}
