/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 10, 2006 3:02:30 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.BusinessCaseSearchRequest;
import com.osserp.core.Comparators;
import com.osserp.core.Options;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.dao.ProductPlanningCache;
import com.osserp.core.dao.ProjectQueries;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.finance.ReceivableDisplay;
import com.osserp.core.model.BusinessCaseRelationStatVO;
import com.osserp.core.projects.results.ProjectByActionDate;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.sales.SalesListItemSorter;
import com.osserp.core.sales.SalesListProductItem;
import com.osserp.core.sales.SalesMonitoringItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectQueriesDao extends AbstractBusinessCaseContextDao implements ProjectQueries {
    private static Logger log = LoggerFactory.getLogger(ProjectQueriesDao.class.getName());
    private ProductPlanningCache planning = null;
    private FlowControlActions actionsDao = null;
    private String projectMountingsView = null;
    private String deliveryMonitoringView = null;
    private String salesDeliveryListView = null;
    private String salesListView = null;
    private String salesMonitoringView = null;
    private String salesReceivablesView = null;
    private String salesRecordIdsView = null;
    private String salesSearchView = null;
    private String salesWithMissingInvoiceView = null;
    private String serviceSalesView = null;
    private String requestTypesTable = null;

    public ProjectQueriesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            FlowControlActions flowControlActions,
            ProductPlanningCache productPlanningCache) {
        super(jdbcTemplate, tables, sessionFactory, cache);
        actionsDao = flowControlActions;
        planning = productPlanningCache;
        salesDeliveryListView = getTable(TableKeys.SALES_DELIVERY_LIST);
        projectMountingsView = getTable(TableKeys.PROJECT_MOUNTINGS_VIEW);
        deliveryMonitoringView = getTable(TableKeys.DELIVERY_MONITORING_VIEW);
        salesListView = getTable(TableKeys.SALES_LIST_VIEW);
        salesMonitoringView = getTable(TableKeys.SALES_MONITORING_VIEW);
        salesReceivablesView = getTable(TableKeys.SALES_RECEIVABLES_VIEW);
        salesRecordIdsView =  getTable(TableKeys.SALES_RECORD_IDS_VIEW);
        salesSearchView =  getTable(TableKeys.SALES_SEARCH_VIEW);
        salesWithMissingInvoiceView = getTable(TableKeys.SALES_WITH_MISSING_INVOICE);
        serviceSalesView = getTable(TableKeys.OPEN_SERVICE_SALES);
        requestTypesTable = getTable(Options.REQUEST_TYPES);
    }

    public List<SalesListProductItem> getProjectsByDelivery() {
        return addDeliveryStatus((List<SalesListProductItem>) jdbcTemplate.query(
                SalesListProductDeliveryRowMapper.createQueryForAll(salesDeliveryListView).toString(),
                SalesListProductDeliveryRowMapper.getReader(getOptionsCache(), "delivery")));
    }

    public List<SalesListItem> getProjectsByMounting() {
        return (List<SalesListItem>) jdbcTemplate.query(
                ProjectMountingRowMapper.createQueryForAll(projectMountingsView).toString(),
                ProjectMountingRowMapper.getReader(getOptionsCache()));
    }

    public List<ProjectByActionDate> getByActionAndDate(
            Long projectType,
            Long actionId,
            Date from,
            Date til,
            boolean withCanceled,
            boolean withClosed,
            String zipcode) {
        if (log.isDebugEnabled()) {
            log.debug("getByActionAndDate() invoked [type=" + projectType + ", action=" + actionId + "]");
        }

        final Object[] params = {
                projectType,
                actionId,
                new Timestamp(from.getTime()),
                new Timestamp(til.getTime()),
                withCanceled,
                withClosed,
                zipcode
        };
        final int[] types = {
                Types.BIGINT,
                Types.BIGINT,
                Types.TIMESTAMP,
                Types.TIMESTAMP,
                Types.BOOLEAN,
                Types.BOOLEAN,
                Types.VARCHAR
        };
        return (List<ProjectByActionDate>) jdbcTemplate.query(
                ProjectProcs.PROJECTS_BY_ACTION,
                params,
                types,
                ProjectByActionRowMapper.getReader(getOptionsCache(), actionsDao));
    }

    public List<ProjectByActionDate> getByMissingAction(
            Long projectType,
            Long missingAction,
            Long withAction,
            boolean withCanceled,
            boolean withClosed) {
        
        if (log.isDebugEnabled()) {
            log.debug("getByMissingAction() invoked [type=" + projectType + ", action=" + missingAction + "]");
        }

        final Object[] params = {
                projectType,
                missingAction,
                getLong(withAction),
                withCanceled,
                withClosed
        };
        final int[] types = {
                Types.BIGINT,
                Types.BIGINT,
                Types.BIGINT,
                Types.BOOLEAN,
                Types.BOOLEAN
        };
        return (List<ProjectByActionDate>) jdbcTemplate.query(
                ProjectProcs.PROJECTS_WITHOUT_ACTION,
                params,
                types,
                ProjectByActionRowMapper.getReader(getOptionsCache(), actionsDao));
    }

    public List<SalesListItem> getByManager(Long managerId, boolean openOnly) {
        if (log.isDebugEnabled()) {
            log.debug("getByManager() invoked [id=" + managerId + ", openonly=" + openOnly + "]");
        }
        final Object[] params = { managerId, Boolean.valueOf(openOnly) };
        final int[] types = { Types.BIGINT, Types.BOOLEAN };
        return (List<SalesListItem>) jdbcTemplate.query(
                ProjectProcs.MANAGER_PROJECTS,
                params,
                types,
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<SalesListItem> getBySalesperson(Long salesId, boolean openOnly) {
        if (log.isDebugEnabled()) {
            log.debug("getBySalesperson() invoked [id=" + salesId + ", openonly=" + openOnly + "]");
        }
        final Object[] params = { salesId, Boolean.valueOf(openOnly) };
        final int[] types = { Types.BIGINT, Types.BOOLEAN };
        return (List<SalesListItem>) jdbcTemplate.query(
                ProjectProcs.SALES_PROJECTS,
                params,
                types,
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<SalesListItem> getByExistingInvoice(Date from, Date til) {
        if (log.isDebugEnabled()) {
            log.debug("getByExistingInvoice() invoked [from=" + from + ", til=" + til + "]");
        }
        final Object[] params = { from, til };
        final int[] types = { Types.TIMESTAMP, Types.TIMESTAMP };
        return (List<SalesListItem>) jdbcTemplate.query(
                ProjectProcs.PROJECTS_BY_EXISTING_INVOICE,
                params,
                types,
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<SalesListItem> getByMissingInvoices() {
        if (log.isDebugEnabled()) {
            log.debug("getByMissingInvoices() invoked");
        }
        StringBuilder sql = new StringBuilder("SELECT * FROM ");
        sql.append(salesWithMissingInvoiceView);
        return (List<SalesListItem>) jdbcTemplate.query(
                sql.toString(),
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<ReceivableDisplay> getByReceivables() {
        if (log.isDebugEnabled()) {
            log.debug("getByReceivables() invoked");
        }
        StringBuilder sql = new StringBuilder("SELECT * FROM ");
        sql
                .append(salesReceivablesView)
                .append(" WHERE amount > 0")
                .append(" ORDER BY id DESC");
        return (List<ReceivableDisplay>) jdbcTemplate.query(
                sql.toString(),
                ReceivableDisplayRowMapper.getReader());
    }

    public List<SalesListItem> getByCancelledStatus() {
        if (log.isDebugEnabled()) {
            log.debug("getByCancelledStatus() invoked");
        }
        return (List<SalesListItem>) jdbcTemplate.query(
                ProjectProcs.PROJECTS_BY_CANCELLED_STATUS,
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<SalesListItem> getByCancelledStatus(int year) {
        if (log.isDebugEnabled()) {
            log.debug("getByCancelledStatus() invoked [year=" + year + "]");
        }
        List<SalesListItem> list = getByCancelledStatus();
        clearByYear(list, year);
        return list;
    }

    public List<SalesListItem> getByStoppedStatus() {
        if (log.isDebugEnabled()) {
            log.debug("getByStoppedStatus() invoked");
        }
        return (List<SalesListItem>) jdbcTemplate.query(
                ProjectProcs.PROJECTS_BY_STOPPED_STATUS,
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<SalesListItem> getByStoppedStatus(int year) {
        if (log.isDebugEnabled()) {
            log.debug("getByStoppedStatus() invoked [year=" + year + "]");
        }
        List<SalesListItem> list = getByStoppedStatus();
        clearByYear(list, year);
        return list;
    }

    public List<SalesListItem> getCreatedInMonth(int month, int year) {
        if (log.isDebugEnabled()) {
            log.debug("getCreatedInMonth() invoked [month=" + month + ", year=" + year + "]");
        }
        final Object[] params = { month, year };
        final int[] types = { Types.INTEGER, Types.INTEGER };
        return (List<SalesListItem>) jdbcTemplate.query(
                ProjectProcs.PROJECTS_CREATED_IN_MONTH,
                params,
                types,
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<SalesListItem> getCreatedInYear(int year) {
        if (log.isDebugEnabled()) {
            log.debug("getCreatedInYear() invoked [year=" + year + "]");
        }
        final Object[] params = { year };
        final int[] types = { Types.INTEGER };
        return (List<SalesListItem>) jdbcTemplate.query(
                ProjectProcs.PROJECTS_CREATED_IN_YEAR,
                params,
                types,
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<SalesListItem> getById(Long[] ids, String orderBy, boolean desc) {
        StringBuilder sql = new StringBuilder("SELECT * FROM ");
        sql
                .append(salesListView)
                .append(" WHERE id IN (")
                .append(StringUtil.createCommaSeparated(ids));
        if (!isEmpty(orderBy)) {
            sql.append(" ORDER BY ").append(orderBy);
            if (desc) {
                sql.append(" DESC");
            }
        }
        return (List<SalesListItem>) jdbcTemplate.query(
                sql.toString(),
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<SalesListItem> findServiceSales(Long salesId) {
        if (log.isDebugEnabled()) {
            log.debug("findServiceSales() invoked [sales=" + salesId + "]");
        }
        StringBuilder sql = new StringBuilder("SELECT * FROM ");
        sql
                .append(serviceSalesView)
                .append(" WHERE project_id = ")
                .append(salesId)
                .append(" ORDER BY id DESC");
        return (List<SalesListItem>) jdbcTemplate.query(
                sql.toString(),
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
    }

    public List<SalesListItem> getSleepingProjects(int maxDaysAgo) {
        if (log.isDebugEnabled()) {
            log.debug("getSleepingProjects() invoked [days=" + maxDaysAgo + "]");
        }
        final Object[] params = { maxDaysAgo };
        final int[] types = { Types.INTEGER };
        return (List<SalesListItem>) jdbcTemplate.query(
                ProjectProcs.PROJECTS_WITHOUT_ACTIVITY,
                params,
                types,
                SalesListItemRowMapper.getReader(getOptionsCache(), true));
    }

    public List<SalesListItem> find(BusinessCaseSearchRequest request) {
        StringBuilder buffer = new StringBuilder("SELECT DISTINCT ON (id) * FROM ");
        buffer.append(salesSearchView);
        boolean append = false;
        if (!isEmpty(request.getBusinessTypeId())) {
            buffer.append(" WHERE type_id = ").append(request.getBusinessTypeId());
            append = true;
        }
        if (!isEmpty(request.getBranchId())) {
            if (!append) {  
                buffer.append(" WHERE");
                append = true;
            } else {
                buffer.append(" AND"); 
            }  
            buffer.append(" branch_id = ").append(request.getBranchId());
        }
        if (!request.isIncludeCanceled()) {
            if (!append) {  
                buffer.append(" WHERE");
                append = true;
            } else {
                buffer.append(" AND"); 
            }  
            buffer.append(" NOT cancelled");
        }
        if (request.isOpenOnly()) {
            if (!append) {  
                buffer.append(" WHERE");
                append = true;
            } else {
                buffer.append(" AND"); 
            }  
            buffer.append(" status < 100");
        }
        if (!isEmpty(request.getPattern())) {
            if (!append) {  
                buffer.append(" WHERE");
                append = true;
            } else {
                buffer.append(" AND"); 
            }  
            buffer.append(" (");
            buffer.append(bindColumnToRegExPattern(
                (isEmpty(request.getColumnKey()) ? "idx" : request.getColumnKey()), 
                request.getPattern(), 
                request.isStartsWith()));
        
            Long id = request.getPatternAsNumber();
            if (isEmpty(id)) {
                buffer.append(")");
            } else {
                buffer
                    .append(" OR id IN (SELECT sales_id FROM ")
                    .append(salesRecordIdsView)
                    .append(" WHERE id::varchar ~* '")
                    .append(id).append("'))");
            }
        } else {
            buffer.append(" ORDER BY id DESC");
        }
        if (!request.isIgnoreFetchSize() && request.getFetchSize() > 0) {
            buffer.append(" LIMIT ").append(request.getFetchSize());
        }
        String queryString = buffer.toString();
        List<SalesListItem> result = (List<SalesListItem>) jdbcTemplate.query(queryString,
                SalesListItemRowMapper.getReader(getOptionsCache(), false));
        if (log.isDebugEnabled()) {
            log.debug("find() done [sql=" + queryString + ", found=" + result.size() + "]");
        }
        return result;
    }

    public List<SalesListProductItem> getDeliveryMonitoring(Long stockId, boolean descendant) {
        if (log.isDebugEnabled()) {
            log.debug("getDeliveryMonitoring() invoked [stock=" + stockId + ", descendant=" + descendant + "]");
        }
        StringBuilder query = SalesListProductDeliveryRowMapper.createQueryForAll(deliveryMonitoringView);
        if (stockId != null) {
            query.append(" WHERE stock_id = ").append(stockId);
        }
        query.append(" ORDER BY delivery_date");
        if (descendant) {
            query.append(" DESC");
        }
        return (List<SalesListProductItem>) jdbcTemplate.query(query.toString(),
                SalesListProductDeliveryRowMapper.getReader(getOptionsCache(), "delivery_date"));
    }

    public List<SalesMonitoringItem> getSalesMonitoring(String requestTypeSelection) {
        if (log.isDebugEnabled()) {
            log.debug("getSalesMonitoring() invoked [requestTypeSelection=" + requestTypeSelection + "]");
        }
        List<SalesMonitoringItem> result = new ArrayList<SalesMonitoringItem>();
        try {
            List<SalesMonitoringRow> rows = (List<SalesMonitoringRow>) jdbcTemplate.query(
                SalesMonitoringRowMapper.SALES_MONITORING,
                new Object[] { requestTypeSelection },
                new int[] { Types.VARCHAR },
                SalesMonitoringRowMapper.getReader(getOptionsCache()));
            Map<Long, SalesMonitoringItem> map = new HashMap<Long, SalesMonitoringItem>();
            for (int i = 0, j = rows.size(); i < j; i++) {
                SalesMonitoringRow row = rows.get(i);
                SalesMonitoringRowMapper.addItem(map, row);
            }
            result = new ArrayList<SalesMonitoringItem>(map.values());
            if (log.isDebugEnabled()) {
                log.debug("getSalesMonitoring() done [requestTypeSelection=" + requestTypeSelection
                    + ", count=" + result.size()
                    + "]");
            }
        } catch (Exception e) {
            log.error("getSalesMonitoring() caught exception [message=" + e.getMessage() + "]", e);
        }
        return result;
    }

    public List<SalesMonitoringItem> getSalesMonitoring(Long company) {
        if (log.isDebugEnabled()) {
            log.debug("getSalesMonitoring() invoked [company=" + company + "]");
        }
        StringBuilder query = new StringBuilder(SalesMonitoringRowMapper.SELECT_VALUES_FROM);
        query
                .append(salesMonitoringView)
                .append(" WHERE type_id IN (SELECT id FROM ")
                .append(requestTypesTable)
                .append(" WHERE company_id = ")
                .append(company)
                .append(")");
        List<SalesMonitoringRow> rows = (List<SalesMonitoringRow>) jdbcTemplate.query(query.toString(),
                SalesMonitoringRowMapper.getReader(getOptionsCache()));
        Map<Long, SalesMonitoringItem> map = new HashMap<Long, SalesMonitoringItem>();
        for (int i = 0, j = rows.size(); i < j; i++) {
            SalesMonitoringRow row = rows.get(i);
            if (SalesMonitoringRowMapper.addItem(map, row)) {
                SalesMonitoringItem item = map.get(row.id);
                item.setOpenAmount(getAccountBalance(row.id));
            }
        }
        List<SalesMonitoringItem> result = new ArrayList<SalesMonitoringItem>(map.values());
        if (log.isDebugEnabled()) {
            log.debug("getSalesMonitoring() done [company=" + company + ", count=" + result.size() + "]");
        }
        return result;
    }
    
    public List<BusinessCaseRelationStat> getSubcontractors() {
        List<SalesMonitoringItem> all = getSubcontractorMonitoring((Long) null);
        Map<Long, BusinessCaseRelationStatVO> contractorStats = new HashMap<>();
        for (int i = 0, j = all.size(); i < j; i++) {
            SalesMonitoringItem item = all.get(i);
            if (item.getInstallerId() != null) {
                if (contractorStats.containsKey(item.getInstallerId())) {
                    contractorStats.get(item.getInstallerId()).addItem(item);
                } else {
                    Option installer = getOption(Options.SUPPLIERS, item.getInstallerId());
                    contractorStats.put(
                            installer.getId(), 
                            new BusinessCaseRelationStatVO(item, installer.getName()));
                }
            }
        }
        List<BusinessCaseRelationStat> result = new ArrayList<>();
        for (Iterator<Map.Entry<Long, BusinessCaseRelationStatVO>> i = contractorStats.entrySet().iterator(); i.hasNext();) {
            Map.Entry<Long, BusinessCaseRelationStatVO> o = i.next();
            result.add(o.getValue());
        }
        return CollectionUtil.sort(result, Comparators.businessCaseRelationStatByNameComparator(false));
    }

    public List<SalesMonitoringItem> getSubcontractorMonitoring(Long contractorId) {
        StringBuilder query = new StringBuilder(SalesMonitoringRowMapper.SUBCONTRACTOR_MONITORING);
        if (!isEmpty(contractorId)) {
            query.append(" WHERE installer_id = ").append(contractorId);
        }
        List<SalesMonitoringRow> rows = (List<SalesMonitoringRow>) jdbcTemplate.query(
                query.toString(), SalesMonitoringRowMapper.getReader(getOptionsCache()));
        Map<Long, SalesMonitoringItem> map = new HashMap<Long, SalesMonitoringItem>();
        for (int i = 0, j = rows.size(); i < j; i++) {
            SalesMonitoringRow row = rows.get(i);
            SalesMonitoringRowMapper.addItem(map, row);
        }
        List<SalesMonitoringItem> result = new ArrayList<SalesMonitoringItem>(map.values());
        if (log.isDebugEnabled()) {
            log.debug("getSubcontractorMonitoring() done [id=" + contractorId + ", count=" + result.size() + "]");
        }
        SalesListItemSorter.instance().sort("byStatus", new ArrayList(result));
        return result;
    }

    private void clearByYear(List<SalesListItem> list, Integer year) {
        if (year != null && year.intValue() > 2001) {
            for (Iterator<SalesListItem> i = list.iterator(); i.hasNext();) {
                SalesListItem next = i.next();
                int nextYear = DateUtil.getYear(next.getCreated());
                if (nextYear != year) {
                    i.remove();
                }
            }
        }
    }

    private List<SalesListProductItem> addDeliveryStatus(List<SalesListProductItem> list) {
        if (planning != null) {
            for (int i = 0, j = list.size(); i < j; i++) {
                SalesListProductItem pa = list.get(i);
                pa.setVacant(!planning.isAvailableForDelivery(pa.getSalesId(), pa.getStatus()));
            }
        }
        return list;
    }
}
