SET search_path = osserp, pg_catalog;

UPDATE system_properties SET order_id = order_id + 1 WHERE context_name = 'documentConfig' AND order_id > 4 AND order_id < 30;
INSERT INTO system_properties VALUES (220, 'purchaseInvoicePrintHeaderDefault', 'Retoure', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (221, 'documentPrintFontSizeHeaderSubject', '11pt', 'java.lang.String', true, false, 'documentConfig', false, 5);
SELECT pg_catalog.setval('system_properties_id_seq', 221, true);
UPDATE purchase_invoice_types SET printable = true WHERE id = 255;

UPDATE documents SET file_size = 11671 WHERE file_name = 'xsl/records/xsl_rtp_1.vm';
UPDATE documents SET file_size = 8545 WHERE file_name = 'xsl/records/xsl_rtp_2.vm';
UPDATE documents SET file_size = 9412 WHERE file_name = 'xsl/records/xsl_rtp_3.vm';
UPDATE documents SET file_size = 35927 WHERE file_name = 'xsl/records/xsl_rtp_4.vm';
UPDATE documents SET file_size = 39702 WHERE file_name = 'xsl/records/xsl_rtp_5.vm';
UPDATE documents SET file_size = 6852 WHERE file_name = 'xsl/records/xsl_rtp_9.vm';
UPDATE documents SET file_size = 4441 WHERE file_name = 'xsl/records/xsl_rtp_10.vm';
UPDATE documents SET file_size = 8518 WHERE file_name = 'xsl/records/xsl_rtp_102.vm';
UPDATE documents SET file_size = 3358 WHERE file_name = 'xsl/records/xsl_rtp_105.vm';

UPDATE system_properties SET pvalue = '0.15.4', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
