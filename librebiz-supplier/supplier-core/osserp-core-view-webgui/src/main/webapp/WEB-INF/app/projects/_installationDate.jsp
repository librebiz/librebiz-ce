<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="installationDateView" />
<style type="text/css">
</style>
<div class="row row-modal">
    <div class="col-md-6 panel-area panel-area-modal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="table-responsive">
                    <table class="table table-head">
                        <tbody>
                            <tr>
                                <td><h4><fmt:message key="installationDate" /></h4></td>
                                <td class="action">
                                    <c:if test="${!view.createMode and !view.editMode}">
                                        <v:ajaxLink url="/projects/installationDate/enableEditMode" linkId="createLink" targetElement="installationDate_popup">
                                            <o:img name="newIcon" />
                                        </v:ajaxLink>
                                    </c:if>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <c:choose>
                <c:when test="${!view.editMode}">
                    <div class="table-responsive table-responsive-default">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><fmt:message key="appointment" /></th>
                                    <th><fmt:message key="uploadBy" /></th>
                                    <th><fmt:message key="adjustedAt" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:choose>
                                    <c:when test="${empty view.dates}">
                                        <tr>
                                            <td colspan="3"><fmt:message key="noAppointmentCreated" /></td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="tdate" items="${view.dates}">
                                            <tr>
                                                <td><o:date value="${tdate.date}" /></td>
                                                <td><oc:employee value="${tdate.createdBy}" /></td>
                                                <td><o:date value="${tdate.created}" /></td>
                                            </tr>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                </c:when>
                <c:otherwise>
                    <v:ajaxForm name="dynamicForm" targetElement="installationDate_popup" url="/projects/installationDate/save">
                        <div class="form-body">
                        
                            <v:date name="date" styleClass="form-control" label="newAppointment" dateCol="4" picker="true" />

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="estimatedDuration" />
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <v:text name="duration" styleClass="form-control"  />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <fmt:message key="days" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row next">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <v:ajaxLink url="/projects/installationDate/disableEditMode" linkId="exitLink" targetElement="installationDate_popup">
                                                    <input type="button" class="form-control cancel" value="<fmt:message key="exit"/>"/>
                                                </v:ajaxLink>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <o:submit value="save" styleClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                    </v:ajaxForm>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
