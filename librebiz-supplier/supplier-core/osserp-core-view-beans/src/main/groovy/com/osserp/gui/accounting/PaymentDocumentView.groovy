/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 8, 2015
 * 
 */
package com.osserp.gui.accounting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dms.DmsReference
import com.osserp.core.finance.CommonPayment
import com.osserp.core.finance.CommonPaymentManager
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.dms.AbstractDocumentView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class PaymentDocumentView extends AbstractDocumentView {
    private static Logger logger = LoggerFactory.getLogger(PaymentDocumentView.class.getName())

    CommonPayment payment

    PaymentDocumentView() {
        super()
        dependencies = ['commonPaymentView']
    }

    @Override
    protected DmsReference getDmsReference() {
        new DmsReference(CommonPayment.DOCUMENT_TYPE, payment.id)
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        logger.debug("initRequest: invoked")
        if (!list) {
            if (!env.commonPaymentView?.bean) {
                logger.warn("initRequest: not bound [user=${user?.id}, name=commonPaymentView]")
                throw new ViewContextException(this)
            }
            payment = env.commonPaymentView.bean
            reload()
        }
    }
    
    @Override
    void setReference() {
        super.setReference()
        if (lastChangedId) {
            env.commonPaymentView.bean = commonPaymentManager.updateDocumentReference(
                domainEmployee, payment, lastChangedId)
            payment = env.commonPaymentView.bean
        }
    }
    
    void delete() {
        if (lastChangedId == payment.documentId) {
            env.commonPaymentView.bean = commonPaymentManager.updateDocumentReference(
                domainEmployee, payment, null)
            payment = env.commonPaymentView.bean
        }
    }
    
    protected CommonPaymentManager getCommonPaymentManager() {
        getService(CommonPaymentManager.class.getName())
    }
}
