/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 18, 2006 2:21:12 PM 
 * 
 */
package com.osserp.core;

import java.util.Comparator;
import java.util.Date;

import com.osserp.common.Entity;
import com.osserp.common.Option;
import com.osserp.common.Permission;
import com.osserp.common.Property;
import com.osserp.common.User;
import com.osserp.common.mail.EmailAddress;

import com.osserp.core.contacts.Contact;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeDisplay;
import com.osserp.core.employees.EmployeeRoleDisplay;
import com.osserp.core.events.Event;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordMarker;
import com.osserp.core.planning.ProductPlanningSummary;
import com.osserp.core.products.Product;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class Comparators {
    //	private static Logger log = LoggerFactory.getLogger(Comparators.class.getName());

    public static Comparator createEntityComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Entity) a).getId().compareTo(((Entity) b).getId());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Entity) a).getId().compareTo(((Entity) b).getId());
            }
        };
    }

    public static Comparator createEntityByCreatedComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Entity entityA = (Entity) a;
                    Entity entityB = (Entity) b;
                    Date dateA = entityA.getCreated();
                    Date dateB = entityB.getCreated();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Entity entityA = (Entity) a;
                Entity entityB = (Entity) b;
                Date dateA = entityA.getCreated();
                Date dateB = entityB.getCreated();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static Comparator createOptionIdComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Option) a).getId().compareTo(((Option) b).getId());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Option) a).getId().compareTo(((Option) b).getId());
            }
        };
    }

    public static Comparator createSupplierNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Supplier) a).getDisplayName().compareTo(((Supplier) b).getDisplayName());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Supplier) a).getDisplayName().compareToIgnoreCase(((Supplier) b).getDisplayName());
            }
        };
    }

    public static Comparator createOptionNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Option) a).getName().compareTo(((Option) b).getName());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Option) a).getName().compareToIgnoreCase(((Option) b).getName());
            }
        };
    }

    public static Comparator createContactNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Contact) a).getDisplayName().compareTo(((Contact) b).getDisplayName());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Contact) a).getDisplayName().compareToIgnoreCase(((Contact) b).getDisplayName());
            }
        };
    }

    public static Comparator createCustomerNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Customer) a).getDisplayName().compareTo(((Customer) b).getDisplayName());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Customer) a).getDisplayName().compareToIgnoreCase(((Customer) b).getDisplayName());
            }
        };
    }

    public static Comparator createEmployeeNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((Employee) a).getDisplayName().compareTo(((Employee) b).getDisplayName());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((Employee) a).getDisplayName().compareToIgnoreCase(((Employee) b).getDisplayName());
            }
        };
    }

    public static Comparator createEmployeeDisplayNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((EmployeeDisplay) a).getName().compareTo(((EmployeeDisplay) b).getName());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((EmployeeDisplay) a).getName().compareToIgnoreCase(((EmployeeDisplay) b).getName());
            }
        };
    }

    public static Comparator createEmployeeRoleDisplayByEmployeeComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                String sA = ((EmployeeRoleDisplay) a).getEmployee();
                String sB = ((EmployeeRoleDisplay) b).getEmployee();
                if (sA == null && sB == null) {
                    return 0;
                }
                if (sA == null) {
                    return 1;
                }
                if (sB == null) {
                    return -1;
                }
                return sA.compareTo(sB);
            }
        };
    }

    public static Comparator createEmailByContactComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    String emailA = ((Contact) a).getEmail();
                    String emailB = ((Contact) b).getEmail();
                    int result = (emailA.compareToIgnoreCase(emailB));
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                String emailA = ((Contact) a).getEmail();
                String emailB = ((Contact) b).getEmail();
                return (emailA.compareToIgnoreCase(emailB));
            }
        };
    }

    public static Comparator createEmailComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    EmailAddress emailA = (EmailAddress) a;
                    EmailAddress emailB = (EmailAddress) b;
                    int result = (emailA.getEmail().compareToIgnoreCase(emailB.getEmail()));
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                EmailAddress emailA = (EmailAddress) a;
                EmailAddress emailB = (EmailAddress) b;
                return (emailA.getEmail().compareToIgnoreCase(emailB.getEmail()));
            }
        };
    }

    public static Comparator createEventDateComparator(boolean reverse, final boolean created) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Event eventA = (Event) a;
                    Event eventB = (Event) b;
                    Date dateA = created ? eventA.getCreated() : eventA.getEventDate();
                    Date dateB = created ? eventB.getCreated() : eventB.getEventDate();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Event eventA = (Event) a;
                Event eventB = (Event) b;
                Date dateA = created ? eventA.getCreated() : eventA.getEventDate();
                Date dateB = created ? eventB.getCreated() : eventB.getEventDate();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static Comparator createEventReferenceComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Event eventA = (Event) a;
                    Event eventB = (Event) b;
                    Long refA = eventA.getReferenceId();
                    Long refB = eventB.getReferenceId();
                    if (refA == null && refB == null) {
                        return 0;
                    }
                    if (refA == null) {
                        return -1;
                    }
                    if (refB == null) {
                        return 1;
                    }
                    return refB.compareTo(refA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Event eventA = (Event) a;
                Event eventB = (Event) b;
                Long refA = eventA.getReferenceId();
                Long refB = eventB.getReferenceId();
                if (refA == null && refB == null) {
                    return 0;
                }
                if (refA == null) {
                    return 1;
                }
                if (refB == null) {
                    return -1;
                }
                return refA.compareTo(refB);
            }
        };
    }

    public static Comparator createRecordItemByIdComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
            	
            	Item itemA = (Item) a;
            	Item itemB = (Item) b;
                Long idA = itemA.getId();
                Long idB = itemB.getId();
                if (idA == null && idB == null) {
                    return 0;
                }
                if (idA == null) {
                    return 1;
                }
                if (idB == null) {
                    return -1;
                }
                return idA.compareTo(idB);
            }
        };
    }

    public static Comparator createOrderItemsDeliveryDateComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                OrderItemsDisplay orderA = (OrderItemsDisplay) a;
                OrderItemsDisplay orderB = (OrderItemsDisplay) b;
                Date dateA = orderA.getDelivery();
                Date dateB = orderB.getDelivery();
                /*
                 * if (log.isDebugEnabled()) { log.debug("compare() invoked for product [" + orderA.getProductId() + "], date a [" + dateA + "], date b [" +
                 * dateB + "]"); }
                 */
                if (dateA == null && dateB == null) {
                    if (orderA.isSalesItem() && orderB.isSalesItem()) {
                        if ((orderA.isVacant() && orderB.isVacant())
                                || (!orderA.isVacant() && !orderB.isVacant())) {
                            if (orderA.getStatus().equals(orderB.getStatus())) {
                                return orderA.getCreated().compareTo(orderB.getCreated());
                            }
                            return orderA.getStatus().compareTo(orderB.getStatus());
                        }
                        if (orderA.isVacant()) {
                            return 1;
                        }
                        return -1;
                    }
                    if (!orderA.isSalesItem()) {
                        return -1;
                    }
                    if (!orderB.isSalesItem()) {
                        return 1;
                    }
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                if (dateA.equals(dateB)) {
                    /*
                     * if (log.isDebugEnabled()) { log.debug("compare() found equal date [" + orderA.getProductId() + "]"); }
                     */
                    if (orderA.isSalesItem() && orderB.isSalesItem()) {
                        /*
                         * if (log.isDebugEnabled()) { log.debug("compare() both items of type sales, comparing by status"); }
                         */
                        return orderA.getStatus().compareTo(orderB.getStatus());
                    }
                    if (!orderA.isSalesItem() && !orderB.isSalesItem()) {
                        /*
                         * if (log.isDebugEnabled()) { log.debug("compare() both items of type purchase, comparing by created"); }
                         */
                        return orderA.getCreated().compareTo(orderB.getCreated());
                    }
                    if (!orderA.isSalesItem()) {
                        /*
                         * if (log.isDebugEnabled()) { log.debug("compare() [a] is purchase item [" + orderB.getId() + "] result [-1]");
                         * 
                         * }
                         */
                        return -1;
                    }
                    /*
                     * if (log.isDebugEnabled()) { log.debug("compare() [b] is purchase item [" + orderB.getId() + "] result [1]");
                     * 
                     * }
                     */
                    return 1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static Comparator createProductByNameComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                String sA = ((Product) a).getName();
                String sB = ((Product) b).getName();
                if (sA == null && sB == null) {
                    return 0;
                }
                if (sA == null) {
                    return 1;
                }
                if (sB == null) {
                    return -1;
                }
                return sA.compareTo(sB);
            }
        };
    }

    public static Comparator createProductPlanningByNameComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                String sA = ((ProductPlanningSummary) a).getProductName();
                String sB = ((ProductPlanningSummary) b).getProductName();
                if (sA == null && sB == null) {
                    return 0;
                }
                if (sA == null) {
                    return 1;
                }
                if (sB == null) {
                    return -1;
                }
                return sA.compareTo(sB);
            }
        };
    }

    public static Comparator createBusinessTypeComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                String sA = ((BusinessType) a).getName();
                String sB = ((BusinessType) b).getName();
                return sA.compareTo(sB);
            }
        };
    }

    public static Comparator businessCaseRelationStatByNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((BusinessCaseRelationStat) a).getName().compareTo(((BusinessCaseRelationStat) b).getName());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((BusinessCaseRelationStat) a).getName().compareToIgnoreCase(((BusinessCaseRelationStat) b).getName());
            }
        };
    }

    public static Comparator createPermissionComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                Permission pa = (Permission) a;
                Permission pb = (Permission) b;
                String sA = pa != null ? pa.getDescription() : null;
                String sB = pb != null ? pb.getDescription() : null;
                if (sA == null && sB == null) {
                    return 0;
                }
                if (sA == null) {
                    return 1;
                }
                if (sB == null) {
                    return -1;
                }
                return sA.compareTo(sB);
            }
        };
    }

    public static Comparator createPropertyByOrderIdComparator(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Property propA = (Property) a;
                    Property propB = (Property) b;
                    if (Integer.valueOf(propA.getOrderId()) == 0 && Integer.valueOf(propB.getOrderId()) == 0) {
                        return 0;
                    }
                    if (Integer.valueOf(propA.getOrderId()) == 0) {
                        return -1;
                    }
                    if (Integer.valueOf(propB.getOrderId()) == 0) {
                        return 1;
                    }
                    return Comparators.compare(Integer.valueOf(propA.getOrderId()), Integer.valueOf(propB.getOrderId()), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Property propA = (Property) a;
                Property propB = (Property) b;
                if (Integer.valueOf(propA.getOrderId()) == 0 && Integer.valueOf(propB.getOrderId()) == 0) {
                    return 0;
                }
                if (Integer.valueOf(propA.getOrderId()) == 0) {
                    return 1;
                }
                if (Integer.valueOf(propB.getOrderId()) == 0) {
                    return -1;
                }
                return Comparators.compare(Integer.valueOf(propA.getOrderId()), Integer.valueOf(propB.getOrderId()), false);
            }
        };
    }

    public static Comparator createUserLoginNameComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                String sA = ((User) a).getLoginName();
                String sB = ((User) b).getLoginName();
                if (sA == null && sB == null) {
                    return 0;
                }
                if (sA == null) {
                    return 1;
                }
                if (sB == null) {
                    return -1;
                }
                return sA.compareTo(sB);
            }
        };
    }

    public static Comparator createTimeRecordComparator(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    TimeRecord recordA = (TimeRecord) a;
                    TimeRecord recordB = (TimeRecord) b;
                    Date dateA = recordA.getValue();
                    Date dateB = recordB.getValue();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                TimeRecord recordA = (TimeRecord) a;
                TimeRecord recordB = (TimeRecord) b;
                Date dateA = recordA.getValue();
                Date dateB = recordB.getValue();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static Comparator createTimeRecordMarkerComparator(boolean descendant) {
        if (descendant) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    TimeRecordMarker recordA = (TimeRecordMarker) a;
                    TimeRecordMarker recordB = (TimeRecordMarker) b;
                    Date dateA = recordA.getMarkerDate();
                    Date dateB = recordB.getMarkerDate();
                    if (dateA == null && dateB == null) {
                        return 0;
                    }
                    if (dateA == null) {
                        return -1;
                    }
                    if (dateB == null) {
                        return 1;
                    }
                    return dateB.compareTo(dateA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                TimeRecordMarker recordA = (TimeRecordMarker) a;
                TimeRecordMarker recordB = (TimeRecordMarker) b;
                Date dateA = recordA.getMarkerDate();
                Date dateB = recordB.getMarkerDate();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    public static int compare(String nameA, String nameB, boolean reverse) {
        if (nameA == null && nameB == null) {
            return 0;
        }
        if (nameA == null) {
            return reverse ? 1 : -1;
        }
        if (nameB == null) {
            return reverse ? -1 : 1;
        }
        int result = nameA.compareTo(nameB);
        return reverse ? result * (-1) : result;
    }

    public static int compare(Date dateA, Date dateB, boolean reverse) {
        if (dateA == null && dateB == null) {
            return 0;
        }
        if (dateA == null) {
            return reverse ? 1 : -1;
        }
        if (dateB == null) {
            return reverse ? -1 : 1;
        }
        int result = dateA.compareTo(dateB);
        return reverse ? result * (-1) : result;
    }

    public static int compare(Integer intA, Integer intB, boolean reverse) {
        if (intA == null && intB == null) {
            return 0;
        }
        if (intA == null) {
            return reverse ? 1 : -1;
        }
        if (intB == null) {
            return reverse ? -1 : 1;
        }
        int result = intA.compareTo(intB);
        return reverse ? result * (-1) : result;
    }

    public static int compare(Long intA, Long intB, boolean reverse) {
        if (intA == null && intB == null) {
            return 0;
        }
        if (intA == null) {
            return reverse ? 1 : -1;
        }
        if (intB == null) {
            return reverse ? -1 : 1;
        }
        int result = intA.compareTo(intB);
        return reverse ? result * (-1) : result;
    }

    public static int compare(Double doubleA, Double doubleB, boolean reverse) {
        if (doubleA == null && doubleB == null) {
            return 0;
        }
        if (doubleA == null) {
            return reverse ? 1 : -1;
        }
        if (doubleB == null) {
            return reverse ? -1 : 1;
        }
        int result = doubleA.compareTo(doubleB);
        return reverse ? result * (-1) : result;
    }
}
