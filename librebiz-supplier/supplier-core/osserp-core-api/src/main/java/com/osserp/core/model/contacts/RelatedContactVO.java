/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 23-May-2005 10:22:31 
 * 
 */
package com.osserp.core.model.contacts;

import org.jdom2.Element;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ClassifiedContact;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RelatedContactVO extends AbstractContactAware {
    private boolean internal = false;

    protected RelatedContactVO() {
        super();
    }

    public RelatedContactVO(Contact contact) {
        super(contact.getContactId(), Contact.OTHER, (Long) null, contact, Contact.ACTIVATED);
    }

    public RelatedContactVO(Long id, Contact contact) {
        super(id, Contact.OTHER, (Long) null, contact, Contact.ACTIVATED);
    }

    public RelatedContactVO(Long id, Long createdBy, Contact contact) {
        super(id, Contact.OTHER, createdBy, contact, Contact.ACTIVATED);
    }

    protected RelatedContactVO(ClassifiedContact o) {
        super(o);
    }

    @Override
    public Object clone() {
        return new RelatedContactVO(this);
    }

    public boolean isActivated() {
        return (getStatusId() != null
        && Contact.ACTIVATED.equals(getStatusId()));
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    @Override
    public Element getXML() {
        Element root = new Element("supplier");
        if (getId() != null) {
            root.addContent(new Element("supplierId").setText(getId().toString()));
        }
        root.addContent(getAddressXML());
        return root;
    }

    @Override
    public Long getGroupId() {
        Long id = super.getGroupId();
        return (id != null ? id : Contact.SUPPLIER);
    }

}
