<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="createNewCampaign" /></h4>
        </div>
    </div>
    <div class="panel-body">
        <c:if test="${!empty view.selectedCompany && view.multipleClientsAvailable}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="company" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.selectedCompany.name}"/>
                    </div>
                </div>
            </div>
        </c:if>

        <c:if test="${!empty view.parent}">
            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name"><fmt:message key="subCampaignOf" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.parent.name}" />
                    </div>
                </div>
            </div>
        </c:if>
        
        <c:choose>
            <c:when test="${empty view.selectedCompany}">

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="company"><fmt:message key="company" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="company" class="form-control" onchange="gotoUrl(this.value);">
                                <option value="<v:url value="/admin/crm/campaignConfig/selectCompany?id=0"/>"><fmt:message key="selection" /></option>
                                <c:forEach var="obj" items="${view.companies}">
                                    <option value="<v:url value="/admin/crm/campaignConfig/selectCompany?id=${obj.id}"/>" <c:if test="${view.selectedCompany.id == obj.id}">selected="selected"</c:if>>
                                        <o:out value="${obj.name}" />
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                
            </c:when>
            <c:otherwise>
                
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><fmt:message key="name" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="name" />
                        </div>
                    </div>
                </div>
                
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="description"><fmt:message key="description" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="description" />
                        </div>
                    </div>
                </div>
                
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="group"><fmt:message key="group" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="group" class="form-control">
                                <option value="0"><fmt:message key="noSelection"/></option>
                                <c:forEach var="obj" items="${view.groups}">
                                    <option value="${obj.id}" <c:if test="${view.selectedGroup.id == obj.id}">selected="selected"</c:if>>
                                        <o:out value="${obj.name}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="type"><fmt:message key="type" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="type" class="form-control">
                                <option value="0"><fmt:message key="noSelection"/></option>
                                <c:forEach var="obj" items="${view.types}">
                                    <option value="${obj.id}" <c:if test="${view.selectedType.id == obj.id}">selected="selected"</c:if>><o:out value="${obj.name}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                
                <c:if test="${view.reachListAvailable}">
                    <div class="row next">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="reach"><fmt:message key="reachLabel" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="reach" class="form-control">
                                    <c:forEach var="obj" items="${view.reachList}">
                                        <option value="${obj}" <c:if test="${view.bean.reach == obj}">selected="selected"</c:if>><fmt:message key="${obj}"/></option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                
                    <c:if test="${!empty view.branchOfficeList}">
                        <div class="row next">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="branch"><fmt:message key="branch" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="branch" class="form-control">
                                        <option value="-1"><fmt:message key="noSelection"/></option>
                                        <c:forEach var="obj" items="${view.branchOfficeList}">
                                            <option value="${obj.id}" <c:if test="${view.bean.branch == obj.id}">selected="selected"</c:if>><o:out value="${obj.name}"/></option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </c:if>
                
                <v:date name="campaignStarts" styleClass="form-control" label="campaignStart" picker="true" />
                
                <v:date name="campaignEnds" styleClass="form-control" label="campaignEnd" picker="true" />
            
            </c:otherwise>
        </c:choose>
        
        <c:if test="${!empty view.selectedCompany}">

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="/admin/crm/campaignConfig/disableCreateMode" /> 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </c:if>

    </div>
</div>
