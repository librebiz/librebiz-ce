/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.permissions

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.web.PortalView
import com.osserp.groovy.web.AbstractController
import com.osserp.gui.permissions.ShowPagePermissionsView

/**
 * 
 * @author eh <eh@osserp.com>
 *
 */
@RequestMapping("/permissions/showPagePermissions/*")
@Controller class ShowPagePermissionsController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(ShowPagePermissionsController.class.getName())

    @RequestMapping
    def forward(HttpServletRequest request) {
        ShowPagePermissionsView view = getView(request)
        PortalView portalView = request.getSession().getAttribute("portalView")
        view.setPortalView(portalView)
        if (logger.isDebugEnabled()) {
            logger.debug("showPagePermissionsController invoked...")
        }
        return pageArea
    }

    @RequestMapping
    def show(HttpServletRequest request) {
        ShowPagePermissionsView view = getView(request)
        PortalView portalView = request.getSession().getAttribute("portalView")
        view.setPortalView(portalView)
        if (logger.isDebugEnabled()) {
            logger.debug("showPagePermissionsController invoked...")
        }
        return "permissions/_showPagePermissionsNew"
    }
}
