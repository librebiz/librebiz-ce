<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="projectSupplierConfigView" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="involvedPartyEditHeader" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:if test="${!empty view.selectedSupplier}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="company"><fmt:message key="company" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:out value="${view.selectedSupplier.displayName}" />
                        </div>
                    </div>
                </div>
            </c:if>

            <c:choose>
                <c:when test="${!empty view.availableGroupNames}">
                    <c:set var="groupNameInList" value="false" />
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="label"><fmt:message key="label" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="groupName" class="form-control" >
                                    <option value=""><fmt:message key="selection" /></option>
                                    <c:forEach var="grpName" items="${view.availableGroupNames}">
                                        <c:choose>
                                            <c:when test="${grpName == view.bean.groupName}">
                                                <option value="${grpName}" selected="selected"><o:out value="${grpName}" /></option>
                                                <c:set var="groupNameInList" value="true" />
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${grpName}"><o:out value="${grpName}" /></option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="otherLabel"><fmt:message key="otherLabel" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${groupNameInList == 'true'}">
                                        <v:text name="customGroupName" styleClass="form-control" />
                                    </c:when>
                                    <c:otherwise>
                                        <v:text name="customGroupName" styleClass="form-control" value="${view.bean.groupName}" />
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="label"><fmt:message key="label" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:text name="groupName" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>

            <c:choose>
                <c:when test="${!empty view.bean.recordId and !empty view.bean.recordTypeId and !empty view.selectedSupplier}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="deliveryDate"><fmt:message key="deliveryDate" /></label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <v:text name="deliveryDate" styleClass="form-control" readonly="true"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <a href="<c:url value="/${view.bean.voucherType.resourceKey}.do?method=display&id=${view.bean.recordId}&exit=projectSupplierConfig"/>" title="<fmt:message key="change" />">
                                    <o:img name="writeIcon"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <v:date name="deliveryDate" styleClass="form-control" label="deliveryDate" picker="true"  />
                </c:otherwise>
            </c:choose>

            <v:date name="mountingDate" styleClass="form-control" label="mountingDate" picker="true" />

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="mountingDays"><fmt:message key="duration" /></label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <v:text name="mountingDays" styleClass="form-control" />
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <fmt:message key="mountingDaysInfo" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="voucher"><fmt:message key="voucher" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${empty view.bean.recordId and !empty view.selectedSupplier}">
                                <fmt:message key="notAssignedLabel" />
                                <v:link url="/purchasing/orderBySalesProductCollector/forward?selectionTarget=/purchasing/orderBySalesCreator/forward&supplierId=${view.selectedSupplier.id}&exit=/projectDisplay.do">
                                    [<fmt:message key="new" />]
                                </v:link>
                            </c:when>
                            <c:when test="${!empty view.bean.recordId and !empty view.bean.recordTypeId and !empty view.selectedSupplier}">
                                <a href="<c:url value="/${view.bean.voucherType.resourceKey}.do?method=display&id=${view.bean.recordId}&exit=projectSupplierConfig"/>">
                                    <o:out value="${view.bean.voucherType.numberPrefix}"/><o:out value="${view.bean.recordId}"/>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="notAssignedLabel" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/disableEditMode"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="save" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
