/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 16, 2009 5:40:05 PM 
 * 
 */
package com.osserp.core.service.products;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.osserp.core.dao.ProductQueries;
import com.osserp.core.dao.Products;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductComparators;
import com.osserp.core.products.ProductSearch;
import com.osserp.core.products.ProductSearchRequest;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
public class ProductSearchImpl extends AbstractService implements ProductSearch {
    private Products products = null;
    private ProductQueries productQueries = null;

    public ProductSearchImpl(Products products, ProductQueries productQueries) {
        super();
        this.products = products;
        this.productQueries = productQueries;
    }

    public Product find(Long id) {
        return products.find(id);
    }

    public List<Product> find(ProductSearchRequest searchRequest, ProductSelection userSelection) {
        List<Product> result = productQueries.find(
                userSelection, includeEol(searchRequest), fetchsize(searchRequest));
        return addLastPricePaid(searchRequest, result);
    }

    public List<Product> find(ProductSearchRequest searchRequest, ProductSelectionConfigItem selection) {
        List<Product> result = productQueries.find(selection, includeEol(searchRequest), fetchsize(searchRequest));
        return addLastPricePaid(searchRequest, result);
    }

    public List<Product> find(ProductSearchRequest searchRequest, ProductSelectionConfig selectionConfig) {
        List<Product> result = (searchRequest == null || isNotSet(searchRequest.getPattern())) 
                ? productQueries.find(selectionConfig, includeEol(searchRequest), fetchsize(searchRequest))
                : productQueries.find(
                        selectionConfig,
                        searchRequest.getColumnKey(),
                        searchRequest.getPattern(),
                        searchRequest.isStartsWith(),
                        searchRequest.isIncludeEol(),
                        fetchsize(searchRequest));
        return addLastPricePaid(searchRequest, result);
    }

    public List<Product> find(ProductSearchRequest searchRequest, List<ProductSelectionConfig> selectionConfigs) {
        List<Product> result = new ArrayList<>();
        Set<Long> added = new HashSet<>();
        for (int i = 0, j = selectionConfigs.size(); i < j; i++) {
            List<Product> nextList = find(searchRequest, selectionConfigs.get(i));
            for (int k = 0, l = nextList.size(); k < l; k++) {
                Product nextProduct = nextList.get(k);
                if (!added.contains(nextProduct.getProductId())) {
                    result.add(nextProduct);
                    added.add(nextProduct.getProductId());
                }
            }
        }
        return ProductComparators.sort(result);
    }

    public List<ProductClassificationConfig> findClassifications() {
        return productQueries.findClassificationConfigs();
    }

    public List<Product> findTimeAware() {
        return products.findTrackable();
    }

    private boolean includeEol(ProductSearchRequest searchRequest) {
        return searchRequest != null && searchRequest.isIncludeEol();
    }

    private int fetchsize(ProductSearchRequest searchRequest) {
        return searchRequest == null ? 0 : searchRequest.getFetchSize();
    }

    private List<Product> addLastPricePaid(ProductSearchRequest searchRequest, List<Product> list) {
        if (searchRequest != null && searchRequest.getContactReference() != null) {
            return productQueries.addLastPricePaid(list, searchRequest.getContactReference(), searchRequest.isContactReferenceCustomer());
        }
        return list;
    }
}
