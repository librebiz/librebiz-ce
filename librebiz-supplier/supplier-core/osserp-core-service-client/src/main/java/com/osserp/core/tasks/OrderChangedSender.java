/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.tasks;

import java.util.List;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface OrderChangedSender {

    /**
     * Initializes an order changed event about changed delivery date on an order item
     * @param order
     * @param deliveryDateChangedInfo
     */
    void sendDeliveryDateChangedInfo(Order order, DeliveryDateChangedInfo deliveryDateChangedInfo);

    /**
     * Initializes an order changed event about changed delivery dates on several order items
     * @param order
     * @param deliveryDateChangedInfos
     */
    void sendDeliveryDateChangedInfo(Order order, List<DeliveryDateChangedInfo> deliveryDateChangedInfos);

    /**
     * Initializes an order changed event about changed item
     * @param order
     * @param itemChangedInfo
     */
    void sendItemChangedInfo(Order order, ItemChangedInfo itemChangedInfo);

    /**
     * Initializes an order changed event about changed items
     * @param order
     * @param itemChangedInfos
     */
    void sendItemChangedInfo(Order order, List<ItemChangedInfo> itemChangedInfos);

    /**
     * Sends an update event by taskname.
     * @param user
     * @param order
     * @param eventTaskName
     * @param message
     */
    void sendUpdateEvent(Employee user, Order order, String eventTaskName, String message);
}
