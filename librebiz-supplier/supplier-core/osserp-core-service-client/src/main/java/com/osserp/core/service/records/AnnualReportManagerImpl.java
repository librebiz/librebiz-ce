/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 20, 2016
 * 
 */
package com.osserp.core.service.records;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.StringUtil;

import com.osserp.core.dao.records.CommonPaymentQueries;
import com.osserp.core.dao.records.PurchaseQueries;
import com.osserp.core.dao.records.SalesRecordQueries;
import com.osserp.core.finance.AccountingManager;
import com.osserp.core.finance.AnnualReport;
import com.osserp.core.finance.AnnualReportManager;
import com.osserp.core.finance.CommonPaymentSummary;
import com.osserp.core.finance.PaymentDisplay;
import com.osserp.core.finance.RecordSummary;
import com.osserp.core.model.records.AnnualReportVO;
import com.osserp.core.purchasing.PurchaseRecordSummary;
import com.osserp.core.sales.SalesRecordSummary;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AnnualReportManagerImpl extends AbstractService implements AnnualReportManager {
    private static Logger log = LoggerFactory.getLogger(AnnualReportManagerImpl.class.getName());

    private AccountingManager accountingManager;
    private CommonPaymentQueries commonPaymentQueries;
    private PurchaseQueries purchaseQueries;
    private SalesRecordQueries salesRecordQueries;
    private AnnualReportArchive annualReportArchive; 

    /**
     * Creates a new annual report manager
     * @param accountingManager
     * @param commonPaymentQueries
     * @param purchaseQueries
     * @param salesRecordQueries
     */
    public AnnualReportManagerImpl(
            AccountingManager accountingManager,
            CommonPaymentQueries commonPaymentQueries, 
            PurchaseQueries purchaseQueries, 
            SalesRecordQueries salesRecordQueries,
            AnnualReportArchive annualReportArchive) {
        super();
        this.accountingManager = accountingManager;
        this.commonPaymentQueries = commonPaymentQueries;
        this.purchaseQueries = purchaseQueries;
        this.salesRecordQueries = salesRecordQueries;
        this.annualReportArchive = annualReportArchive;
    }

    public AnnualReport getReport(SystemCompany systemCompany, int year) {
        assert systemCompany != null && year > 0;
        AnnualReportVO report = new AnnualReportVO(systemCompany, year);
        List<PaymentDisplay> allPayments = accountingManager.findPayments(systemCompany.getId());
        if (log.isDebugEnabled() && allPayments.size() >= 1) {
            PaymentDisplay firstPayment = allPayments.get(0);
            PaymentDisplay lastPayment = allPayments.get(allPayments.size()-1);
            log.debug("getReport() payments fetched [count=" + allPayments.size()
                    + ", firstId=" + firstPayment.getId() 
                    + ", firstFrom=" + DateFormatter.getDate(firstPayment.getCreated()) 
                    + ", firstPaid=" + DateFormatter.getDate(firstPayment.getPaidOn()) 
                    + ", lastId=" + lastPayment.getId() 
                    + ", lastFrom=" + DateFormatter.getDate(lastPayment.getCreated()) 
                    + ", lastPaid=" + DateFormatter.getDate(lastPayment.getPaidOn()) 
                    );
        }
        List<SalesRecordSummary> salesRecords = new ArrayList(salesRecordQueries.getSummaryByDate(
                systemCompany.getId(), report.getStartDate(), report.getStopDate()));

        List<PurchaseRecordSummary> purchaseRecords = new ArrayList(purchaseQueries.getSummaryByDate(
                systemCompany.getId(), report.getStartDate(), report.getStopDate()));

        List<CommonPaymentSummary> commonRecords = new ArrayList(commonPaymentQueries.getSummaryByDate(
                systemCompany.getId(), report.getStartDate(), report.getStopDate()));

        if (log.isDebugEnabled()) {
            log.debug("getReport() fetched lists [company=" + systemCompany.getId() 
                    + ", year=" + year 
                    + ", salesRecords=" + salesRecords.size()
                    + ", purchaseRecords=" + purchaseRecords.size() 
                    + ", commonRecords=" + commonRecords.size() 
                    + "]");
            
        }
        report.createReport(allPayments, CollectionUtil.reverse(salesRecords), purchaseRecords, commonRecords);
        return report;
    }

    public DocumentData getReportArchive(AnnualReport report) throws ClientException {
        return annualReportArchive.getArchive(report);
    }

    public List<DmsDocument> getBankAccountDocuments(Long bankAccountId, int year) {
        return annualReportArchive.getBankAccountDocuments(bankAccountId, year);
    }

    public String createCashInSheet(AnnualReport report) {
        if (report.getCashInList().isEmpty()) {
            return "";
        }
        String[] columns = { 
                "date", "number", "name", "type", 
                "account", "amount", "invoice" 
                };
        List<String[]> result = new ArrayList<String[]>();
        // add table header columns
        result.add(columns);
        for (int i = 0, j = report.getCashInList().size(); i < j; i++) {
            PaymentDisplay payment = report.getCashInList().get(i);
            String[] obj = createPaymentColumn(report, payment, columns.length);
            result.add(obj);
        }
        return StringUtil.createSheet(result);
    }

    public String createCashOutSheet(AnnualReport report) {
        if (report.getCashOutList().isEmpty()) {
            return "";
        }
        String[] columns = { 
                "date", "number", "name", "type", 
                "account", "amount", "invoice" 
                };
        List<String[]> result = new ArrayList<String[]>();
        // add table header columns
        result.add(columns);
        for (int i = 0, j = report.getCashOutList().size(); i < j; i++) {
            PaymentDisplay payment = report.getCashOutList().get(i);
            String[] obj = createPaymentColumn(report, payment, columns.length);
            result.add(obj);
        }
        return StringUtil.createSheet(result);
    }

    private String[] createPaymentColumn(AnnualReport report, PaymentDisplay payment, int columnCount) {
        String[] obj = new String[columnCount];
        obj[0] = DateFormatter.getDate(payment.getPaidOn());
        if (payment.isPurchaseInvoicePayment() || payment.isSalesInvoicePayment() || payment.isSalesDownpayment()) {
            obj[1] = payment.getInvoiceNumber();
        } else {
            obj[1] = fetchNote(payment.getNote());
        }
        obj[2] = payment.getContactName();
        if (payment.getRecordType() != null) {
            obj[3] = payment.getRecordType().getName();
        } else {
            obj[3] = "unknownType";
        }
        obj[4] = report.getBankAccountByPayment(payment);
        /*
        Double netAmount = 0d; // TODO calculate by gross if possible and required
        Double taxAmount = 0d; // TODO see above 
        obj[5] = NumberFormatter.getValue(netAmount, NumberFormatter.CURRENCY);
        obj[6] = NumberFormatter.getValue(taxAmount, NumberFormatter.CURRENCY);
        obj[7] = NumberFormatter.getValue(payment.getGrossAmount(), NumberFormatter.CURRENCY);
        */
        // No vat extraction / calculation by now
        obj[5] = createCurrencyForSheet(payment.getGrossAmount());
        obj[6] = createDateForSheet(payment.getCreated());
        return obj;
    }

    public String createSalesSheet(AnnualReport report) {
        if (report.getSalesRecords().isEmpty()) {
            return "";
        }
        String[] columns = { 
                "date", "number", "name", "type", 
                "account", "net", "vat", "amount" 
                };
        List<String[]> result = new ArrayList<String[]>();
        // add table header columns
        result.add(columns);
        for (int i = 0, j = report.getSalesRecords().size(); i < j; i++) {
            SalesRecordSummary summary = report.getSalesRecords().get(i);
            if (!summary.isPayment() && !summary.isCanceled() && !summary.isCancellation()) {
                String[] obj = new String[columns.length];
                obj[0] = createDateForSheet(summary.getCreated());
                obj[1] = summary.getNumber();
                obj[2] = summary.getName();
                if (summary.getType() != null) {
                    obj[3] = summary.getType().getName();
                } else {
                    obj[3] = summary.isCreditNote() ? "credit" : "invoice";
                }
                obj[4] = report.getBankAccountBySummary(summary);
                if (summary.isCreditNote()) {
                    obj[5] = createCurrencyForSheet(((-1) * summary.getAmount()));
                    obj[6] = createCurrencyForSheet(((-1) * summary.getTotalTax()));
                    obj[7] = createCurrencyForSheet(((-1) * summary.getGross()));
                } else {
                    obj[5] = createCurrencyForSheet(summary.getAmount());
                    obj[6] = createCurrencyForSheet(summary.getTotalTax());
                    obj[7] = createCurrencyForSheet(summary.getGross());
                }
                result.add(obj);
            }
        }
        return StringUtil.createSheet(result);
    }

    public String createSupplierSheet(AnnualReport report) {
        if (report.getSupplierRecords().isEmpty()) {
            return "";
        }
        String[] columns = {
                "date", "number", "name", "type", 
                "account", "net", "vat", "amount" 
                };
        List<String[]> result = new ArrayList<String[]>();
        // add table header columns
        result.add(columns);
        for (int i = 0, j = report.getSupplierRecords().size(); i < j; i++) {
            RecordSummary nextSummary = report.getSupplierRecords().get(i);
            if (nextSummary instanceof CommonPaymentSummary) {
                CommonPaymentSummary summary = (CommonPaymentSummary) nextSummary;
                String[] obj = new String[columns.length];
                obj[0] = DateFormatter.getDate(summary.getCreated());
                obj[1] = fetchNote(summary.getNote());
                obj[2] = summary.getName();
                if (summary.getType() != null) {
                    obj[3] = summary.getType().getName();
                } else {
                    obj[3] = "common";
                }
                obj[4] = report.getBankAccountBySummary(summary);
                if (summary.isOutpayment()) {
                    obj[5] = createCurrencyForSheet(summary.getAmount());
                    obj[6] = createCurrencyForSheet(summary.getTax());
                    obj[7] = createCurrencyForSheet(summary.getGross());
                } else {
                    obj[5] = createCurrencyForSheet(((-1) * summary.getAmount()));
                    obj[6] = createCurrencyForSheet(((-1) * summary.getTax()));
                    obj[7] = createCurrencyForSheet(((-1) * summary.getGross()));
                }
                result.add(obj);
            } else if (nextSummary instanceof PurchaseRecordSummary) {
                PurchaseRecordSummary summary = (PurchaseRecordSummary) nextSummary;
                if (!summary.isPayment()) {
                    String[] obj = new String[columns.length];
                    obj[0] = createDateForSheet(summary.getCreated());
                    obj[1] = fetchNote(summary.getNote());
                    obj[2] = summary.getName();
                    if (summary.getType() != null) {
                        obj[3] = summary.getType().getName();
                    } else {
                        obj[3] = "purchase";
                    }
                    obj[4] = report.getBankAccountBySummary(summary);
                    obj[5] = createCurrencyForSheet(summary.getAmount());
                    obj[6] = createCurrencyForSheet(summary.getTotalTax());
                    obj[7] = createCurrencyForSheet(summary.getGross());
                    result.add(obj);
                }
            } else if (log.isDebugEnabled()) {
                log.debug("createSupplierSheet() ignoring record [id=" + nextSummary.getId()
                        + ", type=" 
                        + (nextSummary.getType() != null ? nextSummary.getType().getId() 
                                : nextSummary.getTypeId()) + "]");
            }
        }
        return StringUtil.createSheet(result);
    }

    private String fetchNote(String summaryNote) {
        String note = StringUtil.replaceControl(summaryNote, " ");
        return note == null ? "" : StringUtil.cut(note, 40, false);
    }
}
