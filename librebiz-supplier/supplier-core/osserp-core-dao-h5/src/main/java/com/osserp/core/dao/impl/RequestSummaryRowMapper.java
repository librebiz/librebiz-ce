/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 28-Jul-2006 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;

import com.osserp.core.Options;
import com.osserp.core.model.SalesSummaryImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestSummaryRowMapper extends BusinessObjectRowMapper {
    public static final String SELECT_FROM =
            "SELECT plan_id,type_id,sales_id,manager_id,name,created,status_id,stopped FROM ";
    private Map<Long, Option> statusMap = null;

    public static RowMapperResultSetExtractor getReader(OptionsCache cache) {
        return new RowMapperResultSetExtractor(new RequestSummaryRowMapper(cache));
    }

    public RequestSummaryRowMapper(OptionsCache optionsCache) {
        super(optionsCache);
        this.statusMap = optionsCache.getMap(Options.REQUEST_STATUS);
    }

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        Long planId = Long.valueOf(rs.getLong(1));
        Integer status = Integer.valueOf(rs.getInt(7));
        SalesSummaryImpl vo = new SalesSummaryImpl(
                planId,
                fetchBusinessType(Long.valueOf(rs.getLong(2))),
                Long.valueOf(rs.getLong(3)),
                Long.valueOf(rs.getLong(4)),
                rs.getString(5),
                status,
                getStatusName(status),
                rs.getDate(6),
                getCancelled(status),
                rs.getBoolean(8));
        return vo;
    }

    private boolean getCancelled(Integer status) {
        return (status != null && status < 0);
    }

    private String getStatusName(Integer statInt) {
        if (statInt != null) {
            Long status = Long.valueOf(statInt);
            if (statusMap.containsKey(status)) {
                return statusMap.get(status).getName();
            }
        }
        return "n/a";
    }
}
