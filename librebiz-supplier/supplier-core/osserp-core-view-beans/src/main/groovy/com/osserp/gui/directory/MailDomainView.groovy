/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Dec 15, 2013
 *
 */
package com.osserp.gui.directory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.mail.MailDomain
import com.osserp.core.customers.Customer
import com.osserp.core.customers.CustomerManager
import com.osserp.core.system.NocClient
import com.osserp.core.system.NocClientManager
import com.osserp.core.system.SystemCompany
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class MailDomainView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(MailDomainView.class.getName())

    NocClient client
    MailDomain selectedPrimary

    MailDomainView() {
        super()
        headerName = 'mailDomainHeader'
        providesEditMode()
        providesCreateMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            Long clientId = form.getLong('client')
            if (!clientId) {
                client = nocClientManager.defaultClient
            } else {
                client = nocClientManager.getClient(clientId)
            }
            logger.debug("initRequest: load directory config [managedDomains=${client.maildomainSupport}, clientName=${client.name}]")
            loadDomains()
        }
    }

    @Override
    void save() {
        if (editMode) {
            bean = nocClientManager.updateDomain(
                    bean,
                    domainEmployee.id,
                    form.getString('provider'),
                    form.getString('smtpServerName'),
                    form.getString('hostIp'),
                    form.getString('hostMx'),
                    form.getString('hostSecondaryMx'),
                    form.getString('hostMxIp'),
                    form.getString('hostSecondaryMxIp'),
                    form.getDate('domainCreated'),
                    form.getDate('domainExpires'))
            disableEditMode()
        } else if (createMode) {
            bean = nocClientManager.createDomain(
                    domainEmployee.id,
                    client,
                    selectedPrimary,
                    form.getString('name'),
                    form.getString('provider'),
                    form.getDate('domainCreated'),
                    form.getDate('domainExpires'))
            disableCreateMode()
        }
        loadDomains()
    }

    @Override
    void select() {
        super.select()
    }

    String getFetchmailStatus() {
        if (!bean) {
            return 'undefined'
        }
        if (!bean.fetchmailConfigured) {
            return 'notConfigured'
        }
        if (!bean.fetchmailDomain) {
            return 'configuredButDeactivated'
        }
        return 'activated'
    }

    protected void loadDomains() {
        if (client) {
            list = nocClientManager.getDomainsByReference(client.id)
            if (client.maildomainSupport) {
                list.each {
                    if (!it.aliasOnly) {
                        selectedPrimary = it
                    }
                }
            }
        }
        logger.debug("loadDomains: done [available=${list.size()}]")
    }

    protected NocClientManager getNocClientManager() {
        getService(NocClientManager.class.getName())
    }
}
