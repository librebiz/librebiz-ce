/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 21, 2008 11:26:09 AM 
 * 
 */
package com.osserp.core.sales;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseListItem;
import com.osserp.core.BusinessType;
import com.osserp.core.projects.ProjectFcsDisplay;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class SalesUtil {
    private static Logger log = LoggerFactory.getLogger(SalesUtil.class.getName());

    public static Double getDefaultMinimalMargin() {
        // TODO init by prop
        return 15d;
    }

    public static Double getDefaultTargetMargin() {
        // TODO init by prop
        return 20d;
    }

    public static boolean isSupportingRevenueOnly(BusinessCase businessCase) {
        if (businessCase == null
                || DateUtil.getYear(businessCase.getCreated()) < 2007
                || !businessCase.getType().isTrading()) {
            return false;
        }
        return true;
    }

    public static List<? extends BusinessCaseListItem> filter(DomainUser user, List<? extends BusinessCaseListItem> source) {
        List<BusinessCaseListItem> result = new ArrayList<BusinessCaseListItem>();
        for (Iterator<? extends BusinessCaseListItem> i = source.iterator(); i.hasNext();) {
            BusinessCaseListItem next = i.next();
            if (user.isBranchAccessible(next.getBranch())) {
                result.add(next);
            }
        }
        return result;
    }

    public static List<SalesRecordSummary> filterSummary(DomainUser user, List<? extends SalesRecordSummary> source) {
        List<SalesRecordSummary> result = new ArrayList<SalesRecordSummary>();
        for (Iterator<? extends SalesRecordSummary> i = source.iterator(); i.hasNext();) {
            SalesRecordSummary next = i.next();
            boolean exec = next.getCompany() != null && user.getEmployee() != null
                    && user.getEmployee().isCompanyExecutive(next.getCompany().getId());
            if (exec || user.isBranchAccessible(next.getBranch())) {
                result.add(next);
            }
        }
        return result;
    }

    public static List<SalesListItem> filter(BranchOffice office, List<? extends SalesListItem> source) {
        List<SalesListItem> result = new ArrayList<SalesListItem>();
        for (Iterator<? extends SalesListItem> i = source.iterator(); i.hasNext();) {
            SalesListItem next = i.next();
            if (next.getBranch() != null && office.getId().equals(next.getBranch().getId())) {
                result.add(next);
            }
        }
        return result;
    }

    public static List<SalesListItem> filter(BusinessType businessType, List<? extends SalesListItem> source) {
        List<SalesListItem> result = new ArrayList<SalesListItem>();
        for (Iterator<? extends SalesListItem> i = source.iterator(); i.hasNext();) {
            SalesListItem next = i.next();
            if (businessType.getId().equals(next.getType().getId())) {
                result.add(next);
            }
        }
        return result;
    }

    public static String createSpreadSheet(
            List<SalesListItem> list, 
            Map<Long, Option> employeeNames, 
            Map<Long, Option> installerNames) {
        if (!list.isEmpty() && list.get(0) instanceof SalesMonitoringItem) {
            if (log.isDebugEnabled()) {
                log.debug("createSpreadSheet() monitoring items found, creating sheet with status...");
            }
            return createMonitoringSpreadSheet(new ArrayList(list), employeeNames, installerNames);
        }
        List<String[]> result = new ArrayList<String[]>();
        String[] header = {
                "id", "type", "branch", "name", "created", "status",
                "sales", "tec", "capacity", "installer"
        };
        result.add(header);
        
        for (int i = 0, j = list.size(); i < j; i++) {
            SalesListItem next = list.get(i);
            String[] values = new String[header.length];
            values[0] = next.getId().toString();
            values[1] = next.getType().getName();
            values[2] = next.getBranch() == null ? "1" : next.getBranch().getId().toString();
            values[3] = next.getName();
            values[4] = DateFormatter.getISO(next.getCreated());
            values[5] = next.getStatus() == null ? "0" : next.getStatus().toString();
            Option sales = employeeNames.get(next.getSalesId());
            if (sales != null) {
                values[6] = sales.getName();
            } else {
                values[6] = "";
            }
            Option manager = employeeNames.get(next.getManagerId());
            if (manager != null) {
                values[7] = manager.getName();
            } else {
                values[7] = "";
            }
            values[8] = NumberFormatter.getValue(next.getCapacity(), NumberFormatter.SHEET, 3);
            Option installer = (next.getInstallerId() == null || next.getInstallerId() == 0L
                    ? null : installerNames.get(next.getInstallerId())); 
            if (installer != null) {
                values[9] = installer.getName();
            } else {
                values[9] = "";
            }
            result.add(values);
        }
        return StringUtil.createSheet(result);
    }

        public static String createMonitoringSpreadSheet(
                List<SalesMonitoringItem> list, 
                Map<Long, Option> employeeNames, 
                Map<Long, Option> installerNames) {
            List<String[]> result = new ArrayList<String[]>();
            
            String[] header = {
                    "id", "type", "branch", "name", "created", "status",
                    "action", "sales", "tec", "capacity", "installer"
            };
            result.add(header);
            
            for (int i = 0, j = list.size(); i < j; i++) {
                SalesMonitoringItem next = list.get(i);
                String[] values = new String[header.length];
                values[0] = next.getId().toString();
                values[1] = next.getType().getName();
                values[2] = next.getBranch() == null ? "1" : next.getBranch().getId().toString();
                values[3] = next.getName();
                values[4] = DateFormatter.getISO(next.getCreated());
                values[5] = next.getStatus() == null ? "0" : next.getStatus().toString();
                if (next.getFlowControlSheet().isEmpty()) {
                    values[6] = "";
                } else {
                    ProjectFcsDisplay action = next.getFlowControlSheet().get(0);
                    values[6] = action.getName();
                }
                
                Option sales = employeeNames.get(next.getSalesId());
                if (sales != null) {
                    values[7] = sales.getName();
                } else {
                    values[7] = "";
                }
                Option manager = employeeNames.get(next.getManagerId());
                if (manager != null) {
                    values[8] = manager.getName();
                } else {
                    values[8] = "";
                }
                values[9] = NumberFormatter.getValue(next.getCapacity(), NumberFormatter.SHEET, 3);
                Option installer = (next.getInstallerId() == null || next.getInstallerId() == 0L
                        ? null : installerNames.get(next.getInstallerId())); 
                if (installer != null) {
                    values[10] = installer.getName();
                } else {
                    values[10] = "";
                }
                result.add(values);
            }
            return StringUtil.createSheet(result);
        }

    public static Long getBranch(Sales sales) throws ClientException {
        if (sales.getRequest() == null
                || sales.getRequest().getBranch() == null
                || sales.getRequest().getBranch().getId() == null) {
            throw new ClientException(ErrorCode.BRANCH_MISSING);
        }
        return sales.getBranch().getId();
    }
}
