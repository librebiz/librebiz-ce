/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 09-Jan-2007 17:13:39 
 * 
 */
package com.osserp.core.model.calc;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.calc.CalculationType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationTypeImpl extends AbstractOption implements CalculationType {

    private boolean supportsPartlists = false;

    /**
     * Default constructor to implement the serializable interface
     */
    protected CalculationTypeImpl() {
        super();
    }

    public CalculationTypeImpl(String name, String description) {
        super(name, description, (String) null);
    }

    public CalculationTypeImpl(CalculationType other) {
        super(other.getId(), other.getName(), other.getDescription(), (String) null);
        this.supportsPartlists = other.isSupportsPartlists();
    }

    public boolean isSupportsPartlists() {
        return supportsPartlists;
    }

    public void setSupportsPartlists(boolean supportsPartlists) {
        this.supportsPartlists = supportsPartlists;
    }
}
