/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 31-Jul-2006 
 * 
 */
package com.osserp.core.model.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Year;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.beans.YearImpl;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateUtil;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Legalform;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.ManagingDirector;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class SystemCompanyImpl extends OptionImpl implements SystemCompany {

    private Contact contact = null;
    private Legalform legalform = null;
    private BranchOffice registeredOffice = null;
    private Double internalMargin = Constants.DOUBLE_NULL;
    private List<EmployeeGroup> groups = new ArrayList<EmployeeGroup>();
    private List<BankAccount> bankAccounts = new ArrayList<BankAccount>();
    private List<ManagingDirector> managingDirectors = new ArrayList<ManagingDirector>();

    private String localCourt;
    private String tradeRegisterEntry;
    private String taxId;
    private String vatId;
    private String shortname;
    private String letterWindowName;

    private boolean primary = false;
    private boolean holding = false;
    private boolean partner = false;
    private boolean providingServices = false;

    private boolean logoAvailable = false;
    private String logoPath = null;
    
    private boolean vatReports = false;
    private Date vatReportsStart;
    private String vatReportPeriod;
    private String vatReportMethod;
    
    private boolean companyRegistrationAvailable = false;

    protected SystemCompanyImpl() {
        super();
    }

    public SystemCompanyImpl(Long id, String name, Contact contact) {
        super(id, name);
        this.contact = contact;
    }

    public SystemCompanyImpl(
            Long id,
            String name,
            Legalform legalform,
            List<BankAccount> bankAccounts,
            List<ManagingDirector> managingDirectors,
            Double internalMargin,
            String localCourt,
            String tradeRegisterEntry,
            String taxId,
            String vatId,
            Contact contact) {
        super(id, name);
        this.contact = contact;
        this.legalform = legalform;
        this.bankAccounts = bankAccounts;
        this.managingDirectors = managingDirectors;
        this.internalMargin = internalMargin;
        this.localCourt = localCourt;
        this.tradeRegisterEntry = tradeRegisterEntry;
        this.vatId = vatId;
    }

    protected SystemCompanyImpl(SystemCompany o) {
        super(o);
        localCourt = o.getLocalCourt();
        tradeRegisterEntry = o.getTradeRegisterEntry();
        taxId = o.getTaxId();
        vatId = o.getVatId();
        shortname = o.getShortname();
        letterWindowName = o.getLetterWindowName();
        holding = o.isHolding();
        primary = o.isPrimary();
        partner = o.isPartner();
        providingServices = o.isProvidingServices();
        logoAvailable = o.isLogoAvailable();
        logoPath = o.getLogoPath();
        companyRegistrationAvailable = o.isCompanyRegistrationAvailable();
        internalMargin = o.getInternalMargin();

        if (o.getContact() != null) {
            contact = (Contact) o.getContact().clone();
        }
        if (o.getLegalform() != null) {
            legalform = (Legalform) o.getLegalform().clone();
        }
        if (o.getRegisteredOffice() != null) {
            registeredOffice = (BranchOffice) o.getRegisteredOffice().clone();
        }
        groups = new ArrayList<EmployeeGroup>();
        if (o.getGroups() != null) {
            for (int i = 0, j = o.getGroups().size(); i < j; i++) {
                groups.add((EmployeeGroup) o.getGroups().get(i).clone());
            }
        }
        bankAccounts = new ArrayList<BankAccount>();
        if (o.getBankAccounts() != null) {
            for (int i = 0, j = o.getBankAccounts().size(); i < j; i++) {
                bankAccounts.add((BankAccount) o.getBankAccounts().get(i).clone());
            }
        }
        managingDirectors = new ArrayList<ManagingDirector>();
        if (o.getManagingDirectors() != null) {
            for (int i = 0, j = o.getManagingDirectors().size(); i < j; i++) {
                managingDirectors.add((ManagingDirector) o.getManagingDirectors().get(i).clone());
            }
        }
    }

    @Override
    public Object clone() {
        return new SystemCompanyImpl(this);
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Legalform getLegalform() {
        return legalform;
    }

    public void setLegalform(Legalform legalform) {
        this.legalform = legalform;
    }

    public Double getInternalMargin() {
        return internalMargin;
    }

    public void setInternalMargin(Double internalMargin) {
        this.internalMargin = internalMargin;
    }

    public List<EmployeeGroup> getGroups() {
        return groups;
    }

    protected void setGroups(List<EmployeeGroup> groups) {
        this.groups = groups;
    }

    public void addGroup(EmployeeGroup group) {
        boolean alreadyAdded = false;
        for (int i = 0, j = groups.size(); i < j; i++) {
            EmployeeGroup next = groups.get(i);
            if (next.getId().equals(group.getId())) {
                alreadyAdded = true;
            }
        }
        if (!alreadyAdded) {
            groups.add(group);
        }
    }

    public boolean removeGroup(Long id) {
        for (Iterator<EmployeeGroup> i = groups.iterator(); i.hasNext();) {
            EmployeeGroup next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                return true;
            }
        }
        return false;
    }

    public List<Year> getAccountingYears() {
        List<Year> result = new ArrayList<>();
        if (getVatReportsStart() != null) {
            int startYear = DateUtil.getYear(getVatReportsStart());
            result = DateUtil.createYearsTilNow(startYear);
        } else {
            result.add(new YearImpl(DateUtil.getCurrentYear()));
        }
        return CollectionUtil.reverse(result);
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    protected void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public void addBankAccount(
            Employee user,
            String shortkey,
            String bankName,
            String bankNameAddon,
            String bankAccountNumberIntl,
            String bankIdentificationCodeIntl,
            boolean useInDocumentFooter,
            boolean taxAccount) throws ClientException {

        SystemCompanyBankAccountImpl impl = new SystemCompanyBankAccountImpl(
                this,
                shortkey,
                bankName,
                bankNameAddon,
                bankAccountNumberIntl,
                bankIdentificationCodeIntl,
                useInDocumentFooter,
                taxAccount,
                (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
        bankAccounts.add(impl);
    }

    public List<ManagingDirector> getManagingDirectors() {
        return managingDirectors;
    }

    protected void setManagingDirectors(List<ManagingDirector> managingDirectors) {
        this.managingDirectors = managingDirectors;
    }

    public void addManagingDirector(Employee user, String name) throws ClientException {
        for (int i = 0, j = managingDirectors.size(); i < j; i++) {
            ManagingDirector md = managingDirectors.get(i);
            if (md.getName().equalsIgnoreCase(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        ManagingDirectorImpl obj = new ManagingDirectorImpl(user, this, name);
        managingDirectors.add(obj);
    }

    public String getLocalCourt() {
        return localCourt;
    }

    public void setLocalCourt(String localCourt) {
        this.localCourt = localCourt;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getLetterWindowName() {
        return letterWindowName;
    }

    public void setLetterWindowName(String letterWindowName) {
        this.letterWindowName = letterWindowName;
    }

    public String getTradeRegisterEntry() {
        return tradeRegisterEntry;
    }

    public void setTradeRegisterEntry(String tradeRegisterEntry) {
        this.tradeRegisterEntry = tradeRegisterEntry;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public boolean isHolding() {
        return holding;
    }

    public void setHolding(boolean holding) {
        this.holding = holding;
    }

    public boolean isPartner() {
        return partner;
    }

    public void setPartner(boolean partner) {
        this.partner = partner;
    }

    public boolean isLogoAvailable() {
        return logoAvailable;
    }

    public void setLogoAvailable(boolean logoAvailable) {
        this.logoAvailable = logoAvailable;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public boolean isVatReports() {
        return vatReports;
    }

    public void setVatReports(boolean vatReports) {
        this.vatReports = vatReports;
    }

    public Date getVatReportsStart() {
        return vatReportsStart;
    }

    public void setVatReportsStart(Date vatReportsStart) {
        this.vatReportsStart = vatReportsStart;
    }

    public String getVatReportPeriod() {
        return vatReportPeriod;
    }

    public void setVatReportPeriod(String vatReportPeriod) {
        this.vatReportPeriod = vatReportPeriod;
    }

    public String getVatReportMethod() {
        return vatReportMethod;
    }

    public void setVatReportMethod(String vatReportMethod) {
        this.vatReportMethod = vatReportMethod;
    }

    public boolean isCompanyRegistrationAvailable() {
        return companyRegistrationAvailable;
    }

    public void setCompanyRegistrationAvailable(boolean companyRegistrationAvailable) {
        this.companyRegistrationAvailable = companyRegistrationAvailable;
    }

    public BranchOffice getRegisteredOffice() {
        return registeredOffice;
    }

    public void setRegisteredOffice(BranchOffice registeredOffice) {
        this.registeredOffice = registeredOffice;
    }

    public boolean isProvidingServices() {
        return providingServices;
    }

    public void setProvidingServices(boolean providingServices) {
        this.providingServices = providingServices;
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();

        if (contact != null) {
            putIfExists(map, "contact", contact.getMapped());
            // TODO check usage and remove:
            putIfExists(map, "contactId", contact.getContactId());
        }
        putIfExists(map, "id", getId());
        putIfExists(map, "internalMargin", getInternalMargin());
        putIfExists(map, "localCourt", getLocalCourt());
        putIfExists(map, "name", getName());
        putIfExists(map, "letterWindowName", getLetterWindowName());
        putIfExists(map, "resourceKey", getResourceKey());
        putIfExists(map, "tradeRegisterEntry", getTradeRegisterEntry());
        putIfExists(map, "taxId", getTaxId());
        putIfExists(map, "vatId", getVatId());
        putIfExists(map, "primary", isPrimary());
        putIfExists(map, "holding", isHolding());
        putIfExists(map, "partner", isPartner());
        putIfExists(map, "logoAvailable", logoAvailable);
        putIfExists(map, "logoPath", logoPath);

        List<Map<String, Object>> directors = new ArrayList<Map<String, Object>>();
        Iterator<ManagingDirector> i = getManagingDirectors().iterator();
        while (i.hasNext()) {
            ManagingDirector md = i.next();
            directors.add(md.getMapped());
        }

        List<Map<String, Object>> bankAccountsList = new ArrayList<Map<String, Object>>();
        Iterator<BankAccount> j = getBankAccounts().iterator();
        while (j.hasNext()) {
            BankAccount bankAccount = j.next();
            if (bankAccount.isUseInDocumentFooter()) {
                bankAccountsList.add(bankAccount.getMapped());
            }
        }

        putIfExists(map, "managingDirectors", directors);
        putIfExists(map, "bankAccounts", bankAccountsList);
        if (legalform != null) {
            putIfExists(map, "legalform", legalform.getMapped());
        }
        if (registeredOffice != null && !registeredOffice.isEndOfLife()
                && !registeredOffice.getContact().isDeleted()) {
            putIfExists(map, "registeredOffice", registeredOffice.getMapped());
        }
        return map;
    }

    public Map<String, Object> getMapped(boolean ignoreRegisteredOffice) {
        Map<String, Object> map = super.getMapped();

        if (contact != null) {
            putIfExists(map, "contact", contact.getMapped());
            // TODO check usage and remove:
            putIfExists(map, "contactId", contact.getContactId());
        }
        putIfExists(map, "id", getId());
        putIfExists(map, "internalMargin", getInternalMargin());
        putIfExists(map, "localCourt", getLocalCourt());
        putIfExists(map, "name", getName());
        putIfExists(map, "letterWindowName", getLetterWindowName());
        putIfExists(map, "resourceKey", getResourceKey());
        putIfExists(map, "tradeRegisterEntry", getTradeRegisterEntry());
        putIfExists(map, "taxId", getTaxId());
        putIfExists(map, "vatId", getVatId());
        putIfExists(map, "primary", isPrimary());
        putIfExists(map, "holding", isHolding());
        putIfExists(map, "partner", isPartner());
        putIfExists(map, "logoAvailable", logoAvailable);
        putIfExists(map, "logoPath", logoPath);

        List<Map<String, Object>> directors = new ArrayList<Map<String, Object>>();
        Iterator<ManagingDirector> i = getManagingDirectors().iterator();
        while (i.hasNext()) {
            ManagingDirector md = i.next();
            directors.add(md.getMapped());
        }

        List<Map<String, Object>> bankAccountsList = new ArrayList<Map<String, Object>>();
        Iterator<BankAccount> j = getBankAccounts().iterator();
        while (j.hasNext()) {
            BankAccount bankAccount = j.next();
            bankAccountsList.add(bankAccount.getMapped());
        }

        putIfExists(map, "managingDirectors", directors);
        putIfExists(map, "bankAccounts", bankAccountsList);
        if (legalform != null) {
            putIfExists(map, "legalform", legalform.getMapped());
        }
        if (!ignoreRegisteredOffice && registeredOffice != null 
                && !registeredOffice.isEndOfLife()
                && !registeredOffice.getContact().isDeleted()) {
            putIfExists(map, "registeredOffice", registeredOffice.getMapped());
        }
        return map;
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("contact", contact));
        root.addContent(JDOMUtil.createElement("internalMargin", internalMargin));
        // bankAccounts and directors are not required by current use case since
        // they are also provided by map which is used to render the document footer
        //root.addContent(JDOMUtil.createElement("bankAccounts", bankAccounts));
        //root.addContent(JDOMUtil.createElement("managingDirectors", managingDirectors));
        root.addContent(JDOMUtil.createElement("localCourt", localCourt));
        root.addContent(JDOMUtil.createElement("tradeRegisterEntry", tradeRegisterEntry));
        root.addContent(JDOMUtil.createElement("taxId", taxId));
        root.addContent(JDOMUtil.createElement("vatId", vatId));
        root.addContent(JDOMUtil.createElement("holding", holding));
        root.addContent(JDOMUtil.createElement("providingServices", providingServices));
        root.addContent(JDOMUtil.createElement("partner", partner));
        root.addContent(JDOMUtil.createElement("primary", primary));
        root.addContent(JDOMUtil.createElement("logoAvailable", logoAvailable));
        root.addContent(JDOMUtil.createElement("logoPath", logoPath));
        if (legalform instanceof LegalformImpl) {
            root.addContent(((LegalformImpl) legalform).getXML("legalform"));
        }
        if (registeredOffice != null) {
            // TODO add xml
        }
        return root;
    }
}
