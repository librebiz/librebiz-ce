<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="calculationTemplatesView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="calculationTemplatesContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.createMode}">
                        <v:form url="/admin/calculations/calculationTemplates/save">
                            <c:import url="_calculationTemplatesCreate.jsp" />
                        </v:form>
                    </c:when>
                    <c:when test="${!empty view.bean}">
                        <c:import url="_calculationTemplatesBean.jsp" />
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-12 panel-area">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th><v:sortLink key="name">
                                                    <fmt:message key="name" />
                                                </v:sortLink></th>
                                            <th><v:sortLink key="product.name">
                                                    <fmt:message key="product" />
                                                </v:sortLink></th>
                                            <th><v:sortLink key="businessType.name">
                                                    <fmt:message key="orderType" />
                                                </v:sortLink></th>
                                            <th class="action center"><fmt:message key="action" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:choose>
                                            <c:when test="${empty view.list}">
                                                <tr>
                                                    <td colspan="4"><fmt:message key="noCalculationTemplatesAvailable" /></td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="template" items="${view.list}">
                                                    <c:set var="baretitle">
                                                        <fmt:message key="productNumber" />
                                                        <o:out value="${template.product.productId}" />
                                                    </c:set>
                                                    <tr>
                                                        <td><v:link url="/admin/calculations/calculationTemplates/select?id=${template.id}" rawtitle="${baretitle}">
                                                                <o:out value="${template.name}" />
                                                            </v:link></td>
                                                        <td><a href="<c:url value="/products.do?method=load&id=${template.product.productId}&exit=calculationTemplates"/>"> <o:out value="${template.product.name}" />
                                                        </a></td>
                                                        <td><o:out value="${template.businessType.name}" /></td>
                                                        <td class="action center"><v:link url="/admin/calculations/calculationTemplates/delete?id=${template.id}">
                                                                <o:img name="deleteIcon" />
                                                            </v:link></td>
                                                    </tr>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
