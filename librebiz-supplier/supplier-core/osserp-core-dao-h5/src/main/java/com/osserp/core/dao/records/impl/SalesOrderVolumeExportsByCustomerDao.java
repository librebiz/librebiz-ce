/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 29, 2009 9:51:13 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.Tables;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderVolumeExportsByCustomer;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.finance.Record;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;

/**
 * 
 * Exports by customer select all internal orders of billing company.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderVolumeExportsByCustomerDao extends AbstractSalesOrderVolumeExports implements SalesOrderVolumeExportsByCustomer {

    protected SalesOrderVolumeExportsByCustomerDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            SalesOrders salesOrders,
            SalesInvoices salesInvoices) {
        super(jdbcTemplate, tables, sessionFactory, salesOrders, salesInvoices);
    }

    @Override
    protected String createAvailabilityQuery(SalesOrderVolumeExportConfig config) throws ClientException {
        if (config.getExportCompany() == null || config.getExportCustomer() == null) {
            throw new ClientException(ErrorCode.INVALID_CONFIG);
        }
        StringBuilder query = createSelectFrom(false);
        query
                .append(salesOrdersTable)
                .append(" WHERE contact_id = ")
                .append(config.getExportCustomer().getId())
                .append(" AND status >= ")
                .append(Record.STAT_SENT)
                .append(" AND status < ")
                .append(Record.STAT_CLOSED)
                .append(" AND company_id = ")
                .append(config.getExportCompany().getId())
                .append(" AND id NOT IN (SELECT order_id FROM ")
                .append(exportItemsTable)
                .append(")");
        return query.toString();
    }
}
