/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 25, 2006 10:48:38 AM 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.View;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.records.DeliveryNoteView;
import com.osserp.gui.client.records.OrderView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDeliveryNoteAction extends AbstractRecordAction {
    private static Logger log = LoggerFactory.getLogger(AbstractDeliveryNoteAction.class.getName());

    protected DeliveryNoteView getDeliveryNoteView(HttpSession session)
            throws PermissionException {
        return (DeliveryNoteView) getRecordView(session);
    }

    protected abstract Class<? extends View> getOrderViewClass();

    /**
     * Adds a new delivery note for current order
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward add(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("add() request from " + getUser(session).getId());
        }
        DeliveryNoteView view = (DeliveryNoteView) createView(request);
        try {
            view.create();
        } catch (ClientException c) {
            saveError(request, c.getMessage());
            return mapping.findForward(Actions.EXIT);
        }
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    public ActionForward select(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("select() request from " + getUser(session).getId());
        }
        // delivery note view looks up for id in request scope so we don't
        // have to lookup for this here
        DeliveryNoteView view = (DeliveryNoteView) createView(request);
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        try {
            OrderView orderView = (OrderView) fetchView(session, getOrderViewClass());
            if (orderView != null) {
                orderView.refresh();
            }
        } catch (Throwable t) {
            log.warn("exit() ignoring failure while attempt to refresh related order on exit: "
                    + t.toString());
        }
        return super.exit(mapping, form, request, response);
    }

    /**
     * Confirms delete dialog
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward confirmDelete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("confirmDelete() request from " + getUser(session).getId());
        }
        DeliveryNoteView view = getDeliveryNoteView(session);
        view.setConfirmDelete(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes current delivery note
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("delete() request from " + getUser(session).getId());
        }
        DeliveryNoteView view = getDeliveryNoteView(session);

        try {
            view.delete();

        } catch (ClientException c) {
            saveError(request, c.getMessage());
            return mapping.findForward(Actions.DISPLAY);
        } catch (PermissionException p) {
            saveError(request, p.getMessage());
            return mapping.findForward(Actions.DISPLAY);
        }
        try {
            OrderView orderView = (OrderView) fetchView(session, getOrderViewClass());
            if (orderView != null) {
                orderView.refresh();
            }
        } catch (Throwable t) {
            log.warn("exit() ignoring failure while attempt to refresh related order on exit: "
                    + t.toString());
        }
        if (view.isExitTargetAvailable()) {
            if (log.isDebugEnabled()) {
                log.debug("delete() exit target found [" + view.getExitTarget() + "]");
            }
            if (!Actions.LIST.equals(view.getExitTarget())) {
                removeView(session);
            }
            return mapping.findForward(view.getExitTarget());
        }
        return mapping.findForward(Actions.EXIT);
    }

    /**
     * Enables ignore not exsting serial mode if user has required permissions
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableIgnoreNotExistingSerialMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableIgnoreNotExistingSerialMode() request from " + getUser(session).getId());
        }
        DeliveryNoteView view = getDeliveryNoteView(session);
        try {
            view.enableIgnoreNotExistingSerialMode();
            return mapping.findForward(view.getActionTarget());
        } catch (PermissionException e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
    }

    /**
     * Disables delete mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitDelete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitDelete() request from " + getUser(session).getId());
        }
        DeliveryNoteView view = getDeliveryNoteView(session);
        view.setConfirmDelete(false);
        return mapping.findForward(view.getActionTarget());
    }

}
