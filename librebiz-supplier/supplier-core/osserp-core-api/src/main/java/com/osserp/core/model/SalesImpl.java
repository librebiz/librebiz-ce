/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Oct-2006 15:17:44 
 * 
 */
package com.osserp.core.model;

import java.util.Date;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.util.DateUtil;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BusinessAddress;
import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.FcsItem;
import com.osserp.core.customers.Customer;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesClosedStatus;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class SalesImpl extends AbstractBusinessCase implements Sales {
    private static Logger log = LoggerFactory.getLogger(SalesImpl.class.getName());

    private Long id = null;
    private Request request = null;
    private Long parentId = null;
    private Long typeId = null;
    private Long managerId = null;
    private Long managerSubstituteId = null;
    private Long observerId = null;
    private Double capacity = 0.0;
    private boolean cancelled = false;
    private boolean stopped = false;
    private boolean reference = false;
    private String referenceNote = null;
    private Long referenceFrom = null;
    private Date referenceDate = null;
    private boolean internetExportEnabled = false;
    private boolean plantAvailable = false;
    private SalesClosedStatus closedStatus = null;
    private String closedStatusNote = null;

    protected SalesImpl() {
        super();
    }

    protected SalesImpl(Long id, Long createdBy, Request salesRequest) {
        this.id = id;
        typeId = salesRequest.getType() != null ? salesRequest.getType().getId() : null;
        request = salesRequest;
        if (request.getType() != null && request.getType().isDirectSales()) {
            setCreated(request.getCreated());
        } else {
            setCreated(DateUtil.getCurrentDate());
        }
        setCreatedBy(createdBy);
        if (request != null) {
            setBusinessType(request.getType());
            customerId = (request.getCustomer() == null)
                    ? null : request.getCustomer().getId();
            setAccountingReference(request.getAccountingReference());
        } else {
            log.warn("<init> invoked [id=" + id + ", request=null]");
        }
    }

    @Override
    public String getAccountingReference() {
        return request == null ? null : request.getAccountingReference();
    }

    @Override
    public void setAccountingReference(String accountingReference) {
        if (request != null) {
            request.setAccountingReference(accountingReference);
        }
    }

    public Long getPaymentId() {
        return request == null ? null : request.getPaymentId();
    }

    public Long getShippingId() {
        return request == null ? null : request.getShippingId();
    }
    
    public String getExternalReference() {
        return request == null ? null : request.getExternalReference();
    }

    public void setExternalReference(String externalReference) {
        if (request != null) {
            request.setExternalReference(externalReference);
        }
    }

    public String getExternalUrl() {
        return request == null ? null : request.getExternalUrl();
    }

    public void setExternalUrl(String externalUrl) {
        if (request != null) {
            request.setExternalUrl(externalUrl);
        }
    }

    public Long getPrimaryKey() {
        return id;
    }

    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return (request == null ? null : request.getCustomer());
    }

    public String getName() {
        return (request == null ? "" : request.getName());
    }

    public BusinessAddress getAddress() {
        return (request == null ? null : request.getAddress());
    }

    @Override
    public BusinessType getType() {
        return super.getType() == null ? (request != null ? request.getType() : null) : super.getType();
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Long getManagerSubstituteId() {
        return managerSubstituteId;
    }

    public void setManagerSubstituteId(Long managerSubstituteId) {
        this.managerSubstituteId = managerSubstituteId;
    }

    public Long getObserverId() {
        return observerId;
    }

    public void setObserverId(Long observerId) {
        this.observerId = observerId;
    }

    public boolean isReference() {
        return reference;
    }

    public void setReference(boolean reference) {
        this.reference = reference;
    }

    public void changeReference(boolean status, String note, Long setBy) {
        this.reference = status;
        this.referenceNote = note;
        this.referenceFrom = setBy;
    }

    public String getReferenceNote() {
        return referenceNote;
    }

    public void setReferenceNote(String referenceNote) {
        this.referenceNote = referenceNote;
    }

    public Long getReferenceFrom() {
        return referenceFrom;
    }

    public void setReferenceFrom(Long referenceFrom) {
        this.referenceFrom = referenceFrom;
    }

    public Date getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(Date referenceDate) {
        this.referenceDate = referenceDate;
    }

    public boolean isInternetExportEnabled() {
        return internetExportEnabled;
    }

    protected void setInternetExportEnabled(boolean internetExportEnabled) {
        this.internetExportEnabled = internetExportEnabled;
    }

    public void changeInternetExportStatus() {
        this.internetExportEnabled = !this.internetExportEnabled;
    }

    public boolean isPlantAvailable() {
        return plantAvailable;
    }

    public void setPlantAvailable(boolean plantAvailable) {
        this.plantAvailable = plantAvailable;
    }

    public boolean addFlowControl(
            FcsAction action,
            Long employeeId,
            Date created,
            String note,
            String headline,
            FcsClosing closing) throws ClientException {

        if (isFlowControlActionCreateable(action)) {
            checkFlowControlActionNote(action, note);
            FcsItem item = new FlowControlItemImpl(
                    this,
                    action,
                    created,
                    employeeId,
                    note,
                    headline,
                    false,
                    closing);
            addFlowControl(item);
            if (action.isCancelling()) {
                if (log.isDebugEnabled()) {
                    log.debug("addFlowControl() invoked with cancelling action [sales=" + getId() 
                            + ", id=" + action.getId() + "]"); 
                }
                setCancelled(true);
            } else {
                increaseStatus(action.getStatus());
            }
            return true;
        }
        return false;
    }

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isStopped() {
        return stopped;
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    public SalesClosedStatus getClosedStatus() {
        return closedStatus;
    }

    public void setClosedStatus(SalesClosedStatus closedStatus) {
        this.closedStatus = closedStatus;
    }

    public String getClosedStatusNote() {
        return closedStatusNote;
    }

    public void setClosedStatusNote(String closedStatusNote) {
        this.closedStatusNote = closedStatusNote;
    }

    public void close(SalesClosedStatus closedStatus, String closedStatusNote) {
        this.closedStatus = closedStatus;
        this.closedStatusNote = closedStatusNote;
        setStatus(BusinessCase.CLOSED);
    }

    public boolean isChangeable() {
        return (!isClosed() && !isCancelled());
    }

    public boolean isDeliveryClosed() {
        if (isClosed()) {
            return true;
        }
        for (int i = 0, j = getFlowControlSheet().size(); i < j; i++) {
            FcsItem next = getFlowControlSheet().get(i);
            FlowControlAction action = (FlowControlAction) next.getAction();
            if (!next.isCancels()
                    && next.getCanceledBy() == null
                    && action.isClosingDelivery()) {
                return true;
            }
        }
        return false;
    }

    public boolean isReleased() {
        for (int i = 0, j = getFlowControlSheet().size(); i < j; i++) {
            FcsItem next = getFlowControlSheet().get(i);
            FlowControlAction action = (FlowControlAction) next.getAction();
            if (action.isReleaseing() && !next.isCancels()
                    && isNotSet(next.getCanceledBy())) {
                return true;
            }
        }
        return false;
    }

    public String resetCancellation(Long employeeId) {
        StringBuilder businessNote = new StringBuilder("Cancellation reset - ");
        if (isCancelled()) {
            setCancelled(false);
            for (Iterator<FcsItem> i = getFlowControlSheet().iterator(); i.hasNext();) {
                FcsItem next = i.next();
                FlowControlAction action = (FlowControlAction) next.getAction();
                if (action.isCancelling()) {
                    businessNote.append(action.getName());
                    if (next.getNote() != null) {
                        businessNote.append("\n").append(next.getNote());
                    }
                    i.remove();
                    break;
                }
            }
            Long lastStatusBeforeCancelled = 0L;
            for (Iterator<FcsItem> i = getFlowControlSheet().iterator(); i.hasNext();) {
                FcsItem next = i.next();
                FlowControlAction action = (FlowControlAction) next.getAction();
                if (isSet(action.getStatus())) {
                    lastStatusBeforeCancelled = Long.valueOf(action.getStatus());
                    break;
                }
            }
            setStatus(lastStatusBeforeCancelled);
            setChangedBy(employeeId);
            setChanged(new Date(System.currentTimeMillis()));
        } else {
            businessNote.append("Nothing to do!");
        }
        return businessNote.toString();
    }

    public String resetClosed(Long employeeId) {
        StringBuilder businessNote = new StringBuilder("Reset closed - ");
        if (isClosed()) {
            setClosedStatus(null);
            setClosedStatusNote(null);
            Integer closedStatus = Sales.CLOSED.intValue();
            for (Iterator<FcsItem> i = getFlowControlSheet().iterator(); i.hasNext();) {
                FcsItem next = i.next();
                FlowControlAction action = (FlowControlAction) next.getAction();
                if (action.isClosing()) {
                    if (isSet(action.getStatus())
                            && !closedStatus.equals(action.getStatus())) {
                        closedStatus = action.getStatus();
                    }
                    businessNote.append(action.getName());
                    if (next.getNote() != null) {
                        businessNote.append("\n").append(next.getNote());
                    }
                    i.remove();
                    break;
                }
            }
            decreaseStatus(closedStatus);
            setChangedBy(employeeId);
            setChanged(new Date(System.currentTimeMillis()));
        } else {
            businessNote.append("Nothing to do!");
        }
        return businessNote.toString();
    }

    /* properties and methods for backward compatibility */

    /**
     * Property is required even value is provided by request.customer.id We need the value represented directly in sales table to perform dbms queries outside
     * orm
     */
    private Long customerId = null;

    protected Long getCustomerId() {
        return customerId;
    }

    protected void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getSalesAssignment() {
        return (request == null ? null : request.getSalesAssignment());
    }

    public boolean isSalesAssignmentStrict() {
        return (request == null ? false : request.isSalesAssignmentStrict());
    }

    public Long getSalesId() {
        return (request == null ? null : request.getSalesId());
    }

    public Long getSalesCoId() {
        return (request == null ? null : request.getSalesCoId());
    }

    public Double getSalesCommission() {
        return request == null ? 0d : request.getSalesCommission();
    }

    public void setSalesCommission(Double salesCommission) {
        if (request != null) {
            request.setSalesCommission(salesCommission);
        }
    }

    public BranchOffice getBranch() {
        return (request == null ? null : request.getBranch());
    }

    public final boolean isSalesContext() {
        return true;
    }

    public void updateAccountingReference(String accountingReference) {
        
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("id", id));
        root.addContent(JDOMUtil.createElement("request", request));
        root.addContent(JDOMUtil.createElement("parentId", parentId));
        root.addContent(JDOMUtil.createElement("managerId", managerId));
        root.addContent(JDOMUtil.createElement("typeId", typeId));
        root.addContent(JDOMUtil.createElement("managerSubstituteId", managerSubstituteId));
        root.addContent(JDOMUtil.createElement("observerId", observerId));
        root.addContent(JDOMUtil.createElement("capacity", capacity));
        root.addContent(JDOMUtil.createElement("cancelled", cancelled));
        root.addContent(JDOMUtil.createElement("stopped", stopped));
        root.addContent(JDOMUtil.createElement("reference", reference));
        root.addContent(JDOMUtil.createElement("referenceNote", referenceNote));
        root.addContent(JDOMUtil.createElement("referenceFrom", referenceFrom));
        root.addContent(JDOMUtil.createElement("referenceDate", referenceDate));
        root.addContent(JDOMUtil.createElement("internetExportEnabled", internetExportEnabled));
        root.addContent(JDOMUtil.createElement("plantAvailable", plantAvailable));
        return root;
    }

    @Override
    public Long getTypeId() {
        return typeId;
    }

    protected void setTypeId(Long typeId) {
        this.typeId = typeId;
    }
}
