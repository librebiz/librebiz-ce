/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 28, 2005 
 * 
 */
package com.osserp.core.model;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.PermissionException;
import com.osserp.common.User;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeRole;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.sales.Sales;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.Permissions;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DomainUserImpl extends UserImpl implements DomainUser {
    private static Logger log = LoggerFactory.getLogger(DomainUserImpl.class.getName());

    private Employee employee = null;

    protected DomainUserImpl() {
        super();
    }

    public DomainUserImpl(User user, Employee employee) {
        super(user);
        this.employee = employee;
        if (employee != null) {
            setUserEmployee(true);
        }
    }

    protected DomainUserImpl(DomainUser o) {
        super(o);
        if (o.getEmployee() != null) {
            employee = (Employee) o.getEmployee().clone();
        }
    }

    @Override
    public Object clone() {
        return new DomainUserImpl(this);
    }

    public Employee getEmployee() {
        return employee;
    }

    @Override
    public boolean isDomainUser() {
        return true;
    }

    @Override
    public String getEmail() {
        if (employee != null && employee.getEmail() != null) {
            return employee.getEmail();
        }
        return null;
    }

    public boolean isAccounting() {
        return Permissions.isAccounting(this);
    }

    public boolean isExecutiveAccounting() {
        return Permissions.isExecutiveAccounting(this);
    }

    public boolean isCustomerService() {
        return Permissions.isCustomerService(this);
    }

    public boolean isExecutive() {
        return Permissions.isExecutive(this);
    }

    protected boolean isExecutiveSaaS() {
        return Permissions.isExecutiveSaaS(this);
    }

    public boolean isBranchExecutive() {
        return Permissions.isBranchExecutive(this);
    }

    public boolean isOfficeManagement() {
        return Permissions.isOfficeManagement(this);
    }

    public boolean isLogistics() {
        if (employee != null) {
            EmployeeGroup group = employee.getDefaultGroup();
            return (group == null ? false : group.isLogistics());
        }
        return false;
    }

    public boolean isLogisticsOnly() {
        if (employee == null || employee.getRoleConfigs().size() > 1) {
            return false;
        }
        return isLogistics();
    }

    public boolean isLogisticsExecutive() {
        return Permissions.isLogisticsExecutive(this);
    }

    public boolean isSales() {
        if (employee != null) {
            List<EmployeeRoleConfig> rcs = employee.getRoleConfigs();
            for (int i = 0, j = rcs.size(); i < j; i++) {
                EmployeeRoleConfig cfg = rcs.get(i);
                for (int k = 0, l = cfg.getRoles().size(); k < l; k++) {
                    EmployeeRole role = cfg.getRoles().get(k);
                    if (role.getGroup().isSales()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isSalesExecutive() {
        return Permissions.isSalesExecutive(this);
    }

    public boolean isSalesOnly() {
        return Permissions.isSalesOnly(this);
    }
    

    public boolean isGlobalAccessGrant() {
        return (isGlobalBusinessCaseAccessGrant() 
                || (employee != null && employee.isIgnoringBranch()));
    }

    public boolean isGlobalBusinessCaseAccessGrant() {
        return Permissions.isGlobalBusinessCaseAccessGrant(this);
    }

    public boolean isProjectExecutive() {
        return Permissions.isProjectExecutive(this);
    }

    public boolean isProjectManager() {
        if (employee != null) {
            List<EmployeeRoleConfig> rcs = employee.getRoleConfigs();
            for (int i = 0, j = rcs.size(); i < j; i++) {
                EmployeeRoleConfig cfg = rcs.get(i);
                for (int k = 0, l = cfg.getRoles().size(); k < l; k++) {
                    EmployeeRole role = cfg.getRoles().get(k);
                    if (role.getGroup().isTechnician()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isPurchaser() {
        return hasPermission(Permissions.PURCHASING);
    }

    public boolean isWholesale() {
        return hasPermission(Permissions.WHOLESALE);
    }

    @Override
    public void checkPermission(String[] required) throws PermissionException {
        if (!isRoot()) {
            if (isRootRequest(required)) {
                throw new PermissionException();
            }
            super.checkPermission(required);
        }
    }

    @Override
    public boolean isPermissionGrant(String[] required) {
        if (isRoot()) {
            return true;
        }
        return isRootRequest(required) ? false : 
            super.isPermissionGrant(required);
    }
    
    protected final boolean isRootRequest(String[] required) {
        return required != null && required.length == 1 
                && "root".equals(required[0]);
    }
    
    protected final boolean isRoot() {
        return isExecutiveSaaS() && isHidden();
    }

    @Override
    public final boolean isFromHeadquarter() {
        if (employee != null) {
            List<EmployeeRoleConfig> rcs = employee.getRoleConfigs();
            for (int i = 0, j = rcs.size(); i < j; i++) {
                EmployeeRoleConfig cfg = rcs.get(i);
                if (cfg.getBranch() != null
                        && !cfg.getBranch().isThirdparty()
                        && cfg.getBranch().getCompany() != null
                        && cfg.getBranch().getCompany().isHolding()) {
                    if (log.isDebugEnabled()) {
                        log.debug("isFromHeadquarter() reports true [branch=" + cfg.getBranch().getId()
                                + ", company=" + cfg.getBranch().getCompany().getId()
                                + "]");
                    }
                    return true;
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("isFromHeadquarter() reports false...");
        }
        return false;
    }

    @Override
    public Long getDefaultFcsGroup() {
        return super.getDefaultFcsGroup();
    }

    @Override
    public void setDefaultFcsGroup(Long defaultFcsGroup) {
        super.setDefaultFcsGroup(defaultFcsGroup);
    }

    // access control

    public boolean isBranchAccessible(BranchOffice branch) {
        if (branch != null && employee != null) {
            if (employee.isSupportingBranch(branch)
                    || (isGlobalBusinessCaseAccessGrant() 
                            || isFromHeadquarter())) {
                return true;
            }
        }
        return false;
    }

    public boolean isSupportingType(BusinessType type) {
        if (type != null && type.getCompany() != null && employee != null) {
            return employee.isSupportingCompany(type.getCompany());
        }
        return false;
    }

    public boolean isUserRelatedSales(BusinessCase bc) {
        return (bc != null && employee != null
        && employee.getId().equals(bc.getSalesId()));
    }

    public boolean isUserRelatedCoSales(BusinessCase bc) {
        return (bc != null && employee != null
        && employee.getId().equals(bc.getSalesCoId()));
    }

    public boolean isUserForeignSales(BusinessCase bc) {
        Long employeeId = (employee == null ? null : employee.getId());
        if (bc == null || employeeId == null) {
            return false;
        }
        if (employeeId.equals(bc.getSalesCoId())
                || employeeId.equals(bc.getSalesId())) {
            return false;
        }
        if (isSales()
                && !isExecutive()
                && !isSalesExecutive()
                && !isWholesale()) {

            return true;
        }
        return false;
    }

    public boolean isUserRelatedManager(BusinessCase bc) {
        return (bc != null && employee != null
        && employee.getId().equals(bc.getManagerId()));
    }

    public boolean isResponsibleManager(BusinessCase bc) {
        if (bc == null || bc.getManagerId() == null || employee == null) {
            return false;
        }
        boolean sales = (bc instanceof Sales);
        Long substituteId = sales ? ((Sales) bc).getManagerSubstituteId() : null;
        return isResponsibleManager(bc.getPrimaryKey(), sales, bc.getManagerId(), substituteId);
    }

    protected boolean isResponsibleManager(
            Long businessId,
            boolean sales,
            Long managerId, 
            Long managerSubstituteId) {
        if (managerId == null || employee == null) {
            return false;
        }
        boolean result = managerId.equals(
                employee.getId())
                || isManagerExecutive();
        if (!result && isSet(managerSubstituteId)) {
            result =  managerSubstituteId.equals(employee.getId());
        }
        return result;
    }

    public void checkPermissions(BusinessCase bc) throws PermissionException {
        if (employee == null) {
            log.warn("checkPermissions() failed [domainUser.employee=null]");
            throw new PermissionException();
        }
        boolean sales = bc instanceof Sales;
        checkPermission(
                bc.getPrimaryKey(), 
                sales, 
                bc.getBranch(), 
                bc.getCreatedBy(), 
                bc.getSalesId(), 
                bc.getSalesCoId(), 
                bc.getManagerId(), 
                sales ? ((Sales)bc).getManagerSubstituteId() : null);
    }


    protected void checkPermission(
            Long businessId,
            boolean sales,
            BranchOffice branch,
            Long createdBy,
            Long salesId,
            Long salesCoId,
            Long managerId,
            Long substituteId) throws PermissionException {
        
        if (employee == null) {
            log.warn("checkPermissions() failed [domainUser.employee=null]");
            throw new PermissionException();
        }
        Long employeeId = employee.getId();

        if (!isGlobalBusinessCaseAccessGrant() && !isFromHeadquarter()) {
            if (log.isDebugEnabled()) {
                log.debug("checkPermissions() invoked in businessCase context [request="
                        + businessId
                        + ", branch="
                        + (branch == null ? "null" : branch.getId())
                        + ", sales="
                        + salesId
                        + ", employee="
                        + employeeId
                        + "]");
            }
            if (employeeId.equals(salesId)
                    || employeeId.equals(salesCoId)) {
                // sales + salesCo has access
                if (log.isDebugEnabled()) {
                    log.debug("checkPermissions() employee is associated sales");
                }
                return;
            } else if (isSet(createdBy) && createdBy.equals(getId())) {
                // creator has access
                if (log.isDebugEnabled()) {
                    log.debug("checkPermissions() employee is associated creator");
                }
                return;
            }
            if (sales) {
                // we're in sales context
                if (isProjectManager() && managerId == null) {
                    // user is manager and no manager assigned
                    if (employee.isSupportingBranch(branch)) {
                        if (log.isDebugEnabled()) {
                            log.debug("checkPermissions() employee is project manager from same branch");
                        }
                        return;
                    }
                }
            }
            if (employee.isSupportingBranch(branch)
                    && (isCustomerService()
                            || isProjectManager()
                            || isOfficeManagement())) {
                if (log.isDebugEnabled()) {
                    log.debug("checkPermissions() access ok, " +
                            "cs, om or pm from same branch");
                }
                return;
            }
            if (!isBranchExecutive() && !isProjectExecutive()
                    && !isResponsibleManager(
                            businessId, sales, managerId, substituteId)) {
                if (log.isDebugEnabled()) {
                    log.debug("checkPermissions() neither branch- or projectExecutive " +
                            "nor responsibleManager");
                }
                throw new PermissionException();
            }
            if (isBranchExecutive()) {
                if (!getEmployee().isSupportingBranch(branch)) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkPermissions() branchExecutive from foreign branch");
                    }
                    throw new PermissionException();
                }
            }
        }
    }

    public final void checkSalesAccess(BusinessCase bc) throws PermissionException {
        if (log.isDebugEnabled()) {
            log.debug("checkSalesAccess() invoked [businessCase="
                    + bc.getPrimaryKey() + ", user="
                    + getId() + ", employee="
                    + getEmployee().getId() + "]");
        }
        if (bc != null && bc.isSalesContext()) {
            if (isSales()) {
                if (isUserRelatedSales(bc)) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkSalesAccess() ok, user is sales");
                    }
                    return;
                }
                if (isUserRelatedCoSales(bc)) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkSalesAccess() ok, user is co-sales");
                    }
                    return;
                }
                if (!isFromHeadquarter()
                        && (employee != null
                                && employee.isSupportingBranch(bc.getBranch()))
                        && isBranchExecutive()) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkSalesAccess() ok, user is branch executive");
                    }
                    return;
                }
                if (isProjectManager()) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkSalesAccess() user is project manager...");
                    }
                    if (!isResponsibleManager(bc)
                            && !isUserRelatedSales(bc)
                            && !isExecutive()
                            && !isAccounting()
                            && !isOfficeManagement()
                            && !isCustomerService()) {
                        if (log.isDebugEnabled()) {
                            log.debug("checkSalesAccess() user is not responsible manager, " +
                                    "responsible sales, executive, accounting, om or cs");
                        }
                        throw new PermissionException();
                    }
                } else if (!(isAccounting()
                        || isCustomerService()
                        || isExecutive()
                        || isOfficeManagement()
                        || isPurchaser()
                        || getPermissions().containsKey(Permissions.EXECUTIVE_SALES))) {
                    if (!isUserRelatedSales(bc)) {
                        if (log.isDebugEnabled()) {
                            log.debug("checkSalesAccess() user is neither accounting, responsible manager, " +
                                    "responsible or executive sales, executive, accounting, purchaser, om nor cs");
                        }
                        throw new PermissionException();
                    }
                }
            }
        }
    }

    protected boolean isManagerExecutive() {
        return hasPermission(Permissions.EXECUTIVE_TECSTAFF);
    }

    private boolean hasPermission(String permission) {
        return (getPermissions().containsKey(permission));
    }
    
    @Override
    public boolean isHidden() {
        if (isActive() 
                && employee != null 
                && employee.isActive()  
                && Employee.STATUS_DELETED.equals(employee.getStatus())) {
            return true;
        }
        return false;
    }
}
