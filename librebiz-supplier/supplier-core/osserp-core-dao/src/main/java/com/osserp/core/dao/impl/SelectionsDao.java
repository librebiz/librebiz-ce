/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30.05.2012 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.SelectionItem;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.core.dao.Selections;
import com.osserp.core.dao.TableKeys;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SelectionsDao extends AbstractTablesAwareSpringDao implements Selections {

    private String items;

    public SelectionsDao(JdbcTemplate jdbcTemplate, Tables tables) {
        super(jdbcTemplate, tables);
        items = getTable(TableKeys.SELECTION_ITEMS);
    }

    public List<String> getSelections() {
        StringBuilder sql = new StringBuilder("select distinct selection from ");
        sql.append(items).append(" order by selection");
        return (List<String>) jdbcTemplate.query(sql.toString(), getStringRowMapper());
    }

    public List<SelectionItem> getSelection(String selectionName) {
        StringBuilder sql = new StringBuilder(SelectionItemRowMapper.SELECT_VALUES);
        sql.append(items).append(" where not end_of_life and selection = ? order by idx,name");
        return (List<SelectionItem>) jdbcTemplate.query(
                sql.toString(), new Object[] { selectionName }, getRowMapper());
    }

    public void addItem(Long user, String selection, String name) {
        if (!exists(selection, name)) {
            StringBuilder sql = new StringBuilder("insert into ");
            sql.append(items).append("(selection,name,created_by) values (?,?,?)");
            jdbcTemplate.update(sql.toString(), new Object[] { selection, name, user });
        }
    }

    public void deleteItem(Long id) {
        StringBuilder sql = new StringBuilder("update ");
        sql.append(items).append(" set end_of_life = true where id = ?");
        jdbcTemplate.update(sql.toString(), new Object[] { id });
    }

    public void updateItem(String selection, Long id, String name) {
        if (!exists(selection, id, name)) {
            StringBuilder sql = new StringBuilder("update ");
            sql.append(items).append(" set name = ? where id = ?");
            jdbcTemplate.update(sql.toString(), new Object[] { name, id });
        }
    }

    private boolean exists(String selection, String name) {
        StringBuilder sql = new StringBuilder("select count(*) from ");
        sql.append(items).append(" where selection = ? and name = ?");
        return (jdbcTemplate.queryForObject(sql.toString(), new Object[] { selection, name }, Integer.class) > 0);
    }

    private boolean exists(String selection, Long id, String name) {
        StringBuilder sql = new StringBuilder("select count(*) from ");
        sql.append(items).append(" where selection = ? and id <> ? and name = ?");
        return (jdbcTemplate.queryForObject(sql.toString(), new Object[] { selection, id, name }, Integer.class) > 0);
    }

    private RowMapperResultSetExtractor getRowMapper() {
        return new RowMapperResultSetExtractor(new SelectionItemRowMapper());
    }
}
