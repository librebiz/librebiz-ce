<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.dmsDocumentView}" />
<c:set var="referencePermissions" value="${requestScope.dmsReferencePermissions}" />

<c:if test="${view.assignImportsSupport and !empty view.documentsImported}">
    <c:choose>
        <c:when test="${view.validityPeriodSupported}">
            <c:choose>
                <c:when test="${!view.documentsImportedDisplay}">
                    <tr>
                        <td class="doc"><fmt:message key="documentImports" /></td>
                        <td class="size"> </td>
                        <td class="created"><o:out value="${view.documentsImportedCount}"/> </td>
                        <td colspan="5"> </td>
                        <td class="action icon">
                            <v:link url="${view.baseLink}/toggleDocumentsImported" title="documentImportsDisplay">
                                <o:img name="searchIcon" />
                            </v:link>
                        </td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <tr>
                        <th class="doc"><fmt:message key="documentImports" /></th>
                        <th class="size"><fmt:message key="size" /></th>
                        <th class="created" colspan="3"><fmt:message key="date" /></th>
                        <th class="employee" colspan="3"><fmt:message key="uploadBy" /></th>
                        <th class="action">
                            <v:link url="${view.baseLink}/toggleDocumentsImported">
                                <o:img name="backIcon" />
                            </v:link>
                        </th>
                    </tr>
                    <c:forEach items="${view.documentsImported}" var="doc">
                        <tr>
                            <td class="doc"><o:doc obj="${doc}"><o:out value="${doc.displayName}" /></o:doc></td>
                            <td class="size"><o:number value="${doc.fileSize}" format="bytes" /></td>
                            <td class="created" colspan="3"><o:date value="${doc.created}" /></td>
                            <td class="employee" colspan="3">
                                <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${doc.createdBy}">
                                    <oc:employee initials="true" value="${doc.createdBy}" />
                                </v:ajaxLink>
                            </td>
                            <td class="action"><a href="javascript:onclick=confirmLink('<fmt:message key="confirmAssignImport"/>','<v:url value="${view.baseLink}/assignImport?id=${doc.id}" />');" title="<fmt:message key="assignDocument"/>"> <o:img name="enabledIcon" />
                            </a></td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <c:choose>
                <c:when test="${!view.documentsImportedDisplay}">
                    <tr>
                        <td class="doc"><fmt:message key="documentImports" /></td>
                        <td class="size"> </td>
                        <td class="created"><o:out value="${view.documentsImportedCount}"/> </td>
                        <td class="employee"> </td>
                        <c:if test="${view.referenceSupport}">
                            <td class="action"></td>
                        </c:if>
                        <td class="action" colspan="2">
                            <v:link url="${view.baseLink}/toggleDocumentsImported" title="documentImportsDisplay">
                                <o:img name="searchIcon" />
                            </v:link>
                        </td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <tr>
                        <th class="doc"><fmt:message key="documentImports" /></th>
                        <th class="size"><fmt:message key="size" /></th>
                        <th class="created"><fmt:message key="date" /></th>
                        <th class="employee"><fmt:message key="uploadBy" /></th>
                        <c:if test="${view.referenceSupport}">
                            <th class="action"></th>
                        </c:if>
                        <th class="action" colspan="2">
                            <v:link url="${view.baseLink}/toggleDocumentsImported">
                                <o:img name="backIcon" />
                            </v:link>
                        </th>
                    </tr>
                    <c:forEach items="${view.documentsImported}" var="doc">
                        <tr>
                            <td class="doc"><o:doc obj="${doc}"><o:out value="${doc.displayName}" /></o:doc></td>
                            <td class="size"><o:number value="${doc.fileSize}" format="bytes" /></td>
                            <td class="created"><o:date value="${doc.created}" /></td>
                            <td class="employee">
                                <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${doc.createdBy}">
                                    <oc:employee initials="true" value="${doc.createdBy}" />
                                </v:ajaxLink>
                            </td>
                            <c:if test="${view.referenceSupport}">
                                <td class="action"></td>
                            </c:if>
                            <td class="action" colspan="2">
                                <a href="javascript:onclick=confirmLink('<fmt:message key="confirmAssignImport"/>','<v:url value="${view.baseLink}/assignImport?id=${doc.id}" />');" title="<fmt:message key="assignDocument"/>">
                                    <o:img name="enabledIcon" />
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </c:otherwise>
    </c:choose>
</c:if>
