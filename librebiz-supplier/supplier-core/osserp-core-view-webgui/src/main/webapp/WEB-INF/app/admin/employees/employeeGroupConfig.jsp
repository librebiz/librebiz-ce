<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeGroupConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="styles" type="string">
<style type="text/css">
<c:choose>
<c:when test="${view.createMode or view.editMode or !empty view.bean}">

td {
	text-align: left;
}
<c:if test="${view.createMode}">

.submitIcon {
	margin-left: 1em;
}

</c:if>
</c:when>
<c:otherwise>

td, tr .name {
	text-align: left;
	width: 140px;
}

td, tr .uid {
	text-align: right;
	width: 40px;
}

td, tr .flag {
	text-align: center;
	width: 30px;
}

td, tr .action {
	text-align: center;
	width: 35px;
}

</c:otherwise>
</c:choose>
</style>
</tiles:put>

<tiles:put name="headline"><fmt:message key="${view.headerName}"/></tiles:put>
<tiles:put name="headline_right"><v:navigation/></tiles:put>
<tiles:put name="content" type="string">
<c:choose>
<c:when test="${view.createMode}">
<c:import url="_employeeGroupConfigCreate.jsp"/>
</c:when>
<c:when test="${view.bean != null}">
<c:import url="_employeeGroupConfigDisplay.jsp"/>
</c:when>
<c:otherwise>
<c:import url="_employeeGroupConfigList.jsp"/>
</c:otherwise>
</c:choose>
</tiles:put>
</tiles:insert>