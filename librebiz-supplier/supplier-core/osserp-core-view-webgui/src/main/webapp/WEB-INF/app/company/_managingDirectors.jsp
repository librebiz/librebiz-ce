<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="managingDirectorsView" />

<c:if test="${!empty view}">
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <c:choose>
                <c:when test="${view.editMode}">
                    <fmt:message key="managingDirectorEdit" />
                </c:when>
                <c:when test="${view.createMode}">
                    <fmt:message key="managingDirectorCreate" />
                </c:when>
                <c:otherwise>
                    <fmt:message key="${view.headerName}" />
                </c:otherwise>
            </c:choose>
        </div>
        <div class="modalBoxHeaderRight">
            <div class="boxnav">
                <ul>
                    <c:forEach var="nav" items="${view.navigation}">
                        <li><v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}">
                                <o:img name="${nav.icon}" />
                            </v:ajaxLink></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
    <div class="modalBoxData">
        <div>
            <div class="subcolumns">
                <div class="subcolumn">
                    <c:if test="${!empty sessionScope.error}">
                        <div class="spacer"></div>
                        <div class="errormessage">
                            <fmt:message key="error" />
                            :
                            <fmt:message key="${sessionScope.error}" />
                        </div>
                        <o:removeErrors />
                    </c:if>
                    <div class="spacer"></div>
                    <c:choose>
                        <%-- create / edit mode --%>
                        <c:when test="${view.createMode or view.editMode}">
                            <v:ajaxForm name="dynamicForm" targetElement="${view.name}_popup" url="/company/managingDirectors/save">
                                <table class="valueTable input">
                                    <tbody>
                                        <tr>
                                            <td><fmt:message key="name" /></td>
                                            <td><input type="text" class="text" name="name" value="<o:out value="${view.bean.name}" />" /></td>
                                        </tr>
                                        <tr>
                                            <td class="row-submit"><c:choose>
                                                    <c:when test="${view.createMode}">
                                                        <v:ajaxLink url="/company/managingDirectors/disableCreateMode" linkId="exitLink" targetElement="${view.name}_popup">
                                                            <input type="button" class="cancel" value="<fmt:message key="exit"/>" />
                                                        </v:ajaxLink>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <v:ajaxLink url="/company/managingDirectors/disableEditMode" linkId="exitLink" targetElement="${view.name}_popup">
                                                            <input type="button" class="cancel" value="<fmt:message key="exit"/>" />
                                                        </v:ajaxLink>
                                                    </c:otherwise>
                                                </c:choose></td>
                                            <td class="row-submit"><o:submit /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </v:ajaxForm>
                        </c:when>
                        <%-- bean --%>
                        <c:when test="${!empty view.bean}">
                            <table class="valueTable">
                                <tbody>
                                    <tr>
                                        <td><fmt:message key="name" /></td>
                                        <td><o:out value="${view.bean.name}" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </c:when>
                        <%-- list --%>
                        <c:otherwise>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th><fmt:message key="name" /></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="obj" items="${view.list}">
                                        <tr>
                                            <td class="right"><o:out value="${obj.id}" /></td>
                                            <td><v:ajaxLink url="/company/managingDirectors/select?id=${obj.id}" linkId="selectLink" targetElement="${view.name}_popup">
                                                    <o:out value="${obj.name}" />
                                                </v:ajaxLink></td>
                                            <td class="center"><v:ajaxLink url="/company/managingDirectors/delete?id=${obj.id}" linkId="deleteLink" targetElement="${view.name}_popup">
                                                    <o:img name="deleteIcon" styleClass="bigicon" />
                                                </v:ajaxLink></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:otherwise>
                    </c:choose>
                    <div class="spacer"></div>
                </div>
            </div>
        </div>
    </div>
</c:if>

