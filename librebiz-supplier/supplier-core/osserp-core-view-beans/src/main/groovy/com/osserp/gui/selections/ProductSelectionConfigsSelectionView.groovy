/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 15.04.2011 14:18:25 
 * 
 */
package com.osserp.gui.selections

import com.osserp.common.ErrorCode
import com.osserp.common.util.CollectionUtil

import com.osserp.core.Comparators
import com.osserp.core.products.Product
import com.osserp.core.products.ProductCategoryConfig
import com.osserp.core.products.ProductGroupConfig
import com.osserp.core.products.ProductSearch
import com.osserp.core.products.ProductSelectionConfigManager
import com.osserp.core.products.ProductSelectionContext
import com.osserp.core.products.ProductTypeConfig

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class ProductSelectionConfigsSelectionView extends AbstractSelectionView {

    ProductSelectionContext selectedContext = null
    List<ProductSelectionContext> contexts = []

    ProductSelectionConfigsSelectionView() {
        super()
        context.popupView = false
        providesEditMode()
        providesCreateMode()
        headerName = 'configureProductSelectionConfigs'
        dependencies = ['productClassificationSelectionView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest || !contexts) {
            actualSortKey = 'name'
            contexts = productSelectionConfigManager.getSelectionContexts()
            def contextId = form.getLong('context')
            if (contextId) {
                selectedContext = contexts.find { it.id == contextId }
                if (selectedContext) {
                    list = productSelectionConfigManager.getSelectionConfigs(selectedContext)
                    if (selectedContext.fixedConfig) {
                        list = CollectionUtil.sort(list, Comparators.createEntityComparator(false))
                    }
                }
            }
        }
    }

    @Override
    void createCustomNavigation() {
        if (bean && bean.items && bean.items.size() > 0) {
            nav.editNavigation = [disableEditLink, productListLink, homeLink]
        } else {
            nav.editNavigation = [disableEditLink, homeLink]
        }
    }

    @Override
    MenuItem getCreateLink() {
        MenuItem link = super.createLink
        link.permissions = 'product_selections_admin'
        link.permissionInfo = 'permissionConfigureProductSelectionConfigs'
        return link
    }

    MenuItem getProductListLink() {
        MenuItem link = navigationLink(
                "/products/productSelectionList/forward?id=${bean.id}&strutsExit=productSelectionConfigsSelection",
                'searchIcon',
                'displayProductSelection')
        link.ajaxPopup = true
        link.ajaxPopupName = 'productSelectionList'
        return link
    }

    @Override
    void enableEditMode() {
        super.select()
        super.enableEditMode()
        if (form.getLong('context')) {
            env.editOnlyMode = true
        }
        createCustomNavigation()
    }
    
    boolean isEditOnlyMode() {
        env.editOnlyMode
    }

    @Override
    void disableEditMode() {
        super.select()
        super.disableEditMode()
    }

    @Override
    void reload() {
        list = productSelectionConfigManager.getSelectionConfigs(selectedContext)
        actualSortKey = 'name'
        if (bean) {
            bean = list.find { it.id == bean.id }
            createCustomNavigation()
        }
    }

    @Override
    void save() {
        Long contextId = form.getLong('contextId')
        ProductSelectionContext updateContext = contexts.find { it.id == contextId }
        String name = form.getString('name')
        String description = form.getString('description')
        boolean disabled = form.getBoolean('disabled')
        if (editMode) {
            productSelectionConfigManager.update(domainEmployee, bean, updateContext, name, description, disabled)
            if (contextId != selectedContext.id) {
                bean = null
                disableEditMode()
            }
        } else {
            bean = productSelectionConfigManager.create(domainEmployee, selectedContext, name, description)
            disableCreateMode()
            super.enableEditMode()
        }
        reload()
    }

    void addSelection() {
        ProductClassificationSelectionView acs = env.productClassificationSelectionView
        if (!acs) {
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
        }
        ProductTypeConfig type = acs.selectedType
        ProductGroupConfig group = acs.selectedGroup
        ProductCategoryConfig category = acs.selectedCategory
        bean = productSelectionConfigManager.addSelection(domainEmployee, bean, type, group, category)
        createCustomNavigation()
    }

    void addProduct() {
        Long id = form.getLong('id')
        if (id) {
            Product product = productSearch.find(id)
            bean = productSelectionConfigManager.addSelection(domainEmployee, bean, product)
            createCustomNavigation()
        }
    }

    void removeSelection() {
        Long id = form.getLong('id')
        if (id) {
            bean = productSelectionConfigManager.removeSelection(domainEmployee, bean, id)
            createCustomNavigation()
        }
    }

    void toggleExclusion() {
        Long id = form.getLong('id')
        if (id) {
            productSelectionConfigManager.toggleExclusion(id)
            reload()
        }
    }

    private ProductSearch getProductSearch() {
        getService(ProductSearch.class.getName())
    }

    private ProductSelectionConfigManager getProductSelectionConfigManager() {
        getService(ProductSelectionConfigManager.class.getName())
    }
}
