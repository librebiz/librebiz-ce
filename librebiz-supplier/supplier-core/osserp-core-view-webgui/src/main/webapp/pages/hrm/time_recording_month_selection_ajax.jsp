<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.timeRecordingView}"/>
<c:if test="${!empty view}">
    <c:set var="timeRecording" value="${view.timeRecording}"/>
    <c:set var="period" value="${timeRecording.selectedPeriod}"/>
    <c:set var="years" value="${period.supportedYears}"/>

    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <fmt:message key="availableMonths"/>
        </div>
    </div>
    <div class="modalBoxData">
        <div class="col-md-12 panel-area">
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <tbody>
                        <c:choose>
                            <c:when test="${empty years}">
                                <tr>
                                    <td colspan="2"><fmt:message key="noMonthsAvailable"/></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="year" items="${years}">
                                    <c:forEach var="month" items="${year.supportedMonths}">
                                        <tr>
                                            <td><o:out value="${year.year}"/></td>
                                            <td style="<c:if test="${month.currentMonth}">font-weight:bold;</c:if>">
                                                <a href="<c:url value="/timeRecordings.do"><c:param name="method" value="selectMonth"/><c:param name="year" value="${year.year}"/><c:param name="month" value="${month.month}"/></c:url>">
                                                    <fmt:message key="month.${month.month}"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</c:if>
