<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${contactView.bean}" />
<c:set var="paymentUrl" scope="request" value="/suppliers.do" />

<c:if test="${contactView.supplierView}">
    <%--
        <c:if test="${contactView.paymentAgreementEditMode}">
        <style type="text/css">
        .valueTable td:first-child.formLabelRight {
            text-align: right;
            padding-left: 15px;
        }
        </style>
        </c:if>
    --%>
    <div class="subcolumns">
        <div class="column50l">
            <div class="subcolumnl">
                <!-- header -->
                <div class="contentBox">
                    <div class="contentBoxHeader">
                        <div class="contentBoxHeaderLeft">
                            <table class="valueTable first33">
                                <tbody>
                                    <tr>
                                        <td style="padding: 0 5px;">
                                            <span style="margin-right: 8px;">
                                                <o:permission role="supplier_change,supplier_config,supplier_booking_change,executive" info="supplierConfig">
                                                    <v:ajaxLink url="/suppliers/supplierTypeSelection/forward?selectionAttribute=id&selection=${contact.supplierType.id}&selectionTarget=/suppliers.do?method=selectSupplierType" linkId="supplierTypeSelection">
                                                        <c:choose>
                                                            <c:when test="${contact.supplierType.overrideSupplierName}">
                                                                <o:out value="${contact.supplierType.name}" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="supplier" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </v:ajaxLink>
                                                </o:permission>
                                                <o:forbidden role="supplier_change,supplier_config,supplier_booking_change,executive" info="supplierConfig">
                                                    <c:choose>
                                                        <c:when test="${contact.supplierType.overrideSupplierName}">
                                                            <o:out value="${contact.supplierType.name}" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <fmt:message key="supplier" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </o:forbidden>
                                            </span>
                                            <o:out value="${contact.id}" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="contentBoxHeaderRight">
                            <div class="boxnav">
                                <ul>
                                    <li>
                                        <v:link url="/suppliers/supplierDetailsEditor/forward?exit=/supplierDisplay.do" title="supplierDetailsEditorHeader">
                                            <o:img name="writeIcon" />
                                        </v:link>
                                    </li>
                                    <li>
                                        <v:link url="/suppliers/supplierLetter/forward?exit=/supplierDisplay.do" title="correspondenceTitle">
                                            <o:img name="letterIcon" />
                                        </v:link>
                                    </li>
                                    <c:choose>
                                        <c:when test="${!contact.simpleBilling}">
                                            <li>
                                                <v:link url="/suppliers/supplierPurchaseRecords/forward?recipient=${contact.id}&exit=/supplierDisplay.do" title="summaryRecordJournal">
                                                    <o:img name="moneyIcon" />
                                                </v:link>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li>
                                                <v:link url="/accounting/accountTransaction/forward?recipient=${contact.id}&exit=/supplierDisplay.do" title="summaryRecordJournal">
                                                    <o:img name="moneyIcon" />
                                                </v:link>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="contentBoxData">
                        <div class="subcolumns">
                            <div class="subcolumn">
                                <div class="smallSpacer"></div>
                                <table class="valueTable first33">
                                    <tbody>
                                        <c:if test="${!contactView.paymentAgreementEditMode}">
                                            <tr>
                                                <td><fmt:message key="status" /></td>
                                                <td><o:permission role="supplier_change,executive" info="permissionChangeStatus">
                                                        <a href="<c:url value="/suppliers.do?method=changeStatus"/>" title="<fmt:message key="activateForSelection"/>"> <c:choose>
                                                                <c:when test="${contact.activated}">
                                                                    <fmt:message key="active" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <fmt:message key="deactivated" />
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </a>
                                                    </o:permission> <o:forbidden role="supplier_change,executive">
                                                        <c:choose>
                                                            <c:when test="${contact.activated}">
                                                                <fmt:message key="active" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </o:forbidden></td>
                                            </tr>
                                            <c:if test="${!empty contact.shortkey}">
                                                <tr>
                                                    <td><fmt:message key="shortkey" /></td>
                                                    <td><o:out value="${contact.shortkey}" /></td>
                                                </tr>
                                            </c:if>
                                            <c:if test="${!contact.publicInstitution && !contact.simpleBilling}">
                                                <tr>
                                                    <td><fmt:message key="stock" /></td>
                                                    <td><o:permission role="supplier_booking_change,executive" info="permissionChangeDirectInvoiceBooking">
                                                            <c:choose>
                                                                <c:when test="${contact.directInvoiceBookingEnabled}">
                                                                    <a href="<c:url value="/suppliers.do?method=changeDirectInvoiceBooking"/>" title="<fmt:message key="deactivateDirectInvoiceBooking"/>"> <fmt:message key="directPurchaseBookingEnabled" />
                                                                    </a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="<c:url value="/suppliers.do?method=changeDirectInvoiceBooking"/>" title="<fmt:message key="activateDirectInvoiceBooking"/>"> <fmt:message key="directPurchaseBookingDisabled" />
                                                                    </a>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </o:permission> <o:forbidden role="supplier_booking_change,executive">
                                                            <c:choose>
                                                                <c:when test="${contact.directInvoiceBookingEnabled}">
                                                                    <fmt:message key="directPurchaseBookingEnabled" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <fmt:message key="directPurchaseBookingDisabled" />
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </o:forbidden>
                                                    </td>
                                                </tr>
                                            </c:if>
                                            <tr>
                                                <td><fmt:message key="contactDisplay" /></td>
                                                <td><c:choose>
                                                        <c:when test="${contactView.defaultDisplay}">
                                                            <a href="<c:url value="/suppliers.do?method=toggleDefaultDisplayGroup"/>" title="<fmt:message key="contactDisplayLoadDescription"/>"><fmt:message key="contactDisplayLoad" /></a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="<c:url value="/suppliers.do?method=toggleDefaultDisplayGroup"/>" title="<fmt:message key="contactDisplayDefaultDescription"/>"><fmt:message key="contactDisplayDefault" /></a>
                                                        </c:otherwise>
                                                    </c:choose></td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${!contact.publicInstitution && !contact.supplierType.providesBankaccounts}">
                                            <o:permission role="purchasing,accounting,supplier_change,executive" info="permissionPaymentAgreementShowEdit">
                                                <c:import url="/pages/contacts/shared/payment_agreement.jsp" />
                                            </o:permission>
                                        </c:if>
                                        <%-- groups --%>
                                        <c:if test="${!contactView.paymentAgreementEditMode && !contact.simpleBilling}">
                                            <c:choose>
                                                <c:when test="${!contactView.groupSelectionMode}">
                                                    <tr>
                                                        <td><o:permission role="supplier_change,executive" info="permissionSelectGroup">
                                                                <a href="<c:url value="/suppliers.do?method=enableGroupSelectionMode"/>"><fmt:message key="groups" /></a>
                                                            </o:permission> <o:forbidden role="supplier_change,executive">
                                                                <fmt:message key="groups" />
                                                            </o:forbidden></td>
                                                        <td><c:choose>
                                                                <c:when test="${empty contact.productGroups}">
                                                                    <fmt:message key="noGroupsAssigned" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:forEach var="group" items="${contact.productGroups}">
                                                                        <o:out value="${group.name}" />
                                                                        <br />
                                                                    </c:forEach>
                                                                </c:otherwise>
                                                            </c:choose></td>
                                                    </tr>
                                                </c:when>
                                                <c:otherwise>
                                                    <tr>
                                                        <td><a href="<c:url value="/suppliers.do?method=disableGroupSelectionMode"/>">[<fmt:message key="break" />]
                                                            </a></td>
                                                        <td><c:forEach var="group" items="${contact.productGroups}" varStatus="s">
                                                                <a href="<c:url value="/suppliers.do?method=removeGroup&id=${group.id}"/>"><o:img name="cancelIcon" styleClass="smallicon" /> <o:out value="${group.name}" /></a>
                                                                <br />
                                                            </c:forEach> <c:set var="availableGroups" value="${contactView.availableGroups}" /> <c:forEach var="group" items="${availableGroups}" varStatus="s">
                                                                <a href="<c:url value="/suppliers.do?method=addGroup&id=${group.id}"/>"><o:img name="smallForwardIcon" styleClass="smallicon" /> <o:out value="${group.name}" /></a>
                                                                <br />
                                                            </c:forEach></td>
                                                    </tr>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:if>
                                    </tbody>
                                </table>
                                <div class="smallSpacer"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
            </div>
        </div>
        <c:if test="${contact.supplierType.providesBankaccounts}">
            <div class="column50r">
                <div class="subcolumnr">
                    <c:import url="/pages/suppliers/supplier_bankaccounts.jsp"/>
                </div>
            </div>
        </c:if>
    </div>
</c:if>