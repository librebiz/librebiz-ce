/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOutTag extends AbstractSimpleValueTag {
    private static Logger log = LoggerFactory.getLogger(AbstractOutTag.class.getName());
    private static final String BREAK = "<br/>";
    private static final int BREAK_LENGTH = BREAK.length();

    public static final String YESNO = "yesno";
    public static final String YESNOCAP = "yesnocap";
    
    public abstract String getOutput() throws JspException;

    /** maximum length of the string to display */
    protected int limit;
    protected String mode;


    public int getLimit() {
        return limit;
    }
    
    public void setLimit(int maxLength) {
        limit = maxLength;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    @Override
    public final void doTag() throws JspException {
        try {
            JspWriter out = getJspContext().getOut();
            String output = null;
            try {
                output = getOutput();
            } catch (Throwable t) {
                output = "";
            }
            out.print(output);
        } catch (IOException ioe) {
            log.error("doTag() error getting writer");
        }
    }

    protected String cut(String toCut) {
        return StringUtil.cut(toCut, limit, true);
    }

    protected String getObjectOutput(Object obj) {
        if (obj == null) {
            return "";
        }
        boolean capitalize = YESNOCAP.equalsIgnoreCase(mode);
        if (capitalize || YESNO.equalsIgnoreCase(mode)) {
            return fetchBoolean(obj, capitalize);
        }
        String out = null;
        if (obj instanceof String) {
            out = (String) obj;
        } else if (obj instanceof Date) {
            out = DateFormatter.getDate((Date) obj);
        } else {
            out = obj.toString();
        }
        if (limit > 0) {
            return cut(out);
        }
        return out;
    }
    
    private String fetchBoolean(Object obj, boolean capitalize) {
        if (obj instanceof String) {
            String bool = (String) obj;
            if ("true".equalsIgnoreCase(bool)
                    || "yes".equalsIgnoreCase(bool)
                    || "on".equalsIgnoreCase(bool)) {
                return getFormattedBoolean(true, capitalize);
            }
            return getFormattedBoolean(false, capitalize);
            
        } else if (obj instanceof Boolean) {
            return getFormattedBoolean((Boolean) obj, capitalize);
        }
        log.warn("fetchBoolean() invalid tag usage on missing or "
                + "incompatible object value [class="
                + (obj == null ? "null" : obj.getClass().getName()) + "]");
        return getFormattedBoolean(false, capitalize);
    }
    
    private String getFormattedBoolean(boolean value, boolean capitalize) {
        return getResourceString(value ? 
                (capitalize ? "bigYes" : "yes") : 
                    (capitalize ? "bigNo" : "no")); 
    }

    protected final String getFormattedString(Object val) {
        if (val instanceof String) {
            String s = (String) val;
            StringBuilder buffer = new StringBuilder(512);
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                int type = Character.getType(c);
                if (type == Character.CONTROL) {
                    if (!endsWithBreak(buffer)) {
                        buffer.append(BREAK);
                    }
                } else {
                    buffer.append(c);
                }
            }
            return buffer.toString();
        } 
        return (val == null) ? "" : val.toString();
    }

    private boolean endsWithBreak(StringBuilder s) {
        int length = s.length() - BREAK_LENGTH;
        return length > BREAK_LENGTH && s.substring(length, s.length()).equals(BREAK);
    }
    
}
