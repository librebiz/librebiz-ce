/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 4, 2004 
 * 
 */
package com.osserp.core.contacts;

import java.util.List;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ContactSearch extends Search {

    /**
     * Finds contacts by value.
     * @param context (optional, default 'contact')
     * @param column (optional)
     * @param type of the contact as <br />
     * private = 1, <br />
     * business = 2, <br />
     * public = 3, <br />
     * person = 4 (optional)
     * @param value to lookup for
     * @param startsWith
     * @return contacts or empty list
     */
    List<ContactSearchResult> find(String context, String column, Long type, String value, boolean startsWith);

    /**
     * Finds first contact rows. The number of rows fetched is defined by app config param contactSearch.firstRowCount.
     * @param context (optional, default 'contact')
     * @return contacts or empty list
     */
    List<ContactSearchResult> findFirstRows(String context);

    /**
     * Finds phone numbers
     * @param number
     * @return contacts or empty list
     */
    List<ContactSearchResult> findPhoneNumbers(String number);

    /**
     * Tries to find all contacts whose are member of specified contact group
     * @param contactGroup
     * @return contacts
     */
    List<Contact> findAllByGroup(Long contactGroup);

    /**
     * Finds all contacts related to another contact, e.g. contacts contact persons
     * @param contactId
     * @return list with contacts or empty list
     */
    List<Contact> findContactPersons(Long contactId);

    /**
     * Checks if a contact related to another contact with provided name exists
     * @param parent contact
     * @param lastName
     * @param firstName
     * @return true if contact person with same name exists
     */
    boolean contactPersonExisting(Contact parent, String lastName, String firstName);

    /**
     * Tries to find contacts whose zipcode and lastname values match given values
     * @param zipcode
     * @param lastName
     * @return existing contacts
     * @throws ClientException if email already exists
     */
    List<Contact> findExisting(String zipcode, String lastName);

    /**
     * Tries to find contacts whose match some of interests values
     * @param interest
     */
    List<Contact> findExisting(Interest interest);

    /**
     * Tries to find contacts whose email match given email pattern
     * @param email
     * @return of existing contacts or empty if none exists
     */
    List<Contact> findByEmail(String email);

    /**
     * Provides the first- and lastname of the contact
     * @param contactId
     * @return contactSearchResult
     */
    ContactSearchResult findNameByContactId(Long contactId);
}
