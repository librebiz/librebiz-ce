/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 7, 2009 3:14:37 PM 
 * 
 */
package com.osserp.core.dao;

import com.osserp.core.calc.Partlist;
import com.osserp.core.calc.PartlistReference;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Partlists extends ItemLists {

    /**
     * Creates a new partlist
     * @param reference
     * @param referenceProduct
     * @param createdBy
     * @param groupId
     * @return partlist
     */
    Partlist create(PartlistReference reference, Product referenceProduct, Long createdBy, Long groupId);

    /**
     * Copy a existing partlist to a new partlist
     * @param reference
     * @param referenceProduct
     * @param createdBy
     * @param configId
     * @param old
     * @return partlist
     */
    Partlist create(PartlistReference reference, Product referenceProduct, Long createdBy, Long groupId, Partlist old);

    /**
     * Finds a partlist by referenced product if exists
     * @param referenceId
     * @param productId
     * @return partlist or null if not exists
     */
    Partlist findByProduct(Long referenceId, Long productId);

}
