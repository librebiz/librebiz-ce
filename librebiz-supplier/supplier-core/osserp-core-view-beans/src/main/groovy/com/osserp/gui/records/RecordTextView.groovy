/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 22, 2020 
 * 
 */
package com.osserp.gui.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.dms.DocumentData

import com.osserp.core.contacts.ClassifiedContact
import com.osserp.core.dms.Letter
import com.osserp.core.dms.LetterContentManager
import com.osserp.core.dms.LetterManager
import com.osserp.core.dms.LetterParameter
import com.osserp.core.dms.LetterTemplate
import com.osserp.core.dms.LetterType
import com.osserp.core.finance.Record

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.letters.LetterReference
import com.osserp.gui.letters.LetterView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RecordTextView extends LetterView {
    private static Logger logger = LoggerFactory.getLogger(RecordTextView.class.getName())
    
    boolean textAbove
    String headerCreateNew

    RecordTextView() {
        super()
        disableAutoreload()
        dependencies = []
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!(viewReference?.embeddedTextType && viewReference?.record)) {
                logger.warn('initRequest: did not find recordView or textType')
                throw new ViewContextException(this)
            }
            selectedType = viewReference.embeddedTextType
            env['letterReference'] = new RecordTextReference(selectedType.id, viewReference?.record, exitTarget)

            if (viewReference.embeddedTextAbove) {
                bean = viewReference.embeddedTextAbove
                textAbove = true
            } else if (viewReference.embeddedTextBelow) {
                bean = viewReference.embeddedTextBelow
                textAbove = false
            } else {
                String embed = form.getString('embed')
                textAbove = (embed == 'above')
                headerCreateNew = textAbove ? 'createNewRecordCoverLabel' : 'createNewRecordFooterLabel'
                availableTemplates = letterTemplateManager.findTemplates(selectedType, true).findAll { it.embeddedAbove == textAbove }
                enableCreateMode()
            }
            reload()
        }
    }

    @Override    
    protected Letter createByForm() throws ClientException {
        Letter newText = letterManager.create(
            domainUser,
            selectedType,
            letterReference.getRecipient(),
            viewReference?.record.number,
            letterReference.getBusinessId(),
            letterReference.getBranchId(),
            selectedTemplate)
        viewReference?.enableEmbeddedText()
        bean = newText
        return newText
    }

    @Override    
    protected Letter updateByForm() {
        Letter letter = bean
        letterManager.update(
                domainUser,
                letter,
                letter.address.header,
                letter.address.name,
                letter.address.street,
                letter.address.zipcode,
                letter.address.city,
                letter.address.country,
                form.getString('subject'),
                form.getString('salutation'),
                form.getString('greetings'),
                letter.language,
                (form.getLong('signatureLeft') ?: null),
                (form.getLong('signatureRight') ?: null),
                viewReference?.record?.created,
                form.getBoolean('ignoreSalutation'),
                form.getBoolean('ignoreGreetings'),
                form.getBoolean('ignoreHeader'),
                form.getBoolean('ignoreSubject'),
                true,
                letter.type.embeddedAbove,
                form.getString('embeddedSpaceAfter'),
                getContentKeepTogetherValue())
        return letterManager.findLetter(bean.id)
    }

    @Override
    void delete() throws ClientException {
        super.delete()
        try {
            viewReference?.disableEmbeddedText()
            viewReference?.reload()
        } catch (Exception e) {
            logger.warn('delete: failed to reload viewReference', e)
        }
    }

    @Override
    void reload() {
        createCustomNavigation()
    }

    @Override
    void createCustomNavigation() {
        super.createCustomNavigation()
        nav.beanNavigation = [ exitLink, deleteLetterLink, editLink, printLink, homeLink ]
        nav.createNavigation = [ disableCreateLink, setupLink, homeLink]
    }

    @Override
    MenuItem getPrintLink() {
        navigationLink("/records/recordPrint/print?name=${viewReference?.record?.type?.resourceKey}&id=${viewReference?.record?.id}",
             'printIcon', 'documentPrint')
    }

    @Override
    MenuItem getSetupLink() {
        navigationLink("/admin/letters/letterTemplateConfig/forward?id=${selectedType.id}&exit=${exitTargetSource}",
             'configureIcon', 'configuration')
    }

    protected String getPrintLetterTitle() {
        'preview'
    }

    @Override
    protected LetterContentManager getLetterContentManager() {
        letterManager
    }

    @Override
    protected LetterManager getLetterManager() {
        getService(LetterManager.class.getName())
    }
}
