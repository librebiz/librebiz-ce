<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestCreatorView"/>
<c:if test="${view.requestCreated}">
    <c:set var="businessId" value="${view.bean.primaryKey}"/>
    <c:choose>
        <c:when test="${view.bean.salesContext}">
            <c:remove var="requestCreatorView" scope="session"/>
            <c:redirect url="/loadSales.do?id=${businessId}&exit=index"/>
        </c:when>
        <c:otherwise>
            <c:remove var="requestCreatorView" scope="session"/>
            <c:redirect url="/loadRequest.do?id=${businessId}&exit=index"/>
        </c:otherwise>
    </c:choose>
</c:if>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <c:if test="${empty view.bean}">
            <fmt:message key="requestOrderTypeSelection"/>
            <span> - </span>
        </c:if>
        <o:out value="${view.customer.displayName}"/>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>

    <tiles:put name="content" type="string" >
        <div class="content-area" id="requestCreatorContent">
            <c:choose>
                <c:when test="${empty view.bean}">
                    <c:import url="_requestCreatorTypeSelection.jsp"/>
                </c:when>
                <c:otherwise>
                    <c:import url="_requestCreatorForm.jsp"/>
                </c:otherwise>
            </c:choose>
            <div class="spacer"></div>
        </div>
    </tiles:put>
</tiles:insert>
