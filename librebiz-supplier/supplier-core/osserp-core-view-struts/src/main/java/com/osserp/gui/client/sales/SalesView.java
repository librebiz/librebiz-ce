/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Oct-2006 14:23:44 
 * 
 */
package com.osserp.gui.client.sales;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesListItem;
import com.osserp.gui.BusinessCaseView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("businessCaseView")
public interface SalesView extends BusinessCaseView {

    public void init(Sales sales, String exitTarget) throws ClientException, PermissionException;

    /**
     * Updates delivery status
     */
    public void updateDeliveryStatus();

    /**
     * Indicates that delivery warning is signaled
     * @return deliveryWarning
     */
    public boolean isDeliveryWarning();

    /**
     * Indicates that view is in reference setup mode
     * @return referenceSetupMode
     */
    public boolean isReferenceSetupMode();

    /**
     * Enables/disables reference mode
     * @param referenceSetupMode
     */
    public void setReferenceSetupMode(boolean referenceSetupMode);

    /**
     * Changes reference status
     * @param form
     * @throws ClientException
     * @throws PermissionException
     */
    public void updateReferenceStatus(Form form) throws ClientException, PermissionException;

    /**
     * Indicates that view is in parent edit mode
     * @return parentEditMode
     */
    public boolean isParentEditMode();

    /**
     * Enables/disables parent edit mode
     * @param parentEditMode
     */
    public void setParentEditMode(boolean parentEditMode);

    /**
     * Changes parent id
     * @param form
     * @throws ClientException
     * @throws PermissionException
     */
    public void updateParent(Form form) throws ClientException;

    /**
     * Indicates that this sales is service sales
     * @return serviceSales
     */
    public boolean isServiceSales();

    /**
     * Provides parent sales if this sales is service
     * @return serviceSalesParent
     */
    public Sales getServiceSalesParent();

    /**
     * Provides related service sales if existing
     * @return serviceSalesList
     */
    public List<SalesListItem> getServiceSalesList();

    /**
     * Indicates that change values on a closed sales is enabled or disabled
     * @return changeValuesOnClosedMode
     */
    public boolean isChangeValuesOnClosedMode();

    /**
     * Enables/disables change values on a closed sales mode
     * @return changeValuesOnClosedMode
     */
    public void setChangeValuesOnClosedMode(boolean changeValuesOnClosedMode);
    
    /**
     * Delete sales, sales order and all related artefacts and restores
     * previous request status before salesCreate.
     * This is a very invasive method and user should know what he's doing.
     * Caller must check sufficient permissions!
     * @return primary key id
     * @throws ClientException 
     */
    public Long restoreRequestStatus() throws ClientException;

    /**
     * Removes closing fcs action, opens the sales order and restores previous status.
     * @throws ClientException if reopen not possible or current user lacks of required permission. 
     */
    public void reopenClosed() throws ClientException;
}
