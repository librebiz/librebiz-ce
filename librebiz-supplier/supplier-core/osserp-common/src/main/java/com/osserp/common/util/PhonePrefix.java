/**
 *
 * Copyright (C) 2002 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 9, 2002 3:55:15 PM 
 * 
 */
package com.osserp.common.util;

import java.io.Serializable;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PhonePrefix implements Cloneable, Serializable {
    
    private String prefix;
    private long prefixNum;
    private int length;
    private String locality;
    
    
    protected PhonePrefix() {
        super();
    }
    
    public PhonePrefix(long prefixNumber) {
        super();
        prefixNum = prefixNumber;
        prefix = "0"+ prefixNum;
        length = prefix.length();
    }

    protected PhonePrefix(String prefix, long prefixNum, int length, String locality) {
        super();
        this.prefix = prefix;
        this.prefixNum = prefixNum;
        this.length = length;
        this.locality = locality;
    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }
    /**
     * @param prefix the prefix to set
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    /**
     * @return the prefixNum
     */
    public long getPrefixNum() {
        return prefixNum;
    }
    /**
     * @param prefixNum the prefixNum to set
     */
    public void setPrefixNum(long prefixNum) {
        this.prefixNum = prefixNum;
    }
    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }
    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }
    /**
     * @return the locality
     */
    public String getLocality() {
        return locality;
    }
    /**
     * @param locality the locality to set
     */
    public void setLocality(String locality) {
        this.locality = locality;
    }
    
    public boolean isMobileNumber() {
        return locality != null && locality.startsWith("Mobile");
    }

    /* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    @Override
    protected Object clone() {
        return new PhonePrefix(prefix, prefixNum, length, locality);
    }
}
