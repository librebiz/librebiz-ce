<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>


<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <fmt:message key="email" />
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="checkbox">
                <label for="grantContactPerEmail"> <v:checkbox id="grantContactPerEmail" name="grantContactPerEmail" /> <fmt:message key="grantContactPerEmail" />
                </label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <span>&nbsp;</span>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="grantContactPerEmailNote" styleClass="form-control" title="grantContactPerEmailInfo" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <fmt:message key="phone" />
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="checkbox">
                <label for="grantContactPerPhone"> <v:checkbox id="grantContactPerPhone" name="grantContactPerPhone" /> <fmt:message key="grantContactPerPhone" />
                </label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <span>&nbsp;</span>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="grantContactPerPhoneNote" styleClass="form-control" title="grantContactPerPhoneInfo" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <fmt:message key="lockLabel" />
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="checkbox">
                <label for="grantContactNone"> <v:checkbox id="grantContactNone" name="grantContactNone" /> <fmt:message key="grantContactNone" />
                </label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <span>&nbsp;</span>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="grantContactNoneNote" styleClass="form-control" title="grantContactNoneInfo" />
        </div>
    </div>
</div>

