<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${sessionScope.recordView.displayIgnoreNotExistingSerialDialog and not sessionScope.recordView.ignoreNotExistingSerialException}">
<br />
<table style="width: 99%;">
	<tr>
		<td width="70%"><span class="errortext"><fmt:message key="ignoreNotExistingSerialDialog"/>: </span></td>
		<td width="30%" align="right">
			<a href="<c:url value="${requestScope.baseUrl}"><c:param name="method" value="enableIgnoreNotExistingSerialMode"/></c:url>">[<fmt:message key="disableCheck"/>]</a>
		</td>
	</tr>
</table>
<br />
</c:if>
