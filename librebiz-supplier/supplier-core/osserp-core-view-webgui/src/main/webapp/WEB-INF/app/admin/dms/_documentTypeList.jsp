<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-lg-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="action"></th>
                    <th class="docListCount"><fmt:message key="numberShort" /></th>
                    <th class="docListLocation"><fmt:message key="storagePath" /></th>
                    <th class="docListName"><fmt:message key="name" /></th>
                    <th class="docListType"><fmt:message key="type" /></th>
                    <th class="docListDownload"><fmt:message key="downloadLabel" /></th>
                    <th class="docListFormat"><fmt:message key="storage" /></th>
                </tr>
            </thead>
            <tbody>
                <c:if test="${empty view.list}">
                    <tr>
                        <td colspan="6"><fmt:message key="thereIsNoDocumentYet" /></td>
                    </tr>
                </c:if>
                <c:forEach items="${view.list}" var="doc">
                    <tr>
                        <td class="action">
                            <c:choose>
                                <c:when test="${!doc.encryptionSupported}">
                                    <o:img name="toggleStoppedFalseIcon" title="documentEncryptionSupportMissing" />
                                </c:when>
                                <c:when test="${doc.locked}">
                                    <v:link url="${view.baseLink}/reload" title="reload">
                                        <o:img name="reloadIcon" />
                                    </v:link>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${doc.formatPlain}">
                                            <v:link url="${view.baseLink}/select?id=${doc.id}" title="documentCryptConfigHeader">
                                                <o:img name="unlockIcon" />
                                            </v:link>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="${view.baseLink}/select?id=${doc.id}" title="documentCryptConfigHeader">
                                                <o:img name="lockIcon" />
                                            </v:link>
                                        </c:otherwise>
                                     </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="docListCount">
                            <o:out value="${doc.count}" />
                        </td>
                        <td class="docListLocation">
                            <c:choose>
                                <c:when test="${!doc.locked}">
                                    <v:link url="/dms/documentListing/forward?type=${doc.id}&exit=/admin/dms/documentTypeConfig/reload">
                            <c:choose>
                                <c:when test="${doc.databaseStore}">
                                    <fmt:message key="database" />
                                </c:when>
                                <c:otherwise>
                                    <o:out value="${doc.contextPath}" />
                                </c:otherwise>
                            </c:choose>
                                    </v:link>
                                </c:when>
                                <c:otherwise>
                            <c:choose>
                                <c:when test="${doc.databaseStore}">
                                    <fmt:message key="database" />
                                </c:when>
                                <c:otherwise>
                                    <o:out value="${doc.contextPath}" />
                                </c:otherwise>
                            </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="docListName">
                            <span title="ID ${doc.id}"><o:out value="${doc.name}" /></span>
                        </td>
                        <td class="docListType">
                            <c:choose>
                                <c:when test="${!empty doc.suffix and !doc.suffixByFile}">
                                    <o:out value="${doc.suffix}" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="userDefined" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="docListDownload">
                            <v:link url="${view.baseLink}/changeDownloadName?id=${doc.id}" title="change">
                                <c:choose>
                                    <c:when test="${doc.useFilenameForDownload}">
                                        <fmt:message key="originalNameLabel" />
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="filename" />
                                    </c:otherwise>
                                </c:choose>
                            </v:link>
                        </td>
                        <td class="docListFormat">
                            <c:choose>
                                <c:when test="${not doc.encryptionSupported}">
                                    <fmt:message key="unchanged" />
                                </c:when>
                                <c:when test="${doc.formatPlain and doc.locked and doc.encryptionSupported}">
                                    <fmt:message key="runningDecryption" />
                                </c:when>
                                <c:when test="${doc.formatPlain}">
                                    <fmt:message key="notEncrypted" />
                                </c:when>
                                <c:when test="${doc.locked}">
                                    <fmt:message key="runningEncryption" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="encrypted" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
