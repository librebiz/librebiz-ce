/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 11, 2007 11:04:09 PM 
 * 
 */
package com.osserp.gui.client.records.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.StringUtil;
import com.osserp.common.web.Actions;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.ReceivablesManager;
import com.osserp.core.finance.RecordDisplay;

import com.osserp.gui.client.records.InvoiceReminderView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("invoiceReminderView")
public class InvoiceReminderViewImpl extends AbstractRecordDisplayView
        implements InvoiceReminderView {
    private boolean descendent = true;

    protected InvoiceReminderViewImpl() {
        setActionTarget(Actions.DISPLAY);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        refresh();
    }

    public boolean isDescendent() {
        return descendent;
    }

    public void changeDescendentDisplay() {
        descendent = !descendent;
        refresh();
    }

    public void refresh() {
        clearAmounts();
        List<RecordDisplay> rd = getReceivablesManager().getInvoices(descendent);
        for (Iterator<RecordDisplay> i = rd.iterator(); i.hasNext();) {
            RecordDisplay next = i.next();
            if (next.isPaid() || (!isIncludeInternal() && next.isInternal())) {
                i.remove();
            } else if (includeCompany(next.getCompany())) {
                addAmounts(next);
            } else {
                i.remove();
            }
        }
        setList(rd);
    }

    public String getSpreadsheet() {
        List<String[]> valueList = new ArrayList<String[]>();
        List<RecordDisplay> list = getList();
        RecordDisplay next = null;
        for (int i = 0, j = list.size(); i < j; i++) {
            next = list.get(i);
            valueList.add(next.getValues());
        }
        if (next != null) {
            valueList.add(0, next.getValueKeys());
        }
        return StringUtil.createSheet(valueList);
    }

    @Override
    public void setEditMode(boolean editMode) throws PermissionException {
        super.setEditMode(editMode);
        if (!editMode) {
            // we leave settings and refresh list with new settings
            refresh();
        }
    }

    // unused protected setters
    protected void setDescendent(boolean descendent) {
        this.descendent = descendent;
    }

    // helpers 
    private ReceivablesManager getReceivablesManager() {
        return (ReceivablesManager) getService(ReceivablesManager.class.getName());
    }
}
