<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="column50l">
	<div class="subcolumnl">

		<div class="spacer"></div>
		<div class="contentBox">
			<div class="contentBoxHeader">
				<div class="contentBoxHeaderLeft">
					<fmt:message key="salesRegion"/> <o:out value="${view.bean.id}"/><c:if test="${!empty view.bean.name}"> - <o:out value="${view.bean.name}"/></c:if>
				</div>
			</div>
			<div class="contentBoxData">
				<div class="subcolumns">
					<div class="subcolumn">
						<div class="spacer"></div>

						<table class="valueTable first33">
							<tr>
								<td><fmt:message key="salesRegionManager"/></td>
								<td>
									<c:choose>
										<c:when test="${empty view.bean.salesExecutive}">
											<fmt:message key="notAssigned"/>
										</c:when>
										<c:otherwise>
											<v:ajaxLink linkId="salesRegionManagerPopupView" url="/employees/employeePopup/forward?id=${view.bean.salesExecutive.id}">
												<o:out value="${view.bean.salesExecutive.displayName}"/>
											</v:ajaxLink>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
							<tr>
								<td><fmt:message key="salesBy"/></td>
								<td>
									<c:choose>
										<c:when test="${empty view.bean.sales}">
											<fmt:message key="notAssigned"/>
										</c:when>
										<c:otherwise>
											<v:ajaxLink linkId="salesRegionSalesPopupView" url="/employees/employeePopup/forward?id=${view.bean.sales.id}">
												<o:out value="${view.bean.sales.displayName}"/>
											</v:ajaxLink>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</table>

						<div class="spacer"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacer"></div>

	</div>
</div>

<div class="column50r">
	<div class="subcolumnr">

		<div class="spacer"></div>

		<div class="contentBox">
			<div class="contentBoxHeader">
				<div class="contentBoxHeaderLeft">
					<fmt:message key="assignedZipcodes"/>
				</div>
				<div class="contentBoxHeaderRight">
					<c:choose>
						<c:when test="${view.zipcodeAddMode}">
							<a href="<v:url value="/admin/sales/salesRegionConfig/disableZipcodeAddMode"/>"><o:img name="backIcon" styleClass="bigicon"/></a>
						</c:when>
						<c:otherwise>
							<a href="<v:url value="/admin/sales/salesRegionConfig/enableZipcodeAddMode"/>"><o:img name="newIcon" styleClass="bigicon"/></a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<div class="contentBoxData">
				<div class="subcolumns">
					<div class="subcolumn">
						<div class="spacer"></div>
						<c:choose>
							<c:when test="${view.zipcodeAddMode}">
								<v:form name="salesRegionConfigForm" url="/admin/sales/salesRegionConfig/addZipcode">
									<table class="table table-striped">
										<thead>
											<tr>
												<th style="width: 100%;"><fmt:message key="zipcode"/></th>
												<th class="center"><fmt:message key="action"/></th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${!empty errors}">
												<tr>
													<td class="error" colspan="2"><fmt:message key="error"/>: ${errors}</td>
												</tr>
                                                <o:removeErrors/>
											</c:if>
											<tr>
												<td><v:text name="zipcode"/></td>
												<td class="center"><input type="image" name="addZipcode" src="<o:icon name="saveIcon"/>" value="addZipcode" /></td>
											</tr>
										</tbody>
									</table>
								</v:form>
							</c:when>
							<c:otherwise>
								<table class="table table-striped">
									<thead>
										<tr>
											<th style="width: 100%;"><fmt:message key="zipcode"/></th>
											<th class="center"><fmt:message key="action"/></th>
										</tr>
									</thead>
									<c:set var="list" value="${view.bean.zipcodes}"/>
									<c:choose>
										<c:when test="${empty list}">
											<tr>
												<td colspan="2"><fmt:message key="noneAssigned"/></td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach var="zip" items="${list}">
												<tr>
													<td><o:out value="${zip}"/></td>
													<td class="center"><v:link url="/admin/sales/salesRegionConfig/removeZipcode?zipcode=${zip}"><o:img name="deleteIcon"/></v:link></td>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</table>
							</c:otherwise>
						</c:choose>

						<div class="spacer"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacer"></div>

	</div>
</div>
