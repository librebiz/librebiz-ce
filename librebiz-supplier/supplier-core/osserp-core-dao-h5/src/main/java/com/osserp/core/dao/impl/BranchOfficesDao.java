/**
 *
 * Copyright (C) 2008, 2013 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 05-Aug-2008 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.Constants;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.BranchOffices;
import com.osserp.core.model.system.BranchOfficeImpl;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class BranchOfficesDao extends AbstractRepository implements BranchOffices {
    private static Logger log = LoggerFactory.getLogger(BranchOfficesDao.class.getName());
    private String HEADQUARTER_PROPERTY = "companyHeadquarter";

    public BranchOfficesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public BranchOffice getBranchOffice(Long id) {
        return (BranchOffice) getCurrentSession().load(BranchOfficeImpl.class, id);
    }

    public BranchOffice getHeadquarter() {
        return (BranchOffice) getCurrentSession().load(BranchOfficeImpl.class, getHeadquarterId());
    }

    public List<BranchOffice> getBranchOffices() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(BranchOfficeImpl.class.getName()).append(" o where o.contact.status > -10 order by o.company.id, o.name");
        try {
            return getCurrentSession().createQuery(hql.toString()).list();
        } catch (Throwable t) {
            log.error("getBranchOffices() failed [message=" + t.getMessage() + "]", t);
            throw new BackendException(t);
        }
    }

    public List<BranchOffice> getByCompany(Long id) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(BranchOfficeImpl.class.getName())
            .append(" o where o.contact.status > -10 ")
            .append("and o.company.id = :companyId order by o.name");
        try {
            return getCurrentSession().createQuery(hql.toString())
                    .setParameter("companyId", id)
                    .list();
        } catch (Throwable t) {
            log.error("getByCompany() failed [id=" + id + ", message=" + t.getMessage() + "]", t);
            throw new BackendException(t);
        }
    }

    public BranchOffice getByContact(Long contactId) {
        BranchOffice office = findByContact(contactId);
        if (office == null) {
            throw new BackendException("branch with contactId " + contactId + " does not exist");
        }
        return office;
    }

    public BranchOffice findByContact(Long contactId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(BranchOfficeImpl.class.getName()).append(" o where o.contact.status > -10 ")
            .append("and o.contact.contactId = :contactId order by o.name");
        try {
            List<BranchOffice> list = getCurrentSession().createQuery(hql.toString())
                    .setParameter("contactId", contactId)
                    .list();
            if (!list.isEmpty()) {
                return list.get(0);
            }
        } catch (Throwable t) {
            log.error("findByContact() failed [id=" + contactId + ", message=" + t.getMessage() + "]", t);
        }
        return null;
    }

    public Map<String, Object> createValueMap(Long branch) {
        BranchOffice branchOffice = getBranchOffice(branch == null ? getHeadquarterId() : branch);
        if (log.isDebugEnabled()) {
            log.debug("createValueMap() invoked [branch="
                    + (branchOffice.getId() + ", name=" + branchOffice.getName()) + "]");
        }
        return branchOffice.getCompanyMap();
    }

    public void save(BranchOffice branchOffice) {
        getCurrentSession().saveOrUpdate(BranchOfficeImpl.class.getName(), branchOffice);
    }

    private Long getHeadquarterId() {
        Long result = getPropertyId(HEADQUARTER_PROPERTY);
        if (result == null) {
            result = Constants.HEADQUARTER;
        }
        return result;
    }

}
