<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeGroupConfigView"/>
<v:form name="employeeGroupConfigForm" url="/admin/employees/employeeGroupConfig/save">
<div class="subcolumns">

	<div class="column50l">
		<div class="subcolumnl">

			<div class="spacer"></div>
			<div class="contentBox">
				<div class="contentBoxHeader">
					<div class="contentBoxHeaderLeft">
						<o:out value="${view.bean.id}"/> - <o:out value="${view.bean.name}"/>
					</div>
				</div>
				<div class="contentBoxData">
					<div class="subcolumns">
						<div class="subcolumn">
							<div class="spacer"></div>
							
							<table class="valueTable first33">
								<c:if test="${!empty view.bean.ldapGroup}">
									<tr>
										<td><fmt:message key="ldapGroup"/></td>
										<td><o:out value="${view.bean.ldapGroup}"/></td>
										<td> </td>
									</tr>
								</c:if>
								<tr>
									<td><fmt:message key="postalArea"/></td>
									<td><o:out value="${view.bean.key}"/></td>
									<td> </td>
								</tr>
								<tr>
									<td><fmt:message key="settings"/></td>
									<td><fmt:message key="flowControlAwareLabel"/></td>
									<td style="text-align: right;"> 
										<c:choose>
											<c:when test="${view.bean.flowControlAware}">
												<input type="checkbox" name="flowControlAware" checked="checked" />
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="flowControlAware" />
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
								<tr>
									<td> </td>
									<td><fmt:message key="ldapGroupIgnoreGidLabel"/></td>
									<td style="text-align: right;"> 
										<c:choose>
											<c:when test="${view.bean.ldapGroupIgnoreGid}">
												<input type="checkbox" name="ldapGroupIgnoreGid" checked="checked" />
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="ldapGroupIgnoreGid" />
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
								<tr>
									<td> </td>
									<td><fmt:message key="ldapGroupwareGroupLabel"/></td>
									<td style="text-align: right;"> 
										<c:choose>
											<c:when test="${view.bean.ldapGroupwareGroup}">
												<input type="checkbox" name="ldapGroupwareGroup" checked="checked" />
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="ldapGroupwareGroup" />
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
								<tr>
									<td> </td>
									<td><fmt:message key="ignoringBranchLabel"/></td>
									<td style="text-align: right;"> 
										<c:choose>
											<c:when test="${view.bean.ignoringBranch}">
												<input type="checkbox" name="ignoringBranch" checked="checked" />
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="ignoringBranch" />
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
								<tr>
									<td> </td>
									<td><fmt:message key="branchOnlyLabel"/></td>
									<td style="text-align: right;"> 
										<c:choose>
											<c:when test="${view.bean.branchOnly}">
												<input type="checkbox" name="branchOnly" checked="checked" />
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="branchOnly" />
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</table>

							<div class="spacer"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="spacer"></div>

		</div>
	</div>

	<div class="column50r">
		<div class="subcolumnr">
			<div class="spacer"></div>

			<div class="contentBox">
				<div class="contentBoxHeader">
					<div class="contentBoxHeaderLeft">
						<fmt:message key="groupBehaviour"/>
					</div>
				</div>
				<div class="contentBoxData">
					<div class="subcolumns">
						<div class="subcolumn">
							<div class="spacer"></div>

							<table class="table table-striped">
								<tbody>
									<tr>
										<td><fmt:message key="directorLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.executive}">
													<input type="checkbox" name="executive" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="executive" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td>
											<fmt:message key="executiveLabel"/>
										</td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.executiveCompany}">
													<input type="checkbox" name="executiveCompany" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="executiveCompany" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="salesLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.sales}">
													<input type="checkbox" name="sales" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="sales" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="technicianLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.technician}">
													<input type="checkbox" name="technician" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="technician" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="csLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.cs}">
													<input type="checkbox" name="cs" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="cs" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="purchasingLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.purchasing}">
													<input type="checkbox" name="purchasing" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="purchasing" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="omLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.om}">
													<input type="checkbox" name="om" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="om" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="accountingLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.accounting}">
													<input type="checkbox" name="accounting" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="accounting" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="hrmLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.hrm}">
													<input type="checkbox" name="hrm" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="hrm" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="logisticsLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.logistics}">
													<input type="checkbox" name="logistics" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="logistics" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="itLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.it}">
													<input type="checkbox" name="it" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="it" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="installationLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.installation}">
													<input type="checkbox" name="installation" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="installation" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="wholesaleLabel"/></td>
										<td class="center">
											<c:choose>
												<c:when test="${view.bean.wholesale}">
													<input type="checkbox" name="wholesale" checked="checked" />
												</c:when>
												<c:otherwise>
													<input type="checkbox" name="wholesale" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<c:if test="${view.editMode}">
									<tr>
										<td style="text-align: right;">
											<v:link url="/admin/employees/employeeGroupConfig/disableEditMode" title="exitEdit"><o:img name="backIcon"/></v:link>
										</td>
										<td class="center">
											<input type="image" name="submit" src="<c:url value="${applicationScope.saveIcon}"/>" value="save" title="<fmt:message key='saveInput'/>"/>
										</td>
									</tr>
									</c:if>
								</tbody>
							</table>

							<div class="spacer"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="spacer"></div>
		</div>
	</div>
</div>
</v:form>