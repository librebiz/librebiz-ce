/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 29, 2006 9:52:53 AM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Record;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesCreditNoteType;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesCreditNotes extends PaymentAwareRecords, Records {

    /**
     * Creates a new credit note by an existing invoice
     * @param user adding credit note
     * @param invoice
     * @param bookingType
     * @param copyItems indicates that invoice positions should be copied to new credit note
     * @return creditNote
     */
    CreditNote create(Employee user, Invoice invoice, SalesCreditNoteType bookingType, boolean copyItems);

    /**
     * Creates a new empty and record independent credit note
     * @param user
     * @param office
     * @param customer
     * @param userDefinedId use a user defined record id
     * @param userDefinedDate an optional user defined date. Will be set as created date if provided
     * @param historical indicated if record was created by external app initially
     * @return creditNote
     */
    CreditNote create(Employee user, BranchOffice office, Customer customer, Long userDefinedId, Date userDefinedDate, boolean historical);

    /**
     * Creates a new creditNote by copying an existing
     * @param user
     * @param creditNote
     * @param copyReferenceId indicates if id of reference should be set as reference of new record
     * @param userDefinedId
     * @param userDefinedDate
     * @param historical
     * @return creditNote
     */
    CreditNote create(Employee user, CreditNote creditNote, boolean copyReferenceId, Long userDefinedId, Date userDefinedDate, boolean historical);

    /**
     * Updates the branch office by sales
     * @param sales
     */
    void changeBranch(Sales sales);
    
    /**
     * Cancels a credit note
     * @param user
     * @param note to cancel
     * @param date
     * @param customId
     * @return cancellation created
     * @throws ClientException if invalid custom id provided
     */
    Cancellation cancel(Employee user, CreditNote note, Date date, Long customId) throws ClientException;

    /**
     * Restores canceled credit note
     * @param user
     * @param cancellation
     * @return restored credit note
     * @throws ClientException if restore not supported or user has no permission grant
     */
    CreditNote resetCanceled(Employee user, Cancellation cancellation) throws ClientException;

    /**
     * Finds all creditNotes by order
     * @param order
     * @return creditNotes
     */
    List<Record> getByOrder(Record order);

}
