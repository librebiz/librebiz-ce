/**
 *
 * Copyright (C) 2002 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 08.08.2002 
 * 
 */
package com.osserp.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.postgresql.core.Utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StringUtil {

    public static final String BLANK = " ";
    public static final String COMMA = ",";
    public static final String DOT = ".";
    public static final String MINUS = "-";
    public static final String TAB = "\t";
    public static final String UNDERLINE = "_";

    private static final String DOTS = "...";

    /**
     * @param s
     * @return map
     */
    public static Map<Integer, String> getTokenMap(String s) {
        return getTokenMap(s, BLANK);
    }

    /**
     * @param s
     * @param separator
     * @return map
     */
    public static Map<Integer, String> getTokenMap(String s, String separator) {
        StringTokenizer st = new StringTokenizer(s, separator);
        int cntTokens = st.countTokens();
        Map<Integer, String> result = new HashMap<Integer, String>();
        if (cntTokens > 0) {
            for (int i = 1; i <= cntTokens; i++) {
                result.put(Integer.valueOf(i), st.nextToken());
            }
        }
        return result;
    }

    /**
     * Provides a list of strings by a token string
     * @param s string with tokens separated by a blank
     * @return list of strings
     */
    public static LinkedList<String> getTokenList(String s) {
        return getTokenList(s, BLANK);
    }

    /**
     * Provides a list of strings by a token string
     * @param s
     * @param separator tokens separated by
     * @return list of strings
     */
    public static LinkedList<String> getTokenList(String s, String separator) {
        LinkedList<String> result = new LinkedList<String>();
        if (s != null) {
            StringTokenizer st = new StringTokenizer(s, separator);
            int cntTokens = st.countTokens();
            if (cntTokens > 0) {
                for (int i = 1; i <= cntTokens; i++) {
                    result.add(st.nextToken());
                }
            }
        }
        return result;
    }

    /**
     * A string array with all tokens of given String separated by comma.
     * @param s contains comma separated tokens
     * @return separated tokens or null if s is null
     */
    public static String[] getTokenArray(String s) {
        return getTokenArray(s, COMMA);
    }

    /**
     * A string array with all tokens of given String separated by specified separator String
     * @param s contains separated tokens
     * @param separator to seperate by
     * @return separated tokens or null if s is null
     */
    public static String[] getTokenArray(String s, String separator) {
        if (s == null) {
            return null;
        }
        StringTokenizer st = new StringTokenizer(s, separator);
        int count = st.countTokens();
        String[] result = new String[count];
        if (count > 0) {
            for (int i = 1; i <= count; i++) {
                result[i - 1] = st.nextToken();
            }
        }
        return result;
    }

    /**
     * @param s
     * @return array
     */
    public static String[] getTrimmedTokenArray(String s) {
        if (s == null || s.length() < 1) {
            return null;
        }
        StringBuffer buffer = new StringBuffer(s);
        StringUtil.deleteBlanks(buffer);
        return StringUtil.getTokenArray(buffer.toString(), ",");
    }

    /**
     * Creates mapped properties by a property string with comma separated key-value pairs
     * @param keyValueString (e.g. key1=val1,key2=val2,...)
     * @return map with key-value pairs
     */
    public static Map<String, String> getPropertyMap(String keyValueString) {
        return getPropertyMap(keyValueString, COMMA);
    }

    /**
     * Creates mapped properties by a property string
     * @param keyValueString (e.g. key1=val1;key2=val2;...)
     * @param keyValueSeparator (e.g. comma, semicolon, etc.)
     * @return map with key-value pairs
     */
    public static Map<String, String> getPropertyMap(String keyValueString, String keyValueSeparator) {
        String[] keyValuePairs = getTokenArray(keyValueString, keyValueSeparator);
        if (keyValuePairs == null) {
            return new HashMap<String, String>();
        }
        Map<String, String> result = new HashMap<String, String>();
        for (int i = 0; i < keyValuePairs.length; i++) {
            String[] keyValuePair = getTokenArray(keyValuePairs[i], "=");
            if (keyValuePair != null) {
                result.put(keyValuePair[0], keyValuePair[1]);
            }
        }
        return result;
    }

    /**
     * @param html
     * @return with replaced html entities
     */
    public static String filterHTML(String html) {
        StringBuffer buffer = new StringBuffer(html.length());
        char c;
        for (int i = 0; i < html.length(); i++) {
            c = html.charAt(i);
            if (c == '<') {
                buffer.append("&lt;");
            } else if (c == '>') {
                buffer.append("&gt;");
            } else if (c == '"') {
                buffer.append("&quot;");
            } else if (c == '&') {
                buffer.append("&amp;");
            } else {
                buffer.append(c);
            }
        }
        return buffer.toString();
    }

    /**
     * Checks a user input for invalid sql statements and expressions.
     * The check currently does not allow sql commands and brackets.
     * @param inputValue
     * @return inputValue if check did not fail or null if nothing provided or invalid
     */
    public static String fetchSQLInput(String inputValue) {
        try {
            return getSQLInput(inputValue);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Checks a user input for invalid sql statements and expressions.
     * The check currently does not allow sql commands and brackets.
     * @param inputValue
     * @return inputValue if check did not fail
     * @throws ClientException if invalid strings or characters found
     */
    public static String getSQLInput(String inputValue) throws ClientException {
        if (inputValue == null || inputValue.isEmpty() || inputValue.equals(" ")) {
            return null;
        }
        try {
            StringBuffer buffer = new StringBuffer(inputValue.length());
            String encodable = "()[]+*\\";
            char c;
            for (int i = 0; i < inputValue.length(); i++) {
                c = inputValue.charAt(i);
                if (encodable.indexOf(c) >= 0) {
                    buffer.append("\\");
                }
                buffer.append(c);
            }
            String result = buffer.toString();
            return Utils.escapeLiteral(null, result, true).toString();
        } catch (Exception e) {
            throw new ClientException("org.postgresql.core.Utils.escapeLiteral: " + e.getMessage(), e);
        }
    }

    public static String getArrayAsString(String[] array) {
        if (array == null || array.length < 1) {
            return null;
        }
        StringBuilder buffer = new StringBuilder(96);

        for (int i = 0, j = array.length; i < j; i++) {
            buffer.append(array[i]);
            if (i < j - 1) {
                buffer.append(COMMA);
            }
        }
        return buffer.toString();
    }
    
    public static String[] concat(String[] array1, String[] array2) {
        assert (array1 != null && array2 != null);
        String[] target = new String[ array1.length + array2.length ];
        System.arraycopy(array1, 0, target, 0, array1.length);
        int insertAt = array1.length;
        for (int i = 0; i < array2.length; i++) {
            target[insertAt] = array2[i];
            insertAt++;
        }
        return target;
    }

    public static void replace(StringBuffer text, String key, String value) {
        int i = -1;
        while ((i = text.indexOf(key)) >= 0) {
            text.replace(i, (i + key.length()), value);
        }
    }

    public static void replace(StringBuilder text, String key, String value) {
        int i = -1;
        while ((i = text.indexOf(key)) >= 0) {
            text.replace(i, (i + key.length()), value);
        }
    }

    public static void replace(StringBuilder text, String key, Long value) {
        if (value != null) {
            int i = -1;
            while ((i = text.indexOf(key)) >= 0) {
                text.replace(i, (i + key.length()), value.toString());
            }
        }
    }

    public static boolean existing(StringBuffer text, String key) {
        return (text == null || key == null) ? false : text.indexOf(key) >= 0;
    }

    /**
     * Indicates that text contains pattern (case insensitivity)
     * @param text
     * @param pattern
     * @return true if pattern is contained in text
     */
    public static boolean contains(String text, String pattern) {
        if (text == null || pattern == null) {
            return false;
        }
        return (text.toLowerCase().indexOf(pattern.toLowerCase()) >= 0);
    }

    public static void replace(StringBuffer text, String key, long value) {
        replace(text, key, Long.valueOf(value).toString());
    }

    public static void replace(StringBuffer text, String key, int value) {
        replace(text, key, Integer.valueOf(value).toString());
    }

    public static void replace(StringBuffer text, String key, Integer value) {
        replace(text, key, value.toString());
    }

    public static void replace(StringBuffer text, String key, Long value) {
        replace(text, key, value.toString());
    }

    public static void replace(StringBuffer text, String key, double value) {
        replace(text, key, Double.valueOf(value).toString());
    }

    public static String replaceBlanks(StringBuilder text, String value) {
        if (text != null && value != null) {
            int i = -1;
            while ((i = text.indexOf(BLANK)) >= 0) {
                text.replace(i, (i + BLANK.length()), value.toString());
            }
        }
        return text == null ? null : text.toString();
    }

    public static String replaceBlanks(String text, String value) {
        return text == null ? null : replaceBlanks(new StringBuilder(text), value);
    }

    public static String replaceBlanks(String text) {
        return text == null ? null : replaceBlanks(text, UNDERLINE);
    }

    /**
     * Removes the specified string from given buffer if exists.
     * @param text where to lookup for string to remove
     * @param remove key
     * @return true if key found - indicates that it was removed
     */
    public static boolean remove(StringBuffer text, String remove) {
        int i = text.indexOf(remove);
        if (i >= 0) {
            text.delete(i, i + remove.length());
        }
        return (i >= 0);
    }

    /**
     * Removes the specified string from given buffer if exists.
     * @param text where to lookup for string to remove
     * @param remove key
     * @return true if key found - indicates that it was removed
     */
    public static boolean remove(StringBuilder text, String remove) {
        int i = text.indexOf(remove);
        if (i >= 0) {
            text.delete(i, i + remove.length());
        }
        return (i >= 0);
    }

    /**
     * Replaces key with value.
     * @param text where to lookup for string to remove
     * @param key to replace
     * @param value to insert instead key
     * @return text with replaced tokens provided by key
     */
    public static String replace(String text, String key, String value) {
        StringBuilder b = new StringBuilder(text);
        replace(b, key, value);
        return b.toString();
    }

    /**
     * Removes the specified string from given buffer if exists.
     * @param text where to lookup for string to remove
     * @param remove key
     */
    public static void removeAll(StringBuffer text, String remove) {
        while (remove(text, remove))
            ;
    }

    /**
     * Removes the specified string from given buffer if exists.
     * @param text where to lookup for string to remove
     * @param remove key
     */
    public static void removeAll(StringBuilder text, String remove) {
        while (remove(text, remove))
            ;
    }

    /**
     * Deletes all blanks from given string buffer
     * @param text where we remove the blanks
     */
    public static void deleteBlanks(StringBuffer text) {
        if (text != null && text.length() > 0) {
            while (text.indexOf(BLANK) >= 0) {
                remove(text, BLANK);
            }
        }
    }

    /**
     * Deletes all blanks from given string buffer
     * @param text where we remove the blanks
     */
    public static void deleteBlanks(StringBuilder text) {
        if (text != null && text.length() > 0) {
            while (text.indexOf(BLANK) >= 0) {
                remove(text, BLANK);
            }
        }
    }

    /**
     * Deletes all blanks from given string
     * @param text to remove blanks from
     * @return string without blanks
     */
    public static String deleteBlanks(String text) {
        String result = text;
        if (text != null && text.length() > 0) {
            StringBuilder buffer = new StringBuilder(text); 
            while (buffer.indexOf(BLANK) >= 0) {
                remove(buffer, BLANK);
            }
            result = buffer.toString();
        }
        return result;
    }

    /**
     * Deletes all minus signs from given string buffer
     * @param text where we remove the minus signs
     */
    public static void deleteMinus(StringBuffer text) {
        if (text != null && text.length() > 0) {
            while (text.indexOf(MINUS) >= 0) {
                remove(text, MINUS);
            }
        }
    }

    /**
     * Deletes all minus signs from given string buffer
     * @param text where we remove the minus signs
     */
    public static void deleteMinus(StringBuilder text) {
        if (text != null && text.length() > 0) {
            while (text.indexOf(MINUS) >= 0) {
                remove(text, MINUS);
            }
        }
    }

    /**
     * Cuts a string to length specified by limit if length &gt; limit 
     * If dots is true, three dots are appended at (limit - 3)
     * @param toCut
     * @param limit to cut to
     * @param dots indicates that dots should appended
     * @return cutted
     */
    public static String cut(String toCut, int limit, boolean dots) {
        if (toCut == null || limit == 0 || toCut.length() <= limit) {
            return (toCut == null) ? "" : toCut;
        }
        if (dots) {
            StringBuffer buffer = new StringBuffer(
                    toCut.substring(0, limit - 3));
            return buffer.append(DOTS).toString();
        }
        return toCut.substring(0, limit);
    }

    /**
     * Replaces all control characters found in given text with given delimiter
     * @param text to scan for control tokens
     * @param delimiter to replace control
     * @return with replaced tokens
     */
    public static String replaceControl(String text, String delimiter) {
        if (text != null && text.length() > 0) {
            StringBuilder buffer = new StringBuilder(text.length());
            for (int i = 0, j = text.length(); i < j; i++) {
                char c = text.charAt(i);
                int type = Character.getType(c);
                if (type == Character.CONTROL
                        || type == Character.LINE_SEPARATOR) {
                    buffer.append(delimiter);
                } else {
                    buffer.append(c);
                }
            }
            return buffer.toString();
        }
        return text;
    }

    /**
     * @param text
     * @return list
     */
    @SuppressWarnings("unchecked")
    public static LinkedList divideByControl(String text) {
        LinkedList result = new LinkedList();
        String separated = replaceControl(text, "##");
        if (separated != null && separated.length() > 0) {
            String[] s = separated.split("##");
            for (int i = 0, j = s.length; i < j; i++) {
                result.add(s[i]);
            }
            //result = getTokenList(separated,"#");
        }
        return result;
    }
    
    /**
     * Provides language and country components 
     * @param locale 
     * @return locale as array
     */
    public static final String[] getLocaleParts(String locale) {
        String[] result = new String[2];
        if (locale == null) {
            result[0] = Constants.DEFAULT_LANGUAGE;
        } else if (locale.indexOf("_") > -1) {
            result[0] = locale.split("_")[0];
            result[1] = locale.split("_")[1];
        } else if (locale.length() == 2) {
            result[0] = locale.toLowerCase();
        }
        return result;
    }

    /**
     * Ceates a sheet
     * @param list
     * @return with elements tab separated and newline after every list entry
     */
    public static final String createSheet(List<String[]> list) {
        if (list == null || list.isEmpty()) {
            return "";
        }
        StringBuilder buffer = new StringBuilder();
        for (int i = 0, j = list.size(); i < j; i++) {
            String[] array = list.get(i);
            for (int k = 0, l = array.length; k < l; k++) {
                String s = array[k] == null || "null".equalsIgnoreCase(array[k])
                        ? "" : array[k];
                buffer.append(s).append("\t");
            }
            buffer.append("\n");
        }
        return buffer.toString();
    }

    /**
     * @param array
     * @return string
     */
    public static String createCommaSeparated(String[] array) {
        if (array == null || array.length < 1) {
            return "";
        }
        StringBuilder buff = new StringBuilder();
        for (int i = 0, j = array.length; i < j; i++) {
            String next = array[i];
            buff.append(next);
            if (i < j - 1) {
                buff.append(",");
            }
        }
        return buff.toString();
    }

    /**
     * Creates a comma separated string of array values
     * @param array
     * @return string
     */
    public static String createCommaSeparated(Object[] array) {
        if (array == null || array.length < 1) {
            return "";
        }
        StringBuilder buff = new StringBuilder();
        for (int i = 0, j = array.length; i < j; i++) {
            Object next = array[i];
            buff.append(next);
            if (i < j - 1) {
                buff.append(",");
            }
        }
        return buff.toString();
    }

    /**
     * @param array
     * @return string
     */
    public static String createCommaSeparated(List array) {
        if (array == null || array.size() < 1) {
            return "";
        }
        StringBuilder buff = new StringBuilder();
        for (int i = 0, j = array.size(); i < j; i++) {
            if (i > 0) {
                buff.append(COMMA).append(array.get(i));
            } else {
                buff.append(array.get(i));
            }
        }
        return buff.toString();
    }

    /**
     * @param firstName
     * @param lastName
     * @param reverse
     * @return name
     */
    public static String createName(String firstName, String lastName, boolean reverse) {
        StringBuilder buffer = new StringBuilder();
        if (reverse) {
            buffer.append(lastName);
            if (firstName != null) {
                buffer.append(", ").append(firstName);
            }

        } else {
            if (firstName != null) {
                buffer.append(firstName).append(" ");
            }
            buffer.append(lastName);
        }
        return buffer.toString();
    }

    /**
     * @param is
     * @return string
     */
    public static String createStringFromInputStream(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    /**
     * @param str
     * @return true if string is not null and consists of whitespace only
     */
    public static boolean isWhitespace(String str) {
        return StringUtils.isWhitespace(str);
    }

    /**
     * @param str
     * @return the capitalized string
     */
    public static String capitalize(String str) {
        return StringUtils.capitalize(str);
    }

    public static String createLoginUid(String firstname, int firstnameChars, String lastname, int lastnameChars) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(createUidPart(firstname, firstnameChars));
        buffer.append(createUidPart(lastname, lastnameChars));
        return buffer.toString();
    }

    protected static String createUidPart(String name, int chars) {
        StringBuilder buffer = new StringBuilder();
        if (name != null && name.length() > 0 && chars > 0) {
            StringBuilder tempbuffer = new StringBuilder(name);
            deleteBlanks(tempbuffer);
            deleteMinus(tempbuffer);
            String n = replaceUmlauts(tempbuffer.toString());
            chars = (name.length() == chars && n.length() > name.length()) ? n.length() : chars;
            int breakAt = chars > n.length() ? n.length() : chars;
            for (int i = 0; i < breakAt; i++) {
                buffer.append(n.charAt(i));
            }
        }
        return buffer.toString().toLowerCase();
    }

    public static String replaceUmlauts(String text) {
        StringBuilder buffer = new StringBuilder();
        if (text != null && text.length() > 0) {
            for (int i = 0; i < text.length(); i++) {
                int ucode = text.codePointAt(i);
                switch (ucode) {
                    case 228:
                        buffer.append("ae");
                        break;
                    case 252:
                        buffer.append("ue");
                        break;
                    case 246:
                        buffer.append("oe");
                        break;
                    case 196:
                        buffer.append("Ae");
                        break;
                    case 220:
                        buffer.append("Ue");
                        break;
                    case 214:
                        buffer.append("Oe");
                        break;
                    case 223:
                        buffer.append("ss");
                        break;
                    default:
                        buffer.append(text.charAt(i));
                }
            }
        }
        return buffer.toString();
    }

    public static boolean containsUmlaut(String text) {
        if (text != null && text.length() > 0) {
            for (int i = 0; i < text.length(); i++) {
                int ucode = text.codePointAt(i);
                if (ucode == 228       // ae
                    || ucode == 252    // ue
                    || ucode == 246    // oe
                    || ucode == 196    // Ae
                    || ucode == 220    // Ue
                    || ucode == 214    // Oe
                    || ucode == 223) { // ss
                    return true;
                }
            }
        }
        return false;
    }
    
    public static String toString(Object object) {
        return object == null ? "null" : ToStringBuilder.reflectionToString(object);
    }
    
    public static String toString(Map<String, Object> map) {
        return dump(map, ",");
    }
    
    public static String dump(Map<String, Object> mapToDump, String separator) {
        StringBuffer buffer = new StringBuffer();
        for (String key : mapToDump.keySet()) {
            if (buffer.length() > 0) {
                buffer.append(separator != null ? separator : ",");
            }
            Object value = mapToDump.get(key);
            try {
                buffer.append(key != null ? key : "null");
                buffer.append(":");
                buffer.append(value != null ? value.toString() : "");
            } catch (Exception e) {
                throw new RuntimeException("Exception Occured", e);
            }
        }
        return buffer.toString();
    }
}
