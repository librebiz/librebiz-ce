/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 24, 2016 
 * 
 */
package com.osserp.common.web;

import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.context.WebApplicationContext;

import com.osserp.common.service.Locator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContextUtil {
    private static Logger log = LoggerFactory.getLogger(ContextUtil.class.getName());

    /**
     * Provides the application config
     * @return config
     */
    public static WebConfig getConfig(ServletContext ctx) {
        return (WebConfig) ctx.getAttribute(Globals.WEB_CONFIG);
    }

    /**
     * Creates the web config
     * @param context
     * @param config
     */
    public static void setConfig(ServletContext context, WebConfig config) {
        context.setAttribute(Globals.WEB_CONFIG, config);
    }

    public static Object getService(ServletContext context, String serviceName) {
        Locator locator = getLocator(context);
        if (locator != null) {
            return locator.lookupService(serviceName);
        }
        throw new IllegalStateException("service not bound: " + serviceName);
    }

    public static Object getService(HttpSession session, String serviceName) {
        Locator locator = getServiceLocator(session);
        if (locator != null) {
            return locator.lookupService(serviceName);
        }
        throw new IllegalStateException("service not bound: " + serviceName);
    }

    public static Object getService(HttpServletRequest request, String serviceName) {
        Locator locator = getServiceLocator(request);
        if (locator != null) {
            return locator.lookupService(serviceName);
        }
        throw new IllegalStateException("service not bound: " + serviceName);
    }

    public static Locator getServiceLocator(HttpSession session) {
        return getLocator(session.getServletContext());
    }

    public static Locator getServiceLocator(HttpServletRequest request) {
        return getLocator(request.getServletContext());
    }

    private static Locator getLocator(ServletContext context) {
        Object obj = context.getAttribute(Locator.LOCATOR_CONTEXT_NAME);
        if (obj instanceof Locator) {
            return (Locator) obj;
        }
        String serviceName = Locator.class.getName();
        WebApplicationContext wctx = getWebApplicationContext(context);
        if (wctx.containsBean(serviceName)) {
            try {
                obj = wctx.getBean(serviceName);
                if (obj instanceof Locator) {
                    return (Locator) obj;
                }
            } catch (Exception e) {
                log.warn("getLocator ignoring failed lookup [name=" + serviceName + "]", e);
            }
        }
        log.warn("getLocator: locator not bound");
        return null;
    }

    public static WebApplicationContext getWebApplicationContext(ServletContext context) {
        Enumeration<String> attrNames = context.getAttributeNames();
        while (attrNames.hasMoreElements()) {
            String attrName = attrNames.nextElement();
            if (attrName.startsWith(Globals.SPRING_WEB)
                    && context.getAttribute(attrName) instanceof WebApplicationContext) {
                WebApplicationContext wctx = (WebApplicationContext) context.getAttribute(attrName);
                return wctx;
            }
        }
        log.warn("getWebApplicationContext: context not bound");
        return null;
    }

}
