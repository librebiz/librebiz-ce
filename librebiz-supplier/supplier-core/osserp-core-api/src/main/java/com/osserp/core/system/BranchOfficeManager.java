/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2010 9:53:34 AM 
 * 
 */
package com.osserp.core.system;

import java.util.List;

import com.osserp.common.ClientException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BranchOfficeManager {

    /**
     * Provides the branch office with the given id
     * @param id
     * @return branchOffice or null if not exists
     */
    BranchOffice find(Long id);

    /**
     * Provides a list of all available branch offices
     * @return branchOffices
     */
    List<BranchOffice> findAll();

    /**
     * Provides all company related branchs
     * @param id
     * @return branchs by company
     */
    List<BranchOffice> findByCompany(Long id);

    /**
     * Finds branch by contact
     * @param contactId
     * @return branch by contact
     */
    BranchOffice findByContact(Long contactId);

    /**
     * Finds all branch offices available for provided customer. Use this method to retrieve all available branchs for record actions. This method removes the
     * customer from the result set if customer itself is a branch office.
     * @param customerContactId
     * @return branchOffices
     */
    List<BranchOffice> findByCustomer(Long customerContactId);

    /**
     * Updates branch values
     * @param branchOffice
     * @param name
     * @param company
     * @param shortkey
     * @param shortname
     * @param email
     * @param phoneCountry
     * @param phonePrefix
     * @param phoneNumber
     * @param costCenter
     * @param headquarter
     * @param thirdparty
     * @param customTemplates
     * @param recordTemplatesDefault
     * @return branch with updated values
     * @throws ClientException if validation failed
     */
    BranchOffice update(
            BranchOffice branchOffice,
            String name,
            SystemCompany company,
            String shortkey,
            String shortname,
            String email,
            String phoneCountry,
            String phonePrefix,
            String phoneNumber,
            Long costCenter,
            Boolean headquarter,
            Boolean thirdparty,
            Boolean customTemplates,
            Boolean recordTemplatesDefault) throws ClientException;

    /**
     * Creates a new branch office
     * @param branchOffice
     * @return new created branchOffice
     * @throws ClientException if validation failed
     */
    BranchOffice create(BranchOffice branchOffice) throws ClientException;
    
    
    /**
     * Provides the maximum length for the branch shortkey
     * @return max length of shortkey
     */
    int getShortkeyMaxlength();
    
}
