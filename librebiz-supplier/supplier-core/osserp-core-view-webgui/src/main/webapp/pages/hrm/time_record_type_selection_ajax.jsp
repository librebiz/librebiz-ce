<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.timeRecordingView}"/>
<c:set var="intervalOnly" value="${requestScope.intervalOnly}"/>
<c:set var="intervalTypes" value="${view.intervalTypes}"/>

<c:if test="${!empty view}">
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <fmt:message key="promptBookingTypeSelection"/>
        </div>
    </div>
    <div class="modalBoxData">
        <div class="col-md-12 panel-area">
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <tbody>
                        <c:choose>
                            <c:when test="${empty intervalTypes}">
                                <tr><td colspan="2"><fmt:message key="noTypesAvailable"/></td></tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="obj" items="${intervalTypes}">
                                    <tr>
                                        <td><o:out value="${obj.id}"/></td>
                                        <td><a href="<c:url value="/timeRecordings.do"><c:param name="method" value="selectType"/><c:param name="id" value="${obj.id}"/></c:url>"><o:out value="${obj.name}"/></a></td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</c:if>
