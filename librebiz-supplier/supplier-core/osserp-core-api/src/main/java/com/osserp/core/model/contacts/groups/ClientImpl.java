/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Jul-2006 
 * 
 */
package com.osserp.core.model.contacts.groups;

import com.osserp.common.BackendException;
import com.osserp.core.clients.Client;
import com.osserp.core.contacts.Contact;
import com.osserp.core.model.contacts.AbstractContactAware;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ClientImpl extends AbstractContactAware implements Client {

    private String name = null;
    private boolean logoAvailable = false;
    private boolean companyRegistrationAvailable = false;

    protected ClientImpl() {
        setGroupId(Client.CLIENT);
    }

    public ClientImpl(Long createdBy, Contact contact) {
        super(Client.CLIENT, createdBy, contact, null);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLogoAvailable() {
        return logoAvailable;
    }

    public void setLogoAvailable(boolean logoAvailable) {
        this.logoAvailable = logoAvailable;
    }

    public boolean isCompanyRegistrationAvailable() {
        return companyRegistrationAvailable;
    }

    public void setCompanyRegistrationAvailable(boolean companyRegistrationAvailable) {
        this.companyRegistrationAvailable = companyRegistrationAvailable;
    }

    public String getResourceKey() {
        return null;
    }

    public void setResourceKey(String resourceKey) {
    }

    public String getDescription() {
        return null;
    }

    public void setDescription(String description) {
    }

    @Override
    public Object clone() {
        throw new BackendException("CloneNotSupported");
    }

    @Override
    public Long getGroupId() {
        Long id = super.getGroupId();
        return (id != null ? id : Contact.CLIENT);
    }

    public boolean isSelectionLabel() {
        return false;
    }
}
