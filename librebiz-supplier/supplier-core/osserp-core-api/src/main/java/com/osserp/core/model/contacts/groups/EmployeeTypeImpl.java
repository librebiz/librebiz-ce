/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2009 1:46:21 AM 
 * 
 */
package com.osserp.core.model.contacts.groups;

import com.osserp.common.beans.OptionImpl;
import com.osserp.core.employees.EmployeeType;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class EmployeeTypeImpl extends OptionImpl implements EmployeeType {

    private boolean defaultType = false;
    private boolean external = false;
    private boolean salesAgent = false;
    private boolean partner = false;

    protected EmployeeTypeImpl() {
        super();
    }

    protected EmployeeTypeImpl(EmployeeType o) {
        super(o);
        this.defaultType = o.isDefaultType();
        this.external = o.isExternal();
    }

    public boolean isDefaultType() {
        return defaultType;
    }

    protected void setDefaultType(boolean defaultType) {
        this.defaultType = defaultType;
    }

    public boolean isExternal() {
        return external;
    }

    protected void setExternal(boolean external) {
        this.external = external;
    }

    public boolean isSalesAgent() {
        return salesAgent;
    }

    protected void setSalesAgent(boolean salesAgent) {
        this.salesAgent = salesAgent;
    }

    public boolean isPartner() {
        return partner;
    }

    protected void setPartner(boolean partner) {
        this.partner = partner;
    }
}
