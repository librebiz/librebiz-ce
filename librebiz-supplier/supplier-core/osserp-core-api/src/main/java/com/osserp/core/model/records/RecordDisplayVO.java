/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 18, 2008 2:50:00 PM 
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordDisplayItem;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Records;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.purchasing.PurchaseOrder;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordDisplayVO extends FinanceRecordDisplayVO implements RecordDisplay {

    private Long type = null;
    private Long subType = null;
    private Long paymentConditionId = null;
    
    private Double tax = null;
    private Double reducedTax = null;
    
    private Long signatureLeft = null;
    private Long signatureRight = null;
    
    private Date dateSent = null;
    private Date delivery = null;
    private boolean deliveryConfirmed = false;

    private Date maturity = null;
    private Double paidAmount = 0d;

    private Double prorateTax = 0d;
    private Double prorateNetAmount = 0d;
    private Double prorateTaxAmount = 0d;
    private Double prorateReducedTaxAmount = 0d;
    private Double prorateGrossAmount = 0d;

    private List<RecordDisplayItem> items = new ArrayList<RecordDisplayItem>();

    private List<RecordDisplay> creditNotes = new ArrayList<RecordDisplay>();

    protected RecordDisplayVO() {
        super();
    }

    protected RecordDisplayVO(RecordDisplay o) {
        super(o);
        type = o.getType();
        subType = o.getSubType();
        tax = o.getTax();
        reducedTax = o.getReducedTax();
        paymentConditionId = o.getPaymentConditionId();
        signatureLeft = o.getSignatureLeft();
        signatureRight = o.getSignatureRight();
        dateSent = o.getDateSent();
        delivery = o.getDelivery();
        deliveryConfirmed = o.isDeliveryConfirmed();

        maturity = o.getMaturity();
        paidAmount = o.getPaidAmount();

        prorateTax = o.getProrateTax();
        prorateNetAmount = o.getProrateNetAmount();
        prorateTaxAmount = o.getProrateTaxAmount();
        prorateReducedTaxAmount = o.getProrateReducedTaxAmount();
        prorateGrossAmount = o.getProrateGrossAmount();

        items = o.getItems();
    }

    public RecordDisplayVO(Map<String, Object> map) {
        super(map);
        type = (Long) map.get("type");
        subType = (Long) map.get("subType");
        signatureLeft = (Long) map.get("signatureLeft");
        signatureRight = (Long) map.get("signatureRight");
        tax = (Double) map.get("tax");
        reducedTax = (Double) map.get("reducedTax");
        paymentConditionId = (Long) map.get("paymentConditionId");
        dateSent = (Date) map.get("dateSent");
        if (map.containsKey("maturity")) {
            maturity = (Date) map.get("maturity");
        }
        Double pa = (Double) map.get("paidAmount");
        if (isSet(pa)) {
            paidAmount = pa;
        }
        if (isDownpayment()) {
            prorateTax = (Double) map.get("prorateTax");
            prorateNetAmount = (Double) map.get("prorateNetAmount");
            prorateTaxAmount = (Double) map.get("prorateTaxAmount");
            prorateReducedTaxAmount = (Double) map.get("prorateReducedTaxAmount");
            prorateGrossAmount = (Double) map.get("prorateGrossAmount");
        }
        Long itemId = (Long) map.get("itemId");
        if (isSet(itemId)) {
            addItem(map);
        }
    }

    public void addItem(Map<String, Object> map) {
        if (items == null) {
            items = new ArrayList<RecordDisplayItem>();
        }
        RecordDisplayItem item = new RecordDisplayItemVO(map);
        if (isSet(item.getId()) && isSet(item.getProductId())) {
            
            if (item.getDelivery() != null) {
                if (delivery == null) {
                    delivery = item.getDelivery();
                } else if (delivery.after(item.getDelivery())) {
                    delivery = item.getDelivery();
                }
            }
            boolean added = false;
            boolean ddavailable = false;
            for (Iterator<RecordDisplayItem> i = items.iterator(); i.hasNext();) {
                RecordDisplayItem next = i.next();
                if (next.getId().equals(item.getId())) {
                    for (Iterator<Long> types = item.getTypeIds().iterator(); types.hasNext();) {
                        next.getTypeIds().add(types.next());
                    }
                    added = true;
                }
                if (next.getDelivery() != null) {
                    ddavailable = true;
                }
            }
            if (!added) {
                items.add(item);
                if (!item.isDeliveryConfirmed()) {
                    deliveryConfirmed = false;
                }
                if (ddavailable) {
                    CollectionUtil.sort(items, createDeliveryDateComparator());
                } else {
                    CollectionUtil.sort(items, createByIdComparator());
                }
            }
        }
    }

    public Date getMaturity() {
        return maturity;
    }

    public void setMaturity(Date maturity) {
        this.maturity = maturity;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Long getPaymentConditionId() {
        return paymentConditionId;
    }

    public void setPaymentConditionId(Long paymentConditionId) {
        this.paymentConditionId = paymentConditionId;
    }

    public Double getProrateGrossAmount() {
        if (prorateGrossAmount == null) {
            return getGrossAmount();
        }
        return prorateGrossAmount;
    }

    public void setProrateGrossAmount(Double prorateGrossAmount) {
        this.prorateGrossAmount = prorateGrossAmount;
    }

    public Double getProrateNetAmount() {
        if (prorateNetAmount == null) {
            return getNetAmount();
        }
        return prorateNetAmount;
    }

    public void setProrateNetAmount(Double prorateNetAmount) {
        this.prorateNetAmount = prorateNetAmount;
    }

    public Double getProrateReducedTaxAmount() {
        if (prorateGrossAmount == null) {
            return getReducedTaxAmount();
        }
        return prorateReducedTaxAmount;
    }

    public void setProrateReducedTaxAmount(Double prorateReducedTaxAmount) {
        this.prorateReducedTaxAmount = prorateReducedTaxAmount;
    }

    public Double getProrateTax() {
        return prorateTax;
    }

    public void setProrateTax(Double prorateTax) {
        this.prorateTax = prorateTax;
    }

    public Double getProrateTaxAmount() {
        if (prorateGrossAmount == null) {
            return getTaxAmount();
        }
        return prorateTaxAmount;
    }

    public void setProrateTaxAmount(Double prorateTaxAmount) {
        this.prorateTaxAmount = prorateTaxAmount;
    }

    public Double getReducedTax() {
        return reducedTax;
    }

    public void setReducedTax(Double reducedTax) {
        this.reducedTax = reducedTax;
    }

    public Double getTaxTotal() {
        return (getTaxAmount() == null ? 0 : getTaxAmount()) 
                + (getReducedTaxAmount() == null ? 0 : getReducedTaxAmount());
    }

    public Double getProrateTaxTotal() {
        return (getProrateTaxAmount() == null ? 0 : getProrateTaxAmount()) 
                + (getProrateReducedTaxAmount() == null ? 0 : getProrateReducedTaxAmount());
    }

    public Long getSignatureLeft() {
        return signatureLeft;
    }

    public void setSignatureLeft(Long signatureLeft) {
        this.signatureLeft = signatureLeft;
    }

    public Long getSignatureRight() {
        return signatureRight;
    }

    public void setSignatureRight(Long signatureRight) {
        this.signatureRight = signatureRight;
    }

    public Long getSubType() {
        return subType;
    }

    public void setSubType(Long subType) {
        this.subType = subType;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Date getTaxPoint() {
        return getCreated();
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Date getDelivery() {
        return delivery;
    }

    public void setDelivery(Date delivery) {
        this.delivery = delivery;
    }

    public boolean isDeliveryConfirmed() {
        return deliveryConfirmed;
    }

    public void setDeliveryConfirmed(boolean deliveryConfirmed) {
        this.deliveryConfirmed = deliveryConfirmed;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public boolean isDownpayment() {
        return (type == null) ? false : RecordType.SALES_DOWNPAYMENT.equals(type);
    }

    public Double getDueAmount() {
        if (RecordType.SALES_DOWNPAYMENT.equals(type)
                || RecordType.SALES_INVOICE.equals(type)
                || RecordType.PURCHASE_INVOICE.equals(type)) {
            Double amount = getGrossAmount();
            if (isDownpayment()) {
                amount = prorateGrossAmount;
            }
            if (isSet(amount)) {
                double due = amount - (isNotSet(paidAmount) ? 0 : paidAmount);
                if (due > -0.01 && due < 0.01) {
                    return 0d;
                }
                return Double.valueOf(due);
            }
        }
        return 0d;
    }

    public boolean isPaid() {
        return Double.valueOf(0).equals(getDueAmount());
    }

    public boolean isUnchangeable() {
        if (getStatus() == null) {
            return true;
        }
        return (getStatus().longValue() >= Record.STAT_SENT.longValue());
    }

    public boolean isCompleted() {
        if (getStatus() == null) {
            return false;
        }
        return (getStatus().longValue() >= Record.STAT_CLOSED.longValue());
    }

    public boolean isSupportingOverdue() {
        if (type == null) {
            return false;
        }
        return PurchaseOrder.RECORD_TYPE.equals(type);
    }

    public boolean isOverdue() {
        if (!isSupportingOverdue()) {
            return false;
        }
        if (getDelivery() != null) {
            try {
                String tmpNow = DateFormatter.getDate(new Date(System.currentTimeMillis()));
                Date now = DateUtil.createDate(tmpNow, true);
                return now.after(getDelivery());
            } catch (Throwable e) {
            }
        }
        return true;
    }

    public boolean isPaymentRecord() {
        return Records.isPayment(type);
    }

    public boolean isPurchaseRecord() {
        return Contact.SUPPLIER.equals(getContactType());
    }

    @Override
    public String[] getValues() {
        String[] s = super.getValues();
        String[] r = new String[14];
        r[0] = s[0];
        r[1] = s[1];
        r[2] = s[2];
        r[3] = s[3];
        r[4] = s[4];
        r[5] = s[5];
        r[6] = s[6];
        r[7] = s[7];
        r[8] = s[8];
        r[9] = s[9];
        r[10] = createDateForSheet(maturity);
        if (isDownpayment()) {
            r[11] = createCurrencyForSheet(((prorateGrossAmount == null
                    || prorateGrossAmount == 0)
                    ? getGrossAmount() : prorateGrossAmount));
        } else {
            r[11] = createCurrencyForSheet(getGrossAmount());
        }
        r[12] = createCurrencyForSheet(paidAmount);
        r[13] = createCurrencyForSheet(getDueAmount());
        return r;
    }

    @Override
    public String[] getValueKeys() {
        return new String[] {
            "recordId",
            "companyId",
            "contactId",
            "contactName",
            "street",
            "zipcode",
            "city",
            "reference",
            "sales",
            "created",
            "maturity",
            "grossAmount",
            "paidAmount",
            "dueAmount" };
    }

    public List<RecordDisplay> getCreditNotes() {
        return creditNotes;
    }

    public void setCreditNotes(List<RecordDisplay> creditNotes) {
        this.creditNotes = creditNotes;
    }

    public List<RecordDisplayItem> getItems() {
        return items;
    }

    public void setItems(List<RecordDisplayItem> items) {
        this.items = items;
    }

    public boolean matching(List<ProductSelectionConfigItem> selection) {
        if (items == null || items.isEmpty()) {
            return true;
        }
        for (int i = 0, j = items.size(); i < j; i++) {
            RecordDisplayItem next = items.get(i);
            for (int k = 0, l = selection.size(); k < l; k++) {
                ProductSelectionConfigItem config = selection.get(k);
                if (config.isMatching(next.getProductId(), next.getTypeIds(), next.getGroupId(), next.getCategoryId())) {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressWarnings("rawtypes")
    protected Comparator createDeliveryDateComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                RecordDisplayItem orderA = (RecordDisplayItem) a;
                RecordDisplayItem orderB = (RecordDisplayItem) b;
                Date dateA = orderA.getDelivery();
                Date dateB = orderB.getDelivery();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return 1;
                }
                if (dateB == null) {
                    return -1;
                }
                return dateA.compareTo(dateB);
            }
        };
    }

    @SuppressWarnings("rawtypes")
    protected Comparator createByIdComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                RecordDisplayItem recA = (RecordDisplayItem) a;
                RecordDisplayItem recB = (RecordDisplayItem) b;
                Long idA = recA.getId();
                Long idB = recB.getId();
                if (idA == null && idB == null) {
                    return 0;
                }
                if (idA == null) {
                    return 1;
                }
                if (idB == null) {
                    return -1;
                }
                return idA.compareTo(idB);
            }
        };
    }
}
