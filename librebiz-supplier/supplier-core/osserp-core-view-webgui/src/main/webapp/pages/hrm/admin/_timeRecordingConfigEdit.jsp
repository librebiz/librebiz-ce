<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<o:form id="timeRecordingForm" name="timeRecordingForm" url="/timeRecordingConfig.do">
    <input type="hidden" name="method" value="save" />

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><o:out value="${config.id}" /><span> - </span><o:out value="${config.name}" /></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="name" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${config.defaultConfig}">
                                    <o:out value="${config.name}" /><input type="hidden" name="name" value="<o:out value="${config.name}"/>" />
                                </c:when>
                                <c:otherwise>
                                    <input type="text" class="form-control" name="name" value="<o:out value="${config.name}"/>" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="description" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <textarea name="description" class="form-control" rows="4"><o:out value="${config.description}" /></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="annualLeave" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control right" name="annualLeave" value="<o:number value="${config.annualLeave}" format="currency" />" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="form-hint">
                                <fmt:message key="daysLabel" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="hoursPerDayLabel" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control right" name="hoursDaily" value="<o:number value="${config.hoursDaily}" format="currency" />" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="form-hint">
                                <fmt:message key="hourShort" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="maximumCarryoverHoursLabel" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control right" name="carryoverHoursMaxDisplay" value="<o:number value="${config.carryoverHoursMax}" format="currency" />" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="form-hint">
                                <fmt:message key="hourShort" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="carryoverHoursLabel" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control right" name="carryoverHours" value="<o:number value="${period.carryoverHours}" format="currency"/>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="form-hint">
                                <fmt:message key="hourShort" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="maximumCarryoverHoursLabel" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control right" name="carryoverHoursMax" value="<o:number value="${config.carryoverHoursMax}" format="currency" />" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="form-hint">
                                <fmt:message key="hourShort" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="coreTime" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="coreTimeStartHours" value="<o:out value="${config.coreTimeStartHours}"/>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="coreTimeStartMinutes" value="<o:out value="${config.coreTimeStartMinutes}"/>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="form-hint">
                                        <fmt:message key="until" />
                                     </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="coreTimeEndHours" value="<o:out value="${config.coreTimeEndHours}"/>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="coreTimeEndMinutes" value="<o:out value="${config.coreTimeEndMinutes}"/>" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="form-hint">
                                        <fmt:message key="clock" />
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <c:if test="${!config.defaultConfig}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="hoursRecording" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="checkbox">
                                    <c:choose>
                                        <c:when test="${config.recordingHours}">
                                            <label for="hours">
                                                <input type="checkbox" name="recordingHours" checked="checked" />
                                            </label>
                                        </c:when>
                                        <c:otherwise>
                                            <label for="hours">
                                                <input type="checkbox" name="recordingHours" />
                                            </label>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>

                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/timeRecordingConfig.do?method=disableEdit"/>');" value="<fmt:message key="break"/>" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</o:form>
