/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 20, 2006 2:23:52 PM 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.Options;
import com.osserp.core.QuantityUnit;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactSearch;
import com.osserp.core.dao.Letters;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.dms.Letter;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Amounts;
import com.osserp.core.finance.CalculationAwareRecord;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.DeliveryCondition;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.FinanceRecord;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordCorrection;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordOutputService;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordPrintOption;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Records;
import com.osserp.core.model.records.AbstractBookingType;
import com.osserp.core.products.ProductDescription;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.service.doc.XmlService;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class RecordOutputServiceImpl extends AbstractRecordOutputService
        implements RecordOutputService {
    private static Logger log = LoggerFactory.getLogger(RecordOutputServiceImpl.class.getName());

    private static final String FORMAT_CONDITIONS = "documentConditionsFormatted";
    private RecordPrintOptionDefaults recordPrintOptionDefaults = null;
    private SystemConfigs systemConfigs = null;
    private SalesCreditNotes salesCreditNotes = null;
    private SalesInvoices salesInvoices = null;
    private SalesOrders salesOrders = null;
    private SalesOrderDownpayments downpayments = null;
    private ContactSearch contactSearch = null;
    private Products products = null;
    private Projects projects = null;
    private String nullAmount = null;

    /**
     * Creates a new record output service
     * @param resourceLocator 
     * @param optionsCache
     * @param stylesheetService
     * @param xmlService
     * @param systemConfigs
     * @param projects
     * @param salesCreditNotes
     * @param salesInvoices
     * @param salesOrders
     * @param downpayments
     * @param contactSearch
     * @param products
     * @param letters
     * @param recordPrintOptionDefaults
     */
    protected RecordOutputServiceImpl(
            ResourceLocator resourceLocator, 
            OptionsCache optionsCache,
            CoreStylesheetService stylesheetService,
            XmlService xmlService,
            SystemConfigs systemConfigs,
            Projects projects,
            SalesCreditNotes salesCreditNotes,
            SalesInvoices salesInvoices,
            SalesOrders salesOrders,
            SalesOrderDownpayments downpayments,
            ContactSearch contactSearch,
            Products products,
            Letters letters,
            RecordPrintOptionDefaults recordPrintOptionDefaults) {

        super(resourceLocator, optionsCache, stylesheetService, xmlService, letters);
        this.systemConfigs = systemConfigs;
        this.salesCreditNotes = salesCreditNotes;
        this.salesInvoices = salesInvoices;
        this.salesOrders = salesOrders;
        this.downpayments = downpayments;
        this.contactSearch = contactSearch;
        this.products = products;
        this.projects = projects;
        this.recordPrintOptionDefaults = recordPrintOptionDefaults;
        this.nullAmount = NumberFormatter.getValue(new BigDecimal("0.0"), NumberFormatter.CURRENCY);
    }

    protected final boolean isSystemPropertyEnabled(String name) {
        if (systemConfigs != null) {
            return systemConfigs.isSystemPropertyEnabled(name);
        }
        return false;
    }

    public Document createDocument(Employee employee, Record record, boolean preview) {

        Document doc = xmlService.createDocument(employee);
        if (record.getBranchId() != null) {
            doc.getRootElement().addContent(xmlService.createBranch("branch", record.getBranchId()));
        }
        doc.getRootElement().addContent(createCoverLetter(employee, record));
        doc.getRootElement().addContent(createPaymentAgreement(record));
        doc.getRootElement().addContent(createRecord("record", record, preview));
        return doc;
    }

    public void addBusinessCase(Document doc, BusinessCase businessCase) {
        Element bc = xmlService.createBusinessCase("businessCase", businessCase);
        doc.getRootElement().addContent(bc);
        if (log.isDebugEnabled()) {
            log.debug("addBusinessCase() done [id=" + businessCase.getPrimaryKey() + "]");
        }
    }

    public String createStylesheetText(Employee employee, Record record) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("createStylesheetText() invoked [record=" + (record == null ? "null" : (record.getId())
                    + ", type=" + record.getType().getId()) + "]");
        }
        Map<String, Object> templateOptions = createInitialTemplateOptions(record);
        return stylesheetService.getStylesheetText(record, templateOptions);
    }

    public byte[] createPdf(Record record, Document doc) throws ClientException {
        logDocument("recordPdf", doc);
        return createPdf(getXsl(record), doc);
    }

    public byte[] createCorrectionPdf(Document doc, Record record) throws ClientException {
        logDocument("correctionPdf", doc);
        String name = "xsl/records/xsl_record_correction";
        return createPdf(getXsl(name, record.getBranchId(), fetchLocale(record)), doc);
    }

    private void logDocument(String method, Document doc) {
        if (isSystemPropertyEnabled("documentOutputDebugXML") && log.isInfoEnabled()) {
            log.info(method + ":\n" + JDOMUtil.getPrettyString(doc));
        }
    }

    public Element createCorrection(Record record) throws ClientException {
        if (record instanceof PaymentAwareRecord) {
            PaymentAwareRecord pa = (PaymentAwareRecord) record;
            RecordCorrection rc = pa.getCorrection();
            if (rc != null) {
                Element correction = rc.getXml();
                if (rc.getChangedBy() != null
                        && rc.getChanged() != null
                        && rc.getChanged().after(rc.getCreated())) {
                    correction.addContent(xmlService.createEmployee("signatureLeft", rc.getChangedBy()));
                } else if (rc.getCreatedBy() != null) {
                    correction.addContent(xmlService.createEmployee("signatureLeft", rc.getCreatedBy()));
                }
                if (isSet(rc.getNote())) {
                    correction.addContent(createNote(rc.getNote()));
                }
                if (log.isDebugEnabled()) {
                    log.debug("createCorrection() done");
                }
                return correction;
            }
            log.warn("createCorrection() no correction found [record=" + record.getId()
                    + ", type=" + record.getType().getId() + "]");
        } else {
            log.warn("createCorrection() record not required instance [required="
                    + PaymentAwareRecord.class.getName()
                    + ", found=" + (record == null ? "null" : record.getClass().getName()) + "]");
        }
        throw new ClientException(ErrorCode.VALUES_MISSING);
    }
    
    private Locale fetchLocale(Record record) {
        return record.getLanguage() == null ? 
                Constants.DEFAULT_LOCALE_OBJECT :
                    new Locale(record.getLanguage());
    }

    private Element createPaymentAgreement(Record rec) {
        Element pc = new Element("paymentAgreement");
        RecordPaymentAgreement pa = null;
        if (rec instanceof Offer) {
            pa = ((Offer) rec).getPaymentAgreement();
        } else if (rec instanceof Order) {
            pa = ((Order) rec).getPaymentAgreement();
        }
        if (pa != null) {
            addPaymentAgreement(pc, rec.getCreated(), pa);
        }
        return pc;
    }

    private void addPaymentAgreement(Element root, Date created, RecordPaymentAgreement pa) {
        if (log.isDebugEnabled()) {
            log.debug("addPaymentAgreement() invoked...");
        }
        if (pa.getNote() == null) {
            root.addContent(new Element("note"));
        } else {
            root.addContent(createNote(pa.getNote()));
        }
        if (pa.isHidePayments()) {
            root.addContent(new Element("hidePayments").setText("true"));
        } else {
            root.addContent(new Element("hidePayments").setText("false"));
        }
        if (pa.isAmountsFixed()) {
            root.addContent(new Element("amountsFixed").setText("true"));
            root.addContent(new Element("downpaymentAmount").setText(
                    NumberFormatter.getValue(pa.getDownpaymentAmount(), 
                            NumberFormatter.CURRENCY)));
            root.addContent(new Element("deliveryInvoiceAmount").setText(
                    NumberFormatter.getValue(pa.getDeliveryInvoiceAmount(), 
                            NumberFormatter.CURRENCY)));
            root.addContent(new Element("finalInvoiceAmount").setText(
                    NumberFormatter.getValue(pa.getFinalInvoiceAmount(), 
                            NumberFormatter.CURRENCY)));
        } else {
            root.addContent(new Element("amountsFixed").setText("false"));
            root.addContent(new Element("downpaymentPercent").setText(
                    xmlService.createString(pa.getDownpaymentPercent())));
            root.addContent(new Element("deliveryInvoicePercent").setText(
                    xmlService.createString(pa.getDeliveryInvoicePercent())));
            root.addContent(new Element("finalInvoicePercent").setText(
                    xmlService.createString(pa.getFinalInvoicePercent())));
        }
        root.addContent(getPaymentTarget(
                "downpaymentTargetName", pa.getDownpaymentTargetId()));
        root.addContent(getPaymentTarget(
                "deliveryInvoiceTargetName", pa.getDeliveryInvoiceTargetId()));
        root.addContent(getPaymentTarget(
                "finalInvoiceTargetName", pa.getFinalInvoiceTargetId()));

        root.addContent(createPaymentCondition(created, pa.getPaymentCondition()));
        if (log.isDebugEnabled()) {
            log.debug("addPaymentAgreement() done...");
        }
    }

    private Element createPaymentCondition(Date created, PaymentCondition condition) {
        if (log.isDebugEnabled()) {
            log.debug("createPaymentCondition() invoked...");
        }
        Element pc = new Element("paymentCondition");
        if (condition == null) {
            log.warn("createPaymentCondition() received null condition!");
            return pc;
        }
        Integer target = getPaymentTarget(condition.getPaymentTarget());
        Date targetDate = DateUtil.addDays(
                created,
                target.intValue());
        pc.addContent(new Element("id").setText(condition.getId().toString()));
        pc.addContent(new Element("name").setText(condition.getName()));
        pc.addContent(new Element("target").setText(target.toString()));
        pc.addContent(new Element("targetDate").setText(
                DateFormatter.getDate(targetDate)));
        if (condition.isDonation()) {
            pc.addContent(new Element("isDonation").setText("1"));
        } else {
            pc.addContent(new Element("isDonation").setText("0"));
        }
        if (condition.isPaid()) {
            pc.addContent(new Element("isPaid").setText("1"));
        } else {
            pc.addContent(new Element("isPaid").setText("0"));
        }
        if (condition.isWithDiscount()) {
            pc.addContent(new Element("hasDiscount").setText("1"));
        } else {
            pc.addContent(new Element("hasDiscount").setText("0"));
        }
        if (condition.getDiscount1() != null) {
            pc.addContent(new Element("discount1").setText(
                    condition.getDiscount1().toString()));
        } else {
            pc.addContent(new Element("discount1").setText("0"));
        }
        if (condition.getDiscount1Target() != null) {
            Date date = DateUtil.addDays(
                    created,
                    condition.getDiscount1Target().intValue());
            pc.addContent(new Element("discount1Target").setText(
                    DateFormatter.getDate(date)));
        } else {
            pc.addContent(new Element("discount1Target").setText(
                    DateFormatter.getDate(created)));
        }
        if (condition.getDiscount2() != null) {
            pc.addContent(new Element("discount2").setText(
                    condition.getDiscount2().toString()));
        } else {
            pc.addContent(new Element("discount2").setText("0"));
        }
        if (condition.getDiscount2Target() != null) {
            Date date = DateUtil.addDays(
                    created,
                    condition.getDiscount2Target().intValue());
            pc.addContent(new Element("discount2Target").setText(
                    DateFormatter.getDate(date)));
        } else {
            pc.addContent(new Element("discount2Target").setText(
                    DateFormatter.getDate(created)));
        }
        if (log.isDebugEnabled()) {
            log.debug("createPaymentCondition() done...");
        }
        return pc;
    }

    private Integer getPaymentTarget(Integer value) {
        return (value == null) ? PaymentCondition.DEFAULT_TARGET : value;
    }

    private Element createCoverLetter(Employee employee, Record record) {
        Letter letter = getCoverLetter(record);
        if (letter != null && letter.isEmbedded() && letter.isEmbeddedAbove()) {
            try {
                Element result = xmlService.createLetter(employee, letter, "letter");
                result.addContent(new Element("available").setText("true"));
                return result;
            } catch (Exception e) {
                log.warn("createCoverLetter failed: " + e.getMessage(), e);
            }
        }
        return noLetterAvailable();
    }

    private Element noLetterAvailable() {
        Element result = new Element("letter");
        result.addContent(new Element("available").setText("false"));
        return result;
    }

    private Element createRecord(String name, Record rec, boolean preview) {
        Element root = new Element(name);
        root.addContent(new Element("id").setText(rec.getNumber()));
        if (rec.getType() != null) {
            root.addContent(new Element("type").setText(rec.getType().getId().toString()));
        }
        if (isSet(rec.getStatus())) {
            root.addContent(new Element("status").setText(rec.getStatus().toString()));
        }
        root.addContent(new Element("lang").setText(rec.getLanguage()));
        if (rec.getBookingType() instanceof AbstractBookingType) {
            root.addContent(((AbstractBookingType) rec.getBookingType()).getXML("bookingType"));
        }
        root.addContent(new Element("preview").setText(Boolean.valueOf(preview).toString()));
        if (rec.isCanceled()) {
            root.addContent(new Element("canceled").setText(Boolean.valueOf(true).toString()));
        } else {
            root.addContent(new Element("canceled").setText(Boolean.valueOf(false).toString()));
        }
        if (rec.isInternal()) {
            root.addContent(new Element("internal").setText("true"));
        } else {
            root.addContent(new Element("internal").setText("false"));
        }
        // company and branch office
        Element comp = new Element("company");
        
        String companyName = getOptionName(Options.SYSTEM_COMPANIES, rec.getCompany());
        if (isSet(companyName)) {
            comp.addContent(new Element("id").setText(rec.getCompany().toString()));
            comp.addContent(new Element("companyName").setText(companyName));
        } else {
            comp.addContent(new Element("id"));
            comp.addContent(new Element("companyName"));
        } 
        
        String branchName = getOptionName(Options.BRANCH_OFFICES, rec.getBranchId());
        if (isSet(branchName)) {
            comp.addContent(new Element("branch").setText(rec.getBranchId().toString()));
            comp.addContent(new Element("branchName").setText(branchName));
        } else {
            comp.addContent(new Element("branch"));
            comp.addContent(new Element("branchName"));
        }
        root.addContent(comp);

        // contact
        if (rec.getContact() != null) {
            Element classifiedContact = rec.getContact().getXML();
            if (rec.getContact().getAddress() != null && rec.getContact().getAddress().getCountry() != null) {
                String country = xmlService.getCountryName(rec.getContact().getAddress().getCountry());
                if (isSet(country)) {
                    classifiedContact.getChild("address").addContent(
                            new Element("countryName").setText(country));
                } else {
                    classifiedContact.getChild("address").addContent(new Element("countryName"));
                }
            }
            root.addContent(classifiedContact);
        }
        if (rec.getPersonId() != null) {
            root.addContent(createContactPerson(rec));
        }
        if (isSet(rec.getCustomHeader())) {
            root.addContent(new Element("recordHeader").setText(rec.getCustomHeader()));
        } else {
            if (rec.getBookingType() != null && rec.getBookingType().isUseNameOnOutput()) {
                String cname = rec.getBookingType().getName();
                if (rec.getBookingType().getResourceKey() != null && rec.getBookingType().getResourceKey().length() > 0) {
                    String resolvedName = getResourceString(rec.getBookingType().getResourceKey(), fetchLocale(rec));
                    if (resolvedName != null) {
                        cname = resolvedName;
                    }
                }
                if (cname != null) {
                    root.addContent(new Element("recordHeader").setText(cname));
                } else {
                    root.addContent(new Element("recordHeader"));
                }
            }
        }

        if (rec.isPrintComplimentaryClose()) {
            root.addContent(new Element("printComplimentaryClose").setText("true"));
        } else {
            root.addContent(new Element("printComplimentaryClose").setText("false"));
        }
        if (rec.isPrintProjectmanager()) {
            root.addContent(new Element("printProjectmanager").setText("true"));
        } else {
            root.addContent(new Element("printProjectmanager").setText("false"));
        }
        if (rec.isPrintSalesperson()) {
            root.addContent(new Element("printSalesperson").setText("true"));
        } else {
            root.addContent(new Element("printSalesperson").setText("false"));
        }
        if (rec.isPrintBusinessCaseId()) {
            root.addContent(new Element("printBusinessCaseId").setText("true"));
        } else {
            root.addContent(new Element("printBusinessCaseId").setText("false"));
        }
        if (rec.isPrintBusinessCaseInfo()) {
            root.addContent(new Element("printBusinessCaseInfo").setText("true"));
        } else {
            root.addContent(new Element("printBusinessCaseInfo").setText("false"));
        }
        if (rec.isPrintRecordDate()) {
            root.addContent(new Element("printRecordDate").setText("true"));
        } else {
            root.addContent(new Element("printRecordDate").setText("false"));
        }
        if (rec.isPrintRecordDateByStatus()) {
            root.addContent(new Element("printRecordDateByStatus").setText("true"));
        } else {
            root.addContent(new Element("printRecordDateByStatus").setText("false"));
        }
        if (rec.isPrintPaymentNote()) {
            root.addContent(new Element("printPaymentNote").setText("true"));
        } else {
            root.addContent(new Element("printPaymentNote").setText("false"));
        }
        if (rec.isPrintPaymentTarget()) {
            root.addContent(new Element("printPaymentTarget").setText("true"));
        } else {
            root.addContent(new Element("printPaymentTarget").setText("false"));
        }
        if (rec.isPrintConfirmationPlaceholder()) {
            root.addContent(new Element("printConfirmationPlaceholder").setText("true"));
        } else {
            root.addContent(new Element("printConfirmationPlaceholder").setText("false"));
        }
        if (rec.isPrintCoverLetter()) {
            root.addContent(new Element("printCoverLetter").setText("true"));
        } else {
            root.addContent(new Element("printCoverLetter").setText("false"));
        }
        root.addContent(new Element("currency").setText(rec.getCurrencyKey()));

        if (rec.getNote() == null) {
            root.addContent(new Element("note"));
        } else {
            root.addContent(createNote(rec.getNote()));
        }
        // infos
        if (!rec.getInfos().isEmpty()) {
            Element notes = new Element("notes");
            boolean format = isSystemPropertyEnabled(FORMAT_CONDITIONS);
            for (int i = 0, j = rec.getInfos().size(); i < j; i++) {
                RecordInfo info = rec.getInfos().get(i);
                notes.addContent(format ? createFormattedInfo(info.getInfoId())
                        : createInfo(info.getInfoId()));
            }
            root.addContent(notes);
        }
        // created date and user
        if (rec.getCreated() != null) {
            root.addContent(new Element("created").setText(
                    DateFormatter.getDate(rec.getCreated())));
        } else {
            root.addContent(new Element("created"));
        }
        if (rec.getCreatedBy() != null) {
            root.addContent(xmlService.createEmployee(
                    "createdBy", rec.getCreatedBy()));
        } else {
            root.addContent(new Element("createdBy"));
        }

        if (preview && rec.getStatusDate() != null) {
            root.addContent(new Element("statusDate").setText(
                    DateFormatter.getDate(rec.getStatusDate())));
        } else {
            root.addContent(new Element("statusDate").setText(
                    DateFormatter.getCurrentDate()));
        }

        if (rec.getReference() == null) {
            root.addContent(new Element("referenceId"));
        } else {
            root.addContent(new Element("referenceId").setText(rec.getReference().toString()));
        }
        
        root.addContent(createAmounts(rec));
        root.addContent(createItems(rec));
        root.addContent(createOptions(rec));

        if (rec.getSignatureLeft() != null && rec.getSignatureLeft() > 0) {
            if (log.isDebugEnabled()) {
                log.debug("createRecord() trying to add left signature for employee "
                        + rec.getSignatureLeft());
            }
            root.addContent(xmlService.createEmployee("signatureLeft", rec.getSignatureLeft()));
        }
        if (rec.getSignatureRight() != null && rec.getSignatureRight() > 0) {
            if (log.isDebugEnabled()) {
                log.debug("createRecord() trying to add right signature for employee "
                        + rec.getSignatureRight());
            }
            root.addContent(xmlService.createEmployee("signatureRight", rec.getSignatureRight()));
        }
        if (rec instanceof Offer) {
            addOfferValues(root, (Offer) rec);

        } else if (rec instanceof Order) {
            addOrderValues(root, (Order) rec);

        } else if (rec instanceof Invoice) {
            addInvoiceValues(root, (Invoice) rec);

        } else if (rec instanceof CreditNote) {
            addCreditNoteValues(root, (CreditNote) rec);

        } else if (rec instanceof Cancellation) {
            addCancellationValues(root, (Cancellation) rec);
        } else if (rec instanceof DeliveryNote) {
            addDeliveryNoteValues(root, (DeliveryNote) rec);
        }
        return root;
    }

    private void addDeliveryNoteValues(Element root, DeliveryNote deliveryNote) {
        if (deliveryNote.getDelivery() != null) {
            root.getChild("created").setText(DateFormatter.getDate(deliveryNote.getDelivery()));
        }
    }

    private void addOfferValues(Element root, Offer offer) {
        Date delivery = offer.getDelivery();
        if (delivery == null) {
            root.addContent(new Element("deliveryDate"));
            root.addContent(new Element("deliveryWeek"));
        } else {
            root.addContent(new Element("deliveryDate").setText(
                    DateFormatter.getDate(delivery)));
            root.addContent(new Element("deliveryDateWeek").setText(
                    DateFormatter.getWeekOfYear(delivery)));
        }
        Date validUntil = offer.getValidUntil();
        if (validUntil != null) {
            root.addContent(new Element("validUntil").setText(
                    DateFormatter.getDate(validUntil)));
        }
    }

    private void addOrderValues(Element root, Order order) {
        Date delivery = order.getDelivery();
        if (delivery == null) {
            root.addContent(new Element("deliveryDate"));
            root.addContent(new Element("deliveryWeek"));
        } else {
            root.addContent(new Element("deliveryDate").setText(
                    DateFormatter.getDate(delivery)));
            root.addContent(new Element("deliveryDateWeek").setText(
                    DateFormatter.getWeekOfYear(delivery)));

            Map<String, RecordPrintOption> popts = recordPrintOptionDefaults.getDefaults(order.getType().getId());
            RecordPrintOption opt = popts.get("printDeliveryDate");
            if (opt != null && opt.isEnabled()) {
                root.addContent(new Element("printDeliveryDate").setText("true"));
            } else {
                root.addContent(new Element("printDeliveryDate").setText("false"));
            }
        }

        if (order instanceof PurchaseOrder) {
            PurchaseOrder po = (PurchaseOrder) order;
            root.addContent(xmlService.createAddress(
                    "address",
                    po.getDeliveryAddress()));
            if (isSet(po.getSupplierReferenceNumber())) {
                root.addContent(new Element("supplierReferenceNumber").setText(
                        po.getSupplierReferenceNumber()));
            } else {
                root.addContent(new Element("supplierReferenceNumber"));
            }

            if (po.getDeliveryCondition() != null) {
                DeliveryCondition dc = po.getDeliveryCondition();
                if (isSet(dc.getName())) {
                    Element deliveryCondition = new Element("deliveryCondition");
                    deliveryCondition.addContent(new Element("id").setText(dc.getId().toString()));
                    deliveryCondition.addContent(new Element("name").setText(dc.getName()));
                    deliveryCondition.addContent(new Element("description").setText(
                            (dc.getDescription() != null) ? dc.getDescription() : ""
                            ));
                    root.addContent(deliveryCondition);
                } else {
                    root.addContent(new Element("deliveryCondition"));
                }
            }
            if (po.getBusinessCaseId() != null) {
                try {
                    Sales sales = projects.load(po.getBusinessCaseId());
                    Element referencedSales = new Element("referencedSales");
                    referencedSales.addContent(new Element("id").setText(po.getBusinessCaseId().toString()));
                    if (sales.getName() != null) {
                        referencedSales.addContent(new Element("name").setText(sales.getName()));
                    }
                    root.addContent(referencedSales);
                } catch (Exception c) {
                    log.warn("addOrderValues() failed to add referencedSales [message=" + c.getMessage() + "]", c);
                    root.addContent(new Element("referencedSales"));
                }
            } else {
                root.addContent(new Element("referencedSales"));
            }
        }
        Integer version = order.getVersion();
        if (version == null || version == 0) {
            root.addContent(new Element("version").setText("0"));
        } else {
            if (order.getChanged() != null) {
                root.getChild("created").setText(
                        DateFormatter.getDate(order.getChanged()));
                root.addContent(new Element("originallyCreated").setText(
                        DateFormatter.getDate(order.getCreated())));

            }
            root.addContent(new Element("version").setText(version.toString()));
        }
        if (order instanceof SalesOrder) {
            SalesOrder salesOrder = (SalesOrder) order;
            List<Item> openDeliveries = salesOrder.getOpenDeliveries();
            if (!openDeliveries.isEmpty()) {
                root.addContent(createItems(
                        salesOrder,
                        "openDeliveries",
                        openDeliveries));
            }
        }
    }

    private void addInvoiceValues(Element root, Invoice inv) {
        root.addContent(createDeliveryNotes(inv));
        if (inv.getTaxPoint() == null) {
            root.addContent(new Element("taxPoint"));
        } else {
            root.addContent(new Element("taxPoint").setText(
                    DateFormatter.getDate(inv.getTaxPoint())));
        }
        if (inv.getTimeOfSupply() == null) {
            root.addContent(new Element("timeOfSupply"));
        } else {
            root.addContent(new Element("timeOfSupply").setText(inv.getTimeOfSupply()));
        }
        if (inv.getPlaceOfPerformance() == null) {
            root.addContent(new Element("placeOfPerformance"));
        } else {
            root.addContent(new Element("placeOfPerformance").setText(inv.getPlaceOfPerformance()));
        }
        if (inv.isPrintTaxPoint()) {
            root.addContent(new Element("printTaxPoint").setText("true"));
        } else {
            root.addContent(new Element("printTaxPoint").setText("false"));
        }
        if (inv.isPrintTaxPointByItems()) {
            root.addContent(new Element("printTaxPointByItems").setText("true"));
        } else {
            root.addContent(new Element("printTaxPointByItems").setText("false"));
        }
        if (inv.isPrintTimeOfSupply()) {
            root.addContent(new Element("printTimeOfSupply").setText("true"));
        } else {
            root.addContent(new Element("printTimeOfSupply").setText("false"));
        }
        if (inv.isPrintPlaceOfPerformance()) {
            root.addContent(new Element("printPlaceOfPerformance").setText("true"));
        } else {
            root.addContent(new Element("printPlaceOfPerformance").setText("false"));
        }
        
        if (inv.getMaturity() == null) {
            root.addContent(new Element("maturity"));
        } else {
            root.addContent(new Element("maturity").setText(
                    DateFormatter.getDate(inv.getMaturity())));
        }
        root.addContent(createPaymentCondition(inv.getCreated(), inv.getPaymentCondition()));

        if (inv.getPaymentNote() == null) {
            root.addContent(new Element("paymentNote"));
        } else {
            root.addContent(new Element("paymentNote").setText(inv.getPaymentNote()));
        }
        if (inv.getReference() != null) {
            root.getChild("referenceId").setText(getOrderId(inv.getReference()));
        }
        root.addContent(new Element("printOrderItems").setText("false"));
        
        BranchOffice branch = isNotSet(inv.getBranchId()) ? null 
                : (BranchOffice) getOption(Options.BRANCH_OFFICES, inv.getBranchId());
        if (branch != null) {
            if (log.isDebugEnabled()) {
                log.debug("addInvoiceValues() branch found [id=" + branch.getId()
                    + ", vatId=" + branch.getVatId() + "]");
            }
            if (isSet(branch.getVatId())) {
                root.addContent(new Element("vatId").setText(branch.getVatId()));
            } else {
                root.addContent(new Element("vatId"));
            }
            if (isSet(branch.getVatLabel())) {
                root.addContent(new Element("vatLabel").setText(branch.getVatLabel()));
            } else {
                root.addContent(new Element("vatLabel"));
            }
        } else {
            root.addContent(new Element("vatId"));
            root.addContent(new Element("vatLabel"));
            
            log.warn("addInvoiceValues() record.branchId not exists [record=" + inv.getId()
                    + ", type=" + inv.getType().getId() 
                    + ", branchId=" + inv.getBranchId() + "]");
        }
        if (inv instanceof SalesInvoice) {

            boolean printDownpaymentTax = isSystemPropertyEnabled("documentPrintDownpaymentPayments");
            if (printDownpaymentTax) {
                root.addContent(new Element("printDownpaymentPayments").setText("true"));
            } else {
                root.addContent(new Element("printDownpaymentPayments"));
            }

            SalesInvoice salesInvoice = (SalesInvoice) inv;
            // TODO add downpayments
            if (isSet(salesInvoice.getReference())) {
                SalesOrder salesOrder = (SalesOrder) salesOrders.load(salesInvoice.getReference());
                if (salesOrder.getPaymentAgreement() != null && isSet(salesOrder.getPaymentAgreement().getNote())) {
                    // TODO print terms if printOrderTerms enabled
                }
                if (salesInvoice.isPrintOrderItems()) {
                    try {
                        root.getChild("printOrderItems").setText("true");
                        root.addContent(createItems(salesOrder, "orderItems", salesOrder.getItems()));
                        if (salesOrder.getAmounts() != null
                                && salesOrder.getAmounts().getAmount() != null) {
                            root.addContent(new Element("orderAmount").setText(
                                    salesOrder.getAmounts().getAmount().toString()));
                        } else {
                            root.addContent(new Element("orderAmount"));
                        }

                    } catch (ActionException ignorable) {
                    }
                }
            }
            //important: if there are no partial invoices, the element <partialInvoices/> must exist and be empty
            Element partialInvoiceElements = new Element("partialInvoices");
            if (!salesInvoice.isPartial()) {
                List<SalesInvoice> partialInvoices = salesInvoices.getPartialByInvoice(salesInvoice);
                for (int i = 0, j = partialInvoices.size(); i < j; i++) {
                    SalesInvoice pi = partialInvoices.get(i);
                    if (!pi.isCanceled()) {
                        Element partialInvoice = createRecord("partialInvoice", pi, false);
                        partialInvoice.addContent(createPayments(pi));
                        partialInvoiceElements.addContent(partialInvoice);
                    }
                }

                Amounts am = salesInvoice.getAmounts();
                BigDecimal totalAmount = am.getAmount().add(salesInvoice.getPartialInvoiceAmount());
                BigDecimal totalGrossAmount = am.getGrossAmount().add(salesInvoice.getPartialInvoiceAmountGross());
                BigDecimal totalTaxAmount = totalGrossAmount.subtract(totalAmount);

                Element amounts = root.getChild("amounts");
                amounts.addContent(new Element("totalAmount").setText(NumberFormatter.getValue(totalAmount, NumberFormatter.CURRENCY)));
                amounts.addContent(new Element("totalGrossAmount").setText(NumberFormatter.getValue(totalGrossAmount, NumberFormatter.CURRENCY)));
                amounts.addContent(new Element("totalTaxAmount").setText(NumberFormatter.getValue(totalTaxAmount, NumberFormatter.CURRENCY)));
            }
            root.addContent(partialInvoiceElements);
            root.addContent(new Element("partialInvoiceAmount").setText(
                    NumberFormatter.getValue(salesInvoice.getPartialInvoiceAmount(), NumberFormatter.CURRENCY
                            )));
            root.addContent(new Element("partialInvoiceAmountGross").setText(
                    NumberFormatter.getValue(salesInvoice.getPartialInvoiceAmountGross(), NumberFormatter.CURRENCY
                            )));
            if (isSet(salesInvoice.getDeEst35a())) {
                root.addContent(new Element("deEst35a").setText(salesInvoice.getDeEst35a()));
            } else {
                root.addContent(new Element("deEst35a"));
            }
            if (isSet(salesInvoice.getDeEst35aTax())) {
                root.addContent(new Element("deEst35aTax").setText(salesInvoice.getDeEst35aTax()));
            } else {
                root.addContent(new Element("deEst35aTax"));
            }
        } else if (inv instanceof Downpayment) {
            Downpayment dp = (Downpayment) inv;
            if (!dp.isCanceled() || dp.getDateCanceled() == null) {
                root.addContent(new Element("dateCanceled"));
            } else {
                root.addContent(new Element("dateCanceled").setText(
                        DateFormatter.getDate(dp.getDateCanceled())));
            }
            if (isSet(dp.getIdCanceled())) {
                root.addContent(new Element("idCanceled").setText(dp.getIdCanceled().toString()));
            } else {
                root.addContent(new Element("idCanceled"));
            }
        }
    }

    private void addCreditNoteValues(Element root, CreditNote note) {
        try {
            Date created = salesInvoices.getCreated(note.getReference());
            if (created == null) {
                root.addContent(new Element("referenceDate"));

            } else {
                root.addContent(new Element("referenceDate").setText(
                        DateFormatter.getDate(created)));

            }
        } catch (Throwable t) {
            log.warn("createRecord() ignoring failure on credit note "
                    + note.getId() + " of invoice " + note.getReference());
        }
    }

    private void addCancellationValues(Element root, Cancellation rec) {
        Element dpa = new Element("downpayment");
        try {
            Date created = null;
            if (RecordType.SALES_CREDIT_NOTE.equals(rec.getCancelledType())) {
                created = salesCreditNotes.getCreated(rec.getReference());
            } else if (RecordType.SALES_DOWNPAYMENT.equals(rec.getCancelledType())) {
                Downpayment dp = (Downpayment) downpayments.load(rec.getReference());
                created = dp.getCreated();
                dpa.addContent(createAmounts(dp));
            } else {
                created = salesInvoices.getCreated(rec.getReference());
            }
            if (created == null) {
                root.addContent(new Element("referenceDate"));
            } else {
                root.addContent(new Element("referenceDate").setText(
                        DateFormatter.getDate(created)));
            }
        } catch (Throwable t) {
            log.warn("addCancellationValues() ignoring failure on cancellation "
                    + rec.getId() + " of invoice " + rec.getReference());
        }
        if (rec.getCancelledType() != null) {
            root.addContent(new Element("canceledType").setText(rec.getCancelledType().toString()));
        } else {
            root.addContent(new Element("canceledType"));
        }
        root.addContent(dpa);
    }

    private String getOrderId(Long orderId) {
        String result = orderId == null ? "" : orderId.toString(); //Records.createNumber(orderId);
        try {
            SalesOrder order = (SalesOrder) salesOrders.load(orderId);
            if (order.getVersion() != null && order.getVersion() > 0) {
                StringBuilder b = new StringBuilder();
                b.append(result).append("-").append(order.getVersion());
                result = b.toString();
            }
        } catch (Throwable t) {
            // we ignroe this
        }
        return result;
    }

    private Element createDeliveryNotes(Invoice invoice) {
        Element root = new Element("references");
        List<DeliveryNote> deliveries = getDeliveryNotes(invoice);
        for (int i = 0, j = deliveries.size(); i < j; i++) {
            DeliveryNote deliveryNote = deliveries.get(i);
            Element reference = new Element("reference");
            reference.addContent(new Element("id").setText(Records.createNumber(
                    deliveryNote.getType(), deliveryNote.getId(), deliveryNote.isInternal())));
            reference.addContent(new Element("date").setText(DateFormatter.getDate(
                    deliveryNote.getDelivery() != null ? deliveryNote.getDelivery()
                            : deliveryNote.getCreated())));
            root.addContent(reference);
        }
        return root;
    }

    private List<DeliveryNote> getDeliveryNotes(Invoice invoice) {
        List<DeliveryNote> result = new ArrayList<>();
        if (invoice instanceof SalesInvoice && invoice.getReference() != null) {
            try {
                Order order = (Order) salesOrders.load(invoice.getReference());
                for (int i = 0, j = order.getDeliveryNotes().size(); i < j; i++) {
                    DeliveryNote delivery = order.getDeliveryNotes().get(i);
                    if (invoice.containsProductsOf(delivery)) {
                        result.add(delivery);
                    }
                }
            } catch (Throwable t) {
                // ignore
                log.warn("getDeliveryNotes() ignoring exception [message=" + t.toString() + "]", t);
            }
        }
        return result;
    }

    private Element createAmounts(Record rec) {
        Element amounts = new Element("amounts");
        if (rec instanceof DeliveryNote) {
            return amounts;
        }
        Amounts avalues = rec.getAmounts();
        if (avalues.getAmount() == null) {
            addEmptyAmounts(rec, amounts);
        } else {
            addAmounts(rec, amounts, avalues);
        }
        // prorate on advance invoice
        if (rec instanceof Downpayment) {
            if (log.isDebugEnabled()) {
                log.debug("createAmounts() record is downpayment, adding prorated...");
            }
            addProrateAmounts(amounts, (Downpayment) rec);

        }
        if (rec instanceof Invoice) {
            if (log.isDebugEnabled()) {
                log.debug("createAmounts() record is invoice, adding payments...");
            }
            addPayments(amounts, (Invoice) rec);
        }
        return amounts;
    }

    private void addProrateAmounts(Element amounts, Downpayment ai) {
        Element prorate = new Element("prorate");
        if (ai.getProratePercent() == null) {
            prorate.addContent(new Element("percent"));
        } else {
            prorate.addContent(new Element("percent").setText(
                    xmlService.createString(ai.getProratePercent())));
        }
        prorate.addContent(new Element("amount").setText(
                NumberFormatter.getValue(ai.getAmount(), NumberFormatter.CURRENCY)));

        Element salesTax = new Element("salesTax");
        salesTax.addContent(new Element("taxfree").setText(xmlService.createString(ai.isTaxFree())));

        if (ai.isTaxFree()) {
            prorate.addContent(new Element("grossAmount").setText(
                    NumberFormatter.getValue(ai.getAmount(), NumberFormatter.CURRENCY)));
            salesTax.addContent(getTaxFreeDefinition("reason", ai.getTaxFreeId()));
            salesTax.addContent(new Element("reducedTax").setText(nullAmount));
            salesTax.addContent(new Element("tax").setText(nullAmount));
        } else {
            salesTax.addContent(new Element("reason"));
            boolean reducedTaxAvailable = ai.isReducedTaxAvailable();
            prorate.addContent(new Element("reducedTaxAvailable").setText(
                    xmlService.createString(reducedTaxAvailable)));
            prorate.addContent(new Element("grossAmount").setText(
                    NumberFormatter.getValue(ai.getGrossAmount(), NumberFormatter.CURRENCY)));
            Element reducedTax = new Element("reducedTax");
            if (reducedTaxAvailable) {
                Amounts summary = ai.getSummary();
                BigDecimal percent = NumberUtil.getPercent(
                        ai.getAmounts().getAmount(), ai.getAmount(), NumberUtil.DIGITS_PERCENT);
                if (isSet(summary.getAmountWithZeroTax())) {
                    if (isNotSet(summary.getAmountWithBaseTax())
                            && isNotSet(summary.getAmountWithReducedTax())) {
                        // zero tax only
                        prorate.addContent(new Element("amountWithBaseTax"));
                        prorate.addContent(new Element("amountWithReducedTax"));
                        prorate.addContent(new Element("amountWithZeroTax").setText(
                                NumberFormatter.getValue(ai.getAmount(),
                                        NumberFormatter.CURRENCY)));
                    } else {
                        // zero tax and other tax rates
                        prorate.addContent(new Element("amountWithBaseTax").setText(
                                NumberFormatter.getValue(summary.getAmountWithBaseTax().multiply(percent),
                                        NumberFormatter.CURRENCY)));
                        prorate.addContent(new Element("amountWithReducedTax").setText(
                                NumberFormatter.getValue(summary.getAmountWithReducedTax().multiply(percent),
                                        NumberFormatter.CURRENCY)));
                        prorate.addContent(new Element("amountWithZeroTax").setText(
                                NumberFormatter.getValue(summary.getAmountWithZeroTax().multiply(percent),
                                        NumberFormatter.CURRENCY)));
                    }
                } else {
                    prorate.addContent(new Element("amountWithZeroTax"));
                    prorate.addContent(new Element("amountWithBaseTax").setText(
                            NumberFormatter.getValue(summary.getAmountWithBaseTax().multiply(percent),
                                    NumberFormatter.CURRENCY)));
                    prorate.addContent(new Element("amountWithReducedTax").setText(
                            NumberFormatter.getValue(summary.getAmountWithReducedTax().multiply(percent),
                                    NumberFormatter.CURRENCY)));
                }
                if (ai.getReducedTaxAmount() != null
                        && ai.getReducedTaxAmount().doubleValue() > 0) {
                    reducedTax.addContent(new Element("percent").setText(
                            xmlService.createString(ai.getAmounts().getReducedTaxRate())));
                    reducedTax.addContent(new Element("amount").setText(
                            NumberFormatter.getValue(ai.getReducedTaxAmount(), NumberFormatter.CURRENCY)));
                    reducedTax.addContent(getTaxElement("formatted", ai.getAmounts().getReducedTaxRate()));
                }
            } else {
                prorate.addContent(new Element("amountWithReducedTax"));
                prorate.addContent(new Element("amountWithZeroTax"));
                prorate.addContent(new Element("amountWithBaseTax").setText(
                        NumberFormatter.getValue(ai.getTaxAmount(), NumberFormatter.CURRENCY)));
            }
            salesTax.addContent(reducedTax);
            // base tax
            Element tax = new Element("tax");
            if (ai.getTaxAmount() != null
                    && ai.getTaxAmount().doubleValue() > 0) {
                tax.addContent(new Element("percent").setText(
                        xmlService.createString(ai.getAmounts().getTaxRate())));
                tax.addContent(new Element("amount").setText(
                        NumberFormatter.getValue(ai.getTaxAmount(), NumberFormatter.CURRENCY)));
                tax.addContent(getTaxElement("formatted", ai.getAmounts().getTaxRate()));
            }
            salesTax.addContent(tax);
        }
        prorate.addContent(salesTax);
        amounts.addContent(prorate);
    }

    private void addPayments(Element amounts, Invoice inv) {
        BigDecimal paidAmount = inv.getPaidAmount();
        BigDecimal paidNetAmount = new BigDecimal(0);
        BigDecimal paidTaxAmount = new BigDecimal(0);
        BigDecimal dueAmount = inv.getDueAmount();
        if (paidAmount == null || paidAmount.doubleValue() == 0) {
            if (log.isDebugEnabled()) {
                log.debug("addPayments() paid amount is 0");
            }
            amounts.addContent(new Element("paidAmount"));
        } else {
            if (log.isDebugEnabled()) {
                log.debug("addPayments() paid amount is " + paidAmount);
            }
            if (inv.isReducedTaxAvailable()) {
                BigDecimal percent = NumberUtil.getPercent(
                        inv.getAmounts().getGrossAmount(), paidAmount, NumberUtil.DIGITS_PERCENT);
                BigDecimal prorateTaxBase = inv.getAmounts().getTaxAmount().multiply(percent);
                BigDecimal prorateTaxReduced = inv.getAmounts().getReducedTaxAmount().multiply(percent);
                paidTaxAmount = prorateTaxBase.add(prorateTaxReduced);
                paidNetAmount = paidAmount.subtract(paidTaxAmount);
                amounts.addContent(new Element("paidBaseTax").setText(NumberFormatter.getValue(prorateTaxBase, NumberFormatter.CURRENCY)));
                amounts.addContent(new Element("paidReducedTax").setText(NumberFormatter.getValue(prorateTaxReduced, NumberFormatter.CURRENCY)));
            } else {
                amounts.addContent(new Element("paidBaseTax"));
                amounts.addContent(new Element("paidReducedTax"));
                if (inv.isOverridePaidAmount()) {
                    paidAmount = new BigDecimal(inv.getOverrideGrossPaidAmount());
                    paidNetAmount = new BigDecimal(inv.getOverrideNetPaidAmount());
                    paidTaxAmount = new BigDecimal(inv.getOverrideTaxPaidAmount());
                } else {
                    paidNetAmount = inv.getPaidNetAmount();
                    paidTaxAmount = inv.getPaidTax();
                }
            }
            amounts.addContent(new Element("paidAmount").setText(NumberFormatter.getValue(paidAmount, NumberFormatter.CURRENCY)));
            amounts.addContent(new Element("paidNetAmount").setText(NumberFormatter.getValue(paidNetAmount, NumberFormatter.CURRENCY)));
            amounts.addContent(new Element("paidTaxAmount").setText(NumberFormatter.getValue(paidTaxAmount, NumberFormatter.CURRENCY)));
        }
        String payAmount = fixZeroAndMixedTaxAmountToPay(NumberFormatter.getValue(dueAmount, NumberFormatter.CURRENCY));
        amounts.addContent(new Element("payAmount").setText(payAmount));

        if (log.isDebugEnabled()) {
            log.debug("addPayments() amount to pay is " + payAmount);
        }
        if (inv instanceof SalesInvoice) {

            BigDecimal invoiceTaxAmount = inv.getAmounts().getTaxAmount();
            BigDecimal taxToPay = invoiceTaxAmount.subtract(paidTaxAmount);
            double tax =
                    (inv.getAmounts() == null
                    || inv.getAmounts().getTaxRate() == null)
                            ? 0 : inv.getAmounts().getTaxRate();
            if (log.isDebugEnabled()) {
                log.debug("addPayments() done [invoice=" + inv.getId() +
                        ", taxToPay=" + taxToPay.toPlainString() +
                        ", taxRate=" + NumberFormatter.getValue(tax, NumberFormatter.CURRENCY) + "]");
            }
            if (!inv.isTaxFree()) {
                amounts.addContent(new Element("payTaxRate").setText(xmlService.createString(tax)));
                amounts.addContent(getTaxElement("payTaxRateFormatted", tax));
                if (inv.getAmounts().getAmountWithReducedTax() != null
                        && inv.getAmounts().getAmountWithReducedTax().doubleValue() != 0) {
                    amounts.addContent(getTaxElement("payReducedTaxRateFormatted",
                            inv.getAmounts().getReducedTaxRate()));
                } else {
                    amounts.addContent(new Element("payReducedTaxRateFormatted"));
                }
                amounts.addContent(new Element("payTaxAmount").setText(
                        NumberFormatter.getValue(taxToPay, NumberFormatter.CURRENCY)));
            } else {
                amounts.addContent(new Element("payTaxRate").setText(nullAmount));
                amounts.addContent(new Element("payTaxRateFormatted").setText(nullAmount));
                amounts.addContent(new Element("payTaxAmount").setText(nullAmount));
            }
            addDownpayments(amounts, (SalesInvoice) inv);
        }
    }

    private Element createPayment(
            Date created,
            double taxRate,
            double reducedTaxRate,
            BigDecimal net,
            BigDecimal netWithBaseTax,
            BigDecimal netWithReducedTax,
            BigDecimal netWithZeroTax,
            BigDecimal taxBase,
            BigDecimal taxReduced,
            BigDecimal gross) {

        Element payment = new Element("payment");
        payment.addContent(new Element("created").setText(DateFormatter.getDate(created)));

        if (taxRate > 0) {
            payment.addContent(new Element("tax").setText(xmlService.createString(taxRate)));
            payment.addContent(getTaxElement("taxFormatted", taxRate));
        } else {
            payment.addContent(new Element("tax"));
            payment.addContent(new Element("taxFormatted"));
        }

        if (reducedTaxRate > 0) {
            payment.addContent(new Element("reducedTax").setText(xmlService.createString(reducedTaxRate)));
            payment.addContent(getTaxElement("reducedTaxFormatted", reducedTaxRate));
        } else {
            payment.addContent(new Element("reducedTax"));
            payment.addContent(new Element("reducedTaxFormatted"));
        }

        payment.addContent(new Element("netAmount").setText(
                NumberFormatter.getValue(net, NumberFormatter.CURRENCY)));

        if (NumberUtil.isZero(netWithBaseTax)) {
            payment.addContent(new Element("netAmountWithBaseTax"));
        } else {
            payment.addContent(new Element("netAmountWithBaseTax").setText(
                    NumberFormatter.getValue(netWithBaseTax, NumberFormatter.CURRENCY)));
        }

        if (NumberUtil.isZero(netWithReducedTax)) {
            payment.addContent(new Element("netAmountWithReducedTax"));
        } else {
            payment.addContent(new Element("netAmountWithReducedTax").setText(
                    NumberFormatter.getValue(netWithReducedTax, NumberFormatter.CURRENCY)));
        }

        if (NumberUtil.isZero(netWithZeroTax)) {
            payment.addContent(new Element("netAmountWithZeroTax"));
        } else {
            payment.addContent(new Element("netAmountWithZeroTax").setText(
                    NumberFormatter.getValue(netWithZeroTax, NumberFormatter.CURRENCY)));
        }

        BigDecimal taxAmount = new BigDecimal(0);
        if (taxBase != null) {
            taxAmount = taxBase;
            payment.addContent(new Element("taxAmountWithBaseTax").setText(
                    NumberFormatter.getValue(taxBase, NumberFormatter.CURRENCY)));
        } else {
            payment.addContent(new Element("taxAmountWithBaseTax"));
        }
        if (taxReduced != null) {
            taxAmount = taxAmount.add(taxReduced);
            payment.addContent(new Element("taxAmountWithReducedTax").setText(
                    NumberFormatter.getValue(taxReduced, NumberFormatter.CURRENCY)));
        } else {
            payment.addContent(new Element("taxAmountWithReducedTax"));
        }
        if (taxAmount != null && taxAmount.doubleValue() > 0) {
            payment.addContent(new Element("taxAmount").setText(
                    NumberFormatter.getValue(taxAmount, NumberFormatter.CURRENCY)));
        } else {
            payment.addContent(new Element("taxAmount"));
        }

        payment.addContent(new Element("grossAmount").setText(
                NumberFormatter.getValue(gross, NumberFormatter.CURRENCY)));
        return payment;
    }

    private void addDownpayments(Element amounts, SalesInvoice invoice) {
        Element downpaymentElement = new Element("downpayments");
        if (invoice.getReference() != null) {
            List<? extends FinanceRecord> dplist = downpayments.getByReference(invoice.getReference());
            if (!dplist.isEmpty()) {
                BigDecimal downpaymentsNet = new BigDecimal(0);
                BigDecimal downpaymentsTax = new BigDecimal(0);
                BigDecimal downpaymentsBaseTax = new BigDecimal(0);
                BigDecimal downpaymentsReducedTax = new BigDecimal(0);
                BigDecimal downpaymentsGross = new BigDecimal(0);
                BigDecimal paidNet = new BigDecimal(0);
                BigDecimal paidTax = new BigDecimal(0);
                BigDecimal paidGross = new BigDecimal(0);
                BigDecimal payAmount = new BigDecimal(0);
                int added = 0;
                for (int i = 0, j = dplist.size(); i < j; i++) {
                    Downpayment dpobj = (Downpayment) dplist.get(i);
                    if (!dpobj.isCanceled() && !dpobj.getPayments().isEmpty()) {
                        downpaymentsNet = downpaymentsNet.add(dpobj.getAmount());
                        if (dpobj.getReducedTaxAmount() != null) {
                            downpaymentsReducedTax = downpaymentsReducedTax.add(dpobj.getReducedTaxAmount());
                            downpaymentsTax.add(dpobj.getReducedTaxAmount());
                        }
                        if (dpobj.getTaxAmount() != null) {
                            downpaymentsBaseTax = downpaymentsTax.add(dpobj.getTaxAmount());
                            downpaymentsTax = downpaymentsTax.add(dpobj.getTaxAmount());
                        }
                        downpaymentsGross = downpaymentsGross.add(dpobj.getGrossAmount());
                        double taxrate = dpobj.getAmounts().getTaxRate();
                        double reducedtaxrate = dpobj.getAmounts().getReducedTaxRate();
                        Element dpinv = new Element("invoice");
                        dpinv.addContent(new Element("id").setText(dpobj.getId().toString()));
                        if (isSet(dpobj.getIdCanceled())) {
                            dpinv.addContent(new Element("idCanceled").setText(dpobj.getIdCanceled().toString()));
                        } else {
                            dpinv.addContent(new Element("idCanceled"));
                        }
                        dpinv.addContent(new Element("created").setText(
                                DateFormatter.getDate(dpobj.getCreated())));
                        if (log.isDebugEnabled()) {
                            log.debug("addDownpayments() added invoice " + dpobj.getId());
                        }
                        dpinv.addContent(createAmounts(dpobj));
                        Element payments = new Element("payments");
                        for (int k = 0, l = dpobj.getPayments().size(); k < l; k++) {
                            Payment pm = dpobj.getPayments().get(k);
                            Element item = new Element("payment");
                            item.addContent(new Element("created").setText(
                                    DateFormatter.getDate(pm.getPaid())));

                            BigDecimal netAmount = new BigDecimal(0);
                            BigDecimal amountWithBaseTax = null;
                            BigDecimal amountWithReducedTax = null;
                            BigDecimal amountWithZeroTax = null;
                            BigDecimal taxAmount = new BigDecimal(0);

                            BigDecimal percent = NumberUtil.getPercent(
                                    dpobj.getAmounts().getGrossAmount(),
                                    pm.getAmount(), NumberUtil.DIGITS_PERCENT);

                            if (dpobj.isTaxFree()) {
                                netAmount = pm.getAmount();
                                item = createPayment(
                                        pm.getPaid(),
                                        0,
                                        0,
                                        netAmount,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        netAmount);
                                paidNet = paidNet.add(netAmount);
                                paidGross = paidGross.add(netAmount);

                            } else if (!dpobj.isReducedTaxAvailable()) {
                                if (NumberUtil.round(dpobj.getGrossAmount(), 2).doubleValue()
                                        - pm.getAmount().doubleValue() == 0) {
                                    netAmount = dpobj.getAmount();
                                    amountWithBaseTax = dpobj.getAmount();
                                    taxAmount = dpobj.getTaxAmount();
                                    item = createPayment(
                                            pm.getPaid(),
                                            taxrate,
                                            reducedtaxrate,
                                            netAmount,
                                            amountWithBaseTax,
                                            null,
                                            null,
                                            taxAmount,
                                            null,
                                            dpobj.getGrossAmount());
                                } else {
                                    netAmount = new BigDecimal(pm.getAmount().doubleValue() / taxrate);
                                    amountWithBaseTax = netAmount;
                                    taxAmount = pm.getAmount().subtract(netAmount);
                                    item = createPayment(
                                            pm.getPaid(),
                                            taxrate,
                                            reducedtaxrate,
                                            netAmount,
                                            amountWithBaseTax,
                                            null,
                                            null,
                                            taxAmount,
                                            null,
                                            pm.getAmount());
                                }
                            } else {
                                Amounts sum = dpobj.getSummary();
                                if (isSet(sum.getAmountWithZeroTax())
                                        && isNotSet(sum.getAmountWithBaseTax())
                                        && isNotSet(sum.getAmountWithReducedTax())) {
                                    netAmount = pm.getAmount();
                                    amountWithZeroTax = netAmount;
                                    item = createPayment(
                                            pm.getPaid(),
                                            0d,
                                            0d,
                                            netAmount,
                                            null,
                                            null,
                                            amountWithZeroTax,
                                            null,
                                            null,
                                            netAmount);

                                } else if (NumberUtil.round(dpobj.getDueAmount(), 2).doubleValue() == 0
                                        && pm.getAmount().doubleValue() >= dpobj.getAmount().doubleValue()) {
                                    netAmount = dpobj.getAmount();
                                    amountWithBaseTax = sum.getAmountWithBaseTax().multiply(percent);
                                    amountWithReducedTax = sum.getAmountWithReducedTax().multiply(percent);
                                    amountWithZeroTax = sum.getAmountWithZeroTax().multiply(percent);
                                    item = createPayment(
                                            pm.getPaid(),
                                            taxrate,
                                            reducedtaxrate,
                                            netAmount,
                                            amountWithBaseTax,
                                            amountWithReducedTax,
                                            amountWithZeroTax,
                                            dpobj.getTaxAmount(),
                                            dpobj.getReducedTaxAmount(),
                                            dpobj.getGrossAmount());
                                } else {
                                    amountWithBaseTax = sum.getAmountWithBaseTax().multiply(percent);
                                    amountWithReducedTax = sum.getAmountWithReducedTax().multiply(percent);
                                    amountWithZeroTax = sum.getAmountWithZeroTax().multiply(percent);
                                    BigDecimal baseTaxAmount = sum.getTaxAmount().multiply(percent);
                                    BigDecimal reducedTaxAmount = sum.getReducedTaxAmount().multiply(percent);
                                    taxAmount = baseTaxAmount.add(reducedTaxAmount);
                                    netAmount = pm.getAmount().subtract(taxAmount);
                                    item = createPayment(
                                            pm.getPaid(),
                                            taxrate,
                                            reducedtaxrate,
                                            netAmount,
                                            amountWithBaseTax,
                                            amountWithReducedTax,
                                            amountWithZeroTax,
                                            baseTaxAmount,
                                            reducedTaxAmount,
                                            pm.getAmount());
                                }
                            }
                            payments.addContent(item);
                            paidNet = paidNet.add(netAmount);
                            paidTax = paidTax.add(taxAmount);
                            paidGross = paidGross.add(pm.getAmount());
                        }
                        dpinv.addContent(payments);
                        downpaymentElement.addContent(dpinv);
                        payAmount = payAmount.add(dpobj.getDueAmount());
                        added = added + 1;
                    }
                }
                if (added > 0) {
                    Element summary = new Element("summary");
                    //summary.addContent(new Element("tax").setText(xmlService.createString(taxrate)));
                    summary.addContent(new Element("paidNetAmount").setText(
                            NumberFormatter.getValue(NumberUtil.round(paidNet, 2), NumberFormatter.CURRENCY)));
                    summary.addContent(new Element("paidTaxAmount").setText(
                            NumberFormatter.getValue(NumberUtil.round(paidTax, 2), NumberFormatter.CURRENCY)));
                    summary.addContent(new Element("paidGrossAmount").setText(
                            NumberFormatter.getValue(NumberUtil.round(paidGross, 2), NumberFormatter.CURRENCY)));

                    summary.addContent(new Element("downpaymentsNet").setText(
                            NumberFormatter.getValue(NumberUtil.round(downpaymentsNet, 2), NumberFormatter.CURRENCY)));
                    summary.addContent(new Element("downpaymentsBaseTax").setText(
                            NumberFormatter.getValue(NumberUtil.round(downpaymentsBaseTax, 2), NumberFormatter.CURRENCY)));
                    summary.addContent(new Element("downpaymentsReducedTax").setText(
                            NumberFormatter.getValue(NumberUtil.round(downpaymentsReducedTax, 2), NumberFormatter.CURRENCY)));
                    summary.addContent(new Element("downpaymentsTax").setText(
                            NumberFormatter.getValue(NumberUtil.round(downpaymentsTax, 2), NumberFormatter.CURRENCY)));
                    summary.addContent(new Element("downpaymentsGross").setText(
                            NumberFormatter.getValue(NumberUtil.round(downpaymentsGross, 2), NumberFormatter.CURRENCY)));

                    summary.addContent(new Element("demandNet").setText(
                            NumberFormatter.getValue(NumberUtil.round(
                                    invoice.getAmounts().getAmount().subtract(downpaymentsNet), 2), NumberFormatter.CURRENCY)));
                    summary.addContent(new Element("demandTax").setText(
                            NumberFormatter.getValue(NumberUtil.round(
                                    invoice.getAmounts().getTaxAmount().subtract(downpaymentsTax), 2), NumberFormatter.CURRENCY)));
                    summary.addContent(new Element("demandGross").setText(
                            NumberFormatter.getValue(NumberUtil.round(
                                    invoice.getAmounts().getGrossAmount().subtract(downpaymentsGross), 2), NumberFormatter.CURRENCY)));

                    summary.addContent(new Element("payAmount").setText(fixZeroAndMixedTaxAmountToPay(
                            NumberFormatter.getValue(NumberUtil.round(payAmount, 2), NumberFormatter.CURRENCY))));

                    if (payAmount.doubleValue() < 0) {
                        summary.addContent(new Element("creditAvailable").setText("true"));
                        summary.addContent(new Element("remainingAmount").setText("false"));
                    } else if (payAmount.doubleValue() > 0) {
                        summary.addContent(new Element("creditAvailable").setText("false"));
                        summary.addContent(new Element("remainingAmount").setText("true"));
                    } else {
                        summary.addContent(new Element("creditAvailable").setText("false"));
                        summary.addContent(new Element("remainingAmount").setText("false"));
                    }

                    downpaymentElement.addContent(summary);
                }
            }
        }
        amounts.addContent(downpaymentElement);
    }

    private Element createPayments(Invoice invoice) {
        double taxrate = invoice.getAmounts().getTaxRate();
        Element payments = new Element("payments");
        for (int k = 0, l = invoice.getPayments().size(); k < l; k++) {
            Payment pm = invoice.getPayments().get(k);
            if (pm.getRecordId().equals(invoice.getId())) {
                Element item = new Element("payment");
                item.addContent(new Element("created").setText(DateFormatter.getDate(pm.getPaid())));
                if (!invoice.isTaxFree()) {
                    item.addContent(new Element("tax").setText(
                            xmlService.createString(taxrate)));
                    item.addContent(getTaxElement("taxFormatted", taxrate));
                    BigDecimal netAmount = pm.getAmount().divide(new BigDecimal(taxrate), RoundingMode.HALF_UP);
                    BigDecimal taxAmount = pm.getAmount().subtract(netAmount);
                    item.addContent(new Element("netAmount").setText(
                            NumberFormatter.getValue(netAmount, NumberFormatter.CURRENCY)));
                    item.addContent(new Element("taxAmount").setText(
                            NumberFormatter.getValue(taxAmount, NumberFormatter.CURRENCY)));
                    item.addContent(new Element("grossAmount").setText(
                            NumberFormatter.getValue(pm.getAmount(), NumberFormatter.CURRENCY)));
                    payments.addContent(item);
                } else {
                    item.addContent(new Element("tax").setText(nullAmount));
                    BigDecimal netAmount = pm.getAmount();
                    String netString = NumberFormatter.getValue(netAmount, NumberFormatter.CURRENCY);
                    item.addContent(new Element("netAmount").setText(netString));
                    item.addContent(new Element("taxAmount").setText(nullAmount));
                    item.addContent(new Element("grossAmount").setText(netString));
                    payments.addContent(item);
                }
            }
        }
        return payments;
    }

    private void addAmounts(Record rec, Element amounts, Amounts values) {
        if (values != null) {
            amounts.addContent(new Element("amount").setText(NumberFormatter
                    .getValue(values.getAmount(), NumberFormatter.CURRENCY)));

            amounts.addContent(new Element("reducedTaxAvailable").setText(
                    xmlService.createString(rec.isReducedTaxAvailable())));
            BigDecimal amountWithBaseTax = new BigDecimal(0);
            BigDecimal amountWithReducedTax = new BigDecimal(0);
            BigDecimal amountWithZeroTax = new BigDecimal(0);
            if (rec.isReducedTaxAvailable()) {
                for (int i = 0, j = rec.getItems().size(); i < j; i++) {
                    Item item = rec.getItems().get(i);
                    if (item.isIncludePrice()) {
                        if (item.getProduct().isReducedTax()) {
                            amountWithReducedTax = amountWithReducedTax.add(item.getAmount());
                        } else if (NumberUtil.isZeroTax(item.getTaxRate())) {
                            amountWithZeroTax = amountWithZeroTax.add(item.getAmount());
                        } else {
                            amountWithBaseTax = amountWithBaseTax.add(item.getAmount());
                        }
                    }
                }
                if (amountWithBaseTax.doubleValue() > 0) {
                    amounts.addContent(new Element("amountWithBaseTax").setText(NumberFormatter
                            .getValue(amountWithBaseTax, NumberFormatter.CURRENCY)));
                } else {
                    amounts.addContent(new Element("amountWithBaseTax"));
                }
                if (amountWithReducedTax.doubleValue() > 0) {
                    amounts.addContent(new Element("amountWithReducedTax").setText(NumberFormatter
                            .getValue(amountWithReducedTax, NumberFormatter.CURRENCY)));
                } else {
                    amounts.addContent(new Element("amountWithReducedTax"));
                }
                amounts.addContent(new Element("amountWithZeroTax").setText(NumberFormatter
                        .getValue(amountWithZeroTax, NumberFormatter.CURRENCY)));
                if (amountWithZeroTax.doubleValue() > 0) {
                    String zeroTaxTextPrefix = systemConfigs.getSystemProperty("taxRateZeroTextPrefix");
                    if (zeroTaxTextPrefix == null) {
                        zeroTaxTextPrefix = getResourceString("taxRateZeroPrefix");
                    }
                    if (zeroTaxTextPrefix != null) {
                        amounts.addContent(new Element("zeroTaxTextPrefix").setText(zeroTaxTextPrefix));
                    } else {
                        amounts.addContent(new Element("zeroTaxTextPrefix"));
                    }
                    String zeroTaxTextPostfix = systemConfigs.getSystemProperty("taxRateZeroTextPostfix");
                    if (zeroTaxTextPostfix != null) {
                        amounts.addContent(new Element("zeroTaxTextPostfix").setText(zeroTaxTextPostfix));
                    } else {
                        amounts.addContent(new Element("zeroTaxTextPostfix"));
                    }
                }
            } else {
                amounts.addContent(new Element("amountWithBaseTax").setText(NumberFormatter
                        .getValue(values.getAmount(), NumberFormatter.CURRENCY)));
                amounts.addContent(new Element("amountWithReducedTax"));
                amounts.addContent(new Element("amountWithZeroTax"));
            }
            Element salesTax = new Element("salesTax");
            salesTax.addContent(new Element("taxfree").setText(xmlService
                    .createString(rec.isTaxFree())));
            if (rec.isTaxFree()) {
                if (log.isDebugEnabled()) {
                    log.debug("addAmounts() record is tax free");
                }
                amounts.addContent(new Element("grossAmount")
                        .setText(NumberFormatter.getValue(values.getAmount(),
                                NumberFormatter.CURRENCY)));
                if (rec.getTaxFreeId() != null) {
                    salesTax.addContent(getTaxFreeDefinition("reason", rec.getTaxFreeId()));
                } else {
                    salesTax.addContent(new Element("reason"));
                }
                salesTax.addContent(new Element("reducedTax")
                        .setText(nullAmount));
                salesTax.addContent(new Element("tax").setText(nullAmount));

            } else {
                if (log.isDebugEnabled()) {
                    log.debug("addAmounts() record with tax");
                }
                amounts.addContent(new Element("grossAmount")
                        .setText(NumberFormatter.getValue(
                                values.getGrossAmount(),
                                NumberFormatter.CURRENCY)));
                salesTax.addContent(new Element("reason"));
                // reduced tax
                salesTax.addContent(createTax("reducedTax", values, true));
                // base tax
                salesTax.addContent(createTax("tax", values, false));
            }
            amounts.addContent(salesTax);
            if (log.isDebugEnabled()) {
                log.debug("addAmounts() done");
            }
        } else {
            log.warn("addAmounts() invoked with value param as null [record=" + rec.getId()
                    + ", type=" + rec.getType().getId()
                    + "]");
        }
    }

    private String fixZeroAndMixedTaxAmountToPay(String payAmount) {
        if (payAmount.equals("-0.01")) {
            return "0.00";
        }
        if (payAmount.equals("-0,01")) {
            return "0,00";
        }
        return payAmount;
    }

    private Element createTax(String name, Amounts values, boolean reduced) {
        BigDecimal amount = reduced ? values.getReducedTaxAmount() : values.getTaxAmount();
        Double rate = reduced ? values.getReducedTaxRate() : values.getTaxRate();
        Element tax = new Element(name);
        if (amount != null && amount.doubleValue() != 0 && rate != null) {
            tax.addContent(new Element("percent").setText(xmlService.createString(rate)));
            tax.addContent(new Element("amount").setText(NumberFormatter.getValue(
                    amount, NumberFormatter.CURRENCY)));
            tax.addContent(getTaxElement("formatted", rate));
        }
        return tax;
    }

    private Element getTaxElement(String name, Double rate) {
        Double val = isNotSet(rate) ? null : rate;
        if (val != null) {
            return new Element(name).setText(NumberFormatter.getIntValue(rate * 100 - 100));
        }
        return new Element(name);
    }

    private void addEmptyAmounts(Record rec, Element amounts) {
        amounts.addContent(new Element("amount").setText(nullAmount));
        amounts.addContent(new Element("grossAmount").setText(nullAmount));
        Element salesTax = new Element("salesTax");
        salesTax.addContent(new Element("taxfree").setText(
                xmlService.createString(rec.isTaxFree())));
        if (rec.isTaxFree()) {
            salesTax.addContent(getTaxFreeDefinition("reason", rec.getTaxFreeId()));
        } else {
            salesTax.addContent(new Element("reason"));
        }
        salesTax.addContent(new Element("reducedTax").setText(nullAmount));
        salesTax.addContent(new Element("tax").setText(nullAmount));
        amounts.addContent(salesTax);
    }

    private Element createItems(Record rec) {
        return createItems(rec, "items", rec.getItems());
    }

    private Element createOptions(Record rec) {
        if (rec instanceof CalculationAwareRecord) {
            CalculationAwareRecord crecord = (CalculationAwareRecord) rec;
            return createItems(rec, "options", crecord.getOptions());
        }
        return new Element("options");
    }

    private Element createItems(Record rec, String name, List<Item> items) {
        Element root = new Element(name);
        if (!items.isEmpty()) {
            boolean delivery = false;
            if (rec instanceof PurchaseInvoice &&
                    ((PurchaseInvoice) rec).isCreditNote()) {
                delivery = true;
            }
            String lang = rec.getLanguage();
            if (lang == null || !Constants.DEFAULT_LANGUAGE.equals(lang)) {
                if (!products.descriptionsAvailable(items, lang)) {
                    lang = Constants.DEFAULT_LANGUAGE;
                }
            }
            for (int i = 0, j = items.size(); i < j; i++) {
                Item item = items.get(i);
                root.addContent(createItem(item, lang, delivery));
            }
        }
        return root;
    }

    private Element createItem(Item item, String lang, boolean delivery) {
        Element it = item.getXML(false);

        QuantityUnit qunit = (QuantityUnit) getOption(Options.QUANTITY_UNITS, item.getProduct().getQuantityUnit());
        if (qunit != null) {
            Double quantity = item.getQuantity();
            if (delivery && isSet(quantity) && quantity < 0) {
                quantity = quantity * (-1);
            }
            String q = NumberFormatter.getValue(
                    quantity,
                    qunit.getDigits() == 0 ? NumberFormatter.BYTES : NumberFormatter.DECIMAL,
                    qunit.getDigits());
            if (it.getChild("quantity") != null) {
                it.getChild("quantity").setText(q);
            }
        }
        String quantityUnit = qunit != null && isSet(qunit.getName()) ? qunit.getName() : null;
        if (isSet(quantityUnit)) {
            it.addContent(new Element("quantityUnitName").setText(quantityUnit));
        } else {
            it.addContent(new Element("quantityUnitName"));
        }

        if (!Constants.DEFAULT_LANGUAGE.equals(lang)) {
            ProductDescription description = products.findDescription(item.getProduct().getProductId(), lang);
            if (description != null) {
                it.removeChild("description");
                Element el = JDOMUtil.createListByText("description", "line", description.getDescription());
                it.addContent(el);
            }
        }
        return it;
    }

    private Element createContactPerson(Record record) {
        Element cp = new Element("contactPerson");
        if (record.getPersonId() != null && record.getPersonId() != 0) {
            List<Contact> existing = contactSearch.findContactPersons(record.getContact().getContactId());
            for (int i = 0, j = existing.size(); i < j; i++) {
                Contact next = existing.get(i);
                if (next.getContactId().equals(record.getPersonId())) {
                    cp.setText(createContactName(next));
                    if (log.isDebugEnabled()) {
                        log.debug("createContactPerson() contact added " + next.getContactId());
                    }
                    break;
                }
            }
        }
        return cp;
    }

    private String createContactName(Contact c) {
        StringBuilder buffer = new StringBuilder();
        if (c.getSalutation() != null) {
            buffer.append(c.getSalutation().getName()).append(" ");
        }
        if (c.getFirstName() != null) {
            buffer.append(c.getFirstName()).append(" ");
        }
        buffer.append(c.getLastName());
        return buffer.toString();
    }
}
