/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 21-Jun-2005 19:29:00 
 * 
 */
package com.osserp.core.finance;

import java.util.List;
import java.util.Map;

import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BillingTypeSearch {

    /**
     * Provides billing types by billing class context
     * @param contextName
     * @return types
     */
    List<BillingType> getBillingTypes(String contextName);

    /**
     * Provides a list of all invoice types
     * @see com.osserp.core.finance.BillingType
     * @return list of invoice type values
     */
    List<BillingType> findInvoiceTypes();

    /**
     * Provides a list of all payment types
     * @see com.osserp.core.finance.BillingType
     * @return list of payment type values
     */
    List<BillingType> findPaymentTypes();

    /**
     * Provides a list of all payment types depending on provided record
     * and status of the record.
     * @param user
     * @param record
     * @return list of payment type values
     */
    List<BillingType> getPaymentTypes(DomainUser user, PaymentAwareRecord record);

    /**
     * Provides billing fcs action relations
     * @return map of fcs id billing type mapping
     */
    Map<Long, Long> findBillingFcsRelations();
}
