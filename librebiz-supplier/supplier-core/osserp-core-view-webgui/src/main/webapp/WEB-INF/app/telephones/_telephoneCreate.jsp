<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<c:set var="view" value="${sessionScope.telephoneCreateView}" />
<c:set var="url" value="/telephones/telephoneCreate" />

<c:if test="${!empty view}">
    <v:ajaxForm name="dynamicForm" targetElement="${view.name}_popup" url="/telephones/telephoneCreate/save">
        <div class="modalBoxHeader">
            <div class="modalBoxHeaderLeft">
                <fmt:message key="${view.headerName}" />
            </div>
            <div class="modalBoxHeaderRight">
                <div class="boxnav">
                    <ul>
                        <c:forEach var="nav" items="${view.navigation}">
                            <li><v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}">
                                    <o:img name="${nav.icon}" />
                                </v:ajaxLink></li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modalBoxData">
            <div class="subcolumns">
                <div class="subcolumn">
                    <c:if test="${!empty sessionScope.error}">
                        <div class="spacer"></div>
                        <div class="errormessage">
                            <fmt:message key="error" />
                            :
                            <fmt:message key="${sessionScope.error}" />
                        </div>
                        <o:removeErrors />
                    </c:if>
                    <div class="spacer"></div>
                    <c:choose>
                        <c:when test="${empty view.bean}">
                            <table class="valueTable first33 inputs230">
                                <tbody>
                                    <tr>
                                        <td><fmt:message key="macAddress" /></td>
                                        <td><input class="text" type="text" name="macAddress" value="${view.macAddress}" /></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="type" /></td>
                                        <td><oc:select name="type" options="telephoneTypes" value="${view.type}" /></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="telephoneSystem" /></td>
                                        <td><select id="system" name="system">
                                                <option value="0" <c:if test="${view.system == null || view.system == 0}">selected="selected"</c:if>><fmt:message key="selection" /></option>
                                                <c:forEach var="obj" items="${view.telephoneSystems}">
                                                    <option value="${obj.id}" <c:if test="${obj.id == view.system}">selected="selected"</c:if>><o:out value="${obj.name}" /></option>
                                                </c:forEach>
                                        </select></td>
                                    </tr>
                                    <tr>
                                        <td class="row-submit"><v:ajaxLink url="${url}/exit" targetElement="${view.name}_popup">
                                                <input type="button" class="cancel" value="<fmt:message key="exit"/>" />
                                            </v:ajaxLink></td>
                                        <td class="row-submit"><o:submit /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </c:when>
                        <c:otherwise>
                            <table class="valueTable first33 inputs230">
                                <tbody>
                                    <tr>
                                        <td><fmt:message key="macAddress" /></td>
                                        <td><o:out value="${view.bean.mac}" /></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="type" /></td>
                                        <td><c:choose>
                                                <c:when test="${empty view.bean.telephoneType.name}">(<fmt:message key="notSet" />)</c:when>
                                                <c:otherwise>
                                                    <o:out value="${view.bean.telephoneType.name}" />
                                                </c:otherwise>
                                            </c:choose></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="telephoneSystem" /></td>
                                        <td><c:choose>
                                                <c:when test="${empty view.bean.telephoneSystem.name}">(<fmt:message key="notSet" />)</c:when>
                                                <c:otherwise>
                                                    <o:out value="${view.bean.telephoneSystem.name}" />
                                                </c:otherwise>
                                            </c:choose></td>
                                    </tr>
                                </tbody>
                            </table>
                        </c:otherwise>
                    </c:choose>
                    <div class="spacer"></div>
                </div>
            </div>
        </div>
    </v:ajaxForm>
</c:if>
