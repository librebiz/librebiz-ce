/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 4:04:16 PM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.Item;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.ListItem;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.products.Product;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesOrder;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrders extends Orders, SalesRecords, BookingTypeAwareRecords {

    /**
     * Checks if all dependencies and requirements match to create an order
     * @param request
     * @throws ClientException if any required date or dependency missing
     */
    void checkCreate(Request request) throws ClientException;

    /**
     * Creates a new empty order related to given sale
     * @param user
     * @param sales
     * @param calculatorName
     * @param offer if exists (e.g. no direct sales)
     * @param dateConfirmed
     * @param note
     * @return new created order
     */
    Order create(Employee user, Sales sales, String calculatorName, SalesOffer offer, Date dateConfirmed, String note);
    
    /**
     * Creates a new and empty order by provided sales business case. This method is not dedicated to common use.
     * It's required if a business case sales exists without regular pre-existing offer. 
     * @param user
     * @param sales
     * @param defaultProduct an optional product added with quantity 1 if provided by systemConfig param  
     * @return order created empty order
     */
    Order create(Employee user, Sales sales, Product defaultProduct);

    /**
     * Creates a new internal sales order
     * @param user
     * @param company
     * @param branchId
     * @param bookingType
     * @param customer
     * @param reference
     * @param sale if reference.id differs from sale or null
     * @return new created internal order
     */
    Order create(Employee user, Long company, Long branchId, BookingType bookingType, Customer customer, Sales reference, Long sale);

    /**
     * Creates a new sales order by other record
     * @param user
     * @param sales
     * @param other record to copy items from
     * @param overridePrice if true, price will be copied from other record.
     *  If false price is set by product price and/or other rules.
     * @return new sales order
     */
    Order createByRecord(Employee user, Sales sales, Record other, boolean overridePrice) throws ClientException;

    /**
     * Creates a new order by provided sales and items or updates the items
     * of an existing order
     * @param sales
     * @param items
     * @return created or updated order
     */
    Order createOrUpdate(Sales sales, List<Item> items);

    /**
     * Cleans up all order positions
     * @param order
     */
    void clearOrder(Order order);

    /**
     * Updates the branch office by sales
     * @param sales
     */
    void changeBranch(Sales sales);

    /**
     * Tries to find an order related to given sales
     * @param sales
     * @return order or null if non exists
     */
    Order find(Sales sales);

    /**
     * Provides all records of implementing type related to a contact
     * @param contactId
     * @return related
     */
    List<SalesOrder> getByContact(Long contactId);

    /**
     * Tries to find all service order of a sales
     * @param sales
     * @return serviceOrders
     */
    List<Order> getServiceOrders(Sales sales);

    /**
     * Loads an order by sale when an order must exist. Method throws runtime exception if no order for sale exists. Use this method when loading order for
     * updates
     * @param sales
     * @return order
     */
    Order load(Sales sales);

    /**
     * Provides orders of a company
     * @param company
     * @param openonly
     * @return list
     */
    List<RecordDisplay> findByCompany(Long company, boolean openonly);

    /**
     * Provides orders of a company and reference
     * @param company
     * @param reference
     * @return list
     */
    List<RecordDisplay> findByCompanyAndReference(Long company, Long reference);

    /**
     * Resets the delivery date of an order related to a given sales process
     * @param sales
     */
    void resetDeliveryDate(Sales sales);

    /**
     * Tries to find the payment agreement of an order
     * @param orderId
     * @return paymentAgreement
     */
    RecordPaymentAgreement findPaymentAgreement(Long orderId);

    /**
     * Provides all record items related to given record
     * @param recordId
     * @return items
     */
    List<ListItem> getItemListing(Long recordId);

    /**
     * Creates a new delivery date history entry
     * @param user
     * @param current
     * @param previous
     * @return list of delivery date changed infos
     */
    List<DeliveryDateChangedInfo> createDeliveryChangedHistory(Employee user, SalesOrder order, List<Item> current, List<Item> previous);

    /**
     * Creates a new status history entry
     * @param user
     * @param current
     * @param previous
     * @return list of item changed infos
     */
    List<ItemChangedInfo> createItemChangedHistory(Employee user, List<Item> current, List<Item> previous);

    /**
     * Provides a base view operations selecting all orders. Result may be limited by appending a where clause and params
     * @param appendWhereClause
     * @param params
     * @param types
     * @param ignoreDefaultOrder if where clause includes an order by clause
     * @return orders
     */
    List<RecordDisplay> getView(
            String appendWhereClause,
            Object[] params,
            int[] types,
            boolean ignoreDefaultOrder);

    /**
     * Indicates existing lock
     * @param orderId
     * @return true if locked
     */
    boolean locked(Long orderId);
}
