/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 29, 2007 2:39:01 PM 
 * 
 */
package com.osserp.core.service.fcs.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.PermissionError;
import com.osserp.common.beans.OptionImpl;

import com.osserp.core.FcsAction;
import com.osserp.core.FcsConfig;
import com.osserp.core.Options;
import com.osserp.core.dao.RequestFcsActions;
import com.osserp.core.requests.RequestFcsActionManager;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestFcsActionManagerImpl extends AbstractFcsActionManager implements RequestFcsActionManager {
    private static Logger log = LoggerFactory.getLogger(RequestFcsActionManagerImpl.class.getName());
    
    private static final Long MAX_CANCELLATION = -100L;
    private static final Long MAX_ACTION = 100L;

    public RequestFcsActionManagerImpl(
            RequestFcsActions actions,
            OptionsCache optionsCache) {
        super(actions, optionsCache);
    }

    @Override
    protected String getCachedName() {
        return Options.REQUEST_FCS_ACTIONS;
    }

    public FcsConfig getConfig(Long type) {
        return getRequestFcsActions().findConfig(type);
    }
    
    public FcsConfig moveAction(FcsConfig config, Long actionId, boolean up) {
        if (up) {
            config.moveUp(actionId);
        } else {
            config.moveDown(actionId);
        }
        getRequestFcsActions().save(config);
        return getRequestFcsActions().getConfig(config.getId());
    }
    
    public FcsConfig addAction(DomainUser user, FcsConfig config, Long actionId) {
        return changeAction(user, config, actionId, false);
    }
    
    public FcsConfig removeAction(DomainUser user, FcsConfig config, Long actionId) {
        return changeAction(user, config, actionId, true);
    }
    
    private FcsConfig changeAction(DomainUser user, FcsConfig config, Long actionId, boolean remove) {
        if (user == null) {
            throw new PermissionError();
        }
        if (isNotSet(actionId)) {
            return config;
        }
        List<FcsAction> all = findAll();
        FcsAction action = null;
        for (int i = 1, j = all.size(); i < j; i++) {
            FcsAction next = all.get(i);
            if (next.getId().equals(actionId)) {
                action = next;
                break;
            }
        }
        if (action != null) {
            if (remove) {
                config.remove(user.getEmployee(), action);
            } else {
                config.add(user.getEmployee(), action);
            }
            getRequestFcsActions().save(config);
        }
        return getRequestFcsActions().getConfig(config.getId());
    }
    
    public List<FcsAction> findAll() {
        return getActions().getList();
    }
    
    public List<Option> getFreeActionStatus() {
        Set<Long> existing = getAllActionStatus(false);
        List<Option> result = new ArrayList<>();
        for (int i = 1; i < MAX_ACTION; i++) {
            Long next = Long.valueOf(i);
            if (!existing.contains(next)) {
                result.add(new OptionImpl(next, next.toString()));
            }
        }
        log.debug("getFreeCancellationStatus() done [size=" + result.size() + "]");
        return result;
    }
    
    public List<Option> getFreeCancellationStatus() {
        Set<Long> existing = getAllActionStatus(true);
        List<Option> result = new ArrayList<>();
        for (int i = -1; i > MAX_CANCELLATION; i--) {
            Long next = Long.valueOf(i);
            if (!existing.contains(next)) {
                result.add(new OptionImpl(next, next.toString()));
            }
        }
        log.debug("getFreeCancellationStatus() done [size=" + result.size() + "]");
        return result;
    }
    
    private Set<Long> getAllActionStatus(boolean cancellation) {
        Set<Long> result = new HashSet<Long>();
        List<Option> statusList = getRequestFcsActions().getStatusList();
        for (int i = 0, j = statusList.size(); i < j; i++) {
            Option next = statusList.get(i);
            if ((cancellation && next.getId().intValue() < 0)
                    || (!cancellation && next.getId().intValue() > 0)) {
                result.add(next.getId());
            }
        }
        log.debug("getAllActionStatus() done [size=" + result.size() 
                + ", cancellationMode=" + cancellation + "]");
        return result;
    }

    public List<FcsAction> createAction(
            String name, 
            String description, 
            Long groupId, 
            Long status, 
            String statusName)
            throws ClientException {
        if (isNotSet(status)) {
            throw new ClientException(ErrorCode.STATUS_MISSING);
        }
        List<FcsAction> all = findAll();
        for (int i = 0, j = all.size(); i < j; i++) {
            FcsAction next = all.get(i);
            if (next.getName().equalsIgnoreCase(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        Option selectedStatus = null; 
        List<Option> statusList = getRequestFcsActions().getStatusList();
        for (int i = 0, j = statusList.size(); i < j; i++) {
            Option next = statusList.get(i);
            if (next.getId().equals(status)) {
                selectedStatus = next;
                break;
            }
        }
        if (selectedStatus == null) {
            getRequestFcsActions().createStatus(status, isSet(statusName) ? statusName : name);
            getCache().refresh(Options.REQUEST_STATUS);
            selectedStatus = getRequestFcsActions().getStatus(status);
        }
        getRequestFcsActions().createAction(
                name, 
                description, 
                groupId, 
                false,  //displayEverytime, 
                selectedStatus.getId());
        getCache().refresh(Options.REQUEST_FCS_ACTIONS);
        return findAll(); 
    }

    private RequestFcsActions getRequestFcsActions() {
        return (RequestFcsActions) getActions();
    }
}
