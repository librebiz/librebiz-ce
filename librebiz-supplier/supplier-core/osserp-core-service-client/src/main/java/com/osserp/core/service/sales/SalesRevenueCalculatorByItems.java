/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 30, 2011 11:03:02 AM 
 * 
 */
package com.osserp.core.service.sales;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesRevenueCostCollector;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.model.SalesRevenueByItems;
import com.osserp.core.sales.SalesRevenue;
import com.osserp.core.sales.SalesRevenueConfig;
import com.osserp.core.sales.SalesRevenueConfigProvider;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRevenueCalculatorByItems extends AbstractSalesRevenueCalculator {
    private static Logger log = LoggerFactory.getLogger(SalesRevenueCalculatorByItems.class.getName());

    protected SalesRevenueCalculatorByItems(
            Employees employees,
            Products products,
            SystemConfigs systemConfigs,
            SalesCreditNotes salesCreditNotesDao,
            SalesRevenueConfigProvider salesRevenueConfigProvider,
            Map<String, SalesRevenueCostCollector> costCollectors) {
        super(
                employees,
                products,
                systemConfigs,
                salesCreditNotesDao,
                salesRevenueConfigProvider,
                costCollectors);
    }

    public SalesRevenue create(BusinessCase businessCase, Record record) {
        if (record == null) {
            throw new IllegalArgumentException("record must not be null");
        }
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [businessCase=" + businessCase.getPrimaryKey()
                    + ", record=" + record.getId()
                    + ", type=" + record.getClass().getName()
                    + "]");
        }
        SalesRevenueConfig cfg = getConfig();
        Employee salesPerson = getSalesPerson(businessCase);
        SalesRevenueByItems salesRevenue = createByRecord(salesPerson, cfg, businessCase, record);

        if (postCalculationRequired(businessCase, record)) {

            if (log.isDebugEnabled()) {
                log.debug("create: Invocation with billed order [sales=" + businessCase.getPrimaryKey() + "]");
            }
            String collectorName = getDefaultCostCollector();
            SalesRevenueCostCollector costCollector = collectorName == null ? null
                    : getCostCollector(collectorName);
            if (costCollector != null) {
                List<Item> costs = costCollector.getCosts(record);
                List<Item> thirdpartyCosts = costCollector.getThirdpartyCosts(record);
                if (costs.isEmpty()) {
                    // historical order, not billed by deliveryNotes
                    for (int i = 0, j = record.getItems().size(); i < j; i++) {
                        Item clone = (Item) record.getItems().get(i).clone();
                        costs.add(clone);
                    }
                }
                SalesRevenueByItems postCalculationRevenue = new SalesRevenueByItems(
                        businessCase,
                        salesPerson,
                        record,
                        getCreditNotes(record),
                        cfg.getHardwareCostsAFilterDescription(),
                        cfg.getHardwareCostsBFilterDescription(),
                        cfg.getHardwareCostsCFilterDescription(),
                        cfg.getOtherCostsFilterDescription(),
                        cfg.getForeignCostsFilterDescription(),
                        fetchItems(cfg.getHardwareCostsAFilter(), costs),
                        fetchItems(cfg.getHardwareCostsBFilter(), costs),
                        fetchItems(cfg.getHardwareCostsCFilter(), costs),
                        fetchItems(cfg.getOtherCostsFilter(), costs),
                        thirdpartyCosts,
                        fetchItems(cfg.getDevelopmentCostsFilter(), costs),
                        costs);
                postCalculationRevenue.setPostCalculation(true);
                salesRevenue.setPostCalculationRevenue(postCalculationRevenue);
            } else {
                log.info("create: Invalid costCollector configuration");
            }
        }
        return salesRevenue;
    }
}
