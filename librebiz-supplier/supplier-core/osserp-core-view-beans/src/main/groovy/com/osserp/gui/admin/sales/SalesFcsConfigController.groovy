/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 02.05.2011 11:29:00 
 * 
 */
package com.osserp.gui.admin.sales

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.gui.admin.fcs.FlowControlConfigController

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/admin/sales/salesFcsConfig/*")
@Controller class SalesFcsConfigController extends FlowControlConfigController {
    private static Logger logger = LoggerFactory.getLogger(SalesFcsConfigController.class.getName())

    @RequestMapping
    def compareBusinessType(HttpServletRequest request) {
        logger.debug("compareBusinessType: invoked [user=${getUserId(request)}]")
        SalesFcsConfigView view = getView(request)
        try {
            view.compareBusinessType()
        } catch (ClientException e) {
            saveError(request, e.message)
        }
        return defaultPage
    }
    
    @RequestMapping
    def disableComparisonMode(HttpServletRequest request) {
        logger.debug("disableComparisonMode: invoked [user=${getUserId(request)}]")
        SalesFcsConfigView view = getView(request)
        view.disableComparisonMode()
        return defaultPage
    }

    @RequestMapping
    def saveOrder(HttpServletRequest request) {
        logger.debug("saveOrder: invoked [user=${getUserId(request)}]")
        SalesFcsConfigView view = getView(request)
        try {
            view.saveOrder()
        } catch(Exception e) {
            logger.debug("saveOrder: caught exception [class=${e.class.name}, message=${e.message}]")
            saveError(request, e.message)
        }
        return defaultPage
    }

    @RequestMapping
    def enableOrderEditMode(HttpServletRequest request) {
        logger.debug("enableOrderEditMode: invoked [user=${getUserId(request)}]")
        SalesFcsConfigView view = getView(request)
        view.enableOrderEditMode()
        return defaultPage
    }

    @RequestMapping
    def disableOrderEditMode(HttpServletRequest request) {
        logger.debug("disableOrderEditMode: invoked [user=${getUserId(request)}]")
        SalesFcsConfigView view = getView(request)
        view.disableOrderEditMode()
        return defaultPage
    }

    @RequestMapping
    def enableEolDisplay(HttpServletRequest request) {
        logger.debug("enableEolDisplay: invoked [user=${getUserId(request)}]")
        SalesFcsConfigView view = getView(request)
        view.enableEolDisplay()
        return defaultPage
    }

    @RequestMapping
    def disableEolDisplay(HttpServletRequest request) {
        logger.debug("disableEolDisplay: invoked [user=${getUserId(request)}]")
        SalesFcsConfigView view = getView(request)
        view.disableEolDisplay()
        return defaultPage
    }

    @RequestMapping
    def changeEolFlag(HttpServletRequest request) {
        logger.debug("changeEolFlag: invoked [user=${getUserId(request)}]")
        SalesFcsConfigView view = getView(request)
        view.changeEolFlag()
        return defaultPage
    }
}
