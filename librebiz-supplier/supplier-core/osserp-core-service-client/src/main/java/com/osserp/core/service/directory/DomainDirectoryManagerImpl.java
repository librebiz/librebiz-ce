/**
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2012 11:45:06 AM
 * 
 */
package com.osserp.core.service.directory;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.directory.DirectoryGroup;
import com.osserp.common.directory.DirectoryManager;
import com.osserp.common.directory.DirectoryPermission;
import com.osserp.common.directory.DirectoryUser;
import com.osserp.common.service.Locator;
import com.osserp.core.directory.DomainDirectoryManager;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DomainDirectoryManagerImpl extends AbstractDirectoryService implements DomainDirectoryManager {
    private static Logger log = LoggerFactory.getLogger(DomainDirectoryManagerImpl.class.getName());

    public DomainDirectoryManagerImpl(
            SystemConfigManager systemConfigManager,
            DirectoryManager directoryManager,
            Locator locator) {
        super(systemConfigManager, directoryManager, locator);
    }

    public List<DirectoryUser> findUsers() {
        return getDirectoryManager().findUsers(getDirectoryClientName());
    }

    public DirectoryUser findUser(String uid) {
        log.debug("findUser() invoked [uid=" + uid + "]");
        return getDirectoryManager().findUser(uid, getDirectoryClientName());
    }

    public DirectoryUser updateUser(DirectoryUser user, Map values) {
        return getDirectoryManager().update(user, values);
    }

    public DirectoryUser updateUserPassword(DirectoryUser user, String password) {
        return getDirectoryManager().updatePassword(user, password);
    }

    public List<DirectoryGroup> findGroups() {
        return getDirectoryManager().findGroups(getDirectoryClientName());
    }

    public List<DirectoryGroup> findGroups(DirectoryUser user) {
        return getDirectoryManager().findGroups(user);
    }

    public DirectoryGroup updateGroup(DirectoryGroup group, Map values) {
        return getDirectoryManager().update(group, values);
    }

    public List<DirectoryPermission> findPermissions() {
        return getDirectoryManager().findPermissions(getDirectoryClientName());
    }

    public List<DirectoryPermission> findPermissions(DirectoryUser user) {
        return getDirectoryManager().findPermissions(user);
    }

    public DirectoryPermission updatePermission(DirectoryPermission permission, Map values) {
        return getDirectoryManager().update(permission, values);
    }
}
