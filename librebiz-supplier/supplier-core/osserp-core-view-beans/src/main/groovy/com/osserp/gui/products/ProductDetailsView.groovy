/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 12, 2012 
 * 
 */
package com.osserp.gui.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Details
import com.osserp.common.DetailsManager

import com.osserp.core.products.Product
import com.osserp.core.products.ProductManager

import com.osserp.gui.common.AbstractBusinessPropertyView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ProductDetailsView extends AbstractBusinessPropertyView {
    private static Logger logger = LoggerFactory.getLogger(ProductDetailsView.class.getName())

    List<String> measurementUnits = ['mm', 'cm', 'm']
    List<String> weightUnits = ['g', 'Kg', 't']

    ProductDetailsView() {
        super()
        dependencies = ['productView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (isSystemPropertyEnabled('productPropertySupport')) {
                providesCreateMode()
            } else {
                notProvidesCreateMode()
            }
        }
    }

    @Override
    protected Details fetchDetails() {
        product
    }

    @Override
    protected void reloadDetails() {
        env.productView?.reload()
        detailsObject = productManager.get(product.productId)
    }

    void saveMeasurements() {
        product.details.setWidth(form.getInteger('width'))
        product.details.setHeight(form.getInteger('height'))
        product.details.setDepth(form.getInteger('depth'))
        product.details.setMeasurementUnit(form.getString('measurementUnit'))
        product.details.setWeight(form.getDouble('weight'))
        product.details.setWeightUnit(form.getString('weightUnit'))
        product.details.setColor(form.getSelection('color'))
        productManager.update(product)
        reloadDetails()
    }
    
    final Product getProduct() {
        env.productView?.bean
    }

    final String getProductName() {
        if (env.productView?.bean?.productId && env.productView?.bean?.name) {
            return "${env.productView.bean.productId} - ${env.productView.bean.name}"
        }
        getLocalizedMessage('noProductSelectionAvailable')
    }

    protected final ProductManager getProductManager() {
        detailsManager
    }

    @Override
    protected DetailsManager getDetailsManager() {
        getService(ProductManager.class.getName())
    }
}
