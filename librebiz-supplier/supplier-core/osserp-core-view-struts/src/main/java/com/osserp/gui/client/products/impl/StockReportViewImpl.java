/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 24, 2008 12:33:49 PM 
 * 
 */
package com.osserp.gui.client.products.impl;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.util.StringUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductType;

import com.osserp.gui.client.products.ProductComparator;
import com.osserp.gui.client.products.StockReportView;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
@ViewName("stockReportView")
public class StockReportViewImpl extends AbstractProductView implements StockReportView {
    private static Logger log = LoggerFactory.getLogger(StockReportViewImpl.class.getName());
    private ProductGroup selectedProductGroup = null;
    private ProductType selectedProductType = null;
    private ProductCategory selectedProductCategory = null;
    private String sortCriteria = "byName";
    private boolean listVacantMode = true;
    private ProductComparator comparator = new ProductComparator();

    protected StockReportViewImpl() {
        super();
    }

    protected StockReportViewImpl(String defaultSortCriteria) {
        super();
        if (defaultSortCriteria != null) {
            sortCriteria = defaultSortCriteria;
        }
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        String ignorableGroupsList = getSystemConfigManager().getSystemProperty("stockReportIgnoreGroups");
        if (isSet(ignorableGroupsList)) {
            String[] ignorableGroupsArray = StringUtil.getTokenArray(ignorableGroupsList);
            for (int i = 0, j = ignorableGroupsArray.length; i < j; i++) {
                Long nextIgnorable = Long.valueOf(ignorableGroupsArray[i]);
                for (Iterator<ProductGroup> groups = getProductGroups().iterator(); groups.hasNext();) {
                    ProductGroup next = groups.next();
                    if (next.getId().equals(nextIgnorable)) {
                        groups.remove();
                        break;
                    }
                }
            }
        }
        Long defaultGroup = NumberUtil.createLong(getSystemConfigManager().getSystemProperty("stockReportDefaultGroup"));
        if (defaultGroup != null) {
            loadProductGroup(defaultGroup);
        }
        if (getList().isEmpty()) {
            loadValues();
        }
    }

    public boolean isListVacantMode() {
        return listVacantMode;
    }

    public void toggleListVacantMode() {
        listVacantMode = !listVacantMode;
    }

    public ProductCategory getSelectedProductCategory() {
        return selectedProductCategory;
    }

    public ProductGroup getSelectedProductGroup() {
        return selectedProductGroup;
    }

    public ProductType getSelectedProductType() {
        return selectedProductType;
    }

    public void loadProductCategory(Long id) {
        selectedProductCategory = fetchCategory(id);
        loadValues();
    }

    public void loadProductGroup(Long id) {
        selectedProductCategory = null;
        selectedProductGroup = fetchGroup(id);
        loadValues();
    }

    public void loadProductType(Long id) {
        selectedProductCategory = null;
        selectedProductGroup = null;
        selectedProductType = fetchType(id);
        loadValues();
    }

    @Override
    public void selectStock(Long id) {
        super.selectStock(id);
        loadValues();
    }

    public String getSortCriteria() {
        return sortCriteria;
    }

    @Override
    public void sortList(String criteria) {
        sortCriteria = criteria;
        comparator.sort(sortCriteria, getList());
    }

    public ProductComparator getComparator() {
        return comparator;
    }

    public void refresh() {
        loadValues();
    }

    private void loadValues() {
        List<Product> list = null;
        if (selectedProductCategory != null) {
            list = getProductManager().getStockAware(selectedProductCategory);
        } else if (selectedProductGroup != null) {
            list = getProductManager().getStockAware(selectedProductGroup);
        } else {
            list = getProductManager().getStockAware(selectedProductType);
        }

        if (!list.isEmpty() && getSelectedStock() != null) {
            Long stockId = getSelectedStock().getId();
            if (log.isDebugEnabled()) {
                log.debug("loadValues() done, enabling stock [id=" + stockId + "]");
            }
            for (int i = 0, j = list.size(); i < j; i++) {
                Product next = list.get(i);
                next.enableStock(stockId);
            }
        }
        setList(list);
        sortList(sortCriteria);
    }
}
