<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="portalView" value="${sessionScope.portalView}"/>
<o:displayPagePermissions>
<o:ajaxLink linkId="ajaxpopup" url="/app/permissions/showPagePermissions/show"><o:img name="eyesIcon"/></o:ajaxLink>
</o:displayPagePermissions>
<c:if test="${!empty portalView and portalView.helpAvailable}">
<a href="<o:out value="${portalView.helpLinkUrl}"/>" target="_blank" title="${portalView.helpLink.description}"><o:img name="helpIcon"/></a>
</c:if>
