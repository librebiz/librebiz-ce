/**
*
* Copyright (C) 2012 The original author or authors.
*
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
* 
* Created on Dec 23, 2012 
* 
*/
package com.osserp.core.dao;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Salutation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ContactCreator {

    /**
     * Creates a new business contact
     * @param createdBy
     * @param type
     * @param companyAffix
     * @param companyName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalStateId
     * @param federalStateName
     * @param country
     * @param website
     * @param email
     * @param phoneNumber
     * @param faxNumber
     * @param mobileNumber
     * @param initialStatus
     * @return contact
     * @throws ClientException if validation failed
     */
    Contact create(
            Long createdBy,
            Long type,
            String companyAffix,
            String companyName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalStateId,
            String federalStateName,
            Long country,
            String website,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException;

    /**
     * Creates a new freelance or SME contact
     * @param createdBy
     * @param type
     * @param companyAffix
     * @param companyName
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalStateId
     * @param federalStateName
     * @param country
     * @param website
     * @param email
     * @param phoneNumber
     * @param faxNumber
     * @param mobileNumber
     * @param initialStatus
     * @return contact
     * @throws ClientException if validation failed
     */
    Contact create(
            Long createdBy,
            Long type,
            String companyAffix,
            String companyName,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalStateId,
            String federalStateName,
            Long country,
            String website,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException;

    /**
     * Creates a new private contact
     * @param createdBy
     * @param type
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     * @param spouseSalutation
     * @param spouseTitle
     * @param spouseFirstName
     * @param spouseLastName
     * @param userSalutation
     * @param userNameAddressField
     * @param birthDate
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param email
     * @param phoneNumber
     * @param faxNumber
     * @param mobileNumber
     * @param initialStatus
     * @return contact
     * @throws ClientException if validation failed
     */
    Contact create(
            Long createdBy,
            Long type,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            Salutation spouseSalutation,
            Option spouseTitle,
            String spouseFirstName,
            String spouseLastName,
            String userSalutation,
            String userNameAddressField,
            Date birthDate,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber,
            Long initialStatus) throws ClientException;

    /**
     * Creates an initial contact person for companies
     * @param contactRef
     * @param createdBy
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param position
     * @param section
     * @param office
     * @return new created contact person
     * @throws ClientException if validation failed
     */
    Contact create(
            Contact contactRef,
            Long createdBy,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office)
            throws ClientException;

    /**
     * Creates a new contact person
     * @param contactRef related to new contact person
     * @param createdBy
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     * @param birthDate
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param position
     * @param section
     * @param office
     * @param email
     * @param phoneNumber
     * @param faxNumber
     * @param mobileNumber
     * @return new created contact
     * @throws ClientException if validation failed
     */
    Contact create(
            Contact contactRef,
            Long createdBy,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            Date birthDate,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office,
            String email,
            String phoneNumber,
            String faxNumber,
            String mobileNumber)
            throws ClientException;
    
    /**
     * Creates a new contact by typical internet form input. 
     * @param createdBy
     * @param type
     * @param company
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     * @param birthDate
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param email
     * @param phone
     * @param mobile
     * @param fax
     * @param grantEmail
     * @param grantPhone
     * @param grantNone
     * @param externalReference
     * @param externalUrl
     * @return new created contact
     * @throws ClientException if validation of minimal required values failed
     */
    Contact create(
            Long createdBy,
            Long type,
            String company,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            Date birthDate,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String email,
            String phone,
            String mobile,
            String fax,
            boolean grantEmail,
            boolean grantPhone,
            boolean grantNone,
            String externalReference,
            String externalUrl) throws ClientException;


    /**
     * Tries to find a salutation object by id or name. This method may be used 
     * by services retrieving a salutation either by id or string.
     * @param salutationId the id of the salutation if known. Salutation will
     * be load straightforward if provided.
     * @param salutationName the name of the salution if id not known, mapping
     * performed by string or leading external mapping (enable createMissing
     * to create external).
     * @param createMissing indicates whether salutation should be created if
     * a value is provided by name and no such salutation exists
     * @return salutation or null of not found and createMissing is disabled
     */
    Salutation findSalutation(Long salutationId, String salutationName, boolean createMissing);

    /**
     * Tries to find a title object by id or name. This method may be used 
     * by services retrieving a title either by id or string.
     * @param titleId the id of the title if known. Title will
     * be load straightforward if provided.
     * @param titleName the name of the title if id not known, mapping
     * performed by string or leading external mapping (enable createMissing
     * to create external).
     * @param createMissing indicates whether title should be created if
     * a value is provided by name and no such title exists
     * @return title or null of not found and createMissing is disabled
     */
    Option findTitle(Long title, String titleName, boolean createMissing);
}
