/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 4, 2010 2:48:27 PM 
 * 
 */
package com.osserp.core.telephone;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface SfaSubscriber extends AbstractTelephoneConfiguration {

    /**
     * Indicates if synced is true
     * @return synced
     */
    boolean isSynced();

    /**
     * Sets synced
     * @param synced
     */
    void setSynced(boolean synced);

    /**
     * Indicates if unused is true
     * @return unused
     */
    boolean isUnused();

    /**
     * Sets unused
     * @param unused
     */
    void setUnused(boolean unused);

    /**
     * Returns the telephoneSystemId
     * @return telephoneSystemId
     */
    Long getTelephoneSystemId();

    /**
     * Sets the telephoneSystemId
     * @param telephoneSystemId
     */
    void setTelephoneSystemId(Long telephoneSystemId);

    /**
     * Returns the phoneNumber
     * @return phoneNumber
     */
    String getPhoneNumber();

    /**
     * Sets the phoneNumber
     * @param phoneNumber
     */
    void setPhoneNumber(String phoneNumber);

    /**
     * Returns the mac
     * @return mac
     */
    String getMac();

    /**
     * Sets the mac
     * @param mac
     */
    void setMac(String mac);
}
