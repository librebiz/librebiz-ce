/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 8:57:24 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.Option;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsClosing;
import com.osserp.core.FcsConfig;
import com.osserp.core.FcsItem;
import com.osserp.core.dao.FcsWastebaskets;
import com.osserp.core.dao.RequestFcsActions;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.RequestFcsActionImpl;
import com.osserp.core.model.RequestFcsClosingImpl;
import com.osserp.core.model.RequestFcsConfigImpl;
import com.osserp.core.model.RequestFcsItemImpl;
import com.osserp.core.model.RequestStatusImpl;
import com.osserp.core.requests.Request;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestFcsActionsDao extends AbstractFcsActionsDao implements RequestFcsActions {
    private static Logger log = LoggerFactory.getLogger(RequestFcsActionsDao.class.getName());

    public RequestFcsActionsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            FcsWastebaskets baskets,
            String actionsTableKey,
            String dependencyTableKey,
            String wastebasketDefaultsTableKey) {
        super(jdbcTemplate, tables, sessionFactory, baskets, actionsTableKey,
                dependencyTableKey, wastebasketDefaultsTableKey);
    }

    public List<Option> getStatusList() {
        return getCurrentSession().createQuery(
                "from " + RequestStatusImpl.class.getName() + " o order by o.id")
                .list();
    }

    public Option getStatus(Long id) {
        return (Option) getCurrentSession().load(RequestStatusImpl.class, id);
    }

    public void createStatus(Long id, String name) {
        boolean alreadyExisting = false;
        List<Option> existing = getStatusList();
        for (int i = 0, j = existing.size(); i < j; i++) {
            Option next = existing.get(i);
            if (next.getId().equals(id)) {
                alreadyExisting = true;
                break;
            }
        }
        if (!alreadyExisting) {
            RequestStatusImpl obj = new RequestStatusImpl(id, name);
            getCurrentSession().save(RequestStatusImpl.class.getName(), obj);
        }
    }

    public FcsAction createAction(String name, String description, Long groupId, boolean displayEverytime, Long status) {
        RequestFcsActionImpl obj = new RequestFcsActionImpl(name, description, groupId, displayEverytime, status);
        getCurrentSession().save(RequestFcsActionImpl.class.getName(), obj);
        return obj;
    }

    @Override
    public FcsAction fetchAction(BusinessType type, Long id, String name) {
        FcsAction result = super.fetchAction(type, id, name);
        if (result == null) {
            List<FcsAction> list = getList();
            for (int i = 0, j = list.size(); i < j; i++) {
                FcsAction action = list.get(i);
                if (action != null && (id != null && id.equals(action.getId())
                        || name != null && name.equals(action.getName()))) {
                    result = action;
                    if (log.isDebugEnabled()) {
                        log.debug("fetchAction() found unassigned action [id="
                                + result.getId() + ", name=" + result.getName() + "]");

                    }
                    break;
                }
            }
        }
        return result;
    }

    @Override
    protected List<FcsAction> findActions(BusinessCase object) {
        return findByType(((Request) object).getType().getId());
    }

    @Override
    protected boolean isMatchingExternalStatus(FcsAction action, BusinessType businessType, String externalStatus) {
        // this method is only used to determine a starting or stopping action 
        // to create or update a businessCase by external data but start and
        // stop actions for requests are automatically determined on create.
        return false;
    }

    public List<FcsAction> getList() {
        return getCurrentSession().createQuery(
                "from " + RequestFcsActionImpl.class.getName() + " a order by a.status")
                .list();
    }

    public FcsAction load(Long id) {
        return (FcsAction) getCurrentSession().load(RequestFcsActionImpl.class, id);
    }

    public List<FcsAction> findByType(Long type) {
        if (type == null) {
            return getList();
        }
        FcsConfig config = findConfig(type);
        return config == null ? new ArrayList<FcsAction>() : config.getActions();
    }

    public FcsConfig findConfig(Long type) {
        List<FcsConfig> cfgList = getCurrentSession().createQuery(
                "from " + RequestFcsConfigImpl.class.getName() +
                " a where a.reference = :typeId")
                .setParameter("typeId", type)
                .list();
        if (cfgList.isEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("getConfig() no config defined [type=" + type + "]");
            }
            return null;
        }
        return cfgList.get(0);
    }

    public FcsConfig getConfig(Long id) {
        return (FcsConfig) getCurrentSession().load(RequestFcsConfigImpl.class, id);
    }

    public void save(FcsAction action) {
        getCurrentSession().saveOrUpdate(RequestFcsActionImpl.class.getName(), action);
    }

    public void save(FcsConfig config) {
        getCurrentSession().saveOrUpdate(RequestFcsConfigImpl.class.getName(), config);
    }

    public List<FcsItem> findItemsByReference(Long reference) {
        return getCurrentSession().createQuery(
                "from " + RequestFcsItemImpl.class.getName() +
                        " f where f.reference = :referenceId order by f.created desc")
                .setParameter("referenceId", reference)
                .list();
    }

    public void createActions(Employee user, BusinessType source, BusinessType target) {
        // create_request_fcs_actions(createdby bigint, sourceid bigint, targetid bigint)
        int result = jdbcTemplate.queryForObject(
                "select create_request_fcs_actions(?, ?, ?)",
                new Object[] { user.getId(), source.getId(), target.getId() },
                new int[] { Types.BIGINT, Types.BIGINT, Types.BIGINT }, Integer.class);
        if (log.isDebugEnabled()) {
            log.debug("createActions() done [count=" + result + "]");
        }
    }

    public List<FcsClosing> findClosings(Long type) {
        List<FcsClosing> result = new ArrayList<FcsClosing>();
        List<FcsClosing> list = getCurrentSession().createQuery(
                "from " + RequestFcsClosingImpl.class.getName() + " c order by c.name")
                .list();
        for (int i = 0, j = list.size(); i < j; i++) {
            FcsClosing next = list.get(i);
            if (type == null || next.getReference() == null || next.getReference().equals(type)) {
                result.add(next);
            }
        }
        return result;
    }

    public FcsClosing createClosing(Long createdBy, String name, String description) {
        FcsClosing result = null;
        List<FcsClosing> list = getCurrentSession().createQuery(
                "from " + RequestFcsClosingImpl.class.getName() + " c order by c.name")
                .list();
        for (int i = 0, j = list.size(); i < j; i++) {
            FcsClosing next = list.get(i);
            if (next.getName().equalsIgnoreCase(name)) {
                result = next;
                break;
            }
        }
        if (result == null) {
            RequestFcsClosingImpl closing = new RequestFcsClosingImpl(createdBy, name, description);
            getCurrentSession().save(RequestFcsClosingImpl.class.getName(), closing);
            result = closing;
        }
        return result;
    }
}
