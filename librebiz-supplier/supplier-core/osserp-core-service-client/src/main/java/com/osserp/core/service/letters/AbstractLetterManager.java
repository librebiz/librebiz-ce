/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 24, 2008 9:26:07 PM 
 * 
 */
package com.osserp.core.service.letters;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.Status;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.LetterDocuments;
import com.osserp.core.dao.Letters;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterManager;
import com.osserp.core.dms.LetterParameter;
import com.osserp.core.dms.LetterTemplate;
import com.osserp.core.dms.LetterType;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractLetterManager extends AbstractLetterContentManager implements LetterManager {
    private static Logger log = LoggerFactory.getLogger(AbstractLetterManager.class.getName());

    private LetterDocuments letterDocuments = null;

    protected AbstractLetterManager(OptionsCache optionsCache, Letters lettersDao, LetterDocuments letterDocuments, LetterOutputService letterOutputService) {
        super(optionsCache, lettersDao, letterOutputService);
        this.letterDocuments = letterDocuments;
    }

    public int getCount(LetterType type, ClassifiedContact recipient, Long businessId) {
        return getLettersDao().count(type.getId(), recipient.getId(), businessId);
    }

    public Letter findLetter(Long id) {
        return (Letter) getLettersDao().find(id);
    }

    public List<Letter> findLetters(
            LetterType type,
            ClassifiedContact recipient,
            Long businessId) {
        return getLettersDao().find(type.getId(), recipient.getId(), businessId);
    }

    public Letter create(
            DomainUser user,
            LetterType type,
            ClassifiedContact recipient,
            String name,
            Long businessId,
            Long branchId,
            LetterTemplate template)
            throws ClientException {

        String newName = name;
        if (isNotSet(newName)) {
            if (template == null || isNotSet(template.getSubject())) {
                throw new ClientException(ErrorCode.NAME_MISSING);
            }
            newName = template.getName();
        }
        List<Letter> existing = findLetters(type, recipient, businessId);
        if (!existing.isEmpty()) {
            for (int i = 0, j = existing.size(); i < j; i++) {
                Letter next = existing.get(i);
                if (next.getName().equalsIgnoreCase(newName)) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }
        return getLettersDao().create(
                getBusinessStatus(Status.CREATED),
                user.getEmployee(),
                type,
                recipient,
                newName,
                businessId,
                (branchId == null ? Constants.HEADQUARTER : branchId),
                template);
    }

    public void update(
            DomainUser user,
            Letter letter,
            String header,
            String name,
            String street,
            String zipcode,
            String city,
            Long country,
            String subject,
            String salutation,
            String greetings,
            String language,
            Long signatureLeft,
            Long signatureRight,
            Date printDate,
            boolean ignoreSalutation,
            boolean ignoreGreetings,
            boolean ignoreHeader,
            boolean ignoreSubject,
            boolean embedded,
            boolean embeddedAbove,
            String embeddedSpaceAfter,
            String contentKeepTogether)
            throws ClientException {

        letter.update(
                user.getEmployee(),
                header,
                name,
                street,
                zipcode,
                city,
                country,
                subject,
                salutation,
                greetings,
                language,
                signatureLeft,
                signatureRight,
                printDate,
                ignoreSalutation,
                ignoreGreetings,
                ignoreHeader,
                ignoreSubject,
                embedded,
                embeddedAbove,
                embeddedSpaceAfter,
                contentKeepTogether);
        getLettersDao().save(letter);
    }

    public void updateParameters(DomainUser user, Letter letter, Map<String, String> parameters) {
        for (Iterator<LetterParameter> i = letter.getParameters().iterator(); i.hasNext();) {
            LetterParameter next = i.next();
            String value = parameters.get(next.getName());
            if (isSet(value)) {
                next.setValue(value);
            }
        }
        letter.setChanged(new Date(System.currentTimeMillis()));
        letter.setChangedBy(user.getEmployee().getId());
        getLettersDao().save(letter);
    }

    public void close(DomainUser user, Letter letter) throws ClientException {
        Status status = getBusinessStatus(Status.RELEASED);
        if (status != null) {
            DocumentData output = createPdf(user, letter);
            if (output != null) {
                letter.setStatus(status);
                getLettersDao().save(letter);
                DmsDocument doc = letterDocuments.create(user, letter, output.getBytes());
                if (log.isDebugEnabled() && doc != null) {
                    log.debug("close() done letter [id=" + letter.getId()
                            + ", doc=" + doc.getId() + "]");
                }
            }
        }
    }

    public DocumentData getPdf(DomainUser user, LetterContent letterContent) {
        Letter letter = (Letter) letterContent;
        if (letter.isClosed()) {
            return fetchPdf(letter);
        }
        DocumentData pdf = createPdf(user, letter);
        if (!Status.PRINT.equals(letter.getStatus().getId())) {
            letter.setStatus(getBusinessStatus(Status.PRINT));
            getLettersDao().save(letter);
        }
        return pdf;
    }

    private DocumentData fetchPdf(Letter letter) {
        List<DmsDocument> list = letterDocuments.findByReference(new DmsReference(letterDocuments.getDocumentType().getId(), letter.getId()));
        DmsDocument doc = (list.isEmpty() ? null : list.get(0));
        return (doc == null ? null : new DocumentData(
                letterDocuments.getDocumentData(doc), 
                doc.getRealFileName(),
                Constants.MIME_TYPE_PDF));
    }

    private Letters getLettersDao() {
        return (Letters) getLetterContentsDao();
    }
}
