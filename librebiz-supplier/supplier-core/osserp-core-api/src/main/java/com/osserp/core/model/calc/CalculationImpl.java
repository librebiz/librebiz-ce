/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 15-Dec-2006 10:39:40 
 * 
 */
package com.osserp.core.model.calc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationTemplate;
import com.osserp.core.calc.CalculationTemplateItem;
import com.osserp.core.calc.CalculationUtil;
import com.osserp.core.model.ItemImpl;
import com.osserp.core.products.Package;
import com.osserp.core.products.Product;
import com.osserp.core.sales.SalesUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationImpl extends AbstractItemList implements Calculation {
    private static Logger log = LoggerFactory.getLogger(CalculationImpl.class.getName());
    private static final Double DISCOUNT_QUANTITY = 1d;

    private String calculatorClass = null;
    private Double minimalMargin = SalesUtil.getDefaultMinimalMargin();
    private Double targetMargin = SalesUtil.getDefaultTargetMargin();
    private Double plantCapacity;
    private boolean initial = false;
    private boolean planAvailable = false;
    private boolean packagePriceOverridden = false;
    private boolean salesPriceLocked = false;
    private List<ItemPosition> options = new ArrayList<ItemPosition>();

    /**
     * Creates a new and empty calculation required for object serialization. 
     */
    protected CalculationImpl() {
        super();
    }

    /**
     * Creates a new calculation with minimal required values
     * @param calculatorName
     * @param reference
     * @param contextName
     * @param wholesale
     * @param name
     * @param number
     * @param createdBy
     * @param initialCreated
     * @param minimalMargin
     * @param targetMargin
     */
    public CalculationImpl(
            String calculatorName,
            Long reference,
            String contextName,
            boolean wholesale,
            String name,
            Integer number,
            Long createdBy,
            Date initialCreated,
            Double minimalMargin,
            Double targetMargin) {
        super(reference, contextName, name, number, createdBy, initialCreated, wholesale);
        this.calculatorClass = calculatorName;
        this.minimalMargin = minimalMargin;
        this.targetMargin = targetMargin;
    }

    public String getCalculatorClass() {
        return calculatorClass;
    }

    public void setCalculatorClass(String calculatorClass) {
        this.calculatorClass = calculatorClass;
    }

    public Double getMinimalMargin() {
        return minimalMargin;
    }

    public void setMinimalMargin(Double minimalMargin) {
        this.minimalMargin = minimalMargin;
    }

    public Double getTargetMargin() {
        return targetMargin;
    }

    public void setTargetMargin(Double targetMargin) {
        this.targetMargin = targetMargin;
    }

    public boolean isPlanAvailable() {
        return planAvailable;
    }

    public void setPlanAvailable(boolean planAvailable) {
        this.planAvailable = planAvailable;
    }

    public Double getPlantCapacity() {
        return plantCapacity;
    }

    public void setPlantCapacity(Double plantCapacity) {
        this.plantCapacity = plantCapacity;
    }

    public boolean isInitial() {
        return initial;
    }

    public void setInitial(boolean initial) {
        this.initial = initial;
    }

    public boolean isPackagePriceOverridden() {
        return packagePriceOverridden;
    }

    protected void setPackagePriceOverridden(boolean packagePriceOverridden) {
        this.packagePriceOverridden = packagePriceOverridden;
    }

    public boolean isSalesPriceLocked() {
        return salesPriceLocked;
    }

    public void setSalesPriceLocked(boolean salesPriceLocked) {
        this.salesPriceLocked = salesPriceLocked;
    }

    @Override
    protected ItemPosition createPosition(String name, Long groupId, boolean discounts, boolean option, boolean partlist, Long partlistId) {
        return new CalculationPositionImpl(name, getId(), groupId, discounts, option, partlist, partlistId);
    }

    @Override
    protected void doAfterUpdateOrReplace(Item updated) {
        if (updated != null && updated.getProduct() != null && updated.getProduct().isPlant()) {
            Item pkg = fetchPackage();
            if (pkg != null && isSet(pkg.getQuantity())) {
                for (int i = 0, j = getPositions().size(); i < j; i++) {
                    ItemPosition pos = getPositions().get(i);
                    for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                        Item next = pos.getItems().get(k);
                        if (!next.getProduct().getProductId().equals(pkg.getProduct().getProductId())
                                && next.getProduct().isQuantityByPlant()) {
                            next.setQuantity(pkg.getQuantity());
                        }
                    }
                }
            }
            packagePriceOverridden = true;
            salesPriceLocked = true;
        }
    }

    public Long addTemplate(CalculationTemplate template, boolean wholeSale, Double reducedTax, Double normalTax) {
        Long errorProductId = null;
        List<CalculationTemplateItem> templateItems = template.getItems();
        Item pkg = fetchPackage();
        for (int j = 0; j < templateItems.size(); j++) {
            CalculationTemplateItem calculationTemplateItem = templateItems.get(j);
            CalculationPositionImpl position = (CalculationPositionImpl) getPositionByGroup(calculationTemplateItem.getCalculationConfigGroupId());
            if (position != null) {
                Double qty = (pkg != null && pkg.getProduct().isPlant() && calculationTemplateItem.getProduct() != null
                        && calculationTemplateItem.getProduct().isQuantityByPlant() ? pkg.getQuantity() : 1d);
                try {
                    CalculationUtil.validateProduct(calculationTemplateItem.getProduct(), qty, calculationTemplateItem.getNote());
                    Double price = wholeSale ? calculationTemplateItem.getProduct().getResellerPrice() : calculationTemplateItem.getProduct()
                            .getConsumerPrice();
                    position.addItem(
                            calculationTemplateItem.getProduct(),
                            null, // customName
                            qty,
                            new BigDecimal(price),
                            calculationTemplateItem.getPriceDate(),
                            false,
                            calculationTemplateItem.getPartnerPrice(),
                            calculationTemplateItem.isPartnerPriceEditable(),
                            false,
                            calculationTemplateItem.getPurchasePrice(),
                            calculationTemplateItem.getProduct().isReducedTax() ? reducedTax : normalTax,
                            calculationTemplateItem.getNote(),
                            !calculationTemplateItem.getProduct().isNullable(),
                            null);
                } catch (ClientException ce) {
                    errorProductId = calculationTemplateItem.getProduct().getProductId();
                    log.warn("addTemplate() error while adding template product [product=" + errorProductId + ", error=" + ce.getMessage() + "]");
                }
            }
        }
        refresh();
        return errorProductId;
    }

    public void removeTemplate(CalculationTemplate template) {
        List<CalculationTemplateItem> templateItems = template.getItems();
        for (int j = 0; j < templateItems.size(); j++) {
            CalculationTemplateItem calculationTemplateItem = templateItems.get(j);
            ItemPosition position = getPositionByGroup(calculationTemplateItem.getCalculationConfigGroupId());
            if (position != null) {
                for (Iterator<Item> ii = position.getItems().iterator(); ii.hasNext();) {
                    Item nextItem = ii.next();
                    if (nextItem.getProduct().getProductId().equals(calculationTemplateItem.getProduct().getProductId())) {
                        ii.remove();
                        break;
                    }
                }
            }
        }
        refresh();
    }

    public void addItem(
            Long positionId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            boolean priceOverridden,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            boolean includePrice,
            Long externalId)
            throws ClientException {

        CalculationUtil.validateProduct(product, quantity, note);
        if (isAlreadyAdded(product, null)) {
            throw new ClientException(ErrorCode.ITEM_EXISTING);
        }
        if (isPackageAvailable()) {
            if (product.isPlant() && isPackageAvailable()) {
                throw new ClientException(ErrorCode.PACKAGE_MAXCOUNT);
            }
            if (product.isQuantityByPlant()) {
                Item pkg = fetchPackage();
                quantity = pkg.getQuantity();
            }
            if (!product.isNullable()) {
                includePrice = true;
            }
        }
        CalculationPositionImpl position = (CalculationPositionImpl) getPosition(positionId);
        position.addItem(
                product,
                customName,
                this.fetchQuantity(product, quantity),
                price,
                priceDate,
                priceOverridden,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                (!product.isNullable() ? true : includePrice),
                externalId);
        refresh();
    }

    @Override
    public void replaceItem(
            Long itemId,
            Product product,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            BigDecimal purchasePrice,
            String note) throws ClientException {

        if (quantity == null || quantity == 0) {
            throw new ClientException(ErrorCode.QUANTITY_MISSING);
        }
        checkBeforeAdd(product);
        Item item = fetchItem(itemId);
        if (product.isPlant() && !item.getProduct().isPlant() && isPackageAvailable()) {
            throw new ClientException(ErrorCode.PACKAGE_MAXCOUNT);
        }
        if (isAlreadyAdded(product, itemId)) {
            throw new ClientException(ErrorCode.ITEM_EXISTING);
        }
        item.replaceProduct(
                product,
                fetchQuantity(product, quantity),
                (price == null ? new BigDecimal(Constants.DOUBLE_NULL) : price),
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                purchasePrice,
                note);
        refresh();
    }

    public void refreshPartnerPrices() {
        Date now = new Date(System.currentTimeMillis());
        for (Iterator<ItemPosition> pi = getPositions().iterator(); pi.hasNext();) {
            ItemPosition pos = pi.next();
            for (Iterator<Item> i = pos.getItems().iterator(); i.hasNext();) {
                ItemImpl item = (ItemImpl) i.next();
                item.setPriceDate(now);
                if (!item.getProduct().isPlant() && !item.isPartnerPriceOverridden()) {
                    if (item.getProduct().isPriceByQuantity()) {
                        item.setPartnerPrice(new BigDecimal(item.getProduct().getPartnerPriceByQuantity(item.getQuantity())));
                    } else {
                        item.setPartnerPrice(new BigDecimal(item.getProduct().getPartnerPrice()));
                    }
                }
            }
        }
    }

    public ItemPosition getOptionPosition(Long id) {
        for (int i = 0, j = options.size(); i < j; i++) {
            ItemPosition pos = options.get(i);
            if (pos.getId().equals(id)) {
                return pos;
            }
        }
        throw new ActionException("option position " + id + " no longer exists!");
    }

    public List<ItemPosition> getOptions() {
        return options;
    }

    protected void setOptions(List<ItemPosition> options) {
        this.options = options;
    }

    public void addOptionPosition(String name, Long groupId) {
        CalculationOptionPositionImpl obj = new CalculationOptionPositionImpl(name, getId(), groupId);
        options.add(obj);
    }

    public ItemPosition getOptionPositionByGroup(Long group) {
        for (int i = 0, j = options.size(); i < j; i++) {
            CalculationOptionPositionImpl next = (CalculationOptionPositionImpl) options.get(i);
            if (next.getGroupId().equals(group)) {
                return next;
            }
        }
        return null;
    }

    public boolean isOptionAvailable() {
        boolean result = false;
        List<ItemPosition> opts = getOptions();
        if (!opts.isEmpty()) {
            for (int i = 0, j = opts.size(); i < j; i++) {
                ItemPosition pos = opts.get(i);
                if (!pos.getItems().isEmpty()) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public void updatePriceDisplayState(Long itemId) throws ClientException {
        Item item = fetchItem(itemId);
        boolean packageAvailable = isPackageAvailable();
        if (item.isIncludePrice()
                && packageAvailable
                && !item.getProduct().isNullable()) {
            if (log.isDebugEnabled()) {
                log.debug("updatePriceDisplayState() not supported operation [item"
                        + item.getId() + ", packageAvailable=" + packageAvailable
                        + ", nullable=" + item.getProduct().isNullable() + "]");
            }
            throw new ClientException(ErrorCode.OPERATION_NOT_SUPPORTED);
        }
        item.setIncludePrice(!item.isIncludePrice());
        refresh();
    }

    public void updatePriceDisplayState(boolean enable) throws ClientException {
        for (int i = 0, j = getPositions().size(); i < j; i++) {
            ItemPosition pos = getPositions().get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.getProduct().isNullable()) {
                    next.setIncludePrice(enable);
                }
            }
        }
        refresh();
    }

    public void addDiscount(Long itemToDiscount, Product discount) throws ClientException {
        Item item = fetchItem(itemToDiscount);
        if (item.getPrice() == null || item.getPrice().equals(Constants.BIG_DECIMAL_NULL)) {
            throw new ClientException(ErrorCode.DISCOUNTABLE);
        } else if (item.getProduct().getProductId().equals(discount.getProductId())) {
            throw new ClientException(ErrorCode.DISCOUNT_NOT_DISCOUNTABLE);
        } else if (item.getProduct().isPlant()) {
            throw new ClientException(ErrorCode.OPERATION_NOT_SUPPORTED);
        }
        BigDecimal qty = new BigDecimal(item.getQuantity() * (-1));
        BigDecimal price = item.getPrice().multiply(qty);
        String note = item.getProduct().getName();
        addItem(
                getDiscountPosition(),
                discount,
                null, // customName
                DISCOUNT_QUANTITY,
                price,
                item.getPriceDate(),
                item.isPriceOverridden(),
                item.getPartnerPrice(),
                item.isPartnerPriceEditable(),
                item.isPartnerPriceOverridden(),
                item.getPurchasePrice(),
                item.getTaxRate(),
                note,
                true,
                null);
        refresh();
    }

    public void addOptionItem(
            Long positionId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            Long externalId)
            throws ClientException {

        if (quantity == null || quantity == 0) {
            throw new ClientException(ErrorCode.QUANTITY_MISSING);
        }
        CalculationOptionPositionImpl position = fetchOptionPosition(positionId);
        position.addItem(
                product, 
                customName, 
                quantity, 
                price, 
                priceDate, 
                false, // priceOverridden
                partnerPrice, 
                partnerPriceEditable, 
                false, // partnerPriceOverridde
                purchasePrice, 
                taxRate, 
                note, 
                true,  // includePrice
                externalId);
    }

    public void updateOptionItem(
            Long itemId,
            String customName,
            Double quantity,
            BigDecimal price,
            String note) throws ClientException {

        if (quantity == null || quantity == 0) {
            throw new ClientException(ErrorCode.QUANTITY_MISSING);
        }
        Item item = fetchOptionItem(itemId);
        item.setCustomName(customName);
        item.setQuantity(quantity);
        item.setPrice(price);
        item.setNote(note);
    }

    public void updateOptionItem(
            Long itemId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            String note) throws ClientException {

        if (quantity == null || quantity == 0) {
            throw new ClientException(ErrorCode.QUANTITY_MISSING);
        }
        checkBeforeAdd(product);
        CalculationOptionItemImpl item = (CalculationOptionItemImpl) fetchOptionItem(itemId);
        item.setProduct(product);
        item.setCustomName(customName);
        item.setQuantity(quantity);
        item.setPrice(price);
        item.setNote(note);
    }

    public void moveUpOptionItem(Long itemId) {
        ItemPosition position = fetchOptionPositionByItem(itemId);
        int itemCount = position.getItems().size();
        if (!(itemCount > 1)) {
            // nothing to do
            return;
        }
        CalculationOptionItemImpl itemToMove = (CalculationOptionItemImpl) fetchOptionItem(itemId);
        int currentOrder = itemToMove.getOrderId();
        if (currentOrder <= 1) {
            // nothing to do
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            CalculationOptionItemImpl next = (CalculationOptionItemImpl) position.getItems().get(i);
            if (next.getOrderId() == currentOrder - 1) {
                next.setOrderId(currentOrder);
                break;
            }
        }
        itemToMove.setOrderId(currentOrder - 1);
    }

    public void moveDownOptionItem(Long itemId) {
        ItemPosition position = fetchOptionPositionByItem(itemId);
        int itemCount = position.getItems().size();
        if (!(itemCount > 1)) {
            // nothing to do  
            return;
        }
        CalculationOptionItemImpl itemToMove = (CalculationOptionItemImpl) fetchOptionItem(itemId);
        int currentOrder = itemToMove.getOrderId();
        if (currentOrder == itemCount) {
            // nothing to do, item is already last
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            CalculationOptionItemImpl next = (CalculationOptionItemImpl) position.getItems().get(i);
            if (next.getOrderId() == currentOrder + 1) {
                next.setOrderId(currentOrder);
                break;
            }
        }
        itemToMove.setOrderId(currentOrder + 1);
    }

    public void removeOptionItem(Long itemId) {
        for (Iterator<ItemPosition> pi = options.iterator(); pi.hasNext();) {
            ItemPosition next = pi.next();
            for (Iterator<Item> ii = next.getItems().iterator(); ii.hasNext();) {
                Item nextItem = ii.next();
                if (nextItem.getId().equals(itemId)) {
                    ii.remove();
                    break;
                }
            }
        }
    }

    public void updateReference(String contextName, Long reference) {
        setContextName(contextName);
        setReference(reference);
        initial = true;
        setHistorical(true);
    }

    public boolean isPackageAvailable() {
        boolean result = false;
        for (int i = 0, j = getPositions().size(); i < j; i++) {
            ItemPosition pos = getPositions().get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.getProduct().isPlant()) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public void updatePackagePrice(BigDecimal partnerPrice, BigDecimal newPrice) {
        initCalculate();
        Item item = fetchPackage();
        if (item != null) {
            if (isSet(getPlantCapacity())) {
                BigDecimal power = new BigDecimal(getPlantCapacity());
                if (log.isDebugEnabled()) {
                    log.debug("updatePackagePrice() invoked for plant with power [id=" + getId()
                            + ", power=" + power
                            + ", packagePrice=" + item.getPrice()
                            + ", partnerPrice=" + partnerPrice
                            + ", salesPrice=" + newPrice
                            + "]");
                }
                item.setCalculatedPartnerPrice(partnerPrice.divide(power, 3, RoundingMode.HALF_UP));
                if (!salesPriceLocked) {
                    item.setPrice(newPrice.divide(power, 3, RoundingMode.HALF_UP));
                }
                if (log.isDebugEnabled()) {
                    log.debug("updatePackagePrice() done [calculation="
                            + getId()
                            + ", packagePrice=" + item.getPrice()
                            + ", partnerPrice=" + item.getPartnerPrice()
                            + ", salesPriceLocked=" + salesPriceLocked
                            + "]");
                }
                refresh();
            }
        }
    }

    public void validateCopyAttempt() throws ClientException {
        List<Item> items = getAllItems();
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            try {
                CalculationUtil.validateProduct(next.getProduct(), next.getQuantity(), next.getNote());
            } catch (ClientException c) {
                if (log.isDebugEnabled()) {
                    log.debug("validateCopyAttempt() failed [message=" + c.getMessage() + "]");
                }
                throw c;
            }
        }
    }

    private Item fetchPackage() {
        List<Item> allItems = getAllItems();
        for (int i = 0, j = allItems.size(); i < j; i++) {
            Item next = allItems.get(i);
            if (next.getProduct().isPlant()) {
                return next;
            }
        }
        return null;
    }

    private Item fetchOptionItem(Long itemId) {
        for (int i = 0, j = options.size(); i < j; i++) {
            ItemPosition pos = options.get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.getId().equals(itemId)) {
                    return next;
                }
            }
        }
        throw new ActionException("option item " + itemId + " no longer exists!");
    }

    private CalculationOptionPositionImpl fetchOptionPosition(Long id) {
        for (int i = 0, j = options.size(); i < j; i++) {
            ItemPosition pos = options.get(i);
            if (pos.getId().equals(id)) {
                return (CalculationOptionPositionImpl) pos;
            }
        }
        throw new ActionException("option position " + id + " no longer exists!");
    }

    private ItemPosition fetchOptionPositionByItem(Long itemId) {
        for (int i = 0, j = options.size(); i < j; i++) {
            ItemPosition pos = options.get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.getId().equals(itemId)) {
                    return pos;
                }
            }
        }
        throw new ActionException("option item " + itemId + " has no position!");
    }

    @Override
    protected void refresh() {
        setPrice(new BigDecimal(Constants.DOUBLE_NULL));
        initCalculate();
        for (int i = 0, j = getPositions().size(); i < j; i++) {
            ItemPosition pos = getPositions().get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.isIncludePrice()) {
                    BigDecimal itemSummary = new BigDecimal(Constants.DOUBLE_NULL);
                    if (next.getPrice() != null && next.getQuantity() != null) {
                        itemSummary = next.getPrice().multiply(new BigDecimal(next.getQuantity()));
                    }
                    setPrice(getPrice().add(itemSummary));
                }
            }
        }
        setPrice(getPrice().setScale(5, RoundingMode.HALF_UP));
    }

    protected void checkBeforeAdd(Product product) throws ClientException {
        // default implementation does nothing
    }

    protected void initCalculate() {
        if (log.isDebugEnabled()) {
            log.debug("initCalculate() invoked [action=none]");
        }
        setPlantCapacity(Constants.DOUBLE_NULL);
    }

    protected void updatePriceByQuantity(Item item, Double externalQuantity) {
        if (item.getProduct().isPriceByQuantity()) {
            Double quantity = ((externalQuantity == null || externalQuantity == 0) ? item.getQuantity() : externalQuantity);
            if (!item.isPartnerPriceOverridden()) {
                Double pp = item.getProduct().getPartnerPriceByQuantity(quantity);
                if (log.isDebugEnabled()) {
                    log.debug("updatePriceByQuantity() updating partnerPrice [calculation=" + getId()
                            + ", reference=" + getReference()
                            + ", product=" + item.getProduct().getProductId()
                            + ", quantity=" + quantity
                            + ", regularPartnerPrice=" + item.getProduct().getPartnerPrice()
                            + ", partnerPriceByQuantity=" + pp
                            + "]");
                }
                item.setCalculatedPartnerPrice(new BigDecimal(pp));
            } else if (log.isDebugEnabled()) {
                log.debug("updatePriceByQuantity() leaving partnerPrice by quantity unchanged [calculation=" + getId()
                        + ", reference=" + getReference()
                        + ", product=" + item.getProduct().getProductId()
                        + ", reason=partnerPriceOveridden]");
            }

            if (!item.isPriceOverridden()) {
                Double sp = isWholesale() ? item.getProduct().getResellerPriceByQuantity(quantity) : item.getProduct()
                        .getConsumerPriceByQuantity(quantity);
                if (log.isDebugEnabled()) {
                    log.debug("updatePriceByQuantity() updating salesPrice [calculation=" + getId()
                            + ", reference=" + getReference()
                            + ", product=" + item.getProduct().getProductId()
                            + ", quantity=" + quantity
                            + ", regularSalesPrice=" + (isWholesale() ? item.getProduct().getResellerPrice() : item.getProduct().getConsumerPrice())
                            + ", salesPriceByQuantity=" + sp
                            + ", salesPriceType=" + (isWholesale() ? "reseller" : "consumer")
                            + "]");
                }
                item.setPrice(new BigDecimal(sp));
            } else if (log.isDebugEnabled()) {
                log.debug("updatePriceByQuantity() leaving salesPrice unchanged [calculation=" + getId()
                        + ", reference=" + getReference()
                        + ", product=" + item.getProduct().getProductId()
                        + ", reason=salesPriceLocked]");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("updatePriceByQuantity() invoked with invalid product [calculation=" + getId()
                    + ", reference=" + getReference()
                    + ", product=" + item.getProduct().getProductId()
                    + ", reason=priceByQuantityDisabled]");
        }
    }

    private Long getDiscountPosition() throws ClientException {
        List<ItemPosition> positions = getPositions();
        for (int i = 0, j = positions.size(); i < j; i++) {
            ItemPosition pos = positions.get(i);
            if (pos.isDiscounts()) {
                return pos.getId();
            }
        }
        throw new ClientException(ErrorCode.DISCOUNT_DISABLED);
    }

    @Override
    protected Double fetchQuantity(Product product, Double quantity) {
        if (log.isDebugEnabled()) {
            log.debug("getQuantity() invoked [product="
                    + product.getProductId()
                    + ", quantity="
                    + quantity + "]");
        }
        if (product.isPlant()) {
            if (log.isDebugEnabled()) {
                log.debug("getQuantity() product is package...");
            }
            if (product.getDetails() != null
                    && product.getDetails() instanceof Package) {
                Package pkg = (Package) product.getDetails();
                if (pkg.isExtension() || pkg.isExchange()) {
                    if (log.isDebugEnabled()) {
                        log.debug("getQuantity() product is extension or exchange, result=1");
                    }
                    return 1d;
                }
            } else {
                String msg = product.getDetails() == null ? "details=null" : "details=" + product.getDetails().getClass().getName();
                log.warn("getQuantity() package info not available [product=" + product.getProductId()
                        + ", message=" + msg + "]");
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getQuantity() invoked [product=" + product.getProductId()
                    + ", quantity=" + quantity + "]");
        }
        return quantity;
    }

    public String getLogValues() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("[id=").append(getId()).append(", name=").append(getName()).append("]:\n");
        for (int i = 0, j = getPositions().size(); i < j; i++) {
            ItemPosition nextPos = getPositions().get(i);
            buffer
                    .append(">>> position [id=")
                    .append(nextPos.getId())
                    .append(", name=")
                    .append(nextPos.getName())
                    .append(", salesPriceLocked=")
                    .append(isSalesPriceLocked())
                    .append("]\n");
            for (int k = 0, l = nextPos.getItems().size(); k < l; k++) {
                Item next = nextPos.getItems().get(k);
                buffer
                        .append("--> item     [id=")
                        .append(next.getId())
                        .append(", quantity=")
                        .append(next.getQuantity())
                        .append(", product=")
                        .append(next.getProduct().getProductId())
                        .append(", name=")
                        .append(next.getProduct().getName())
                        .append("]\n");
            }
        }
        return buffer.toString();
    }
}
