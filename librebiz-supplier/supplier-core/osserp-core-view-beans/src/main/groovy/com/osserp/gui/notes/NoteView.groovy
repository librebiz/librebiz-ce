/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 18, 2012 12:42:33 PM 
 * 
 */
package com.osserp.gui.notes

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ActionException

import com.osserp.core.BusinessNote
import com.osserp.core.NoteAware
import com.osserp.core.NoteManager
import com.osserp.core.NoteType
import com.osserp.core.mail.MailMessageContext

import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class NoteView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(NoteView.class.getName())

    protected String serviceName
    NoteType type

    NoteAware noteAware
    BusinessNote stickyNote

    Boolean sendMail = false
    MailMessageContext mailMessageContext

    NoteView() {
        dependencies = [MailMessageContext.NAME]
        providesCreateMode()
        enableAutoreload()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            String noteTypeName = form.getString('name')
            serviceName = "${noteTypeName}Manager"
            type = noteManager.getType(noteTypeName)
            logger.debug("initRequest: initialized serviceName [type=${type?.id}, name=${serviceName}]")

            Long id = form.getLong('id')
            noteAware = noteManager.load(id)
            setupList()
            logger.debug("initRequest: done [reference=${noteAware.primaryKey}, availableNotes=${list.size()}]")
            if (!list) {
                enableCreateMode()
            }
        } else if (requestIs('addMailStatus')) {
            logger.debug("initRequest: invoked in addMailStatus context [status=${mailMessageContext.sendStatus}]")
        }
    }

    @Override
    void enableEditMode() {
        Long id = form.getLong('noteId')
        if (id) {
            bean = list.find { it.id == id }
            if (bean) {
                super.enableEditMode()
            }
        }
    }

    @Override
    void reload() {
        if (noteAware?.primaryKey) {
            noteAware = noteManager.load(noteAware.primaryKey)
            setupList()
        }
    }

    @Override
    void save() {

        String message = form.getString('message')
        String headline = form.getString('headline')
        sendMail = form.getBoolean('sendMail')
        noteAware = noteManager.load(noteAware.primaryKey)

        if (createMode) {
            noteAware = noteManager.addNote(domainEmployee, noteAware, headline, message)
            bean = noteAware.lastNote
            setupList()
            logger.debug("save: new note created [id=${bean?.id}, reference=${bean?.reference}, availableNotes=${list.size()}, sendMail=${sendMail}]")
            disableCreateMode()
        } else if (editMode) {
            noteAware = noteManager.updateNote(domainEmployee, noteAware, bean.id, message)
            setupList()
            bean = list.find { it.id == bean.id }
            disableEditMode()
        }
        if (sendMail) {
            createMailMessageContext()
        }
    }

    private void setupList() {
        list = noteAware.notes
        stickyNote = list.find { BusinessNote note -> note.sticky == true }
    }

    void addMailStatus() {
        if (bean) {
            noteAware = noteManager.completeNote(
                    noteAware,
                    bean.id,
                    mailMessageContext.allRecipients,
                    mailMessageContext.mail.subject,
                    mailMessageContext.mail.text)
            setupList()
            bean = null
        }
    }

    void setSticky() {
        Long id = form.getLong('noteId')
        if (id) {
            BusinessNote note = list.find { it.id == id }
            if (note) {
                noteAware = noteManager.setSticky(noteAware, note.id, !note.sticky)
                setupList()
            }
        }
    }

    void createMailMessageContext() {
        Long id = form.getLong('id')
        if (id) {
            bean = list.find { it.id == id }
        }
        if (bean) {
            Map<String, String> opts = [
                successTarget: '/notes/note/addMailStatus',
                exitTarget: '/notes/note/reload',
                viewHeader: noteAware.name,
                message: bean.note
            ]
            if (bean.email) {
                mailMessageContext = noteManager.setupMail(domainEmployee, bean, opts)
            } else {
                mailMessageContext = noteManager.setupMail(domainEmployee, noteAware, opts)
            }
        } else {
            logger.warn('createMailMessageContext: did not find note to send')
            throw new ActionException()
        }
    }

    BusinessNote fetchSelectedNote(Long id) {
        def note
        if (id) {
            note = list.find { it.id == id }
        }
        return note
    }

    @Override
    void createCustomNavigation() {
        nav.beanListNavigation = [ exitLink, createLink, homeLink ]
    }

    protected NoteManager getNoteManager() {
        String noteManagerName = getSystemProperty(serviceName)
        if (!noteManagerName) {
            logger.warn("getNoteManager: invalid property [serviceName=${serviceName}]")
            throw new ActionException()
        }
        return getService(getSystemProperty(serviceName))
    }
}
