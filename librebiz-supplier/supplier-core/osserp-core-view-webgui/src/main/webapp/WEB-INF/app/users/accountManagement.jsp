<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="accountManagementView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="setupContent">
            <div class="row">
                <v:form id="accountManagementForm" name="accountManagementForm" url="/users/accountManagement/save">
                    <c:choose>
                        <c:when test="${view.createMode}">
                            <c:import url="${viewdir}/users/_accountCreate.jsp"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="userUid" scope="request" value="${view.bean.ldapUid}"/>
                            <c:import url="${viewdir}/shared/_passwordChangeForm.jsp" />
                        </c:otherwise>
                    </c:choose>
                </v:form>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
