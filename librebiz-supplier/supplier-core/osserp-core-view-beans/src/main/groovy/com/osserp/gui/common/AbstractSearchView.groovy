/**
 *
 * Copyright (C) 2008, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 6, 2016 
 * 
 */
package com.osserp.gui.common

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.SearchRequest

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractSearchView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractSearchView.class.getName())

    SearchRequest searchRequest
    Map searchPropertyNames = [:]

    AbstractSearchView() {
        super()
        enableAutoreload()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {

            def selectionTarget = form.getString('selectionTarget')
            if (selectionTarget) {
                setSelectionTarget(createTarget(selectionTarget))
            } else {
                setSelectionTarget(createTarget(selectionTargetDefault))
            }
            def selectionExit = form.getString('selectionExit')
            if (selectionExit) {
                setSelectionExit(selectionExit)
            } else {
                setSelectionExit(selectionExitDefault)
            }
            logger.debug("initRequest: forward request done [selectionTarget=${env.selectionTarget}, selectionExit=${env.selectionExit}]")
        }
    }

    protected SearchRequest initSearchRequest(SearchRequest sr) {
        if (searchPropertyNames) {
            if (searchPropertyNames['orderByColumn']) {
                sr.orderByColumn = getSystemProperty(searchPropertyNames['orderByColumn'])
                actualSortKey = sr.orderByColumn ?: actualSortKey
            }
            if (searchPropertyNames['orderByDescendent']) {
                sr.orderByDescendent = isSystemPropertyEnabled(searchPropertyNames['orderByDescendent'])
                if (sr.orderByDescendent) {
                    actualSortReverse = true
                }
            }
            if (searchPropertyNames['ignoreFetchSize']) {
                sr.ignoreFetchSize = isSystemPropertyEnabled(searchPropertyNames['ignoreFetchSize'])
            }
            if (!sr.ignoreFetchSize && searchPropertyNames['fetchSize']) {
                sr.fetchSize = systemPropertyAsInt(searchPropertyNames['fetchSize'])
            }
        }
        return sr
    }

    abstract String getSelectionExitDefault()

    abstract String getSelectionTargetDefault()

    /**
     * Creates the searchRequest object by values provided by form. The result
     * is assigned to searchRequest which is used to perform the query.
     * @return search request object by user input 
     */
    protected abstract SearchRequest createSearchRequest()

    /**
     * Creates the searchRequest object used to execute an initial search without
     * user input on forward.
     * @return initial search object 
     */
    protected abstract SearchRequest createInitialSearchRequest()

    /**
     * Executes implementation specific search and returns the result.
     * This method will be called by save and reload after searchRequest
     * initialization.
     * @return resulting list
     */
    protected abstract List executeSearch()

    @Override
    void reload() {
        boolean initialEmpty = false
        if (!searchRequest) {
            searchRequest = createInitialSearchRequest()
            initialEmpty = true
        }
        if (searchRequest && !searchRequest.pattern) {
            searchRequest.pattern = initialSearchPattern
            initialEmpty = true
        }
        list = executeSearch()
        sort()
        if (initialEmpty) {
            searchRequest.pattern = null
        }
        logger.debug("reload: done [count=${list?.size()}]")
    }

    @Override
    final void save() {
        searchRequest = createSearchRequest()
        if (searchRequest.changed) {
            list = executeSearch()
            sort()
            logger.debug("save: done [count=${list?.size()}]")
        }
    }

    protected String getInitialSearchPattern() {
        'a'
    }
}
