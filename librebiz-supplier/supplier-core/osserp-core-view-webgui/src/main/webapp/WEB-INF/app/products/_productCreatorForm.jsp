<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productCreatorView" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <span style="text-decoration:italic;"><o:out value="${view.suggestionId}" /></span>
                <span> - </span><fmt:message key="createNewProduct"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productNumber"><fmt:message key="number" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text name="productId" value="${view.suggestionId}" styleClass="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productName"><fmt:message key="name" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text name="productName" styleClass="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productDescription"><fmt:message key="description" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:textarea styleClass="form-control" name="productDescription" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productType">
                        <v:link url="${view.baseLink}/updateClassification" title="productClassificationSelection">
                            <fmt:message key="productType" />
                        </v:link>
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="form-value">
                        <o:out value="${view.productClassificationConfig.type.name}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productGroup"><fmt:message key="group" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="form-value">
                        <o:out value="${view.productClassificationConfig.group.name}"/>
                    </div>
                </div>
            </div>
        </div>

        <c:if test="${!empty view.productClassificationConfig.category.name}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="productCategory"><fmt:message key="category" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="form-value">
                            <o:out value="${view.productClassificationConfig.category.name}"/>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="manufacturer"><fmt:message key="manufacturer" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <oc:select name="manufacturer" value="${view.form.params['manufacturer']}" promptKey="setupOption" options="productManufacturers" styleClass="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="newManufacturer"> </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text name="newManufacturer" placeholder="newManufacturer" styleClass="form-control"  />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productMatchcode"><fmt:message key="matchcode" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text name="productMatchcode" styleClass="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="productPurchaseText"><fmt:message key="purchaseText" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:textarea styleClass="form-control" name="purchaseText" rows="1" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="quantityUnit"><fmt:message key="quantityUnit" /></label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <oc:select name="quantityUnit" value="${view.form.params['quantityUnit']}" options="quantityUnits" styleClass="form-control" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="packagingUnit"><fmt:message key="packagingUnit" /></label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group right">
                    <v:text name="packagingUnit" styleClass="form-control right" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <o:submit styleClass="form-control" value="create" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
