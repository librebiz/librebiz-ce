/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 5, 2004 
 * 
 */
package com.osserp.core.contacts;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PhoneType {

    public static final String PHONE_KEY = "phone";
    public static final String FAX_KEY = "fax";
    public static final String MOBILE_KEY = "mobile";

    public static final Long PHONE = 1L;
    public static final Long MOBILE = 2L;
    public static final Long FAX = 3L;
    public static final Long OFFICE = 4L;

    public static final String getName(Long type) { // NO_UCD
        if (PHONE.equals(type)) {
            return PHONE_KEY;
        }
        if (MOBILE.equals(type)) {
            return MOBILE_KEY;
        }
        if (FAX.equals(type)) {
            return FAX_KEY;
        }
        return "unknown";
    }

    public static final Long getType(String name) { // NO_UCD
        if (PHONE_KEY.equals(name)) {
            return PHONE;
        }
        if (MOBILE_KEY.equals(name)) {
            return MOBILE;
        }
        if (FAX_KEY.equals(name)) {
            return FAX;
        }
        return PHONE;
    }
}
