/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 13, 2007 10:36:14 PM 
 * 
 */
package com.osserp.core.system;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.osserp.common.ClientException;
import com.osserp.common.Property;

import com.osserp.core.AddressLocality;
import com.osserp.core.BankAccount;
import com.osserp.core.Plugins;
import com.osserp.core.finance.Stock;
import com.osserp.core.employees.Employee;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SystemConfigManager {

    /**
     * Provides a system company
     * @param id
     * @return company
     */
    SystemCompany getCompany(Long id);

    /**
     * Provides all active companies
     * @return companies
     */
    List<SystemCompany> getActiveCompanies();

    /**
     * Provides activated plugins
     * @return plugins
     */
    Plugins getActivePlugins();

    /**
     * Provides all companies
     * @return companies
     */
    List<SystemCompany> getCompanies();

    /**
     * Provides a branch office by primary key
     * @return branch
     */
    BranchOffice getBranch(Long id);

    /**
     * Provides activated branch offices
     * @return branch office list
     */
    List<BranchOffice> getActivatedBranchs();

    /**
     * Provides options for printing
     * @param branchId primary key of selected branch office
     * @return printoptions map
     */
    Map<String, Object> getPrintOptions(Long branchId);

    /**
     * Provides all branch offices
     * @return branchs
     */
    List<BranchOffice> getBranchs();

    /**
     * Provides all available branch offices of a company
     * @return branchs by company
     */
    List<BranchOffice> getBranchs(Long company);

    /**
     * Provides all available branch offices accessible by employee
     * @return branchs employee has access to
     */
    List<BranchOffice> getBranchs(Employee employee);

    /**
     * Provides a branch selection by primary key
     * @param id
     * @return branch selection
     */
    BranchSelection getBranchSelection(Long id);

    /**
     * Provides a branch selection by name
     * @param name
     * @return branch selection
     */
    BranchSelection getBranchSelection(String name);
    
    /**
     * Provides address localities matching provided zipcode pattern
     * @param zipcode pattern
     * @return list of localities or empty list if no matching localities found
     */
    List<AddressLocality> getLocalitySelection(String zipcode);

    /**
     * Provides the headquarter of a company
     * @param company
     * @return headquarter
     * @throws ClientException if no headquarter found
     */
    BranchOffice getHeadquarter(Long company) throws ClientException;

    /**
     * Loads a company bankAccount
     * @param id primary key of the bank account
     * @return bankAccount
     */
    BankAccount getBankAccount(Long id);
    
    /**
     * Provides all mail domain names managed by system.
     * @return managed mail domain names
     */
    Set<String> getManagedMailDomainNames();

    /**
     * Provides value of a system property
     * @param name
     * @return value
     */
    String getSystemProperty(String name);

    /**
     * Indicates if a system property is enabled
     * @param name
     * @return true if property set to true (ignoring case)
     */
    boolean isSystemPropertyEnabled(String name);

    /**
     * Provides all system properties
     * @return properties
     */
    List<Property> getProperties();

    /**
     * Changes the value of a boolean system property
     * @param name
     */
    void switchBooleanSystemProperty(String name);

    boolean isStockManagementEnabled();

    void toggleStockManagement();

    /**
     * Updates existing stock object
     * @param user the updating user
     * @param stock
     * @param shortKey
     * @param name
     * @param description
     * @param active
     * @param activationDate
     * @param planningAware
     * @return stock
     * @throws ClientException if any string property already exists
     */
    Stock updateStock(
            DomainUser user,
            Stock stock,
            String shortKey,
            String name,
            String description,
            boolean active,
            Date activationDate,
            boolean planningAware)
        throws ClientException;

    /**
     * Provides the default stock
     * @return default stock
     */
    Stock getDefaultStock();

    /**
     * Provides all available stocks
     * @return stocks
     */
    List<Stock> getAvailableStocks();

    /**
     * Creates new stock object or provides existing
     * @param user
     * @param company
     * @param office
     * @return new or existing stock
     */
    Stock createStock(DomainUser user, SystemCompany company, BranchOffice office);

    /**
     * Indicates enabled stocktaking system property
     * @return true if enabled
     */
    boolean isStocktakingSupportEnabled();

    /**
     * Toggles stocktaking activation status
     */
    void toggleStocktakingSupport();

    /**
     * Provides a permission config
     * @param name
     * @return permisson array
     */
    String[] getPermissionConfig(String name);

    /**
     * Initiates an -asynchronous- refresh of all currently activated option caches
     */
    void initOptionsCacheRefreshTask();

    /**
     * Synchronizes branchs with groupware server
     */
    void synchronizeBranchs();

    /**
     * Checks if credentials available
     * @param context
     * @param name
     * @return true if calling getSerialValue returns a result
     */
    boolean credentialValueAvailable(String context, String name);

    /**
     * Saves a credential value
     * @param context
     * @param name
     * @param value
     * @throws ClientException if validation failed
     */
    void saveCredentials(String context, String name, String value) throws ClientException;

    /**
     * Changes a system property
     * @param name
     * @param value
     */
    void updateSystemProperty(String name, String value);
}
