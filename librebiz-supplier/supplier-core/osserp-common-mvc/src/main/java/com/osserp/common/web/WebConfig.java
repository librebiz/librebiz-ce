/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 15.08.2004 
 * 
 */
package com.osserp.common.web;

import java.io.Serializable;

/**
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class WebConfig implements Serializable {

    private String charset = null;
    private String displayName = null;
    private String displayTitle = null;
    private String displayCopyright = null;
    private String displayDistributor = null;
    private String displayRevision = null;
    private String displayVersion = null;
    private String layoutDefaultPath = "layout_default";
    private String layoutPath = "layout";
    private String distributionLabel = null;
    private String distributionPoweredByLabel = null;
    private String distributionPoweredByText = null;
    private String distributionPoweredByUrl = null;
    private String distributionText = null;
    private String distributionUrl = null;
    private String supportMailAddress = null;
    private boolean ssoEnabled = false;

    public WebConfig() {
        super();
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getDisplayCopyright() {
        return displayCopyright;
    }

    public void setDisplayCopyright(String displayCopyright) {
        this.displayCopyright = displayCopyright;
    }

    public String getDisplayDistributor() {
        return displayDistributor;
    }

    public void setDisplayDistributor(String displayDistributor) {
        this.displayDistributor = displayDistributor;
    }

    public String getDisplayVersion() {
        return displayVersion;
    }

    public void setDisplayVersion(String displayVersion) {
        this.displayVersion = displayVersion;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public String getDisplayRevision() {
        return displayRevision;
    }

    public void setDisplayRevision(String displayRevision) {
        this.displayRevision = displayRevision;
    }

    public String getLayoutDefaultPath() {
        return layoutDefaultPath;
    }

    public void setLayoutDefaultPath(String layoutDefaultPath) {
        this.layoutDefaultPath = layoutDefaultPath;
    }

    public String getLayoutPath() {
        return layoutPath;
    }

    public void setLayoutPath(String layoutPath) {
        this.layoutPath = layoutPath;
    }

    public String getDistributionLabel() {
        return distributionLabel;
    }

    public void setDistributionLabel(String distributionLabel) {
        this.distributionLabel = distributionLabel;
    }

    public String getDistributionText() {
        return distributionText;
    }

    public void setDistributionText(String distributionText) {
        this.distributionText = distributionText;
    }

    public String getDistributionUrl() {
        return distributionUrl;
    }

    public void setDistributionUrl(String distributionUrl) {
        this.distributionUrl = distributionUrl;
    }

    public String getDistributionPoweredByLabel() {
        return distributionPoweredByLabel;
    }

    public void setDistributionPoweredByLabel(String distributionPoweredByLabel) {
        this.distributionPoweredByLabel = distributionPoweredByLabel;
    }

    public String getDistributionPoweredByText() {
        return distributionPoweredByText;
    }

    public void setDistributionPoweredByText(String distributionPoweredByText) {
        this.distributionPoweredByText = distributionPoweredByText;
    }

    public String getDistributionPoweredByUrl() {
        return distributionPoweredByUrl;
    }

    public void setDistributionPoweredByUrl(String distributionPoweredByUrl) {
        this.distributionPoweredByUrl = distributionPoweredByUrl;
    }

    /**
     * Support Team mail address. Displayed in Error Pages.
     * @return supportMailAddress
     */
    public String getSupportMailAddress() {
        return supportMailAddress;
    }

    /**
     * Sets the Support Team mail address.
     * @param supportMailAddress
     */
    public void setSupportMailAddress(String supportMailAddress) {
        this.supportMailAddress = supportMailAddress;
    }

    /**
     * Indicates SSO availability
     * @return true if sso is enabled
     */
    public boolean isSsoEnabled() {
        return ssoEnabled;
    }

    public void setSsoEnabled(boolean ssoEnabled) {
        this.ssoEnabled = ssoEnabled;
    }
}
