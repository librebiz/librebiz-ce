package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.core.model.SelectionItemImpl;

public class SelectionItemRowMapper extends AbstractRowMapper implements RowMapper {

    public static final String VALUES = "selection,name,i18nkey,idx,created,created_by,end_of_life";
    public static final String SELECT_VALUES = "SELECT id," + VALUES;

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new SelectionItemImpl(
                rs.getLong("id"),
                this.fetchString(rs, "selection"),
                this.fetchString(rs, "name"),
                this.fetchString(rs, "i18nkey"),
                this.fetchBoolean(rs, "end_of_life"),
                this.fetchDate(rs, "created"),
                this.fetchLong(rs, "createdBy"),
                rs.getInt("idx"));
    }
}
