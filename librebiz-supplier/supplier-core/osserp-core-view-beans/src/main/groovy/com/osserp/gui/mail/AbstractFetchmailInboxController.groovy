/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.mail

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.ViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractFetchmailInboxController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(AbstractFetchmailInboxController.class.getName())

    @RequestMapping
    def delete(HttpServletRequest request) {
        logger.debug("delete: invoked [user=${getUserId(request)}]")
        AbstractFetchmailInboxView view = getView(request)
        try {
            view.delete()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        defaultPage
    }

    @RequestMapping
    def deleteAttachment(HttpServletRequest request) {
        logger.debug("deleteAttachment: invoked [user=${getUserId(request)}]")
        AbstractFetchmailInboxView view = getView(request)
        try {
            view.deleteAttachment()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        defaultPage
    }

    @RequestMapping
    def downloadAttachment(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("downloadAttachment: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        AbstractFetchmailInboxView view = getView(request)
        try {
            savePdf(request, view.downloadAttachment())
            renderPdf(request, response)

        } catch (Exception e) {
            logger.debug("print: caught exception [message=${e.message}]", e)
            saveError(request, e.message)
            return currentPage
        }
    }

    @RequestMapping
    def ignoreAttachment(HttpServletRequest request) {
        logger.debug("deleteAttachment: invoked [user=${getUserId(request)}]")
        AbstractFetchmailInboxView view = getView(request)
        try {
            view.ignoreAttachment()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        defaultPage
    }

    @RequestMapping
    def resetBusinessCase(HttpServletRequest request) {
        logger.debug("resetBusinessCase: invoked [user=${getUserId(request)}]")
        AbstractFetchmailInboxView view = getView(request)
        try {
            view.resetBusinessCase()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        return forward(request)
    }

    @RequestMapping
    def resetContact(HttpServletRequest request) {
        logger.debug("resetContact: invoked [user=${getUserId(request)}]")
        AbstractFetchmailInboxView view = getView(request)
        try {
            view.resetContact()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        return forward(request)
    }

    @RequestMapping
    def takeover(HttpServletRequest request) {
        logger.debug("takeover: invoked [user=${getUserId(request)}]")
        AbstractFetchmailInboxView view = getView(request)
        try {
            view.takeover()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        return forward(request)
    }
}
