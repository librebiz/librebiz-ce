/**
 *
 * Copyright (C) 2006, 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 8, 2006 6:32:32 AM 
 * Created on Dec 9, 2013 10:10:01 AM (Groovy implementation) 
 * 
 */
package com.osserp.gui.users

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.ViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/users/userPermissionSetup/*")
@Controller class UserPermissionSetupController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(UserPermissionSetupController.class.getName())

    @RequestMapping
    def grantPermission(HttpServletRequest request) {
        logger.debug("grantPermission: invoked [user=${getUserId(request)}]")
        UserPermissionSetupView view = getView(request)
        view.grantPermission()
        defaultPage
    }

    @RequestMapping
    def revokePermission(HttpServletRequest request) {
        logger.debug("revokePermission: invoked [user=${getUserId(request)}]")
        UserPermissionSetupView view = getView(request)
        view.revokePermission()
        defaultPage
    }

    @RequestMapping
    def toggleActivation(HttpServletRequest request) {
        logger.debug("toggleActivation: invoked [user=${getUserId(request)}]")
        UserPermissionSetupView view = getView(request)
        view.toggleActivation()
        defaultPage
    }
}
