/**
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 15, 2013 4:07:31 PM
 * 
 */
package com.osserp.core.service.directory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.directory.DirectoryManager;
import com.osserp.common.service.Locator;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDirectoryService extends AbstractService {
    private static Logger log = LoggerFactory.getLogger(AbstractDirectoryService.class.getName());

    private SystemConfigManager systemConfigManager;
    private DirectoryManager directoryManager;
    private Locator locator;

    protected AbstractDirectoryService() {
        super();
    }

    /**
     * Creates a common directoryService
     * @param systemConfigManager
     * @param directoryManager
     */
    protected AbstractDirectoryService(
            SystemConfigManager systemConfigManager,
            DirectoryManager directoryManager) {
        super();
        this.systemConfigManager = systemConfigManager;
        this.directoryManager = directoryManager;
    }

    /**
     * Creates a common directoryService
     * @param systemConfigManager
     * @param directoryManager
     */
    protected AbstractDirectoryService(
            SystemConfigManager systemConfigManager,
            DirectoryManager directoryManager,
            Locator locator) {
        super();
        this.systemConfigManager = systemConfigManager;
        this.directoryManager = directoryManager;
        this.locator = locator;
    }

    protected DirectoryManager getDirectoryManager() {
        return directoryManager;
    }

    protected SystemConfigManager getSystemConfigManager() {
        return systemConfigManager;
    }

    protected final String getDirectoryClientName() {
        return getProperty("ldapClientName");
    }

    protected final String getProperty(String name) {
        return systemConfigManager.getSystemProperty(name);
    }

    protected final boolean nocAvailable() {
        return isEnabled("syncNocEnabled");
    }

    protected final boolean isEnabled(String name) {
        return systemConfigManager.isSystemPropertyEnabled(name);
    }

    protected final DirectoryMapper getMapper(String name) {
        try {
            return (DirectoryMapper) getService(getProperty(name));
        } catch (Exception e) {
            log.warn("getMapper() caught exception [message=" + e.getMessage() + "]");
        }
        return null;
    }

    protected final Object getService(String name) {
        try {
            if (locator != null) {
                return locator.lookupService(name);
            }

            log.warn("getService() invoked while locator not available [name=" + name + "]");

        } catch (Exception e) {
            log.warn("getService() caught exception [message=" + e.getMessage() + "]", e);
        }
        return null;
    }

}
