/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.common.directory

import com.osserp.common.Constants
import com.osserp.common.ErrorCode
import com.osserp.common.PermissionException
import com.osserp.common.beans.AbstractEntity
import com.osserp.common.util.SecurityUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class DirectoryUser extends AbstractEntity {


    static final String BUSINESS_CATEGORY_EMPLOYEE = 'Employee'
    static final String BUSINESS_CATEGORY_MAILBOX = 'Mailbox'
    static final String BUSINESS_CATEGORY_SYSTEM = 'System'
    static final String BUSINESS_CATEGORY_SUBSCRIBER = 'Subscriber'

    // status active, password never expires
    static final String STATUS_SAMBA_ACCT_DEFAULT = 'UX'
    static final String STATUS_SAMBA_ACCT_DEFAULT_VALUE = '[UX]'
    // status Workstation, indicates none user account
    static final String STATUS_SAMBA_ACCT_WORKSTATION = 'W'
    static final String STATUS_SAMBA_ACCT_WORKSTATION_VALUE = '[W]'
    // status account disabled
    static final String STATUS_SAMBA_ACCT_DISABLED = 'D'
    static final String STATUS_SAMBA_ACCT_DISABLED_VALUE = '[D]'

    static final String GROUPWARE_ENABLED = 'OK'
    static final String GROUPWARE_DISABLED = 'NO'

    private static final Long POSIX_SYSTEM_ACCOUNT_MAX_ID = Long.valueOf(999)

    private static final String DEFAULT_OU = 'Users'
    private static final String RESIGN_OU = 'Resign'

    private static final String COMPUTER_ACCOUNT_DESCRIPTION = 'Computer'

    private static final List MAILBOX_ATTRIBUTES = [
        'uid',
        'uidnumber',
        'gidnumber',
        'osserpuid',
        'sn',
        'cn',
        'description',
        'employeetype',
        'ou',
        'gecos',
        'mail',
        'mailhomedir',
        'mailstoredir',
        'homedirectory',
        'loginshell'
    ]
    private static final List EMPLOYEE_ATTRIBUTES = [
        'uid',
        'uidnumber',
        'gidnumber',
        'osserpuid',
        'ou',
        'osserpcategory',
        'employeenumber',
        'employeetype',
        'gecos',
        'title',
        'givenname',
        'sn',
        'cn',
        'displayname',
        'street',
        'postalcode',
        'l',
        'st',
        'telephonenumber',
        'mobile',
        'mail',
        'mailhomedir',
        'mailstoredir',
        'homedirectory',
        'loginshell'
    ]

    Map values = [:]

    Long getId() {
        values.uidnumber ? Long.valueOf(values.uidnumber) : null
    }

    List getAttributeNames() {
        if (mailboxAccount || systemAccount) {
            return MAILBOX_ATTRIBUTES
        }
        EMPLOYEE_ATTRIBUTES
    }

    List getAllAttributeNames() {
        List list = []
        for (key in values.keySet()) {
            list << key
        }
        list.sort{it}
    }

    void setPassword(String password) {
        if (password) {
            values.userpassword = SecurityUtil.createPassword(password).getBytes()
        }
    }

    void setPasswords(String password) {
        if (password) {
            setPassword(password)
        }
    }

    /**
     * Checks cleartext password against stored hash
     * @param password
     * @throws PermissionException if password not set or available or password not match
     */
    void checkPassword(String password) throws PermissionException {
        String userPwd = getUserPasswordDisplay()
        if (!SecurityUtil.checkPassword(password, userPwd)) {
            throw new PermissionException(ErrorCode.LOGIN_FAILED)
        }
    }

    String getUserPasswordDisplay() {
        values.userpassword == null ? null : new String(values.userpassword)
    }

    /**
     * Indicates that samba account is disabled
     * @return sambaDeactivated
     */
    boolean isSambaDeactivated() {
        STATUS_SAMBA_ACCT_DISABLED_VALUE == values.sambaacctflags
    }

    /**
     * Deactivates the ldap user account
     */
    void deactivate() {
        values.loginshell = '/bin/false'
        values.ou = RESIGN_OU
        if (values.sambaacctflags) {
            values.sambaacctflags = STATUS_SAMBA_ACCT_DISABLED_VALUE
        }
    }

    boolean isGroupwareEnabled() {
        'true' == values.mailenabled
    }

    void enableGroupware() {
        values.mailenabled = 'true'

    }
    void disableGroupware() {
        values.mailenabled = 'false'
    }

    boolean isComputer() {
        COMPUTER_ACCOUNT_DESCRIPTION.equalsIgnoreCase(values.employeetype)
    }

    boolean isEmployeeAccount() {
        BUSINESS_CATEGORY_EMPLOYEE.equalsIgnoreCase(values.osserpcategory)
    }

    boolean isMailboxAccount() {
        BUSINESS_CATEGORY_MAILBOX.equalsIgnoreCase(values.osserpcategory)
    }

    boolean isSubscriberAccount() {
        BUSINESS_CATEGORY_SUBSCRIBER.equalsIgnoreCase(values.osserpcategory)
    }

    boolean isIgnoreMail() {
        ignoreGroupware
    }

    boolean isIgnoreGroupware() {
        if (values.uid && values.uid.indexOf('$') > -1) {
            return true
        }
        systemAccount
    }

    boolean isResign() {
        RESIGN_OU == values.ou
    }

    boolean isSystemAccount() {
        Long uidnumber = id
        uidnumber == null || uidnumber <= POSIX_SYSTEM_ACCOUNT_MAX_ID || BUSINESS_CATEGORY_SYSTEM.equalsIgnoreCase(values.osserpcategory)
    }

    boolean isDevice() {
        if (!values.sambaacctflags) {
            return false
        }
        return (computer || (values.sambaacctflags.indexOf(STATUS_SAMBA_ACCT_WORKSTATION) > -1)
                || (values.uid && values.uid.indexOf('$') > -1))
    }

    String getCurrentMailAccountName() {
        Map existing = values.mailAccount
        if (existing?.fetchmailusername) {
            return existing.fetchmailusername
        }
        return values.gecos
    }

    void addMailAccount(
            Map fetchmailDefaults,
            String userName,
            String password) {
        // TODO check if method is required at this place when using new ldapManager
        // remove method or implement
    }

    boolean isSambaSchemaAvailable() {
        def objectClasses = values.objectClass
        if (objectClasses) {
            objectClasses.each {
                if ('sambaSamAccount' == it) {
                    return true
                }
            }
        }
        return false
    }

    void update(Map<String, Object> valueMap) {

        valueMap.each { key, value ->
            values[key] = value
        }
        if (!values.preferedlanguage) {
            values.preferedlanguage = Constants.DEFAULT_LANGUAGE
        }
        if (!values.ou) {
            values.ou = DEFAULT_OU
        }
    }
}

