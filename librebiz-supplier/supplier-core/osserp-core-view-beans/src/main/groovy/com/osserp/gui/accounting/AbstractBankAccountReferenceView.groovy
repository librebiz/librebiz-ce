/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on 17.05.2013
 * 
 */
package com.osserp.gui.accounting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Constants
import com.osserp.common.util.DateUtil

import com.osserp.core.BankAccount
import com.osserp.core.contacts.ClassifiedContact

import com.osserp.gui.CoreView
import com.osserp.groovy.web.MenuItem

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractBankAccountReferenceView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractBankAccountReferenceView.class.getName())

    List<BankAccount> bankAccounts = []
    BankAccount selectedBankAccount

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            
            if (bankAccountPreselectionEnabled) {
            
                Long id = getLong('account')
                if (id) {
                    preselectSelectedBankAccount(id)
                } else {
                    bankAccounts = selectedCompany?.bankAccounts
                }
            }
        }
    }
    
    final int getCurrentYear() {
        DateUtil.currentYear
    }
    
    /**
     * Provides a link to return from previous selectBankAccount invocation
     * @return deselectBankAccountLink
     */
    protected MenuItem getDeselectBankAccountLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/selectBankAccount", icon: 'backIcon', title: 'backToLast')
    }

    void selectBankAccount() {
        selectSelectedBankAccount(form.getLong('id'))
    }
    
    void preselectSelectedBankAccount(Long id) {
        if (selectedCompany) {
            bankAccounts = selectedCompany.bankAccounts
            if (id && bankAccounts) {
                selectSelectedBankAccount(id)
            }
        }
    }

    protected void selectSelectedBankAccount(Long id) {
        if (!id) {
            selectedBankAccount = null
            logger.debug("selectSelectedBankAccount: reset done")
        } else {
            selectedBankAccount = bankAccounts.find { id == it.id }
            logger.debug("selectSelectedBankAccount: done [account=${selectedBankAccount?.id}]")
            if (selectedBankAccount?.reference && selectedBankAccount?.reference != selectedCompany?.id) {
                selectSelectedCompany(selectedBankAccount.reference)
            }
        }
    }
    
    boolean isBankAccountPreselectionEnabled() {
        env.bankAccountPreselection
    }
    
    void enableBankAccountPreselection() {
        env.bankAccountPreselection = true
    }
    
    void disableBankAccountPreselection() {
        env.bankAccountPreselection = false
    }
}
