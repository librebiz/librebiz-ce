/**
 *
 * Copyright (C) 2009, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.service.projects;

import java.util.Date;
import java.util.List;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.OptionsCache;
import com.osserp.common.Status;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.BusinessCase;
import com.osserp.core.dao.ProjectTrackings;
import com.osserp.core.dao.ProjectTrackingDocuments;
import com.osserp.core.projects.ProjectTracking;
import com.osserp.core.projects.ProjectTrackingManager;
import com.osserp.core.projects.ProjectTrackingRecord;
import com.osserp.core.projects.ProjectTrackingRecordType;
import com.osserp.core.projects.ProjectTrackingType;
import com.osserp.core.sales.Sales;
import com.osserp.core.service.impl.AbstractCoreService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectTrackingManagerImpl extends AbstractCoreService implements ProjectTrackingManager {

    private ProjectTrackings projectTrackings;
    private ProjectTrackingDocuments projectTrackingDocuments;
    private ProjectTrackingOutputService projectTrackingOutputService;

    /**
     * Creates a new tracking manager
     * @param optionsCache
     * @param projectTrackings
     * @param projectTrackingDocuments
     * @param projectTrackingOutputService
     */
    protected ProjectTrackingManagerImpl(
            OptionsCache optionsCache, 
            ProjectTrackings projectTrackings, 
            ProjectTrackingDocuments projectTrackingDocuments,
            ProjectTrackingOutputService projectTrackingOutputService) {
        super(optionsCache);
        this.projectTrackings = projectTrackings;
        this.projectTrackingDocuments = projectTrackingDocuments;
        this.projectTrackingOutputService = projectTrackingOutputService;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.projects.ProjectTrackingManager#create(com.osserp.core.users.DomainUser, com.osserp.core.BusinessCase, com.osserp.core.projects.ProjectTrackingType, java.lang.String, java.util.Date, java.util.Date)
     */
    public ProjectTracking create(DomainUser user, BusinessCase businessCase, ProjectTrackingType type, String name, Date startDate, Date endDate) {
        checkRequiredUser(user);
        Status initialSatus = getBusinessStatus(Status.CREATED);
        return projectTrackings.create(initialSatus, user.getEmployee(), businessCase, type, name, startDate, endDate);
    }
    
    /* (non-Javadoc)
     * @see com.osserp.core.projects.ProjectTrackingManager#getTrackingCount(com.osserp.core.BusinessCase)
     */
    public int getTrackingCount(BusinessCase businessCase) {
        Long reference = (businessCase instanceof Sales) ? 
                ((Sales) businessCase).getRequest().getPrimaryKey() : 
                    businessCase.getPrimaryKey(); 
        return projectTrackings.getTrackingCount(reference);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.projects.ProjectTrackingManager#find(com.osserp.core.BusinessCase)
     */
    public ProjectTracking findLatest(BusinessCase businessCase) {
        if (businessCase == null) {
            throw new ActionException("businessCase missing");
        }
        return projectTrackings.find(businessCase);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.projects.ProjectTrackingManager#findAll(com.osserp.core.BusinessCase)
     */
    public List<ProjectTracking> findAll(BusinessCase businessCase) {
        return projectTrackings.findAll(businessCase);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.projects.ProjectTrackingManager#update(com.osserp.core.users.DomainUser, com.osserp.core.projects.ProjectTracking, com.osserp.core.projects.ProjectTrackingType, java.lang.String, java.util.Date, java.util.Date)
     */
    public ProjectTracking update(DomainUser user, ProjectTracking tracking, ProjectTrackingType type, String name, Date startDate, Date endDate) throws ClientException {
        checkRequiredUser(user);
        tracking.update(user.getId(), type, name, startDate, endDate);
        projectTrackings.save(tracking);
        return projectTrackings.getTracking(tracking.getId());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.projects.ProjectTrackingManager#updateTrackingData(com.osserp.core.users.DomainUser, com.osserp.core.BusinessCase, com.osserp.core.projects.ProjectTracking, com.osserp.core.projects.ProjectTrackingRecord, java.lang.String, java.lang.String, java.util.Date, java.util.Date, java.lang.Double)
     */
    public ProjectTracking updateTrackingData(
            DomainUser user, 
            ProjectTracking tracking, 
            ProjectTrackingRecord record, 
            ProjectTrackingRecordType recordType, 
            String name, 
            String description,
            String internalNote,
            Date startDate, 
            Date endDate, 
            Double time) 
        throws ClientException {
        checkRequiredUser(user);
        assert (tracking != null);
        if (record != null) {
            tracking.updateRecord(user.getId(), record, recordType, name, description, internalNote, startDate, endDate, time);
        } else {
            tracking.addRecord(user.getId(), recordType, name, description, internalNote, startDate, endDate, time);
        }
        projectTrackings.save(tracking);
        return projectTrackings.getTracking(tracking.getId());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.projects.ProjectTrackingManager#deleteTrackingData(com.osserp.core.users.DomainUser, com.osserp.core.projects.ProjectTracking, java.lang.Long)
     */
    public ProjectTracking deleteTrackingData(DomainUser user, ProjectTracking tracking, Long recordId) {
        checkRequiredUser(user);
        tracking.removeRecord(recordId);
        tracking.setChanged(new Date(System.currentTimeMillis()));
        tracking.setChangedBy(user.getId());
        projectTrackings.save(tracking);
        return projectTrackings.getTracking(tracking.getId());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.projects.ProjectTrackingManager#close(com.osserp.core.users.DomainUser, com.osserp.core.projects.ProjectTracking)
     */
    public void close(DomainUser user, ProjectTracking tracking) throws ClientException {
        Status status = getBusinessStatus(Status.RELEASED);
        if (status != null) {
            DocumentData output = createPdf(user, tracking);
            if (output != null) {
                tracking.update(user.getId(), status);
                projectTrackings.save(tracking);
                projectTrackingDocuments.create(user, tracking, output.getBytes());
            }
        }
    }

    /* (non-Javadoc)
     * @see com.osserp.core.projects.ProjectTrackingManager#toggleIgnore(com.osserp.core.users.DomainUser, com.osserp.core.projects.ProjectTracking, java.lang.Long)
     */
    public ProjectTracking toggleIgnore(DomainUser user, ProjectTracking tracking, Long id) throws ClientException {
        tracking.toggleIgnore(user.getId(), id);
        projectTrackings.save(tracking);
        return projectTrackings.getTracking(tracking.getId());
    }

    /* (non-Javadoc)
     * @see com.osserp.core.projects.ProjectTrackingManager#getPdf(com.osserp.core.users.DomainUser, com.osserp.core.projects.ProjectTracking)
     */
    public DocumentData getPdf(DomainUser user, ProjectTracking tracking) {
        checkRequiredUser(user);
        assert (tracking != null);
        
        if (tracking.isClosed()) {
            return fetchPdf(tracking);
        }
        DocumentData pdf = createPdf(user, tracking);
        if (tracking.getStatus() == null || tracking.getStatus().getId() < Status.PRINT) {
            tracking.update(user.getId(), getBusinessStatus(Status.PRINT));
            projectTrackings.save(tracking);
        }
        return pdf;
    }

    private DocumentData fetchPdf(ProjectTracking tracking) {
        List<DmsDocument> list = projectTrackingDocuments.findByReference(new DmsReference(
                projectTrackingDocuments.getDocumentType().getId(), tracking.getId()));
        DmsDocument doc = (list.isEmpty() ? null : list.get(0));
        return (doc == null ? null : new DocumentData(
                projectTrackingDocuments.getDocumentData(doc), 
                doc.getRealFileName(),
                Constants.MIME_TYPE_PDF));
    }
    
    /**
     * Creates the tracking pdf
     * @param user
     * @param tracking
     * @return documentData
     */
    protected final DocumentData createPdf(DomainUser user, ProjectTracking tracking) {
        return projectTrackingOutputService.createPdf(user, tracking);
    }

}
