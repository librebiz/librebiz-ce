<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="customerSalesRecordsView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="headline">
		<o:listSize value="${view.list}" /> <fmt:message key="recordsFor" /> <o:out value="${view.customer.displayName}" />:
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="customerSalesRecordsContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
    				<c:import url="_customerSalesRecordList.jsp"/>
                </div>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
