<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="list" value="${requestScope.selectList}" />
<c:set var="url" value="${requestScope.selectUrl}" />
<c:choose>
    <c:when test="${!empty requestScope.selectMethod}">
        <c:set var="method" value="${requestScope.selectMethod}" />
    </c:when>
    <c:otherwise>
        <c:set var="method" value="select" />
    </c:otherwise>
</c:choose>

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><fmt:message key="companySlashName" /></th>
                <th><fmt:message key="street" /></th>
                <th><fmt:message key="city" /></th>
                <th class="center" colspan="2"> </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="contact" items="${list}" varStatus="s">
                <tr>
                    <td>
                        <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${contact.id}">
                            <o:out value="${contact.displayName}" />
                        </v:ajaxLink>
                    </td>
                    <td><o:out value="${contact.address.street}" /></td>
                    <td><o:out value="${contact.address.zipcode}" /> <o:out value="${contact.address.city}" /></td>
                    <td class="icon center">
                        <o:ajaxExecute url="${url}?method=${method}&id=${contact.id}" linkId="foobar"><o:img name="enabledIcon"/></o:ajaxExecute>
                    </td>
                    <td class="icon center">  </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
