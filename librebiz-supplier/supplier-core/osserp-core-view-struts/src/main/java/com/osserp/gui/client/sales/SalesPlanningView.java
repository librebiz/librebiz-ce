/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 24-Sep-2006 08:42:31 
 * 
 */
package com.osserp.gui.client.sales;

import java.util.Date;
import java.util.List;

import com.osserp.core.planning.SalesPlanningSummary;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductTypeConfig;
import com.osserp.core.views.OrderItemsDisplay;

import com.osserp.gui.client.products.StockSelectingView;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 */
public interface SalesPlanningView extends StockSelectingView {

    /**
     * Provides the planning horizon
     * @return horizon
     */
    public Date getHorizon();

    /**
     * Indicates that view lists unreleased orders
     * @return true if so
     */
    public boolean isListUnreleased();

    /**
     * Indicates that view lists only open orders of an product
     * @return true if so
     */
    public boolean isProductMode();

    /**
     * Provides the product if view is in product mode
     * @return product
     */
    public Product getProduct();

    /**
     * Indicates that view is in summary mode
     * @return true if so
     */
    public boolean isSummaryMode();

    /**
     * Enables/disables summary mode, depending on current mode
     */
    public void changeSummaryMode();

    /**
     * Indicates that view should display summary of a selected product only
     * @return productSummaryMode
     */
    public boolean isProductSummaryMode();

    /**
     * Provides the product id to display when in product summary mode
     * @return productSummaryDisplayId
     */
    public Long getProductSummaryDisplayId();

    /**
     * Provides the name of the product when in product summary mode
     * @return productName
     */
    public String getProductSummaryDisplayName();

    /**
     * Enables/disbales product summary mode
     * @param productId or null to disable
     */
    public void setProductSummaryMode(Long productId);

    /**
     * Provides types for type selection
     * @return productTypes
     */
    public List<ProductTypeConfig> getProductTypes();

    /**
     * Provides stock aware groups for group selection
     * @return productGroups
     */
    public List<ProductGroupConfig> getProductGroups();

    /**
     * Provides categories for categories selection
     * @return productCategories
     */
    public List<ProductCategoryConfig> getProductCategories();

    /**
     * Provides current selected product type
     * @return selectedProductType, null if all types selected
     */
    public Long getSelectedProductType();

    /**
     * Provides current selected product group
     * @return selectedProductGroup, null if all groups selected
     */
    public Long getSelectedProductGroup();

    /**
     * Provides current selected product category
     * @return selectedProductCategory, null if all categories selected
     */
    public Long getSelectedProductCategory();

    /**
     * Selects an product category to limit display to products of that category
     * @param id
     */
    public void selectProductCategory(Long id);

    /**
     * Selects an product group to limit display to products of that group
     * @param id
     */
    public void selectProductGroup(Long id);

    /**
     * Selects an product type to limit display to products of that type
     * @param id
     */
    public void selectProductType(Long id);

    /**
     * Indicates if only orders with open purchase orders should be displayed
     * @return true if so
     */
    public boolean isOpenPurchaseOnly();

    /**
     * Enables/disables open purchase order mode
     */
    public void togglePurchaseOrderedOnly();

    /**
     * Indicates if only confirmed purchase orders should be displayed
     * @return true if so
     */
    public boolean isConfirmedPurchaseOnly();

    /**
     * Enables/disables confirmed purchase order only mode
     */
    public void toggleConfirmedPurchaseOrderOnly();

    /**
     * Sets the planning horizon to disable the default setting of unlimited planning
     * @param horizon or null to enable unlimited planning
     */
    public void setPlanningHorizon(Date horizon);

    /**
     * Provides the product summary over all open orders
     * @return summary
     */
    public List<SalesPlanningSummary> getSummary();

    /**
     * Indicates that view is in vacancy display mode
     * @return vacancyDisplayMode
     */
    public boolean isVacancyDisplayMode();

    /**
     * Toggles between vacancy display enabled/disabled state
     */
    public void toggleVacancyDisplay();

    /**
     * Indicates that view was created to display summary only. An action could use this to force view removement and direct forwarding to an exit target
     * instead of displaying planning overview
     * @return displaySummaryOnly
     */
    public boolean isDisplaySummaryAndExit();

    /**
     * Enables summary and exit mode
     * @param displaySummaryAndExit
     */
    public void enableSummaryAndExit(Long productId, String exitTarget);

    /**
     * Fetches an item
     * @param id of the item
     * @return item or null if none exists
     */
    public OrderItemsDisplay fetchItem(Long id);

    /**
     * Synchronizes current list with backend values
     */
    public void refresh();
}
