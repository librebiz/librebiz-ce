/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 2, 2006 8:53:23 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SelectOptions {

    /**
     * Provides a list of all options available under specified table
     * @param tableKey representing a table
     * @return list of all options.
     */
    List<? extends Option> getList(String tableKey);

    /**
     * Adds the given value to the option type specified by given table name
     * @param tableKey of table where we add the new value
     * @param value to add
     * @throws ClientException if value is null or already existing
     */
    int add(String tableKey, String value) throws ClientException;

    /**
     * Renames the value under the given id of the option type specified by given table name
     * @param tableKey of table where we rename a value
     * @param id of the value to rename
     * @param value to add
     * @throws ClientException if value is null or already existing
     */
    int rename(String tableKey, Long id, String value) throws ClientException;

    /**
     * Toggles the eol flag of an options list
     * @param tableKey
     * @param id
     * @return number of rows affected, should be 1 on success
     */
    int toggleEol(String tableKey, Long id);

}
