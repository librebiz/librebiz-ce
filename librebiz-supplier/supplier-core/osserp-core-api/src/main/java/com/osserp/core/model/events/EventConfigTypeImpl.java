/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Oct-2005 13:51:55 
 * 
 */
package com.osserp.core.model.events;

import com.osserp.common.beans.OptionImpl;
import com.osserp.core.events.EventConfigType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventConfigTypeImpl extends OptionImpl implements EventConfigType {
    private static final long serialVersionUID = 42L;

    private Long recipientType = null;
    private String sequenceName = null;
    private boolean appointment = false;
    private boolean flowControl = false;
    private boolean flowControlByBusinessType = false;
    private String flowControlActionsName = null;
    private String flowControlActionManager = null;
    private String flowControlActionTable = null;
    private String flowControlStartActionView = null;

    private boolean information = false;
    private boolean task = false;
    private boolean configurable = true;
    private String actionLink = null;
    private Long infoActionId;

    public EventConfigTypeImpl() {
        super();
    }

    public EventConfigTypeImpl(Long id, String name, Long recipientType) {
        super(id, name);
        this.recipientType = recipientType;
    }

    protected EventConfigTypeImpl(EventConfigType o) {
        super();
        recipientType = o.getRecipientType();
        sequenceName = o.getSequenceName();
        appointment = o.isAppointment();
        flowControl = o.isFlowControl();
        flowControlByBusinessType = o.isFlowControlByBusinessType();
        flowControlActionsName = o.getFlowControlActionsName();
        flowControlActionManager = o.getFlowControlActionManager();
        flowControlActionTable = o.getFlowControlActionTable();
        flowControlStartActionView = o.getFlowControlStartActionView();
        information = o.isInformation();
        task = o.isTask();
        configurable = o.isConfigurable();
        actionLink = o.getActionLink();
        infoActionId = o.getInfoActionId();
    }

    public Long getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(Long recipientType) {
        this.recipientType = recipientType;
    }

    public String getSequenceName() {
        return sequenceName;
    }

    public void setSequenceName(String sequenceName) {
        this.sequenceName = sequenceName;
    }

    public boolean isAppointment() {
        return appointment;
    }

    protected void setAppointment(boolean appointment) {
        this.appointment = appointment;
    }

    public boolean isFlowControl() {
        return flowControl;
    }

    protected void setFlowControl(boolean flowControl) {
        this.flowControl = flowControl;
    }

    public boolean isFlowControlByBusinessType() {
        return flowControlByBusinessType;
    }

    protected void setFlowControlByBusinessType(boolean flowControlByBusinessType) {
        this.flowControlByBusinessType = flowControlByBusinessType;
    }

    public String getFlowControlActionsName() {
        return flowControlActionsName;
    }

    protected void setFlowControlActionsName(String flowControlActionsName) {
        this.flowControlActionsName = flowControlActionsName;
    }

    public String getFlowControlActionManager() {
        return flowControlActionManager;
    }

    protected void setFlowControlActionManager(String flowControlActionManager) {
        this.flowControlActionManager = flowControlActionManager;
    }

    public String getFlowControlActionTable() {
        return flowControlActionTable;
    }

    protected void setFlowControlActionTable(String flowControlActionTable) {
        this.flowControlActionTable = flowControlActionTable;
    }

    public String getFlowControlStartActionView() {
        return flowControlStartActionView;
    }

    protected void setFlowControlStartActionView(String flowControlStartActionView) {
        this.flowControlStartActionView = flowControlStartActionView;
    }

    public boolean isInformation() {
        return information;
    }

    protected void setInformation(boolean information) {
        this.information = information;
    }

    public boolean isTask() {
        return task;
    }

    protected void setTask(boolean task) {
        this.task = task;
    }

    public boolean isConfigurable() {
        return configurable;
    }

    protected void setConfigurable(boolean configurable) {
        this.configurable = configurable;
    }

    public String getActionLink() {
        return actionLink;
    }

    public void setActionLink(String actionLink) {
        this.actionLink = actionLink;
    }

    public Long getInfoActionId() {
        return infoActionId;
    }

    public void setInfoActionId(Long infoActionId) {
        this.infoActionId = infoActionId;
    }

    @Override
    public Object clone() {
        return new EventConfigTypeImpl(this);
    }
}
