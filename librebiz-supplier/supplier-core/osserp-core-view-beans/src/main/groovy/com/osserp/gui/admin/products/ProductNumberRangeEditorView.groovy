/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 6, 2009 
 * 
 */
package com.osserp.gui.admin.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.products.ProductNumberRange
import com.osserp.core.products.ProductNumberRangeManager
import com.osserp.core.employees.Employee
import com.osserp.gui.CoreView



class ProductNumberRangeEditorView extends CoreView {

    private static Logger logger = LoggerFactory.getLogger(ProductNumberRangeEditorView.class.getName())

    String targetUrl
    Long currentRange

    ProductNumberRangeEditorView() {
        super()
        providesCreateMode()
        providesEditMode()
        enablePopupView()
        headerName = 'numberRangeConfig'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            list = productNumberRangeManager.getNumberRanges()
            targetUrl = form.getString('target')
            currentRange = form.getLong('range')
            logger.debug("initRequest: done loading ranges [count=${list.size()}, targetAvailable=${(targetUrl != null)}]")
        }
    }

    @Override
    void disableEditMode() {
        super.disableEditMode()
        bean = null
    }

    @Override
    void select() {
        super.select()
        enableEditMode()
    }

    @Override
    void save() {
        String name = form.getString('name')
        Long start = form.getLong('rangeStart')
        Long end = form.getLong('rangeEnd')
        if (createMode) {

            productNumberRangeManager.createNumberRange(domainEmployee, name, start, end)
            disableCreateMode()
        } else {

            if (bean) {
                productNumberRangeManager.update(domainEmployee, bean, name, start, end)
                bean = null // display list after update
            }
            disableEditMode()
        }
        list = productNumberRangeManager.getNumberRanges()
    }

    protected ProductNumberRangeManager getProductNumberRangeManager() {
        getService(ProductNumberRangeManager.class.getName())
    }
}
