/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 20:31:31 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.CommonPayment;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CommonPaymentImpl extends AbstractPayment implements CommonPayment {

    private boolean templateRecord = false;
    private boolean templateOnly = false;
    private String templateName = null;
    
    /**
     * Default constructor
     */
    protected CommonPaymentImpl() {
        super();
    }

    /**
     * Creates a common payment (e.g. not associated to a sales or purchase record)
     * @param user
     * @param contact
     * @param company
     * @param branch
     * @param bankAccountId
     * @param currency
     * @param billingType
     * @param recordCreated
     * @param paid the date the amount was paid or null if outstanding payment.
     * @param amount the amount paid or the amount to pay if date paid is not provided.
     * @param note
     * @param reducedTax
     * @param taxFree
     * @param taxFreeId
     * @param taxRate
     * @param template marks record as template 
     * @param templateName
     */
    public CommonPaymentImpl(
        Employee user, 
        ClassifiedContact contact, 
        Long company, 
        Long branch,
        Long bankAccountId,
        Long currency, 
        BillingType billingType,
        Date recordCreated,
        Date paid,
        BigDecimal amount,
        String note,
        boolean reducedTax,
        boolean taxFree, 
        Long taxFreeId,
        Double taxRate,
        boolean template,
        String templateName) {
        super(
            user, 
            contact, 
            company, 
            branch,
            bankAccountId,
            currency, 
            billingType, 
            (recordCreated != null ? recordCreated :paid), 
            paid, 
            amount, 
            note,
            reducedTax,
            taxFree, 
            taxFreeId,
            taxRate);
        
        if (template && isSet(templateName)) {
            enableTemplate(templateName);
        }
    }
    
    /**
     * Updates common payment data if provided
     * @param user
     * @param billingType
     * @param currency
     * @param amount
     * @param recordDate
     * @param paid
     * @param note
     * @param reducedTax
     * @param taxFree
     * @param taxFreeId
     * @param customHeader
     * @param template
     * @param templateName
     */
    public void update(
            Employee user, 
            BillingType billingType,
            Long currency,
            BigDecimal amount,
            Date recordDate,
            Date paid,
            String note,
            boolean reducedTax,
            boolean taxFree, 
            Long taxFreeId,
            String customHeader,
            boolean template,
            String templateName) {
        
        super.update(
                user, 
                billingType, 
                currency, 
                amount, 
                recordDate, 
                paid, 
                note, 
                reducedTax, 
                taxFree, 
                taxFreeId,
                customHeader);
        
        if (template && isSet(templateName)) {
            enableTemplate(templateName);
        } else {
            disableTemplate();
        }
    }

    public boolean isTemplateRecord() {
        return templateRecord;
    }

    public void setTemplateRecord(boolean templateRecord) {
        this.templateRecord = templateRecord;
    }

    public boolean isTemplateOnly() {
        return templateOnly;
    }

    protected void setTemplateOnly(boolean templateOnly) {
        this.templateOnly = templateOnly;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void enableTemplate(String name) {
        if (isSet(name)) {
            templateRecord = true;
            templateName = name;
        }
    }

    public void disableTemplate() {
        if (!templateOnly) {
            templateRecord = false;
            templateName = null;
        }
    }
}
