<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<o:ajaxLookupForm name="contactSearchForm" url="${view.appLink}/save" targetElement="ajaxContent" minChars="0" events="onkeyup" activityElement="value">
    <div class="form-body">

        <div class="row">

            <div class="col-md-2">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input class="form-control" type="text" name="value" id="value" value="<o:out value="${view.selectedValue}"/>" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="javascript: $('contactSearchForm').onkeyup();"> <o:img name="replaceIcon" styleClass="bigicon" style="padding-top: 5px;" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <select class="form-control" name="group" onChange="gotoUrl(this.value);"<c:if test="${!view.groupSelectionEnabled}"> disabled="disabled"</c:if>>
                        <c:forEach var="group" items="${view.groups}">
                            <option value="<c:url value="${view.appLink}/selectGroup?group=${group}"/>" <c:if test="${view.selectedGroup == group}"> selected="selected"</c:if>>
                                <fmt:message key="contactSearch.${group}" />
                            </option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <select class="form-control" name="method" size="1" id="method" onchange="javascript: $('contactSearchForm').onkeyup();">
                        <c:forEach var="column" items="${view.columns}">
                            <option value="<o:out value="${column}"/>" <c:if test="${view.selectedColumn == column}"> selected="selected"</c:if>><fmt:message key="contactSearch.${column}" /></option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <c:choose>
                <c:when test="${view.customerSearch}">
                    <div class="col-md-2">
                        <div class="form-group">
                            <select class="form-control" name="customerType" size="1" id="customerType" onchange="javascript: $('contactSearchForm').onkeyup();">
                                <c:choose>
                                    <c:when test="${view.selectedCustomerType == null}">
                                        <option value="" selected="selected"><fmt:message key="customerType"/>...</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value=""><fmt:message key="customerType"/>...</option>
                                    </c:otherwise>
                                </c:choose>
                                <c:forEach var="type" items="${view.customerTypes}">
                                    <c:if test="${!type.endOfLife}">
                                        <option value="<o:out value="${type.id}"/>"<c:if test="${view.selectedCustomerType == type.id}"> selected="selected"</c:if>><o:out value="${type.name}"/></option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <select class="form-control" name="customerStatus" size="1" id="customerStatus" onchange="javascript: $('contactSearchForm').onkeyup();">
                                <c:choose>
                                    <c:when test="${view.selectedCustomerStatus == null}">
                                        <option value="" selected="selected"><fmt:message key="customerStatus"/>...</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value=""><fmt:message key="customerStatus"/>...</option>
                                    </c:otherwise>
                                </c:choose>
                                <c:forEach var="status" items="${view.customerStatus}">
                                    <c:if test="${!status.endOfLife}">
                                        <option value="<o:out value="${status.id}"/>"<c:if test="${view.selectedCustomerStatus == status.id}"> selected="selected"</c:if>><o:out value="${status.name}"/></option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="checkbox">
                                <label> <v:checkbox name="startsWith" value="${view.startsWith}" onchange="javascript: $('contactSearchForm').onkeyup();" /> <fmt:message key="beginningWith" />
                                </label>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-2">
                        <div class="form-group">
                            <select class="form-control" name="type" size="1" id="type" onchange="javascript: $('contactSearchForm').onkeyup();">
                                <c:forEach var="type" items="${view.contactTypes}">
                                    <option value="<o:out value="${type.id}"/>" <c:if test="${view.selectedType == type.id}"> selected="selected"</c:if>><o:out value="${type.name}" /></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="checkbox">
                                <label> <v:checkbox name="startsWith" value="${view.startsWith}" onchange="javascript: $('contactSearchForm').onkeyup();" /> <fmt:message key="beginningWith" />
                                </label>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>

        </div>
    </div>
</o:ajaxLookupForm>
<div id="ajaxDiv">
    <div id="ajaxContent">
        <c:if test="${!empty view.list}">
            <c:import url="${requestScope.resultPage}" />
        </c:if>
    </div>
</div>
