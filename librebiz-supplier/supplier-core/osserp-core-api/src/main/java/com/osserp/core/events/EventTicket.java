/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Oct-2005 20:25:20 
 * 
 */
package com.osserp.core.events;

import java.util.List;

import com.osserp.common.EntityRelation;
import com.osserp.common.Parameter;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EventTicket extends EntityRelation {

    /**
     * Provides the event action config
     * @return config
     */
    EventConfig getConfig();

    /**
     * Provides the id of the eventAction configType if config not available (termination-only tickets)
     * @return configTypeId
     */
    Long getConfigTypeId();

    /**
     * Provides the caller id if ticket is termination only
     * @return callerId
     */
    Long getCallerId();

    String getDescription();

    String getMessage();

    void setMessage(String message);

    String getHeadline();

    void setHeadline(String headline);

    List<Parameter> getParameters();

    void addParameter(String name, String value);

    Long[] getRecipients();

    void addRecipient(Long recipient);

    boolean isCanceled();

    boolean isSent();

    void setSent(boolean sent);

    boolean isClosed();

    void close();

}
