<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactPersonView" value="${sessionScope.contactPersonView}" />
<c:set var="contact" value="${contactPersonView.bean}" />

<table class="valueTable first33">
    <tbody>
        <tr>
            <td><fmt:message key="position" /></td>
            <td>
                <c:choose>
                    <c:when test="${!empty contact.position}"><o:out value="${contact.position}" /></c:when>
                    <c:otherwise><fmt:message key="notSpecifiedLabel" /></c:otherwise>
                </c:choose>
            </td>
        </tr>
        <tr>
            <td><fmt:message key="office" /></td>
            <td>
                <c:choose>
                    <c:when test="${!empty contact.office}"><o:out value="${contact.office}" /></c:when>
                    <c:otherwise><fmt:message key="notSpecifiedLabel" /></c:otherwise>
                </c:choose>
            </td>
        </tr>
        <tr>
            <td><fmt:message key="section" /></td>
            <td>
                <c:choose>
                    <c:when test="${!empty contact.section}"><o:out value="${contact.section}" /></c:when>
                    <c:otherwise><fmt:message key="notSpecifiedLabel" /></c:otherwise>
                </c:choose>
            </td>
        </tr>
        <c:if test="${!empty contact.address.street and !empty contact.address.zipcode and !empty contact.address.city}">
            <tr>
                <td><fmt:message key="address" /></td>
                <td><o:out value="${contact.address.street}" /><br /> <o:out value="${contact.address.zipcode}" /> <o:out value="${contact.address.city}" /></td>
            </tr>
            <c:if test="${contactPersonView.customFederalState and !empty contact.address.federalStateName}">
                <tr>
                    <td></td>
                    <td><o:out value="${contact.address.federalStateName}" /></td>
                </tr>
            </c:if>
            <c:if test="${!contactPersonView.customFederalState and contact.address.federalStateId != 0 and !empty contact.address.federalStateId}">
                <tr>
                    <td><fmt:message key="federalState" /></td>
                    <td><oc:options name="federalStates" value="${contact.address.federalStateId}" /></td>
                </tr>
            </c:if>
            <tr>
                <td><fmt:message key="country" /></td>
                <td><oc:options name="countryNames" value="${contact.address.country}" /></td>
            </tr>
        </c:if>
        <tr>
            <td><fmt:message key="newsletter" /></td>
            <td>
                <c:choose>
                    <c:when test="${contact.newsletterConfirmation}"><fmt:message key="bigYes" /></c:when>
                    <c:otherwise><fmt:message key="bigNo" /></c:otherwise>
                </c:choose> 
                <c:if test="${contact.vip}">
                    <span style="margin-left: 20px;"><fmt:message key="vip" /></span>
                </c:if> 
            </td>
        </tr>
    </tbody>
</table>
