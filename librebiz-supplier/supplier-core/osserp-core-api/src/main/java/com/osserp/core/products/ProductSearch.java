/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 16, 2009 6:21:01 PM 
 * 
 */
package com.osserp.core.products;

import java.util.List;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
public interface ProductSearch {

    /**
     * Returns the product value object specified by primary key id
     * @param primary key id
     * @return product
     */
    Product find(Long id);

    /**
     * Finds all active products available by an optional selection
     * @param searchRequest
     * @param userSelection
     * @return products
     */
    List<Product> find(ProductSearchRequest searchRequest, ProductSelection userSelection);

    /**
     * Finds all products matching a selection
     * @param searchRequest
     * @param selection
     * @return products
     */
    List<Product> find(ProductSearchRequest searchRequest, ProductSelectionConfigItem selection);

    /**
     * Finds all products related to the given selection config, columnKey, value, startsWith and includeEol
     * @param selectionConfig
     * @param includeEol
     * @return products
     */
    List<Product> find(ProductSearchRequest searchRequest, ProductSelectionConfig selectionConfig);

    /**
     * Finds all products related to the given selection config, columnKey, value, startsWith and includeEol
     * @param selectionConfigs
     * @param includeEol
     * @return products
     */
    List<Product> find(ProductSearchRequest searchRequest, List<ProductSelectionConfig> selectionConfigs);

    /**
     * Provides available configurations
     * @return list
     */
    List<ProductClassificationConfig> findClassifications();

    /**
     * Provides all products whose quantity unit is interpreted as time value.
     * @return time aware products
     */
    List<Product> findTimeAware();

}
