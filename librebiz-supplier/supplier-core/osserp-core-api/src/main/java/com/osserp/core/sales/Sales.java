/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 30, 2006 11:51:14 AM 
 * 
 */
package com.osserp.core.sales;

import java.util.Date;

import com.osserp.common.Entity;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.requests.Request;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Sales extends BusinessCase, Entity {

    static final Long STATUS_BILLED = 95L;
    static final Long STATUS_CONFIRMED = 15L;
    static final Long STATUS_DELIVERY_CLOSED = 90L;
    
    static final Long DOCUMENTS = 14L;
    static final Long PICTURES = 13L;

    /**
     * The type of the sales
     * @return type
     */
    BusinessType getType();

    /**
     * The request for sales
     * @return request
     */
    Request getRequest();

    /**
     * The parent id if sales is part of a big sale
     * @return parentId
     */
    Long getParentId();

    /**
     * Sets the parent id
     * @param parentId
     */
    void setParentId(Long parentId);

    /**
     * Sets the project manager id
     * @param managerId
     */
    void setManagerId(Long managerId);

    /**
     * The project manager substitute id
     * @return managerSubstituteId
     */
    Long getManagerSubstituteId();

    /**
     * Sets the project manager substitute id
     * @param managerSubstituteId
     */
    void setManagerSubstituteId(Long managerSubstituteId);

    /**
     * The project observer id
     * @return observerId
     */
    Long getObserverId();

    /**
     * Sets the project observer id
     * @param observerId
     */
    void setObserverId(Long observerId);

    /**
     * Current project status
     * @return status
     */
    Long getStatus();

    /**
     * Sets a new status, use with care!
     * @param status
     */
    void setStatus(Long status);

    /**
     * Indicates that this sale is changeable e.g. not closed and not cancelled
     * @return changeable
     */
    boolean isChangeable();

    /**
     * Indicates that sales is released
     * @return released
     */
    boolean isReleased();

    /**
     * Indicates that sales is closed
     * @return closed
     */
    boolean isClosed();

    /**
     * Project closed status
     * @return closedStatus
     */
    SalesClosedStatus getClosedStatus();

    /**
     * Closed status note
     * @return closedStatusNote
     */
    String getClosedStatusNote();

    /**
     * Closes sales
     * @param closedStatus
     * @param closedStatusNote
     */
    void close(SalesClosedStatus closedStatus, String closedStatusNote);

    /**
     * Provides the capacity
     * @return capacity
     */
    Double getCapacity();

    /**
     * Sets the capacity
     * @param capacity
     */
    void setCapacity(Double capacity);

    /**
     * Indicates that sales is a reference sales
     * @return reference
     */
    boolean isReference();

    /**
     * Changes sales reference status
     * @param status
     * @param note
     * @param setBy
     */
    void changeReference(boolean status, String note, Long setBy);

    /**
     * Provides a note wrote when setting reference status
     * @return reference note
     */
    String getReferenceNote();

    /**
     * Indicates employee who sets the reference status
     * @return reference from
     */
    Long getReferenceFrom();

    /**
     * The date when reference status was last modified
     * @return referenceDate
     */
    Date getReferenceDate();

    /**
     * Indicates that sales data should be exported to internet website
     * @return internetExportEnabled
     */
    boolean isInternetExportEnabled();

    /**
     * Changes internet export status flag
     */
    void changeInternetExportStatus();

    /**
     * Indicates that monitoring plant available
     * @return plantAvailable
     */
    boolean isPlantAvailable();

    /**
     * Sets that plant is available
     * @param plantAvailable
     */
    void setPlantAvailable(boolean plantAvailable);

    /**
     * Indicates that delivery is closed for this sales
     * @return deliveryClosed
     */
    boolean isDeliveryClosed();

    /**
     * Resets a previous cancellation by removing cancelling action and 
     * resetting status to last status before cancelled.
     * @param employeeId
     * @return deleted note added to cancellation fcs
     */
    String resetCancellation(Long employeeId);

    /**
     * Removes closing fcs action and reopens order for changes
     * @param employeeId
     * @return reopen note added to cancellation fcs
     */
    String resetClosed(Long employeeId);
}
