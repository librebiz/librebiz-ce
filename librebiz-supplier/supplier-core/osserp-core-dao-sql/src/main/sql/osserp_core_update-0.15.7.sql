SET search_path = osserp, pg_catalog;

DROP VIEW v_products;

CREATE VIEW v_product_type_relations AS
  SELECT
    p.product_id,
    tr.type_id,
    t.name as type_name
  FROM products p, product_types_relations tr, product_types t
  WHERE p.product_id = tr.product_id AND tr.type_id = t.id;

CREATE VIEW v_products AS
  SELECT
    p.product_id,
    p.name,
    p.description,
    p.matchcode,
    p.manufacturer_id,
    m.name AS manufacturer,
    t.type_id,
    t.type_name,
    p.group_id,
    g.name AS group_name,
    p.category_id,
    c.name AS category_name,
    p.detail_height AS height,
    p.detail_width AS width,
    p.detail_depth AS depth,
    p.detail_dunit AS dunit,
    p.detail_weight AS weight,
    p.detail_wunit AS wunit,
    p.detail_color AS color_id,
    col.name AS color_name,
    p.quantity_unit AS qunit_id,
    q.name AS qunit_name,
    p.power,
    p.power_unit AS punit_id,
    pu.name AS punit_name,
    p.estimated_purchase_price AS ep,
    p.consumer_price AS cp,
    p.reseller_price AS rp,
    p.partner_price AS pp,
    p.is_bundle,
    p.is_kanban,
    p.is_nullable,
    p.is_plant,
    p.quantity_by_plant,
    p.has_stock,
    p.default_stock AS stock_id,
    s.shortkey AS stock_name,
    p.end_of_life,
    p.gtin8,
    p.gtin13,
    p.gtin14
  FROM osserp.products p
  LEFT JOIN osserp.v_product_type_relations t ON (p.product_id = t.product_id)
  LEFT JOIN osserp.product_manufacturers m ON (p.manufacturer_id = m.id)
  LEFT JOIN osserp.product_groups g ON (p.group_id = g.id)
  LEFT JOIN osserp.product_categories c ON (p.category_id = c.id)
  LEFT JOIN osserp.product_colors col ON (p.detail_color = col.id)
  LEFT JOIN osserp.product_quantity_units q ON (p.quantity_unit = q.id)
  LEFT JOIN osserp.product_quantity_units pu ON (p.power_unit = pu.id)
  LEFT JOIN osserp.system_company_stocks s ON (p.default_stock = s.id);

CREATE VIEW v_product_classification_configs AS
  SELECT
    t.id            AS type_id,
    t.name          AS type_name,
    t.end_of_life   AS type_eol,
    g.id            AS group_id,
    g.name          AS group_name,
    g.end_of_life   AS group_eol,
    gcr.category_id AS category_id,
    c.name          AS category_name,
    CASE WHEN (c.end_of_life IS NULL) THEN false ELSE c.end_of_life END AS category_eol
  FROM product_types_groups_relation tgr
  LEFT JOIN product_types t ON (tgr.type_id = t.id)
  LEFT JOIN product_groups g ON (tgr.group_id = g.id)
  LEFT JOIN product_groups_categories_relation gcr ON (tgr.group_id = gcr.group_id)
  LEFT JOIN product_categories c ON (gcr.category_id = c.id)
  ORDER BY tgr.group_id;

INSERT INTO system_properties VALUES (225, 'productClassificationOrderByColumns', 'type_id, group_name', 'java.lang.String', true, false, null, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 225, true);

UPDATE system_menu_links SET link = '/products/productCreator/forward' WHERE id = 13;

INSERT INTO plugin_types (id, name, core_plugin) VALUES (5, 'caldav', false);
SELECT pg_catalog.setval('plugin_types_id_seq', 5, true);

INSERT INTO plugins (id, type_id, vendor, name, description, i18nkey, is_disabled, is_default) VALUES (5, 5, 'core', 'caldav','CalDAV Support','caldavPlugin', true, true);

CREATE VIEW v_product_export AS
  SELECT
    p.product_id,
    p.name,
    p.description,
    p.matchcode,
    p.manufacturer,
    p.type_name,
    p.group_name,
    p.category_name,
    p.height,
    p.width,
    p.depth,
    p.dunit,
    p.weight,
    p.wunit,
    p.color_name,
    p.qunit_name,
    p.power,
    p.punit_name,
    p.ep,
    p.cp,
    p.rp,
    p.pp,
    p.is_bundle AS bundle,
    p.is_kanban AS kanban,
    p.is_nullable AS zero,
    p.is_plant AS pkg,
    p.quantity_by_plant AS qty_by_pkg,
    p.end_of_life AS iseol,
    p.has_stock AS stock,
    p.stock_name,
    p.stock_id,
    p.gtin8,
    p.gtin13,
    p.gtin14
  FROM osserp.v_products p;

CREATE VIEW v_employee_export_de AS
 SELECT c.id AS "Mitarbeiter",
    c.lastname AS "Nachname",
    c.firstname AS "Vorname",
    c.street AS "Strasse",
    c.zipcode AS "PLZ",
    c.city AS "Ort",
    osserp.get_contact_email(c.contact_id) AS "E-Mail",
    osserp.get_contact_phone_display(c.contact_id) AS "Telefon",
    osserp.get_contact_mobile_display(c.contact_id) AS "Mobiltelefon",
    osserp.get_contact_fax_display(c.contact_id) AS "Fax"
   FROM osserp.v_employees c
  ORDER BY c.lastname;

ALTER TABLE record_documents ADD COLUMN header_name TEXT;
ALTER TABLE record_document_versions ADD COLUMN header_name TEXT;

UPDATE documents SET file_size = 9276 WHERE file_name = 'xsl/records/xsl_rtp_2.vm';
UPDATE documents SET file_size = 1744 WHERE file_name = 'xsl/records/shared/record_customer_address.vm';

UPDATE system_properties SET pvalue = '0.15.7', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
