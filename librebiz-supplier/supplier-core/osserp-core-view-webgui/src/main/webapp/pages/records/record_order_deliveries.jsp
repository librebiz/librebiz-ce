<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="openItems" value="${view.openDeliveries}"/>
<c:set var="record" value="${view.record}"/>
<c:set var="baseUrl" value="${requestScope.baseUrl}"/>
<c:if test="${!empty openItems}">
<table class="table table-striped">
	<tr>
		<th class="productNumber"><fmt:message key="product"/></th>
		<th class="productName"><fmt:message key="toDeliver"/></th>
		<th class="quantity" class="amount"><fmt:message key="quantity"/></th>
        <th class="quantityUnit" class="amount"><fmt:message key="quantityUnitShort"/></th>
		<c:choose>
		<c:when test="${view.stockEditMode}">
		<th class="recordStock">
			<a href="<c:url value="${baseUrl}"><c:param name="method" value="disableStockEditMode"/></c:url>" title="<fmt:message key="changeStockExit"/>">
				<o:img name="backIcon" styleClass="bigicon"/>
			</a>
		</th>
		</c:when>
		<c:otherwise>
		<th class="recordStock">
			<a href="<c:url value="${baseUrl}"><c:param name="method" value="enableStockEditMode"/></c:url>" title="<fmt:message key="changeStock"/>">
				<fmt:message key="stockShort"/>
			</a>
		</th>
		</c:otherwise>
		</c:choose>
        <th class="amount"><fmt:message key="stock"/></th>
        <th class="total"><fmt:message key="orders"/></th>
	</tr>
	<c:forEach var="item" items="${openItems}" varStatus="s">
	<c:set var="alternate" value="${((s.index % 2) > 0)}"/>
    <tr<c:choose><c:when test="${view.salesRecordView and !item.product.kanban and item.outOfStockCount > 0}"> class="red"</c:when><c:when test="${!alternate}"> class="altrow"</c:when></c:choose>>
    	<td class="productNumber">
            <c:choose>
                <c:when test="${!empty requestScope.productExitTarget}">
                    <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=${requestScope.productExitTarget}"/>" title="<fmt:message key="loadProduct"/>">
                        <o:out value="${item.product.productId}" />
                    </a>
                </c:when>
                <c:otherwise>
                    <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                        <o:out value="${item.product.productId}" />
                    </o:popupSelect>
                </c:otherwise>
            </c:choose>
		</td>
		<td class="productName">
            <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                <o:out value="${item.productName}" />
            </o:popupSelect>
        </td>
		<td class="quantity"><fmt:formatNumber value="${item.quantity}" minFractionDigits="3"/></td>
        <td class="quantityUnit"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
        <td class="recordStock">
            <c:choose>
            <c:when test="${view.stockEditMode}">
            <select name="selectAction" size="1" onChange="gotoUrl(this.value);">
            <c:forEach var="obj" items="${applicationScope.stockKeys}">
            <c:choose>
            <c:when test="${obj.id == item.stockId}">
                <option value="<c:url value="${baseUrl}"><c:param name="method" value="updateStock"/><c:param name="productId" value="${item.product.productId}"/><c:param name="stockId" value="${obj.id}"/></c:url>" selected="selected"><o:out value="${obj.name}"/></option>
            </c:when>
            <c:otherwise>
                <option value="<c:url value="${baseUrl}"><c:param name="method" value="updateStock"/><c:param name="productId" value="${item.product.productId}"/><c:param name="stockId" value="${obj.id}"/></c:url>"><o:out value="${obj.name}"/></option>
            </c:otherwise>
            </c:choose>
            </c:forEach>
            </select>
            </c:when>
            <c:otherwise>
            <oc:options name="stockKeys" value="${item.stockId}"/>
            </c:otherwise>
            </c:choose>
        </td>
		<c:choose>
		<c:when test="${item.product.kanban}">
		<td class="amount"><fmt:formatNumber value="${item.quantity}" minFractionDigits="3"/></td>
		</c:when>
		<c:otherwise>
		<td class="amount"><fmt:formatNumber value="${item.product.summary.availableStock}" minFractionDigits="3"/></td>
		</c:otherwise>
		</c:choose>
        <td class="total"><fmt:formatNumber value="${item.product.summary.ordered}" minFractionDigits="3"/></td>
	</tr>
    <tr<c:if test="${!alternate}"> class="altrow"</c:if>>
		<td class="productNumber">
			<c:choose><c:when test="${!empty item.delivery}"><o:date value="${item.delivery}"/></c:when><c:otherwise><fmt:message key="delivery"/> ?</c:otherwise></c:choose>
		</td>
		<td colspan="6"><o:out value="${item.deliveryNote}"/></td>
	</tr>
	</c:forEach>
</table>
</c:if>
