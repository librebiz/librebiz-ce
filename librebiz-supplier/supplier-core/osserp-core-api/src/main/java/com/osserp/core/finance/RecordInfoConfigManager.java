/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 28, 2008 4:21:02 PM 
 * 
 */
package com.osserp.core.finance;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordInfoConfigManager {

    /**
     * Creates a new record info text
     * @param text
     */
    void create(String text);

    /**
     * Updates an eol flag
     * @param id
     */
    void toggleEol(Long id);

    /**
     * Updates an existing record info text
     * @param id
     * @param text
     */
    void update(Long id, String text);

    /**
     * Provides all infos except eol
     * @return all activated
     */
    List<Option> getActivated();

    /**
     * Provides all infos
     * @return all
     */
    List<Option> getAll();

    /**
     * Provides the default infos for given record and request type
     * @param recordType
     * @param requestType
     * @return defaultInfos
     */
    List<Long> getDefaultInfos(Long recordType, Long requestType);

    /**
     * Adds a new default info for a record and request type
     * @param recordType
     * @param requestType
     * @param infoId to add
     * @return true if added
     * @throws ClientException if info already exists
     */
    boolean addDefaultInfo(Long recordType, Long requestType, Long infoId)
            throws ClientException;

    /**
     * Removes a default info for a record and request type
     * @param recordType
     * @param requestType
     * @param infoId to remove
     * @return true if successful removed
     */
    boolean removeDefaultInfo(Long recordType, Long requestType, Long infoId);
    
    /**
     * Moves an infos order id up
     * @param recordType
     * @param requestType
     * @param infoId
     * @return updated list
     */
    List<Long> moveInfoUp(Long recordType, Long requestType, Long infoId);
    
    /**
     * Moves an infos order id down
     * @param recordType
     * @param requestType
     * @param infoId
     * @return updated list
     */
    List<Long> moveInfoDown(Long recordType, Long requestType, Long infoId);

}
