/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 14, 2014 9:52:36 AM
 * 
 */
package com.osserp.core.model.records;


import com.osserp.common.EntityRelation;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.finance.TaxReport;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TaxReportItem extends AbstractEntity implements EntityRelation {
    
    private Long exportId;
    private Long recordId;
    private Long recordTypeId;
    private Long recordSubTypeId;

    protected TaxReportItem() {
        super();
    }

    /**
     * Creates a new taxReport item by added export.
     * @param taxReport
     * @param export
     */
    public TaxReportItem(TaxReport taxReport, Long export) {
        super((Long) null, taxReport.getId());
        exportId = export;
    }

    /**
     * Creates a new tax item by added record.
     * @param taxReport
     * @param recordId
     * @param recordTypeId
     * @param recordSubTypeId
     */
    public TaxReportItem(TaxReport taxReport, 
            Long recordId, 
            Long recordTypeId, 
            Long recordSubTypeId) {
        super((Long) null, taxReport.getId());
        this.recordId = recordId;
        this.recordTypeId = recordTypeId;
        this.recordSubTypeId = recordSubTypeId;
    }

    public Long getExportId() {
        return exportId;
    }

    public void setExportId(Long exportId) {
        this.exportId = exportId;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(Long recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    public Long getRecordSubTypeId() {
        return recordSubTypeId;
    }

    public void setRecordSubTypeId(Long recordSubTypeId) {
        this.recordSubTypeId = recordSubTypeId;
    }
}
