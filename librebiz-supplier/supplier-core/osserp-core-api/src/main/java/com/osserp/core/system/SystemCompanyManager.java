/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 16, 2010 8:47:11 AM 
 * 
 */
package com.osserp.core.system;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.dms.DmsDocument;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.Contact;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SystemCompanyManager {

    /**
     * Creates a poposal for a new system company. Company is initialized by contact, name as contact display name and id with next available company id
     * assigned.
     * @param contact
     * @return companyProposal
     * @throws ClientException if contact is not a valid system company candidate
     */
    SystemCompany createPoposal(Contact contact) throws ClientException;

    /**
     * Creates a new system company.
     * @param company with required values added
     * @return company
     * @throws ClientException if validation failed
     */
    SystemCompany create(SystemCompany company) throws ClientException;

    /**
     * loads all companies
     * @return list
     */
    List<SystemCompany> getAll();

    /**
     * Tries to find a system company by id, contactId or customer id
     * @param id
     * @return company or null if not found
     */
    SystemCompany find(Long id);

    /**
     * Adds a new managing director to a system company
     * @param user
     * @param company
     * @param directorName
     * @return company updated
     * @throws ClientException if director already added
     */
    SystemCompany addManagingDirector(Employee user, SystemCompany company, String directorName) throws ClientException;

    /**
     * Removes a managing director from a system company
     * @param company
     * @param directorId
     * @return company updated
     * @throws ClientException if removal failed
     */
    SystemCompany removeManagingDirector(SystemCompany company, Long directorId) throws ClientException;

    /**
     * Renames a managing director
     * @param company
     * @param directorId
     * @param directorName
     * @return company updated
     * @throws ClientException if renaming failed
     */
    SystemCompany renameManagingDirector(SystemCompany company, Long directorId, String directorName) throws ClientException;

    /**
     * updates a company's details
     * @param id
     * @param name
     * @param letterWindowName
     * @param localCourt
     * @param tradeRegisterEntry
     * @param taxId
     * @param vatId
     * @param internalMargin
     * @return the updated company
     * @throws ClientException
     */
    SystemCompany update(
            Long id,
            String name,
            String letterWindowName,
            String localCourt,
            String tradeRegisterEntry,
            String taxId,
            String vatId,
            Double internalMargin) throws ClientException;

    /**
     * @param user
     * @param company
     * @param shortkey
     * @param bankName
     * @param bankNameAddon
     * @param bankAccountNumberIntl
     * @param bankIdentificationCodeIntl
     * @param useInDocumentFooter
     * @param taxAccount
     * @return the updated company
     * @throws ClientException
     */
    SystemCompany addBankAccount(
            Employee user,
            SystemCompany company,
            String shortkey,
            String bankName,
            String bankNameAddon,
            String bankAccountNumberIntl,
            String bankIdentificationCodeIntl,
            boolean useInDocumentFooter,
            boolean taxAccount) throws ClientException;
    
    /**
     * Loads a bank account data from persistent storage.
     * @param id
     * @return account or throws RuntimeException if not exists
     */
    BankAccount getBankAccount(Long id);
    
    /**
     * Provides all available bank accounts of all available clients.
     * @return bankAccounts or empty list if non exists
     */
    List<BankAccount> getBankAccounts();

    /**
     * @param company
     * @param id
     * @return the updated company
     * @throws ClientException
     */
    SystemCompany removeBankAccount(SystemCompany company, Long id) throws ClientException;

    /**
     * Assigns a supplier contact reference to a system company bankAccount.
     * @param bankAccountId
     * @param supplierId
     */
    void assignBankAccount(Long bankAccountId, Long supplierId);

    /**
     * @param company
     * @param id
     * @param shortkey
     * @param bankName
     * @param bankNameAddon
     * @param bankAccountNumberIntl
     * @param bankIdentificationCodeIntl
     * @param useInDocumentFooter
     * @param taxAccount
     * @return the updated company
     * @throws ClientException
     */
    SystemCompany updateBankAccount(
            SystemCompany company,
            Long id,
            String shortkey,
            String bankName,
            String bankNameAddon,
            String bankAccountNumberIntl,
            String bankIdentificationCodeIntl,
            boolean useInDocumentFooter,
            boolean taxAccount) throws ClientException;

    /**
     * Updates a company's document availability flag
     * @param company to update
     * @param docType of the document
     * @param available wether document available
     */
    void updateDocumentFlag(SystemCompany company, Long docType, boolean available) throws ClientException;

    /**
     * Copies an uploaded document to logo directory to make it available to
     * the stylesheet processor (e.g. embedd logo in PDF documents).
     * @param company
     * @param document
     * @throws ClientException if copy failed for any reason
     */
    void deployLogo(SystemCompany company, DmsDocument document) throws ClientException;

    /**
     * Removes a previously copied company logo from stylesheet logo directory.
     * @param company
     * @param sourceFilename the name of the original source file 
     * @throws ClientException if remove failed for any reason
     */
    void removeLogo(SystemCompany company, String sourceFilename) throws ClientException;
    
    /**
     * Provides the complete filesystem path and name of the company logo if available
     * @param office
     * @return logo path and name or null
     */
    String getLogoPath(BranchOffice office);
    
}
