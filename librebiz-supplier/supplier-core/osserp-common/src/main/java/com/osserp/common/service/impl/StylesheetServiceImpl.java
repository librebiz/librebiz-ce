/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 20, 2008 12:19:34 PM 
 * 
 */
package com.osserp.common.service.impl;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.StylesheetService;
import com.osserp.common.service.TemplateManager;
import com.osserp.common.util.JsonObject;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.common.xml.XSLTransformer;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StylesheetServiceImpl extends AbstractResourceAwareService implements StylesheetService {
    private static Logger log = LoggerFactory.getLogger(StylesheetServiceImpl.class.getName());

    protected TemplateManager templateManager = null;
    private boolean documentOutputDebugMap = false;
    private boolean documentOutputMapAsJson = false;
    private String userConfigFile = null;

    public StylesheetServiceImpl(
            ResourceLocator resourceLocator,
            TemplateManager templateManager,
            String userConfigFile) {
        super(resourceLocator);
        this.templateManager = templateManager;
        this.userConfigFile = userConfigFile;
    }

    public StylesheetServiceImpl(
            ResourceLocator resourceLocator,
            TemplateManager templateManager,
            String userConfigFile,
            boolean documentOutputDebugMap) {
        this(resourceLocator, templateManager, userConfigFile);
        this.documentOutputDebugMap = documentOutputDebugMap;
    }
    
    protected final String getTemplateRoot() {
        return templateManager == null ? "" : templateManager.getTemplatePath();
    }
    
    /**
     * This method provides the initial map required to configure 
     * the template related header, footer and other template options.
     * Override this method to set your own stylesheetService options. 
     * @param object
     * @return default implementation provides an empty map
     */
    protected Map<String, Object> createInitialMap(Object object) {
        return new HashMap<>();
    }

    public boolean sheetExists(String stylesheetName) {
        return templateManager.templateExists(stylesheetName);
    }

    public final byte[] createPdf(Document xml, Source stylesheet) {
        try {
            return XSLTransformer.createPdf(xml, stylesheet, userConfigFile);
        } catch (Throwable t) {
            log.error("createPdf() failed [message=" + t.getMessage() + "]\n", t);
            return null;
        }
    }

    public final byte[] createPdf(Document xml, String stylesheetName) throws ClientException {
        try {
            Source xsl = getStylesheetSource(stylesheetName);
            return XSLTransformer.createPdf(xml, xsl, userConfigFile);
        } catch (Throwable t) {
            throw new ClientException(ErrorCode.STYLESHEET_MISSING);
        }
    }

    public String getUserConfigFile() {
        return userConfigFile;
    }

    protected void setUserConfigFile(String userConfigFile) {
        this.userConfigFile = userConfigFile;
    }

    public Source getStylesheetSource(String name) {
        return getStylesheetSource(name, Constants.DEFAULT_LOCALE_OBJECT);
    }
    
    protected final String createStylesheetSource(String name) {
        return createStylesheetSource(name, Constants.DEFAULT_LOCALE_OBJECT);
    }
    
    protected final String createStylesheetSource(String name, Locale locale) {
        Map<String, Object> values = createMap(locale);
        String xsl = templateManager.createText(name, values);
        return xsl;
    }

    protected final Source getStylesheetSource(String name, Locale locale) {
        String xsl = createStylesheetSource(name, locale);
        StreamSource sheet = new StreamSource(new StringReader(xsl));
        return sheet;
    }

    protected final Map<String, Object> createMap(Locale locale) {
        Map<String, Object> values = createInitialMap(null);
        return completeMap(values, locale);
    }

    protected final Map<String, Object> completeMap(Map<String, Object> values, Locale locale) {
        addProperty(values, "documentFooterLayoutCentered", "centeredFooter");
        if (log.isDebugEnabled() && isDocumentOutputDebugMap()) {
            log.debug("completeMap() values (without i18n):\n" + JDOMUtil.getPrettyString(JDOMUtil.mapToXML(values, "root")));
        }
        if (log.isDebugEnabled() && isDocumentOutputMapAsJson()) {
            JsonObject obj = new JsonObject(values);
            log.debug("completeMap() values (without i18n):\n" + obj.toString() + "\n");
        }
        Map i18n = getResourceMap(locale != null ? locale : Constants.DEFAULT_LOCALE_OBJECT);
        //if (log.isDebugEnabled()) {
        //    JsonObject obj = new JsonObject(i18n);
        //    log.debug("completeMap() i18n as JSON:\n" + obj.toString());
        //}
        values.put("i18n", i18n);
        return values;
    }

    protected boolean isPropertyEnabled(String name) {
        return false;
    }

    protected boolean isDocumentOutputDebugMap() {
        return documentOutputDebugMap;
    }

    protected void setDocumentOutputDebugMap(boolean documentOutputDebugMap) {
        this.documentOutputDebugMap = documentOutputDebugMap;
    }

    protected boolean isDocumentOutputMapAsJson() {
        return documentOutputMapAsJson;
    }

    protected void setDocumentOutputMapAsJson(boolean documentOutputMapAsJson) {
        this.documentOutputMapAsJson = documentOutputMapAsJson;
    }

    private void addProperty(Map<String, Object> values, String name, String label) {
        if (isPropertyEnabled(name)) {
            values.put(label, "true");
        } else {
            values.put(label, "false");
        }
    }

}
