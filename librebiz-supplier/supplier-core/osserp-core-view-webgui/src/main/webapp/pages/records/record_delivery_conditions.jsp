<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>

<c:set var="deliveryCondition" value="${record.deliveryCondition}"/>

<c:choose>
    <c:when test="${view.deliveryConditionEditMode}">
        <div class="recordLabelValue">
            <fmt:message key="deliveryConditions"/>:
            <a style="margin-left: 20px;" href="<c:url value='/purchaseOrder.do?method=disableDeliveryConditionEdit' />">
                <c:choose>
                    <c:when test="${deliveryCondition != null}"><o:out value="${deliveryCondition.name}"/></c:when>
                    <c:otherwise><fmt:message key="none" /></c:otherwise>
                </c:choose>
            </a>

            <c:set var="cachedDeliveryConditionId">
                <o:out value="${view}" param="deliveryConditionId"/>
            </c:set>

            <o:form name="recordForm" url="/purchaseOrder.do">
                <input type="hidden" name="method" value="updateDeliveryCondition" />
                <oc:select name="deliveryConditionId" value="${record.deliveryCondition}" options="recordDeliveryConditions" prompt="false" style="width:50%;"/>
                <input type="submit" value="OK" />
            </o:form>
        </div>
    </c:when>
    <c:otherwise>
        <div class="recordLabelValue">
            <fmt:message key="deliveryConditions"/>:
            <a style="margin-left: 20px;" href="<c:url value='/purchaseOrder.do?method=enableDeliveryConditionEdit' />">
                <c:choose>
                    <c:when test="${deliveryCondition != null}"><o:out value="${deliveryCondition.name}"/></c:when>
                    <c:otherwise><fmt:message key="none" /></c:otherwise>
                </c:choose>
            </a>
            <c:set var="cachedDeliveryConditionId">
                <o:out value="${view}" param="deliveryConditionId"/>
            </c:set>
        </div>
    </c:otherwise>
</c:choose>

