/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Sep-2008 16:32:33 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.core.model.contacts.groups.EmployeeRoleDisplayVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeRoleDisplayRowMapper implements RowMapper {
    private static Logger log = LoggerFactory.getLogger(EmployeeRoleDisplayRowMapper.class.getName());
    public static final String SELECT_ALL_FROM =
            "SELECT " +
                    "uid," +
                    "employee," +
                    "bid," +
                    "branch," +
                    "db," +
                    "gid," +
                    "groupname," +
                    "sid," +
                    "status," +
                    "dr," +
                    "cid," +
                    "company FROM ";

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        try {
            return new EmployeeRoleDisplayVO(
                    rs.getString("branch"),
                    rs.getLong("bid"),
                    rs.getString("company"),
                    rs.getLong("cid"),
                    rs.getBoolean("db"),
                    rs.getBoolean("dr"),
                    rs.getString("employee"),
                    rs.getLong("uid"),
                    rs.getString("groupname"),
                    rs.getLong("gid"),
                    rs.getString("status"),
                    rs.getLong("sid"));
        } catch (SQLException e) {
            log.error("mapRow() failed [message=" + e.toString() + "]");
            throw e;
        }
    }

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new EmployeeRoleDisplayRowMapper());
    }

}
