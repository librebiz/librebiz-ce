<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:set var="view" value="${sessionScope.recordView}" />
<c:set var="record" value="${view.record}" />
<c:set var="baseUrl" value="${requestScope.baseUrl}" />
<c:set var="productUrl" value="${requestScope.productUrl}" />
<c:set var="productExitTarget" value="${requestScope.productExitTarget}" />
<c:choose>
    <c:when test="${!empty requestScope.displayPrice}">
        <c:set var="displayPrice" value="${requestScope.displayPrice}" />
    </c:when>
    <c:otherwise>
        <c:set var="displayPrice" value="true" />
    </c:otherwise>
</c:choose>
<div class="recordItems">
    <table class="table<c:if test="${!view.itemEditMode and !view.itemPasteMode}"> table-striped</c:if>">
        <tr>
            <th id="number" class="productNumber">
                <c:choose>
                    <c:when test="${view.productEditMode}">
                        <c:choose>
                            <c:when test="${record.itemsChangeable}">
                                <a href="<c:url value="${productUrl}?method=forwardSelection"/>" title="<fmt:message key="addProducts"/>"> <fmt:message key="product" />
                                </a>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="product" />
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${!empty productUrl and (!record.unchangeable or record.historical) and (record.itemsChangeable or record.itemsEditable)}">
                                <a href="<c:url value="${productUrl}?method=editProducts"/>" title="<fmt:message key="change"/>">
                                    <fmt:message key="product" />
                                </a>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="product" />
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </th>
            <c:choose>
                <c:when test="${!empty baseUrl}">
                    <th id="description" class="productName"><a href="<c:url value="${baseUrl}"><c:param name="method" value="changeDescriptionDisplay"/></c:url>"><fmt:message key="description" /></a></th>
                </c:when>
                <c:otherwise>
                    <th id="description" class="productName"><fmt:message key="description" /></th>
                </c:otherwise>
            </c:choose>
            <th id="quantity" class="quantity"><fmt:message key="quantity" /></th>
            <th id="quantity_unit" class="quantityUnit"><fmt:message key="quantityUnitShort" /></th>
            <c:set var="colspanVal" value="4" />
            <c:if test="${record.type.stockAware}">
                <th id="stock_short" class="recordStock"><fmt:message key="stockShort" /></th>
                <c:set var="colspanVal" value="${colspanVal + 1}" />
            </c:if>
            <c:choose>
                <c:when test="${!view.productEditMode}">
                    <c:if test="${displayPrice}">
                        <c:choose>
                            <c:when test="${record.type.supportingDeliveryDateOnItems and view.itemDeliveryDateEditMode}">
                                <th id="price" class="right"><fmt:message key="price" /></th>
                                <th id="vat" class="right"><fmt:message key="vat" /></th>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${!empty productUrl and (!record.unchangeable or record.historical) and (!record.itemsChangeable or !record.itemsEditable)}">
                                        <th id="vat" class="right">
                                            <a href="<c:url value="${productUrl}?method=editProducts"/>" title="<fmt:message key="change"/>">
                                                <fmt:message key="vat" />
                                            </a>
                                        </th>
                                    </c:when>
                                    <c:otherwise>
                                        <th id="vat" class="right"><fmt:message key="vat" /></th>
                                    </c:otherwise>
                                </c:choose>
                                <th id="price" class="right"><fmt:message key="price" /></th>
                            </c:otherwise>
                        </c:choose>
                        <th id="total" class="total"><fmt:message key="total" /></th>
                        <c:set var="colspanVal" value="${colspanVal + 1}" />
                    </c:if>
                </c:when>
                <c:otherwise>
                    <th id="vat" class="right"><fmt:message key="vat" /></th>
                    <th id="price" class="right"><fmt:message key="price" /></th>
                    <th id="action" class="action" colspan="6"><fmt:message key="actions" /></th>
                    <c:set var="colspanVal" value="${colspanVal + 6}" />
                </c:otherwise>
            </c:choose>
        </tr>
        <o:logger write="colspanVal=${colspanVal}" level="debug" />
        <c:if test="${empty record.items}">
            <tr>
                <td colspan="${colspanVal}">
                    <fmt:message key="error.record.items.missing" />
                    <c:if test="${!empty productUrl and record.itemsChangeable and !record.unchangeable and !view.productEditMode}">
                        <span style="padding-left:20px;">
                            <a href="<c:url value="${productUrl}"><c:param name="method" value="editProducts"/></c:url>" title="<fmt:message key="change"/>">[<fmt:message key="add"/>]</a>
                        </span>
                    </c:if>
                </td>
            </tr>
        </c:if>
        <c:choose>
            <c:when test="${!view.itemEditMode}">
                <%-- DELIVERY DATE EDIT MODE --%>
                <c:choose>
                    <c:when test="${record.type.supportingDeliveryDateOnItems and view.itemDeliveryDateEditMode}">
                        <c:forEach var="item" items="${record.items}" varStatus="s">
                            <c:if test="${view.selectedItem.id == item.id}">
                                <tr>
                                    <td class="productNumber">
                                        <c:choose>
                                            <c:when test="${!empty productExitTarget}">
                                                <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=${productExitTarget}"/>" title="<fmt:message key="loadProduct"/>">
                                                    <o:out value="${item.product.productId}" />
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                                                    <o:out value="${item.product.productId}" />
                                                </o:popupSelect>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="productName"><div class="recordTextColumn">
                                            <o:out value="${item.productName}" />
                                        </div></td>
                                    <td class="quantity"><o:number value="${item.quantity}" format="decimal" /></td>
                                    <td class="quantityUnit"><oc:options name="quantityUnits" value="${item.product.quantityUnit}" /></td>
                                    <c:if test="${record.type.stockAware}">
                                        <td class="recordStock">
                                            <c:choose>
                                                <c:when test="${item.product.affectsStock}">
                                                    <oc:options name="stockKeys" value="${item.stockId}" />
                                                </c:when>
                                                <c:otherwise>
                                                    <span title="<fmt:message key="productNotStockAware"/>">-/-</span>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </c:if>
                                    <c:if test="${displayPrice}">
                                        <c:choose>
                                            <c:when test="${item.includePrice}">
                                                <td class="right" id="<o:out value="${item.id}"/>price"><o:number value="${item.price}" format="currency" /></td>
                                                <td class="right" id="<o:out value="${item.id}"/>vat"><o:number value="${item.taxRate}" format="tax" /></td>
                                                <td class="total" id="<o:out value="${item.id}"/>amount"><o:number value="${item.amount}" format="currency" /></td>
                                            </c:when>
                                            <c:otherwise>
                                                <td class="right" id="<o:out value="${item.id}"/>price"><o:number value="0.00" format="currency" /></td>
                                                <td class="right" id="<o:out value="${item.id}"/>vat"> </td>
                                                <td class="total" id="<o:out value="${item.id}"/>amount"><o:number value="0.00" format="currency" /></td>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </tr>
                                <tr>
                                    <o:form name="recordForm" url="${requestScope.baseUrl}">
                                        <input type="hidden" name="method" value="updateDeliveryDate" />
                                        <td valign="top" nowrap="nowrap" style="width: 150px;"><input type="text" class="form-control" name="delivery" id="${item.id}deliveryValue" value="<o:date value="${item.delivery}"/>" style="text-align: right; width: 80%; display: inline;" /> <o:datepicker input="${item.id}deliveryValue" style="vertical-align:middle;" /></td>
                                        <td><input type="text" class="form-control" name="deliveryNote" id="itemNoteValue" value="<o:out value="${item.deliveryNote}"/>" /></td>
                                        <td colspan="4"></td>
                                        <td><a href="<c:url value="${requestScope.baseUrl}?method=changeItemDeliveryDateMode"/>"> <input type="button" class="form-control cancel" value="<fmt:message key="exit"/>" />
                                        </a></td>
                                        <td><input type="submit" class="form-control" value="<fmt:message key="save"/>" /></td>
                                    </o:form>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </c:when>
                    <%-- DISPLAY MODE --%>
                    <c:otherwise>
                        <c:forEach var="item" items="${record.items}" varStatus="s">
                            <tr id="product${item.product.productId}"<c:if test="${view.itemPasteMode && view.currentItem.id == item.id}"> class="red unreleased"</c:if><c:if test="${!view.itemPasteMode && item.ignoreInDocument}"> class="ignorable"</c:if>>
                                <td id="<o:out value="${item.id}"/>id" class="productNumber">
                                    <c:choose>
                                        <c:when test="${!empty productExitTarget}">
                                            <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=${productExitTarget}&exitId=product${item.product.productId}"/>" title="<fmt:message key="loadProduct"/>">
                                                <o:out value="${item.product.productId}" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                                                <o:out value="${item.product.productId}" />
                                            </o:popupSelect>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="productName"><o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                                        <div class="recordTextColumn">
                                            <o:out value="${item.productName}" />
                                        </div>
                                    </o:popupSelect>
                                </td>
                                <td class="quantity" id="<o:out value="${item.id}"/>quantity">
                                    <c:choose>
                                        <c:when test="${!empty productUrl and (!record.unchangeable or record.historical) and (record.itemsChangeable or record.itemsEditable)}">
                                            <a href="<c:url value="${productUrl}?method=editProducts&itemId=${item.id}"/>" title="<fmt:message key="change"/>">
                                                <o:number value="${item.quantity}" format="decimal" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <o:number value="${item.quantity}" format="decimal" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="quantityUnit"><oc:options name="quantityUnits" value="${item.product.quantityUnit}" /></td>
                                <c:if test="${record.type.stockAware}">
                                    <td class="recordStock">
                                        <c:choose>
                                            <c:when test="${item.product.affectsStock}">
                                                <oc:options name="stockKeys" value="${item.stockId}" />
                                            </c:when>
                                            <c:otherwise>
                                                <span title="<fmt:message key="productNotStockAware"/>">-/-</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </c:if>
                                <c:if test="${displayPrice}">
                                    <c:choose>
                                        <c:when test="${!view.productEditMode}">
                                            <c:choose>
                                                <c:when test="${!empty ignoreItemSupport}">
                                                    <c:choose>
                                                        <c:when test="${item.ignoreInDocument}">
                                                            <c:set var="toggleIgnoreItemTitle"><fmt:message key="ignoreInDocumentEnabled" /></c:set>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:set var="toggleIgnoreItemTitle"><fmt:message key="ignoreInDocumentDisabled" /></c:set>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <c:choose>
                                                        <c:when test="${item.includePrice}">
                                                            <td class="right" id="<o:out value="${item.id}"/>vat"><o:number value="${item.taxRate}" format="tax" /></td>
                                                            <td class="right" id="<o:out value="${item.id}"/>price"><o:number value="${item.price}" format="currency" /></td>
                                                            <td class="total" id="<o:out value="${item.id}"/>amount">
                                                                <a href="<c:url value="${requestScope.baseUrl}?method=changeItemIgnoreInDocument&id=${item.id}"/>" title="<o:out value="${toggleIgnoreItemTitle}"/>">
                                                                    <o:number value="${item.amount}" format="currency" />
                                                                </a>
                                                            </td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td class="right" id="<o:out value="${item.id}"/>vat"> </td>
                                                            <td class="right" id="<o:out value="${item.id}"/>price"><o:number value="0.00" format="currency" /></td>
                                                            <td class="total" id="<o:out value="${item.id}"/>amount">
                                                                <a href="<c:url value="${requestScope.baseUrl}?method=changeItemIgnoreInDocument&id=${item.id}"/>" title="<o:out value="${toggleIgnoreItemTitle}"/>">
                                                                    <o:number value="0.00" format="currency" />
                                                                </a>
                                                            </td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:choose>
                                                        <c:when test="${item.includePrice}">
                                                            <td class="right" id="<o:out value="${item.id}"/>vat"><o:number value="${item.taxRate}" format="tax" /></td>
                                                            <td class="right" id="<o:out value="${item.id}"/>price"><o:number value="${item.price}" format="currency" /></td>
                                                            <td class="total" id="<o:out value="${item.id}"/>amount"><o:number value="${item.amount}" format="currency" /></td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td class="right" id="<o:out value="${item.id}"/>vat"> </td>
                                                            <td class="right" id="<o:out value="${item.id}"/>price"><o:number value="0.00" format="currency" /></td>
                                                            <td class="total" id="<o:out value="${item.id}"/>amount"><o:number value="0.00" format="currency" /></td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <%-- EDIT MODE --%>
                                        <c:otherwise>
                                            <td class="right"><o:number value="${item.taxRate}" format="tax" /></td>
                                            <td class="right"><o:number value="${item.price}" format="currency" /></td>
                                            <c:choose>
                                                <c:when test="${view.itemPasteMode}">
                                                    <td class="icon center"> </td>
                                                    <td class="icon center"> </td>
                                                    <td class="icon center"> </td>
                                                    <td class="icon center">
                                                        <c:if test="${view.currentItem.id != item.id}">
                                                            <a href="<c:url value="${productUrl}?method=pasteItem&id=${item.id}"/>" title="<fmt:message key="title.insert.item.here"/>">
                                                                <o:img name="enabledIcon" style="margin-right: 0px;" styleClass="bigicon" />
                                                            </a>
                                                        </c:if>
                                                    </td>
                                                    <td class="icon center"> </td>
                                                    <td class="icon center"> </td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="icon center">
                                                        <c:if test="${record.itemsChangeable}">
                                                            <a href="<c:url value="${productUrl}?method=moveUpItem&id=${item.id}"/>" title="<fmt:message key="moveUp"/>"><o:img name="upIcon" style="margin-right: 0px;" styleClass="bigicon" /></a>
                                                        </c:if>
                                                    </td>
                                                    <td class="icon center">
                                                        <c:if test="${record.itemsChangeable}">
                                                            <a href="<c:url value="${productUrl}?method=moveDownItem&id=${item.id}"/>" title="<fmt:message key="moveDown"/>"><o:img name="downIcon" style="margin-right: 0px;" styleClass="bigicon" /></a>
                                                        </c:if>
                                                    </td>
                                                    <td class="icon center">
                                                        <c:if test="${record.itemsChangeable}">
                                                            <a href="<c:url value="${productUrl}?method=replaceItem&id=${item.id}"/>" title="<fmt:message key="replaceProduct"/>"><o:img name="replaceIcon" style="margin-right: 0px;" styleClass="bigicon" /></a>
                                                        </c:if>
                                                    </td>
                                                    <td class="icon center">
                                                        <c:if test="${record.itemsChangeable}">
                                                            <a href="<c:url value="${productUrl}?method=cutItem&id=${item.id}"/>" title="<fmt:message key="cutAndPaste"/>"><o:img name="cutIcon" style="margin-right: 0px;" styleClass="bigicon" /></a>
                                                        </c:if>
                                                    </td>
                                                    <td class="icon center">
                                                        <c:if test="${record.itemsChangeable or record.itemsDeletable}">
                                                            <a href="javascript:onclick=confirmLink('<fmt:message key="title.confirm.delete"/>','<c:url value="${productUrl}?method=deleteItem&id=${item.id}"/>');" title="<fmt:message key="title.position.delete"/>">
                                                                <o:img name="deleteIcon" style="margin-right: 0px;" styleClass="bigicon" />
                                                            </a>
                                                        </c:if>
                                                    </td>
                                                    <td class="icon center">
                                                        <c:if test="${(record.itemsChangeable or record.itemsDeletable) or ((!record.itemsChangeable or !record.itemsEditable) and item.includePrice)}">
                                                            <a href="<c:url value="${productUrl}?method=editItem&id=${item.id}"/>" title="<fmt:message key="title.quantity.change"/>">
                                                                <o:img name="writeIcon" style="margin-right: 0px;" styleClass="bigicon" />
                                                            </a>
                                                        </c:if>
                                                    </td>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </tr>
                            <%-- item description --%>
                            <c:if test="${!view.productEditMode and view.displayDescription and
                                    (!empty item.product.description or !empty item.note or record.type.supportingDeliveryDateOnItems)}">
                                <tr id="<o:out value="${item.id}"/>delivery">
                                    <td valign="top" nowrap="nowrap">
                                        <c:choose>
                                            <c:when test="${record.type.supportingDeliveryDateOnItems and !view.itemDeliveryDateEditMode}">
                                                <a href="<c:url value="${baseUrl}"><c:param name="method" value="changeItemDeliveryDateMode"/><c:param name="id" value="${item.id}"/></c:url>">
                                                    <c:choose>
                                                        <c:when test="${!empty item.delivery}">
                                                            <o:date value="${item.delivery}" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <fmt:message key="delivery" />
                                                            <span>?</span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </a>
                                                <br />
                                            </c:when>
                                            <c:when test="${record.type.supportingDeliveryDateOnItems}">
                                                <c:choose>
                                                    <c:when test="${!empty item.delivery}">
                                                        <o:date value="${item.delivery}" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:message key="delivery" />
                                                        <span>?</span>
                                                    </c:otherwise>
                                                </c:choose>
                                                <br />
                                            </c:when>
                                            <c:otherwise>
                                                <span> </span>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:if test="${record.type.supportingDeliveryAck}">
                                            <c:choose>
                                                <c:when test="${item.deliveryConfirmed}">
                                                    <a href="<c:url value="${baseUrl}"><c:param name="method" value="changeDeliveryConfirmation"/><c:param name="id" value="${item.id}"/></c:url>"><fmt:message key="confirmed" /></a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="<c:url value="${baseUrl}"><c:param name="method" value="changeDeliveryConfirmation"/><c:param name="id" value="${item.id}"/></c:url>"><fmt:message key="expected" /></a>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:if>
                                    </td>
                                    <%-- if we display the price, there are more columns --%>
                                    <c:choose>
                                        <c:when test="${record.type.stockAware and displayPrice}">
                                            <td>
                                                <div class="recordTextColumn">
                                                    <o:textOut value="${item.product.description}" />
                                                    <c:if test="${!empty item.note}">
                                                        <c:if test="${!empty item.product.description}">
                                                            <br />
                                                        </c:if>
                                                        <o:textOut value="${item.note}" />
                                                    </c:if>
                                                </div>
                                            </td>
                                            <td colspan="6" class="right">&nbsp;</td>
                                        </c:when>
                                        <c:when test="${displayPrice}">
                                            <td>
                                                <div class="recordTextColumn">
                                                    <o:textOut value="${item.product.description}" />
                                                    <c:if test="${!empty item.note}">
                                                        <c:if test="${!empty item.product.description}">
                                                            <br />
                                                        </c:if>
                                                        <o:textOut value="${item.note}" />
                                                    </c:if>
                                                </div>
                                            </td>
                                            <td colspan="5" class="right">&nbsp;</td>
                                        </c:when>
                                        <c:when test="${record.type.stockAware}">
                                            <td>
                                                <div class="recordTextColumn">
                                                    <o:textOut value="${item.product.description}" />
                                                    <c:if test="${!empty item.note}">
                                                        <c:if test="${!empty item.product.description}">
                                                            <br />
                                                        </c:if>
                                                        <o:textOut value="${item.note}" />
                                                    </c:if>
                                                </div>
                                            </td>
                                            <td colspan="5" class="right">&nbsp;</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>
                                                <div class="recordTextColumn">
                                                    <o:textOut value="${item.product.description}" />
                                                    <c:if test="${!empty item.note}">
                                                        <c:if test="${!empty item.product.description}">
                                                            <br />
                                                        </c:if>
                                                        <o:textOut value="${item.note}" />
                                                    </c:if>
                                                </div>
                                            </td>
                                            <td class="right">&nbsp;</td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>

                <c:if test="${view.productEditMode and record.itemsChangeable and !empty record.items and !view.itemPasteMode}">
                    <tr>
                        <td class="productNumber">&nbsp;</td>
                        <td class="productName" <c:choose><c:when test="${record.type.stockAware}"> colspan="5"</c:when><c:otherwise> colspan="4"</c:otherwise></c:choose>></td>
                        <td colspan="6"></td>
                        <td class="icon center"><a href="<c:url value="${productUrl}?method=forwardSelection"/>" title="<fmt:message key="addFurtherProduct" />"><o:img name="newdataIcon" styleClass="bigicon" /></a></td>
                    </tr>
                </c:if>
            </c:when>
            <c:otherwise>
                <%-- ITEM EDIT --%>
                <o:form name="recordForm" url="${productUrl}">
                    <input type="hidden" name="method" value="updateItem" />
                    <c:set var="selected" value="${view.currentItem}" />
                    <c:forEach var="item" items="${record.items}" varStatus="s">
                        <c:choose>
                            <c:when test="${item.id == selected.id}">
                                <c:if test="${(record.itemsChangeable and record.itemsEditable) or (record.itemsEditable and item.product.quantityUnitTime)}">>
                                    <c:set var="itemNoteEditable" value="true"/>
                                </c:if>
                                <tr id="product${item.product.productId}" class="altrow">
                                    <td class="productNumber">
                                        <c:choose>
                                            <c:when test="${!empty productExitTarget}">
                                                <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=${productExitTarget}&exitId=product${item.product.productId}"/>" title="<fmt:message key="loadProduct"/>">
                                                    <o:out value="${item.product.productId}" />
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                                                    <o:out value="${item.product.productId}" />
                                                </o:popupSelect>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="productName">
                                        <oc:systemPropertyEnabled name="productCustomNameSupport">
                                            <c:choose>
                                                <c:when test="${!item.product.affectsStock and (item.product.group.customNameSupport or item.product.category.customNameSupport) and (record.itemsChangeable and record.itemsEditable)}">
                                                    <input type="text" class="form-control recordTextColumn" style="text-align: left;" name="customName" value="<o:out value="${item.productName}"/>" />
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="recordTextColumn">
                                                        <o:out value="${item.product.name}" />
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </oc:systemPropertyEnabled>
                                        <oc:systemPropertyDisabled name="productCustomNameSupport">
                                            <div class="recordTextColumn">
                                                <o:out value="${item.productName}" />
                                            </div>
                                        </oc:systemPropertyDisabled>
                                    </td>
                                    <td class="quantity">
                                        <input type="text" name="quantity" class="form-control right" value="<o:number value="${item.quantity}" format="decimal"/>"<c:if test="${!record.itemsChangeable and (!record.itemsEditable and !item.product.quantityUnitTime)}"> readonly="readonly"</c:if>/>
                                    </td>
                                    <td class="quantityUnit">
                                        <oc:options name="quantityUnits" value="${item.product.quantityUnit}" />
                                    </td>
                                    <c:if test="${record.type.stockAware}">
                                        <td class="recordStock"><oc:select name="stockId" value="${item.stockId}" options="stockKeys" styleClass="form-control" prompt="false" /></td>
                                    </c:if>
                                    <td class="right">
                                        <select name="taxRate" class="form-control">
                                            <c:choose>
                                                <c:when test="${item.product.reducedTax}">
                                                    <c:forEach var="rate" items="${view.reducedTaxRates}">
                                                        <c:choose>
                                                            <c:when test="${record.amounts.reducedTaxRate == rate}">
                                                                <option value="<o:out value="${rate}"/>" selected="selected"><o:number value="${rate}" format="tax"/> %</option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="<o:out value="${rate}"/>"><o:number value="${rate}" format="tax"/> %</option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:forEach var="rate" items="${view.taxRates}">
                                                        <c:choose>
                                                            <c:when test="${record.amounts.taxRate == rate}">
                                                                <option value="<o:out value="${rate}"/>" selected="selected"><o:number value="${rate}" format="tax"/> %</option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="<o:out value="${rate}"/>"><o:number value="${rate}" format="tax"/> %</option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </c:otherwise>
                                            </c:choose>
                                        </select>
                                    </td>
                                    <td class="right">
                                        <input type="text" name="price" class="form-control right" value="<o:number value="${item.price}" format="currency"/>" <c:if test="${!record.itemsChangeable or !record.itemsEditable}"> readonly="readonly"</c:if>/>
                                    </td>
                                    <td class="action">
                                        <table style="width: 100%;">
                                            <tr class="altrow">
                                                <td style="width: 50%; text-align: center;"><a href="<c:url value="${productUrl}"><c:param name="method" value="exitItemEdit"/></c:url>" title="<fmt:message key="title.edit.exit"/>"> <o:img name="backIcon" styleClass="bigicon" />
                                                </a></td>
                                                <td style="width: 50%; text-align: center;"><input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="updateItem" style="margin-top: 3px;" class="bigicon" title="<fmt:message key="title.data.save"/>" /></td>
                                            </tr>
                                        </table>
                                        <c:if test="${!itemNoteEditable}"><input type="hidden" name="note" value="<o:out value="${item.note}" />"/></c:if>
                                    </td>
                                </tr>
                                <c:if test="${itemNoteEditable}">
                                    <tr class="altrow">
                                        <td class="productNumber">&nbsp;</td>
                                        <td><textarea class="form-control recordTextColumn" name="note" rows="4"><o:out value="${item.note}" /></textarea></td>
                                        <c:choose>
                                            <c:when test="${record.type.stockAware}">
                                                <td colspan="6">&nbsp;</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td colspan="5">&nbsp;</td>
                                            </c:otherwise>
                                        </c:choose>
                                    </tr>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <tr id="product${item.product.productId}">
                                    <td class="productNumber">
                                        <c:choose>
                                            <c:when test="${!empty productExitTarget}">
                                                <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=${productExitTarget}&exitId=product${item.product.productId}"/>" title="<fmt:message key="loadProduct"/>">
                                                    <o:out value="${item.product.productId}" />
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" header="product" dimension="${popupPageSize}" title="displayProductData">
                                                    <o:out value="${item.product.productId}" />
                                                </o:popupSelect>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="productName">
                                        <div class="recordTextColumn">
                                            <o:out value="${item.productName}" />
                                        </div>
                                    </td>
                                    <td class="quantity"><o:number value="${item.quantity}" format="decimal" /></td>
                                    <td class="quantityUnit"><oc:options name="quantityUnits" value="${item.product.quantityUnit}" /></td>
                                    <c:if test="${record.type.stockAware}">
                                        <td class="recordStock">
                                            <c:if test="${item.product.affectsStock}">
                                                <oc:options name="stockKeys" value="${item.stockId}" />
                                            </c:if>
                                        </td>
                                    </c:if>
                                    <td class="right" id="<o:out value="${item.id}"/>vat"><o:number value="${item.taxRate}" format="tax" /></td>
                                    <td class="right" id="<o:out value="${item.id}"/>price"><o:number value="${item.price}" format="currency" /></td>
                                    <td class="action" id="<o:out value="${item.id}"/>amount"></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </o:form>
            </c:otherwise>
        </c:choose>
        <c:if test="${!empty record.items and !view.productEditMode and displayPrice}">
            <c:choose>
                <c:when test="${record.taxFree}">
                    <tr>
                        <th colspan="<c:choose><c:when test="${record.type.stockAware}">4</c:when><c:otherwise>3</c:otherwise></c:choose>">
                            <fmt:message key="totalSum" /> (<fmt:message key="netTurnoverTaxGross" />)
                        </th>
                        <th class="right"><o:out value="${record.currencyKey}" /></th>
                        <th style="width: 100px;" class="right" id="netAmount"><o:number value="${record.amounts.amount}" format="currency" /></th>
                        <th class="right" id="taxAmount"><o:number value="0.00" format="currency" /></th>
                        <th style="width: 100px;" class="right" id="grossAmount"><o:number value="${record.amounts.amount}" format="currency" /></th>
                    </tr>
                </c:when>
                <c:otherwise>
                    <tr>
                        <th colspan="<c:choose><c:when test="${record.type.stockAware}">4</c:when><c:otherwise>3</c:otherwise></c:choose>">
                            <fmt:message key="totalSum" />
                            (<fmt:message key="netTurnoverTaxGrossBegin"/> <o:number value="${record.amounts.taxRate}" format="tax"/>%
                            <c:if test="${amounts.reducedTaxAmount > 0}"><span> / </span><o:number value="${record.amounts.reducedTaxRate}" format="tax"/>%</c:if>
                            <span> <fmt:message key="netTurnoverTaxGrossEnd"/></span>)
                        </th>
                        <th class="right"><o:out value="${record.currencyKey}" /></th>
                        <th style="width: 100px;" class="right" id="netAmount"><o:number value="${record.amounts.amount}" format="currency" /></th>
                        <th class="right" id="taxAmount"><o:number value="${record.amounts.taxAmount + record.amounts.reducedTaxAmount}" format="currency" /></th>
                        <th style="width: 100px;" class="right" id="grossAmount"><o:number value="${record.amounts.grossAmount}" format="currency" /></th>
                    </tr>
                </c:otherwise>
            </c:choose>
        </c:if>
    </table>
</div>
