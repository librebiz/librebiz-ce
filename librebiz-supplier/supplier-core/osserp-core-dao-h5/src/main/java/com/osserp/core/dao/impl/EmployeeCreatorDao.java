/**
 *
 * Copyright (C) 2007, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 17, 2016 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.User;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.Address;
import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.ContactCreator;
import com.osserp.core.dao.EmployeeCreator;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Users;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeRole;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.employees.EmployeeStatus;
import com.osserp.core.employees.EmployeeType;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeCreatorDao extends AbstractTablesAwareSpringDao implements EmployeeCreator {
    private static Logger log = LoggerFactory.getLogger(EmployeeCreatorDao.class.getName());

    private static final Long EMPLOYEE_STATUS_DEFAULT = 3L;
    private static final Long EMPLOYEE_STATUS_EXECUTIVE = 2L;
    
    private PlatformTransactionManager transactionManager;
    private BranchOffices branchOffices;
    private ContactCreator contactCreator;
    private Employees employees;
    private OptionsCache cache;
    private Users users;

    public EmployeeCreatorDao(
            JdbcTemplate jdbcTemplate, 
            Tables tables,
            PlatformTransactionManager transactionManager, 
            OptionsCache cache, 
            Employees employees, 
            Users users,
            ContactCreator contactCreator,
            BranchOffices branchOffices) {
        super(jdbcTemplate, tables);
        this.transactionManager = transactionManager;
        this.cache = cache;
        this.contactCreator = contactCreator;
        this.employees = employees;
        this.users = users;
        this.branchOffices = branchOffices;
    }

    public Employee create(
            final Employee user,
            final Contact contact,
            final EmployeeType type,
            final BranchOffice branch,
            final EmployeeStatus status,
            final Date beginDate,
            final String initials,
            final String role) throws Exception {
        Long userId = user == null ? null : user.getId();
        Employee employee = createEmployee(userId, contact);
        employee.setBeginDate(beginDate);
        employee.setEmployeeType(type);
        employee.setInitials(initials);
        employee.setRole(role);
        employee.addRoleConfig(user, branch, null);
        employees.save(employee);
        return employee;
    }

    private Employee createEmployee(final Long createdBy, final Contact contact) throws Exception {

        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        Object result = transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction(TransactionStatus transactionStatus) {
                try {
                    Employee employee = (Employee) employees.create(createdBy, contact, null);
                    if (log.isDebugEnabled()) {
                        log.debug("createEmployee() employee created [id=" + employee.getId() + "]");
                    }
                    User user = users.create(createdBy, employee);
                    if (log.isInfoEnabled()) {
                        log.info("createEmployee() user created [id=" + user.getId() + "]");
                    }
                    return employee;

                } catch (Exception e) {
                    log.error("createEmployee() failed [message=" + e.getMessage() + ", type=" + e.getClass().getName() + "]", e);
                    transactionStatus.setRollbackOnly();
                    return new Exception(ErrorCode.UNKNOWN, e);
                }
            }
        });
        if (!(result instanceof Exception)) {
            reloadCache();
            return (Employee) result;
        }
        throw ((Exception) result);
    }

    public Employee createAdministrator(
            Employee setupUser,
            SystemCompany systemCompany,
            Long adminId,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            String emailAddress,
            String phoneNumber,
            String mobileNumber,
            boolean isExecutiveOfficer) throws ClientException {
        
        if (systemCompany == null || systemCompany.getContact() == null 
                || systemCompany.getContact().getAddress() == null) {
            throw new ClientException(ErrorCode.COMPANY_INVALID);
        }
        BranchOffice branchOffice = branchOffices.getHeadquarter();
        if (branchOffice == null || branchOffice.getCompany() == null
                || branchOffice.getContact() == null 
                || branchOffice.getContact().getContactId() == null 
                || !branchOffice.getContact().getContactId().equals(
                        systemCompany.getContact().getContactId())) {
            throw new ClientException(ErrorCode.SETUP_DB_HEADQUARTER);
        }
        Employee existing = (Employee) employees.find(adminId);
        if (existing != null) {
            if (log.isDebugEnabled()) {
                log.debug("createAdministrator() user already exists [id=" + adminId + "]");
            }
            return existing;
        }
        try {
            Address address = systemCompany.getContact().getAddress();
            Contact contact = contactCreator.create(
                    Constants.SYSTEM_EMPLOYEE,
                    Contact.TYPE_PRIVATE,
                    salutation,
                    title,
                    firstName,
                    lastName,
                    null, // spouseSalutation
                    null, // spouseTitle
                    null, // spouseFirstName
                    null, // spouseLastName
                    null, // customSalutation
                    null, // customAddressName
                    null, // birthDate
                    address.getStreet(), 
                    null, // streetAddon
                    address.getZipcode(),
                    address.getCity(), 
                    null, // federalState
                    null, // federalStateName
                    address.getCountry(),
                    emailAddress,
                    phoneNumber,
                    null, // faxNumber
                    mobileNumber,
                    Contact.STATUS_OK);
            
            Employee admin = createEmployee(Constants.SYSTEM_EMPLOYEE, contact);
            
            // add headquarter role
            admin.addRoleConfig(setupUser, branchOffice, null);
            employees.save(admin);
            reloadCache();
            admin = employees.get(admin.getId());
            
            List<EmployeeGroup> groups = new ArrayList(cache.getList(Options.EMPLOYEE_GROUPS));
            EmployeeRoleConfig hqRole = admin.getDefaultRoleConfig();
            EmployeeStatus employeeStatus = (EmployeeStatus) cache.getMapped(Options.EMPLOYEE_STATUS, EMPLOYEE_STATUS_DEFAULT);
            if (isExecutiveOfficer) {
                log.debug("createAdministrator() admin is executive...");
                employeeStatus = (EmployeeStatus) cache.getMapped(Options.EMPLOYEE_STATUS, EMPLOYEE_STATUS_EXECUTIVE);
                for (int i = 0, j = groups.size(); i < j; i++) {
                    EmployeeGroup next = groups.get(i);
                    if (next.isExecutiveCompany()) {
                        admin.addRole(hqRole, setupUser, next, employeeStatus);
                        break;
                    }
                }
            } 
            for (int i = 0, j = groups.size(); i < j; i++) {
                EmployeeGroup next = groups.get(i);
                if (next.isApplicationAdmin()) {
                    admin.addRole(hqRole, setupUser, next, employeeStatus);
                    break;
                }
            }
            employees.save(admin);
            reloadCache();
            admin = employees.get(admin.getId());
            
            for (int i = 0, j = admin.getRoleConfigs().size(); i < j; i++) {
                EmployeeRoleConfig config = admin.getRoleConfigs().get(i);
                for (int k = 0, l = config.getRoles().size(); k < l; k++) {
                    EmployeeRole role = config.getRoles().get(k);
                    if (role.getGroup() != null) {
                        EmployeeGroup next = role.getGroup();
                        users.grantPermissions(setupUser, admin, next);
                    }
                }
            }
            reloadCache();
            return employees.get(admin.getId());
        } catch (Exception e) {
            throw new ClientException(e.getMessage(), e);
        }
    }
    
    private void reloadCache() {
        try {
            cache.refresh(Options.EMPLOYEES);
            cache.refresh(Options.EMPLOYEE_KEYS);

        } catch (Exception e) {
            log.error("reloadCache() failed [message=" + e.getMessage() + ", type=" + e.getClass().getName() + "]");
        }
    }

}
