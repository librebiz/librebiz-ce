/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 28, 2006 3:55:24 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.Item;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.PurchaseInvoices;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.dao.records.PurchasePayments;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Stocktaking;
import com.osserp.core.finance.StocktakingItem;
import com.osserp.core.model.records.PurchaseInvoiceImpl;
import com.osserp.core.model.records.PurchaseInvoiceItemChangedInfoImpl;
import com.osserp.core.products.Product;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceType;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseInvoicesDao extends AbstractPurchaseInvoices implements PurchaseInvoices {
    private static Logger log = LoggerFactory.getLogger(PurchaseInvoicesDao.class.getName());
    private static final String DELETE_PERMISSION_DEFAULT = "executive_accounting,purchase_invoice_delete";
    private PurchaseOrders purchaseOrders = null;
    private ProductSummaryCache productSummaryCache = null;

    public PurchaseInvoicesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            ProductSummaryCache productSummaryCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Suppliers suppliers,
            PurchaseOrders purchaseOrders,
            Long kanbanSupplierId,
            PurchasePayments purchasePayments) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                suppliers,
                TableKeys.PURCHASE_INVOICES,
                TableKeys.PURCHASE_INVOICE_ITEMS,
                purchasePayments);
        this.productSummaryCache = productSummaryCache;
        this.purchaseOrders = purchaseOrders;
        initializeDeletePermissions(DELETE_PERMISSION_DEFAULT);
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.PURCHASE_INVOICE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.PurchaseInvoices#create(com.osserp.core.employees.Employee, com.osserp.core.purchasing.PurchaseOrder)
     */
    public PurchaseInvoice create(Employee user, PurchaseOrder order) {
        RecordType recordType = getRecordType();
        PurchaseInvoiceImpl obj = new PurchaseInvoiceImpl(
                getNextRecordId(user, order.getCompany(), recordType, null),
                recordType,
                (PurchaseInvoiceType) getBookingType(PurchaseInvoiceType.GOODS),
                order,
                user);
        addItems(user, obj, order);
        if (!Constants.DEFAULT_CURRENCY.equals(order.getCurrency())) {
            obj.setCurrency(Constants.DEFAULT_CURRENCY);
            for (int i = 0, j = obj.getItems().size(); i < j; i++) {
                Item item = obj.getItems().get(i);
                item.setPrice(getDecimal(0));
            }
            obj.calculateSummary();
        }
        saveInitial(obj);
        return obj;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.PurchaseInvoices#create(com.osserp.core.employees.Employee, java.lang.Long, java.lang.Long, java.lang.Long)
     */
    public PurchaseInvoice create(Employee user, Long company, Long branchId, Long bookingType) throws ClientException {
        Supplier supplier = getInternalSupplier(company);
        if (!supplier.isActivated()) {
            throw new ClientException(ErrorCode.SUPPLIER_DEACTIVATED);
        }
        // param sale is set to null due to internal inventory booking is not referenced by sale
        return create(user, company, branchId, bookingType, supplier, null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.records.PurchaseInvoices#create(com.osserp.core.employees.Employee, java.lang.Long, java.lang.Long, java.lang.Long,
     * com.osserp.core.suppliers.Supplier, java.lang.Long)
     */
    public PurchaseInvoice create(
            Employee user,
            Long company,
            Long branchId,
            Long bookingType,
            Supplier supplier,
            Long sale) {
        return create(user, company, branchId, getBookingType(bookingType), supplier, sale);
    }

    public PurchaseInvoice create(Employee user, Stocktaking st) {
        Supplier supplier = null;
        try {
            supplier = getInternalSupplier(st.getCompany());
        } catch (ClientException e) {
            throw new BackendException("did not find supplier of company " 
                    + st.getCompany());
        }
        BookingType bType = getBookingType(PurchaseInvoiceType.CORRECTION);
        PurchaseInvoiceImpl obj = create(
                user,
                st.getCompany(),
                st.getBranchId(),
                bType,
                supplier,
                null); // stocktaking correction is not referenced by sale 
        List<StocktakingItem> list = st.getItems();
        for (int i = 0, j = list.size(); i < j; i++) {
            StocktakingItem next = list.get(i);
            if (!next.getExpectedQuantity().equals(next.getCountedQuantity())) {
                double quantity = next.getCountedQuantity() - next.getExpectedQuantity();
                BigDecimal price = getDecimal(next.getAveragePurchasePrice());
                obj.addItem(
                        st.getCompany(),
                        next.getProduct(),
                        null,
                        quantity,
                        next.getProduct().isReducedTax() ?
                                obj.getAmounts().getReducedTaxRate() :
                                    obj.getAmounts().getTaxRate(),
                        price,
                        new Date(), 
                        price, 
                        false, 
                        false, 
                        price, 
                        null, 
                        true, 
                        null);
            }
        }
        obj.setItemsEditable(false);
        saveInitial(obj);
        return obj;
    }

    public PurchaseInvoice createCreditNote(Employee user, PurchaseInvoice invoice, String customHeader) {
        PurchaseInvoiceImpl obj = new PurchaseInvoiceImpl(
            getNextRecordId(user, invoice.getCompany(), invoice.getType(), null),
            (PurchaseInvoiceType) getBookingType(PurchaseInvoiceType.CREDIT_NOTE),
            invoice,
            user);
        if (customHeader != null) {
            obj.setCustomHeader(customHeader);
        }
        saveInitial(obj);
        return obj;
    }

    public ItemChangedInfo createItemChangedInfo(ItemChangedInfo notPersistentInfo) {
        try {
            ItemChangedInfo persistent = new PurchaseInvoiceItemChangedInfoImpl(notPersistentInfo);
            getCurrentSession().saveOrUpdate(PurchaseInvoiceItemChangedInfoImpl.class.getName(), persistent);
            return persistent;
        } catch (Exception e) {
            log.error("createItemChangedInfo() failed [message=" + e.getMessage() + "]", e);
        }
        return null;
    }

    public Product createValuation(Employee user, Product product, Long company, Long branchId, Double valuation) throws ClientException {
        productSummaryCache.loadSummary(product);
        if (product.getSummary().getStock() < 1) {
            throw new ClientException(ErrorCode.STOCK_EMPTY);
        }
        if (product.getSummary().getReceipt() > 0) {
            throw new ClientException(ErrorCode.PURCHASE_RECEIPT_FOUND);
        }
        Double lastPurchasePrice = product.getSummary().getLastPurchasePrice();
        PurchaseInvoice invoice = create(
                user,
                company,
                branchId,
                getBookingType(PurchaseInvoiceType.VALUATION),
                getInternalSupplier(company),
                null); // product valuation is not referenced by sale
        BigDecimal val = new BigDecimal(valuation);
        invoice.addItem(
                product.getSummary().getStockId(),
                product,
                null, // customName
                product.getSummary().getStock() * -1,
                product.isReducedTax() ? reducedTaxRate : taxRate,
                new BigDecimal(lastPurchasePrice),
                new Date(),
                val,
                false,
                false,
                val,
                null,
                true,
                null);
        invoice.addItem(
                product.getSummary().getStockId(),
                product,
                null, // customName
                product.getSummary().getStock(),
                product.isReducedTax() ? reducedTaxRate : taxRate,
                val,
                new Date(),
                val,
                false,
                false,
                val,
                null,
                true,
                null);
        invoice.updateStatus(PurchaseInvoice.STAT_NEW);
        saveInitial(invoice);
        Product updated = productSummaryCache.refreshAndLoad(product);
        if (log.isDebugEnabled()) {
            log.debug("createValuation() done [user=" + user.getId()
                    + ", product=" + updated.getProductId()
                    + ", company=" + company
                    + ", oldLastPurchasePrice=" + lastPurchasePrice
                    + ", newLastPurchasePrice=" + updated.getSummary().getLastPurchasePrice()
                    + ", valuation=" + valuation
                    + "]");
        }
        return updated;
    }

    public List<RecordDisplay> findOpen(Date from, Date til) {
        return getRecords("status < " + PurchaseInvoice.STAT_BOOKED, from, til);
    }

    public List<RecordDisplay> findClosed(Date from, Date til) {
        return getRecords("status >= " + PurchaseInvoice.STAT_BOOKED, from, til);
    }

    protected List<RecordDisplay> getRecords(String statusStatement, Date from, Date til) {
        StringBuilder query = new StringBuilder("SELECT ");
        query
                .append(RecordDisplayRowMapper.RECORD_VALUES)
                .append(" FROM v_")
                .append(getQueryContext())
                .append("_query WHERE ")
                .append(statusStatement);

        Object[] params = null;
        int[] types = null;
        if (from != null || til != null) {
            if (from != null && til != null) {
                query.append(" AND created >= ? AND created <= ?");
                params = new Object[] { from, til };
                types = new int[] { Types.TIMESTAMP, Types.TIMESTAMP };
            } else if (from != null) {
                query.append(" AND created >= ?");
                params = new Object[] { from };
                types = new int[] { Types.TIMESTAMP };
            } else {
                query.append(" AND created <= ?");
                params = new Object[] { til };
                types = new int[] { Types.TIMESTAMP };
            }
        }
        query.append(" ORDER BY created desc");
        List<Map<String, Object>> result = (List<Map<String, Object>>)
                (params == null ?
                        jdbcTemplate.query(
                                query.toString(),
                                RecordDisplayRowMapper.getReader())
                        : jdbcTemplate.query(
                                query.toString(),
                                params,
                                types,
                                RecordDisplayRowMapper.getReader()));
        return RecordDisplayRowMapper.createDisplayList(getOptionsCache(), result);
    }

    public void reopenClosed(Employee user, PurchaseInvoice invoice) {
        if (invoice == null || !(invoice instanceof PurchaseInvoiceImpl)) {
            throw new BackendException("[message=invalid.param, expectedClass="
                    + PurchaseInvoiceImpl.class.getName() + ", foundClass="
                    + (invoice == null ? "null" : invoice.getClass().getName()) + "]");
        }
        PurchaseInvoiceImpl obj = (PurchaseInvoiceImpl) invoice;
        obj.reopen(user);
        save(obj);
    }

    private void addItems(Employee user, PurchaseInvoiceImpl obj, PurchaseOrder order) {

        List<DeliveryNote> deliveryNotes = order.getDeliveryNotes();
        if (deliveryNotes.isEmpty()) {

            obj.addItems(order.getItems());
            // products could be added or deleted
            obj.setItemsChangeable(true);

        } else {
            Map<Long, Item> orderedItems = getOrdered(order.getItems());
            List<Item> toAdd = new ArrayList<Item>();
            HashMap<Long, Item> deliveredProducts = new HashMap<Long, Item>();
            for (int i = 0, j = deliveryNotes.size(); i < j; i++) {
                DeliveryNote note = deliveryNotes.get(i);
                for (int k = 0, l = note.getItems().size(); k < l; k++) {
                    Item item = note.getItems().get(k);
                    if (deliveredProducts.containsKey(item.getProduct().getProductId())) {
                        Item alreadyDelivered = deliveredProducts.get(item.getProduct().getProductId());
                        alreadyDelivered.setQuantity(alreadyDelivered.getQuantity() + item.getQuantity());
                        updateDelivered(toAdd, item);
                    } else {
                        deliveredProducts.put(item.getProduct().getProductId(), (Item) item.clone());
                        addPriceFromOrder(orderedItems, item);
                        toAdd.add(item);
                    }
                }
            }
            obj.addItems(toAdd);
            createOverflowOrder(user, order, deliveredProducts);
            // no items could be added or deleted
            obj.setItemsChangeable(false);
        }
        // products could always be edited to change price and quantity
        obj.setItemsEditable(true);
    }

    private void updateDelivered(List<Item> delivered, Item item) {
        for (int i = 0, j = delivered.size(); i < j; i++) {
            Item current = delivered.get(i);
            if (current.getProduct().getProductId().equals(
                    item.getProduct().getProductId())) {
                current.setQuantity(Double.valueOf(current.getQuantity().doubleValue()
                        + item.getQuantity().doubleValue()));
            }
        }
    }

    private void addPriceFromOrder(Map<Long, Item> ordered, Item toUpdate) {
        if (ordered.containsKey(toUpdate.getProduct().getProductId())) {
            Item fromOrder = ordered.get(toUpdate.getProduct().getProductId());
            toUpdate.setPrice(fromOrder.getPrice());
            if (log.isDebugEnabled()) {
                log.debug("addPriceFromOrder() set price done [product="
                        + toUpdate.getProduct().getProductId()
                        + ", price=" + fromOrder.getPrice()
                        + "]");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("addPriceFromOrder() ordered does not contain item [product=" + toUpdate.getProduct().getProductId() + "]");
        }
    }

    private Map<Long, Item> getOrdered(List<Item> items) {
        Map<Long, Item> result = new HashMap<Long, Item>();
        for (int i = 0, j = items.size(); i < j; i++) {
            Item item = items.get(i);
            result.put(item.getProduct().getProductId(), item);
        }
        return result;
    }

    private void createOverflowOrder(
            Employee user,
            PurchaseOrder order,
            Map<Long, Item> delivered) {

        List<Item> ordered = order.getItems();
        List<Item> carryOver = new ArrayList<Item>();
        for (int i = 0, j = ordered.size(); i < j; i++) {
            Item orderedItem = ordered.get(i);
            if (delivered.containsKey(orderedItem.getProduct().getProductId())) {
                Item nextDelivered = delivered.get(orderedItem.getProduct().getProductId());
                double overflow = orderedItem.getQuantity() - nextDelivered.getQuantity();
                if (overflow > 0) {
                    Item open = (Item) orderedItem.clone();
                    open.setQuantity(overflow);
                    carryOver.add(open);
                }
            } else {
                carryOver.add(orderedItem);
            }
        }
        if (!carryOver.isEmpty()) {
            purchaseOrders.create(user, order, carryOver);
        }
    }

    private PurchaseInvoiceImpl create(
            Employee user,
            Long company,
            Long branchId,
            BookingType bookingType,
            Supplier supplier,
            Long sale) {
        RecordType recordType = getRecordType();
        PurchaseInvoiceImpl obj = new PurchaseInvoiceImpl(
                getNextRecordId(user, company, recordType, null),
                recordType,
                (PurchaseInvoiceType) bookingType,
                company,
                branchId,
                supplier,
                user,
                sale,
                taxRate,
                reducedTaxRate);
        saveInitial(obj);
        if (log.isDebugEnabled()) {
            log.debug("create() done [invoice=" + obj.getId() + "]");
        }
        return obj;
    }

    public PurchaseInvoice create(
            Employee user, 
            Long company, 
            Long branchId, 
            BookingType bookingType, 
            Supplier supplier, 
            Date recordDate,
            Long recordNumber,
            Date invoiceDate,
            String invoiceNumber, 
            Long productStockId, 
            Product product,
            String customName,
            String productNote, 
            Double productQuantity, 
            BigDecimal productPrice)
            throws ClientException {
        
        RecordType recordType = getRecordType();
        
        Long recordId = recordNumber;
        if (!isEmpty(recordId) && exists(recordId)) {
            throw new ClientException(ErrorCode.ID_EXISTS);
        }
        if (isEmpty(recordId)) {
            recordId = getNextRecordId(user, company, recordType, recordDate);
        }
        
        PurchaseInvoiceImpl obj = new PurchaseInvoiceImpl(
                recordId,
                recordType,
                (PurchaseInvoiceType) bookingType,
                company,
                branchId,
                supplier,
                user,
                taxRate,
                reducedTaxRate,
                recordDate,
                invoiceDate,
                invoiceNumber,
                company,
                product,
                customName,
                productNote,
                productQuantity,
                productPrice);
        saveInitial(obj);
        if (log.isDebugEnabled()) {
            log.debug("create() done [invoice=" + obj.getId() + "]");
        }
        return obj;
    }

    public PurchaseInvoice create(
            Employee user, 
            PurchaseInvoice existing, 
            Date recordDate, 
            Long recordNumber, 
            Date invoiceDate, 
            String invoiceNumber)
            throws ClientException {
        
        RecordType recordType = existing.getType();
        
        Long recordId = recordNumber;
        if (!isEmpty(recordId) && exists(recordId)) {
            throw new ClientException(ErrorCode.ID_EXISTS);
        }
        if (isEmpty(recordId)) {
            recordId = getNextRecordId(user, existing.getCompany(), recordType, recordDate);
        }
        
        PurchaseInvoiceImpl obj = new PurchaseInvoiceImpl(
                recordId,
                existing,
                user,
                recordDate,
                invoiceDate,
                invoiceNumber);
        saveInitial(obj);
        if (log.isDebugEnabled()) {
            log.debug("create() done [invoice=" + obj.getId() + "]");
        }
        return obj;
    }

    @Override
    protected void loadPayments(PaymentAwareRecord record) {
        if (isSupportingPayments()) {
            PurchaseInvoiceImpl invoice = (PurchaseInvoiceImpl) record;
            List<Payment> list = null;
            if (invoice.getReference() == null) {
                list = getPayments().getByRecord(invoice);
            } else {
                list = getPayments().getByReference(invoice.getReference());
            }
            for (int i = 0, j = list.size(); i < j; i++) {
                Payment next = list.get(i);
                if (!invoice.isItemsChangeable() && invoice.getReference() != null) {
                    // invoice is a project based invoice
                    invoice.getPayments().add(next);
                } else if (next.getRecordId().equals(invoice.getId())) {
                    // invoice is standalone
                    invoice.getPayments().add(next);
                }
            }
        }
    }

    private Supplier getInternalSupplier(Long company) throws ClientException {
        return getSuppliers().findByCompany(company);
    }

    @Override
    protected Class<PurchaseInvoiceImpl> getPersistentClass() {
        return PurchaseInvoiceImpl.class;
    }

    @Override
    protected Class<PurchaseInvoiceItemChangedInfoImpl> getItemChangedInfoClass() {
        return PurchaseInvoiceItemChangedInfoImpl.class;
    }

    @Override
    protected Long getDefaultBookingTypeId() {
        return PurchaseInvoiceType.GOODS;
    }
}
