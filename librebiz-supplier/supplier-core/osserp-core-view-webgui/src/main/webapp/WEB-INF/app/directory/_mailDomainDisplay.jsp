<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="domain" value="${view.bean}"/>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><o:out value="${domain.name}" /></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:if test="${!empty domain.provider}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                <fmt:message key="provider"/>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.provider}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty domain.domainCreated}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""><fmt:message key="registered"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:date value="${domain.domainCreated}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty domain.domainExpires}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""><fmt:message key="til"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:date value="${domain.domainExpires}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="mailReceipt"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${domain.aliasOnly}">
                                <fmt:message key="aliasOnlyLabel"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="mailboxDomainPrimary"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${domain.aliasOnly}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""><fmt:message key="aliasOfLabel"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.parentDomain.name}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty domain.smtpServerName}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">SMTP Hostname</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.smtpServerName}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty domain.hostMx}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">MX Hostname</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.hostMx}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty domain.hostMxIp}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">MX IP</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.hostMxIp}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty domain.hostSecondaryMx}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Secondary MX</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.hostSecondaryMx}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty domain.hostSecondaryMxIp}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Secondary MX IP</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.hostSecondaryMxIp}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Fetchmail</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <fmt:message key="${view.fetchmailStatus}"/>
                    </div>
                </div>
            </div>

            <c:if test="${!empty domain.serverName}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fetchmail Server</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.serverName}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty domain.serverType}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""><fmt:message key="mailboxType"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.serverType}"/>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${!empty domain.fetchMode}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fetch Mode</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${domain.fetchMode}"/>
                        </div>
                    </div>
                </div>
            </c:if>

        </div>
    </div>
</div>
