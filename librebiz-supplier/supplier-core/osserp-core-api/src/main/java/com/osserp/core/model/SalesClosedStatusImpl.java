/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 11-Jan-2011 13:26:17 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.beans.OptionImpl;

import com.osserp.core.sales.SalesClosedStatus;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesClosedStatusImpl extends OptionImpl implements SalesClosedStatus {

    private boolean defaultStatus = false;
    private String permissions = null;
    private boolean ignoreDeliveries = false;
    private boolean ignoreInvoices = false;
    private boolean ignoreOrders = false;

    public SalesClosedStatusImpl() {
        super();
    }

    public boolean isDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(boolean defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public boolean isIgnoreDeliveries() {
        return ignoreDeliveries;
    }

    public void setIgnoreDeliveries(boolean ignoreDeliveries) {
        this.ignoreDeliveries = ignoreDeliveries;
    }

    public boolean isIgnoreInvoices() {
        return ignoreInvoices;
    }

    public void setIgnoreInvoices(boolean ignoreInvoices) {
        this.ignoreInvoices = ignoreInvoices;
    }

    public boolean isIgnoreOrders() {
        return ignoreOrders;
    }

    public void setIgnoreOrders(boolean ignoreOrders) {
        this.ignoreOrders = ignoreOrders;
    }

}
