/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 03, 2009 13:51:43 PM 
 * 
 */
package com.osserp.core.model.hrm;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecordMarkerType;

/**
 * 
 * @author jg <jg@osserp.com>
 * 
 */
public class TimeRecordMarkerTypeImpl extends AbstractOption implements TimeRecordMarkerType {

    private boolean eol = false;
    private boolean expireMonth = false;
    private boolean expireDay = false;
    private boolean expireLeave = false;
    private Integer orderId = 0;

    protected TimeRecordMarkerTypeImpl() {
        super();
    }

    public TimeRecordMarkerTypeImpl(Employee user, Long id, String name, String description, Integer orderId) {
        super(id, (Long) null, name, description, null, user.getId());
        this.orderId = orderId;
    }

    public boolean isEol() {
        return eol;
    }

    public void setEol(boolean eol) {
        this.eol = eol;
    }

    public boolean isExpireMonth() {
        return expireMonth;
    }

    public void setExpireMonth(boolean expireMonth) {
        this.expireMonth = expireMonth;
    }

    public boolean isExpireDay() {
        return expireDay;
    }

    public void setExpireDay(boolean expireDay) {
        this.expireDay = expireDay;
    }

    public boolean isExpireLeave() {
        return expireLeave;
    }

    public void setExpireLeave(boolean expireLeave) {
        this.expireLeave = expireLeave;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
}
