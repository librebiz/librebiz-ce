/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode

import com.osserp.core.system.SystemCompany
import com.osserp.core.system.SystemCompanyManager

import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
public class BankAccountsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(BankAccountsView.class.getName())

    BankAccountsView() {
        dependencies = ['clientCompanyView']
        headerName = 'bankAccountsLabel'
        enablePopupView()
        providesEditMode()
        providesCreateMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!env.clientCompanyView?.bean) {
            logger.error('initRequest: client not bound')
            throw new ViewContextException(this)
        }
        if (!editMode && !createMode && !bean) {
            def client = systemCompanyManager.find(env.clientCompanyView.bean.id)
            list = client?.bankAccounts ?: []
        }
    }

    @Override
    void save() {
        SystemCompany client = systemCompanyManager.find(env.clientCompanyView.bean.id)

        String shortkey = form.getString('shortkey')
        String name = form.getString('name')
        String nameAddon = form.getString('nameAddon')
        String bankAccountNumberIntl = form.getString('bankAccountNumberIntl')
        String bankIdentificationCodeIntl = form.getString('bankIdentificationCodeIntl')
        boolean useInDocumentFooter = form.getBoolean('useInDocumentFooter')
        boolean taxAccount = form.getBoolean('taxAccount')

        if (createMode) {
            systemCompanyManager.addBankAccount(
                    domainEmployee,
                    client,
                    shortkey,
                    name,
                    nameAddon,
                    bankAccountNumberIntl,
                    bankIdentificationCodeIntl,
                    useInDocumentFooter,
                    taxAccount)
            disableCreateMode()
        } else if (editMode) {
            systemCompanyManager.updateBankAccount(
                    client,
                    bean.id,
                    shortkey,
                    name,
                    nameAddon,
                    bankAccountNumberIntl,
                    bankIdentificationCodeIntl,
                    useInDocumentFooter,
                    taxAccount)
            bean.shortkey = shortkey
            bean.name = name
            bean.nameAddon = nameAddon
            bean.bankAccountNumberIntl = bankAccountNumberIntl
            bean.bankIdentificationCodeIntl = bankIdentificationCodeIntl
            bean.useInDocumentFooter = useInDocumentFooter
            bean.taxAccount = taxAccount
            disableEditMode()
        }
        list = client?.bankAccounts ?: []
        env.clientCompanyView.reload()
    }

    void delete() {
        SystemCompany client = systemCompanyManager.find(env.clientCompanyView.bean.id)
        def id = form.getLong('id')
        systemCompanyManager.removeBankAccount(client, id)
        list = client?.bankAccounts ?: []
        env.clientCompanyView.reload()
    }
    
    protected SystemCompanyManager getSystemCompanyManager() {
        getService(SystemCompanyManager.class.getName())
    }
}
