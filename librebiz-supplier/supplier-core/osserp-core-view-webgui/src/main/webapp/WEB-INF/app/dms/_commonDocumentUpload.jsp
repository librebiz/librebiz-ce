<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource />

<c:set var="view" value="${requestScope.dmsDocumentView}" />
<c:set var="deletePermissions" value="${requestScope.dmsDeletePermissions}" />
<c:set var="referencePermissions" value="${requestScope.dmsReferencePermissions}" />

<%-- replace by view.baseLink --%>
<c:set var="dmsUrl" value="${requestScope.dmsDocumentUrl}" />

<c:choose>
    <c:when test="${!empty view.bean}">
        <c:import url="${viewdir}/dms/_documentUpdateMetadata.jsp" />
    </c:when>
    <c:when test="${view.galleryMode}">
        <div class="col-md-12 panel-area">
            <div class="row" id="galleryRow">
                <c:import url="${viewdir}/dms/_galleryDisplay.jsp" />
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="col-md-12 panel-area">
            <v:form url="${view.baseLink}/save" multipart="true">
                <div class="table-responsive table-responsive-default">
                    <table class="table table-striped">
                        <c:choose>
                            <c:when test="${empty view.list}">
                                <tbody>
                                    <c:import url="${viewdir}/dms/_commonDocumentUploadColumn.jsp" />
                                    <c:import url="${viewdir}/dms/_commonDocumentImports.jsp" />
                                </tbody>
                            </c:when>
                            <c:otherwise>
                                <thead>
                                    <tr>
                                        <th class="doc"><fmt:message key="${view.documentTypeName}" /></th>
                                        <th class="size"><fmt:message key="size" /></th>
                                        <th class="created"><fmt:message key="date" /></th>
                                        <th class="employee"><fmt:message key="uploadBy" /></th>
                                        <c:if test="${view.referenceSupport}">
                                            <th class="action"><fmt:message key="reference" /></th>
                                        </c:if>
                                        <th class="action" colspan="2"><fmt:message key="action" /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:import url="${viewdir}/dms/_commonDocumentUploadColumn.jsp" />
                                    <c:forEach items="${view.list}" var="doc">
                                        <tr>
                                            <td class="doc">
                                                <o:doc obj="${doc}">
                                                    <c:choose>
                                                        <c:when test="${doc.thumbnailAvailable}">
                                                            <oc:thumbnail value="${doc}" url="${view.baseLink}/thumbnail" height="${view.thumbnailHeightList}" width="auto" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <o:out value="${doc.displayName}" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </o:doc>
                                            </td>
                                            <td class="size"><o:number value="${doc.fileSize}" format="bytes" /></td>
                                            <td class="created"><o:date value="${doc.displayDate}" /></td>
                                            <td class="employee"><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${doc.createdBy}">
                                                    <oc:employee initials="true" value="${doc.createdBy}" />
                                                </v:ajaxLink></td>
                                            <c:if test="${view.referenceSupport}">
                                                <td class="action">
                                                    <o:permission role="${referencePermissions}" info="permissionDocumentsReference">
                                                        <v:link url="${view.baseLink}/setReference?id=${doc.id}" title="setAsReference">
                                                            <c:choose>
                                                                <c:when test="${doc.referenceDocument}">
                                                                    <o:img name="enabledIcon" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <o:img name="toggleStoppedFalseIcon" />
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </v:link>
                                                    </o:permission>
                                                    <o:forbidden role="${referencePermissions}" info="permissionDocumentsReference">
                                                        <c:choose>
                                                            <c:when test="${doc.referenceDocument}">
                                                                <o:img name="enabledIcon" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <o:img name="toggleStoppedFalseIcon" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </o:forbidden>
                                                </td>
                                            </c:if>
                                            <td class="action icon"><o:permission role="${deletePermissions}" info="permissionDocumentsDelete">
                                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="${view.baseLink}/delete?id=${doc.id}" />');" title="<fmt:message key="delete"/>"><o:img name="deleteIcon" /></a>
                                                </o:permission> <o:forbidden role="${deletePermissions}" info="permissionDocumentsDelete">
                                                    <c:choose>
                                                        <c:when test="${doc.createdBy == view.user.id}">
                                                            <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="${view.baseLink}/delete?id=${doc.id}" />');" title="<fmt:message key="delete"/>"><o:img name="deleteIcon" /></a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <o:img name="toggleStoppedFalseIcon" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </o:forbidden></td>
                                            <td class="action icon">
                                                <v:link url="${view.baseLink}/select?id=${doc.id}" title="documentEdit">
                                                    <o:img name="writeIcon" />
                                                </v:link></td>
                                        </tr>
                                    </c:forEach>
                                    <c:import url="${viewdir}/dms/_commonDocumentImports.jsp" />
                                </tbody>
                            </c:otherwise>
                        </c:choose>
                    </table>
                </div>
            </v:form>
        </div>
    </c:otherwise>
</c:choose>
