/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 9, 2014 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordSummary extends AbstractOption {
    
    private RecordType type = null;
    private Long typeId = null;
    private RecordType referenceType = null;
    private Long contactReference = null;
    private Long status = null;
    private boolean taxFree = false;
    private Double amount = null;
    private Double tax = null;
    private Double reducedTax = null;
    private Double gross = null;
    private Long sales = null;
    private boolean canceled = false;
    private boolean internal = false;
    private SystemCompany company;
    private BranchOffice branch;
    private Double paidAmount = null;
    private Double taxRate = 1d;
    private String note = null;
    private Long documentId = null;
    private Long bankAccountId = null;

    /**
     * Default constructor adding serialializable capability to entities
     */
    protected RecordSummary() {
        super();
    }

    /**
     * Creates a new salesRecordSummary value object
     * @param id
     * @param type
     * @param reference
     * @param referenceType
     * @param contactReference
     * @param name
     * @param status
     * @param created
     * @param createdBy
     * @param taxFree
     * @param amount
     * @param tax
     * @param reducedTax
     * @param gross
     * @param sales
     * @param canceled
     * @param internal
     * @param company
     * @param branch
     * @param paidAmount
     * @param taxRate optional taxRate required by payments only
     */
    public RecordSummary(
            Long id,
            RecordType type, 
            Long reference, 
            RecordType referenceType, 
            Long contactReference,
            String name,
            Long status, 
            Date created,
            Long createdBy,
            boolean taxFree, 
            Double amount, 
            Double tax, 
            Double reducedTax,
            Double gross, 
            Long sales, 
            boolean canceled, 
            boolean internal,
            SystemCompany company,
            BranchOffice branch,
            Double paidAmount,
            Double taxRate) {
        super(id, reference, name, created, createdBy);
        this.type = type;
        this.typeId = type.getId();
        this.referenceType = referenceType;
        this.contactReference = contactReference;
        this.status = status;
        this.taxFree = taxFree;
        this.amount = amount;
        this.tax = tax;
        this.reducedTax = reducedTax;
        this.gross = gross;
        this.sales = sales;
        this.canceled = canceled;
        this.internal = internal;
        this.company = company;
        this.branch = branch;
        this.paidAmount = paidAmount;
        if (isSet(taxRate)) {
            this.taxRate = taxRate;
        }
    }

    public RecordType getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(RecordType referenceType) {
        this.referenceType = referenceType;
    }

    public boolean isUnchangeable() {
        return (status == null) ? false : status >= 3;
    }

    public boolean isPayment() {
        return Records.isPayment(typeId);
    }

    public boolean isBooksOnly() {
        return false;
    }
    
    public boolean isOutpayment() {
        return Records.isOutpayment(typeId);
    }
    
    public boolean isAmountOpen() {
        return (getOpenAmount() > 0);
    }
    
    public Double getOpenAmount() {
        if (type != null && RecordType.SALES_CREDIT_NOTE.equals(type.getId())) {
            return fetchDouble(gross) + fetchDouble(paidAmount);
        }
        return fetchDouble(gross) - fetchDouble(paidAmount);
    }

    public boolean isCanceled() {
        return canceled;
    }

    public Long getContactReference() {
        return contactReference;
    }

    public boolean isInternal() {
        return internal;
    }

    public Long getSales() {
        return sales;
    }

    public Long getStatus() {
        return status;
    }

    public Double getAmount() {
        return amount;
    }

    public Double getGross() {
        return gross;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public Double getReducedTax() {
        return reducedTax;
    }

    public Double getTax() {
        return tax;
    }

    public Double getTotalTax() {
        return (isSet(tax) ? tax : 0d) 
                + (isSet(reducedTax) ? reducedTax : 0d);
    }

    public boolean isTaxFree() {
        return taxFree;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public String getNumber() {
        return Records.createNumber(type, getId(), internal);
    }
    
    public String getReferenceNumber() {
        if (getReferenceType() == null && getReference() == null) {
            return null;
        }
        return Records.createNumber(referenceType, getReference(), internal);
    }

    protected void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    protected void setTaxFree(boolean taxFree) {
        this.taxFree = taxFree;
    }

    protected void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    protected void setContactReference(Long contactReference) {
        this.contactReference = contactReference;
    }

    protected void setInternal(boolean internal) {
        this.internal = internal;
    }

    protected void setSales(Long sales) {
        this.sales = sales;
    }

    protected void setStatus(Long status) {
        this.status = status;
    }

    protected void setAmount(Double amount) {
        this.amount = amount;
    }

    protected void setGross(Double gross) {
        this.gross = gross;
    }

    protected void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    protected void setReducedTax(Double reducedTax) {
        this.reducedTax = reducedTax;
    }

    protected void setTax(Double tax) {
        this.tax = tax;
    }

    public RecordType getType() {
        return type;
    }

    protected void setType(RecordType type) {
        this.type = type;
    }

    public void addRecordType(RecordType recordType) {
        this.type = recordType;
    }

    public Long getTypeId() {
        return typeId;
    }

    protected void setTypeId(Long typeId) {
        this.typeId = typeId;
    }
    
    public SystemCompany getCompany() {
        return company;
    }

    protected void setCompany(SystemCompany company) {
        this.company = company;
    }

    public BranchOffice getBranch() {
        return branch;
    }

    protected void setBranch(BranchOffice branch) {
        this.branch = branch;
    }
    
    public String getNote() {
        return note;
    }

    protected void setNote(String note) {
        this.note = note;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public boolean isCommonPayment() {
        return false;
    }

    public boolean isPurchaseRecord() {
        return false;
    }
    
    public boolean isSalesRecord() {
        return false;
    }

    @Override
    public Object clone() {
        throw new com.osserp.common.BackendException("CloneNotSupported");
    }
}
