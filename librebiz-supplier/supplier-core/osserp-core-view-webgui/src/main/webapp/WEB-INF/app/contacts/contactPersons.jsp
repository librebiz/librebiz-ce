<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="contactPersonsView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="headline">
        <fmt:message key="${view.headerName}"/><c:if test="${!empty view.related}"> - <o:out value="${view.related.displayName}"/></c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="contactPersonsContent">
            <div class="row">
                <c:import url="_contactPersonsList.jsp"/>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
