/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 11-Aug-2006 14:31:31 
 * 
 */
package com.osserp.core.service.records;

import javax.xml.transform.Source;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.DateFormatter;

import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordExport;
import com.osserp.core.finance.RecordType;
import com.osserp.core.service.doc.XmlService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordExportOutputServiceImpl extends AbstractRecordOutputService
        implements RecordExportOutputService {
    
    private static Logger log = LoggerFactory.getLogger(RecordExportOutputServiceImpl.class.getName());
    private static final String STYLESHEET = "record_export";
    private RecordExport export;

    /**
     * Creates a recordExport output service
     * @param resourceLocator
     * @param optionsCache
     * @param stylesheetService
     * @param xmlService
     */
    protected RecordExportOutputServiceImpl(
            ResourceLocator resourceLocator, 
            OptionsCache optionsCache, 
            CoreStylesheetService stylesheetService,
            XmlService xmlService) {
        super(resourceLocator, optionsCache, stylesheetService, xmlService, null);
    }

    public Document createOutput(Employee employee, RecordExport recordExport) {
        if (log.isDebugEnabled()) {
            log.debug("createOutput() invoked [type="
                    + (recordExport.getType() == null ? "null]" : (recordExport.getType().toString() + "]")));
        }
        export = recordExport;
        Document doc = xmlService.createDocument(employee);
        Element root = doc.getRootElement();

        root.addContent(new Element("currentType").setText(recordExport.getType().toString()));
        Element records = new Element("records");
        addRecords(records);
        root.addContent(records);
        if (log.isDebugEnabled()) {
            log.debug("createOutput() done");
        }
        return doc;
    }

    private void addRecords(Element records) {
        for (int i = 0, j = export.getRecords().size(); i < j; i++) {
            RecordDisplay rec = export.getRecords().get(i);
            records.addContent(createRecord(rec));
        }
    }

    private Element createRecord(RecordDisplay rec) {
        Element record = new Element("record");
        record.addContent(new Element("id").setText(rec.getId().toString()));
        record.addContent(new Element("type").setText(rec.getType().toString()));
        if (rec.getReference() != null) {
            record.addContent(new Element("referenceId").setText(rec.getReference().toString()));
        } else {
            record.addContent(new Element("referenceId"));
        }
        if (rec.getSales() != null) {
            record.addContent(new Element("saleId").setText(rec.getSales().toString()));
        } else {
            record.addContent(new Element("saleId"));
        }
        record.addContent(new Element("companyId").setText(rec.getCompany().toString()));
        record.addContent(new Element("contactId").setText(rec.getContactId().toString()));
        record.addContent(new Element("contactType").setText(rec.getContactType().toString()));
        record.addContent(new Element("contactName").setText(rec.getContactName()));
        record.addContent(new Element("created").setText(
                DateFormatter.getDate(rec.getCreated())));
        record.addContent(xmlService.createEmployee("createdBy", rec.getCreatedBy()));
        record.addContent(xmlService.createEmployee("signatureLeft", rec.getSignatureLeft()));
        record.addContent(xmlService.createEmployee("signatureRight", rec.getSignatureRight()));
        record.addContent(new Element("currency").setText(rec.getCurrencyKey()));
        record.addContent(new Element("baseTax").setText(xmlService.createString(rec.getTax())));
        record.addContent(new Element("reducedTax").setText(xmlService.createString(rec.getReducedTax())));
        if (rec.isTaxFree()) {
            record.addContent(new Element("taxFree").setText("true"));
            record.addContent(getTaxFreeDefinition("taxFreeReason", rec.getTaxFreeId()));
            record.addContent(new Element("netAmount").setText(
                    xmlService.createString(rec.getNetAmount())));
            record.addContent(new Element("taxAmount"));
            record.addContent(new Element("reducedTaxAmount"));
            record.addContent(new Element("grossAmount").setText(
                    xmlService.createString(rec.getNetAmount())));
        } else {
            record.addContent(new Element("taxFree").setText("false"));
            record.addContent(new Element("taxFreeReason"));
            record.addContent(new Element("netAmount").setText(
                    xmlService.createString(rec.getNetAmount())));
            record.addContent(new Element("taxAmount").setText(
                    xmlService.createString(rec.getTaxAmount())));
            record.addContent(new Element("reducedTaxAmount").setText(
                    xmlService.createString(rec.getReducedTaxAmount())));
            record.addContent(new Element("grossAmount").setText(
                    xmlService.createString(rec.getGrossAmount())));
        }
        if (RecordType.SALES_DOWNPAYMENT.equals(rec.getType())
                || RecordType.SALES_INVOICE.equals(rec.getType())) {
            record.addContent(new Element("maturity").setText(
                    DateFormatter.getDate(rec.getMaturity())));
        }
        if (RecordType.SALES_DOWNPAYMENT.equals(rec.getType())) {
            if (rec.isTaxFree()) {
                record.addContent(new Element("proratedNetAmount").setText(
                        xmlService.createString(rec.getProrateNetAmount())));
                record.addContent(new Element("proratedTaxAmount"));
                record.addContent(new Element("proratedReducedTaxAmount"));
                record.addContent(new Element("proratedGrossAmount").setText(
                        xmlService.createString(rec.getProrateNetAmount())));
            } else {
                record.addContent(new Element("proratedNetAmount").setText(
                        xmlService.createString(rec.getNetAmount())));
                record.addContent(new Element("proratedTaxAmount").setText(
                        xmlService.createString(rec.getTaxAmount())));
                record.addContent(new Element("proratedReducedTaxAmount").setText(
                        xmlService.createString(rec.getReducedTaxAmount())));
                record.addContent(new Element("proratedGrossAmount").setText(
                        xmlService.createString(rec.getGrossAmount())));
            }
        }
        return record;
    }

    public byte[] createPdf(Document doc) throws ClientException {
        return createPdf(getXsl((Record) null), doc);
    }

    @Override
    protected Source getXsl(Record record) throws ClientException {
        return stylesheetService.getStylesheetSource(STYLESHEET);
    }

}
