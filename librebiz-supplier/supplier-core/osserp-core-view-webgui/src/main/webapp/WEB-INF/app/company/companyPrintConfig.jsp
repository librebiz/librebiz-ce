<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="companyPrintConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="styles" type="string">
        <style type="text/css">
            .pvalue { text-align: right; } 
        </style>
    </tiles:put>
	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <v:form id="companyPrintConfigForm" url="/company/companyPrintConfig/updateProperty">
                    <c:import url="_companyPrintConfig.jsp"/>
                </v:form>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
