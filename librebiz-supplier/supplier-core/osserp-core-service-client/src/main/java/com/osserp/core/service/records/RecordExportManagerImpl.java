/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Feb-2007 15:20:04 
 * 
 */
package com.osserp.core.service.records;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jdom2.Document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.StringUtil;

import com.osserp.core.Options;
import com.osserp.core.dao.records.RecordExports;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordExport;
import com.osserp.core.finance.RecordExportManager;
import com.osserp.core.finance.RecordExportType;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.RecordTypeImpl;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordExportManagerImpl extends AbstractService implements RecordExportManager {
    private static Logger log = LoggerFactory.getLogger(RecordExportManagerImpl.class.getName());
    private RecordExports exportsDao = null;
    private RecordExportArchive recordExportArchive = null;
    private RecordExportOutputService outputService = null;
    private ResourceLocator resourceLocator = null;
    private OptionsCache optionsCache = null;

    public RecordExportManagerImpl(
            RecordExports exportsDao,
            RecordExportOutputService outputService,
            OptionsCache optionsCache,
            ResourceLocator resourceLocator,
            RecordExportArchive recordExportArchive) {
        super();
        this.exportsDao = exportsDao;
        this.outputService = outputService;
        this.optionsCache = optionsCache;
        this.resourceLocator = resourceLocator;
        this.recordExportArchive = recordExportArchive;
    }

    public List<RecordExportType> getExportableTypes() {
        List<RecordExportType> recordTypes = new ArrayList<RecordExportType>();
        recordTypes.add(createType(RecordType.SALES_DOWNPAYMENT));
        recordTypes.add(createType(RecordType.SALES_INVOICE));
        recordTypes.add(createType(RecordType.SALES_CANCELLATION));
        recordTypes.add(createType(RecordType.SALES_CREDIT_NOTE));
        recordTypes.add(createType(RecordType.SALES_PAYMENT));
        recordTypes.add(createType(RecordType.PURCHASE_INVOICE));
        recordTypes.add(createType(RecordType.PURCHASE_PAYMENT));
        return recordTypes;
    }
    
    private RecordExportType createType(Long type) {
        RecordType recordType = (RecordType) optionsCache.getMapped(Options.RECORD_TYPES, type);
        if (recordType == null) {
            recordType = (RecordType) optionsCache.getMapped(Options.BILLING_TYPES, type);
        } 
        if (recordType == null) {
            recordType = (RecordType) optionsCache.getMapped(Options.PAYMENT_TYPES, type);
        } 
        if (recordType == null) {
            if (RecordType.SALES_PAYMENT.equals(type)) {
                recordType = new RecordTypeImpl(type, resourceLocator.getMessage("paymentSales"), true);
            } else if (RecordType.PURCHASE_PAYMENT.equals(type)) {
                recordType = new RecordTypeImpl(type, resourceLocator.getMessage("paymentPurchase"), true);
            } else {
                log.warn("createType() did not find provided recordType [id=" + type + "]");
            }
        } 
        return new RecordExportType(
                recordType, 
                exportsDao.getExistingExportCount(type), 
                exportsDao.getExportCount(type));
    }

    public List<RecordExport> findByType(Long recordType) {
        return exportsDao.getExports(recordType);
    }

    public List<RecordExport> listByType(Long recordType) {
        return exportsDao.getExportsList(recordType);
    }

    public void addRecords(RecordExport export) {
        this.exportsDao.addRecords(export);
    }

    public int createExport(Long recordType, Long employeeId) {
        return exportsDao.createExport(recordType, employeeId);
    }

    public int createExport(Long recordType, Long employeeId, Date start, Date end) {
        return exportsDao.createExport(recordType, employeeId, start, end);
    }

    public RecordExport getLatest(Long recordType) {
        return exportsDao.getLatest(recordType);
    }

    public Integer getCount(Long recordType) {
        return exportsDao.getExportCount(recordType);
    }

    public int createExport(Long recordType, Long employeeId, List<RecordDisplay> records) {
        return exportsDao.createExport(recordType, employeeId, records);
    }

    public List<RecordDisplay> getRecords(Long type, boolean reverse) {
        return exportsDao.getRecords(type, reverse);
    }

    public String createXls(Employee employee, RecordExport export) {
        List<String[]> valueList = new ArrayList<String[]>();
        List<RecordDisplay> list = export.getRecords();
        for (int i = 0, j = list.size(); i < j; i++) {
            RecordDisplay next = list.get(i);
            String[] nextValues = next.getValues();
            valueList.add(nextValues);
        }
        return StringUtil.createSheet(valueList);
    }

    public Document createXml(Employee employee, RecordExport export) throws ClientException {
        return outputService.createOutput(employee, export);
    }

    public byte[] createPdf(Employee employee, RecordExport export) throws ClientException {
        Document doc = createXml(employee, export);
        return outputService.createPdf(doc);
    }

    public DocumentData createZip(RecordExport recordExport) throws ClientException {
        return recordExportArchive.getArchive(recordExport);
    }

}
