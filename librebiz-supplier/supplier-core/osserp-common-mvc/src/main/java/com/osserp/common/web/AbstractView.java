/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2007 3:53:16 PM 
 * 
 */
package com.osserp.common.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.beans.AbstractClass;
import com.osserp.common.service.Locator;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractView extends AbstractClass implements View {
    private static Logger log = LoggerFactory.getLogger(AbstractView.class.getName());

    private boolean ajaxMode = false;
    private String name = null;
    private String errorMessage = null;
    private Long lastId = null;
    private volatile boolean createMode = false;
    private volatile boolean editMode = false;
    private boolean readOnlyMode = false;
    private volatile boolean setupMode = false;
    private User user = null;
    private Object bean = null;
    private boolean reloadWhenExistsOnCreate = false;

    private List<Object> list = new ArrayList<>();
    private List<Object> listBackup = new ArrayList<>();
    private boolean listSelectionMode = false;
    private int listStart = 0;
    private int listEnd = 0;
    private int listStep = 50;
    private String listSortMethod = null;
    private String listTarget = null;
    private boolean selectionView = false;
    private String selectionTarget = null;
    private String selectionTargetIdParam = null;

    private String actionTarget = null;
    private String actionUrl = null;
    private String selectionUrl = null;
    private String exitId = null;
    private String exitTarget = null;
    private String forwardTarget = null;
    private String pageHeaderKey = null;
    private Form form = null;
    private List<Integer> percentageList = new ArrayList<>();
    private Map<String, String> requiredPermissions = new HashMap<>();

    private boolean keepViewWhenExistsOnCreate = false;
    private boolean createBackupWhenExisting = false;
    private boolean exitAfterSave = false;
    private boolean mightyMode = false;
    private Map<String, Object> cache = new HashMap<>();
    private Map<Locale, ResourceBundle> resourceBundles = new HashMap<>();
    private List<String> scriptsList = new ArrayList<>();
    private List<String> scriptsListBottom = new ArrayList<>();
    private List<String> stylesList = new ArrayList<>();

    /**
     * Initializes view name, actionUrl and selectionUrl as annotated and initializes action target as 'success'
     */
    protected AbstractView() {
        super();
        if (getClass().isAnnotationPresent(ViewName.class)) {
            ViewName viewName = getClass().getAnnotation(ViewName.class);
            setName(viewName.value());
            if (log.isDebugEnabled()) {
                log.debug("<init> annotated name [" + getName() + "]");
            }
        }
        if (getClass().isAnnotationPresent(ActionUrl.class)) {
            ActionUrl url = getClass().getAnnotation(ActionUrl.class);
            actionUrl = url.value();
            if (log.isDebugEnabled()) {
                log.debug("<init> annotated action url [" + actionUrl + "]");
            }
        }
        if (getClass().isAnnotationPresent(SelectUrl.class)) {
            SelectUrl url = getClass().getAnnotation(SelectUrl.class);
            selectionUrl = url.value();
            if (log.isDebugEnabled()) {
                log.debug("<init> annotated selection url [" + selectionUrl + "]");
            }
        }
        setActionTarget(Actions.SUCCESS);
    }

    public String getTargetElementName() {
        if (getClass().isAnnotationPresent(TargetElementName.class)) {
            TargetElementName viewName = getClass().getAnnotation(TargetElementName.class);
            String value = viewName.value();
            if (log.isDebugEnabled()) {
                log.debug("getAjaxViewName() done [name=" + getName() + "]");
            }
            return value;
        }
        return null;
    }

    /**
     * Creates new view by existing view. Local user, list and bean will be copied if available
     * @param view
     */
    protected AbstractView(View view) {
        user = view.getUser();
        if (view.getList() != null) {
            list = view.getList();
        }
        if (view.getBean() != null) {
            setBean(view.getBean());
        }
    }
    

    /**
     * Reloads current business objects from the backend if supported by implementing view.
     * Default is doing nothing.
     */
    public void reload() {
        // Implement if required
    }
    
    public boolean isReloadWhenExistsOnCreate() {
        return reloadWhenExistsOnCreate;
    }

    protected void setReloadWhenExistsOnCreate(boolean reloadWhenExistsOnCreate) {
        this.reloadWhenExistsOnCreate = reloadWhenExistsOnCreate;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) throws PermissionException {
        this.editMode = editMode;
    }

    public void disableEditMode() {
        this.editMode = false;
    }

    public void enableEditMode() {
        this.editMode = true;
    }

    public boolean isAjaxMode() {
        return ajaxMode;
    }

    public void setAjaxMode(boolean ajaxMode) {
        this.ajaxMode = ajaxMode;
    }

    public boolean isCreateMode() {
        return createMode;
    }

    public void setCreateMode(boolean createMode) throws PermissionException {
        this.createMode = createMode;
    }

    protected void disableCreateMode() {
        this.createMode = false;
    }

    public boolean isReadOnlyMode() {
        return readOnlyMode;
    }

    public void setReadOnlyMode(boolean readOnlyMode) {
        this.readOnlyMode = readOnlyMode;
    }

    public boolean isSetupMode() {
        return setupMode;
    }

    public void setSetupMode(boolean setupMode) {
        this.setupMode = setupMode;
    }

    protected void disableSetupMode() {
        setupMode = false;
    }

    public boolean isMightyMode() {
        return mightyMode;
    }

    protected void setMightyMode(boolean mightyMode) {
        this.mightyMode = mightyMode;
    }

    public boolean isSelectionView() {
        return selectionView;
    }

    protected void setSelectionView(boolean selectionView) {
        this.selectionView = selectionView;
    }

    public void enableSelectionView() {
        this.selectionView = true;
    }

    public List getList() {
        return list;
    }

    @SuppressWarnings("unchecked")
    protected void setList(List list) {
        this.list = list;
    }

    public List<Integer> getPercentageList() {
        if (percentageList.isEmpty()) {
            percentageList.add(Integer.valueOf(0));
            for (int i = 1, j = 10; i <= j; i++) {
                percentageList.add(Integer.valueOf(i * 10));
            }
        }
        return percentageList;
    }

    protected void setPercentageList(List<Integer> percentageList) {
        this.percentageList = percentageList;
    }

    public int getListStart() {
        return listStart;
    }

    protected void setListStart(int listStart) {
        this.listStart = listStart;
    }

    public int getListEnd() {
        return listEnd > 0 ? listEnd : getListCount() - 1;
    }

    protected void setListEnd(int listEnd) {
        this.listEnd = listEnd;
    }

    public int getListStep() {
        return listStep;
    }

    public void setListStep(int listStep) {
        this.listStep = listStep;
        this.listStart = 0;
        this.listEnd = listStep - 1;
    }

    public void nextList() {
        if (log.isDebugEnabled()) {
            log.debug("nextList() invoked [start=" + listStart
                    + ", end=" + listEnd + "]");
        }
        if (listStart + listStep >= getListCount()) {
            listStart = 0;
            listEnd = listStep - 1;
        } else {
            listStart = listStart + listStep;
            listEnd = listEnd + listStep;
        }
        if (log.isDebugEnabled()) {
            log.debug("nextList() done [start=" + listStart
                    + ", end=" + listEnd + "]");            
        }
    }

    public void lastList() {
        if (log.isDebugEnabled()) {
            log.debug("lastList() invoked [start=" + listStart
                    + ", end=" + listEnd + "]");
        }
        if (listStart - listStep < 0) {
            int cnt = getListCount();
            int it = cnt / listStep;
            if (cnt % listStep == 0) {
                it = it - 1;
            }
            listStart = it * listStep;
            listEnd = listStart + listStep - 1;
        } else {
            listStart = listStart - listStep;
            listEnd = listStart + listStep;
        }
        if (log.isDebugEnabled()) {
            log.debug("lastList() done [start=" + listStart
                    + ", end=" + listEnd + "]");
        }
    }

    public boolean isFirstList() {
        return (listStart == 0);
    }

    public boolean isLastList() {
        return (listEnd == 0 || listEnd >= getListCount() - 1);
    }

    protected int getListCount() {
        return (list == null ? 0 : list.size());
    }

    public void resetListCounters() {
        listStart = 0;
        listEnd = listStart + listStep;
    }

    public String getListTarget() {
        return listTarget;
    }

    public void setListTarget(String listTarget) {
        this.listTarget = listTarget;
    }

    /**
     * Provides the current selected object to display/edit
     * @return object
     */
    public Object getBean() {
        return bean;
    }

    public boolean isBeanAvailable() {
        return (bean != null);
    }

    protected void setBean(Object bean) {
        this.bean = bean;
    }

    protected void addListObject(Object obj) {
        if (list == null) {
            list = new ArrayList<Object>();
        }
        list.add(obj);
    }

    public boolean isListSelectionMode() {
        return listSelectionMode;
    }

    public void enableListSelectionMode() {
        listBackup.clear();
        for (int i = 0, j = list.size(); i < j; i++) {
            listBackup.add(list.get(i));
        }
        listSelectionMode = true;
    }

    public void disableListSelectionMode() {
        list = listBackup;
        listSelectionMode = false;
    }

    public void cutList() {
        if (bean != null) {
            CollectionUtil.removeByProperty(
                    list,
                    getSelectPropertyName(),
                    bean);
            bean = null;
        }
    }

    protected void setListSelectionMode(boolean listSelectionMode) {
        this.listSelectionMode = listSelectionMode;
    }

    public void sortList(String method) {
        // default implementation does nothing
    }

    public String getListSortMethod() {
        return listSortMethod;
    }

    protected void setListSortMethod(String listSortMethod) {
        this.listSortMethod = listSortMethod;
    }

    /**
     * Selects bean from current list. Override method if bean in list has no 'id' property
     * @param id of the selected entity from current list
     */
    public void setSelection(Long id) {
        String propertyName = getSelectPropertyName();
        if (log.isDebugEnabled()) {
            log.debug("setSelection() invoked [id=" + id + ", property=" + propertyName + "]");
        }
        if (id == null) {
            Object obj = bean;
            if (obj != null && list != null) {
                // update list with current bean and remove bean
                CollectionUtil.replaceByProperty(
                        list,
                        propertyName,
                        obj);
            }
            setBean(null);
            editMode = false;
        } else {
            bean = CollectionUtil.getByProperty(
                    list,
                    propertyName,
                    id);
            lastId = id;
            if (log.isDebugEnabled()) {
                log.debug("setSelection() done [listSize="
                        + (list == null ? "null" : list.size())
                        + ", class="
                        + (bean == null ? "null" : bean.getClass().getName())
                        + "]");
            }
        }
    }

    public boolean jumpToExists() {
        return (lastId != null);
    }

    public String createJumpTo(String jumpToUrl) {
        StringBuilder result = new StringBuilder(jumpToUrl);
        if (lastId != null) {
            result.append("#").append(lastId.toString());
        }
        return result.toString();
    }

    public void setJumpTo(Long jumpTo) {
        this.lastId = jumpTo;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    public boolean isActionTargetAvailable() {
        return (actionTarget != null);
    }

    public String getActionTarget() {
        return actionTarget;
    }

    public void setActionTarget(String actionTarget) {
        this.actionTarget = actionTarget;
    }

    public final boolean isExitIdAvailable() {
        return (exitId != null);
    }

    public String getExitId() {
        return exitId;
    }

    public void setExitId(String exitId) {
        this.exitId = exitId;
    }

    public final boolean isExitTargetAvailable() {
        return (exitTarget != null);
    }

    public String getExitTarget() {
        return exitTarget;
    }

    public void setExitTarget(String exitTarget) {
        this.exitTarget = exitTarget;
    }

    public boolean isForwardTargetAvailable() {
        return forwardTarget != null;
    }

    public String getForwardTarget() {
        return forwardTarget;
    }

    public void setForwardTarget(String forwardTarget) {
        this.forwardTarget = forwardTarget;
    }

    public final String getSelectionUrl() {
        return selectionUrl;
    }

    public void setSelectionUrl(String selectionUrl) {
        this.selectionUrl = selectionUrl;
    }

    public String getSelectionTarget() {
        return selectionTarget;
    }

    public void setSelectionTarget(String selectionTarget) {
        this.selectionTarget = selectionTarget;
    }

    public String getSelectionTargetIdParam() {
        return selectionTargetIdParam != null ? selectionTargetIdParam : "id";
    }

    public void setSelectionTargetIdParam(String selectionTargetIdParam) {
        this.selectionTargetIdParam = selectionTargetIdParam;
    }

    public String getPageHeaderKey() {
        return pageHeaderKey;
    }

    public void setPageHeaderKey(String pageHeaderKey) {
        this.pageHeaderKey = pageHeaderKey;
    }

    public boolean isKeepViewWhenExistsOnCreate() {
        return keepViewWhenExistsOnCreate;
    }

    public void setKeepViewWhenExistsOnCreate(boolean keepViewWhenExistsOnCreate) {
        this.keepViewWhenExistsOnCreate = keepViewWhenExistsOnCreate;
    }

    public User getUser() {
        return user;
    }
    
    public boolean isPermissionDenied(String[] permission) {
        return !isPermissionGrant(permission);
    }

    public boolean isPermissionGrant(String permissionValues) {
        return isPermissionGrant(StringUtil.getTokenArray(permissionValues));
    }

    public final boolean isPermissionGrant(String[] permissions) {
        if (isNotSet(permissions)) {
            return true;
        }
        if (user == null) {
            return false;
        }
        return getUser().isPermissionGrant(permissions);
    }

    public void checkPermission(String[] permissions) throws PermissionException {
        if (!isPermissionGrant(permissions)) {
            throw new PermissionException();
        }
    }

    public void checkPermission(String permissions) throws PermissionException {
        checkPermission(StringUtil.getTokenArray(permissions));
    }

    public void checkPermission(String permissions, String errorKey) throws PermissionException {
        try {
            checkPermission(StringUtil.getTokenArray(permissions));
        } catch (PermissionException e) {
            throw new PermissionException(errorKey);
        }
    }

    protected boolean isDomainView() {
        if (user != null && user.isDomainUser()) {
            return true;
        }
        return false;
    }

    public boolean isUserFromBranch() {
        return false;
    }

    public boolean isBranchExecutive() {
        return false;
    }

    protected String getSelectPropertyName() {
        return "id";
    }

    public boolean isCreateBackupWhenExisting() {
        return createBackupWhenExisting;
    }

    public void setCreateBackupWhenExisting(boolean createBackupWhenExisting) {
        this.createBackupWhenExisting = createBackupWhenExisting;
    }

    public boolean isExitAfterSave() {
        return exitAfterSave;
    }

    protected void setExitAfterSave(boolean exitAfterSave) {
        this.exitAfterSave = exitAfterSave;
    }

    public final String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public void setCurrentUser(User user) {
        setUser(user);
    }

    /**
     * Fetches another view if available
     * @param request
     * @param _class
     * @return view or null if none exists
     */
    protected View fetchView(HttpServletRequest request, Class<? extends View> _class) {
        return ViewManager.fetchView(request.getSession(), _class);
    }

    /**
     * Fetches view data either from request or session scope
     * @param request
     * @return data or null if
     */
    protected Map<String, Object> fetchViewData(HttpServletRequest request) {
        return ViewManager.fetchViewData(request, this);
    }

    public void save(Form webform) throws ClientException, PermissionException {
        // implement if required
    }

    // utility methods

    public Map<String, String> getRequiredPermissions() {
        return requiredPermissions;
    }

    protected void setRequiredPermissions(Map<String, String> requiredPermissions) {
        if (requiredPermissions != null) {
            this.requiredPermissions = requiredPermissions;
        }
    }

    protected void clearList() {
        list = new ArrayList<Object>();
    }

    public final Map<String, Object> getData() {
        return cache;
    }

    public Object getLocalValue(String nameVal) {
        return cache.get(nameVal);
    }

    public boolean isLocalValueExisting(String nameVal) {
        return cache.containsKey(nameVal);
    }

    public void setLocalValue(String name, Object value) {
        addLocalValue(name, value);
    }

    protected final void addLocalValue(String nameVal, Object obj) {
        if (obj != null) {
            cache.put(nameVal, obj);
        }
    }

    protected final void addLocalValues(Map<String, Object> obj) {
        if (obj != null && !obj.isEmpty()) {
            for (Iterator<Map.Entry<String, Object>> i = obj.entrySet().iterator(); i.hasNext();) {
                Map.Entry<String, Object> next = i.next();
                if (next.getValue() != null) {
                    cache.put(next.getKey(), next.getValue());
                }
            }
        }
    }

    protected void removeLocalValue(String nameVal) {
        cache.remove(nameVal);
    }

    protected final void replaceLocalValues(Map<String, Object> obj) {
        cache.clear();
        addLocalValues(obj);
    }

    protected final void removeLocalValues() {
        cache.clear();
    }

    /**
     * Provides current form
     * @return form
     */
    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    protected void setUser(User user) {
        this.user = user;
    }

    public List<String> getScriptsList() {
        return scriptsList;
    }

    public void setScriptsList(List<String> scriptsList) {
        this.scriptsList = scriptsList;
    }

    public List<String> getScriptsListBottom() {
        return scriptsListBottom;
    }

    public void setScriptsListBottom(List<String> scriptsListBottom) {
        this.scriptsListBottom = scriptsListBottom;
    }

    public List<String> getStylesList() {
        return stylesList;
    }

    public void setStylesList(List<String> stylesList) {
        this.stylesList = stylesList;
    }

    /**
     * @return the resourceBundles
     */
    protected Map<Locale, ResourceBundle> getResourceBundles() {
        return resourceBundles;
    }

    /**
     * @param resourceBundles the resourceBundles to set
     */
    protected void setResourceBundles(Map<Locale, ResourceBundle> resourceBundles) {
        this.resourceBundles = resourceBundles;
    }

    public void addResourceBundle(Locale locale, ResourceBundle bundle) {
        this.resourceBundles.put(locale, bundle);
    }

    protected final ResourceBundle fetchResourceBundle(Locale locale) {
        return resourceBundles.get(locale);
    }

    protected final ResourceBundle fetchResourceBundle() {
        Locale locale = (getUser() != null ? getUser().getLocale() : null);
        if (locale == null) {
            locale = Constants.DEFAULT_LOCALE_OBJECT;
        }
        return fetchResourceBundle(locale);
    }

    protected final Object getService(String name) {
        if (serviceLocator != null) {
            try {
                return serviceLocator.lookupService(name);
            } catch (Exception e) {
                log.debug("getService: locator available but service not bound [name=" + name + "]");
            }
        } else {
            log.debug("getService: serviceLocator not bound");
        }
        return null;
    }

    private transient Locator serviceLocator;
    protected final Locator getLocator() {
        return serviceLocator;
    }
    public void setLocator(Locator locator) {
        serviceLocator = locator;
    }
}
