<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.selectedType.name}" />
                -
                <o:out value="${view.selectedType.description}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
        
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="irreversibleAction"><fmt:message key="attention"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <fmt:message key="changeContactTypeToBusinessIsNotReversible"/>!
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="companyName"><fmt:message key="companyNameLabel" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="company" styleClass="form-control" title="companyNameLabelHint" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <v:submitExit url="/contacts/contactEditor/disableTypeChangeMode"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:submit value="save" styleClass="form-control" />
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
