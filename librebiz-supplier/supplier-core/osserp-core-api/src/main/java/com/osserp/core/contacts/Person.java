/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 15, 2007 10:21:31 AM 
 * 
 */
package com.osserp.core.contacts;

import java.io.Serializable;
import java.util.Date;

import com.osserp.common.Option;
import com.osserp.common.PersistentObject;

import com.osserp.core.Address;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Person extends PersistentObject, Cloneable, Serializable {

    static final Long STATUS_CREATED = 0L;
    static final Long STATUS_EXTERNAL = 1L;
    static final Long STATUS_DELETED = -10L;
    static final Long STATUS_EXISTING = -2L;
    static final Long STATUS_TRANSFERED = -1L;
    
    static final Long STATUS_IN_PROGRESS = 25L;
    static final Long STATUS_OK = 50L;
    static final Long STATUS_VALID = 100L;

    /**
     * Returns the display status of the person. See Person.STATUS_name for valid values.
     * @return status
     */
    Long getStatus();

    /**
     * Sets the display status of the person. See Person.STATUS_name for valid values.
     * @param status
     */
    void setStatus(Long status);
    
    static final Long TYPE_PRIVATE = 1L;
    static final Long TYPE_BUSINESS = 2L;
    static final Long TYPE_PUBLIC = 3L;
    static final Long TYPE_PERSON = 4L;
    static final Long TYPE_FREELANCE = 5L;

    /**
     * Returns the type
     * @return type
     */
    ContactType getType();

    /**
     * Sets the type
     * @param type
     */
    void setType(ContactType type);

    /**
     * Provides the contact name depending on contact type, e.g. person or company
     * @return 'company name' or 'firstName lastName'
     */
    String getName();

    /**
     * Provides the contact display name depending on contact type e.g. person or company
     * @return 'company name' or 'lastName, firstName'
     */
    String getDisplayName();

    /**
     * Provides salutation, title if exists and name
     * @return salutationDisplay
     */
    String getSalutationDisplay();

    /**
     * Provides salutation, title if exists, firstname and lastname
     * @return salutationDisplayLong
     */
    String getSalutationDisplayLong();

    /**
     * Provides salutation and title if exists
     * @return salutationDisplayShort
     */
    String getSalutationDisplayShort();

    /**
     * Returns the salutation
     * @return salutation
     */
    Salutation getSalutation();

    /**
     * Sets the salutation
     * @param Salutation salutation
     */
    void setSalutation(Salutation salutation);

    /**
     * Returns the title id
     * @return title
     */
    Option getTitle();

    /**
     * Setss the title
     * @param title
     */
    void setTitle(Option title);

    /**
     * Returns the first name
     * @return firstName
     */
    String getFirstName();

    /**
     * Sets the contact persons first name
     * @param firstName
     */
    void setFirstName(String firstName);

    /**
     * Indicates if firstname should be placed as name prefix if person is company
     * @return firstNamePrefix
     */
    boolean isFirstNamePrefix();

    /**
     * Sets if firstname should be placed as name prefix if person is company
     * @param firstNamePrefix
     */
    void setFirstNamePrefix(boolean firstNamePrefix);

    /**
     * Returns the last name
     * @return lastName
     */
    String getLastName();

    /**
     * Sets the last name
     * @param lastName
     */
    void setLastName(String lastName);

    /**
     * The primary address
     * @return address
     */
    Address getAddress();

    /**
     * The primary email address
     * @return email
     */
    String getEmail();

    /**
     * The primary phone number
     * @return phone
     */
    Phone getPhone();

    /**
     * The primary fax number
     * @return fax
     */
    Phone getFax();

    /**
     * The primary mobile number
     * @return mobile
     */
    Phone getMobile();

    /**
     * The creation-date of the entity. This value is set at creation time
     * @return personCreated
     */
    Date getPersonCreated();

    /**
     * The user who created this entity. This value is set at creation time
     * @return personCreatedBy
     */
    Long getPersonCreatedBy();

    /**
     * The last changed-date of the entity. This value is set at creation time
     * @return personChanged
     */
    Date getPersonChanged();

    /**
     * Sets the date when the entity data was changed
     * @return personChanged
     */
    void setPersonChanged(Date personChanged);

    /**
     * The user who changed this entity. This value is set at creation time
     * @return personChangedBy
     */
    Long getPersonChangedBy();

    /**
     * Sets the user who changed the entity data
     * @param personChangedBy
     */
    void setPersonChangedBy(Long personChangedBy);

    /**
     * Indicates that person is a business, using lastname as company name with separate contact persons.
     * @return business if contact type public or business
     */
    boolean isBusiness();

    /**
     * Indicates that person is a freelancer business, using firstname and lastname for person and office as company name
     * @return freelancer business if contact type freelancer
     */
    boolean isFreelance();

    /**
     * Indicates that person is a private person or freelancer (e.g. natural person)
     * Persons do not provide contact persons.
     * @return true if contact type private or freelance
     */
    boolean isPerson();

    /**
     * Indicates that person is a public institution or interest body, using lastname as name of instition. 
     * Contact persons are stored as contacts of type person.
     * @return true if contact type is public institution
     */
    boolean isPublicInstitution();

    /**
     * Indicates that person is a private person or consumer (e.g. natural person) 
     * @return true if contact is private person
     */
    boolean isPrivatePerson();

    /**
     * Indicates if person is signed as deleted if app does not delete contacts physically
     * @return deleted
     */
    boolean isDeleted();

    /**
     * Provides the primary key of an external source (if person was created
     * by another application or other source)  
     * @return externalReference or null
     */
    String getExternalReference();

    /**
     * Sets the primary key of an external source
     * @param externalReference
     */
    void setExternalReference(String externalReference);

    /**
     * Provides an url to invoke the person at the external source 
     * @return externalUrl
     */
    String getExternalUrl();

    /**
     * Sets an external url of the person at the external source
     * @param externalUrl
     */
    void setExternalUrl(String externalUrl);

    /**
     * Clones the person
     * @return clone
     */
    Object clone();
}
