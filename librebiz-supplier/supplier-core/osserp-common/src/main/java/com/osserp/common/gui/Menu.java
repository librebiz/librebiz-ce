/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 18, 2007 8:36:02 PM 
 * 
 */
package com.osserp.common.gui;

import java.util.List;
import java.util.Map;

import com.osserp.common.Option;
import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Menu extends Option {

    /**
     * Indicates that this menu is activated
     * @return activated
     */
    boolean isActivated();

    /**
     * Activates/deactivates this menu
     * @param activated
     */
    void setActivated(boolean activated);

    /**
     * Indicates that this menu is default
     * @return defaultMenu
     */
    boolean isDefaultMenu();

    /**
     * Indicates if menu is shared
     * @return sharedMenu
     */
    boolean isSharedMenu();

    /**
     * Indicates if modules are enabled and links should be displayed
     * @return modulesEnabled
     */
    boolean isModulesEnabled();

    /**
     * Enabled/disables module display status
     * @param modulesEnabled
     */
    void setModulesEnabled(boolean modulesEnabled);

    /**
     * Indicates if static links should be displayed
     * @return staticLinksEnabled
     */
    boolean isStaticLinksEnabled();

    /**
     * Enabled/disables static links display status
     * @param staticLinksEnabled
     */
    void setStaticLinksEnabled(boolean staticLinksEnabled);
    
    /**
     * Adds a new link to a menu position
     * @param position
     * @param url
     * @param name
     * @param permissions comma separated list of permissions required to see or execute the link
     */
    void addLink(String position, String url, String name, String permissions);

    /**
     * Provides the menu headers
     * @return headers
     */
    List<MenuHeader> getHeaders();
    
    /**
     * Adds a new menu position
     * @param name
     */
    void addHeader(String name);

    /**
     * Toggles a menu headers activation status
     * @param name
     */
    void toggleHeader(String name);
    
    /**
     * Provides a customized menu. 
     * @param user
     * @return menu without disabled and/or non-accessible items.
     */
    Menu getCustomized(User user);

    /**
     * Creates a shared menu. The shared menu is a copy of the menu with prefixed links
     * @param url
     * @return shared menu
     */
    Menu createShared(String url);

    /**
     * Provides all values as map
     * @return
     */
    Map<String, Object> getMapped();
}
