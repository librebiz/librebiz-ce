/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Jun-2006 12:23:05 
 * 
 */
package com.osserp.gui.client.contacts;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RelatedContactView extends ContactView {

    /**
     * Indicates if view is in details edit mode
     * @return true if so
     */
    public boolean isDetailsEditMode();

    /**
     * Enables/disables details edit mode
     * @param detailsEditMode
     */
    public void setDetailsEditMode(boolean detailsEditMode);

    /**
     * Updates the customer details
     * @param form with values to update, see form def. for values
     * @throws ClientException if validation of new values failed
     */
    public void updateDetails(Form fh) throws ClientException, PermissionException;

    /**
     * Indicates that relation type is default when loading contacts display
     * @return defaultDisplay
     */
    public boolean isDefaultDisplay();

    /**
     * Activates/deactivates default relation type
     */
    public void toggleDefaultDisplay();
}
