/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01.10.2004 
 * 
 */
package com.osserp.gui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.web.PortalView;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.SessionUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PortalUtil {
    
    private static Logger log = LoggerFactory.getLogger(PortalUtil.class.getName());

    /**
     * Updates logged in user
     * @param request
     * @param user
     * @throws IllegalAccessException if provided user not logged in user
     */
    public static void updateUser(HttpServletRequest request, User user) throws IllegalAccessException {
        HttpSession session = request.getSession();
        PortalView portalView = (PortalView) session.getAttribute("portalView");
        if (portalView instanceof PortalViewImpl) {
            ((PortalViewImpl) portalView).updateUser(user);
            SessionUtil.setUser(session, user);
        }
    }

    /**
     * Provides the portal view
     * @param request
     * @return portalView
     * @throws ClientException
     * @throws PermissionException
     */
    public static PortalView getPortalView(HttpServletRequest request) throws ClientException, PermissionException {
        HttpSession session = request.getSession(true);
        PortalView portalView = (PortalView) session.getAttribute("portalView");
        if (portalView == null) {
            portalView = new PortalViewImpl();
            ((PortalViewImpl) portalView).init(request);
            session.setAttribute("portalView", portalView);
        }
        return portalView;
    }

    public static PortalView ssoLogin(HttpServletRequest request, Long userId, String sid) {
        String loginName = request.getRemoteUser();
        if (loginName != null) {
            String remoteAddress = RequestUtil.getRemoteAddress(request);
            try {
                PortalView view = getPortalView(request);
                if (view.getWebConfig() != null && view.getWebConfig().isSsoEnabled()
                        && (view.getUser() == null || !loginName.equals(view.getUser().getLdapUid()))) {

                    log.debug("ssoLogin: authenticated user found [host="
                            + remoteAddress + ", remoteUser=" + loginName + "]");

                    User user = view.login(loginName, remoteAddress);
                    if (user != null) {
                        SessionUtil.setUser(request.getSession(), user);
                        request.getSession().setAttribute("org.osserp.app.session.master.ID", sid);
                        if (!user.getId().equals(userId)) {
                            if (log.isInfoEnabled()) {
                                log.info("ssoLogin() invoking userId differs from sso user [userId="
                                        + userId + ", user=" + user.getId() + "]");
                            }
                        }
                    }
                }
                return view;
            } catch (Exception e) {
                log.debug("ssoLogin: caught ignored exception [message="
                        + e.getMessage() + ", ip=" + remoteAddress + ", remoteUser=" + loginName + "]");

            }
        } else if (log.isInfoEnabled()) {
            log.info("ssoLogin() no sso user found [userId=" + userId + ", callerSessionId=" + sid + "]");
        }
        return null;
    }

}
