<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="projectTrackingView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="styles" type="string">
        <style type="text/css">
        .listsummary { text-align: right; }
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <o:out value="${view.businessCase.id}" />
        <o:out value="${view.businessCase.name}" />
        <c:if test="${!empty view.headerName}">- <fmt:message key="${view.headerName}" />
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="contractContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.createMode or view.editMode}">
                        <v:form id="projectTrackingForm" url="/projects/projectTracking/save">
                            <c:choose>
                                <c:when test="${view.createMode}">
                                    <c:import url="${viewdir}/projects/_projectTrackingCreate.jsp" />
                                </c:when>
                                <c:otherwise>
                                    <c:import url="${viewdir}/projects/_projectTrackingEdit.jsp" />
                                </c:otherwise>
                            </c:choose>
                        </v:form>
                    </c:when>
                    <c:when test="${!empty view.bean}">

                        <c:choose>
                            <c:when test="${view.textEditMode}">
                                <v:form id="projectTrackingForm" url="/projects/projectTracking/saveText">
                                    <c:import url="${viewdir}/projects/_projectTrackingInputRecord.jsp" />
                                </v:form>
                            </c:when>
                            <c:when test="${empty view.bean.records}">
                                <c:import url="${viewdir}/projects/_projectTrackingDisplay.jsp" />
                            </c:when>
                            <c:otherwise>
                                <c:import url="${viewdir}/projects/_projectTrackingRecords.jsp" />
                                <c:import url="${viewdir}/projects/_projectTrackingDisplay.jsp" />
                            </c:otherwise>
                        </c:choose>

                    </c:when>
                    <c:otherwise>
                        <c:import url="${viewdir}/projects/_projectTrackingList.jsp" />
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
