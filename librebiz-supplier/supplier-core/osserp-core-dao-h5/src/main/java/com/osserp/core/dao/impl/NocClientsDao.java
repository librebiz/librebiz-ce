/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2009 2:52:36 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.Tables;
import com.osserp.common.mail.MailDomain;

import com.osserp.core.dao.NocClients;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.MailDomainImpl;
import com.osserp.core.model.system.NocClientImpl;
import com.osserp.core.system.NocClient;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class NocClientsDao extends AbstractDao implements NocClients {
    private static Logger log = LoggerFactory.getLogger(NocClientsDao.class.getName());
    private String domainsTable = null;

    public NocClientsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        this.domainsTable = getTable(TableKeys.MAIL_DOMAINS);
    }

    public List<NocClient> getAllClients() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(NocClientImpl.class.getName()).append(" o order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public NocClient getDefaultClient() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(NocClientImpl.class.getName()).append(" o where o.defaultClient = true");
        List<NocClient> result = getCurrentSession().createQuery(hql.toString()).list();
        NocClient client = (result.isEmpty() ? null : result.get(0));
        if (client == null && !getAllClients().isEmpty()) {
            log.warn("Multiple clients found but default client not defined");
            return null;
        }
        return client != null ? client : createDefaultClient();
    }

    private NocClient createDefaultClient() {
        String name = getPropertyString("ldapClientName");
        if (name == null) {
            name = Constants.DEFAULT_CLIENT_NAME;
        }
        NocClientImpl client = new NocClientImpl(name);
        client.setDefaultClient(true);
        getCurrentSession().save(NocClientImpl.class.getName(), client);
        return client;
    }

    public NocClient getClient(Long id) {
        return (NocClient) getCurrentSession().load(NocClientImpl.class.getName(), id);
    }

    public void save(NocClient nocClient) {
        getCurrentSession().saveOrUpdate(NocClientImpl.class.getName(), nocClient);
    }

    public List<MailDomain> getAllDomains() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(MailDomainImpl.class.getName()).append(" o order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public List<MailDomain> getDomains(Long clientId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(MailDomainImpl.class.getName())
            .append(" o where o.reference = :clientId order by o.name");
        return getCurrentSession().createQuery(
                hql.toString()).setParameter("clientId", clientId).list();
    }

    public MailDomain getDomain(Long id) {
        return (MailDomain) getCurrentSession().load(MailDomainImpl.class.getName(), id);
    }

    public List<String> getSupportedDomainNames() {
        StringBuilder sql = new StringBuilder("SELECT name FROM ");
        sql.append(domainsTable).append(" ORDER BY name ");
        return (List<String>) jdbcTemplate.query(sql.toString(), getStringRowMapper());
    }

    public MailDomain createDomain(
            Long user,
            Long client,
            MailDomain parentDomain,
            String name,
            String provider,
            Date domainCreated,
            Date domainExpires)
            throws ClientException {
        if (isEmpty(client)) {
            throw new IllegalArgumentException("client id required but missing");
        }
        if (isEmpty(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        List<MailDomain> allDomains = getAllDomains();
        for (int i = 0, j = allDomains.size(); i < j; i++) {
            MailDomain next = allDomains.get(i);
            if (next.getName().equalsIgnoreCase(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        List<MailDomain> clientDomains = getDomains(client);
        for (int i = 0, j = clientDomains.size(); i < j; i++) {
            MailDomain next = clientDomains.get(i);
            if (!next.isAliasOnly() && parentDomain == null) {
                throw new ClientException(ErrorCode.PRIMARY_DOMAIN_EXISTS);
            }
        }
        MailDomain domain = new MailDomainImpl(user, client, parentDomain, name, provider, domainCreated, domainExpires);
        getCurrentSession().saveOrUpdate(MailDomainImpl.class.getName(), domain);
        if (log.isDebugEnabled()) {
            log.debug("createDomain() done [id=" + domain.getId() + ", name=" + name + ", client=" + client + "]");
        }
        return domain;
    }

    public void save(MailDomain domain) {
        getCurrentSession().saveOrUpdate(MailDomainImpl.class.getName(), domain);
    }
}
