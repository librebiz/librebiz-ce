/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13-Aug-2005 08:02:10 
 * 
 */
package com.osserp.core.users;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class Permissions {

    public static final String ACCOUNTING = "accounting";
    public static final String CUSTOMER_SERVICE = "customer_service";
    public static final String TELEPHONE_CONFIGURATION_OWN_EDIT = "telephone_configuration_own_edit";
    public static final String EXECUTIVE = "executive";
    public static final String EXECUTIVE_ACCOUNTING = "executive_accounting";
    public static final String EXECUTIVE_BRANCH = "executive_branch";
    public static final String EXECUTIVE_LOGISTICS = "executive_logistics";
    public static final String EXECUTIVE_SAAS = "executive_saas";
    public static final String EXECUTIVE_SALES = "executive_sales";
    public static final String EXECUTIVE_TECSTAFF = "executive_tecstaff";
    public static final String HISTORICAL_RECORD_IMPORT = "record_historical_import";
    public static final String GLOBAL_BUSINESS_CASE_ACCESS = "global_business_case_access";
    public static final String LOGISTICS_ONLY = "logistics_only";
    public static final String OFFICE_MANAGEMENT = "office_management";
    public static final String OPEN_PROJECT_LIST = "open_project_list";
    public static final String PURCHASING = "purchasing";
    public static final String SALES = "sales";
    public static final String SALES_ONLY = "sales_only";
    public static final String TELEPHONE_EXCHANGE_CLICK_TO_CALL = "telephone_exchange_click_to_call";
    public static final String TELEPHONE_EXCHANGE_EDIT = "telephone_exchange_edit";
    public static final String TELEPHONE_EXCHANGE_CALLS = "telephone_exchange_calls";
    public static final String TELEPHONE_SYSTEM_ADMIN = "telephone_system_admin";
    public static final String WHOLESALE = "wholesale";

    public static final String[] BRANCH_IGNORE_PERMISSIONS = {
            "executive",
            "requests_queries_by_branch",
            "sales_monitoring_branch_selection",
            "global_business_case_access"
    };

    public static boolean isAccounting(DomainUser user) {
        return hasPermission(user, ACCOUNTING);
    }

    public static boolean isExecutiveAccounting(DomainUser user) {
        return hasPermission(user, EXECUTIVE_ACCOUNTING);
    }

    public static boolean isCustomerService(DomainUser user) {
        return hasPermission(user, CUSTOMER_SERVICE);
    }

    public static boolean isExecutive(DomainUser user) {
        return hasPermission(user, EXECUTIVE);
    }

    public static boolean isExecutiveSaaS(DomainUser user) {
        return hasPermission(user, EXECUTIVE_SAAS);
    }

    public static boolean isLogisticsExecutive(DomainUser user) {
        return (hasPermission(user, EXECUTIVE)
                || hasPermission(user, PURCHASING)
                || hasPermission(user, EXECUTIVE_LOGISTICS));
    }

    public static boolean isLogisticsOnly(DomainUser user) {
        return (hasPermission(user, LOGISTICS_ONLY));
    }

    public static boolean isBranchExecutive(DomainUser user) {
        return hasPermission(user, EXECUTIVE_BRANCH);
    }

    public static boolean isGlobalBusinessCaseAccessGrant(DomainUser user) {
        return hasPermission(user, GLOBAL_BUSINESS_CASE_ACCESS);
    }

    public static boolean isOfficeManagement(DomainUser user) {
        return hasPermission(user, OFFICE_MANAGEMENT);
    }

    public static boolean isProjectExecutive(DomainUser user) {
        return hasPermission(user, EXECUTIVE_TECSTAFF);
    }

    public static boolean isSalesExecutive(DomainUser user) {
        return hasPermission(user, EXECUTIVE_SALES);
    }

    public static boolean isSalesOnly(DomainUser user) {
        return hasPermission(user, SALES_ONLY);
    }

    public static boolean hasPermission(DomainUser user, String permission) {
        return (user.getPermissions().containsKey(permission));
    }

}
