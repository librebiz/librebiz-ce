/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 1, 2005 9:41:49 AM 
 * 
 */
package com.osserp.common.web.tags;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPopupTag extends AbstractLinkTag {

    protected static final String POPUP_START =
            "<a href=\"javascript:;\" onclick=\"window.open('";

    /** Dimension of the popup */
    protected String dimension = null;

    /**
     * Returns the dimension
     * @return dimension.
     */
    public String getDimension() {
        return dimension;
    }

    /**
     * Sets the dimension
     * @param dimension
     */
    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    /** Header of the popup */
    protected String header = null;

    /**
     * Returns the header
     * @return header.
     */
    public String getHeader() {
        return header;
    }

    /**
     * Sets the header
     * @param header
     */
    public void setHeader(String header) {
        this.header = header;
    }

    protected String getHeaderString() {
        return header == null ? "" : getResourceString(header);
    }

    /** Type param */
    protected String type = null;

    /**
     * Returns the type
     * @return type.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /** Resizable param */
    protected boolean resizable = false;

    /**
     * Returns resizable
     * @return resizable
     */
    public boolean isResizable() {
        return resizable;
    }

    /**
     * Sets resizable
     * @param resizable
     */
    public void setResizable(boolean resizable) {
        this.resizable = resizable;
    }

    /** Scroll param */
    protected boolean scroll = false;

    /**
     * Returns scroll
     * @return scroll
     */
    public boolean isScroll() {
        return scroll;
    }

    /**
     * Sets scroll
     * @param scroll
     */
    public void setScroll(boolean scroll) {
        this.scroll = scroll;
    }

    @Override
    public void release() {
        super.release();
        this.dimension = null;
        this.header = null;
        this.type = null;
        this.resizable = false;
        this.scroll = false;
    }

}
