<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>


<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="capitalFlow" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table">
                <tbody>
                    <tr>
                        <td>
                            <c:choose>
                                <c:when test="${!empty view.bean.cashInList and not view.cashInListMode}">
                                    <v:link url="/accounting/annualReport/enableCashInListMode">
                                        <fmt:message key="cashInLabel" />
                                    </v:link>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="cashInLabel" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="right"><o:number value="${view.bean.cashIn}" format="currency" /></td>
                    </tr>
                    <tr>
                        <td>
                            <c:choose>
                                <c:when test="${!empty view.bean.cashOutList and not view.cashOutListMode}">
                                    <v:link url="/accounting/annualReport/enableCashOutListMode">
                                        <fmt:message key="cashOutLabel" />
                                    </v:link>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="cashOutLabel" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="right"><o:number value="${view.bean.cashOut}" format="currency" /></td>
                    </tr>
                    <tr><td colspan="2"> </td></tr>
                    <tr>
                        <td>
                            <c:choose>
                                <c:when test="${!empty view.bean.salesRecords and not view.salesListMode}">
                                    <v:link url="/accounting/annualReport/enableSalesListMode">
                                        <fmt:message key="debitors" />
                                    </v:link>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="debitors" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="right"><o:number value="${view.bean.salesAmount}" format="currency" /></td>
                    </tr>
                    <tr>
                        <td>
                            <c:choose>
                                <c:when test="${!empty view.bean.supplierRecords and not view.supplierListMode}">
                                    <v:link url="/accounting/annualReport/enableSupplierListMode">
                                        <fmt:message key="creditors" />
                                    </v:link>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="creditors" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="right"><o:number value="${view.bean.purchaseTotalAmount}" format="currency" /></td>
                    </tr>
                    <tr><td colspan="2"> </td></tr>
                    <tr>
                        <td><fmt:message key="taxPayments" /></td>
                        <td class="right"><o:number value="${view.bean.taxPaymentAmount}" format="currency" /></td>
                    </tr>
                    <tr>
                        <td><fmt:message key="taxRefunds" /></td>
                        <td class="right"><o:number value="${view.bean.taxRefundAmount}" format="currency" /></td>
                    </tr>
                    <tr><td colspan="2"> </td></tr>
                    <tr>
                        <td><fmt:message key="salaryCosts" /></td>
                        <td class="right"><o:number value="${view.bean.salaryAmount}" format="currency" /></td>
                    </tr>
                    <c:if test="${view.bean.salaryRefundAmount > 0}">
                        <tr>
                            <td><fmt:message key="salaryCostsRefunds" /></td>
                            <td class="right"><o:number value="${view.bean.salaryRefundAmount}" format="currency" /></td>
                        </tr>
                    </c:if>
                    <tr><td colspan="2"> </td></tr>
                    <c:if test="${!empty view.bankAccounts}">
                        <tr class="altrow">
                            <td><fmt:message key="accountTransactions" /></td>
                            <td style="text-align:right;"><fmt:message key="bankAccountDocumentShort" /></td>
                        </tr>
                        <c:forEach var="obj" items="${view.bankAccounts}">
                            <tr>
                                <o:permission role="accounting,accounting_bank_display,organisation_admin" info="permissionViewBankAccounts">
                                    <td>
                                        <v:link url="/accounting/paymentRecords/forward?account=${obj.id}&year=${view.year}&exit=/accounting/annualReport/reload">
                                            <o:out value="${obj.name}" />
                                        </v:link>
                                    </td>
                                    <td class="right">
                                        <c:choose>
                                            <c:when test="${!empty obj.accountDocuments}">
                                                <v:link url="/accounting/annualReport/selectBankAccountDocuments?id=${obj.id}">
                                                    <o:img name="pdfIcon"/>
                                                </v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <span title="<fmt:message key="noDocumentAvailable" />">
                                                    <o:img name="toggleStoppedFalseIcon"/>
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </o:permission>
                                <o:forbidden role="accounting,accounting_bank_display,organisation_admin">
                                    <td colspan="2"><o:out value="${obj.name}" /></td>
                                </o:forbidden>
                            </tr>
                        </c:forEach>
                    </c:if>
                    <tr><td colspan="2"> </td></tr>
                    <tr class="altrow">
                        <td><fmt:message key="downloadDocumentsAsArchive" /></td>
                        <td style="text-align:right;">
                            <v:link url="/accounting/annualReport/downloadArchive" title="downloadDocumentsAsArchiveTitle">
                                <o:img name="downloadIcon"/>
                            </v:link>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
