<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="recordInfoEditorView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="recordInfoEditorContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.editMode or view.createMode}">
                        <c:import url="_recordInfoEdit.jsp" />
                    </c:when>
                    <c:otherwise>
                        <div class="col-lg-12 panel-area">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <tbody>
                                        <c:forEach var="option" items="${view.list}">
                                            <tr>
                                                <td class="icon center">
                                                    <v:link url="/admin/records/recordInfoEditor/toggleEol?id=${option.id}">
                                                        <c:choose>
                                                            <c:when test="${option.endOfLife}">
                                                                <o:img name="enableIcon" styleClass="bigicon" title="activate" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <o:img name="enabledIcon" styleClass="bigicon" title="deactivate" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </v:link>
                                                </td>
                                                <td class="icon center">
                                                    <v:link url="/admin/records/recordInfoEditor/select?id=${option.id}">
                                                        <o:img name="writeIcon" styleClass="bigicon" title="changeText" />
                                                    </v:link>
                                                </td>
                                                <td><o:out value="${option.name}" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
