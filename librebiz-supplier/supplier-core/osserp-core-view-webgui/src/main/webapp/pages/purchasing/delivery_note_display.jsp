<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.purchaseDeliveryNoteView or empty sessionScope.purchaseDeliveryNoteView.bean}">
    <c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.purchaseDeliveryNoteView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="project" value="${sessionScope.businessCaseView.sales}"/>
<c:set var="baseUrl" scope="request" value="/purchaseDeliveryNote.do"/>
<c:set var="bookRecordUrl" scope="request" value="/purchaseDeliveryNote.do"/>
<c:set var="deleteRecordUrl" scope="request" value="/purchaseDeliveryNote.do"/>
<c:set var="productUrl" scope="request" value="/purchaseDeliveryNoteProducts.do"/>
<c:set var="productExitTarget" scope="request" value="purchaseDeliveryNoteProductReload"/>
<c:set var="productAddEnabled" scope="request" value="true"/>
<o:permission role="purchase_delivery_create" info="permissionChangeSupplierReferenceNumber">
<c:set var="supplierReferenceNumberPermissionGrant" scope="request" value="true"/>
</o:permission>


<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
    <style type="text/css">
        <c:import url="/css/records.css"/>
    </style>
</tiles:put>

<tiles:put name="headline"><fmt:message key="stockReceiptTo"/> <o:out value="${record.contact.displayName}"/></tiles:put>
<c:choose>
<c:when test="${recordView.setupMode}">
<tiles:put name="headline_right">
<a href="<c:url value="/purchaseDeliveryNote.do?method=disableSetup"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>
<tiles:put name="content" type="string">
<c:import url="/pages/purchasing/delivery_note_settings.jsp"/>
</tiles:put>
</c:when>

<c:otherwise>
<tiles:put name="headline_right">
<a href="<c:url value="/purchaseDeliveryNote.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<c:if test="${!record.unchangeable}">
<a href="<c:url value="/purchaseDeliveryNote.do?method=confirmDelete"/>" title="<fmt:message key="title.record.delete"/>"><o:img name="deleteIcon"/></a>
<a href="javascript:onclick=confirmLink('<fmt:message key="confirmRecordRelease"/>','<c:url value="/purchaseDeliveryNote.do?method=release"/>');" title="<fmt:message key="recordRelease"/>"><o:img name="enabledIcon"/></a>
</c:if>
<c:if test="${record.unchangeable}">
<o:permission role="purchase_delivery_note_change" info="permissionPurchaseDeliveryNoteChange">
<a href="<c:url value="/purchaseDeliveryNote.do?method=enableSetup"/>" title="<fmt:message key="enhancedFunctionality"/>"><o:img name="configureIcon"/></a>
</o:permission>
</c:if>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                
                    <c:import url="/pages/records/record_delete_dialog.jsp"/>
                
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
                
                    <div class="column66l">
                        <div class="recordLabelValue">
                            <fmt:message key="deliveryToPurchaseOrderLabel"/>:
                            <span style="margin-left: 20px;">
                                <c:choose>
                                    <c:when test="${empty record.reference}"><fmt:message key="none"/></c:when>
                                    <c:when test="${!recordView.provideOrderLink}"><o:out value="${record.referenceNumber}"/></c:when>
                                    <c:otherwise>
                                        <a href="<c:url value="/purchaseOrder.do?method=display&id=${record.reference}&exit=${recordView.exitTarget}"/>" style="margin-left: 20px;">
                                            <o:out value="${record.referenceNumber}"/>
                                        </a>
                                    </c:otherwise>
                                </c:choose>
                            </span>
                        </div>
                        <c:import url="/pages/purchasing/supplier_reference.jsp"/>
                        <c:import url="/pages/records/record_note_display.jsp"/>
                    </div>
                    <div class="column33r">
                       <c:import url="/pages/records/record_status.jsp"/>
                    </div>
                    
                </div>
            </div>
        </div>
        <br />
        <c:import url="/pages/records/delivery_items.jsp"/>
    </div>
</div>
</tiles:put> 
</c:otherwise>
</c:choose>
</tiles:insert>
<c:remove var="recordView" scope="session"/>
