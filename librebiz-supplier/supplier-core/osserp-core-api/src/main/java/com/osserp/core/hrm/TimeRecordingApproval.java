/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.hrm;

import java.util.Date;

import com.osserp.common.EntityRelation;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordingApproval extends EntityRelation {

    static final Long STATUS_APPROVED = 1L;
    static final Long STATUS_NEW = 0L;
    static final Long STATUS_NOT_APPROVED = -1L;

    /**
     * Provides an updated record for approval
     * @return the record if update event
     */
    TimeRecord getRecord();

    /**
     * Provides the correction record if update event
     * @return correction
     */
    TimeRecordCorrection getCorrection();

    /**
     * Provides the type for approval
     * @return the type if create event
     */
    TimeRecordType getType();

    /**
     * Provides the status of approval<br/>
     * approved = 1<br/>
     * new = 0<br/>
     * not approved = -1
     * @return the status
     */
    Long getStatus();

    /**
     * Updates approval status
     * @param user
     * @param status
     * @param update indicates create or update mode
     */
    void updateStatus(Employee user, Long status, boolean update);

    /**
     * The start date of booking
     * @return the startDate
     */
    Date getStartDate();

    /**
     * The end date of booking
     * @return the endDate
     */
    Date getEndDate();

    /**
     * A note
     * @return the note
     */
    String getNote();

    /**
     * Indicates that approval is required
     * @return approvalRequired
     */
    boolean isApprovalRequired();
}
