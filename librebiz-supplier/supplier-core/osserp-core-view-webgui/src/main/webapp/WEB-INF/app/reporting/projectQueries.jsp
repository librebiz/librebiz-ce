<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:userContext/>
<v:view viewName="projectQueriesView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>

	<tiles:put name="headline">
		<c:choose>
			<c:when test="${!empty view.list}">
				<o:listSize value="${view.list}"/> <fmt:message key="${view.headerName}"/>
				<c:if test="${!view.report.current}">
					[<c:if test="${view.currentListMonthly}"><o:out value="${view.report.monthDisplay}"/> / </c:if><o:out value="${view.report.yearDisplay}"/>]
				</c:if>
			</c:when>
			<c:otherwise>
				<fmt:message key="orderBacklog"/>
			</c:otherwise>
		</c:choose>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area" id="projectQueriesContent">
            <div class="row">
    			<c:choose>
                    <c:when test="${view.listMode}">
                        <c:set var="selectUrl" scope="request" value="/loadSales.do"/>
                        <c:set var="selectExitTarget" scope="request" value="projectQueriesReload"/>
                        <c:set var="collection" scope="request" value="${view.list}"/>
                        <c:import url="_projectList.jsp"/>
                    </c:when>
                    <c:when test="${view.listUnreferencedMode}">
                        <c:set var="selectExitTarget" scope="request" value="projectQueriesReload"/>
                        <c:set var="collection" scope="request" value="${view.list}"/>
                        <div class="subcolumn">
                            <c:import url="_unreferencedList.jsp"/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${!empty view and !empty view.report}">
                            <c:import url="_projectStats.jsp"/>
                        </c:if>
                        <c:import url="_projectQueries.jsp"/>
    				</c:otherwise>
	       		</c:choose>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
