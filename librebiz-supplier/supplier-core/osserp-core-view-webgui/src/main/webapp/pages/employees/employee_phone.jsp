<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${contactView.bean}" />
<div class="contentBox">
	<div class="contentBoxHeader">
		<div class="contentBoxHeaderLeft">
			<fmt:message key="office"/>
		</div>
		<div class="contentBoxHeaderRight">
			<oc:systemPropertyEnabled name="telephoneSupport">
				<c:if test="${!empty contact.roleConfigs}">
					<div class="boxnav">
						<ul>
							<li><o:ajaxLink url="/officePhones.do?method=forward" linkId="office_phone"><o:img name="newIcon"/></o:ajaxLink></li>
						</ul>
					</div>
				</c:if>
			</oc:systemPropertyEnabled>
		</div>
	</div>
	<div class="contentBoxData">
		<div class="smallSpacer"></div>
		<table class="valueTable first33">
			<tbody>
				<c:if test="${!empty contact.internalPhone.number}">
					<tr>
						<td><fmt:message key="external"/></td>
						<td><oc:phone value="${contact.internalPhone}"/></td>
					</tr>
				</c:if>
				<c:if test="${!empty contact.internalPhone}">
					<tr>
						<td><fmt:message key="directDial"/></td>
						<td>
							<c:choose>
								<c:when test="${editTelephoneConfigurationPermission or (contactView.userCurrentContact and editOwnTelephoneConfigurationPermission)}">
									<script src="<c:url value="/js/wz_tooltip.js"/>" type="text/javascript"></script>
									<c:set var="telephoneConfiguration"><fmt:message key="telephoneConfiguration"/></c:set>
									<v:ajaxLink url="/telephones/telephoneConfigurationPopup/load?employeeId=${contact.id}" linkId="telephoneConfigurationPopupView" title="${telephoneConfiguration}">
										<o:out value="${contact.internalPhone.internal}"/>
									</v:ajaxLink>
								</c:when>
								<c:otherwise>
									<o:out value="${contact.internalPhone.internal}"/>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:if>
				<%-- RECORD PHONE SELECTION --%>
				<c:choose>
					<c:when test="${!contactView.recordPhoneSelectionMode}">
						<tr>
							<td><a href="<c:url value="/employees.do?method=enableRecordPhoneSelectionMode"/>"><fmt:message key="coverLetter"/></a></td>
							<td>
								<c:choose>
									<c:when test="${contact.printPhoneDevice == officeDev}">
										<oc:phone value="${contact.internalPhone}"/>
									</c:when>
									<c:when test="${contact.printPhoneDevice == phoneDev}">
										<oc:phone value="${contact.phone}"/>
									</c:when>
									<c:when test="${contact.printPhoneDevice == mobileDev}">
										<oc:phone value="${contact.mobile}"/>
									</c:when>
									<c:otherwise>
										<fmt:message key="notAssigned"/>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<td><a href="<c:url value="/employees.do?method=disableRecordPhoneSelectionMode"/>"><fmt:message key="coverLetter"/></a></td>
							<td>
								<c:if test="${!empty contact.internalPhone and !empty contact.internalPhone.number}">
									<a href="<c:url value="/employees.do"><c:param name="method" value="selectRecordPhone"/><c:param name="id" value="${officeDev}"/></c:url>">
										<o:out value="${contact.internalPhone.country}"/> - <o:out value="${contact.internalPhone.prefix}"/> - <o:out value="${contact.internalPhone.number}" limit="25"/>
									</a>
									<br />
								</c:if>
								<c:if test="${!empty contact.phone and !empty contact.phone.number}">
									<a href="<c:url value="/employees.do"><c:param name="method" value="selectRecordPhone"/><c:param name="id" value="${phoneDev}"/></c:url>">
										<o:out value="${contact.phone.country}"/> - <o:out value="${contact.phone.prefix}"/> - <o:out value="${contact.phone.number}" limit="25"/>
									</a>
									<br />
								</c:if>
								<c:if test="${!empty contact.mobile and !empty contact.mobile.number}">
									<a href="<c:url value="/employees.do"><c:param name="method" value="selectRecordPhone"/><c:param name="id" value="${mobileDev}"/></c:url>">
										<o:out value="${contact.mobile.country}"/> - <o:out value="${contact.mobile.prefix}"/> - <o:out value="${contact.mobile.number}" limit="25"/>
									</a>
								</c:if>
							</td>
						</tr>
					</c:otherwise>
				</c:choose>
				<c:import url="/pages/employees/employee_website_phone.jsp"/>
			</tbody>
		</table>
		<div class="smallSpacer"></div>
	</div>
</div>
<div class="spacer"></div>
