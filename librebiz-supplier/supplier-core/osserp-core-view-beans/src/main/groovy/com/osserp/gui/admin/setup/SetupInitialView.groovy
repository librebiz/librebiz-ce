/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 22, 2017 
 * 
 */
package com.osserp.gui.admin.setup

import com.osserp.common.Constants
import com.osserp.common.web.PortalView

import com.osserp.core.system.InitialSetupService

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreAdminView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class SetupInitialView extends CoreAdminView {

    boolean companyUpdate = false
    boolean adminUpdate = false
    boolean customFederalState = false

    SetupInitialView() {
        super()
        permissions = 'organisation_admin,runtime_config'
        enableCompanyPreselection()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        
        if (forwardRequest) {
            if (multipleClientsAvailable || !selectedCompany) {
                throw new ViewContextException('invalid state')
            }
            form.setValue('country', Constants.GERMANY)
            addressAutocompletionSupport = addressAutocompletionSupportEnabled
            enableCreateMode()
        }
    }

    @Override
    void save() {
        if (!updateOnly && !companyUpdate && selectedCompany) {
            String newLoginName = form.getString('loginname')
            def adminUser = initialSetupService.initCompany(
                    domainUser,
                    selectedCompany,
                    form.getString('company'),
                    getOption('salutations', form.getLong('salutation')),
                    getOption('titles', form.getLong('title')),
                    form.getString('lastName'),
                    form.getString('firstName'), 
                    form.getString('street'),
                    form.getString('zipcode'),
                    form.getString('city'),
                    form.getLong('country'),
                    form.getLong('federalStateId'),
                    form.getString('pcountry'),
                    form.getString('pprefix'),
                    form.getString('pnumber'),
                    form.getString('fcountry'),
                    form.getString('fprefix'),
                    form.getString('fnumber'),
                    form.getString('email'),
                    newLoginName,
                    form.getString('password'),
                    form.getString('confirmPassword'),
                    form.getString('currentPassword'))
            if (adminUser.ldapUid == newLoginName) {
                companyUpdate = true
            }
        }
    }

    PortalView getPortalView() {
        env.portalView
    }

    protected InitialSetupService getInitialSetupService() {
        getService(InitialSetupService.class.getName())
    }

}
