/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 28, 2015 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PaymentDisplay extends FinanceRecordDisplay {

    /**
     * Provides the payment date or planned date if amountToPay is set
     * and grossAmount is not set 
     * @return date paid on
     */
    Date getPaidOn();
    
    /**
     * Provides the year part of the paid date
     * @return year
     */
    int getPaidInYear();
    
    /**
     * Indicates if payment includes reduced tax
     * @return true if includes reduced tax
     */
    boolean isTaxReduced();

    /**
     * Provides the record note
     * @return note
     */
    String getNote();
    
    /**
     * Provides the related invoice id if such a relation exists
     * @return invoice id or null for common payments
     */
    Long getInvoiceId();
    
    /**
     * Provides the invoice id as formatted string
     * @return invoid id as string or null if not set.
     */
    String getInvoiceNumber();

    /**
     * Provides the id of the bank account
     * @return bank account id
     */
    Long getBankAccountId();

    /**
     * Provides the id of the type of the related invoice 
     * @return record type id of invoice or null if no invoice related
     */
    Long getRecordTypeId();
    
    /**
     * Indicates if payment is not related to any other record
     * @return true if common payment
     */
    boolean isCommonPayment();
    
    /**
     * Indicates if payment is related to a purchase invoice
     * @return true if related to purchase invoice
     */
    boolean isPurchaseInvoicePayment();
    
    /**
     * Indicates if payment is related to a sales order downpayemt
     * @return true if related to sales order downpayment
     */
    boolean isSalesDownpayment();
    
    /**
     * Indicates if payment is related to a sales invoice
     * @return true if related to sales invoice
     */
    boolean isSalesInvoicePayment();

    /**
     * Indicates if payment is internal account or own bank to own bank booking
     * @return true if books only, ignore in summaries
     */
    boolean isBooksOnly();
    
    /**
     * Indicates if amount is an outflow or income of cash
     * @return true if outflow of cash
     */
    boolean isOutflowOfCash();

    /**
     * Indicates if payment represents an account status only (e.g. summary).
     * @return true if not real payment
     */
    boolean isAccountStatusOnly();

    /**
     * Account status name i18n key used as type name
     * @return account status name
     */
    String getAccountStatusName();

    /**
     * Account status summary at the time of this payment. This value is not
     * persistent and must be calculated and set after fetching the list.
     * @return account status name
     */
    Double getAccountStatusSummary();
    
    /**
     * Sets the summary if required by lists.
     * @param summary
     */
    void setAccountStatusSummary(Double summary);
}
