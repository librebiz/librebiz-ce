/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 5, 2005 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractLinkTag extends AbstractBeanTag {
    private static Logger log = LoggerFactory.getLogger(AbstractLinkTag.class.getName());
    
    protected static final String LINK_START = "<a href=\"";
    protected static final String TAG_END = ">";
    protected static final String LINK_END = "</a>";

    /**
     * Implement this method to create the link
     * @return startTag &lt;a href...&gt;
     */
    public abstract String getStartTag() throws JspException;

    /** The action to execute when link is clicked */
    private String action = null;

    /**
     * Returns the action
     * @return action.
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the action to execute
     * @param action
     */
    public void setAction(String action) {
        this.action = action;
    }

    /** Target of link */
    protected String target = null;

    /**
     * Returns the target
     * @return target.
     */
    public String getTarget() {
        return target;
    }

    /**
     * Sets the target
     * @param target
     */
    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print(getStartTag());
        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print(LINK_END);
        } catch (IOException ioe) {
            log.error("doEndTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    protected void appendEnd(StringBuilder buffer) {
        buffer.append(renderStyles());
        buffer = addTitle(buffer);
        if (getTarget() != null) {
            buffer.append(" target=\"").append(getTarget()).append("\"");
        }
        buffer.append(">");
    }

    protected String createUrl() {
        return createUrl(action);
    }

    @Override
    public void release() {
        super.release();
        action = null;
        target = null;
    }
}
