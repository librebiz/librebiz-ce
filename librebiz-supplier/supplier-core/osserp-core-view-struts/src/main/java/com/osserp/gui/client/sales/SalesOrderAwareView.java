/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 5, 2008 7:37:48 PM 
 * 
 */
package com.osserp.gui.client.sales;

import com.osserp.common.ClientException;
import com.osserp.core.sales.Sales;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.records.OrderView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrderAwareView extends OrderView {

    /**
     * Loads sales order by business case
     * @param businessCaseView
     * @throws ClientException
     */
    public void load(BusinessCaseView businessCaseView) throws ClientException;

    /**
     * Provides referenced businessCaseView
     * @return businessCaseView view or null
     */
    public BusinessCaseView getBusinessCaseView();

    /**
     * Creates a packagelist with all open delivery items
     * @return packagelist as pdf stream
     * @throws ClientException
     */
    public byte[] createPackagelist() throws ClientException;

    /**
     * Indicates that view is in logistics display mode
     * @return logisticsDisplayMode
     */
    public boolean isLogisticsDisplayMode();

    /**
     * Enables/disables logistics display mode
     * @param modeEnabled
     */
    public void setLogisticsDisplayMode(boolean modeEnabled);

    /**
     * Provides the related sale
     * @return relatedSales or null if no sales is related
     */
    public Sales getRelatedSales();

}
