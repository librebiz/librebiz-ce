/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 1, 2007 1:08:38 AM 
 * 
 */
package com.osserp.common.web.tags;

import org.owasp.encoder.Encode;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TagUtil {
    
    /**
     * Renders a datepicker calendar input popup link with default image.
     * @param contextPath uri as provided by tag or servlet
     * @param input name of the date input field
     * @param autoupdate indicates if form autoupdate should be set to enabled
     * @return calendar image with datepicker popup link 
     */
    public static String createDatepicker(String contextPath, String input, boolean autoupdate) {
        return createDatepicker(contextPath, input, autoupdate, null, null, null); 
    }

    /**
     * Renders a datepicker calendar input popup link with a customized image.
     * @param contextPath
     * @param input
     * @param autoupdate
     * @param image
     * @param imageStyle
     * @param imageStyleClass
     * @return custom image with datepicker popup link 
     */
    public static String createDatepicker(
            String contextPath, 
            String input, 
            boolean autoupdate, 
            String image, 
            String imageStyle, 
            String imageStyleClass) {
        
        StringBuilder buffer = new StringBuilder(
                "<a href=\"javascript:;\" onclick=\"var cal1x = new CalendarPopup('datePicker'");
        if (autoupdate) {
            buffer.append(",'true'");
        }
        buffer
                .append("); cal1x.select($('")
                .append(input)
                .append("'),'anchor1x','dd.MM.yyyy');\" " +
                        "title=\"cal1x.select(document.forms[0].date1x," +
                        "'anchor1x','MM/dd/yyyy'); return false;\" " +
                        "name=\"anchor1x\" id=\"anchor1x\">")
                .append(createImage(imageStyle, imageStyleClass))
                .append("</a>");
        return buffer.toString();
    }
    
    private static String createImage(String style, String styleClass) {
        StringBuilder buffer = new StringBuilder("<i class=\"");
        if (styleClass != null) {
            buffer.append(styleClass);
        } else {
            buffer.append("fa fa-calendar");
        }
        buffer.append("\"");
        if (style != null) {
            buffer.append(" style=\"").append(style).append("\"");
        }
        buffer.append(" aria-hidden=\"true\"></i>");
        return buffer.toString();
    }
    
    public static final StringBuilder createUpdateOnlyFormFields(StringBuilder buffer, boolean updateonly) {
        if (updateonly) {
            buffer.append("\n<input type=\"hidden\" name=\"updateOnly\" value==\"true\" />");
            
        } else {
            buffer.append("\n<input type=\"hidden\" name=\"updateOnly\" value=\"false\" />");
        }
        buffer.append("\n<input type=\"hidden\" name=\"updateOnlyTarget\" value=\"\" />\n");
        return buffer;
    }


    /**
     * Renders a form tag
     * @param id
     * @param name
     * @param url
     * @param commonAttributes
     * @param disableAutoupdate
     * @param updateonly
     * @param tokenName
     * @param tokenValue
     * @return
     */
    public static String createForm(
            String id,
            String name,
            String url,
            String commonAttributes,
            boolean disableAutoupdate,
            boolean updateonly,
            String tokenName,
            String tokenValue) {
        
        StringBuilder buffer = new StringBuilder();
        buffer.append("<form");
        if (id != null && id.length() > 0) {
            buffer.append(" id=\"").append(id).append("\"");
        } else if (name != null && name.length() > 0) {
            buffer.append(" id=\"").append(name).append("\"");
        }
        if (name != null && name.length() > 0) {
            buffer.append(" name=\"").append(name).append("\"");
        } else {
            buffer.append(" name=\"actionForm\"");
        }
        buffer.append(" method=\"POST\"");
        if (url != null) {
            buffer.append(" action=\"").append(url).append("\"");
        } else {
            buffer.append(" action=\"#\"");
        }
        if (commonAttributes != null) {
            buffer.append(commonAttributes).append(">");
        }
        buffer.append(createCsrfInput(tokenName, tokenValue));
        if (!disableAutoupdate) {
            buffer = TagUtil.createUpdateOnlyFormFields(buffer, updateonly);
        }
        return buffer.toString();
    }
    
    public static String createCsrfInput(String tokenName, String tokenValue) {
        StringBuilder buffer = new StringBuilder();
        if (tokenName != null && tokenValue != null) {
            buffer
                .append("\n<input type=\"hidden\" name=\"")
                .append(tokenName)
                .append("\" value=\"")
                .append(tokenValue)
                .append("\"/>");
        }
        return buffer.toString();
    }

    public static String encodeForAttribute(String untrusted) {
        return Encode.forHtmlAttribute(untrusted);
    }

    public static String encodeForElement(String untrusted) {
        return Encode.forHtml(untrusted);
    }

    public static String encodeForUri(String untrusted) {
        return Encode.forUriComponent(untrusted);
    }

    public static String createMapsLink(String provider, String street, String zip, String city) {
        StringBuilder buffer = new StringBuilder();
        String selectedProvider = provider != null ? provider : "osm";
        if (street != null) { buffer.append(street).append(","); }
        if (zip != null) { buffer.append(zip).append(" "); }
        if (city != null) { buffer.append(city); }
        String address = buffer.toString();
        if (address == null || address.length() < 1) {
            return "";
        }
        if (selectedProvider.equals("ggl")) {
            return "https://maps.google.de/?q=" + encodeForUri(address) + "&t=m";
        }
        return "https://www.openstreetmap.org/search?query=" + encodeForUri(address);
    }
}
