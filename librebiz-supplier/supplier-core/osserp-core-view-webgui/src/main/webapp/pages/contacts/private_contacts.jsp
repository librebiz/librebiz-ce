<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.privateContactsView}"><c:redirect url="/errors/error_context.jsp"/></c:if> 
<c:set var="contactView" scope="request" value="${sessionScope.privateContactsView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="headline"> <o:listSize value="${contactView.list}"/> <fmt:message key="contactsFound"/></tiles:put>
	<tiles:put name="headline_right">
		<ul>
			<li><a href="<c:url value="/privateContacts.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
			<li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
		</ul>
	</tiles:put>
	<tiles:put name="content" type="string">
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<div class="table-responsive table-responsive-default">
				<table class="table table-striped">
					<thead>
						<tr>
							<th style="width: 195px;"><fmt:message key="companySlashName"/></th>
							<th style="width: 150px;"><fmt:message key="street"/></th>
							<th style="width: 150px;"><fmt:message key="city"/></th>
							<th style="width: 125px;"><fmt:message key="phone"/></th>
							<th style="width: 125px;"><fmt:message key="mobile"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="privateContact" items="${contactView.list}"	varStatus="s">
							<c:set var="contact" value="${privateContact.contact}"/>
							<tr>
								<td style="width: 195px;">
									<c:choose>
										<c:when test="${!empty privateContact.company or contact.type.contactPerson}">
											<a href="<c:url value="/contactPersons.do?method=load&id=${contact.contactId}&exit=privateContacts"/>"><o:out value="${contact.displayName}"/></a>
										</c:when>
										<c:otherwise>
											<a href="<c:url value="/contacts.do?method=load&id=${contact.contactId}&exit=privateContacts"/>"><o:out value="${contact.displayName}"/></a>
										</c:otherwise>
									</c:choose>
								</td>
								<td style="width: 150px;"><o:out value="${contact.address.street}"/></td>
								<td style="width: 150px;"><o:out value="${contact.address.zipcode}"/> <o:out value="${contact.address.city}"/></td>
								<td style="width: 125px;"><oc:phone value="${contact.phone}"/></td>
								<td style="width: 125px;"><oc:phone value="${contact.mobile}"/></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
