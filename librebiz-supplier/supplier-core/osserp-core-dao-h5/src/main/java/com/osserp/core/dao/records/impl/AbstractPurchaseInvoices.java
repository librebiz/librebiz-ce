/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 28, 2009 10:46:30 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.PurchaseInvoiceAwares;
import com.osserp.core.dao.records.PurchasePayments;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.model.records.PurchaseInvoiceTypeImpl;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPurchaseInvoices extends AbstractPurchaseRecords implements PurchaseInvoiceAwares {

    /**
     * Creates a new purchase invoice dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param cache
     * @param taxRates
     * @param infoConfigs
     * @param paymentConditions
     * @param recordNumberCreator
     * @param recordPrintOptionDefaults
     * @param recordMailLogs
     * @param suppliers
     * @param recordTableKey
     * @param recordItemTableKey
     * @param purchasePayments
     */
    protected AbstractPurchaseInvoices(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Suppliers suppliers,
            String recordTableKey,
            String recordItemTableKey,
            PurchasePayments purchasePayments) {
        super(jdbcTemplate, tables, sessionFactory, cache, taxRates, companies,
                infoConfigs, paymentConditions, recordNumberCreator,
                recordPrintOptionDefaults, recordMailLogs, suppliers, recordTableKey,
                recordItemTableKey, purchasePayments);
    }

    public List<Long> getCreditNoteIds(PurchaseInvoice invoice) {
        StringBuilder sql = new StringBuilder("SELECT id FROM ");
        sql
                .append(getRecordTableName())
                .append(" WHERE reference_id = ")
                .append(invoice.getId())
                .append(" AND book_type_id = ")
                .append(PurchaseInvoiceType.CREDIT_NOTE)
                .append(" ORDER BY id");
        return (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
    }

    @Override
    protected Class<PurchaseInvoiceTypeImpl> getPersistentBookingTypeClass() {
        return PurchaseInvoiceTypeImpl.class;
    }

}
