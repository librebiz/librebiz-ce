/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 22, 2008 
 * 
 */
package com.osserp.core.model.contacts.groups;

import com.osserp.common.beans.AbstractRole;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeRole;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.employees.EmployeeStatus;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeRoleImpl extends AbstractRole implements EmployeeRole {

    private EmployeeGroup group = null;
    private EmployeeStatus status = null;
    private boolean ignoringBranch = false;

    protected EmployeeRoleImpl() {
        super();
    }

    protected EmployeeRoleImpl(
            Employee creatingUser,
            EmployeeRoleConfig reference,
            EmployeeGroup group,
            EmployeeStatus status,
            boolean defaultRole) {
        super(creatingUser.getId(), reference.getId(), defaultRole);
        this.group = group;
        this.status = status;
        this.ignoringBranch = group.isIgnoringBranch();
    }

    protected EmployeeRoleImpl(EmployeeRole role) {
        super(role);
        this.group = (EmployeeGroup) role.getGroup().clone();
        this.status = role.getStatus();
        this.ignoringBranch = role.isIgnoringBranch();
    }

    @Override
    public String getName() {
        if (super.getName() != null) {
            return super.getName();
        }
        return group == null ? null : group.getName();
    }

    public EmployeeGroup getGroup() {
        return group;
    }

    public void setGroup(EmployeeGroup group) {
        this.group = group;
    }

    public EmployeeStatus getStatus() {
        return status;
    }

    public void setStatus(EmployeeStatus status) {
        this.status = status;
    }

    public boolean isIgnoringBranch() {
        return ignoringBranch;
    }

    public void setIgnoringBranch(boolean ignoringBranch) {
        this.ignoringBranch = ignoringBranch;
    }

    @Override
    public Object clone() {
        return new EmployeeRoleImpl(this);
    }
}
