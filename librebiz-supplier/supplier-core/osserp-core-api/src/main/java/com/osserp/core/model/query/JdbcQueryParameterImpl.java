/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.query;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.beans.AbstractOption;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dao.JdbcParameter;
import com.osserp.common.dao.JdbcQuery;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class JdbcQueryParameterImpl extends AbstractOption implements JdbcParameter {
    private static Logger log = LoggerFactory.getLogger(JdbcQueryParameterImpl.class.getName());
    private String typeName = null;
    private String label = null;
    private Integer width = Constants.INT_NULL;
    private boolean optional = false;
    private Object value = null;
    private boolean checkbox = false;
    private boolean select = false;
    private String selectNames = null;
    private String defaultValue = null;

    protected JdbcQueryParameterImpl() {
        super();
    }

    public JdbcQueryParameterImpl(
            String label,
            String name,
            Long reference,
            String value) {
        super(name, reference);
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public boolean isCheckbox() {
        return checkbox;
    }

    protected void setCheckbox(boolean checkbox) {
        this.checkbox = checkbox;
    }

    public boolean isSelect() {
        return select;
    }

    protected void setSelect(boolean select) {
        this.select = select;
    }

    public String getSelectNames() {
        return selectNames;
    }

    protected void setSelectNames(String selectNames) {
        this.selectNames = selectNames;
    }

    public Option getInputType() {
        if (checkbox) {
            return new OptionImpl(JdbcParameter.CHECKBOX, "checkbox");
        } else if (select) {
            return new OptionImpl(JdbcParameter.SELECT, "select");
        } else {
            return new OptionImpl(JdbcParameter.TEXT, "text");
        }
    }

    public void setInputType(Long type, String selectNames) {
        if (CHECKBOX.equals(type)) {
            this.checkbox = true;
            this.select = false;
            this.selectNames = null;
        } else if (SELECT.equals(type)) {
            this.checkbox = false;
            this.select = true;
            this.selectNames = selectNames;
        } else {
            this.checkbox = false;
            this.select = false;
            this.selectNames = null;
        }
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Object getValue() {
        return value;
    }

    protected void setValue(Object value) {
        this.value = value;
    }

    public void setParamValue(String inputValue) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("setParamValue() invoked for " + inputValue);
        }
        try {
            if (JdbcQuery.BOOLEAN.equals(typeName)) {
                value = false;
                if (inputValue != null) {
                    if (inputValue.equalsIgnoreCase("ON")
                            || inputValue.equalsIgnoreCase("true")) {
                        value = true;
                    }
                }
            } else if (JdbcQuery.DOUBLE.equals(typeName)) {
                value = NumberUtil.createDouble(inputValue);

            } else if (JdbcQuery.INTEGER.equals(typeName)) {
                value = Integer.valueOf(inputValue);

            } else if (JdbcQuery.LONG.equals(typeName)) {
                value = Long.valueOf(inputValue);

            } else if (JdbcQuery.TIMESTAMP.equals(typeName)) {
                value = DateUtil.createDate(inputValue, true);

            } else {
                value = inputValue;
            }
        } catch (Throwable t) {
            if (log.isDebugEnabled()) {
                log.debug("setParamValue() failed: " + t.toString());
            }
            throw new ClientException(ErrorCode.VALUES_INVALID);
        }
    }

    public int getType() {
        if (isNotSet(typeName)) {
            return Types.NULL;
        }
        if (JdbcQuery.BOOLEAN.equals(typeName)) {
            return Types.BOOLEAN;
        } else if (JdbcQuery.DOUBLE.equals(typeName)) {
            return Types.DOUBLE;
        } else if (JdbcQuery.INTEGER.equals(typeName)) {
            return Types.INTEGER;
        } else if (JdbcQuery.LONG.equals(typeName)) {
            return Types.BIGINT;
        } else if (JdbcQuery.TIMESTAMP.equals(typeName)) {
            return Types.TIMESTAMP;
        } else {
            return Types.VARCHAR;
        }
    }

    public Element getXML(OptionsCache cache) {
        Element param = new Element("param");
        param.addContent(new Element("class").setText(getInputType().getName()));
        param.addContent(new Element("name").setText(getName()));
        param.addContent(new Element("label").setText(label));
        param.addContent(new Element("type").setText(typeName));
        if (width != null && width > 0) {
            param.addContent(new Element("width").setText(width.toString()));
        } else {
            param.addContent(new Element("width").setText("0"));
        }
        if (isSelect()) {
            List<Option> options = cache.getList(selectNames);
            for (int i = 0, j = options.size(); i < j; i++) {
                Option next = options.get(i);
                Element opt = new Element("option");
                opt.setAttribute("value", next.getId().toString());
                opt.setText(next.getName());
                param.addContent(opt);
            }
        }
        return param;
    }
}
