/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2009 12:26:02 PM 
 * 
 */
package com.osserp.core;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessTypeManager {

    /**
     * Loads business type with given id
     * @return businessType
     */
    BusinessType load(Long id);

    /**
     * Provides type selection for search actions
     * @param user
     * @param requestContext indicates search for request context
     * @return search type selection
     */
    List<BusinessType> createSearchSelection(DomainUser user, boolean requestContext);

    /**
     * Provides a request type selection by user. Used to provide a selection to create new requests
     * @param user
     * @param switchRequest indicates if type selection is required for a requestType change
     * @return create type selection
     */
    List<BusinessType> createTypeSelection(DomainUser user, boolean switchRequest);

    /**
     * Provides a named businessType selection
     * @param name
     * @return selection or null if not exists
     */
    BusinessTypeSelection findTypeSelection(String name);

    /**
     * Provides a named businessType selection
     * @param contextName name of system prooperty to lookup for
     * @return default type or null if not exists
     */
    BusinessType findDefaultType(String contextName);

    /**
     * Provides all activated business types
     * @return businessTypes
     */
    List<BusinessType> findActive();

    /**
     * Provides all available business types
     * @return businessTypes
     */
    List<BusinessType> findAll();

    /**
     * Provides all request context types
     * @return requestContext
     */
    List<BusinessTypeContext> findByRequestContext();

    /**
     * Provides all sales context types
     * @return salesContext
     */
    List<BusinessTypeContext> findBySalesContext();

    /**
     * Provides the next possible id
     * @return idSuggestion
     */
    Long getIdSuggestion();

    /**
     * Updates business type
     * @param type
     * @param name
     * @param key
     * @param context
     * @param salesContext
     * @param active
     * @param requestContextOnly
     * @param directSales
     * @param trading
     * @param packageRequired
     * @param interestContext
     * @param salesPersonByCollector
     * @param supportingDownpayments
     * @param supportingPictures
     * @param supportingOffers
     * @param createSla
     * @param includePriceByDefault
     * @param dummy
     * @param service
     * @param subscription
     * @param wholeSale
     * @param commissionType
     * @param capacityFormat
     * @param capacityUnit
     * @param capacityLabel
     * @param language
     * @param recordByCalculation
     * @param calculationConfig
     * @param requestLetterType
     * @param salesLetterType
     * @param installationDateSupported
     * @param startupSupported
     * @param externalIdProvided
     * @param externalStatusQualifiedRequestKeys
     * @param externalStatusSalesKeys
     * @param externalStatusSalesClosedKeys
     * @param defaultContractType
     * @param defaultOrigin
     * @param defaultOriginType
     * @throws ClientException if validation failed
     */
    void update(
            BusinessType type,
            String name,
            String key,
            String context,
            String salesContext,
            boolean active,
            boolean requestContextOnly,
            boolean directSales,
            boolean trading,
            boolean packageRequired,
            boolean interestContext,
            boolean salesPersonByCollector,
            boolean supportingDownpayments,
            boolean supportingPictures,
            boolean supportingOffers,
            boolean createSla,
            boolean includePriceByDefault,
            boolean dummy,
            boolean service,
            boolean subscription,
            boolean wholeSale,
            String commissionType,
            String capacityFormat,
            String capacityUnit,
            String capacityLabel,
            String language,
            boolean recordByCalculation,
            Long calculationConfig,
            Long requestLetterType,
            Long salesLetterType,
            boolean installationDateSupported,
            boolean startupSupported,
            boolean externalIdProvided,
            String externalStatusQualifiedRequestKeys, 
            String externalStatusSalesKeys,
            String externalStatusSalesClosedKeys,
            Long defaultContractType,
            Long defaultOrigin,
            Long defaultOriginType) throws ClientException;

    /**
     * Creates a new business type
     * @param user
     * @param id
     * @param company
     * @param name
     * @param key
     * @param source
     * @return new created type
     * @throws ClientException if validation failed
     */
    BusinessType create(
            Employee user,
            Long id,
            Long company,
            String name,
            String key,
            BusinessType source) throws ClientException;

    /**
     * Provides available default payment agreements
     * @return default payment agreements
     */
    List<PaymentAgreement> getDefaultPaymentAgreements();

    /**
     * Provides the associated default payment agreement of a business type
     * @param type
     * @return payment agreement
     */
    PaymentAgreement getDefaultPaymentAgreement(BusinessType type);

    /**
     * Updates a default payment agreement values on persistent storage
     * @param user performing update
     * @param paymentAgreement to update
     * @throws ClientException if validation failed
     */
    void updateDefaultPaymentAgreement(Employee user, PaymentAgreement paymentAgreement) throws ClientException;

    /**
     * Provids contract type list
     * @return contract types
     */
    List<ContractType> getContractTypes();

}
