<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="recordView" scope="session"	value="${sessionScope.salesOfferView}" />
<c:set var="recordView" value="${sessionScope.recordView}" />
<c:set var="records" value="${recordView.list}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="styles" type="string">
<style type="text/css">
.table .recordDate {
    padding-left: 2%;
}
</style>
</tiles:put>
<tiles:put name="headline">
    <c:choose>
        <c:when test="${!empty sessionScope.businessCaseView}">
            <c:choose>
                <c:when test="${!empty sessionScope.message}">
                    <o:out value="${sessionScope.businessCaseView.request.requestId}" /> - <o:out value="${sessionScope.businessCaseView.request.name}" />: <o:out value="${sessionScope.message}" />
                    <c:remove var="message" scope="session"/>
                </c:when>
                <c:otherwise>
                    <fmt:message key="offersTo" />
                    <o:out value="${sessionScope.businessCaseView.request.requestId}" /> - <o:out value="${sessionScope.businessCaseView.request.name}" />
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <c:choose>
                <c:when test="${empty sessionScope.message}"><fmt:message key="offers" /></c:when>
                <c:otherwise><o:out value="${sessionScope.message}" /><c:remove var="message" scope="session"/></c:otherwise>
            </c:choose>
        </c:otherwise>
    </c:choose>
</tiles:put>
<tiles:put name="headline_right">
    <a href="<c:url value="/salesOffer.do?method=exitList"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
    <v:link url="/index" title="backToMenu"><o:img name="homeIcon" /></v:link>
</tiles:put>

<tiles:put name="content" type="string">
    <div class="content-area" id="requestQueryIndexContent">
        <div class="row">
            <div class="col-md-12 panel-area">
                <div class="table-responsive table-responsive-default">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="recordNumber"><fmt:message key="offerNumber"/></th>
                                <th class="recordDate"><fmt:message key="createdOn"/></th>
                                <th class="recordCreator"><fmt:message key="by"/></th>
                                <th class="recordAmount"><fmt:message key="orderValue"/></th>
                                <th class="recordDate">
                                    <c:choose>
                                        <c:when test="${sessionScope.businessCaseView.request.closed}">
                                            <fmt:message key="status"/>
                                        </c:when>
                                        <c:otherwise><fmt:message key="validUntil"/></c:otherwise>
                                    </c:choose>
                                </th>
                                <th class="recordName"><fmt:message key="name"/></th>
                                <c:choose>
                                    <c:when test="${!empty recordView.businessTemplates}">
                                        <th class="center" colspan="4"> </th>
                                    </c:when>
                                    <c:otherwise>
                                        <th class="center" colspan="3"> </th>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="record" items="${records}" varStatus="s">
                                <tr>
                                    <td class="recordNumber"><a href="<c:url value="/salesOffer.do?method=select&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                    <td class="recordDate"><o:date value="${record.created}"/></td>
                                    <td class="recordCreator"><oc:employee value="${record.createdBy}" /></td>
                                    <td class="recordAmount">
                                        <o:number value="${record.amounts.amount}" format="currency"/>
                                        <o:out value="${record.currencySymbol}" />
                                    </td>
                                    <td class="recordDate">
                                        <c:choose>
                                            <c:when test="${sessionScope.businessCaseView.request.closed}">
                                                <c:choose>
                                                    <c:when test="${record.canceled}">
                                                        <fmt:message key="canceled"/>
                                                    </c:when>
                                                    <c:when test="${record.closed}">
                                                        <fmt:message key="order"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:message key="discarded"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise><o:date value="${record.validUntil}"/></c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="recordName"><o:out value="${record.calculationInfo}"/></td>
                                    <td class="icon center">
                                        <o:permission role="executive,executive_sales,sales_revenue" info="permissionPostCalculation">
                                            <v:ajaxLink linkId="salesRevenueView" url="/sales/salesRevenue/forward?id=${record.id}"><o:img name="moneyIcon"/></v:ajaxLink>
                                        </o:permission>
                                    </td>
                                    <c:if test="${!empty recordView.businessTemplates}">
                                        <td class="icon center">
                                            <v:ajaxLink linkId="businessTemplateDocumentView" url="/requests/requestTemplateDocument/forward?record=${record.id}"><o:img name="letterIcon"/></v:ajaxLink>
                                        </td>
                                    </c:if>
                                    <td class="icon center">
                                        <v:link url="/records/recordPrint/print?name=salesOffer&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                    </td>
                                    <td class="icon center">
                                        <o:permission role="executive,executive_sales,create_offer_copy" info="createOfferCopy">
                                            <c:if test="${!recordView.unreleasedOfferAvailable && record.unchangeable && !sessionScope.businessCaseView.request.closed && !sessionScope.businessCaseView.flowControlMode}">
                                                <c:choose>
                                                    <c:when test="${sessionScope.businessCaseView.businessCase.type.recordByCalculation}">
                                                        <a href="<c:url value="/salesOffer.do?method=selectForCopy&id=${record.id}"/>"><o:img name="copyIcon"/></a>
                                                    </c:when>
                                                    <c:when test="${recordView.createCopyAvailable}">
                                                        <a href="<c:url value="/salesOffer.do?method=createCopy&id=${record.id}"/>" title="<fmt:message key="createByCopy"/>"><o:img name="copyIcon"/></a>
                                                    </c:when>
                                                </c:choose>
                                            </c:if>
                                        </o:permission>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</div>
</tiles:put>
</tiles:insert>
