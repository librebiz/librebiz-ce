/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 25, 2006 9:08:47 AM 
 * 
 */
package com.osserp.gui.web.purchasing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.ContextUtil;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;

import com.osserp.core.finance.Invoice;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductManager;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.system.SystemCompany;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.products.ProductView;
import com.osserp.gui.client.purchasing.PurchaseInvoiceView;
import com.osserp.gui.client.purchasing.PurchaseOrderView;
import com.osserp.gui.client.records.OrderView;
import com.osserp.gui.web.records.AbstractOrderAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseOrderAction extends AbstractOrderAction {
    private static Logger log = LoggerFactory.getLogger(PurchaseOrderAction.class.getName());

    @Override
    protected Class<PurchaseOrderView> getViewClass() {
        return PurchaseOrderView.class;
    }

    /**
     * Exists purchase orders and returns to 'exit' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward exitList(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        return super.exit(mapping, form, request, response);
    }

    /**
     * Creates a copy of current order and forwards to display new created order
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward createCopy(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createCopy() request from "
                    + getUser(session).getId());
        }
        PurchaseOrderView view = getPurchaseOrderView(session);
        view.createCopy();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to purchase open purchase orders by product
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward forwardOpenByProduct(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardOpenByProduct() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        Product product = null;
        ProductView productView = (ProductView) fetchView(session, ProductView.class);

        if (productView != null && productView.getBean() != null) {
            Product current = productView.getProduct();
            if (log.isDebugEnabled()) {
                log.debug("forwardOpenByProduct() found product view "
                        + "with current product " + current.getProductId());
            }
            if (current.getProductId().equals(id)) {
                product = current;
                if (log.isDebugEnabled()) {
                    log.debug("forwardOpenByProduct() selected is current...");
                }
            }
        }
        if (product == null) {
            if (log.isDebugEnabled()) {
                log.debug("forwardOpenByProduct() no productView " +
                        "with product found, trying to load " + id);
            }
            ProductManager manager = (ProductManager) ContextUtil.getService(request, (ProductManager.class.getName()));
            product = manager.get(id);
        }
        String target = RequestUtil.getExit(request);
        Long selectedStock = RequestUtil.fetchLong(request, "stockId");
        PurchaseOrderView view = (PurchaseOrderView) createView(request);
        view.initDisplay(product, selectedStock, target);
        return mapping.findForward(view.getListTarget());
    }

    /**
     * Forwards to create new purchase order
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward forwardCreate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardCreate() request from " + getUser(session).getId());
        }
        PurchaseOrderView view = (PurchaseOrderView) createView(request);
        Long id = RequestUtil.getId(request);
        view.selectSupplier(id);
        view.setCreateMode(true);
        String exitTarget = RequestUtil.getExit(request);
        if (exitTarget != null) {
            view.setExitTarget(exitTarget);
        }
        if (view.getClients().size() == 1) {
            SystemCompany company = view.getClients().get(0);
            return create(mapping, view, company.getId());
        }
        return mapping.findForward(Actions.COMPANY_SELECTION);
    }

    /**
     * Sets the company selection during create workflow
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward setCompany(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("setCompany() request from " + getUser(session).getId());
        }
        Long company = RequestUtil.getId(request);
        PurchaseOrderView view = getPurchaseOrderView(session);
        return create(mapping, view, company);
    }

    /**
     * Sets the company selection during create workflow
     * @param mapping
     * @param view
     * @param company
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    private ActionForward create(
            ActionMapping mapping,
            PurchaseOrderView view,
            Long company)
            throws Exception {

        view.setCompany(company);
        view.create();
        view.setCreateMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes a purchase order
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("delete() request from " + getUser(session).getId());
        }
        PurchaseOrderView view = getPurchaseOrderView(session);
        try {
            view.delete();
            if (view.isExitTargetAvailable()) {
                return mapping.findForward(view.getExitTarget());
            }
            return mapping.findForward(view.getListTarget());
        } catch (ClientException c) {
            saveError(request, c.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward setDeliveryAddressByBusinessCase(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        PurchaseOrderView view = getPurchaseOrderView(session);
        try {
            view.setDeliveryAddressByBusinessCase();
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        if (view.isEditMode()) {
            return mapping.findForward(Actions.EDIT);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward resetDeliveryAddress(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        PurchaseOrderView view = getPurchaseOrderView(session);
        view.resetDeliveryAddress();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Creates a new invoice based on current order
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward createInvoice(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createInvoice() request from " + getUser(session).getId());
        }
        PurchaseOrderView view = getPurchaseOrderView(session);
        try {
            view.checkDirectInvoiceBooking();
            PurchaseInvoiceView invoiceView = createPurchaseInvoiceView(request);
            invoiceView.create((PurchaseOrder) view.getOrder());
            invoiceView.setExitTarget(Actions.ORDER);

        } catch (ClientException e) {
            saveError(request, e.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
        return mapping.findForward(Actions.INVOICE);
    }

    /**
     * Creates a new invoice based on current order
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward forwardInvoice(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardInvoice() request from " + getUser(session).getId());
        }
        PurchaseOrderView view = getPurchaseOrderView(session);
        Invoice invoice = ((PurchaseOrder) view.getOrder()).getInvoices().get(0);
        PurchaseInvoiceView invoiceView = createPurchaseInvoiceView(request);
        invoiceView.initDisplay(invoice, "order");
        return mapping.findForward(Actions.INVOICE);
    }

    /**
     * Reloads current purchase order from backend. Method should be used after create or deleting invoices
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward refresh(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("refresh() request from " + getUser(session).getId());
        }
        // TODO 
        PurchaseOrderView view = getPurchaseOrderView(session);
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to create new purchase order
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    @Override
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        ActionForward forward = super.forward(mapping, form, request, response);
        PurchaseOrderView view = (PurchaseOrderView) fetchView(request.getSession());
        if (view == null || view.getBean() == null) {
            return mapping.findForward("purchaseOrderQueryForward");
        }
        return forward;
    }

    /**
     * Reloads current purchase order from backend. Method should be used after create or deleting invoices
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward deleteItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("deleteItem() request from " + getUser(session).getId());
        }
        PurchaseOrderView view = getPurchaseOrderView(session);
        try {
            view.deleteItem(RequestUtil.getId(request));
            view.refresh();
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Restores items from backup
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception if any unexpected exception occurs
     */
    public ActionForward restoreItems(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("restoreItems() request from " + getUser(session).getId());
        }
        PurchaseOrderView view = getPurchaseOrderView(session);
        view.restoreItems();
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    public ActionForward select(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ActionForward forward = super.select(mapping, form, request, response);
        PurchaseOrderView view = getPurchaseOrderView(request.getSession());
        if (!view.isDisplayDescription()) {
            view.changeDisplayDescriptionState();
        }
        return forward;
    }

    public ActionForward enableDeliveryConditionEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        PurchaseOrderView view = getPurchaseOrderView(request.getSession());
        view.setDeliveryConditionEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward disableDeliveryConditionEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        PurchaseOrderView view = getPurchaseOrderView(request.getSession());
        view.setDeliveryConditionEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    public ActionForward updateDeliveryCondition(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        PurchaseOrderView view = getPurchaseOrderView(request.getSession());
        try {
            view.updateDeliveryCondition(new StrutsForm(form));
            view.setDeliveryConditionEditMode(false);
        } catch (Exception e) {
            view.setErrorMessage(e.toString());
        }
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    protected OrderView createVersionView(HttpServletRequest request) {
        try {
            return getPurchaseOrderView(request.getSession());
        } catch (PermissionException e) {
            throw new ActionException(e);
        }
    }

    private PurchaseOrderView getPurchaseOrderView(HttpSession session) throws PermissionException {
        PurchaseOrderView view = fetchPurchaseOrderView(session);
        if (view == null) {
            throw new ActionException(ActionException.NO_VIEW_BOUND, PurchaseOrderView.class.getName());
        }
        return view;
    }

    private PurchaseOrderView fetchPurchaseOrderView(HttpSession session) throws PermissionException {
        return (PurchaseOrderView) fetchView(session);
    }

    private PurchaseInvoiceView createPurchaseInvoiceView(HttpServletRequest request) throws ClientException, PermissionException {
        return (PurchaseInvoiceView) createView(request, PurchaseInvoiceView.class.getName());
    }
}
