<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="userPermissionsView"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="permissionLabel"/> - <o:out value="${view.permission}"/>
	</div>
	<div class="modalBoxHeaderRight">
		<div class="boxnav">
		</div>
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<table class="table" style="width: 350px; text-align:left;">
				<c:choose>
					<c:when test="${empty view.list}">
						<tr>
							<th><fmt:message key="info"/></th>
						</tr>
						<tr>
							<td><fmt:message key="permissionNotGrant"/></td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th style="text-align: center;">UID</th>
							<th class="small"><fmt:message key="user"/></th>
						</tr>
						<c:forEach var="obj" items="${view.list}">
							<tr>
								<td style="text-align: center;"><o:out value="${obj.id}"/></td>
								<td><o:out value="${obj.name}"/></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</table>
			<div class="spacer"></div>
		</div>
	</div>
</div>
