<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesOrderListingView}">
    <c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="view" value="${sessionScope.salesOrderListingView}"/>
<c:set var="recordListingView" scope="request" value="${view}"/>
<c:set var="data" value="${view.data}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="headline">
        <c:choose>
            <c:when test="${!empty data['header']}">
                <o:out value="${data['header']}"/>
            </c:when>
            <c:otherwise>
                <fmt:message key="orders"/>
            </c:otherwise>
        </c:choose>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="${view.actionUrl}?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="orderListContent">
            <c:import url="/pages/records/record_listing_by_map.jsp"/>
        </div>
    </tiles:put>
</tiles:insert>
