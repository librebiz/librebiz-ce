<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="statusHistory" value="${sessionScope.recordStatusHistory}"/>
<table class="table table-striped">
    <tr>
        <th><fmt:message key="date"/></th>
        <th><fmt:message key="name"/></th>
        <th><fmt:message key="action"/></th>
    </tr>
    <c:forEach var="stat" items="${statusHistory}">
        <tr>
            <td><o:date value="${stat.created}"/></td>
            <td><o:out value="${stat.description}"/></td>
            <td><o:out value="${stat.name}"/></td>
        </tr>
    </c:forEach>
</table>
