/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Dec 7, 2012 9:24:12 AM
 *
 */
package com.osserp.core.dao.records.impl

import com.osserp.core.employees.Employee
import com.osserp.core.finance.RecordNumberCreator
import com.osserp.core.finance.RecordType

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RecordNumberCreatorImpl implements RecordNumberCreator {

    RecordNumberCreator creatorByDate
    RecordNumberCreator creatorBySequence
    RecordNumberCreator creatorByYearlySequence

    Long create(Employee user, Long company, RecordType type, Date created) {
        getCreator(type)?.create(user, company, type, created)
    }

    protected RecordNumberCreator getCreator(RecordType type) {
        if ('yearlySequence' == type.numberCreatorName && creatorByYearlySequence) {
            return creatorByYearlySequence
        }
        if ('sequence' == type.numberCreatorName && creatorBySequence) {
            return creatorBySequence
        }
        creatorByDate
    }
}
