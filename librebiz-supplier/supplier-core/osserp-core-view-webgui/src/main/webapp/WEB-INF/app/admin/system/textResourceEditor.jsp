<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="textResourceEditorView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:link url="/admin" title="backToMenu">
            <o:img name="homeIcon" />
        </v:link>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="textResourceEditorContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped" style="table-layout: fixed;">
                            <tbody>
                            <c:choose>
                                <c:when test="${!empty view.bean}">
                                    <v:form url="/admin/system/textResourceEditor/save">
                                        <c:forEach var="prop" items="${view.list}">
                                            <c:if test="${prop.resourceKey == view.bean.resourceKey}">
                                                <tr>
                                                    <td style="width: 80%">
                                                        <v:text name="name" styleClass="form-control" />
                                                        <input type="hidden" name="resourceKey" value="${prop.resourceKey}" />
                                                    </td>
                                                    <td><v:submitExit url="/admin/system/textResourceEditor/select" /></td>
                                                    <td><o:submit value="save" styleClass="form-control" /></td>
                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                    </v:form>
                                </c:when>
                                <c:otherwise>
                                    <c:if test="${!empty view.list}">
                                        <c:forEach var="prop" items="${view.list}">
                                            <tr>
                                                <td>
                                                    <v:link url="/admin/system/textResourceEditor/select?resourceKey=${prop.resourceKey}">
                                                        <o:out value="${prop.name}" />
                                                    </v:link>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
