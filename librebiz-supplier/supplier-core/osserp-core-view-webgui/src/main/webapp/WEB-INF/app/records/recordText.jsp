<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="recordTextView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <o:out value="${view.selectedType.name}" />
        <c:if test="${!empty view.viewReference.record}">
            <o:out value="${view.viewReference.record.number}" /> - <o:out value="${view.viewReference.record.contact.name}" />
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="recordTextContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.createMode}">
                        <c:import url="${viewdir}/records/_recordTextCreate.jsp" />
                    </c:when>
                    <c:when test="${view.editMode}">
                        <c:import url="${viewdir}/records/_recordTextEdit.jsp" />
                    </c:when>
                    <c:when test="${view.bean != null}">
                        <c:import url="${viewdir}/letters/_letterDisplay.jsp" />
                    </c:when>
                    <c:otherwise>
                        <c:import url="${viewdir}/letters/_letterList.jsp" />
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
