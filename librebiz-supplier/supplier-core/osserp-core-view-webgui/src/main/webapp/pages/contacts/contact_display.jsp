<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.contactView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${contactView.bean}" />
<c:set var="actionUrl" scope="request" value="/contacts.do" />
<c:set var="editorContactView" scope="request" value="${sessionScope.contactView}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">.city { width: 143px; }</style>
    </tiles:put>

    <tiles:put name="headline">
        <c:choose>
            <c:when test="${contact.grantContactNone}">
                <span class="errormessage"><fmt:message key="contactForbidden" /></span>
            </c:when>
            <c:when test="${contact.client}">
                <fmt:message key="client" />
            </c:when>
            <c:otherwise>
                <fmt:message key="contactData" />
            </c:otherwise>
        </c:choose>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <c:if test="${!contactView.assignMode}">
                <li><a href="<c:url value="/contacts.do?method=exitDisplay"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                <li><a href="<v:url value="/contacts/contactDocuments/forward?exit=/contactDisplay.do"/>" title="<fmt:message key="documents"/>"><o:img name="docIcon" /></a></li>
                <c:if test="${!contact.client}">
                    <o:permission role="executive,office_management,customer_service" info="permissionContactFurtherSettings">
                        <li><a href="<c:url value="/contacts.do?method=forwardSettings"/>" title="<fmt:message key="furtherSettings"/>"><o:img name="configureIcon" /></a></li>
                    </o:permission>
                    <c:if test="${!contact.branchOffice}">
                        <o:permission role="sync_contacts" info="permissionSyncAllPrivateContactsAgain">
                            <li><o:ajaxLink linkId="privateContactDisplay" url="/privateContacts.do?method=syncAllPrivateContacts" title="startSynchronization">
                                    <o:img name="replaceIcon" />
                                </o:ajaxLink></li>
                        </o:permission>
                    </c:if>
                </c:if>
                <c:if test="${contactView.privateContactCreateable}">
                    <c:choose>
                        <c:when test="${contactView.privateContactOfCurrentEmployee}">
                            <li><o:ajaxLink linkId="privateContactDisplay" url="${'/privateContacts.do?method=load&id='}${contact.contactId}" title="displayMyContactDetailsLabel">
                                    <o:img name="userAddIcon" />
                                </o:ajaxLink></li>
                            <o:permission role="secretary" info="permissionSecretary">
                                <li><o:ajaxLink linkId="privateContactDisplay" url="/privateContacts.do?method=enableCreate" title="assignContact">
                                        <o:img name="enabledIcon" />
                                    </o:ajaxLink></li>
                            </o:permission>
                        </c:when>
                        <c:otherwise>
                            <o:permission role="secretary" info="permissionSecretary">
                                <li><o:ajaxLink linkId="privateContactDisplay" url="/privateContacts.do?method=enableCreate" title="assignContact">
                                        <o:img name="enabledIcon" />
                                    </o:ajaxLink></li>
                            </o:permission>
                            <o:forbidden role="secretary">
                                <li><a href="<c:url value="/contacts.do?method=enablePrivateContact"/>" title="<fmt:message key="addContactToMyContacts"/>"><o:img name="enabledIcon" /></a></li>
                            </o:forbidden>
                        </c:otherwise>
                    </c:choose>
                </c:if>
                <o:permission role="notes_contacts_deny" info="permissionNotes">
                    <li><v:link url="/notes/note/forward?id=${contact.contactId}&name=contactNote&exit=/contactDisplay.do" title="displayAndWriteNotes">
                            <o:img name="texteditIcon" />
                        </v:link></li>
                </o:permission>
                <c:if test="${!contact.client and !contact.branchOffice and contact.business and !contact.freelance}">
                    <li><v:link url="/contacts/contactPersons/forward?id=${contact.contactId}&exit=/contactDisplay.do" title="contactPerson">
                            <o:img name="peopleIcon" />
                        </v:link></li>
                </c:if>
                <li><v:link url="/contacts/contactEditor/forward?id=${contact.contactId}&exit=/contactDisplay.do" title="contactEdit">
                        <o:img name="writeIcon" />
                    </v:link></li>
                <li><v:link url="/index" title="backToMenu">
                        <o:img name="homeIcon" />
                    </v:link></li>
            </c:if>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="subcolumns">
            <div class="column50l">
                <div class="subcolumnl">
                    <div class="spacer"></div>
                    <c:import url="/pages/contacts/shared/contact_data_header.jsp" />
                    <div class="spacer"></div>

                    <div class="contentBox">
                        <div class="contentBoxData">
                            <div class="subcolumns">
                                <div class="subcolumn">
                                    <div class="smallSpacer"></div>
                                    <c:import url="/pages/contacts/shared/contact_data_main.jsp" />
                                    <c:import url="/pages/contacts/shared/contact_groups.jsp" />
                                </div>
                            </div>
                            <div class="smallSpacer"></div>
                        </div>
                    </div>
                    <div class="spacer"></div>
                </div>
            </div>

            <c:if test="${!contactView.assignMode}">
                <div class="column50r">
                    <div class="subcolumnr">
                        <c:if test="${!contact.grantContactNone}">
                            <div class="spacer"></div>
                            <c:set var="communicationView" scope="request" value="${contactView}" />
                            <c:import url="/pages/contacts/shared/contact_phones.jsp" />
                            <div class="spacer"></div>
                            <c:import url="/pages/contacts/shared/contact_fax.jsp" />
                            <div class="spacer"></div>
                            <c:choose>
                                <c:when test="${!contact.business or contact.freelance}">
                                    <c:import url="/pages/contacts/shared/contact_mobiles.jsp" />
                                    <div class="spacer"></div>
                                </c:when>
                                <c:otherwise>
                                    <oc:systemPropertyEnabled name="contactB2BMobilePhoneEnabled">
                                        <c:import url="/pages/contacts/shared/contact_mobiles.jsp" />
                                        <div class="spacer"></div>
                                    </oc:systemPropertyEnabled>
                                    <oc:systemPropertyDisabled name="contactB2BMobilePhoneEnabled">
                                        <c:if test="${!empty contact.mobiles || contactView.contactPersonWithMobilePhoneExisting}">
                                            <c:import url="/pages/contacts/shared/contact_mobiles.jsp" />
                                            <div class="spacer"></div>
                                        </c:if>
                                    </oc:systemPropertyDisabled>
                                </c:otherwise>
                            </c:choose>
                            <c:import url="/pages/contacts/shared/contact_emails.jsp" />
                        </c:if>
                        <c:if test="${!empty contactView.stickyNote}">
                            <div class="spacer"></div>
                            <c:import url="/pages/contacts/shared/contact_note.jsp" />
                        </c:if>
                    </div>
                </div>
            </c:if>
        </div>
        <c:if test="${!contactView.assignMode and !contact.branchOffice}">
            <div class="spacer"></div>

            <c:if test="${contactView.customerView}">
                <c:import url="/pages/customers/customer_details.jsp" />
            </c:if>
            <c:if test="${contactView.employeeView}">
                <c:import url="/pages/employees/employee_details.jsp" />
            </c:if>
            <c:if test="${contactView.supplierView}">
                <c:import url="/pages/suppliers/supplier_details.jsp" />
            </c:if>
        </c:if>
    </tiles:put>
</tiles:insert>
