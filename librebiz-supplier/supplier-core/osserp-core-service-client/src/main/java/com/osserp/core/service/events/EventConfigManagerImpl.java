/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 4, 2007 11:17:20 AM 
 * 
 */
package com.osserp.core.service.events;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.core.dao.EventConfigs;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventConfigManager;
import com.osserp.core.events.EventConfigType;
import com.osserp.core.events.EventTermination;
import com.osserp.core.events.RecipientPool;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.views.EventDisplayView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventConfigManagerImpl extends AbstractService implements EventConfigManager {
    private static Logger log = LoggerFactory.getLogger(EventConfigManagerImpl.class.getName());
    private EventConfigs eventConfigs = null;

    /**
     * Creates a new event config manager
     * @param eventConfigs
     */
    public EventConfigManagerImpl(EventConfigs eventConfigs) {
        super();
        this.eventConfigs = eventConfigs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findActionsByCaller(java.lang.Long)
     */
    public List<EventAction> findActionsByCaller(EventConfigType eventConfigType, Long callerId) {
        if (eventConfigType == null) {
            throw new IllegalArgumentException("eventConfigType must no be null");
        }
        return eventConfigs.findActionsByCaller(eventConfigType.getId(), callerId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findActionsByEventType(com.osserp.core.events.EventConfigType)
     */
    public List<EventAction> findActionsByEventType(EventConfigType eventConfigType) {
        return eventConfigs.findActionsByEventType(eventConfigType.getId());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findActionsByRequestType(java.lang.Long)
     */
    public List<EventAction> findFlowControlActions(EventConfigType configType, Long requestType) {
        return eventConfigs.findFlowControlActions(configType, requestType);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findAction(java.lang.Long)
     */
    public EventAction findAction(Long actionId) {
        return eventConfigs.loadAction(actionId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findTermination(java.lang.Long)
     */
    public EventTermination findTermination(Long terminationId) throws ClientException {
        return eventConfigs.loadTermination(terminationId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findPool(java.lang.Long)
     */
    public RecipientPool findPool(Long poolId) throws ClientException {
        return eventConfigs.loadPool(poolId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findPools()
     */
    public List<RecipientPool> findPools() {
        return eventConfigs.findPools();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findTypes()
     */
    public List<EventConfigType> findTypes() {
        return eventConfigs.findTypes();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findType(java.lang.Long)
     */
    public EventConfigType findType(Long typeId) {
        return eventConfigs.loadType(typeId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#synchronizeFcs()
     */
    public void synchronizeFcs() {
        eventConfigs.synchronizeFcsActions();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#createAction(java.lang.Long, com.osserp.core.events.EventConfigType, java.lang.String, java.lang.Long)
     */
    public EventAction createAction(
            Long requestType,
            EventConfigType type,
            String name,
            Long caller) {
        return eventConfigs.createAction(requestType, type, name, caller);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#setAlert(com.osserp.core.events.EventAction, com.osserp.core.events.RecipientPool)
     */
    public void setAlert(EventAction action, RecipientPool pool) {
        eventConfigs.setAlert(action, pool);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#createPool(java.lang.String)
     */
    public RecipientPool createPool(String name) throws ClientException {
        List<RecipientPool> pools = findPools();
        for (int i = 0, j = pools.size(); i < j; i++) {
            RecipientPool pool = pools.get(i);
            if (pool.getName().equals(name)) {
                log.warn("createPool() found existing pool under name " + name);
                throw new ClientException(ErrorCode.VALUES_DUPLICATE);
            }
        }
        return eventConfigs.createPool(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#save(com.osserp.core.events.EventAction)
     */
    public void save(EventAction action) {
        eventConfigs.save(action);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#delete(com.osserp.core.events.EventAction)
     */
    public void delete(EventAction action) throws ClientException {
        eventConfigs.delete(action);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#save(com.osserp.core.events.EventTermination)
     */
    public void save(EventTermination termination) {
        eventConfigs.save(termination);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#delete(com.osserp.core.events.EventTermination)
     */
    public void delete(EventTermination termination) {
        eventConfigs.delete(termination);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#save(com.osserp.core.events.RecipientPool)
     */
    public void save(RecipientPool pool) {
        eventConfigs.save(pool);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#findStartActionView(java.lang.Long)
     */
    public List<EventDisplayView> findStartActionView(EventConfigType configType, Long requestType) {
        return eventConfigs.findStartActionView(configType, requestType);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.events.EventConfigManager#close(com.osserp.core.events.EventAction)
     */
    public void close(EventAction action) {
        eventConfigs.closeEvents(action);
    }

}
