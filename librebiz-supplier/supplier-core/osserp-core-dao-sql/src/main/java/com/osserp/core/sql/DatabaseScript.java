package com.osserp.core.sql;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.core.io.Resource;

public class DatabaseScript {

    private Long id;
    String version;
    private Resource resource;

    public DatabaseScript(Long id, Resource resource) {
        super();
        this.id = id;
        this.resource = resource;
    }

    public DatabaseScript(String version, Resource resource) {
        super();
        this.id = 1l;
        this.resource = resource;
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public Resource getResource() {
        return resource;
    }

    public String getVersion() {
        return version;
    }

    public static List<DatabaseScript> sort(List<DatabaseScript> list) {
        Collections.sort(list, getComparator());
        return list;
    }

    public static Comparator<DatabaseScript> getComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((DatabaseScript) a).getId().compareTo(((DatabaseScript) b).getId());
            }
        };
    }

    public static int compare(String nextVersion, String currentVersion) {
        String[] next = nextVersion.split("\\.");
        String[] current = currentVersion.split("\\.");
        if (next.length < current.length) {
            return -1;
        }
        if (next.length > current.length) {
            return 1;
        }
        for (int i = 0; i < next.length; i++) {
            if(Integer.parseInt(next[i]) < Integer.parseInt(current[i])) {
                return -1;
            }
            if(Integer.parseInt(next[i]) > Integer.parseInt(current[i])) {
                return 1;
            }
        }
        return 0;
    }
}
