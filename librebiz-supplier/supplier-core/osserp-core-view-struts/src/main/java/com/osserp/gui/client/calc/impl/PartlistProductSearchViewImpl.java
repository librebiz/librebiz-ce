/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 07, 2010 at 14:11:15 AM 
 * 
 */
package com.osserp.gui.client.calc.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.CalculationConfigManager;
import com.osserp.core.calc.Partlist;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSearch;
import com.osserp.core.products.ProductSearchRequest;

import com.osserp.gui.client.calc.PartlistProductSearchView;
import com.osserp.gui.client.products.impl.AbstractProductSearchView;

/**
 * @author jg <jg@osserp.com>
 * 
 */
@ViewName("partlistProductSearchView")
public class PartlistProductSearchViewImpl extends AbstractProductSearchView implements PartlistProductSearchView {
    private static Logger log = LoggerFactory.getLogger(PartlistProductSearchViewImpl.class.getName());

    private ItemPosition selectedPartlistPosition = null;
    private Item selectedPartlistItem = null;
    private Partlist partlist = null;
    private CalculationConfigGroup selectedPartlistGroup = null;
    private Set<Long> redunancyIgnorables = new HashSet();

    protected PartlistProductSearchViewImpl() {
        super();
    }

    protected PartlistProductSearchViewImpl(Set<String> methods, String defaultMethod) {
        super(methods, defaultMethod, true);
        setKeepViewWhenExistsOnCreate(false);
        setIncludeEolAvailable(false);
    }

    public ItemPosition getSelectedPartlistPosition() {
        return selectedPartlistPosition;
    }

    public Item getSelectedPartlistItem() {
        return selectedPartlistItem;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        setProductSelectionConfigsAvailable(false);

        this.partlist = null;
        Object obj = request.getSession().getAttribute("partlist");
        if (obj != null && obj instanceof Partlist) {
            this.partlist = (Partlist) obj;
        }
        Long positionId = RequestUtil.getLong(request, "positionId");
        if (partlist != null) {
            this.selectedPartlistPosition = partlist.getPosition(positionId);
            Long itemId = RequestUtil.fetchLong(request, "itemId");
            if (itemId != null) {
                this.selectedPartlistItem = selectedPartlistPosition.getItem(itemId);
                if (selectedPartlistItem == null) {
                    log.warn("init() itemId provided but no item found!");
                }
            }
            this.redunancyIgnorables = fetchIgnorables();
            CalculationConfigManager manager = getCalculationConfigManager();
            this.selectedPartlistGroup = manager.findGroup(selectedPartlistPosition);
            boolean ignorePreselection = RequestUtil.getBoolean(request, "ignorePreselection");
            if (log.isDebugEnabled()) {
                log.debug("init() initialized search [partlist="
                        + partlist.getId()
                        + ", position=" + selectedPartlistPosition.getId()
                        + ", ignorePreselection=" + ignorePreselection
                        + ", calculationGroup="
                        + (selectedPartlistGroup == null ? "null" : selectedPartlistGroup.getId())
                        + "]");
            }

            if (selectedPartlistGroup != null
                    && !ignorePreselection
                    && !this.selectedPartlistGroup.getProductSelections().isEmpty()) {

                setProductSelectionConfigs(selectedPartlistGroup.getProductSelections());
                setProductSelectionConfigsAvailable(true);

                ProductSearch search = (ProductSearch) getService(ProductSearch.class.getName());
                List<Product> list = search.find(
                        new ProductSearchRequest(
                                getContactReference(), 
                                isContactReferenceCustomer(), 
                                isIncludeEol(),
                                ProductSearchRequest.IGNORE_FETCHSIZE),
                        getProductSelectionConfigs());
                setList(list);
                if (!list.isEmpty()) {
                    setProductSelectionConfigsMode(true);
                }
                if (log.isDebugEnabled()) {
                    log.debug("init() done by product selection "
                            + "[partlist=" + selectedPartlistPosition.getReference()
                            + ", position=" + positionId
                            + ", calculationGroup=" + selectedPartlistGroup.getId()
                            + ", productSelectionConfigsMode=" + isProductSelectionConfigsMode()
                            + ", productCount=" + list.size()
                            + "]");
                }
            } else if (log.isDebugEnabled()) {
                StringBuilder buffer = new StringBuilder("init() preselection disabled: ");
                if (selectedPartlistGroup == null) {
                    buffer.append("selected product group not available");
                } else if (selectedPartlistGroup.getProductSelections().isEmpty()) {
                    buffer.append("product selections empty");
                }
                log.debug(buffer.toString());
            }

        } else {
            log.warn("init() partlist not bound!");
        }
    }

    @Override
    protected void setList(List list) {
        for (Iterator<Product> i = list.iterator(); i.hasNext();) {
            Product next = i.next();
            if (partlist.isAlreadyAdded(next)
                    && !this.redunancyIgnorables.contains(next.getProductId())) {

                if (log.isDebugEnabled()) {
                    log.debug("setList() removed already added product [id="
                            + next.getProductId() + "]");
                }
                i.remove();

            }
        }
        super.setList(list);
    }

    private Set<Long> fetchIgnorables() {
        Set<Long> igns = new java.util.HashSet();
        try {
            CalculationConfigManager manager = getCalculationConfigManager();
            CalculationConfig cfg = manager.getConfig(partlist.getConfigId());
            for (int i = 0, j = cfg.getAllowedRedundancies().size(); i < j; i++) {
                Product next = cfg.getAllowedRedundancies().get(i);
                igns.add(next.getProductId());
            }
        } catch (Exception e) {
            log.warn("fetchIgnorables() ignoring exception on attempt to get products [message="
                    + e.getMessage() + "]");
        }
        if (log.isDebugEnabled() && !igns.isEmpty()) {
            log.debug("fetchIgnorables() added redundancy check ignorables [count=" + igns.size() + "]");
        }
        return igns;
    }

    private CalculationConfigManager getCalculationConfigManager() {
        return (CalculationConfigManager) getService(CalculationConfigManager.class.getName());
    }
}
