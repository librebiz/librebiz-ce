<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesCancellationView or empty sessionScope.salesCancellationView.bean}">
<o:logger write="invoice_cancellation_display.jsp no view found" level="warn"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesCancellationView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="baseUrl" scope="request" value="/salesCancellation.do"/>
<c:set var="infoUrl" scope="request" value="/salesCancellationInfos.do"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records.css"/>
</style>
</tiles:put>

<tiles:put name="headline">
    <c:import url="/pages/sales/records/voucher_header.jsp"/>
</tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/salesCancellation.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<c:if test="${!record.unchangeable and !empty record.items}">
<v:link url="/records/recordPrint/release?view=salesCancellationView" confirm="true" message="confirmRecordRelease" title="recordRelease"><o:img name="enabledIcon" /></v:link>
</c:if>
<v:link url="/records/recordPrint/print?view=salesCancellationView" title="documentPrint"><o:img name="printIcon" /></v:link>
<c:choose>
<c:when test="${!record.unchangeable}">
<a href="<c:url value="/salesCancellation.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon"/></a>
<c:if test="${recordView.recordDeletable}">
<a href="<c:url value="/salesCancellation.do?method=delete"/>" title="<fmt:message key="delete"/>"><o:img name="deleteIcon" /></a>
</c:if>
</c:when>
<c:otherwise>
<v:link url="/records/recordMail/forward?view=salesCancellationView&exit=/salesCancellation.do?method=reload" title="sendViaEmail"><o:img name="mailIcon" /></v:link>
</c:otherwise>
</c:choose>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                    
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
                    <div class="column66l">
                        <c:import url="/pages/records/record_sigs_display.jsp"/>
                        <c:import url="/pages/records/record_taxfree_display.jsp"/>
                        <c:if test="${!record.type.displayNotesBelowItems}">
                            <c:import url="/pages/records/record_note_display.jsp"/>
                        </c:if>
                        <br />
                    </div>
                    <div class="column33r">
                        <c:import url="/pages/records/record_status.jsp"/>
                    </div>
                    <c:import url="/pages/records/record_summary.jsp"/>
                </div>
            </div>
        </div>
 
        <c:import url="/pages/records/record_items.jsp"/>
        <c:if test="${record.type.displayNotesBelowItems}">
            <c:import url="/pages/records/record_note_display.jsp"/>
        </c:if>
        <c:import url="/pages/records/record_conditions.jsp"/>
    </div>
</div>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
