/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 10, 2014 
 * 
 */
package com.osserp.gui.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.contacts.ClassifiedContact
import com.osserp.core.contacts.RelatedContactSearch

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractRecordCreatorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractRecordCreatorView.class.getName())

    ClassifiedContact selectedContact

    /**
     * Creates a new instance of view. This constructor initializes the
     * branchOffice headquarter list. 
     * Automatically assigns selectedBranch if only one headquarter exists.
     */
    AbstractRecordCreatorView() {
        super()
    }
    
    /**
     * Assigns the selectedContact property if form['recipient'] provided. 
     * @param atts
     * @param headers
     * @param params
     */
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            enableHeadquarterSelection()
            selectContact('recipient')
        }
    }

    protected void selectContact(String paramName) {
        Long id = (paramName == null) ? form.getLong('id') : form.getLong(paramName)
        if (id) {
            selectedContact = relatedContactSearch.find(id)
            if (selectedContact) {
                logger.debug("selectContact: done [id=${selectedContact.id}]")
            }
        }
    }
    
    protected abstract RelatedContactSearch getRelatedContactSearch();
}
