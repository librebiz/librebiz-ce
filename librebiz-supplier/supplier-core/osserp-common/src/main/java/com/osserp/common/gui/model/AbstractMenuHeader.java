/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 18, 2007 8:42:18 PM 
 * 
 */
package com.osserp.common.gui.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.osserp.common.gui.MenuLink;
import com.osserp.common.gui.MenuHeader;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractMenuHeader extends AbstractMenuItem
        implements MenuHeader {

    private List<MenuLink> links = new ArrayList<MenuLink>();
    private boolean activated = false;

    protected AbstractMenuHeader() {
        super();
    }

    protected AbstractMenuHeader(int orderId, String name) {
        super(name, orderId, null);
    }

    protected AbstractMenuHeader(MenuHeader vo) {
        super(vo);
        activated = vo.isActivated();
        links = new ArrayList<MenuLink>();
        for (int i = 0, j = vo.getLinks().size(); i < j; i++) {
            MenuLink link = vo.getLinks().get(i);
            links.add((MenuLink) link.clone());
        }
    }

    protected AbstractMenuHeader(MenuHeader vo, List<MenuLink> linkList) {
        super(vo);
        activated = vo.isActivated();
        links = new ArrayList<MenuLink>();
        for (int i = 0, j = linkList.size(); i < j; i++) {
            MenuLink link = linkList.get(i);
            links.add((MenuLink) link.clone());
        }
    }

    /**
     * Mapped constructor, copying all values found including id! You have to reset or reassign the id manually if value only copying intended.
     * @param map
     */
    protected AbstractMenuHeader(Map<String, Object> map) {
        super(map);
        activated = fetchBoolean(map, "activated");
        links = new ArrayList<MenuLink>();
        List<Map<String, Object>> mappedLinks = (List<Map<String, Object>>) map.get("links");
        if (mappedLinks != null) {
            for (int i = 0, j = mappedLinks.size(); i < j; i++) {
                Map<String, Object> mappedLink = mappedLinks.get(i);
                links.add(new MenuLinkImpl(mappedLink));
            }
        }
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "activated", activated);
        List<Map<String, Object>> mappedLinks = new ArrayList<Map<String, Object>>();
        for (int i = 0, j = links.size(); i < j; i++) {
            MenuLink menuLink = links.get(i);
            mappedLinks.add(menuLink.getMapped());
        }
        map.put("links", mappedLinks);
        return map;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public List<MenuLink> getLinks() {
        return links;
    }

    protected void setLinks(List<MenuLink> links) {
        this.links = links;
    }
    
    public void addLink(String url, String name, String permissions) {
        boolean added = false;
        for (int i = 0, j = links.size(); i < j; i++) {
            MenuLink link = links.get(i);
            if (link.getUrl().equals(url)) {
                added = true;
            }
        }
        if (!added) {
            MenuLink link = new MenuLinkImpl(links.size(), url, name, permissions);
            links.add(link);
        }
    }

    @Override
    public abstract Object clone();

}
