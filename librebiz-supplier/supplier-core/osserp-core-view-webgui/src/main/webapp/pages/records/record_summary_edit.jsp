<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<tr>
    <td><fmt:message key="overrideTaxCalculation" /></td>
    <td style="text-align: right;"><input type="checkbox" name="overrideTaxCalculation" /></td>
</tr>
<tr>
    <td><fmt:message key="net"/></td>
    <td style="text-align: right;"><input type="text" name="netValue" value="<o:number value="${record.amounts.amount}" format="currency"/>" readonly="readonly" disabled="disabled" style="width: 100px; margin-left: 20px; text-align: right;"/></td>
    <%-- <td style="text-align: right; padding-right: 15px;"><o:number value="${record.amounts.amount}" format="currency"/></td> --%>
</tr>
<c:if test="${record.amounts.reducedTaxAmount > 0}">
<tr>
    <td><fmt:message key="reducedTurnoverTax"/></td>
    <td style="text-align: right;"><input type="text" name="reducedTaxValue" value="<o:number value="${record.amounts.reducedTaxAmount}" format="currency"/>" style="width: 100px; margin-left: 20px; text-align: right;"/></td>
</tr>
</c:if>
<tr>
    <td><fmt:message key="turnoverTax"/></td>
    <td style="text-align: right;"><input type="text" name="taxValue" value="<o:number value="${record.amounts.taxAmount}" format="currency"/>" style="width: 100px; margin-left: 20px; text-align: right;"/></td>
</tr>
<tr>
    <td><fmt:message key="gross"/></td>
    <td style="text-align: right;"><input type="text" name="grossValue" value="<o:number value="${record.amounts.grossAmount}" format="currency"/>" style="width: 100px; margin-left: 20px; text-align: right;"/></td>
</tr>
