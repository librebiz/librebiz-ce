<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.timeRecordingView}"/>
<c:set var="booking" value="${view.selectedRecord}"/>
<c:if test="${!empty view and !empty view.selectedRecord}">
    <c:if test="${view.selectedRecord.correctionAvailable}">
        <c:set var="corrections" value="${view.recordCorrections}"/>
    </c:if>
    <table class="table" style="width: 450px; background-color: #FFFFFF; text-align:left;">
        <tr>
            <th class="small"><fmt:message key="id"/></th>
            <th class="small"><fmt:message key="date"/></th>
            <th class="small"><fmt:message key="start"/></th>
            <th class="small"><fmt:message key="end"/></th>
        </tr>
        <tr>
            <td colspan="2"><fmt:message key="noTypesAvailable"/></td>
        </tr>
        <c:if test="${!empty corrections}">
            <c:forEach var="obj" items="${startTypes}">
                <c:if test="${!intervalOnly or obj.interval}">
                    <tr>
                        <td><o:out value="${obj.id}"/></td>
                        <td><a href="<c:url value="/timeRecordings.do"><c:param name="method" value="selectType"/><c:param name="id" value="${obj.id}"/></c:url>"><o:out value="${obj.name}"/></a></td>
                    </tr>
                </c:if>
            </c:forEach>
        </c:if>
    </table>
</c:if>
